# -*- coding: latin1 -*-

"""
BR201 (PED): Sant Andreu de la Barca (outlayer de dias de demora)
BR149 00168 (ADL): Martorell Urba (Outlayer de dias de demora)
"""

import collections as c
from time import time
import sisapUtils as u
from sisaptools import years_between
from dateutil import relativedelta as rd
from datetime import datetime, date
import calendar
import csv
import itertools

# Si es volen actualitzar els �ltims 12 messos de la taula d'exadata, posar MODE = 1
# Si es vol crear la taula d'exadata des de zero, posar MODE = 0
MODE = 1

exadata = "exadata"
derivacions = "derivacions"
taula_exadata = "derivacions_indicadors"
taula_exadata_jail = taula_exadata + "_jail"
taula_derivacions = "exp_derivacions_indicadors"
taula_derivacions_jail = taula_derivacions + "_jail"
taula_derivacions_aj = "exp_derivacions_pre_indicadors_aj"
columnes_exadata = "(indicador varchar(21), codi_sector varchar(4), periode varchar(5), br varchar2(5), analisis varchar2(3), agrupador_prova varchar2(40), especialitat varchar2(40), br_desti varchar2(10), n number)"
columnes_derivacions = "(indicador varchar(21), codi_sector varchar(4), periode varchar(5), br varchar(5), analisis varchar(3), agrupador_prova varchar(40), especialitat varchar(40), br_desti varchar(10), n int)"  # noqa
columnes_derivacions_aj = "(indicador varchar(21), periode varchar(5), br varchar(5), analisis varchar(3), agrupador_prova varchar(40), especialitat varchar(40), br_desti varchar(10), n int)"  # noqa

# Creaci� de diccionari amb codis de especialitats redu�ts a strings de m�xim 6 car�cters
especialitats_6caracters = dict()
file = "./noesb/especialitats_6caracters.txt"
with open(file,'rb') as f:
    row = csv.reader(f, delimiter="@")
    for especialitat, especialitat_6caracters in row:
        especialitats_6caracters[especialitat] = especialitat_6caracters
        
# Dates d'extracci�

periodes_12m_gap0, periodes_12m_gap3 = c.OrderedDict(), c.OrderedDict()

sql = """
        SELECT
            data_ext
        FROM
            dextraccio
        """
dext, = u.getOne(sql, "nodrizas")
dext_dia, dext_mes, dext_any = dext.day, dext.month, dext.year

if dext_dia < 21:

    dext_menys2anys3mesos = dext - rd.relativedelta(months=27)
    dext_menys2anys3mesos = date(dext_menys2anys3mesos.year, dext_menys2anys3mesos.month, 1)  

    dext_menys1any3mesos = dext - rd.relativedelta(months=15)
    dext_menys1any3mesos = date(dext_menys1any3mesos.year, dext_menys1any3mesos.month, 1)          

    dext_menys1any = dext - rd.relativedelta(months=12)
    dext_menys1any = date(dext_menys1any.year, dext_menys1any.month, 1)                                    

    final_calcul_mensuals = date(dext_any, dext_mes-1, calendar.monthrange(dext_any, dext_mes-1)[1])

else:

    dext_menys2anys3mesos = dext - rd.relativedelta(months=26)
    dext_menys2anys3mesos = date(dext_menys2anys3mesos.year, dext_menys2anys3mesos.month, 1)

    dext_menys1any3mesos = dext - rd.relativedelta(months=14)
    dext_menys1any3mesos = date(dext_menys1any3mesos.year, dext_menys1any3mesos.month, 1)

    dext_menys1any = dext - rd.relativedelta(months=11)
    dext_menys1any = date(dext_menys1any.year, dext_menys1any.month, 1)                                       

    final_calcul_mensuals = dext

# Explorem els periodes als quals entraria una dada d'un mes concret al c�lcul d'un indicador anual

periode_actual = "A{}".format(final_calcul_mensuals.strftime("%y%m"))
periode_menys1any = "A{}".format(dext_menys1any.strftime("%y%m"))
rang_mesos = range(final_calcul_mensuals.month + 12 * (final_calcul_mensuals.year - 2019))

for mes in rang_mesos:
    periodes_12m_gap0["A{}".format(date(2019 + mes // 12, mes % 12 + 1, 1).strftime("%y%m"))] = ["A{}".format(date(2019 + mes_12m // 12, mes_12m % 12 + 1, 1).strftime("%y%m")) for mes_12m in range(mes, mes+12) if "A{}".format(date(2019 + mes_12m // 12, mes_12m % 12 + 1, 1).strftime("%y%m")) <= periode_actual]
    periodes_12m_gap3["A{}".format(date(2019 + mes // 12, mes % 12 + 1, 1).strftime("%y%m"))] = ["A{}".format(date(2019 + mes_12m // 12, mes_12m % 12 + 1, 1).strftime("%y%m")) for mes_12m in range(mes+3, mes+15) if "A{}".format(date(2019 + mes_12m // 12, mes_12m % 12 + 1, 1).strftime("%y%m")) <= periode_actual]

# Trobem el periode a partir del qual les dades ser�n eliminades a la taula derivacions_indicadors d'exadata
primer_periode = "A{}".format(dext_menys1any.strftime("%y%m"))

print("  - Data d'extraccio: {}".format(dext))

def get_centres():
    """ . """

    global centres_up2br, centres_br2up, centres_br2medea, centres_jail, centres_sectors

    centres_up2br = dict()
    centres_br2up = dict()
    centres_br2medea = dict()
    centres_sectors = dict()

    sql = """
            SELECT
                scs_codi,
                ics_codi,
                sector,
                medea
            FROM
                cat_centres
            UNION
            SELECT
                scs_codi,
                ics_codi,
                sector,
                medea
            FROM
                jail_centres
            """
    for up, br, codi_sector, medea in u.getAll(sql, "nodrizas"):
        centres_up2br[up] = {"br": br}
        centres_br2up[br] = {"up": up}
        centres_br2medea[br] = medea
        centres_sectors[up] = codi_sector
        centres_sectors[br] = codi_sector

    sql = """
            SELECT
                ics_codi
            FROM
                jail_centres
            """
    centres_jail = [br for br, in u.getAll(sql, "nodrizas")]

    print("  - Nombre de centres: {}".format(len(centres_up2br)))


def get_poblacio():
    """."""

    global poblacio, count_pobatesa_pedia, count_pobtotal_pedia, count_pobatesa_adults, count_pobtotal_adults

    poblacio = dict()
    count_pobatesa_pedia, count_pobtotal_pedia = c.defaultdict(c.Counter), c.defaultdict(c.Counter)
    count_pobatesa_adults, count_pobtotal_adults = c.defaultdict(c.Counter), c.defaultdict(c.Counter)

    sql = """
            SELECT
                id_cip_sec,
                up,
                data_naix,
                edat,
                sexe,
                ates
            FROM
                assignada_tot_with_jail
            """
    for id_cip_sec, up, data_naix, edat, sexe, ates in u.getAll(sql, "nodrizas"):
        poblacio[id_cip_sec] = {"up": up, "data_naix": data_naix, "edat": edat, "sexe": sexe, "ates": ates}
        sexe_k, edat_k = u.sexConverter(sexe), u.ageConverter(edat)
        medea = centres_br2medea[centres_up2br[up]["br"]]
        if ates:
            if edat < 15:
                count_pobatesa_pedia[up][(edat_k, sexe_k, medea)] += 1
            else:
                count_pobatesa_adults[up][(edat_k, sexe_k, medea)] += 1
        if edat < 15:
            count_pobtotal_pedia[up][(edat_k, sexe_k, medea)] += 1
        else:
            count_pobtotal_adults[up][(edat_k, sexe_k, medea)] += 1

    print("  - Poblaci� assignada carregada: {}".format(datetime.now()))

def create_tables():

    u.createTable(taula_derivacions_jail, columnes_derivacions, derivacions, rm=1)
    if MODE == 1:
        u.createTable(taula_derivacions_aj, columnes_derivacions_aj, derivacions, rm=1)

    u.execute("DROP TABLE IF EXISTS {_taula_derivacions}".format(_taula_derivacions=taula_derivacions), derivacions)
    particions_derivacions = ", ".join("PARTITION {_taula_derivacions}_{_sector} VALUES IN ('{_sector}')".format(_taula_derivacions=taula_derivacions, _sector=sector) for sector in u.sectors)
    sql = """
            CREATE TABLE {_taula_derivacions} 
            {_columnes_derivacions}
            PARTITION BY LIST COLUMNS (CODI_SECTOR) ({_particions_derivacions})
            """
    u.execute(sql.format(_taula_derivacions=taula_derivacions, _particions_derivacions=particions_derivacions, _columnes_derivacions=columnes_derivacions), derivacions)

    if MODE == 1:
        sql = "DELETE FROM {_taula_exadata} WHERE periode >= '{_primer_periode}'"
        u.execute(sql.format(_taula_exadata=taula_exadata, _primer_periode=primer_periode), exadata)
        u.execute(sql.format(_taula_exadata=taula_exadata_jail, _primer_periode=primer_periode), exadata)
        
    elif MODE == 0:
        u.createTable(taula_exadata_jail, columnes_exadata, exadata, rm=1)
        
        try:
            u.execute("DROP TABLE {_taula_exadata}".format(_taula_exadata=taula_exadata), exadata)
        except:
            pass
        particions_exadata = ", ".join("PARTITION {_taula_exadata}_{_sector} VALUES ('{_sector}')".format(_taula_exadata=taula_exadata, _sector=sector) for sector in u.sectors)
        sql = """
                CREATE TABLE {_taula_exadata}
                {_columnes_exadata}
                PARTITION BY LIST (CODI_SECTOR) ({_particions_exadata})
              """
        u.execute(sql.format(_taula_exadata=taula_exadata, _columnes_exadata=columnes_exadata, _particions_exadata=particions_exadata), exadata)

        u.grantSelect(taula_exadata, 'DWSISAP_ROL', exadata)
        u.grantSelect(taula_exadata_jail, 'DWSISAP_ROL', exadata)

def sub_get_derivacions(input):
    
    def add_indicador(indicador, codi_sector, periode_mensual, br, analisis, agrupador_prova, especialitat, br_desti, quantitat):
        for espec in (especialitat, "DERESPECIALITAT"):
            indicadors[(indicador, codi_sector, periode_mensual, br, analisis, agrupador_prova, espec, br_desti)] += quantitat

    def add_indicador_aj(indicador, periode_mensual, br, analisis, agrupador_prova, especialitat, br_desti, edat, sexe, medea, quantitat):
        if MODE == 1:
            for espec in (especialitat, "DERESPECIALITAT"):
                sub_pre_indicadors_aj[(indicador, periode_mensual, br, analisis, agrupador_prova, espec, br_desti, edat, sexe, medea)] += quantitat
                if periode_mensual in periodes_12m_gap0[periode_menys1any]:
                    sub_pre_indicadors_aj[(indicador+"_12M", periode_actual, br, analisis, agrupador_prova, espec, br_desti, edat, sexe, medea)] += quantitat

    tb, db = input
    
    tb_exadata = taula_exadata if db == "import" else taula_exadata_jail
    tb_derivacions = taula_derivacions if db == "import" else taula_derivacions_jail
    
    indicadors = c.Counter()
    indicadors_aj = c.Counter()
    sub_pre_indicadors_aj = c.Counter()
    sub_distincts_periode_mensual = set()
    distincts_br = set()
    
    oc_data_llindar_inicial = "BETWEEN STR_TO_DATE('{}', '%Y-%m-%d') AND".format(dext_menys1any) if MODE == 1 else "<="
    count_infrelacioderiv_null = 1
    
    sql = """
            SELECT
                id_cip_sec,
                inf_relacio_deriv,
                oc_up_ori,
                der_up_scs_des,
                oc_data,
                der_data_prog,
                der_data_real,
                codi_especialitat_final,
                CASE WHEN
                    agrupador_prova = 'Primera presencial' THEN 'PRESEN'
                    ELSE 'NOPRESEN' END agrupador_prova_curt,
                agrupador_rebuig,
                der_tipus_fluxe,
                inf_prioritat,
                flag_peticio_sollicitada,   -- DER01, DER02
                flag_peticio_tramitada,     -- DER04, DER05, DER06, DER07
                flag_peticio_pendent,       -- DER04
                flag_peticio_realitzada,    -- DER06 (DER05)
                flag_peticio_programada,    -- DER05
                flag_peticio_rebutjada      -- DER07
            FROM
                {}
            WHERE
                agrupador_prova IN ('Primera presencial', 'No presencial')
                AND especialitat_final IS NOT NULL
                AND NOT (der_data_prog IS NULL
                            AND der_data_real IS NOT NULL
                            AND inf_servei_d_codi IS NULL)
                AND oc_data {} STR_TO_DATE('{}', '%Y-%m-%d')
            """.format(tb, oc_data_llindar_inicial, final_calcul_mensuals)
    for (id_cip_sec, inf_relacio_deriv, up_origen, up_desti, oc_data, der_data_prog, der_data_real, especialitat, agrupador_prova_curt,
            agrupador_rebuig, fluxe, prioritat, FLAG_PETICIO_SOLLICITADA, FLAG_PETICIO_TRAMITADA, FLAG_PETICIO_PENDENT, \
            FLAG_PETICIO_REALITZADA, FLAG_PETICIO_PROGRAMADA, FLAG_PETICIO_REBUTJADA) in u.getAll(sql, db):
        if up_origen in centres_up2br and id_cip_sec in poblacio:
            codi_sector = centres_sectors[up_origen]
            data_naix, sexe = (poblacio[id_cip_sec][k] for k in ("data_naix", "sexe"))
            edat = years_between(data_naix, oc_data)
            sexe_k, edat_k = u.sexConverter(sexe), u.ageConverter(edat)
            inf_relacio_deriv = inf_relacio_deriv if inf_relacio_deriv else "NULL_" + codi_sector + "_" + str(count_infrelacioderiv_null).zfill(9)
            count_infrelacioderiv_null += 1
            sufix = "P" if 0 <= edat < 15 else ""
            pipa = True if fluxe == 'PIPA' else False
            periode_mensual = "A{}".format(oc_data.strftime("%y%m"))
            br_desti = "CDEST" + up_desti if up_desti else "CDESTSENSE"
            br = centres_up2br[up_origen]["br"]
            br = "BR405" if br == "BR408" else br
            medea = centres_br2medea[br]
            
            der_data_prog = der_data_prog if der_data_prog and der_data_prog.year < 9999 else None
            der_data_real = der_data_real if der_data_real else None
            especialitat = especialitats_6caracters.get(especialitat, 'DERALT')
            FLAG_PETICIO_PROGRAMADA = 1 if FLAG_PETICIO_PROGRAMADA and der_data_prog and der_data_prog.year < 9999 else 0
            
            dims_4 = (agrupador_prova_curt, "DERTIPUS")
            dims_6 = ('NACDEST', 'CDEST')
            
            if not (sufix == 'P' and br in centres_jail):

                sub_distincts_periode_mensual.add(periode_mensual)
                distincts_br.add(br)
                if FLAG_PETICIO_SOLLICITADA:
                    add_indicador("DERIV01{}".format(sufix), codi_sector, periode_mensual, br, "NUM", agrupador_prova_curt, especialitat, br_desti, 1)
                    for dim_4 in dims_4:
                        for dim_6 in dims_6:
                            add_indicador("DERIV02A{}".format(sufix), codi_sector, periode_mensual, br, "NUM", dim_4, especialitat, dim_6, 1)
                            add_indicador("DERIV02B{}".format(sufix), codi_sector, periode_mensual, br, "NUM", dim_4, especialitat, dim_6, 1)
                            add_indicador_aj("DERIV02A{}".format(sufix), periode_mensual, br, "NUM", dim_4, especialitat, dim_6, edat_k, sexe_k, medea, 1)
                            add_indicador_aj("DERIV02B{}".format(sufix), periode_mensual, br, "NUM", dim_4, especialitat, dim_6, edat_k, sexe_k, medea, 1)
                if FLAG_PETICIO_TRAMITADA and pipa:
                    if FLAG_PETICIO_PENDENT or \
                        (FLAG_PETICIO_REALITZADA and der_data_real >= oc_data) or \
                        (not FLAG_PETICIO_REALITZADA and FLAG_PETICIO_PROGRAMADA and der_data_prog >= oc_data):

                        add_indicador("DERIV04{}".format(sufix), codi_sector, periode_mensual, br, "DEN", agrupador_prova_curt, especialitat, br_desti, 1)
                        add_indicador("DERIV06{}".format(sufix), codi_sector, periode_mensual, br, "DEN", agrupador_prova_curt, especialitat, br_desti, 1)

                        if FLAG_PETICIO_PENDENT:
                            add_indicador("DERIV04{}".format(sufix), codi_sector, periode_mensual, br, "NUM", agrupador_prova_curt, especialitat, br_desti, 1)
                        dies_demora = 0
                        if FLAG_PETICIO_REALITZADA and der_data_real >= oc_data:
                            dies_demora = u.daysBetween(oc_data, der_data_real)
                            add_indicador("DERIV06{}".format(sufix), codi_sector, periode_mensual, br, "NUM", agrupador_prova_curt, especialitat, br_desti, 1)
                        elif not FLAG_PETICIO_REALITZADA and FLAG_PETICIO_PROGRAMADA and der_data_prog >= oc_data:
                            dies_demora = u.daysBetween(oc_data, der_data_prog)
                        if not FLAG_PETICIO_REBUTJADA:      
                            if dies_demora < 365*3 and not FLAG_PETICIO_PENDENT:
                                add_indicador("DERIV05{}".format(sufix), codi_sector, periode_mensual, br, "DEN", agrupador_prova_curt, especialitat, br_desti, 1)
                                add_indicador("DERIV05{}".format(sufix), codi_sector, periode_mensual, br, "NUM", agrupador_prova_curt, especialitat, br_desti, dies_demora)
                                if prioritat in ("P", "U", "O"):
                                    if prioritat in ("P", "U"):
                                        prioritat = "U"
                                    add_indicador("DERIV05{}{}".format(sufix, prioritat), codi_sector, periode_mensual, br, "DEN", agrupador_prova_curt, especialitat, br_desti, 1)
                                    add_indicador("DERIV05{}{}".format(sufix, prioritat), codi_sector, periode_mensual, br, "NUM", agrupador_prova_curt, especialitat, br_desti, dies_demora)

                    add_indicador("DERIV07A{}".format(sufix), codi_sector, periode_mensual, br, "DEN", agrupador_prova_curt, especialitat, br_desti, 1)
                    add_indicador("DERIV07B{}".format(sufix), codi_sector, periode_mensual, br, "DEN", agrupador_prova_curt, especialitat, br_desti, 1)                                                      
                    if FLAG_PETICIO_REBUTJADA:
                        if agrupador_rebuig == "REBUIG_HOSPITAL":
                            add_indicador("DERIV07A{}".format(sufix), codi_sector, periode_mensual, br, "NUM", agrupador_prova_curt, especialitat, br_desti, 1)
                        elif agrupador_rebuig == "REBUIG_PACIENT":
                            add_indicador("DERIV07B{}".format(sufix), codi_sector, periode_mensual, br, "NUM", agrupador_prova_curt, especialitat, br_desti, 1)

    indicadors_deriv02_afegir = c.Counter()
    indicadors_deriv02_afegir_aj = c.Counter()
    
    # Afegir DENominadors faltants

    for indicador in ('DERIV02B','DERIV02A','DERIV02AP','DERIV02BP'):
        for periode_mensual in sub_distincts_periode_mensual:
            for br in distincts_br:
                codi_sector = centres_sectors[br]
                if not (indicador[-1] == 'P' and br in centres_jail):

                    for especialitat in itertools.chain(especialitats_6caracters.values(), ["DERALT", "DERESPECIALITAT"]):

                        dims_4 = ("PRESEN", "NOPRESEN", "DERTIPUS")
                        dims_6 = ('NACDEST', 'CDEST')

                        for dim_4 in dims_4:
                            for dim_6 in dims_6:
                                if br in centres_br2up:
                                    obj = None
                                    indicador_pare = indicador[:8]
                                    up = centres_br2up[br]["up"]
                                    if indicador_pare == "DERIV02A":
                                        if len(indicador) == 8:
                                            obj = count_pobatesa_adults[up]
                                        elif len(indicador) == 9 and indicador[8] == "P":    
                                            obj = count_pobatesa_pedia[up]
                                    elif indicador_pare == "DERIV02B":
                                        if len(indicador) == 8:
                                            obj = count_pobtotal_adults[up]
                                        elif len(indicador) == 9 and indicador[8] == "P":
                                            obj = count_pobtotal_pedia[up]
                                    if obj is not None:
                                        if (indicador, codi_sector, periode_mensual, br, "DEN", dim_4, especialitat, dim_6) not in indicadors:
                                            indicadors[(indicador, codi_sector, periode_mensual, br, "DEN", dim_4, especialitat, dim_6)] = sum(obj.values())
                                        if MODE == 1:
                                            for k, quantitat in obj.items():
                                                if ((indicador, periode_mensual, br, "DEN", dim_4, especialitat, dim_6) + k) not in sub_pre_indicadors_aj:
                                                    sub_pre_indicadors_aj[(indicador, periode_mensual, br, "DEN", dim_4, especialitat, dim_6) + k] = quantitat
                                                    if periode_mensual in periodes_12m_gap0[periode_menys1any]:
                                                        sub_pre_indicadors_aj[(indicador+"_12M", periode_actual, br, "DEN", dim_4, especialitat, dim_6) + k] += quantitat
        
    # Enviar dades a les taules
    resultats = [
        [indicador,
        codi_sector,
        periode,
        br_origen,
        analisis,
        agrupador_prova,
        especialitat,
        br_desti,
        n] for (indicador,
                codi_sector,
                periode,
                br_origen,
                analisis,
                agrupador_prova,
                especialitat,
                br_desti), n in indicadors.items()]
    u.listToTable(resultats, tb_exadata, exadata)
    u.listToTable(resultats, tb_derivacions, derivacions)

    return sub_pre_indicadors_aj, sub_distincts_periode_mensual

def get_derivacions():
    """."""

    global distincts_periode_mensual
    taules = [("derivacions", "import_jail")]
    taules.extend([(tb, "import") for tb in u.getSubTables("derivacions")])
    pre_indicadors_aj = c.Counter()
    indicadors_aj = c.Counter()
    factors_aj = c.defaultdict(c.Counter)
    distincts_periode_mensual = set()

    for sub_pre_indicadors_aj, sub_distincts_periode_mensual in u.multiprocess(sub_get_derivacions, ((tb, db) for tb, db in taules), 8):
        distincts_periode_mensual.update(sub_distincts_periode_mensual)
        if MODE == 1:
            for key, quantitat in sub_pre_indicadors_aj.items():
                if key[3] == "DEN":
                    pre_indicadors_aj[key] = quantitat
                elif key[3] == "NUM":
                    pre_indicadors_aj[key] += quantitat

    if MODE == 1:

        for (indicador, periode, br, analisi, agrupador_prova, especialitat, br_desti, edat, sexe, medea), n in pre_indicadors_aj.items():
            factors_aj[(indicador, periode, agrupador_prova, especialitat, br_desti, edat, sexe, medea)][analisi] += n
        pesos_aj = {key: (values["NUM"] / float(values["DEN"]) if values["DEN"] else 0)
                 for (key, values) in factors_aj.items()}
        for (indicador, periode_mensual, br_origen, analisis, agrupador_prova, especialitat, br_desti, edat, sexe, medea), quantitat in pre_indicadors_aj.items():
            if analisis == "DEN":
                pes = pesos_aj[(indicador, periode_mensual, agrupador_prova, especialitat, br_desti, edat, sexe, medea)]
            else:
                pes = 1
            indicadors_aj[(indicador+"_AJ", periode_mensual, br_origen, analisis, agrupador_prova, especialitat, br_desti)] += quantitat * pes

        resultats_aj = [
            [indicador,
            periode,
            br_origen,
            analisis,
            agrupador_prova,
            especialitat,
            br_desti,
            n] for (indicador,
                    periode,
                    br_origen,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti), n in indicadors_aj.items()]
        u.listToTable(resultats_aj, taula_derivacions_aj, derivacions)

def sub_get_anuals(sector):
    
    
    tb_exadata = taula_exadata if sector != "6951" else taula_exadata_jail
    tb_derivacions = taula_derivacions if sector != "6951" else taula_derivacions_jail
    
    indicadors = c.Counter()
    distincts_periode = set()
    distincts_br = set()
    
    if tb_exadata == taula_exadata:
        sql = """
                SELECT
                    *
                FROM
                    dwsisap.{}
                PARTITION ({})
                """.format(tb_exadata, tb_exadata+"_"+sector)
        
    else: # tb_exadata == taula_exadata_jail
        sql = """
                SELECT
                    *
                FROM
                    dwsisap.{}
                """.format(tb_exadata)
    
    for indicador, codi_sector, periode, br, analisis, agrupador_prova, especialitat, br_desti, n in u.getAll(sql, "exadata"):
        distincts_periode.add(periode)
        distincts_br.add(br)
        indicador_pare = indicador[:8] if indicador[:7] == "DERIV02" else indicador[:7]
        br = "BR405" if br == "BR408" else br
        if br in centres_br2up and not (indicador[-1] == 'P' and br in centres_jail):
            up = centres_br2up[br]["up"]
            if not "_12M" in indicador:
                if indicador_pare in ("DERIV01", "DERIV02A", "DERIV02B"):
                    if periode in periodes_12m_gap0:
                        for periode_12m in periodes_12m_gap0[periode]:
                            if (periode_12m >= periode_menys1any and MODE == 1) or (periode_12m >= "A1912" and MODE == 0):
                                if not (indicador_pare in ("DERIV02A", "DERIV02B") and analisis == "DEN"):
                                    indicadors[(indicador+"_12M", codi_sector, periode_12m, br, analisis, agrupador_prova, especialitat, br_desti)] += n
                elif indicador_pare not in ("DERIV01", "DERIV02"):
                    if periode in periodes_12m_gap3:
                        for periode_12m in periodes_12m_gap3[periode]:
                            if (periode_12m >= periode_menys1any and MODE == 1) or (periode_12m >= "A2003" and MODE == 0):
                                indicadors[(indicador+"_12M", codi_sector, periode_12m, br, analisis, agrupador_prova, especialitat, br_desti)] += n
      
    # Afegir DENominadors faltants    

    for indicador in ('DERIV02B','DERIV02A','DERIV02AP','DERIV02BP'):
        for periode_mensual in distincts_periode_mensual:
            for br in distincts_br:
                codi_sector = centres_sectors[br]
                if not (indicador[-1] == 'P' and br in centres_jail):
                    for especialitat in itertools.chain(especialitats_6caracters.values(), ["DERALT", "DERESPECIALITAT"]):

                        dims_4 = ("PRESEN", "NOPRESEN", "DERTIPUS")
                        dims_6 = ("NACDEST", "CDEST")

                        for dim_4 in dims_4:
                            for dim_6 in dims_6:

                                if (indicador+"_12M", codi_sector, periode_mensual, br, "DEN", dim_4, especialitat, dim_6) not in indicadors:
                                    indicador_pare = indicador[:8]
                                    br = "BR405" if br == "BR408" else br
                                    if br in centres_br2up:
                                        up = centres_br2up[br]["up"]
                                        if indicador_pare == "DERIV02A":
                                            if len(indicador) == 8:
                                                indicadors[(indicador+"_12M", codi_sector, periode_mensual, br, "DEN", dim_4, especialitat, dim_6)] = sum(count_pobatesa_adults[up].values())
                                            elif len(indicador) == 9:
                                                if indicador[8] == "P":
                                                    indicadors[(indicador+"_12M",codi_sector, periode_mensual, br, "DEN", dim_4, especialitat, dim_6)] = sum(count_pobatesa_pedia[up].values())                        
                                        elif indicador_pare == "DERIV02B":
                                            if len(indicador) == 8:
                                                indicadors[(indicador+"_12M",codi_sector, periode_mensual, br, "DEN", dim_4, especialitat, dim_6)] = sum(count_pobtotal_adults[up].values())
                                            elif len(indicador) == 9:
                                                if indicador[8] == "P":
                                                    indicadors[(indicador+"_12M",codi_sector, periode_mensual, br, "DEN", dim_4, especialitat, dim_6)] = sum(count_pobtotal_pedia[up].values())
   
    # Enviar dades a les taules

    resultats = [
        [indicador,
        codi_sector,
        periode,
        br_origen,
        analisis,
        agrupador_prova,
        especialitat,
        br_desti,
        n] for (indicador,
                codi_sector,
                periode,
                br_origen,
                analisis,
                agrupador_prova,
                especialitat,
                br_desti), n in indicadors.items()]
    u.listToTable(resultats, tb_exadata, exadata)
    u.listToTable(resultats, tb_derivacions, derivacions)
    
def get_anuals():
    
    sectors = u.sectors
    sectors.append("6951")

    u.multiprocess(sub_get_anuals, sectors, 8)

def export_khalix_mode1():
    """."""

    file_1 = "DERIVINDTAX"

    periodes = {"_1R": tuple(periodes_12m_gap0[periode_menys1any][:4]),
                "_2N": tuple(periodes_12m_gap0[periode_menys1any][4:8]),
                "_3R":tuple(periodes_12m_gap0[periode_menys1any][8:12])}

    sql_1 = """
                SELECT DISTINCT
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti, 
                    'N',
                    n
                FROM
                    {_derivacions}.{_taula_derivacions}
                WHERE
                    indicador < 'DERIV04'
                    AND periode in {_condicio}
                    AND analisis = 'DEN'
                UNION
                SELECT DISTINCT
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti, 
                    'N',
                    sum(n)
                FROM
                    {_derivacions}.{_taula_derivacions}
                WHERE
                    indicador < 'DERIV04'
                    AND periode in {_condicio}
                    AND analisis = 'NUM'
                GROUP BY
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti
            """
    for semestre, condicio in periodes.items():
        u.exportKhalix(sql_1.format(_derivacions=derivacions, _taula_derivacions=taula_derivacions, _condicio=condicio), file_1+semestre)               

    file_2 = "DERIVIND"
    sql_2 = """
                SELECT DISTINCT
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti, 
                    'N',
                    n
                FROM
                    {_derivacions}.{_taula_derivacions}
                WHERE
                    indicador >= 'DERIV04'
                GROUP BY
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti
            """.format(_derivacions=derivacions, _taula_derivacions=taula_derivacions)
    u.exportKhalix(sql_2, file_2)


    file_3 = "DERIVINDTAX_JAIL"
    sql_3 = """
                SELECT DISTINCT
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti, 
                    'N',
                    n
                FROM
                    {}.{}
                WHERE
                    indicador < 'DERIV04'
            """.format(derivacions, taula_derivacions_jail)
    u.exportKhalix(sql_3, file_3)

    file_4 = "DERIVIND_JAIL"
    sql_4 = """
                SELECT DISTINCT
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti, 
                    'N',
                    n
                FROM
                    {}.{}
                WHERE
                    indicador >= 'DERIV04'
            """.format(derivacions, taula_derivacions_jail)
    u.exportKhalix(sql_4, file_4)

    if MODE == 1:
        file_3 = "DERIVINDAJ_MENSUAL"
        sql_3 = """
                    SELECT DISTINCT
                        indicador,
                        periode,
                        br,
                        analisis,
                        agrupador_prova,
                        especialitat,
                        br_desti,  
                        'N',
                        n
                    FROM
                        {}.{}
                    inner join nodrizas.cat_Centres
                        on br = ics_codi
                    WHERE
                        indicador in ('DERIV02A_AJ', 'DERIV02AP_AJ')
                """.format(derivacions, taula_derivacions_aj)
        u.exportKhalix(sql_3, file_3)

        file_4 = "DERIVINDAJ_ANUAL"
        sql_4 = """
                    SELECT DISTINCT
                        indicador,
                        periode,
                        br,
                        analisis,
                        agrupador_prova,
                        especialitat,
                        br_desti, 
                        'N',
                        n
                    FROM
                        {}.{}
                    inner join nodrizas.cat_Centres
                        on br = ics_codi
                    WHERE
                        indicador in ('DERIV02A_12M_AJ', 'DERIV02AP_12M_AJ')
                """.format(derivacions, taula_derivacions_aj)
        u.exportKhalix(sql_4, file_4)


if (MODE == 1 and u.IS_MENSUAL) or MODE == 0:

    get_centres();                              print("Done: get_centres()")
    get_poblacio();                             print("Done: get_poblacio()")
    create_tables();                            print("Done: create_tables()")
    get_derivacions();                          print("Done: get_derivacions()")
    get_anuals();                               print("Done: get_anuals()")
    if MODE == 1:
        export_khalix_mode1();                  print("Done: export_khalix_mode1()")
    # elif MODE == 0:
    #     export_khalix_mode0();                  print("Done: export_khalix_mode0()")