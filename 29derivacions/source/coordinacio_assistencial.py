# coding: latin1

import sisapUtils as u


class Structures():
    def __init__(self):
        # self.create_uba()
        # self.create_up()
        self.create_catalogue()
    
    def create_uba(self):
        cols = """
            (indicador varchar2(64),
            periode varchar2(32), 
            br varchar2(5), 
            tipus_prof varchar2(1),
            uba varchar2(5),
            num number(38,0), 
            den number(38,0),
            up varchar2(5),
            up_des varchar2(255),
            abs_cod varchar2(5),
            abs_des varchar2(255),
            aga_cod varchar2(5),
            AGA_DES varchar2(255),
            REGIO_COD varchar2(5),
            REGIO_DES varchar2(255),
            SAP_COD varchar2(5),
            SAP_DES varchar2(255),
            ambit_cod varchar2(5),
            ambit_des varchar2(255),
            gap_cod number,
            gap_des varchar2(255),
            medea varchar2(5),
            tip_cod varchar2(10),
            tip_des varchar2(100),
            SUBTIP_COD varchar2(10),
            SUBTIP_DES varchar2(100))
        """
        u.createTable('coordinacio_uba', cols, 'exadata', rm=True)
        u.grantSelect('coordinacio_uba', ('DWSISAP_ROL'), 'exadata')
    
    def create_up(self):
        cols = """
            (indicador varchar2(64),
            periode varchar2(32), 
            br varchar2(5), 
            dim1 varchar2(32),
            dim2 varchar2(32),
            dim3 varchar2(32),
            num number(38,0), 
            den number(38,0),
            up varchar2(5),
            up_des varchar2(255),
            abs_cod varchar2(5),
            abs_des varchar2(255),
            aga_cod varchar2(5),
            AGA_DES varchar2(255),
            REGIO_COD varchar2(5),
            REGIO_DES varchar2(255),
            SAP_COD varchar2(5),
            SAP_DES varchar2(255),
            ambit_cod varchar2(5),
            ambit_des varchar2(255),
            gap_cod number,
            gap_des varchar2(255),
            medea varchar2(5),
            tip_cod varchar2(10),
            tip_des varchar2(100),
            SUBTIP_COD varchar2(10),
            SUBTIP_DES varchar2(100))
        """
        u.createTable('coordinacio_up', cols, 'exadata', rm=True)
        u.grantSelect('coordinacio_up', ('DWSISAP_ROL'), 'exadata')
    
    def create_catalogue(self):
        cols = '(pantalla varchar2(50), indicador varchar2(50), desripcio varchar2(500), responsable varchar2(50), link_umi varchar2(250))'
        u.createTable('coordinacio_cat', cols, 'exadata', rm=True)
        u.grantSelect('coordinacio_cat', ('DWSISAP_ROL'), 'exadata')
        data = [
            ('ATDOM', 'ATD057A','Taxa de caigudes per cada 100 ATDOM','SISAP'),
            ('ATDOM', 'ATD062A',"""Taxa bruta d'ingressos per cada 100 ATDOM""",'SISAP'),
            # ('ATDOM', 'ATD073A','Poblaci� ATDOM amb valoraci� del risc social','SISAP'),
            ('ATDOM', 'ATD078A','Percentatge de persones ATDOM visitades a urg�ncies hospital�ries o CUAP o PAC','SISAP'),
            ('ATDOM', 'ATD078AA','Percentatge de persones ATDOM visitades a urg�ncies hospital�ries','SISAP'),
            ('ATDOM', 'ATD078AB','Percentatge de persones ATDOM visitades a urg�ncies CUAP o PAC','SISAP'),
            ('ATDOM', 'ATD079A',"""Percentatge de persones ATDOM que han ingressat a l'Hospital""",'SISAP'),
            ('ATDOM', 'ATD080A',"""Percentatge de persones ATDOM que reingressa a l'hospital""",'SISAP'),
            ('ATDOM', 'ATD081A','Mitjana de "dies a casa" de persones ATDOM','SISAP'),
            ('ATDOM', 'ATD082A',"""Mitjana de dies d'ingr�s hospitalari de les persones ATDOM""",'SISAP'),
            ('ATDOM', 'ATD083A',"""% d'�xitus durant l'ingr�s hospitalari de les persones ATDOM""",'SISAP'),
            ('ATDOM', 'ATD084A','Percentatge de persones ATDOM amb ingr�s a interm�dia','SISAP'),
            ('ATDOM', 'ATD085A',"""Taxa bruta d'ingressos a interm�dia per cada 100 persones ATDOM""",'SISAP'),
            # ('Gesti� de casos', 'GC0000',"""Pacients inclosos en gesti� de casos""",'SISAP'),
            # ('Gesti� de casos', 'GC0001',"""Pacients en gesti� de casos amb pla de cures""",'SISAP'),
            # ('Gesti� de casos', 'GC0001A',"""Pacients en gesti� de casos amb pla de cures cr�nic""",'SISAP'),
            # ('Gesti� de casos', 'GC0001B',"""Pacients en gesti� de casos amb pla de cures agut""",'SISAP'),
            # ('Gesti� de casos', 'GC0002',"""Pacients en gesti� de casos amb valoraci� integral""",'SISAP'),
            # ('Gesti� de casos', 'GC0003',"""Pacients en gesti� de casos amb registre d'adher�ncia al tractament""",'SISAP'),
            # ('Gesti� de casos', 'GC0004',"""Pacients en gesti� de casos amb cribratge de caigudes""",'SISAP'),
            # ('Gesti� de casos', 'GC0005',"""Pacients en gesti� de casos PCC o MACA amb PIIC""",'SISAP'),
            # ('Gesti� de casos', 'GC0006',"""Pacients en gesti� de casos MACA amb pla de decisions anticipades""",'SISAP'),
            # ('Gesti� de casos', 'GC0007',"""Pacients en gesti� de casos amb registre de l'estat nutricional""",'SISAP'),
            # ('Gesti� de casos', 'GC0008',"""Freq�entaci� dels pacients en gesti� de casos""",'SISAP'),
            # ('Gesti� de casos', 'GC0009',"""Pacients en gesti� de casos inclosos en el programa ATDOM""",'SISAP'),
            # ('Gesti� de casos', 'GC0010',"""Pacients en gesti� de casos amb c�lcul del grau de complexitat""",'SISAP'),
            # ('Gesti� de casos', 'GC0010A',"""Pacients en gesti� de casos amb complexitat alta""",'SISAP'),
            # ('Gesti� de casos', 'GC0010B',"""Pacients en gesti� de casos amb complexitat mitja""",'SISAP'),
            # ('Gesti� de casos', 'GC0010C',"""Pacients en gesti� de casos amb complexitat baixa""",'SISAP'),
            # ('Gesti� de casos', 'GC0011',"""Pacients en gesti� de casos amb cuidador principal identificat""",'SISAP'),
            # ('Gesti� de casos', 'GC0011A',"""Pacients en gesti� de casos amb valoraci� de l'entorn del cuidador""",'SISAP'),
            # ('Gesti� de casos', 'GC0012',"""Pacients en gesti� de casos amb valoraci� de la qualitat de vida""",'SISAP'),
            # ('Gesti� de casos', 'GC0013',"""Seguiment de pacients amb MPOC en gesti� de casos""",'SISAP'),
            # ('Gesti� de casos', 'GC0014',"""Seguiment de pacients amb Insufici�ncia Card�aca en gesti� de casos""",'SISAP'),
            # ('Gesti� de casos', 'GC0015',"""Visites totals dels pacients en gesti� de casos""",'SISAP'),
            # ('Gesti� de casos', 'GC0015A',"""Percentatge de visites presencials dels pacients en gesti� de casos""",'SISAP'),
            # ('Gesti� de casos', 'GC0015B',"""Percentatge de visites presencials dels pacients en gesti� de casos""",'SISAP'),
            # ('Gesti� de casos', 'GC0015C',"""Percentatge de visites domicili�ries dels pacients en gesti� de casos""",'SISAP'),
            # ('Gesti� de casos', 'GC0015D',"""Percentatge de visites telef�niques dels pacients en gesti� de casos""",'SISAP'),
            # ('Gesti� de casos', 'GC0016',"""Percentatge de pacients en gesti� de casos amb derivaci�""",'SISAP'),
            # ('Gesti� de casos', 'GC0017',"""Percentatge de pacients amb derivaci� que no estan en gesti� de casos""",'SISAP'),
            # ('Gesti� de casos', 'GC0018',"""Percentatge de pacients en gesti� de casos amb coordinaci�""",'SISAP'),
            # ('Gesti� de casos', 'GC0019',"""Percentatge de pacients amb coordinaci� que no estan en gesti� de casos""",'SISAP'),
            ('Gesti� de casos', 'GC0020',"""Pacients en gesti� de casos PCC amb ingr�s a l'hospital""",'SISAP'),
            ('Gesti� de casos', 'GC0021',"""Pacients en gesti� de casos MACA amb ingr�s a l'hospital""",'SISAP'),
            ('Gesti� de casos', 'GC0022',"""Pacients en gesti� de casos PCC que reingressa a l'hospital""",'SISAP'),
            ('Gesti� de casos', 'GC0023',"""Pacients en gesti� de casos MACA que reingressa a l'hospital""",'SISAP'),
            ('IT', 'QCIT001', 'Durada mitjana de les IT','SISAP'),
            ('IT', 'QCIT001_ALT', 'Durada mitjana de les IT (altres)','SISAP'),
            ('IT', 'QCIT001_OST', 'Durada mitjana de les IT (osteomuscular)','SISAP'),
            ('IT', 'QCIT001_SG', 'Durada mitjana de les IT (signes diversos)','SISAP'),
            ('IT', 'QCIT001_SM', 'Durada mitjana de les IT (Salut Mental)','SISAP'),
            ('IT', 'QCIT001_TRAU', 'Durada mitjana de les IT (Traumatologia)','SISAP'),
            ('IT', 'QCIT002', 'Dies d\'IT per poblaci� activa','SISAP'),
            ('IT', 'QCIT002_ALT', 'Dies d\'IT per poblaci� activa (altres)','SISAP'),
            ('IT', 'QCIT002_OST', 'Dies d\'IT per poblaci� activa (osteomuscular)','SISAP'),
            ('IT', 'QCIT002_SG', 'Dies d\'IT per poblaci� activa (signes diversos)','SISAP'),
            ('IT', 'QCIT002_SM', 'Dies d\'IT per poblaci� activa (Salut Mental)','SISAP'),
            ('IT', 'QCIT002_TRAU', 'Dies d\'IT per poblaci� activa (Traumatologia)','SISAP'),
            ('IT', 'QCIT003', 'Incid�ncia d\'IT per poblaci� activa','SISAP'),
            ('IT', 'QCIT003_ALT', 'Incid�ncia d\'IT per poblaci� activa (altres)','SISAP'),
            ('IT', 'QCIT003_OST', 'Incid�ncia d\'IT per poblaci� activa (osteomuscular)','SISAP'),
            ('IT', 'QCIT003_SG', 'Incid�ncia d\'IT per poblaci� activa (signes diversos)','SISAP'),
            ('IT', 'QCIT003_SM', 'Incid�ncia d\'IT per poblaci� activa (Salut Mental)','SISAP'),
            ('IT', 'QCIT003_TRAU', 'Incid�ncia d\'IT per poblaci� activa (Traumatologia)','SISAP'),
            ('IT', 'QCITPAM01', 'Percentatge de PAM contestades en 3 dies','SISAP'),
            ('IT', 'QCITPAM02', 'Percentatge de PAM contestades en 3 dies i amb alta del pacient','SISAP'),
            ('IT', 'QCITPRO', 'Percentatge de pacients amb una baixa(IT) > 5 dies que tenen almenys una visita amb data de sol�licitud igual a la data de baixa','SISAP'),
            ('IQF', 'MATMA', 'Utilitzaci� medicaments sense valor terapeutic afegit amb alternatives mes adequades (llista fixa)','SISAP'),
            ('IQF', 'MATMA_DINAMICA', 'Utilitzaci� medicaments sense valor terapeutic afegit amb alternatives mes adequades (llista din�mica)(envasos)','SISAP'),
            ('IQF', 'EPILEPSIA', "Perampanel respecte al total d'antiepil�ptics",'SISAP'),
            ('IQF', 'HIPERK', 'F�rmacs nous per a la hiperpotass�mia respecte al total de f�rmacs per a la hiperpotass�mia','SISAP'),
            ('IQF', 'GLARGINA', 'Utilitzaci� d insulina glargina biosimilar','SISAP'),
            ('IQF', 'ENOXAPARINA', 'Utilitzaci� d enoxaparina biosimilar','SISAP'),
            ('IQF', 'TERIPARATIDA', 'Utilitzaci� de teriparatida biosimilar i gen�rica','SISAP'),
            ('IQF', 'BIOSIMILARS', 'Utilitzaci� de medicaments biosimilars (AGA)','SISAP'),
            ('IQF', 'BIOSIMILARS_EAP', 'Utilitzaci� de medicaments biosimilars (envasos)','SISAP'),
            ('IQF', 'DHD_ST_AINES', 'Hiperprescripci� d\'AINE i d\'altres medicaments per a patologies musculoesqueletiques','SISAP'),
            ('IQF', 'DHD_ST_SYSADOA', 'Hiperprescripci� de farmacs d\'acci� lenta sobre els smptomes de l artrosi (SYSADOA)','SISAP'),
            ('IQF', 'DHD_ST_ANTIULCEROSOS', 'Hiperprescripci� d\'antiulcerosos','SISAP'),
            ('IQF', 'DHD_ST_BENZODIAZEPINES', 'Hiperprescripci� de benzodiazepines i farmacs relacionats','SISAP'),
            ('IQF', 'DHD_ST_ANTIBACTERIANS', 'Hiperprescripci� d\'antibacterians d us sistemic','SISAP'),
            ('IQF', 'DHD_ST_ANTIESPASMODICS', 'Hiperprescripci� d\'antiespasm�dics urinaris','SISAP'),
            ('IQF', 'ANTIHIPERTENSIUS', 'Utilitzaci� d\'antihipertensius recomanats','SISAP'),
            ('IQF', 'IBP', 'Utilitzaci� d\'inhibidors de la bomba de protons recomanats','SISAP'),
            ('IQF', 'OSTEOPOROSI', 'Utilitzaci� de medicaments per a l\'osteoporosi recomanats','SISAP'),
            ('IQF', 'HIPOCOLESTEROLEMIANTS', 'Utilitzaci� d\'hipocolesterolemiants recomanats','SISAP'),
            ('IQF', 'ANTIDEPRESSIUS_1A', 'Utilitzaci� d\'antidepressius de primera elecci� recomanats','SISAP'),
            ('IQF', 'ANTIDEPRESSIUS_2A', 'Utilitzaci� d\'antidepressius no ISRS de segona elecci� recomanats','SISAP'),
            ('IQF', 'MPOC_SELECCIO', 'Utilitzaci� de ter�pies inhalades triples amb LAMA, LABA i CI en pacients amb diagnostic de MPOC','SISAP'),
            ('IQF', 'HIPOGLUCEMIANTS_BITERAPIA_REC', 'Combinacions recomanades de dos HNI en pacients DM2','SISAP'),
            ('IQF', 'HIPOGLUCEMIANTS_BITERAPIA', 'Paciemtn amb DM2 amb 2 o m�s HNI','SISAP'),
            ('IQF', 'HIPOGLUCEMIANTS_MONOTERAPIA_REC', 'F�rmacs recomanats en pacients DM2 amb un HNI','SISAP'),
            ('IQF', 'HIPOGLUCEMIANTS_MONOTERAPIA', 'Pacient amb DM2 amb monoter�pia HNI','SISAP'),
            ('Gesti� de casos', 'PREALT', 'Altes hospital�ries de la poblaci� assignada incloses en el protocol PREALT amb contacte amb l\'EAP abans de 48 hores des de la data de registre de l\'alta a l\'ECAP (s\'exclouen els diumenges).','SISAP')  
            ]
        # cataleg
        ind = ['DERIV05_12M',
                'DERIV05O_12M',
                'DERIV05U_12M',
                'DERIV01_12M',
                'DERIV02A_12M',
                'DERIV02B_12M',
                'DERIV04_12M',
                'DERIV05_12M',
                'DERIV06_12M',
                'DERIV07A_12M',
                'DERIV07B_12M',
                'DERIV04',
                'DERIV05',
                'DERIV05O',
                'DERIV05U',
                'DERIV06',
                'DERIV07A',
                'DERIV07B']
        umi = "select nom, descripcio from indicador_indicador \
               where nom in {}".format(tuple(ind))
        cat = [('Derivacions', ind, nom, 'SISAP') for (ind, nom) in u.getAll(umi, ("umi", "x0001"))]
        ind = [
                'PROV07B_12M',
                'PROV05P_12M',
                'PROV07AP',
                'PROV07BP',
                'PROV05',
                'PROV05O',
                'PROV07B',
                'PROV05O_12M',
                'PROV05P',
                'PROV07A_12M',
                'PROV07AP_12M',
                'PROV05PO_12M',
                'PROV06P_12M',
                'PROV05PO',
                'PROV05_12M',
                'PROV05PU',
                'PROV07BP_12M',
                'PROV07A',
                'PROV04P_12M',
                'PROV05U_12M',
                'PROV04',
                'PROV06_12M',
                'PROV05U',
                'PROV06P',
                'PROV04P',
                'PROV04_12M',
                'PROV05PU_12M',
                'PROV06'
        ]
        umi = "select nom, descripcio from indicador_indicador \
               where nom in {}".format(tuple(ind))
        cat1 = [('Proves', ind, nom, 'SISAP') for (ind, nom) in u.getAll(umi, ("umi", "x0001"))]
        data = data + cat + cat1
        upload = []
        link = 'http://10.80.217.201/sisap-umi/indicador/codi/'
        for (pantalla, ind, desc, resp) in data:
            upload.append((pantalla, ind, desc, resp, link + ind))
        u.listToTable(upload, 'coordinacio_cat', 'exadata')


class Indicadors():
    def __init__(self, dates):
        self.periode = dates
        self.get_centres()
        self.get_data_uba()
        self.get_data_up()
        
    def get_centres(self):
        sql = """SELECT
                    br,
                    up_cod,
                    up_des,
                    abs_cod,
                    abs_des,
                    aga_cod,
                    AGA_DES,
                    REGIO_COD,
                    REGIO_DES,
                    SAP_COD,
                    SAP_DES,
                    ambit_cod,
                    ambit_des,
                    gap_cod,
                    gap_des,
                    medea
                FROM
                    dwsisap.DBC_CENTRES dc"""
        self.primaria = {row[0]: row[1:] for row in u.getAll(sql, 'exadata')}

        sql = """SELECT
                    up_cod,
                    tip_cod,
                    tip_des,
                    SUBTIP_COD,
                    SUBTIP_DES,
                    up_cod,
                    up_des,
                    abs_cod,
                    abs_des,
                    aga_cod,
                    AGA_DES,
                    REGIO_COD,
                    REGIO_DES,
                    NULL SAP_COD,
                    NULL SAP_DES,
                    NULL ambit_cod,
                    NULL ambit_des,
                    NULL gap_cod,
                    NULL gap_des,
                    NULL medea
                FROM
                    dwsisap.DBC_RUP"""
        self.tipus_centre = {row[0]: row[1:] for row in u.getAll(sql, 'exadata')}
    
    def get_data_uba(self):
        sql = """SELECT
                    DIM_0,
                    DIM_1,
                    substr(DIM_2,1,5) AS br,
                    substr(DIM_2,6,1) AS tipus,
                    substr(DIM_2,7,5) AS uba,
                    sum(NUMERADOR),
                    sum(DENOMINADOR)
                FROM
                    dwsisap.LV_PRE_UBA_MENSUAL
                WHERE
                    DIM_0 IN (
                    'DERIV05_12M',
                    'DERIV05O_12M',
                    'DERIV05U_12M',
                    'DERIV01_12M',
                    'DERIV02A_12M',
                    'DERIV02B_12M',
                    'DERIV04_12M',
                    'DERIV05_12M',
                    'DERIV06_12M',
                    'DERIV07A_12M',
                    'DERIV07B_12M',
                    'DERIV04',
                    'DERIV05',
                    'DERIV05O',
                    'DERIV05U',
                    'DERIV06',
                    'DERIV07A',
                    'DERIV07B',
                    'QCIT001',
                    'QCIT001_ALT',
                    'QCIT001_OST',
                    'QCIT001_SG',
                    'QCIT001_SM',
                    'QCIT001_TRAU',
                    'QCIT002',
                    'QCIT002_ALT',
                    'QCIT002_OST',
                    'QCIT002_SG',
                    'QCIT002_SM',
                    'QCIT002_TRAU',
                    'QCIT003',
                    'QCIT003_ALT',
                    'QCIT003_OST',
                    'QCIT003_SG',
                    'QCIT003_SM',
                    'QCIT003_TRAU',
                    'QCITPAM01',
                    'QCITPAM02',
                    'QCITPRO',
                    'GC0020',
                    'GC0021',
                    'GC0022',
                    'GC0023',
                    'ATD057A',
                    'ATD078A',
                    'ATD078AA',
                    'ATD078AB',
                    'ATD079A',
                    'ATD080A',
                    'ATD062A',
                    'ATD081A',
                    'ATD082A',
                    'ATD083A',
                    'ATD084A',
                    'ATD085A'
                    )
                        OR (arxiu = 'IQF' AND DIM_0 NOT IN ('OSTEOPOROSI_UNIVERSAL', 'MATMA_EAP'))
                    GROUP BY DIM_0,
                        DIM_1,
                        substr(DIM_2,1,5),
                        substr(DIM_2,6,1),
                        substr(DIM_2,7,5)"""
        update_uba = []
        for row in u.getAll(sql, 'exadata'):
            if row[2] in self.primaria:
                if self.primaria[row[2]][0] in self.tipus_centre:
                    update_uba.append(row + self.primaria[row[2]] + self.tipus_centre[self.primaria[row[2]][0]][0:4])
        print(update_uba[0])
        u.listToTable(update_uba, 'coordinacio_uba', 'exadata')
    
    def get_data_up(self):
        sql = """SELECT
                    DIM_0,
                    DIM_1,
                    DIM_2,
                    DIM_4 edat,
                    DIM_5 sexe,
                    DIM_6 pob,
                    NUMERADOR,
                    DENOMINADOR 
                FROM
                    dwsisap.LV_PRE_UP_MENSUAL
                WHERE
                dim_1 in {periodes} AND (
                    DIM_0 IN (
                'QCIT001',
                'QCIT001_ALT',
                'QCIT001_OST',
                'QCIT001_SG',
                'QCIT001_SM',
                'QCIT001_TRAU',
                'QCIT002',
                'QCIT002_ALT',
                'QCIT002_OST',
                'QCIT002_SG',
                'QCIT002_SM',
                'QCIT002_TRAU',
                'QCIT003',
                'QCIT003_ALT',
                'QCIT003_OST',
                'QCIT003_SG',
                'QCIT003_SM',
                'QCIT003_TRAU',
                'QCITPAM01',
                'QCITPAM02',
                'QCITPRO',
                'GC0020',
                'GC0021',
                'GC0022',
                'GC0023',
                'ATD057A',
                'ATD078A',
                'ATD078AA',
                'ATD078AB',
                'ATD079A',
                'ATD080A',
                'ATD062A',
                'ATD081A',
                'ATD082A',
                'ATD083A',
                'ATD084A',
                'ATD085A'
                )
                    OR (arxiu = 'IQF' AND DIM_0 NOT IN ('OSTEOPOROSI_UNIVERSAL', 'MATMA_EAP')))"""
        
        update_up = []
        for row in u.getAll(sql.format(periodes=self.periode), 'exadata'):
            if row[2] in self.primaria:
                if self.primaria[row[2]][0] in self.tipus_centre:
                    update_up.append(row + self.primaria[row[2]] + self.tipus_centre[self.primaria[row[2]][0]][0:4])
        u.listToTable(update_up, 'coordinacio_up', 'exadata')

        # cas especial derivacions
        # prim�ria
        sql = """SELECT
                    DIM_0,
                    DIM_1,
                    DIM_2,
                    DIM_4,
                    DIM_5,
                    DIM_6,
                    NUMERADOR,
                    DENOMINADOR 
                FROM
                    dwsisap.LV_PRE_UP_MENSUAL
                WHERE
                    DIM_0 IN (
                'DERIV05_12M',
                'DERIV05O_12M',
                'DERIV05U_12M',
                'DERIV01_12M',
                'DERIV02A_12M',
                'DERIV02B_12M',
                'DERIV04_12M',
                'DERIV05_12M',
                'DERIV06_12M',
                'DERIV07A_12M',
                'DERIV07B_12M',
                'DERIV04',
                'DERIV05',
                'DERIV05O',
                'DERIV05U',
                'DERIV06',
                'DERIV07A',
                'DERIV07B')
                AND DIM_1 in {periodes}
                AND dim_6 = 'NACDEST'
                AND DIM_4 IN ('PRESEN', 'NOPRESEN')
                AND DIM_5 != 'DERESPECIALITAT'"""
        for row in u.getAll(sql.format(periodes=self.periode), 'exadata'):
            if row[2] in self.primaria:
                if self.primaria[row[2]][0] in self.tipus_centre:
                    update_up.append(row + self.primaria[row[2]] + self.tipus_centre[self.primaria[row[2]][0]][0:4])
        u.listToTable(update_up, 'coordinacio_up', 'exadata')

        # Hospitals
        sql = """SELECT
                    DIM_0,
                    DIM_1,
                    SUBSTR(DIM_6,6,10) AS UP,
                    DIM_4,
                    DIM_5,
                    'CDESTSENSE',
                    SUM(NUMERADOR),
                    SUM(DENOMINADOR)
                FROM
                    dwsisap.LV_PRE_UP_MENSUAL
                WHERE
                    DIM_0 IN (
                'DERIV05_12M',
                'DERIV05O_12M',
                'DERIV05U_12M',
                'DERIV01_12M',
                'DERIV02A_12M',
                'DERIV02B_12M',
                'DERIV04_12M',
                'DERIV05_12M',
                'DERIV06_12M',
                'DERIV07A_12M',
                'DERIV07B_12M',
                'DERIV04',
                'DERIV05',
                'DERIV05O',
                'DERIV05U',
                'DERIV06',
                'DERIV07A',
                'DERIV07B')
                AND DIM_1 in {periodes}
                AND dim_6 NOT in ('NACDEST', 'CDESTSENSE')
                AND length(DIM_6) > 5
                AND DIM_4 IN ('PRESEN', 'NOPRESEN')
                AND DIM_5 != 'DERESPECIALITAT'
                GROUP BY DIM_0,
                    DIM_1,
                    SUBSTR(DIM_6,6,10),
                    DIM_4,
                    DIM_5,
                    'CDESTSENSE'"""
        update_up = []
        for row in u.getAll(sql.format(periodes=self.periode), 'exadata'):
            if row[2] in self.tipus_centre:
                    update_up.append(row +
                                    self.tipus_centre[row[2]][4:] +
                                    self.tipus_centre[row[2]][0:4])
        print(update_up[0])
        u.listToTable(update_up, 'coordinacio_up', 'exadata')

        # cas especial proves
        # prim�ria
        sql = """SELECT
                    DIM_0,
                    DIM_1,
                    DIM_2,
                    DIM_4,
                    DIM_5,
                    DIM_6,
                    NUMERADOR,
                    DENOMINADOR 
                FROM
                    dwsisap.LV_PRE_UP_MENSUAL
                WHERE
                arxiu = 'PROVIND'
                AND DIM_1 in {periodes}"""
        for row in u.getAll(sql.format(periodes=self.periode), 'exadata'):
            if row[2] in self.primaria:
                if self.primaria[row[2]][0] in self.tipus_centre:
                    update_up.append(row + self.primaria[row[2]] + self.tipus_centre[self.primaria[row[2]][0]][0:4])
        u.listToTable(update_up, 'coordinacio_up', 'exadata')

        # Hospitals
        sql = """SELECT
                    DIM_0,
                    DIM_1,
                    SUBSTR(DIM_6,6,10) AS UP,
                    DIM_4,
                    DIM_5,
                    'CDESTSENSE',
                    SUM(NUMERADOR),
                    SUM(DENOMINADOR)
                FROM
                    dwsisap.LV_PRE_UP_MENSUAL
                WHERE
                arxiu = 'PROVIND'
                AND DIM_1 in {periodes}
                GROUP BY DIM_0,
                    DIM_1,
                    SUBSTR(DIM_6,6,10),
                    DIM_4,
                    DIM_5,
                    'CDESTSENSE'"""
        update_up = []
        for row in u.getAll(sql.format(periodes=self.periode), 'exadata'):
            if row[2] in self.tipus_centre:
                    update_up.append(row +
                                    self.tipus_centre[row[2]][4:] +
                                    self.tipus_centre[row[2]][0:4])
        # print(update_up[0])
        u.listToTable(update_up, 'coordinacio_up', 'exadata')

        sql = """select
                    a.cuenta,
                    '{}',
                    a.br,
                    a.edat,
                    a.sexe,
                    a.origen,
                    num,
                    den
                from
                    (
                    select
                        cuenta,
                        br,
                        analisi,
                        edat,
                        origen,
                        sexe,
                        recompte as num
                    from
                        altres.exp_khalix_prealt
                    where
                        analisi in ('ALTES48h1')
                        and 
                origen = 'ALTAHOSPI') a,
                    (
                    select
                        cuenta,
                        br,
                        analisi,
                        edat,
                        origen,
                        sexe,
                        recompte as den
                    from
                        altres.exp_khalix_prealt
                    where
                        analisi in ('ALTES')
                            and 
                origen = 'ALTAHOSPI') b
                where
                    a.cuenta = b.cuenta
                    and
                        a.br = b.br
                    and
                        a.edat = b.edat
                    and
                        a.origen = b.origen
                    and
                        a.sexe = b.sexe
                """.format(self.periode[0])
        update_up = []
        for row in u.getAll(sql, 'altres'):
            if row[2] in self.primaria:
                if self.primaria[row[2]][0] in self.tipus_centre:
                    update_up.append(row + self.primaria[row[2]] + self.tipus_centre[self.primaria[row[2]][0]][0:4])
        u.listToTable(update_up, 'coordinacio_up', 'exadata')


if __name__ == "__main__":
    sql = """select data_ext from nodrizas.dextraccio"""
    periode, = u.getOne(sql, 'nodrizas')
    year = periode.year
    month = '0' + str(periode.month)
    dateA = 'A' + str(year)[-2:] + month[-2:]
    dateB = 'B' + str(year)[-2:] + month[-2:]
    dates = (dateA, dateB)
    print(dates)
    if u.IS_MENSUAL:
        # Structures() # --> no fer, elimina totes les dades
        Indicadors(dates)
        u.grantSelect('coordinacio_up', ('DWRRODRIGUEZ', 'DWLRODRIGUEZ', 'DWPBILLE'), 'exadata')
        u.grantSelect('coordinacio_cat', ('DWRRODRIGUEZ', 'DWLRODRIGUEZ', 'DWPBILLE'), 'exadata')
        u.grantSelect('coordinacio_uba', ('DWRRODRIGUEZ', 'DWLRODRIGUEZ', 'DWPBILLE'), 'exadata')
