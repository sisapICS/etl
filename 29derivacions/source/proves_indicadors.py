# -*- coding: latin1 -*-

"""
BR201 (PED): Sant Andreu de la Barca (outlayer de dias de demora)
BR149 00168 (ADL): Martorell Urba (Outlayer de dias de demora)
"""

import collections as c
from time import time
import sisapUtils as u
from sisaptools import years_between
from dateutil import relativedelta as rd
from datetime import datetime, date
import calendar
import csv
import itertools

# Si es volen actualitzar els �ltims 12 messos de la taula d'exadata, posar MODE = 1
# Si es vol crear la taula d'exadata des de zero, posar MODE = 0
MODE = 1

exadata = "exadata"
derivacions = "derivacions"
taula_exadata = "proves_indicadors_2"
taula_exadata_jail = taula_exadata + "_jail"
taula_proves = "exp_proves_indicadors_2"
taula_proves_jail = taula_proves + "_jail"
columnes_exadata = "(indicador varchar2(21), codi_sector varchar(4), periode varchar2(5), br varchar2(5), analisis varchar2(3), agrupador_prova varchar2(40), especialitat varchar2(40), br_desti varchar2(10), n number)"
columnes_proves = "(indicador varchar(21), codi_sector varchar(4), periode varchar(5), br varchar(5), analisis varchar(3), agrupador_prova varchar(40), especialitat varchar(40), br_desti varchar(10), n int)"  # noqa


CENTRES_DESTI_NO_CONSIDERATS = ("X0039", "X0148")

# Creaci� de diccionari amb codis de especialitats redu�ts a strings de m�xim 6 car�cters
especialitats_6caracters = dict()
file = "./noesb/especialitats_6caracters.txt"
with open(file,'rb') as f:
    row = csv.reader(f, delimiter="@")
    for especialitat, especialitat_6caracters in row:
        especialitats_6caracters[especialitat] = especialitat_6caracters
        
# Dates d'extracci�

periodes_12m_gap0, periodes_12m_gap3 = c.OrderedDict(), c.OrderedDict()

sql = """
        SELECT
            data_ext
        FROM
            dextraccio
        """
dext, = u.getOne(sql, "nodrizas")
dext_dia, dext_mes, dext_any = dext.day, dext.month, dext.year

if dext_dia < 21:

    dext_menys2anys3mesos = dext - rd.relativedelta(months=27)
    dext_menys2anys3mesos = date(dext_menys2anys3mesos.year, dext_menys2anys3mesos.month, 1)  

    dext_menys1any3mesos = dext - rd.relativedelta(months=15)
    dext_menys1any3mesos = date(dext_menys1any3mesos.year, dext_menys1any3mesos.month, 1)          

    dext_menys1any = dext - rd.relativedelta(months=12)
    dext_menys1any = date(dext_menys1any.year, dext_menys1any.month, 1)                                    

    final_calcul_mensuals = date(dext_any, dext_mes-1, calendar.monthrange(dext_any, dext_mes-1)[1])

else:

    dext_menys2anys3mesos = dext - rd.relativedelta(months=26)
    dext_menys2anys3mesos = date(dext_menys2anys3mesos.year, dext_menys2anys3mesos.month, 1)

    dext_menys1any3mesos = dext - rd.relativedelta(months=14)
    dext_menys1any3mesos = date(dext_menys1any3mesos.year, dext_menys1any3mesos.month, 1)

    dext_menys1any = dext - rd.relativedelta(months=11)
    dext_menys1any = date(dext_menys1any.year, dext_menys1any.month, 1)                                       

    final_calcul_mensuals = dext

# Explorem els periodes als quals entraria una dada d'un mes concret al c�lcul d'un indicador anual

periode_actual = "A{}".format(final_calcul_mensuals.strftime("%y%m"))
periode_menys1any = "A{}".format(dext_menys1any.strftime("%y%m"))
rang_mesos = range(final_calcul_mensuals.month + 12 * (final_calcul_mensuals.year - 2019))

for mes in rang_mesos:
    periodes_12m_gap0["A{}".format(date(2019 + mes // 12, mes % 12 + 1, 1).strftime("%y%m"))] = ["A{}".format(date(2019 + mes_12m // 12, mes_12m % 12 + 1, 1).strftime("%y%m")) for mes_12m in range(mes, mes+12) if "A{}".format(date(2019 + mes_12m // 12, mes_12m % 12 + 1, 1).strftime("%y%m")) <= periode_actual]
    periodes_12m_gap3["A{}".format(date(2019 + mes // 12, mes % 12 + 1, 1).strftime("%y%m"))] = ["A{}".format(date(2019 + mes_12m // 12, mes_12m % 12 + 1, 1).strftime("%y%m")) for mes_12m in range(mes+3, mes+15) if "A{}".format(date(2019 + mes_12m // 12, mes_12m % 12 + 1, 1).strftime("%y%m")) <= periode_actual]

# Trobem el periode a partir del qual les dades ser�n eliminades a la taula proves_indicadors d'exadata
primer_periode = "A{}".format(dext_menys1any.strftime("%y%m"))

print("  - Data d'extraccio: {}".format(dext))

def get_centres():
    """ . """

    global centres_up2br, centres_br2up, centres_jail

    centres_up2br = dict()
    centres_br2up = dict()

    sql = """
            SELECT
                scs_codi,
                ics_codi
            FROM
                cat_centres
            UNION
            SELECT
                scs_codi,
                ics_codi
            FROM
                jail_centres
            """
    for up, br in u.getAll(sql, "nodrizas"):
        if up not in CENTRES_DESTI_NO_CONSIDERATS:
            centres_up2br[up] = {"br": br}
            centres_br2up[br] = {"up": up}

    sql = """
            SELECT
                ics_codi
            FROM
                jail_centres
            """
    centres_jail = [br for br, in u.getAll(sql, "nodrizas")]

    print("  - Nombre de centres: {}".format(len(centres_up2br)))


def get_poblacio():
    """."""

    global poblacio, count_pobatesa_pedia, count_pobtotal_pedia, count_pobatesa_adults, count_pobtotal_adults

    poblacio = dict()
    count_pobatesa_pedia, count_pobtotal_pedia = c.Counter(), c.Counter()
    count_pobatesa_adults, count_pobtotal_adults = c.Counter(), c.Counter()

    sql = """
            SELECT
                id_cip_sec,
                up,
                data_naix,
                edat,
                sexe,
                ates
            FROM
                assignada_tot_with_jail
            """
    for id_cip_sec, up, data_naix, edat, sexe, ates in u.getAll(sql, "nodrizas"):
        poblacio[id_cip_sec] = {"up": up, "data_naix": data_naix, "edat": edat, "sexe": sexe, "ates": ates}
        up = "15443" if up == "16257" else up
        if ates:
            if edat < 15:
                count_pobatesa_pedia[up] += 1
            else:
                count_pobatesa_adults[up] += 1
        if edat < 15:
            count_pobtotal_pedia[up] += 1
        else:
            count_pobtotal_adults[up] += 1

    print("  - Poblaci� assignada carregada: {}".format(datetime.now()))

def get_agrupadors_codi_proves():
    """."""

    global agrupadors_codi_proves, proves_pares
    
    FILE = "./noesb/agrupadors_codi_proves.txt"
    agrupadors_codi_proves = {codi_prova: agrupador_codi_prova for codi_prova, agrupador_codi_prova in u.readCSV(FILE, sep="@")}

    FILE = "./noesb/proves_pares.txt"
    proves_pares = {agrupador_codi_prova: agrupador_codi_prova_pare for agrupador_codi_prova, agrupador_codi_prova_pare in u.readCSV(FILE, sep="@")}

def create_tables():

    u.createTable(taula_proves_jail, columnes_proves, derivacions, rm=1)

    u.execute("DROP TABLE IF EXISTS {_taula_proves}".format(_taula_proves=taula_proves), derivacions)
    particions_proves = ", ".join("PARTITION {_taula_proves}_{_sector} VALUES IN ('{_sector}')".format(_taula_proves=taula_proves, _sector=sector) for sector in u.sectors)
    sql = """
            CREATE TABLE {_taula_proves} 
            {_columnes_proves}
            PARTITION BY LIST COLUMNS (CODI_SECTOR) ({_particions_proves})
            """
    u.execute(sql.format(_taula_proves=taula_proves, _particions_proves=particions_proves, _columnes_proves=columnes_proves), derivacions)

    if MODE == 1:
        sql = "DELETE FROM {_taula_exadata} WHERE periode >= '{_primer_periode}'"
        u.execute(sql.format(_taula_exadata=taula_exadata, _primer_periode=primer_periode), exadata)
        u.execute(sql.format(_taula_exadata=taula_exadata_jail, _primer_periode=primer_periode), exadata)
        
    elif MODE == 0:
        u.createTable(taula_exadata_jail, columnes_exadata, exadata, rm=1)
        try:
            u.execute("DROP TABLE {_taula_exadata}".format(_taula_exadata=taula_exadata), exadata)
        except:
            pass
        particions_exadata = ", ".join("PARTITION {_taula_exadata}_{_sector} VALUES ('{_sector}')".format(_taula_exadata=taula_exadata, _sector=sector) for sector in u.sectors)
        sql = """
                CREATE TABLE {_taula_exadata}
                {_columnes_exadata}
                PARTITION BY LIST (CODI_SECTOR) ({_particions_exadata})
              """
        u.execute(sql.format(_taula_exadata=taula_exadata, _columnes_exadata=columnes_exadata, _particions_exadata=particions_exadata), exadata)

        u.grantSelect(taula_exadata, 'DWSISAP_ROL', exadata)
        u.grantSelect(taula_exadata_jail, 'DWSISAP_ROL', exadata)

def sub_get_proves(input):
    
    def add_indicador(indicador, codi_sector, periode_mensual, br, analisis, agrupador_prova, especialitat, br_desti, quantitat):
        if not (indicador[:6] == "PROV02" and analisis == "DEN"):
            indicadors[(indicador, codi_sector, periode_mensual, br, analisis, agrupador_prova, especialitat, br_desti)] += quantitat
        else:
            indicadors[(indicador, codi_sector, periode_mensual, br, analisis, agrupador_prova, especialitat, br_desti)] = quantitat

    tb, db = input
    
    tb_exadata = taula_exadata if db == "import" else taula_exadata_jail
    tb_derivacions = taula_proves if db == "import" else taula_proves_jail
    
    indicadors = c.Counter()
    sub_distincts_periode_mensual = set()
    distincts_br = set()
    
    oc_data_llindar_inicial = "BETWEEN STR_TO_DATE('{}', '%Y-%m-%d') AND".format(dext_menys1any) if MODE == 1 else "<="
    count_infrelacioprov_null = 1

    sub_inf_codi_prova_faltants = c.Counter()
    
    sql = """
            SELECT
                id_cip_sec,
                codi_sector,
                inf_relacio_deriv,
                oc_up_ori,
                der_up_scs_des,
                oc_data,
                der_data_prog,
                der_data_real,
                inf_codi_prova,
                agrupador_prova,
                agrupador_rebuig,
                der_tipus_fluxe,
                inf_prioritat,
                flag_peticio_sollicitada,   -- PROV01, PROV02
                flag_peticio_tramitada,     -- PROV04, PROV05, PROV06, PROV07
                flag_peticio_pendent,       -- PROV04
                flag_peticio_realitzada,    -- PROV06 (PROV05)
                flag_peticio_programada,    -- PROV05
                flag_peticio_rebutjada      -- PROV07
            FROM
                {}
            WHERE
                NOT (der_data_prog IS NULL
                    AND der_data_real IS NOT NULL
                    AND inf_servei_d_codi IS NULL)
                AND oc_data {} STR_TO_DATE('{}', '%Y-%m-%d')
            """.format(tb, oc_data_llindar_inicial, final_calcul_mensuals)
    for (id_cip_sec, codi_sector, inf_relacio_deriv, up_origen, up_desti, oc_data, der_data_prog, der_data_real, inf_codi_prova, agrupador_prova,
            agrupador_rebuig, fluxe, prioritat, FLAG_PETICIO_SOLLICITADA, FLAG_PETICIO_TRAMITADA, FLAG_PETICIO_PENDENT, \
            FLAG_PETICIO_REALITZADA, FLAG_PETICIO_PROGRAMADA, FLAG_PETICIO_REBUTJADA) in u.getAll(sql, db):
        up_origen = "15443" if up_origen == "16257" else up_origen
        if up_origen in centres_up2br and id_cip_sec in poblacio:
            if inf_codi_prova in agrupadors_codi_proves:
                data_naix, sexe = (poblacio[id_cip_sec][k] for k in ("data_naix", "sexe"))
                edat = years_between(data_naix, oc_data)
                inf_relacio_deriv = inf_relacio_deriv if inf_relacio_deriv else "NULL_" + codi_sector + "_" + str(count_infrelacioprov_null).zfill(9)
                count_infrelacioprov_null += 1
                sufix = "P" if 0 <= edat < 15 else ""
                pipa = True if fluxe == 'PIPA' else False
                periode_mensual = "A{}".format(oc_data.strftime("%y%m"))
                br_desti = "CDEST" + up_desti if up_desti else "CDESTSENSE"
                br = centres_up2br[up_origen]["br"]
                
                der_data_prog = der_data_prog if der_data_prog and der_data_prog.year < 9999 else None
                der_data_real = der_data_real if der_data_real else None
                agrupador_codi_prova = agrupadors_codi_proves[inf_codi_prova]
                agrupador_codi_prova_pare = proves_pares[agrupador_codi_prova]
                FLAG_PETICIO_PROGRAMADA = 1 if FLAG_PETICIO_PROGRAMADA and der_data_prog and der_data_prog.year < 9999 else 0

                if sufix == 'P':
                    pobs_A = count_pobatesa_pedia[up_origen]
                    pobs_B = count_pobtotal_pedia[up_origen]
                else:
                    pobs_A = count_pobatesa_adults[up_origen]
                    pobs_B = count_pobtotal_adults[up_origen]

                
                dims_4 = (agrupador_codi_prova, agrupador_codi_prova_pare, "GRUPPROVES")
                dims_6 = ('NACDEST', 'CDEST')
                
                if not (sufix == 'P' and br in centres_jail):

                    sub_distincts_periode_mensual.add(periode_mensual)
                    distincts_br.add(br)
                    
                    for dim_4 in dims_4:
                        for dim_6 in dims_6:       
                            add_indicador("PROV02A{}".format(sufix), codi_sector, periode_mensual, br, "DEN", dim_4, 'NOIMP', dim_6, pobs_A)
                            add_indicador("PROV02B{}".format(sufix), codi_sector, periode_mensual, br, "DEN", dim_4, 'NOIMP', dim_6, pobs_B)
                    if FLAG_PETICIO_SOLLICITADA:
                        add_indicador("PROV01{}".format(sufix), codi_sector, periode_mensual, br, "NUM", agrupador_codi_prova, 'NOIMP', br_desti, 1)
                        for dim_4 in dims_4:
                            for dim_6 in dims_6:
                                add_indicador("PROV02A{}".format(sufix), codi_sector, periode_mensual, br, "NUM", dim_4, 'NOIMP', dim_6, 1)
                                add_indicador("PROV02B{}".format(sufix), codi_sector, periode_mensual, br, "NUM", dim_4, 'NOIMP', dim_6, 1)

                    if FLAG_PETICIO_TRAMITADA and pipa:
                        if (FLAG_PETICIO_PENDENT)  or (FLAG_PETICIO_REALITZADA and der_data_real >= oc_data) \
                            or (not FLAG_PETICIO_REALITZADA and FLAG_PETICIO_PROGRAMADA and der_data_prog >= oc_data):
                        
                            add_indicador("PROV04{}".format(sufix), codi_sector, periode_mensual, br, "DEN", agrupador_codi_prova, 'NOIMP', br_desti, 1)
                            add_indicador("PROV06{}".format(sufix), codi_sector, periode_mensual, br, "DEN", agrupador_codi_prova, 'NOIMP', br_desti, 1)
                
                            if FLAG_PETICIO_PENDENT:
                                add_indicador("PROV04{}".format(sufix), codi_sector, periode_mensual, br, "NUM", agrupador_codi_prova, 'NOIMP', br_desti, 1)
                            dies_demora = 0
                            if FLAG_PETICIO_REALITZADA and der_data_real >= oc_data:
                                dies_demora = u.daysBetween(oc_data, der_data_real)
                                add_indicador("PROV06{}".format(sufix), codi_sector, periode_mensual, br, "NUM", agrupador_codi_prova, 'NOIMP', br_desti, 1)
                            elif not FLAG_PETICIO_REALITZADA and FLAG_PETICIO_PROGRAMADA and der_data_prog >= oc_data:
                                dies_demora = u.daysBetween(oc_data, der_data_prog)
                            if not FLAG_PETICIO_REBUTJADA:      
                                if dies_demora < 365*3 and not FLAG_PETICIO_PENDENT:
                                    add_indicador("PROV05{}".format(sufix), codi_sector, periode_mensual, br, "DEN", agrupador_codi_prova, 'NOIMP', br_desti, 1)
                                    add_indicador("PROV05{}".format(sufix), codi_sector, periode_mensual, br, "NUM", agrupador_codi_prova, 'NOIMP', br_desti, dies_demora)
                                    if prioritat in ("P", "U", "O"):
                                        prioritat = "U" if prioritat in ("P", "U") else prioritat
                                        add_indicador("PROV05{}{}".format(sufix, prioritat), codi_sector, periode_mensual, br, "DEN", agrupador_codi_prova, 'NOIMP', br_desti, 1)
                                        add_indicador("PROV05{}{}".format(sufix, prioritat), codi_sector, periode_mensual, br, "NUM", agrupador_codi_prova, 'NOIMP', br_desti, dies_demora)

                        add_indicador("PROV07A{}".format(sufix), codi_sector, periode_mensual, br, "DEN", agrupador_codi_prova, 'NOIMP', br_desti, 1)
                        add_indicador("PROV07B{}".format(sufix), codi_sector, periode_mensual, br, "DEN", agrupador_codi_prova, 'NOIMP', br_desti, 1)                                                      
                        if FLAG_PETICIO_REBUTJADA:
                            if agrupador_rebuig == "REBUIG_HOSPITAL":
                                add_indicador("PROV07A{}".format(sufix), codi_sector, periode_mensual, br, "NUM", agrupador_codi_prova, 'NOIMP', br_desti, 1)
                            elif agrupador_rebuig == "REBUIG_PACIENT":
                                add_indicador("PROV07B{}".format(sufix), codi_sector, periode_mensual, br, "NUM", agrupador_codi_prova, 'NOIMP', br_desti, 1)

            else:
                sub_inf_codi_prova_faltants[inf_codi_prova] += 1

   
    # Afegir DENominadors faltants

    for indicador in ('PROV02B','PROV02A','PROV02AP','PROV02BP'):
        for periode_mensual in sub_distincts_periode_mensual:
            for br in distincts_br:
                if not (indicador[-1] == 'P' and br in centres_jail):
                    for agrupador_codi_prova, agrupador_codi_prova_pare in set(proves_pares.items()):

                        dims_4 = (agrupador_codi_prova, agrupador_codi_prova_pare, "GRUPPROVES")
                        dims_6 = ('NACDEST', 'CDEST')

                        for dim_4 in dims_4:
                            for dim_6 in dims_6:
                                if (indicador, codi_sector, periode_mensual, br, "DEN", dim_4, 'NOIMP', dim_6) not in indicadors:
                                    indicador_pare = indicador[:7]
                                    br = "BR405" if br == "BR408" else br
                                    if br in centres_br2up:
                                        up = centres_br2up[br]["up"]
                                        up = "15443" if up == "16257" else up
                                        if indicador_pare == "PROV02A":
                                            if len(indicador) == 7:
                                                indicadors[(indicador, codi_sector, periode_mensual, br, "DEN", dim_4, 'NOIMP', dim_6)] = count_pobatesa_adults[up]
                                            elif len(indicador) == 8:
                                                if indicador[7] == "P":
                                                    indicadors[(indicador, codi_sector, periode_mensual, br, "DEN", dim_4, 'NOIMP', dim_6)] = count_pobatesa_pedia[up]                      
                                        elif indicador_pare == "PROV02B":
                                            if len(indicador) == 7:
                                                indicadors[(indicador, codi_sector, periode_mensual, br, "DEN", dim_4, 'NOIMP', dim_6)] = count_pobtotal_adults[up]
                                            elif len(indicador) == 8:
                                                if indicador[7] == "P":
                                                    indicadors[(indicador, codi_sector, periode_mensual, br, "DEN", dim_4, 'NOIMP', dim_6)] = count_pobtotal_pedia[up]
        
    # Enviar dades a les taules
    resultats = [
        [indicador,
        codi_sector,
        periode,
        br_origen,
        analisis,
        agrupador_codi_prova,
        especialitat,
        br_desti,
        n] for (indicador,
                codi_sector,
                periode,
                br_origen,
                analisis,
                agrupador_codi_prova,
                especialitat,
                br_desti), n in indicadors.items()]
    u.listToTable(resultats, tb_exadata, exadata)
    u.listToTable(resultats, tb_derivacions, derivacions)

    return sub_inf_codi_prova_faltants, sub_distincts_periode_mensual


def get_proves():
    """."""

    global inf_codi_prova_faltants;         inf_codi_prova_faltants = c.Counter()
    global distincts_periode_mensual;       distincts_periode_mensual = set()

    taules = [("proves", "import_jail")]
    taules.extend([(tb, "import") for tb in u.getSubTables("proves")])

    for sub_inf_codi_prova_faltants, sub_distincts_periode_mensual in u.multiprocess(sub_get_proves, ((tb, db) for tb, db in taules), 8):
        distincts_periode_mensual.update(sub_distincts_periode_mensual)
        for inf_codi_prova, n in sub_inf_codi_prova_faltants.items():
            inf_codi_prova_faltants[inf_codi_prova] += n

def sub_get_anuals(sector):    
    
    tb_exadata = taula_exadata if sector != "6951" else taula_exadata_jail
    tb_derivacions = taula_proves if sector != "6951" else taula_proves_jail
    
    indicadors = c.Counter()
    distincts_periode = set()
    distincts_br = set()
    
    if tb_exadata == taula_exadata:
        sql = """
                SELECT
                    *
                FROM
                    dwsisap.{}
                PARTITION ({})
                """.format(tb_exadata, tb_exadata+"_"+sector)
        
    else: # tb_exadata == taula_exadata_jail
        sql = """
                SELECT
                    *
                FROM
                    dwsisap.{}
                """.format(tb_exadata)
    
    for indicador, codi_sector, periode, br, analisis, agrupador_prova, especialitat, br_desti, n in u.getAll(sql, "exadata"):
        distincts_periode.add(periode)
        distincts_br.add(br)
        indicador_pare = indicador[:7] if indicador[:6] == "PROV02" else indicador[:6]
        br = "BR405" if br == "BR408" else br
        if br in centres_br2up and not (indicador[-1] == 'P' and br in centres_jail) and not "_12M" in indicador:
            up = centres_br2up[br]["up"]
            up = "15443" if up == "16257" else up
            indicador_12m = indicador+"_12M"
            if indicador_pare in ("PROV01", "PROV02A", "PROV02B"):
                if periode in periodes_12m_gap0:
                    for periode_12m in periodes_12m_gap0[periode]:
                        if (periode_12m >= periode_menys1any and MODE == 1) or (periode_12m >= "A1912" and MODE == 0):
                            if not (indicador_pare in ("PROV02A", "PROV02B") and analisis == "DEN"):
                                indicadors[(indicador_12m, codi_sector, periode_12m, br, analisis, agrupador_prova, especialitat, br_desti)] += n
            elif indicador_pare not in ("PROV01", "PROV02"):
                if periode in periodes_12m_gap3:
                    for periode_12m in periodes_12m_gap3[periode]:
                        if (periode_12m >= periode_menys1any and MODE == 1) or (periode_12m >= "A2003" and MODE == 0):
                            indicadors[(indicador_12m, codi_sector, periode_12m, br, analisis, agrupador_prova, especialitat, br_desti)] += n
    
    # Afegir DENominadors faltants

    for indicador in ('PROV02B','PROV02A','PROV02AP','PROV02BP'):
        for periode_mensual in distincts_periode_mensual:
            for br in distincts_br:
                if not (indicador[-1] == 'P' and br in centres_jail):
                    indicador_12m = indicador+"_12M"
                    for agrupador_codi_prova, agrupador_codi_prova_pare in set(proves_pares.items()):

                        dims_4 = (agrupador_codi_prova, agrupador_codi_prova_pare, "GRUPPROVES")
                        dims_6 = ('NACDEST', 'CDEST')

                        for dim_4 in dims_4:
                            for dim_6 in dims_6:
                                if (indicador_12m, codi_sector, periode_mensual, br, "DEN", dim_4, 'NOIMP', dim_6) not in indicadors:
                                    indicador_pare = indicador[:7]
                                    br = "BR405" if br == "BR408" else br
                                    if br in centres_br2up:
                                        up = centres_br2up[br]["up"]
                                        up = "15443" if up == "16257" else up
                                        if indicador_pare == "PROV02A":
                                            if len(indicador) == 7:
                                                indicadors[(indicador_12m, codi_sector, periode_mensual, br, "DEN", dim_4, 'NOIMP', dim_6)] = count_pobatesa_adults[up]
                                            elif len(indicador) == 8:
                                                if indicador[7] == "P":
                                                    indicadors[(indicador_12m, codi_sector, periode_mensual, br, "DEN", dim_4, 'NOIMP', dim_6)] = count_pobatesa_pedia[up]                      
                                        elif indicador_pare == "PROV02B":
                                            if len(indicador) == 7:
                                                indicadors[(indicador_12m, codi_sector, periode_mensual, br, "DEN", dim_4, 'NOIMP', dim_6)] = count_pobtotal_adults[up]
                                            elif len(indicador) == 8:
                                                if indicador[7] == "P":
                                                    indicadors[(indicador_12m, codi_sector, periode_mensual, br, "DEN", dim_4, 'NOIMP', dim_6)] = count_pobtotal_pedia[up]

    # Enviar dades a les taules

    resultats = [
        [indicador,
        codi_sector,
        periode,
        br_origen,
        analisis,
        agrupador_prova,
        especialitat,
        br_desti,
        n] for (indicador,
                codi_sector,
                periode,
                br_origen,
                analisis,
                agrupador_prova,
                especialitat,
                br_desti), n in indicadors.items()]
    u.listToTable(resultats, tb_exadata, exadata)
    u.listToTable(resultats, tb_derivacions, derivacions)
    
def get_anuals():
    
    sectors = u.sectors
    sectors.append("6951")

    u.multiprocess(sub_get_anuals, sectors, 8)

def export_khalix_mode1():
    """."""

    file_1 = "PROVINDTAX"
    sql_1 = """
                SELECT DISTINCT
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti, 
                    'N',
                    n
                FROM
                    {_derivacions}.{_taula_proves}
                WHERE
                    indicador < 'PROV04'
                    AND analisis = 'DEN'
                UNION
                SELECT
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti, 
                    'N',
                    sum(n)
                FROM
                    {_derivacions}.{_taula_proves}
                WHERE
                    indicador < 'PROV04'
                    AND analisis = 'NUM'
                GROUP BY
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti
            """.format(_derivacions=derivacions, _taula_proves=taula_proves)
    u.exportKhalix(sql_1, file_1)

    file_2 = "PROVIND"
    sql_2 = """
                SELECT DISTINCT
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti, 
                    'N',
                    n
                FROM
                    {_derivacions}.{_taula_proves}
                WHERE
                    indicador >= 'PROV04'
                GROUP BY
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti
            """.format(_derivacions=derivacions, _taula_proves=taula_proves)
    u.exportKhalix(sql_2, file_2)

    file_3 = "PROVINDTAX_JAIL"
    sql_3 = """
                SELECT DISTINCT
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti, 
                    'N',
                    n
                FROM
                    {_derivacions}.{_taula_proves_jail}
                WHERE
                    indicador < 'PROV04'
            """.format(_derivacions=derivacions, _taula_proves_jail=taula_proves_jail)
    u.exportKhalix(sql_3, file_3)

    file_4 = "PROVIND_JAIL"
    sql_4 = """
                SELECT DISTINCT
                    indicador,
                    periode,
                    br,
                    analisis,
                    agrupador_prova,
                    especialitat,
                    br_desti, 
                    'N',
                    n
                FROM
                    {_derivacions}.{_taula_proves_jail}
                WHERE
                    indicador >= 'PROV04'
            """.format(_derivacions=derivacions, _taula_proves_jail=taula_proves_jail)
    u.exportKhalix(sql_4, file_4)

# def export_khalix_mode0():
#     """."""
    
#     for year in range(2019, int(periode_actual[1:3])+2001):
#         year_curt = year - 2000

#         file_1 = "PROVINDTAX_ANY{}".format(year)
#         sql_1 = """
#                     SELECT
#                         indicador,
#                         periode,
#                         br,
#                         analisis,
#                         agrupador_codi_prova,
#                         'NOIMP',
#                         br_desti, 
#                         'N',
#                         n
#                     FROM
#                         {}.{}
#                     WHERE
#                         indicador < 'PROV04'
#                         AND periode like 'A{}%'
#                 """.format(derivacions, taula_proves, year_curt)
#         u.exportKhalix(sql_1, file_1)
        
#         file_2 = "PROVIND_ANY{}".format(year)
#         sql_2 = """
#                     SELECT
#                         indicador,
#                         periode,
#                         br,
#                         analisis,
#                         agrupador_codi_prova,
#                         'NOIMP',
#                         br_desti, 
#                         'N',
#                         n
#                     FROM
#                         {}.{}
#                     WHERE
#                         indicador >= 'PROV04'
#                         AND periode like 'A{}%'
#                 """.format(derivacions, taula_proves, year_curt)
#         u.exportKhalix(sql_2, file_2)

#         file_3 = "PROVINDTAX_JAIL_ANY{}".format(year)
#         sql_3 = """
#                     SELECT
#                         indicador,
#                         periode,
#                         br,
#                         analisis,
#                         agrupador_codi_prova,
#                         'NOIMP',
#                         br_desti, 
#                         'N',
#                         n
#                     FROM
#                         {}.{}
#                     WHERE
#                         indicador < 'PROV04'
#                         AND periode like 'A{}%'
#                 """.format(derivacions, taula_proves_jail, year_curt)
#         u.exportKhalix(sql_3, file_3)
        
#         file_4 = "PROVIND_JAIL_ANY{}".format(year)
#         sql_4 = """
#                     SELECT
#                         indicador,
#                         periode,
#                         br,
#                         analisis,
#                         agrupador_codi_prova,
#                         'NOIMP',
#                         br_desti, 
#                         'N',
#                         n
#                     FROM
#                         {}.{}
#                     WHERE
#                         indicador >= 'PROV04'
#                         AND periode like 'A{}%'
#                 """.format(derivacions, taula_proves_jail, year_curt)
#         u.exportKhalix(sql_4, file_4)


def send_email_inf_codi_prova_faltants():
    """."""
    nous_inf_codi_prova = ""
    for inf_codi_prova, n in inf_codi_prova_faltants.items():
        nous_inf_codi_prova += "\n     - {}: {}".format(inf_codi_prova, n)

    me = "SISAP <sisap@gencat.cat>"
    to = "nmorenom.bnm.ics@gencat.cat"
    cc = "alejandrorivera@gencat.cat"
    subject = "Nous inf_codi_prova a master_proves"
    text = "Bon dia\n Trobem nous codis inf_codi_prova a master_proves, s�n els seg�ents: {}.\n Salutacions,\n\nEl teu bot de confian�a *__*".format(str(tuple(nous_serveis)))
    u.sendGeneral(me, to, cc, subject, text)

if (MODE == 1 and u.IS_MENSUAL) or MODE == 0:

    get_centres();                              print("Done: get_centres()")
    get_poblacio();                             print("Done: get_poblacio()")
    get_agrupadors_codi_proves();               print("Done: get_agrupadors_codi_proves()")
    create_tables();                            print("Done: create_tables()")
    get_proves();                               print("Done: get_proves()")
    get_anuals();                               print("Done: get_anuals()")
    if MODE == 1:
        export_khalix_mode1();                  print("Done: export_khalix_mode1()")
    # elif MODE == 0:
    #     export_khalix_mode0();                  print("Done: export_khalix_mode0()")
    if inf_codi_prova_faltants:
        send_email_inf_codi_prova_faltants();   print("Done: send_email_inf_codi_prova_faltants()")