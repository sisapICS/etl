
import sisapUtils as u

redic_2_covid = {}
covid_2_redics = {}
sql = """"SELECT hash_redics, hash_covid from pdptb101_relacio"""
for h_redics, h_covid in u.getAll(sql, 'pdp'):
    redic_2_covid[h_redics] = h_covid
    covid_2_redics[h_covid] = h_redics


al_circuit = {}
sql = """SELECT PACIENT, CASE WHEN  motiu_prior LIKE '%PLANIFICAT>CON>INF%' THEN 'I'
        WHEN motiu_prior LIKE '%PLANIFICAT>CON>MG%' THEN 'M'
        ELSE '' END CONJUNTA, trunc(DATA)
        FROM dwsisap.SISAP_MASTER_VISITES smv 
        WHERE (motiu_prior LIKE '%PLANIFICAT>CON%')
        AND SITUACIO = 'R'"""

for pacient, conjunta, dia in u.getAll(sql, 'exadata'):
    pacient = redic_2_covid[pacient]
    if pacient in al_circuit:
        if al_circuit[pacient] < dia: al_circuit[pacient] = dia
    else: al_circuit[pacient] = dia



info_up_uba = {}
# Baixem dades dbs
sql = """SELECT c_cip, grup, c_up, c_centre, c_metge, C_SECTOR, C_INFERMERA, C_EDAT_ANYS,
            CASE WHEN PS_ATDOM_DATA IS NOT NULL THEN 1 END ATDOM,
            CASE WHEN C_INSTITUCIONALITZAT IS NOT NULL THEN 'R' WHEN PS_ATDOM_DATA IS NOT NULL AND C_INSTITUCIONALITZAT IS NULL THEN 'A' END ATDOM_RESIDENCIA, 
            CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL THEN 1 end CI,
            CASE WHEN PS_ACV_MCV_DATA IS NOT NULL THEN 1 end AVC,
            CASE WHEN PS_HTA_DATA IS NOT NULL THEN 1 END HTA,
            CASE WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 1 END dm2,
            CASE WHEN PS_INSUF_CARDIACA_DATA IS NOT NULL THEN 1 END IC,
            CASE WHEN PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 1 END MPOC_GREU,
            CASE WHEN PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 1 END MPOC_LLEU,
            CASE WHEN PS_DISLIPEMIA_DATA IS NOT NULL THEN 1 END dislipemia,
            (CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL OR PS_INSUF_CARDIACA_DATA IS NOT NULL 
            OR PS_ACV_MCV_DATA IS NOT NULL OR (PS_MPOC_ENFISEMA_DATA IS NOT NULL)THEN 3
            WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 2 ELSE 1 END) risc,
            CASE WHEN F_HTA_COMBINACIONS is NOT NULL OR F_HTA_DIURETICS IS NOT NULL OR F_HTA_IECA_ARA2 IS NOT NULL THEN 1 ELSE 0 END FarmHTA,
            CASE WHEN PS_HIPOTIROIDISME_DATA IS NOT NULL THEN 1 END HIPOTIROIDISME,
            V_HBA1C_DATA glicada_DM,
            V_COL_TOTAL_DATA  colest,
            V_ECG_AMB_DATA ECG,
            CASE WHEN (V_FEV1_DATA >= V_FEV1_FVC_DATA or V_FEV1_FVC_DATA IS  null) AND (V_FEV1_DATA >= VC_VAL_ESPIRO_DATA or VC_VAL_ESPIRO_DATA IS  null) THEN V_FEV1_DATA 
                WHEN (V_FEV1_FVC_DATA >= V_FEV1_DATA or V_FEV1_DATA IS  null) AND (V_FEV1_FVC_DATA >= VC_VAL_ESPIRO_DATA or VC_VAL_ESPIRO_DATA IS  null) THEN V_FEV1_FVC_DATA 
                WHEN (VC_VAL_ESPIRO_DATA >= V_FEV1_DATA or V_FEV1_DATA IS  null) AND (VC_VAL_ESPIRO_DATA >= V_FEV1_FVC_DATA or V_FEV1_FVC_DATA IS  null) THEN VC_VAL_ESPIRO_DATA 
			END espiro1,
            V_FEV1_FVC_DATA espiro2,
            V_FONS_ULL_DATA fons_ull
        fROM 
            dbs 
        WHERE 
            C_EDAT_ANYS > 14 AND
            c_sector != '6951' AND
            (PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL 
            OR ps_hta_data IS NOT NULL 
            OR PS_DIABETIS2_DATA IS NOT NULL OR 
            PS_INSUF_CARDIACA_DATA is NOT NULL OR 
            PS_MPOC_ENFISEMA_DATA IS NOT NULL  OR 
            PS_DISLIPEMIA_DATA IS NOT NULL OR
            PS_ACV_MCV_DATA IS NOT NULL) AND 
            PR_MACA_DATA IS NULL AND
            PS_DEMENCIA_DATA IS NULL AND
            PS_CURES_PALIATIVES_DATA IS NULL AND 
            (CASE WHEN (PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL 
            OR PS_INSUF_CARDIACA_DATA IS NOT NULL 
            OR PS_MPOC_ENFISEMA_DATA IS NOT NULL
            OR PS_ACV_MCV_DATA IS NOT NULL)THEN 3
            WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 2 ELSE 1 END) IN (2,3)"""
upload = []

info_resucitar = defaultdict(list)
cronics_set = set()

for (c_cip, grup, c_up, c_centre, c_metge, C_SECTOR, C_INFERMERA, c_edat, atdom, ATDOM_RESIDENCIA,
        CI, AVC, HTA, dm2, IC, MPOC_GREU, MPOC_LLEU, dislipemia, risc, FarmHTA,
        HIPOTIROIDISME, glicada_DM, colest, ECG, espiro1, espiro2, fons_ull) in u.getAll(sql, 'redics'):
    if c_cip in al_circuit:
        colest = al_circuit[c_cip]
    elif c_cip in analitiques:
        colest = analitiques[c_cip]
    if grup == 13 and c_cip not in al_circuit:
        colest = espiro1
    upload.append([c_cip, c_up, c_centre, c_metge, C_SECTOR, C_INFERMERA, c_edat, atdom, ATDOM_RESIDENCIA, 
                    CI, AVC, HTA, dm2, IC, MPOC_GREU, MPOC_LLEU, dislipemia, 
                    risc, FarmHTA, HIPOTIROIDISME, glicada_DM, colest, ECG, 
                    espiro1, espiro2, fons_ull])
    if risc >= 2:
        info_resucitar[c_cip].append(C_SECTOR)
        cronics_set.add((c_cip, C_SECTOR))
    info_up_uba[c_cip] = [c_up, c_metge]