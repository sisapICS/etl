# coding: latin1

import sisaptools as u
import sisapUtils as u2
import re

# Nom de la taula original
table = "dbs"

# Obt� la data d'extracci� de la base de dades i genera el nom de la taula anual
dext, = u.Database("p2262", "nodrizas").get_one("select DATE'2025-12-31' from dextraccio")  # noqa
anual = "dbs_{}".format(dext.year)


def worker(letter):
    """
    Processa i sincronitza dades entre Sidics i Exadata per els identificadors 
    de pacient que comencen amb digits espec�fics.

    Arguments:
    ----------
    letter : str
        L'inici del codi CIP (ex: '00', ..., 25', ..., 31', ..., 54', ..., 76', ..., C6', ..., FE', ... 'FF') per filtrar les dades.

    Procediment:
    ------------
    1. Obt� les dades de la taula 'dbs' de Redics on 'c_cip' comen�a per 'letter'.
    2. Obt� les dades de la taula 'dbs' d'Exadata on 'c_cip' comen�a per 'letter'.
    3. Desa les dades filtrades en la taula anual de Sidics i Exadata.
    """

    # Obtenir dades de Sidics per c_cip que comencen per 'letter'
    sql = "select * from {} where c_cip like '{}%'".format(table, letter)
    dades = list(u.Database("redics", "data").get_all(sql))
    with u.Database("sidics", "dbs") as sidics:
        sidics.execute("set session wait_timeout = 28800")
        sidics.list_to_table(dades, anual)

    # Obtenir dades de exadata per c_cip que comencen per 'letter'
    sql = "select * from {} where c_cip like '{}%'".format(table, letter)
    dades = list(u.Database("exadata", "data").get_all(sql))
    with u.Database("exadata", "data") as exadata:
        exadata.list_to_table(dades, anual)


if __name__ == "__main__":
    """
    Executa el proc�s principal de transfer�ncia i gesti� de dades.
    
    1. Verifica si �s el proc�s anual (desembre).
    2. Obt� les columnes de la taula 'dbs' de Redics.
    3. Obt� les columnes de la taula 'dbs' d'Exadata.
    3. Crea la taula anual a Sidics i Exadata.
    4. Assigna permisos als usuaris de DWH.
    5. Processa els prefixos de 'c_cip' en paral�lel amb m�ltiples processos.
    """
    if u2.IS_MENSUAL and dext.month == 12:

        # Obtenir columnes de Sidics en format de creaci� de taula
        with u.Database("redics", "data") as redics:
            columns = [redics.get_column_information(column, table)["create"]
                       for column in redics.get_table_columns(table)]

        # Crear la taula anual a Sidics 
        u.Database("sidics", "dbs").create_table(anual, columns, remove=True)

        # Obtenir columnes d'Exadata en format de creaci� de taula i crear la taula anual a Exadata
        with u.Database("exadata", "data") as exadata:
            columns = [exadata.get_column_information(column, table)["create"]
                       for column in exadata.get_table_columns(table)]
            # Mapeig de tipus de columnes de MySQL/MariaDB a Oracle
            mapeig_tipus_columnes = {
                'varchar': 'VARCHAR2',
                'int': 'NUMBER(10)',
                'double': 'NUMBER',
                'date': 'DATE'}

            # Convertir tipus de dades de MySQL/MariaDB a Oracle
            columnes_exadata = []
            for columna in columns:
                nom_columna, tipus_sidics = columna.split()

                # Si el tipus t� longitut, extreure-la (ex: varchar(40))
                match = re.match(r'(\w+)(\(\d+\))?', tipus_sidics)
                if match:
                    tipus_base = match.group(1)  # Tipus sense par�ntesis
                    longitut = match.group(2) if match.group(2) else ""  # Mantenir longitut si existeix
                    tipus_exadata = mapeig_tipus_columnes.get(tipus_base, tipus_base)  # Convertir tipus
                    columnes_exadata.append("{} {}{}".format(nom_columna, tipus_exadata, longitut)) # Afegir columna i format convertit

        # Crear la taula anual a Exadata 
        u.Database("exadata", "data").create_table(anual, columnes_exadata, remove=True)
        u2.grantSelect(anual, "DWSISAP_ROL", "exadata")

        # Preparar la llista de prefixos de 'c_cip' per processar en paral�lel
        jobs = []
        digits = [format(digit, 'x').upper() for digit in range(16)]
        for digit1 in digits:
            for digit2 in digits:
                jobs.append(digit1 + digit2)
        
        # Executar el proc�s en paral�lel amb 4 processos simultanis
        u2.multiprocess(worker, jobs, 4)