# coding: latin1
import sisapUtils as u
import parameters as p
from sys import exit
import datetime as d


force = True
COVID = False
POCS = True


def is_good():
    prd, dev, good, whats_wrong = {}, {}, {}, []
    label = ['n', 'ps', 'far', 'n lab', 'val lab', 'n var', 'val lab', 'vac',
             'nen']
    sql = """
        SELECT
            c_sector,
            count(1),
            sum(decode(ps_dislipemia_data, null, 0, 1)),
            sum(decode(f_hipolipemiants, null, 0, 1)),
            sum(decode(v_col_ldl_valor, null, 0, 1)),
            avg(v_col_ldl_valor),
            sum(decode(v_tas_valor, null, 0, 1)),
            avg(v_tas_valor),
            decode(c_sector, '6951', 9, sum(decode(i_hib_data, null, 0, 1))),
            sum(decode(v_ppes_data, null, 0, 1))
        FROM {}
        GROUP BY c_sector
        """
    for row in u.getAll(sql.format(p.dbs_tb_prd), p.dbs_db):
        prd[row[0]] = row[1:]
    for row in u.getAll(sql.format(p.dbs_tb_dev), p.dbs_db):
        dev[row[0]] = row[1:]
    for sector, valors in prd.items():
        if sector not in dev:
            good[sector] = False
            whats_wrong.append([sector, "no existeix"])
        else:
            good[sector] = all([prd[sector][i] * 0.95 <= dev[sector][i] <= prd[sector][i] * 1.1 for i in range(len(valors))])  # noqa
            whats_wrong.append([(sector, label[i], prd[sector][i], dev[sector][i]) for i in range(len(valors)) if not prd[sector][i] * 0.95 <= dev[sector][i] <= prd[sector][i] * 1.1])  # noqa
    return (all(good.values()), whats_wrong)


def dev_to_prd():
    # u.backupTable(dbs_tb_prd, dbs_db)
    proc = """
        BEGIN
            :out := trunca_dbs;
        END;
    """
    u.executeProc(proc, p.dbs_db)
    excluded = ("C_CIP", "C_SECTOR", "C_SAP", "C_UP", "C_UP_ORIGEN",
                "C_UP_ABS", "C_CENTRE", "C_METGE", "C_INFERMERA", "HASH")
    for column in [column for column
                   in u.getTableColumns(p.dbs_tb_prd, p.dbs_db)
                   if column not in excluded]:
        u.execute('alter table redics.{} drop column {}'.format(p.dbs_tb_prd,
                                                                column),
                  p.dbs_db)
    columns = [u.getColumnInformation(column, p.dbs_tb_dev, p.dbs_db, desti='ora')['create']
               for column in u.getTableColumns(p.dbs_tb_dev, p.dbs_db)
               if column not in excluded]
    columns.append('grup int')
    if COVID:
        columns.extend([u.getColumnInformation(column, "SISAP_COVID_DBC", p.dbs_db, desti='ora')['create']
                        for column in u.getTableColumns("SISAP_COVID_DBC", p.dbs_db)
                        if column not in excluded])
    for column in columns:
        u.execute('alter table redics.{} add ({})'.format(p.dbs_tb_prd,
                                                          column),
                  p.dbs_db)
        try:
            u.execute('alter table {} add ({})'.format(p.dbs_tb_frm, column),
                      p.dbs_farm_db)
        except Exception:
            pass
    if COVID:
        dbc = [u.getColumnInformation(column, "SISAP_COVID_DBC", p.dbs_db, desti='ora')['query']
               for column in u.getTableColumns("SISAP_COVID_DBC", p.dbs_db)
               if column not in excluded]
        sql = """insert into {} select a.*, 
            (CASE WHEN a.PS_INSUF_CARDIACA_DATA IS NOT NULL AND a.PS_DIABETIS2_DATA IS NOT NULL AND a.PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 1
            WHEN a.PS_INSUF_CARDIACA_DATA IS NOT NULL AND a.PS_DIABETIS2_DATA IS NOT NULL THEN 2
            WHEN a.PS_INSUF_CARDIACA_DATA IS NOT NULL AND a.PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 3
            WHEN a.PS_INSUF_CARDIACA_DATA IS NOT NULL THEN 4
            WHEN (a.PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL or PS_ACV_MCV_DATA is not null or PS_RENAL_CRO_data is not null) AND a.PS_DIABETIS2_DATA IS NOT NULL AND a.PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 5
            WHEN (a.PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL or PS_ACV_MCV_DATA is not null or PS_RENAL_CRO_data is not null) AND a.PS_DIABETIS2_DATA IS NOT NULL THEN 6
            WHEN (a.PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL or PS_ACV_MCV_DATA is not null or PS_RENAL_CRO_data is not null) AND a.PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 7
            WHEN (a.PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL or PS_ACV_MCV_DATA is not null or PS_RENAL_CRO_data is not null) THEN 8
            WHEN a.PS_DIABETIS2_DATA IS NOT NULL AND a.PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 9
            WHEN a.PS_DIABETIS2_DATA IS NOT NULL THEN 10
            WHEN a.PS_MPOC_ENFISEMA_DATA IS NOT NULL AND a.PS_HTA_DATA IS NOT NULL THEN 11
            WHEN a.PS_MPOC_ENFISEMA_DATA IS NOT NULL AND a.PS_DISLIPEMIA_DATA IS NOT NULL THEN 12
            WHEN a.PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 13
            WHEN a.PS_HTA_DATA IS NOT NULL THEN 14
            WHEN a.PS_DISLIPEMIA_DATA IS NOT NULL THEN 15
            ELSE 0 END) c_GRUP,
            {} from {} a 
            left join sisap_covid_dbc b on a.c_cip = b.hash""".format(p.dbs_tb_prd, ",".join(dbc), p.dbs_tb_dev)
    else:
        sql = """insert into {} select a.*, 
            (CASE WHEN a.PS_INSUF_CARDIACA_DATA IS NOT NULL AND a.PS_DIABETIS2_DATA IS NOT NULL AND a.PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 1
            WHEN a.PS_INSUF_CARDIACA_DATA IS NOT NULL AND a.PS_DIABETIS2_DATA IS NOT NULL THEN 2
            WHEN a.PS_INSUF_CARDIACA_DATA IS NOT NULL AND a.PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 3
            WHEN a.PS_INSUF_CARDIACA_DATA IS NOT NULL THEN 4
            WHEN (a.PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL or PS_ACV_MCV_DATA is not null or PS_RENAL_CRO_data is not null) AND a.PS_DIABETIS2_DATA IS NOT NULL AND a.PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 5
            WHEN (a.PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL or PS_ACV_MCV_DATA is not null or PS_RENAL_CRO_data is not null) AND a.PS_DIABETIS2_DATA IS NOT NULL THEN 6
            WHEN (a.PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL or PS_ACV_MCV_DATA is not null or PS_RENAL_CRO_data is not null) AND a.PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 7
            WHEN (a.PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL or PS_ACV_MCV_DATA is not null or PS_RENAL_CRO_data is not null) THEN 8
            WHEN a.PS_DIABETIS2_DATA IS NOT NULL AND a.PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 9
            WHEN a.PS_DIABETIS2_DATA IS NOT NULL THEN 10
            WHEN a.PS_MPOC_ENFISEMA_DATA IS NOT NULL AND a.PS_HTA_DATA IS NOT NULL THEN 11
            WHEN a.PS_MPOC_ENFISEMA_DATA IS NOT NULL AND a.PS_DISLIPEMIA_DATA IS NOT NULL THEN 12
            WHEN a.PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 13
            WHEN a.PS_HTA_DATA IS NOT NULL THEN 14
            WHEN a.PS_DISLIPEMIA_DATA IS NOT NULL THEN 15
            ELSE 0 END) c_GRUP
            from {} a""".format(p.dbs_tb_prd, p.dbs_tb_dev)
    u.execute(sql, p.dbs_db)
    if POCS:
        try:
            u.execute("drop table pocs purge", p.dbs_db)
        except Exception as e:
            pass
        sqls = (
            """CREATE TABLE pocs AS
                SELECT x.*,
                CASE WHEN fragilitat=1 THEN '3.molt alt' WHEN patologia=1 AND fragilitat=0 THEN '2.alt' WHEN patologia=0 AND fragilitat=0 THEN '1.baix' END risc2 from
                (SELECT
                c_cip,
                c_sector,
                c_up,
                c_metge,
                a.C_INFERMERA,
                c_edat_anys edat,
                decode(c_institucionalitzat, null, 0, 1) c_institucionalitzat,
                decode(pr_pcc_data, null, 0, 1) c_pcc,
                decode(pr_maca_data, null, 0, 1) c_maca,
                decode(ps_atdom_data, null, 0, 1) c_atdom,
                CASE WHEN PS_DEPENDENCIA_DATA IS NOT NULL
                OR V_BARTHEL_VALOR<=60
                OR PR_MACA_DATA IS NOT NULL
                OR PR_PCC_DATA IS NOT NULL
                OR PS_ATDOM_DATA IS NOT NULL
                OR PS_GENT_GRAN_VULNERABLE_DATA IS NOT NULL
                OR PS_FRAGIL_DATA IS NOT NULL
                OR VC_DEPENDENCIA_VALOR IN ('Gran dependència','Dependència severa','Dependència severa nivell 2','Gran dependència nivell 2','Gran dependència nivell 1','Dependència severa nivell 1')
                OR VC_GERONTOP_VALOR IN ('Fràgil i accepta intervenció','Fràgil i no accepta intervenció','Sí fràgil (Aquest ítem no aplica a partir del 30/03/2023')
                OR V_FRAGIL_VIG_valor>0.2
                OR PC_FRGL_M_A_DATA IS NOT NULL OR PC_FRGL_PG_DATA IS NOT NULL THEN 1 ELSE 0 end fragilitat,
                CASE WHEN PS_DEMENCIA_DATA IS NOT NULL OR
                PS_MPOC_ENFISEMA_DATA IS NOT NULL OR
                PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL OR
                PS_INSUF_CARDIACA_DATA IS NOT NULL OR
                PS_RENAL_CRO_DATA IS NOT NULL OR
                PS_M_NEURO_DATA is NOT NULL THEN 1 ELSE 0 END patologia,
                CASE WHEN PS_VIU_SOL_DATA IS NOT NULL
                OR VC_VIU_SOL_VALOR='Viu sol/a' THEN 1 ELSE 0 END viu_sol,
                CASE WHEN a.PS_RESIDENCIA_HABITATGE_DATA IS NOT NULL
                OR PS_INGBAI_DATA IS NOT NULL THEN 1 ELSE 0 END p_social,
                PS_DEPENDENCIA_DATA , v_barthel_valor, pr_maca_data, pr_pcc_data, ps_atdom_data, ps_gent_gran_vulnerable_data, ps_fragil_data, vc_dependencia_valor,
                vc_gerontop_valor, v_fragil_vig_valor, PC_FRGL_M_A_DATA, ps_demencia_data, ps_mpoc_enfisema_data, ps_cardiopatia_isquemica_data, ps_insuf_cardiaca_data,
                ps_renal_cro_data, ps_m_neuro_data, ps_viu_sol_data, vc_viu_sol_valor, PS_RESIDENCIA_HABITATGE_DATA, PS_INGBAI_DATA,
                case when ambit_cod = '03' or regio_cod = '79' then 1 else 0 end toshow,
                v_pocs_data ultima_activitat
                FROM dbs a
                inner join sisap_covid_dbc_centres b on a.c_up = b.up_cod
                WHERE c_edat_anys>=75 AND C_INSTITUCIONALITZAT IS null) x
            """,
            "GRANT SELECT ON pocs TO pdp",
            "CREATE or replace synonym pdp.pocs FOR preduffa.pocs",
            "create index pocs_idx on pocs (c_cip)",
            "update dbs set c_risc_pocs = (select risc2 from pocs where dbs.c_cip = pocs.c_cip) where exists (select 1 from pocs where dbs.c_cip = pocs.c_cip)"
        ) 
        for sql in sqls:
            u.execute(sql, p.dbs_db)
        # exadata
        sql = "select c.usua_cip, a.*, b.revisat \
               from pocs a \
               left join pocsinc2024 b on a.c_cip = b.cip \
               inner join pdptb101 c on a.c_cip = c.usua_cip_cod"
        dades = list(u.getAll(sql, "pdp"))
        cols = u.getTableColumns("pocs", "pdp")
        columns = [u.getColumnInformation(col, "pocs", "pdp", desti="ora")["create"] for col in cols]
        columns.insert(0, "cip varchar2(13)")
        columns.append("revisat date")
        u.createTable("pocs", "({})".format(", ".join(columns)), "exadata", rm=True)
        u.listToTable(dades, "pocs", "exadata")
        u.grantSelect("pocs", "DWSISAP_ROL", "exadata")
    sql = "select id_cip_sec, cast(substr(risc2, 0, 1) as int) \
           from pocs a \
           inner join sisap_u11 b on a.c_cip = b.hash_a and a.c_sector = b.sector"
    dades = [row for row in u.getAll(sql, "redics")]
    cols = "(id_cip_sec int, pocs int)"
    u.createTable("pocs", cols, "nodrizas", rm=True)
    u.listToTable(dades, "pocs", "nodrizas")
    u.execute('truncate table {}'.format(p.dbs_tb_dev), p.dbs_db)
    create_views()
    dat, = u.getOne("SELECT date_format(data_ext, '%d %b') FROM dextraccio", p.nod_db)
    u.sendSISAP(['amercadecosta@gencat.cat',
                 'jcamus@gencat.cat',
                 'framospe@gencat.cat',
                 'lmendezboo@gencat.cat',
                 'sflayeh.bnm.ics@gencat.cat'],
                'Càrrega DBS',
                'all',
                "S\'ha carregat la taula DBS a REDICS amb dades corresponents a {0}.".format(dat))

def create_views():
    excluded = {'adults': ['C_UP_ORIGEN', 'C_EDAT_MESOS',
                           'C_ULTIMA_VISITA_ODN', 'C_RNSA_DATA',
                           'C_RNSA_MESOS'],
                'nens': ['C_ULTIMA_VISITA_ODN'],
                'odn': ['C_EDAT_MESOS', 'C_RNSA_DATA', 'C_RNSA_MESOS']}
    prefix = {'problemes': 'P', 'variables': 'V', 'farmacs': 'F'}
    columns = u.getTableColumns(p.dbs_tb_prd, p.dbs_db)
    for categoria in ('adults', 'nens', 'odn'):
        include = [agr for agr, in u.getAll("""
                                            SELECT agrupador
                                            FROM cat_dbscatdesc
                                            WHERE
                                                {} = 1 and
                                                agrupador not like 'C_%'
                                            """.format(categoria), p.imp_db)]
        columns_categoria = [column for column in columns
                             if (column[0] == 'C'
                                 and column not in excluded[categoria]) or
                             any([col_in for col_in in include
                                  if col_in in column])]
        sql = """
            CREATE OR REPLACE view dbs_{} AS
            SELECT {} FROM {}
            """.format(categoria, ', '.join(columns_categoria), p.dbs_tb_prd)
        if categoria in ('adults', 'nens'):
            cond = " WHERE c_edat_anys {} 15"
            cond = cond.format('>=' if categoria == 'adults' else '<')
            sql += cond
        u.execute(sql, p.dbs_farm_db)

    categoria = 'adults'
    include = [agr for agr, in u.getAll("""
                                        SELECT agrupador
                                        FROM cat_dbscatdesc
                                        WHERE
                                            {} = 1 AND
                                            agrupador NOT LIKE 'C_%'
                                        """.format(categoria), 'import')]
    columns_categoria = [column for column in columns
                         if (column[0] == 'C'
                             and column not in excluded[categoria])
                         or [col_in for col_in in include
                             if (col_in in column
                                 and ((column[0] + column[1]) != 'PS')
                                 and (column[0] != 'V')
                                 and ((column[0] + column[1]) != 'F_'))
                             ]
                         ]
    tipus = 'general'
    nomview = categoria + '_' + tipus

    sql = """
        CREATE OR REPLACE view dbs_{} AS SELECT {} FROM {}
        """.format(nomview, ', '.join(columns_categoria), p.dbs_tb_prd)

    if categoria in ('adults', 'nens'):
        cond = """ where c_edat_anys {} 15"""
        cond = cond.format('>=' if categoria == 'adults' else '<')
        sql += cond
    u.execute(sql, p.dbs_farm_db)

    for tipus in ('problemes', 'variables', 'farmacs'):
        nomview = categoria + '_' + tipus
        columns_categoria = [column for column in columns
                             if (column[0] == 'C'
                                 and column not in excluded[categoria])
                             or [col_in for col_in in include
                                 if (col_in in column
                                     and (column[0] + column[1]) == 'PR')
                                 or [col_in for col_in in include
                                     if (col_in in column
                                         and (column[0] == prefix[tipus]))
                                     and ((column[0] + column[1]) != 'PR')]]]
        sql = """
            CREATE OR REPLACE VIEW dbs_{} AS SELECT {} FROM {}
            """.format(nomview, ', '.join(columns_categoria), p.dbs_tb_prd)
        if categoria in ('adults', 'nens'):
            cond = """ where c_edat_anys {} 15"""
            cond = cond.format('>=' if categoria == 'adults' else '<')
            sql += cond
        u.execute(sql, p.dbs_farm_db)


def createTweet(estatus):
    dext, = u.getOne('select data_ext from dextraccio', 'nodrizas')
    tuitGooden = "A partir de dilluns tindreu el #DBS i #DBSForm actualitzats amb dades fins al " + str(dext)  # noqa
    tuitGood = tuitGooden.decode('latin1')
    tuitNotGood = "Per motius técnics aquesta setmana no s'ha pogut actualitzar el #DBS ni el DBSForm #sisap".decode('latin1')  # noqa
    ara = d.datetime.today()
    diaSet = d.datetime.today().weekday()
    avui = d.date.today()
    string_date = str(avui) + " 18:00:00.0"
    deadline = d.datetime.strptime(string_date, "%Y-%m-%d %H:%M:%S.%f")
    if estatus == 'Good':
        if ara < deadline and diaSet == 6:
            u.newTweet(tuitGood)
        elif diaSet == 5:
            u.newTweet(tuitGood)
        else:
            pass
    if estatus == 'Not Good':
        u.newTweet(tuitNotGood)


def changeDate(estatus):
    query = "select data_ext from nodrizas.dextraccio"
    table = "DBSData"
    if estatus == 'Good':
        u.exportPDP(query=query, table=table, truncate=True)


if __name__ == '__main__':
    all_good, whats_wrong = is_good()
    if force or all_good:
        dev_to_prd()
        p.clean_redis()
        # p.clean_my()
        # createTweet('Good')
        changeDate('Good')
    else:
        # createTweet('Not Good')
        exit(whats_wrong)
