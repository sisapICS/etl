# coding: latin1
from sisapUtils import getRedisConnection, getAll, multiprocess, createTable, listToTable
from parameters import *
from json import loads


class Process(object):

    def __init__(self):
        self.get_agrupadors()
        self.get_structure()
        self.create_structure()
        self.atc_desc = {cod: des for (cod, des) in getAll('select atc_codi, atc_desc from cat_cpftb010_def', imp_db)}
        multiprocess(self.process_it, range(partitions), procs, close=True)

    def get_agrupadors(self):
        self.agrupadors = {}
        for taula, agrupador in getAll('select taula, agrupador from cat_dbscat', imp_db):
            if taula not in self.agrupadors:
                self.agrupadors[taula] = set()
            self.agrupadors[taula].add(agrupador)
        self.num_to_txt = {}
        fets = set()
        sql = "select agrupador, valor, descripcio \
               from dbs_categoriques_descripcions"
        for agr, cod, des in getAll(sql, "redics"):
            self.num_to_txt[(agr, cod)] = des
            fets.add(agr)
        sql = "select a.agrupador, b.vi_min, b.vi_des \
               from cat_dbscat a \
               inner join cat_prstb10{} b on a.codi = b.vi_cod \
               where a.agrupador like 'VC%' and b.codi_sector = '6102'"
        for digit in (6, 7):
            for agr, cod, des in getAll(sql.format(digit), imp_db):
                if agr not in fets:
                    self.num_to_txt[(agr, cod)] = des
        vc_presons = [['VC_CONT_LLIGAMS'] + item
                      for item in [[1, 'Adequat'], [2, 'Inadequat'], [3, 'Fi contencio']]]
        for agr, cod, des in vc_presons:
            if agr not in fets:
                self.num_to_txt[(agr, cod)] = des

    def get_structure(self):
        self.structure = loads(getRedisConnection(redis_host).get('dbs_create'))
        inici = len(self.structure)
        self.structure += [('{}_DATA'.format(agr[0:25]), 'date') for agr in sorted(list(self.agrupadors['problemes'])) if agr[:2] == 'PS']
        self.structure += [('{}_EPISODIS'.format(agr[0:21]), 'number(5, 0)') for agr in sorted(list(self.agrupadors['problemes'])) if agr[:2] == 'EP']
        # self.structure += [('{}_VISITES1'.format(agr[0:21]), 'number(5, 0)') for agr in sorted(list(self.agrupadors['visites1'])) if agr[:2] == 'VI']
        self.structure += [(agr, 'varchar2(500)') for agr in sorted(list(self.agrupadors['visites1']))]
        self.structure += [(agr, 'varchar2(500)') for agr in sorted(list(self.agrupadors['tractaments']))]
        # self.structure += [('{}_DATA'.format(agr), 'date') for agr in sorted(list(self.agrupadors['ares']))]
        temp = set()
        for taula in ('laboratori', 'variables', 'serologies', 'activitats', 'xml_detall_cb', 'vacunes', 'nen12', 'nen11'):
            if taula in self.agrupadors:
                temp |= self.agrupadors[taula]
        for agr in sorted(list(set(temp))):
            if agr[:2] == 'AL':
                self.structure.append((agr, 'number(6, 0)'))
            else:
                self.structure.append(('{}_DATA'.format(agr), 'date'))
                self.structure.append(('{}_VALOR'.format(agr), 'varchar2(255)' if agr[:3] == 'VC_' else 'number(17, 2)'))
        temp = set()
        for taula in self.agrupadors:
            if taula[:3] == 'odn':
                temp |= self.agrupadors[taula]
        for agr in sorted(list(set(temp))):
            if agr[:2] == 'OE':
                self.structure.append((agr, 'varchar2(255)'))
            elif agr[:2] in ('OR', 'OT'):
                self.structure.append((agr, 'date'))
            elif agr[:5] == 'OC_IR':
                self.structure.append((agr, 'number(5, 2)'))
            else:
                self.structure.append((agr, 'number(4, 0)'))
        temp = set()
        for taula in self.agrupadors:
            if taula in ('ares', 'alertes', 'presc_social'):
                temp |= self.agrupadors[taula]
        for agr in sorted(list(set(temp))):
            self.structure.append(('{}_DATA'.format(agr), 'date'))
        self.position = {field: i for i, (field, format) in enumerate(self.structure)}
        self.diff = len(self.structure) - inici

    def create_structure(self):
        sql = ','.join(['{} {}'.format(field, format) for field, format in self.structure])
        createTable(dbs_tb_dev, '({})'.format(sql), dbs_db, rm=True)

    def process_it(self, partition):
        data = {}
        sql = 'select id_cip_sec, agr, attr_txt, attr_num from {} partition (p{})'.format(my_tb, partition)
        for id, agr, txt, num in getAll(sql, my_db):
            domini = agr.split('_')[0]
            if domini == 'PS':
                key = (id, '{}_DATA'.format(agr))
                if key not in data:
                    data[key] = txt
                elif txt < data[key]:
                    data[key] = txt
            elif domini == 'F':
                key = (id, agr)
                if key not in data:
                    data[key] = set()
                data[key].add(txt)
            elif domini == 'V':
                key_d = (id, '{}_DATA'.format(agr))
                key_v = (id, '{}_VALOR'.format(agr))
                if key_d not in data:
                    data[key_d] = txt
                    data[key_v] = num
                elif txt > data[key_d]:
                    data[key_d] = txt
                    data[key_v] = num
                elif txt == data[key_d]:
                    if "SEROLOGIA" in agr and num < data[key_v]:
                        data[key_v] = num
                    elif num > data[key_v]:
                        data[key_v] = num
            elif domini == 'I':
                key_d = (id, '{}_DATA'.format(agr))
                key_v = (id, '{}_VALOR'.format(agr))
                if key_d not in data:
                    data[key_d] = txt
                    data[key_v] = set()
                elif txt > data[key_d]:
                    data[key_d] = txt
                data[key_v].add(txt)
            elif domini == 'VC':
                key_d = (id, '{}_DATA'.format(agr))
                key_v = (id, '{}_VALOR'.format(agr))
                num_des = self.num_to_txt.get((agr, int(num)))
                if num_des:
                    if key_d not in data:
                        data[key_d] = txt
                        data[key_v] = num_des
                    elif txt > data[key_d]:
                        data[key_d] = txt
                        data[key_v] = num_des
            elif domini == 'EP':
                key = (id, '{}_EPISODIS'.format(agr))
                if key not in data:
                    data[key] = set()
                data[key].add(txt)
            elif domini == 'AL':
                key = (id, agr)
                first = '_INI' in agr
                if key not in data:
                    data[key] = num
                elif (first and num < data[key]) or (not first and num > data[key]):
                    data[key] = num
            elif domini == 'OC':
                key = (id, agr)
                data[key] = num
            elif domini in ('OP', 'OE'):
                key = (id, agr)
                if key not in data:
                    data[key] = set()
                data[key].add(num)
            elif domini in ('OR', 'OT'):
                key = (id, agr)
                if key not in data:
                    data[key] = txt
                elif txt > data[key]:
                    data[key] = txt
            elif domini in ('PC', 'AV', 'S'):
                key = (id, '{}_DATA'.format(agr))
                if key not in data:
                    data[key] = txt
                elif txt > data[key]:
                    data[key] = txt
            elif domini == 'VI':
                key = (id, agr)
                if key not in data:
                    data[key] = []
                data[key].append(txt)
        self.upload_it(partition, data)

    def upload_it(self, partition, data):
        poblacio = loads(getRedisConnection(redis_host).get('dbs_pob:{}'.format(partition)))
        poblacio = {int(key): info + [None] * self.diff for key, info in poblacio.items()}
        for (id, field), val in data.items():
            domini = field.split('_')[0]
            if domini == 'F':
                poblacio[id][self.position[field]] = '�'.join(sorted([self.atc_desc[atc] for atc in val]))
            elif domini in ('EP', 'OP', 'VI'):
                poblacio[id][self.position[field]] = len(val)
            elif domini == 'OE':
                poblacio[id][self.position[field]] = '�'.join(sorted([str(int(peca)) for peca in val]))
            elif domini == 'I':
                if field[-5:] == 'VALOR':
                    poblacio[id][self.position[field]] = len(val)
                else:
                    poblacio[id][self.position[field]] = val
            else:
                poblacio[id][self.position[field]] = val
        listToTable([info for id, info in poblacio.items()], dbs_tb_dev, dbs_db, format_date='YYYYMMDD')


if __name__ == '__main__':
    process = Process()
