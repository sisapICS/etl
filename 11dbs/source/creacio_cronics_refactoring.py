# -*- coding: utf-8 -*-

import sisapUtils as u
from collections import defaultdict
import pandas as pd
import math
from datetime import datetime
import numpy as np
from dateutil.relativedelta import relativedelta


# Ignorar warnings
import warnings
warnings.filterwarnings("ignore")

############## AUXILIAR FUNCTIONS ##############
def up_metges_mes(up_metge, col, metges_vistos):
    """
    Returns the up of the doctor
    """
    if col.month not in (1,8,12):
        return up_metge
    else: 
        if up_metge not in metges_vistos: return up_metge
        else: return ' '

def up_mes(df, metges_vistos):
    """ 
    Doctor information and vectors of anual days 
    """
    return pd.Series([
        up_metges_mes(up_metge, col, metges_vistos)
        for (up_metge, col) in 
        zip(df['up_metge'], df['COLEST'])
      ])

def incrementant(one_year):
    """
    Marks with -1 non-visitable days
    Aka, weekends, January, August and 15 
    last days of December
    """
    increment = []
    for i in range(len(one_year)):
        # weekends
        if one_year[i].weekday() > 4:
            increment.append(-1)
        # January and August
        elif one_year[i].month == 1 or one_year[i].month == 8:
            increment.append(-1)
        # 15 last days of December
        elif one_year[i].month == 12 and one_year[i].day >= 15:
            increment.append(-1) 
        else: increment.append(1)
    return np.array(increment)

def cap_setmana(one_year):
    """
    Computes whether its is weekend or not
    """
    increment_DS = []
    increment_DG = []
    for i in range(len(one_year)):
        if one_year[i].day == 6: increment_DS.append(i)
        elif one_year[i].day == 7: increment_DG.append(i)
    return increment_DS, increment_DG

def dia_1_mes(one_year):
    """
    Computes the fist programmable day of the desidered months
    """
    dies_1 = []
    dies_1.append(0)
    for i in range(len(one_year)):
        if one_year[i].day == 1 and one_year[i].month not in (1,8,12): dies_1.append(i)
    return dies_1

"""
All the following functions compute the
next test depending on the test frequency
and the patient diagnosis.
"""

def colest(date, CI, IC, DM, GREU, LLEU, HTA):
    """
    Computes next colesterol date in one year
    for all groups but 13 (MPOC aïllat)
    """
    if (GREU == 1 or LLEU == 1) and (CI + IC + DM == 0):
        return (date + pd.offsets.DateOffset(years=2))
    else: return (date + pd.offsets.DateOffset(years=1))

def proper_COLEST(df):
    """
    Computes next colesterol date
    """
    return pd.Series([
        colest(date, CI, IC, DM, GREU, LLEU, HTA)
        for (date, CI, IC, DM, GREU, LLEU, HTA) in 
        zip(df['COLEST'], df['CI'], df['IC'], df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'], df['HTA'])
      ])

def analisi_sang(edat, grup):
    """
    Computes the next blood test taking into account
    the age and the dx group of the patient
    """
    if (edat > 80 and (grup == 12 or grup == 15)) or grup == 13:
        return 0
    else: 
        return 1

def P_ANALISI_SANG(df):
    """
    Next blood test
    """
    return pd.Series([
        analisi_sang(edat, grup)
        for (edat, grup) in 
        zip(df['C_EDAT_ANYS'], df['grup'])
      ])

def orina(edat, HTA, DM2, grup):
    """
    Computes the next urine analysis taking into account
    the age and the dx group of the patient
    """
    if (edat > 80 and DM2 == 1) or (HTA == 1 and edat > 75 and DM2 == 0) or (grup in (12,13,15)):
        return 0
    else: 
        return 1

def P_ORINA(df):
    """
    Next urine analysis
    """
    return pd.Series([
        orina(edat, HTA, DM2, grup)
        for (edat, HTA, DM2, grup) in 
        zip(df['C_EDAT_ANYS'], df['HTA'], df['DM2'], df['grup'])
      ])

def espiro1(espiro, visita, grup, edat, ATDOM):
    """
    Computes the next spirometry taking into account
    the age and the dx group of the patient
    """
    if ATDOM == 1 or edat > 80:
        return 0
    elif grup in (1,3,5,7,9,11,12,13):
        if espiro is None: return 1
        elif visita is None:
            if espiro + relativedelta(days=365) < datetime.now(): return 1
            else: return 0
        elif espiro <= visita or espiro - relativedelta(days=180) <= visita:
            return 1
    else: 
        return 0

def P_ESPIRO(df):
    """
    Next spirometry
    """
    return pd.Series([
        espiro1(ESPIRO, VISITA, grup, edat, ATDOM)
        for (ESPIRO, VISITA, grup, edat, ATDOM) in 
        zip(df['proper_ESPIRO1'], df['data_final'], df['grup'], df['C_EDAT_ANYS'], df['ATDOM'])
      ])

def FO_(data, visita, grup, ATDOM, edat):
    """
    Computes the next diabetics eye test into account
    the age, the dx group of the patient and
    whether the patient is atdom
    """
    if ATDOM == 1 or edat > 80:
        return 0
    elif grup in (1,2,5,6,9,10):
        if data is None: return 1
        elif visita is None: 
            if data + relativedelta(days=180) < datetime.now(): return 1
            else: return 0
        elif data + relativedelta(days=180) > visita:
            return 0
        else: return 1
    else: 
        return 0

def P_FO(df):
    """
    Next diabetics eye test
    """
    return pd.Series([
        FO_(FO, VISITA, DM, ATDOM, edat)
        for (FO, VISITA, DM, ATDOM, edat) in zip(df['proper_FONS_ULL'], df['data_final'], df['grup'], df['ATDOM'],df['C_EDAT_ANYS'])
      ])

def COLEST(data, visita, CI, IC, DM, GREU, LLEU, DIS, HTA):
    """
    Computes the next blood test taking into account
    the age, the dx group of the patient and
    whether the patient is atdom
    """
    if ((CI + IC + DM) == 0 and (GREU == 1 or LLEU == 1)) or ((LLEU + GREU + HTA + DIS) >= 3 and (CI + IC + DM) == 0) or ((HTA + DIS) == 2 and (CI + IC + DM + LLEU + GREU) == 0):
        return 0
    else: 
        if data is None: return 1
        elif visita is None: 
            if data + relativedelta(days=180) < datetime.now(): return 1
            else: return 0
        elif data + relativedelta(days=180) > visita:
            return 0
        else: return 1

def P_LIPIDS(df):
    """
    Next blood test
    """
    return pd.Series([
        COLEST(COL, VISITA, CI, IC, DM, GREU, LLEU, DIS, HTA)
        for (COL, VISITA, CI, IC, DM, GREU, LLEU, DIS, HTA) in zip(df['proper_COLEST'], df['data_final'],
                                     df['CI'], df['IC'], df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'], df['DISLIPEMIA'], df['HTA'])
      ])

def ECG1(data, visita, grup, ATDOM):
    """
    Computes the next electrocardiogram test taking into account
    the age, the dx group of the patient and
    whether the patient is atdom
    """
    if grup in (12,13,15):
        return 0
    elif ATDOM == 1:
        return 0
    else: 
        if data is None: return 1
        elif visita is None: 
            if data + relativedelta(days=180) < datetime.now(): return 1
            else: return 0
        elif data + relativedelta(days=180) > visita:
            return 0
        else: return 1

def P_ECG(df):
    """
    Next electrocardiogram
    """
    return pd.Series([
        ECG1(EC, VISITA, grup, ATDOM)
        for (EC, VISITA, grup, ATDOM) in zip(df['proper_ECG'], df['data_final'], 
                                     df['grup'], df['ATDOM'])
      ])

def fons_ull(date, grup):
    """
    Computes the next eye test taking into account
    the age and the dx group of the patient
    """
    if grup in (1,2,5,6,9,10):
        if date is None: return datetime.now()
        else: return date + relativedelta(years=2)
    else:
        return datetime(1970,1,1,0,0)
    
def proper_FONS_ULL(df):
    """
    Next eye test
    """
    return pd.Series([
        fons_ull(date, grup)
        for (date, grup) in 
        zip(df['FONS_ULL'], df['grup'])
      ])

def espiro(date, grup):
    """
    Computes the next spirometry taking into account
    the age, the dx group of the patient and
    whether the patient is atdom
    """
    if date is None and grup in (1,3,5,77,9,11,12,13): return datetime.now()
    if grup not in (1,3,5,77,9,11,12,13): return datetime(1970,1,1,0,0)
    else: return (date + relativedelta(years=2) if grup in (1,3,5,77,9,11,12,13) else datetime(1970,1,1,0,0))
    
def proper_espiro(df):
    """
    Next spirometry
    """
    return pd.Series([
        espiro(date, grup)
        for (date, grup) in zip(df['ESPIRO1'], df['grup'])
      ])

def ECG(date, grup):
    """
    Computes the next electrocardiogram test taking into account
    the age, the dx group of the patient and
    whether the patient is atdom
    """
    if grup in (9,10,11,14):
        if date is None: return datetime.now()
        else: return date + relativedelta(years=2)
    elif grup in (1,2,3,4,5,6,7,8):
        if date is None: return datetime.now()
        else: return date + relativedelta(years=1)
    else: return datetime(1970,1,1,0,0)
    
def proper_ECG(df):
    """
    Next electrocardiogram
    """
    return pd.Series([
        ECG(date, grup)
        for (date, grup) in zip(df['ECG'], df['grup'])
      ])

def grups(CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU, AVC):
    """
    Computes the group to the one the user
    belogs to taking into account the dx
    """
    if IC == 1:
        if DM == 1 and (GREU == 1 or LLEU == 1): return 1
        elif DM == 1: return 2
        elif (GREU == 1 or LLEU == 1): return 3
        else: return 4
    elif CI == 1 or AVC == 1:
        if DM == 1 and (GREU == 1 or LLEU == 1): return 5
        elif DM == 1: return 6
        elif (GREU == 1 or LLEU == 1): return 7
        else: return 8
    elif DM == 1:
        if GREU == 1 or LLEU == 1: return 9
        else: return 10
    elif GREU == 1 or LLEU == 1:
        if HTA == 1: return 11
        elif DISLIPEMIA == 1: return 12
        else: return 13
    elif HTA == 1: return 14
    elif DISLIPEMIA == 1: return 15

def agrupadors(df):
    """
    Planifi.cat groups
    """
    return pd.Series([
        grups(CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU, AVC)
        for (CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU, AVC) in zip(df['CI'], df['IC'], df['HTA'], df['DISLIPEMIA'],
                                                        df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'], df['AVC'])
      ])

def dx(CI, IC, AVC, HTA, DISLIPEMIA, DM, GREU, LLEU):
    """
    Defines the text of the dx
    """
    txt = ''
    buit = True
    if IC == 1:
        txt = txt + 'IC'
        buit = False
    if CI == 1:
        if buit:
            txt = txt + 'CI'
            buit = False
        else:
            txt = txt + ',CI'
    if AVC == 1:
        if buit:
            txt = txt + 'AVC'
            buit = False
        else:
            txt = txt + ',AVC'
    if DM == 1:
        if buit:
            txt = txt + 'DM2'
            buit = False
        else:
            txt = txt + ',DM2'
    if GREU == 1 or LLEU == 1:
        if buit:
            txt = txt + 'MPOC'
            buit = False
        else:
            txt = txt + ',MPOC'
    if HTA == 1:
        if buit:
            txt = txt + 'HTA'
            buit = False
        else:
            txt = txt + ',HTA'
    if DISLIPEMIA == 1:
        if buit:
            txt = txt + 'dislipemia'
            buit = False
        else:
            txt = txt + ',dislipemia'
    return txt

def diagnostic(df):
    """
    Computes dx in text format
    """
    return pd.Series([
        dx(CI, IC, AVC, HTA, DISLIPEMIA, DM, GREU, LLEU)
        for (CI, IC, AVC, HTA, DISLIPEMIA, DM, GREU, LLEU) in zip(df['CI'], df['IC'], df['AVC'], df['HTA'], df['DISLIPEMIA'],
                                                        df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'])
      ])


class Planificat():
    def __init__(self):
        self.get_dextraccio()
        self.particio1, self.particio2  = self.get_particions()
        self.get_analitiques()
        self.get_dbs_pandas()
        self.transformacio_df()
        self.calcul_professionals_anual()
        self.assignacio_dies()
        self.assignacio_grups()
        self.exclusions()
        self.diagnostics_tsh_iono()
        self.export()
        # self.copia_seguretat()
        # self.afegir_nous_pacients()
        # self.ressucitar()

    def get_dextraccio(self):
        """ Get the extraction date """
        ts = datetime.now()
        sql = """select day(curdate()), month(curdate()), year(curdate()), weekday(curdate()) from nodrizas.dextraccio"""
        self.dia, self.mes, self.any, self.dia_set = u.getOne(sql, 'nodrizas')
        print('dextraccio:', datetime.now()-ts)
        
    def get_particions(self):
        """ Gets the last two partitions for a table that contains 2 partitions per month """
        ts = datetime.now()
        if self.dia >= 16:
            if len(str(self.mes)) == 1: self.mes = '0'+str(self.mes)
            particio1 = 't'+str(self.any)+str('{}'.format(str(self.mes) if len(str(self.mes)) != 1 else '0'+str(self.mes)))+'01'
            particio2 = 't'+str(self.any)+str('{}'.format(str(self.mes) if len(str(self.mes)) != 1 else '0'+str(self.mes)))+'16'
        else: 
            if self.mes != 1:
                particio1 = 't'+str(self.any)+str('{}'.format(str(self.mes) if len(str(self.mes)) != 1 else '0'+str(self.mes)))+'01'
                particio2 = 't'+str(self.any)+str('{}'.format(str(self.mes-1) if len(str(self.mes-1)) != 1 else '0'+str(self.mes-1)))+'16'
            else:
                particio1 = 't'+str(self.any)+str(self.mes)+'01'+str('{}'.format(str(self.mes) if len(str(self.mes)) != 1 else '0'+str(self.mes)))+'01'
                particio2 = 't'+str(self.any-1)+str('{}'.format(str(self.mes-1) if len(str(self.mes-1)) != 1 else '0'+str(self.mes-1)))+'16'
        print('particions:', datetime.now()-ts)
        return particio1, particio2
        
    def get_analitiques(self):
        """ Gets last blood test from sidics"""
        ts = datetime.now()
        self.analitiques = dict()
        sql = """select cr_cip, cr_data_reg from sidics.labtb101 partition({}, {}) where cr_codi_prova_ics in (
                        select codi from catalegs.cat_dbscat where agrupador = 'V_COL_TOTAL')
                        order by cr_data_reg """.format(self.particio1, self.particio2)
        try:
            for cip, data in u.getAll(sql, ("sidics", "x0002")):
                self.analitiques[cip] = data
        except:
            pass
        print('analitiques:', datetime.now()-ts)

    def get_dbs_pandas(self):
        """ Gets data from DBS and stores it in a PD """
        ts = datetime.now()
        self.info_up_uba = {}
        # Downloads DBS information
        sql = """SELECT c_cip, c_up, c_centre, c_metge, C_SECTOR, C_INFERMERA, C_EDAT_ANYS,
                CASE WHEN PS_ATDOM_DATA IS NOT NULL THEN 1 END ATDOM,
                CASE WHEN C_INSTITUCIONALITZAT IS NOT NULL THEN 'R' WHEN PS_ATDOM_DATA IS NOT NULL AND C_INSTITUCIONALITZAT IS NULL THEN 'A' END ATDOM_RESIDENCIA, 
                CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL THEN 1 end CI,
                CASE WHEN PS_ACV_MCV_DATA IS NOT NULL THEN 1 end AVC,
                CASE WHEN PS_HTA_DATA IS NOT NULL THEN 1 END HTA,
                CASE WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 1 END dm2,
                CASE WHEN PS_INSUF_CARDIACA_DATA IS NOT NULL THEN 1 END IC,
                CASE WHEN PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 1 END MPOC_GREU,
                CASE WHEN PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 1 END MPOC_LLEU,
                CASE WHEN PS_DISLIPEMIA_DATA IS NOT NULL THEN 1 END dislipemia,
                (CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL OR PS_INSUF_CARDIACA_DATA IS NOT NULL 
                OR PS_ACV_MCV_DATA IS NOT NULL OR (PS_MPOC_ENFISEMA_DATA IS NOT NULL)THEN 3
                WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 2 ELSE 1 END) risc,
                CASE WHEN F_HTA_COMBINACIONS is NOT NULL OR F_HTA_DIURETICS IS NOT NULL OR F_HTA_IECA_ARA2 IS NOT NULL THEN 1 ELSE 0 END FarmHTA,
                CASE WHEN PS_HIPOTIROIDISME_DATA IS NOT NULL THEN 1 END HIPOTIROIDISME,
                V_HBA1C_DATA glicada_DM,
                V_COL_TOTAL_DATA  colest,
                V_ECG_AMB_DATA ECG,
                CASE WHEN (V_FEV1_DATA >= V_FEV1_FVC_DATA or V_FEV1_FVC_DATA IS  null) AND (V_FEV1_DATA >= VC_VAL_ESPIRO_DATA or VC_VAL_ESPIRO_DATA IS  null) THEN V_FEV1_DATA 
                    WHEN (V_FEV1_FVC_DATA >= V_FEV1_DATA or V_FEV1_DATA IS  null) AND (V_FEV1_FVC_DATA >= VC_VAL_ESPIRO_DATA or VC_VAL_ESPIRO_DATA IS  null) THEN V_FEV1_FVC_DATA 
                    WHEN (VC_VAL_ESPIRO_DATA >= V_FEV1_DATA or V_FEV1_DATA IS  null) AND (VC_VAL_ESPIRO_DATA >= V_FEV1_FVC_DATA or V_FEV1_FVC_DATA IS  null) THEN VC_VAL_ESPIRO_DATA 
                END espiro1,
                V_FEV1_FVC_DATA espiro2,
                V_FONS_ULL_DATA fons_ull
            fROM 
                dbs 
            WHERE 
                C_EDAT_ANYS > 14 AND
                c_sector != '6951' AND
                (PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL 
                OR ps_hta_data IS NOT NULL 
                OR PS_DIABETIS2_DATA IS NOT NULL OR 
                PS_INSUF_CARDIACA_DATA is NOT NULL OR 
                PS_MPOC_ENFISEMA_DATA IS NOT NULL  OR 
                PS_DISLIPEMIA_DATA IS NOT NULL OR
                PS_ACV_MCV_DATA IS NOT NULL) AND 
                PR_MACA_DATA IS NULL AND
                PS_DEMENCIA_DATA IS NULL AND
                PS_CURES_PALIATIVES_DATA IS NULL AND 
                (CASE WHEN (PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL 
                OR PS_INSUF_CARDIACA_DATA IS NOT NULL 
                OR PS_MPOC_ENFISEMA_DATA IS NOT NULL
                OR PS_ACV_MCV_DATA IS NOT NULL)THEN 3
                WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 2 ELSE 1 END) IN (2,3)"""
        upload = []
        # Patients that we want to wake up
        self.info_resucitar = defaultdict(list)
        # Patients that belong to the project
        self.cronics_set = set()
        for (c_cip, c_up, c_centre, c_metge, C_SECTOR, C_INFERMERA, c_edat, atdom, ATDOM_RESIDENCIA,
                CI, AVC, HTA, dm2, IC, MPOC_GREU, MPOC_LLEU, dislipemia, risc, FarmHTA,
                HIPOTIROIDISME, glicada_DM, colest, ECG, espiro1, espiro2, fons_ull) in u.getAll(sql, 'redics'):
            if c_cip in self.analitiques:
                colest = self.analitiques[c_cip]
            upload.append([c_cip, c_up, c_centre, c_metge, C_SECTOR, C_INFERMERA, c_edat, atdom, ATDOM_RESIDENCIA, 
                            CI, AVC, HTA, dm2, IC, MPOC_GREU, MPOC_LLEU, dislipemia, 
                            risc, FarmHTA, HIPOTIROIDISME, glicada_DM, colest, ECG, 
                            espiro1, espiro2, fons_ull])
            if risc >= 2:
                self.info_resucitar[c_cip].append(C_SECTOR)
                self.cronics_set.add((c_cip, C_SECTOR))
            self.info_up_uba[c_cip] = [c_up, c_metge]  
        self.dbs = pd.DataFrame(upload)
        self.dbs.columns =['C_CIP', 'C_UP', 'C_CENTRE', 'C_METGE', 'C_SECTOR', 'C_INFERMERA',
                        'C_EDAT_ANYS', 'ATDOM',
                        'ATDOM_RESIDENCIA', 'CI', 'AVC', 'HTA', 'DM2', 'IC', 
                        'MPOC_GREU', 'MPOC_LLEU', 'DISLIPEMIA', 'RISC', 'FARMHTA',
                        'HIPOTIROIDISME', 'GLICADA_DM', 'COLEST', 'ECG', 
                        'ESPIRO1', 'ESPIRO2', 'FONS_ULL']
        print('len dbs:', len(self.dbs))
        print('Time DBS execution {}'.format(datetime.now() - ts))
        # Weird bug
        self.dbs = self.dbs.replace(np.nan, np.nan)

    def transformacio_df(self):
        # Data transformation to datetime format
        ts = datetime.now()
        for date_type in ('GLICADA_DM', 'COLEST'):
            self.dbs[date_type]= pd.to_datetime(self.dbs[date_type])
        for e in ('ESPIRO1', 'ESPIRO2', 'FONS_ULL', 'ECG'):
            print(e)
            for i in range(len(self.dbs)):
                try:
                    if np.isnan(self.dbs[e][i]) == False:
                        pass
                except:
                    if str(self.dbs[e][i])[0:3] not in ('200', '199', '201', '202'):
                        self.dbs[e][i] = np.nan
            self.dbs[e]= pd.to_datetime(self.dbs[e])
        # We need 5 characters up in a string format
        self.dbs['C_UP'] = self.dbs['C_UP'].apply(lambda x: str(x).zfill(5))
        # Just patients with risk = 2 or 3
        self.dbs_risc = self.dbs[self.dbs.RISC >= 2]
        self.dbs_risc.reset_index(inplace=True, drop=True)
        print('len dels de risc: ', len(self.dbs_risc), 'transformations time', datetime.now()-ts)

    def calcul_professionals_anual(self):
        ts = datetime.now()
        # Doctors to dict, id_metge and number of annual patients
        self.dbs_risc['up_metge'] = self.dbs_risc["C_UP"].astype(str)+self.dbs_risc["C_METGE"]
        self.metges_vistos = []
        self.dbs_risc['up_metge'] = self.dbs_risc["C_UP"].astype(str)+self.dbs_risc["C_METGE"]    
        self.dbs_risc['up_metge_mesos'] = up_mes(self.dbs_risc, self.metges_vistos)
        self.metges = self.dbs_risc['up_metge_mesos'].value_counts()
        self.metges = self.metges.to_dict()
        self.metges_int = dict(zip(np.arange(len(self.metges)), self.metges.keys()))
        self.metges_int_reves = dict(zip(self.metges.keys(), np.arange(len(self.metges))))
        self.metges_id = list(self.metges_int.keys())
        # Distribute doctors along the year, by months (9)
        # Number of patients to visit per month and doctor
        self.metges = {k: np.ceil(v / 11) for k, v in self.metges.items()}
        # Days of the year
        start = datetime.today() + relativedelta(days=21)
        end = start + relativedelta(years=1)
        self.one_year = pd.date_range(start = start, end = end, normalize = True).tolist()
        print('one year', len(self.one_year))
        print('prof anuals:', datetime.now()-ts)
    
    def dia(self, id, data, metge):
        """
        Computes next visit day available
        """
        if id in self.exclosos: 
            if data >= datetime.today() + relativedelta(days=21):
                if data.weekday == 5: i = -1
                if data.weekday == 6: i = 1
                else: i = 0
                return data + relativedelta(days=i)
            else:
                data_final = datetime.today() + relativedelta(days=25)
                if data_final.weekday == 5: i = -1
                if data_final.weekday == 6: i = 1
                else: i = 0
                return data_final + relativedelta(days=i)

        if data >= datetime.today() + relativedelta(days=21):
            # If the visit is during a weekend day, we will move it
            # to Monday if it was due in Sunday and to friday in case
            # it was due in Saturday.
            if data.weekday == 5: i = -1
            if data.weekday == 6: i = 1
            else: i = 0
            try:
                final = int((data + relativedelta(days=i)).timestamp() * 1000)
                itemindex = np.where(np.array(self.one_year2)==final)[0][0]
                self.matrix[self.metges_int_reves[metge],itemindex] -= 1
            except: 
                pass
            return data + relativedelta(days=i)
        else:
            try:
                index = np.nonzero(self.matrix[self.metges_int_reves[metge]] > 0)[0][0]
                self.matrix[self.metges_int_reves[metge]][index] -= 1
                return self.one_year[index]
            except:
                self.no_fets += 1

    def assignar_metge(self):
        """ Assigning doctor """
        try:
            return pd.Series([
                self.dia(id, data, metge)
                for (id, data, metge) in 
                zip(self.dbs_risc['C_CIP'], self.dbs_risc['proper_COLEST'], self.dbs_risc['up_metge'])
            ])
        except:
            print(self.dbs_risc.head())

    def assignacio_dies(self):
        """ Compute the visit day of each patient"""
        ts = datetime.now()
        # Build a zero matrix where # rows = # doctors i # columns = # days
        self.matrix = np.zeros((len(self.metges_id), len(self.one_year)), dtype=int)
        # Assuming there are 20 working days x month
        for key, value in self.metges_int.items():
            v = int(self.metges[value] // 20) * incrementant(self.one_year) # Nombre de persones que ha de visitar a diari
            r = self.metges[value] % 20
            dia1 = dia_1_mes(self.one_year)
            for i, j in zip(dia1, dia1[1:] + [366]):
                r1 = r
                while r1 > 0:
                    if i >= j: r1 = 0
                    elif v[i] >= 0:
                        v[i] += 1
                        r1 -= 1
                    i += 1       
            self.matrix[key] = v # Els que ha de visitar si o si a diari
        # Order DF by coleterol date
        self.dbs_risc = self.dbs_risc.sort_values(by=['COLEST'], ascending=(False))
        self.dbs_risc.reset_index(inplace=True, drop=True)
        # Null imputation to epoch date
        for e in ('GLICADA_DM', 'COLEST', 'ECG', 'ESPIRO1', 'FONS_ULL'):
            self.dbs_risc[e] = self.dbs_risc[e].apply(lambda x: x if not pd.isnull(x) or x is not None else datetime(1970,1,1,0,0))
            # New columns creation
            e = 'proper_' + e
            self.dbs_risc[e] = np.nan
        self.dbs_risc['proper_COLEST'] = proper_COLEST(self.dbs_risc)
        self.no_fets = 0
        self.exclosos = set()
        # Excluding excluded patients to not taking them into account
        sql = """select HASH 
                    from planificat
                    where estat in (2, 5)
                    AND (TRUNC(SYSDATE) - trunc(to_date(estat_data, 'yyyy/mm/dd hh24:mi:ss'))) < 400"""
        for hash, in u.getAll(sql, 'pdp'):
            self.exclosos.add(hash)
        self.one_year2 = [int(t.timestamp() * 1000) for t in self.one_year]
        print('colest before', self.dbs_risc['proper_COLEST'][0:10]) 
        self.dbs_risc = self.dbs_risc.sort_values(by=['proper_COLEST'], ascending=False)
        self.dbs_risc.reset_index(inplace=True, drop=True)
        print('colest', self.dbs_risc['proper_COLEST'][0:10]) 
        self.dbs_risc['data_final'] = self.assignar_metge()
        print('no fets: ', self.no_fets)
        print('Time execution seguent visita {}'.format(datetime.now() - ts))
    
    def assignacio_grups(self):
        """
        Assign protocol group to each patient and
        compute the next test date
        """
        print('assignacio grups')
        ts = datetime.now()
        self.dbs_risc['grup'] = agrupadors(self.dbs_risc)
        print('Time execution per agrupadors {}'.format(datetime.now() - ts))
        print('calcul de les properes proves')
        ts = datetime.now()
        self.dbs_risc['proper_FONS_ULL'] = proper_FONS_ULL(self.dbs_risc)
        self.dbs_risc['proper_ESPIRO1'] = proper_espiro(self.dbs_risc)
        self.dbs_risc['proper_ECG'] = proper_ECG(self.dbs_risc)
        print('Time execution per calcul seguents proves {}'.format(datetime.now() - ts))

    def exclusions(self):
        """
        Compute exclusions, mainly based on age
        """
        print('exclusions')
        ts = datetime.now()
        self.dbs_risc['p_analisi_sang'] = P_ANALISI_SANG(self.dbs_risc)
        self.dbs_risc['p_orina'] = P_ORINA(self.dbs_risc)
        self.dbs_risc['p_espiro'] = P_ESPIRO(self.dbs_risc)
        self.dbs_risc['p_fo'] = P_FO(self.dbs_risc)
        self.dbs_risc['p_lipids'] = P_LIPIDS(self.dbs_risc)
        self.dbs_risc['p_ecg'] = P_ECG(self.dbs_risc)
        print('Time execution per calcul seguents proves exclusions {}'.format(datetime.now() - ts))

    def diagnostics_tsh_iono(self):
        """ Add two new diagnoses for blood tests """
        ts = datetime.now()
        print('diagnostics')
        self.dbs_risc['dx'] = diagnostic(self.dbs_risc)
        print('TSH I IONOGRAMA')
        self.dbs_risc['TSH'] = self.dbs_risc['HIPOTIROIDISME'].apply(lambda x: 1 if x == 1 else 0)
        self.dbs_risc['IONOGRAMA'] = (self.dbs_risc['HTA'] + self.dbs_risc['FARMHTA']).apply(lambda x: 1 if x <= 1 else 0)
        print('tsh_iono:', datetime.now()-ts)

    def export(self):
        """ Filter and Export DF to csv"""
        ts = datetime.now()
        print('filtrem columnes')
        dbs_joan_1 = self.dbs_risc[['C_CIP', 'C_UP', 'C_CENTRE', 'C_SECTOR', 'C_METGE', 'C_INFERMERA', 'ATDOM_RESIDENCIA', 'dx',
                        'GLICADA_DM', 'COLEST', 'ECG', 'ESPIRO1',
                        'FONS_ULL', 'data_final', 'TSH',
                        'p_espiro', 'p_fo', 'p_ecg', 'p_analisi_sang', 'p_orina', 'grup']]
        dbs_joan_1 = dbs_joan_1.rename(columns={'GLICADA_DM': 'DM_uv', 'COLEST': 'lipids_uv', 'ECG': 'ECG_uv', 
                                            'ESPIRO1' : 'espiro_uv', 'FONS_ULL': 'FU_uv', 'data_final': 'data_recomanada'})
        dbs_joan_1['data_recomanada'] = dbs_joan_1['data_recomanada'].apply(lambda x: printing(x))
        dbs_joan_1['data_recomanada'] = dbs_joan_1['data_recomanada'].apply(lambda x: x.date())
        print('transformacions de dates')
        for e in ('DM_uv', 'lipids_uv', 'ECG_uv', 'espiro_uv', 'FU_uv'):
            dbs_joan_1[e] = dbs_joan_1[e].apply(lambda x: x if x.year not in (1970, 1971, 1972, 1973) else pd.NaT)
        print('crear csv')
        dbs_joan_1.to_csv(u.tempFolder + "visualitzacio_proves.csv")
        print('export:', datetime.now()-ts)

    def copia_seguretat(self):
        """ Creates backup copy """
        ts = datetime.now()
        print('còpia seguretat')
        # crear còpia de seguretat a planificat_copseg
        sql = """select weekday(current_date) from nodrizas.dextraccio"""
        weekday = 0
        for day, in u.getAll(sql, 'nodrizas'):
            weekday = day
        if weekday == 4:
            u.execute('drop table PLANIFICAT_COPSEG_dijous', 'pdp')
            u.execute('create table PLANIFICAT_COPSEG_dijous as select * from PLANIFICAT where estat <>1', 'pdp')
        else:
            u.execute('drop table PLANIFICAT_COPSEG_diumenge', 'pdp')
            u.execute('create table PLANIFICAT_COPSEG_diumenge as select * from PLANIFICAT where estat <>1', 'pdp')
        print('copia seg:', datetime.now()-ts)

    def afegir_nous_pacients(self):
        """" Adding new planifi.cat patients to DDBB """
        ts = datetime.now()
        self.presents = set()
        sql = """select hash, c_sector
                    from planificat"""
        for hash, sector in u.getAll(sql, 'pdp'):
            self.presents.add((hash, sector))
        print('tinc els presents i son', len(self.presents))

        upload = []
        upload_audit = []
        for e in self.cronics_set:
            if e not in self.presents:
                info = self.info_up_uba[e[0]]
                up = info[0]
                uba = info[1]
                now = datetime.now()
                dt_string = now.strftime("%Y/%m/%d %H:%M:%S")
                upload.append(tuple([None, None, e[0], e[1], '1', 'ALGORITME', dt_string] + 81 * [None]))
                upload_audit.append(tuple([up, uba, e[0], e[1], '1', 'ALGORITME', dt_string] + 81 * [None]))
        print('anem a penjar')
        if len(upload) != 0:
            print(len(upload[0]))
            u.listToTable(upload, 'planificat', 'pdp')
            u.listToTable(upload_audit, 'planificat_audit', 'pdp')
        print('nous pacients:', datetime.now()-ts)

    def ressucitar(self):
        """ Compute who we need to wake up """
        ts = datetime.now()
        print('a resucitar')
        sql = """SELECT * FROM(
        select hash,
                    case
                        when (TRUNC(SYSDATE) - trunc(to_date(estat_data, 'yyyy/mm/dd hh24:mi:ss'))) >= 400 then 1
                        when 
                            (estat = 10 and ((TRUNC(SYSDATE) - trunc(to_date(fadm_visita_1, 'dd/mm/yyyy'))) >= 1 
                                        OR fadm_visita_1 IS NULL)) AND
                            (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_2, 'dd/mm/yyyy'))) >= 1 
                                        OR fadm_visita_2 IS NULL) AND
                            (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_3, 'dd/mm/yyyy'))) >= 1 
                                        OR fadm_visita_3 IS NULL) AND
                            (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_1post, 'dd/mm/yyyy'))) >= 1 
                                        OR fadm_visita_1post IS NULL) AND
                            (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_2post, 'dd/mm/yyyy'))) >= 1 
                                        OR fadm_visita_2post IS NULL) AND
                            (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_3post, 'dd/mm/yyyy'))) >= 1 
                                    OR fadm_visita_3post IS NULL) AND
                            (estat = 10 and (trunc(sysdate) - trunc(fadm_data_recomanada)) >= 1) then 1
                        else 0 
                    end resucitar, c_sector
                    from planificat)
                    WHERE resucitar = 1"""
        a_resucitar = []
        upload = []
        upload_audit = []
        now = datetime.now()
        dt_string = now.strftime("%Y/%m/%d %H:%M:%S")

        for hash, resucitar, sector in u.getAll(sql, 'pdp'):
            if resucitar == 1 and hash in self.info_up_uba: 
                info = self.info_up_uba[hash]
                up = info[0]
                uba = info[1]
                upload.append(tuple([None, None, hash, sector, '1', 'ALGORITME', dt_string] + 81 * [None]))
                upload.append(tuple([up, uba, hash, sector, '1', 'ALGORITME', dt_string] + 81 * [None]))
                a_resucitar.append(hash)

        print('gent que resucitem', len(a_resucitar))
        if len(a_resucitar) > 1:
            sql = """delete from planificat where hash in {}""".format(tuple(a_resucitar))
            print(sql)
            u.execute(sql, 'pdp')
        elif len(a_resucitar) == 1:
            sql = """delete from planificat where hash = '{}'""".format(a_resucitar[0])
            print(sql)
            u.execute(sql, 'pdp')
        if len(upload) > 0:
            print(len(upload), 'els q afegim')
            u.listToTable(upload, 'planificat', 'pdp')
            u.listToTable(upload_audit, 'planificat_audit', 'pdp')
        print('ressucitats:', datetime.now()-ts)

        ts = datetime.now()
        # eliminem els duplicats causats per 
        max_dates_duplicates = {}
        sql = """SELECT hash, MAX(to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS')) data
                FROM planificat
                where estat = 1
                GROUP BY hash
                HAVING count(1) > 1"""
        for hash, data in u.getAll(sql, 'pdp'):
            max_dates_duplicates[hash] = data

        a_eliminar = {}
        sql = """SELECT hash, to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS'), estat_data 
                    FROM planificat WHERE hash in(
                    SELECT hash
                    FROM planificat
                    WHERE estat =1 
                    GROUP BY hash
                    HAVING count(1) > 1)"""    
        for hash, data, str_data in u.getAll(sql, 'pdp'):
            if hash in max_dates_duplicates:
                if data < max_dates_duplicates[hash]:
                    a_eliminar[hash] = str_data

        conn = u.connect('pdp')
        c=conn.cursor()
        for hash, str_data in a_eliminar.items():
            sql = "delete from planificat where hash = '{}' and estat_data = '{}'".format(hash, str_data)
            c.execute(sql)
        conn.commit()
        conn.close()
        print('delete possibles errors:', datetime.now()-ts)

def printing(x):
    if x is None: print(type(x), x)
    return x



if __name__ == "__main__":
    Planificat()