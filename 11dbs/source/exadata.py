import hashlib as h
import itertools as i

import sisaptools as u
import sisapUtils as u2


def upload_exadata():
    """."""
    columns = []
    with u.Database("redics", "data") as redics:
        for column in redics.get_table_columns("dbs"):
            columns.append(redics.get_column_information(column, "dbs", desti="ora")["create"])  # noqa
    with u.Database("exadata", "data", retry=5) as exadata:
        exadata.create_table("dbs", columns, remove=True)
        users = ("DWBO", "DWAQUAS", "DWETL", "DWSISAP_ROL")
        exadata.set_grants("select", "dbs", users, inheritance=False)
    digits = [format(digit, "x").upper() for digit in range(16)]
    for digit1 in digits:
        sql = "select usua_cip_cod, usua_cip from pdptb101 \
               where usua_cip_cod like '{}{{}}%'".format(digit1)
        jobs = [(sql.format("".join(digit2)), "pdp")
                for digit2 in digits]
        conversor = {}
        for chunk in u2.multiprocess(u2.get_data, jobs):
            for hash, cip in chunk:
                conversor[hash] = h.sha1(cip).hexdigest().upper()
        sql = "select * from dbs where c_cip like '{}{{}}%'".format(digit1)
        jobs = [(sql.format("".join(digit2)), "redics")
                for digit2 in digits]
        upload = []
        for chunk in u2.multiprocess(u2.get_data, jobs):
            for row in chunk:
                if row[0] in conversor:
                    upload.append((conversor[row[0]],) + row[1:])
            if upload:
                u.Database("exadata", "data", retry=5).list_to_table(upload, "dbs", chunk=10**5)  # noqa
                upload = []
    u.Database("exadata", "data", retry=5).execute("create index dbs_hash on dbs(c_cip)")  # noqa


def upload_cataleg():
    """."""
    sql = "select agrupador, literal_curt from dbscatdesc"
    dades = list(u.Database("redics", "data").get_all(sql))
    cols = ("codi varchar2(32)", "descripcio varchar2(255)")
    with u.Database("exadata", "data", retry=5) as exadata:
        exadata.create_table("dbs_dim", cols, remove=True)
        exadata.list_to_table(dades, "dbs_dim")
        exadata.set_grants("select", "dbs_dim", "DWSISAP_ROL", inheritance=False)  # noqa


if __name__ == "__main__" and u2.IS_MENSUAL:
    upload_exadata()
    upload_cataleg()
