# coding: utf8

"""
Procés que afegeix les patologies cròniques que van escollir els pediatres
Necessita que la taula existeixi. Si s'ha de tirar tot de nou hauria de tirar-se després del procés
d obtenció de dades
"""

import urllib as w
import os

import sisapUtils as u
from collections import defaultdict,Counter

table = 'SISAP_MODEL_PEDIA_uba_pacient'
tb = 'prova'
db = 'test'



class ncroniques(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_hashos()
        self.get_codis()
        self.get_hash()
        self.get_problemes()
        self.get_dades()
        self.export_dades()
        
    def get_hashos(self):
        """Aconseguim els hashos per creuar amb taula ppal"""
        ok = 1
    
    def get_codis(self):
        """escollim els codis cim10"""
        file = u.baseFolder + ["64ModelPedia", "mcronCIM10.txt"]
        file = "\\".join(file)
        crit = []
        for row, in u.readCSV(file, sep="@"):
            crit.append(row)
        self.codis = tuple(crit) 

    def get_hash(self):
        """Conversió ID-HASH."""
        self.hashos = {}
        sql = ("select id_cip_sec, hash_d from u11", "import")
        for id, hash in u.getAll(*sql):
            self.hashos[id] = hash
            
    def get_problemes(self):
        """Agafem els problemes segons el llistats que ens van fer arribar"""
        self.pac = {}
        self.npac = Counter()
        sql = ("select id_cip_sec from problemes,  nodrizas.dextraccio_calculs \
                where  pr_cod_ps in {} \
                   and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext) and (pr_dba = 0 or pr_dba > data_ext)".format(self.codis),
               "import")
        for id, in u.getAll(*sql):
            if id in self.hashos:
                hash = self.hashos[id]
                self.pac[hash] = True
                self.npac[hash] += 1
            
    def get_dades(self):
        """."""
        self.upload = []
        sql = "select * from {}".format(table)
        for row in u.getAll(sql, db):
            pacCron = 1 if row[0] in self.pac else 0
            nCron = self.npac[row[0]] if row[0] in self.npac else 0
            self.upload.append([row[0], row[1], row[2], row[3], row[4],
                  row[5], row[6], row[7], row[8], row[9], row[10], row[11],
                  row[12], row[13], row[14], row[15], row[16], row[17],
                  row[18], row[19], row[20], row[21], row[22], row[23], row[24], pacCron, nCron])
            

    def export_dades(self):
        """Exporta les dades de nivell pacient."""
        columns = ('id varchar(40)', 'edat int', 'sexe varchar(1)',
                   'gma_codi varchar(3)', 'gma_complexitat double',
                   'gma_num_croniques int', 'medea_seccio double',
                   'institucionalitzat int', 'immigracio_baixa_renta int',
                   'atdom int', 'patologia_social int', 'patologia_mental int','pensionista int', 'nouvinguts int', 'nouvinguts_imm int', 'up varchar(5)', 'uba varchar(5)',
                   'codi_postal varchar(5)', 'visites_centre_normals int',
                   'visites_capes_9d int', 'visites_capes_9t int',
                    'visites_capes_9e int',
                   'visites_sequencials_domicili int', 'visites_virtuals int', 'visites_centre_RNSA int', 'pat_cronica int', 'num_pat_cronica int')
        columns_str = "({})".format(', '.join(columns))
        u.createTable(table, columns_str, db, rm=True)
        u.listToTable(self.upload, table, db)
  
Calcula = False  
sql = "select pat_cronica from {} limit 1".format(table)
try:
    for c1, in u.getAll(sql, db):
        print c1
except:
    calcula = True
    


if __name__ == '__main__':
    if Calcula:
        print 'calcula'
        ncroniques()
    else:
        print "Ja tenim aquesta variable actualitzada"
