# coding: utf8

"""
Visites per mes
"""

import urllib as w
import os

import sisapUtils as u
from collections import defaultdict,Counter


class Frequentacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_numcol()
        self.get_nensa()
        self.get_poblacio()
        self.get_hash()
        self.get_visites()
        self.get_virtuals()
        self.export_dades_pacient()


    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_numcol(self):
        """Agafem numcols de mf"""
    
        self.numcols = {}
        self.numsllocs = {}
        
        sql = "select codi_sector, lloc_codi_lloc_de_treball, lloc_numcol, lloc_especialit_codi_especiali from cat_pritb025 where lloc_especialit_codi_especiali in ('10999', '10117', '10888')"
        for sector, lloc, numcol, espe in u.getAll(sql, "import"):
            self.numsllocs[lloc, sector] = numcol
        
        sql = " select codi_sector, uab_codi_up,uab_codi_uab,uab_lloc_de_tr_codi_lloc_de_tr from import.cat_vistb039 where uab_servei_codi_servei in ('MG', 'PED')"
        for sector, up, uba, lloctr in u.getAll(sql, "import"):
            if up in self.centres:
                if (lloctr, sector) in self.numsllocs:
                    numcol = self.numsllocs[lloctr, sector]
                    self.numcols[sector, numcol] = {'up': up, 'uba': uba}
    
    
    def get_nensa(self):
        """Busquem els registres de RNSA"""
        self.rnsa = {}
        for table in u.getSubTables('variables'):
            dat, = u.getOne('select year(vu_dat_act) from {} limit 1'.format(table), 'import')
            if dat == 2017:
                print table
                sql = "select id_cip_sec, vu_dat_act, vu_up from {} where vu_cod_vs='RNSA'".format(table)
                for id, dat, up in u.getAll(sql, "import"):
                    self.rnsa[(id, dat, up)] = True
    
    def get_poblacio(self):
        """
        Obté la població d'estudi:
           - actius al tancament
           - Menors de 15 anys
        """
        self.dades = {}
        pob2017 = {}
        sql = "select id_cip_sec, up, uab       from assignadahistorica  where dataany = 2017"
        for id, up, uba in u.getAll(sql, "import"):
            pob2017[id] = {'up': up, 'uba': uba}
        institucionalitzats = set([id for id, in u.getAll("select id_cip_sec \
                                    from institucionalitzats a inner join cat_ppftb011_def b \
                                    on a.ug_codi_grup=b.gu_codi_grup and a.codi_sector=b.codi_sector", "import")])
        sql = ("select id_cip_sec, codi_sector, usua_sexe,usua_nacionalitat, cart_tipus, usua_uab_servei_centre, usua_uab_servei_centre_classe, \
              (year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')), year(usua_data_alta) \
                from assignada, nodrizas.dextraccio_calculs \
                where (year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')) < 15", "import")
        for id, sec, sex, nac, cob, cod, cla, edt, alta in u.getAll(*sql):
            if id in pob2017:
                up = pob2017[id]['up']
                uba = pob2017[id]['uba']
                if up in self.centres:
                    insti = 1 if id in institucionalitzats else 0
                    if edt >= 0 and insti == 0:
                        self.dades[id] = {'hash': None, 'sec': sec, 'up': up, 'uba': uba}

    def get_hash(self):
        """Conversió ID-HASH."""
        sql = ("select id_cip_sec, hash_d from eqa_u11", "nodrizas")
        for id, hash in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['hash'] = hash


    def get_visites(self):
        """
        Agafem visites de qualsevol EAP de l'ICS i fetes en una
        agenda del servei MG o ped.
        Si l'agenda és per capes, separem les visites segons el seu tipus.
        En el cas de les agendes seqüencials, classifiquem les visites
        segons el lloc de realització.
        """
        visitesCentre = {}
        self.dades_visites = Counter()
        for table in u.getSubTables('visites'):
            dat, = u.getOne('select year(visi_data_visita) from {} limit 1'.format(table), 'import')
            if dat == 2017:
                print table
                sql = "select id_cip_sec, visi_up, visi_tipus_visita, visi_lloc_visita, visi_data_visita, date_format(visi_data_visita,'%Y%m')\
                        from {} \
                        where visi_situacio_visita = 'R' and visi_servei_codi_servei in ('MG','PED')".format(table)
                for id, up, tipus, lloc, data, periode in u.getAll(sql, "import"):
                    if id in self.dades and up in self.centres:
                        hash = self.dades[id]['hash']
                        up = self.dades[id]['up']
                        uba = self.dades[id]['uba']
                        if tipus in ('9D', '9T', '9E'):
                            self.dades_visites[hash,  up, uba, periode, tipus] += 1
                        elif lloc in ('D'):
                             self.dades_visites[hash,  up, uba, periode, lloc] += 1
                        elif tipus in ('9C', '9R') or lloc in ('C'):
                            if (id, data, up) in self.rnsa and (id, data, up) not in visitesCentre:
                                self.dades_visites[hash,  up, uba,  periode, 'RNSA'] += 1
                                visitesCentre[(id, data, up)] = True
                            else:
                                self.dades_visites[hash, up, uba, periode, 'centre'] += 1
                            
    def get_virtuals(self):
        """
        Agafem les visites virtuals de la taula original de vvirtuals tenint en compte 
        """
        sql = ("select codi_sector, id_cip_sec, vvi_num_col, date_format(vvi_data_alta, '%Y%m')\
                from vvirtuals \
                where vvi_situacio='R' and \
                      vvi_data_alta between '20170101' and '20171231'",
               "import")
        for sector, id, numcol, periode in u.getAll(*sql):
            if (sector, numcol) in self.numcols:
                up, uba = self.numcols[sector, numcol]['up'], self.numcols[sector, numcol]['uba']
                if id in self.dades:
                    up2, uba2 = self.dades[id]['up'], self.dades[id]['uba']
                    if up == up2 and uba == uba2:
                        self.dades_visites[hash, up, uba, periode, 'V'] += 1

    @staticmethod
    def export_dades(tb, columns, dades):
        """Mètode per crear i omplir taules."""
        table = 'SISAP_MODEL_PEDIA_uba_{}'.format(tb)
        columns_str = "({})".format(', '.join(columns))
        db = "test"
        u.createTable(table, columns_str, db, rm=True)
        #u.grantSelect(table, ('PREDUMMP', 'PREDUPRP',
        #                      'PREDUECR'), db)
        u.listToTable(dades, table, db)

    def export_dades_pacient(self):
        """Exporta les dades de nivell pacient."""
        dades = []
        for (hash, up, uba, periode, tipus), d in self.dades_visites.items():
            dades.append([hash, up, uba, periode, tipus, d])
        columns = ('id varchar(40)', 'up varchar(5)', 'uba varchar(5)', 'periode varchar(6)','tipus_visita varchar(10)',
                   'recomptes int')
        Frequentacio.export_dades("MESOS", columns, dades)

if __name__ == '__main__':
    Frequentacio()