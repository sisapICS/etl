---
title: "Model d'assignació de pediatria"
output: 
  flexdashboard::flex_dashboard:
    vertical_layout: scroll
    horizontal_layout: scroll
    orientation: rows
---

<style>

body {
    font-family: "Source Sans Pro",Calibri,Candara,Arial,sans-serif;
    font-size: 15px;
    line-height: 1.42857143;
    color: #333333;
    background-color: #ffffff; 
}
.navbar {
  background-color:#0061a9;
  border-color:#0061a9;
}
.navbar-inverse .navbar-nav>.active>a, .navbar-inverse .navbar-nav>.active>a:hover, .navbar-inverse .navbar-nav>.active>a:focus {
    color: #ffffff;
    background-color: #00243E;
}
.chart-wrapper, .nav-tabs-custom, .sbframe-commentary {
    border: 0px;
    <!-- border-right: 1px solid #e2e2e2; -->
}
.chart-wrapper1, .nav-tabs-custom, .sbframe-commentary {
    border: 0px;
}
.chart-wrapper2, .nav-tabs-custom, .sbframe-commentary {
    border: 0px;
    padding-top: 50px;
}
.chart-wrapper .chart-notes {
    background: #ffffff;
    border-top: 0px ;
    color: #696969;
    font-size: 15px;
    padding: 8px 10px 5px;
}
.chart-shim {
    position: relative;
}
.image-container {
    position: relative;
}

blockquote {
    padding: 8px 10px 5px;
    margin: 0 0 21px;
    font-size: 15px;
    background: #ffffff;
    border-left: 0px ;
}

caption {
      color: #696969;
}

tfoot {
    color: #696969;
}

.textRow {
  background-color: #0061a9;
  font-weight: bold;
  font-size: 30px;
  color: white
}

.textRow1 {
  background-color: #0061a9;
  font-weight: bold;
  font-size: 20px;
  color: white
}
.textRow2 {
  background-color: #ffffff;
  color: #0061a9;
  font-weight: bold;
  font-size: 18px;
  padding-left: 350px;
}
.value-box1 .value {
    color: #0061a9;
    background: #ffffff;
}
.value-box1 .caption {
    color: #0061a9;
    background: #ffffff;
}
.value-box1 > .inner {
    BACKGROUND: WHITE;
}

<!-- .nav-tabs-custom > .nav-tabs > li.active > a, .nav-tabs-custom > .nav-tabs > li.active:hover > a { -->
<!--     background-color: #fff; -->
<!--     color: #666; -->
<!--     font-size: 20px -->
<!-- } -->
.nav-tabs-custom > .nav-tabs > li > a, .nav-tabs-custom > .nav-tabs > li > a:active {
    color: #666;
    font-weight: 300;
    font-size: 20px;
    border-radius: 0;
    padding: 3px 10px 5px;
    text-transform: none;
}
    
</style>

```{r message=FALSE, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning = FALSE)

library(data.table)
library(ggplot2)
library(hrbrthemes)
library(knitr)
library(car)
library(corrplot)
library(kableExtra)
library(gridExtra)
library(flexdashboard)
library(shiny)
```

```{r}
load("Dades/DadesModelPedia_uba.RData")
load("Dades/UPModelPedia.RData")
load("Dades/15153010_Model_pedia_FINAL.RData")
load("Dades/DadesPrediccio_FINAL.RData")
```

```{r estimacio_model, message=FALSE, warning=FALSE}
up[, POBASS_f := cut(POBASS, breaks = c(-1, 1000, 3500, 6000, 22000), include.lowest = F, labels = c("Menys de 1k", "De 1K a 3.5K", "De 3.5K a 6k", "Més de 6K"))]

maxgma <- max(up$gma_mean)
mingma <- min(up$gma_mean)
dm0_gma <- up[gma_mean < maxgma & gma_mean > mingma]
dm1_gma <- dm0_gma[! medea_f %in% c("0R", "1R")]
```

Resultats
=====================================

Al següent taulell es poden consultar els resultats principals del model d'assignació de pediatria.

  * Un resum del projecte.
  * Els valors esperats per:
    1. El temps mig esperat per pacient pediàtric i any global de l'ICS.
    2. El nombre de pediatres esperats per assumir la demanda pediàtrica de l'ICS.
    3. La ràtio de pacients esperats per pediatre global de l'ICS.
  * Els valors estimats pels coeficients associtats a les variables de demanda que ajusten el model

#### **Resum del projecte**

L'any 2017 es va construir un model d'assignació de metges de família. Seguint la mateixa metodologia, l'any 2018 s'ha replicat el model d'adults per assignar pediatres als equips d'atenció primària (EAP) de l'Institut Català de la Salut (ICS) en funció de la característica de la demanda.

Per a estimar el model s'han tingut en compte `r nrow(dm1_gma)` EAP de l'ICS i `r format(nrow(dades[up %in% dm1_gma$up]), big.mark = ".")` pacients pediàtrics assignats a algun dels EAP. **S'han exclòs els EAP rurals i semi-rurals, 3 EAP amb UBAs que tenen un número de visites de revisió de nen sa (RNSA) molt anòmal i 2 EAP amb valors extrems per la mitjana del GMA**.

S'han tingut en compte visites de centre, de revisió de nen sa , domiciliàries i no presencials.

Per cada pacient s'ha obtingut el temps total de visita sumant el nombre de visites realitzades durant el 2017 ponderades segons la tipologia de visita. De manera que una visita de centre i una visita de RNSA dura 15 minuts, una visita domiciliària dura 30 minuts i una visita no presencial dura 10 minuts.

Per cada EAP s'ha calculat el temps mig de visita fent la mitja de tots els seus pacients assignats. I aquesta ha estat la variable resposta que s'ha modelat.

Com a variables d'ajust s'han utilitzat variables relacionades amb la demanda dels EAP: percentatge de nens/es menors de 3 anys, percentatge de nenes, complexitat mitjana, percentatge de nenes/es amb patologia social i/o mental, diferents variables socioeconòmiques (AQUAS, percentatge d'immigració de baixa renta, percentatge de pensionistes, nouvinguts immigrants) i nombre de pacients pediàtrics assignats.

S'ha utilitzat un model de regressió linial per ajustar el temps mig de visita en funció de la demanda, considerant com a variable explicativa del model aquella amb un coeficient de regressió significatiu amb un p-valor inferior a 0,05.

Finalment, les variables que s'han utilitzat per ajustar el model han estat (en ordre d'importància): índex de privació socioeconòmica AQUAS, GMA mitjana del EAP, percentatge de nens/es amb patologia mental, tamany del centre segons població pediàtrica assignada i percentatge de nens/es menors de 3 anys. La bondat d'ajust del model ha estat del `r paste0(format(summary(m1)$r.squared*100, digits = 4), "%")`.

Per a predir el temps mig de visita per pacient i any global de l'ICS, s'han aplicat els coeficients estimats del model als `r nrow(tt)` EAP pediàtrics de l'ICS.

#### **A continuació es mostren els coeficients estimats a partir dels quals es calcula en nombre de pediatres que necessita cada EAP.**

Row1
-------------------------------------

```{r resultats}
results_m1 <- data.table(Variables = names(m1$coefficients), Coeficient = m1$coefficients)
results_m1 <- cbind(results_m1, confint(m1))
colnames(results_m1)[c(3, 4)] <- c("l_inf", "l_sup")
results_m1[, Variables := c("(Intercept)",
                            "% de nens/es amb  menors de 3 anys",
                            "% de nens/es amb patologia mental",
                            "AQUAS",
                            "Centres mitjans (de 3.5K fins a 6K pacients assignats) vs Centres petits (menys de 3.5K pacients assignats)",
                            "Centres grans (més de 6K pacients assignats) vs Centres petits (menys de 3.5K pacients assignats)",
                            "Complexitat GMA"
)]
require(caret)
score_m1 <- data.table(varImp(m1), keep.rownames = T)[order(-Overall)]
score_m1[, Variables := c("AQUAS",
                          "Complexitat GMA",
                          "% de nens/es amb patologia mental",
                          "Centres mitjans (de 3.5K fins a 6K pacients assignats) vs Centres petits (menys de 3.5K pacients assignats)",
                          "Centres grans (més de 6K pacients assignats) vs Centres petits (menys de 3.5K pacients assignats)",
                           "% de nens/es amb  menors de 3 anys")]

results_m1[, vars_f := factor(Variables, levels = c(score_m1$Variables))]

g <- ggplot(results_m1[!is.na(vars_f)], aes(x = vars_f, y = Coeficient,  ymin = l_inf, ymax = l_sup)) +
  geom_pointrange(color = "#0061a9") +
  geom_hline(yintercept = 0, linetype = 2)  +
  scale_x_discrete(limits = c("% de nens/es amb  menors de 3 anys", "Centres grans (més de 6K pacients assignats) vs Centres petits (menys de 3.5K pacients assignats)", "Centres mitjans (de 3.5K fins a 6K pacients assignats) vs Centres petits (menys de 3.5K pacients assignats)", "% de nens/es amb patologia mental", "Complexitat GMA", "AQUAS"), labels = c("% de nens/es amb  menors de 3 anys", "Centres grans vs Centres petits", "Centres mitjans vs Centres petits ", "% de nens/es amb patologia mental", "Complexitat GMA", "AQUAS")) +
  coord_flip() +
  xlab("") + ylab("Valor dels coeficients") +
    theme_ipsum(grid_col = "transparent", axis_title_size = 10) +
    theme(
      plot.title = element_text(hjust = 0.5, size = 10, colour = "grey35", margin=margin(0, 0, 10, 0)),
      plot.caption = element_text(size = 8, colour = "grey35"),
      legend.position = "bottom",
      axis.title = element_text(colour = "grey35"),
      axis.text.x = element_text(colour = "grey35"),
      axis.text.y = element_text(vjust = 0.5, hjust = 1, size = 10),
      axis.line = element_line(colour = "grey75"),
      legend.text = element_text(colour = "grey35"),
      strip.text = element_text(colour = "grey35")) +
    labs(title = "Valors dels coeficients associats a les variables de demanda\n Estimació puntual i interval de confiança",
       caption = "FONT: SISAP")
```

### Column1 {.no-title data-width=700}

```{r}
t <- results_m1[order(vars_f), c(1:4)]
names(t) <- c("", "Coeficients",
              paste0("Límit Inferior", footnote_marker_alphabet(1)),
              paste0("Límit Superior", footnote_marker_alphabet(1)))

t %>%
  kable(digits = 3, row.names = F, escape = F, caption = "Valors dels coeficients associats a les variables de demanda. Estimació puntual i interval de confiança",
        align = c("l", "c", "c", "c", "c", "c"), format.args = list(decimal.mark = ',', big.mark = "."))%>%
  kable_styling(bootstrap_options = c("hover", "condensed", "responsive", "stripped"), position = "center", full_width = T) %>%
  row_spec(0, bold = T, align = "c", background = "#0061a9", color = "white") %>%
  column_spec(1, bold = F, background = "#E7F0F7") %>%
  column_spec(2, border_right = "2px solid #E7F0F7") %>%
  footnote(alphabet = c("Límits per l'interval de confiança del 95%"))
```

>La taula mostra, per a cada una de les variables associades a la demanda dels EAP, el temps de més o de menys que cal sumar o restar al temps mig per pacient i any global (ICS) per cada unitat de canvi. El valor dels coeficients correspon a l'estimació puntual d'aquest paràmetre, mentre que els límits inferior i superior corresponen a l'estimació de l'interval de confiança amb un nivell de significància del 95%. <br>Qual el signe dels coeficients i de l'interval de confiança son positiu, vol dir que a més valor de la característica en qüestió fa augmentar el temps mig de visita per pacient i any. En canvi, quan el signe dels coeficients és negatiu, vol dir que la variable fa disminuir el temps mig de visita per pacient i any.<br>L'ordre de disposició de les variables a la taula ve determinat per la magnitud de part explicada que aporta cada variable d'ajust al model. <br>L'índex socioeconòmic AQUAS és la variable que més explica la diferència en el temps mig de visita per pacient i any. Així un EAP amb un valor de 100 d'aquest índex (un EAP amb la privació més alta) té de mitja 19,4 minuts més de visita per pacient i any que un EAP amb un valor de 0 de l'índex AQUAS (un EAP amn la privació més baixa).

### Column2 {.no-title data-width=700}

```{r fig.width=8, results="asis"}
g
```

> Al gràfic s'hi poden observar els coeficients amb el seu interval de confiança per a totes les variables associades a la demanda dels EAP que s'han ajustat en el model. Qual els segments es troben a la dreta de la línia disconinua vol dir que a més valor de la característica en qüestió més temps mig de visita per pacient i any. En canvi, quan el segment es troba a l'esquerra de la línia discontunua, vol dir que la variable fa disminuir el temps mig de visita per pacient i any

Row2  {data-height=50 .textRow}
-------------------------------------

R^2^ del model = `r paste0(format(summary(m1)$r.squared*100, nsmall = 2, digits = 3, big.mark = ".", decimal.mark = ","), "%")`

Row3 {data-height=10}
-------------------------------------

```{r}
tt[, paste0(c("fit_m1", "lwr_m1", "upr_m1"), "_total") := lapply(.SD, function(x) x*POBASS), .SDcols = c("fit_m1", "lwr_m1", "upr_m1")]
t <- tt[, lapply(.SD, function(x) sum(x)), .SDcols = c("fit_m1_total", "lwr_m1_total", "upr_m1_total")]/sum(tt$POBASS)

# valueBox(paste0(format(t$fit_m1_total, nsmall = 1, digits = 3, big.mark = ".", decimal.mark = ","),
#                 " [",
#                 format(t$lwr_m1_total, nsmall = 1, digits = 3, big.mark = ".", decimal.mark = ","),
#                 "-",
#                 format(t$upr_m1_total, nsmall = 1, digits = 3, big.mark = ".", decimal.mark = ","),
#                 "]")
#          , color = "#0061a9")
```

Variable resposta
=====================================

Aquest taulell dona informació referent al temps de visita per pacient i any. 

  * Definició de temps de visita.
  * Exclusions d'unitats d'anàlisi (EAP) per motius de distribució de la variable resposta.
  
Row1  {data-height=50 .textRow}
-------------------------------------

Definició

Row2
-------------------------------------

### Column1 {.no-title data-width=350}

```{r}
t <- data.table(rbind(
  c("Visites de centre", "15 minuts"),
  c("Revisions del nen sa (RNSA)", "15 minuts"),
  c("Visites a domicili", "30 minuts"),
  c("Visites no presencials", "10 minuts")
))
colnames(t) <- c("Tipus de visita", "Temps mig de durada")

t %>%
  kable(digits = 3, row.names = F, escape = F, caption = "Temps mig de durada associat als diferents tipus de visita", 
        align = c("l", "c"), format.args = list(decimal.mark = ',', big.mark = "."))%>%
  kable_styling(bootstrap_options = c("hover", "condensed", "responsive", "stripped"), position = "center", full_width = T) %>%
  row_spec(0, bold = T, align = "c", background = "#0061a9", color = "white") %>%
  column_spec(1, bold = F, background = "#E7F0F7") 
```

> S'han considerat 4 tipus de visites diferents.<br> A cada tipus de visita se li ha assignat un temps mig de durada.

### Column2 {.no-title data-width=550}

```{r}
up[, visites_domicili_sum := visites_sequencials_domicili_sum + visites_capes_9d_sum]
up[, visites_no_presencial_sum := visites_virtuals_sum + visites_capes_9t_sum]
t <- data.table(rbind(
  nvisites = up[up %in% dm1_gma$up, lapply(.SD, function(x) sum(x)), .SDcols = c("visites_centre_normals_sum", "visites_centre_RNSA_sum", "visites_domicili_sum", "visites_no_presencial_sum")],
  tvisites = up[up %in% dm1_gma$up, lapply(.SD, function(x) sum(x)), .SDcols = c("visites_centre_normals_sum", "visites_centre_RNSA_sum", "visites_domicili_sum", "visites_no_presencial_sum")]*c(15, 15, 30, 10)
))
t[, total := visites_centre_normals_sum + visites_centre_RNSA_sum + visites_domicili_sum + visites_no_presencial_sum]
t_mig_ics <- t[2, total]/sum(dm1_gma$POBASS)
t[, t_mig := c(NA, t_mig_ics)]
t[, unitat := c("Número de visites", "Temps total de visita (minuts")]
colnames(t) <- c("Visites de centre", "Revisions del nen sa (RNSA)", "Visites a domicili", "Visites no presencials", "Totes les visites", paste0("Mitjana per pacient i any", footnote_marker_alphabet(1)), "")
```

```{r}
options(knitr.kable.NA = '')
t[, c(7, 1:6)] %>%
  kable(digits = 3, row.names = F, escape = F, caption = "Nombre de visites i temps segons tipus de visita", 
        align = c("l", "c", "c", "c", "c", "c", "c"), format.args = list(decimal.mark = ',', big.mark = ".", digits = 4))%>%
  kable_styling(bootstrap_options = c("hover", "condensed", "responsive", "stripped"), position = "center", full_width = T) %>%
  row_spec(0, bold = T, align = "c", background = "#0061a9", color = "white") %>%
  column_spec(1, bold = F, background = "#E7F0F7") %>%
  column_spec(5, border_right = "2px solid #E7F0F7") %>%
  footnote(alphabet = paste0("Pacients pediàtrics assignats als EAP estudiats: ", format(sum(dm1_gma$POBASS), big.mark = ".", decimal.mark =   ",")))
```

> La variable resposta és el temps mig de visita per pacient i any.<br> 
La taula mostra el nombre de visites i el temps total per cadascun dels 4 tipus.<br> 
El temps total de visita correspon a sumar el nombre de visites per el temps mig de durada especificat a la taula de l'esquerra.<br>
La cinquena columna de la taula mostra el sumatori del nombre de visites i el temps de totes les visites. I la última columna mostra el temps mig per pacient i any, resultat de dividir el temps total de visita per totes les visites pel total de pacients assignats.

### Column3 {.no-title data-width=550}

```{r fig.height=4}
ggplot(dm1_gma, aes(x = t_tot_mean)) +
  geom_histogram(fill = "#0061a9", color = "white") +
  xlab("Temps mig de visita per pacient i any") + ylab("Nombre d'EAP") +
   theme_ipsum(grid_col = "transparent", axis_title_size = 10) +
    theme(
      plot.title = element_text(hjust = 0.5, size = 10, colour = "grey35", margin=margin(0, 0, 10, 0)),
      plot.caption = element_text(size = 8, colour = "grey35"),
      legend.position = "bottom",
      axis.title = element_text(colour = "grey35"),
      axis.text.x = element_text(colour = "grey35"),
      axis.text.y = element_text(vjust = 0.5, hjust = 1, size = 10),
      axis.line = element_line(colour = "grey75"),
      legend.text = element_text(colour = "grey35"),
      strip.text = element_text(colour = "grey35")) +
    labs(title = "Temps mig de visita per pacient i any\ per cada EAP",
       caption = "FONT: SISAP")
                   
```

> Aques histograma mostra la distribució dels temps mig de visita per pacient i any de cada EAP. Cada barra del gràfic representa el nombre d’EAP que tenen un mateix temps de visita.

Row3  {data-height=50 .textRow}
-------------------------------------

Exclusions

Row4
-------------------------------------

```{r}
# Visites totals
dades[, visites_tot := visites_centre_normals + visites_capes_9d + visites_capes_9t + visites_capes_9e + visites_sequencials_domicili + visites_centre_RNSA + visites_virtuals]
# dades <- dades[visites_tot <40]
# Agregació per UBA
dades[, ":=" (visites_tot_sum = sum(visites_tot, na.rm = T),
              visites_centre_RNSA_sum = sum(visites_centre_RNSA, na.rm = T)
              ), by = c("up", "uba")]
up_uba <- unique(dades[, c("up", "uba", "descripcio_up", "visites_tot_sum", "visites_centre_RNSA_sum")])
up_uba <- up_uba[visites_tot_sum != 0]

# Percentatge de RNSA
up_uba[, RNSA_p := visites_centre_RNSA_sum/(visites_tot_sum)*100]
a <- boxplot(up_uba$RNSA_p, main = "Distribució del percentatge de RNSA de les UBAs", plot = F)
up_uba_depurada <- up_uba[RNSA_p > a$stats[1, ] & RNSA_p < a$stats[5,]]

up_orig <- up_uba[, lapply(.SD, function(x) sum(x)), .SDcols = c("visites_tot_sum", "visites_centre_RNSA_sum"), by = c("up")]
up_orig[, RNSA_p := visites_centre_RNSA_sum/visites_tot_sum]
up[, RNSA_p := visites_centre_RNSA_sum/visites_tot_sum]

up_incloses <- up$up
```

### Column1 {.no-title}

```{r fig.align="right", fig.height=4}
ggplot(up_uba, aes(x = "", y = RNSA_p)) +
  geom_boxplot(fill = "#0061a9") +
  xlab("") + ylab("Percentatge de RNSA") +
   theme_ipsum(grid_col = "transparent", axis_title_size = 10) +
    theme(
      plot.title = element_text(hjust = 0.5, size = 10, colour = "grey35", margin=margin(0, 0, 10, 0)),
      plot.caption = element_text(size = 8, colour = "grey35"),
      legend.position = "bottom",
      axis.title = element_text(colour = "grey35"),
      axis.text.x = element_text(colour = "grey35"),
      axis.text.y = element_text(vjust = 0.5, hjust = 1, size = 10),
      axis.line = element_line(colour = "grey75"),
      legend.text = element_text(colour = "grey35"),
      strip.text = element_text(colour = "grey35", hjust = 0.5)) +
  coord_flip() +
  labs(title = "Percentatge de RNSA per cada UBA",
       caption = "FONT: SISAP")
```

### Column2 {.no-title}

#### **Problemes amb la variable RNSA**

  * **Definició**: La visita de RNSA no es registra de manera homogènia i, per tant, no es pot fer servir ni la rivita 9R ni etiquetes.
  Per a resoldre aquest problema, s'ha mirat si hi ha registre de la variable de revisió del nen sa de l’ecap (*RNSA*). Si existeix, llavors la visita del centre es considera del nen sa.

  * **Variabilitat entre professionals**: El percentatge de RNSA sobre el total de les visites és molt variable entre professionals (rang: `r paste0(format(min(up_uba$RNSA_p, na.rm = T), digits = 4, nsmall = 2, decimal.mark = ","), "%")` - `r paste0(format(max(up_uba$RNSA_p, na.rm = T), digits = 4, nsmall = 2, decimal.mark = ","), "%")`).
  Per a resoldre aquest problema s'ha estudiat la distribució del percentatge de RNSA de totes les UBA de l'ICS i **s'han exclòs aquelles UBA amb valors extrems tant superiors com inferiors**.
  L'eliminació d'aquestes UBA ha fet que s'excloguessin **`r length(unique(up_uba[!up %in% up_incloses]$up))` EAP senceres**.

El gràfic de l'esquerra és un diagrama de caixes o boxplot. En una sola imatge es poden observar el rang, la mediana, el percentil 25 i 75 i els valors extrems per una variable. L'eix de les X indica els valors del percentatge de visites de RNSA que fan les diferents UBA. El límit de l'esquerra de la caixa indica el percentil 25, és a dir, el valor que deixa el 25% de les UBA amb valors inferiors i el 75% de les UBA amb valors superiors. El percentil 75 queda delimitat pel límit dret de la caixa i la mediana (valor que separa el 50% de les UBA) és la línia recta que hi ha al mig de la caixa. Els dos segments que surten de la caixa (a esquerra i dreta) mostren a partir de quin moment es considera que una UBA és una observació de valor extrem. Aquestes UBA amb valors extrems per a la variable RNSA son els puntets de color negre que s'observen als extrems del gràfic. Aquestes mateixes UBA son les que s'exclouen de l'estudi.

Variables de demanda
=====================================

Aquest taulell dona informació sobre les variables explicatives que s'han tingut en compte per ajustar el model.

Es distribueix en dos apartats:

  * Una taula amb els valors de l'anàlisi descriptiva dels EAP per les variables de demanda.
  * Una explicació de les exclusions que s'han considerat per algunes de les variables explicatives.

Row3  {data-height=50 .textRow}
-------------------------------------

Caracterització de la demanda

Row2
-------------------------------------

### Column1 {.no-title}

  * **Edat:** percentatge de nens/es menors de 3 anys.
  * **Sexe:** percentatge de nenes d'un EAP.
  * **Complexitat dels pacients:** 
    * GMA mitjana.
    * Percentatge de nens/es amb més de dos malalties cròniques.
    * Percentatge de nens/es amb patologia social. [Aquí caldria explicar què s'ha considerat per patologia social]
    * Percentatge de nens/es amb patologia mental. [Aquí caldria explicar què s'ha considerat per patologia mental]
  * **Variables socioeconòmiques:** 
    * Percentatge de nens/es immigrants. [Aquí caldria explicar què s'ha considerat per immigració (països de baixa renta)]
    * Percentatge de nens/es pensionistes. 
    * Percentatge de nens/es nouvinguts. [Aquí caldria explicar què s'ha considerat per nouvingut (on s'han exclòs els nounats)]
    * Percentatge de nens/es nouvinguts immigrants. [Aquí caldria explicar què s'ha considerat per nouvingut (on s'han exclòs els nounats)]
    * Índex socioecònomic de privació AQUAS. [Link amb info de l'AQUAS???]
  * **Mida del EAP**.
    Els EAP s'han classificat en
      - Centres petits: De 1000 a 3.500 pacients assignats.
      - Centres mitjans: De 3.500 pacients assignats a 6.000.
      - Centres grans: Més de 6.000 pacients assignats.
  * **Ruralitat**
  La ruralitat s'ha tingut en compte a partir de la variable MEDEA que classifica els centres en rurals i urbans.
  
### Column2 {.no-title .chart-wrapper1}

```{r}
t <- do.call("rbind",
             lapply(c("edat_MenysO2_mean", "gma_mean", "patologia_mental_mean", "aquas"), function(x){
               dt <- data.table(Variable = x,
                                N = nrow(dm1_gma[!is.na(get(x))]),
                                Mitjana = mean(dm1_gma[, get(x)], na.rm = T),
                                DE = sd(dm1_gma[, get(x)], na.rm = T),
                                Mediana = mean(dm1_gma[, get(x)], probs = .5, na.rm = T),
                                P5 = quantile(dm1_gma[, get(x)], probs = .05, na.rm = T),
                                P95 = quantile(dm1_gma[, get(x)], probs = .95, na.rm = T))
             }))
t_cat <- dm1_gma[, .N, POBASS_f]
t_cat[, Mitjana := N/nrow(dm1_gma[!is.na(POBASS_f)])*100]
t_cat[, c("DE", "Mediana", "P5", "P95") := NA]
names(t_cat)[1] <- "Variable"
t <- rbind(t, t_cat)
t[, Variables := c("Percentatge de nens/es menors de 3 anys", 
                   # "Percentatge de nenes",
                   "GMA mitjana",
                   # "Percentatge de nens/es amb més de dos malalties cròniques",
                   # "Percentatge de nens/es amb patologia social",
                   "Percentatge de nens/es amb patologia mental",
                   # "Percentatge de nens/es immigrants",
                   # "Percentatge de nens/es pensionistes",
                   # "Percentatge de nens/es nouvinguts",
                   # "Percentatge de nens/es nouvinguts immigrants",
                   "Índex de privació socioeconòmica AQUAS",
                   "Centres petits: Menys de 3.500 pacients assignats",
                   "Centres mitjans: De 3.500 pacients assignats a 6.000",
                   "Centres grans: Més de 6.000 pacients assignats"
                   # "Centres semi rurals (2R)",
                   # "Centres urbans amb baixa privació (1U)",
                   # "Centres urbans amb mitja-baixa privació (2U)",
                   # "Centres urbans amb mitja-alta privació (3U)",
                   # "Centres urbans amb alta privació (4U)"
                   )]
t <- t[, c("Variables", "N", "Mitjana", "Mediana", "DE", "P5", "P95")]
```

```{r}
names(t) <- c("",
              "Nombre d'EAP",
              "Mitjana", 
              paste0("DE", footnote_marker_alphabet(1)), 
              "Mediana", 
              paste0("P5", footnote_marker_alphabet(2)), 
              paste0("P95", footnote_marker_alphabet(3)))

t %>%
  kable(digits = 3, row.names = F, escape = F, caption = "Descripció dels EAP segons les variables de demanda que finalment entren al model",
        align = c("l", "c", "c", "c", "c", "c"), format.args = list(decimal.mark = ',', big.mark = "."))%>%
  kable_styling(bootstrap_options = c("hover", "condensed", "responsive", "stripped"), position = "center", full_width = T) %>%
  row_spec(0, bold = T, align = "c", background = "#0061a9", color = "white") %>%
  column_spec(1, bold = F, background = "#E7F0F7") %>%
  footnote(alphabet = c("Desviació Estàndard; ", "Percentil 5; ", "Percentil 95; ")) %>%
  group_rows("Mida del EAP", 5, 7, label_row_css = "background-color: #0061a9; color: white;" )
```



Row3  {data-height=50 .textRow}
-------------------------------------

Exclusions

Row4
-------------------------------------

### Column1{.no-title}

#### **Centres amb valors extrems de GMA**

```{r fig.align="center", fig.height=4}
ggplot(up[!medea_f %in% c("0R", "1R")], aes(x = "", y = gma_mean)) +
  geom_boxplot(fill = "#0061a9") +
  xlab("") + ylab("Mitjana GMA") +
   theme_ipsum(grid_col = "transparent", axis_title_size = 10) +
    theme(
      plot.title = element_text(hjust = 0.5, size = 10, colour = "grey35", margin=margin(0, 0, 10, 0)),
      plot.caption = element_text(size = 8, colour = "grey35"),
      legend.position = "bottom",
      axis.title = element_text(colour = "grey35"),
      axis.text.x = element_text(colour = "grey35"),
      axis.text.y = element_text(vjust = 0.5, hjust = 1, size = 10),
      axis.line = element_line(colour = "grey75"),
      legend.text = element_text(colour = "grey35"),
      strip.text = element_text(colour = "grey35", hjust = 0.5)) +
  coord_flip() +
  labs(title = "Mitjana de la GMA per cada UBA",
       caption = "FONT: SISAP")
```

>El gràfic de l’esquerra és un diagrama de caixes o boxplot. En una sola imatge es poden observar el rang, la mediana, el percentil 25 i 75 i els valors extrems per una variable. L’eix de les X indica la mitjana de la GMA dels EAP. El límit de l’esquerra de la caixa indica el percentil 25, és a dir, el valor que deixa el 25% dels EAP amb valors inferiors i el 75% dels EAP amb valors superiors. El percentil 75 queda delimitat pel límit dret de la caixa i la mediana (valor que separa el 50% dels EAP) és la línia recta que hi ha al mig de la caixa. Els dos segments que surten de la caixa (a esquerra i dreta) mostren a partir de quin moment es considera que una EAP és una observació de valor extrem.<br> Com es pot observar amb aquest gràfic, tot i haver uns quants EAP que es consideren valors extrems (o outliers), n'hi ha un d'inferior i un de superior que s'allunya molt considerablement de la majoria dels EAP.<br> Per evitar problemes de sobre dotació de recursos i biaixos de predicció,  s'ha optat per excloure de l'ajust del model les dues EAP amb valors per la mitjana del GMA més extrems.

### Column2{.no-title}

#### **Centres rural i semi-rurals**

  * Centres molt petits 

  * Dificultats per explicar la freqüentació en funció de la demanda

  * No modelem les UPs rurals (0R i 1R)

