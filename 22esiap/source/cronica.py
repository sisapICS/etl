# coding: latin1

"""
.
"""

import collections as c

import shared as s
import sisapUtils as u


INDICADOR = "ESIAP0301"
FORA = ("HTA", "DM", "MPOC", "IC")

agr_ps = 764
agr_vs = {917: "= 1", 918: ">= 0.20"}

class Cronica(object):
    """."""

    def __init__(self):
        """."""
        self.get_date_dextraccio()
        self.get_problemes()
        self.getmultiprocess_vs()
        self.getmultiprocess_ares()
        self.get_visitats()
        self.get_resultat()
        self.export()

    def get_date_dextraccio(self):
            """Get current extraction date. Returns date in datetime.date format"""
            sql="select data_ext from dextraccio"
            self.current_date = u.getOne(sql, "nodrizas")[0]

    def get_problemes(self):
        """."""
        self.problemes = set()
        sql = "select id, ps from inf_problemes"
        for id, ps in u.getAll(sql, "nodrizas"):
            if not any([grup in FORA for grup in ps.split(",")]):
                self.problemes.add(id)

    def get_variables(self,table):
        pac=set()

        filter = str()
        for i, (vu_cod_vs, vu_val) in enumerate(self.variables_codis.items()):
            if i == 0:
                filter += "(vu_cod_vs = '{}' AND vu_val {}) ".format(vu_cod_vs, vu_val)
            else:
                filter += "OR (vu_cod_vs = '{}' AND vu_val {}) ".format(vu_cod_vs, vu_val)
        sql='select id_cip_sec from {} where {}'.format(table,filter)
        
        for id, in u.getAll(sql,"import"):
            pac.add(id)
        return pac

    def get_ares(self,table):
        pac=set()
        sql='select id_cip_sec, pi_data_inici, pi_data_fi from {} where pi_codi_pc in {}'.format(table,str(tuple(self.problemes_codis)))

        for id, data_inici, data_fi in u.getAll(sql,"import"):
            if data_inici < self.current_date and not data_fi:
                pac.add(id)
            elif data_inici < self.current_date <= data_fi:
                pac.add(id)
        return pac

    def getmultiprocess_vs(self):
        self.variables_codis={criteri_codi: agr_vs[agrupador] for agrupador, criteri_codi, in u.getAll("SELECT agrupador, criteri_codi FROM eqa_criteris WHERE agrupador in {}".format(str(tuple(agr_vs.keys()))), "nodrizas")}
        self.variables = set.union(*[id for id in u.multiprocess(self.get_variables, u.getSubTables('variables'))])
    
    def getmultiprocess_ares(self):
        self.problemes_codis=set(criteri_codi for criteri_codi, in u.getAll("SELECT criteri_codi FROM eqa_criteris WHERE agrupador = {}".format(agr_ps), "nodrizas"))
        self.pla_cures = set.union(*[id for id in u.multiprocess(self.get_ares, u.getSubTables('ares'))])

    def get_visitats(self):
        """."""
        sql = "select id from mst_pobatinf where esiap = 1"
        self.visitats = set([id for id, in u.getAll(sql, "altres")])

    def get_resultat(self):
        """."""
        self.resultat = c.Counter()
        self.denominador = set().union(self.problemes, self.variables, self.pla_cures)
        sql = "select id_cip_sec, up, ubainf from assignada_tot \
               where edat > 14 and ates = 1"
        for id, up, uba in u.getAll(sql, "nodrizas"):
            if id in self.denominador:
                self.resultat[(up, uba, "DEN")] += 1
                if id in self.visitats:
                    self.resultat[(up, uba, "NUM")] += 1

    def export(self):
        """."""
        upload = [(INDICADOR, k[0], k[1], k[2], v)
                  for (k, v) in self.resultat.items()]
        u.listToTable(upload, s.TABLE, s.DATABASE)


if __name__ == "__main__":
    Cronica()
