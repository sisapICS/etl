# coding: latin1

"""
.
"""

import collections as c

import shared as s
import sisapUtils as u
from dateutil.relativedelta import relativedelta


class Preventives(object):
    """."""

    def __init__(self):
        """."""
        self.cribrats = c.defaultdict(set)
        self.cribrables = c.defaultdict(set)
        self.get_edats()
        self.get_variables()
        self.get_tabac()
        self.get_problemes()
        self.get_hashos()
        self.get_resultat()
        self.export()
    
    def get_edats(self):
        self.edats = {}
        sql = """select id_cip_sec, edat from nodrizas.assignada_tot"""
        for id, edat in u.getAll(sql, 'nodrizas'):
            self.edats[id] = edat

    def get_variables(self):
        """."""
        codis = {84: "ESIAP0401", 240: "ESIAP0403", 241: "ESIAP0403",
                 242: "ESIAP0403", 243: "ESIAP0403", 859: "ESIAP0403"}
        sql = "select id_cip_sec, agrupador \
               from eqa_variables, dextraccio \
               where agrupador in {} and \
                     usar = 1 and \
                     data_var between adddate(data_ext, interval -2 year) and \
                                      data_ext".format(tuple(codis.keys()))
        for id, agr in u.getAll(sql, "nodrizas"):
            self.cribrats[codis[agr]].add(id)

    def get_tabac(self):
        """."""
        # ESIAP0402 Darrers 24 mesos, si t� ? 25 anys o consta que en algun moment hagi estat fumador
        # Nom�s 1 sol registre, si t� >25 anys i tots els registres indiquen que mai ha sigut fumador.
        fumador_ex_fumador = set()
        no_fum = set()
        sql = """select id_cip_sec, tab
                    from nodrizas.eqa_tabac"""
        for id, tab in u.getAll(sql, 'nodrizas'):
            if tab in (1,2):
                fumador_ex_fumador.add(id)
            else:
                no_fum.add(id)
        for id in no_fum:
            if id in self.edats:
                edat = self.edats[id]
                if edat > 25 and id not in fumador_ex_fumador:
                    self.cribrats["ESIAP0402"].add(id)
        sql = "select id_cip_sec, tab \
               from eqa_tabac, dextraccio \
               where last = 1 and \
                     dlast between adddate(data_ext, interval -2 year) and \
                                   data_ext"
        for id, tab in u.getAll(sql, "nodrizas"):
            if id in self.edats:
                edat = self.edats[id]
                if edat <= 25 or id in fumador_ex_fumador:
                    self.cribrats["ESIAP0402"].add(id)
            if tab == 1:
                self.cribrables["ESIAP0403"].add(id)
        self.dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        menys1 = self.dext - relativedelta(years=1)
        self.resultat = c.Counter()
        self.medicina = c.Counter()
        sql = """
            select distinct a.id_cip_sec, b.up, b.uba, b.ubainf
            from eqa_tabac a
            inner join assignada_tot b
            on a.id_cip_sec = b.id_cip_sec
            where b.edat > 14
            and DATE '{DEXTD}' between dalta and dbaixa
            and a.last = 1
            and (a.tab  = 1
            or (a.tab = 2
            and a.dlast >= DATE '{menys1}'))
        """.format(DEXTD=self.dext, menys1 = menys1)
        # dades = c.Counter()
        ups = c.defaultdict(set)
        for id, up, uba, ubainf in u.getAll(sql, 'nodrizas'):
            self.medicina[('ESIAP0408', up, uba, 'DEN')] += 1
            self.resultat[('ESIAP0408', up, ubainf, 'DEN')] += 1
            # dades[(up, uba, 'DEN')] += 1
            ups[id].add((up, uba, ubainf))
            # self.cribrables["ESIAP0408"].add(id)
        
        sql = """
            SELECT distinct id_cip_sec
            FROM eqa_variables
            WHERE agrupador = 827
            AND data_var >= DATE '{menys1}'
        """.format(menys1=menys1)
        for id, in u.getAll(sql, 'nodrizas'):
            # self.cribrats["ESIAP0408"].add(id)
            if id in ups:
                for (up, uba, ubainf) in ups[id]:
                    # dades[(up, uba, "NUM")] += 1
                    self.medicina[('ESIAP0408', up, uba, 'NUM')] += 1
                    self.resultat[('ESIAP0408', up, ubainf, 'NUM')] += 1
        # upload = []
        # for (up, analisi), val in dades.items():
        #     upload.append((up, analisi, val))
        # cols = "(up varchar(10), uba varchar(10), analisi varchar(3), val int)"
        # u.createTable('prova_tabac', cols, 'altres', rm=True)
        # u.listToTable(upload, 'prova_tabac', 'altres')

    def get_problemes(self):
        """."""
        sql = "select id_cip_sec, ps \
               from eqa_problemes \
               where ps in (84, 18, 24, 239, 267, 47, 55)"
        for id, ps in u.getAll(sql, "nodrizas"):
            if ps == 84:
                self.cribrats["ESIAP0401"].add(id)
                # self.cribrats["ESIAP0401A"].add(id)
            else:
                self.cribrables["ESIAP0403"].add(id)
    
    def get_hashos(self):
        self.cip_2_hash = {}
        sql = """select id_cip_sec, hash_d from import.u11"""
        for id, hash in u.getAll(sql, 'import'):
            self.cip_2_hash[id] = hash

    def get_resultat(self):
        """."""
        self.llistats = []
        sql = "select id_cip_sec, up, ubainf, uba, codi_sector, edat \
               from assignada_tot \
               where edat between 15 and 60 and \
                     ates = 1"
        for id, up, uba, med, sector, edat in u.getAll(sql, "nodrizas"):
            for ind in self.cribrats:
                if (ind not in self.cribrables or id in self.cribrables[ind]):
                    den = 1
                    num = 0 
                    self.resultat[(ind, up, uba, "DEN")] += 1
                    self.medicina[(ind, up, med, "DEN")] += 1
                    if id in self.cribrats[ind]:
                        self.resultat[(ind, up, uba, "NUM")] += 1
                        self.medicina[(ind, up, med, "NUM")] += 1
                        num = 1
                    # if ind in ('ESIAP0401', 'ESIAP0402'):
                    if ind in ('ESIAP0401'):
                        if id in self.cip_2_hash:
                            hash = self.cip_2_hash[id]
                            if num == 0 and den == 1:
                                self.llistats.append((up, med, 'M', ind, hash, sector, 0))
                                self.llistats.append((up, uba, 'I', ind, hash, sector, 0))

    def export(self):
        """."""
        upload = [k + (v,) for (k, v) in self.resultat.items()]
        u.listToTable(upload, s.TABLE, s.DATABASE)
        upload_m = [k + (v,) for (k, v) in self.medicina.items()]
        u.listToTable(upload_m, s.TABLE_MED, s.DATABASE)
        cols = """(up varchar(5), uba varchar(5), tipus varchar(5), 
                    indicador varchar(15), hash varchar(40), 
                    sector varchar(5), exclos int)"""
        u.createTable(s.TABLE_LLISTATS, cols, s.DATABASE)
        u.listToTable(self.llistats, s.TABLE_LLISTATS, s.DATABASE)


if __name__ == "__main__":
    Preventives()
