# coding: latin1

"""
.
"""

import collections as c

import shared as s
import sisapUtils as u
import sisaptools as t


INDICADOR = "ESIAP0300"


class Cronica(object):
    """."""

    def __init__(self):
        """."""
        self.get_date_dextraccio()
        self.poblacio_interes()
        self.get_catalegs_cures()
        self.get_no_agudes()
        self.get_plans_de_cures()
        self.get_resultat()
        self.export()

    def get_date_dextraccio(self):
        """Get current extraction date. Returns date in datetime.date format"""
        sql="select data_ext from dextraccio"
        self.current_date = u.getOne(sql, "nodrizas")[0]
        print(self.current_date)
    
    def poblacio_interes(self):
        """Get the patients of interest"""
        print('pop')
        self.poblacio = {}
        sql = """select id_cip_sec, up, ubainf 
            from nodrizas.assignada_tot
            where ates_inf = 1 and edat > 14"""
        for id, up, ubainf in u.getAll(sql, 'nodrizas'):
            self.poblacio[id] = (up, ubainf)

    def get_catalegs_cures(self):
        """Get convertions between offical codes and ECAP codes"""
        print('catalegs')
        self.cat_activitats = {}
        self.cat_variables = {}
        sql = """SELECT AC_COD, AC_COD_OFIC  FROM import.cat_PRSTB002"""
        for cod, cod_ofi in u.getAll(sql, 'import'):
            self.cat_activitats[cod_ofi] = cod
        sql = """SELECT VS_COD, VS_COD_OFIC FROM import.cat_PRSTB004"""
        for cod, cod_ofi in u.getAll(sql, 'import'):
            self.cat_variables[cod_ofi] = cod
    
    def get_no_agudes(self):
        self.no_agudes = set()
        sql = """SELECT pc_codi FROM import.cat_prstb335
                    WHERE pc_aguda = 'N'"""
        for pc_codi, in u.getAll(sql, 'import'):
            self.no_agudes.add(pc_codi)

    def get_plans_de_cures(self):
        """Get the numerator: people in de den with some pla de cures active which is not HTA, MPOC, DM2, IC"""
        print('pla_de_cures')
        self.num = set()
        jobs1 = [(sector, self.current_date, self.cat_variables, self.cat_activitats, self.no_agudes) for sector in u.sectors]
        pool = t.NoDaemonPool(len(jobs1))
        self.result = pool.map(get_data, jobs1)
        pool.close()
        pool.join()

    
    def get_resultat(self):
        """."""
        print('resultat')
        self.num = set()
        for num in self.result:
            self.num = self.num.union(num)
        self.data = c.Counter()
        for id, (up, uba) in self.poblacio.items():
            self.data[(up, uba, 'DEN')] += 1
            if id in self.num:
                self.data[(up, uba, 'NUM')] += 1

    def export(self):
        """."""
        print('export')
        upload = [(INDICADOR, k[0], k[1], k[2], v)
                  for (k, v) in self.data.items()]
        u.listToTable(upload, s.TABLE, s.DATABASE)


def get_data(params):
    sector, current_date, cat_variables, cat_activitats, no_agudes = params
    print(sector)
    actius = set()
    num = set()
    variables = set()
    activitats = set()
    print('query 1')
    sql = """SELECT iD_cip_sec, PI_CODI_SEQ, pi_codi_pc,
            CASE WHEN pi_data_inici > DATE '{data}' - 365 THEN 1 ELSE 0 END ultim_any
            FROM import.ares_s{sector} 
            WHERE (pi_data_fi > DATE '{data}'
            OR pi_data_fi IS NULL)
            AND pi_codi_pc NOT IN ('PC0031',
            'PC0057',
            'PC0028',
            'PC0032',
            'PC0029',
            'PC0040',
            'PC0039',
            'PC0041',
            'PC0042',
            'PC0043',
            'PC0044',
            'PC0051',
            'PC0052',
            'PC0053',
            'PC0054')""".format(data=current_date,sector=sector)
    for cip, seq, pi_codi, ultim_any in u.getAll(sql, 'import'):
        if pi_codi in no_agudes:
            if ultim_any: 
                num.add(cip)
            else:
                actius.add(seq)
    print('query 2')
    sql = """SELECT ELI_CODI_SEQ, id_cip_sec, ELI_CODI_ELEMENT,
            CASE WHEN ELI_DIA_INICI > DATE '{data}' - 365 THEN 1 ELSE 0 END ultim_any
            FROM import.atic_s{sector}""".format(data=current_date,sector=sector)
    for seq, cip, codi_element, ultim_any in u.getAll(sql, 'import'):
        if seq in actius:
            if ultim_any:
                num.add(cip)
            else:
                if codi_element in cat_variables:
                    codi_element = cat_variables[codi_element]
                    variables.add((cip, codi_element))
                elif codi_element in cat_activitats:
                    codi_element = cat_activitats[codi_element]
                    activitats.add((cip, codi_element))
    print('query 3')
    sql = """select au_cod_ac, id_cip_sec from IMPORT.ACTIVITATS1""".format(current_date)
    for act, id in u.getAll(sql, 'import'):
        if (id, act) in activitats:
            num.add(cip)
    print('query 4')
    sql = """select vu_cod_vs, id_cip_sec from IMPORT.VARIABLES1""".format(current_date)
    for var, id in u.getAll(sql, 'import'):
        if (id, act) in variables:
            num.add(cip)
    print(sector, 'finished!!!')
    return num


if __name__ == "__main__":
    Cronica()
    # try:
    #     Cronica()
    #     u.sendGeneral('SISAP <sisap@gencat.cat>',
    #                     ('roser.cantenys@catsalut.cat', 'roser.cantenys@catsalut.cat'),
    #                     ('roser.cantenys@catsalut.cat', 'roser.cantenys@catsalut.cat'),
    #                     'cronica 1 good',
    #                     ':)')
    # except Exception as e:
    #     to_send = str(e)
    #     u.sendGeneral('SISAP <sisap@gencat.cat>',
    #                     ('roser.cantenys@catsalut.cat', 'roser.cantenys@catsalut.cat'),
    #                     ('roser.cantenys@catsalut.cat', 'roser.cantenys@catsalut.cat'),
    #                     'cronica 1 bad',
    #                     to_send)
    
