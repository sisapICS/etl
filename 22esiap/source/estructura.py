# coding: latin1

"""
.
"""

import shared as s
import sisapUtils as u


cols = ("indicador varchar(15)", "up varchar(10)", "uba varchar(10)",
        "analisi varchar(15)", "valor decimal(10, 2)")
for table in (s.TABLE, s.TABLE_MED):
    u.createTable(table, "({})".format(", ".join(cols)), s.DATABASE, rm=True)

cols = ("indicador varchar(15)", "literal varchar(200)", "pare varchar(20)", "invers int")
u.createTable(s.TABLE_CATALEG, "({})".format(", ".join(cols)), s.DATABASE, rm=True)