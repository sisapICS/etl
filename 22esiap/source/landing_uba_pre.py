# -*- coding: utf8 -*-


import sisapUtils as u
import collections as c
from datetime import date, datetime
import sisaptools as t


class Landing_UBA():
    def __init__(self):
        sql = """DELETE FROM dwlanding.INDICADORS_uba_tr
                WHERE INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002',
                'ECONS0008', 'ACC5D', 'POBASS80M')
                OR indicador LIKE 'EQ%' or indicador like 'ADM%'
                OR indicador like 'ESIAP%'"""
        u.execute(sql, 'exadata_pre')
        cols = """(
            up varchar(5),
            uba varchar(20),
            servei varchar(20),
            indicador varchar(30),
            num int,
            den int,
            no_resolts int,
            valor varchar(20),
            situacio varchar(20),
            ordre int,
            dextraccio date,
            avui date
        )"""
        u.createTable('proves_landing_uba', cols, 'eqa_ind', rm=True)
        sql = """select data_ext from dextraccio"""
        for dext, in u.getAll(sql, 'nodrizas'):
            self.data_ext = dext
        self.TO_DO = c.defaultdict(dict)
        sql = """SELECT indicador, ordre_sanitari, ordre_adm 
                FROM DWLANDING.indicadors 
                WHERE responsable = 'SISAP'
                """
        for indicador, ordre_san, ordre_adm in u.getAll(sql, 'exadata_pre'):
            self.TO_DO['SAN'][indicador] = ordre_san
            self.TO_DO['ADM'][indicador] = ordre_adm
        self.indicadors_uba_fets = c.defaultdict(set)
        self.today = datetime.now()
        self.get_centres()
        self.get_data()
        self.transform_data()
        self.uploading()

    def get_centres(self):
        self.centres = {}
        sql = """select scs_codi, ics_codi from nodrizas.cat_centres"""
        for up, br in u.getAll(sql, 'nodrizas'):
            self.centres[br] = up

    def get_data(self):
        print('grep data')
        self.data = c.defaultdict(dict)
        self.metes = c.defaultdict(dict)
        sql = """select up, uba, tipus,
                    concat(substring(indicador, 1, 3), 
                    substring(indicador, 5)) indicador, analisis, valor 
                    from eqa_ind.exp_khalix_uba_ind
                    where tipus in ('M', 'I')
                union
                select up, uba, 'I', indicador, analisi, valor
                from esiap.exp_khalix
                where uba <> ''"""
        for up, uba, tipus, indi, analisi, valor in u.getAll(sql, 'eqa_ind'):
            if analisi in ('AGMMINRES', 'AGMMAXRES', 'AGRESOLTR'):
                self.metes[(up, uba, tipus, indi)][analisi] = valor
            if analisi in ('NUM', 'DEN'):
                self.data[(up, uba, tipus, indi)][analisi] = valor
                self.indicadors_uba_fets[(up, uba, tipus)].add(indi)

        self.cat_inversos = {}
        sql = """select indicador, invers from eqa_ind.exp_ecap_cataleg
                    where pantalla = 'GENERAL'
                    and TOSHOW = 1"""
        for indicador, invers in u.getAll(sql, 'eqa_ind'):
            self.cat_inversos[indicador] = invers
        
        sql = """select indicador, invers from esiap.esiap_cataleg"""
        for indicador, invers in u.getAll(sql, 'esiap'):
            self.cat_inversos[indicador] = invers
            
        self.pitjors_indis = c.defaultdict(list)
        # 5 pitjors per cada uba
        sql = """select a.up, a.uba, a.tipus,
                concat(substring(a.indicador, 1, 3),
                substring(a.indicador, 5)) indicador, a.valor
                from eqa_ind.exp_khalix_uba_ind a, EQA_IND.EXP_ECAP_CATALEGPUNTS b
                where a.tipus in ('M', 'I')
                and a.analisis = 'AGASSOLP'
                and b.CATEGORIA = 'P'
                and a.tipus = b.tipus
                and concat(substring(a.indicador, 1, 3),
                substring(a.indicador, 5)) = b.indicador
                order by a.UP, a.UBA, a.TIPUS, a.VALOR"""
        for up, uba, tipus, indicador, valor in u.getAll(sql, 'eqa_ind'):
            if indicador in self.cat_inversos:
                if (up, uba, tipus, indicador) in self.data:
                    self.pitjors_indis[(up, uba, tipus)].append(indicador)

        self.pitjors_esiap = c.defaultdict(list)
        # 5 pitjors per cada uba
        sql = """select up, uba, 'I' as tipus, indicador, num*100/den as valor
                 from esiap.landing_esiap
                 where uba <> ''
                 AND indicador not in ('ESIAP0201', 'ESIAP0202', 'ESIAP0602', 'ESIAPP0201', 'ESIAPP0202', 'ESIAPP0602')
                 order by up, uba, 'I', indicador, num*100/den
            """
        for up, uba, tipus, indicador, valor in u.getAll(sql, 'esiap'):
            if indicador in self.cat_inversos:
                if (up, uba, tipus, indicador) in self.data:
                    self.pitjors_esiap[(up, uba, tipus)].append(indicador)

        # demora
        # sql = """select SUBSTRING(K2, 1,5) UP, SUBSTRING(K2, 7,5) UBA,  
        #             SUBSTRING(K2, 6,1) tipus, k0 indicador, 'N' analisi, v
        #             from altres.demora_uba
        #             where k0 = 'ECONS0008'
        #             and SUBSTRING(K2, 6,1) in ('M', 'I')"""
        # for br, uba, tipus, indi, analisi, valor in u.getAll(sql, 'eqa_ind'):
        #     if br in self.centres:
        #         up = self.centres[br]
        #         self.data[(up, uba, tipus, indi)][analisi] = valor
        #         self.indicadors_uba_fets[(up, uba, tipus)].add(indi)
        
        # demora 9e
        sql = """select 'ECONS0008', substring(ent, 1, 5) up, substring(ent, 7, 5) as uba,
                    substring(ent, 6, 1) as tipus,
                    analisis, n 
                    from altres.exp_khalix_uba_econsulta ekue 
                    where periode = 'MENSUAL'
                    and indicador = 'ECONS0008'"""
        for indi, br, uba, tipus, analisi, n in u.getAll(sql, 'altres'):
            if br in self.centres:
                up = self.centres[br]
                self.data[(up, uba, tipus, indi)][analisi] = n
                self.indicadors_uba_fets[(up, uba, tipus)].add(indi)

        # accessibilitat 5D
        sql = """SELECT max(dataany) FROM ACCindicadors
                WHERE INDICADOR = 'ACC5D'"""
        for maxim, in u.getAll(sql, 'pdp'):
            max_any = maxim

        # accessibilitat 5D
        sql = """SELECT max(datames) FROM ACCindicadors
                WHERE INDICADOR = 'ACC5D'
                AND dataany = {}""".format(max_any)
        for maxim, in u.getAll(sql, 'pdp'):
            max_mes = maxim

        sql = """SELECT tipus, up, uab, NUMERADOR, DENOMINADOR 
                    FROM ACCindicadors 
                    WHERE dataany = {} and indicador = 'ACC5D' 
                    AND datames = {}""".format(max_any, max_mes)
        for tipus, up, uba, num, den in u.getAll(sql, 'pdp'):
            self.data[(up, uba, tipus, 'ACC5D')]['NUM'] = num
            self.data[(up, uba, tipus, 'ACC5D')]['DEN'] = den
            self.indicadors_uba_fets[(up, uba, tipus)].add('ACC5D')

        # poblacio longitudinaliat
        # detectem data maxima de carrega
        sql = """SELECT EXTRACT(YEAR FROM MAX(to_date(concat(dataany, datames), 'YYYYMM'))) year,
                EXTRACT(month FROM MAX(to_date(concat(dataany, datames), 'YYYYMM'))) month
                FROM accindicadors
                WHERE indicador = 'CONT0002'"""
        for y, m in u.getAll(sql, 'pdp'):
            year = y 
            month = m 
        
        # importem dades
        sql = """SELECT UP, UAB, TIPUS, INDICADOR, NUMERADOR, DENOMINADOR 
                FROM accindicadors
                WHERE indicador = 'CONT0002'
                and dataany = {year} AND datames = {month}
                AND tipus IN ('M', 'I')""".format(year=year, month=month)
        for up, uba, tipus, indi, num, den in u.getAll(sql, 'pdp'):
            self.data[(up, uba, tipus, indi)]['NUM'] = num
            self.data[(up, uba, tipus, indi)]['DEN'] = den
            self.indicadors_uba_fets[(up, uba, tipus)].add(indi)

        # sintetic EQA
        # detectem data maxima de carrega
        print('sintetic')
        sql = """SELECT max(dataany) FROM eqasintetic"""
        for y, in u.getAll(sql, 'pdp'):
            year = y 
        
        sql = """SELECT max(datames) FROM eqasintetic where dataany = {year}""".format(year = year)
        for m, in u.getAll(sql, 'pdp'):
            month = m 

        sql = """SELECT UP, UAB, TIPUS, 'EQADULTS', PUNTS, PUNTSMAX FROM eqasintetic
                WHERE dataany = {year} AND datames = {month} AND tipus in ('M','I')""".format(year=year, month=month)
        print('sql sintetic', sql)
        for up, uba, tipus, indi, num, den in u.getAll(sql, 'pdp'):
            self.data[(up, uba, tipus, indi)]['NUM'] = num
            self.data[(up, uba, tipus, indi)]['DEN'] = 1000
            self.indicadors_uba_fets[(up, uba, tipus)].add(indi)

        sql = """
            SELECT up, uba, 'ESIAP_SINTETIC', sum(punts)
            FROM esiap_sintetic
            GROUP BY up, uba
        """
        for up, uba, indi, punts in u.getAll(sql, 'esiap'):
            self.data[(up, uba, 'I', indi)]['NUM'] = punts
            self.data[(up, uba, 'I', indi)]['DEN'] = 100
            self.indicadors_uba_fets[(up, uba, 'I')].add(indi)

        # metes sintètic
        self.metes_sintetic = c.defaultdict(dict)
        sql = """SELECT up, uab, tipus, mmin, mint, mmax FROM eqasinteticmetes
                    WHERE dataany = {year}
                    AND tipus IN ('M', 'I')""".format(year = year)
        for up, uba, tipus, mmin, mint, mmax in u.getAll(sql, 'pdp'):
            self.metes_sintetic[(up, uba, tipus)]['MMIN'] = mmin
            self.metes_sintetic[(up, uba, tipus)]['MMAX'] = mmax
            self.metes_sintetic[(up, uba, tipus)]['MINT'] = mint
        
        # indicadors poblacionals
        print('dbs')
        self.poblacionals = c.Counter()
        sql = """SELECT c_up, c_metge, c_infermera, c_uas, C_EDAT_ANYS,
                CASE WHEN C_VISITES_ANY > 0 THEN 1 ELSE 0 END ates,
                CASE WHEN PR_MACA_DATA IS NOT NULL THEN 1 ELSE 0 end maca,
                CASE WHEN pr_pcc_data IS NOT NULL THEN 1 ELSE 0 end pcc,
                CASE WHEN ps_atdom_data IS NOT NULL THEN 1 ELSE 0 end atdom,  
                case when C_GMA_COMPLEXITAT IS NULL THEN 0 ELSE  C_GMA_COMPLEXITAT END complex
                FROM dbs"""
        self.ubas_incloses = set()
        for up, uba, uba_inf, uas, edat, ates, maca, pcc, atdom, comple in u.getAll(sql, 'redics'):
            self.poblacionals[(up, uba, 'M', 'POBASS', self.TO_DO['SAN']['POBASS'])] += 1
            self.poblacionals[(up, uba_inf, 'I', 'POBASS', self.TO_DO['SAN']['POBASS'])] += 1
            self.poblacionals[(up, uas, 'A', 'POBASS', self.TO_DO['ADM']['POBASS'])] += 1
            self.ubas_incloses.add((up, uba_inf, 'I'))
            self.ubas_incloses.add((up, uba, 'M'))
            self.ubas_incloses.add((up, uas, 'A'))
            self.indicadors_uba_fets[(up, uba, 'M')].add('POBASS')
            self.indicadors_uba_fets[(up, uba_inf, 'I')].add('POBASS')
            self.indicadors_uba_fets[(up, uas, 'A')].add('POBASS')
            if ates == 1: 
                self.poblacionals[(up, uba, 'M', 'POBASSAT', self.TO_DO['SAN']['POBASSAT'])] += 1
                self.poblacionals[(up, uba_inf, 'I', 'POBASSAT', self.TO_DO['SAN']['POBASSAT'])] += 1
                self.indicadors_uba_fets[(up, uba, 'M')].add('POBASSAT')
                self.indicadors_uba_fets[(up, uba_inf, 'I')].add('POBASSAT')
            if maca == 1: 
                self.poblacionals[(up, uba, 'M', 'APMACA', self.TO_DO['SAN']['APMACA'])] += 1
                self.poblacionals[(up, uba_inf, 'I', 'APMACA', self.TO_DO['SAN']['APMACA'])] += 1
                self.indicadors_uba_fets[(up, uba, 'M')].add('APMACA')
                self.indicadors_uba_fets[(up, uba_inf, 'I')].add('APMACA')
                self.poblacionals[(up, uas, 'A', 'APMACA', self.TO_DO['ADM']['APMACA'])] += 1
                self.indicadors_uba_fets[(up, uas, 'A')].add('APMACA')
            if pcc == 1: 
                self.poblacionals[(up, uba, 'M', 'APPCC', self.TO_DO['SAN']['APPCC'])] += 1
                self.poblacionals[(up, uba_inf, 'I', 'APPCC', self.TO_DO['SAN']['APPCC'])] += 1
                self.indicadors_uba_fets[(up, uba, 'M')].add('APPCC')
                self.indicadors_uba_fets[(up, uba_inf, 'I')].add('APPCC')
                self.poblacionals[(up, uas, 'A', 'APPCC', self.TO_DO['ADM']['APPCC'])] += 1
                self.indicadors_uba_fets[(up, uas, 'A')].add('APPCC')
            if atdom == 1: 
                self.poblacionals[(up, uba, 'M', 'MA0009', self.TO_DO['SAN']['MA0009'])] += 1
                self.poblacionals[(up, uba_inf, 'I', 'MA0009', self.TO_DO['SAN']['MA0009'])] += 1
                self.indicadors_uba_fets[(up, uba, 'M')].add('MA0009')
                self.indicadors_uba_fets[(up, uba_inf, 'I')].add('MA0009')
                self.poblacionals[(up, uas, 'A', 'MA0009', self.TO_DO['ADM']['MA0009'])] += 1
                self.indicadors_uba_fets[(up, uas, 'A')].add('MA0009')
            if comple > 0: 
                self.poblacionals[(up, uba, 'M', 'MA0006', self.TO_DO['SAN']['MA0006'])] += comple
                self.poblacionals[(up, uba_inf, 'I', 'MA0006', self.TO_DO['SAN']['MA0006'])] += comple
                self.indicadors_uba_fets[(up, uba, 'M')].add('MA0006')
                self.indicadors_uba_fets[(up, uba_inf, 'I')].add('MA0006')
            if edat > 65: 
                self.poblacionals[(up, uba, 'M', 'POBASS65M', self.TO_DO['SAN']['POBASS65M'])] += 1
                self.poblacionals[(up, uba_inf, 'I', 'POBASS65M', self.TO_DO['SAN']['POBASS65M'])] += 1
                self.indicadors_uba_fets[(up, uba, 'M')].add('POBASS65M')
                self.indicadors_uba_fets[(up, uba_inf, 'I')].add('POBASS65M')
                self.poblacionals[(up, uas, 'A', 'POBASS65M', self.TO_DO['ADM']['POBASS65M'])] += 1
                self.indicadors_uba_fets[(up, uas, 'A')].add('POBASS65M')
            if edat > 80: 
                self.poblacionals[(up, uba, 'M', 'POBASS80M', self.TO_DO['SAN']['POBASS80M'])] += 1
                self.poblacionals[(up, uba_inf, 'I', 'POBASS80M', self.TO_DO['SAN']['POBASS80M'])] += 1
                self.indicadors_uba_fets[(up, uba, 'M')].add('POBASS80M')
                self.indicadors_uba_fets[(up, uba_inf, 'I')].add('POBASS80M')
                self.poblacionals[(up, uas, 'A', 'POBASS80M', self.TO_DO['ADM']['POBASS80M'])] += 1
                self.indicadors_uba_fets[(up, uas, 'A')].add('POBASS80M')
        
        # indicadors ADM
        sql = """select indicador, up, uas, analisi, sum(value)
                from gad.indicadors_adm
                where indicador in ('ADM004')
                group by indicador, up, uas, analisi"""
        for indi, up, uas, analisi, valor in u.getAll(sql, 'gad'):
            self.data[(up, uas, 'A', indi)][analisi] = valor
            self.data[(up, uas, 'A', indi)]['DEN'] = 1000
            self.indicadors_uba_fets[(up, uas, 'A')].add(indi)

    
    def transform_data(self):
        print('transform data')
        self.upload = set()
        for (up, uba, tipus, indi) in self.data.keys():
            if uba:
                den = 0
                num = 0
                n = 0
                if tipus == 'M': 
                    tip_prof = 'MG'
                elif tipus == 'I':
                    tip_prof = 'INF'
                elif tipus == 'A':
                    tip_prof = 'UAS'
                else: tip_prof = ''
                for analisi, valor in self.data[(up, uba, tipus, indi)].items():
                    if analisi == 'DEN': den = valor
                    elif analisi == 'NUM': num = valor
                    elif analisi == 'N': n = valor
                if tip_prof != 'UAS':
                    if indi in self.cat_inversos:
                        # els 5 pitjors indicadors x cada professional
                        ordre = None
                        for i, worst_ind in enumerate(self.pitjors_indis[(up, uba, tipus)]):
                            if i <= 4:
                                if worst_ind == indi:
                                    ordre = i+1
                        # els 5 pitjors ESIAP x cada professional
                        if tip_prof == 'INF':
                            ordre_esiap = None
                            for i, worst_ind in enumerate(self.pitjors_esiap[(up, uba, tipus)]):
                                if i <= 4:
                                    if worst_ind == indi:
                                        ordre_esiap = i+1
                        # metes
                        mmin, mmax, resultat = 0, 0, 0
                        for analisi, val in self.metes[(up, uba, tipus, indi)].items():
                            if analisi == 'AGMMINRES':
                                mmin = val
                            elif analisi == 'AGMMAXRES':
                                mmax = val 
                            elif analisi == 'AGRESOLTR':
                                resultat = val
                        mitjana = (mmin + mmax) / float(2)
                        no_resolts = 0
                        invers = self.cat_inversos[indi]
                        if invers:
                            no_resolts = num
                            if resultat >= mmax: situacio = '1'
                            if resultat >= mitjana and resultat < mmax: situacio = '2'
                            if resultat >= mmin and resultat < mitjana: situacio = '3'
                            if resultat < mmin: situacio = '4'
                        else: 
                            no_resolts = den - num
                            if indi[0:5] == 'EQD09':
                                no_resolts = None
                                num = min(num, den)
                            if resultat >= mmax: situacio = '4'
                            if resultat >= mitjana and resultat < mmax: situacio = '3'
                            if resultat >= mmin and resultat < mitjana: situacio = '2'
                            if resultat < mmin: situacio = '1'
                        if mmin == mmax: situacio = None
                        if den > 0:
                            if indi[:5] == 'ESIAP':
                                self.upload.add((up, uba, tip_prof, indi, num, den, no_resolts, '', '', ordre_esiap, self.data_ext, self.today))
                            else:
                                self.upload.add((up, uba, tip_prof, indi, num, den, no_resolts, '', situacio, ordre, self.data_ext, self.today))
                    else:
                        if indi == 'ECONS0008':
                            self.upload.add((up, uba, tip_prof, indi, num, den, None, '', '', None, self.data_ext, self.today))
                        elif indi in ('CONT0002', 'EQADULTS', 'ACC5D', 'ESIAP_SINTETIC'):
                            mmin, mmax, mint = 0,0,0
                            if indi == 'EQADULTS':
                                if (up, uba, tipus) in self.metes_sintetic:
                                    resultat = num
                                    mmin = self.metes_sintetic[(up, uba, tipus)]['MMIN']
                                    mmax = self.metes_sintetic[(up, uba, tipus)]['MMAX']
                                    mitjana = self.metes_sintetic[(up, uba, tipus)]['MINT']
                                    situacio = ''
                                    if resultat >= mmax: situacio = '4'
                                    if resultat >= mitjana and resultat < mmax: situacio = '3'
                                    if resultat >= mmin and resultat < mitjana: situacio = '2'
                                    if resultat < mmin: situacio = '1'
                                    self.upload.add((up, uba, tip_prof, indi, num, 1000, None, '', situacio, None, self.data_ext, self.today))
                            elif indi == 'ACC5D':
                                if den > 0:
                                    self.upload.add((up, uba, tip_prof, indi, num, den, None, '', '', 4, self.data_ext, self.today))
                            else:
                                if den > 0:
                                    self.upload.add((up, uba, tip_prof, indi, num, den, None, '', '', None, self.data_ext, self.today))
                else:
                    self.upload.add((up, uba, tip_prof, indi, num, den, None, '', '', self.TO_DO['ADM'][indi], self.data_ext, self.today))

        
        for (up, uba, tipus, indi, ordre), val in self.poblacionals.items():
            if uba and ordre != 0:
                if tipus == 'M': 
                    tip_prof = 'MG'
                elif tipus == 'I':
                    tip_prof = 'INF'
                elif tipus == 'A':
                    tip_prof = 'UAS'
                else: tip_prof = ''
                den = self.poblacionals[(up, uba, tipus, 'POBASS', 1)]
                if indi == 'POBASS':
                    self.upload.add((up, uba, tip_prof, indi, val, 1, None, '', '', ordre, self.data_ext, self.today))
                else:
                    self.upload.add((up, uba, tip_prof, indi, val, den, None, '', '', ordre, self.data_ext, self.today))
        
        # imputem 0s el valor dels indicadors de les ubas que no tenen algun indicador
        for (up, uba, tipus) in self.ubas_incloses:
            if uba:
                if tipus == 'M': 
                    tip_prof = 'MG'
                elif tipus == 'I':
                    tip_prof = 'INF'
                elif tipus == 'A':
                    tip_prof = 'UAS'
                else: tip_prof = ''
                if tip_prof != 'UAS':
                    if (up, uba, tipus) in self.indicadors_uba_fets:
                        indicadors_fets = self.indicadors_uba_fets[(up, uba, tipus)]
                        for indicador in self.TO_DO['SAN']:
                            if indicador not in indicadors_fets and indicador != 'POBASS80M':
                                ordre = self.TO_DO['SAN'][indicador]
                                if ordre != 0:
                                    self.upload.add((up, uba, tip_prof, indicador, 0, 0, None, '', '', ordre, self.data_ext, self.today))
                    else:
                        for indicador in self.TO_DO['SAN']:
                            ordre = self.TO_DO['SAN'][indicador]
                            if ordre != 0:
                                self.upload.add((up, uba, tip_prof, indicador, 0, 0, None, '', '', ordre, self.data_ext, self.today))
                elif tip_prof == 'UAS':
                    if (up, uba, tipus) in self.indicadors_uba_fets:
                        indicadors_fets = self.indicadors_uba_fets[(up, uba, tipus)]
                        for indicador in self.TO_DO['ADM']:
                            if indicador not in indicadors_fets:
                                ordre = self.TO_DO['ADM'][indicador]
                                if ordre != 0:
                                    self.upload.add((up, uba, tip_prof, indicador, 0, 0, None, '', '', ordre, self.data_ext, self.today))
                    else:
                        for indicador in self.TO_DO['ADM']:
                            ordre = self.TO_DO['ADM'][indicador]
                            if ordre != 0:
                                self.upload.add((up, uba, tip_prof, indicador, 0, 0, None, '', '', ordre, self.data_ext, self.today))

    def uploading(self):
        print('uploading data')
        self.upload = list(self.upload)
        u.listToTable(self.upload, 'proves_landing_uba', 'eqa_ind')
        print(self.upload[0])
        u.listToTable(self.upload, 'dwlanding.INDICADORS_uba_tr', 'exadata_pre')
        u.execute("""begin
                    delete from dwlanding.INDICADORS_uba WHERE INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                            'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002',
                            'ECONS0008', 'ACC5D', 'POBASS80M')
                            OR indicador LIKE 'EQ%' OR INDICADOR LIKE 'ADM%'
                            OR indicador like 'ESIAP%';
                    insert into dwlanding.INDICADORS_uba select DISTINCT * from dwlanding.INDICADORS_uba_tr
                            WHERE (INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                            'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002',
                            'ECONS0008', 'ACC5D', 'POBASS80M')
                            OR indicador LIKE 'EQ%' OR INDICADOR LIKE 'ADM%'
                            OR indicador like 'ESIAP%')
                            and INDICADOR IN (SELECT INDICADOR  FROM DWLANDING.INDICADORS);
                    commit;
                    end;""", 'exadata_pre')



class Landing_UP_Global():
    def __init__(self):
        sql = """DELETE FROM dwlanding.INDICADORS_up_tr
                WHERE INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002',
                'ECONS0008', 'ACC5D', 'POBASS80M', 'AGAGENDQC1', 'AGACC5DF')
                OR indicador LIKE 'EQ%' OR INDICADOR LIKE 'ADM%'"""
        u.execute(sql, 'exadata_pre')
        sql = """DELETE FROM dwlanding.INDICADORS_global_tr
                WHERE INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002',
                'ECONS0008', 'ACC5D', 'POBASS80M', 'AGAGENDQC1', 'AGACC5DF')
                OR indicador LIKE 'EQ%' OR INDICADOR LIKE 'ADM%'"""
        u.execute(sql, 'exadata_pre')
        sql = """select indicador, meta, valor 
                    from eqa_ind.eqa_metesres_subind_anual
                    where z4 = 'NOINSAT'"""
        self.metes = c.defaultdict(dict)
        for indi, meta, valor in u.getAll(sql, 'eqa_ind'):
            self.metes[indi][meta] = valor 
        self.cat_inversos = {}
        sql = """select indicador, invers from eqa_ind.exp_ecap_cataleg"""
        for indicador, invers in u.getAll(sql, 'eqa_ind'):
            self.cat_inversos[indicador] = invers
        self.TO_DO = c.defaultdict(dict)
        sql = """SELECT indicador, ordre_sanitari, ordre_adm 
                FROM DWLANDING.indicadors 
                WHERE responsable = 'SISAP'
                """
        for indicador, ordre_san, ordre_adm in u.getAll(sql, 'exadata_pre'):
            self.TO_DO['SAN'][indicador] = ordre_san
            self.TO_DO['ADM'][indicador] = ordre_adm
        
        self.create_up()
        self.create_global()
    
    def create_up(self):
        print('up')
        upload = []
        sql = """SELECT up, servei, indicador, sum(numerador), sum(denominador), data_calcul, data_carrega
                    FROM dwlanding.INDICADORS_uba_tr
                    where indicador != 'EQADULTS'
                    and (INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                    'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002',
                    'ECONS0008', 'ACC5D', 'POBASS80M', 'ADM004')
                    OR indicador LIKE 'EQ%')
                    GROUP BY up, servei, indicador, data_calcul, data_carrega"""
        for up, servei, indicador, num, den, data_cal, data_carr in u.getAll(sql, 'exadata_pre'):
            ordre = None
            if indicador in ('POBASS', 'POBASSAT', 'APMACA', 'APPCC', 'MA0009', 'MA0006', 'POBASS65M'):
                if indicador == 'POBASS': ordre = 1
                if indicador == 'POBASSAT': ordre = 2
                if indicador == 'POBASS65M': ordre = 3
                if indicador == 'APMACA': ordre = 4
                if indicador == 'APPCC': ordre = 5
                if indicador == 'MA0009': ordre = 6
                if indicador == 'MA0006': ordre = 7
            if indicador in self.metes:
                mmitja, mmax, mmin = 0, 0, 0
                for meta, valor in self.metes[indicador].items():
                    if meta == 'AGMINTRES': mmitja = valor
                    elif meta == 'AGMMAXRES': mmax = valor
                    elif meta == 'AGMMINRES': mmin = valor
                if indicador in self.cat_inversos:
                    if self.cat_inversos[indicador] == 1:
                        if num >= mmax: situacio = '1'
                        if num >= mmitja and num < mmax: situacio = '2'
                        if num >= mmin and num < mmitja: situacio = '3'
                        if num < mmin: situacio = '4'
                    else: 
                        if num >= mmax: situacio = '4'
                        if num >= mmitja and num < mmax: situacio = '3'
                        if num >= mmin and num < mmitja: situacio = '2'
                        if num < mmin: situacio = '1'
                upload.append((up, servei, indicador, num, den, None, None, situacio, ordre, data_cal, data_carr, None, None, None))
            else:
                upload.append((up, servei, indicador, num, den, None, None, '', ordre, data_cal, data_carr, None, None, None))   
        
        # Indicadors accessibilitat
        dades = c.defaultdict(dict)
        sql = """select INDICADOR, scs_codi, analisi, sum(val) 
                    from ALTRES.agendes_anual aa inner join nodrizas.cat_centres cc
                    on aa.br = cc.ics_codi 
                    where indicador = 'AGENDQC1'
                    group by INDICADOR, scs_codi, analisi"""
        for indi, up, analisi, n in u.getAll(sql, 'nodrizas'):
            dades[('AGAGENDQC1', up)][analisi] = n

        sql = """SELECT D0, scs_codi, D3, SUM(N) 
                from ALTRES.exp_khalix_forats aa 
                inner join nodrizas.cat_centres cc
                on substr(aa.D2,1,5) = cc.ics_codi 
                where D0 = 'ACC5DF'
                and length(d2) = 5
                group by D0, scs_codi, D3"""
        for indi, up, analisi, n in u.getAll(sql, 'nodrizas'):
            dades[('AGACC5DF', up)][analisi] = n
        
        # Indicadors administratius
        sql = """select indicador, scs_codi, analisi, SUM(value) 
                from gad.indicadors_adm aa  inner join nodrizas.cat_centres cc
                on aa.br = cc.ics_codi 
                where indicador = 'ADM003A'   
                group by indicador, scs_codi, analisi"""
        for indi, up, analisi, n in u.getAll(sql, 'nodrizas'):
            dades[('ADM003A', up)][analisi] = n

        br_2_up = {}
        sql = """select scs_codi, ics_codi from nodrizas.cat_centres"""
        for up, br in u.getAll(sql, 'nodrizas'):
            br_2_up[br] = up

        metes = c.defaultdict(dict)
        queries = ["ACC5DF;DEF2024;NOENT;AGMMIN,AGMMAX;NOCAT;TIPPROF;DIM6SET",
                    "AGAGENDQC1;DEF2023;AMBITOS###;AGMMIN,AGMMAX;NOCAT;NOIMP;DIM6SET"]
        for query in queries:
            for ind, pe, ent, meta, nocat, tip, dim, val in u.LvClient().query(query):
                if ent in br_2_up:
                    up = br_2_up[ent]
                    metes[(ind, up)][meta] = val
                elif ind == 'ACC5DF' and ent == 'NOENT':
                    metes[('AGACC5DF', ent)][meta] = val

        for (indi, up), vals in dades.items():
            num, den = 0, 0
            for tipus, n in vals.items():
                if tipus == 'NUM': num = n
                if tipus == 'DEN': den = n
            mmax, mmin, mint = 0,0,0
            if (indi, up) in metes:
                mmax = float(metes[(indi, up)]['AGMMAX'])
                mmin = float(metes[(indi, up)]['AGMMIN'])
                mint = (mmax+mmin)/float(2)
            elif (indi, 'NOENT') in metes:
                mmax = float(metes[(indi, 'NOENT')]['AGMMAX'])
                mmin = float(metes[(indi, 'NOENT')]['AGMMIN'])
                mint = (mmax+mmin)/float(2)
            upload.append((up, 'UAS', indi, num, den, None, None, '', self.TO_DO['ADM'][indi], data_cal, data_carr, mmin, mint, mmax))
            print((up, 'UAS', indi, num, den, None, None, '', self.TO_DO['ADM'][indi], data_cal, data_carr, mmin, mint, mmax))

        u.listToTable(upload, 'dwlanding.INDICADORS_up_tr', 'exadata_pre')
        u.execute("""begin
                    delete from dwlanding.INDICADORS_up WHERE INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                            'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002', 
                            'ECONS0008', 'ACC5D', 'POBASS80M', 'AGACC5DF', 'AGAGENDQC1')
                            OR indicador LIKE 'EQ%' OR INDICADOR LIKE 'ADM%';
                    insert into dwlanding.INDICADORS_up select distinct * from dwlanding.INDICADORS_up_tr
                            WHERE (INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                            'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002',
                            'ECONS0008', 'ACC5D', 'POBASS80M', 'AGACC5DF', 'AGAGENDQC1')
                            OR indicador LIKE 'EQ%' OR INDICADOR LIKE 'ADM%')
                            and INDICADOR IN (SELECT INDICADOR  FROM DWLANDING.INDICADORS);
                    commit;
                    end;""", 'exadata_pre')
    
    def create_global(self):
        print('global')
        upload = []
        sql = """SELECT servei, indicador, sum(numerador), sum(denominador), ordre, data_calcul, data_carrega
                    FROM dwlanding.INDICADORS_up_tr
                    WHERE INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                    'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002',
                    'ECONS0008', 'ACC5D', 'POBASS80M', 'AGACC5DF', 'AGAGENDQC1')
                    OR indicador LIKE 'EQ%' OR INDICADOR LIKE 'ADM%'
                    GROUP BY servei, indicador, data_calcul, data_carrega, ordre
                    """

        for servei, indicador, num, den, ordre, data_cal, data_carr in u.getAll(sql, 'exadata_pre'):
            if indicador in self.metes:
                mmitja, mmax, mmin = 0, 0, 0
                for meta, valor in self.metes[indicador].items():
                    if meta == 'AGMINTRES': mmitja = valor
                    elif meta == 'AGMMAXRES': mmax = valor
                    elif meta == 'AGMMINRES': mmin = valor
                if indicador in self.cat_inversos:
                    if self.cat_inversos[indicador] == 1:
                        if num >= mmax: situacio = '1'
                        if num >= mmitja and num < mmax: situacio = '2'
                        if num >= mmin and num < mmitja: situacio = '3'
                        if num < mmin: situacio = '4'
                    else: 
                        if num >= mmax: situacio = '4'
                        if num >= mmitja and num < mmax: situacio = '3'
                        if num >= mmin and num < mmitja: situacio = '2'
                        if num < mmin: situacio = '1'
                upload.append((servei, indicador, num, den, None, None, situacio, ordre, data_cal, data_carr))
            else:
                upload.append((servei, indicador, num, den, None, None, '', ordre, data_cal, data_carr))      
        u.listToTable(upload, 'dwlanding.INDICADORS_global_tr', 'exadata_pre')
        u.execute("""begin
                    delete from dwlanding.INDICADORS_global WHERE INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                            'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002', 'POBASS80M',
                            'ECONS0008', 'ACC5D')
                            OR indicador LIKE 'EQ%' OR INDICADOR LIKE 'ADM%';
                    insert into dwlanding.INDICADORS_global select * from dwlanding.INDICADORS_global_tr
                            WHERE (INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                            'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002',
                            'ECONS0008', 'ACC5D', 'POBASS80M')
                            OR indicador LIKE 'EQ%' OR INDICADOR LIKE 'ADM%')
                            and INDICADOR IN (SELECT INDICADOR  FROM DWLANDING.INDICADORS);
                    commit;
                    end;""", 'exadata_pre')

if __name__ == "__main__":
    Landing_UBA()
    Landing_UP_Global()
    sql = """UPDATE dwlanding.INDICADORS_uba_tr uba SET situacio = 'mitjana' WHERE indicador = 'ECONS0008'"""
    u.execute(sql, 'exadata_pre')
    sql = """UPDATE dwlanding.INDICADORS_uba_tr uba SET situacio = 'sobre' WHERE indicador = 'ECONS0008' AND 
                denominador > 0 and 
                (numerador/denominador) > ( SELECT (numerador/denominador + (numerador/denominador * 10/100))
                FROM dwlanding.INDICADORS_UP_tr up WHERE up.up = uba.up AND up.indicador = uba.indicador
                AND up.servei = uba.servei)"""
    u.execute(sql, 'exadata_pre')
    sql = """UPDATE dwlanding.INDICADORS_UBA_tr uba SET situacio = 'sota' WHERE indicador = 'ECONS0008'
                AND denominador > 0 and 
                (numerador/denominador) < ( SELECT (numerador/denominador + (numerador/denominador * 10/100))
                FROM dwlanding.INDICADORS_UP_tr up WHERE up.up = uba.up AND up.indicador = uba.indicador
                AND up.servei = uba.servei)"""
    u.execute(sql, 'exadata_pre')
    u.execute("""begin
                    delete from dwlanding.INDICADORS_uba WHERE INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                            'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002',
                            'ECONS0008', 'ACC5D', 'POBASS80M')
                            OR indicador LIKE 'EQ%' OR INDICADOR LIKE 'ADM%';
                    insert into dwlanding.INDICADORS_uba select * from dwlanding.INDICADORS_uba_tr
                            WHERE (INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                            'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002',
                            'ECONS0008', 'ACC5D', 'POBASS80M')
                            OR indicador LIKE 'EQ%' OR INDICADOR LIKE 'ADM%')
                            and INDICADOR IN (SELECT INDICADOR  FROM DWLANDING.INDICADORS);
                    commit;
                    end;""", 'exadata_pre')
    u.execute("""begin
                    delete from dwlanding.INDICADORS_global WHERE INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                            'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002',
                            'ECONS0008', 'ACC5D', 'POBASS80M')
                            OR indicador LIKE 'EQ%' OR INDICADOR LIKE 'ADM%';
                    insert into dwlanding.INDICADORS_global select * from dwlanding.INDICADORS_global_tr
                            WHERE (INDICADOR IN ('POBASS', 'POBASSAT', 'APMACA', 
                            'APPCC', 'MA0009', 'MA0006', 'POBASS65M', 'CONT0002',
                            'ECONS0008', 'ACC5D', 'POBASS80M')
                            OR indicador LIKE 'EQ%' OR INDICADOR LIKE 'ADM%')
                            and INDICADOR IN (SELECT INDICADOR  FROM DWLANDING.INDICADORS);
                    commit;
                    end;""", 'exadata_pre')

