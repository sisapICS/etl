# coding: latin1

"""
.
"""

import shared as s
import sisapUtils as u
import collections as c


sql = "select indicador, 'Aperiodo', ics_codi, analisi, \
              'NOCAT', 'NOIMP', 'DIM6SET', 'N', sum(valor) \
       from {}.{} a \
       inner join nodrizas.cat_centres b on a.up = b.scs_codi \
       where tip_eap = 'M' or \
             (tip_eap = 'A' and indicador not like 'ESIAPP%') or \
             (tip_eap = 'N' and indicador like 'ESIAPP%') \
       group by indicador, ics_codi, analisi"
u.exportKhalix(sql.format(s.DATABASE, s.TABLE), s.FILE_UP)


sql = "select indicador, 'Aperiodo', concat(ics_codi, 'I', a.uba), analisi, \
              'NOCAT', 'NOIMP', 'DIM6SET', 'N', valor \
       from {0}.{1} a \
       inner join nodrizas.cat_centres b on a.up = b.scs_codi \
       inner join eqa_ind.mst_ubas c on a.up = c.up and a.uba = c.uba \
       where indicador not like 'ESIAPP%' and \
             c.tipus = 'I' \
       union \
       select indicador, 'Aperiodo', concat(ics_codi, 'I', a.uba), analisi, \
              'NOCAT', 'NOIMP', 'DIM6SET', 'N', valor \
       from {0}.{1} a \
       inner join nodrizas.cat_centres b on a.up = b.scs_codi \
       inner join pedia.mst_ubas c on a.up = c.up and a.uba = c.uba \
       where indicador like 'ESIAPP%' and \
             c.tipus = 'I' \
       union \
       select indicador, 'Aperiodo', concat(ics_codi, 'M', a.uba), analisi, \
              'NOCAT', 'NOIMP', 'DIM6SET', 'N', valor \
       from {0}.{2} a \
       inner join nodrizas.cat_centres b on a.up = b.scs_codi \
       inner join eqa_ind.mst_ubas c on a.up = c.up and a.uba = c.uba \
       where indicador not like 'ESIAPP%' and \
             c.tipus = 'M' \
       union \
       select indicador, 'Aperiodo', concat(ics_codi, 'M', a.uba), analisi, \
              'NOCAT', 'NOIMP', 'DIM6SET', 'N', valor \
       from {0}.{2} a \
       inner join nodrizas.cat_centres b on a.up = b.scs_codi \
       inner join pedia.mst_ubas c on a.up = c.up and a.uba = c.uba \
       where indicador like 'ESIAPP%' and \
             c.tipus = 'M'"
u.exportKhalix(sql.format(s.DATABASE, s.TABLE, s.TABLE_MED), s.FILE_UBA)

# taules per landing infermeria
file = u.baseFolder + ["22esiap/dades_noesb", "ESIAP_cataleg.txt"]
file = "/".join(file)
cataleg = []
for codi, desc, pare, invers in u.readCSV(file, sep="@"):
      cataleg.append((codi, desc, pare, invers))

u.listToTable(cataleg, s.TABLE_CATALEG, s.DATABASE)

sql = """
      SELECT indicador, up, uba, analisi, valor
      FROM exp_khalix
      WHERE uba <> ''
"""
dades = c.defaultdict(dict)
for ind, up, uba, analisi, val in u.getAll(sql, s.DATABASE):
      dades[(ind, up, uba)][analisi] = val

upload = []
for (ind, up, uba), valors in dades.items():
      num = valors['NUM'] if 'NUM' in valors else 0
      den = valors['DEN']
      upload.append((ind, up, uba, num, den))

cols = "(indicador varchar(15), up varchar(10), uba varchar(10), num int, den int)"
u.createTable(s.TABLE_LANDING, cols, s.DATABASE, rm =True)
u.listToTable(upload, s.TABLE_LANDING, s.DATABASE)