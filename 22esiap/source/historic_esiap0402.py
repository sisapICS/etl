# coding: latin1

"""
.
"""
from dateutil.relativedelta import relativedelta
import collections as c

import datetime
import sisaptools as u
import sisapUtils as t

TABLE = "exp_khalix_historic"
TABLE_MED = TABLE + "_med"
DATABASE = "esiap"
FILE_UP = "ESIAP_historic"
FILE_UBA = "ESIAP_UBA_historic"


class Preventives(object):
    """."""

    def __init__(self, data, ):
        """."""
        self.date = data
        self.cribrats = c.defaultdict(set)
        self.cribrables = c.defaultdict(set)
        self.get_edats()
        self.get_tabac()
        self.get_resultat()
        self.export()
    
    def get_edats(self):
        self.edats = {}
        sql = """select id_cip_sec, data_naix from nodrizas.assignada_tot"""
        for id, naix in u.Database("p2262", "nodrizas").get_all(sql):
            edat = relativedelta(naix, self.date).years
            self.edats[id] = edat

    def get_tabac(self):
        """."""
        # ESIAP0402 Darrers 24 mesos, si t� ? 25 anys o consta que en algun moment hagi estat fumador
        # Nom�s 1 sol registre, si t� >25 anys i tots els registres indiquen que mai ha sigut fumador.
        fumador_ex_fumador = set()
        no_fum = set()
        data_format = str(self.date.year) + '-' + str(self.date.month) + '-' + str(self.date.day)
        sql = """select id_cip_sec, tab
                    from nodrizas.eqa_tabac
                    where dalta < date '{d}'""".format(d=data_format)
        for id, tab in u.Database("p2262", "nodrizas").get_all(sql):
            if tab in (1,2):
                fumador_ex_fumador.add(id)
            else:
                no_fum.add(id)
        for id in no_fum:
            if id in self.edats:
                edat = self.edats[id]
                if edat > 25 and id not in fumador_ex_fumador:
                    self.cribrats["ESIAP0402"].add(id)
        sql = "select id_cip_sec, tab \
               from eqa_tabac \
               where last = 1 and \
                     dlast between adddate(date '{d}', interval -2 year) and \
                                   date '{d}'".format(d=data_format)
        for id, tab in u.Database("p2262", "nodrizas").get_all(sql):
            if id in self.edats:
                edat = self.edats[id]
                if edat <= 25 or id in fumador_ex_fumador:
                    self.cribrats["ESIAP0402"].add(id)

    def get_resultat(self):
        """."""
        self.resultat = c.Counter()
        self.medicina = c.Counter()
        sql = "select id_cip_sec, up, ubainf, uba, codi_sector \
               from assignada_tot \
               where edat between 15 and 60 and \
                     ates = 1"
        for id, up, uba, med, sector in u.Database("p2262", "nodrizas").get_all(sql):
            for ind in self.cribrats:
                if (ind not in self.cribrables or id in self.cribrables[ind]):
                    self.resultat[(ind, up, uba, "DEN")] += 1
                    self.medicina[(ind, up, med, "DEN")] += 1
                    if id in self.cribrats[ind]:
                        self.resultat[(ind, up, uba, "NUM")] += 1
                        self.medicina[(ind, up, med, "NUM")] += 1

    def export(self):
        """."""
        month_2dig = '0'+str(self.date.month) if len(str(self.date.month)) == 1 else str(self.date.month)
        peiode = 'A' + str(self.date.year)[2:] + month_2dig
        with u.Database("p2262", "essiap") as nod:
            upload = [(periode,) + k + (v,) for (k, v) in self.resultat.items()]
            nod.list_to_table(upload, TABLE, DATABASE)
            upload_m = [(periode,) + k + (v,) for (k, v) in self.medicina.items()]
            nod.list_to_table(upload_m, TABLE_MED, DATABASE)


def to_khalix_hist():
    sql = "select indicador, concat(concat(substr(periode,1,3), '0'), substr(periode,4,1)), ics_codi, analisi, \
            'NOCAT', 'NOIMP', 'DIM6SET', 'N', sum(valor) \
        from {}.{} a \
        inner join nodrizas.cat_centres b on a.up = b.scs_codi \
        where tip_eap = 'M' or \
                (tip_eap = 'A' and indicador not like 'ESIAPP%') or \
                (tip_eap = 'N' and indicador like 'ESIAPP%') \
        group by indicador, concat(concat(substr(periode,1,3), '0'), substr(periode,4,1)), ics_codi, analisi"
    t.exportKhalix(sql.format(DATABASE, TABLE), FILE_UP)


    sql = "select indicador, concat(concat(substr(periode,1,3), '0'), substr(periode,4,1)), concat(ics_codi, 'I', a.uba), analisi, \
                'NOCAT', 'NOIMP', 'DIM6SET', 'N', valor \
        from {0}.{1} a \
        inner join nodrizas.cat_centres b on a.up = b.scs_codi \
        inner join eqa_ind.mst_ubas c on a.up = c.up and a.uba = c.uba \
        where indicador not like 'ESIAPP%' and \
                c.tipus = 'I' \
        union \
        select indicador, concat(concat(substr(periode,1,3), '0'), substr(periode,4,1)), concat(ics_codi, 'M', a.uba), analisi, \
                'NOCAT', 'NOIMP', 'DIM6SET', 'N', valor \
        from {0}.{2} a \
        inner join nodrizas.cat_centres b on a.up = b.scs_codi \
        inner join eqa_ind.mst_ubas c on a.up = c.up and a.uba = c.uba \
        where indicador not like 'ESIAPP%' and \
                c.tipus = 'M'"
    t.exportKhalix(sql.format(DATABASE, TABLE, TABLE_MED), FILE_UBA)


if __name__ == "__main__":
    cols = ("periode varchar(10)", "indicador varchar(15)", "up varchar(10)", "uba varchar(10)",
        "analisi varchar(15)", "valor decimal(10, 2)")
    for table in (TABLE, TABLE_MED):
        with u.Database("p2262", "essiap") as nod:
            nod.create_table(table, cols, remove=False)
    for date in (datetime.date(2023, 1, 31),
                datetime.date(2023, 2, 28),
                datetime.date(2023, 3, 31),
                datetime.date(2023, 4, 30),
                datetime.date(2023, 5, 31),
                datetime.date(2023, 6, 30)):
        print(date)
        Preventives(date)
    to_khalix_hist()
