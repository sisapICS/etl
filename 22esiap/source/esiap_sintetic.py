import sisapUtils as u
import sisaptools as t
import collections as c
import datetime as d
import shared as s

# punts -> ESIAP0406{DEF2025{NOENT{PUNTSMAX{NOCAT{NOIMP{DIM6SET{4.000000000
# metes -> ESIAP0201{DEF2025{NOENT{AGMMIN{NOCAT{NOIMP{DIM6SET{399.000000000

punts = {}
file = u.baseFolder + ["22esiap/dades_noesb", "ESIAP_punts.txt"]
file = "/".join(file)
for row in u.readCSV(file, sep='{'):
    punts[row[0]] = row[-1]

metes = c.defaultdict(dict)
file = u.baseFolder + ["22esiap/dades_noesb", "ESIAP_metes.txt"]
file = "/".join(file)
for row in u.readCSV(file, sep='{'):
    metes[row[0]][row[3]] = row[-1]

sql = """
    SELECT indicador, up, uba, analisi, valor
    FROM exp_khalix
    WHERE uba <> ''
"""
dades = c.defaultdict(dict)
for ind, up, uba, analisi, val in u.getAll(sql, 'esiap'):
    dades[(ind, up, uba)][analisi] = val

taxes = ['ESIAP0201', 'ESIAP0202', 'ESIAP0602', 'ESIAPP0201', 'ESIAPP0202', 'ESIAPP0602']

sintetic = []

for (ind, up, uba), analisi in dades.items():
    if ind in punts and ind in metes and ind not in taxes:
        num = analisi['NUM'] if 'NUM' in analisi else 0
        den = analisi['DEN']
        res = float(num)*100/float(den) if den != 0 else 0
        pond = float(punts[ind])
        mmin = float(metes[ind]['AGMMIN'])
        mint = float(metes[ind]['AGMINT'])
        mmax = float(metes[ind]['AGMMAX'])
        punt = 0
        if res >= mmax: punt = pond
        if mmin < res < mmax:
            punt = pond * float(res - mmin)/float(mmax-mmin)
        sintetic.append((ind, up, uba, punt))

cols = "(indicador varchar(10), up varchar(10), uba varchar(10), punts double)"
u.createTable(s.TABLE_SINTETIC, cols, 'esiap', rm=True)
u.listToTable(sintetic, s.TABLE_SINTETIC, 'esiap')
