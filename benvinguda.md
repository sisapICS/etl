# BENVINGUDA AL SISAP, N�RIA!

En aquest document de benvinguda hi he incorporat tot all� que m'hagu�s agradat tenir i saber el primer dia que vaig aterrar aqu�. Per tant, espero que aquest docuemnt et serveixi de consulta per tal que la teva arribada sigui el m�s smooth possible (no obstant, pregunta TOT le que necessitis i quan vulguis:))). 
Aix� doncs, a continuaci� et far� un resum de tot el que crec que necessitar�s per treballar a l'equip d'explotaci� i una mica de documentaci� de taules, llibreries que utilitzem, processos interns...

## ESTRUCTURA DEL SISAP I FUNCIONAMENT

Abans de res per�, una mica d'hist�ria i cultura sisapera.

El SISAP (Sistema d�Informaci� dels Serveis d�Atenci� Prim�ria) va fer els primers passos l�any 2006, amb l�objectiu de proporcionar informaci�, principalment per a la gesti� cl�nica, als diferents professionals i a les estructures de gesti�. Proporcionar informaci� significa obtenir dades de diferents fonts i processar-les per construir indicadors. En el moment de publicar-se, aquests han de ser �tils per als professionals assistencials, per als gestors o per a tots dos.

Internament ens organitzem en 4 equips:
- DEFINICI�: Detecten els problemes i pensen solucions que defineixen i ens demanen implementar. Cap: Mireia F�bregas. A l'equip de definici� hi ha molta altra gent que anir�s coneixent. No obstant, has de con�ixer la Merc� Bustos a qui li haur�s de demanar dies de teletreball, vacances, material que et falti...
- EXPLOTACI�: Fem explotaci� de dades: creem bases de dades, indicadors, optimitzem processos... i ajudem en tot el que podem. Cap: Francesc Fina. Altres: Alejandro i una servidora.
- VISUALITZACI�: Fan pantalles per mostrar tota la informaci� que generem als professionals de la salut. Cap: Albert Mercader. Aquest grup est� format per 4 persones i totes tenen unes funcions diferents. Joan: s'encarrega de SISAP-ECAP (informaci� actualiyzada de forma setmanal). Bel�n: s'encarrega de LongView (informaci� mensualment). D�dac: crea pantalles.
- AN�LISI: Com el seu nom indica analitzen dades, publiquen articles... Cap: Ermengol Coma. Altres: Edu, N�ria i Leo.



## SET-UP DE PROGRAMARI
### BASES DE DADES
Per tal de treballar amb les BBDD fem servir l'aplicaci� de programari client de SQL DBeaver. Pots instal�lar-ho des de https://dbeaver.io/download/. 
Adjunt a aquest document hi ha un arxiu TNSNAMES amb totes les connexions que podr�s necessitar. Pel que fa usuaris i contrasenyes, pots demanar-ho a qualsevol de l'equip o a mi sempre que ho necessitis:)


## INTRODUCCI� A LES BBDD

### QUINES DADES TENIM?
Principalment treballem amb les dades de l'atenci� prim�ria. Aquestes estan guardades en diferents llocs. Principalment hi ha:
- SECTORS: 21 m�quines organitzades per territori on es bolquen totes les dades de la regi� en q�esti�. NOTA: tenim una connexi� per cada sector ja que cadascun es tradueix en una base de dades diferent. No obstant, la terminologia de les taules en tots ells �s la mateixa. Aix� doncs, per obtenir per exemple el nom i n�mero de tel�fon de tots els habitants de Catalunya caldria accedir i importar-se la informaci� de tots els sectors. Aqu� hi ha una part a producci� a la que nosaltres nom�s accedim en casos puntuals quan �s estrictament necessari. Aix� doncs, treballem sempre amb les dades d'explotaci� que s'actualitzen de forma setmanal. (ORACLE)
- REDICS: una �nica base de dades que cont� informaci� de tots els sectors. (ORACLE)
- EXADATA: base de dades amb informaci� de tota la poblaci�, molt gran i amb motor de cerca gran al radera (ORACLE)
- NODRIZAS: base de dades que cuinem des del SISAP, �s a dir, les dades aqu� dintre han passat majorit�rimanet un proc�s de transformaci�. A aqu� tenim total control de les taules ja que en som els propietaris i els que hi donem suport. Aquesta base de dades est� a un servidor de MariaDB que t� capacitats limitades (tant de c�lcum com de mem�ria). Per tant, agra�m que tot el que hi fem sigui �ptim i ocupi el m�nim possible, �s a dir, que sigui eficient tant computacionalment com en termes de mem�ria.

#### IDENTIFICADORS DE PACIENTS
Les dades amb les que treballem s�n pseudoanonimitzades (s�n reals, per� se'ls ha encriptat el seu identificador en alguns casos) i cada base de dades t� identificadors diferents. Hi ha diferents taules que permeten fer aquestes conversions. 

Quins identificadors hi ha a cada base de dades?
- REDICS: HASH DE REDICS = HASH256(CIP)
- EXADATA: HASH D'EXADATA = HASH1(CIP) / NIA / CIP DE 13 I DE 14 / RCA
- SECTORS: CIP DE 13
- NODRIZAS: id_cip i id_cip_sec. Id_cip �s �nic per pacient, id_cip_sec �s �nic per pacient i sector (m�quina) i s'actualitza cada vegada que fem el c�lcul des del SISAP, �s a dir, cada cap de setmana aproximadament. Molt important tenir en compte doncs que aquests identificadors canviaran d'una setmana a l'altra i no es podran desencriptar despr�s d'aquest temps.

Com passem d'un identificador a un altre?
- ID_CIP / ID_CIP_SEC a HASH REDICS --> import.u11 (NODRIZAS)
- HASH REDICS a CIP DE 13 --> pdp.pdptb101 (REDICS)
- HASH COVID a HASH REDICS a CIP DE 13 --> pdp.pdptb101_relacio (REDICS)
- HASH COVID a HASH REDICS --> pdptb101 (EXADATA)
- NIA a CIP DE 14 a HASH COVID --> dwsisap.rca_cip_nia (EXADATA)

### COM TROBAR LES DADES QUE BUSQUEM?
A continuaci� trobar�s una llista estil cheatsheet de les taules que m�s fem servir per tal que s�pigues on buscar la informaci� ja que hi ha molta "cultura popular" dintre del SISAP i moltes vegades la part m�s complicada de comen�ar una tasca �s saber d'on treure la informaci�. Aix� doncs, esperem que aix� et sigui d'all� m�s �til. No obstant, cada vegada que et demanin alguna cosa que no saps on buscar, pregunta'ns sense cap mena de problema. Jo fa dos anys quasi que treballo aqu� i encara pregunto molt sovint per noms de taules i contingut aix� que no et preocupis i que no et s�piga greu ni et faci cosa preguntar!

#### SECTORS
- usutb040 : tots els pacients. Considerem que un pacient est� aqu� si la seva situaci� �s A i t� una uba assignada
- vistb043 : visites
- PRITB000 : cat�leg de taules i les seves variables
- prstb337: visites
- prstb335: cat�leg malalties
- pritb992: DNI pacients
- labtb120: laboratori sectors
- prstb261: visites agudes
- prstb337: ares (codi visita + dates)
- prstb335: cat�leg cures (codi + nom + descripci�...)
- prstb015: problemes : problemes de salut x pacient, data, codi problema...

!!! ACCEDIR A LES BASES DE DADES DE SECTORS �S MOLT LENT (b�sicament perqu� hi accedim per hash i no per id_cip). Hi ha taules a IMPORT que s�n equivalents a les de la info de sectors

#### NODRIZAS

Com descarreguem les taules a nodrizas?

Ho fem amb els scripts de 01down/proc de dataFather.csv i dataChild.csv.

L'estructura del dataFather �s la seg�ent:

- Id de la taula (�nic) 
- Nom de la taula a dest�
- Nom de la taula a origen
- Id d'on es descarrega, aquest pot ser:
    - 0 o 1 es descarrega de redics, el 0 baixa la taula tal qual i el 1 fa una taula intermedia particionada per sectors. La web de control ens permet veure quan tarda a crear la taula intermitja particionada abans de descarregar-la.
    - 2 es descarrega de Sidics (Sidics es fa els dimecres al migdia amb dades de sectors d'explotaci� que s'actualitzen entre dss i dg a la nit).
    - 3 the whole (Exadata) [de moment nom�s laboratori]
- valors entre 0 i 6, aplica nom�s a presons. 
    - 0: si no ho volem per presons
    - 2: Si ve de sidics
    - La resta s'han de mirar al generate.py de 01down/source per saber quina �s la select i contra qu� ho hem de descarregar.

L'estructura de dataChild �s la seg�ent:
- Id de la taula que volem descarregar
- Nom de la columna en origen
- Where: si no et vols baixar tota la taula, et filtra pel que li diguis de la columna en q�esti�

**Nota:** Si volem alguna taula nova a nodrizas o noves columnes... cal fer-ho abans de dimecres i fer push perqu� SIDICS ho llegir� dimecres al migdia (el que hi hagi pushejat).

##### IMPORT
(Desc�rregues de sectors)
- import.estats
- import.u11: **id_cip i id_cip_sec a hash**
- import.problemes: diagn�stics, malalties x id_cip_sec
- import.baixes: it's, motius baixes, duracio, inici, fi...

##### NODRIZAS
(Taules de import tractades)
- nodrizas.eqa_u11_with_jail: **tota la poblaci�**, id_cip, id_cip_sec, codi_sector i hash
- nodrizas.ass_embaras: **assist�ncia embar�s**
- nodrizas.assignada_tot: pacients assignats a un professional


##### ALTRES


#### EXADATA

##### M�STERS

**VISITES:**

afegir variables al master de visites no �s tan f�cil com hauria de ser:

1. Primer cal fer els canvis al codi seguint aquest exemple > https://git.intranet.gencat.cat/3567/dadessalut1/atencioprimaria/explotacio/nix/-/commit/66942bddeb06cfb34cd220f5c5d02fb23f1d5324

2. Despr�s cal crear la nova variable tan a SISAP_CORONAVIRUS_ACTIVITAT_F com a SISAP_CORONAVIRUS_ACTIVITAT_N

3. Finalment cal crear la nova variable a SISAP_MASTER_VISITES, per� ha de quedar situada abans de totes les calculades en el propi master. Aix� es va amb invisible/visible > Re-order columns of table in Oracle (https://stackoverflow.com/questions/4939735/re-order-columns-of-table-in-oracle)


#### REDICS
- usutb011: per **passar de hash vell a hash nou**
- SISAP_COVID_CAT_UP: **CAPs** catalunya, up, tipus, nom...
- REDICS.MD_POBLACIO_ACTIVA: **poblaci� activa**, cada persona on est� assignada...
- PRSTB261: **visites agudes infermeria**
- PRSTB335:
- visites1:
- prstb015: **visites amb motius i diagnostics** a pr_cod_ps
- DBS: **taula s�per completa per cada pacient**, info personal, problemes de salut...!!!!!
- prstb017: **dign�stics pacients**
- ppftb016: **pacients i f�rmacs** + info f+armacs com preu, unitats....


## LLIBRERIES QUE FEM SERVIR

Al SISAP hist�ricament s'han creat llibreries de Python per poder treballar amb les bases de dades que disposem de forma efica�. Les funcions que hi ha b�sicament serveixen per minimitzar el codi a fer ja que s�n funcions que fem servir a diari i repetides vegades.

Aquestes funcions les classificarem b�sicament en 3 blocs:
1. Per llegir dades
2. Per enviar dades a Khalix/LV (a la Bel�n) o a SFTPs
3. Per fer altres tasques com llegir csv, enviar mails autom�tics...

Hi ha dues llibreries que contenen totes aquestes funcions $sisapUtils$ i $sisaptools$. Al .68 hi viuen les dues mentre que al .84 nom�s hi ha carregada la de $sisaptools$. Aquesta darrera �s la m�s "nova".

A continuaci� et presento les funcions que m�s far�s servir de les dues. No obstant, aquestes no s�n totes i podr�s consultar-les als repositoris de Bitbucket que porten per nom el mateix que les llibreries.

### SisapUtils
```{python}
import sisapUtils as u
```

**Per llegir/escriure dades**

Per llegir una base de dades fem servir la seg�ent funci�:
```{python}
u.getAll(sql, database)
```
on sql �s un string de python amb una query en format sql i database �s una base de dades o esquema. Les database acceptades s�n: 'nodrizas', 'altres', 'import'... (tots els esquemes de nodrizas), 'redics' i 'exadata'.

Aquesta funci� ens retorna una tupla amb totes les variables que haguem seleccionat a la sql que li hem passat. NOTA: Si nom�s selecciones una columna a la query haur�s de fer alguna cosa de l'estil:
```{}
-- opci� 1
for columna, in u.getAll(sql, 'db'):
    TBD

-- opci� 2
for row in u.getAll(sql, 'db'):
    column = row[0]
    TBD
```
Altrament el resultat no ser� l'esperat ja que ser� una tupla amb un �nic valor.

Aix� tamb� ho podem fer amb un mutiproc�s:
```{python}
u.multiprocess(function, parameters, workers)
```
On:
- function : funci� que executes en multiproc�s
- parametres: llistat dels diferents par�metres que passes a cada funci�
- workers : nombre de workers, recomanable no pujar dels 8

Per rec�rrer tots els sectors es pot fer amb una variable global de la llibreria que retorna el nom de cada sector en format string:
```{python}
u.sectors
```

Per fer una select d'un �nic registre podem fer servir:
```{python}
u.getOne(sql, database)
```


Per executar sent�ncies sql des de python podem fer-ho amb aquesta llibreria de la seg�ent manera:
```{}
conn = u.connect('DB')  # ens connectem a la BD
c = conn.cursor()  # creem cursor
c.execute("SQL")  # executem SQL
conn.close()  # tanquem connexi�
```

Una altra manera de fer el mateix seria:
```{python}
u.execute(SQL, BD)
```

**Creaci� de taules**
```{python}
u.createTable(taula, sql, db, rm=False)
```
on:
- taula: string del nom de la taula que vols crear
- sql: pot contenir nom�s les columnes de la taula que volem crear amb els seus formats i altra informaci� extra com partitions. Exemples a continuaci�:
1. Nom�s columnes: sql = '(col1 varchar(x), col2 date, col3 int, ...., colN number(30,9))'
2. Columnes i particions: sql = '(col1 varchar(x),..., colN date) partition by list(particio) (partition p1 vaues in (1,2,3))'
- db : string de la base de dades
- rm : True si volem borrar i crear la taula si existia anteriorment, False si no volem crear la taula si ja existia. En cas que sigui la 2a opci� atenci� perqu� els valors que contenia no es borren i es fa com un insert del que li posem a continuaci�. NOTA: aquest par�metre �s opcional i per decte �s False.

Per donar acc�s a altres usuaris ho podem fer de la seg�ent manera:
```{python}
u.grantSelect(taula, ("usuari1", ..., "usuarin"), db)
```
on:
- usuari1 i usuarin s�n els usuaris als que donarem acc�s. 
Si nom�s volem donar acc�s a 1 usuari no cal posar-lo dintre de la tupla.

**Omplir taules**
```{python}
u.listToTable(upload, taula, db, partition="nom")
```
on:
- upload : llistat amb la informaci� que volem penjar. Per tant, �s un llistat de tuples. El nombre d'elements de cada tupla ha de coincidir amb el nombre de columnes de la taula. Exemple d'una taula amb 3 columnes on afegim 2 files: [(c1,c2,c3), (d1,d2,d3)]
- taula : string del nom de la taula que vols crear
- db : string de la base de dades
- partition : Per defecte no hi ha res i no �s obligatori passar-hi un valor. Serveix per si la teva taula est� particionada i vols afegir observacions a una partici� en concret.


**Per enviar dades a Khalix/LV (a la Bel�n) o a SFTPs**

Per enviar dades a Khalix/LV (Bel�n)
```{python}
u.exportKhalix(sql, file)
```
on:
- sql : query en format string de la select que volem enviar a Khalix
- file : nom del fitxer que rebran a Khalix

Observacions: Khalix �s el mateix que Longview (LV) i funciona amb dimensions, concretament t� 9 valor, 8 dimensions i un valor. Per ordre que han d'enviar-se s�n:
- INDICADOR: nom indicador
- PER�ODE: per�ode de c�lcul �s A + 2 �ltims d�gits any + 2 d�gits mes. Ex: A2306. Es pot posar $concat('A','periodo')$ a la query i aix� Khalix ho sap traduir al per�ode actual.
- ENTITY: UP en format BR si els indicadors s�n per equip. $BR+I/M+UBA$ si l'indicador �s per professional I per infermeria i M per medicina.
- AN�LISI: NUM/DEN/RES... quina part de l'indicador �s pot ser NUm (numerador), den (denominador), N (valor �nic), RES (resultat), PCT (percentatge)...
- NOCAT: edat (pot ser en grups de 5 anys, de 10, altres, el que demanin). Si no hi volem cap valor hi posarem NOCAT.
- NOIMP: poblaci�. Pot ser institucionalitzada i atesa, institucionalitzada i no atesa, assignada atesa i assignada no atesa. Si no hi volem cap valor hi posarem NOIMP.
- DIM6SET: sexe. Si no ho volem separar per sexe hi posarem DIM6SET
- N: posem N per defecte
- valor: valor de l'indicador per aquest creuament.
NOTA: el que cont� cada variable pot canviar depenent de la jerarquia de cada indicador.


Per enviar dades a SISAP-ECAP (Joan)
```{python}
u.exportPDP(query=sql, table=table, dat=True, truncate=False, datAny=True)
```
on:
- Query : sent�ncia sql que cont� el que volem enviar
- Table : taula de pdp on enviem les dades
- Dat : trunquem per la data actual i substitu�m el que hi havia amb el que carreguem ara
- DatAny : trunquem per l'any actual i substitu�m el que hi havia amb el que carreguem ara
- Truncate : per eliminar tot el que hi havia fins ara i que a l'acabar la c�rrega nom�s hi hagi el que afegim nosaltres ara


Per posar dades a una SFTP
```{python}
with u.SFTP(ubicacio) as sftp:  # accedim a la SFTP
    sftp.put(file, file_name)  # pugem l'arxiu
```
On:
- ubicacio : sftp on enviem l'arxiu, tenim configurada la nostra com a 'sisap' per exemple.
- file : arxiu que volem penjar
- file_name : nom que li volem posar a l'arxiu a la sftp

**Com afegim una nova sftp?**

Passos a seguir:
1. Obrim repo sisaptools que viu a X0002.
2. Editem fitxer services.json
3. La password es codifica entrant a un python, important sisaptools com u i fent u.aes.AESCipher().encrypt('contrasenya_a_codificar')
4. Editem fitxer setup.py, augmentem en 1 el n�mero de la versi�
5. Executem des de fora el contenidor: bash publish.sh 
6. Entrem al contenidor i actualitzem llibreria: pip install --upgrade sisaptools
7. Comprovem que vagi:)


**Per fer altres tasques com llegir csv, enviar mails autom�tics...**

Escriure txt o csv
```{python}
u.TextFile(file_name).write_iterable(values, ";", "\n")
```
On:
- file_name : nom de l'arxiu
- values : valors que escriurem
- ';' : separador, es pot posar el que es vulgui
- "\n" : salt de l�nia 

```{python}
u.writeCSV(file_name, [('col1', 'col2', ... , 'colN')] + upload, sep=";")
```
On:
- file_name : nom de l'arxiu
- [('col1', 'col2', ... , 'colN')] : nom de les columens del csv, el header si en volem
- upload : llistat de tuples a penjar
- sep : separador, es pot posar el que es vulgui

Llegir txt o csv
```{python}
u.readCSV(file_name, sep='@')
```
On:
- file_name : nom de l'arxiu
- sep : separador de l'arxiu, potser qualsevol. El que ens vingui de Khalix estar� habitualment amb @ o amb {.

Enviar mails
```{python}
mail = u.Mail()
mail.to.append(el_correu)
mail.subject = concepte
mail.text = cos
mail.send()
```
On:
- el_correu : correu que vols que ho rebi
- concepte : t�tol del correu
- cos : contingut del mail


```{python}
u.sendGeneral('SISAP <sisap@gencat.cat>',  # correu des del que s'envia el mail, sempre aquest
            (mail1, mail2),  # destinataris
            (mail3, mail4),  # destinataris en c�pia
            subject,  # t�tol
            text,  # missatge
            file)  # fitxer_adjunt
```

Per enviar-ho amb c�pia als "caps" del SISAP
```{python}
u.sendSISAP(SUBSCRIBERS, t�tol, nom, text)
```
On:
- SUBSCRIBERS : llistat de mails que rebran mail
- t�tol : cap�alera mail
- nom : nom destinatari
- text : cos del mail

Enviar-ho nom�s a qui posem
```{python}
u.sendPolite(to, subject, text, file=FILE)
```
On:
- to : a qui ho enviem
- subject : t�tol
- text : cos mail
- file : si hi ha fitxer adjunt, posar el nom d'aquest

**ALTRES**

Per mirar �s de recursos:
```{python}
>> u.printTime(arg=Funci� que volem mesurar)
```


### Sisaptools
```{python}
>> import sisaptools as t
```

**Per llegir dades**
```{python}
>> t.Database("bbdd", "data").get_one(sql)
>> t.Database("bbdd", "data").get_all(sql)
```

**Per fer altres tasques com llegir csv, enviar mails autom�tics...**
```{python}
mail = t.Mail()
mail.to.append("ffinaaviles@gencat.cat")
mail.cc.extend((
    "lmendezboo@gencat.cat",
    ))
mail.subject = subject
mail.text = text
mail.send()
```

## COM DESCARREGAR TAULES

Des de la p�gina de do not push, fent un prepare.py des de la carpeta sisap i marcant les taules que volem descarregar si aquestes han estat pr�viament descarregades a SIDICS.

Si les taules no han estat carregades pr�viament a SIDICS cal for�ar la seva desc�rrega. Abans per�, hem de garantir que tot el que hi hagi al data child i al data father estigui commitejat correctament i fer un "prepare.py res" des de sisap (ETL).

Si es tracta d'una desc�rrega completa s'ha de llen�ar el fitxer de catalegs.py per descarregar els cat�legs i el de ecap_utils.py per descarregar la resta de les taules.

Si el que volem �s descarregar una �nica taula perqu� s'han afegit nous camps a descarregar, perqu� �s una taula nova... per� no volem descarregar-ho tot, cal a la condici� de la l�nia 60 de l'arxiu ecap_utils.py afegir el nom de la taula que volem (name = nom de nodrizas, table = nom a ECAP) i comentar les l�nies 63-65.

Una vegada haguem fet la desc�rrega a SIDICS cal fer el que es comenta al primer punt d'executar a partir de do not push.

### CAT�LEGS SIDICS
Ordenar alfab�ticament i mirar si venen de sectors. Si est� igual a tots els sectors posar un �nic sector. Posar exadata = True per penjar-ho a exadata. Aix� fa directament el sin�nim a exadata.

## JUPYTER NOTEBOOK

1. Per obtenir el token dintre del contenidor executar: jupyter notebook list

2. http://10.80.217.68:8888/tree/ obrir aquesta p�gina per pre i el mateix amb el 84 per prod i posar el token obtingut.



## SERVIDORS
Disposem de 2 servidors diferents:
- 10.80.217.68 en el que hi ha tots els temes hist�rics del SISAP i al que treballem la majoria de les vegades. �s on hi ha les bases de dades de nodrizas.
- 10.80.217.84 en el que hi ha els processos m�s nous, repositori de covid, programaci� per motius, the whole... i no el fem servir de forma di�ria a no ser que estiguis en un dels projectes esmentats anteriorment.

Els dos servidors tenen muntat un servei docker. Per accedir als contenidors d'aquests disposem d'una �lies per fer-ho m�s senzill. Si es vol accedir al .68 cal escriure $sisap$ mentre que si vols accedir al .84 pots escriure $covid$, $the\_whole$... per entrar als respectius entorns.

No s� si est�s familiaritzada amb entorns docker per� et deixo a continuaci� les dues comandes que cal que coneguis b�sicament per comen�ar:

```{}
>> docker-compose down  -- tumbar contenidors
>> docker-compose up -d  -- carregar contenidors
```

**Acc�s per ILO**

Si algun dels servidors cau, es pot arreglar accedint-hi per ILO. Amb els seg�ents passos:

1. Introduir adre�a del servidor al navegador: 10.80.217.69 (p2262) o 10.80.217.47 (x002).

2. Activar extensi� IE tools del Chrome per poder accedir al p2262 que �s molt vell i nom�s va amb explorer antic sin�.

3. Logar-se amb user: sisap i pwd: el de sempre per� amb 3*X al final

4. Anar a power management > server power > force system reset --> clicar reset


### PROCESSOS
Com hem comentat anteriorment, hi ha diferents processos que es llencen de frma autom�tica. El m�s important �s el proc�s d'actualitzaci� de dades que s'executa cada divendres. Per fer-ho hi ha diferents requisits/condicions.
- DESC�RREGA DE DADES ACTUALITZADES: es fa de manera autom�tica cada dimecres a la nit a una base de dades que es diu SIDICS i �s 100% independent a la resta. Les dades es descarregues de sectors principalment menys la taula de laboratori que ve de The Whole (ja t'explicar� m�s endavant qu� �s exactament).
- EXECUCI� DEL PROC�S: es llan�a de manera manual cada divendres a les 14h (tradicionalment i si no hi ha cap imprevist). Per llen�ar-ho hi ha diferents requisits:
1. No pot haver-hi cap commit pendent
2. S'han de tumbar contenidors
3. Hi ha dos procediments diferents depenent de si el proc�s �s setmanal o mensual. Els passos a seguir estan guardats a: https://bitbucket.org/sisapICS/etl/wiki/Home

Els processos mensuals es tiren per fer tancament de mes, l'�ltim cap de setmana del mes en curs o el primer del seg�ent. NOTA: La data d'actualitzaci� SEMPRE �s del darrer dimecres per molt que tirem el proc�s el divendres, per tant tenim un decalatge de fins a 10 dies la majoria de les vegades.

Hi ha unes p�gines web internes per poder consultar l'estat del servidor i/o l'estat del proc�s (tot sobre el .68).
- http://10.80.217.68/control/health.php : per consultar estat del servidor, mem�ria ocupada, queries que s'hi estan llen�ant...
- http://10.80.217.68/control/ : per seguir el proc�s setmanal o mensual. Aquesta p�gina ens mostra tamb� com va la desc�rrega de les taules al MariaDB (nodrizas). **Nota:** Les taules qe el % de desc�rrega �s superior al 100% (sobretot laboratori, ITs...) s�n taules que no venen de sectors, les tenim amb cip i intentem imputar els resultats d'aquests laboratoris, baixes... a tots els id_cip_sec (ids per sector) a cada pacient, per tant, el que estem fent �s una duplicitat de registres, per tant, no podem fer un count(1) a laboratori per exemple per saber quants colesterols s'han fet l'�ltim any, si ho fem aix� a nodrizas n'obtindrem molts m�s els reals multiplicats pel nombre de sectors on ha estat hist�ricament cada pacient.
- http://10.80.217.68/control/doNotPush.php : per tirar processos (sripts de python, sql...) per web. Necessitar�s una pwd per poder-ho fer, pregunta quan la necessitis:)


#### GESTI� DE PROCESSOS

A continuaci� et deixo unes comandes que et seran �tils en algun futur.

Quan es tira un proc�s per web i aquest es mata abans que acabi, la web del do not push es queda bloquejada. Per desbloquejar-la cal executar la seg�ent sent�ncia a Dbeaver:
```{SQL}
update sisap_ctl.donotpushagr set finished = 1
```

Quan el proc�s dona errors, a la web de control els fitxers que han donat error surten pintats en vermell. En aquests casos, ho arreglem a m� i moltes vegades ho rellencem a m�. Si tot va b�, hem de pintar de color verd aquest proc�s a la web. Per fer-ho cal executar:
```{SQL}
update ESQUEMA.ctl_ESQUEMA set status = 0, error = '' where file = 'FITXER'
```
on:
- ESQUEMA = esquema en el que es tira el proc�s
- FITXER = arxiu que ha petat i est� de color vermell



## SET UP R TO EXADATA

```{R}
# Boilerplate connexi� a Exadata des d'R
options(java.parameters = "-Xmx16g")

library(RJDBC)
library(DBI)
library(dbplyr)
library(dplyr)

# Get the driver
driver_path = "C:/Users/rcantenys/Downloads/pdi-ce-9.3.0.0-428/data-integration/lib/ojdbc8.jar"
driver = JDBC("oracle.jdbc.driver.OracleDriver", driver_path)

# Connection params PRO
host          = "excdox-scan.cpd4.intranet.gencat.cat"
port          = 1522
service_name  = "excdox01srv"

# Connection params PRE
host          = "excdot-scan.cpd4.intranet.gencat.cat"
port          = 1522
service_name  = "excdot01srv"


#############################
username      = "DWUSER"      
password      = "your_pwd"
############################

# Make DSN
dsn = paste0("jdbc:oracle:thin:@//", host, ":", port, "/", service_name)

connection = dbConnect(driver,
                       dsn,
                       username,
                       password)

sample_query = "
  SELECT * FROM DWSISAP.bdcap_pacientes_2022
  FETCH  FIRST 100 ROWS ONLY
"

result = dbGetQuery(connection,
                    sample_query)

```


## COMANDES GIT

git reset --soft HEAD~ : quan hem fet commit per� no push d'algun arxiu i ens hem equivocat
git reset HEAD~1 : quan hem fet commit per� no push d'algun arxiu i ens hem equivocat

git stash && git pull --rebase && git push && git stash pop: quan hi ha canvis als dos servidors i ens dona error per pujar / descarregar

## ROTACI� BASES DE DADES

fora del contenidor (�s igual si PRE o PRO):
- desde /sisap/
- cd ../databases
- python3 cycle.py

## BASALS

Penjar llistats a FMO dels canvis del proper any a SISAP-ECAP.

## RESID�NCIES

a 25resis/source hi ha la creaci� del m�ster de resid�ncies (codi master_residencies.py). Aquest m�ster s'utilitza despr�s al codi creacio_indicadors.py, que �s on hi ha la majoria d'indicadors del QC de resid�ncies. Els codis gida.py i nafres_upp.py son heredats del Leo i no s'hi han de fer canvis gaireb� mai. El codi indicadors_hospitalitzacions.py t� tots els indicadors relacionats amb urg�ncies, serveis d'interm�dia i ingressos del QC de resid�ncies. Tots els exports a LV es fan a export_resis.py.

Hi ha unes quantes taules importants:
- dwsisap.residencies_cataleg: t� totes les resid�ncies, l'EAP al que estan lligades i els grups d'usuaris
- dwsisap.residencies_cens: t� una "foto" de qui hi ha a cada resid�ncies per cada dia
- import.cat_sisap_map_residencies: semblant a cat�leg de resid�ncies
- import.cat_sisap_map_grup_rup: c�pia de sisap_map_grup_rup de redics
- sisap_map_grup_rup: cat�leg amb codi RESES associat. No �s un cat�leg fiable, s'actualitza manualment

Per actualitzar el cat�leg, quan la Paqui passa nous grups d'usuaris, s'han d'afegir al codi 96informes/upsert_SISAP_MAP_GRUP_RUP.txt i executar despr�s el codi 96informes/236covid_new_residencies_diari.py

## ATDOM

l'estructura d'ATDOM �s gaireb� igual a resid�ncies. S'utilitza el m�ster de resid�ncies al codi creacio_indicadors.py d'atdom. Els codis gida.py i nafres_upp.py son del Leo i indicadors_hospitalitzacions.py t� indicadors d'urg�ncies, ingressos, interm�dia, etc.

## VACUNES

nom�s hi ha a 03ind/source un codi (vacunes.sql) que crea un "m�ster de vacunes" per poder calcular els indicadors de l'EQA0508/09/10/11/12/13. S'ha de parlar amb l'Anna Re�� perqu� es volen fer canvis d'agrupadors de pneumoc�ccica, i afectar� a la majoria d'aquests indicadors.

## LANDING

Landing Cronicitat ja est� a PRO amb el codi landing_prioritzats.py. Les noves dades d'ESIAP estan a landing_uba_pre.py

## EINA OFERTA

El codi est� a 08alt/source/calcul_oferta.py. �s l'eina d'ATC, i de visualitzaci� ho porta en Joan, per si de cas demanen algo. S'executa un cop al mes i agafa indicadors de LV del mes passat.