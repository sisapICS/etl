# -*- coding: utf8 -*-

from collections import defaultdict
import sisapUtils as u
import numpy as np
import datetime as d
import os

cols = """(c_up varchar2(5), up_desc varchar2(100), uab varchar2(6), uab_descripcio varchar2(200), 
        ambit varchar2(100), sap varchar2(100), FASE int, dia date, total int,
        EXCLUSIO_SANITARIA int, PENDENT_PETICIO_ANALITICA int, LLEST_PER_PROGRAMAR int, 
        EXCLUSIO_ADMINISTRATIVA int, TASQUES_PENDENTS int, RETORNAT_AL_SANITARI int,
        PROGRAMAT int, programats_totals int, programats_totals_anuals int, 
        REVISAR_PROGRAMACIO int, uba_iniciada int, uba_programada int,
        cobertura_iniciats number(38,0), cobertura_programats number(38,0),
        iniciats int, programats_iniciats number(38,0))
        """
u.createTable('seguiment_direccio_complet', cols, 'pdp', rm=True)

info_ups = dict()
sql = """SELECT UP_CODI_UP_SCS, UP_DESC_UP_ICS, AMB_DESC_AMB, DAP_DESC_DAP FROM centres"""
for up, desc_up, ambit, sap in u.getAll(sql, 'pdp'):
        info_ups[up] = (desc_up, ambit, sap)

upload = []
upload1 = []
sql = """SELECT c_up, professional, UAB_DESCRIPCIO, FASE, dia, sum(N) total,
        sum(EXCLUSIO_SANITARIA) EXCLUSIO_SANITARIA, sum(PENDENT_PETICIO_ANALITICA) PENDENT_PETICIO_ANALITICA,
        sum(LLEST_PER_PROGRAMAR) LLEST_PER_PROGRAMAR, sum(EXCLUSIO_ADMINISTRATIVA) EXCLUSIO_ADMINISTRATIVA, sum(TASQUES_PENDENTS) TASQUES_PENDENTS,
        sum(RETORNAT_AL_SANITARI) RETORNAT_AL_SANITARI, sum(PROGRAMAT) PROGRAMAT, sum(PROGRAMATS_TOTALS) PROGRAMATS_TOTALS, sum(PROGRAMATS_TOTALS_ANUALS) PROGRAMATS_TOTALS_ANUALS,
        sum(REVISAR_PROGRAMACIO_0_3)+sum(REVISAR_PROGRAMACIO_3_6)+sum(REVISAR_PROGRAMACIO_6_9)+sum(REVISAR_PROGRAMACIO_9_12) REVISAR_PROGRAMACIO,
        CASE WHEN (sum(EXCLUSIO_SANITARIA)+sum(PENDENT_PETICIO_ANALITICA)
        +sum(LLEST_PER_PROGRAMAR)+sum(EXCLUSIO_ADMINISTRATIVA)+sum(TASQUES_PENDENTS)
        +sum(RETORNAT_AL_SANITARI)+sum(PROGRAMAT)) > 10 THEN 1 
        ELSE 0 END UBA_INICIADA,
        CASE WHEN sum(PROGRAMATS_TOTALS) > 10 THEN 1 ELSE 0 END uba_programada,
        ROUND((sum(EXCLUSIO_SANITARIA)+sum(PENDENT_PETICIO_ANALITICA)
        +sum(LLEST_PER_PROGRAMAR)+sum(EXCLUSIO_ADMINISTRATIVA)+sum(TASQUES_PENDENTS)
        +sum(RETORNAT_AL_SANITARI)+sum(PROGRAMAT))/sum(N)* 100, 2) cobertura_iniciats,
        ROUND((sum(PROGRAMAT)/sum(N))*100, 2) cobertura_programats,
        sum(EXCLUSIO_SANITARIA)+sum(PENDENT_PETICIO_ANALITICA)
                +sum(LLEST_PER_PROGRAMAR)+sum(EXCLUSIO_ADMINISTRATIVA)+sum(TASQUES_PENDENTS)
                +sum(RETORNAT_AL_SANITARI)+sum(PROGRAMAT) iniciats,
        CASE (sum(EXCLUSIO_SANITARIA)+sum(PENDENT_PETICIO_ANALITICA)
                +sum(LLEST_PER_PROGRAMAR)+sum(EXCLUSIO_ADMINISTRATIVA)+sum(TASQUES_PENDENTS)
                +sum(RETORNAT_AL_SANITARI)+sum(PROGRAMAT)) WHEN 0 then 0 --whatever you want 
                ELSE ROUND(sum(PROGRAMAT)/(sum(EXCLUSIO_SANITARIA)+sum(PENDENT_PETICIO_ANALITICA)
                        +sum(LLEST_PER_PROGRAMAR)+sum(EXCLUSIO_ADMINISTRATIVA)+sum(TASQUES_PENDENTS)
                        +sum(RETORNAT_AL_SANITARI)+sum(PROGRAMAT))* 100, 2)
                        END PROGRAMATS_INICIATS
        FROM ANALISI_PLANIFICAT
        WHERE tipus = 'M'
        AND TO_CHAR (dia, 'd') = '2'
        GROUP BY c_up, professional, UAB_DESCRIPCIO, fase, dia"""

for c_up, prof, uab, FASE, dia, total,EXCLUSIO_SANITARIA, PENDENT_PETICIO_ANALITICA, LLEST_PER_PROGRAMAR, EXCLUSIO_ADMINISTRATIVA, TASQUES_PENDENTS, RETORNAT_AL_SANITARI,PROGRAMAT, PROGRAMATS_TOTALS, PROGRAMATS_TOTALS_ANUALS, REVISAR_PROGRAMACIO, UBA_INICIADA, UBA_PROGRAMADA, COBERTURA_INICIATS, COBERTURA_PROGRAMATS, iniciats, programats_iniciats in u.getAll(sql, 'pdp'):
    upload.append((c_up, info_ups[c_up][0], prof, uab, info_ups[c_up][1], info_ups[c_up][2], 
                FASE, dia, total,EXCLUSIO_SANITARIA, 
                PENDENT_PETICIO_ANALITICA, LLEST_PER_PROGRAMAR, 
                EXCLUSIO_ADMINISTRATIVA, TASQUES_PENDENTS, 
                RETORNAT_AL_SANITARI,PROGRAMAT, PROGRAMATS_TOTALS, REVISAR_PROGRAMACIO,
                UBA_INICIADA, UBA_PROGRAMADA, COBERTURA_INICIATS, COBERTURA_PROGRAMATS,
                iniciats, programats_iniciats))
    upload1.append((c_up, info_ups[c_up][0], prof, uab, info_ups[c_up][1], info_ups[c_up][2], 
                FASE, dia, total,EXCLUSIO_SANITARIA, 
                PENDENT_PETICIO_ANALITICA, LLEST_PER_PROGRAMAR, 
                EXCLUSIO_ADMINISTRATIVA, TASQUES_PENDENTS, 
                RETORNAT_AL_SANITARI,PROGRAMAT, PROGRAMATS_TOTALS, 
                PROGRAMATS_TOTALS_ANUALS, REVISAR_PROGRAMACIO,
                UBA_INICIADA, UBA_PROGRAMADA, COBERTURA_INICIATS, COBERTURA_PROGRAMATS,
                iniciats, programats_iniciats))
print(upload[0], upload[1])
u.listToTable(upload1, 'seguiment_direccio_complet', 'pdp')
print(upload1[0], upload1[1])

SMS_DIA = (d.datetime.now().date() - d.timedelta(days=3))
file_eap = u.tempFolder + 'Seguiment_planificat_TOTAL{}.csv'.format(SMS_DIA)
u.writeCSV(file_eap, [('UP', 'DES_UP', 'UBA', 'DES_UBA', 'AMBIT', 'SAP', 'FASE', 'DIA', 'TOTAL', 'EXCLUSIO_SANITARIA', 
                'PENDENT_PETICIO_ANALITICA', 'LLEST_PER_PROGRAMAR', 
                'EXCLUSIO_ADMINISTRATIVA', 'TASQUES_PENDENTS', 'RETORNAT_AL_SANITARI',
                'PROGRAMAT', 'PROGRAMATS_TOTALS', 'REVISAR_PROGRAMACIO', 'UBA_INICIADA',
                'UBA_PROGRAMADA', 'COBERTURA_INICIATS', 'COBERTURA_PROGRAMATS', 
                'INICIATS', 'PROGRAMATS_INICIATS')] + upload, sep=";")

file_eap1 = u.tempFolder + 'Seguiment_planificat_TOTAL1{}.csv'.format(SMS_DIA)
u.writeCSV(file_eap1, [('UP', 'DES_UP', 'UBA', 'DES_UBA', 'AMBIT', 'SAP', 'FASE', 'DIA', 'TOTAL', 'EXCLUSIO_SANITARIA', 
                'PENDENT_PETICIO_ANALITICA', 'LLEST_PER_PROGRAMAR', 
                'EXCLUSIO_ADMINISTRATIVA', 'TASQUES_PENDENTS', 'RETORNAT_AL_SANITARI',
                'PROGRAMAT', 'PROGRAMATS_TOTALS', 'PROGRAMATS_TOTALS_ANUALS', 'REVISAR_PROGRAMACIO', 'UBA_INICIADA',
                'UBA_PROGRAMADA', 'COBERTURA_INICIATS', 'COBERTURA_PROGRAMATS', 
                'INICIATS', 'PROGRAMATS_INICIATS')] + upload1, sep=";")

# email
# subject = 'Seguiment Planifi.cat'
# text = "Bon dia.\n\nAdjuntem arxiu .csv amb les dades de seguiment setmanals del Planifi.cat.\n"
# text += "\n\nSalutacions."

# u.sendGeneral('SISAP <sisap@gencat.cat>',
#                       ('jblade.tgn.ics@gencat.cat',
#                         'caguilar.ebre.ics@gencat.cat',
#                         'cmartinezca.mn.ics@gencat.cat',
#                         'ivila.pirineu.ics@gencat.cat',
#                         'jflorensa.pirineu.ics@gencat.cat',
#                         'mjgarcia.cp.ics@gencat.cat',
#                         'mhoms.cc.ics@gencat.cat',
#                         'skaur.cc.ics@gencat.cat',
#                         'mpiqueras.tgn.ics@gencat.cat',
#                         'mmingot.lleida.ics@gencat.cat',
#                         'nolona.bcn.ics@gencat.cat',
#                         'nparellada.cp.ics@gencat.cat',
#                         'lpalacios.tgn.ics@gencat.cat',
#                         'irecasens.ics@gencat.cat',
#                         'xmarin.cc.ics@gencat.cat',
#                         'cpladevall@catsalut.cat',
#                         'daniel.algar@catsalut.cat',
#                         'jsalabert@catsalut.cat',
#                         'pol.hospital@catsalut.cat',
#                         'cfranco@catsalut.cat',
#                         'abel.maja@catsalut.cat',
#                         'meritxell.durant@catsalut.cat',
#                         'leonard.bergada@catsalut.cat',
#                         'glopez.ics@gencat.cat',
#                         'mtorres.girona.ics@gencat.cat',
#                         'srodoreda.mn.ics@gencat.cat', 
#                         'amasc@gencat.cat', 
#                         'mgarcia.canela@gencat.cat',
#                         'lazlor.cc.ics@gencat.cat'),
#                        ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat', 'mbustos.bnm.ics@gencat.cat'),
#                       subject,
#                       text,
#                       file_eap)

subject = 'Seguiment Planifi.cat'

u.sendGeneral('SISAP <sisap@gencat.cat>',
                      ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat', 'mbustos.bnm.ics@gencat.cat'),
                       ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat', 'mbustos.bnm.ics@gencat.cat'),
                      subject,
                      'PROVES REBREM NOS NOMES',
                      file_eap1)

# esborrar arxius
os.remove(file_eap)
os.remove(file_eap1)