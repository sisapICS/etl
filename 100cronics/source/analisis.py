
from collections import defaultdict,Counter
import sisapUtils as u
import numpy as np
import hashlib as h
import sisaptools as t
import pandas as pd
import math
from datetime import datetime

# print('dbs')
# cip_visita = dict()
# sql = """SELECT C_CIP, C_VISITES_ANY FROM DBS WHERE 
#             C_EDAT_ANYS > 14 AND
#             PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL 
#             OR ps_hta_data IS NOT NULL 
#             OR PS_DIABETIS2_DATA IS NOT NULL OR 
#             PS_INSUF_CARDIACA_DATA is NOT NULL OR 
#             PS_MPOC_ENFISEMA_DATA IS NOT NULL  OR 
#             PS_DISLIPEMIA_DATA IS NOT NULL AND 
#             PR_MACA_DATA IS NULL AND
#             PS_DEMENCIA_DATA IS NULL AND
#             PS_CURES_PALIATIVES_DATA IS NULL AND 
#             (CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL OR PS_INSUF_CARDIACA_DATA IS NOT NULL 
#             OR (PS_MPOC_ENFISEMA_DATA IS NOT NULL)THEN 3
#             WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 2 ELSE 1 END) IN (2,3)"""
# for cip, visites in u.getAll(sql, 'redics'):
#     cip_visita[cip] = visites

# print('cronics')
# grup_cip = defaultdict(list)
# sql = """SELECT c_cip, grup FROM SEGUENT_VISITA_CRONICS svc """
# for cip, grup in u.getAll(sql, 'pdp'):
#     grup_cip[grup].append(cip)

# print('calcul')
# upload = []
# median = []
# for e in grup_cip:
#     n = 0
#     v = 0
#     m1 = []
#     for cip in grup_cip[e]:
#         n += 1
#         v += cip_visita[cip]
#         m1.append(cip_visita[cip])
#     print(e, v, n, np.mean(m1), np.median(m1))
#     upload.append((e, v, n, np.mean(m1), np.median(m1)))

# print(upload)

# cols = '(grup int, servei varchar(10), mean double, median double, p25 double, p75 double)'
# u.createTable('percentatge_visites', cols, 'test', rm=True)

# print('0')
# def get_hash(cip):
#     """."""
#     if t.constants.IS_PYTHON_3:
#         cip = cip.encode("latin1")
#     return h.sha1(cip).hexdigest().upper()

# sql = "select usua_cip_cod, usua_cip from pdptb101"
# hash_to_cip = {hash: get_hash(cip) for (hash, cip) in u.getAll(sql, "pdp")}

# print('1')
# sql = """SELECT c_cip, grup FROM SEGUENT_VISITA_CRONICS svc """
# cip_grup = dict()
# for cip, grup in u.getAll(sql, 'pdp'):
#     cip_grup[hash_to_cip[cip]] = grup

# print('centres')
# ups = set()
# sql = """select scs_codi from nodrizas.cat_centres"""
# for up, in u.getAll(sql, 'nodrizas'):
#     ups.add(up)

# print('2')
# pacients_grups = Counter()
# sql = """SELECT PACIENT, SISAP_SERVEI_CLASS, UP 
#             FROM dwsisap.sisap_master_visites
#             WHERE FLAG_4CW = 0
#             AND SISAP_SERVEI_CLASS IN ('MF', 'INF', 'PED', 'URG', 'UAC', 'AUX')
#             AND EXTRACT(YEAR FROM DATA) = 2021"""
# for pacient, servei, up in u.getAll(sql, 'exadata'):
#     if pacient in cip_grup and up in ups:
#         pacients_grups[(pacient, servei, cip_grup[pacient])] += 1

# print('3')
# grup_servei = defaultdict(list)
# for k, v in pacients_grups.items():
#     servei = k[1]
#     grup = k[2]
#     grup_servei[(grup, servei)].append(v)

# print('uploading')
# upload = []
# for k, v in grup_servei.items():
#     print(k[0], k[1], np.mean(v), np.median(v), np.quantile(v, 0.25), np.quantile(v, 0.75))
#     upload.append((k[0], k[1], np.mean(v), np.median(v), np.quantile(v, 0.25), np.quantile(v, 0.75)))

# u.listToTable(upload, 'percentatge_visites', 'test')


# -------------------------------------------------------------
# print('centres')
# centres = set()
# sql = """SELECT up FROM CENTRES_PILOTS cp"""
# for up, in u.getAll(sql, 'pdp'):
#     centres.add(up)

# print('query gran')
# sql = """SELECT c_cip, c_up, c_metge, C_SECTOR, C_INFERMERA, C_EDAT_ANYS,
#             CASE WHEN PS_ATDOM_DATA IS NOT NULL THEN 1 END ATDOM,
#             CASE WHEN C_INSTITUCIONALITZAT IS NOT NULL THEN 'R' WHEN PS_ATDOM_DATA IS NOT NULL AND C_INSTITUCIONALITZAT IS NULL THEN 'A' END ATDOM_RESIDENCIA, 
#             CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL THEN 1 end CI,
#             CASE WHEN PS_ACV_MCV_DATA IS NOT NULL THEN 1 end AVC,
#             CASE WHEN PS_HTA_DATA IS NOT NULL THEN 1 END HTA,
#             CASE WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 1 END dm2,
#             CASE WHEN PS_INSUF_CARDIACA_DATA IS NOT NULL THEN 1 END IC,
#             CASE WHEN PS_MPOC_ENFISEMA_DATA IS NOT NULL and( VC_GOLD_VALOR LIKE '%%greu%%' OR (V_FEV1_VALOR<50 AND VC_GOLD_VALOR IS null)) THEN 1 END MPOC_GREU,
#             CASE WHEN PS_MPOC_ENFISEMA_DATA IS NOT NULL and( VC_GOLD_VALOR NOT LIKE '%%greu%%' AND (V_FEV1_VALOR>=50 AND VC_GOLD_VALOR IS NOT null)) THEN 1 END MPOC_LLEU,
#             CASE WHEN PS_DISLIPEMIA_DATA IS NOT NULL THEN 1 END dislipemia,
#             (CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL OR PS_INSUF_CARDIACA_DATA IS NOT NULL 
#             OR (PS_MPOC_ENFISEMA_DATA IS NOT NULL and( VC_GOLD_VALOR LIKE '%%greu%%' OR (V_FEV1_VALOR<50 AND VC_GOLD_VALOR IS null)))THEN 3
#             WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 2 ELSE 1 END) risc,
#             CASE WHEN F_HTA_COMBINACIONS is NOT NULL OR F_HTA_DIURETICS IS NOT NULL OR F_HTA_IECA_ARA2 IS NOT NULL THEN 1 ELSE 0 END FarmHTA,
#             CASE WHEN PS_HIPOTIROIDISME_DATA IS NOT NULL THEN 1 END HIPOTIROIDISME,
#             V_HBA1C_DATA glicada_DM,
#             V_COL_TOTAL_DATA  colest,
#             V_ECG_AMB_DATA ECG,
#             V_FEV1_DATA espiro1,
#             V_FEV1_FVC_DATA espiro2,
#             VC_FONS_ULL_DATA fons_ull
#         fROM 
#             dbs 
#         WHERE 
#             C_EDAT_ANYS > 14 AND
#             PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL 
#             OR ps_hta_data IS NOT NULL 
#             OR PS_DIABETIS2_DATA IS NOT NULL OR 
#             PS_INSUF_CARDIACA_DATA is NOT NULL OR 
#             PS_MPOC_ENFISEMA_DATA IS NOT NULL  OR 
#             PS_DISLIPEMIA_DATA IS NOT NULL AND 
#             PR_MACA_DATA IS NULL AND
#             PS_DEMENCIA_DATA IS NULL AND
#             PS_CURES_PALIATIVES_DATA IS NULL AND 
#             (CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL OR PS_INSUF_CARDIACA_DATA IS NOT NULL 
#             OR (PS_MPOC_ENFISEMA_DATA IS NOT NULL)THEN 3
#             WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 2 ELSE 1 END) IN (1)"""

# upload = []
# for (c_cip, c_up, c_metge, C_SECTOR, C_INFERMERA, c_edat, atdom, ATDOM_RESIDENCIA, 
#     CI, AVC, HTA, dm2, IC, MPOC_GREU, MPOC_LLEU, dislipemia, risc, FarmHTA,
#     HIPOTIROIDISME, glicada_DM, colest, ECG, espiro1, espiro2, 
#     fons_ull) in u.getAll(sql, 'redics'):
#     if c_up in centres:
#         upload.append([c_cip, c_up, c_metge, C_SECTOR, C_INFERMERA, c_edat, atdom, ATDOM_RESIDENCIA, 
#                         CI, AVC, HTA, dm2, IC, MPOC_GREU, MPOC_LLEU, dislipemia, 
#                         risc, FarmHTA, HIPOTIROIDISME, glicada_DM, colest, ECG, 
#                         espiro1, espiro2, fons_ull])

# print('crear dict')    
# dbs = pd.DataFrame(upload)
# dbs.columns =['C_CIP', 'C_UP', 'C_METGE', 'C_SECTOR', 'C_INFERMERA',
#                 'C_EDAT_ANYS', 'ATDOM',
#                 'ATDOM_RESIDENCIA', 'CI', 'AVC', 'HTA', 'DM2', 'IC', 
#                 'MPOC_GREU', 'MPOC_LLEU', 'DISLIPEMIA', 'RISC', 'FARMHTA',
#                 'HIPOTIROIDISME', 'GLICADA_DM', 'COLEST', 'ECG', 
#                 'ESPIRO1', 'ESPIRO2', 'FONS_ULL']
# print('len dbs:', len(dbs))

# print('assignacio grups')
# ts = datetime.now()
# def grups(CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU):
#     if IC == 1:
#         if DM == 1 and (GREU == 1 or LLEU == 1): return 1
#         elif DM == 1: return 2
#         elif (GREU == 1 or LLEU == 1): return 3
#         else: return 4
#     elif CI == 1:
#         if DM == 1 and (GREU == 1 or LLEU == 1): return 5
#         elif DM == 1: return 6
#         elif (GREU == 1 or LLEU == 1): return 7
#         else: return 8
#     elif DM == 1:
#         if GREU == 1 or LLEU == 1: return 9
#         else: return 10
#     elif GREU == 1 or LLEU == 1:
#         if HTA == 1: return 11
#         elif DISLIPEMIA == 1: return 12
#         else: return 13
#     elif HTA == 1: return 14
#     elif DISLIPEMIA == 1: return 15


# def agrupadors(df):
#     return pd.Series([
#         grups(CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU)
#         for (CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU) in zip(df['CI'], df['IC'], df['HTA'], df['DISLIPEMIA'],
#                                                         df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'])
#       ])

# print('agrupadors i N')
# dbs['grup'] = agrupadors(dbs)
# dbs['N'] = dbs['grup'].apply(lambda x: 1 if x in (14, 15) else 0)

# dbs = dbs[['C_CIP', 'C_UP', 'C_METGE','grup', 'N']]

# dbs = dbs.groupby(['C_UP', 'C_METGE','grup'])['N'].agg("sum")
# dbs = dbs.reset_index()

# print('crear csv')
# dbs.to_csv("grups_14_15.csv")


sql = """SELECT C_UP, UAB_DESCRIPCIO FROM analisi_planificat
        WHERE TO_CHAR (dia, 'dd') = '22'
        AND tipus = 'M'"""
quedar_nos = set()
for up, uba in u.getAll(sql, 'pdp'):
    quedar_nos.add((up, uba))

sql = """SELECT c_up, PROFESSIONAL, TIPUS, FASE, EXCLUSIO_SANITARIA, PENDENT_PETICIO_ANALITICA, 
LLEST_PER_PROGRAMAR, EXCLUSIO_ADMINISTRATIVA, TASQUES_PENDENTS, 
RETORNAT_AL_SANITARI, PROGRAMAT, N, DIA, REVISAR_PROGRAMACIO_0_3,
REVISAR_PROGRAMACIO_3_6, REVISAR_PROGRAMACIO_6_9, REVISAR_PROGRAMACIO_9_12,
UAB_DESCRIPCIO FROM analisi_planificat"""

upload = []
for (c_up, PROFESSIONAL, TIPUS, FASE, EXCLUSIO_SANITARIA, PENDENT_PETICIO_ANALITICA, 
        LLEST_PER_PROGRAMAR, EXCLUSIO_ADMINISTRATIVA, TASQUES_PENDENTS, 
        RETORNAT_AL_SANITARI, PROGRAMAT, N, DIA, REVISAR_PROGRAMACIO_0_3,
        REVISAR_PROGRAMACIO_3_6, REVISAR_PROGRAMACIO_6_9, REVISAR_PROGRAMACIO_9_12,
        UAB_DESCRIPCIO) in u.getAll(sql, 'pdp'):
        if (c_up, UAB_DESCRIPCIO) in quedar_nos:
            upload.append((c_up, PROFESSIONAL, TIPUS, FASE, EXCLUSIO_SANITARIA, PENDENT_PETICIO_ANALITICA, 
                LLEST_PER_PROGRAMAR, EXCLUSIO_ADMINISTRATIVA, TASQUES_PENDENTS, 
                RETORNAT_AL_SANITARI, PROGRAMAT, N, DIA, REVISAR_PROGRAMACIO_0_3,
                REVISAR_PROGRAMACIO_3_6, REVISAR_PROGRAMACIO_6_9, REVISAR_PROGRAMACIO_9_12,
                UAB_DESCRIPCIO))

u.listToTable(upload, 'analisi_planificat_2', 'pdp')
