# -*- coding: utf8 -*-

import sisapUtils as u
import datetime as d


class DadesCatalunya():
    def __init__(self):
        self.get_data()
        self.send()
    
    def get_data(self):
        grup_description = {}
        sql = """SELECT grup, descripcio FROM visita_cronics_cat"""
        for grup, des in u.getAll(sql, 'pdp'):
            grup_description[grup] = des
        professional_description = {}
        sql = """SELECT up, uab, tipus, UAB_DESCRIPCIO  FROM professionals"""
        for up, uba, tipus, desc in u.getAll(sql, 'pdp'):
            professional_description[(up, uba, tipus)] = desc
        self.upload = []
        desc_centres = {}
        sql = """select scs_codi, ics_desc from nodrizas.cat_centres"""
        for up, centre in u.getAll(sql, 'nodrizas'):
            desc_centres[up] = centre
        sql = """SELECT c_up, c_metge uba, 'M' as tipus, grup, count(1) AS N
                    FROM dbs
                    where grup > 0
                    GROUP BY c_up, c_metge, grup
                    UNION 
                    SELECT c_up, c_infermera uba, 'I' as tipus, grup, count(1) AS N
                    FROM dbs
                    where grup > 0
                    GROUP BY c_up, c_infermera, grup"""
        for up, professional, tipus, grup, N in u.getAll(sql, 'redics'):
            if (up, professional, tipus) in professional_description:
                desc_professional = professional_description[(up, professional, tipus)]
            else: 
                desc_professional = ''
            if grup in grup_description:
                desc_grup = grup_description[grup]
            else:
                desc_grup = ''
            if up in desc_centres:
                desc_up = desc_centres[up]
            else: desc_up = ''
            self.upload.append((up, desc_up, professional, tipus, desc_professional, grup, desc_grup, N))

    def send(self):
        cols = ()
        SMS_DIA = (d.datetime.now().date())
        file_eap = u.tempFolder + 'Up_uba_planificat_grups_{}.csv'.format(SMS_DIA)
        u.writeCSV(file_eap, [("UP", "DESC_UP", "PROFESSIONAL", "TIPUS", "DESC_PROFESSIONAL", "GRUP", "DESC_GRUP", "N")] + self.upload, sep=";")
        # email
        subject = 'Seguiment N equips Planifi.cat'
        text = "Bon dia.\n\nAdjuntem arxiu .csv amb les dades mensuals de les Ns per tots els equips, ubas i grups Planifi.cat de Catalunya independentment que hagin comencat Planifi.cat o no.\n"
        text += "\n\nSalutacions."

        u.sendGeneral('SISAP <sisap@gencat.cat>',
                            ('cpladevall@catsalut.cat',
                             'abel.maja@catsalut.cat',
                             'jsalabert@catsalut.cat'),
                            ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat', 'mbustos.bnm.ics@gencat.cat'),
                            subject,
                            text,
                            file_eap)

if __name__ == "__main__":
    if u.IS_MENSUAL:
        DadesCatalunya()