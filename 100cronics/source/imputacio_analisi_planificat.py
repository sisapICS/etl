import sisapUtils as u
import collections


class Analisi_planificat():
    def __init__(self, dia):
        self.dia = dia
        self.professionals()
        self.centres()
        self.imputacio_metges()
        self.imputacio_infermers()
        self.penjar()

    def professionals(self):
        print('profesisonals')
        self.metges = {}
        sql = """SELECT UP, UAB, UAB_DESCRIPCIO 
                FROM PROFESSIONALS p WHERE tipus = 'M'"""
        for up, uba, desc in u.getAll(sql, 'pdp'):
            self.metges[(up, uba, 'M')] = desc
        self.infermers = {}
        sql = """SELECT UP, UAB, UAB_DESCRIPCIO 
                FROM PROFESSIONALS p WHERE tipus = 'I'"""
        for up, uba, desc in u.getAll(sql, 'pdp'):
            self.infermers[(up, uba, 'I')] = desc
    
    def centres(self):
        print('centres')
        self.info_centres = {}
        sql = """SELECT up, fase, ics_desc FROM centres_pilots"""
        for up, fase, ics_desc in u.getAll(sql, 'pdp'):
            self.info_centres[up] = [fase, ics_desc]
        
    def imputacio_metges(self):
        print('metges')
        historics = collections.Counter()
        mirant = 0
        a_fer = set()
        sql = """SELECT DISTINCT p.hash, EXTRACT(YEAR FROM p.FADM_DATA_RECOMANADA), UP, UBA
                FROM PLANIFICAT_AUDIT_TOTAL p
                WHERE p.estat = 10
                AND to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS') < to_date('{}', 'YYYY/MM/DD')""".format(self.dia)
        for hash, year, up, metge in u.getAll(sql, 'pdp'):
            historics[(up, metge)] += 1
            mirant += 1
            a_fer.add((up, metge))

        print('mirant', mirant)
        print('len a fer', len(a_fer))
        d = collections.defaultdict(dict)
        sql = """SELECT up, uba, estat, count(1), to_date('{}', 'YYYY/MM/DD')
                FROM PLANIFICAT_AUDIT_total t 
                JOIN 
                (SELECT hash, max(to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS')) estat_data
                FROM planificat_audit_total
                WHERE to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS') < to_date('{}', 'YYYY/MM/DD')
                GROUP BY hash) m 
                ON m.hash=t.hash AND m.estat_data = to_date(t.estat_data, 'YYYY/MM/DD HH24:MI:SS')
                WHERE m.hash IN (SELECT c_cip FROM SEGUENT_VISITA_CRONICS svc)
                AND estat != 1
                GROUP BY up, uba, estat""".format(self.dia, self.dia)
        for up, metge, estat, n, dia in u.getAll(sql, 'pdp'):
            d[(up, metge, 'M',dia)][estat] = n
        print('1')
        print(len(d))

        sql = """SELECT up, uba, to_date('{}', 'YYYY/MM/DD'), 
                    MONTHS_BETWEEN(svc.DATA_RECOMANADA, to_date('{}', 'YYYY/MM/DD'))
                    FROM PLANIFICAT_AUDIT_total t 
                    JOIN 
                    (SELECT hash, max(to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS')) estat_data
                    FROM planificat_audit_total
                    WHERE to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS') < to_date('{}', 'YYYY/MM/DD')
                    GROUP BY hash) m 
                    ON m.hash=t.hash AND m.estat_data = to_date(t.estat_data, 'YYYY/MM/DD HH24:MI:SS')
                    INNER JOIN 
                    SEGUENT_VISITA_CRONICS svc
                    ON svc.C_CIP = m.hash 
                    WHERE estat = 1""".format(self.dia, self.dia, self.dia)
        for up, metge, dia, mesos in u.getAll(sql, 'pdp'):
            if mesos < 3: 
                try:
                    d[(up, metge, 'M', dia )]['1_0'] += 1
                except:
                    d[(up, metge, 'M', dia )]['1_0'] = 1
            elif mesos >= 3 and mesos < 6: 
                try:
                    d[(up, metge, 'M', dia )]['1_3'] += 1
                except:
                    d[(up, metge, 'M', dia )]['1_3'] = 1
            elif mesos >= 6 and mesos < 9: 
                try:
                    d[(up, metge, 'M', dia )]['1_6'] += 1
                except:
                    d[(up, metge, 'M', dia )]['1_6'] = 1
            elif mesos >= 9: 
                try:
                    d[(up, metge, 'M', dia )]['1_9'] += 1
                except:
                    d[(up, metge, 'M', dia )]['1_9'] = 1

        self.upload = []

        print('2')
        print(len(d))
        comptant = 0
        fets = set()
        for e in d.keys():
            try: e1_0 = d[e]['1_0']
            except: e1_0 = 0
            try: e1_3 = d[e]['1_3']
            except: e1_3 = 0
            try: e1_6 = d[e]['1_6']
            except: e1_6 = 0
            try: e1_9 = d[e]['1_9']
            except: e1_9 = 0
            try: e2 = d[e][2]
            except: e2 = 0
            try: e3 = d[e][3]
            except: e3 = 0
            try: e4 = d[e][4]
            except: e4 = 0
            try: e5 = d[e][5]
            except: e5 = 0
            try: e6 = d[e][6]
            except: e6 = 0
            try: e7 = d[e][7]
            except: e7 = 0
            try: e10 = d[e][10]
            except: e10 = 0
            try: 
                h = historics[(e[0], e[1])]
                fets.add((e[0], e[1]))
            except: h = 0
            comptant += h
            N = e1_0+e1_3+e1_6+e1_9+e2+e3+e4+e5+e6+e7+e10
            try:
                up = e[0]
                uba = e[1]
                fase = self.info_centres[up][0]
                desc = self.metges[(up, uba, 'M')]
                self.upload.append((e[0], e[1], e[2], fase, e2, 
                        e3, e4, e5, e6, e7, e10, N, e[3], 
                        e1_0, e1_3, e1_6, e1_9, desc, h))
            except:
                pass
        print('comptant', comptant)
        print('fets', len(fets))
        print('diff', a_fer - fets)
    
    def imputacio_infermers(self):
        print('infermers')
        historics = collections.Counter()
        mirant = 0
        a_fer = set()
        sql = """SELECT DISTINCT p.hash, EXTRACT(YEAR FROM p.FADM_DATA_RECOMANADA), UP, UBA_INF
                FROM PLANIFICAT_AUDIT_TOTAL p
                WHERE p.estat = 10
                AND to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS') < to_date('{}', 'YYYY/MM/DD')""".format(self.dia)
        for hash, year, up, metge in u.getAll(sql, 'pdp'):
            historics[(up, metge)] += 1
            mirant += 1
            a_fer.add((up, metge))

        print('mirant', mirant)
        print('len a fer', len(a_fer))
        d = collections.defaultdict(dict)
        sql = """SELECT up, uba_INF, estat, count(1), to_date('{}', 'YYYY/MM/DD')
                FROM PLANIFICAT_AUDIT_total t 
                JOIN 
                (SELECT hash, max(to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS')) estat_data
                FROM planificat_audit_total
                WHERE to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS') < to_date('{}', 'YYYY/MM/DD')
                GROUP BY hash) m 
                ON m.hash=t.hash AND m.estat_data = to_date(t.estat_data, 'YYYY/MM/DD HH24:MI:SS')
                WHERE m.hash IN (SELECT c_cip FROM SEGUENT_VISITA_CRONICS svc)
                AND estat != 1
                GROUP BY up, uba_INF, estat""".format(self.dia, self.dia)
        for up, metge, estat, n, dia in u.getAll(sql, 'pdp'):
            d[(up, metge, 'I',dia)][estat] = n
        print('1')
        print(len(d))

        sql = """SELECT up, uba_INF, to_date('{}', 'YYYY/MM/DD'), 
                    MONTHS_BETWEEN(svc.DATA_RECOMANADA, to_date('{}', 'YYYY/MM/DD'))
                    FROM PLANIFICAT_AUDIT_total t 
                    JOIN 
                    (SELECT hash, max(to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS')) estat_data
                    FROM planificat_audit_total
                    WHERE to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS') < to_date('{}', 'YYYY/MM/DD')
                    GROUP BY hash) m 
                    ON m.hash=t.hash AND m.estat_data = to_date(t.estat_data, 'YYYY/MM/DD HH24:MI:SS')
                    INNER JOIN 
                    SEGUENT_VISITA_CRONICS svc
                    ON svc.C_CIP = m.hash 
                    WHERE estat = 1""".format(self.dia, self.dia, self.dia)
        for up, metge, dia, mesos in u.getAll(sql, 'pdp'):
            if mesos < 3: 
                try:
                    d[(up, metge, 'I', dia )]['1_0'] += 1
                except:
                    d[(up, metge, 'I', dia )]['1_0'] = 1
            elif mesos >= 3 and mesos < 6: 
                try:
                    d[(up, metge, 'I', dia )]['1_3'] += 1
                except:
                    d[(up, metge, 'I', dia )]['1_3'] = 1
            elif mesos >= 6 and mesos < 9: 
                try:
                    d[(up, metge, 'I', dia )]['1_6'] += 1
                except:
                    d[(up, metge, 'I', dia )]['1_6'] = 1
            elif mesos >= 9: 
                try:
                    d[(up, metge, 'I', dia )]['1_9'] += 1
                except:
                    d[(up, metge, 'I', dia )]['1_9'] = 1

        print('2')
        print(len(d))
        comptant = 0
        fets = set()
        for e in d.keys():
            try: e1_0 = d[e]['1_0']
            except: e1_0 = 0
            try: e1_3 = d[e]['1_3']
            except: e1_3 = 0
            try: e1_6 = d[e]['1_6']
            except: e1_6 = 0
            try: e1_9 = d[e]['1_9']
            except: e1_9 = 0
            try: e2 = d[e][2]
            except: e2 = 0
            try: e3 = d[e][3]
            except: e3 = 0
            try: e4 = d[e][4]
            except: e4 = 0
            try: e5 = d[e][5]
            except: e5 = 0
            try: e6 = d[e][6]
            except: e6 = 0
            try: e7 = d[e][7]
            except: e7 = 0
            try: e10 = d[e][10]
            except: e10 = 0
            try: 
                h = historics[(e[0], e[1])]
                fets.add((e[0], e[1]))
            except: h = 0
            comptant += h
            N = e1_0+e1_3+e1_6+e1_9+e2+e3+e4+e5+e6+e7+e10
            try:
                up = e[0]
                uba = e[1]
                fase = self.info_centres[up][0]
                desc = self.infermers[(up, uba, 'I')]
                self.upload.append((e[0], e[1], e[2], fase, e2, 
                        e3, e4, e5, e6, e7, e10, N, e[3], 
                        e1_0, e1_3, e1_6, e1_9, desc, h))
            except:
                pass
        print('comptant', comptant)
        print('fets', len(fets))
        print('diff', a_fer - fets)
    
    def penjar(self):
        print('pengem')
        u.listToTable(self.upload, 'analisi_planificat', 'pdp')


class Imputacio_Infermeria():
    def __init__(self, dia):
        self.dia = dia
        self.professionals()
        self.centres()
        self.imputacio_infermers()
        self.penjar()

    def professionals(self):
        print('profesisonals')
        self.infermers = {}
        sql = """SELECT UP, UAB, UAB_DESCRIPCIO 
                FROM PROFESSIONALS p WHERE tipus = 'I'"""
        for up, uba, desc in u.getAll(sql, 'pdp'):
            self.infermers[(up, uba, 'I')] = desc
    
    def centres(self):
        print('centres')
        self.info_centres = {}
        sql = """SELECT up, fase, ics_desc FROM centres_pilots"""
        for up, fase, ics_desc in u.getAll(sql, 'pdp'):
            self.info_centres[up] = [fase, ics_desc]
    
    def imputacio_infermers(self):
        print('infermers')
        historics = collections.Counter()
        mirant = 0
        a_fer = set()
        sql = """SELECT DISTINCT p.hash, EXTRACT(YEAR FROM p.FADM_DATA_RECOMANADA), UP, UBA_INF
                FROM PLANIFICAT_AUDIT_TOTAL p
                WHERE p.estat = 10
                AND to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS') < to_date('{}', 'YYYY/MM/DD')""".format(self.dia)
        for hash, year, up, metge in u.getAll(sql, 'pdp'):
            historics[(up, metge)] += 1
            mirant += 1
            a_fer.add((up, metge))

        print('mirant', mirant)
        print('len a fer', len(a_fer))
        d = collections.defaultdict(dict)
        sql = """SELECT up, uba_INF, estat, count(1), to_date('{}', 'YYYY/MM/DD')
                    FROM PLANIFICAT_AUDIT_total t 
                    JOIN 
                    (SELECT hash, max(to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS')) estat_data
                    FROM planificat_audit_total
                    WHERE to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS') < to_date('{}', 'YYYY/MM/DD')
                    GROUP BY hash) m 
                    ON m.hash=t.hash AND m.estat_data = to_date(t.estat_data, 'YYYY/MM/DD HH24:MI:SS')
                    AND estat != 1
                    GROUP BY up, uba_INF, estat""".format(self.dia, self.dia)
        for up, metge, estat, n, dia in u.getAll(sql, 'pdp'):
            d[(up, metge, 'I',dia)][estat] = n
        print('1')
        print(len(d))

        sql = """SELECT up, uba_INF, to_date('{}', 'YYYY/MM/DD'), 
                    FROM PLANIFICAT_AUDIT_total t 
                    JOIN 
                    (SELECT hash, max(to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS')) estat_data
                    FROM planificat_audit_total
                    WHERE to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS') < to_date('{}', 'YYYY/MM/DD')
                    GROUP BY hash) m 
                    ON m.hash=t.hash AND m.estat_data = to_date(t.estat_data, 'YYYY/MM/DD HH24:MI:SS')
                    WHERE estat = 1""".format(self.dia, self.dia, self.dia)
        for up, metge, dia, mesos in u.getAll(sql, 'pdp'):
            if mesos < 3: 
                try:
                    d[(up, metge, 'I', dia )]['1_0'] += 1
                except:
                    d[(up, metge, 'I', dia )]['1_0'] = 1
            elif mesos >= 3 and mesos < 6: 
                try:
                    d[(up, metge, 'I', dia )]['1_3'] += 1
                except:
                    d[(up, metge, 'I', dia )]['1_3'] = 1
            elif mesos >= 6 and mesos < 9: 
                try:
                    d[(up, metge, 'I', dia )]['1_6'] += 1
                except:
                    d[(up, metge, 'I', dia )]['1_6'] = 1
            elif mesos >= 9: 
                try:
                    d[(up, metge, 'I', dia )]['1_9'] += 1
                except:
                    d[(up, metge, 'I', dia )]['1_9'] = 1

        print('2')
        print(len(d))
        comptant = 0
        fets = set()
        for e in d.keys():
            try: e1_0 = d[e]['1_0']
            except: e1_0 = 0
            try: e1_3 = d[e]['1_3']
            except: e1_3 = 0
            try: e1_6 = d[e]['1_6']
            except: e1_6 = 0
            try: e1_9 = d[e]['1_9']
            except: e1_9 = 0
            try: e2 = d[e][2]
            except: e2 = 0
            try: e3 = d[e][3]
            except: e3 = 0
            try: e4 = d[e][4]
            except: e4 = 0
            try: e5 = d[e][5]
            except: e5 = 0
            try: e6 = d[e][6]
            except: e6 = 0
            try: e7 = d[e][7]
            except: e7 = 0
            try: e10 = d[e][10]
            except: e10 = 0
            try: 
                h = historics[(e[0], e[1])]
                fets.add((e[0], e[1]))
            except: h = 0
            comptant += h
            N = e1_0+e1_3+e1_6+e1_9+e2+e3+e4+e5+e6+e7+e10
            try:
                up = e[0]
                uba = e[1]
                fase = self.info_centres[up][0]
                desc = self.infermers[(up, uba, 'I')]
                self.upload.append((e[0], e[1], e[2], fase, e2, 
                        e3, e4, e5, e6, e7, e10, N, e[3], 
                        e1_0, e1_3, e1_6, e1_9, desc, h))
            except:
                pass
        print('comptant', comptant)
        print('fets', len(fets))
        print('diff', a_fer - fets)
    
    def penjar(self):
        print('pengem')
        u.listToTable(self.upload, 'analisi_planificat', 'pdp')


if __name__ == "__main__":
    Analisi_planificat('2022/05/14')
