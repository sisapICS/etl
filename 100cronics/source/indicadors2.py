# -*- coding: utf8 -*-

import sisapUtils as u
import collections as c
import sisaptools as t


TALL = False  # ex: '2022-01-01' o False: si anem al dia.

def get_dextraccio():
        db = 'nodrizas'
        if TALL:
            data_ext = TALL
        else:
            data_ext = [row.strftime('%Y-%m-%d')
                            for row, in u.getAll(
                                "select data_ext from dextraccio",
                                db)
                        ][0]
        return data_ext

def get_codi_indicador(bloc, periode, motiu):
    if bloc == 1:
        if periode == 'MENSUAL' and motiu == 'CON':
            return 'PLA001'
        elif periode == 'ANUAL' and motiu == 'CON':
            return 'PLA002'
        elif periode == 'MENSUAL' and motiu == 'SEG':
            return 'PLA003'
        elif periode == 'ANUAL' and motiu == 'SEG':
            return 'PLA004'
    elif bloc == 2:
        if periode == 'MENSUAL' and motiu == 'TOTAL':
            return 'PLA006'
        elif periode == 'ANUAL' and motiu == 'TOTAL':
            return 'PLA007'
        elif periode == 'MENSUAL' and motiu == 'VISITES':
            return 'PLA006A'
        elif periode == 'ANUAL' and motiu == 'VISITES':
            return 'PLA007A'
        elif periode == 'MENSUAL' and motiu == 'PLANIFICAT':
            return 'PLA006B'
        elif periode == 'ANUAL' and motiu == 'PLANIFICAT':
            return 'PLA007B'
    elif bloc == 3:
        if motiu == 'M':
            return 'PLA008'
        elif motiu == 'I':
            return 'PLA009'
        else: 
            return 'PLA010'
    elif bloc == 4:
        if periode == 1 and motiu == 4:
            return 'PLA011A'
        elif periode == 0 and motiu == 4:
            return 'PLA012A'
        elif periode == 1 and motiu == 2:
            return 'PLA011B'
        elif periode == 0 and motiu == 2:
            return 'PLA012B'
    elif bloc == 5:
        if periode == 'MENSUAL' and motiu == 'CON':
            return 'ADMPLA01A'
        elif periode == 'MENSUAL' and motiu == 'SEG':
            return 'ADMPLA01B'
        elif periode == 'ANUAL' and motiu == 'CON':
            return 'ADMPLA02A'
        elif periode == 'ANUAL' and motiu == 'SEG':
            return 'ADMPLA02B'
        

class Planificat():
    def __init__(self):
        self.get_centres()
        self.get_administratius()
        self.create_table()
        self.get_patinet_info()
        self.get_hash_converter()
        self.get_data()
        self.first_block()
        self.second_block()
        self.third_block()
        self.fourth_block()
        self.pantalla_admins()
        self.uploading()

    def get_centres(self):
        print('centres')
        self.centres = {}
        sql = """select ics_codi AS br, up, fase from DWSISAP.CENTRES_PILOTS"""
        self.ups_planificat = set()
        for br, up, fase in u.getAll(sql, 'exadata'):
            self.ups_planificat.add(up)
            self.centres[up] = [br, fase]

    def get_administratius(self):
        print('admis')
        dni_user = {}
        self.uas_to_adm = {}
        sql = """SELECT DNI, HASH, UP, uas FROM DWSISAP.ASSIGNADA_ADMINISTRATIUS"""
        for dni, hash, up, uas in u.getAll(sql, 'exadata'):
            if up in self.centres:
                br = self.centres[up][0]
                dni_user[dni] = br + 'A' + hash
                self.uas_to_adm[(up, uas)] = br + 'A' + hash
        self.login_user = {}
        sql = """SELECT LOGIN, NIF FROM DWSISAP.CAT_ADMINISTRATIUS"""
        for login, dni in u.getAll(sql, 'exadata'):
            if dni in dni_user:
                self.login_user[login] = dni_user[dni]

    def create_table(self):
        print('taula')
        cols = """(indicador varchar(50), periode varchar(10), up varchar(15), 
                    uba varchar(19), prof varchar(5),
                    analisi varchar(10), grup int, valor decimal(10,3))"""
        u.createTable('pla_indi', cols, 'altres', rm=True)
    
    def get_patinet_info(self):
        print('geting dbs')
        self.patients = {}
        sql = """SELECT c_cip, c_up, c_metge, C_INFERMERA, c_uas, grup 
                    FROM dwsisap.dbs
                    WHERE grup > 0
                    and C_INSTITUCIONALITZAT IS null"""
        for hash, up, metge, inf, uas, grup in u.getAll(sql, 'exadata'):
            self.patients[hash] = (up, metge, inf, uas, grup)

    def get_hash_converter(self):
        print('hash converter')
        sql = """SELECT hash_redics, hash_covid FROM dwsisap.PDPTB101_RELACIO"""
        self.hash_redics_2_covid = {}
        for hash_r, hash_c in u.getAll(sql, 'exadata'):
            self.hash_redics_2_covid[hash_r] = hash_c

    def get_data(self):
        print('grep data')
        """Per equip i per administratiu mirar el nombre de pacients programats per mes"""
        self.n_pacients = c.defaultdict(set)
        self.n_visites = c.Counter()
        self.n_prof = c.defaultdict(set)
        self.mesos_espera = c.defaultdict(lambda: c.defaultdict(dict))
        self.intriduits_ultim_mes, self.intriduits_no_ultim_mes = set(), set()
        self.actius = c.defaultdict(set)
        self.cobertura_total = c.defaultdict(lambda: c.defaultdict(set))
        self.pacients_coberts_pmp = set()
        self.last_batch = c.defaultdict(set)
        self.cobertures = c.Counter()

        sql = """SELECT up, USUARI as login, motiu_prior, pacient, 
                    round(MONTHS_BETWEEN(DATA, PETICIO)),
                    CASE WHEN motiu_prior LIKE '%PLANIFICAT>CON>INF%' THEN 'INF'
                        WHEN motiu_prior LIKE '%PLANIFICAT>CON>MG%' THEN 'MG'
                        END CONJ,
                    case when MONTHS_BETWEEN(DATE '{tall}', PETICIO) BETWEEN 0 AND 1 then 1 else 0 end last_month,
                    case when MONTHS_BETWEEN(DATE '{tall}', PETICIO) BETWEEN 0 AND 3 then 1 else 0 end activament
                    FROM DWSISAP.SISAP_MASTER_VISITES A
                    WHERE (MOTIU_PRIOR LIKE '%PLANIFICAT>SEG%' OR motiu_prior LIKE '%PLANIFICAT>CON%')
                    and MONTHS_BETWEEN(DATE '{tall}', PETICIO) 
                    BETWEEN 0 AND 12""".format(tall=TALL if TALL else get_dextraccio())
        print(sql)
        for up, login, motiu, pacient, mesos, prof_conj, last_month, activament in u.getAll(sql, 'exadata'):
            if up in self.ups_planificat:
                if pacient in self.patients:
                    up_a, metge, inf, uas, grup =  self.patients[pacient]
                    # MENSUAL
                    if last_month == 1:
                        self.intriduits_ultim_mes.add(pacient)
                        if login in self.login_user:
                            br_a = self.login_user[login]
                            self.cobertura_total['VISITES'][('MENSUAL', up_a, br_a, 'A', grup)].add(pacient)
                            self.n_pacients[('MENSUAL', 'ADMI', up_a, br_a, grup)].add(pacient)
                            self.n_visites[('MENSUAL', 'ADMI', up_a, br_a, motiu[11:14], grup, up)] += 1
                            self.n_visites[('MENSUAL', 'I', up_a, inf, motiu[11:14], grup, up)] += 1
                            self.n_visites[('MENSUAL', 'M', up_a, metge, motiu[11:14], grup, up)] += 1
                            self.n_prof[('MENSUAL', motiu[11:14], up)].add(br_a)
                        if motiu[11:14] == 'SEG': 
                            self.n_prof[('MENSUAL', motiu[11:14], up)].add(login)
                        elif motiu[11:14] == 'CON': 
                            self.n_prof[('MENSUAL', motiu[11:14], up)].add(login)
                        self.n_visites[('MENSUAL', 'UP', up_a, '', motiu[11:14], grup, '')] += 1
                        self.cobertura_total['VISITES'][('MENSUAL', up_a, metge, 'M', grup)].add(pacient)
                        self.cobertura_total['VISITES'][('MENSUAL', up_a, inf, 'I', grup)].add(pacient)
                        self.n_pacients[('MENSUAL', 'M', up_a, metge, grup)].add(pacient)
                        self.n_pacients[('MENSUAL', 'I', up_a, inf, grup)].add(pacient)
                        self.n_pacients[('MENSUAL', 'ADMI_UAS', up_a, uas, grup)].add(pacient)
                    else:
                        self.intriduits_no_ultim_mes.add(pacient)
                        if pacient in self.intriduits_ultim_mes:
                            self.intriduits_ultim_mes.remove(pacient)
                    # ANUAL
                    self.pacients_coberts_pmp.add(pacient)
                    if login in self.login_user:
                        br_a = self.login_user[login]
                        self.cobertura_total['VISITES'][('ANUAL', up_a, br_a, 'A', grup)].add(pacient)
                        self.n_prof[('ANUAL', motiu[11:14], up)].add(br_a)
                        self.n_pacients[('ANUAL', 'ADMI', br_a, '', grup)].add(pacient)
                        self.n_visites[('ANUAL', 'ADMI', up_a, br_a, motiu[11:14], grup, up)] += 1
                        self.n_visites[('ANUAL', 'I', up_a, inf, motiu[11:14], grup, up)] += 1
                        self.n_visites[('ANUAL', 'M', up_a, metge, motiu[11:14], grup, up)] += 1
                    if motiu[11:14] == 'SEG': 
                        self.n_prof[('ANUAL', motiu[11:14], up)].add(login)
                    elif motiu[11:14] == 'CON': 
                        self.n_prof[('ANUAL', motiu[11:14], up)].add(login)
                        self.mesos_espera[(up, grup)][pacient][prof_conj] =  mesos
                    self.n_visites[('ANUAL', 'UP', up_a, '', motiu[11:14], grup, '')] += 1
                    self.cobertura_total['VISITES'][('ANUAL', up_a, metge, 'M', grup)].add(pacient)
                    self.cobertura_total['VISITES'][('ANUAL', up_a, inf, 'I', grup)].add(pacient)
                    self.n_pacients[('ANUAL', 'M', up_a, metge, grup)].add(pacient)
                    self.n_pacients[('ANUAL', 'I', up_a, inf, grup)].add(pacient)
                    self.n_pacients[('ANUAL', 'ADMI_UAS', up_a, uas, grup)].add(pacient)
                    # pacients programats en els ultims 3 mesos
                    if activament == 1:
                        if login in self.login_user:
                            br_a = self.login_user[login]
                            self.actius[('ADMI',up_a,br_a)].add(pacient)
                        self.actius[('M',up_a,metge)].add(pacient)
                        self.actius[('I',up_a,inf)].add(pacient)
        
        sql = """SELECT 
                    hash,
                    CASE WHEN 
                            MONTHS_BETWEEN(DATE '{tall}', to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) 
                            BETWEEN 0 AND 3 THEN 1 ELSE 0 END actiu,
                    CASE WHEN 
                            MONTHS_BETWEEN(DATE '{tall}', to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) 
                            BETWEEN 0 AND 1 THEN 1 ELSE 0 END last_month,
                    ESTAT
                    FROM PLANIFICAT_AUDIT
                    WHERE MONTHS_BETWEEN(DATE '{tall}', to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) 
                    BETWEEN 0 AND 12""".format(tall=TALL if TALL else get_dextraccio())
        self.estat_planificat = c.defaultdict(set)
        estat_planificat_a = c.defaultdict(set)
        for hash_r, actiu, last_month, estat in u.getAll(sql, 'pdp'):
            if hash_r in self.hash_redics_2_covid:
                hash = self.hash_redics_2_covid[hash_r]
                if hash in self.patients:
                    up_a, metge, inf, uas, grup =  self.patients[hash]
                    if estat == 10:
                        if up_a in self.ups_planificat:
                            if hash not in self.pacients_coberts_pmp:
                                self.cobertura_total['PLANIFICAT'][('ANUAL', up_a, metge, 'M', grup)].add(hash)
                                self.cobertura_total['PLANIFICAT'][('ANUAL', up_a, inf, 'I', grup)].add(hash)
                                if last_month == 1:
                                    self.cobertura_total['PLANIFICAT'][('MENSUAL', up_a, metge, 'M', grup)].add(hash)
                                    self.cobertura_total['PLANIFICAT'][('MENSUAL', up_a, inf, 'I', grup)].add(hash)
                            if actiu == 1:
                                self.actius[('M',up_a,metge)].add(hash)
                                self.actius[('I',up_a,inf)].add(hash)
                                self.actius[('ADMI_UAS',up_a,uas)].add(hash)
                    elif estat == 2:
                        if hash not in self.last_batch[(2, last_month)]:
                            self.cobertures[(2, 0, up_a, metge, inf, uas, grup)] += 1
                            if last_month == 1: 
                                self.cobertures[(2, last_month, up_a, metge, inf, uas, grup)] += 1
                        self.last_batch[(2, 0)].add(hash)
                        self.last_batch[(2, last_month)].add(hash)
                    if estat >= 4:
                        if hash not in self.last_batch[(4, last_month)]:
                            self.cobertures[(4, 0, up_a, metge, inf, uas, grup)] += 1
                            if last_month == 1:
                                self.cobertures[(4, last_month, up_a, metge, inf, uas, grup)] += 1
                        self.last_batch[(4, 0)].add(hash)
                        self.last_batch[(4, last_month)].add(hash)

        sql = """SELECT 
                    hash,
                    CASE WHEN 
                            MONTHS_BETWEEN(DATE '{tall}', to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) 
                            BETWEEN 0 AND 3 THEN 1 ELSE 0 END actiu,
                    CASE WHEN 
                            MONTHS_BETWEEN(DATE '{tall}', to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) 
                            BETWEEN 0 AND 1 THEN 1 ELSE 0 END last_month,
                    ESTAT
                    FROM dwsisap.PLANIFICAT_AUDIT a INNER JOIN dwsisap.DBC_POBLACIO b
                    ON a.cip = b.rca_cip
                    WHERE MONTHS_BETWEEN(DATE '{tall}', to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) 
                    BETWEEN 0 AND 12""".format(tall=TALL if TALL else get_dextraccio())
        self.estat_planificat = c.defaultdict(set)
        estat_planificat_a = c.defaultdict(set)
        for hash, actiu, last_month, estat in u.getAll(sql, 'exadata'):
            if hash in self.patients:
                up_a, metge, inf, uas, grup =  self.patients[hash]
                if estat == 10:
                    if up_a in self.ups_planificat:
                        if hash not in self.pacients_coberts_pmp:
                            self.cobertura_total['PLANIFICAT'][('ANUAL', up_a, metge, 'M', grup)].add(hash)
                            self.cobertura_total['PLANIFICAT'][('ANUAL', up_a, inf, 'I', grup)].add(hash)
                            if last_month == 1:
                                self.cobertura_total['PLANIFICAT'][('MENSUAL', up_a, metge, 'M', grup)].add(hash)
                                self.cobertura_total['PLANIFICAT'][('MENSUAL', up_a, inf, 'I', grup)].add(hash)
                        if actiu == 1:
                            self.actius[('M',up_a,metge)].add(hash)
                            self.actius[('I',up_a,inf)].add(hash)
                            self.actius[('ADMI_UAS',up_a,uas)].add(hash)
                elif estat == 2:
                    if hash not in self.last_batch[(2, last_month)]:
                        self.cobertures[(2, 0, up_a, metge, inf, uas, grup)] += 1
                        if last_month == 1: 
                            self.cobertures[(2, last_month, up_a, metge, inf, uas, grup)] += 1
                    self.last_batch[(2, 0)].add(hash)
                    self.last_batch[(2, last_month)].add(hash)
                if estat >= 4:
                    if hash not in self.last_batch[(4, last_month)]:
                        self.cobertures[(4, 0, up_a, metge, inf, uas, grup)] += 1
                        if last_month == 1:
                            self.cobertures[(4, last_month, up_a, metge, inf, uas, grup)] += 1
                    self.last_batch[(4, 0)].add(hash)
                    self.last_batch[(4, last_month)].add(hash)

        
        print('---------we got the data----------')
        print('self.n_pacients ', len(self.n_pacients ))
        print('self.n_visites', len(self.n_visites))
        print('self.n_prof', len(self.n_prof))
        print('self.mesos_espera', len(self.mesos_espera))
        print('self.intriduits_ultim_mes', len(self.intriduits_ultim_mes))
        print('self.intriduits_no_ultim_mes', len(self.intriduits_no_ultim_mes))
        print('self.actius', len(self.actius))
        print('self.cobertura_total', len(self.cobertura_total))
        print('self.cobertures', len(self.cobertures))

    def first_block(self):
        # Indicadors de PLANVISITES: nombre de visites programades amb X criteris
        print('1,2,3,4')
        self.upload = []
        # Per administratius
        # Mensual
        # Aprofitem per calcular els d'administratius també (els q van a l'altra pantalla)
        self.indi_pantalla_adm = c.Counter()
        print(len(self.n_visites))
        for (periode, prof, up, uba, motiu, grup, up_p), v in self.n_visites.items():
            indicador = get_codi_indicador(1, periode, motiu)
            self.upload.append((indicador, periode, up, uba, prof, 'NUM', grup, v))
            if periode == 'MENUSAL' and motiu == 'CON' and prof == 'ADMI':
                self.indi_pantalla_adm[(periode, motiu, up)] += 1
            elif periode == 'MENUSAL' and motiu == 'SEG' and prof == 'ADMI':
                self.indi_pantalla_adm[(periode, motiu, up)] += 1

        print('5')
        # PLA005: mitjana de mesos entre la visita conjunta i el dia de peticio d'aquesta
        print(len(self.mesos_espera))
        mitjana_mesos = c.Counter()
        visites_con = c.Counter()
        for (up, grup) in self.mesos_espera:
            for pacient in self.mesos_espera[(up, grup)]:
                prof_conj = self.mesos_espera[(up, grup)][pacient].keys()
                visites_con[(up, grup)] += 1
                if 'INF' in prof_conj: mitjana_mesos[(up, grup)] += self.mesos_espera[(up, grup)][pacient]['INF']
                elif 'MG' in prof_conj: mitjana_mesos[(up, grup)] += self.mesos_espera[(up, grup)][pacient]['MG']
        for (up, grup) in mitjana_mesos:
            m = mitjana_mesos[(up, grup)]
            v = visites_con[(up, grup)]
            self.upload.append(('PLA005', 'MENSUAL', up, '', 'T', 'NUM', grup, m))
            self.upload.append(('PLA005', 'MENSUAL', up, '', 'T', 'DEN', grup, v))

    def second_block(self):
        print('6-7')
        cobertura = c.Counter()
        # cal fer-ho aixp pq modifiquem diccionari dintre de for
        for motiu in list(self.cobertura_total.keys()):
            print(motiu)
            for (periode, up, uba, prof, grup) in self.cobertura_total[motiu]:
                if motiu != 'TOTAL':
                    pacients = self.cobertura_total[motiu][(periode, up, uba, prof, grup)]
                    if up in self.centres:
                        br, fase = self.centres[up]
                        indicador = get_codi_indicador(2, periode, motiu)
                        self.upload.append((indicador, periode, br, uba, prof, 'NUM', grup, len(pacients)))
                    for p in pacients:
                        self.cobertura_total['TOTAL'][(periode, up, uba, prof, grup)].add(p)
        
        for (periode, up, uba, prof, grup), pacients in self.cobertura_total['TOTAL'].items():
            if up in self.centres:
                br, fase = self.centres[up]
                indicador = get_codi_indicador(2, periode, 'TOTAL')
                self.upload.append((indicador, periode, br, uba, prof, 'NUM', grup, len(pacients)))
                if prof == 'M':
                    cobertura[(br, grup)] += len(pacients)
        
        # denominadors
        poblacio = c.Counter()
        for hash, (up, metge, inf, uas, grup) in self.patients.items():
            poblacio['M', up, metge, grup] +=1 
            poblacio['I', up, inf, grup] +=1 
            if (up, uas) in self.uas_to_adm:
                adm = self.uas_to_adm[(up, uas)] 
                poblacio['A', up, uas, grup] +=1 
        
        for (prof, up, uba, grup), v in poblacio.items():
            if up in self.centres:
                br, fase = self.centres[up]
                for (indi, periode) in (('PLA006','MENSUAL'),
                                        ('PLA007','ANUAL'),
                                        ('PLA006A','MENSUAL'),
                                        ('PLA007A','ANUAL'),
                                        ('PLA006B','MENSUAL'),
                                        ('PLA007B', 'ANUAL'),
                                        ('PLA011A', 'MENSUAL'),
                                        ('PLA011B', 'MENSUAL'),
                                        ('PLA012A', 'ANUAL'),
                                        ('PLA012B', 'ANUAL')):
                    self.upload.append((indi, periode, br, uba, prof, 'DEN', grup, v))

    def third_block(self):
        # Indicadors professionals actius
        print('8,9,10')
        prof_actius = c.Counter()
        for (prof, up, uba), pacients in self.actius.items():
            if len(pacients) > 20:
                prof_actius[(prof, up)] += 1
                    
        for (prof, up), val in prof_actius.items():
            indicador = get_codi_indicador(3, '', prof)
            self.upload.append((indicador, 'MENSUAL', up, '', prof, 'NUM', 0, val))

    def fourth_block(self):
        print('QUART BLOC')
        # 11 AND 12
        for (estat, last_month, up_a, metge, inf, uas, grup), N in self.cobertures.items():
            if up_a in self.centres:
                br, fase = self.centres[up_a]
                indi = get_codi_indicador(4, last_month, estat)
                if last_month == 1: periode = 'MENSUAL'
                else: periode = 'ANUAL'
                self.upload.append((indi, periode, br, metge, 'M', 'NUM', grup, N))
                self.upload.append((indi, periode, br, inf, 'I', 'NUM', grup, N))

    def pantalla_admins(self):
        # Indicadors pantalla administratius
        print('administratius')
        for (periode, motiu, up), admins in self.n_prof.items():
            indicador = get_codi_indicador(5, periode, motiu)
            self.upload.append((indicador, periode, up, '', 'A', 'DEN', 0, len(admins)))
        
        for (periode, motiu, up), n in self.indi_pantalla_adm.items():
            indicador = get_codi_indicador(5, periode, motiu)
            self.upload.append((indicador, periode, up, '', 'A', 'NUM', 0, n))

    def uploading(self):
        print('self.upload', len(self.upload))
        u.listToTable(self.upload, 'pla_indi', 'altres')


if __name__ == "__main__":
    Planificat()
    # try:
    #     Planificat()
    # except Exception as e:
    #     mail = t.Mail()
    #     mail.to.append("roser.cantenys@catsalut.cat")
    #     mail.subject = "Planificat!!!"
    #     mail.text = str(e)
    #     mail.send()
