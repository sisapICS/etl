
import sisapUtils as u


LANDING = False

class Absencies():
    def __init__(self):
        self.get_converters()
        cols = """(visi_id varchar2(17), cip varchar(50), 
            sector varchar2(17), up varchar2(17), uba varchar2(17), ubainf varchar2(17),
            data date, servei varchar(50), modul varchar(50), 
            tipus varchar(50), motiu_pxm varchar(500), text varchar(1000),
            centre varchar(9), classe varchar(2))"""
        u.createTable('PLANIFICAT_NOPRESENTATS', cols, 'pdp', rm=True)
        self.get_missed()
    
    def get_converters(self):
        self.covid_redics = {}
        sql = """SELECT hash_redics, hash_covid, cip FROM pdptb101_relacio"""
        for redics, covid, cip in u.getAll(sql, 'pdp'):
            self.covid_redics[covid] = (redics, cip)
        
        self.pacients = {}
        sql = """SELECT c_cip, P.c_sector, c_up, c_metge, c_infermera 
                    FROM SEGUENT_VISITA_CRONICS svc, PLANIFICAT_ECAP p 
                    WHERE SVC.C_CIP = P.HASH AND P.C_SECTOR = SVC.C_SECTOR 
                    AND P.ESTAT != 1"""
        for hash, sector, up, uba, ubainf in u.getAll(sql, 'pdp'):
            self.pacients[hash] = [sector, up, uba, ubainf]

    def get_missed(self):
        pendents = []
        sql = """SELECT visi_id,
                    pacient,
                    servei,
                    MODUL,
                    TIPUS,
                    DATA,
                    motiu_prior,
                    USUARI_TEXT,
                    centre_codi,
                    centre_classe 
                FROM
                    dwsisap.sisap_master_visites
                WHERE
                    (motiu_prior LIKE '%PLANIFICAT>CON%'
                        OR motiu_prior LIKE '%PLANIFICAT>SEG%')
                    AND DATA < TRUNC(SYSDATE)
                    AND DATA > add_months(SYSDATE,-2)
                    AND situacio != 'R'"""
        for visi_id, pacient, servei, moduul, agenda, data, motiu, text, centre, classe in u.getAll(sql, 'exadata'):
            if pacient in self.covid_redics:
                hash, cip = self.covid_redics[pacient]
                if hash in self.pacients:
                    sector, up, uba, ubainf = self.pacients[hash]
                    pendents.append((visi_id, hash, sector, up, uba, ubainf, data, servei, moduul, agenda, motiu, text, centre, classe))

        u.listToTable(pendents, 'PLANIFICAT_NOPRESENTATS', 'pdp')
        u.execute('create index pacients on PLANIFICAT_NOPRESENTATS (cip)', 'pdp')
        u.execute('create index pacients_visi on PLANIFICAT_NOPRESENTATS (cip, visi_id)', 'pdp')
        u.execute('create index sector_i on PLANIFICAT_NOPRESENTATS (sector)', 'pdp')
        u.execute('create index uba_iden on PLANIFICAT_NOPRESENTATS (sector, up, uba)', 'pdp')
        u.execute('create index ubainf_iden on PLANIFICAT_NOPRESENTATS (sector, up, ubainf)', 'pdp')
        if LANDING:
            sql = """SELECT visi_id, usua_cip, sector, up, uba, ubainf, DATA, 
                    servei, modul, tipus, motiu_pxm, text
                    FROM PLANIFICAT_NOPRESENTATS
                    INNER JOIN pdptb101 ON cip = usua_cip_cod"""
            pendents_landing = [row for row in u.getAll(sql, 'pdp')] 
            u.execute("delete from dwlanding.PLANIFICAT_NOPRESENTATS", "exadata_pre")
            u.listToTable(pendents_landing, 'dwlanding.PLANIFICAT_NOPRESENTATS', 'exadata_pre')

if __name__ == "__main__":
    Absencies()
