# coding: latin1 

import sisapUtils as u
import collections as c
from datetime import datetime


def hes_landing():
    no_tocar = [
    
    ]
    a_modificar = [

        ]

    print('here')
    u.execute('drop table planificat_landing_recover', 'exadata')
    u.execute('create table planificat_landing_recover as select * from dwsisap.planificat_landing', 'exadata')
    u.execute('grant select on planificat_landing_recover to dwsisap_rol', 'exadata')
    print('done')

    eliminats = 0
    for cip in a_modificar:
        if cip not in no_tocar:
            u.execute("delete from planificat_landing_recover where cip = '{}'".format(cip), 'exadata')
            eliminats += 1
    print('eliminats', eliminats)

    to_do = c.defaultdict()
    afegits = 0
    sql = """SELECT pa.*,  to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS') data_estat  FROM dwsisap.PLANIFICAT_AUDIT pa"""
    for row in u.getAll(sql, 'exadata'):
        cip = str(row[2])
        estat = int(row[4])
        modi = row[-1]
        registre = row
        if estat != 1:
            if cip in a_modificar:
                if cip not in no_tocar:
                    if cip in to_do:
                        if to_do[cip][-1] < modi:
                            to_do[cip] = registre
                    else:
                        to_do[cip] = registre
                        afegits += 1
    print('afegits', afegits)
    upload = []
    for cip, register in to_do.items():
        upload.append(list(register[:-1]))
    u.listToTable(upload, 'planificat_landing_recover', 'exadata')

    # u.execute("drop table planificat_landing", 'exadata')
    # u.execute("alter table planificat_landing_recover rename to planificat_landing", 'exadata')


def sisap_ecap():
    sql = """SELECT hash FROM PLANIFICAT WHERE estat = 1 AND estat_data LIKE '2024/03/20%'"""
    a_modificar = []
    for hash, in u.getAll(sql, 'pdp'):
        a_modificar.append(hash)
    
    tocar = set()
    sql = """SELECT hash_redics FROM dwsisap.PDPTB101_RELACIO WHERE HASH_COVID in(
                SELECT DISTINCT PACIENT FROM dwsisap.sisap_master_visites 
                WHERE DATA > DATE '2024-03-20' AND (motiu_PRIOR LIKE 'PLANIFICAT>%'
                OR FLAG_SISAP = 1))"""
    for hash, in u.getAll(sql, 'exadata'):
        tocar.add(hash)
    
    print('here')
    u.execute('drop table planificat_recover', 'pdp')
    u.execute('create table planificat_recover as select * from planificat', 'pdp')
    print('done')

    eliminats = 0
    for cip in a_modificar:
        if cip in tocar:
            u.execute("delete from planificat_recover where hash = '{}'".format(cip), 'pdp')
            eliminats += 1
    print('eliminats', eliminats)

    to_do = c.defaultdict()
    to_be_or_not_to_be = c.defaultdict()
    afegits = 0
    sql = """SELECT pa.*,  to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS') data_estat  FROM PLANIFICAT_AUDIT pa"""
    for row in u.getAll(sql, 'pdp'):
        cip = str(row[2])
        estat = int(row[4])
        modi = row[-1]
        registre = row
        if estat != 1:
            if cip in a_modificar:
                if cip in tocar:
                    if cip in to_do:
                        if to_do[cip][-1] < modi:
                            to_do[cip] = registre
                    else:
                        to_do[cip] = registre
                        afegits += 1
                else:
                    if cip in to_be_or_not_to_be:
                        if to_be_or_not_to_be[cip][-1] < modi:
                            to_be_or_not_to_be[cip] = registre
                    else:
                        to_be_or_not_to_be[cip] = registre

    for cip in to_be_or_not_to_be:
        if cip in a_modificar and cip not in tocar:
            registre = to_be_or_not_to_be[cip]
            estat = int(registre[4])
            if estat == 10:
                data_recomanada = registre[30]  # data
                vis1pre = datetime.strptime(str(registre[32]), '%d/%m/%Y') if registre[32] else None  # txt = dd/mm/yyyy
                vis2pre = datetime.strptime(str(registre[33]), '%d/%m/%Y') if registre[33] else None  # txt = dd/mm/yyyy
                vis3pre = datetime.strptime(str(registre[34]), '%d/%m/%Y') if registre[34] else None  # txt = dd/mm/yyyy
                vis1post = datetime.strptime(str(registre[35]), '%d/%m/%Y') if registre[35] else None  # txt = dd/mm/yyyy
                vis2post = datetime.strptime(str(registre[36]), '%d/%m/%Y') if registre[36] else None  # txt = dd/mm/yyyy
                vis3post = datetime.strptime(str(registre[37]), '%d/%m/%Y') if registre[37] else None  # txt = dd/mm/yyyy
                counting = 0
                if vis1pre is None: counting += 1
                elif vis1pre <= datetime.strptime('20/03/2024', '%d/%m/%Y'): counting += 1
                if vis2pre is None: counting += 1
                elif vis2pre <= datetime.strptime('20/03/2024', '%d/%m/%Y'): counting += 1
                if vis3pre is None: counting += 1
                elif vis3pre <= datetime.strptime('20/03/2024', '%d/%m/%Y'): counting += 1
                if vis1post is None: counting += 1
                elif vis1post <= datetime.strptime('20/03/2024', '%d/%m/%Y'): counting += 1
                if vis2post is None: counting += 1
                elif vis2post <= datetime.strptime('20/03/2024', '%d/%m/%Y'): counting += 1
                if vis3post is None: counting += 1
                elif vis3post <= datetime.strptime('20/03/2024', '%d/%m/%Y'): counting += 1
                if counting == 6:
                    u.execute("delete from planificat_recover where hash = '{}'".format(cip), 'pdp')
                    to_do[cip] = registre
                    afegits += 1

    print('afegits', afegits)
    upload = []
    for cip, register in to_do.items():
        upload.append(list(register[:-1]))
    u.listToTable(upload, 'planificat_recover', 'pdp')

    # u.execute("drop table planificat", 'pdp')
    # u.execute("alter table planificat_recover rename to planificat", 'pdp')
    

if __name__ == "__main__":
    sisap_ecap()