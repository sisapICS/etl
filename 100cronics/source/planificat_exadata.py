
import sisapUtils as u

cols = """(UP varchar2(40),UBA varchar2(40),HASH varchar2(40),C_SECTOR varchar2(40),ESTAT varchar2(40),
            ESTAT_LOGIN varchar2(40),ESTAT_DATA varchar2(40),FSAN_LOGIN varchar2(40),
            FSAN_DATA varchar2(40),FSAN_ANALITICA_DEMANADA varchar2(40),FSAN_TSH varchar2(40),
            FSAN_ESPIRO varchar2(40),FSAN_FO varchar2(40),FSAN_ECG varchar2(40),FSAN_P_SANG varchar2(40),
            FSAN_P_ORINA varchar2(40),FSAN_V_MF varchar2(40),FSAN_V_INF varchar2(40),
            FSAN_VISITA_1 varchar2(40),FSAN_VISITA_2 varchar2(40),FSAN_VISITA_3 varchar2(40),
            FSAN_VISITA_1POST varchar2(40),FSAN_VISITA_2POST varchar2(40),
            FSAN_VISITA_3POST varchar2(40),FSAN_ANALITICA_1ANT varchar2(40),
            FSAN_ANALITICA_1POST varchar2(40),FSAN_ANALITICA_2ANT varchar2(40),
            FSAN_ANALITICA_2POST varchar2(40),FADM_LOGIN varchar2(40),FADM_DATA varchar2(40),
            FADM_DATA_RECOMANADA date,FADM_GRUP int,FADM_VISITA_1 varchar2(40),
            FADM_VISITA_2 varchar2(40),FADM_VISITA_3 varchar2(40),FADM_VISITA_1POST varchar2(40),
            FADM_VISITA_2POST varchar2(40),FADM_VISITA_3POST varchar2(40),FADM_ANALITICA_1ANT varchar2(40),
            FADM_ANALITICA_1POST varchar2(40),FADM_ANALITICA_2ANT varchar2(40),
            FADM_ANALITICA_2POST varchar2(40),FADM_C_UP varchar2(40),FADM_C_METGE varchar2(40),
            FADM_C_INFERMERA varchar2(40),FADM_DX varchar2(100),FADM_ATDOM_RESIDENCIA varchar2(40),
            FADM_ANALISI_UV varchar2(40),FADM_ECG_UV varchar2(40),FADM_ESPIRO_UV varchar2(40),
            FADM_FO_UV varchar2(40),FADM_TSH varchar2(40),FADM_ESPIRO varchar2(40),FADM_FO varchar2(40),
            FADM_ECG varchar2(40),FADM_P_ANALISI varchar2(40),FADM_P_ORINA varchar2(40),
            FADM_P_SANG varchar2(40),FADM_V_MF varchar2(40),FADM_V_INF varchar2(40),
            FADM_PETICIO_FETA varchar2(400),FADM_PROTOCOL varchar2(1000),PRG_LOGIN varchar2(40),
            PRG_DATA varchar2(40),VISITA_1_CHK varchar2(40),VISITA_2_CHK varchar2(40),
            VISITA_3_CHK varchar2(40),DATA_RECOMANADA_CHK varchar2(40),
            VISITA_1POST_CHK varchar2(40),VISITA_2POST_CHK varchar2(40),
            VISITA_3POST_CHK varchar2(40),ANALITICA_1ANT_CHK varchar2(40),
            ANALITICA_1POST_CHK varchar2(40),ANALITICA_2ANT_CHK varchar2(40),
            ANALITICA_2POST_CHK varchar2(40),V_MF_CHK varchar2(40),V_INF_CHK varchar2(40),
            TSH_CHK varchar2(40),ESPIRO_CHK varchar2(40),FO_CHK varchar2(40),ECG_CHK varchar2(40),SANG_CHK varchar2(40),
            ORINA_CHK varchar2(40),SAN_COMENTARIS varchar2(500),SAN_ADM_COMENTARIS varchar2(500),
            ADM_COMENTARIS varchar2(500),
            FSAN_ANALITICA_SEG_DEMANADA varchar2(40),UBA_INF varchar2(40))"""

u.createTable('planificat', cols, 'exadata', rm=True)

upload = []
sql = """select * from planificat"""
for row in u.getAll(sql, 'pdp'):
    upload.append(row)

u.listToTable(upload, 'planificat', 'exadata')
u.grantSelect('planificat', 'DWSISAP_ROL', 'exadata')

