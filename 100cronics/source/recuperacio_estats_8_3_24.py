import sisapUtils as u
import collections as c

def prova_1():
    sql = """SELECT hash_redics
            FROM DWSISAP.SISAP_MASTER_VISITES A, dwsisap.PDPTB101_RELACIO pr 
            WHERE hash_covid = pacient AND 
            (MOTIU_PRIOR LIKE '%PLANIFICAT>SEG%' 
            OR motiu_prior LIKE '%PLANIFICAT>CON%')
            AND DATA >= sysdate"""

    pacients_amb_visites = set()
    for hash, in u.getAll(sql, 'exadata'):
        pacients_amb_visites.add(hash)


    errors = {}
    sql = """SELECT * FROM planificat_audit
            WHERE estat = 1 AND substr(estat_data, 1, 10) = '2024/03/08'"""
    for row in u.getAll(sql, 'pdp'):
        errors[row[2]] = row


    ultim_estat = c.defaultdict(dict)
    sql = """SELECT * FROM planificat_audit
        WHERE to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS') < date '2024-03-08'"""
    for row in u.getAll(sql, 'pdp'):
        ultim_estat[row[2]][row[6]] = row 

    upload = []
    for hash in errors:
        if hash not in pacients_amb_visites:
            upload.append(errors[hash])
        else:
            if hash in ultim_estat:
                ultim = max(ultim_estat[hash].keys())
                upload.append((ultim_estat[hash][ultim]))

    u.listToTable(upload, 'errors_8_3_24', 'pdp')


def prova_2():
    errors = {}
    sql = """SELECT * FROM planificat
            WHERE estat = 1 AND substr(estat_data, 1, 10) = '2024/03/08'"""
    for row in u.getAll(sql, 'pdp'):
        errors[row[2]] = row


    ultim_estat = c.defaultdict(dict)
    sql = """SELECT * FROM planificat_audit
        WHERE to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS') < date '2024-03-08'"""
    for row in u.getAll(sql, 'pdp'):
        ultim_estat[row[2]][row[6]] = row 

    upload = []
    for hash in errors:
        if hash in ultim_estat:
            ultim = max(ultim_estat[hash].keys())
            upload.append((ultim_estat[hash][ultim]))

    u.listToTable(upload, 'errors_8_3_24', 'pdp')

"""
DROP table planificat_tr_8_3;
CREATE TABLE planificat_tr_8_3 AS SELECT * FROM planificat;
SELECT * FROM planificat_tr_8_3;
DELETE FROM planificat_tr_8_3 WHERE hash IN (SELECT hash FROM errors_8_3_24) AND estat = 1;
SELECT * FROM planificat_tr_8_3;
INSERT INTO planificat_tr_8_3 SELECT * FROM errors_8_3_24;
SELECT count(1) FROM planificat_tr_8_3; -- 1015312 -- 1014881
SELECT * FROM planificat WHERE hash NOT IN (SELECT hash FROM planificat_tr_8_3);
SELECT count(1) FROM planificat;

CREATE TABLE planificat_copia_seg_13_3_24 AS SELECT * FROM planificat;
SELECT * FROM planificat_copia_seg_13_3_24 WHERE trunc(to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) = date '2024-03-08';


SELECT * FROM planificat WHERE trunc(to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) = date '2024-03-08' AND estat = 1;
ALTER TABLE planificat_tr_8_3 RENAME TO planificat
"""

if __name__ == "__main__":
    prova_2()
