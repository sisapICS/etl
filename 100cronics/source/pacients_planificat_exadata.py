# -*- coding: utf8 -*-

"""
.
"""

import sisapUtils as u
import hashlib as h
from datetime import date

def get_hash(cip):
    """."""
    return h.sha1(cip).hexdigest().upper()
    
# taking conversion
print('conversion')
hash_cip = dict()
sql = """SELECT USUA_CIP, USUA_CIP_COD FROM pdptb101"""
for cip, hash in u.getAll(sql, 'pdp'):
    hash_cip[hash] = cip

# taking cronics
print('chronics')
cronics = set()
sql = """SELECT c_cip FROM SEGUENT_VISITA_CRONICS"""
for hash, in u.getAll(sql, 'pdp'):
    if hash in hash_cip:
        cronics.add(hash_cip[hash])

print('transforming')
# transforming to exadata
hash_covid = []
for e in cronics:
    hash_covid.append((get_hash(e), date.today()))

print('upload')
cols = "(hash varchar2(40), day date)"
u.createTable('pacients_cronics', cols, 'exadata', rm=True)
u.listToTable(hash_covid, 'pacients_cronics', 'exadata')
u.grantSelect('pacients_cronics',('DWEHERMOSILLA','DWSISAP_ROL'),'exadata')

