# -*- coding: utf-8 -*-

import sisapUtils as u
import collections as c


class Enviament():
    def __init__(self):
        self.catalegs()
        self.to_terres_de_lebre()
        self.to_enginyers_catsalut()
    
    def catalegs(self):
        indicadors = {
            'PLA001': (u'Nombre de visites conjuntes programades en l\'últim mes', 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4670/ver/'),
            'PLA002': (u'Nombre de visites conjuntes programades en l\'últim any', 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4916/ver/'),
            'PLA003': (u'Nombre de visites de seguiment programades en l\'últim mes', 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4673/ver/'),
            'PLA004': (u'Nombre de visites de seguiment programades en l\'últim any', 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4917/ver/'),
            'PLA005': (u'Mitjana de mesos entre la visita conjunta i el dia de petició d\'aquesta', 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4676/ver/'),
            'PLA006': (u'Cobertura pacients Planificat en l\'últim mes', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/4918/ver/'),
            'PLA006A': (u'Cobertura pacients Planificat últim mes per visites programades (pantalla multiprogramació ECAP)', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/4919/ver/'),
            'PLA006B': (u'Cobertura pacients Planificat en l\'últim mes per ESTAT PROGRAMAT', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/4920/ver/'),
            'PLA007': (u'Cobertura de pacients Planificat en l\'últim any', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/4921/ver/'),
            'PLA007A': (u'Cobertura pacients Planificat l\'últim any per visites programades (pantalla multiprogramació ECAP)', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/4922/ver/'),
            'PLA007B': (u'Cobertura de pacients Planificat en l\'últim any per estat de programat', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/4923/ver/'),
            'PLA008': (u'Número de metges que participen activament al projecte', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/5020/ver/'),
            'PLA009': (u'Número d\'infermeres que participen activament al projecte', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/5021/ver/'),
            'PLA010': (u'Número d\'administratius que participen activament al projecte', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/5022/ver/'),
            'PLA011': (u'Cobertura mensual de pacients revisats pels sanitaris ', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/5145/ver/'),
            'PLA011A': (u'Cobertura mensual de pacients enviats a programar pels sanitaris', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/5150/ver/'),
            'PLA011B': (u'Cobertura mensual de pacients exclosos pels sanitaris', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/5149/ver/'),
            'PLA012': (u'Cobertura anual de pacients revisats pels sanitaris', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/5146/ver/'),
            'PLA012A': (u'Cobertura anual de pacients enviats a programar pels sanitaris', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/5147/ver/'),
            'PLA012B': (u'Cobertura anual de pacients exclosos pels sanitaris', 100, 'http://sisap-umi.eines.portalics/indicador/indicador/5148/ver/')
        }
        upload = []
        for indi, (desc, pct, umi) in indicadors.items():
            upload.append((indi, desc, pct, umi))
        cols = '(indicadors varchar2(15), descripcio varchar2(200), proporcio int, link_umi varchar2(500))'
        u.createTable('cataleg_indicadors_planificat', cols, 'redics', rm=True)
        print(upload)
        u.listToTable(upload, 'cataleg_indicadors_planificat', 'redics')
        u.grantSelect('cataleg_indicadors_planificat', 'pdp', 'redics')

    def to_terres_de_lebre(self):
        """
        dades de Terres de l'Ebre de les taules
        per a: caguilar.ebre.ics@gencat.cat
        """
        sql = """SELECT
                    indicador,
                    up,
                    grup,
                    num,
                    den,
                    periode
                FROM
                    indicadors_planificat_up_final
                WHERE
                    up_br IN (
                    SELECT
                        br
                    FROM
                        SISAP_COVID_DBC_CENTRES
                    WHERE
                        sector_cod = '6360')"""
        up_indi = []
        for indicador, up, grup, num, den, data in u.getAll(sql, 'redics'):
            up_indi.append((indicador, up, grup, num, den, data))
        
        file_eap = u.tempFolder + 'Seguiment_planificat_TOTAL_up.csv'
        u.writeCSV(file_eap, [('indicador', 'up', 'grup', 'num', 'den', 'data')] + up_indi, sep=";")

        sql = """SELECT
                indicador, up, tipus, uba, uba_desc, grup, num, den, periode 
            FROM
                indicadors_planificat_uba_final
            WHERE
                up_br IN (
                SELECT
                    br
                FROM
                    SISAP_COVID_DBC_CENTRES
                WHERE
                    sector_cod = '6360')"""
        uba_indi = []
        for row in u.getAll(sql, 'redics'):
            uba_indi.append(row)
        
        file_eap2 = u.tempFolder + 'Seguiment_planificat_TOTAL_uba.csv'
        u.writeCSV(file_eap2, [('indicador', 'up', 'professional', 'uba', 'descripcio', 'grup', 'num', 'den', 'data')] + uba_indi, sep=";")

        # email
        subject = 'Seguiment Planifi.cat up'
        text = "Bon dia.\n\nAdjuntem arxiu .csv amb les dades de seguiment setmanals del Planifi.cat per up.\n"
        text += "\n\nSalutacions."

        u.sendGeneral('SISAP <sisap@gencat.cat>',
                            ('caguilar.ebre.ics@gencat.cat', 'cguiriguet.bnm.ics@gencat.cat'),
                            ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat', 'mbustos.bnm.ics@gencat.cat'),
                            subject,
                            text,
                            file_eap)

        # email
        subject = 'Seguiment Planifi.cat uba'
        text = "Bon dia.\n\nAdjuntem arxiu .csv amb les dades de seguiment setmanals del Planifi.cat per uba.\n"
        text += "\n\nSalutacions."

        u.sendGeneral('SISAP <sisap@gencat.cat>',
                            ('caguilar.ebre.ics@gencat.cat', 'cguiriguet.bnm.ics@gencat.cat'),
                            ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat', 'mbustos.bnm.ics@gencat.cat'),
                            subject,
                            text,
                            file_eap2)

    def to_enginyers_catsalut(self):
        """
        dades de Terres de l'Ebre de les taules
        per a: caguilar.ebre.ics@gencat.cat
        """
        sql = """SELECT
                    indicador, up, grup, num, den, periode
                FROM
                    indicadors_planificat_up_final"""
        up_indi = []
        for indicador, up, grup, num, den, data in u.getAll(sql, 'redics'):
            up_indi.append((indicador, up, grup, num, den, data))
        
        file_eap = u.tempFolder + 'Seguiment_planificat_TOTAL_up.csv'
        u.writeCSV(file_eap, [('indicador', 'up', 'grup', 'num', 'den', 'data')] + up_indi, sep=";")

        sql = """SELECT
                indicador, up, tipus, uba, uba_desc, grup, num, den, periode 
            FROM
                indicadors_planificat_uba_final"""
        uba_indi = []
        for row in u.getAll(sql, 'redics'):
            uba_indi.append(row)
        
        file_eap2 = u.tempFolder + 'Seguiment_planificat_TOTAL_uba.csv'
        u.writeCSV(file_eap2, [('indicador', 'up', 'professional', 'uba', 'descripcio', 'grup', 'num', 'den', 'data')] + uba_indi, sep=";")

        # email
        subject = 'Seguiment Planifi.cat up'
        text = "Bon dia.\n\nAdjuntem arxiu .csv amb les dades de seguiment setmanals del Planifi.cat per up.\n"
        text += "\n\nSalutacions."

        u.sendGeneral('SISAP <sisap@gencat.cat>',
                            ('daniel.algar@catsalut.cat','cpladevall@catsalut.cat'),
                            ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat', 'mbustos.bnm.ics@gencat.cat'),
                            subject,
                            text,
                            file_eap)

        # email
        subject = 'Seguiment Planifi.cat uba'
        text = "Bon dia.\n\nAdjuntem arxiu .csv amb les dades de seguiment setmanals del Planifi.cat per uba.\n"
        text += "\n\nSalutacions."

        u.sendGeneral('SISAP <sisap@gencat.cat>',
                            ('daniel.algar@catsalut.cat','cpladevall@catsalut.cat'),
                            ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat', 'mbustos.bnm.ics@gencat.cat'),
                            subject,
                            text,
                            file_eap2)
            

if __name__ == '__main__':
    Enviament()
