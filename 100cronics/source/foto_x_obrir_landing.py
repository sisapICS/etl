import sisapUtils as u

class Estat_Planificat():
    def __init__(self):
        self.get_converter()
        self.get_pacients()

    def get_converter(self):
        self.converter = {}
        sql = """SELECT cip FROM dwsisap.rca_cip_nia"""
        for cip, in u.getAll(sql, 'exadata'):
            self.converter[cip[:13]] = cip

    def get_pacients(self):
        self.upload = []
        sql = """SELECT distinct a.usua_cip, p.* 
                FROM planificat p INNER JOIN pdptb101 a
                ON p.hash = a.usua_cip_cod
                INNER JOIN seguent_visita_cronics c
                ON p.hash = c.c_cip
                WHERE c.c_up in ('00015', '00352', '00101',
                '00102', '00016', '04704', '00340', '00151',
                '00195', '00052', '00053', '01796', '04863',
                '04374', '06175', '01790')"""
        no_trobats = 0
        with open('foto_fase3.csv', 'w') as f:
            for row in u.getAll(sql, 'pdp'):
                first = True
                linia = ''
                continua = True
                for i, r in enumerate(row):
                    if i == 0:
                        if r in self.converter:
                            r = self.converter[r]
                            linia += str(r)
                            continua = True
                        else:
                            continua = False
                            no_trobats += 1
                    elif continua:
                        if r is None:
                            linia += ','
                        elif i in (7,9,30,31,64):
                            r_i = str(r).replace('-', '/')
                            r_i = str(r_i).replace('  ', ' ')
                            r_i = r_i[:19]
                            element = ',' + '"' + str(r_i) + '"'
                            linia += element
                        elif i in (33,34,35,36,37,38,48,49,50,51):
                            r_i = str(r).replace('-', '/')
                            r_i = str(r_i).replace('  ', ' ')
                            r_i = r_i[:10]
                            element = ',' + '"' + str(r_i) + '"'
                            linia += element
                        else:
                            r_i = str(r).replace('\r\n', '\n')
                            r_i = str(r_i).replace('"', '""')
                            element = ',' + '"' + str(r_i) + '"'
                            linia += element
                if linia != '':
                    f.write(linia+'\n')
        print(no_trobats)

if __name__ == "__main__":
    Estat_Planificat()
