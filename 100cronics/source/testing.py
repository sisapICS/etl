import sisapUtils as u
import collections
import datetime as dt
d = collections.defaultdict(dict)

sql1 = """SELECT *
        from planificat p
        INNER JOIN SEGUENT_VISITA_CRONICS svc ON p.hash = svc.c_cip
        INNER JOIN centres_pilots c ON svc.c_up = c.up
        INNER JOIN professionals p2 ON svc.c_up = p2.up AND svc.c_metge = p2.uab
        WHERE p2.TIPUS = 'M' aND p.estat = 1"""
sql2 = """SELECT *
        from planificat p
        INNER JOIN SEGUENT_VISITA_CRONICS svc ON p.hash = svc.c_cip
        INNER JOIN centres_pilots c ON svc.c_up = c.up
        WHERE p.estat = 1"""
pre = dt.datetime.now()
for i, row in enumerate(u.getAll(sql1, 'pdp')):
    if i % 10**3 == 0:
        print(i, str(dt.datetime.now() - pre))
        pre = dt.datetime.now()
