# -*- coding: utf8 -*-

import sisapUtils as u
import collections as c
import sisaptools as t


TALL = False  # ex: '2022-01-01' o False: si anem al dia.

def get_dextraccio():
        db = 'nodrizas'
        if TALL:
            data_ext = TALL
        else:
            data_ext = [row.strftime('%Y-%m-%d')
                            for row, in u.getAll(
                                "select current_date from dextraccio",
                                db)
                        ][0]
        return data_ext

def get_codi_indicador(bloc, periode, motiu):
    if bloc == 1:
        if periode == 'MENSUAL' and motiu == 'CON':
            return 'PLA001'
        elif periode == 'ANUAL' and motiu == 'CON':
            return 'PLA002'
        elif periode == 'MENSUAL' and motiu == 'SEG':
            return 'PLA003'
        elif periode == 'ANUAL' and motiu == 'SEG':
            return 'PLA004'
    elif bloc == 2:
        if periode == 'MENSUAL' and motiu == 'TOTAL':
            return 'PLA006'
        elif periode == 'ANUAL' and motiu == 'TOTAL':
            return 'PLA007'
        elif periode == 'MENSUAL' and motiu == 'VISITES':
            return 'PLA006A'
        elif periode == 'ANUAL' and motiu == 'VISITES':
            return 'PLA007A'
        elif periode == 'MENSUAL' and motiu == 'PLANIFICAT':
            return 'PLA006B'
        elif periode == 'ANUAL' and motiu == 'PLANIFICAT':
            return 'PLA007B'
    elif bloc == 3:
        if motiu == 'M':
            return 'PLA008'
        elif motiu == 'I':
            return 'PLA009'
        else: 
            return 'PLA010'
    elif bloc == 4:
        if periode == 1 and motiu == 2:
            return 'PLA011A'
        elif periode == 0 and motiu == 4:
            return 'PLA012A'
        elif periode == 0 and motiu == 2:
            return 'PLA011B'
        elif periode == 1 and motiu == 4:
            return 'PLA012B'
    elif bloc == 5:
        if periode == 'MENSUAL' and motiu == 'CON':
            return 'ADMPLA01A'
        elif periode == 'MENSUAL' and motiu == 'SEG':
            return 'ADMPLA01B'
        elif periode == 'ANUAL' and motiu == 'CON':
            return 'ADMPLA02A'
        elif periode == 'ANUAL' and motiu == 'SEG':
            return 'ADMPLA02B'
        

class Planificat():
    def __init__(self):
        self.get_centres()
        self.get_administratius()
        self.create_table()
        self.get_ups_planificat()
        self.get_patinet_info()
        self.get_hash_converter()
        self.get_data()
        self.first_block()
        self.second_block()
        self.third_block()
        self.fourth_block()
        self.pantalla_admins()
        self.uploading()

    def get_centres(self):
        print('centres')
        self.centres = {}
        sql = """select ics_codi AS br, up, fase from DWSISAP.CENTRES_PILOTS"""
        for br, up, fase in u.getAll(sql, 'exadata'):
            self.centres[up] = [br, fase]

    def get_administratius(self):
        print('admis')
        dni_user = {}
        self.uas_to_adm = {}
        sql = """SELECT DNI, HASH, UP, uas FROM DWSISAP.ASSIGNADA_ADMINISTRATIUS"""
        for dni, hash, up, uas in u.getAll(sql, 'exadata'):
            if up in self.centres:
                br = self.centres[up][0]
                dni_user[dni] = br + 'A' + hash
                self.uas_to_adm[(up, uas)] = br + 'A' + hash
        self.login_user = {}
        sql = """SELECT LOGIN, NIF FROM DWSISAP.CAT_ADMINISTRATIUS"""
        for login, dni in u.getAll(sql, 'exadata'):
            if dni in dni_user:
                self.login_user[login] = dni_user[dni]

    def create_table(self):
        print('taula')
        cols = """(indicador varchar(50), periode varchar(10), up varchar(15), 
                    uba varchar(19), prof varchar(5),
                    analisi varchar(10), grup int, valor decimal(10,3))"""
        u.createTable('pla_indi_web', cols, 'altres', rm=True)

    def get_ups_planificat(self):
        print('geting ups')
        sql = """SELECT up FROM centres_pilots"""
        self.ups_planificat = set()
        for up, in u.getAll(sql, 'pdp'):
            self.ups_planificat.add(up)
    
    def get_patinet_info(self):
        print('geting dbs')
        self.patients = {}
        sql = """SELECT c_cip, c_up, c_metge, C_INFERMERA, c_uas, grup 
                    FROM dwsisap.dbs
                    WHERE grup > 0
                    and C_INSTITUCIONALITZAT IS null"""
        for hash, up, metge, inf, uas, grup in u.getAll(sql, 'exadata'):
            self.patients[hash] = (up, metge, inf, uas, grup)

    def get_hash_converter(self):
        print('hash converter')
        sql = """SELECT hash_redics, hash_covid FROM dwsisap.PDPTB101_RELACIO"""
        self.hash_redics_2_covid = {}
        for hash_r, hash_c in u.getAll(sql, 'exadata'):
            self.hash_redics_2_covid[hash_r] = hash_c

    def get_data(self):
        print('grep data')
        """Per equip i per administratiu mirar el nombre de pacients programats per mes"""
        self.n_pacients = c.defaultdict(set)
        self.n_visites = c.Counter()
        self.n_prof = c.defaultdict(set)
        self.mesos_espera = c.defaultdict(lambda: c.defaultdict(dict))
        self.intriduits_ultim_mes, self.intriduits_no_ultim_mes = set(), set()
        self.actius = c.defaultdict(set)
        self.cobertura_total = c.defaultdict(lambda: c.defaultdict(set))
        self.pacients_coberts_pmp = set()
        self.last_batch = c.defaultdict(set)
        self.cobertures = c.Counter()

        sql = """SELECT up, USUARI as login, motiu_prior, pacient, 
                    round(MONTHS_BETWEEN(DATA, PETICIO)),
                    CASE WHEN motiu_prior LIKE '%PLANIFICAT>CON>INF%' THEN 'INF'
                        WHEN motiu_prior LIKE '%PLANIFICAT>CON>MG%' THEN 'MG'
                        END CONJ,
                    case when MONTHS_BETWEEN(DATE '{tall}', PETICIO) BETWEEN 0 AND 1 then 1 else 0 end last_month,
                    case when MONTHS_BETWEEN(DATE '{tall}', PETICIO) BETWEEN 0 AND 3 then 1 else 0 end activament
                    FROM DWSISAP.SISAP_MASTER_VISITES A
                    WHERE (MOTIU_PRIOR LIKE '%PLANIFICAT>SEG%' OR motiu_prior LIKE '%PLANIFICAT>CON%')
                    and MONTHS_BETWEEN(DATE '{tall}', PETICIO) 
                    BETWEEN 0 AND 12""".format(tall=TALL if TALL else get_dextraccio())
        print(sql)
        for up, login, motiu, pacient, mesos, prof_conj, last_month, activament in u.getAll(sql, 'exadata'):
            if up in self.ups_planificat:
                if pacient in self.patients:
                    up_a, metge, inf, uas, grup =  self.patients[pacient]
                    # MENSUAL
                    if last_month == 1:
                        self.intriduits_ultim_mes.add(pacient)
                        if login in self.login_user:
                            br_a = self.login_user[login]
                            self.cobertura_total['VISITES'][('MENSUAL', up_a, br_a, 'A', grup)].add(pacient)
                            self.n_pacients[('MENSUAL', 'ADMI', up_a, br_a, grup)].add(pacient)
                            self.n_visites[('MENSUAL', 'ADMI', up_a, br_a, motiu[11:14], grup, up)] += 1
                            self.n_visites[('MENSUAL', 'I', up_a, inf, motiu[11:14], grup, up)] += 1
                            self.n_visites[('MENSUAL', 'M', up_a, metge, motiu[11:14], grup, up)] += 1
                            self.n_prof[('MENSUAL', motiu[11:14], up)].add(br_a)
                        if motiu[11:14] == 'SEG': 
                            self.n_prof[('MENSUAL', motiu[11:14], up)].add(login)
                        elif motiu[11:14] == 'CON': 
                            self.n_prof[('MENSUAL', motiu[11:14], up)].add(login)
                        self.n_visites[('MENSUAL', 'UP', up_a, '', motiu[11:14], grup, '')] += 1
                        self.cobertura_total['VISITES'][('MENSUAL', up_a, metge, 'M', grup)].add(pacient)
                        self.cobertura_total['VISITES'][('MENSUAL', up_a, inf, 'I', grup)].add(pacient)
                        self.n_pacients[('MENSUAL', 'M', up_a, metge, grup)].add(pacient)
                        self.n_pacients[('MENSUAL', 'I', up_a, inf, grup)].add(pacient)
                        self.n_pacients[('MENSUAL', 'ADMI_UAS', up_a, uas, grup)].add(pacient)
                    else:
                        self.intriduits_no_ultim_mes.add(pacient)
                        if pacient in self.intriduits_ultim_mes:
                            self.intriduits_ultim_mes.remove(pacient)
                    # ANUAL
                    self.pacients_coberts_pmp.add(pacient)
                    if login in self.login_user:
                        br_a = self.login_user[login]
                        self.cobertura_total['VISITES'][('ANUAL', up_a, br_a, 'A', grup)].add(pacient)
                        self.n_prof[('ANUAL', motiu[11:14], up)].add(br_a)
                        self.n_pacients[('ANUAL', 'ADMI', br_a, '', grup)].add(pacient)
                        self.n_visites[('ANUAL', 'ADMI', up_a, br_a, motiu[11:14], grup, up)] += 1
                        self.n_visites[('ANUAL', 'I', up_a, inf, motiu[11:14], grup, up)] += 1
                        self.n_visites[('ANUAL', 'M', up_a, metge, motiu[11:14], grup, up)] += 1
                    if motiu[11:14] == 'SEG': 
                        self.n_prof[('ANUAL', motiu[11:14], up)].add(login)
                    elif motiu[11:14] == 'CON': 
                        self.n_prof[('ANUAL', motiu[11:14], up)].add(login)
                        self.mesos_espera[(up, grup)][pacient][prof_conj] =  mesos
                    self.n_visites[('ANUAL', 'UP', up_a, '', motiu[11:14], grup, '')] += 1
                    self.cobertura_total['VISITES'][('ANUAL', up_a, metge, 'M', grup)].add(pacient)
                    self.cobertura_total['VISITES'][('ANUAL', up_a, inf, 'I', grup)].add(pacient)
                    self.n_pacients[('ANUAL', 'M', up_a, metge, grup)].add(pacient)
                    self.n_pacients[('ANUAL', 'I', up_a, inf, grup)].add(pacient)
                    self.n_pacients[('ANUAL', 'ADMI_UAS', up_a, uas, grup)].add(pacient)
                    # pacients programats en els ultims 3 mesos
                    if activament == 1:
                        if login in self.login_user:
                            br_a = self.login_user[login]
                            self.actius[('ADMI',up_a,br_a)].add(pacient)
                        self.actius[('M',up_a,metge)].add(pacient)
                        self.actius[('I',up_a,inf)].add(pacient)
        
        sql = """SELECT 
                    hash,
                    CASE WHEN 
                            MONTHS_BETWEEN(DATE '{tall}', to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) 
                            BETWEEN 0 AND 3 THEN 1 ELSE 0 END actiu,
                    CASE WHEN 
                            MONTHS_BETWEEN(DATE '{tall}', to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) 
                            BETWEEN 0 AND 1 THEN 1 ELSE 0 END last_month,
                    ESTAT
                    FROM PLANIFICAT_AUDIT
                    WHERE MONTHS_BETWEEN(DATE '{tall}', to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) 
                    BETWEEN 0 AND 12""".format(tall=TALL if TALL else get_dextraccio())
        self.estat_planificat = c.defaultdict(set)
        estat_planificat_a = c.defaultdict(set)
        for hash_r, actiu, last_month, estat in u.getAll(sql, 'pdp'):
            if hash_r in self.hash_redics_2_covid:
                hash = self.hash_redics_2_covid[hash_r]
                if hash in self.patients:
                    up_a, metge, inf, uas, grup =  self.patients[hash]
                    if estat == 10:
                        if up_a in self.ups_planificat:
                            if hash not in self.pacients_coberts_pmp:
                                self.cobertura_total['PLANIFICAT'][('ANUAL', up_a, metge, 'M', grup)].add(hash)
                                self.cobertura_total['PLANIFICAT'][('ANUAL', up_a, inf, 'I', grup)].add(hash)
                                if last_month == 1:
                                    self.cobertura_total['PLANIFICAT'][('MENSUAL', up_a, metge, 'M', grup)].add(hash)
                                    self.cobertura_total['PLANIFICAT'][('MENSUAL', up_a, inf, 'I', grup)].add(hash)
                            if actiu == 1:
                                self.actius[('M',up_a,metge)].add(hash)
                                self.actius[('I',up_a,inf)].add(hash)
                                self.actius[('ADMI_UAS',up_a,uas)].add(hash)
                    elif estat == 2:
                        if hash not in self.last_batch[(2, last_month)]:
                            self.cobertures[(2, 0, up_a, metge, inf, uas, grup)] += 1
                            if last_month == 1: 
                                self.cobertures[(2, last_month, up_a, metge, inf, uas, grup)] += 1
                        self.last_batch[(2, last_month)].add(hash)
                    if estat >= 4:
                        if hash not in self.last_batch[(4, last_month)]:
                            self.cobertures[(4, 0, up_a, metge, inf, uas, grup)] += 1
                            if last_month == 1:
                                self.cobertures[(4, last_month, up_a, metge, inf, uas, grup)] += 1
                        self.last_batch[(4, last_month)].add(hash)

        
        print('---------we got the data----------')
        print('self.n_pacients ', len(self.n_pacients ))
        print('self.n_visites', len(self.n_visites))
        print('self.n_prof', len(self.n_prof))
        print('self.mesos_espera', len(self.mesos_espera))
        print('self.intriduits_ultim_mes', len(self.intriduits_ultim_mes))
        print('self.intriduits_no_ultim_mes', len(self.intriduits_no_ultim_mes))
        print('self.actius', len(self.actius))
        print('self.cobertura_total', len(self.cobertura_total))
        print('self.cobertures', len(self.cobertures))

    def first_block(self):
        # Indicadors de PLANVISITES: nombre de visites programades amb X criteris
        print('1,2,3,4')
        self.upload = []
        # Per administratius
        # Mensual
        # Aprofitem per calcular els d'administratius també (els q van a l'altra pantalla)
        self.indi_pantalla_adm = c.Counter()
        print(len(self.n_visites))
        for (periode, prof, up, uba, motiu, grup, up_p), v in self.n_visites.items():
            indicador = get_codi_indicador(1, periode, motiu)
            self.upload.append((indicador, periode, up, uba, prof, 'NUM', grup, v))
            if periode == 'MENUSAL' and motiu == 'CON' and prof == 'ADMI':
                self.indi_pantalla_adm[(periode, motiu, up)] += 1
            elif periode == 'MENUSAL' and motiu == 'SEG' and prof == 'ADMI':
                self.indi_pantalla_adm[(periode, motiu, up)] += 1

        print('5')
        # PLA005: mitjana de mesos entre la visita conjunta i el dia de peticio d'aquesta
        print(len(self.mesos_espera))
        mitjana_mesos = c.Counter()
        visites_con = c.Counter()
        for (up, grup) in self.mesos_espera:
            for pacient in self.mesos_espera[(up, grup)]:
                prof_conj = self.mesos_espera[(up, grup)][pacient].keys()
                visites_con[(up, grup)] += 1
                if 'INF' in prof_conj: mitjana_mesos[(up, grup)] += self.mesos_espera[(up, grup)][pacient]['INF']
                elif 'MG' in prof_conj: mitjana_mesos[(up, grup)] += self.mesos_espera[(up, grup)][pacient]['MG']
        for (up, grup) in mitjana_mesos:
            m = mitjana_mesos[(up, grup)]
            v = visites_con[(up, grup)]
            self.upload.append(('PLA005', 'MENSUAL', up, '', 'T', 'NUM', grup, m))
            self.upload.append(('PLA005', 'MENSUAL', up, '', 'T', 'DEN', grup, v))

    def second_block(self):
        print('6-7')
        cobertura = c.Counter()
        # cal fer-ho aixp pq modifiquem diccionari dintre de for
        for motiu in list(self.cobertura_total.keys()):
            print(motiu)
            for (periode, up, uba, prof, grup) in self.cobertura_total[motiu]:
                if motiu != 'TOTAL':
                    pacients = self.cobertura_total[motiu][(periode, up, uba, prof, grup)]
                    if up in self.centres:
                        br, fase = self.centres[up]
                        indicador = get_codi_indicador(2, periode, motiu)
                        self.upload.append((indicador, periode, br, uba, prof, 'NUM', grup, len(pacients)))
                    for p in pacients:
                        self.cobertura_total['TOTAL'][(periode, up, uba, prof, grup)].add(p)
        
        for (periode, up, uba, prof, grup), pacients in self.cobertura_total['TOTAL'].items():
            if up in self.centres:
                br, fase = self.centres[up]
                indicador = get_codi_indicador(2, periode, 'TOTAL')
                self.upload.append((indicador, periode, br, uba, prof, 'NUM', grup, len(pacients)))
                if prof == 'M':
                    cobertura[(br, grup)] += len(pacients)
        
        # denominadors
        poblacio = c.Counter()
        for hash, (up, metge, inf, uas, grup) in self.patients.items():
            poblacio['M', up, metge, grup] +=1 
            poblacio['I', up, inf, grup] +=1 
            if (up, uas) in self.uas_to_adm:
                adm = self.uas_to_adm[(up, uas)] 
                poblacio['A', up, uas, grup] +=1 
        
        for (prof, up, uba, grup), v in poblacio.items():
            if up in self.centres:
                br, fase = self.centres[up]
                for (indi, periode) in (('PLA006','MENSUAL'),
                                        ('PLA007','ANUAL'),
                                        ('PLA006A','MENSUAL'),
                                        ('PLA007A','ANUAL'),
                                        ('PLA006B','MENSUAL'),
                                        ('PLA007B', 'ANUAL'),
                                        ('PLA011A', 'MENSUAL'),
                                        ('PLA011B', 'MENSUAL'),
                                        ('PLA012A', 'ANUAL'),
                                        ('PLA012B', 'ANUAL')):
                    self.upload.append((indi, periode, br, uba, prof, 'DEN', grup, v))

    def third_block(self):
        # Indicadors professionals actius
        print('8,9,10')
        prof_actius = c.Counter()
        for (prof, up, uba), pacients in self.actius.items():
            if len(pacients) > 20:
                prof_actius[(prof, up)] += 1
                    
        for (prof, up), val in prof_actius.items():
            indicador = get_codi_indicador(3, '', prof)
            self.upload.append((indicador, 'MENSUAL', up, '', prof, 'NUM', 0, val))

    def fourth_block(self):
        print('QUART BLOC')
        # 11 AND 12
        for (estat, last_month, up_a, metge, inf, uas, grup), N in self.cobertures.items():
            if up_a in self.centres:
                br, fase = self.centres[up_a]
                indi = get_codi_indicador(4, last_month, estat)
                if last_month == 1: periode = 'MENSUAL'
                else: periode = 'ANUAL'
                self.upload.append((indi, periode, br, metge, 'M', 'NUM', grup, N))
                self.upload.append((indi, periode, br, inf, 'I', 'NUM', grup, N))

    def pantalla_admins(self):
        # Indicadors pantalla administratius
        print('administratius')
        for (periode, motiu, up), admins in self.n_prof.items():
            indicador = get_codi_indicador(5, periode, motiu)
            self.upload.append((indicador, periode, up, '', 'A', 'DEN', 0, len(admins)))
        
        for (periode, motiu, up), n in self.indi_pantalla_adm.items():
            indicador = get_codi_indicador(5, periode, motiu)
            self.upload.append((indicador, periode, up, '', 'A', 'NUM', 0, n))

    def uploading(self):
        print('self.upload', len(self.upload))
        u.listToTable(self.upload, 'pla_indi_web', 'altres')


class Web_setmanal():
    def __init__(self):
        dext, = u.getOne("select current_date from dextraccio", 'nodrizas')
        year = str(dext.year)[2:]
        month = str(dext.month) if len(str(dext.month)) == 2 else '0' + str(dext.month)
        day = str(dext.day) if len(str(dext.day)) == 2 else '0' + str(dext.day)
        self.periode = 'A' + year + month + day
        u.execute("delete from indicadors_planificat_up_final where periode like '{}%'".format(self.periode[0:5]), 'redics')
        u.execute("delete from indicadors_planificat_uba_final where periode like '{}%'".format(self.periode[0:5]), 'redics')
        self.taula_up = 'indicadors_planificat_up_final'
        self.taula_uba = 'indicadors_planificat_uba_final'
        self.catalegs()
        self.upload_up()
        self.upload_uba()

    def catalegs(self):
        self.info_centres = {}
        sql = """select scs_codi, ics_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs from nodrizas.cat_centres"""
        for scs_codi, ics_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs in u.getAll(sql, 'nodrizas'):
            self.info_centres[ics_codi] = [scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs]
        
        self.professionals = {}
        sql = """SELECT up, uab, tipus, UAB_DESCRIPCIO  FROM professionals"""
        for up, uba, tipus, desc in u.getAll(sql, 'pdp'):
            self.professionals[(up, uba, tipus)] = desc
    
    def upload_uba(self):
        ubas = c.defaultdict(dict)
        up_uba = []
        sql = """select INDICADOR,UP, PROF, UBA, ANALISI, AGRUPATS, 
                    SUM(VALOR) from (
                    select INDICADOR, UP, UBA, PROF, ANALISI, 
                    CASE WHEN GRUP < 14 then 'PRIORITZATS' else 'NOPRIORITZATS' end AGRUPATS,
                    VALOR
                    from altres.pla_indi_web 
                    where (indicador like 'PLA006%' or indicador like 'PLA007%' 
                    or indicador like 'PLA012%' or indicador like 'PLA011%') AND prof in ('I', 'M')) A
                    group by INDICADOR, UP, PROF, UBA, ANALISI, AGRUPATS
                UNION
                select
                    INDI, UP, PROF, UBA, ANALISI, AGRUPATS, SUM(VALOR)
                from
                    (
                    select
                        distinct substr(INDICADOR, 1, 6) as INDI,
                        UP,
                        UBA,
                        PROF,
                        ANALISI,
                        case
                            when GRUP < 14 then 'PRIORITZATS'
                            else 'NOPRIORITZATS'
                        end AGRUPATS,
                        VALOR
                    from
                        altres.pla_indi_web
                    where
                        prof in ('I', 'M')
                        and indicador in ('PLA011A', 'PLA012A')
                        and analisi = 'DEN') A
                group by
                    INDI, UP, PROF, UBA, ANALISI, AGRUPATS
                union
                select
                    substr(INDICADOR, 1, 6),
                    UP, PROF, UBA,ANALISI,AGRUPATS, SUM(VALOR)
                from
                    (
                    select
                        distinct
                        INDICADOR,
                        UP,
                        UBA,
                        PROF,
                        ANALISI,
                        case
                            when GRUP < 14 then 'PRIORITZATS'
                            else 'NOPRIORITZATS'
                        end AGRUPATS,
                        VALOR
                    from
                        altres.pla_indi_web
                    where
                        prof in ('I', 'M')
                        and ANALISI = 'NUM'
                        AND (INDICADOR like 'PLA012%' or indicador like 'PLA011%')) A
                group by
                    substr(INDICADOR, 1, 6),UP, PROF, UBA,
                    ANALISI,
                    AGRUPATS
                    """
        for indicador, up, prof, uba, analisi, agrupat, n in u.getAll(sql, 'altres'):
            ubas[(indicador, up, prof, uba, agrupat)][analisi] = n
        
        for (indi, br, tipus, uba, grup), analisi_val in ubas.items():
            if br in self.info_centres:
                scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs = self.info_centres[br]
                if (scs_codi, uba, tipus) in self.professionals:
                    uba_desc = self.professionals[(scs_codi, uba, tipus)]
                    num, den = 0,0
                    for analisi, val in analisi_val.items():
                        if analisi == 'NUM': num = int(float(val))
                        elif analisi == 'DEN': den = int(float(val))
                    row = [indi, self.periode, br, uba, tipus, uba_desc, scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs, grup, num, den]
                    up_uba.append((row))
        u.listToTable(up_uba, self.taula_uba, 'redics')

    def upload_up(self):
        ups = c.defaultdict(dict)
        up_up = []
        sql = """select
                INDICADOR,
                b.ics_codi,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                VALOR
            from
                altres.pla_indi_web a,
                nodrizas.cat_centres b
            where
                a.up = b.scs_codi
                and
                ((prof = 'UP'
                and indicador in ('PLA001', 'PLA002', 'PLA003', 'PLA004'))
                or
                (prof = 'T'
                and indicador = 'PLA005'))
            union
                                select
                INDICADOR,
                up,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                SUM(VALOR)
            from
                altres.pla_indi_web a
            where
                prof = 'M'
                and (indicador like 'PLA006%' or indicador like 'PLA007%'
                or indicador like 'PLA011%' or indicador like 'PLA012%')
            group by
                INDICADOR,
                UP,
                ANALISI,
                GRUP
            union
                                select
                INDICADOR,
                b.ics_codi,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                sum(VALOR)
            from
                altres.pla_indi_web a,
                nodrizas.cat_centres b
            where
                a.up = b.scs_codi and
                INDICADOR in ('PLA008', 'PLA009', 'PLA010')
            group by INDICADOR,
                b.ics_codi,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end
            union
            select
                substr(INDICADOR,1,6) as INDI,
                up,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                SUM(VALOR)
            from
                altres.pla_indi_web a
            where
                prof = 'M'
                and (indicador like 'PLA011%' or indicador like 'PLA012%')
                and analisi = 'NUM'
            group by
                substr(INDICADOR,1,6),
                UP,
                ANALISI,
                GRUP
            union
            select
                substr(INDICADOR,1,6) as INDI,
                up,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                SUM(VALOR)
            from
                altres.pla_indi_web a
            where
                prof = 'M'
                and (indicador IN ('PLA011A', 'PLA012A'))
                and analisi = 'DEN'
            group by
                substr(INDICADOR,1,6),
                UP,
                ANALISI,
                GRUP
                """
        for indicador, up, analisi, agrupat, n in u.getAll(sql, 'altres'):
            ups[(indicador, up, agrupat)][analisi] = n
        
        for (indi, br, grup), analisi_val in ups.items():
            if br in self.info_centres:
                scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs = self.info_centres[br]
                num, den = 0,0
                for analisi, val in analisi_val.items():
                    if analisi == 'NUM': num = int(float(val))
                    elif analisi == 'DEN': den = int(float(val))
                row = [indi, self.periode, br, scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs, grup, num, den]
                up_up.append((row))
        u.listToTable(up_up, self.taula_up, 'redics')      


if __name__ == "__main__":
    Planificat()
    Web_setmanal()
