import sisapUtils as u
import collections as c


class Web_historic():
    def __init__(self):
        self.create_tables()
        self.catalegs()
        self.get_up()
        self.get_uba()
        self.transform_and_upload()
    
    def create_tables(self):
        cols_up = """(indicador varchar2(20), periode varchar2(20), up_br varchar2(20), up varchar2(20), 
                    up_desc varchar2(40), sap_codi varchar2(20), sap_desc varchar2(40), amb_codi varchar2(20), 
                    amb_desc varchar2(40), aga varchar2(20), rs varchar2(20), grup varchar2(20), num int, den int)"""
        self.taula_up = 'indicadors_planificat_up_final'
        u.createTable(self.taula_up, cols_up, 'redics', rm=True)
        u.grantSelect(self.taula_up, 'pdp', 'redics')
        cols_uba = """(indicador varchar2(20), periode varchar2(20), up_br varchar2(20), uba varchar2(20), tipus varchar2(20), uba_desc varchar2(80), up varchar2(20), 
                    up_desc varchar2(40), sap_codi varchar2(20), sap_desc varchar2(40), amb_codi varchar2(20), 
                    amb_desc varchar2(40), aga varchar2(20), rs varchar2(20), grup varchar2(20), num int, den int)"""
        self.taula_uba = 'indicadors_planificat_uba_final'
        u.createTable(self.taula_uba, cols_uba, 'redics', rm=True)
        u.grantSelect(self.taula_uba, 'pdp', 'redics')

    def catalegs(self):
        self.info_centres = {}
        sql = """select scs_codi, ics_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs from nodrizas.cat_centres"""
        for scs_codi, ics_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs in u.getAll(sql, 'nodrizas'):
            self.info_centres[ics_codi] = [scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs]
        
        self.professionals = {}
        sql = """SELECT up, uab, tipus, UAB_DESCRIPCIO  FROM professionals"""
        for up, uba, tipus, desc in u.getAll(sql, 'pdp'):
            self.professionals[(up, uba, tipus)] = desc

    def get_up(self):
        self.ups = c.defaultdict(dict)
        query = "PLANIFPROG#99;AYR23###,AYR22###;AMBITOS###;NUM,DEN;PLAGRUP###;NOIMP;DIM6SET"
        for ind, _a, br, analisi, _c, tipprof, _d, value in u.LvClient().query(query):
            self.ups[(ind, _a, br, _c, tipprof, _d)][analisi] =  value

    def get_uba(self):
        sql = "select a.sym_name, b.description \
            from icsap.klx_master_symbol a \
            inner join icsap.klx_sym_desc b \
                    on a.dim_index = b.minor_obj_id and a.sym_index = b.micro_obj_id \
            where dim_index = 2 and lang_code = 'FR'"
        conversio = {row[0]: row[1] for row in u.getAll(sql, "khalix")}
        self.ubas = c.defaultdict(dict)
        query = "PLANIFPROG#99;AYR23###,AYR22###;PAMBITOS###;NUM,DEN;PLANIFICATGRUPS###;NOIMP;DIM6SET"
        for ind, _a, br, analisi, _c, tipprof, _d, value in u.LvClient().query(query):
            if br in conversio:
                uba = conversio[br]
                self.ubas[(ind, _a, uba, _c, tipprof, _d)][analisi] =  value

    def transform_and_upload(self):
        upload_up = []
        for (ind, periode, br, grup, tipprof, _d), analisi_val in self.ups.items():
            if br in self.info_centres:
                scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs = self.info_centres[br]
                num, den = 0,0
                for analisi, val in analisi_val.items():
                    if analisi == 'NUM': num = int(float(val))
                    elif analisi == 'DEN': den = int(float(val))
                row = [ind, periode, br, scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs, grup, num, den]
                upload_up.append((row))
        u.listToTable(upload_up, self.taula_up, 'redics')
        upload_uba = []
        for (ind, periode, ubatotal, grup, tipprof, _d), analisi_val in self.ubas.items():
            if len(ubatotal) > 5:
                br = ubatotal[0:5]
                tipus = ubatotal[5]
                uba = ubatotal[6:]
                if br in self.info_centres:
                    scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs = self.info_centres[br]
                    if (scs_codi, uba, tipus) in self.professionals:
                        uba_desc = self.professionals[(scs_codi, uba, tipus)]
                        num, den = 0,0
                        for analisi, val in analisi_val.items():
                            if analisi == 'NUM': num = int(float(val))
                            elif analisi == 'DEN': den = int(float(val))
                        row = [ind, periode, br, uba, tipus, uba_desc, scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs, grup, num, den]
                        upload_uba.append((row))
        u.listToTable(upload_uba, self.taula_uba, 'redics')

class Web():
    def __init__(self):
        dext, mensual = u.getOne("select data_ext, mensual from dextraccio", 'nodrizas')
        year = str(dext.year)[2:]
        month = str(dext.month) if len(str(dext.month)) == 2 else '0' + str(dext.month)
        day = str(dext.day) if len(str(dext.day)) == 2 else '0' + str(dext.day)
        self.periode = 'A' + year + month if mensual else 'A' + year + month + day
        u.execute("delete from indicadors_planificat_up_final where periode like '{}%'".format(self.periode[0:5]), 'redics')
        u.execute("delete from indicadors_planificat_uba_final where periode like '{}%'".format(self.periode[0:5]), 'redics')
        self.taula_up = 'indicadors_planificat_up_final'
        self.taula_uba = 'indicadors_planificat_uba_final'
        self.catalegs()
        self.upload_up()
        self.upload_uba()

    def catalegs(self):
        self.info_centres = {}
        sql = """select scs_codi, ics_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs from nodrizas.cat_centres"""
        for scs_codi, ics_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs in u.getAll(sql, 'nodrizas'):
            self.info_centres[ics_codi] = [scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs]
        
        self.professionals = {}
        sql = """SELECT up, uab, tipus, UAB_DESCRIPCIO  FROM professionals"""
        for up, uba, tipus, desc in u.getAll(sql, 'pdp'):
            self.professionals[(up, uba, tipus)] = desc
    
    def upload_uba(self):
        ubas = c.defaultdict(dict)
        up_uba = []
        sql = """select INDICADOR,UP, PROF, UBA, ANALISI, AGRUPATS, 
                    SUM(VALOR) from (
                    select INDICADOR, UP, UBA, PROF, ANALISI, 
                    CASE WHEN GRUP < 14 then 'PRIORITZATS' else 'NOPRIORITZATS' end AGRUPATS,
                    VALOR
                    from altres.pla_indi 
                    where (indicador like 'PLA006%' or indicador like 'PLA007%' 
                    or indicador like 'PLA012%' or indicador like 'PLA011%') AND prof in ('I', 'M')) A
                    group by INDICADOR, UP, PROF, UBA, ANALISI, AGRUPATS
                UNION
                select
                    INDI, UP, PROF, UBA, ANALISI, AGRUPATS, SUM(VALOR)
                from
                    (
                    select
                        distinct substr(INDICADOR, 1, 6) as INDI,
                        UP,
                        UBA,
                        PROF,
                        ANALISI,
                        case
                            when GRUP < 14 then 'PRIORITZATS'
                            else 'NOPRIORITZATS'
                        end AGRUPATS,
                        VALOR
                    from
                        altres.pla_indi
                    where
                        prof in ('I', 'M')
                        and indicador in ('PLA011A', 'PLA012A')
                        and analisi = 'DEN') A
                group by
                    INDI, UP, PROF, UBA, ANALISI, AGRUPATS
                union
                select
                    substr(INDICADOR, 1, 6),
                    UP, PROF, UBA,ANALISI,AGRUPATS, SUM(VALOR)
                from
                    (
                    select
                        distinct
                        INDICADOR,
                        UP,
                        UBA,
                        PROF,
                        ANALISI,
                        case
                            when GRUP < 14 then 'PRIORITZATS'
                            else 'NOPRIORITZATS'
                        end AGRUPATS,
                        VALOR
                    from
                        altres.pla_indi
                    where
                        prof in ('I', 'M')
                        and ANALISI = 'NUM'
                        AND (INDICADOR like 'PLA012%' or indicador like 'PLA011%')) A
                group by
                    substr(INDICADOR, 1, 6),UP, PROF, UBA,
                    ANALISI,
                    AGRUPATS
                    """
        for indicador, up, prof, uba, analisi, agrupat, n in u.getAll(sql, 'altres'):
            ubas[(indicador, up, prof, uba, agrupat)][analisi] = n
        
        for (indi, br, tipus, uba, grup), analisi_val in ubas.items():
            if br in self.info_centres:
                scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs = self.info_centres[br]
                if (scs_codi, uba, tipus) in self.professionals:
                    uba_desc = self.professionals[(scs_codi, uba, tipus)]
                    num, den = 0,0
                    for analisi, val in analisi_val.items():
                        if analisi == 'NUM': num = int(float(val))
                        elif analisi == 'DEN': den = int(float(val))
                    row = [indi, self.periode, br, uba, tipus, uba_desc, scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs, grup, num, den]
                    up_uba.append((row))
        u.listToTable(up_uba, self.taula_uba, 'redics')

    def upload_up(self):
        ups = c.defaultdict(dict)
        up_up = []
        sql = """select
                INDICADOR,
                b.ics_codi,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                VALOR
            from
                altres.pla_indi a,
                nodrizas.cat_centres b
            where
                a.up = b.scs_codi
                and
                ((prof = 'UP'
                and indicador in ('PLA001', 'PLA002', 'PLA003', 'PLA004'))
                or
                (prof = 'T'
                and indicador = 'PLA005'))
            union
                                select
                INDICADOR,
                up,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                SUM(VALOR)
            from
                altres.pla_indi a
            where
                prof = 'M'
                and (indicador like 'PLA006%' or indicador like 'PLA007%'
                or indicador like 'PLA011%' or indicador like 'PLA012%')
            group by
                INDICADOR,
                UP,
                ANALISI,
                GRUP
            union
                                select
                INDICADOR,
                b.ics_codi,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                sum(VALOR)
            from
                altres.pla_indi a,
                nodrizas.cat_centres b
            where
                a.up = b.scs_codi and
                INDICADOR in ('PLA008', 'PLA009', 'PLA010')
            group by INDICADOR,
                b.ics_codi,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end
            union
            select
                substr(INDICADOR,1,6) as INDI,
                up,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                SUM(VALOR)
            from
                altres.pla_indi a
            where
                prof = 'M'
                and (indicador like 'PLA011%' or indicador like 'PLA012%')
                and analisi = 'NUM'
            group by
                substr(INDICADOR,1,6),
                UP,
                ANALISI,
                GRUP
            union
            select
                substr(INDICADOR,1,6) as INDI,
                up,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                SUM(VALOR)
            from
                altres.pla_indi a
            where
                prof = 'M'
                and (indicador IN ('PLA011A', 'PLA012A'))
                and analisi = 'DEN'
            group by
                substr(INDICADOR,1,6),
                UP,
                ANALISI,
                GRUP
                """
        for indicador, up, analisi, agrupat, n in u.getAll(sql, 'altres'):
            ups[(indicador, up, agrupat)][analisi] = n
        
        for (indi, br, grup), analisi_val in ups.items():
            if br in self.info_centres:
                scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs = self.info_centres[br]
                num, den = 0,0
                for analisi, val in analisi_val.items():
                    if analisi == 'NUM': num = int(float(val))
                    elif analisi == 'DEN': den = int(float(val))
                row = [indi, self.periode, br, scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc, aga, rs, grup, num, den]
                up_up.append((row))
        u.listToTable(up_up, self.taula_up, 'redics')

if __name__ == "__main__":
    historic = False
    if historic:
        Web_historic() 
    else: Web()       
