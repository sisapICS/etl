# -*- coding: utf-8 -*-

"""
.
"""

import sisapUtils as u
import collections as c

# cols = '(up varchar(5), prof varchar(20), servei varchar(20), peticio date, n int)'
# u.createTable('peticions', cols, 'test', rm=True)

# distribucio_peticions = c.Counter()
# sql = """SELECT pacient, peticio, up, sisap_SERVEI_class, professional 
#             FROM dwsisap.sisap_master_visites
#             where DATA BETWEEN to_date('2019-01-01', 'YYYY-MM-DD')
#                     AND to_date('2019-12-31', 'YYYY-MM-DD')
#             and sisap_SERVEI_class IN ('MF', 'URG')
#             and ATRIBUTS_AGENDA_COD in('1.1','2.1', '2.4')"""

# for p, peticio, up, servei, prof in u.getAll(sql, 'exadata'):
#     distribucio_peticions[(peticio, up, servei, prof)] += 1

# upload = []
# for i, ((peticio, up, servei, prof), n) in enumerate(distribucio_peticions.items()):
#     upload.append((up, prof, servei, peticio, n))
#     if i % 10000 == 0:
#         u.listToTable(upload, 'peticions', 'test')
#         upload = []

# u.listToTable(upload, 'peticions', 'test')

# cols = '(peticio date, up varchar(5), uba varchar(10), servei varchar(20),  n int)'
# u.createTable('joguina_2', cols, 'test', rm=True)

# covid_2_redics = {}
# sql = """SELECT hash_redics, hash_covid FROM dwsisap.pdptb101_relacio"""
# for redics, covid in u.getAll(sql, 'exadata'):
#     covid_2_redics[covid] = redics

# dbs = {}
# sql = """SELECT c_cip, c_up, c_metge  FROM dbs"""
# for p, up, uba in u.getAll(sql, 'redics'):
#     dbs[p] = (up, uba)

# distribucio_peticions = c.Counter()
# sql = """SELECT pacient, peticio, up, servei FROM dwsisap.sisap_master_visites
#             WHERE SERVEI IN ('MG', 'URGEN')
#             AND EXTRACT(YEAR FROM PETICIO) = 2022"""

# for p, peticio, up, servei in u.getAll(sql, 'exadata'):
#     if p in covid_2_redics:
#         p = covid_2_redics[p]
#         if p in dbs:
#             up, uba = dbs[p]
#             distribucio_peticions[(peticio, up, uba, servei)] += 1

# upload = []
# for i, ((peticio, up, uba, servei), n) in enumerate(distribucio_peticions.items()):
#     upload.append((peticio, up, uba, servei, n))
#     if i % 10000 == 0:
#         u.listToTable(upload, 'joguina_2', 'test')
#         upload = []

# u.listToTable(upload, 'joguina_2', 'test')

# upload = []
# sql = """SELECT * FROM PACIENTS_GRUPS_NUT"""
# for row in u.getAll(sql, 'pdp'):
#     upload.append(row)
# u.createTable('pacients_grups_nut', '(hash varchar2(40))', 'exadata', rm=True)
# u.listToTable(upload, 'pacients_grups_nut', 'exadata')
# u.grantSelect('pacients_grups_nut',('DWSISAP_ROL'), 'exadata')

# upload = []
# sql = """SELECT * FROM PACIENTS_GRUPS_REBE"""
# for row in u.getAll(sql, 'pdp'):
#     upload.append(row)
# u.createTable('pacients_grups_rebe', '(hash varchar2(40))', 'exadata', rm=True)
# u.listToTable(upload, 'pacients_grups_rebe', 'exadata')
# u.grantSelect('pacients_grups_rebe',('DWSISAP_ROL'), 'exadata')

# u.createTable('caducitats', '(cip varchar(15), up varchar(5), estat varchar(50), N int)', 'test', rm=True)

# caducats = c.Counter()
# sql = """select alv_cip, alv_uab_up, alv_text4
#                     from prstb550
#                     where alv_tipus='RES'
#                     AND alv_text4 < 31"""
# for s in u.sectors:
#     print(s)
#     for cip, up, data in u.getAll(sql, s):
#         data = int(data)
#         if data < 0:
#             caducats[(cip, up, 'caducat')] += 1
#         elif data >=0 and data <= 10:
#             caducats[(cip, up, 'menys10')] += 1
#         elif data > 10 and data <= 30:
#             caducats[(cip, up, 'entre10i30')] += 1

# upload = []
# for k, v in caducats.items():
#     cip, up, estat = k
#     upload.append((cip, up, estat, v))

# u.listToTable(upload, 'caducitats', 'test')

# u.createTable('colests', '(id int, num int)', 'test', rm=True)

# si_2022 = set()
# sql = """select id_cip_sec from nodrizas.eqa_variables
#             where agrupador = 197
#             and year(data_var) = 2022"""
# for id, in u.getAll(sql, 'import'):
#     si_2022.add(id)

# users_col = c.Counter()
# sql = """select id_cip_sec from nodrizas.eqa_variables
#             where agrupador = 197
#             and year(data_var) > 2017"""
# for id, in u.getAll(sql, 'import'):   
#     if id in si_2022:
#         users_col[id] += 1

# upload = []
# for id, num in users_col.items():
#     upload.append((id, num))

# u.listToTable(upload, 'colests', 'test')

# u.createTable('colesterols_n', '(grup int, visites int, pacients int)', 'test', rm=True)

# hash_grup = {}
# sql = """SELECT c_cip, grup FROM dbs"""
# for hash, grup in u.getAll(sql, 'redics'):
#     hash_grup[hash] = grup

# cip_2_hash = {}
# sql = """select id_cip_sec, hash_d from import.u11"""
# for cip, hash in u.getAll(sql, 'import'):
#     cip_2_hash[cip] = hash

# cip_nums = {}
# sql = """select id, num from test.colests"""
# for cip, num in u.getAll(sql, 'test'):
#     cip_nums[cip] = num

# counting = c.Counter()
# for cip, n in cip_nums.items():
#     if cip in cip_2_hash:
#         hash = cip_2_hash[cip]
#         if hash in hash_grup:
#             grup = hash_grup[hash]
#             counting[(grup, n)] += 1

# upload = []
# for (grup, n), v in counting.items():
#     upload.append((grup, n, v))

# u.listToTable(upload, 'colesterols_n', 'test')


# -*- coding: utf-8 -*-

# """
# .
# """

# import sisapUtils as u
# import collections as c
# import csv
# import math
# import multiprocessing as m
# import os
# import sisaptools as t

# CONNEXIO = ("p2262", "pedia")

# DETECCIO = {"EQD0910": ("OBESITAT", ("EQA1104",)),  # noqa
#             "EQD0911": ("ASMA", ("EQA1101",))}
# LV_POB = "exp_khalix_up_pob"
# LV_UP = "exp_khalix_up_ind"
# LV_DET = "exp_khalix_up_det"
# ES_BASAL = True

# class EQA():
#     def __init__(self):
#         # detecci�
#         self.deteccio_inv = c.defaultdict(list)
#         self.deteccio_lit = []
#         for nou, (literal, antics) in DETECCIO.items():
#             for antic in antics:
#                 self.deteccio_inv[antic].append(nou)
#             self.deteccio_lit.append((nou, literal))
#         this = os.path.dirname(os.path.realpath(__file__))
#         that = "/prevalences.txt"
#         self.deteccio_file = this.replace("source", "dades_noesb") + that

#     def get_prev_anual(self):
#         """."""
#         dades = c.defaultdict(c.Counter)
#         ind = tuple(self.deteccio_inv.keys())
#         sql = """select a.indicador, nvl(b.medea, '2U'), a.edat, a.comb,
#                         a.sexe, a.n, c.n
#                  from {} a
#                  inner join nodrizas.cat_centres b on a.up = b.scs_codi
#                  inner join {}
# 						c on a.up = c.up and a.edat = c.edat and
#                         a.comb = c.comb and a.sexe = c.sexe
#                  where indicador in {} and
#                        conc = 'DEN'""".format(LV_UP, LV_POB, ind)
#         for row in t.Database(*CONNEXIO).get_all(sql):
#             for nou in self.deteccio_inv[row[0]]:
#                 dades[(nou,) + row[1:-2]]["num"] += row[-2]
#                 dades[(nou,) + row[1:-2]]["den"] += row[-1]
#         upload = [key + (vals["num"] / float(vals["den"]),)
#                   for key, vals in dades.items()]
#         t.TextFile(self.deteccio_file).write_iterable(upload, delimiter="{",
#                                                       endline="\r\n")
    
#     def get_lv_deteccio(self):
#         """."""
#         # detectats
#         dades = c.Counter()
#         ind = tuple(self.deteccio_inv.keys())
#         sql = """select a.up, a.indicador, nvl(b.medea, '2U'), a.edat, a.comb,
#                         a.sexe, a.n
#                     from {} a
#                     inner join nodrizas.cat_centres b
#                                on a.up = b.scs_codi
#                     where indicador in {} and
#                           conc = 'DEN'""".format(LV_UP, ind)
#         for row in t.Database(*CONNEXIO).get_all(sql):
#             for nou in self.deteccio_inv[row[1]]:
#                 key = (row[0], row[3], row[5], row[4], nou, "NUM")
#                 dades[key] += row[-1]
#         upload = [k + (v,) for (k, v) in dades.items()]
#         # esperats
#         poblacio = c.defaultdict(dict)
#         sql = """select a.up, nvl(b.medea, '2U'), a.edat, a.comb, a.sexe, a.n
#                  from {} a
#                  inner join nodrizas.cat_centres b
#                             on a.up = b.scs_codi""".format(LV_POB)  # noqa
#         for up, medea, edat, pob, sex, n in t.Database(*CONNEXIO).get_all(sql):
#             poblacio[(medea, edat, pob, sex)][up] = n
#         for ind, medea, edat, pob, sex, prev in csv.reader(open(self.deteccio_file), delimiter="{"):  # noqa
#             ups = poblacio[(medea, edat, pob, sex)]
#             for up, n in ups.items():
#                 upload.append((up, edat, sex, pob, ind, "DEN", n * float(prev)))  # noqa
#         # upload
#         cols = ("up varchar(5)", "edat varchar(10)", "sexe varchar(4)",
#                 "comb varchar(10)", "indicador varchar(10)",
#                 "conc varchar(10)", "n double")
#         with t.Database(*CONNEXIO) as conn:
#             conn.create_table(LV_DET, cols, remove=True)
#             conn.list_to_table(upload, LV_DET)

import sisapUtils as u


class COMPTATGES():
    def __init__(self):
        self.get_data()
        self.create_tables()
        self.upload()
    
    def get_data(self):
        self.upload1 = []
        with open('ACTASSIST_YTD_12_2022.txt') as f:
            for line in f:
                u = line.strip().split('{')
                u[-1] = int(u[-1])
                u = tuple(u)
                self.upload1.append(u)
                
        self.upload2 = []
        with open('ACTASSIST_YTD_12_2022_B.txt') as f:
            for line in f:
                u = line.strip().split('{')
                u[-1] = int(u[-1])
                u = tuple(u)
                self.upload2.append(u)

        self.upload3 = []
        with open('ACTASSIST_YTD_12_2022_C.txt') as f:
            for line in f:
                u = line.strip().split('{')
                u[-1] = int(u[-1])
                u = tuple(u)
                self.upload3.append(u)

    def create_tables(self):
        cols = '(cuentas varchar(20), periode varchar(5), br varchar(5), nocli varchar(20), edat varchar(20), noimp varchar(20), sexe varchar(20), n varchar(1), val int)'
        u.createTable('ACT_ASSISTENCIAL_A', cols, 'test', rm=True)
        u.createTable('ACT_ASSISTENCIAL_B', cols, 'test', rm=True)
        u.createTable('ACT_ASSISTENCIAL_C', cols, 'test', rm=True)

    def upload(self):
        u.listToTable(self.upload1, 'ACT_ASSISTENCIAL_A', 'test')
        u.listToTable(self.upload2, 'ACT_ASSISTENCIAL_B', 'test')
        u.listToTable(self.upload3, 'ACT_ASSISTENCIAL_C', 'test')



if __name__ == "__main__":
    COMPTATGES()
    # eqa = EQA()
    # # eqa.get_master()
    # # eqa.get_lv_up()
    # # eqa.get_lv_pob()
    # if ES_BASAL:
    #     eqa.get_prev_anual()
    # eqa.get_lv_deteccio()
    # # eqa.get_ecap_uba()
    # # eqa.get_lv_uba()
    # # eqa.get_ecap_pacients()
    # # eqa.get_ecap_catalegs()                                                      