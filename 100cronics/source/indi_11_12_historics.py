# -*- coding: utf8 -*-

import sisapUtils as u
import collections as c
import sisaptools as t


def get_codi_indicador(bloc, periode, motiu):
    if bloc == 1:
        if periode == 'MENSUAL' and motiu == 'CON':
            return 'PLA001'
        elif periode == 'ANUAL' and motiu == 'CON':
            return 'PLA002'
        elif periode == 'MENSUAL' and motiu == 'SEG':
            return 'PLA003'
        elif periode == 'ANUAL' and motiu == 'SEG':
            return 'PLA004'
    elif bloc == 2:
        if periode == 'MENSUAL' and motiu == 'TOTAL':
            return 'PLA006'
        elif periode == 'ANUAL' and motiu == 'TOTAL':
            return 'PLA007'
        elif periode == 'MENSUAL' and motiu == 'VISITES':
            return 'PLA006A'
        elif periode == 'ANUAL' and motiu == 'VISITES':
            return 'PLA007A'
        elif periode == 'MENSUAL' and motiu == 'PLANIFICAT':
            return 'PLA006B'
        elif periode == 'ANUAL' and motiu == 'PLANIFICAT':
            return 'PLA007B'
    elif bloc == 3:
        if motiu == 'M':
            return 'PLA008'
        elif motiu == 'I':
            return 'PLA009'
        else: 
            return 'PLA010'
    elif bloc == 4:
        if periode == 1 and motiu == 4:
            return 'PLA011A'
        elif periode == 0 and motiu == 4:
            return 'PLA012A'
        elif periode == 1 and motiu == 2:
            return 'PLA011B'
        elif periode == 0 and motiu == 2:
            return 'PLA012B'
    elif bloc == 5:
        if periode == 'MENSUAL' and motiu == 'CON':
            return 'ADMPLA01A'
        elif periode == 'MENSUAL' and motiu == 'SEG':
            return 'ADMPLA01B'
        elif periode == 'ANUAL' and motiu == 'CON':
            return 'ADMPLA02A'
        elif periode == 'ANUAL' and motiu == 'SEG':
            return 'ADMPLA02B'
        

class Planificat():
    def __init__(self):
        self.get_centres()
        self.create_table()
        self.get_ups_planificat()
        self.get_patinet_info()
        for year in (2022, 2023):
            for month in range(1,13):
                if year == 2022 or (year == 2023 and month < 10):
                    if month in (1,3,5,7,8,10,12): day = 31
                    elif month in (4,5,9,11): day = 30
                    else: day = 28
                    self.month = str(month) if len(str(month)) == 2 else '0' + str(month)
                    self.TALL = '{}-{}-{}'.format(year, self.month, day)
                    self.year = str(year)[2:]
                    self.khalix_p = 'A' + self.year + self.month
                    print(self.TALL)
                    # self.get_administratius()
                    self.get_data()
                    self.fourth_block()
                    self.uploading()
    
    def get_administratius(self):
        dni_user = {}
        self.uas_to_adm = {}
        sql = """SELECT distinct uba.hash, uba.up, uba.uas, admi.nom, admi.cognom_1, admi.cognom_2 
                    FROM dwsisap.cat_administratius_uba_h uba, dwsisap.CAT_ADMINISTRATIUS_h admi 
                    WHERE uba.dni = admi.nif
                    and EXTRACT(MONTH FROM uba.DATA) = {month}
                    AND extract(YEAR FROM uba.data) = {year}
                    AND EXTRACT(MONTH FROM admi.DATA) = {month}
                    AND extract(YEAR FROM admi.data) = {year}"""
        if self.year == '23':
            year = 2023
            month = int(self.month)
        elif self.year == '22':
            year = 2023
            if int(self.month) < 9:
                month = 9
            else:
                month = int(self.month)
        sql = sql.format(year=year, month=month)
        for hash, up, uas, nom, cognom1, cognom2 in u.getAll(sql, 'exadata'):
            self.uas_to_adm[(up, uas)] = (hash, nom + ' ' + cognom1 + ' ' + cognom2)

    def get_centres(self):
        print('centres')
        self.centres = {}
        sql = """select ics_codi AS br, up, fase from DWSISAP.CENTRES_PILOTS"""
        for br, up, fase in u.getAll(sql, 'exadata'):
            self.centres[up] = [br, fase]

    def create_table(self):
        print('taula')
        cols = """(indicador varchar(50), periode varchar(10), carrega varchar(10), up varchar(15), 
                    uba varchar(19), prof varchar(5),
                    analisi varchar(10), grup int, valor decimal(10,3))"""
        u.createTable('pla_indi_historic', cols, 'altres', rm=True)

    def get_ups_planificat(self):
        print('geting ups')
        sql = """SELECT up FROM centres_pilots"""
        self.ups_planificat = set()
        for up, in u.getAll(sql, 'pdp'):
            self.ups_planificat.add(up)
    
    def get_patinet_info(self):
        print('geting dbs')
        self.patients = {}
        self.poblacio = c.Counter()
        sql = """SELECT c_cip, c_up, c_metge, C_INFERMERA, c_uas, grup 
                    FROM dbs
                    WHERE grup > 0
                    and C_INSTITUCIONALITZAT IS null"""
        for hash, up, metge, inf, uas, grup in u.getAll(sql, 'redics'):
            self.patients[hash] = (up, metge, inf, uas, grup)
            self.poblacio['M', up, metge, grup] +=1 
            self.poblacio['I', up, inf, grup] +=1 

    def get_data(self):
        print('grep data')
        """Per equip i per administratiu mirar el nombre de pacients programats per mes"""
        self.cobertures = c.Counter()
        self.last_batch = c.defaultdict(set)
        
        sql = """SELECT 
                    hash,
                    CASE WHEN 
                            MONTHS_BETWEEN(DATE '{tall}', to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) 
                            BETWEEN 0 AND 3 THEN 1 ELSE 0 END actiu,
                    CASE WHEN 
                            MONTHS_BETWEEN(DATE '{tall}', to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) 
                            BETWEEN 0 AND 1 THEN 1 ELSE 0 END last_month,
                    ESTAT
                    FROM PLANIFICAT_AUDIT
                    WHERE MONTHS_BETWEEN(DATE '{tall}', to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS')) 
                    BETWEEN 0 AND 12""".format(tall=self.TALL)
        print(sql)
        for hash, actiu, last_month, estat in u.getAll(sql, 'pdp'):
            if hash in self.patients:
                up_a, metge, inf, uas, grup =  self.patients[hash]
                if estat == 2:
                    if hash not in self.last_batch[(2, last_month)]:
                        self.cobertures[(2, 0, up_a, metge, inf, uas, grup)] += 1
                        if last_month == 1: 
                            self.cobertures[(2, last_month, up_a, metge, inf, uas, grup)] += 1
                    self.last_batch[(2, 0)].add(hash)
                    self.last_batch[(2, last_month)].add(hash)
                if estat >= 4:
                    if hash not in self.last_batch[(4, last_month)]:
                        self.cobertures[(4, 0, up_a, metge, inf, uas, grup)] += 1
                        if last_month == 1:
                            self.cobertures[(4, last_month, up_a, metge, inf, uas, grup)] += 1
                    self.last_batch[(4, 0)].add(hash)
                    self.last_batch[(4, last_month)].add(hash)
        
        print('---------we got the data----------')
        print('self.cobertures', len(self.cobertures))

    def fourth_block(self):
        self.upload = []
        print('QUART BLOC')
        # 11 AND 12
        for (estat, last_month, up_a, metge, inf, uas, grup), N in self.cobertures.items():
            if up_a in self.centres:
                br, fase = self.centres[up_a]
                indi = get_codi_indicador(4, last_month, estat)
                if last_month == 1: periode = 'MENSUAL'
                else: periode = 'ANUAL'
                self.upload.append((indi, periode, self.khalix_p, br, metge, 'M', 'NUM', grup, N))
                self.upload.append((indi, periode, self.khalix_p, br, inf, 'I', 'NUM', grup, N))
        for (prof, up, uba, grup), v in self.poblacio.items():
            if up in self.centres:
                br, fase = self.centres[up]
                for (indi, periode) in (('PLA011A', 'MENSUAL'),
                                        ('PLA011B', 'MENSUAL'),
                                        ('PLA012A', 'ANUAL'),
                                        ('PLA012B', 'ANUAL')):
                    self.upload.append((indi, periode,self.khalix_p, br, uba, prof, 'DEN', grup, v))

    def uploading(self):
        print('self.upload', len(self.upload))
        u.listToTable(self.upload, 'pla_indi_historic', 'altres')


if __name__ == "__main__":
    Planificat()
    # to_khalix
    print('up')
    sql = """select
    	INDICADOR,
    	carrega,
    	up,
    	ANALISI,
    	case
    		when a.GRUP = 0 then 'NOPLAGRUP'
    		when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
    		when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
    	end GRUPING,
    	'NOIMP',
    	'DIM6SET',
    	'N',
    	SUM(VALOR)
    from
    	altres.pla_indi_historic a
    where
    	prof = 'M'
    	and (indicador like 'PLA011%'
    		or indicador like 'PLA012%')
    group by
    	INDICADOR,
    	carrega,
    	UP,
    	ANALISI,
    	GRUP
    union
                select
    	substr(INDICADOR, 1, 6) as INDI,
    	carrega,
    	up,
    	ANALISI,
    	case
    		when a.GRUP = 0 then 'NOPLAGRUP'
    		when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
    		when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
    	end GRUPING,
    	'NOIMP',
    	'DIM6SET',
    	'N',
    	SUM(VALOR)
    from
    	altres.pla_indi_historic a
    where
    	prof = 'M'
    	and (indicador in ('PLA011A', 'PLA012A'))
    	and analisi = 'DEN'
    group by
    	substr(INDICADOR, 1, 6),
    	carrega,
        UP,
    	ANALISI,
    	GRUP
    union
                    select
    	substr(INDICADOR, 1, 6) as INDI,
    	carrega,
    	up,
    	ANALISI,
    	case
    		when a.GRUP = 0 then 'NOPLAGRUP'
    		when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
    		when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
    	end GRUPING,
    	'NOIMP',
    	'DIM6SET',
    	'N',
    	SUM(VALOR)
    from
    	altres.pla_indi_historic a
    where
    	prof = 'M'
    	and (indicador like 'PLA011%'
    		or indicador like 'PLA012%')
    	and analisi = 'NUM'
    group by
    	substr(INDICADOR, 1, 6),
    	carrega,
        UP,
    	ANALISI,
    	GRUP"""
    u.exportKhalix(sql, 'HISTORIC_PLA11_12', force=True)

    print('uba')
    sql = """select
            INDICADOR,
            CARREGA,
            CONCAT(CONCAT(UP, PROF), UBA),
            ANALISI,
            AGRUPATS,
            'NOIMP',
            'DIM6SET',
            'N',
            SUM(VALOR)
        from
            (
            select
                INDICADOR,
                CARREGA,
                UP,
                UBA,
                PROF,
                ANALISI,
                case
                    when GRUP < 14 then 'PRIORITZATS'
                    else 'NOPRIORITZATS'
                end AGRUPATS,
                VALOR
            from
                altres.pla_indi_historic
            where
                prof in ('I', 'M')) A
        group by
            INDICADOR,
            CARREGA,
            CONCAT(CONCAT(UP, PROF), UBA),
            ANALISI,
            AGRUPATS,
            'NOIMP',
            'DIM6SET',
            'N'
        union
        select
            INDI,
            CARREGA,
            CONCAT(CONCAT(UP, PROF), UBA),
            ANALISI,
            AGRUPATS,
            'NOIMP',
            'DIM6SET',
            'N',
            SUM(VALOR)
        from
            (
            select
                distinct substr(INDICADOR, 1, 6) as INDI,
                CARREGA,
                UP,
                UBA,
                PROF,
                ANALISI,
                case
                    when GRUP < 14 then 'PRIORITZATS'
                    else 'NOPRIORITZATS'
                end AGRUPATS,
                VALOR
            from
                altres.pla_indi_historic
            where
                prof in ('I', 'M')
                and indicador in ('PLA011A', 'PLA012A')
                and analisi = 'DEN') A
        group by
            INDI,
            CARREGA,
            CONCAT(CONCAT(UP, PROF), UBA),
            ANALISI,
            AGRUPATS,
            'NOIMP',
            'DIM6SET',
            'N'
        union
        select
            substr(INDICADOR, 1, 6),
            CARREGA,
            CONCAT(CONCAT(UP, PROF), UBA),
            ANALISI,
            AGRUPATS,
            'NOIMP',
            'DIM6SET',
            'N',
            SUM(VALOR)
        from
            (
            select
                distinct
                INDICADOR,
                CARREGA,
                UP,
                UBA,
                PROF,
                ANALISI,
                case
                    when GRUP < 14 then 'PRIORITZATS'
                    else 'NOPRIORITZATS'
                end AGRUPATS,
                VALOR
            from
                altres.pla_indi_historic
            where
                prof in ('I', 'M')
                and ANALISI = 'NUM') A
        group by
            substr(INDICADOR, 1, 6),
            CARREGA,
            CONCAT(CONCAT(UP, PROF), UBA),
            ANALISI,
            AGRUPATS,
            'NOIMP',
            'DIM6SET',
            'N'"""
    u.exportKhalix(sql, 'HISTORIC_PLA11_12_UBA', force=True)
