import sisapUtils as u
import collections as c
import datetime


# orina no te data prova
JSON_CONJUNTA = '{{"tipus" : "visita_conjunta","data" : "{data_reco}","visites" : [{{"tipus_vista" : "vm","aplica" : {metgeconj}}},{{"tipus_vista" : "vi","aplica" : {infconj}}}],"proves" : [{{"tipus_prova" : "ORINA","data_ultima_prova": "{d_orina}", "fer_prova" : {p_orina}}},{{"tipus_prova" : "SANG","data_ultima_prova": "{d_sang}","fer_prova" : {p_sang},"TSH": {tsh}}},{{"tipus_prova" : "ECG","data_ultima_prova": "{d_ecg}","fer_prova" : {p_ecg}}},{{"tipus_prova" : "FO","data_ultima_prova": "{d_fo}","fer_prova" : {p_fo}}},{{"tipus_prova" : "ESPIRO","data_ultima_prova": "{d_espiro}","fer_prova" : {p_espiro}}}]}}'

JSON_SEGUIMENT = """{{\
    "tipus" : "visita_seguiment",\
    "mesos_diferencia" : "{numero_proves}",\
    "finestra_temporal" : 365,\
    "marge_primera_visita": 15,\
    "proves" : [\
        {{\
        "mes_visita" : "{n_visita}",\
        "tipus_prova" : "SANG",\
        "fer_prova" : {sang_6m},\
        "TSH": {tiroides6m}\
        }}\
    ]\
}}"""

class PlanificatLanding():
    def __init__(self):
        u.execute("""delete from dwlanding.pacients_planificat_tr""", 'exadata_pre')
        u.execute("""delete from landing_pacients_planificat""", 'pdp')
        self.get_converters()
        self.seguent_visita()
        self.planificat()
        self.eqas()
        self.reviure()
        self.uploading()

    def get_converters(self):
        print('converters')
        self.hash2cip = {}
        sql = """SELECT hash_redics, cip  
                FROM dwsisap.rca_cip_nia a,
                dwsisap.pdptb101_relacio b
                WHERE hash = hash_covid"""
        for hash, cip in u.getAll(sql, 'exadata'):
            self.hash2cip[hash] = cip

    def seguent_visita(self):
        print('seg_visit')
        sql = """SELECT grup, periodicitat, 
            CASE WHEN revisio_anual = 'MF/INF' OR revisio_anual = 'MF' THEN 1 else 0 END METGE, 
            CASE WHEN revisio_anual = 'MF/INF' OR revisio_anual = 'INF'  THEN 1 else 0 END INF FROM visita_cronics_cat"""
        grup_2_visits = {}
        metge, inf = {}, {}
        for grup, n_visites, mf, i in u.getAll(sql, 'pdp'):
            grup_2_visits[int(grup)] = n_visites
            metge[grup] = mf
            inf[grup] = i
        self.patient = c.defaultdict(dict)
        sql = """select c_cip, c_up, c_centre, c_sector, c_metge, 
                c_infermera, atdom_residencia, dx, analisi_uv, 
                ecg_uv, espiro_uv, fo_uv, data_recomanada,
                CASE WHEN tsh IS NULL THEN 0 ELSE tsh END tsh, 
                CASE WHEN p_espiro IS NULL THEN 0 ELSE p_espiro END p_espiro, 
                CASE WHEN p_fo IS NULL THEN 0 ELSE p_fo END p_fo, 
                CASE WHEN p_ecg IS NULL THEN 0 ELSE p_ecg END p_ecg, 
                CASE WHEN p_analisi IS NULL THEN 0 ELSE p_analisi END p_analisi,
                CASE WHEN p_orina IS NULL THEN 0 ELSE p_orina END p_orina, 
                grup, edat, uas from seguent_visita_cronics
                where data_recomanada IS not null
                """
        for (c_cip, c_up, c_centre, c_sector, c_metge, 
                c_infermera, atdom_residencia, dx, analisi_uv, 
                ecg_uv, espiro_uv, fo_uv, data_recomanada,
                tsh, p_espiro, p_fo, p_ecg, p_analisi,
                p_orina, grup, edat, uas) in u.getAll(sql, 'pdp'):
            self.patient[c_cip]['UP'] = c_up
            self.patient[c_cip]['SECTOR'] = c_sector
            self.patient[c_cip]['CENTRE'] = c_centre[0:9]
            self.patient[c_cip]['CLASSE'] = c_centre[10:]
            self.patient[c_cip]['UBA'] = c_metge
            self.patient[c_cip]['UBAINF'] = c_infermera
            self.patient[c_cip]['ATDOMRESI'] = atdom_residencia
            self.patient[c_cip]['DX'] = dx
            self.patient[c_cip]['DATA'] = data_recomanada
            self.patient[c_cip]['GRUP'] = int(grup)
            self.patient[c_cip]['EDAT'] = edat
            self.patient[c_cip]['UAS'] = uas
            self.patient[c_cip]['JSON_CONJ'] = JSON_CONJUNTA.format(data_reco=data_recomanada,
                                                                     metgeconj=metge[grup],           
                                                                     infconj=inf[grup], 
                                                                     d_orina='',     
                                                                     p_orina=int(p_orina),
                                                                     d_sang=analisi_uv,
                                                                     p_sang=int(p_analisi),
                                                                     tsh=int(tsh),
                                                                     d_ecg=ecg_uv,
                                                                     p_ecg=int(p_ecg),
                                                                     d_fo=fo_uv,
                                                                     p_fo=int(p_fo),
                                                                     d_espiro=espiro_uv,
                                                                     p_espiro=int(p_espiro))
            if edat < 80 and 'DM2' in dx:
                sang_6m = 1
                if tsh:
                    tsh_6m = 1
                else: tsh_6m = 0
            else: 
                sang_6m = 0
                tsh_6m = 0
            n_visites = grup_2_visits[int(grup)]
            self.patient[c_cip]['JSON_SEG'] = JSON_SEGUIMENT.format(numero_proves=n_visites,
                                                                    n_visita=6,
                                                                    sang_6m=sang_6m,
                                                                    tiroides6m=int(tsh_6m)) 
            self.patient[c_cip]['N_RECO'] = None
            self.patient[c_cip]['RECOMANACIONS'] = None
            self.patient[c_cip]['ESTAT'] = None
            self.patient[c_cip]['DATA_ESTAT'] = None  
            self.patient[c_cip]['RESUCITAR'] = 0                                                 
        
    def planificat(self):
        print('planificat')
        sql = """SELECT hash, estat, estat_data FROM planificat"""
        for hash, estat, estat_data in u.getAll(sql, 'pdp'):
            if hash in self.patient:
                self.patient[hash]['ESTAT'] = int(estat)
                self.patient[hash]['DATA_ESTAT'] = str(estat_data)

    def eqas(self):
        print('eqas')
        sql = """SELECT hash, n_reco, recomanacio FROM planificateqa"""
        for hash, n_reco, reco_txt in u.getAll(sql, 'pdp'):
            if hash in self.patient:
                self.patient[hash]['N_RECO'] = n_reco
                self.patient[hash]['RECOMANACIONS'] = reco_txt
    
    def reviure(self):
        # Calculem a qui hem de resucitar
        print('a resucitar')
        hash_visites_post = set()
        sql = """SELECT hash_redics
                FROM DWSISAP.SISAP_MASTER_VISITES A, dwsisap.PDPTB101_RELACIO pr 
                WHERE hash_covid = pacient AND 
                (MOTIU_PRIOR LIKE '%PLANIFICAT>SEG%' 
                OR motiu_prior LIKE '%PLANIFICAT>CON%')
                AND DATA > sysdate"""
        
        for hash, in u.getAll(sql, 'exadata'):
            hash_visites_post.add(hash)

        sql = """SELECT  hash, resucitar, c_sector FROM(
                    select hash, estat,
                    case
                        when (TRUNC(SYSDATE) - trunc(to_date(estat_data, 'yyyy/mm/dd hh24:mi:ss'))) >= 400 and estat != 10 then 1
                        when 
                            (estat = 10 and ((TRUNC(SYSDATE) - trunc(to_date(fadm_visita_1, 'dd/mm/yyyy'))) >= 1 
                                        OR fadm_visita_1 IS NULL)) AND
                            (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_2, 'dd/mm/yyyy'))) >= 1 
                                        OR fadm_visita_2 IS NULL) AND
                            (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_3, 'dd/mm/yyyy'))) >= 1 
                                        OR fadm_visita_3 IS NULL) AND
                            (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_1post, 'dd/mm/yyyy'))) >= 1 
                                        OR fadm_visita_1post IS NULL) AND
                            (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_2post, 'dd/mm/yyyy'))) >= 1 
                                        OR fadm_visita_2post IS NULL) AND
                            (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_3post, 'dd/mm/yyyy'))) >= 1 
                                    OR fadm_visita_3post IS NULL) AND
                            (estat = 10 and (trunc(sysdate) - trunc(fadm_data_recomanada)) >= 1) then 1
                        else 0 
                    end resucitar, c_sector
                    from planificat)
                    WHERE resucitar = 1
                    and estat != 1"""
        for hash, resucitar, sector in u.getAll(sql, 'pdp'):
            if resucitar == 1: 
                if hash in self.patient and hash not in hash_visites_post:
                    self.patient[hash]['RESUCITAR'] = 1


    def uploading(self):
        upload = []
        currentdate = datetime.datetime.now()
        print('upload')
        print(len(self.patient))
        for hash, proves in self.patient.items():
            if hash in self.hash2cip:
                cip = self.hash2cip[hash]           
                upload.append((
                    cip,
                    proves['GRUP'],
                    proves['UBA'],
                    proves['UBAINF'],
                    proves['SECTOR'],
                    proves['CENTRE'],
                    proves['CLASSE'],
                    proves['UP'],
                    proves['UAS'],
                    proves['ATDOMRESI'],
                    proves['JSON_CONJ'],
                    proves['JSON_SEG'],
                    proves['DX'],
                    proves['N_RECO'],
                    proves['RECOMANACIONS'],
                    proves['RESUCITAR'],
                    proves['ESTAT'],
                    proves['DATA_ESTAT'],
                    currentdate
                ))
        # u.listToTable(upload, 'landing_pacients_planificat', 'pdp')
        u.listToTable(upload, 'dwlanding.pacients_planificat_tr', 'exadata_pre')
        # u.listToTable(upload, 'dwlanding.pacients_planificat', 'exadata_pre')
        u.execute("""begin
                    delete from dwlanding.pacients_planificat;
                    insert into dwlanding.pacients_planificat select * from dwlanding.pacients_planificat_tr;
                    commit;
                    end;""", 'exadata_pre')

    

if __name__ == "__main__":
    # sql = """SELECT cip, grup, uba, uba_inf, sector, up, 
    #         uas, atdomresi, json_con, json_seg,  
    #         dx, n_reco, n_reco_txt, resucitar
    #         FROM landing_pacients_planificat"""
    # upload = []
    # for row in u.getAll(sql, 'pdp'):
    #     upload.append(row)
    # u.listToTable(upload, 'dwlanding.pacients_planificat_tr', 'exadata_pre')
    # u.listToTable(upload, 'dwlanding.pacients_planificat', 'exadata_pre')
    PlanificatLanding()