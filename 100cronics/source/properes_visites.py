import sisapUtils as u
from collections import defaultdict
import pandas as pd
import math
from datetime import datetime
import numpy as np


class CRONICS():

    def __init__(self):
        self.getting_data()

    def getting_data(self):
        ts = datetime.now()
        print('comencem')
        # Baixem dades dbs
        sql = """SELECT c_cip, c_up, c_metge, C_SECTOR, C_INFERMERA,
                    CASE WHEN C_INSTITUCIONALITZAT IS NOT NULL THEN 'R' WHEN PS_ATDOM_DATA IS NOT NULL AND C_INSTITUCIONALITZAT IS NULL THEN 'A' END ATDOM_RESIDENCIA, 
                    CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL THEN 1 end CI,
                    CASE WHEN PS_ACV_MCV_DATA IS NOT NULL THEN 1 end AVC,
                    CASE WHEN PS_HTA_DATA IS NOT NULL THEN 1 END HTA,
                    CASE WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 1 END dm2,
                    CASE WHEN PS_INSUF_CARDIACA_DATA IS NOT NULL THEN 1 END IC,
                    CASE WHEN PS_MPOC_ENFISEMA_DATA IS NOT NULL and( VC_GOLD_VALOR LIKE '%%greu%%' OR (V_FEV1_VALOR<50 AND VC_GOLD_VALOR IS null)) THEN 1 END MPOC_GREU,
                    CASE WHEN PS_MPOC_ENFISEMA_DATA IS NOT NULL and( VC_GOLD_VALOR NOT LIKE '%%greu%%' AND (V_FEV1_VALOR>=50 AND VC_GOLD_VALOR IS NOT null)) THEN 1 END MPOC_LLEU,
                    CASE WHEN PS_DISLIPEMIA_DATA IS NOT NULL THEN 1 END dislipemia,
                    (CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL OR PS_INSUF_CARDIACA_DATA IS NOT NULL 
                    OR (PS_MPOC_ENFISEMA_DATA IS NOT NULL and( VC_GOLD_VALOR LIKE '%%greu%%' OR (V_FEV1_VALOR<50 AND VC_GOLD_VALOR IS null)))THEN 3
                    WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 2 ELSE 1 END) risc,
                    CASE WHEN F_HTA_COMBINACIONS is NOT NULL OR F_HTA_DIURETICS IS NOT NULL OR F_HTA_IECA_ARA2 IS NOT NULL THEN 1 ELSE 0 END FarmHTA,
                    CASE WHEN PS_HIPOTIROIDISME_DATA IS NOT NULL THEN 1 END HIPOTIROIDISME,
                    V_HBA1C_DATA glicada_DM,
                    V_COL_TOTAL_DATA  colest,
                    V_ECG_AMB_DATA ECG,
                    V_FEV1_DATA espiro1,
                    V_FEV1_FVC_DATA espiro2,
                    V_FONS_ULL_DATA fons_ull
                fROM 
                    dbs 
                WHERE 
                    PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL 
                    OR ps_hta_data IS NOT NULL 
                    OR PS_DIABETIS2_DATA IS NOT NULL OR 
                    PS_INSUF_CARDIACA_DATA is NOT NULL OR 
                    PS_MPOC_ENFISEMA_DATA IS NOT NULL  OR 
                    PS_DISLIPEMIA_DATA IS NOT NULL AND 
                    PR_MACA_DATA IS NULL AND
                    (PS_NEOPLASIA_M_DATA IS NULL OR PS_NEOPLASIA_M_DATA < add_months(CURRENT_DATE, -12*1)) AND (CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL OR PS_INSUF_CARDIACA_DATA IS NOT NULL 
                    OR (PS_MPOC_ENFISEMA_DATA IS NOT NULL and( VC_GOLD_VALOR LIKE'%%greu%%' OR (V_FEV1_VALOR<50 AND VC_GOLD_VALOR IS null)))THEN 3
                    WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 2 ELSE 1 END) IN (2,3)"""
        upload = []

        for (c_cip, c_up, c_metge, C_SECTOR, C_INFERMERA, ATDOM_RESIDENCIA, 
            CI, AVC, HTA, dm2, IC, MPOC_GREU, MPOC_LLEU, dislipemia, risc, FarmHTA,
            HIPOTIROIDISME, glicada_DM, colest, ECG, espiro1, espiro2, 
            fons_ull) in u.getAll(sql, 'redics'):

            upload.append([c_cip, c_up, c_metge, C_SECTOR, C_INFERMERA, ATDOM_RESIDENCIA, 
                            CI, AVC, HTA, dm2, IC, MPOC_GREU, MPOC_LLEU, dislipemia, 
                            risc, FarmHTA, HIPOTIROIDISME, glicada_DM, colest, ECG, 
                            espiro1, espiro2, fons_ull])

        print('crear dict')    
        self.dbs = pd.DataFrame(upload)
        self.dbs.columns =['c_cip', 'c_up', 'c_metge', 'C_SECTOR', 'C_INFERMERA', 
                        'ATDOM_RESIDENCIA', 'CI', 'AVC', 'HTA', 'dm2', 'IC', 
                        'MPOC_GREU', 'MPOC_LLEU', 'dislipemia', 'risc', 'FarmHTA',
                        'HIPOTIROIDISME', 'glicada_DM', 'colest', 'ECG', 
                        'espiro1', 'espiro2', 'fons_ull']
        print('len dbs:', len(self.dbs))
        print('Time execution {}'.format(datetime.now() - ts))
    
    def neteja_dbs(self):
        # Weird bug
        self.dbs = self.dbs.replace(np.nan, np.nan)

        # Ho passem a datetimes tot
        for date_type in ('GLICADA_DM', 'COLEST'):
            self.dbs[date_type]= pd.to_datetime(self.dbs[date_type])
    
        for e in ('ESPIRO1', 'ESPIRO2', 'FONS_ULL', 'ECG'):
            print(e)
            for i in range(len(self.dbs)):
                try:
                    if np.isnan(self.dbs[e][i]) == False:
                        pass
                except:
                    if self.dbs[e][i][0:3] not in ('200', '199', '201', '202'):
                        self.dbs[e][i] = np.nan
            self.dbs[e]= pd.to_datetime(self.dbs[e])