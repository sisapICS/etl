import sisapUtils as u



class NoRCA():
    def __init__(self):
        self.get_pacients()
        self.uploading()
    
    def get_pacients(self):
        cips14 = {}
        sql = """SELECT cip FROM dwsisap.RCA_CIP_NIA rcn"""
        for cip, in u.getAll(sql, 'exadata'):
            cips14[cip[:13]] = cip
        sql = """SELECT
                    usua_cip,
                    svc.c_up,
                    c_centre,
                    ESTAT,
                    ESTAT_DATA 
                FROM
                    SEGUENT_VISITA_CRONICS svc
                    LEFT JOIN PLANIFICAT p 
                    ON svc.C_CIP = p.HASH 
                    INNER JOIN pdptb101 tb
                    ON svc.c_cip = tb.USUA_CIP_COD 
                WHERE
                    svc.c_up IN (
                                '07962',
                                '00352',
                                '00291',
                                '00195',
                                '00052',
                                '00053',
                                '00340',
                                '04863',
                                '00101',
                                '00102',
                                '06175',
                                '04374',
                                '00088',
                                '00151',
                                '00016',
                                '00015',
                                '04704',
                                '00386',
                                '01790',
                                '01796'
                                )"""
        self.upload = []
        for cip13, up, centre, estat, estat_data in u.getAll(sql, 'pdp'):
            if cip13 not in cips14:
                self.upload.append((cip13, up, centre, str(estat), str(estat_data)))
    
    def uploading(self):
        print(self.upload)
        cols = """(cip13 varchar2(13), up varchar2(5), centre varchar2(20), estat varchar2(20), estat_data  varchar2(25))"""
        u.createTable('planificat_no_rca', cols, 'exadata', rm=True)
        u.grantSelect('planificat_no_rca', 'DWSISAP_ROL', 'exadata')
        u.listToTable(self.upload, 'planificat_no_rca', 'exadata')


if __name__ == "__main__":
    NoRCA()