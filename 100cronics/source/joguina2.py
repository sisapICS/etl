import sisapUtils as u
import collections as c

cols = '(up varchar(5), servei varchar(20), visita date, prof varchar(20), n int)'
u.createTable('visites_mf_urg', cols, 'test', rm=True)

distribucio_peticions = c.Counter()
sql = """SELECT pacient, data, up, servei, professional FROM dwsisap.sisap_master_visites
            WHERE EXTRACT(YEAR FROM data) = 2019 and
            sisap_SERVEI_class IN ('MF', 'URG') and 
            ATRIBUTS_AGENDA_COD in('1.1','2.1', '2.4')"""

for p, peticio, up, servei, prof in u.getAll(sql, 'exadata'):
    distribucio_peticions[(peticio, up, servei, prof)] += 1

upload = []
for i, ((peticio, up, servei, prof), n) in enumerate(distribucio_peticions.items()):
    upload.append((up, servei, peticio, prof, n))
    if i % 10000 == 0:
        u.listToTable(upload, 'visites_mf_urg', 'test')
        upload = []

u.listToTable(upload, 'visites_mf_urg', 'test')

# cip_2_hash = {}
# sql = """SELECT usua_cip, usua_cip_cod FROM pdptb101"""
# for cip, hash in u.getAll(sql, 'pdp'):
#     cip_2_hash[cip] = hash

# u.createTable('caducitats', '(cip varchar(45), up varchar(5), data_c varchar(30))', 'test', rm=True)

# upload = []
# sql = """select alv_cip, alv_uab_up, alv_text2
#                     from prstb550
#                     where alv_tipus='RES'
#                     AND alv_text4 < 31"""
# for s in u.sectors:
#     print(s)
#     for cip, up, data in u.getAll(sql, s):
#         if cip in cip_2_hash:
#             upload.append((cip_2_hash[cip], up, data))

# u.listToTable(upload, 'caducitats', 'test')

# upload = []
# sql ="""SELECT
#         c_CIP,
#         c_up,
#         c_metge,
#         c_infermera
#         FROM
#         dbs
#         WHERE
#         c_edat_anys >59
#         AND
#         C_SAP IN ('F3', 'F6', 'C1', 'F4', '94')
#         AND
#         I_GRIP_DATA IS NULL"""

# for hash, up, metge, inf in u.getAll(sql, 'redics'):
#     upload.append(hash+'@'+up+'@'+metge+'@'+inf)

# with open('no_tenen_cap_registre_VAG.txt', 'w') as fp:
#     for item in upload:
#         # write each item on a new line
#         fp.write("%s\n" % item)
#     print('Done')




