import sisapUtils as u


users = ["PREDUECR", "PREDUMMP", "PREDULMB", "PREDUJVG", 
        "PREDUPRP", "PREDUMQS", "PREDUMPP", "PREDUXMG", "PREDUTFP",
        "PREDUMGS", "PREDUMAA", "PREDUEVC", "PREDUMAG"]

taule = ['cat_centres_pilots', 'indicadors_planificat_up', 'indicadors_planificat_uba', 'cataleg_indicadors_planificat']


for user in users:
    u.execute('grant select on cat_centres_pilots to {}'.format(user), 'redics')