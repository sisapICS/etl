# -*- coding: utf8 -*-

import sisapUtils as u
import collections as c
import sisaptools as t


TALL = False  # ex: '2022-01-01' o False: si anem al dia.

def get_dextraccio():
        db = 'nodrizas'
        if TALL:
            data_ext = TALL
        else:
            data_ext = [row.strftime('%Y-%m-%d')
                            for row, in u.getAll(
                                "select data_ext from dextraccio",
                                db)
                        ][0]
        return data_ext

class Planificat():
    def __init__(self):
        self.get_centres()
        self.get_administratius()
        self.create_table()
        self.get_ups_planificat()
        self.get_patinet_info()
        self.get_hash_converter()
        self.PLA()

    def get_centres(self):
        print('centres')
        self.centres = {}
        sql = """select ics_codi AS br, up, fase from DWSISAP.CENTRES_PILOTS"""
        for br, up, fase in u.getAll(sql, 'exadata'):
            self.centres[up] = [br, fase]

    def get_administratius(self):
        print('admis')
        dni_user = {}
        self.uas_to_adm = {}
        sql = """SELECT DNI, HASH, UP, uas FROM DWSISAP.ASSIGNADA_ADMINISTRATIUS"""
        for dni, hash, up, uas in u.getAll(sql, 'exadata'):
            if up in self.centres:
                br = self.centres[up][0]
                dni_user[dni] = br + 'A' + hash
                self.uas_to_adm[(up, uas)] = br + 'A' + hash
        self.login_user = {}
        sql = """SELECT LOGIN, NIF FROM DWSISAP.CAT_ADMINISTRATIUS"""
        for login, dni in u.getAll(sql, 'exadata'):
            if dni in dni_user:
                self.login_user[login] = dni_user[dni]

    def create_table(self):
        print('taula')
        cols = """(indicador varchar(50), periode varchar(10), up varchar(15), 
                    uba varchar(19), prof varchar(5),
                    analisi varchar(10), grup int, valor decimal(10,3))"""
        u.createTable('pla_indi', cols, 'altres', rm=True)

    def get_ups_planificat(self):
        print('geting ups')
        sql = """SELECT up FROM centres_pilots"""
        self.ups_planificat = set()
        for up, in u.getAll(sql, 'pdp'):
            self.ups_planificat.add(up)
    
    def get_patinet_info(self):
        print('geting dbs')
        self.patients = {}
        self.den_professionals = c.Counter()
        self.den_totals = c.Counter()
        sql = """SELECT c_cip, c_up, c_metge, C_INFERMERA, c_uas, grup 
                    FROM dwsisap.dbs
                    WHERE grup > 0"""
        for hash, up, metge, inf, uas, grup in u.getAll(sql, 'exadata'):
            self.patients[hash] = (up, metge, inf, uas, grup)
            self.den_professionals[(up, metge, 'M', grup)] += 1
            self.den_professionals[(up, inf, 'I', grup)] += 1
            self.den_professionals[(up, uas, 'A', grup)] += 1
            self.den_totals[(up, grup)] += 1

    def get_hash_converter(self):
        print('hash converter')
        sql = """SELECT hash_redics, hash_covid FROM PDPTB101_RELACIO pr"""
        self.hash_redics_2_covid = {}
        for hash_r, hash_c in u.getAll(sql, 'pdp'):
            self.hash_redics_2_covid[hash_r] = hash_c

    def PLA(self):
        print('grep data')
        """Per equip i per administratiu mirar el nombre de pacients programats per mes"""
        n_pacients = c.defaultdict(set)
        n_pacients_anual = c.defaultdict(set)
        n_pacients_m = c.defaultdict(set)
        n_pacients_a = c.defaultdict(set)
        n_visites = c.Counter()
        n_visites_anual = c.Counter()
        n_visites_metge = c.Counter()
        n_visites_metge_anual = c.Counter()
        n_visites_infermera = c.Counter()
        n_visites_infermera_anual = c.Counter()
        n_admins = c.defaultdict(set)
        n_visites_up = c.Counter()
        n_visites_up_anual = c.Counter()
        n_prof_seg = c.defaultdict(set)
        n_prof_seg_anual = c.defaultdict(set)
        n_prof_con = c.defaultdict(set)
        mesos_espera = c.defaultdict(lambda: c.defaultdict(dict))
        intriduits_ultim_mes, intriduits_no_ultim_mes = set(), set()
        admins_actius = c.defaultdict(set)
        infermeres_actius = c.defaultdict(set)
        metges_actius = c.defaultdict(set)
        cobertura_total_m = c.defaultdict(set)
        cobertura_total_a = c.defaultdict(set)
        sql = """SELECT up, USUARI as login, motiu_prior, pacient, 
                    round(MONTHS_BETWEEN(DATA, PETICIO)),
                    CASE WHEN motiu_prior LIKE '%PLANIFICAT>CON>INF%' THEN 'INF'
                        WHEN motiu_prior LIKE '%PLANIFICAT>CON>MG%' THEN 'MG'
                        END CONJ,
                    case when MONTHS_BETWEEN(DATE '{tall}', PETICIO) BETWEEN 0 AND 1 then 1 else 0 end last_month,
                    case when MONTHS_BETWEEN(DATE '{tall}', PETICIO) BETWEEN 0 AND 3 then 1 else 0 end activament
                    FROM DWSISAP.SISAP_MASTER_VISITES A
                    WHERE (MOTIU_PRIOR LIKE '%PLANIFICAT>SEG%' OR motiu_prior LIKE '%PLANIFICAT>CON%')
                    and MONTHS_BETWEEN(PETICIO,  DATE '{tall}') 
                    BETWEEN 0 AND 12""".format(tall=TALL if TALL else get_dextraccio())
        for up, login, motiu, pacient, mesos, prof_conj, last_month, activament in u.getAll(sql, 'exadata'):
            if up in self.ups_planificat and pacient in self.patients:
                up_a, metge, inf, uas, grup =  self.patients[pacient]
                if last_month == 1:
                    intriduits_ultim_mes.add(pacient)
                    if login in self.login_user:
                        br_a = self.login_user[login]
                        cobertura_total_m[(br_a, '', 'A', grup)].add(pacient)
                        n_pacients[(br_a, grup)].add(pacient)
                        n_visites[(br_a, motiu[11:14], grup, up)] += 1
                        n_visites_infermera[(up_a, inf, motiu[11:14], grup)] += 1
                        n_visites_metge[(up_a, metge, motiu[11:14], grup)] += 1
                        n_admins[('MENSUAL', motiu[11:14], up)].add(br_a)
                    if motiu[11:14] == 'SEG': n_prof_seg[(up, grup)].add(login)
                    elif motiu[11:14] == 'CON': n_prof_con[(up, grup)].add(login)
                    n_visites_up[(up, motiu[11:14], grup)] += 1
                    cobertura_total_m[(up_a, metge, 'M', grup)].add(pacient)
                    cobertura_total_m[(up_a, inf, 'I', grup)].add(pacient)
                    n_pacients_m[(up_a, metge, 'M', grup)].add(pacient)
                    n_pacients_m[(up_a, inf, 'I', grup)].add(pacient)
                    n_pacients_m[(up_a, uas, 'A', grup)].add(pacient)
                else:
                    intriduits_no_ultim_mes.add(pacient)
                    if pacient in intriduits_ultim_mes:
                        intriduits_ultim_mes.remove(pacient)
                    if login in self.login_user:
                        br_a = self.login_user[login]
                        cobertura_total_a[(br_a, '', 'A', grup)].add(pacient)
                        n_admins[('MENSUAL', motiu[11:14], up)].add(br_a)
                        n_pacients_anual[(br_a, grup)].add(pacient)
                        n_visites_anual[(br_a, motiu[11:14], grup, up)] += 1
                        n_visites_infermera_anual[(up_a, inf, motiu[11:14], grup)] += 1
                        n_visites_metge_anual[(up_a, metge, motiu[11:14], grup)] += 1
                    if motiu[11:14] == 'SEG': n_prof_seg_anual[(up, grup)].add(login)
                    elif motiu[11:14] == 'CON': 
                        mesos_espera[(up, grup)][pacient][prof_conj] =  mesos
                    n_visites_up_anual[(up, motiu[11:14], grup)] += 1
                    cobertura_total_a[(up_a, metge, 'M', grup)].add(pacient)
                    cobertura_total_a[(up_a, inf, 'I', grup)].add(pacient)
                    n_pacients_a[(up_a, metge, 'M', grup)].add(pacient)
                    n_pacients_a[(up_a, inf, 'I', grup)].add(pacient)
                    n_pacients_a[(up_a, uas, 'A', grup)].add(pacient)
                # pacients programats en els ultims 3 mesos
                if activament == 1:
                    if login in self.login_user:
                        br_a = self.login_user[login]
                        admins_actius[(up_a,br_a)].add(pacient)
                    metges_actius[(up_a,metge)].add(pacient)
                    infermeres_actius[(up_a,inf)].add(pacient)

        # CHECK
        fet = False
        for (up, uba), pacients in admins_actius.items():
            if len(pacients) > 20: 
                print('si que hi ha admins')
                fet = True
        if not fet: print('ESTIC TRIST, NO TINC ADMINISTRATIUS:(((((')
        
        sql = """SELECT 
                    hash,
                    CASE WHEN 
                            MONTHS_BETWEEN(to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS'),  DATE '2022-02-02') 
                            BETWEEN 0 AND 3 THEN 1 ELSE 0 END actiu
                    FROM PLANIFICAT_AUDIT
                    WHERE estat = 10
                    AND MONTHS_BETWEEN(to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS'),  DATE '2022-02-02') BETWEEN 0 AND 12"""
        estat_planificat_m = c.defaultdict(set)
        estat_planificat_a = c.defaultdict(set)
        for hash_r, actiu in u.getAll(sql, 'pdp'):
            if hash_r in self.hash_redics_2_covid:
                hash = self.hash_redics_2_covid[hash_r]
                if hash in self.patients:
                    up_a, metge, inf, uas, grup =  self.patients[hash]
                    metges_actius[(up_a,metge)].add(hash)
                    infermeres_actius[(up_a,inf)].add(hash)
                    estat_planificat_m[(up_a, inf, 'I', grup)].add(hash)
                    estat_planificat_a[(up_a, inf, 'I', grup)].add(hash)
                    estat_planificat_m[(up_a, metge, 'M', grup)].add(hash)
                    estat_planificat_a[(up_a, metge, 'M', grup)].add(hash)
                    cobertura_total_a[(up_a, metge, 'M', grup)].add(hash)
                    cobertura_total_a[(up_a, inf, 'I', grup)].add(hash)
                    cobertura_total_m[(up_a, metge, 'M', grup)].add(hash)
                    cobertura_total_m[(up_a, inf, 'I', grup)].add(hash)

        print('1,2,3,4')
        upload = []
        # Indicadors de PLANVISITES: nombre de visites programades amb X criteris
        # Per administratius
        # Mensual
        # Aprofitem per calcular els d'administratius també (els q van a l'altra pantalla)
        indi_pantalla_adm = c.Counter()
        for (br_a, motiu, grup, up), v in n_visites.items():
            if motiu == 'CON':
                upload.append(('PLA001', 'MENSUAL', br_a, '', 'A', 'NUM', grup, v))
                indi_pantalla_adm[('MENSUAL', motiu, up)] += 1
            elif motiu == 'SEG':
                upload.append(('PLA003', 'MENUSAL', br_a, '', 'A', 'NUM', grup, v))
                indi_pantalla_adm[('MENSUAL', motiu, up)] += 1
        # Anual
        for (br_a, motiu, grup, up), v in n_visites_anual.items():
            if motiu == 'CON':
                upload.append(('PLA002', 'ANUAL', br_a, '', 'A', 'NUM', grup, v))
                indi_pantalla_adm[('ANUAL', motiu, up)] += 1
            elif motiu == 'SEG':
                upload.append(('PLA004', 'ANUAL', br_a, '', 'A', 'NUM', grup, v))
                indi_pantalla_adm[('ANUAL', motiu, up)] += 1
        # Per metges
        # Mensual
        for (up, uba, motiu, grup), v in n_visites_metge.items():
            if up in self.centres:
                br = self.centres[up][0]
                if motiu == 'CON':
                    upload.append(('PLA001', 'MENSUAL', br, uba, 'M', 'NUM', grup, v))
                elif motiu == 'SEG':
                    upload.append(('PLA003', 'MENUSAL', br, uba, 'M', 'NUM', grup, v))
        # Anual
        for (up, uba, motiu, grup), v in n_visites_metge_anual.items():
            if up in self.centres:
                br = self.centres[up][0]
                if motiu == 'CON':
                    upload.append(('PLA002', 'ANUAL', br, uba, 'M', 'NUM', grup, v))
                elif motiu == 'SEG':
                    upload.append(('PLA004', 'ANUAL', br, uba, 'M', 'NUM', grup, v))
        # Per infermeres
        # Mensual
        for (up, uba, motiu, grup), v in n_visites_infermera.items():
            if up in self.centres:
                br = self.centres[up][0]
                if motiu == 'CON':
                    upload.append(('PLA001', 'MENSUAL', br, uba, 'M', 'NUM', grup, v))
                elif motiu == 'SEG':
                    upload.append(('PLA003', 'MENUSAL', br, uba, 'M', 'NUM', grup, v))
        # Anual
        for (up, uba, motiu, grup), v in n_visites_infermera_anual.items():
            if up in self.centres:
                br = self.centres[up][0]
                if motiu == 'CON':
                    upload.append(('PLA002', 'ANUAL', br, uba, 'I', 'NUM', grup, v))
                elif motiu == 'SEG':
                    upload.append(('PLA004', 'ANUAL', br, uba, 'I', 'NUM', grup, v))
        # Per UP
        # Mensual
        for (up, motiu, grup), v in n_visites_up.items():
            if motiu == 'CON':
                upload.append(('PLA001', 'MENSUAL', up, '', 'T', 'NUM', grup, v))
            elif motiu == 'SEG':
                upload.append(('PLA003', 'MENSUAL', up, '', 'T', 'NUM', grup, v))
        # Anual
        for (up, motiu, grup), v in n_visites_up_anual.items():
            if motiu == 'CON':
                upload.append(('PLA002', 'ANUAL', up, '', 'T', 'NUM', grup, v))
            elif motiu == 'SEG':
                upload.append(('PLA004', 'ANUAL', up, '', 'T', 'NUM', grup, v))

        print('5')
        # PLA005: mitjana de mesos entre la visita conjunta i el dia de peticio d'aquesta
        print(len(mesos_espera))
        mitjana_mesos = c.Counter()
        visites_con = c.Counter()
        for (up, grup) in mesos_espera:
            for pacient in mesos_espera[(up, grup)]:
                prof_conj = mesos_espera[(up, grup)][pacient].keys()
                visites_con[(up, grup)] += 1
                if 'INF' in prof_conj: mitjana_mesos[(up, grup)] += mesos_espera[(up, grup)][pacient]['INF']
                elif 'MG' in prof_conj: mitjana_mesos[(up, grup)] += mesos_espera[(up, grup)][pacient]['MG']
        for (up, grup) in mitjana_mesos:
            m = mitjana_mesos[(up, grup)]
            v = visites_con[(up, grup)]
            upload.append(('PLA005', 'MENSUAL', up, '', 'T', 'NUM', grup, m))
            upload.append(('PLA005', 'MENSUAL', up, '', 'T', 'DEN', grup, v))

        print('6-11')
        cobertura_m = c.Counter()
        cobertura_a = c.Counter()
        for (up, uba, t, grup), pacients in cobertura_total_m.items():
            if up in self.centres:
                br, fase = self.centres[up]
                upload.append(('PLA006','MENSUAL', br, uba, t, 'NUM', grup, len(pacients)))
                if t == 'M':
                    cobertura_m[(br, grup)] += len(pacients)

        for (up, uba, t, grup), pacients in cobertura_total_a.items():
            if up in self.centres:
                br, fase = self.centres[up]
                upload.append(('PLA007','ANUAL', br, uba, t, 'NUM', grup, len(pacients)))
                if t == 'M':
                    cobertura_a[(br, grup)] += len(pacients)

        for (br, grup), v in cobertura_m.items():
            upload.append(('PLA006','MENSUAL', br, '', 'T', 'NUM', grup, v))
        
        for (br, grup), v in cobertura_a.items():
            upload.append(('PLA007','ANUAL', br, '', 'T', 'NUM', grup, v))


        # Mirant-ho per visites
        cobertura_m = c.Counter()
        cobertura_a = c.Counter()
        for (up, uba, t, grup), pacients in n_pacients_m.items():
            if up in self.centres:
                br, fase = self.centres[up]
                if t in ('M', 'I'):
                    upload.append(('PLA008','MENSUAL', br, uba, t, 'NUM', grup, len(pacients)))
                else:
                    if (up, uba) in self.uas_to_adm:
                        adm = self.uas_to_adm[(up, uba)] 
                        upload.append(('PLA008','MENSUAL', adm, '', 'A', 'NUM', grup, len(pacients)))
                if t == 'M':
                    cobertura_m[(br, grup)] += len(pacients)

        for (up, uba, t, grup), pacients in n_pacients_a.items():
            if up in self.centres:
                br, fase = self.centres[up]
                if t in ('M', 'I'):
                    upload.append(('PLA009','ANUAL', br, uba, t, 'NUM', grup, len(pacients)))
                else:
                    if (up, uba) in self.uas_to_adm:
                        adm = self.uas_to_adm[(up, uba)] 
                        upload.append(('PLA009','ANUAL', adm, '', 'A', 'NUM', grup, len(pacients)))
                if t == 'M':
                    cobertura_a[(br, grup)] += len(pacients)

        for (br, grup), v in cobertura_m.items():
            upload.append(('PLA008','MENSUAL', br, '', 'T', 'NUM', grup, v))
        
        for (br, grup), v in cobertura_a.items():
            upload.append(('PLA009','ANUAL', br, '', 'T', 'NUM', grup, v))

        
        # Mirant-ho per estat de programacio
        cobertura_m = c.Counter()
        cobertura_a = c.Counter()
        for (up, uba, t, grup), pacients in estat_planificat_m.items():
            if up in self.centres:
                br, fase = self.centres[up]
                if t in ('M', 'I'):
                    if up and uba:
                        upload.append(('PLA010','MENSUAL', br, uba, t, 'NUM', grup, len(pacients)))
                else:
                    if (up, uba) in self.uas_to_adm:
                        adm = self.uas_to_adm[(up, uba)] 
                        upload.append(('PLA010','MENSUAL', adm, '', 'A', 'NUM', grup, len(pacients)))
                if t == 'M':
                    cobertura_m[(br, grup)] += len(pacients)

        for (up, uba, t, grup), pacients in estat_planificat_a.items():
            if up in self.centres:
                br, fase = self.centres[up]
                if t in ('M', 'I'):
                    if up and uba:
                        upload.append(('PLA011','ANUAL', br, uba, t, 'NUM', grup, len(pacients)))
                else:
                    if (up, uba) in self.uas_to_adm:
                        adm = self.uas_to_adm[(up, uba)] 
                        upload.append(('PLA011','ANUAL', adm, '', 'A', 'NUM', grup, len(pacients)))
                if t == 'M':
                    cobertura_a[(br, grup)] += len(pacients)

        for (br, grup), v in cobertura_m.items():
            upload.append(('PLA010','MENSUAL', br, '',  'T', 'NUM', grup, v))
        
        for (br, grup), v in cobertura_a.items():
            upload.append(('PLA011','ANUAL', br, 'T', '', 'NUM', grup, v))
        
        # denominadors
        for (up, uba, t, grup), v in self.den_professionals.items():
            if up in self.centres:
                br, fase = self.centres[up]
                for (indi, periode) in (('PLA006','MENSUAL'),
                                        ('PLA007','ANUAL'),
                                        ('PLA008','MENSUAL'),
                                        ('PLA009','ANUAL'),
                                        ('PLA010','MENSUAL'),
                                        ('PLA011', 'ANUAL')):
                    if t in ('I', 'M'):
                        if up and uba:
                            upload.append((indi, periode, br, uba, t, 'DEN', grup, v))
                    else:
                        if (up, uba) in self.uas_to_adm:
                            adm = self.uas_to_adm[(up, uba)] 
                            upload.append((indi, periode, adm, '', 'A', 'DEN', grup, v))
        
        for (up, grup), v in self.den_totals.items():
            if up in self.centres:
                br, fase = self.centres[up]
                for (indi, periode) in (('PLA006','MENSUAL'),
                                    ('PLA007','ANUAL'),
                                    ('PLA008','MENSUAL'),
                                    ('PLA009','ANUAL'),
                                    ('PLA010','MENSUAL'),
                                    ('PLA011', 'ANUAL')):
                    upload.append((indi, periode, br, '', 'T', 'DEN', grup, v))

        print('6')
        # Indicadors professionals actius
        # Administratius
        professionals_actius = c.Counter()
        for (up, admi), pacients in admins_actius.items():
            if len(pacients) >= 20:
                print('tinc + de 20 pacients a administratius')
                if up in self.centres:
                    br = self.centres[up][0]
                    professionals_actius[('A',br)]+=1
        # Metges
        for (up,metge), pacients in metges_actius.items():
            if len(pacients) >= 20:
                if up in self.centres:
                    br = self.centres[up][0]
                    professionals_actius[('M',br)]+=1
        # Infermeres
        for (up,metge), pacients in infermeres_actius.items():
            if len(pacients) >= 20:
                if up in self.centres:
                    br = self.centres[up][0]
                    professionals_actius[('I',br)]+=1
                    
        for (prof, up), val in professionals_actius.items():
            if prof == 'A':
                upload.append(('PLA014', 'MENSUAL', up, '', prof, 'NUM', 0, val))
            elif prof == 'M':
                upload.append(('PLA012', 'MENSUAL', up, '', prof, 'NUM', 0, val))
            elif prof == 'I':
                upload.append(('PLA013', 'MENSUAL', up, '', prof, 'NUM', 0, val))


        # ELS D'ADMINISTRATIUS!!!
        for (periode, tipus, up), administratius in n_admins.items():  
            if periode == 'MENSUAL':
                if tipus == 'SEG':
                    upload.append(('ADMPLA01B', periode, up, '', 'A', 'DEN', 0, len(administratius)))
                else:
                    upload.append(('ADMPLA01A', periode, up, '', 'A', 'DEN', 0, len(administratius)))
            else:
                if tipus == 'SEG':
                    upload.append(('ADMPLA02B', periode, up, '', 'A', 'DEN', 0, len(administratius)))
                else:
                    upload.append(('ADMPLA02A', periode, up, '', 'A', 'DEN', 0, len(administratius)))

        for (periode, tipus, up), v in indi_pantalla_adm.items():
            if periode == 'MENSUAL':
                if tipus == 'SEG':
                    upload.append(('ADMPLA01B', periode, up, '', 'A', 'NUM', 0, v))
                else:
                    upload.append(('ADMPLA01A', periode, up, '', 'A', 'NUM', 0, v))
            else:
                if tipus == 'SEG':
                    upload.append(('ADMPLA02B', periode, up, '', 'A', 'NUM', 0, v))
                else:
                    upload.append(('ADMPLA02A', periode, up, '', 'A', 'NUM', 0, v))

        print('upload', len(upload))
        u.listToTable(upload, 'pla_indi', 'altres')


# Creuament arxiu ADM per up i per administratiu:
# indicador{A2301{centre{NUM{edat{NOIMP{DIM6SET{N{5

# edat: per exemple ED1544, EDM75, EDSC....
# EDSC = no m'importa edat


if __name__ == "__main__":
    try:
        Planificat()
    except Exception as e:
        mail = t.Mail()
        mail.to.append("roser.cantenys@catsalut.cat")
        mail.subject = "Planificat!!!"
        mail.text = str(e)
        mail.send()

