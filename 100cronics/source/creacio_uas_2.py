# -*- coding: utf-8 -*-

import sisapUtils as u
from collections import defaultdict
import pandas as pd
import math
from datetime import datetime
import numpy as np
import time
from dateutil.relativedelta import relativedelta
import sisaptools as tools

# Ignorar warnings
import warnings
warnings.filterwarnings("ignore")

sql = """select day(curdate()), month(curdate()), year(curdate()), weekday(curdate()) from nodrizas.dextraccio"""
dia, mes, any, dia_set = u.getOne(sql, 'nodrizas')
mes_st = str(mes).zfill(2)
ant_st = str(mes-1).zfill(2)
any_st = str(any)

ts = datetime.now()
print('gent dintre planificat')
redic_2_covid = {}
covid_2_redics = {}
cip_2_redics = {}

sql = """SELECT hash_redics, hash_covid, cip from pdptb101_relacio"""
for h_redics, h_covid, cip in u.getAll(sql, 'pdp'):
    redic_2_covid[h_redics] = h_covid
    covid_2_redics[h_covid] = h_redics
    cip_2_redics[cip] = h_redics

analitiques = dict()
sql = """SELECT id, data FROM dwtw.laboratori_detall
            WHERE DATA_MODI > DATE '{}-{}-{}'
            AND prova IN ('BB030','Q13285')""".format(any_st, mes_st, dia)

print(sql)
counting_analitiques = 0
for cip, data in tools.Database('exadata', 'thewhole').get_all(sql):
    if cip[0:13] in cip_2_redics:
        hash_r = cip_2_redics[cip[0:13]]
        analitiques[hash_r] = data
        counting_analitiques += 1
print('he comptat ', counting_analitiques, ' analitiques')

al_circuit = {}
sql = """SELECT PACIENT, CASE WHEN  motiu_prior LIKE '%PLANIFICAT>CON>INF%' THEN 'I'
        WHEN motiu_prior LIKE '%PLANIFICAT>CON>MG%' THEN 'M'
        ELSE '' END CONJUNTA, trunc(DATA)
        FROM dwsisap.SISAP_MASTER_VISITES smv 
        WHERE (motiu_prior LIKE '%PLANIFICAT>CON%')
        AND SITUACIO = 'R'
        UNION
        SELECT PACIENT, 'M', TRUNC(DATA) FROM DWSISAP.SISAP_MASTER_VISITES
        WHERE FLAG_SISAP = 1
        AND situacio = 'R' AND SERVEI = 'MG'"""

for pacient, conjunta, dia in u.getAll(sql, 'exadata'):
    if pacient in covid_2_redics:
        pacient = covid_2_redics[pacient]
        if pacient in al_circuit:
            if al_circuit[pacient] < dia: al_circuit[pacient] = dia
        else: al_circuit[pacient] = dia

print('Time execution {}'.format(datetime.now() - ts))

ts = datetime.now()
print('comencem')
# Baixem dades dbs
sql = """SELECT c_cip, grup, c_up, c_centre, c_metge, C_SECTOR, C_INFERMERA, C_EDAT_ANYS,
            CASE WHEN PS_ATDOM_DATA IS NOT NULL THEN 1 END ATDOM,
            CASE WHEN C_INSTITUCIONALITZAT IS NOT NULL THEN 'R' WHEN PS_ATDOM_DATA IS NOT NULL AND C_INSTITUCIONALITZAT IS NULL THEN 'A' END ATDOM_RESIDENCIA, 
            CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL THEN 1 end CI,
            CASE WHEN PS_ACV_MCV_DATA IS NOT NULL THEN 1 end AVC,
            CASE WHEN ps_renal_cro_data IS NOT NULL THEN 1 end MRC,
            CASE WHEN PS_HTA_DATA IS NOT NULL THEN 1 END HTA,
            CASE WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 1 END dm2,
            CASE WHEN PS_INSUF_CARDIACA_DATA IS NOT NULL THEN 1 END IC,
            CASE WHEN PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 1 END MPOC_GREU,
            CASE WHEN PS_MPOC_ENFISEMA_DATA IS NOT NULL THEN 1 END MPOC_LLEU,
            CASE WHEN PS_DISLIPEMIA_DATA IS NOT NULL THEN 1 END dislipemia,
            (CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL OR PS_INSUF_CARDIACA_DATA IS NOT NULL 
            OR PS_ACV_MCV_DATA IS NOT NULL OR (PS_MPOC_ENFISEMA_DATA IS NOT NULL)THEN 3
            WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 2 ELSE 1 END) risc,
            CASE WHEN F_HTA_COMBINACIONS is NOT NULL OR F_HTA_DIURETICS IS NOT NULL OR F_HTA_IECA_ARA2 IS NOT NULL THEN 1 ELSE 0 END FarmHTA,
            CASE WHEN PS_HIPOTIROIDISME_DATA IS NOT NULL THEN 1 END HIPOTIROIDISME,
            V_HBA1C_DATA glicada_DM,
            V_COL_TOTAL_DATA  colest,
            V_ECG_AMB_DATA ECG,
            CASE WHEN (V_FEV1_DATA >= V_FEV1_FVC_DATA or V_FEV1_FVC_DATA IS  null) AND (V_FEV1_DATA >= VC_VAL_ESPIRO_DATA or VC_VAL_ESPIRO_DATA IS  null) THEN V_FEV1_DATA 
                WHEN (V_FEV1_FVC_DATA >= V_FEV1_DATA or V_FEV1_DATA IS  null) AND (V_FEV1_FVC_DATA >= VC_VAL_ESPIRO_DATA or VC_VAL_ESPIRO_DATA IS  null) THEN V_FEV1_FVC_DATA 
                WHEN (VC_VAL_ESPIRO_DATA >= V_FEV1_DATA or V_FEV1_DATA IS  null) AND (VC_VAL_ESPIRO_DATA >= V_FEV1_FVC_DATA or V_FEV1_FVC_DATA IS  null) THEN VC_VAL_ESPIRO_DATA 
			END espiro1,
            V_FEV1_FVC_DATA espiro2,
            V_FONS_ULL_DATA fons_ull,
            C_UAS
        fROM 
            dbs 
        WHERE 
            C_EDAT_ANYS > 14 AND
            c_sector != '6951' AND
            ((c_up = '00441' and c_metge = '4T') OR 
                C_UP in ('00386', '00296', '00201','00170','00198','00183','00173', '00371', '00088', '00151')) AND 
            (PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL 
            OR ps_hta_data IS NOT NULL 
            OR PS_DIABETIS2_DATA IS NOT NULL OR 
            PS_INSUF_CARDIACA_DATA is NOT NULL OR 
            PS_MPOC_ENFISEMA_DATA IS NOT NULL  OR 
            PS_DISLIPEMIA_DATA IS NOT NULL OR
            PS_ACV_MCV_DATA IS NOT NULL OR
            ps_renal_cro_data IS NOT NULL) AND 
            PR_MACA_DATA IS NULL AND
            PS_DEMENCIA_DATA IS NULL AND
            PS_CURES_PALIATIVES_DATA IS NULL AND 
            (CASE WHEN (PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL 
            OR PS_INSUF_CARDIACA_DATA IS NOT NULL 
            OR PS_MPOC_ENFISEMA_DATA IS NOT NULL
            OR PS_ACV_MCV_DATA IS NOT NULL)THEN 3
            WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 2 ELSE 1 END) IN (1)"""
upload = []

info_resucitar = defaultdict(list)
cronics_set = set()
info_up_uba = {}

for (c_cip, grup, c_up, c_centre, c_metge, C_SECTOR, C_INFERMERA, c_edat, atdom, ATDOM_RESIDENCIA,
        CI, AVC, MRC, HTA, dm2, IC, MPOC_GREU, MPOC_LLEU, dislipemia, risc, FarmHTA,
        HIPOTIROIDISME, glicada_DM, colest, ECG, espiro1, espiro2, fons_ull, C_UAS) in u.getAll(sql, 'redics'):
    visita = colest
    if c_cip in al_circuit:
        visita = al_circuit[c_cip]
    elif c_cip in analitiques:
        visita = analitiques[c_cip]
        colest = analitiques[c_cip]
    if grup == 13 and c_cip not in al_circuit:
        visita = espiro1
    upload.append([c_cip, c_up, c_centre, c_metge, C_SECTOR, C_INFERMERA, c_edat, atdom, ATDOM_RESIDENCIA, 
                    CI, AVC, MRC, HTA, dm2, IC, MPOC_GREU, MPOC_LLEU, dislipemia, 
                    risc, FarmHTA, HIPOTIROIDISME, glicada_DM, colest, visita, ECG, 
                    espiro1, espiro2, fons_ull, C_UAS])
    if risc == 1:
        info_resucitar[c_cip].append(C_SECTOR)
        cronics_set.add((c_cip, C_SECTOR))
    info_up_uba[c_cip] = [c_up, c_metge]

print('crear dict')    
dbs = pd.DataFrame(upload)
dbs.columns =['C_CIP', 'C_UP', 'C_CENTRE', 'C_METGE', 'C_SECTOR', 'C_INFERMERA',
                'C_EDAT_ANYS', 'ATDOM',
                'ATDOM_RESIDENCIA', 'CI', 'AVC', 'MRC', 'HTA', 'DM2', 'IC', 
                'MPOC_GREU', 'MPOC_LLEU', 'DISLIPEMIA', 'RISC', 'FARMHTA',
                'HIPOTIROIDISME', 'GLICADA_DM', 'COLEST', 'VISITA', 'ECG', 
                'ESPIRO1', 'ESPIRO2', 'FONS_ULL', 'C_UAS']
print('len dbs:', len(dbs))
print('Time execution {}'.format(datetime.now() - ts))

# Weird bug
dbs = dbs.replace(np.nan, np.nan)
print(dbs.columns)

# Transformacio de les dades, conversio a datetime
for date_type in ('GLICADA_DM', 'COLEST', 'VISITA'):
    dbs[date_type]= pd.to_datetime(dbs[date_type])
    
    
for e in ('ESPIRO1', 'ESPIRO2', 'FONS_ULL', 'ECG'):
    print(e)
    for i in range(len(dbs)):
        try:
            if np.isnan(dbs[e][i]) == False:
                pass
        except:
            if str(dbs[e][i])[0:3] not in ('200', '199', '201', '202'):
                dbs[e][i] = np.nan
    dbs[e]= pd.to_datetime(dbs[e])


# Necessitem que les UPs tinguin 5 caracters i que les poguem tractar com strings
dbs['C_UP'] = dbs['C_UP'].apply(lambda x: str(x).zfill(5))


# Nomes ens quedem amb els que tenen un risc de 1 a dbs_risc
dbs_risc = dbs[dbs.RISC == 1]
dbs_risc.reset_index(inplace=True, drop=True)
print('len dels de risc: ', len(dbs_risc))


# Informaciometges i vectors dies anuals
def up_metges_mes(up_metge, col, metges_vistos):
    if col.month not in (1,8,12):
        return up_metge
    else: 
        if up_metge not in metges_vistos: return up_metge
        else: return ' '

def up_mes(df, metges_vistos):
    return pd.Series([
        up_metges_mes(up_metge, col, metges_vistos)
        for (up_metge, col) in 
        zip(df['up_metge'], df['VISITA'])
      ])

# Metges to dict, id_metge i nombre de pacients anuals
metges_vistos = []
dbs_risc['up_metge'] = dbs_risc["C_UP"].astype(str)+dbs_risc["C_METGE"]    
dbs_risc['up_metge_mesos'] = up_mes(dbs_risc, metges_vistos)
metges = dbs_risc['up_metge_mesos'].value_counts()
metges = metges.to_dict()
metges_int = dict(zip(np.arange(len(metges)), metges.keys()))
metges_int_reves = dict(zip(metges.keys(), np.arange(len(metges))))
metges_id = list(metges_int.keys())
# Repartim els metges al llarg de l'any, ho fem per mesos, tenim 9 mesos
# Nombre de persones que ha de visitar cada metge per mes
metges = {k: np.ceil(v / 11) for k, v in metges.items()}
# Variable dies de l'any
start = datetime.today() + relativedelta(days=21)
end = start + relativedelta(years=1)
one_year = pd.date_range(start = start, end = end).to_pydatetime().tolist()
len(one_year)

# Construïm matriu de zeros on # files = # metges i # columnes = # dies any
matrix = np.zeros((len(metges_id), len(one_year)), dtype=int)


# Funcio que marca amb -1 els dies de l'any que no 
# volem que la gent es visiti
# Caps de setmana + gener i agost + 15 dies desembre
def incrementant(one_year):
    increment = []
    for i in range(len(one_year)):
        # caps de setmana
        if one_year[i].weekday() > 4:
            increment.append(-1)
        # gener i agost
        elif one_year[i].month == 1 or one_year[i].month == 8:
            increment.append(-1)
        # 15 ultims dies desembre
        elif one_year[i].month == 12 and one_year[i].day >= 15:
            increment.append(-1) 
        else: increment.append(1)
    return np.array(increment)

# Calcular si es cap de setmana
def cap_setmana(one_year):
    increment_DS = []
    increment_DG = []
    for i in range(len(one_year)):
        if one_year[i].day == 6: increment_DS.append(i)
        elif one_year[i].day == 7: increment_DG.append(i)
    return increment_DS, increment_DG

# Calculem els primers dies dels mesos que volem programar
def dia_1_mes(one_year):
    dies_1 = []
    dies_1.append(0)
    for i in range(len(one_year)):
        if one_year[i].day == 1 and one_year[i].month not in (1,8,12): dies_1.append(i)
    return dies_1

# Estimem que cada mes tenim 20 dies laborables
for key, value in metges_int.items():
    v = int(metges[value] // 20) * incrementant(one_year) # Nombre de persones que ha de visitar a diari
    r = metges[value] % 20
    
    dia1 = dia_1_mes(one_year)
    for i, j in zip(dia1, dia1[1:] + [366]):
        r1 = r
        while r1 > 0:
            if i >= j: r1 = 0
            elif v[i] >= 0:
                v[i] += 1
                r1 -= 1
            i += 1
            
    matrix[key] = v # Els que ha de visitar sí o sí a diari

# Ordenem el dataframe per colest i després glicada 
dbs_baix = dbs_risc.sort_values(by=['VISITA'], ascending=(False))
dbs_baix.reset_index(inplace=True, drop=True)

for e in ('GLICADA_DM', 'COLEST', 'ECG', 'ESPIRO1', 'FONS_ULL', 'VISITA'):
    # Els Nulls els posem a data epoch
    dbs_baix[e] = dbs_baix[e].apply(lambda x: x if not pd.isnull(x) else datetime(1970,1,1,0,0))
    # Creem les columnes noves
    e = 'proper_' + e
    dbs_baix[e] = np.nan

# Sumem a totes les proves la seva frequencia (1 o 2 anys dependen de diagnostics)
def colest(date, CI, IC, DM, GREU, LLEU, HTA):
    if (GREU == 1 or LLEU == 1) and (CI + IC + DM == 0):
        return None 
    else: return (date + pd.offsets.DateOffset(years=1))
print('colest')
# CALCUL DE LES PROPERES PROVES
def proper_COLEST(df):
    return pd.Series([
        colest(date, CI, IC, DM, GREU, LLEU, HTA)
        for (date, CI, IC, DM, GREU, LLEU, HTA) in 
        zip(df['COLEST'], df['CI'], df['IC'], df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'], df['HTA'])
      ])

dbs_baix['proper_COLEST'] = proper_COLEST(dbs_baix)



print('calcul propera visita')
no_fets = 0


# Sumem a totes les proves la seva frequencia (1 o 2 anys dependen de diagnostics)
def visita(date, CI, IC, DM, GREU, LLEU, HTA):
    return (date + pd.offsets.DateOffset(years=1))
# CALCUL DE LES PROPERES PROVES
print('visita')
def propera_VISITA(df):
    return pd.Series([
        visita(date, CI, IC, DM, GREU, LLEU, HTA)
        for date, CI, IC, DM, GREU, LLEU, HTA in 
        zip(df['VISITA'], df['CI'], df['IC'], df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'], df['HTA'])
      ])
dbs_baix['propera_VISITA'] = propera_VISITA(dbs_baix)
print('calcul propera visita')
no_fets = 0

exclosos = set()
# Exclusió dels exclosos per tal de no recalcular-los constantment
sql = """select HASH 
            from planificat
            where estat in (2, 5)
            AND (TRUNC(SYSDATE) - trunc(to_date(estat_data, 'yyyy/mm/dd hh24:mi:ss'))) < 400"""
for hash, in u.getAll(sql, 'pdp'):
    exclosos.add(hash)

ts = datetime.now()
one_year2 = [int(time.mktime(t.timetuple()) + t.microsecond/1e6 * 1000) for t in one_year]
def dia(id, data, metge, matrix, metges_int_reves, one_year2, one_year, no_fets, exclosos):
    if id in exclosos: 
        if data >= datetime.today() + relativedelta(days=21):
            if data.weekday == 5: i = -1
            if data.weekday == 6: i = 1
            else: i = 0
            return data + relativedelta(days=i)
        else:
            data_final = datetime.today() + relativedelta(days=25)
            if data_final.weekday == 5: i = -1
            if data_final.weekday == 6: i = 1
            else: i = 0
            return data_final + relativedelta(days=i)

    if data >= datetime.today() + relativedelta(days=21):
        # Per no posar gent durant els caps de setmana. gent que li toca dss passa a dv i els de dg a dll
        if data.weekday == 5: i = -1
        if data.weekday == 6: i = 1
        else: i = 0
        try:
            final = int((data + relativedelta(days=i)).timestamp() * 1000)
            itemindex = np.where(np.array(one_year2)==final)[0][0]
            matrix[metges_int_reves[metge],itemindex] -= 1
        except: 
            pass
        return data + relativedelta(days=i)
    else:
        try:
            index = np.nonzero(matrix[metges_int_reves[metge]] > 0)[0][0]
            matrix[metges_int_reves[metge]][index] -= 1
            return one_year[index]
        except:
            no_fets += 1

def assignar_metge(df, matrix, metges_int_reves, one_year2, one_year, no_fets, exclosos):
    return pd.Series([
        dia(id, data, metge, matrix, metges_int_reves, one_year2, one_year, no_fets, exclosos)
        for (id, data, metge) in 
        zip(df['C_CIP'], df['propera_VISITA'], df['up_metge'])
      ])

dbs_baix = dbs_baix.sort_values(by=['propera_VISITA'], ascending=False)
dbs_baix.reset_index(inplace=True, drop=True)
dbs_baix['data_final'] = assignar_metge(dbs_baix, matrix, metges_int_reves, one_year2, one_year, no_fets, exclosos)
print('no fets: ', no_fets)

print('Time execution seguent visita {}'.format(datetime.now() - ts))

print('assignacio grups')
ts = datetime.now()
def grups(CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU, AVC, MRC):
    if IC == 1:
        if DM == 1 and (GREU == 1 or LLEU == 1): return 1
        elif DM == 1: return 2
        elif (GREU == 1 or LLEU == 1): return 3
        else: return 4
    elif CI == 1 or AVC == 1 or MRC==1:
        if DM == 1 and (GREU == 1 or LLEU == 1): return 5
        elif DM == 1: return 6
        elif (GREU == 1 or LLEU == 1): return 7
        else: return 8
    elif DM == 1:
        if GREU == 1 or LLEU == 1: return 9
        else: return 10
    elif GREU == 1 or LLEU == 1:
        if HTA == 1: return 11
        elif DISLIPEMIA == 1: return 12
        else: return 13
    elif HTA == 1: return 14
    elif DISLIPEMIA == 1: return 15


def agrupadors(df):
    return pd.Series([
        grups(CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU, AVC, MRC)
        for (CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU, AVC, MRC) in zip(df['CI'], df['IC'], df['HTA'], df['DISLIPEMIA'],
                                                        df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'], df['AVC'], df['MRC'])
      ])

dbs_baix['grup'] = agrupadors(dbs_baix)
print('Time execution per agrupadors {}'.format(datetime.now() - ts))

print('calcul de les properes proves')
ts = datetime.now()
def fons_ull(date, grup):
    if grup in (1,2,5,6,9,10):
        return date + relativedelta(years=2)
    else:
        return datetime(1970,1,1,0,0)
    
def proper_FONS_ULL(df):
    return pd.Series([
        fons_ull(date, grup)
        for (date, grup) in 
        zip(df['FONS_ULL'], df['grup'])
      ])

def espiro(date, grup):
    return (date + relativedelta(years=2) if grup in (1,3,5,77,9,11,12,13) else datetime(1970,1,1,0,0))
    
def proper_espiro(df):
    return pd.Series([
        espiro(date, grup)
        for (date, grup) in zip(df['ESPIRO1'], df['grup'])
      ])

def ECG(date, grup):
    if grup in (9,10,11,14):
        return date + relativedelta(years=2)
    elif grup in (1,2,3,4,5,6,7,8):
        return date + relativedelta(years=1)
    else: return datetime(1970,1,1,0,0)
    
def proper_ECG(df):
    return pd.Series([
        ECG(date, grup)
        for (date, grup) in zip(df['ECG'], df['grup'])
      ])

dbs_baix['proper_FONS_ULL'] = proper_FONS_ULL(dbs_baix)
dbs_baix['proper_ESPIRO1'] = proper_espiro(dbs_baix)
dbs_baix['proper_ECG'] = proper_ECG(dbs_baix)

print('Time execution per calcul seguents proves {}'.format(datetime.now() - ts))

print('exclusions')
ts = datetime.now()
def analisi_sang(edat, grup):
    if (edat > 80 and (grup == 12 or grup == 15)) or grup == 13:
        return 0
    else: 
        return 1

def P_ANALISI_SANG(df):
    return pd.Series([
        analisi_sang(edat, grup)
        for (edat, grup) in 
        zip(df['C_EDAT_ANYS'], df['grup'])
      ])

def orina(edat, HTA, DM2, grup):
    if (edat > 80 and DM2 == 1) or (HTA == 1 and edat > 75 and DM2 == 0) or (grup in (12,13,15)):
        return 0
    else: 
        return 1

def P_ORINA(df):
    return pd.Series([
        orina(edat, HTA, DM2, grup)
        for (edat, HTA, DM2, grup) in 
        zip(df['C_EDAT_ANYS'], df['HTA'], df['DM2'], df['grup'])
      ])

def espiro1(espiro, visita, grup, edat, ATDOM):
    if ATDOM == 1 or edat > 80:
        return 0
    elif grup in (1,3,5,7,9,11,12,13):
        if espiro <= visita or espiro - relativedelta(days=180) <= visita:
            return 1
    else: 
        return 0

def P_ESPIRO(df):
    return pd.Series([
        espiro1(ESPIRO, VISITA, grup, edat, ATDOM)
        for (ESPIRO, VISITA, grup, edat, ATDOM) in 
        zip(df['proper_ESPIRO1'], df['data_final'], df['grup'], df['C_EDAT_ANYS'], df['ATDOM'])
      ])

def FO_(data, visita, grup, ATDOM, edat):
    if ATDOM == 1 or edat > 80:
        return 0
    elif grup in (1,2,5,6,9,10):
        if data + relativedelta(days=180) > visita:
            return 0
        else: return 1
    else: 
        return 0

def P_FO(df):
    return pd.Series([
        FO_(FO, VISITA, DM, ATDOM, edat)
        for (FO, VISITA, DM, ATDOM, edat) in zip(df['proper_FONS_ULL'], df['data_final'], df['grup'], df['ATDOM'], df['C_EDAT_ANYS'])
      ])

def COLEST(data, visita, CI, IC, DM, GREU, LLEU, DIS, HTA):
    if ((CI + IC + DM) == 0 and (GREU == 1 or LLEU == 1)) or ((LLEU + GREU + HTA + DIS) >= 3 and (CI + IC + DM) == 0) or ((HTA + DIS) == 2 and (CI + IC + DM + LLEU + GREU) == 0):
        return 0
    else: 
        if data <= visita or data - relativedelta(days=180) <= visita: return 1
        else: return 0

def P_LIPIDS(df):
    return pd.Series([
        COLEST(COL, VISITA, CI, IC, DM, GREU, LLEU, DIS, HTA)
        for (COL, VISITA, CI, IC, DM, GREU, LLEU, DIS, HTA) in zip(df['proper_COLEST'], df['data_final'],
                                     df['CI'], df['IC'], df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'], df['DISLIPEMIA'], df['HTA'])
      ])

def ECG1(data, visita, grup, ATDOM):
    if grup in (12,13,15):
        return 0
    elif ATDOM == 1:
        return 0
    else: 
        if data <= visita or data - relativedelta(days=180) <= visita: return 1
        else: return 0

def P_ECG(df):
    return pd.Series([
        ECG1(EC, VISITA, grup, ATDOM)
        for (EC, VISITA, grup, ATDOM) in zip(df['proper_ECG'], df['data_final'], 
                                     df['grup'], df['ATDOM'])
      ])

dbs_baix['p_analisi_sang'] = P_ANALISI_SANG(dbs_baix)
dbs_baix['p_orina'] = P_ORINA(dbs_baix)
dbs_baix['p_espiro'] = P_ESPIRO(dbs_baix)
dbs_baix['p_fo'] = P_FO(dbs_baix)
dbs_baix['p_lipids'] = P_LIPIDS(dbs_baix)
dbs_baix['p_ecg'] = P_ECG(dbs_baix)

print('Time execution per calcul seguents proves exclusions {}'.format(datetime.now() - ts))

print('diagnostics')
def dx(CI, IC, AVC, MRC, HTA, DISLIPEMIA, DM, GREU, LLEU):
    txt = ''
    buit = True
    if IC == 1:
        txt = txt + 'IC'
        buit = False
    if CI == 1:
        if buit:
            txt = txt + 'CI'
            buit = False
        else:
            txt = txt + ',CI'
    if AVC == 1:
        if buit:
            txt = txt + 'AVC'
            buit = False
        else:
            txt = txt + ',AVC'
    if MRC == 1:
        if buit:
            txt = txt + 'MRC'
            buit = False
        else:
            txt = txt + ',MRC'
    if DM == 1:
        if buit:
            txt = txt + 'DM2'
            buit = False
        else:
            txt = txt + ',DM2'
    if GREU == 1 or LLEU == 1:
        if buit:
            txt = txt + 'MPOC'
            buit = False
        else:
            txt = txt + ',MPOC'
    if HTA == 1:
        if buit:
            txt = txt + 'HTA'
            buit = False
        else:
            txt = txt + ',HTA'
    if DISLIPEMIA == 1:
        if buit:
            txt = txt + 'dislipemia'
            buit = False
        else:
            txt = txt + ',dislipemia'
    return txt


def diagnostic(df):
    return pd.Series([
        dx(CI, IC, AVC, MRC, HTA, DISLIPEMIA, DM, GREU, LLEU)
        for (CI, IC, AVC, MRC, HTA, DISLIPEMIA, DM, GREU, LLEU) in zip(df['CI'], df['IC'], df['AVC'], df['MRC'], df['HTA'], df['DISLIPEMIA'],
                                                        df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'])
      ])

dbs_baix['dx'] = diagnostic(dbs_baix)

print('TSH I IONOGRAMA')
dbs_baix['TSH'] = dbs_baix['HIPOTIROIDISME'].apply(lambda x: 1 if x == 1 else 0)
dbs_baix['IONOGRAMA'] = (dbs_baix['HTA'] + dbs_baix['FARMHTA']).apply(lambda x: 1 if x >= 1 else 0)

print('filtrem columnes')
dbs_joan_1 = dbs_baix[['C_CIP', 'C_UP', 'C_CENTRE', 'C_SECTOR', 'C_METGE', 'C_INFERMERA', 'ATDOM_RESIDENCIA', 'dx',
                'GLICADA_DM', 'COLEST', 'ECG', 'ESPIRO1',
                'FONS_ULL', 'data_final', 'TSH',
                'p_espiro', 'p_fo', 'p_ecg', 'p_analisi_sang', 'p_orina', 'grup', 'C_EDAT_ANYS', 'C_UAS']]

dbs_joan_1 = dbs_joan_1.rename(columns={'GLICADA_DM': 'DM_uv', 'COLEST': 'lipids_uv', 'ECG': 'ECG_uv', 
                                    'ESPIRO1' : 'espiro_uv', 'FONS_ULL': 'FU_uv', 'data_final': 'data_recomanada'})
dbs_joan_1['data_recomanada'] = dbs_joan_1['data_recomanada'].apply(lambda x: x.date())
print('transformacions de dates')
for e in ('DM_uv', 'lipids_uv', 'ECG_uv', 'espiro_uv', 'FU_uv'):
    dbs_joan_1[e] = dbs_joan_1[e].apply(lambda x: x if x.year not in (1970, 1971, 1972, 1973) else pd.NaT)


print('crear csv')
dbs_joan_1.to_csv(u.tempFolder + "visualitzacio_14_15_prova_uas.csv")
