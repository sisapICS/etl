import sisapUtils as u
from collections import defaultdict
import pandas as pd
import math
from datetime import datetime
import numpy as np


# peticions fetes
print('getting cips')
cips = dict()
sql = """SELECT b.usua_cip_cod, DBMS_LOB.SUBSTR(a.protocols, 4000,1), PROGRAMAT FROM PREDUFFA.SISAP_COVID_GIS_PETICIONS a 
        INNER JOIN pdptb101 b ON a.cip = b.usua_cip"""
for cip, protocol, programat in u.getAll(sql, "pdp"):
    if protocol:
        cips[cip] = (str(protocol.encode('ISO-8859-1')), int(programat))
    else: cips[cip] = ('', int(programat))

print("reading dbs")
dbs = pd.read_csv("../11dbs/visualitzacio.csv")

columnes = ['lipids_uv', 'ECG_uv', 'espiro_uv', 'FU_uv', 'data_recomanada']
for col in columnes:
    dbs[col]= pd.to_datetime(dbs[col])

# NECESSITEM UP AMB 5 CARACTERS I COM A STRING
def convert_5(x):
    # return '0' * len(str(x)) + str(x)
    x = str(x)
    if len(x) == 1:
        x = '0000' + x
    elif len(x) == 2:
        x = '000' + x
    elif len(x) == 3:
        x = '00' + x
    elif len(x) == 4:
        x = '0' + x
    return x
dbs['C_UP'] = dbs['C_UP'].apply(lambda x: convert_5(x))

print("creem taula")
table = 'cronics_filtrats'
db = 'pdp'

u.createTable(table, "(C_CIP varchar(40), C_UP varchar(5), C_SECTOR varchar(5), C_METGE varchar(6), C_INFERMERA varchar(6)\
                    , ATDOM_RESIDENCIA varchar(1), dx varchar(50), analisi_uv date, ECG_uv date\
                    , espiro_uv date, fo_uv date, data_recomanada date\
                    , tsh int, p_espiro int, p_fo int, p_ecg int, p_analisi int, p_orina int\
                    , grup int, peticio_feta int, protocol varchar(1000))", db, rm=True) 

upload = []

for i in range(len(dbs)):
    v = [
        dbs['C_CIP'][i], 
        dbs['C_UP'][i],
        dbs['C_SECTOR'][i],
        dbs['C_METGE'][i],
        dbs['C_INFERMERA'][i],
        dbs['ATDOM_RESIDENCIA'][i],
        str(dbs['dx'][i]),
        dbs['lipids_uv'][i],
        dbs['ECG_uv'][i],
        dbs['espiro_uv'][i],
        dbs['FU_uv'][i],
        dbs['data_recomanada'][i],
        dbs['TSH'][i],
        dbs['p_espiro'][i],
        dbs['p_fo'][i],
        dbs['p_ecg'][i],
        dbs['p_analisi_sang'][i],
        dbs['p_orina'][i],
        dbs['grup'][i], 
        0, 
        '',
        0
    ]

    v = [
        None
        if (isinstance(v_i, float) and math.isnan(v_i)) or
        isinstance(v_i, type(pd.NaT)) else v_i
        for v_i in v
    ]

    cip = dbs['C_CIP'][i]
    if cip in cips:
        v[-3] = 1
        v[-2] = cips[cip][0]
        v[-1] = cips[cip][1]

    upload.append(v)
    if i % 10000 == 0: 
        u.listToTable(upload, table, db)
        print(i)
        upload = []


u.listToTable(upload, table, db)
print('fi', len(upload))

# u.execute("DROP TABLE seguent_visita_cronics", 'pdp')
# u.execute("ALTER TABLE pdp.cronics_filtrats  RENAME TO seguent_visita_cronics", 'pdp')
# u.execute("CREATE INDEX up_metge ON pdp.cronics_filtrats(c_up, c_metge)", 'pdp')
# u.execute("CREATE INDEX cip ON pdp.cronics_filtrats(c_cip)", 'pdp')
  

