import sisaptools as t
import re


sql = "select HASH, ecap_nom, ecap_tlf_1, ecap_tlf_2, RCA_UP \
               from dbc_poblacio b \
               where SITUACIO = 'A' and \
               ecap_sector is not null"
upload = {}
for hash, _nom, _tlf1, _tlf2, up in t.Database("exadata", "data").get_all(sql):  # noqa
    nom = t.aes.AESCipher().decrypt(_nom)
    for el in (_tlf1, _tlf2):
        if el:
            conv = t.aes.AESCipher().decrypt(el)
            this = re.sub(r"\D", "", str(conv))
            if len(this) == 9 and this[0] in ("6", "7"):  # noqa
                upload[hash] = (nom, up, this)
                break
print('pob')
sql = """select scs_codi, ics_desc, amb_desc, sap_desc
        from nodrizas.cat_centres"""
centres_desc = {}
for up, up_d, amb, sap in t.Database("p2262", "nodrizas").get_all(sql):
    centres_desc[up] = (up_d, amb, sap)

to_send = set()
sql = """SELECT DISTINCT pacient FROM
            dwsisap.SISAP_MASTER_VISITES SAMPLE (0.35)
            WHERE situacio = 'R'
            AND (MOTIU_PRIOR LIKE '%PLANIFICAT>SEG%' OR motiu_prior
            LIKE '%PLANIFICAT>CON%')"""
for hash, in t.Database("exadata", "data").get_all(sql):
    if hash in upload:
        nom, up, telf = upload[hash]
        if up in centres_desc:
            up_d, amb, sap = centres_desc[up]
        if len(to_send) < 200:
            to_send.add((nom, telf, amb, sap, up_d))
print('visites')
to_send = list(to_send)
file = "enquesta_satisfaccio"
t.TextFile(file).write_iterable(to_send, ";", "\r\n")
with t.SFTP("sisap") as sftp:
    sftp.put(file, "sms/planificat/{}".format(file))

print(to_send[0])
