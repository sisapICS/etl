import sisapUtils as u
import collections as c

# ids = set()
# sql = """select id_cip_sec from import.problemes 
#             where year(pr_dde) = 2021 AND pr_cod_o_ps = 'C' AND
#             pr_data_baixa = 0 AND pr_hist = '1' and pr_up = ''
#             and pr_cod_ps in (select criteri_codi from nodrizas.eqa_criteris ec where agrupador = 722)"""
# print('a')
# for id, in u.getAll(sql, 'import'):
#     ids.add(id)

# print('b')
# sql = """select id_cip_sec, up from nodrizas.assignada_tot"""

# ups = c.Counter()
# up_id = dict()
# for id, up in u.getAll(sql, 'nodrizas'):
#     up_id[id] = up
#     if id in ids:
#         ups[up] += 1

# sql = """select id_cip_sec, up from import.assignadahistorica_s2020"""
# for id, up in u.getAll(sql, 'import'):
#     if id not in up_id:
#         if id in ids:
#             ups[up] += 1

# print('c')
# upload = []
# for k, v in ups.items():
#     upload.append((k,v))

# cols = '(up varchar(5), missings int)'
# u.createTable('ups_missings', cols, 'test', rm=True)
# u.listToTable(upload, 'ups_missings', 'test')

cips = (
    'COMU153021800',
    'ALDA061010100',
    'BEGG064033000',
    'SOME031022600',
    'COBU139051500',
    'MAHF150082700',
    'GAJI048122300',
    'SIMI082040200',
    'ALFE136030600',
    'ROTR030101800',
    'RUMI062082000',
    'CEOL135050200',
    'MUBA130112400',
    'BELA160021800',
    'ENBE144110600',
    'CAVE170060200',
    'ALAN054071800',
    'SAGO072031400',
    'GAVA040050800'
)

sql = """SELECT USUA_CIP, USUA_UAB_SERVEI_CENTRE  
        FROM usutb040 where usua_situacio = 'A' """
for s in u.sectors:
    print(s)
    for cip, centre in u.getAll(sql, s):
        if cip in cips:
            print(cip, centre)