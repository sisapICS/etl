# -*- coding: utf8 -*-

import sisapUtils as u
import collections as c
import sisaptools as t


LITERALS = {
        'EQA0201':	u'Revisar tractament anticoagulant o antiagregant',
        'EQA0202':	u'Revisar tractament anticoagulant o antiagregant',
        'EQA0203':	u'Revisar tractament anticoagulant o antiagregant',
        'EQA0204':	u'Control LDL',
        'EQA0205':	u'Control pressió arterial',
        'EQA0206':	u'Revisar tractament betabloquejant',
        'EQA0207':	u'Revisar tractament IECA/ARA2',
        'EQA0208':	u'Cribratge peu DM2',
        'EQA0209':	u'Control HbA1C',
        'EQA0210':	u'Cribratge retinopatia DM2',
        'EQA0212':	u'Control pressió arterial',
        'EQA0213':	u'Control pressió arterial',
        'EQA0235':	u'Control pressió arterial',
        'EQA0214':	u'Control LDL',
        'EQA0215':	u'Càlcul RCV',
        'EQA0220':	u'Verificar inhaladors',
        'EQA0219':	u'Hb i ferritina de seguiment',
        'EQA0227':	u'Revisar TSH',
        'EQA0301':	u'Cribratge/intervenció alcohol/tabac',
        'EQA0302':	u'Cribratge/intervenció alcohol/tabac',
        'EQA0304':	u'Cribratge/intervenció alcohol/tabac',
        'EQA0305':	u'Cribratge/intervenció alcohol/tabac',
        'EQA0501':	u'Revisar vacunació',
        'EQA0502':	u'Revisar vacunació',
        'EQA0308':	u'Revisar vacunació', 
        'EQA0309':	u'Revisar vacunació',
        'EQA0310':	u'Revisar vacunació', 
        'EQA0312':	u'Revisar vacunació',
        'EQA0503':	u'Revisar vacunació',
        'EQA0504':	u'Revisar vacunació', 
        'EQA0401':	u'Valoració integral ATDOM',
        'EQA0216':	u'Revisar inadequació estatines',
        'EQA0222':	u'Revisar gastroprotecció IBP',
        'EQA0238':	u'Revisar inadequació estatines'
}


class Estats_EQA():
    def __init__(self):
        print('taula')
        self.table = 'planificateqa'
        self.db = 'pdp'
        cols = """(hash varchar2(40), sector varchar2(5), n_missings int, missings varchar2(500), n_reco int, recomanacio varchar2(500))"""
        u.createTable(self.table, cols, self.db, rm=True)
        self.db2 = 'exadata'
        u.createTable(self.table, cols, self.db2, rm=True)
        u.execute("GRANT SELECT ON planificateqa TO DWSISAP_ROL", 'exadata')
        self.table1 = 'planificat_eqa'
        self.db1 = 'altres'
        cols = """(id int, sector varchar(5), up varchar(5), uba varchar(5), ubainf varchar(5), grup int, N_missing int, N_missing_txt varchar(500), N_reco int, recomanacio varchar(500))"""
        u.createTable(self.table1, cols, self.db1, rm=True)
        sql = """select grip from nodrizas.dextraccio"""
        for grip, in u.getAll(sql, 'nodrizas'):
            if grip == 1: self.grip = True
            else: self.grip = False
        print('pacients')
        self.get_planificat()
        print('eqa')
        self.get_eqa()
        print('upload')
        self.uploading()
        u.execute("CREATE INDEX pac ON planificateqa(hash)", 'pdp')
    
    def get_planificat(self):
        hash_2_id = {}
        self.id_2_hash = {}
        sql = """select id_cip_sec, hash_d, codi_sector from import.u11"""
        for id, hash, sector in u.getAll(sql, 'import'):
            hash_2_id[(hash, sector)] = id
            self.id_2_hash[id] = hash
        
        self.planificat = {}
        sql = """SELECT c_cip, grup, c_sector FROM dbs WHERE grup > 0"""
        for hash, grup, sector in u.getAll(sql, 'redics'):
            if (hash, sector) in hash_2_id:
                id_cip_sec = hash_2_id[(hash, sector)]
                self.planificat[id_cip_sec] = (grup, sector)

    def get_eqa(self):
        self.missings = c.defaultdict(set)
        sql = """select id_cip_sec, up, uba, ubainf, ind_codi from eqa_ind.mst_indicadors_pacient 
                 where excl_edat = 0 and excl = 0 and ci = 0 and clin = 0"""
        for id_cip_sec, up, uba, ubainf, indicador in u.getAll(sql, 'eqa_ind'):
            if indicador[0:7] in LITERALS.keys():
                if id_cip_sec in self.planificat:
                    grup, sector = self.planificat[id_cip_sec]
                    if indicador[0:7] == 'EQA0501':
                        if self.grip:
                            self.missings[(id_cip_sec, up, uba, ubainf, grup, sector)].add(indicador[0:7])
                    else:
                        self.missings[(id_cip_sec, up, uba, ubainf, grup, sector)].add(indicador[0:7])

    def uploading(self):
        upload = []
        upload1 = []
        for (id_cip_sec, up, uba, ubainf, grup, sector), indicadors in self.missings.items():
            if id_cip_sec in self.id_2_hash:
                hash = self.id_2_hash[id_cip_sec]
                txt = ''
                reco = set()
                for indi in indicadors:
                    if indi in LITERALS:
                        reco.add(LITERALS[indi])
                    txt = txt + indi + ', '
                recomanacio = ''
                n_reco = 0
                for r in reco:
                    n_reco += 1
                    recomanacio = recomanacio + r + ', '
                recomanacio = recomanacio[:-2]
                txt = txt[:-2]
                upload.append((hash, sector, len(indicadors), txt, n_reco, recomanacio))
                upload1.append((id, sector, up, uba, ubainf, grup, len(indicadors), txt, n_reco, recomanacio))
        u.listToTable(upload, self.table, self.db)
        u.listToTable(upload, self.table, self.db2)
        # u.listToTable(upload1, self.table1, self.db1)


if __name__ == "__main__":
    Estats_EQA()