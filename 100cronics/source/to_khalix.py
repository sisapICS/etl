# -*- coding: utf-8 -*-

import sisapUtils as u
import hashlib as h
from unicodedata import normalize

KHALIX_CATALEG = 'ADM_CATALEG'
KHALIX_UBA = 'PLANIFPROG_UBA'
KHALIX_UP = 'PLANIFPROG'
KHALIX_ADM = 'PLANIFPROG_ADM'
DB = 'GAD'


class Export():
    def __init__(self):
        self.cataleg()
        self.export_khalix()
        self.tb = 'export_ecap_adm'
        self.db = 'gad'
        # self.centres()
        # self.get_dni()
        self.sisap_ecap_pla()

    def cataleg(self):
        # primer ho hem de copiar a nodrizas
        ups = {}
        sql = """select scs_codi, ics_codi from nodrizas.cat_centres"""
        for ics, br in u.getAll(sql, 'nodrizas'):
            ups[ics] = br
        upload = []
        sql = """select DISTINCT UP_ICS, nif, 
                    concat(CONCAT(nom, ' '), concat(CONCAT(cognom_1, ' '), cognom_2)) AS UAS_DESC
                    FROM DWSISAP.CAT_ADMINISTRATIUS
                    WHERE FIX_VARIABLE in ('F', 'N')
                    AND UP_ICS IS NOT NULL"""""
        for up, nif, desc in u.getAll(sql, 'exadata'):
            hash = h.sha1(nif).hexdigest().upper()[0:6]
            if up in ups:
                entity = ups[up] + 'A' + hash
                upload.append((entity, desc))
        entities = tuple(set([item for item, in u.getAll("select entity from adm_cataleg", "exadata")]))
        sql = """select DISTINCT CONCAT(SUBSTRING(UP, 1, 5), 'ALTRES') from altres.pla_indi
                    where up not in {} 
                    and LENGTH(UP) > 5""".format(entities)
        for up, in u.getAll(sql, 'altres'):
            upload.append((up, 'ALTRES'))
        cols = """(entity varchar(15), desc varchar(100))"""
        u.createTable(KHALIX_CATALEG, '(entity varchar(15), desc_adm varchar(100))', DB, rm = True)
        u.listToTable(upload, KHALIX_CATALEG, DB)
        # han d'haver-hi 4 camps a l'hora d'enviar a kahlix x la funció
        sql = """select *, concat('A','periodo'), '' from gad.ADM_CATALEG"""
        u.exportKhalix(sql,KHALIX_CATALEG)

    def export_khalix(self):
        sql = """select INDICADOR, concat('A','periodo'), 
                CONCAT(CONCAT(UP, PROF), UBA), ANALISI, AGRUPATS, 
                'NOIMP', 'DIM6SET', 'N', SUM(VALOR) from (
                select INDICADOR, UP, UBA, PROF, ANALISI, 
                CASE WHEN GRUP < 14 then 'PRIORITZATS' else 'NOPRIORITZATS' end AGRUPATS,
                VALOR
                from altres.pla_indi 
                where (indicador like 'PLA006%' or indicador like 'PLA007%'
                or indicador like 'PLA012%' or indicador like 'PLA011%') AND prof in ('I', 'M')) A
                group by INDICADOR, concat('A','periodo'), 
                CONCAT(CONCAT(UP, PROF), UBA), ANALISI, AGRUPATS, 
                'NOIMP', 'DIM6SET', 'N'
                union
        select
            INDI,
            concat('A','periodo'), 
            CONCAT(CONCAT(UP, PROF), UBA),
            ANALISI,
            AGRUPATS,
            'NOIMP',
            'DIM6SET',
            'N',
            SUM(VALOR)
        from
            (
            select
                distinct substr(INDICADOR, 1, 6) as INDI,
                concat('A','periodo'), 
                UP,
                UBA,
                PROF,
                ANALISI,
                case
                    when GRUP < 14 then 'PRIORITZATS'
                    else 'NOPRIORITZATS'
                end AGRUPATS,
                VALOR
            from
                altres.pla_indi
            where
                prof in ('I', 'M')
                and indicador in ('PLA011A', 'PLA012A')
                and analisi = 'DEN') A
        group by
            INDI,
            concat('A','periodo'), 
            CONCAT(CONCAT(UP, PROF), UBA),
            ANALISI,
            AGRUPATS,
            'NOIMP',
            'DIM6SET',
            'N'
        union
        select
            substr(INDICADOR, 1, 6),
            concat('A','periodo'), 
            CONCAT(CONCAT(UP, PROF), UBA),
            ANALISI,
            AGRUPATS,
            'NOIMP',
            'DIM6SET',
            'N',
            SUM(VALOR)
        from
            (
            select
                distinct
                INDICADOR,
                concat('A','periodo'), 
                UP,
                UBA,
                PROF,
                ANALISI,
                case
                    when GRUP < 14 then 'PRIORITZATS'
                    else 'NOPRIORITZATS'
                end AGRUPATS,
                VALOR
            from
                altres.pla_indi
            where
                prof in ('I', 'M')
                and ANALISI = 'NUM'
                AND (INDICADOR like 'PLA012%' or indicador like 'PLA011%')) A
        group by
            substr(INDICADOR, 1, 6),
            concat('A','periodo'), 
            CONCAT(CONCAT(UP, PROF), UBA),
            ANALISI,
            AGRUPATS,
            'NOIMP',
            'DIM6SET',
            'N'
                """
        u.exportKhalix(sql,KHALIX_UBA)

        sql = """select indicador, concat('A','periodo'),
                UP, analisi, 
                case when GRUP = 0 then 'NOPLAGRUP'
                when grup between 1 and 9 then concat('PLAGRUP0', GRUP)
                when GRUP >= 10 then concat('PLAGRUP', GRUP)
                end GRUPING,
                'NOIMP', 'DIM6SET', 'N', 
                VALOR
                from altres.pla_indi
                where length(up) >= 5 and uba = ''
                and prof in ('A') and indicador like 'PLA%'"""
        # u.exportKhalix(sql,KHALIX_ADM)

        sql = """select
                INDICADOR,
                concat('A', 'periodo'),
                b.ics_codi,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                'NOIMP',
                'DIM6SET',
                'N',
                VALOR
            from
                altres.pla_indi a,
                nodrizas.cat_centres b
            where
                a.up = b.scs_codi
                and
                ((prof = 'UP'
                and indicador in ('PLA001', 'PLA002', 'PLA003', 'PLA004'))
                or
                (prof = 'T'
                and indicador = 'PLA005'))
            union
                                select
                INDICADOR,
                concat('A', 'periodo'),
                up,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                'NOIMP',
                'DIM6SET',
                'N',
                SUM(VALOR)
            from
                altres.pla_indi a
            where
                prof = 'M'
                and (indicador like 'PLA006%' or indicador like 'PLA007%'
                or indicador like 'PLA011%' or indicador like 'PLA012%')
            group by
                INDICADOR,
                UP,
                ANALISI,
                GRUP
            union
                                select
                INDICADOR,
                concat('A', 'periodo'),
                b.ics_codi,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                'NOIMP',
                'DIM6SET',
                'N',
                sum(VALOR)
            from
                altres.pla_indi a,
                nodrizas.cat_centres b
            where
                a.up = b.scs_codi and
                INDICADOR in ('PLA008', 'PLA009', 'PLA010')
            group by INDICADOR,
                b.ics_codi,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end
            union
            select
                substr(INDICADOR,1,6) as INDI,
                concat('A', 'periodo'),
                up,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                'NOIMP',
                'DIM6SET',
                'N',
                SUM(VALOR)
            from
                altres.pla_indi a
            where
                prof = 'M'
                and (indicador like 'PLA011%' or indicador like 'PLA012%')
                and analisi = 'NUM'
            group by
                substr(INDICADOR,1,6),
                UP,
                ANALISI,
                GRUP
            union
            select
                substr(INDICADOR,1,6) as INDI,
                concat('A', 'periodo'),
                up,
                ANALISI,
                case
                    when a.GRUP = 0 then 'NOPLAGRUP'
                    when a.grup between 1 and 9 then concat('PLAGRUP0', a.GRUP)
                    when a.GRUP >= 10 then concat('PLAGRUP', a.GRUP)
                end GRUPING,
                'NOIMP',
                'DIM6SET',
                'N',
                SUM(VALOR)
            from
                altres.pla_indi a
            where
                prof = 'M'
                and (indicador IN ('PLA011A', 'PLA012A'))
                and analisi = 'DEN'
            group by
                substr(INDICADOR,1,6),
                UP,
                ANALISI,
                GRUP
                """
        u.exportKhalix(sql,KHALIX_UP)
    
    def centres(self):
        self.up_br = {}
        sql = """select ics_codi, scs_codi from nodrizas.cat_centres"""
        for br, up in u.getAll(sql, 'nodrizas'):
            self.up_br[up] = br
    
    def get_dni(self):
        self.entity_dni = {}
        sql = """SELECT up_ics, nif
                FROM DWSISAP.CAT_ADMINISTRATIUS
                where up_ics is not null"""
        for up_ics, dni in u.getAll(sql, 'exadata'):
            if up_ics in self.up_br:
                br = self.up_br[up_ics]
                hash = h.sha1(dni).hexdigest().upper()[0:6]
                entity = br + 'A' + hash
                self.entity_dni[entity] = dni

    def sisap_ecap_pla(self):
        # part x afegir indicadors de PLANIFICATxADM
        # upload = []
        # br_to_up = {}
        # sql = """select scs_codi, ics_codi from nodrizas.cat_centres"""
        # for up, br in u.getAll(sql, 'nodrizas'):
        #     br_to_up[br] = up
        # sql = """select up, indicador, valor
        #             from altres.pla_indi
        #             where length(up) > 5  
        #             and analisi = 'NUM'"""
        # for entity, indicador, valor in u.getAll(sql, 'altres'):
        #     if entity in self.entity_dni:
        #         dni = self.entity_dni[entity]
        #         br = entity[0:5]
        #         up = br_to_up[br]
        #         upload.append((up, dni, '', '', indicador, '', valor, 1, valor, 1))
        # u.listToTable(upload, self.tb, self.db)

        # aquesta taula cont� els indicadors ADM% tamb�, s�n els �nics que pengem de moment
        sql = """select * from {}.{}""".format(self.db, self.tb)
        u.exportPDP(query=sql,
                table="admindicadors",
                dat=True)

if __name__ == "__main__":
    if u.IS_MENSUAL:
        Export()