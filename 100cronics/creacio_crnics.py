import sisapUtils as u
from collections import defaultdict
import pandas as pd
import math
from datetime import datetime
import numpy as np
from dateutil.relativedelta import relativedelta


ts = datetime.now()
print('comencem')
# Baixem dades dbs
sql = """SELECT c_cip, c_up, c_metge, C_SECTOR, C_INFERMERA, C_EDAT_ANYS,
            CASE WHEN PS_ATDOM_DATA IS NOT NULL THEN 1 END ATDOM,
            CASE WHEN C_INSTITUCIONALITZAT IS NOT NULL THEN 'R' WHEN PS_ATDOM_DATA IS NOT NULL AND C_INSTITUCIONALITZAT IS NULL THEN 'A' END ATDOM_RESIDENCIA, 
            CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL THEN 1 end CI,
            CASE WHEN PS_ACV_MCV_DATA IS NOT NULL THEN 1 end AVC,
            CASE WHEN PS_HTA_DATA IS NOT NULL THEN 1 END HTA,
            CASE WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 1 END dm2,
            CASE WHEN PS_INSUF_CARDIACA_DATA IS NOT NULL THEN 1 END IC,
            CASE WHEN PS_MPOC_ENFISEMA_DATA IS NOT NULL and( VC_GOLD_VALOR LIKE '%%greu%%' OR (V_FEV1_VALOR<50 AND VC_GOLD_VALOR IS null)) THEN 1 END MPOC_GREU,
            CASE WHEN PS_MPOC_ENFISEMA_DATA IS NOT NULL and( VC_GOLD_VALOR NOT LIKE '%%greu%%' AND (V_FEV1_VALOR>=50 AND VC_GOLD_VALOR IS NOT null)) THEN 1 END MPOC_LLEU,
            CASE WHEN PS_DISLIPEMIA_DATA IS NOT NULL THEN 1 END dislipemia,
            (CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL OR PS_INSUF_CARDIACA_DATA IS NOT NULL 
            OR (PS_MPOC_ENFISEMA_DATA IS NOT NULL and( VC_GOLD_VALOR LIKE '%%greu%%' OR (V_FEV1_VALOR<50 AND VC_GOLD_VALOR IS null)))THEN 3
            WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 2 ELSE 1 END) risc,
            CASE WHEN F_HTA_COMBINACIONS is NOT NULL OR F_HTA_DIURETICS IS NOT NULL OR F_HTA_IECA_ARA2 IS NOT NULL THEN 1 ELSE 0 END FarmHTA,
            CASE WHEN PS_HIPOTIROIDISME_DATA IS NOT NULL THEN 1 END HIPOTIROIDISME,
            V_HBA1C_DATA glicada_DM,
            V_COL_TOTAL_DATA  colest,
            V_ECG_AMB_DATA ECG,
            V_FEV1_DATA espiro1,
            V_FEV1_FVC_DATA espiro2,
            V_FONS_ULL_DATA fons_ull
        fROM 
            dbs 
        WHERE 
            PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL 
            OR ps_hta_data IS NOT NULL 
            OR PS_DIABETIS2_DATA IS NOT NULL OR 
            PS_INSUF_CARDIACA_DATA is NOT NULL OR 
            PS_MPOC_ENFISEMA_DATA IS NOT NULL  OR 
            PS_DISLIPEMIA_DATA IS NOT NULL AND 
            PR_MACA_DATA IS NULL AND
            PS_DEMENCIA_DATA IS NULL AND
            PS_CURES_PALIATIVES_DATA IS NULL AND 
            (PS_NEOPLASIA_M_DATA IS NULL OR PS_NEOPLASIA_M_DATA < add_months(CURRENT_DATE, -12*1)) AND 
            (CASE WHEN PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL OR PS_INSUF_CARDIACA_DATA IS NOT NULL 
            OR (PS_MPOC_ENFISEMA_DATA IS NOT NULL and( VC_GOLD_VALOR LIKE'%%greu%%' OR (V_FEV1_VALOR<50 AND VC_GOLD_VALOR IS null)))THEN 3
            WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 2 ELSE 1 END) IN (2,3)"""
upload = []

for (c_cip, c_up, c_metge, C_SECTOR, C_INFERMERA, c_edat, atdom, ATDOM_RESIDENCIA, 
    CI, AVC, HTA, dm2, IC, MPOC_GREU, MPOC_LLEU, dislipemia, risc, FarmHTA,
    HIPOTIROIDISME, glicada_DM, colest, ECG, espiro1, espiro2, 
    fons_ull) in u.getAll(sql, 'redics'):

    upload.append([c_cip, c_up, c_metge, C_SECTOR, C_INFERMERA, c_edat, atdom, ATDOM_RESIDENCIA, 
                    CI, AVC, HTA, dm2, IC, MPOC_GREU, MPOC_LLEU, dislipemia, 
                    risc, FarmHTA, HIPOTIROIDISME, glicada_DM, colest, ECG, 
                    espiro1, espiro2, fons_ull])

print('crear dict')    
dbs = pd.DataFrame(upload)
dbs.columns =['C_CIP', 'C_UP', 'C_METGE', 'C_SECTOR', 'C_INFERMERA',
                'C_EDAT_ANYS', 'ATDOM',
                'ATDOM_RESIDENCIA', 'CI', 'AVC', 'HTA', 'DM2', 'IC', 
                'MPOC_GREU', 'MPOC_LLEU', 'DISLIPEMIA', 'RISC', 'FARMHTA',
                'HIPOTIROIDISME', 'GLICADA_DM', 'COLEST', 'ECG', 
                'ESPIRO1', 'ESPIRO2', 'FONS_ULL']
print('len dbs:', len(dbs))
print('Time execution {}'.format(datetime.now() - ts))


# Weird bug
dbs = dbs.replace(np.nan, np.nan)
print(dbs.columns)

# Transformacio de les dades, conversio a datetime
for date_type in ('GLICADA_DM', 'COLEST'):
    dbs[date_type]= pd.to_datetime(dbs[date_type])
    
    
for e in ('ESPIRO1', 'ESPIRO2', 'FONS_ULL', 'ECG'):
    print(e)
    for i in range(len(dbs)):
        try:
            if np.isnan(dbs[e][i]) == False:
                pass
        except:
            if str(dbs[e][i])[0:3] not in ('200', '199', '201', '202'):
                dbs[e][i] = np.nan
    dbs[e]= pd.to_datetime(dbs[e])


# Necessitem que les UPs tinguin 5 caracters i que les poguem tractar com strings
def convert_5(x):
    x = str(x)
    if len(x) == 1:
        x = '0000' + x
    elif len(x) == 2:
        x = '000' + x
    elif len(x) == 3:
        x = '00' + x
    elif len(x) == 4:
        x = '0' + x
    return x
dbs['C_UP'] = dbs['C_UP'].apply(lambda x: convert_5(x))


# Nomes ens quedem amb els que tenen un risc de 2 o 3 a dbs_risc
dbs_risc = dbs[dbs.RISC >= 2]
dbs_risc.reset_index(inplace=True, drop=True)
print('len dels de risc: ', len(dbs_risc))

# Informaciometges i vectors dies anuals
def up_metges_mes(up_metge, col, metges_vistos):
    if col.month not in (1,8,12):
        return up_metge
    else: 
        if up_metge not in metges_vistos: return up_metge
        else: return ' '

def up_mes(df, metges_vistos):
    return pd.Series([
        up_metges_mes(up_metge, col, metges_vistos)
        for (up_metge, col) in 
        zip(df['up_metge'], df['COLEST'])
      ])

# Metges to dict, id_metge i nombre de pacients anuals
dbs_risc['up_metge'] = dbs_risc["C_UP"].astype(str)+dbs_risc["C_METGE"]
metges_vistos = []
dbs_risc['up_metge'] = dbs_risc["C_UP"].astype(str)+dbs_risc["C_METGE"]    
dbs_risc['up_metge_mesos'] = up_mes(dbs_risc, metges_vistos)
metges = dbs_risc['up_metge_mesos'].value_counts()
metges = metges.to_dict()
metges_int = dict(zip(np.arange(len(metges)), metges.keys()))
metges_int_reves = dict(zip(metges.keys(), np.arange(len(metges))))
metges_id = list(metges_int.keys())
# Repartim els metges al llarg de l'any, ho fem per mesos, tenim 9 mesos
# Nombre de persones que ha de visitar cada metge per mes
metges = {k: np.ceil(v / 11) for k, v in metges.items()}
# Variable dies de l'any
start = datetime.today() + relativedelta(days=15)
end = start + relativedelta(years=1)
one_year = pd.date_range(start = start, end = end, normalize = True).tolist()
len(one_year)

# Funcio que marca amb -1 els dies de l'any que no 
# volem que la gent es visiti
# Caps de setmana + gener i agost + 15 dies desembre
def incrementant(one_year):
    increment = []
    for i in range(len(one_year)):
        # caps de setmana
        if one_year[i].weekday() > 4:
            increment.append(-1)
        # gener i agost
        elif one_year[i].month == 1 or one_year[i].month == 8:
            increment.append(-1)
        # 15 ultims dies desembre
        elif one_year[i].month == 12 and one_year[i].day >= 15:
            increment.append(-1) 
        else: increment.append(1)
    return np.array(increment)

# Calcular si es cap de setmana
def cap_setmana(one_year):
    increment_DS = []
    increment_DG = []
    for i in range(len(one_year)):
        if one_year[i].day == 6: increment_DS.append(i)
        elif one_year[i].day == 7: increment_DG.append(i)
    return increment_DS, increment_DG

# Calculem els primers dies dels mesos que volem programar
def dia_1_mes(one_year):
    dies_1 = []
    dies_1.append(0)
    for i in range(len(one_year)):
        if one_year[i].day == 1 and one_year[i].month not in (1,8,12): dies_1.append(i)
    return dies_1

# Construim matriu de zeros on # files = # metges i # columnes = # dies any
matrix = np.zeros((len(metges_id), len(one_year)), dtype=int)

# Estimem que cada mes tenim 20 dies laborables
for key, value in metges_int.items():
    v = int(metges[value] // 20) * incrementant(one_year) # Nombre de persones que ha de visitar a diari
    r = metges[value] % 20
    
    dia1 = dia_1_mes(one_year)
    for i, j in zip(dia1, dia1[1:] + [366]):
        r1 = r
        while r1 > 0:
            if i >= j: r1 = 0
            elif v[i] >= 0:
                v[i] += 1
                r1 -= 1
            i += 1       
    matrix[key] = v # Els que ha de visitar si o si a diari

# Ordenem el dataframe per colest, variable que fem servir per assignar
dbs_risc = dbs_risc.sort_values(by=['COLEST'], ascending=(False))
dbs_risc.reset_index(inplace=True, drop=True)

# Imputem els nuls com a data de lepoch
for e in ('GLICADA_DM', 'COLEST', 'ECG', 'ESPIRO1', 'FONS_ULL'):
    # Els Nulls els posem a data epoch
    dbs_risc[e] = dbs_risc[e].apply(lambda x: x if not pd.isnull(x) else datetime(1970,1,1,0,0))
    # Creem les columnes noves
    e = 'proper_' + e
    dbs_risc[e] = np.nan

# Sumem a totes les proves la seva frequencia (1 o 2 anys dependen de diagnostics)
def colest(date, CI, IC, DM, GREU, LLEU, HTA):
    if (GREU == 1 or LLEU == 1) and (CI + IC + DM == 0):
        return None 
    else: return (date + pd.offsets.DateOffset(years=1))

print('calcul de les properes proves')
ts = datetime.now()
# CALCUL DE LES PROPERES PROVES
def proper_COLEST(df):
    return pd.Series([
        colest(date, CI, IC, DM, GREU, LLEU, HTA)
        for (date, CI, IC, DM, GREU, LLEU, HTA) in 
        zip(df['COLEST'], df['CI'], df['IC'], df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'], df['HTA'])
      ])

def fons_ull(date, CI, IC, DM, GREU, LLEU, HTA):
    if DM == 1 and (CI + IC + GREU + LLEU == 0):
        return date + relativedelta(years=3)
    elif LLEU == 1 or GREU == 1 and (CI + IC + DM == 0):
        return datetime(1970,1,1,0,0)
    else: return date + relativedelta(years=2)
    
def proper_FONS_ULL(df):
    return pd.Series([
        fons_ull(date, CI, IC, DM, GREU, LLEU, HTA)
        for (date, CI, IC, DM, GREU, LLEU, HTA) in 
        zip(df['FONS_ULL'], df['CI'], df['IC'], df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'], df['HTA'])
      ])

def espiro(date, greu, lleu):
    return (date + relativedelta(years=2) if greu == 1 or lleu == 1 else datetime(1970,1,1,0,0))
    
def proper_espiro(df):
    return pd.Series([
        espiro(date, greu, lleu)
        for (date, greu, lleu) in zip(df['ESPIRO1'], df['MPOC_GREU'], df['MPOC_LLEU'])
      ])

def ECG(date, CI, IC, DM, GREU, LLEU, HTA, dis):
    return (date + relativedelta(years=2) if DM == 1 and (CI + IC + GREU + LLEU == 0)
            else None if ((GREU == 1 or LLEU == 1) or dis == 1) and (CI + IC + DM == 0)
            else date + relativedelta(years=1))
    
def proper_ECG(df):
    return pd.Series([
        ECG(date, CI, IC, DM, GREU, LLEU, HTA, dis)
        for (date, CI, IC, DM, GREU, LLEU, HTA, dis) in zip(df['ECG'], df['CI'], df['IC'], df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'], df['HTA'], df['DISLIPEMIA'])
      ])

dbs_risc['proper_COLEST'] = proper_COLEST(dbs_risc)
dbs_risc['proper_FONS_ULL'] = proper_FONS_ULL(dbs_risc)
dbs_risc['proper_ESPIRO1'] = proper_espiro(dbs_risc)
dbs_risc['proper_ECG'] = proper_ECG(dbs_risc)

print('Time execution per calcul seguents proves {}'.format(datetime.now() - ts))

print('calcul propera visita')
no_fets = 0

ts = datetime.now()
one_year2 = [int(t.timestamp() * 1000) for t in one_year]
def dia(data, metge, matrix, metges_int_reves, one_year2, one_year, no_fets):
    if data >= datetime.today():
        # Per no posar gent durant els caps de setmana. gent que li toca dss passa a dv i els de dg a dll
        if data.weekday == 5: i = -1
        if data.weekday == 6: i = 1
        else: i = 0
        try:
            final = int((data + relativedelta(days=i)).timestamp() * 1000)
            itemindex = np.where(np.array(one_year2)==final)[0][0]
            matrix[metges_int_reves[metge],itemindex] -= 1
        except: 
            pass
        return data + relativedelta(days=i)
    else:
        try:
            index = np.nonzero(matrix[metges_int_reves[metge]] > 0)[0][0]
            matrix[metges_int_reves[metge]][index] -= 1
            return one_year[index]
        except:
            no_fets += 1

def assignar_metge(df, matrix, metges_int_reves, one_year2, one_year, no_fets):
    return pd.Series([
        dia(data, metge, matrix, metges_int_reves, one_year2, one_year, no_fets)
        for (data, metge) in 
        zip(df['proper_COLEST'], df['up_metge'])
      ])

dbs_risc = dbs_risc.sort_values(by=['proper_COLEST'], ascending=False)
dbs_risc.reset_index(inplace=True, drop=True)
dbs_risc['data_final'] = assignar_metge(dbs_risc, matrix, metges_int_reves, one_year2, one_year, no_fets)
print('no fets: ', no_fets)

print('Time execution seguent visita {}'.format(datetime.now() - ts))

print('assignacio grups')
ts = datetime.now()
def grups(CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU):
    if IC == 1:
        if DM == 1 and (GREU == 1 or LLEU == 1): return 1
        elif DM == 1: return 2
        elif (GREU == 1 or LLEU == 1): return 3
        else: return 4
    elif CI == 1:
        if DM == 1 and (GREU == 1 or LLEU == 1): return 5
        elif DM == 1: return 6
        elif (GREU == 1 or LLEU == 1): return 7
        else: return 8
    elif DM == 1:
        if GREU == 1 or LLEU == 1: return 9
        else: return 10
    elif GREU == 1 or LLEU == 1:
        if HTA == 1: return 11
        elif DISLIPEMIA == 1: return 12
        else: return 13
    elif HTA == 1: return 14
    elif DISLIPEMIA == 1: return 15


def agrupadors(df):
    return pd.Series([
        grups(CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU)
        for (CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU) in zip(df['CI'], df['IC'], df['HTA'], df['DISLIPEMIA'],
                                                        df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'])
      ])

dbs_risc['grup'] = agrupadors(dbs_risc)
print('Time execution per agrupadors {}'.format(datetime.now() - ts))


print('exclusions')
ts = datetime.now()
def analisi_sang(edat, grup):
    if edat > 80 and (grup == 12 or grup == 15):
        return 0
    else: 
        return 1

def P_ANALISI_SANG(df):
    return pd.Series([
        analisi_sang(edat, grup)
        for (edat, grup) in 
        zip(df['C_EDAT_ANYS'], df['grup'])
      ])

def orina(edat, HTA, DM2):
    if (edat > 80 and DM2 == 1) or (HTA == 1 and edat > 75 and DM2 == 0):
        return 0
    else: 
        return 1

def P_ORINA(df):
    return pd.Series([
        orina(edat, HTA, DM2)
        for (edat, HTA, DM2) in 
        zip(df['C_EDAT_ANYS'], df['HTA'], df['DM2'])
      ])

def espiro1(espiro, visita, MPOC_GREU, MPOC_LLEU, edat, ATDOM):
    if ATDOM == 1 or edat > 80:
        return 0
    elif MPOC_GREU == 1 or MPOC_LLEU == 1:
        if espiro <= visita or espiro - relativedelta(days=180) <= visita:
            return 1
    else: 
        return 0

def P_ESPIRO(df):
    return pd.Series([
        espiro1(ESPIRO, VISITA, MPOC_GREU, MPOC_LLEU, edat, ATDOM)
        for (ESPIRO, VISITA, MPOC_GREU,  MPOC_LLEU, edat, ATDOM) in 
        zip(df['proper_ESPIRO1'], df['data_final'], df['MPOC_GREU'], df['MPOC_LLEU'], df['C_EDAT_ANYS'], df['ATDOM'])
      ])

def FO_(data, visita, DM, ATDOM):
    if ATDOM == 1:
        return 0
    elif DM == 1:
        if data + relativedelta(days=180) > visita:
            return 0
        else: return 1
    else: 
        return 0

def P_FO(df):
    return pd.Series([
        FO_(FO, VISITA, DM, ATDOM)
        for (FO, VISITA, DM, ATDOM) in zip(df['proper_FONS_ULL'], df['data_final'], df['DM2'], df['ATDOM'])
      ])

def COLEST(data, visita, CI, IC, DM, GREU, LLEU, DIS, HTA):
    if ((CI + IC + DM) == 0 and (GREU == 1 or LLEU == 1)) or ((LLEU + GREU + HTA + DIS) >= 3 and (CI + IC + DM) == 0) or ((HTA + DIS) == 2 and (CI + IC + DM + LLEU + GREU) == 0):
        return 0
    else: 
        if data <= visita or data - relativedelta(days=180) <= visita: return 1
        else: return 0

def P_LIPIDS(df):
    return pd.Series([
        COLEST(COL, VISITA, CI, IC, DM, GREU, LLEU, DIS, HTA)
        for (COL, VISITA, CI, IC, DM, GREU, LLEU, DIS, HTA) in zip(df['proper_COLEST'], df['data_final'],
                                     df['CI'], df['IC'], df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'], df['DISLIPEMIA'], df['HTA'])
      ])

def ECG1(data, visita, CI, IC, DM, GREU, LLEU, dis, ATDOM):
    if (((CI + IC + DM) == 0 and (GREU == 1 or LLEU == 1)) or ((CI + IC + DM + GREU + LLEU) == 0 and dis == 1)):
        return 0
    elif ATDOM == 1:
        return 0
    else: 
        if data <= visita or data - relativedelta(days=180) <= visita: return 1
        else: return 0

def P_ECG(df):
    return pd.Series([
        ECG1(EC, VISITA, CI, IC, DM, GREU, LLEU, DIS, ATDOM)
        for (EC, VISITA, CI, IC, DM, GREU, LLEU, DIS, ATDOM) in zip(df['proper_ECG'], df['data_final'], 
                                     df['CI'], df['IC'], df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'], df['DISLIPEMIA'], df['ATDOM'])
      ])

dbs_risc['p_analisi_sang'] = P_ANALISI_SANG(dbs_risc)
dbs_risc['p_orina'] = P_ORINA(dbs_risc)
dbs_risc['p_espiro'] = P_ESPIRO(dbs_risc)
dbs_risc['p_fo'] = P_FO(dbs_risc)
dbs_risc['p_lipids'] = P_LIPIDS(dbs_risc)
dbs_risc['p_ecg'] = P_ECG(dbs_risc)

print('Time execution per calcul seguents proves exclusions {}'.format(datetime.now() - ts))

print('diagnostics')
def dx(CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU):
    txt = ''
    buit = True
    if IC == 1:
        txt = txt + 'IC'
        buit = False
    if CI == 1:
        if buit:
            txt = txt + 'CI'
            buit = False
        else:
            txt = txt + ',CI'

    if DM == 1:
        if buit:
            txt = txt + 'DM2'
            buit = False
        else:
            txt = txt + ',DM2'
    if GREU == 1 or LLEU == 1:
        if buit:
            txt = txt + 'MPOC'
            buit = False
        else:
            txt = txt + ',MPOC'
    if HTA == 1:
        if buit:
            txt = txt + 'HTA'
            buit = False
        else:
            txt = txt + ',HTA'
    if DISLIPEMIA == 1:
        if buit:
            txt = txt + 'dislipemia'
            buit = False
        else:
            txt = txt + ',dislipemia'
    return txt


def diagnostic(df):
    return pd.Series([
        dx(CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU)
        for (CI, IC, HTA, DISLIPEMIA, DM, GREU, LLEU) in zip(df['CI'], df['IC'], df['HTA'], df['DISLIPEMIA'],
                                                        df['DM2'], df['MPOC_GREU'], df['MPOC_LLEU'])
      ])

dbs_risc['dx'] = diagnostic(dbs_risc)

print('TSH I IONOGRAMA')
dbs_risc['TSH'] = dbs_risc['HIPOTIROIDISME'].apply(lambda x: 1 if x == 1 else 0)
dbs_risc['IONOGRAMA'] = (dbs_risc['HTA'] + dbs_risc['FARMHTA']).apply(lambda x: 1 if x <= 1 else 0)

print('filtrem columnes')
dbs_joan_1 = dbs_risc[['C_CIP', 'C_UP', 'C_SECTOR', 'C_METGE', 'C_INFERMERA', 'ATDOM_RESIDENCIA', 'dx',
                'GLICADA_DM', 'COLEST', 'ECG', 'ESPIRO1',
                'FONS_ULL', 'data_final', 'TSH',
                'p_espiro', 'p_fo', 'p_ecg', 'p_analisi_sang', 'p_orina', 'grup']]

dbs_joan_1 = dbs_joan_1.rename(columns={'GLICADA_DM': 'DM_uv', 'COLEST': 'lipids_uv', 'ECG': 'ECG_uv', 
                                    'ESPIRO1' : 'espiro_uv', 'FONS_ULL': 'FU_uv', 'data_final': 'data_recomanada'})
dbs_joan_1['data_recomanada'] = dbs_joan_1['data_recomanada'].apply(lambda x: x.date())
print('transformacions de dates')
for e in ('DM_uv', 'lipids_uv', 'ECG_uv', 'espiro_uv', 'FU_uv'):
    dbs_joan_1[e] = dbs_joan_1[e].apply(lambda x: x if x.year not in (1970, 1971, 1972, 1973) else pd.NaT)


# print("convertim df en files i les anem penjant a pdp")
# upload = []
# for i in range(len(dbs_joan_1)):
#     row = dbs_joan_1.iloc[i].to_list()
#     upload.append(row)
#     if i != 10

print('crear csv')
dbs_joan_1.to_csv("visualitzacio.csv")