
import sisapUtils as u
import collections as c


def do_it(info):

    sector, descriptors = info

    conversio_codis = c.defaultdict()
    max_cod = set()
    sql = """SELECT dgppf_pmc_codi, max(dgppf_versio)
            FROM ppftb023
            GROUP BY dgppf_pmc_codi"""
    for cod, max_c in u.getAll(sql, sector):
        max_cod.add((cod, max_c))
    
    sql = """SELECT DGPPF_PS_COD, dgppf_pmc_codi,
            dgppf_versio, dgppf_data_alta
            FROM ppftb023"""
    for ps, cod, versio, data in u.getAll(sql, sector):
        if (cod, versio) in max_cod:
            if cod in conversio_codis:
                data_anterior = conversio_codis[cod][0]
                if data:
                    if data_anterior:
                        if data > data_anterior:
                            conversio_codis[cod] = [data, ps]
                    else:
                        conversio_codis[cod] = [data, ps]
            else:
                conversio_codis[cod] = [data, ps]
    
    sql = """SELECT ppfmc_pmc_amb_cod_up, ppfmc_atccodi, ppfmc_pmc_codi
            FROM ppftb016
            WHERE ppfmc_atccodi IN ('B03BA01','B03BA51')
            AND ppfmc_data_fi > sysdate
            """
    cooking = c.Counter()
    for up, atc, codi_lligar in u.getAll(sql, sector):
        if codi_lligar in conversio_codis:
            ps = conversio_codis[codi_lligar][1]
            if ps in descriptors:
                des, des_c = descriptors[ps]
                cooking[(up, atc, ps, des, des_c)] += 1
    upload = []
    for (up, atc, ps, des, des_c), n in cooking.items():
        upload.append((up, atc, ps, des, des_c, n))
    u.listToTable(upload, 'b12', 'altres')


class B12():
    def __init__(self):

        cols = '(up varchar(5), atc varchar(20), ps varchar(20), des varchar(50), des_c varchar(100), n int)'
        u.createTable('b12', cols, 'altres', rm=True)

        self.descriptors = {}
        for sector in u.sectors:
            descriptors = self.get_descriptors(sector)
            self.descriptors.update(descriptors)

        self.get_dades()
        

    def get_descriptors(self, sector):

        descriptors = {}
        
        sql = """SELECT PS_COD, PS_DES, PS_DES_C
                from prstb001"""
        for cod, des, des_c in u.getAll(sql, sector):
            descriptors[cod] = [des, des_c]
        
        return descriptors

    def get_dades(self):

        jobs = [(sector, self.descriptors) for sector in u.sectors]
        u.multiprocess(do_it, jobs, 8)

if __name__ == '__main__':
    B12()