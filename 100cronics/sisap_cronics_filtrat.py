from collections import defaultdict
from datetime import datetime

import sisapUtils as u
import sisaptools as t
import numpy as np
import pandas as pd
import math
import os

TOT_OK = True
LANDING = set()

ups_landing = set()
for up, in u.getAll('SELECT up FROM centres_pilots WHERE pilot_landing = 1', 'pdp'):
    ups_landing.add(up)

old_ecg, old_analisi, old_n = 0,0,0
sql = """SELECT sum(p_ecg), sum(p_analisi), count(1) FROM seguent_visita_cronics"""
for ecg, analisi, n in u.getAll(sql, 'pdp'):
    old_ecg, old_analisi, old_n = ecg, analisi, n

cips_greus = set()

# peticions fetes
print('getting cips')
cips = dict()
sql = """SELECT b.usua_cip_cod, DBMS_LOB.SUBSTR(a.protocols, 4000,1) FROM PREDUFFA.SISAP_COVID_GIS_PETICIONS a 
        INNER JOIN pdptb101 b ON a.cip = b.usua_cip where programat = 0"""
for cip, protocol in u.getAll(sql, "pdp"):
    if protocol:
        cips[cip] = str(protocol.encode('ISO-8859-1'))
    else: cips[cip] = ''

print("reading dbs")
t.SFTP("sisap").get("planificat/planificat.csv", u.tempFolder + "planificat.csv")
dbs = pd.read_csv(u.tempFolder + "planificat.csv")

columnes = ['lipids_uv', 'ECG_uv', 'espiro_uv', 'FU_uv', 'data_recomanada', 'AV_PRESCRIPCIONS_CAD_DATA']
for col in columnes:
    dbs[col]= pd.to_datetime(dbs[col])

# NECESSITEM UP AMB 5 CARACTERS I COM A STRING
def convert_5(x):
    # return '0' * len(str(x)) + str(x)
    x = str(x)
    if len(x) == 1:
        x = '0000' + x
    elif len(x) == 2:
        x = '000' + x
    elif len(x) == 3:
        x = '00' + x
    elif len(x) == 4:
        x = '0' + x
    return x
dbs['C_UP'] = dbs['C_UP'].apply(lambda x: convert_5(x))

print("creem taula")
table = 'cronics_filtrats'
db = 'pdp'

u.createTable(table, "(C_CIP varchar(40), C_UP varchar(5), C_CENTRE varchar(12), C_SECTOR varchar(5), C_METGE varchar(6), C_INFERMERA varchar(6)\
                    , ATDOM_RESIDENCIA varchar(1), dx varchar(50), analisi_uv date, ECG_uv date\
                    , espiro_uv date, fo_uv date, data_recomanada date\
                    , tsh int, p_espiro int, p_fo int, p_ecg int, p_analisi int, p_orina int\
                    , grup int, edat int, uas varchar(5), AV_PRESCRIPCIONS_CAD_DATA date, peticio_feta int\
                    , protocol varchar(1000), codi_sector varchar(5))", db, rm=True) 

upload = []

for i in range(len(dbs)):
    cips_greus.add(dbs['C_CIP'][i])
    v = [
        dbs['C_CIP'][i], 
        dbs['C_UP'][i],
        dbs['C_CENTRE'][i],
        dbs['C_SECTOR'][i],
        dbs['C_METGE'][i],
        dbs['C_INFERMERA'][i],
        dbs['ATDOM_RESIDENCIA'][i],
        str(dbs['dx'][i]),
        dbs['lipids_uv'][i],
        dbs['ECG_uv'][i],
        dbs['espiro_uv'][i],
        dbs['FU_uv'][i],
        dbs['data_recomanada'][i],
        dbs['TSH'][i],
        dbs['p_espiro'][i],
        dbs['p_fo'][i],
        dbs['p_ecg'][i],
        dbs['p_analisi_sang'][i],
        dbs['p_orina'][i],
        dbs['grup'][i], 
        dbs['C_EDAT_ANYS'][i],
        dbs['C_UAS'][i],
        dbs['AV_PRESCRIPCIONS_CAD_DATA'][i],
        0, 
        '',
        dbs['C_SECTOR'][i]
    ]
    if dbs['C_UP'][i] in ups_landing:
        LANDING.add(dbs['C_CIP'][i])

    v = [
        None
        if (isinstance(v_i, float) and math.isnan(v_i)) or
        isinstance(v_i, type(pd.NaT)) else v_i
        for v_i in v
    ]

    cip = dbs['C_CIP'][i]
    if cip in cips:
        v[-3] = 1
        v[-2] = cips[cip]

    upload.append(v)
    if i % 10000 == 0: 
        u.listToTable(upload, table, db)
        print(i)
        upload = []


u.listToTable(upload, table, db)
print('fi', len(upload))


## SISAP CRONICS FILTRAT 14 I 15
print("reading dbs")
t.SFTP("sisap").get("planificat/planificat_lleus.csv", u.tempFolder + "planificat_lleus.csv")
dbs = pd.read_csv(u.tempFolder + "planificat_lleus.csv")

columnes = ['lipids_uv', 'ECG_uv', 'espiro_uv', 'FU_uv', 'data_recomanada', 'AV_PRESCRIPCIONS_CAD_DATA']
for col in columnes:
    dbs[col]= pd.to_datetime(dbs[col])

# NECESSITEM UP AMB 5 CARACTERS I COM A STRING
def convert_5(x):
    # return '0' * len(str(x)) + str(x)
    x = str(x)
    if len(x) == 1:
        x = '0000' + x
    elif len(x) == 2:
        x = '000' + x
    elif len(x) == 3:
        x = '00' + x
    elif len(x) == 4:
        x = '0' + x
    return x
dbs['C_UP'] = dbs['C_UP'].apply(lambda x: convert_5(x))

print("afegim a la taula")

upload = []

for i in range(len(dbs)):
    if dbs['C_CIP'][i] not in cips_greus:
        v = [
            dbs['C_CIP'][i], 
            dbs['C_UP'][i],
            dbs['C_CENTRE'][i],
            dbs['C_SECTOR'][i],
            dbs['C_METGE'][i],
            dbs['C_INFERMERA'][i],
            dbs['ATDOM_RESIDENCIA'][i],
            str(dbs['dx'][i]),
            dbs['lipids_uv'][i],
            dbs['ECG_uv'][i],
            dbs['espiro_uv'][i],
            dbs['FU_uv'][i],
            dbs['data_recomanada'][i],
            dbs['TSH'][i],
            dbs['p_espiro'][i],
            dbs['p_fo'][i],
            dbs['p_ecg'][i],
            dbs['p_analisi_sang'][i],
            dbs['p_orina'][i],
            dbs['grup'][i], 
            dbs['C_EDAT_ANYS'][i],
            dbs['C_UAS'][i],
            dbs['AV_PRESCRIPCIONS_CAD_DATA'][i],
            0, 
            '',
            dbs['C_SECTOR'][i]
        ]
        if dbs['C_UP'][i] in ups_landing:
            LANDING.add(dbs['C_CIP'][i])

        v = [
            None
            if (isinstance(v_i, float) and math.isnan(v_i)) or
            isinstance(v_i, type(pd.NaT)) else v_i
            for v_i in v
        ]

        cip = dbs['C_CIP'][i]
        if cip in cips:
            v[-3] = 1
            v[-2] = cips[cip]

        upload.append(v)
        if i % 10000 == 0: 
            u.listToTable(upload, table, db)
            print(i)
            upload = []


u.listToTable(upload, table, db)
print('fi', len(upload))



# Calculem a qui hem de resucitar
print('a resucitar')

sql = """SELECT hash_redics
        FROM DWSISAP.SISAP_MASTER_VISITES A, dwsisap.PDPTB101_RELACIO pr 
        WHERE hash_covid = pacient AND 
        (MOTIU_PRIOR LIKE '%PLANIFICAT>SEG%' 
        OR motiu_prior LIKE '%PLANIFICAT>CON%')
        AND DATA >= sysdate"""
hash_visites_post = set()
for hash, in u.getAll(sql, 'exadata'):
    hash_visites_post.add(hash)

sql = """SELECT  hash, resucitar, c_sector FROM(
select hash, estat,
            case
                when (TRUNC(SYSDATE) - trunc(to_date(estat_data, 'yyyy/mm/dd hh24:mi:ss'))) >= 365 and estat != 10 then 1
                when 
                    (estat = 10 and ((TRUNC(SYSDATE) - trunc(to_date(fadm_visita_1, 'dd/mm/yyyy'))) >= 1 
                    			OR fadm_visita_1 IS NULL)) AND
                    (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_2, 'dd/mm/yyyy'))) >= 1 
                    			OR fadm_visita_2 IS NULL) AND
                    (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_3, 'dd/mm/yyyy'))) >= 1 
                    			OR fadm_visita_3 IS NULL) AND
                    (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_1post, 'dd/mm/yyyy'))) >= 1 
                    			OR fadm_visita_1post IS NULL) AND
                    (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_2post, 'dd/mm/yyyy'))) >= 1 
                    			OR fadm_visita_2post IS NULL) AND
                    (estat = 10 and (TRUNC(SYSDATE) - trunc(to_date(fadm_visita_3post, 'dd/mm/yyyy'))) >= 1 
                    		OR fadm_visita_3post IS NULL) AND
                    (estat = 10 and (trunc(sysdate) - trunc(fadm_data_recomanada)) >= 1) then 1
                else 0
            end resucitar, c_sector
            from planificat)
            WHERE resucitar = 1
            and estat != 1"""
a_resucitar = []
upload = []
now = datetime.now()
dt_string = now.strftime("%Y/%m/%d %H:%M:%S")

for hash, resucitar, sector in u.getAll(sql, 'pdp'):
    if resucitar == 1 and hash not in hash_visites_post and hash not in LANDING:
        upload.append((None, None, hash, sector, '1', 'ALGORITME', dt_string, \
            None, None, None, None, None, None, None, None, None, None, \
            None, None, None, None, None, None, None, None, None, None, \
            None, None, None, None, None, None, None, None, None, None, \
            None, None, None, None, None, None, None, None, None, None, \
            None, None, None, None, None, None, None, None, None, None, \
            None, None, None, None, None, None, None, None, None, None, \
            None, None, None, None, None, None, None, None, None, None, \
            None, None, None, None, None, None, None, None, None, None, None))
        a_resucitar.append(hash)

print('gent que resucitem', len(a_resucitar))
if len(a_resucitar) >= 1:
    for renovate in a_resucitar:
        sql = """delete from planificat where hash = '{}'""".format(renovate)
        u.execute(sql, 'pdp')
if len(upload) > 0:
    print(len(upload), 'els q afegim')
    u.listToTable(upload, 'planificat', 'pdp')
    u.listToTable(upload, 'planificat_audit', 'pdp')


new_ecg, new_analisi, new_n = 0,0,0
sql = """SELECT sum(p_ecg), sum(p_analisi), count(1) FROM cronics_filtrats"""
for ecg, analisi, n in u.getAll(sql, 'pdp'):
    new_ecg, new_analisi, new_n = ecg, analisi, n

info = ''
if (old_ecg - new_ecg)/float(old_ecg) > 10/float(100) or (old_ecg - new_ecg)/float(old_ecg) < -10/float(100): 
    info += 'caiguda o augment ecg superior 10%'
    TOT_OK = False
if (old_analisi - new_analisi)/float(old_analisi) > 10/float(100) or (old_analisi - new_analisi)/float(old_analisi) < -10/float(100):
    info += 'caiguda o augment analisi superior 10%'
    TOT_OK = False
if (old_n - new_n)/float(old_n) > 10/float(100) or (old_n - new_n)/float(old_n) < -10/float(100): 
    info += 'caiguda o augment N superior 10%'
    TOT_OK = False



if TOT_OK == True:
    try:
        u.execute("create table pdp.cronics_filtrats_aux as SELECT distinct * FROM cronics_filtrats", 'pdp')
        u.execute("DROP TABLE seguent_visita_cronics", 'pdp')
        u.execute("ALTER TABLE pdp.cronics_filtrats_aux  RENAME TO seguent_visita_cronics", 'pdp')
        u.execute("CREATE INDEX up_metge ON pdp.seguent_visita_cronics(c_up, c_metge)", 'pdp')
        u.execute("CREATE INDEX cip ON pdp.seguent_visita_cronics(c_cip)", 'pdp')
        u.execute("CREATE INDEX sector_cro ON SEGUENT_VISITA_CRONICS(c_sector)", 'pdp')
        u.execute("CREATE INDEX data_reco_cro ON SEGUENT_VISITA_CRONICS(data_recomanada)", 'pdp')
        u.execute("CREATE INDEX grup_cro ON SEGUENT_VISITA_CRONICS(grup)", 'pdp')
        u.execute("ALTER TABLE seguent_visita_cronics ADD frequentacio varchar2(8)", 'pdp')
        u.execute("UPDATE seguent_visita_cronics SET frequentacio = (CASE WHEN MONTHS_BETWEEN(CURRENT_DATE, analisi_uv) < 15 THEN '<15' ELSE frequentacio end)", 'pdp')
        u.execute("UPDATE seguent_visita_cronics SET frequentacio = (CASE WHEN MONTHS_BETWEEN(CURRENT_DATE, analisi_uv) >= 15 AND MONTHS_BETWEEN(CURRENT_DATE, analisi_uv) < 30 THEN '15-30' ELSE frequentacio end)", 'pdp')
        u.execute("UPDATE seguent_visita_cronics SET frequentacio = (CASE WHEN MONTHS_BETWEEN(CURRENT_DATE, analisi_uv) > 30 THEN '>30' ELSE frequentacio end)", 'pdp')
        u.execute("UPDATE seguent_visita_cronics SET frequentacio = (CASE WHEN frequentacio NOT IN ('<15', '15-30', '>30') THEN 'MAI' ELSE frequentacio end)", 'pdp')
        u.execute("ALTER TABLE seguent_visita_cronics ADD PRIMARY KEY (C_CIP)", 'pdp')
    except Exception as e:
        to_send = str(e)
        u.sendGeneral('SISAP <sisap@gencat.cat>',
                        ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                        ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                        'seguent visita cronics baaad',
                        to_send)
    u.grantSelect('seguent_visita_cronics', ('PREDUFFA', 'PLANIFICA'), 'pdp')
    u.calcStatistics('seguent_visita_cronics', 'pdp')
else:
    to_send = 'Increment o decrement en {}'.format(info)
    u.sendGeneral('SISAP <sisap@gencat.cat>',
                    ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                    ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                    'Caiguda o increment brutal planificat !!!',
                    to_send)