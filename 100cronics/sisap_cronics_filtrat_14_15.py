import sisapUtils as u
from collections import defaultdict
import pandas as pd
import math
from datetime import datetime
import numpy as np
import os

TOT_OK = True

old_ecg, old_analisi, old_n = 0,0,0
sql = """SELECT sum(p_ecg), sum(p_analisi), count(1) FROM seguent_visita_cronics"""
for ecg, analisi, n in u.getAll(sql, 'pdp'):
    old_ecg, old_analisi, old_n = ecg, analisi, n


# peticions fetes
print('getting cips')
cips = dict()
sql = """SELECT b.usua_cip_cod, DBMS_LOB.SUBSTR(a.protocols, 4000,1) FROM PREDUFFA.SISAP_COVID_GIS_PETICIONS a 
        INNER JOIN pdptb101 b ON a.cip = b.usua_cip 
        where programat = 0"""
for cip, protocol in u.getAll(sql, "pdp"):
    if protocol:
        cips[cip] = str(protocol.encode('ISO-8859-1'))
    else: cips[cip] = ''

print("reading dbs")
# file = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/11dbs/visualitzacio.csv"  # noqa
dbs = pd.read_csv(u.tempFolder + "visualitzacio_14_15.csv")

columnes = ['lipids_uv', 'ECG_uv', 'espiro_uv', 'FU_uv', 'data_recomanada']
for col in columnes:
    dbs[col]= pd.to_datetime(dbs[col])

# NECESSITEM UP AMB 5 CARACTERS I COM A STRING
def convert_5(x):
    # return '0' * len(str(x)) + str(x)
    x = str(x)
    if len(x) == 1:
        x = '0000' + x
    elif len(x) == 2:
        x = '000' + x
    elif len(x) == 3:
        x = '00' + x
    elif len(x) == 4:
        x = '0' + x
    return x
dbs['C_UP'] = dbs['C_UP'].apply(lambda x: convert_5(x))

print("afegim a la taula")
table = 'cronics_filtrats'
db = 'pdp'

upload = []

for i in range(len(dbs)):
    v = [
        dbs['C_CIP'][i], 
        dbs['C_UP'][i],
        dbs['C_CENTRE'][i],
        dbs['C_SECTOR'][i],
        dbs['C_METGE'][i],
        dbs['C_INFERMERA'][i],
        dbs['ATDOM_RESIDENCIA'][i],
        str(dbs['dx'][i]),
        dbs['lipids_uv'][i],
        dbs['ECG_uv'][i],
        dbs['espiro_uv'][i],
        dbs['FU_uv'][i],
        dbs['data_recomanada'][i],
        dbs['TSH'][i],
        dbs['p_espiro'][i],
        dbs['p_fo'][i],
        dbs['p_ecg'][i],
        dbs['p_analisi_sang'][i],
        dbs['p_orina'][i],
        dbs['grup'][i], 
        dbs['C_EDAT_ANYS'][i],
        0, 
        '',
        dbs['C_SECTOR'][i]
    ]

    v = [
        None
        if (isinstance(v_i, float) and math.isnan(v_i)) or
        isinstance(v_i, type(pd.NaT)) else v_i
        for v_i in v
    ]

    cip = dbs['C_CIP'][i]
    if cip in cips:
        v[-3] = 1
        v[-2] = cips[cip]

    upload.append(v)
    if i % 10000 == 0: 
        u.listToTable(upload, table, db)
        print(i)
        upload = []


u.listToTable(upload, table, db)
print('fi', len(upload))

new_ecg, new_analisi, new_n = 0,0,0
sql = """SELECT sum(p_ecg), sum(p_analisi), count(1) FROM cronics_filtrats"""
for ecg, analisi, n in u.getAll(sql, 'pdp'):
    new_ecg, new_analisi, new_n = ecg, analisi, n

info = ''
if (old_ecg - new_ecg)/float(old_ecg) > 10/float(100) or (old_ecg - new_ecg)/float(old_ecg) < -10/float(100): 
    info += 'caiguda o augment ecg superior 10%'
    TOT_OK = False
if (old_analisi - new_analisi)/float(old_analisi) > 10/float(100) or (old_analisi - new_analisi)/float(old_analisi) < -10/float(100):
    info += 'caiguda o augment analisi superior 10%'
    TOT_OK = False
if (old_n - new_n)/float(old_n) > 10/float(100) or (old_n - new_n)/float(old_n) < -10/float(100): 
    info += 'caiguda o augment N superior 10%'
    TOT_OK = False



if TOT_OK == True:
    u.execute("create table pdp.cronics_filtrats_aux as SELECT distinct * FROM cronics_filtrats", 'pdp')
    u.execute("DROP TABLE seguent_visita_cronics", 'pdp')
    u.execute("ALTER TABLE pdp.cronics_filtrats_aux  RENAME TO seguent_visita_cronics", 'pdp')
    u.execute("CREATE INDEX up_metge ON pdp.seguent_visita_cronics(c_up, c_metge)", 'pdp')
    u.execute("CREATE INDEX cip ON pdp.seguent_visita_cronics(c_cip)", 'pdp')
    u.execute("ALTER TABLE seguent_visita_cronics ADD frequentacio varchar2(8)", 'pdp')
    u.execute("UPDATE seguent_visita_cronics SET frequentacio = (CASE WHEN MONTHS_BETWEEN(CURRENT_DATE, analisi_uv) < 15 THEN '<15' ELSE frequentacio end)", 'pdp')
    u.execute("UPDATE seguent_visita_cronics SET frequentacio = (CASE WHEN MONTHS_BETWEEN(CURRENT_DATE, analisi_uv) >= 15 AND MONTHS_BETWEEN(CURRENT_DATE, analisi_uv) < 30 THEN '15-30' ELSE frequentacio end)", 'pdp')
    u.execute("UPDATE seguent_visita_cronics SET frequentacio = (CASE WHEN MONTHS_BETWEEN(CURRENT_DATE, analisi_uv) > 30 THEN '>30' ELSE frequentacio end)", 'pdp')
    u.execute("UPDATE seguent_visita_cronics SET frequentacio = (CASE WHEN frequentacio NOT IN ('<15', '15-30', '>30') THEN 'MAI' ELSE frequentacio end)", 'pdp')
    try:
        u.execute("ALTER TABLE seguent_visita_cronics ADD PRIMARY KEY (C_CIP)", 'pdp')
    except Exception as e:
        to_send = str(e)
        u.sendGeneral('SISAP <sisap@gencat.cat>',
                        ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                        ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                        'seguent visita cronics baaad',
                        to_send)
    u.grantSelect('seguent_visita_cronics', ('PREDUFFA', 'PLANIFICA'), 'pdp')
    u.calcStatistics('seguent_visita_cronics', 'pdp')
else:
    to_send = 'Increment o decrement en {}'.format(info)
    u.sendGeneral('SISAP <sisap@gencat.cat>',
                    ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                    ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                    'Caiguda o increment brutal planificat !!!',
                    to_send)
