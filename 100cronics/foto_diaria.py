import sisapUtils as u
import collections

class Diaris():
    def __init__(self):
        self.audit()
        self.foto()
    
    def audit(self):
        upload = []
        print('2')
        sql = """SELECT * FROM PLANIFICAT_AUDIT pa 
                WHERE trunc(to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS')) = trunc(SYSDATE-1)
                AND pa.hash NOT IN (SELECT usua_cip
                FROM usutb040 WHERE usua_uab_up IS NOT null)"""
        for row in u.getAll(sql, 'pdp'):
                upload.append(row)
        u.listToTable(upload, 'planificat_audit_total', 'pdp')
        upload = []
        print('1')
        sql = """SELECT usua_uab_up, USUA_UAB_CODI_UAB, p.*, ASS_CODI_UNITAT 
                FROM usutb040 u, PLANIFICAT_AUDIT p
                WHERE u.usua_cip = p.hash
                AND trunc(to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS')) = trunc(SYSDATE-1)
                AND usua_uab_up IS NOT null"""
        for row in u.getAll(sql, 'pdp'):
                p1 = list(row[0:2])
                p2 = list(row[4:-2])
                p3 = list([row[-1]])
                upload.append(p1+p2+p3)
        u.listToTable(upload, 'planificat_audit_total', 'pdp')

    def foto(self):
        historics = collections.Counter()
        mirant = 0
        a_fer = set()
        sql = """SELECT DISTINCT p.hash, EXTRACT(YEAR FROM p.FADM_DATA_RECOMANADA), UP, UBA
                FROM PLANIFICAT_AUDIT_TOTAL p
                WHERE p.estat = 10"""
        for hash, year, up, metge in u.getAll(sql, 'pdp'):
            historics[(up, metge)] += 1
            mirant += 1
            a_fer.add((up, metge))
        
        historics1 = collections.Counter()
        sql = """SELECT DISTINCT p.hash, EXTRACT(YEAR FROM p.FADM_DATA_RECOMANADA), UP, UBA
                FROM PLANIFICAT_AUDIT_TOTAL p
                WHERE p.estat = 10
                AND MONTHS_BETWEEN(to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS'), CURRENT_DATE) < 12"""
        for hash, year, up, metge in u.getAll(sql, 'pdp'):
            historics1[(up, metge)] += 1

        print('mirant', mirant)
        print('len a fer', len(a_fer))

        upload = []
        d = collections.defaultdict(dict)
        sql = """SELECT svc.C_UP, svc.C_METGE, p2.UAB_DESCRIPCIO, 
                c.fase, p.estat, count(1) N, TRUNc(sysdate) dia
                from planificat p
                INNER JOIN SEGUENT_VISITA_CRONICS svc ON p.hash = svc.c_cip
                INNER JOIN centres_pilots c ON svc.c_up = c.up
                INNER JOIN professionals p2 ON svc.c_up = p2.up AND svc.c_metge = p2.uab
                WHERE p2.TIPUS = 'M' aND p.estat != 1
                GROUP BY svc.C_UP, svc.C_METGE, p2.UAB_DESCRIPCIO, c.fase, p.estat"""
        for up, metge, desc_metge, fase, estat, n, dia in u.getAll(sql, 'pdp'):
            d[(up, metge, 'M', desc_metge, dia, fase)][estat] = n
        print('1')
        print(len(d))

        sql = """SELECT svc.C_UP, svc.C_METGE, p2.UAB_DESCRIPCIO, 
                c.fase, TRUNc(sysdate) dia, MONTHS_BETWEEN(svc.DATA_RECOMANADA, SYSDATE)
                from planificat p
                INNER JOIN SEGUENT_VISITA_CRONICS svc ON p.hash = svc.c_cip
                INNER JOIN centres_pilots c ON svc.c_up = c.up
                INNER JOIN professionals p2 ON svc.c_up = p2.up AND svc.c_metge = p2.uab
                WHERE p2.TIPUS = 'M' aND p.estat = 1"""
        for up, metge, desc_metge, fase, dia, mesos in u.getAll(sql, 'pdp'):
            if mesos < 3: 
                try:
                    d[(up, metge, 'M', desc_metge, dia, fase)]['1_0'] += 1
                except:
                    d[(up, metge, 'M', desc_metge, dia, fase)]['1_0'] = 1
            elif mesos >= 3 and mesos < 6: 
                try:
                    d[(up, metge, 'M', desc_metge, dia, fase)]['1_3'] += 1
                except:
                    d[(up, metge, 'M', desc_metge, dia, fase)]['1_3'] = 1
            elif mesos >= 6 and mesos < 9: 
                try:
                    d[(up, metge, 'M', desc_metge, dia, fase)]['1_6'] += 1
                except:
                    d[(up, metge, 'M', desc_metge, dia, fase)]['1_6'] = 1
            elif mesos >= 9: 
                try:
                    d[(up, metge, 'M', desc_metge, dia, fase)]['1_9'] += 1
                except:
                    d[(up, metge, 'M', desc_metge, dia, fase)]['1_9'] = 1
        print('2')
        print(len(d))
        comptant = 0
        fets = set()
        for e in d.keys():
            try: e1_0 = d[e]['1_0']
            except: e1_0 = 0
            try: e1_3 = d[e]['1_3']
            except: e1_3 = 0
            try: e1_6 = d[e]['1_6']
            except: e1_6 = 0
            try: e1_9 = d[e]['1_9']
            except: e1_9 = 0
            try: e2 = d[e][2]
            except: e2 = 0
            try: e3 = d[e][3]
            except: e3 = 0
            try: e4 = d[e][4]
            except: e4 = 0
            try: e5 = d[e][5]
            except: e5 = 0
            try: e6 = d[e][6]
            except: e6 = 0
            try: e7 = d[e][7]
            except: e7 = 0
            try: e10 = d[e][10]
            except: e10 = 0
            try: 
                h = historics[(e[0], e[1])]
                fets.add((e[0], e[1]))
            except: h = 0
            try: 
                h1 = historics1[(e[0], e[1])]
            except: h1 = 0
            comptant += h
            N = e1_0+e1_3+e1_6+e1_9+e2+e3+e4+e5+e6+e7+e10
            upload.append((e[0], e[1], e[2], e[5], e2, e3, e4, e5, e6, e7, e10, N, e[4], e1_0, e1_3, e1_6, e1_9, e[3], h, h1))
        print('comptant', comptant)
        print('fets', len(fets))
        print('diff', a_fer - fets)

        # NO HO FEM X INF
        historics = collections.Counter()
        sql = """SELECT DISTINCT p.hash, EXTRACT(YEAR FROM p.FADM_DATA_RECOMANADA), 
                    up, uba_inf
                    FROM PLANIFICAT_AUDIT_TOTAL p 
                    WHERE p.estat = 10 """
        for hash, year, up, inf in u.getAll(sql, 'pdp'):
            historics[(up, inf)] += 1

        
        historics1 = collections.Counter()
        sql = """SELECT DISTINCT p.hash, EXTRACT(YEAR FROM p.FADM_DATA_RECOMANADA), 
                    up, uba_inf
                    FROM PLANIFICAT_AUDIT_TOTAL p 
                    WHERE p.estat = 10 
                    AND MONTHS_BETWEEN(to_date(estat_data, 'YYYY/MM/DD HH24:MI:SS'), CURRENT_DATE) < 12"""
        for hash, year, up, inf in u.getAll(sql, 'pdp'):
            historics1[(up, inf)] += 1


        d = collections.defaultdict(dict)

        sql = """SELECT svc.C_UP, svc.C_INFERMERA, p2.UAB_DESCRIPCIO, 
                c.fase, p.estat, count(1) N, TRUNc(sysdate) dia
                from planificat p
                INNER JOIN SEGUENT_VISITA_CRONICS svc ON p.hash = svc.c_cip
                INNER JOIN centres_pilots c ON svc.c_up = c.up
                INNER JOIN professionals p2 ON svc.c_up = p2.up AND svc.c_infermera = p2.uab
                WHERE p2.TIPUS = 'I' aND p.estat != 1
                GROUP BY svc.C_UP, svc.C_INFERMERA, p2.UAB_DESCRIPCIO, c.fase, p.estat"""
        for up, metge, desc_metge, fase, estat, n, dia in u.getAll(sql, 'pdp'):
            d[(up, metge, 'I', desc_metge, dia, fase)][estat] = n
        print('3')
        sql = """SELECT svc.C_UP, svc.C_INFERMERA, p2.UAB_DESCRIPCIO, 
                c.fase, TRUNc(sysdate) dia, MONTHS_BETWEEN(svc.DATA_RECOMANADA, SYSDATE)
                from planificat p
                INNER JOIN SEGUENT_VISITA_CRONICS svc ON p.hash = svc.c_cip
                INNER JOIN centres_pilots c ON svc.c_up = c.up
                INNER JOIN professionals p2 ON svc.c_up = p2.up AND svc.c_infermera = p2.uab
                WHERE p2.TIPUS = 'I' aND p.estat = 1"""
        for up, metge, desc_metge, fase, dia, mesos in u.getAll(sql, 'pdp'):
            if mesos < 3: 
                try:
                    d[(up, metge, 'I', desc_metge, dia, fase)]['1_0'] += 1
                except:
                    d[(up, metge, 'I', desc_metge, dia, fase)]['1_0'] = 1
            elif mesos >= 3 and mesos < 6: 
                try:
                    d[(up, metge, 'I', desc_metge, dia, fase)]['1_3'] += 1
                except:
                    d[(up, metge, 'I', desc_metge, dia, fase)]['1_3'] = 1
            elif mesos >= 6 and mesos < 9: 
                try:
                    d[(up, metge, 'I', desc_metge, dia, fase)]['1_6'] += 1
                except:
                    d[(up, metge, 'I', desc_metge, dia, fase)]['1_6'] = 1
            elif mesos >= 9: 
                try:
                    d[(up, metge, 'I', desc_metge, dia, fase)]['1_9'] += 1
                except:
                    d[(up, metge, 'I', desc_metge, dia, fase)]['1_9'] = 1
        print('4')
        for e in d.keys():
            try: e1_0 = d[e]['1_0']
            except: e1_0 = 0
            try: e1_3 = d[e]['1_3']
            except: e1_3 = 0
            try: e1_6 = d[e]['1_6']
            except: e1_6 = 0
            try: e1_9 = d[e]['1_9']
            except: e1_9 = 0
            try: e2 = d[e][2]
            except: e2 = 0
            try: e3 = d[e][3]
            except: e3 = 0
            try: e4 = d[e][4]
            except: e4 = 0
            try: e5 = d[e][5]
            except: e5 = 0
            try: e6 = d[e][6]
            except: e6 = 0
            try: e7 = d[e][7]
            except: e7 = 0
            try: e10 = d[e][10]
            except: e10 = 0
            try: h = historics[(e[0], e[1])]
            except: h = 0
            try: h1 = historics1[(e[0], e[1])]
            except: h1 = 0
            N = e1_0+e1_3+e1_6+e1_9+e2+e3+e4+e5+e6+e7+e10
            upload.append((e[0], e[1], e[2], e[5], e2, e3, e4, e5, e6, e7, e10, N, e[4], e1_0, e1_3, e1_6, e1_9, e[3], h, h1))

        print(upload[0])
        print(upload[1])

        u.listToTable(upload, 'analisi_planificat', 'pdp')


if __name__ == "__main__":
    try:
        Diaris()
    except:
        u.sendGeneral('SISAP <sisap@gencat.cat>',
                      ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                       ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                      'ANALISI PLANIFICAT BAD',
                      '.')
    else:
         u.sendGeneral('SISAP <sisap@gencat.cat>',
                      ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                       ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                      'ANALISI PLANIFICAT OK',
                      '.')
    # Diaris()
