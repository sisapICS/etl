# -*- coding: iso-8859-1 -*-

import sisapUtils as u
import hashlib as h
from collections import defaultdict
import pandas as pd
import xlwt
import datetime as d
import os
import sisaptools as t
import traceback

def crear_taules():
    cols = "(hash varchar2(40), cip varchar2(13), dia_visita varchar(10), hora_visita varchar(5), modul_visita varchar2(5), servei_visita varchar(5), up varchar2(5), centre_codi varchar2(9), centre_classe varchar2(2))"
    u.createTable('sms_visites', cols, 'pdp', rm=True)
    cols = """(cip varchar2(13), hash varchar2(40), nom varchar(15), cognom1 varchar(20), i_cognom varchar(1),
                cognom2 varchar(20), idioma varchar(8), telf1 varchar(10), telf2 varchar(10), grup int, data_programacio date)"""
    u.createTable('sms_info_pacients', cols, 'pdp', rm=True)
    cols = """(telf varchar2(10), missatge varchar2(2000))"""
    u.createTable('missatge_sms', cols, 'pdp', rm=True)
    cols = """(hash varchar2(40), missatge varchar2(2000), dia date)"""
    # u.createTable('no_sms_planificat', cols, 'pdp', rm=True)
    
def visites():
    print('visites')
    upload = []
    hash_cip = dict()
    cip_hash = dict()
    hash_r_2_cip, cip_2_hash_r, hash_c_2_r, hash_c_2_cip = {}, {}, {}, {}
    sql = """SELECT cip, hash_redics, hash_covid FROM PDPTB101_relacio"""
    for cip, hash_r, hash_c in u.getAll(sql, 'pdp'):
        hash_r_2_cip[hash_r] = cip
        cip_2_hash_r[cip] = hash_r
        hash_c_2_r[hash_c] = hash_r
        hash_c_2_cip[hash_c] = cip
    sql = """SELECT pacient, to_char(DATA, 'dd-mm-yyyy') data, hora,
                MODUL, servei, up, centre_codi, centre_classe
                FROM dwsisap.SISAP_CORONAVIRUS_ACTIVITAT_N 
                WHERE (FLAG_SISAP = 1 or MOTIU_PRIOR LIKE '%PLANIFICAT>SEG%' OR motiu_prior LIKE '%PLANIFICAT>CON%')
                and servei not in ('ADM',	'GIS', 'UAAU', 'UAU', 'UAC')
                AND TIPUS NOT IN ('9E', '9T') AND DATA > sysdate and situacio = 'P'"""
    for hash_c, data, hora, modul, servei, up, centre_codi, centre_classe in u.getAll(sql, 'exadata'):
        if hash_c in hash_c_2_r:
            hash_r = hash_c_2_r[hash_c]
            cip = hash_c_2_cip[hash_c] 
            upload.append((hash_r, cip, data, hora, modul, servei, up, centre_codi, centre_classe))
            hash_cip[hash_r] = cip
            cip_hash[cip] = hash_r
    u.listToTable(upload, 'sms_visites', 'pdp')
    return hash_cip, cip_hash


# def get_info(info):
#     sector, dia_estat, cip_hash, grups = info[0], info[1], info[2], info[3]
#     print('vamos a workear')
#     upload = 
#     sql = """SELECT usua_cip, usua_nom, USUA_COGNOM1, USUA_I_COGNOMS, USUA_COGNOM2, 
#                 USUA_IDIOMA, USUA_TELEFON_PRINCIPAL, USUA_TELEFON_SECUNDARI 
#                 FROM USUTB040"""
#     for id, nom, cog1, i, cog2, idioma, telf1, telf2 in u.getAll(sql, sector):
#         try:
#             hash = cip_hash[id]
#             data_prog = dia_estat[hash]
#             grup = grups[hash]

#             upload.append((id, hash, nom, cog1, i, cog2, idioma, telf1, telf2, grup, data_prog))
#         except:
#             pass
#     u.listToTable(upload, 'sms_info_pacients', 'pdp')

def get_hash(cip):
    """."""
    if u.constants.IS_PYTHON_3:
        cip = cip.encode("latin1")
    return h.sha1(cip).hexdigest().upper()

def pacients(hash_cip, cip_hash):
    print('planificat')
    dia_estat = dict()
    grups = dict()
    sql = """SELECT p.hash, to_date(p.ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS') DATA, a.grup 
            FROM PLANIFICAT p, SEGUENT_VISITA_CRONICS a 
            WHERE p.estat in (6, 10) AND p.HASH = a.C_CIP"""
    for hash, estat_data, grup in u.getAll(sql, 'pdp'):
        try:
            dia_estat[hash_cip[hash]] = estat_data
            grups[hash_cip[hash]] = grup
        except:
            pass

    # u.multiprocess(get_info, (u.sectors, dia_estat, cip_hash, grups), 12, close=True)
    upload = dict()
    for e, sector in enumerate(u.sectors):
        print(e, sector)
        sql = """SELECT usua_cip, usua_nom, USUA_COGNOM1, USUA_I_COGNOMS, USUA_COGNOM2, 
                    USUA_IDIOMA, USUA_TELEFON_PRINCIPAL, USUA_TELEFON_SECUNDARI 
                    FROM USUTB040
                    WHERE USUA_SITUACIO = 'A'
                    and usua_uab_up is not null
                    """
        for id, nom, cog1, i, cog2, idioma, telf1, telf2 in u.getAll(sql, sector):
            if id in cip_hash:
                try:
                    data_prog = dia_estat[id]
                    grup = int(grups[id])
                    upload[id] =  (cip_hash[id], nom, cog1, i, cog2, idioma, telf1, telf2, grup, data_prog)
                except:
                    pass
        print(len(upload))
    to_load = []
    for k, v in upload.items():
        to_load.append((k, v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[8], v[9]))

    u.listToTable(to_load, 'sms_info_pacients', 'pdp')

def output():
    print("planificat")
    sql = """select TO_CHAR(current_date, 'D'), current_date from planificat WHERE rownum < 2"""
    weekday = 0
    dies = {
        2: 4,
        3: 4,
        4: 2,
        5: 2,
        6: 2
    }
    for n_day, day in u.getAll(sql, 'pdp'):
        weekday = n_day
        today = day

    links_cat = {
        1: 'https://bit.ly/3NSJvlc',
        2: 'https://bit.ly/3ahGtcJ',
        3: 'https://bit.ly/3bWXmtq',
        4: 'https://bit.ly/3IiaSUN',
        5: 'https://bit.ly/3anjsVM',
        6: 'https://bit.ly/3uspUBK',
        7: 'https://bit.ly/3PcZsUD',
        8: 'https://bit.ly/3IlAUXa',
        9: 'https://bit.ly/3yMrmRO',
        10: 'https://bit.ly/3bWEzP8',
        11: 'https://bit.ly/3ORVPU6',
        12: 'https://bit.ly/3nLrriA',
        13: 'https://bit.ly/3OONikX'
    }
    links_cast = {
        1: 'https://bit.ly/3nLMiSM',
        2: 'https://bit.ly/3bWcgjP',
        3: 'https://bit.ly/3IjdBgL',
        4: 'https://bit.ly/3urtOdN',
        5: 'https://bit.ly/3NU7Gj7',
        6: 'https://bit.ly/3nIkxug',
        7: 'https://bit.ly/3NU7Y9H',
        8: 'https://bit.ly/3nIubNB',
        9: 'https://bit.ly/3bPTTgh', 
        10: 'https://bit.ly/3AwPxVH',
        11: 'https://bit.ly/3NNj1Bx',
        12: 'https://bit.ly/3bVV1im',
        13: 'https://bit.ly/3yMYYPo'
    }

    # hash redics
    sql = """SELECT HASH FROM planificat
            WHERE EXTRACT(DAY FROM to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS'))= extract(DAY FROM SYSDATE-{}) 
            AND EXTRACT(MONTH FROM to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS'))= extract(MONTH FROM SYSDATE-{})
            AND EXTRACT(YEAR FROM to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS'))= extract(YEAR FROM SYSDATE-{})
            AND estat in (6, 10)""".format(dies[int(weekday)], dies[int(weekday)], dies[int(weekday)])
    # sql = """SELECT HASH FROM planificat
    #         WHERE EXTRACT(DAY FROM to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS'))= 21 
    #         AND EXTRACT(MONTH FROM to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS'))= 2
    #         AND EXTRACT(YEAR FROM to_date(ESTAT_DATA, 'YYYY/MM/DD HH24:MI:SS'))= 2023
    #         AND estat in (6, 10)"""
    hashos = []
    for hash, in u.getAll(sql, 'pdp'):
        hashos.append(hash)
    
    print("visites")
    visites = defaultdict(lambda: defaultdict(dict))
    sql = """SELECT hash, to_date(DIA_VISITA, 'DD-MM-YYYY'), HORA_VISITA, MODUL_VISITA, SERVEI_VISITA, UP, CENTRE_CODI, CENTRE_CLASSE 
                FROM SMS_VISITES sv
                WHERE to_date(DIA_VISITA, 'DD-MM-YYYY') > SYSDATE 
                ORDER BY to_date(DIA_VISITA, 'DD-MM-YYYY'), HORA_VISITA """
    for  hash, DIA_VISITA, HORA_VISITA, MODUL_VISITA, SERVEI_VISITA, UP, CENTRE_CODI, CENTRE_CLASSE in u.getAll(sql, 'pdp'):
        if hash in hashos:
            visites[hash][(CENTRE_CODI, UP, CENTRE_CLASSE)][(DIA_VISITA, HORA_VISITA)] = (MODUL_VISITA, SERVEI_VISITA)
    
    print("info pacients")
    info = dict()
    sql = """select hash, nom, idioma, telf1, telf2, cognom1, cognom2, grup FROM SMS_INFO_PACIENTS"""
    for hash, nom, idioma, telf1, telf2, cog1, cog2, grup in u.getAll(sql, 'pdp'):
        telefon = None
        nom = nom + ' ' + cog1[0] 
        try:
            nom = nom + '.' + cog2[0] + '.'
        except:
            pass
        if telf1 is not None:
            if len(telf1) == 9 and telf1[0] in ('6', '7'):
                telefon = telf1
        elif telf2 is not None:
            if len(telf2) == 9 and telf2[0] in ('6', '7'):
                telefon = telf2
        info[hash] = (nom, idioma, telefon, grup)
    
    print('centres')
    centres = dict()
    sql = """select CENT_CODI_CENTRE, cent_classe_centre, CENT_CODI_UP, cent_nom_centre from catalegs.cat_pritb010"""
    for centre_codi, centre_classe, UP, nom in u.getAll(sql, ("sidics", "x0002")):
        centres[(centre_codi, UP, centre_classe)] = nom

    print('descriptius')
    descriptiu = dict()
    sql = """select modu_centre_codi_centre, modu_centre_classe_centre,
                modu_servei_codi_servei, modu_codi_modul,modu_descripcio
                from catalegs.cat_vistb027"""
    for centre_codi, centre_classe, servei, modul, des in u.getAll(sql, ("sidics", "x0002")):
        descriptiu[(centre_codi, centre_classe, modul, servei)] = des

    print("omplir")
    upload = []
    no_upload = []
    for usuari in visites:
        print(usuari)
        text_visites = ""
        try:
            if usuari in info:
                nom, idioma, telf, grup = info[usuari]
                if grup not in (14,15):
                    if idioma[0:5] == 'CATAL': 
                        text_visites = "Bon dia " + nom + ", li recordem les properes visites de seguiment de les seves malalties cr�niques:\n"
                        for centre in visites[usuari]:
                            centre_codi, UP, centre_classe = centre
                            try:
                                nom_centre = centres[(centre_codi, UP, centre_classe)]
                                text_visites = text_visites + " \n Al " + nom_centre + ":\n"
                                sorted_visites = sorted(visites[usuari][centre].keys())
                                for visita in sorted_visites:
                                # for visita in visites[usuari][centre]:
                                    dia = str(visita[0])[0:10]
                                    dia = dia[8:] + '-' + dia[5:7] + '-' + dia[0:4]
                                    hora = str(visita[1])
                                    modul, servei= visites[usuari][centre][visita]
                                    des = descriptiu[(centre_codi, centre_classe, modul, servei)]
                                    if servei in ('EXTRA', 'LAB'):
                                        text_visites = text_visites + dia + " a les " + hora + "h t� cita per fer-se una ANAL�TICA" + "\n"
                                    else: 
                                        text_visites = text_visites + dia + " a les " + hora + "h t� cita amb " + servei + " - " + des + "\n"
                            except:
                                pass
                        text_visites = text_visites + " En aquest enlla� trobar� la seva pauta de seguiment: "
                        text_visites = text_visites + links_cat[grup]
                    else:
                        text_visites = "Buenos d�as " + nom + ", le recordamos las pr�ximas visitas de seguimiento de sus enfermedades cr�nicas: \n"
                        for centre in visites[usuari]:
                            centre_codi, UP, centre_classe = centre
                            nom_centre = centres[(centre_codi, UP, centre_classe)]
                            text_visites = text_visites + " \n En el " + nom_centre + ":\n"
                            for visita in visites[usuari][centre]:
                                dia = str(visita[0])
                                hora = str(visita[1])
                                modul, servei= visites[usuari][centre][visita]
                                des = descriptiu[(centre_codi, centre_classe, modul, servei)]
                                if servei in ('EXTRA', 'LAB'):
                                    text_visites = text_visites + dia + " a las " + hora + "h tiene cita para hacerse un ANALISIS" + "\n"
                                else: 
                                    text_visites = text_visites + dia + " a las " + hora + "h tiene cita con " + servei + " - " + des + "\n"
                        text_visites = text_visites + " En este enlace encontrar� su pauta de seguimiento: "
                        text_visites = text_visites + links_cast[grup]
                    if telf is not None:
                        upload.append((telf, text_visites))
                    else:
                        no_upload.append((usuari, text_visites, today))
                else:
                    if idioma[0:5] == 'CATAL': 
                        text_visites = "Bon dia " + nom + ", li recordem les properes visites de seguiment de les seves malalties cr�niques:\n"
                        for centre in visites[usuari]:
                            centre_codi, UP, centre_classe = centre
                            try:
                                nom_centre = centres[(centre_codi, UP, centre_classe)]
                                text_visites = text_visites + " \n Al " + nom_centre + ":\n"
                                for visita in visites[usuari][centre]:
                                    dia = str(visita[0])
                                    hora = str(visita[1])
                                    modul, servei= visites[usuari][centre][visita]
                                    des = descriptiu[(centre_codi, centre_classe, modul, servei)]
                                    if servei in ('EXTRA', 'LAB'):
                                        text_visites = text_visites + dia + " a les " + hora + "h t� cita per fer-se una ANAL�TICA" + "\n"
                                    else: 
                                        text_visites = text_visites + dia + " a les " + hora + "h t� cita amb " + servei + " - " + des + "\n"
                            except:
                                pass
                    else:
                        text_visites = "Buenos d�as " + nom + ", le recordamos las pr�ximas visitas de seguimiento de sus enfermedades cr�nicas: \n"
                        for centre in visites[usuari]:
                            centre_codi, UP, centre_classe = centre
                            nom_centre = centres[(centre_codi, UP, centre_classe)]
                            text_visites = text_visites + " \n En el " + nom_centre + ":\n"
                            for visita in visites[usuari][centre]:
                                dia = str(visita[0])
                                hora = str(visita[1])
                                modul, servei= visites[usuari][centre][visita]
                                des = descriptiu[(centre_codi, centre_classe, modul, servei)]
                                if servei in ('EXTRA', 'LAB'):
                                    text_visites = text_visites + dia + " a las " + hora + "h tiene cita para hacerse un ANALISIS" + "\n"
                                else: 
                                    text_visites = text_visites + dia + " a las " + hora + "h tiene cita con " + servei + " - " + des + "\n"
                    if telf is not None:
                        upload.append((telf, text_visites))
                    else:
                        no_upload.append((usuari, text_visites, today))

        except:
            raise
    u.listToTable(upload, 'missatge_sms', 'pdp')
    u.listToTable(no_upload, 'no_sms_planificat', 'pdp')
    print(len(no_upload))

    SMS_DIA = (d.datetime.now().date())

    wb1 = xlwt.Workbook()
    # ho guardem en diferents fulles a un excel
    ws_1 = wb1.add_sheet(u'1')
    wb2 = xlwt.Workbook()
    # ho guardem en diferents fulles a un excel
    ws_2 = wb2.add_sheet(u'1')
    wb3 = xlwt.Workbook()
    # ho guardem en diferents fulles a un excel
    ws_3 = wb3.add_sheet(u'1')
    i_1, i_2, i_3 = 0, 0, 0
    for e in upload:
        print(e[0].decode('iso-8859-1'))
        words = e[1].split(' ')
        txt = ''
        iteration = 1
        for word in words:
            if len(txt + word) - 11 < 540:
                txt = txt + word + ' '
            else:
                txt = txt + '(continua)'
                if iteration == 1: 
                    ws_1.write(i_1, 0, e[0].decode('iso-8859-1'))
                    ws_1.write(i_1, 1, txt.decode('iso-8859-1'))
                    i_1 += 1
                if iteration == 2: 
                    ws_2.write(i_2, 0, e[0].decode('iso-8859-1'))
                    ws_2.write(i_2, 1, txt.decode('iso-8859-1'))
                    i_2 += 1
                txt = word + ' '
                iteration += 1

        if iteration == 1: 
            ws_1.write(i_1, 0, e[0].decode('iso-8859-1'))
            ws_1.write(i_1, 1, txt.decode('iso-8859-1'))
            i_1 += 1
        elif iteration == 2: 
            print(e[0], txt)
            ws_2.write(i_2, 0, e[0].decode('iso-8859-1'))
            ws_2.write(i_2, 1, txt.decode('iso-8859-1'))
            i_2 += 1
        # elif iteration == 3: 
        #     ws_3.write(i_3, 0, e[0].decode('iso-8859-1'))
        #     ws_3.write(i_3, 1, txt.decode('iso-8859-1'))
        #     i_3 += 1

    wb1.save(u.tempFolder + 'sms_planificat_{}_1.xls'.format(SMS_DIA.strftime("%Y_%m_%d")))  # noqa)
    wb2.save(u.tempFolder + 'sms_planificat_{}_2.xls'.format(SMS_DIA.strftime("%Y_%m_%d")))  # noqa)
    # en cas que algun dia enviem sms MOLT MOLT llargs
    # wb3.save(u.tempFolder + 'sms_planificat_{}_3.xls'.format(SMS_DIA.strftime("%Y_%m_%d")))  # noqa)

    with t.SFTP("sisap") as sftp:
        file = 'sms_planificat_{}_1.xls'.format(SMS_DIA.strftime("%Y_%m_%d"))  # noqa
        sftp.put(u.tempFolder + file, "sms/planificat/{}".format(file))
        os.remove(u.tempFolder + file)
        file = 'sms_planificat_{}_2.xls'.format(SMS_DIA.strftime("%Y_%m_%d"))  # noqa
        sftp.put(u.tempFolder + file, "sms/planificat/{}".format(file))
        os.remove(u.tempFolder + file)

    # wb1.save(u.tempFolder + 'sms_planificat_{}_1.xls'.format('23_02_2023'))  # noqa)
    # wb2.save(u.tempFolder + 'sms_planificat_{}_2.xls'.format('23_02_2023'))  # noqa)
    # # en cas que algun dia enviem sms MOLT MOLT llargs
    # # wb3.save(u.tempFolder + 'sms_planificat_{}_3.xls'.format(SMS_DIA.strftime("%Y_%m_%d")))  # noqa)

    # with t.SFTP("sisap") as sftp:
    #     file = 'sms_planificat_{}_1.xls'.format('23_02_2023')  # noqa
    #     sftp.put(u.tempFolder + file, "sms/planificat/{}".format(file))
    #     os.remove(u.tempFolder + file)
    #     file = 'sms_planificat_{}_2.xls'.format('23_02_2023')  # noqa
    #     sftp.put(u.tempFolder + file, "sms/planificat/{}".format(file))
    #     os.remove(u.tempFolder + file)
    

if __name__ == "__main__":
    try:
        crear_taules()
        hash_cip, cip_hash = visites()
        pacients(hash_cip, cip_hash)
        output()
    except Exception:
        text = traceback.format_exc()
        print(text)
        mail = t.Mail()
        mail.to.append("roser.cantenys@catsalut.cat")
        mail.subject = "SMS PLANIFICAT bad!!!"
        mail.text = text
        mail.send()