# -*- coding: utf8 -*-

import hashlib as h

"""
Cataleg sanitaris
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""
        #self.get_dni()
        #self.get_categ2()
        self.get_cohorts()
        self.export_files()

  
    def get_dni(self):
        """."""
        u.printTime("cohort")
        self.dni_to_hash = {}
        sql = """SELECT dni,hash FROM dwsisap.RCA_CIP_NIA"""
        for dni, hash in u.getAll(sql, 'exadata'):
            self.dni_to_hash[dni] = hash
    
    def get_categ2(self):
        """."""
        u.printTime("cohort")
        self.categ2 = []
        sql = """SELECT idprofessional, idcategoriaprofesional FROM dwcatsalut.CNS_PROFESSIONALS_ENTITAT WHERE domini = 'SALUT'"""
        for id, categ in u.getAll(sql, 'exadata'):
            hash = self.dni_to_hash[id] if id in self.dni_to_hash else 'e'
            self.categ2.append([hash, categ])
            
    
    def get_cohorts(self):
        """agafem de cataleg """
        u.printTime("cohort")
        self.desc_sani = []
        sql = """SELECT cip_rca, categoria, desc_categ, grup_ugap FROM dwsisap.ppe_vac_p where cip_rca is not null"""
        for cip, categ, desc_categ, grup_ugap in u.getAll(sql, 'exadata'):
            hash = h.sha1(cip[:13]).hexdigest().upper()
            self.desc_sani.append([hash, categ, desc_categ, grup_ugap])

    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_recerca_cat_sanitaris"
        cols = ("hash varchar(40)", "categoria varchar(30)", "desc_categ varchar(300)", "grup_ugap varchar(100)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.desc_sani, tb, db)
        
        tb = "sisap_covid_recerca_vac_categ2_san"
        cols = ("hash varchar(40)", "categoria varchar(300)")
        #u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        #u.listToTable(self.categ2, tb, db)
                                
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final") 