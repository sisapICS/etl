# -*- coding: utf8 -*-

import hashlib as h

"""
Gravetat omicron. Mirem gravetat dels casos omicron versus altres onades, preferentment delta
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""
        self.get_tipus()
        self.get_residents()
        self.get_cip()
        self.get_dbs()
        self.get_dbs2020()
        self.get_cohorts()
        self.export_files()
  
    
    def get_tipus(self):
        """tipus residencia"""
        u.printTime("tipus")
        self.codis = {}
        sql = """SELECT a.codi, tipus, rct.nom AS grup FROM 
                        dwsisap.residencies_cataleg  a
                        LEFT JOIN dwsisap.RESIDENCIES_CATALEG_TIPUS rct 
                        ON a.tipus=rct.codi"""
        for codi, tipus, desc in u.getAll(sql, 'exadata'):
            self.codis[codi] = {'tipus': tipus, 'desc': desc}
    
    
    def get_residents(self):
        """ens quedem el cens i la data per veure si era resident o treballador resis al moment del cas"""
        u.printTime("residents")
        
        self.residents = {}
        sql = """SELECT hash, DATA, perfil, residencia 
                FROM 
                    dwsisap.RESIDENCIES_CENS 
                WHERE 
                    to_char(data, 'YYYYMMDD') >= '20201227'"""
        for hash, dat, perfil, resi in u.getAll(sql, 'exadata'):
            if resi in self.codis:
                tipus = self.codis[resi]['tipus']
                tip_desc = self.codis[resi]['desc']
                self.residents[(hash, dat)] = {'perfil': perfil, 'resi': resi, 'tipus': tipus, 'desc': tip_desc}
                    
    
    def get_cip(self):
        """hash to cip per recuperar DBS antic!"""
        u.printTime("cip")
        self.hash_to_cip = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.hash_to_cip[hash] = cip
       
    def get_dbs(self):
        """Dades del dbs actual (gener 2022)"""
        u.printTime("DBS")
        self.factors = {}
        self.in_dbs_true = {}
        sql = """select c_cip,c_gma_codi, c_gma_complexitat, PR_PCC_DATA, PR_MACA_DATA, PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    V_RCV_DATA, V_RCV_VALOR,
                     F_HTA_IECA_ARA2, F_HTA_CALCIOA, F_HTA_BETABLOQ, F_HTA_DIURETICS, F_HTA_ALFABLOQ, F_HTA_ALTRES, F_HTA_COMBINACIONS,
                    F_DIAB_ADO, F_DIAB_INSULINA, F_MPOC_ASMA,F_ANTIAGREGANTS_ANTICOA, F_AINE, F_ANALGESICS, 
                    F_ANTIDEPRESSIUS, F_ANSIO_HIPN, F_ANTIPSICOTICS, F_ANTIULCEROSOS, F_ANTIESP_URI, F_CORTIC_SISTEM,
                    F_ANTIEPILEPTICS,F_HIPOLIPEMIANTS, F_MHDA_VIH
                from DWSISAP.DBS"""
        for (cip, gma, gma1, pcc, maca,
             hta, dm1, dm2, mpoc,
             asma, bc, ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             rcv_dat, rcv_val,
             ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
             ado, insulina, f_mpoc, antiag, aine, analg,
             antidep, ansiol, antipsic, ulcer, anties, cortis,
             epil, hipol, mhda) in u.getAll(sql, 'exadata'):   
            self.factors[(cip, "in_dbs")] = 1
            self.in_dbs_true[(cip)] = True
            self.factors[(cip, "gma")] = gma
            self.factors[(cip, "gma1")] = gma1
            self.factors[(cip, "pcc")] = pcc
            self.factors[(cip, "maca")] = maca
            if hta:
                self.factors[(cip, "hta")] = hta
            if dm1:
                self.factors[(cip, "dm")] = dm1
            if dm2:
                self.factors[(cip, "dm")] = dm2
            if mpoc:
                self.factors[(cip, "mpoc")] = mpoc
            if asma:
                self.factors[(cip, "asma")] = asma
            if bc:
                self.factors[(cip, "bc")] = bc
            if ci:
                self.factors[(cip, "ci")] = ci
            if mcv:
                self.factors[(cip, "mcv")] = mcv
            if ic:
                self.factors[(cip, "ic")] = ic
            if acfa:
                self.factors[(cip, "acfa")] = acfa
            if valv:
                self.factors[(cip, "valv")] = valv
            if hepat:
                self.factors[(cip, "hepat")] = hepat
            if vhb:
                self.factors[(cip, "vhb")] = vhb
            if vhc:
                self.factors[(cip, "vhc")] = vhc
            if neo:
                self.factors[(cip, "neo")] = neo
            if mrc:
                self.factors[(cip, "mrc")] = mrc
            if obes:
                self.factors[(cip, "obes")] = obes
            if vih:
                self.factors[(cip, "vih")] = vih
            if sida:
                self.factors[(cip, "sida")] = sida
            if demencia:
                self.factors[(cip, "demencia")] = demencia
            if artrosi:
                self.factors[(cip, "artrosi")] = artrosi 
            if rcv_dat:
                self.factors[(cip, "rcv_dat")] = rcv_dat
                self.factors[(cip, "rcv_val")] = rcv_val
            if ieca:
                self.factors[(cip, "ieca")] = ieca    
            if calcioa:
                self.factors[(cip, "calcioa")] = calcioa
            if diur:
                self.factors[(cip, "diur")] = diur
            if betb:
                self.factors[(cip, "betb")] = betb
            if alfab:
                self.factors[(cip, "alfab")] = alfab
            if hta_alt:
               self.factors[(cip, "hta_alt")] = hta_alt
            if hta_comb:
                self.factors[(cip, "hta_comb")] = hta_comb    
            if ado:
                self.factors[(cip, "ado")] = ado
            if insulina:
                self.factors[(cip, "insulina")] = insulina
            if f_mpoc:
                self.factors[(cip, "f_mpoc")] = f_mpoc
            if antiag:
                self.factors[(cip, "antiag")] = antiag
            if aine:
                self.factors[(cip, "aine")] = aine
            if analg:
                self.factors[(cip, "analg")] = analg 
            if antidep:
                self.factors[(cip, "antidep")] = antidep
            if ansiol:
                self.factors[(cip, "ansiol")] = ansiol
            if antipsic:
                self.factors[(cip, "antipsic")] = antipsic
            if ulcer:
                self.factors[(cip, "ulcer")] = ulcer
            if anties:
                self.factors[(cip, "anties")] = anties    
            if cortis:
                self.factors[(cip, "cortis")] = cortis
            if epil:
                self.factors[(cip, "epil")] = epil
            if hipol:
                self.factors[(cip, "hipol")] = hipol
            if mhda:
                self.factors[(cip, "mhda")] = mhda
    
    def get_dbs2020(self):
        """Dades del dbs antic de desembre 2020"""
        u.printTime("DBS 2020")
        
        SIDICS_DB = ("dbs", "x0002")
        sql = """select c_cip,c_gma_codi, c_gma_complexitat, PR_PCC_DATA, PR_MACA_DATA, PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    V_RCV_DATA, V_RCV_VALOR,
                     F_HTA_IECA_ARA2, F_HTA_CALCIOA, F_HTA_BETABLOQ, F_HTA_DIURETICS, F_HTA_ALFABLOQ, F_HTA_ALTRES, F_HTA_COMBINACIONS,
                    F_DIAB_ADO, F_DIAB_INSULINA, F_MPOC_ASMA,F_ANTIAGREGANTS_ANTICOA, F_AINE, F_ANALGESICS, 
                    F_ANTIDEPRESSIUS, F_ANSIO_HIPN, F_ANTIPSICOTICS, F_ANTIULCEROSOS, F_ANTIESP_URI, F_CORTIC_SISTEM,
                    F_ANTIEPILEPTICS,F_HIPOLIPEMIANTS, F_MHDA_VIH
                from dbs.dbs_2020"""
        for (cip1, gma, gma1, pcc, maca,
             hta, dm1, dm2, mpoc,
             asma, bc, ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             rcv_dat, rcv_val,
             ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
             ado, insulina, f_mpoc, antiag, aine, analg,
             antidep, ansiol, antipsic, ulcer, anties, cortis,
             epil, hipol, mhda) in u.getAll(sql, SIDICS_DB):
            if cip1 in self.hash_to_cip:
                cip2 = self.hash_to_cip[cip1]
                cip = h.sha1(cip2).hexdigest().upper()
                if cip not in self.in_dbs_true:
                    self.factors[(cip, "in_dbs")] = 1
                    self.in_dbs_true[(cip)] = True
                    self.factors[(cip, "gma")] = gma
                    self.factors[(cip, "gma1")] = gma1
                    self.factors[(cip, "pcc")] = pcc
                    self.factors[(cip, "maca")] = maca
                    if hta:
                        self.factors[(cip, "hta")] = hta
                    if dm1:
                        self.factors[(cip, "dm")] = dm1
                    if dm2:
                        self.factors[(cip, "dm")] = dm2
                    if mpoc:
                        self.factors[(cip, "mpoc")] = mpoc
                    if asma:
                        self.factors[(cip, "asma")] = asma
                    if bc:
                        self.factors[(cip, "bc")] = bc
                    if ci:
                        self.factors[(cip, "ci")] = ci
                    if mcv:
                        self.factors[(cip, "mcv")] = mcv
                    if ic:
                        self.factors[(cip, "ic")] = ic
                    if acfa:
                        self.factors[(cip, "acfa")] = acfa
                    if valv:
                        self.factors[(cip, "valv")] = valv
                    if hepat:
                        self.factors[(cip, "hepat")] = hepat
                    if vhb:
                        self.factors[(cip, "vhb")] = vhb
                    if vhc:
                        self.factors[(cip, "vhc")] = vhc
                    if neo:
                        self.factors[(cip, "neo")] = neo
                    if mrc:
                        self.factors[(cip, "mrc")] = mrc
                    if obes:
                        self.factors[(cip, "obes")] = obes
                    if vih:
                        self.factors[(cip, "vih")] = vih
                    if sida:
                        self.factors[(cip, "sida")] = sida
                    if demencia:
                        self.factors[(cip, "demencia")] = demencia
                    if artrosi:
                        self.factors[(cip, "artrosi")] = artrosi 
                    if rcv_dat:
                        self.factors[(cip, "rcv_dat")] = rcv_dat
                        self.factors[(cip, "rcv_val")] = rcv_val
                    if ieca:
                        self.factors[(cip, "ieca")] = ieca    
                    if calcioa:
                        self.factors[(cip, "calcioa")] = calcioa
                    if diur:
                        self.factors[(cip, "diur")] = diur
                    if betb:
                        self.factors[(cip, "betb")] = betb
                    if alfab:
                        self.factors[(cip, "alfab")] = alfab
                    if hta_alt:
                        self.factors[(cip, "hta_alt")] = hta_alt
                    if hta_comb:
                        self.factors[(cip, "hta_comb")] = hta_comb    
                    if ado:
                        self.factors[(cip, "ado")] = ado
                    if insulina:
                        self.factors[(cip, "insulina")] = insulina
                    if f_mpoc:
                        self.factors[(cip, "f_mpoc")] = f_mpoc
                    if antiag:
                        self.factors[(cip, "antiag")] = antiag
                    if aine:
                        self.factors[(cip, "aine")] = aine
                    if analg:
                        self.factors[(cip, "analg")] = analg 
                    if antidep:
                        self.factors[(cip, "antidep")] = antidep
                    if ansiol:
                        self.factors[(cip, "ansiol")] = ansiol
                    if antipsic:
                        self.factors[(cip, "antipsic")] = antipsic
                    if ulcer:
                        self.factors[(cip, "ulcer")] = ulcer
                    if anties:
                        self.factors[(cip, "anties")] = anties    
                    if cortis:
                        self.factors[(cip, "cortis")] = cortis
                    if epil:
                        self.factors[(cip, "epil")] = epil
                    if hipol:
                        self.factors[(cip, "hipol")] = hipol
                    if mhda:
                        self.factors[(cip, "mhda")] = mhda
    
    
    def get_cohorts(self):
        """agafem els casos segons cohort"""
        u.printTime("cohort")
        self.pacients = []
        sql = """SELECT 
                    a.hash, a.data_naixement,  a.sexe, a.rca_up, a.RCA_MUNICIPI, es_cens_salut,
                    CASE WHEN CAS_DATA_CAS BETWEEN DATE '2020-09-29' AND  DATE '2020-12-05' THEN '2 original'
                    WHEN CAS_DATA_CAS BETWEEN DATE  '2020-12-15' AND DATE '2021-02-15' THEN '3 original/alfa'
                    WHEN CAS_DATA_CAS BETWEEN DATE '2021-03-15' AND DATE '2021-05-15' THEN '4 alfa '
                    WHEN CAS_DATA_CAS BETWEEN DATE '2021-05-16' AND DATE '2021-06-30' THEN '5 alfa '
                    WHEN cas_Data_cas BETWEEN DATE '2021-07-01' AND DATE '2021-08-26' THEN '5 delta'
                    WHEN cas_Data_cas BETWEEN DATE '2021-11-01' AND DATE '2021-12-10' THEN '6 delta'
                    WHEN cas_Data_cas BETWEEN DATE '2021-12-11' AND DATE '2021-12-19' THEN '6 delta/omicron'
                    WHEN cas_Data_cas BETWEEN DATE '2021-12-20' AND sysdate THEN '6 omicron' ELSE 'altres'
                    END variant,
                    vacuna_1_data, immunitzat_data, booster_administracio_data,
                    CASE WHEN immunitzat=0 OR vacuna_1_data > cas_data_cas THEN 'no_vacunat'
                    WHEN   cas_data_cas - BOOSTER_ADMINISTRACIO_DATA> 14 THEN 'booster'
                    WHEN cas_data_cas - IMMUNITZAT_DATA> 14 THEN 'completa'
                    ELSE 'parcial' END vacuna,
                    cas_data_cas, ingres_primer, ingres_uci_primer, exitus_covid,
                    CASE WHEN INGRES_PRIMER-CAS_DATA_CAS BETWEEN 0 AND 7 THEN 1 ELSE 0 end hosp7,
                    CASE WHEN INGRES_uci_PRIMER-CAS_DATA_CAS BETWEEN 0 AND 7 THEN 1 ELSE 0 end uci7,
                    CASE WHEN INGRES_PRIMER-CAS_DATA_CAS BETWEEN 0 AND 14 THEN 1 ELSE 0 end hosp14,
                    CASE WHEN INGRES_uci_PRIMER-CAS_DATA_CAS BETWEEN 0 AND 14 THEN 1 ELSE 0 end uci14,
                    CASE WHEN exitus_covid-CAS_DATA_CAS BETWEEN 0 AND 28 THEN 1 ELSE 0 end exitus28,
                    CASE WHEN sysdate - 8 > cas_data_cas then 1 else 0 end cas7, 
                    CASE WHEN sysdate - 15 > cas_data_cas then 1 else 0 end cas14,
                    CASE WHEN sysdate - 29 > cas_data_cas then 1 else 0 end cas28
                FROM 
                    dwsisap.DBC_METRIQUES a
                LEFT JOIN 
                    dwsisap.DBC_VACUNA B
                ON 
                    a.hash=b.hash
                WHERE 
                    cas_data_cas is not null and a.edat_final is not null"""
        for hash, naix, sexe, up, municipi, cens_salut, variant, vac1, immunitzat, booster, vacunal, cas_data, ingres, uci, exitus, hx7, uci7, hx14, uci14, exitus28, cas7, cas14, cas28 in u.getAll(sql, 'exadata'):
            cens_resi = self.residents[(hash, cas_data)]['perfil'] if (hash, cas_data) in self.residents else 'No resi'
            resi = self.residents[(hash, cas_data)]['resi'] if (hash, cas_data) in self.residents else None
            tipus_resi = self.residents[(hash, cas_data)]['tipus'] if (hash, cas_data) in self.residents else 999
            desc_tipus_resi = self.residents[(hash, cas_data)]['desc'] if (hash, cas_data) in self.residents else None
            hta, dm, mpoc = None, None, None
            asma, bc, ci = None, None, None
            mcv, ic, acfa, valv = None, None, None, None
            hepat, vhb, vhc, neo, mrc = None, None, None, None, None
            obes, vih, sida = None, None, None
            demencia, artrosi = None, None
            medea = None
            in_dbs = 0
            rcv_dat, rcv_val = None, None
            ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb = None, None,None,None,None,None,None
            ado, insulina, f_mpoc, antiag, aine, analg = None,None,None,None,None,None
            antidep, ansiol, antipsic, ulcer, anties, cortis = None,None,None,None,None,None
            epil, hipol, mhda = None, None, None
            hcq, azitro, salbutamol = None, None, None
            if hash in self.in_dbs_true:
                in_dbs = self.factors[(hash, "in_dbs")] if (hash, "in_dbs") in self.factors else 0
                gma = self.factors[(hash, "gma")] if (hash, "gma") in self.factors else None
                gma1 = self.factors[(hash, "gma1")] if (hash, "gma1") in self.factors else None
                pcc = self.factors[(hash, "pcc")] if (hash, "pcc") in self.factors else None
                maca = self.factors[(hash, "maca")] if (hash, "maca") in self.factors else None
                hta = self.factors[(hash, "hta")] if (hash, "hta") in self.factors else None
                dm = self.factors[(hash, "dm")] if (hash, "dm") in self.factors else None
                mpoc = self.factors[(hash, "mpoc")] if (hash, "mpoc") in self.factors else None
                asma = self.factors[(hash, "asma")] if (hash, "asma") in self.factors else None
                bc = self.factors[(hash, "bc")] if (hash, "bc") in self.factors else None
                ci = self.factors[(hash, "ci")] if (hash, "ci") in self.factors else None
                mcv = self.factors[(hash, "mcv")] if (hash, "mcv") in self.factors else None
                ic = self.factors[(hash, "ic")] if (hash, "ic") in self.factors else None
                acfa = self.factors[(hash, "acfa")] if (hash, "acfa") in self.factors else None
                valv = self.factors[(hash, "valv")] if (hash, "valv") in self.factors else None
                hepat = self.factors[(hash, "hepat")] if (hash, "hepat") in self.factors else None
                vhb = self.factors[(hash, "vhb")] if (hash, "vhb") in self.factors else None
                vhc = self.factors[(hash, "vhc")] if (hash, "vhc") in self.factors else None
                neo = self.factors[(hash, "neo")] if (hash, "neo") in self.factors else None
                mrc = self.factors[(hash, "mrc")] if (hash, "mrc") in self.factors else None
                obes = self.factors[(hash, "obes")] if (hash, "obes") in self.factors else None
                vih = self.factors[(hash, "vih")] if (hash, "vih") in self.factors else None
                sida = self.factors[(hash, "sida")] if (hash, "sida") in self.factors else None
                demencia = self.factors[(hash, "demencia")] if (hash, "demencia") in self.factors else None
                artrosi = self.factors[(hash, "artrosi")] if (hash, "artrosi") in self.factors else None
                rcv_dat = self.factors[(hash, "rcv_dat")] if (hash, "rcv_dat") in self.factors else None
                rcv_val = self.factors[(hash, "rcv_val")] if (hash, "rcv_val") in self.factors else None
                ieca = self.factors[(hash, "ieca")] if (hash, "ieca") in self.factors else None
                calcioa = self.factors[(hash, "calcioa")] if (hash, "calcioa") in self.factors else None
                betb = self.factors[(hash, "betb")] if (hash, "betb") in self.factors else None
                diur = self.factors[(hash, "diur")] if (hash, "diur") in self.factors else None
                alfab = self.factors[(hash, "alfab")] if (hash, "alfab") in self.factors else None
                hta_alt = self.factors[(hash, "hta_alt")] if (hash, "hta_alt") in self.factors else None
                hta_comb = self.factors[(hash, "hta_comb")] if (hash, "hta_comb") in self.factors else None
                ado = self.factors[(hash, "ado")] if (hash, "ado") in self.factors else None
                insulina = self.factors[(hash, "insulina")] if (hash, "insulina") in self.factors else None
                f_mpoc = self.factors[(hash, "f_mpoc")] if (hash, "f_mpoc") in self.factors else None
                antiag = self.factors[(hash, "antiag")] if (hash, "antiag") in self.factors else None
                aine = self.factors[(hash, "aine")] if (hash, "aine") in self.factors else None
                analg = self.factors[(hash, "analg")] if (hash, "analg") in self.factors else None
                antidep = self.factors[(hash, "antidep")] if (hash, "antidep") in self.factors else None
                ansiol = self.factors[(hash, "ansiol")] if (hash, "ansiol") in self.factors else None
                antipsic = self.factors[(hash, "antipsic")] if (hash, "antipsic") in self.factors else None
                ulcer = self.factors[(hash, "ulcer")] if (hash, "ulcer") in self.factors else None
                anties = self.factors[(hash, "anties")] if (hash, "anties") in self.factors else None
                cortis = self.factors[(hash, "cortis")] if (hash, "cortis") in self.factors else None
                epil = self.factors[(hash, "epil")] if (hash, "epil") in self.factors else None
                hipol = self.factors[(hash, "hipol")] if (hash, "hipol") in self.factors else None
                mhda = self.factors[(hash, "mhda")] if (hash, "mhda") in self.factors else None
            self.pacients.append([hash, naix, sexe, up, municipi, cens_salut,cens_resi, resi, tipus_resi, desc_tipus_resi,
                variant, vac1, immunitzat, booster, vacunal, 
                cas_data, ingres, uci, exitus, hx7, uci7, hx14, uci14, exitus28,
                cas7, cas14, cas28,
                in_dbs, hta, dm, mpoc,asma, bc, ci, mcv, ic, acfa, valv, hepat, vhb, vhc, neo, mrc, obes, vih, sida,
                     demencia, artrosi,
                     ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
                     ado, insulina, f_mpoc, antiag, aine, analg,
                     antidep, ansiol, antipsic, ulcer, anties, cortis,
                     epil, hipol, mhda])                  

    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_recerca_variants"
        cols = ("hash varchar(40)", "data_naix date", "sexe varchar(10)",  "up varchar(10)", "municipi varchar(10)", 
                "cens_salut int", "cens_residencia varchar(50)", "residencia varchar(10)", "tipus_residencia int", "desc_tipus_residencia varchar(100)",
                "variant varchar(100)", "vacuna_1_data date", "immunitzat_data date", "booster_data date", "estat_vacunal varchar(100)",
                "cas_data_cas date", "ingres_primer date", "ingres_uci_primer date", "exitus_covid date", 
                "ingres7d int", "uci7d int", "ingres14d int", "uci14 int", "exitus28 int",
                "cas7 int", "cas14 int", "cas28 int","in_dbs int",
            "FR_hta date",  "FR_dm date", "FR_mpoc date", "FR_asma date", "FR_bc date", "FR_ci date", "FR_mcv date",
            "FR_ic date", "FR_acfa date", "FR_valv date", "FR_hepat date", "FR_vhb date", "FR_vhc date", "FR_neo date", "FR_mrc date", "FR_obes date",
            "FR_vih date", "FR_sida date", "FR_demencia date", "FR_artrosi date",
            "F_HTA_IECA_ARA2 varchar(500)", "F_HTA_CALCIOA varchar(500)","F_HTA_BETABLOQ varchar(500)","F_HTA_DIURETICS varchar(500)",
            "F_HTA_ALFABLOQ varchar(500)", "F_HTA_ALTRES varchar(500)", "F_HTA_COMBINACIONS varchar(500)",
             "F_DIAB_ADO varchar(500)", "F_DIAB_INSULINA varchar(500)", "F_MPOC_ASMA varchar(500)", 
             "F_ANTIAGREGANTS_ANTICOA varchar(500)", "F_AINE varchar(500)", "F_ANALGESICS varchar(500)",
             "F_ANTIDEPRESSIUS varchar(500)", "F_ANSIO_HIPN varchar(500)", "F_ANTIPISCOTICS varchar(500)",
             "F_ANTIULCEROSOS varchar(500)", "F_ANTIESP_URI varchar(500)", "F_CORTIC_SISTEM varchar(500)",
             "F_ANTIEPILEPTICS varchar(500)", "F_HIPOLIPEMIANTS varchar(500)", "F_MHDA_VIH varchar(500)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pacients, tb, db)

                        
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final") 