---
title: "KM vacunats vs no vacunats Profes"
output: html_notebook
---

```{r include=FALSE}
library(data.table)
library(lubridate)
library(tidyr)
library(dplyr)
library(kableExtra)
library(ggplot2)

options(knitr.kable.NA = '')
```

# Anàlisi estadística 4

Per a cada cohort inclosa en l'estudi, s'ha analitzat, mitjançant la SMD, l'existència de possibles variables confusores com l'edat, el sexe i la patologia i medicació crònica entre els grups de vacunats i no vacunats. S'ha utilitzat com a punt de tall una SMD > 0.1.

Les probabilitats fins a cada outcome s'han obtingut amb les estimacions de Kaplan-Meier, estratificant pel grup de vacunats i no vacunats. S'han utilitzat els models de Cox ajustats per edat i sexe per a calcular els hazard ratios (HRs) per a cada outcome i cada cohort. 

S'han considerat els següents intervals de seguiment

- Període global, dels 27 de desembre de 2020 fins a 12  de febrer de 2021.
- 12 primers dies després de rebre la 1a dosi de la vacuna
- Seguiment fins a rebre la 2a dosi de la vacuna
- Seguiment després de rebre la 2a dosi de la vacuna.

Per a cada període s'ha calculat la incidència per persona dia, sumant el nombre d'outcomes observats i dividint-lo per la suma total dels dies de seguiment. Aquest valor es dóna multiplicat per 10.000.


# Dades

```{r include = FALSE, warning = FALSE, message = FALSE}
library(RMySQL)
## cat_centres
drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password="",host = "10.80.217.68", port=3307, dbname="nodrizas")
nfetch <- -1
query <- dbSendQuery(con,
                     statement = "select * from permanent.sisap_covid_recerca_vac_profe")
dades <- data.table(fetch(query, n = nfetch))

dbDisconnect(con)
```

```{r}
# Dates
# dades[inici == "0000-00-00", inici := NA]
dades[data_cas == "0000-00-00", data_cas := NA]
dades[ingres_primer == "0000-00-00", ingres_primer := NA]
dades[uci_primer == "0000-00-00", uci_primer := NA]
dades[vac_1dosi == "0000-00-00", vac_1dosi := NA]
dades[vac_2dosi == "0000-00-00", vac_2dosi := NA]
dades[exitus == "0000-00-00", exitus := NA]


dades[, c("data_naix", "data_cas", "ingres_primer", "uci_primer", "vac_1dosi", "vac_2dosi", "exitus") := lapply(.SD, function(x){
  as.Date(x)
  }), .SDcols = c("data_naix", "data_cas", "ingres_primer", "uci_primer", "vac_1dosi", "vac_2dosi", "exitus")]

# FR
fr <- names(dades)[which(substr(names(dades), 1, 2) == "FR")]
dades[, tolower(fr) := lapply(.SD, function(x){
  factor(ifelse(x != "0000-00-00", "Sí", "No"))
}
), .SDcols = fr]
fr <- tolower(fr)
# Fàrmacs
tts <- names(dades)[which(substr(names(dades), 1, 1) == "F" & substr(names(dades), 1, 2) != "FR")]

dades[, tolower(tts) := lapply(.SD, function(x){
  factor(ifelse(x != "", "Sí", "No"))
}
), .SDcols = tts]

tts <- tolower(tts)
```

## Depurar

Eliminar totes les morts < "2020-12-27" i inicialitzar data inici a "2020-12-27"

```{r}
dades <- dades[exitus >= as.Date("2021-03-01") | is.na(exitus)]
dades[, inici := as.Date("2021-03-01")]
```

Eliminar els casos amb data_cas == vac_1dosi

```{r}
dades <- dades[data_cas !=  vac_1dosi | is.na(data_cas) | is.na(vac_1dosi)]
```

Tallar a 2020-02-12. Posar tots els outcomes posteriors a 12 de febrer, a NULL.

```{r}
dades[data_cas > as.Date("2021-04-30"), data_cas := NA]
dades[vac_1dosi > as.Date("2021-04-30"), vac_1dosi := NA]
dades[vac_2dosi > as.Date("2021-04-30"), vac_2dosi := NA]
dades[ingres_primer > as.Date("2021-04-30"), ingres := NA]
dades[exitus > as.Date("2021-04-30"), exitus := NA]
```


## Crear variables

```{r}
dades[, edat := as.period(interval(start = data_naix, end = today() - days(1)))$year]
dades[, vacunats_1 := ifelse(!is.na(vac_1dosi), "1a dosi", "No vacunat")]
dades[, covid := ifelse(!is.na(data_cas), "Sí", "No")]
dades[, ingres_cat := ifelse(!is.na(ingres_primer), "Sí", "No")]
dades[, uci_cat := ifelse(!is.na(uci_primer), "Sí", "No")]
dades[, exitus_cat := ifelse(!is.na(exitus), "Sí", "No")]
```

```{r}
# data_cas < inici
t <- data.table(
  `vac_1dosi < inici` = nrow(dades[vac_1dosi < inici]),
  `vac_2dosi < inici` = nrow(dades[vac_2dosi < inici]),
  `is.na(vac_1dosi) & !is.na(vac_2dosi)` = nrow(dades[!is.na(vac_2dosi) & is.na(vac_1dosi)]),
  `vacuna_1dosi > vacuna_2dosi` = nrow(dades[vac_2dosi < vac_1dosi])) 
```

Si vac_2dosi i no vac_1dosi --> vac_1dosi := vac_2dosi

```{r}
dades[!is.na(vac_2dosi) & is.na(vac_1dosi), vac_1dosi := vac_2dosi]
```

```{r}
dades_sanitaris <- dades
```


# Descriptiva

```{r}
N_vacuna_1 <- dades[, .(dosi_1 = .N), by = c("vac_1dosi")]
N_vacuna_1[, N := sum(dosi_1)]
N_vacuna_1[order(vac_1dosi), cs := cumsum(dosi_1)]
N_vacuna_1[, cobertura := cs/N*100]
#fwrite(N_vacuna_1, "Majors80/cobertura_vacunal.csv", sep = ";", dec = ",", row.names = F)
```

```{r}
dades[, dif_1_2 := as.numeric(vac_2dosi - vac_1dosi)]
```

# Anàlisi supervivència (KM)

```{r}
library("survival")
library("survminer")
```

## COVID

### Majors de 80

### Tot el període

```{r}
a <- dades[,.N, by=.(edat)]
km_sanitaris <- dades[, c("inici", "data_cas", "vac_1dosi", "covid", "edat", "sexe")]
km_sanitaris_no_vacunats <- km_sanitaris[is.na(vac_1dosi) | vac_1dosi > inici]
#km_sanitaris_no_vacunats <- km_sanitaris[is.na(vac_1dosi)]
km_sanitaris_no_vacunats[, vacunats_1 := "No vacunats"]
# Cas o censura
km_sanitaris_no_vacunats[data_cas < vac_1dosi | (is.na(vac_1dosi) & covid == "Sí"), status := 2]
#km_sanitaris_no_vacunats[(is.na(vac_1dosi) & covid == "Sí"), status := 2]
km_sanitaris_no_vacunats[is.na(vac_1dosi) & is.na(data_cas), status := 1]
km_sanitaris_no_vacunats[!is.na(vac_1dosi) & (covid == "No" | data_cas > vac_1dosi), status := 1]
#km_sanitaris_no_vacunats[!is.na(vac_1dosi) & (covid == "No" ), status := 1]
# Seguiment
km_sanitaris_no_vacunats[vac_1dosi > inici & (vac_1dosi < data_cas | is.na(data_cas)), time := as.numeric(vac_1dosi - inici)]
km_sanitaris_no_vacunats[vac_1dosi > inici & (vac_1dosi >= data_cas), time := as.numeric(data_cas - inici)]
km_sanitaris_no_vacunats[is.na(vac_1dosi) & data_cas <= as.Date("2021-04-30"), time := as.numeric(data_cas - inici)]
km_sanitaris_no_vacunats[is.na(vac_1dosi) & is.na(data_cas), time := as.numeric(as.Date("2021-04-30") - inici)]



km_sanitaris_vacunats <- km_sanitaris[!is.na(vac_1dosi) & (is.na(data_cas) | data_cas > vac_1dosi)]
km_sanitaris_vacunats[, vacunats_1 := "Vacunats"]
km_sanitaris_vacunats[, time := ifelse(!is.na(data_cas), as.numeric(data_cas - vac_1dosi), as.numeric(as.Date("2021-04-30") - vac_1dosi))]
km_sanitaris_vacunats[, status := ifelse(!is.na(data_cas), 2, 1)]

km_sanitaris_data <- rbind(km_sanitaris_vacunats[, .SD, .SDcols = c("inici", "data_cas", "vac_1dosi", "covid", "edat", "sexe", "vacunats_1", "status", "time")], km_sanitaris_no_vacunats[, .SD, .SDcols = c("inici", "data_cas", "vac_1dosi", "covid", "edat", "sexe", "vacunats_1", "status", "time")])
```

##### Taula 2

```{r}
taula_2_tot_periode_covid_sanitaris <- km_sanitaris_data[, .(
  N = .N,
  casos = sum(status == 2),
  seguiment = sum(time),
  seguiment_mig = mean(time)
), vacunats_1]
taula_2_tot_periode_covid_sanitaris[, ":=" (
  rate_pers_dia = casos/(seguiment)*10000
)]
taula_2_tot_periode_covid_sanitaris_ir <- taula_2_tot_periode_covid_sanitaris[vacunats_1 == "Vacunats"]$rate_pers_dia/taula_2_tot_periode_covid_sanitaris[vacunats_1 == "No vacunats"]$rate_pers_dia
taula_2_tot_periode_covid_sanitaris_sd <- (1/taula_2_tot_periode_covid_sanitaris[vacunats_1 == "Vacunats"]$casos + 1/taula_2_tot_periode_covid_sanitaris[vacunats_1 == "No vacunats"]$casos)^1/2
taula_2_tot_periode_covid_sanitaris_ir_inf <- exp(log(taula_2_tot_periode_covid_sanitaris_ir) - 1.96*taula_2_tot_periode_covid_sanitaris_sd)
taula_2_tot_periode_covid_sanitaris_ir_sup <- exp(log(taula_2_tot_periode_covid_sanitaris_ir) + 1.96*taula_2_tot_periode_covid_sanitaris_sd)

taula_2_tot_periode_covid_sanitaris[vacunats_1 == "Vacunats", RRR := paste0(format(round((1 - taula_2_tot_periode_covid_sanitaris_ir)*100, 4), decimal.mark = ".", big.mark = ","), " IC95%: [", format(round((1 - taula_2_tot_periode_covid_sanitaris_ir_inf)*100, 4), decimal.mark = ".", big.mark = ","), " - ", format(round((1 - taula_2_tot_periode_covid_sanitaris_ir_sup)*100, 4), decimal.mark = ".", big.mark = ","), "]")]
taula_2_tot_periode_covid_sanitaris[, periode := "Total follow-up"]
```

```{r}
fit_covid_sanitaris <- survfit(Surv(time, status) ~ vacunats_1, data = km_sanitaris_data)
fit_covid_sanitaris
summary(fit_covid_sanitaris)
```

```{r}
km_covid_sanitaris_results <- data.table(
  Type  =c(rep("No vacunats", fit_covid_sanitaris$strata[[1]]), rep("Vacunats", fit_covid_sanitaris$strata[[2]])),
  time = fit_covid_sanitaris$time,
  N_risk = fit_covid_sanitaris$n.risk,
  N_event = fit_covid_sanitaris$n.event,
  N_censor= fit_covid_sanitaris$n.censor,
  Prop_surv = fit_covid_sanitaris$surv,
  IC95_low = fit_covid_sanitaris$lower,
  IC95_upp = fit_covid_sanitaris$upper,
  std_error = fit_covid_sanitaris$std.err,
  cum_hazard = fit_covid_sanitaris$cumhaz,
  std_chaz = fit_covid_sanitaris$std.chaz
  
)
#fwrite(km_covid_sanitaris_results, "Majors80/km_covid_sanitaris_results_tot_el_periode.csv", sep = ";", dec = ".", row.names = F)
```


```{r message=FALSE, warning=FALSE}
tiff("Majors80/km_covid_sanitaris_tot_el_periode.jpg", units="mm", width=150, height = 100, res=300)
ggsurvplot(fit_covid_sanitaris,  size = 1,  # change line size
           linetype = "strata", # change line type by groups
           break.time.by = 5, # break time axis by 250
           palette = "grey",
           conf.int = TRUE, # Add confidence interval
           pval = TRUE, # Add p-value
           pval.coord = c(0, .98),
           ylim = c(.98, 1),
           xlim = c(0, 57),
           ggtheme = theme_classic(),
           legend.labs = c("No vacunats", "Vacunats"),
           title = "COVID-19 infections",
           subtitle = "Docents",
           legend = "bottom",
           censor = TRUE
           )
```

```{r}
# Fit a Cox proportional hazards model
fit_covid_sanitaris_coxph <- coxph(Surv(time, status) ~ vacunats_1, data = km_sanitaris_data)
hr_covid_sanitaris_tot_el_periode <-  summary(fit_covid_sanitaris_coxph)$conf.int
test <- cox.zph(fit_covid_sanitaris_coxph)
ggcoxzph(test)

fit_covid_sanitaris_coxph_age_sex <- coxph(Surv(time, status) ~ vacunats_1 + edat + sexe, data = km_sanitaris_data)
hr_covid_sanitaris_age_sex_tot_el_periode <-  summary(fit_covid_sanitaris_coxph_age_sex)$conf.int[1, ]
test <- cox.zph(fit_covid_sanitaris_coxph_age_sex)
ggcoxzph(test)[1]
```
