# -*- coding: utf8 -*-

import hashlib as h

"""
Vacunes en majors de 80 20210218
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""

        self.get_cohorts()
        self.export_files()
     
    def get_cohorts(self):
        """excloem si covid previ a 27/12"""
        u.printTime("cohort")
        self.pacients = []
        sql = """select a.hash, a.sexe, a.data_naixement, a.rca_up, es_atdom, ministeri_codi, pdia_primer_positiu, ingres_primer, ingres_ultim, ingres_uci_primer,  ingres_uci_ultim, vac_dosi_1, vac_dosi_2, exitus_covid
                FROM DWSISAP.DBC_vacuna a left join dwsisap.dbc_metriques  b on a.hash=b.hash
                where a.edat between 18 and 55 and es_cens_profe=1 and
                (PDIA_PRIMER_POSITIU is null or to_char(PDIA_PRIMER_POSITIU, 'YYYYMMDD') >'20210301') and
                (exitus_covid is null or to_char(exitus_covid, 'YYYYMMDD') >'20210301') and 
                 (vac_dosi_1 is null or to_char(vac_dosi_1, 'YYYYMMDD')>'20210301')"""
        for hash, sexe, naix, up, atdom, tipus, data_cas, ingres_p, ingres_u, uci_p, uci_u, vac1, vac2, exitus in u.getAll(sql, 'exadata'):
            self.pacients.append([hash, naix, sexe, up, atdom, tipus, data_cas, ingres_p, ingres_u, uci_p, uci_u, vac1, vac2, exitus])
                   
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_recerca_vac_profe"
        cols = ("hash varchar(40)", "data_naix date", "sexe varchar(10)",  "up varchar(10)", "atdom int", "tipus int", "data_cas date", 
        "ingres_primer date", "ingres_ultim date", "uci_primer date", "uci_ultim date","vac_1dosi date", "vac_2dosi date", "exitus date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pacients, tb, db)
                         
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final") 