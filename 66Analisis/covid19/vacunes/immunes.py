# -*- coding: utf8 -*-

import hashlib as h

"""
Vacunes en residencia
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""
        self.get_cohorts()
        self.export_files()

     
    def get_cohorts(self):
        """excloem si covid previ a 27/12"""
        u.printTime("cohort")
        self.pacients = []
        sql = """select residencia, perfil, dia_antics,dia_poblacio, (dia_antics/dia_poblacio)*100 from 
                        preduffa.SISAP_COVID_RES_WEB_RESUM 
                        where to_char(data,'YYYYMMDD')='20201227'"""
        for resi, perfil, dia, pob, perc in u.getAll(sql, 'redics'):
            self.pacients.append([resi, perfil, dia, pob, perc])
                   
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_recerca_vac_immunes"
        cols = ("residencia varchar(40)", "perfila varchar(10)", "immunitat int",  "poblacio int",  "perc_immune double")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pacients, tb, db)
                         
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final") 