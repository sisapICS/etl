# -*- coding: utf8 -*-

"""
Vacunes en residencia
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""
        self.get_tipus()
        self.get_simptomes()
        self.get_inicials()
        self.get_dbs()
        self.get_cohorts()
        self.export_files()


    def get_tipus(self):
        """tipus residencia"""
        u.printTime("tipus")
        self.codis = {}
        sql = """select codi,tipus 
                from dwsisap.residencies_cataleg 
                where tipus in (1,7,8)"""
        for codi, tipus in u.getAll(sql, 'exadata'):
            self.codis[codi] = tipus
    
    
    def get_simptomes(self):
        """simptomes al moment de pcr"""
        u.printTime("simptomes")
        self.simptomes = {}
        sql = """select hash,data, max(simptomes) 
        from preduffa.sisap_covid_pac_prv_raw 
        where prova like ('%PCR%') or prova like ('%Antigen%')
        group by hash, data"""
        for id, data, simptom in u.getAll(sql, 'redics'):
            self.simptomes[(id, data)] = simptom
            
    
    def get_inicials(self):
        """Agafem del cens de resis a dia 27/12"""
        u.printTime("inicial")
        self.inicials = {}
        sql = """select hash, data, perfil, residencia 
                from dwsisap.residencies_cens 
                where to_char(data, 'YYYYMMDD') >= '20201227'"""
        for id, data, perfil, resi in u.getAll(sql, 'exadata'):
            if resi in self.codis:
                if id in self.inicials:
                    dant = self.inicials[id]['inici']
                    if data < dant:
                        self.inicials[id] = {'inici':data, 'perfil': perfil, 'resi': resi}
                else:
                    self.inicials[id] = {'inici':data, 'perfil': perfil, 'resi': resi}
    
    def get_dbs(self):
        """Dades del dbs"""
        u.printTime("DBS")
        self.factors = {}
        self.in_dbs_true = {}
        sql = """select c_cip,PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    V_RCV_DATA, V_RCV_VALOR,
                     F_HTA_IECA_ARA2, F_HTA_CALCIOA, F_HTA_BETABLOQ, F_HTA_DIURETICS, F_HTA_ALFABLOQ, F_HTA_ALTRES, F_HTA_COMBINACIONS,
                    F_DIAB_ADO, F_DIAB_INSULINA, F_MPOC_ASMA,F_ANTIAGREGANTS_ANTICOA, F_AINE, F_ANALGESICS, 
                    F_ANTIDEPRESSIUS, F_ANSIO_HIPN, F_ANTIPSICOTICS, F_ANTIULCEROSOS, F_ANTIESP_URI, F_CORTIC_SISTEM,
                    F_ANTIEPILEPTICS,F_HIPOLIPEMIANTS, F_MHDA_VIH
                from DWSISAP.DBS"""
        for (cip,
             hta, dm1, dm2, mpoc,
             asma, bc, ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             rcv_dat, rcv_val,
             ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
             ado, insulina, f_mpoc, antiag, aine, analg,
             antidep, ansiol, antipsic, ulcer, anties, cortis,
             epil, hipol, mhda) in u.getAll(sql, 'exadata'):
            if cip in self.inicials:
                self.factors[(cip, "in_dbs")] = 1
                self.in_dbs_true[(cip)] = True
                if hta:
                    self.factors[(cip, "hta")] = hta
                if dm1:
                    self.factors[(cip, "dm")] = dm1
                if dm2:
                    self.factors[(cip, "dm")] = dm2
                if mpoc:
                    self.factors[(cip, "mpoc")] = mpoc
                if asma:
                    self.factors[(cip, "asma")] = asma
                if bc:
                    self.factors[(cip, "bc")] = bc
                if ci:
                    self.factors[(cip, "ci")] = ci
                if mcv:
                    self.factors[(cip, "mcv")] = mcv
                if ic:
                    self.factors[(cip, "ic")] = ic
                if acfa:
                    self.factors[(cip, "acfa")] = acfa
                if valv:
                    self.factors[(cip, "valv")] = valv
                if hepat:
                    self.factors[(cip, "hepat")] = hepat
                if vhb:
                    self.factors[(cip, "vhb")] = vhb
                if vhc:
                    self.factors[(cip, "vhc")] = vhc
                if neo:
                    self.factors[(cip, "neo")] = neo
                if mrc:
                    self.factors[(cip, "mrc")] = mrc
                if obes:
                    self.factors[(cip, "obes")] = obes
                if vih:
                    self.factors[(cip, "vih")] = vih
                if sida:
                    self.factors[(cip, "sida")] = sida
                if demencia:
                    self.factors[(cip, "demencia")] = demencia
                if artrosi:
                    self.factors[(cip, "artrosi")] = artrosi 
                if rcv_dat:
                    self.factors[(cip, "rcv_dat")] = rcv_dat
                    self.factors[(cip, "rcv_val")] = rcv_val
                if ieca:
                    self.factors[(cip, "ieca")] = ieca    
                if calcioa:
                    self.factors[(cip, "calcioa")] = calcioa
                if diur:
                    self.factors[(cip, "diur")] = diur
                if betb:
                    self.factors[(cip, "betb")] = betb
                if alfab:
                    self.factors[(cip, "alfab")] = alfab
                if hta_alt:
                    self.factors[(cip, "hta_alt")] = hta_alt
                if hta_comb:
                    self.factors[(cip, "hta_comb")] = hta_comb    
                if ado:
                    self.factors[(cip, "ado")] = ado
                if insulina:
                    self.factors[(cip, "insulina")] = insulina
                if f_mpoc:
                    self.factors[(cip, "f_mpoc")] = f_mpoc
                if antiag:
                    self.factors[(cip, "antiag")] = antiag
                if aine:
                    self.factors[(cip, "aine")] = aine
                if analg:
                    self.factors[(cip, "analg")] = analg 
                if antidep:
                    self.factors[(cip, "antidep")] = antidep
                if ansiol:
                    self.factors[(cip, "ansiol")] = ansiol
                if antipsic:
                    self.factors[(cip, "antipsic")] = antipsic
                if ulcer:
                    self.factors[(cip, "ulcer")] = ulcer
                if anties:
                    self.factors[(cip, "anties")] = anties    
                if cortis:
                    self.factors[(cip, "cortis")] = cortis
                if epil:
                    self.factors[(cip, "epil")] = epil
                if hipol:
                    self.factors[(cip, "hipol")] = hipol
                if mhda:
                    self.factors[(cip, "mhda")] = mhda
    
    def get_cohorts(self):
        """excloem si covid previ a 27/12"""
        u.printTime("cohort")
        self.residents = []
        self.treballadors = []
        sql = """select hash, sexe, data_naixement, rca_up, pdia_primer_positiu, ingres_primer, ingres_uci_primer, vac_dosi_1, vac_dosi_2, exitus_covid  
                    from dwsisap.dbc_metriques 
                    where (PDIA_PRIMER_POSITIU is null or to_char(PDIA_PRIMER_POSITIU, 'YYYYMMDD') >'20201227') and
                    (exitus_covid is null or to_char(exitus_covid, 'YYYYMMDD') >'20201227')"""
        for hash, sexe, naix, up, data_cas, ingres, uci, vac1, vac2, exitus in u.getAll(sql, 'exadata'):
            if hash in self.inicials:
                inici = self.inicials[hash]['inici']
                perfil = self.inicials[hash]['perfil']
                resi = self.inicials[hash]['resi']
                simptom = self.simptomes[(hash, data_cas)] if (hash, data_cas) in self.simptomes else None
                hta, dm, mpoc = None, None, None
                asma, bc, ci = None, None, None
                mcv, ic, acfa, valv = None, None, None, None
                hepat, vhb, vhc, neo, mrc = None, None, None, None, None
                obes, vih, sida = None, None, None
                demencia, artrosi = None, None
                medea = None
                in_dbs = 0
                rcv_dat, rcv_val = None, None
                ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb = None, None,None,None,None,None,None
                ado, insulina, f_mpoc, antiag, aine, analg = None,None,None,None,None,None
                antidep, ansiol, antipsic, ulcer, anties, cortis = None,None,None,None,None,None
                epil, hipol, mhda = None, None, None
                hcq, azitro, salbutamol = None, None, None
                if hash in self.in_dbs_true:
                    in_dbs = self.factors[(hash, "in_dbs")] if (hash, "in_dbs") in self.factors else 0
                    hta = self.factors[(hash, "hta")] if (hash, "hta") in self.factors else None
                    dm = self.factors[(hash, "dm")] if (hash, "dm") in self.factors else None
                    mpoc = self.factors[(hash, "mpoc")] if (hash, "mpoc") in self.factors else None
                    asma = self.factors[(hash, "asma")] if (hash, "asma") in self.factors else None
                    bc = self.factors[(hash, "bc")] if (hash, "bc") in self.factors else None
                    ci = self.factors[(hash, "ci")] if (hash, "ci") in self.factors else None
                    mcv = self.factors[(hash, "mcv")] if (hash, "mcv") in self.factors else None
                    ic = self.factors[(hash, "ic")] if (hash, "ic") in self.factors else None
                    acfa = self.factors[(hash, "acfa")] if (hash, "acfa") in self.factors else None
                    valv = self.factors[(hash, "valv")] if (hash, "valv") in self.factors else None
                    hepat = self.factors[(hash, "hepat")] if (hash, "hepat") in self.factors else None
                    vhb = self.factors[(hash, "vhb")] if (hash, "vhb") in self.factors else None
                    vhc = self.factors[(hash, "vhc")] if (hash, "vhc") in self.factors else None
                    neo = self.factors[(hash, "neo")] if (hash, "neo") in self.factors else None
                    mrc = self.factors[(hash, "mrc")] if (hash, "mrc") in self.factors else None
                    obes = self.factors[(hash, "obes")] if (hash, "obes") in self.factors else None
                    vih = self.factors[(hash, "vih")] if (hash, "vih") in self.factors else None
                    sida = self.factors[(hash, "sida")] if (hash, "sida") in self.factors else None
                    demencia = self.factors[(hash, "demencia")] if (hash, "demencia") in self.factors else None
                    artrosi = self.factors[(hash, "artrosi")] if (hash, "artrosi") in self.factors else None
                    rcv_dat = self.factors[(hash, "rcv_dat")] if (hash, "rcv_dat") in self.factors else None
                    rcv_val = self.factors[(hash, "rcv_val")] if (hash, "rcv_val") in self.factors else None
                    ieca = self.factors[(hash, "ieca")] if (hash, "ieca") in self.factors else None
                    calcioa = self.factors[(hash, "calcioa")] if (hash, "calcioa") in self.factors else None
                    betb = self.factors[(hash, "betb")] if (hash, "betb") in self.factors else None
                    diur = self.factors[(hash, "diur")] if (hash, "diur") in self.factors else None
                    alfab = self.factors[(hash, "alfab")] if (hash, "alfab") in self.factors else None
                    hta_alt = self.factors[(hash, "hta_alt")] if (hash, "hta_alt") in self.factors else None
                    hta_comb = self.factors[(hash, "hta_comb")] if (hash, "hta_comb") in self.factors else None
                    ado = self.factors[(hash, "ado")] if (hash, "ado") in self.factors else None
                    insulina = self.factors[(hash, "insulina")] if (hash, "insulina") in self.factors else None
                    f_mpoc = self.factors[(hash, "f_mpoc")] if (hash, "f_mpoc") in self.factors else None
                    antiag = self.factors[(hash, "antiag")] if (hash, "antiag") in self.factors else None
                    aine = self.factors[(hash, "aine")] if (hash, "aine") in self.factors else None
                    analg = self.factors[(hash, "analg")] if (hash, "analg") in self.factors else None
                    antidep = self.factors[(hash, "antidep")] if (hash, "antidep") in self.factors else None
                    ansiol = self.factors[(hash, "ansiol")] if (hash, "ansiol") in self.factors else None
                    antipsic = self.factors[(hash, "antipsic")] if (hash, "antipsic") in self.factors else None
                    ulcer = self.factors[(hash, "ulcer")] if (hash, "ulcer") in self.factors else None
                    anties = self.factors[(hash, "anties")] if (hash, "anties") in self.factors else None
                    cortis = self.factors[(hash, "cortis")] if (hash, "cortis") in self.factors else None
                    epil = self.factors[(hash, "epil")] if (hash, "epil") in self.factors else None
                    hipol = self.factors[(hash, "hipol")] if (hash, "hipol") in self.factors else None
                    mhda = self.factors[(hash, "mhda")] if (hash, "mhda") in self.factors else None
                if perfil == 'Resident':
                    self.residents.append([hash, naix, sexe, resi, up, inici, data_cas, simptom, ingres, uci, vac1, vac2, exitus,
                            in_dbs, hta, dm, mpoc,asma, bc, ci, mcv, ic, acfa, valv, hepat, vhb, vhc, neo, mrc, obes, vih, sida,
                             demencia, artrosi,
                             ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
                             ado, insulina, f_mpoc, antiag, aine, analg,
                             antidep, ansiol, antipsic, ulcer, anties, cortis,
                             epil, hipol, mhda])
                else:
                    self.treballadors.append([hash, naix, sexe, resi, up, inici, data_cas, simptom, ingres, uci, vac1, vac2, exitus,
                                            in_dbs, hta, dm, mpoc,asma, bc, ci, mcv, ic, acfa, valv, hepat, vhb, vhc, neo, mrc, obes, vih, sida,
                                     demencia, artrosi,
                                     ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
                                     ado, insulina, f_mpoc, antiag, aine, analg,
                                     antidep, ansiol, antipsic, ulcer, anties, cortis,
                                     epil, hipol, mhda])
                    

    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_recerca_vac_res_back"
        tb2 = "sisap_covid_recerca_vac_tre_back"
        cols = ("hash varchar(40)", "data_naix date", "sexe varchar(10)", "resi varchar(10)", "up varchar(10)", "inici date", "data_cas date", "simptomes varchar(30)", "ingres date", "uci date", "vac_1dosi date", "vac_2dosi date", "exitus date",
                "in_dbs int",
            "FR_hta date",  "FR_dm date", "FR_mpoc date", "FR_asma date", "FR_bc date", "FR_ci date", "FR_mcv date",
            "FR_ic date", "FR_acfa date", "FR_valv date", "FR_hepat date", "FR_vhb date", "FR_vhc date", "FR_neo date", "FR_mrc date", "FR_obes date",
            "FR_vih date", "FR_sida date", "FR_demencia date", "FR_artrosi date",
            "F_HTA_IECA_ARA2 varchar(500)", "F_HTA_CALCIOA varchar(500)","F_HTA_BETABLOQ varchar(500)","F_HTA_DIURETICS varchar(500)",
            "F_HTA_ALFABLOQ varchar(500)", "F_HTA_ALTRES varchar(500)", "F_HTA_COMBINACIONS varchar(500)",
             "F_DIAB_ADO varchar(500)", "F_DIAB_INSULINA varchar(500)", "F_MPOC_ASMA varchar(500)", 
             "F_ANTIAGREGANTS_ANTICOA varchar(500)", "F_AINE varchar(500)", "F_ANALGESICS varchar(500)",
             "F_ANTIDEPRESSIUS varchar(500)", "F_ANSIO_HIPN varchar(500)", "F_ANTIPISCOTICS varchar(500)",
             "F_ANTIULCEROSOS varchar(500)", "F_ANTIESP_URI varchar(500)", "F_CORTIC_SISTEM varchar(500)",
             "F_ANTIEPILEPTICS varchar(500)", "F_HIPOLIPEMIANTS varchar(500)", "F_MHDA_VIH varchar(500)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.residents, tb, db)
        
        cols = ("hash varchar(40)", "data_naix date", "sexe varchar(10)", "resi varchar(10)", "up varchar(10)", "inici date", "data_cas date", "simptomes varchar(30)",  "ingres date", "uci date", "vac_1dosi date", "vac_2dosi date", "exitus date",
        "in_dbs int",
            "FR_hta date", "FR_dm date", "FR_mpoc date", "FR_asma date", "FR_bc date", "FR_ci date", "FR_mcv date",
            "FR_ic date", "FR_acfa date", "FR_valv date", "FR_hepat date", "FR_vhb date", "FR_vhc date", "FR_neo date", "FR_mrc date", "FR_obes date",
            "FR_vih date", "FR_sida date", "FR_demencia date", "FR_artrosi date",
            "F_HTA_IECA_ARA2 varchar(500)", "F_HTA_CALCIOA varchar(500)","F_HTA_BETABLOQ varchar(500)","F_HTA_DIURETICS varchar(500)",
            "F_HTA_ALFABLOQ varchar(500)", "F_HTA_ALTRES varchar(500)", "F_HTA_COMBINACIONS varchar(500)",
             "F_DIAB_ADO varchar(500)", "F_DIAB_INSULINA varchar(500)", "F_MPOC_ASMA varchar(500)", 
             "F_ANTIAGREGANTS_ANTICOA varchar(500)", "F_AINE varchar(500)", "F_ANALGESICS varchar(500)",
             "F_ANTIDEPRESSIUS varchar(500)", "F_ANSIO_HIPN varchar(500)", "F_ANTIPISCOTICS varchar(500)",
             "F_ANTIULCEROSOS varchar(500)", "F_ANTIESP_URI varchar(500)", "F_CORTIC_SISTEM varchar(500)",
             "F_ANTIEPILEPTICS varchar(500)", "F_HIPOLIPEMIANTS varchar(500)", "F_MHDA_VIH varchar(500)")
        u.createTable(tb2, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.treballadors, tb2, db)
                        
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final") 