# -*- coding: utf8 -*-

import hashlib as h

"""
Gravetat en vacunades
"""

import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u


tb = "embarassos_gravetat"
tb2 = "embarassos_gravetat_controls_"
db = 'permanent'

class Embarassades(object):
    """."""

    def __init__(self):
        """."""
        self.get_controls()
        self.get_casos()
        self.export_data()
            
    def get_controls(self):
        """Casos sense embaràs per buscar controls"""
        u.printTime("controls")
        self.covid = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(set))))
        self.pacients = {}
        sql = """SELECT hash, edat, sexe, cas_data_pdia AS data_cas,
                    CASE WHEN vac_dosi_1 IS NULL THEN 'No vacunats'
                    WHEN CAS_DATA_PDIA < VAC_DOSI_1 + 14 THEN 'No vacunats'
                    WHEN cas_data_pdia < vac_dosi_2 + 14 THEN '1 dosis' 
                    ELSE '2 dosis' END AS vacuna,
                    ingres_primer,
                    ingres_uci_primer
                FROM  dwsisap.dbc_metriques WHERE cas_data_pdia IS NOT NULL
                and hash not in (select hash from dwsisap.dbc_embaras)
                and edat is not null"""
        for hash,edat, sexe, cas, vacuna, dingres, duci in u.getAll(sql, 'exadata'):
            self.pacients[hash] = {'cas': cas, 'dingres': dingres, 'duci': duci, 'edat': edat, 'vacuna': vacuna}
            edt = u.ageConverter(edat,5)
            edt = edat
            self.covid[cas][edt][sexe][vacuna].add(hash)

    def get_casos(self):
        """Agafem casos en embarassades"""
        u.printTime("embarassades")
        self.controls = []
        self.casos_v = []
        seleccionat = {}
        a = 0
        sql = """SELECT a.hash, edat, sexe, a.ini, a.fi, b.data_cas, b.ingres_primer, b.ingres_uci_primer, 
                    CASE WHEN b.ingres_primer BETWEEN a.ini AND (a.fi - 7) THEN 1 ELSE 0 END AS ingres,
                    CASE WHEN b.ingres_uci_primer BETWEEN a.ini AND (a.fi - 7) THEN 1 ELSE 0 END AS uci, b.vacuna
                from
                    (SELECT hash, inici AS ini, FINAL AS fi FROM dwsisap.dbc_embaras WHERE  inici >= DATE '2020-01-01') a
                LEFT join
                    (SELECT hash, edat, sexe, cas_data_pdia AS data_cas,
                    CASE WHEN vac_dosi_1 IS NULL THEN 'No vacunats'
                    WHEN CAS_DATA_PDIA < VAC_DOSI_1 + 14 THEN 'No vacunats'
                    WHEN cas_data_pdia < vac_dosi_2 + 14 THEN '1 dosis' 
                    ELSE '2 dosis' END AS vacuna,
                    ingres_primer,
                    ingres_uci_primer
                    FROM  dwsisap.dbc_metriques WHERE cas_data_pdia IS NOT NULL) b
                ON a.hash=b.hash
                WHERE ini >= DATE '2020-06-01'
                    AND CASE WHEN b.data_cas BETWEEN a.ini AND (a.fi - 7) THEN 1 ELSE 0 END  =1
                    and edat is not null"""
        for hash,edat, sexe, inici, fi, cas,  dingres, duci, ingres, uci, vacuna in u.getAll(sql, 'exadata'):
            control = 0
            conv = u.ageConverter(edat, 5)
            conv = edat
            di = cas - timedelta(days=5)
            df = cas + timedelta(days=5)
            for single_date in u.dateRange(di, df):
                if single_date in self.covid:
                    for ed in self.covid[single_date]:
                        if conv == ed:
                            for sex in self.covid[single_date][conv]:
                                if sexe == sex:
                                    for vac in self.covid[single_date][conv][sexe]:
                                        if vacuna == vac:
                                            for id in self.covid[single_date][conv][sexe][vacuna]:
                                                if control == 0 and id not in seleccionat:
                                                    control = 1
                                                    seleccionat[id] = True
                                                    c_cas =  self.pacients[id]['cas'] if id in self.pacients else None
                                                    c_ing =  self.pacients[id]['dingres'] if id in self.pacients else None
                                                    c_uci =  self.pacients[id]['duci'] if id in self.pacients else None
                                                    c_vacuna =  self.pacients[id]['vacuna'] if id in self.pacients else None
                                                    edt = self.pacients[id]['edat'] if id in self.pacients else None
                                                    self.controls.append([id, edt, ed,sex,c_cas, c_ing, c_uci,c_vacuna, hash])
            self.casos_v.append([hash, edat, conv, sexe, inici, fi, cas, vacuna, dingres, duci, ingres, uci,control])
          
            
    def export_data(self):
        """."""  
        u.printTime("export")
        cols = ("hash varchar(40)", "edat int", "grup_e varchar(20)","sexe varchar(10)","inici_e date", "fi_e date", "data_covid date", "vacuna varchar(20)","dingres date", "duci date", 
            "ingres int", "uci int", "control int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.casos_v, tb, db)
        
        cols = ("hash varchar(40)", "edat int",  "grup_e varchar(20)","sexe varchar(10)","data_covid date",  "dingres date", "duci date",
        "vacuna varchar(20)", "cas varchar(40)")
        u.createTable(tb2, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.controls, tb2, db)

                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Embarassades()
    
    u.printTime("Final")    