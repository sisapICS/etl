
# -*- coding: utf8 -*-

import hashlib as h

"""
Vacunes en residencia
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""
        self.get_inicials()
        self.export_files()


    def get_inicials(self):
        """Agafem del cens de resis a dia 27/12"""
        u.printTime("inicial")
        self.inicials = []
        sql = """select hash, perfil, residencia, count(*)
                from dwsisap.residencies_cens 
                where to_char(data, 'YYYYMMDD') >= '20201227' and perfil='Resident' group by hash, perfil, residencia"""
        for id,  perfil, resi, n in u.getAll(sql, 'exadata'):
            self.inicials.append([id,  perfil])

    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_recerca_vac_cens"

        cols = ("hash varchar(40)", "perfil varchar(40)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.inicials, tb, db)
                    
                        
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final") 