# -*- coding: utf8 -*-


import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

"""
10	(7) Menors de 70 anys amb condicions d'alt risc
"""


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""

        self.get_cohorts()
        self.export_files()

    
    def get_cohorts(self):
        """excloem si covid previ a 27/12"""
        u.printTime("cohort")
        self.pacients = []
        sql = """select a.hash, a.sexe, a.data_naixement, a.rca_up, es_atdom, ministeri_codi, pdia_primer_positiu, ingres_primer, to_char(ingres_primer, 'YYYYMMDD'),
        ingres_ultim, to_char(ingres_ultim, 'YYYYMMDD'), ingres_uci_primer,  to_char(ingres_uci_primer, 'YYYYMMDD'),
        ingres_uci_ultim, to_char(ingres_uci_ultim, 'YYYYMMDD'), vac_dosi_1, vac_dosi_2, exitus_covid
                FROM DWSISAP.DBC_vacuna a left join dwsisap.dbc_metriques  b on a.hash=b.hash
                where  ministeri_codi in (10) and ministeri_essencial IN ('Tractament citostàtic','Tractament immunosupressor') and
                (PDIA_PRIMER_POSITIU is null or to_char(PDIA_PRIMER_POSITIU, 'YYYYMMDD') >'20210209') and
                (exitus_covid is null or to_char(exitus_covid, 'YYYYMMDD') >'20210209') and 
                 (vac_dosi_1 is null or to_char(vac_dosi_1, 'YYYYMMDD')>'20210209')"""
        for hash, sexe, naix, up, atdom, tipus, data_cas, ingres_p, ingres_pchar, ingres_u, ingres_uchar, uci_p, uci_pchar, uci_u, uci_uchar, vac1, vac2, exitus in u.getAll(sql, 'exadata'):
            if ingres_pchar <= '20210301':
                if ingres_uchar > '20210301':
                    ingres_p=ingres_u
                else:
                    ingres_p = None
            if uci_pchar <= '20210301':
                if uci_uchar > '20210301':
                    uci_p=uci_u
                else:
                    uci_p = None
            self.pacients.append([hash, naix, sexe, up, atdom, tipus, data_cas, ingres_p, uci_p,  vac1, vac2, exitus])
                
                   
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_recerca_vac_grup7"""
        cols = ("hash varchar(40)", "data_naix date", "sexe varchar(10)",  "up varchar(10)", "atdom int", "tipus int", "data_cas date", 
        "ingres_primer date",  "uci_primer date", "vac_1dosi date", "vac_2dosi date", "exitus date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pacients, tb, db)
                         
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final") 