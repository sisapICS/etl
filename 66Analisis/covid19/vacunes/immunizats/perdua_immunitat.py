# -*- coding: utf8 -*-

import hashlib as h

"""
Immunitzats
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""
        self.get_tipus()
        self.get_residents()
        self.get_cip()
        self.get_dbs()
        self.get_dbs2020()
        self.get_cohorts()
        self.export_files()
  
    
    def get_tipus(self):
        """tipus residencia"""
        u.printTime("tipus")
        self.codis = {}
        sql = """select codi,tipus 
                from dwsisap.residencies_cataleg 
                where tipus in (1)"""
        for codi, tipus in u.getAll(sql, 'exadata'):
            self.codis[codi] = tipus
    
    
    def get_residents(self):
        """residents q hagi estat en algun moment al cens o tambe a inici 6a onada"""
        u.printTime("residents")
        
        self.residents, self.treballadors = {}, {}
        self.residents5, self.treballadors5 = {}, {}
        sql = """SELECT 
                    hash, to_char(data, 'YYYYMMDD'), perfil, residencia
                FROM 
                    dwsisap.RESIDENCIES_CENS 
                WHERE 
                    to_char(data, 'YYYYMMDD') >= '20201227'"""
        for hash, dat, perfil, resi in u.getAll(sql, 'exadata'):
            if resi in self.codis:
                if perfil == 'Resident':
                    self.residents[hash] = True
                    if dat == '20211101':
                        self.residents5[hash] = True
                if perfil == 'Personal':
                    self.treballadors[hash] = True
                    if dat == '20211101':
                        self.treballadors5[hash] = True
                    
    
    def get_cip(self):
        """hash to cip"""
        u.printTime("cip")
        self.hash_to_cip = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.hash_to_cip[hash] = cip
       
    def get_dbs(self):
        """Dades del dbs"""
        u.printTime("DBS")
        self.factors = {}
        self.in_dbs_true = {}
        sql = """select c_cip,c_gma_codi, c_gma_complexitat, PR_PCC_DATA, PR_MACA_DATA, PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    V_RCV_DATA, V_RCV_VALOR,
                     F_HTA_IECA_ARA2, F_HTA_CALCIOA, F_HTA_BETABLOQ, F_HTA_DIURETICS, F_HTA_ALFABLOQ, F_HTA_ALTRES, F_HTA_COMBINACIONS,
                    F_DIAB_ADO, F_DIAB_INSULINA, F_MPOC_ASMA,F_ANTIAGREGANTS_ANTICOA, F_AINE, F_ANALGESICS, 
                    F_ANTIDEPRESSIUS, F_ANSIO_HIPN, F_ANTIPSICOTICS, F_ANTIULCEROSOS, F_ANTIESP_URI, F_CORTIC_SISTEM,
                    F_ANTIEPILEPTICS,F_HIPOLIPEMIANTS, F_MHDA_VIH
                from DWSISAP.DBS"""
        for (cip, gma, gma1, pcc, maca,
             hta, dm1, dm2, mpoc,
             asma, bc, ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             rcv_dat, rcv_val,
             ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
             ado, insulina, f_mpoc, antiag, aine, analg,
             antidep, ansiol, antipsic, ulcer, anties, cortis,
             epil, hipol, mhda) in u.getAll(sql, 'exadata'):   
            self.factors[(cip, "in_dbs")] = 1
            self.in_dbs_true[(cip)] = True
            self.factors[(cip, "gma")] = gma
            self.factors[(cip, "gma1")] = gma1
            self.factors[(cip, "pcc")] = pcc
            self.factors[(cip, "maca")] = maca
            if hta:
                self.factors[(cip, "hta")] = hta
            if dm1:
                self.factors[(cip, "dm")] = dm1
            if dm2:
                self.factors[(cip, "dm")] = dm2
            if mpoc:
                self.factors[(cip, "mpoc")] = mpoc
            if asma:
                self.factors[(cip, "asma")] = asma
            if bc:
                self.factors[(cip, "bc")] = bc
            if ci:
                self.factors[(cip, "ci")] = ci
            if mcv:
                self.factors[(cip, "mcv")] = mcv
            if ic:
                self.factors[(cip, "ic")] = ic
            if acfa:
                self.factors[(cip, "acfa")] = acfa
            if valv:
                self.factors[(cip, "valv")] = valv
            if hepat:
                self.factors[(cip, "hepat")] = hepat
            if vhb:
                self.factors[(cip, "vhb")] = vhb
            if vhc:
                self.factors[(cip, "vhc")] = vhc
            if neo:
                self.factors[(cip, "neo")] = neo
            if mrc:
                self.factors[(cip, "mrc")] = mrc
            if obes:
                self.factors[(cip, "obes")] = obes
            if vih:
                self.factors[(cip, "vih")] = vih
            if sida:
                self.factors[(cip, "sida")] = sida
            if demencia:
                self.factors[(cip, "demencia")] = demencia
            if artrosi:
                self.factors[(cip, "artrosi")] = artrosi 
            if rcv_dat:
                self.factors[(cip, "rcv_dat")] = rcv_dat
                self.factors[(cip, "rcv_val")] = rcv_val
            if ieca:
                self.factors[(cip, "ieca")] = ieca    
            if calcioa:
                self.factors[(cip, "calcioa")] = calcioa
            if diur:
                self.factors[(cip, "diur")] = diur
            if betb:
                self.factors[(cip, "betb")] = betb
            if alfab:
                self.factors[(cip, "alfab")] = alfab
            if hta_alt:
               self.factors[(cip, "hta_alt")] = hta_alt
            if hta_comb:
                self.factors[(cip, "hta_comb")] = hta_comb    
            if ado:
                self.factors[(cip, "ado")] = ado
            if insulina:
                self.factors[(cip, "insulina")] = insulina
            if f_mpoc:
                self.factors[(cip, "f_mpoc")] = f_mpoc
            if antiag:
                self.factors[(cip, "antiag")] = antiag
            if aine:
                self.factors[(cip, "aine")] = aine
            if analg:
                self.factors[(cip, "analg")] = analg 
            if antidep:
                self.factors[(cip, "antidep")] = antidep
            if ansiol:
                self.factors[(cip, "ansiol")] = ansiol
            if antipsic:
                self.factors[(cip, "antipsic")] = antipsic
            if ulcer:
                self.factors[(cip, "ulcer")] = ulcer
            if anties:
                self.factors[(cip, "anties")] = anties    
            if cortis:
                self.factors[(cip, "cortis")] = cortis
            if epil:
                self.factors[(cip, "epil")] = epil
            if hipol:
                self.factors[(cip, "hipol")] = hipol
            if mhda:
                self.factors[(cip, "mhda")] = mhda
    
    def get_dbs2020(self):
        """Dades del dbs"""
        u.printTime("DBS 2020")
        
        SIDICS_DB = ("dbs", "x0002")
        sql = """select c_cip,c_gma_codi, c_gma_complexitat, PR_PCC_DATA, PR_MACA_DATA, PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    V_RCV_DATA, V_RCV_VALOR,
                     F_HTA_IECA_ARA2, F_HTA_CALCIOA, F_HTA_BETABLOQ, F_HTA_DIURETICS, F_HTA_ALFABLOQ, F_HTA_ALTRES, F_HTA_COMBINACIONS,
                    F_DIAB_ADO, F_DIAB_INSULINA, F_MPOC_ASMA,F_ANTIAGREGANTS_ANTICOA, F_AINE, F_ANALGESICS, 
                    F_ANTIDEPRESSIUS, F_ANSIO_HIPN, F_ANTIPSICOTICS, F_ANTIULCEROSOS, F_ANTIESP_URI, F_CORTIC_SISTEM,
                    F_ANTIEPILEPTICS,F_HIPOLIPEMIANTS, F_MHDA_VIH
                from dbs.dbs_2020"""
        for (cip1, gma, gma1, pcc, maca,
             hta, dm1, dm2, mpoc,
             asma, bc, ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             rcv_dat, rcv_val,
             ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
             ado, insulina, f_mpoc, antiag, aine, analg,
             antidep, ansiol, antipsic, ulcer, anties, cortis,
             epil, hipol, mhda) in u.getAll(sql, SIDICS_DB):
            if cip1 in self.hash_to_cip:
                cip2 = self.hash_to_cip[cip1]
                cip = h.sha1(cip2).hexdigest().upper()
                if cip not in self.in_dbs_true:
                    self.factors[(cip, "in_dbs")] = 1
                    self.in_dbs_true[(cip)] = True
                    self.factors[(cip, "gma")] = gma
                    self.factors[(cip, "gma1")] = gma1
                    self.factors[(cip, "pcc")] = pcc
                    self.factors[(cip, "maca")] = maca
                    if hta:
                        self.factors[(cip, "hta")] = hta
                    if dm1:
                        self.factors[(cip, "dm")] = dm1
                    if dm2:
                        self.factors[(cip, "dm")] = dm2
                    if mpoc:
                        self.factors[(cip, "mpoc")] = mpoc
                    if asma:
                        self.factors[(cip, "asma")] = asma
                    if bc:
                        self.factors[(cip, "bc")] = bc
                    if ci:
                        self.factors[(cip, "ci")] = ci
                    if mcv:
                        self.factors[(cip, "mcv")] = mcv
                    if ic:
                        self.factors[(cip, "ic")] = ic
                    if acfa:
                        self.factors[(cip, "acfa")] = acfa
                    if valv:
                        self.factors[(cip, "valv")] = valv
                    if hepat:
                        self.factors[(cip, "hepat")] = hepat
                    if vhb:
                        self.factors[(cip, "vhb")] = vhb
                    if vhc:
                        self.factors[(cip, "vhc")] = vhc
                    if neo:
                        self.factors[(cip, "neo")] = neo
                    if mrc:
                        self.factors[(cip, "mrc")] = mrc
                    if obes:
                        self.factors[(cip, "obes")] = obes
                    if vih:
                        self.factors[(cip, "vih")] = vih
                    if sida:
                        self.factors[(cip, "sida")] = sida
                    if demencia:
                        self.factors[(cip, "demencia")] = demencia
                    if artrosi:
                        self.factors[(cip, "artrosi")] = artrosi 
                    if rcv_dat:
                        self.factors[(cip, "rcv_dat")] = rcv_dat
                        self.factors[(cip, "rcv_val")] = rcv_val
                    if ieca:
                        self.factors[(cip, "ieca")] = ieca    
                    if calcioa:
                        self.factors[(cip, "calcioa")] = calcioa
                    if diur:
                        self.factors[(cip, "diur")] = diur
                    if betb:
                        self.factors[(cip, "betb")] = betb
                    if alfab:
                        self.factors[(cip, "alfab")] = alfab
                    if hta_alt:
                        self.factors[(cip, "hta_alt")] = hta_alt
                    if hta_comb:
                        self.factors[(cip, "hta_comb")] = hta_comb    
                    if ado:
                        self.factors[(cip, "ado")] = ado
                    if insulina:
                        self.factors[(cip, "insulina")] = insulina
                    if f_mpoc:
                        self.factors[(cip, "f_mpoc")] = f_mpoc
                    if antiag:
                        self.factors[(cip, "antiag")] = antiag
                    if aine:
                        self.factors[(cip, "aine")] = aine
                    if analg:
                        self.factors[(cip, "analg")] = analg 
                    if antidep:
                        self.factors[(cip, "antidep")] = antidep
                    if ansiol:
                        self.factors[(cip, "ansiol")] = ansiol
                    if antipsic:
                        self.factors[(cip, "antipsic")] = antipsic
                    if ulcer:
                        self.factors[(cip, "ulcer")] = ulcer
                    if anties:
                        self.factors[(cip, "anties")] = anties    
                    if cortis:
                        self.factors[(cip, "cortis")] = cortis
                    if epil:
                        self.factors[(cip, "epil")] = epil
                    if hipol:
                        self.factors[(cip, "hipol")] = hipol
                    if mhda:
                        self.factors[(cip, "mhda")] = mhda
    
    
    def get_cohorts(self):
        """agafem immunitzats i mirem les dates de les coses"""
        u.printTime("cohort")
        self.pacients = []
        sql = """SELECT 
                    a.hash, a.data_naixement, a.sexe, a.rca_up, a.RCA_MUNICIPI, ministeri_codi, ministeri_desc, vacuna_1_fabricant, vacuna_2_fabricant, immunitzat, IMMUNITZAT_DOSI, IMMUNITZAT_DATA, immunitzat_motiu, 
                    pdia_primer_positiu, ingres_primer, ingres_ultim, ingres_uci_primer, Ingres_uci_ultim, exitus_covid, es_atdom, ES_DEPENDENT_1, ES_DEPENDENT_2, ES_DEPENDENT_3, es_cens_salut,
                    vacuna_1_data, vacuna_2_data, vacuna_3_data, cas_reinfeccio
                FROM 
                    dwsisap.dbc_vacuna a
                left JOIN 
                    dwsisap.DBC_METRIQUES b
                ON 
                    a.hash=b.HASH"""
        for hash, naix, sexe, up, municipi, mcodi, mdesc, fabricant1, fabricant2, immunitzat, dosi, immunitzat_data, motiu, data_cas, ingres1, ingres2, uci1, uci2, exitus, atdom, dependent1, dependent2, dependent3, salut, vac1, vac2, vac3, reinfeccio in u.getAll(sql, 'exadata'):
            resident = 1 if hash in self.residents else 0
            treballador = 1 if hash in self.treballadors else 0
            resident5 = 1 if hash in self.residents5 else 0
            treballador5 = 1 if hash in self.treballadors5 else 0
            try:
                cas_previ = 1 if data_cas<=immunitzat_data else 0
            except TypeError:
                cas_previ=0
            hta, dm, mpoc = None, None, None
            asma, bc, ci = None, None, None
            mcv, ic, acfa, valv = None, None, None, None
            hepat, vhb, vhc, neo, mrc = None, None, None, None, None
            obes, vih, sida = None, None, None
            demencia, artrosi = None, None
            medea = None
            in_dbs = 0
            rcv_dat, rcv_val = None, None
            ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb = None, None,None,None,None,None,None
            ado, insulina, f_mpoc, antiag, aine, analg = None,None,None,None,None,None
            antidep, ansiol, antipsic, ulcer, anties, cortis = None,None,None,None,None,None
            epil, hipol, mhda = None, None, None
            hcq, azitro, salbutamol = None, None, None
            if hash in self.in_dbs_true:
                in_dbs = self.factors[(hash, "in_dbs")] if (hash, "in_dbs") in self.factors else 0
                gma = self.factors[(hash, "gma")] if (hash, "gma") in self.factors else None
                gma1 = self.factors[(hash, "gma1")] if (hash, "gma1") in self.factors else None
                pcc = self.factors[(hash, "pcc")] if (hash, "pcc") in self.factors else None
                maca = self.factors[(hash, "maca")] if (hash, "maca") in self.factors else None
                hta = self.factors[(hash, "hta")] if (hash, "hta") in self.factors else None
                dm = self.factors[(hash, "dm")] if (hash, "dm") in self.factors else None
                mpoc = self.factors[(hash, "mpoc")] if (hash, "mpoc") in self.factors else None
                asma = self.factors[(hash, "asma")] if (hash, "asma") in self.factors else None
                bc = self.factors[(hash, "bc")] if (hash, "bc") in self.factors else None
                ci = self.factors[(hash, "ci")] if (hash, "ci") in self.factors else None
                mcv = self.factors[(hash, "mcv")] if (hash, "mcv") in self.factors else None
                ic = self.factors[(hash, "ic")] if (hash, "ic") in self.factors else None
                acfa = self.factors[(hash, "acfa")] if (hash, "acfa") in self.factors else None
                valv = self.factors[(hash, "valv")] if (hash, "valv") in self.factors else None
                hepat = self.factors[(hash, "hepat")] if (hash, "hepat") in self.factors else None
                vhb = self.factors[(hash, "vhb")] if (hash, "vhb") in self.factors else None
                vhc = self.factors[(hash, "vhc")] if (hash, "vhc") in self.factors else None
                neo = self.factors[(hash, "neo")] if (hash, "neo") in self.factors else None
                mrc = self.factors[(hash, "mrc")] if (hash, "mrc") in self.factors else None
                obes = self.factors[(hash, "obes")] if (hash, "obes") in self.factors else None
                vih = self.factors[(hash, "vih")] if (hash, "vih") in self.factors else None
                sida = self.factors[(hash, "sida")] if (hash, "sida") in self.factors else None
                demencia = self.factors[(hash, "demencia")] if (hash, "demencia") in self.factors else None
                artrosi = self.factors[(hash, "artrosi")] if (hash, "artrosi") in self.factors else None
                rcv_dat = self.factors[(hash, "rcv_dat")] if (hash, "rcv_dat") in self.factors else None
                rcv_val = self.factors[(hash, "rcv_val")] if (hash, "rcv_val") in self.factors else None
                ieca = self.factors[(hash, "ieca")] if (hash, "ieca") in self.factors else None
                calcioa = self.factors[(hash, "calcioa")] if (hash, "calcioa") in self.factors else None
                betb = self.factors[(hash, "betb")] if (hash, "betb") in self.factors else None
                diur = self.factors[(hash, "diur")] if (hash, "diur") in self.factors else None
                alfab = self.factors[(hash, "alfab")] if (hash, "alfab") in self.factors else None
                hta_alt = self.factors[(hash, "hta_alt")] if (hash, "hta_alt") in self.factors else None
                hta_comb = self.factors[(hash, "hta_comb")] if (hash, "hta_comb") in self.factors else None
                ado = self.factors[(hash, "ado")] if (hash, "ado") in self.factors else None
                insulina = self.factors[(hash, "insulina")] if (hash, "insulina") in self.factors else None
                f_mpoc = self.factors[(hash, "f_mpoc")] if (hash, "f_mpoc") in self.factors else None
                antiag = self.factors[(hash, "antiag")] if (hash, "antiag") in self.factors else None
                aine = self.factors[(hash, "aine")] if (hash, "aine") in self.factors else None
                analg = self.factors[(hash, "analg")] if (hash, "analg") in self.factors else None
                antidep = self.factors[(hash, "antidep")] if (hash, "antidep") in self.factors else None
                ansiol = self.factors[(hash, "ansiol")] if (hash, "ansiol") in self.factors else None
                antipsic = self.factors[(hash, "antipsic")] if (hash, "antipsic") in self.factors else None
                ulcer = self.factors[(hash, "ulcer")] if (hash, "ulcer") in self.factors else None
                anties = self.factors[(hash, "anties")] if (hash, "anties") in self.factors else None
                cortis = self.factors[(hash, "cortis")] if (hash, "cortis") in self.factors else None
                epil = self.factors[(hash, "epil")] if (hash, "epil") in self.factors else None
                hipol = self.factors[(hash, "hipol")] if (hash, "hipol") in self.factors else None
                mhda = self.factors[(hash, "mhda")] if (hash, "mhda") in self.factors else None
            self.pacients.append([hash, naix, sexe,  up, municipi, resident, resident5, treballador, treballador5, salut, atdom, dependent1, dependent2, dependent3, 
            gma, gma1, pcc, maca, immunitzat, cas_previ, immunitzat_data, mcodi, mdesc, vac1, fabricant1, vac2, fabricant2, dosi, motiu, vac3, data_cas, reinfeccio, ingres1, ingres2, uci1, uci2, exitus,
                    in_dbs, hta, dm, mpoc,asma, bc, ci, mcv, ic, acfa, valv, hepat, vhb, vhc, neo, mrc, obes, vih, sida,
                     demencia, artrosi,
                     ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
                     ado, insulina, f_mpoc, antiag, aine, analg,
                     antidep, ansiol, antipsic, ulcer, anties, cortis,
                     epil, hipol, mhda])                  

    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_recerca_immunitzats"
        cols = ("hash varchar(40)", "data_naix date", "sexe varchar(10)",  "up varchar(10)", "municipi varchar(10)", "resident int", "resident_6a int", "personal_resi int", "personal_resi_6a int", "es_cens_salut int",
            "es_atdom int", "es_dependent1 int", "es_dependent2 int", "es_dependent3 int", "gma_codi varchar(10)", "gma_compexitat double", "pcc date", "maca date", "immunitzat int", "cas_previ int", "immunitzat_data date", 
                "ministeri_codi varchar(10)", "ministeri_desc varchar(300)","vacuna_1_data date", "fabricant_dosi1 varchar(100)", "vacuna_2_data date", "fabricant_dosi2 varchar(100)", "immunitzat_dosi int", "immunitzat_motiu varchar(100)", "vacuna_3_data date", "pdia_primer_positiu date",
                 "cas_reinfeccio date", "ingres_primer date", "ingres_ultim date", "ingres_uci_primer date", "ingres_uci_ulltim date","exitus_covid date",
                "in_dbs int",
            "FR_hta date",  "FR_dm date", "FR_mpoc date", "FR_asma date", "FR_bc date", "FR_ci date", "FR_mcv date",
            "FR_ic date", "FR_acfa date", "FR_valv date", "FR_hepat date", "FR_vhb date", "FR_vhc date", "FR_neo date", "FR_mrc date", "FR_obes date",
            "FR_vih date", "FR_sida date", "FR_demencia date", "FR_artrosi date",
            "F_HTA_IECA_ARA2 varchar(500)", "F_HTA_CALCIOA varchar(500)","F_HTA_BETABLOQ varchar(500)","F_HTA_DIURETICS varchar(500)",
            "F_HTA_ALFABLOQ varchar(500)", "F_HTA_ALTRES varchar(500)", "F_HTA_COMBINACIONS varchar(500)",
             "F_DIAB_ADO varchar(500)", "F_DIAB_INSULINA varchar(500)", "F_MPOC_ASMA varchar(500)", 
             "F_ANTIAGREGANTS_ANTICOA varchar(500)", "F_AINE varchar(500)", "F_ANALGESICS varchar(500)",
             "F_ANTIDEPRESSIUS varchar(500)", "F_ANSIO_HIPN varchar(500)", "F_ANTIPISCOTICS varchar(500)",
             "F_ANTIULCEROSOS varchar(500)", "F_ANTIESP_URI varchar(500)", "F_CORTIC_SISTEM varchar(500)",
             "F_ANTIEPILEPTICS varchar(500)", "F_HIPOLIPEMIANTS varchar(500)", "F_MHDA_VIH varchar(500)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pacients, tb, db)

                        
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final") 