# -*- coding: utf8 -*-

import hashlib as h

"""
Proves (usat per paper vacunes i paper immunotzats)
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""
        self.upload = []
        #self.get_sanitaris()
        #self.get_treballadors()
        #self.get_residents()
        #self.get_immunitzats()
        #self.get_astra()
        #self.get_proves()
        self.get_masks()
        self.get_proves2()
        self.export_files()
 
    def get_sanitaris(self):
        """sanitaris paper vacunes"""
        u.printTime("sanitaris")
        self.sanitaris = {}
        sql = """select hash, data_cas
                from sisap_covid_recerca_vac_sanitari5
                where in_dbs=1"""
        for hash, cas in u.getAll(sql, db):
            self.sanitaris[(hash)] = True
    
    def get_treballadors(self):
        """staff paper vacunes"""
        u.printTime("staff")
        self.staff = {}
        sql = """select hash, data_cas
                from sisap_covid_recerca_vac_tre_ger5
                where in_dbs=1"""
        for hash, cas in u.getAll(sql, db):
            self.staff[(hash)] = True
    
    def get_residents(self):
        """residents paper vacunes"""
        u.printTime("residents")
        self.residents = {}
        sql = """select hash, data_cas
                from sisap_covid_recerca_vac_res_ger5
                where in_dbs=1"""
        for hash, cas in u.getAll(sql, db):
            self.residents[(hash)] = True
    
    def get_immunitzats(self):
        """immunitzats"""
        u.printTime("residents")
        self.immunes = {}
        sql = """select hash
                from permanent.sisap_covid_recerca_immunitzats 
                where resident=1 or personal_resi=1  or es_cens_salut=1"""
        for hash, in u.getAll(sql, db):
            self.immunes[(hash)] = True
    
    def get_astra(self):
        """astra heterolog"""
        u.printTime("residents")
        self.astra = {}
        sql = """select hash
                from permanent.sisap_covid_recerca_astra"""
        for hash, in u.getAll(sql, db):
            self.astra[(hash)] = True
    
    def get_masks(self):
        """proves mascaretes 21-22"""
        u.printTime("masks")
        self.masks = {}
        sql = """select hash
                from permanent.maskoles_20_21"""
        for hash, in u.getAll(sql, db):
            self.masks[(hash)] = True
    
    def get_proves(self):
        """proves"""
        u.printTime("simptomes")

        sql = """select hash,data, prova, resultat_des, motiu, entorn,simptomes
                        from preduffa.sisap_covid_pac_prv_raw 
        where (prova like ('%PCR%') or prova like ('%Antigen%')) and to_char(data, 'YYYYMMDD')>'20201226'"""
        for id, data, prova, res, motiu, entorn, simpt in u.getAll(sql, 'redics'):
            if id in self.sanitaris:
                self.upload.append([id, 'sanitari', data, prova, res, motiu, entorn, simpt])
            if id in self.staff:
                self.upload.append([id, 'treballadors', data, prova, res, motiu, entorn, simpt])
            if id in self.residents:
                self.upload.append([id, 'residents', data, prova, res, motiu, entorn, simpt])
    
    def get_proves2(self):
        """proves"""
        u.printTime("proves")
        sql = """select hash,data, prova, resultat_des, motiu, entorn,simptomes
                        from preduffa.sisap_covid_pac_prv_raw 
        where (prova like ('%PCR%') or prova like ('%Antigen%')) and to_char(data, 'YYYYMMDD')>'20200913' """
        for id, data, prova, res, motiu, entorn, simpt in u.getAll(sql, 'redics'):
            if id in self.masks:
                self.upload.append([id, data, prova, res, motiu, entorn, simpt])
    
    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_recerca_masks20_21_proves"
        cols = ("hash varchar(40)", "data_prova date", "tipus_prova varchar(100)", "resultat varchar(100)", "motiu varchar(300)", "entorn varchar(300)", "simptomes varchar(40)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db) 

if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final")   