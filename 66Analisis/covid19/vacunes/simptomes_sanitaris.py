# -*- coding: utf8 -*-

import hashlib as h

"""
Vacunes en residencia
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""
        self.get_simptomes()
        self.get_sanitaris()
        self.export_files()
 
    def get_simptomes(self):
        """simptomes al moment de pcr"""
        u.printTime("simptomes")
        self.simptomes = {}
        sql = """select hash,data, max(simptomes) 
        from preduffa.sisap_covid_pac_prv_raw 
        where prova like ('%PCR%') or prova like ('%Antigen%')
        group by hash, data"""
        for id, data, simptom in u.getAll(sql, 'redics'):
            self.simptomes[(id, data)] = simptom

    def get_sanitaris(self):
        """sanitaris"""
        self.upload = []
        sql = """select hash, data_cas
                from sisap_covid_recerca_vac_sanitari
                where in_dbs=1"""
        for hash, cas in u.getAll(sql, db):
            simpt = self.simptomes[(hash, cas)] if (hash, cas) in self.simptomes else None
            self.upload.append([hash, cas, simpt])
    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_recerca_vac_sanitari_simpt"
        cols = ("hash varchar(40)",   "data_cas date",  "simptomes varcchar(40)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db) 

if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final")         
            