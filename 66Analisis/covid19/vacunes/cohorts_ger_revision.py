# -*- coding: utf8 -*-

import hashlib as h

"""
Vacunes en residencia. El mes fàcil és passar una copia de dbc_metriques a permanent i actualitzar les taules orginials acabades en 5 fent previament un backup
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""
        self.get_cohorts()
        self.export_files()

  
    def get_cohorts(self):
        """excloem si covid previ a 27/12"""
        u.printTime("cohort")
        self.residents = []
        sql = """select hash, sexe, data_naixement, rca_up, pdia_primer_positiu, ingres_primer, ingres_uci_primer, ingres_ultim, ingres_uci_ultim, vac_dosi_1, vac_dosi_2, exitus_covid  
                    from dwsisap.dbc_metriques"""
        for hash, sexe, naix, up, data_cas, ingres, uci, ingres2, uci2, vac1, vac2, exitus in u.getAll(sql, 'exadata'):
            self.residents.append([hash, naix, sexe, up, data_cas, ingres, ingres2, uci, uci2, vac1, vac2, exitus])

    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_recerca_vac_metriques"
        cols = ("hash varchar(40)", "data_naix date", "sexe varchar(10)", "up varchar(10)", "data_cas date",  "ingres date", "ingres_ultim date", "uci date", "uci_ultim date", "vac_1dosi date", "vac_2dosi date", "exitus date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.residents, tb, db)
                                
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final") 