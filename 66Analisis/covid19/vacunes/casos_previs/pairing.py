# -*- coding: utf8 -*-

import hashlib as h

"""
Astra, pauta homologa vs heterologa
"""

import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u
from random import shuffle


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""
        self.get_cohort1()
        self.get_aparellament()
        self.export_files()

    def get_cohort1(self):
        """cohort 2 dosis"""
        u.printTime("cohort 2 dosis")
        self.controls = c.defaultdict(set)
        sql = """select hash, edatfinal, sexe, up, vac_2_data from sisap_covid_recerca_previs where cohort='2dosis' and edatfinal>11"""
        for hash, edat, sexe, up, vac2 in u.getAll(sql, db):
            key = str(edat) + '-' + sexe + '-' + up + '-' + str(vac2)
            self.controls[key].add(hash)
            
    def get_aparellament(self):
        u.printTime("cas_dosi")
        self.casos = []
        
        sql = """select hash, edatfinal, sexe, up, vac_1_data from sisap_covid_recerca_previs where cohort='cas_dosi' and edatfinal>11"""
        for hash, edat, sexe, up, vac in u.getAll(sql, db):
            te_control = 0
            control_selected= 'No control'
            edati = int(edat) - 2
            edatf = int(edat) + 2
            dat_i= vac - timedelta(days=2)
            dat_f= vac + timedelta(days=3)
            for ed in range(edati, edatf):
                for dat in u.dateRange(dat_i, dat_f):
                    key = str(ed) + '-' + sexe + '-' + up + '-' + str(dat)
                    if te_control == 0:
                        try:
                            cont1 = self.controls[key]
                        except KeyError:
                            continue
                        cont = list(cont1)
                        shuffle(cont)
                        try:
                            control_selected = cont[0]
                            self.controls[key].remove(control_selected)
                            te_control = 1
                            self.casos.append([hash, control_selected, te_control])
                        except IndexError:
                            ok=1                        
            if te_control == 0:
                self.casos.append([hash, control_selected, te_control])
            
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "pairing_proves"
        
        cols = ("hash varchar(40)", 
				"control varchar(40)", "te_control int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.casos, tb, db)
        
        
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final")