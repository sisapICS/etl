# -*- coding: utf8 -*-

import hashlib as h

"""
Cas dosi vs 2 dosis
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""
        self.get_pacients()
        self.get_hospi()
        self.export_files()
        
    def get_pacients(self):
        """.agafem els que formen paert estudi"""
        u.printTime("pacients")
        self.pacients = {}
        
        sql = """select hash, cohort from sisap_covid_recerca_previs"""
        for hash, cohort in u.getAll(sql, 'permanent'):
            self.pacients[hash] = cohort
    
    def get_hospi(self):
        """agafem hx i ucis"""
        u.printTime("hx")
        self.upload = []
        sql = """select substr(cip, 0, 13), trunc(data_inici_hosp), trunc(DATA_ALTA),
                      trunc(data_ini_crit) , DATA_FI_CRITIC 
               from dwcatsalut.hospitalitzacio_covid19 a, 
                    dwcatsalut.d_tipus_up b 
               where a.centre = b.codi_up and 
                     b.tipus_up = 'Hospitals' and 
                     (covid_ini = 1 or covid_alta = 1 or 
                      covid_crit = 1 or covid_fi_critic = 1) and 
                     (l_llit_ini = 1 or l_llit_alta = 1) and 
                     (t_llit_ini in (1, 2, 4) or t_llit_alta in (1, 2, 4)) and
                     data_inici_hosp >DATE '2021-08-31'"""
        for cip, ini_hx, alta, ini_uci, fi_uci in u.getAll(sql, 'exadata'):
            try:
                hash = h.sha1(cip).hexdigest().upper()
            except:
                hash = None
            if hash in self.pacients:
                cohort = self.pacients[hash]
                self.upload.append([hash, cohort, ini_hx, alta, ini_uci, fi_uci])
                
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_recerca_previs_hx"
        cols = ("hash varchar(40)", "cohort varchar(100)", "data_inici_hosp date", "data_fi_hosp date", "data_inici_uci date", "data_fi_uci date" )
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)

                        
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Fi")