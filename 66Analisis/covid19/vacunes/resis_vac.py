# -*- coding: utf8 -*-

"""
Vacunes en residencia
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'redics'


class vacunes(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_data_calcul()
        self.get_inici()
        self.get_vacunats()
        self.get_casos()
        self.export()
        self.get_exitus()
    
    def get_data_calcul(self):
        """data maxim de la taula"""
        sql = "select max(data) from sisap_covid_res_web_resum"
        for dat, in u.getAll(sql, db):
            self.calcul_avui = dat 
            
    def get_inici(self):
        """Agafem les que fa més de 12 dies""" 
        self.resis_i = {}
        sql = """select residencia,min(data) 
                from preduffa.sisap_covid_res_web_resum 
                where dia_vacunats_1>0 and perfil='Resident' 
                group by residencia"""
        for resi, dat in u.getAll(sql, db):
            dies = u.daysBetween(dat, self.calcul_avui)
            ini12 = 1 if dies > 12 else 0
            self.resis_i[str(resi)] = ini12
            
    def get_vacunats(self):
        """cobertures"""
        self.resis_vacunades = {}
        sql = """select data, residencia, dia_vacunats_1, dia_poblacio 
                from preduffa.SISAP_COVID_RES_WEB_RESUM 
                join preduffa.sisap_covid_cat_residencia on residencia=cod where tipus=1 and perfil='Resident'
                and to_char(data, 'DD-MM-YYYY')='{0}'""". format(self.calcul_avui.strftime("%d-%m-%Y"))
        for data, resi, vacunats, pob in u.getAll(sql, db):
            cobertura = (float(vacunats)/float(pob))*100
            dies = self.resis_i[resi] if resi in self.resis_i else 0
            tipus = 1 if dies == 1 and cobertura>70 else 0
            self.resis_vacunades[resi] = tipus  

    def get_casos(self):
        """casos"""
        self.casos = c.Counter()
        sql = """select data,residencia,sum(diaris) from 
                preduffa.SISAP_COVID_RES_CASOS a join preduffa.sisap_covid_cat_residencia b on residencia=cod 
                where tipus=1 and estat='Confirmat' and (a.origen like ('%PCR%') or a.origen='Test antigen') and to_char(data,'YYYYMMDD')>'20210101'
                    group by data, residencia"""
        for data, resi, casos in u.getAll(sql, db):
            tipus = self.resis_vacunades[resi]
            self.casos[(data, tipus, 'casos')] += casos
          
    def export(self):
        """export dades"""
        upload = []
        for (dat, vac, tip),n in self.casos.items():
            upload.append([dat, vac,  n])
        file = u.tempFolder + 'resis_vac.txt'
        u.writeCSV(file, upload, sep='@')
        
        
    def get_exitus(self):
        """casos"""
        self.morts = c.Counter()
        sql =  """select data, residencia, sum(setmana_exitus), sum(setmana_poblacio)
                from preduffa.SISAP_COVID_RES_WEB_RESUM 
                join preduffa.sisap_covid_cat_residencia on residencia=cod where tipus=1 and perfil='Resident'
                group by data, residencia"""
        for data, resi, m, pob in u.getAll(sql, db):
            tipus = self.resis_vacunades[resi]
            self.morts[(data, tipus, 'pob')] += pob
            self.morts[(data, tipus, 'mort')] += m

        upload = []
        for (dat, vac, tip),n in self.morts.items():
            if tip == 'pob':
                mort = self.morts[(dat, vac, 'mort')]
                upload.append([dat, vac, mort, n])
        file = u.tempFolder + 'morts_res.txt'
        u.writeCSV(file, upload, sep='@')
            
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final")  