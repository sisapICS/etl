# -*- coding: utf8 -*-

import hashlib as h

"""
Pneumònies
"""

import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u


tb = "vacunats_gravetat_casos_all"
tb2 = "vacunats_gravetat_controls_all"
db = 'permanent'

class Vacunats(object):
    """."""

    def __init__(self):
        """."""
        self.get_resis()
        self.get_casos()
        self.get_vacunats()
        self.export_data()
            
    def get_resis(self):
        """Excloem pacients de resis"""
        u.printTime("inicial")
        self.resis = {}
        sql = """select hash, data, perfil, residencia 
                from dwsisap.residencies_cens 
                where to_char(data, 'YYYYMMDD') >= '20201227' and perfil='Resident'"""
        for id, data, perfil, resi in u.getAll(sql, 'exadata'):
            self.resis[id] = True
    
    def get_casos(self):
        """Casos sense vacuna per buscar controls"""
        u.printTime("cohort")
        self.covid = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(set)))
        self.pacients = {}
        sql = """select  hash, edat,sexe, cas_data_pdia, ingres_primer, ingres_ultim, ingres_uci_primer, ingres_uci_ultim, exitus_covid 
                from dwsisap.dbc_metriques where vac_dosi_1 is 
                null and cas_data_pdia is not null and edat is not null"""
        for hash,edat, sexe, cas, ingres1, ingres2, uci1, uci2, exitus in u.getAll(sql, 'exadata'):
            if hash not in self.resis:
                self.pacients[hash] = {'cas': cas, 'ingres_p': ingres1, 'ingres_u': ingres2, 'edat': edat, 'uci1':uci1,'uci2':uci2,'exitus':exitus}
                edt = u.ageConverter(edat,5)
                self.covid[cas][edt][sexe].add(hash)
        
    def get_vacunats(self):
        """Agafem casos en vacunats"""
        u.printTime("vacunes")
        self.controls = []
        self.casos_v = []
        seleccionat = {}
        a = 0
        sql = """select hash, edat,sexe, cas_data_pdia, ingres_primer, ingres_ultim, vac_dosi_1, cas_data_pdia - vac_dosi_1, ingres_uci_primer, ingres_uci_ultim, exitus_covid  from 
                dwsisap.dbc_metriques 
                where vac_dosi_1 is not null and (cas_data_pdia>vac_dosi_1 and cas_data_pdia is not null) and edat is not null"""
        for hash,edat, sexe, cas, ingres1, ingres2, vac, dies, uci1, uci2, exitus in u.getAll(sql, 'exadata'):
            if hash not in self.resis:
                control = 0
                conv = u.ageConverter(edat, 5)
                di = cas - timedelta(days=5)
                df = cas + timedelta(days=5)
                for single_date in u.dateRange(di, df):
					if single_date in self.covid:
						for ed in self.covid[single_date]:
							if conv == ed:
								for sex in self.covid[single_date][conv]:
									if sexe == sex:
										for id in self.covid[single_date][conv][sexe]:
											if control == 0 and id not in seleccionat:
												control = 1
												seleccionat[id] = True
												c_cas =  self.pacients[id]['cas'] if id in self.pacients else None
												c_ing1 =  self.pacients[id]['ingres_p'] if id in self.pacients else None
												c_ing2 =  self.pacients[id]['ingres_u'] if id in self.pacients else None
												u1 =  self.pacients[id]['uci1'] if id in self.pacients else None
												u2 =  self.pacients[id]['uci2'] if id in self.pacients else None
												ex =  self.pacients[id]['exitus'] if id in self.pacients else None
												edt = self.pacients[id]['edat'] if id in self.pacients else None
												self.controls.append([id, edt, ed,sex,c_cas, c_ing1, c_ing2,u1,u2,ex, hash])
                self.casos_v.append([hash, edat, conv, sexe, cas, vac, dies, ingres1, ingres2,uci1, uci2, exitus, control])
          
            
    def export_data(self):
        """."""  
        u.printTime("export")
        cols = ("hash varchar(40)", "edat int", "grup_e varchar(20)","sexe varchar(10)","data_covid date", "vac_1dosi date", "dies int","ingres_p date", "ingres_u date", 
            "uci_p date", "uci_u date", "exitus date", "control int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.casos_v, tb, db)
        
        cols = ("hash varchar(40)", "edat int",  "grup_e varchar(20)","sexe varchar(10)","data_covid date",  "ingres_p date", "ingres_u date",
        "uci_p date", "uci_u date", "exitus date", "cas varchar(40)")
        u.createTable(tb2, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.controls, tb2, db)

                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Vacunats()
    
    u.printTime("Final")    