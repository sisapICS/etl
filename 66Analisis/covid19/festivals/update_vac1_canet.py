# -*- coding: utf8 -*-

"""
actualització vacunes Canet
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import hashlib as h

import sisapUtils as u
from random import shuffle


db = 'permanent'
class cruilla(object):
    """."""

    def __init__(self):
        """."""
        self.get_casos()
        self.get_metrica()
        self.export_files()
        
        
    def get_casos(self):
        """Obtenim els hash de casos i cobntrols"""
        u.printTime("casos")
        self.casos = {}
        
        sql = """select 
                    hash,  control
                from 
                    canet_casos
                where 
                    te_control=1"""
        for hash, control in u.getAll(sql, db):
            self.casos[hash] = True
            self.casos[control] = True
                    
    def get_metrica(self):
        """*"""
        u.printTime("metriques")
        self.upload = []
            
        sql = """SELECT 
                    hash, vacuna_1_data, to_char(cas_data, 'YYYYMMDD')
                FROM 
                    dwsisap.DBC_VACUNA"""
        for hash, cas_data, cas_data_txt in u.getAll(sql, 'exadata'):
            if hash in self.casos:
                self.upload.append([hash, cas_data])
                    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "update_canet_vac1_ctl"
        cols = ("hash varchar(40)", 
				 "cas_data date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
            
if __name__ == '__main__':
    u.printTime("Inici")
     
    cruilla()
    
    u.printTime("Final") 