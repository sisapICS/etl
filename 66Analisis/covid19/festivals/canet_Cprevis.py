# -*- coding: utf8 -*-

"""
Festival Canet, recuperar els casos previs: Canet dia = 03072021
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import hashlib as h

import sisapUtils as u
from random import shuffle


db = 'permanent'
class cruilla(object):
    """."""

    def __init__(self):
        """."""
        self.get_exclosos()
        self.get_reinfeccio()
        self.get_controls()
        self.get_assistents_no_aparellats()
        self.export_files()
    
    def get_exclosos(self):
        """No poden ser control els assistents a festivals NI els ja inclosos com a control en el Cruilla o Canet"""
        u.printTime("exclosos")        
        self.exclosos = {}
        sql = """select 
                    hash 
                from 
                    sisap_festi_casos_edat_"""
        for hash, in u.getAll(sql, db):
            self.exclosos[hash] = True
        
        sql = """select 
                    control 
                from 
                    sisap_festi_casos_edat_ 
                where 
                    concert='Canet' and te_control=1"""
        for hash, in u.getAll(sql, db):
            self.exclosos[hash] = True
            
        sql = """select 
                    hash 
                from 
                    cruilla_controls"""
        for hash, in u.getAll(sql, db):
            self.exclosos[hash] = True
                    
    def get_reinfeccio(self):
        """Agafem reinfeccions d abans del 28 de juliol (ultim dia seguiment estudi)"""
        u.printTime("reinfeccions")
        self.reinfeccions = {}
        sql = """SELECT 
                    persona_id, pdia_primera 
                FROM 
                    dwsisap.COVID19_SEQ_EPISODi 
                WHERE 
                    es_reinfeccio=1 and to_char(PDIA_PRIMERA, 'YYYYMMDD') < '20210728'
                order by
                    pdia_primera asc"""
        for hash, dat in u.getAll(sql, 'exadata'):
            self.reinfeccions[(hash)] = dat
    
    def get_controls(self):
        """dades dels casos"""
        u.printTime("controls")
        self.dbc_vacuna = {}
        self.controlsP = c.defaultdict(set)
        self.controls = c.defaultdict(set)
        sql = """SELECT 
                    hash,  edat, CASE WHEN sexe IS NULL THEN '99' ELSE sexe END AS sexe, CASE WHEN rca_abs IS NULL THEN '99' ELSE rca_abs END AS rca,
                    CASE WHEN  IMMUNITZAT_DATA + 14 < DATE '2021-07-03' THEN '2' WHEN VACUNA_1_DATA < DATE '2021-07-03' THEN '1' ELSE '0' END AS Cruilla_I,
                    CASE WHEN CAS_DATA IS NOT NULL AND CAS_DATA < DATE '2021-07-03' THEN '1' WHEN CAS_DATA IS NULL THEN '0' ELSE '2' END AS Cruilla_c,
                    CAS_DATA,
                    to_char(CAS_DATA, 'IW'),
                    to_char(cas_data, 'YYYY'),
                    to_char(cas_data, 'MM'),
                    CASE WHEN CAS_DATA IS NOT NULL AND DATE '2021-07-03' - CAS_DATA >90 THEN '1' ELSE '0' end
                FROM 
                    dwsisap.DBC_VACUNA"""
        for hash, grup, sexe, abs, cruilla_i, cruilla_c,  cas_data, setm, anys, mes, d90 in u.getAll(sql,'exadata'):
            if hash not in self.exclosos:
                try:
                    setmana1 = int(setm)
                    mes = int(mes)
                except TypeError:
                    setmana1 = None
                if anys == '2020':
                    setmana1 = setmana1 - 1
                if anys == '2021' and setmana1 == 53:
                    setmana1 = 52
                try:
                    setmanaA = setmana1 - 1
                    setmanaB = setmana1 + 1
                except TypeError:
                    setmanaA = None
                    setmanaB = None
                setmana = str(anys) + '-' + str(setmana1)
                setmanaA1 = str(anys) + '-' + str(setmanaA)
                setmanaB1 = str(anys) + '-' + str(setmanaB)
                mes_ = str(anys) + '-' + str(mes)
                if cruilla_c == '1':
                    key_cruillaP = str(grup) + '-' + sexe + '-' + abs + '-' + cruilla_i + '-' + str(setmana)
                    key_cruillaPA = str(grup) + '-' + sexe + '-' + abs + '-' + cruilla_i + '-' + str(setmanaA1)
                    key_cruillaPB = str(grup) + '-' + sexe + '-' + abs + '-' + cruilla_i + '-' + str(setmanaB1)
                    key_cruillaM = str(grup) + '-' + sexe + '-' + abs + '-' + cruilla_i + '-' + str(mes_)
                    key_cruilla90 = str(grup) + '-' + sexe + '-' + abs + '-' + cruilla_i + '-' + str(d90)

                    
                    #self.controlsP[key_cruillaP].add(hash)
                    #self.controlsP[key_cruillaP1].add(hash)
                    #self.controlsP[key_cruillaP2].add(hash)
                    #self.controlsP[key_cruillaPA].add(hash)
                    #self.controlsP[key_cruillaPB].add(hash)
                    if d90 == '1':
                        self.controlsP[key_cruillaM].add(hash)
                    else:
                        self.controlsP[key_cruilla90].add(hash)
                else:
                    key_cruilla = str(grup) + '-' + sexe + '-' + abs + '-' + cruilla_i 
                    self.controls[key_cruilla].add(hash)
                self.dbc_vacuna[hash] = {'grup': grup, 'sexe': sexe, 'abs': abs,  'cas_data': cas_data, 'i': cruilla_i, 'c':cruilla_c}    
     
    def get_assistents_no_aparellats(self):
        """Intento aparellar assistents no aparellats o bé per tema edat o be per tema cas previ, matntinc esclusio de trasllats"""
        u.printTime("assistents")
        self.casos = []
        self.controls_sele = []
        
        sql = """select 
                    hash, naix_rca,grup, sexe, abs, sitaucio, excl_sit, cas, immune, cas_data,  week(cas_data, 7), year(cas_data), month(cas_data),
                    if(cas_data < '2021-04-04','1','0')
                from 
                    sisap_festi_casos_edat_ 
                where 
                    concert='Canet' and te_control=0 and excl_sit=0"""
        for hash, naix, grup, sexe, abs, sit, excl_sit, cas, immune, cas_data, setm, anys, mes, d90 in u.getAll(sql, db):
            te_control = 0
            setmana = str(anys) + '-' + str(setm)
            mes_ = str(anys) + '-' + str(mes)
            edati = int(grup) - 3
            edatf = int(grup) + 3
            if cas == 1:
                for grup1 in range (edati, edatf):
                    if te_control == 0:
                        #key = str(grup1) + '-' + sexe + '-' + abs + '-' + str(immune) + '-' + str(setmana)
                        key =  str(grup1) + '-' + sexe + '-' + abs + '-' + str(immune) + '-' + str(mes_)
                        key90 =  str(grup1) + '-' + sexe + '-' + abs + '-' + str(immune) + '-' + str(d90)
                        if d90 == '1':
                            cont1 = self.controlsP[key]
                        else:
                            cont1 = self.controlsP[key90]
                        cont = list(cont1)
                        shuffle(cont)
                        try:
                            control_selected = cont[0]
                            if d90 == '1':
                                self.controlsP[key].remove(control_selected)
                            else:
                                self.controlsP[key90].remove(control_selected)
                            te_control = 1
                        except IndexError:
                            control_selected= 'No control'
                        sgrup = self.dbc_vacuna[control_selected]['grup'] if control_selected in self.dbc_vacuna else '99'
                        ssexe = self.dbc_vacuna[control_selected]['sexe'] if control_selected in self.dbc_vacuna else '99'
                        sabs = self.dbc_vacuna[control_selected]['abs'] if control_selected in self.dbc_vacuna else '99'
                        scas_data = self.dbc_vacuna[control_selected]['cas_data'] if control_selected in self.dbc_vacuna else None
                        scas_c = self.dbc_vacuna[(control_selected)]['c']  if control_selected in self.dbc_vacuna else '99'
                        simmune_i = self.dbc_vacuna[(control_selected)]['i'] if control_selected in self.dbc_vacuna else '99'
                        sreinfeccio = self.reinfeccions[(control_selected)] if control_selected in self.reinfeccions else '99'
                        scas_previ = None
                        if sreinfeccio != '99':
                            scas_previ = scas_data
                            scas_data = sreinfeccio
                        if control_selected != 'No control':
                            self.controls_sele.append([control_selected, sgrup, ssexe, sabs,  scas_c, simmune_i, scas_data, scas_previ])
            else:
                key = str(grup) + '-' + sexe + '-' + abs + '-' + str(immune) 
                cont1 = self.controls[key]
                cont = list(cont1)
                shuffle(cont)
                try:
                    control_selected = cont[0]
                    self.controls[key].remove(control_selected)
                    te_control = 1
                except IndexError:
                    control_selected= 'No control'
                sgrup = self.dbc_vacuna[control_selected]['grup'] if control_selected in self.dbc_vacuna else '99'
                ssexe = self.dbc_vacuna[control_selected]['sexe'] if control_selected in self.dbc_vacuna else '99'
                sabs = self.dbc_vacuna[control_selected]['abs'] if control_selected in self.dbc_vacuna else '99'
                scas_data = self.dbc_vacuna[control_selected]['cas_data'] if control_selected in self.dbc_vacuna else None
                scas_c = self.dbc_vacuna[(control_selected)]['c']  if control_selected in self.dbc_vacuna else '99'
                simmune_i = self.dbc_vacuna[(control_selected)]['i'] if control_selected in self.dbc_vacuna else '99'
                if control_selected != 'No control':
                    self.controls_sele.append([control_selected, sgrup, ssexe, sabs,  scas_c, simmune_i, scas_data, None])
            reinfeccio = self.reinfeccions[(hash)] if hash in self.reinfeccions else '99'
            cas_previ = None
            if reinfeccio != '99':
                cas_previ = cas_data
                cas_data = reinfeccio
            self.casos.append([hash, naix, grup, sexe, abs, sit, excl_sit,  cas, immune, cas_data, cas_previ, te_control,control_selected ])
            
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "canet_casos_previs"
        tb2 = "canet_controls_previs"
        cols = ("hash varchar(40)", 
				"naix_rca date", "grup int", "sexe varchar(1)","abs varchar(10)","situacio varchar(1)", "excl_sit int", "cas int", "immune int", "cas_data date", "cas_previ date", "te_control int", "control varchar(40)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.casos, tb, db)
        cols = ("hash varchar(40)", 
				 "grup int", "sexe varchar(1)","abs varchar(10)","cas int", "immune int", "cas_data date", "cas_previ date")
        u.createTable(tb2, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.controls_sele, tb2, db)

            
if __name__ == '__main__':
    u.printTime("Inici")
     
    cruilla()
    
    u.printTime("Final") 