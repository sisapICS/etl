# -*- coding: utf8 -*-

"""
En la review demanen mitjana i mediana de data de vacuna
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import hashlib as h

import sisapUtils as u
from random import shuffle


db = 'permanent'
class cruilla(object):
    """."""

    def __init__(self):
        """."""
        self.get_casos()
        self.get_metrica()
        self.export_files()
        
        
    def get_casos(self):
        """Obtenim els hash de casos de canet i cruilla"""
        u.printTime("casos")
        self.canet = {}
        self.cruilla= {}
        
        sql = """select 
                    hash,  control
                from 
                    canet_casos
                where 
                    te_control=1"""
        for hash, control in u.getAll(sql, db):
            self.canet[hash] = True
            
        sql = """select 
                    hash,  control
                from 
                    cruilla_casos
                where 
                    te_control=1"""
        for hash, control in u.getAll(sql, db):
            self.cruilla[hash] = True
            
    def get_metrica(self):
        """*"""
        u.printTime("metriques")
        self.upload = []
            
        sql = """SELECT 
                    hash, vacuna_1_data, vacuna_2_data, immunitzat_data,
                    case when  vacuna_1_data< DATE '2021-07-03' then vacuna_1_data else Null end as vac1_canet,
                    case when  vacuna_1_data< DATE '2021-07-08' then vacuna_1_data else Null end as vac1_cruilla,
                    case when  vacuna_2_data< DATE '2021-07-03' then vacuna_2_data else Null end as vac2_canet,
                    case when  vacuna_2_data< DATE '2021-07-08' then vacuna_2_data else Null end as vac2_cruilla,
                    case when  immunitzat_data + 14 < DATE '2021-07-03' then immunitzat_data else Null end as imm_canet,
                    case when  immunitzat_data + 14 < DATE '2021-07-08' then immunitzat_data else Null end as imm_cruilla,
                    DATE '2021-07-03' - VACUNA_1_DATA AS tvac1_canet,
                    DATE '2021-07-03' - VACUNA_2_DATA AS tvac2_canet,
                    DATE '2021-07-03' - immunitzat_DATA AS timm_canet,
                    DATE '2021-07-08' - VACUNA_1_DATA AS tvac1_cruilla,
                    DATE '2021-07-08' - VACUNA_2_DATA AS tvac2_cruilla,
                    DATE '2021-07-08' - immunitzat_DATA AS timm_cruilla
                FROM 
                    dwsisap.DBC_VACUNA"""
        for hash, vac1, vac2, imm, vac1_canet, vac1_cruilla, vac2_canet, vac2_cruilla, imm_canet, imm_cruilla, t1_canet, t2_canet, ti_canet, t1_cruilla, t2_cruilla, ti_cruilla in u.getAll(sql, 'exadata'):
            if hash in self.canet:
                festival = 'Canet'
                self.upload.append([hash, festival, vac1, vac2, imm, vac1_canet, vac2_canet, imm_canet, t1_canet, t2_canet, ti_canet,])
            if hash in self.cruilla:
                festival = 'Cruilla'
                self.upload.append([hash, festival, vac1, vac2, imm, vac1_cruilla, vac2_cruilla, imm_cruilla, t1_cruilla, t2_cruilla, ti_cruilla ])
                    
                    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "festivals_review_vac"
        cols = ("hash varchar(40)", "festival varchar(50)", 
				 "vac1 date", "vac2 date", "immunitzat_data date",
                 "vac1_estudi date", "vac2_estudi date", "imunitzat_estudi date",
                 "temps_vac1 int", "temps_vac2 int", "temps_immunitzat int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
            
if __name__ == '__main__':
    u.printTime("Inici")
     
    cruilla()
    
    u.printTime("Final") 