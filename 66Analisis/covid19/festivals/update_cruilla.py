# -*- coding: utf8 -*-

"""
Fer actualitzacio dels positius post dia 27 del festival
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import hashlib as h

import sisapUtils as u
from random import shuffle


db = 'permanent'
class cruilla(object):
    """."""

    def __init__(self):
        """."""
        self.get_casos()
        self.get_metrica()
        self.export_files()
        
        
    def get_casos(self):
        """Obtenim els hash de casos i cobntrols"""
        u.printTime("casos")
        self.casos = {}
        
        sql = """select 
                    hash,  control
                from 
                    cruilla_casos
                where 
                    te_control=1"""
        for hash, control in u.getAll(sql, db):
            self.casos[hash] = True
            self.casos[control] = True
                    
    def get_metrica(self):
        """Obtenim casos post 27 del 7 i abans 09 de 8"""
        u.printTime("metriques")
        self.upload = []
            
        sql = """SELECT 
                    hash, CAS_DATA, to_char(cas_data, 'YYYYMMDD')
                FROM 
                    dwsisap.DBC_VACUNA"""
        for hash, cas_data, cas_data_txt in u.getAll(sql, 'exadata'):
            if hash in self.casos:
                if cas_data_txt > '20210727' and cas_data_txt < '20210809':
                    self.upload.append([hash, cas_data])
                    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "update_cruilla"
        cols = ("hash varchar(40)", 
				 "cas_data date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
            
if __name__ == '__main__':
    u.printTime("Inici")
     
    cruilla()
    
    u.printTime("Final") 