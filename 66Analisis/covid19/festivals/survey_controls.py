# -*- coding: utf8 -*-

"""
Dades de la survey cruilla
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import hashlib as h

import sisapUtils as u
from random import shuffle


db = 'permanent'

dx_file = u.tempFolder + "survey_C.txt"


class cruilla(object):
    """."""

    def __init__(self):
        """."""
        self.get_reinfeccio()
        self.get_controls()
        self.get_survey()
        self.export_files()

    def get_reinfeccio(self):
        """Agafem reinfeccions d abans del 28 de juliol (ultim dia seguiment estudi)"""
        u.printTime("reinfeccions")
        self.reinfeccions = {}
        sql = """SELECT 
                    persona_id, pdia_primera 
                FROM 
                    dwsisap.COVID19_SEQ_EPISODi 
                WHERE 
                    es_reinfeccio=1 and to_char(PDIA_PRIMERA, 'YYYYMMDD') < '20210728'
                order by
                    pdia_primera asc"""
        for hash, dat in u.getAll(sql, 'exadata'):
            self.reinfeccions[(hash)] = dat
    
    def get_controls(self):
        """dades dels casos"""
        u.printTime("controls")
        self.dbc_vacuna = {}
        self.controlsP = c.defaultdict(set)
        self.controls = c.defaultdict(set)
        sql = """SELECT 
                    hash,  edat, CASE WHEN sexe IS NULL THEN '99' ELSE sexe END AS sexe, CASE WHEN rca_abs IS NULL THEN '99' ELSE rca_abs END AS rca,
                    CASE WHEN  IMMUNITZAT_DATA + 14 < DATE '2021-07-08' THEN '2' WHEN VACUNA_1_DATA + 14 < DATE '2021-07-08' THEN '1' ELSE '0' END AS Cruilla_I,
                    CASE WHEN CAS_DATA IS NOT NULL AND CAS_DATA < DATE '2021-07-08' THEN '1' ELSE '0' END AS Cruilla_c,
                    CAS_DATA,
                    to_char(cas_data, 'YYYY-MM')
                FROM 
                    dwsisap.DBC_VACUNA"""
        for hash, grup, sexe, abs, cruilla_i, cruilla_c,  cas_data,  mes in u.getAll(sql,'exadata'):
            grup1, grup2 = '99', '99'    
            if cruilla_c == '1':
                key_cruilla = str(grup) + '-' + sexe + '-' + cruilla_i + '-' + str(mes)
                key_cruillaN = str(grup) + '-' + 'No binari' + '-' + cruilla_i + '-' + str(mes)
                self.controlsP[key_cruilla].add(hash)
                self.controlsP[key_cruillaN].add(hash)
            else:
                key_cruilla = str(grup) + '-' + sexe + '-' +  cruilla_i 
                key_cruillaN = str(grup) + '-' + 'No binari' + '-' + cruilla_i 
                self.controls[key_cruilla].add(hash)
                self.controls[key_cruillaN].add(hash)
            self.dbc_vacuna[hash] = {'grup': grup, 'sexe': sexe, 'abs': abs,  'cas_data': cas_data, 'i': cruilla_i, 'c':cruilla_c}   
    
    
            
    def get_survey(self):
        """enquesta"""
        u.printTime("in_concert")        
        self.casos = []
        self.controls_sele = []
        for (id, sexe, edat, area, vacuna, cas_previ, data_cas, mes_cas) in u.readCSV(dx_file, sep='@'): 
            te_control = 0
            edati = int(edat) - 3
            edatf = int(edat) + 3
            if cas_previ == 'Si':
                for ed in range(edati, edatf):
                    if te_control == 0:
                        key =  str(ed) + '-' + sexe + '-' + str(vacuna) + '-' + str(mes_cas)
                        cont1 = self.controlsP[key]
                        cont = list(cont1)
                        shuffle(cont)
                        try:
                            control_selected = cont[0]
                            self.controlsP[key].remove(control_selected)
                            te_control = 1
                        except IndexError:
                            control_selected= 'No control'
                        sgrup = self.dbc_vacuna[control_selected]['grup'] if control_selected in self.dbc_vacuna else '99'
                        ssexe = self.dbc_vacuna[control_selected]['sexe'] if control_selected in self.dbc_vacuna else '99'
                        scas_data = self.dbc_vacuna[control_selected]['cas_data'] if control_selected in self.dbc_vacuna else None
                        scas_c = self.dbc_vacuna[(control_selected)]['c']  if control_selected in self.dbc_vacuna else '99'
                        simmune_i = self.dbc_vacuna[(control_selected)]['i'] if control_selected in self.dbc_vacuna else '99'
                        sreinfeccio = self.reinfeccions[(control_selected)] if control_selected in self.reinfeccions else '99'
                        scas_previ = None
                        if sreinfeccio != '99':
                            scas_previ = scas_data
                            scas_data = sreinfeccio
                        if control_selected != 'No control':
                            self.controls_sele.append([control_selected, sgrup, ssexe, scas_c, simmune_i, scas_data, scas_previ, id])
            else:
                key = str(edat) + '-' + sexe + '-' + str(vacuna) 
                cont1 = self.controls[key]
                cont = list(cont1)
                shuffle(cont)
                try:
                    control_selected = cont[0]
                    self.controls[key].remove(control_selected)
                    te_control = 1
                except IndexError:
                    control_selected= 'No control'
                sgrup = self.dbc_vacuna[control_selected]['grup'] if control_selected in self.dbc_vacuna else '99'
                ssexe = self.dbc_vacuna[control_selected]['sexe'] if control_selected in self.dbc_vacuna else '99'
                scas_data = self.dbc_vacuna[control_selected]['cas_data'] if control_selected in self.dbc_vacuna else None
                scas_c = self.dbc_vacuna[(control_selected)]['c']  if control_selected in self.dbc_vacuna else '99'
                simmune_i = self.dbc_vacuna[(control_selected)]['i'] if control_selected in self.dbc_vacuna else '99'
                if control_selected != 'No control':
                    self.controls_sele.append([control_selected, sgrup, ssexe,  scas_c, simmune_i, scas_data, None, id])
            self.casos.append([id, sexe, edat, area, vacuna, cas_previ, data_cas, mes_cas, te_control,control_selected])
            
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "survey_cruilla_c2"
        tb2 = "survey_cruilla_ctl2"
        cols = ("id varchar(40)", "sexe varchar(20)",
				"edat int", "area varchar(100)","immune int", "cas varchar(10)", "cas_data varchar(40)", "mes varchar(40)", "te_control int", "control varchar(40)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.casos, tb, db)
        cols = ("hash varchar(40)", "edat int", "sexe varchar(20)",
				 "cas int", "immune int", "cas_data date", "previ date", "id_assistent varchar(40)")
        u.createTable(tb2, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.controls_sele, tb2, db)
    
if __name__ == '__main__':
    u.printTime("Inici")
     
    cruilla()
    
    u.printTime("Final") 
