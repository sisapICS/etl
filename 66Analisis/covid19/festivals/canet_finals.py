# -*- coding: utf8 -*-

"""
Obtenim base de dades final amb les dades de la primera analisi + les dades de casos previs
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import hashlib as h

import sisapUtils as u
from random import shuffle


db = 'permanent'
class canet(object):
    """."""

    def __init__(self):
        """."""
        self.get_casos()
        self.get_controls()
        self.get_controls()
        self.export_files()
        
        
    def get_casos(self):
        """Obtenim caso aparellats en informe i posteriors"""
        u.printTime("casos")
        self.casos = []
        
        sql = """select 
                    hash, naix_rca,grup, sexe, abs, sitaucio, excl_sit, cas, immune, cas_data, te_control, control
                from 
                    sisap_festi_casos_edat_ 
                where 
                    concert='Canet' and te_control=1"""
        for hash, naix, grup, sexe, abs, sit, excl_sit, cas, immune, cas_data, te_control, control in u.getAll(sql, db):
             self.casos.append([hash, naix, grup, sexe, abs, sit, excl_sit,  cas, immune, cas_data, None, te_control,control ])
                    
        sql = """select 
                     hash, naix_rca,grup, sexe, abs, sitaucio, excl_sit, cas, immune, cas_data, te_control, control
                from 
                    sisap_festi_casos_edat_ 
                where 
                    concert='Canet' and te_control=0 and excl_sit=1"""
        for hash, naix, grup, sexe, abs, sit, excl_sit, cas, immune, cas_data, te_control, control in u.getAll(sql, db):
             self.casos.append([hash, naix, grup, sexe, abs, sit, excl_sit,  cas, immune, cas_data, None, te_control,control ])
        
        sql = """select
                    hash, naix_rca,grup, sexe, abs, situacio, excl_sit, cas, immune, cas_data, cas_previ, te_control, control
                from    
                    canet_casos_previs"""
        for hash, naix, grup, sexe, abs, sit, excl_sit,  cas, immune, cas_data, cas_previ, te_control,control_selected in u.getAll(sql, db):
            self.casos.append([hash, naix, grup, sexe, abs, sit, excl_sit,  cas, immune, cas_data, cas_previ, te_control,control_selected ])
            
    def get_controls(self):
        """Obtenim controls aparellats en informe i posteriors"""
        u.printTime("controls")
        self.controls_sele = []
            
        sql = """select
                    hash, grup, sexe, abs, cas, immune, cas_data
                from
                    sisap_festi_controls_edat_
                where
                    concert='Canet' and hash<>'No control'"""
        for hash, grup, sexe, abs, cas, immune, cas_data in u.getAll(sql, db):
            self.controls_sele.append([hash, grup, sexe, abs,  cas, immune, cas_data, None])
        
        sql = """select
                    hash, grup, sexe, abs, cas, immune, cas_data, cas_previ
                from
                    canet_controls_previs"""
        for hash, grup, sexe, abs, cas, immune, cas_data, cas_previ in u.getAll(sql, db):
            self.controls_sele.append([hash, grup, sexe, abs,  cas, immune, cas_data, cas_previ])
            
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "canet_casos"
        tb2 = "canet_controls"
        cols = ("hash varchar(40)", 
				"naix_rca date", "grup int", "sexe varchar(1)","abs varchar(10)","situacio varchar(1)", "excl_sit int", "cas int", "immune int", "cas_data date", "cas_previ date", "te_control int", "control varchar(40)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.casos, tb, db)
        cols = ("hash varchar(40)", 
				 "grup int", "sexe varchar(1)","abs varchar(10)","cas int", "immune int", "cas_data date", "cas_previ date")
        u.createTable(tb2, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.controls_sele, tb2, db)

            
if __name__ == '__main__':
    u.printTime("Inici")
     
    canet()
    
    u.printTime("Final") 