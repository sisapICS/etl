# -*- coding: utf8 -*-

"""
Festivals, vida, cruilla i canet
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import hashlib as h

import sisapUtils as u
from random import shuffle


db = 'permanent'

def get_festi_data(fest):
    dfest = 0
    if fest == 'Canet':
        dfest = '20210703'
    elif fest == 'CRUILLA':
        dfest = '20210708'
    elif fest == 'Vida':
        dfest = '20210701'
    else:
        'error'
    return dfest

dx_file = u.tempFolder + "festi_casos.txt"

class sitges(object):
    """."""

    def __init__(self):
        """."""
        self.get_hash()
        self.get_in_concert()
        self.get_dades()
        self.get_casos()
        self.export_files()
        
    def get_hash(self):
        u.printTime("obtenim hash")
        """Obtenim la relacio hash covid - dni"""
        self.dni_to_hash = {}
        
        sql = """SELECT dni, hash, data_naixement, sexe, situacio 
            FROM dwsisap.RCA_CIP_NIA where situacio <>'F'"""
        for dni, hash, naix, sexe, sit in u.getAll(sql, 'exadata'):
            self.dni_to_hash[dni] = {'hash': hash, 'naix': naix, 'sexe': sexe, 'sit':sit}
    
    def get_in_concert(self):
        """els q han estat de concert no poden ser controls"""
        u.printTime("in_concert")        
        self.in_concert = {}
        for (dni, concert, naix, sexe1, sexe2) in u.readCSV(dx_file, sep='@'):
            hash = self.dni_to_hash[dni]['hash'] if dni in self.dni_to_hash else None
            self.in_concert[hash] = True
    
    def get_dades(self):
        """dades dels casos"""
        u.printTime("dades")
        self.dbc_vacuna = {}
        self.controls = c.defaultdict(set)
        self.estats = {}
        sql = """SELECT hash,  grup_codi, CASE WHEN sexe IS NULL THEN '99' ELSE sexe END AS sexe, CASE WHEN rca_abs IS NULL THEN '99' ELSE rca_abs END AS rca,
        CASE WHEN  IMMUNITZAT_DATA + 14 < DATE '2021-07-03' THEN '2' WHEN VACUNA_1_DATA + 14 < DATE '2021-07-03' THEN '1' ELSE '0' END AS Canet_I,
        CASE WHEN CAS_DATA IS NOT NULL AND CAS_DATA < DATE '2021-07-03' THEN '1' WHEN CAS_DATA IS NULL THEN '0' ELSE '2' END AS Canet_c,
        CASE WHEN  IMMUNITZAT_DATA + 14 < DATE '2021-07-08' THEN '2' WHEN VACUNA_1_DATA + 14 < DATE '2021-07-08' THEN '1' ELSE '0' END AS Cruilla_I,
        CASE WHEN CAS_DATA IS NOT NULL AND CAS_DATA < DATE '2021-07-08' THEN '1' WHEN CAS_DATA IS NULL THEN '0' ELSE '2' END AS Cruilla_c,
         CASE WHEN  IMMUNITZAT_DATA + 14 < DATE '2021-07-01' THEN '2' WHEN VACUNA_1_DATA + 14 < DATE '2021-07-01' THEN '1' ELSE '0' END AS Vida_I,
        CASE WHEN CAS_DATA IS NOT NULL AND CAS_DATA < DATE '2021-07-01' THEN '1' WHEN CAS_DATA IS NULL THEN '0' ELSE '2' END AS Vida_c,
        CASE WHEN EDAT < 5 OR EDAT>65 THEN 1 ELSE 0 END AS EXCLOS_EDAT,
        CAS_DATA
        FROM dwsisap.DBC_VACUNA"""
        for hash, grup, sexe, abs, canet_i, canet_c, cruilla_i, cruilla_c, vida_i, vida_c, exc, cas_data in u.getAll(sql,'exadata'):
            if canet_c <> '1':
                key_canet = 'Canet' + '-' + str(grup) + '-' + sexe + '-' + abs + '-' + canet_i 
                if exc == 0 and hash not in self.in_concert:
                    self.controls[key_canet].add(hash)
            if cruilla_c <> '1':
                key_cruilla = 'CRUILLA' + '-' + str(grup) + '-' + sexe + '-' + abs + '-' + cruilla_i
                if exc == 0 and hash not in self.in_concert:
                    self.controls[key_cruilla].add(hash)
            if vida_c <> '1':
                key_vida  = 'Vida' + '-' + str(grup) + '-' + sexe + '-' + abs + '-' + vida_i
                if exc == 0 and hash not in self.in_concert:
                    self.controls[key_vida].add(hash)
            self.dbc_vacuna[hash] = {'grup': grup, 'sexe': sexe, 'abs': abs,  'exc':exc, 'cas_data': cas_data}
            self.estats[(hash, 'Canet')] = {'i': canet_i, 'c': canet_c}
            self.estats[(hash, 'CRUILLA')] = {'i': cruilla_i, 'c': cruilla_c}
            self.estats[(hash, 'Vida')] = {'i': vida_i, 'c': vida_c}

    
    def get_casos(self):
        """Casos"""
        u.printTime("Casos")
        self.duplicats = {}
        self.casos = []
        self.controls_sele = []
   
        for (dni, concert, naix, sexe1, sexe2) in u.readCSV(dx_file, sep='@'):
            self.duplicats[(dni, concert, naix, sexe1, sexe2)] = True
            
        for (dni, concert, naix, sexe1, sexe2), n in self.duplicats.items():
            hash = self.dni_to_hash[dni]['hash'] if dni in self.dni_to_hash else None    
            naixRCA = self.dni_to_hash[dni]['naix'] if dni in self.dni_to_hash else None
            sexeRCA = self.dni_to_hash[dni]['sexe'] if dni in self.dni_to_hash else None
            sit = self.dni_to_hash[dni]['sit'] if dni in self.dni_to_hash else None
            exclos_sit = 0 if sit == 'A' else 1
            grup = self.dbc_vacuna[hash]['grup'] if hash in self.dbc_vacuna else '99'
            sexe = self.dbc_vacuna[hash]['sexe'] if hash in self.dbc_vacuna else '99'
            abs = self.dbc_vacuna[hash]['abs'] if hash in self.dbc_vacuna else '99'
            exc_edat = self.dbc_vacuna[hash]['exc'] if hash in self.dbc_vacuna else None
            cas_data = self.dbc_vacuna[hash]['cas_data'] if hash in self.dbc_vacuna else None
            cas_c = self.estats[(hash, concert)]['c'] if (hash, concert) in self.estats else '99'
            immune_i = self.estats[(hash, concert)]['i'] if (hash, concert) in self.estats else '99'
            te_control = 0
            if exclos_sit == 0 and exc_edat == 0 and cas_c <> '1':
                key = concert + '-' + str(grup) + '-' + sexe + '-' + abs + '-' + immune_i
                cont1 = self.controls[key]
                cont = list(cont1)
                shuffle(cont)
                try:
                    control_selected = cont[0]
                    te_control = 1
                except IndexError:
                    control_selected= 'No control'
                sgrup = self.dbc_vacuna[control_selected]['grup'] if control_selected in self.dbc_vacuna else '99'
                ssexe = self.dbc_vacuna[control_selected]['sexe'] if control_selected in self.dbc_vacuna else '99'
                sabs = self.dbc_vacuna[control_selected]['abs'] if control_selected in self.dbc_vacuna else '99'
                scas_data = self.dbc_vacuna[control_selected]['cas_data'] if control_selected in self.dbc_vacuna else None
                scas_c = self.estats[(control_selected, concert)]['c']  if control_selected in self.dbc_vacuna else '99'
                simmune_i = self.estats[(control_selected, concert)]['i'] if control_selected in self.dbc_vacuna else '99'
                self.controls_sele.append([control_selected, concert, sgrup, ssexe, sabs,  scas_c, simmune_i, scas_data])
            self.casos.append([hash, concert, naixRCA, grup, sexe, abs, sit, exclos_sit,  exc_edat, cas_c, immune_i, cas_data, te_control,control_selected ])
           
    
    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_festi_pob"
        tb = "sisap_festi_casos"
        tb2 = "sisap_festi_controls"
        cols = ("hash varchar(40)", "concert varchar(40)",
				"naix_rca date", "grup varchar(20)", "sexe int","abs varchar(10)","sitaucio varchar(1)", "excl_sit int", "excl_edat int", "cas int", "immune int", "cas_data date", "te_control int",
                "control varchar(40)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.casos, tb, db)
        cols = ("hash varchar(40)", "concert varchar(40)",
				 "grup varchar(20)", "sexe int","abs varchar(10)","cas int", "immune int", "cas_data date")
        u.createTable(tb2, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.controls_sele, tb2, db)

        
        
if __name__ == '__main__':
    u.printTime("Inici")
     
    sitges()
    
    u.printTime("Final") 