# -*- coding: utf8 -*-

import hashlib as h
import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'redics'


tb = 'sisap_covid_rec_resis_m'


class article_jacobo(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_tipus()
        self.get_cip()
        self.get_inicials()
        self.get_medea()
        self.get_estats()
        self.get_dbs()
        self.get_dbs_2019()
        self.get_master()
        self.get_rca()
        self.export_data()
        self.dona_grants()
        
    def get_tipus(self):
        """tipus residencia"""
        u.printTime("tipus resi")
        self.codis = {}
        sql = """select codi,tipus 
                from dwsisap.residencies_cataleg 
                where tipus in (1)"""
        for codi, tipus in u.getAll(sql, 'exadata'):
            self.codis[codi] = tipus
    
    def get_cip(self):
        """hash to cip"""
        u.printTime("hash to cip")
        self.hash_to_cip = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.hash_to_cip[hash] = cip

    def get_inicials(self):
        """Agafem del cens de resis de residents dins del periode d estudi"""
        u.printTime("residents")
        self.residents = {}
        sql = """select hash, data, perfil, residencia 
                from dwsisap.residencies_cens 
                where (to_char(data, 'YYYYMMDD') >= '20200301' and to_char(data, 'YYYYMMDD') <= '20200430') and perfil='Resident'"""
        for id, data, perfil, resi in u.getAll(sql, 'exadata'):
            if resi in self.codis:
                if id in self.residents:
                    dant = self.residents[id]['inici']
                    if data < dant:
                        self.residents[id] = {'inici':data,'resi': resi}
                else:
                    self.residents[id] = {'inici':data,  'resi': resi}            
        
    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        u.printTime("medea")
        self.censal = {}
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              'redics')}
        sql = ("select usua_cip, sector_censal from md_poblacio", 'redics')
        for id, sector in u.getAll(*sql):
            if id in self.hash_to_cip:
                cip2 = self.hash_to_cip[id]
                cipCov = h.sha1(cip2).hexdigest().upper()
                if sector in valors:
                    self.censal[cipCov] = valors[sector]
    
    def get_estats(self):
        """."""
        u.printTime("estats")
        codis = {"ER0001": "PCC", "ER0002": "MACA"}
        sql = "select es_cod_u, es_cod \
               from prstb715 a"
        self.estats = {}
        for cip, cod in u.getAll(sql, 'redics'):
            if cip in self.hash_to_cip:
                cip2 = self.hash_to_cip[cip]
                cipCov = h.sha1(cip2).hexdigest().upper()
                v = codis[cod]
                self.estats[(cipCov, v)] = True
    
    def get_dbs(self):
        """Obtenim dades de dbs"""
        u.printTime("DBS")
        sql = """select c_cip,
                    c_nacionalitat,
                    PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    v_barthel_data, v_barthel_valor,
                    v_pfeifer_data, v_pfeifer_valor
                from DWSISAP.DBS"""
        self.factors = {}
        self.in_dbs_true = {}
        for (cip,
             nac,
             hta, dm1, dm2, mpoc,
             ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             barthel_d, barthel_v,
             pfeifer_d, pfeifer_v) in u.getAll(sql, 'exadata'):
            self.factors[(cip, "in_dbs")] = 1
            self.in_dbs_true[(cip)] = True
            if nac:
                self.factors[(cip, "nacionalitat")] = nac
            if hta:
                self.factors[(cip, "hta")] = hta
            if dm1:
                self.factors[(cip, "dm")] = dm1
            if dm2:
                self.factors[(cip, "dm")] = dm2
            if mpoc:
                self.factors[(cip, "mpoc")] = mpoc
            if ci:
                self.factors[(cip, "ci")] = ci
            if mcv:
                self.factors[(cip, "mcv")] = mcv
            if ic:
                self.factors[(cip, "ic")] = ic
            if acfa:
                self.factors[(cip, "acfa")] = acfa
            if valv:
                self.factors[(cip, "valv")] = valv
            if hepat:
                self.factors[(cip, "hepat")] = hepat
            if vhb:
                self.factors[(cip, "vhb")] = vhb
            if vhc:
                self.factors[(cip, "vhc")] = vhc
            if neo:
                self.factors[(cip, "neo")] = neo
            if mrc:
                self.factors[(cip, "mrc")] = mrc
            if obes:
                self.factors[(cip, "obes")] = obes
            if vih:
                self.factors[(cip, "vih")] = vih
            if sida:
                self.factors[(cip, "sida")] = sida
            if demencia:
                self.factors[(cip, "demencia")] = demencia
            if artrosi:
                self.factors[(cip, "artrosi")] = artrosi 
            if barthel_d:
                self.factors[(cip, "barthel_d")] = barthel_d
                self.factors[(cip, "barthel_v")] = barthel_v
            if pfeifer_d:
                self.factors[(cip, "pfeifer_d")] = pfeifer_d
                self.factors[(cip, "pfeifer_v")] = pfeifer_v   
    
    def get_dbs_2019(self):
        """Obtenim dades de dbs"""
        u.printTime("DBS2019")
        SIDICS_DB = ("dbs", "x0002")
        
        sql = """select c_cip,
                    c_nacionalitat,
                    c_gma_codi, c_gma_complexitat,
                    PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    v_barthel_data, v_barthel_valor,
                    v_pfeifer_data, v_pfeifer_valor
                from dbs_2019"""
        self.factors19 = {}
        for (cip,
             nac,
             gma_c, gma_cmplx,
             hta, dm1, dm2, mpoc,
             ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             barthel_d, barthel_v,
             pfeifer_d, pfeifer_v) in u.getAll(sql, SIDICS_DB):
            if cip in self.hash_to_cip:
                cip2 = self.hash_to_cip[cip]
                cipCov = h.sha1(cip2).hexdigest().upper()
                if cipCov not in self.in_dbs_true:
                    self.factors19[(cipCov, "in_dbs")] = 1 
                    if gma_c:
                        self.factors19[(cipCov, "gma_c")] = gma_c
                        self.factors19[(cipCov, "gma_cmplx")] = gma_cmplx
                    if nac:
                        self.factors19[(cipCov, "nacionalitat")] = nac
                    if hta:
                        self.factors19[(cipCov, "hta")] = hta
                    if dm1:
                        self.factors19[(cipCov, "dm")] = dm1
                    if dm2:
                        self.factors19[(cipCov, "dm")] = dm2
                    if mpoc:
                        self.factors19[(cipCov, "mpoc")] = mpoc
                    if ci:
                        self.factors19[(cipCov, "ci")] = ci
                    if mcv:
                        self.factors19[(cipCov, "mcv")] = mcv
                    if ic:
                        self.factors19[(cipCov, "ic")] = ic
                    if acfa:
                        self.factors19[(cipCov, "acfa")] = acfa
                    if valv:
                        self.factors19[(cipCov, "valv")] = valv
                    if hepat:
                        self.factors19[(cipCov, "hepat")] = hepat
                    if vhb:
                        self.factors19[(cipCov, "vhb")] = vhb
                    if vhc:
                        self.factors19[(cipCov, "vhc")] = vhc
                    if neo:
                        self.factors19[(cipCov, "neo")] = neo
                    if mrc:
                        self.factors19[(cipCov, "mrc")] = mrc
                    if obes:
                        self.factors19[(cipCov, "obes")] = obes
                    if vih:
                        self.factors19[(cipCov, "vih")] = vih
                    if sida:
                        self.factors19[(cipCov, "sida")] = sida
                    if demencia:
                        self.factors19[(cipCov, "demencia")] = demencia
                    if artrosi:
                        self.factors19[(cipCov, "artrosi")] = artrosi 
                    if barthel_d:
                        self.factors19[(cipCov, "barthel_d")] = barthel_d
                        self.factors19[(cipCov, "barthel_v")] = barthel_v
                    if pfeifer_d:
                        self.factors19[(cipCov, "pfeifer_d")] = pfeifer_d
                        self.factors19[(cipCov, "pfeifer_v")] = pfeifer_v
    
    def get_master(self):
        """obtenim dades de master"""
        u.printTime("master")
        self.casos = {}
        sql = """
                select hash, edat, sexe, localitat, abs, up, resi, hotel, estat, origen_d, cas_data, 
                dx_cod, dx_dde,dx_dba, dx_sit, pcr_data, pcr_res, pcr_n,pcr_ia_n, test_n, test_ia_n,exitus, 
                gma_codi, gma_complexitat
                from dwsisap.SISAP_CORONAVIRUS_MASTER
                where estat in ('Possible', 'Confirmat') and edat>64 and to_char(cas_data,'YYYYMMDD')<'20200501'
              """
        for (hash, edat,sexe,localitat, abs, up, resi, hotel, estat, origen_d, cas_data, 
                dx_cod, dx_dde,dx_dba, dx_sit, pcr_data, pcr_res, pcr_n,pcr_ia_n, test_n, test_ia_n, exitus,
                gma_codi, gma_complexitat) in u.getAll(sql, 'exadata'): 
            self.casos[hash] = {'estat': estat, 'origen_d': origen_d, 'cas_data': cas_data, 'dx_cod': dx_cod, 'dx_dde': dx_dde, 'dx_dba': dx_dba, 'dx_sit': dx_sit, 'pcr_data': pcr_data, 'pcr_res': pcr_res, 'pcr_n': pcr_n, 'pcr_ia_n': pcr_ia_n,
                    'test_n': test_n, 'test_ia_n': test_ia_n, 'exitus': exitus}
    
    def get_rca(self):
        """agafem vius a 1 de marc del 2020. edat a 1 de marc"""
        u.printTime("rca")
        self.upload = []
        
        sql = """select hash, localitat, abs, eap,
              floor(extract(day from date '2020-03-01' - data_naixement) 
                            / 365.25) edat, data_naixement,
              decode(sexe, 0, 'H', 'D') sexe
               from dwsisap.rca_cip_nia a 
               where data_naixement <= date '2020-03-01' and 
                     (situacio = 'A' or 
                      (situacio = 'D' and 
                       data_defuncio > date '2020-03-01'))"""
        for hash, localitat, abs, up, edat, naix, sexe in u.getAll(sql, 'exadata'):
            if edat > 64:
                resident = 1 if hash in self.residents else 0
                nac = None
                hta, dm, mpoc = None, None, None
                ci = None
                mcv, ic, acfa, valv = None, None, None, None
                hepat, vhb, vhc, neo, mrc = None, None, None, None, None
                obes, vih, sida = None, None, None
                demencia, artrosi = None, None
                medea = None
                gma_codi, gma_complexitat = None, None
                in_dbs = 0
                barthel_d, barthel_v, pfeifer_d, pfeifer_v = None, None,None,None
                maca = 1 if (hash, 'MACA') in self.estats else 0
                pcc = 1 if (hash, 'PCC') in self.estats else 0
                estat = self.casos[hash]['estat'] if hash in self.casos else None
                origen_d = self.casos[hash]['origen_d'] if hash in self.casos else None
                cas_data = self.casos[hash]['cas_data'] if hash in self.casos else None
                dx_cod = self.casos[hash]['dx_cod'] if hash in self.casos else None
                dx_dde = self.casos[hash]['dx_dde'] if hash in self.casos else None
                dx_dba = self.casos[hash]['dx_dba'] if hash in self.casos else None
                dx_sit = self.casos[hash]['dx_sit'] if hash in self.casos else None
                pcr_data = self.casos[hash]['pcr_data'] if hash in self.casos else None
                pcr_res = self.casos[hash]['pcr_res'] if hash in self.casos else None
                pcr_n = self.casos[hash]['pcr_n'] if hash in self.casos else None
                pcr_ia_n = self.casos[hash]['pcr_ia_n'] if hash in self.casos else None
                test_n = self.casos[hash]['test_n'] if hash in self.casos else None
                test_ia_n = self.casos[hash]['test_ia_n'] if hash in self.casos else None
                exitus = self.casos[hash]['exitus'] if hash in self.casos else None
                if maca == 1:
                    pcc = 0
                if hash in self.censal:
                    medea = self.censal[hash]
                if hash in self.in_dbs_true:
                    in_dbs = self.factors[(hash, "in_dbs")] if (hash, "in_dbs") in self.factors else 0
                    gma_codi = self.factors[(hash, "gma_c")] if (hash, "gma_c") in self.factors else gma_codi
                    gma_complexitat = self.factors[(hash, "gma_cmplx")] if (hash, "gma_cmplx") in self.factors else gma_complexitat
                    nac = self.factors[(hash, "nacionalitat")] if (hash, "nacionalitat") in self.factors else None
                    hta = self.factors[(hash, "hta")] if (hash, "hta") in self.factors else None
                    dm = self.factors[(hash, "dm")] if (hash, "dm") in self.factors else None
                    mpoc = self.factors[(hash, "mpoc")] if (hash, "mpoc") in self.factors else None
                    ci = self.factors[(hash, "ci")] if (hash, "ci") in self.factors else None
                    mcv = self.factors[(hash, "mcv")] if (hash, "mcv") in self.factors else None
                    ic = self.factors[(hash, "ic")] if (hash, "ic") in self.factors else None
                    acfa = self.factors[(hash, "acfa")] if (hash, "acfa") in self.factors else None
                    valv = self.factors[(hash, "valv")] if (hash, "valv") in self.factors else None
                    hepat = self.factors[(hash, "hepat")] if (hash, "hepat") in self.factors else None
                    vhb = self.factors[(hash, "vhb")] if (hash, "vhb") in self.factors else None
                    vhc = self.factors[(hash, "vhc")] if (hash, "vhc") in self.factors else None
                    neo = self.factors[(hash, "neo")] if (hash, "neo") in self.factors else None
                    mrc = self.factors[(hash, "mrc")] if (hash, "mrc") in self.factors else None
                    obes = self.factors[(hash, "obes")] if (hash, "obes") in self.factors else None
                    vih = self.factors[(hash, "vih")] if (hash, "vih") in self.factors else None
                    sida = self.factors[(hash, "sida")] if (hash, "sida") in self.factors else None
                    demencia = self.factors[(hash, "demencia")] if (hash, "demencia") in self.factors else None
                    artrosi = self.factors[(hash, "artrosi")] if (hash, "artrosi") in self.factors else None
                    barthel_d = self.factors[(hash, "barthel_d")] if (hash, "barthel_d") in self.factors else None
                    barthel_v = self.factors[(hash, "barthel_v")] if (hash, "barthel_v") in self.factors else None
                    pfeifer_d = self.factors[(hash, "pfeifer_d")] if (hash, "pfeifer_d") in self.factors else None
                    pfeifer_v = self.factors[(hash, "pfeifer_v")] if (hash, "pfeifer_v") in self.factors else None
                else:
                    in_dbs = self.factors19[(hash, "in_dbs")]  if (hash, "in_dbs") in self.factors19 else 0
                    nac = self.factors19[(hash, "nacionalitat")] if (hash, "nacionalitat") in self.factors19 else None
                    gma_codi = self.factors19[(hash, "gma_c")] if (hash, "gma_c") in self.factors19 else gma_codi
                    gma_complexitat = self.factors19[(hash, "gma_cmplx")] if (hash, "gma_cmplx") in self.factors19 else gma_complexitat
                    hta = self.factors19[(hash, "hta")] if (hash, "hta") in self.factors19 else None
                    dm = self.factors19[(hash, "dm")] if (hash, "dm") in self.factors19 else None
                    mpoc = self.factors19[(hash, "mpoc")] if (hash, "mpoc") in self.factors19 else None
                    ci = self.factors19[(hash, "ci")] if (hash, "ci") in self.factors19 else None
                    mcv = self.factors19[(hash, "mcv")] if (hash, "mcv") in self.factors19 else None
                    ic = self.factors19[(hash, "ic")] if (hash, "ic") in self.factors19 else None
                    acfa = self.factors19[(hash, "acfa")] if (hash, "acfa") in self.factors19 else None
                    valv = self.factors19[(hash, "valv")] if (hash, "valv") in self.factors19 else None
                    hepat = self.factors19[(hash, "hepat")] if (hash, "hepat") in self.factors19 else None
                    vhb = self.factors19[(hash, "vhb")] if (hash, "vhb") in self.factors19 else None
                    vhc = self.factors19[(hash, "vhc")] if (hash, "vhc") in self.factors19 else None
                    neo = self.factors19[(hash, "neo")] if (hash, "neo") in self.factors19 else None
                    mrc = self.factors19[(hash, "mrc")] if (hash, "mrc") in self.factors19 else None
                    obes = self.factors19[(hash, "obes")] if (hash, "obes") in self.factors19 else None
                    vih = self.factors19[(hash, "vih")] if (hash, "vih") in self.factors19 else None
                    sida = self.factors19[(hash, "sida")] if (hash, "sida") in self.factors19 else None
                    demencia = self.factors19[(hash, "demencia")] if (hash, "demencia") in self.factors19 else None
                    artrosi = self.factors19[(hash, "artrosi")] if (hash, "artrosi") in self.factors19 else None
                    barthel_d = self.factors19[(hash, "barthel_d")] if (hash, "barthel_d") in self.factors19 else None
                    barthel_v = self.factors19[(hash, "barthel_v")] if (hash, "barthel_v") in self.factors19 else None
                    pfeifer_d = self.factors19[(hash, "pfeifer_d")] if (hash, "pfeifer_d") in self.factors19 else None
                    pfeifer_v = self.factors19[(hash, "pfeifer_v")] if (hash, "pfeifer_v") in self.factors19 else None
                self.upload.append([hash, edat, naix, sexe, nac, localitat, abs, up, medea, resident, estat, origen_d, cas_data, 
                    dx_cod, dx_dde,dx_dba, dx_sit, pcr_data, pcr_res, pcr_n,pcr_ia_n, test_n, test_ia_n, exitus, 
                   gma_codi, gma_complexitat,  pcc, maca,
                     in_dbs, hta, dm, mpoc, ci, mcv, ic, acfa, valv, hepat, vhb, vhc, neo, mrc, obes, vih, sida,
                     demencia, artrosi,
                     barthel_d,barthel_v,pfeifer_d,pfeifer_v])
                     
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("hash varchar2(40)", "edat int", "naixement date",
            "sexe varchar2(1)", "nacionalitat varchar2(100)", "localitat varchar2(7)", "abs int",
            "up varchar2(5)", "medea number(6, 2)", "resident int",
             "estat varchar(16)", "origen_d varchar2(40)","cas_data date",
            "dx_cod varchar2(16)", "dx_dde date", "dx_dba date",
            "dx_sit varchar2(16)",  "pcr_data date", "pcr_res varchar2(30)", "pcr_n int","pcr_ia_n int", "test_n int", "test_ia_n int",
            "exitus date",
            "gma_codi varchar2(3)", "gma_complexitat number(6, 2)", "pcc int", "maca int",
            "in_dbs int",
            "FR_hta date",  "FR_dm date", "FR_mpoc date",  "FR_ci date", "FR_mcv date",
            "FR_ic date", "FR_acfa date", "FR_valv date", "FR_hepat date", "FR_vhb date", "FR_vhc date", "FR_neo date", "FR_mrc date", "FR_obes date",
            "FR_vih date", "FR_sida date", "FR_demencia date", "FR_artrosi date", 
            "V_barthel_data date",  "V_barthel_valor int","V_pfeifer_data date",  "V_pfeifer_valor int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
    def dona_grants(self):
        """."""
        users= ["PREDUPRP",  "PREDUECR", "PREDULMB", "PREDUXMG"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)
    
if __name__ == '__main__':
    u.printTime("Inici")
    
    article_jacobo()
    
    u.printTime("Final")