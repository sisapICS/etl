# -*- coding: utf8 -*-

"""
ITS
"""

import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u

tb = 'sisap_recerca_ITS'
db = 'permanent'

dx_file =  "ITS.txt"

class its_dades(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_assignada()
        self.get_neos()
        self.export()

    def get_assignada(self):
        """."""
        self.pob = {}
        sql = 'select id_cip, usua_sexe, usua_data_naixement, usua_uab_up, usua_abs_codi_abs from assignada'
        for id, sexe, naix, up, abs in u.getAll(sql, 'import'):
            self.pob[id] = {'sexe': sexe, 'naix': naix, 'up': up, 'abs': abs}
    
    
    def get_neos(self):
        """."""
        u.printTime("problemes")
        dx_codis = []
        tip_dx = {}
        self.its_dx = c.Counter()
        for (dx,  tip) in u.readCSV(dx_file, sep='@'):
            dx_codis.append(dx)
            tip_dx[dx] = tip
        in_crit = tuple(dx_codis)
        self.duplicitats = c.defaultdict(lambda: c.defaultdict(set))
        self.duplicitatsVIH = {}
        
        sql = "select id_cip, pr_dde,  pr_cod_ps \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_cod_ps in {}  \
                            and extract(year_month from pr_dde) >'201312' and \
                            pr_data_baixa = 0 group by id_cip, pr_dde, pr_cod_ps".format(in_crit)
        for id, dat, codi in u.getAll(sql, 'import'):
            tipus = tip_dx[codi]
            if tipus == 'VIH/SIDA':
                if (id) in self.duplicitatsVIH:
                    dat2 = self.duplicitatsVIH[id]
                    if dat < dat2:
                        self.duplicitatsVIH[id] = dat
                else:
                    self.duplicitatsVIH[id] = dat
            else:
                self.duplicitats[id][tipus].add(dat)
      
        sql = "select id_cip, pr_dde, date_format(pr_dde,'%Y%m'),  pr_cod_ps, pr_up, count(*) \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_cod_ps in {}  \
                            and extract(year_month from pr_dde) >'201312' and \
                            pr_data_baixa = 0 group by id_cip,pr_dde, date_format(pr_dde,'%Y%m'), pr_cod_ps, pr_up".format(in_crit)
        for id, dat, periode,codi, up, recompte in u.getAll(sql, 'import'):
            sexe = self.pob[id]['sexe'] if id in self.pob else None
            naix = self.pob[id]['naix'] if id in self.pob else None
            up2 = self.pob[id]['up'] if id in self.pob else None
            abs = self.pob[id]['abs'] if id in self.pob else None
            tipus = tip_dx[codi]
            try:
                ed = u.yearsBetween(naix, dat)
            except:
                ed = None
            publica = 0
            if ed != None:
                if tipus == 'VIH/SIDA':
                    dataI = self.duplicitatsVIH[id]
                    if dat == dataI:
                        publica = 1
                elif tipus == 'URETRITIS INESP':
                    publica = 1 
                    if id in self.duplicitats:                         
                        if 'GONOCOC' in self.duplicitats[id]:
                            dbas = self.duplicitats[id]['GONOCOC']
                            for dba in dbas:
                                d16 = dat + timedelta(weeks=16)
                                if dat <= dba <= d16:
                                    publica = 0
                        if 'CLAMIDIA' in self.duplicitats[id]:
                            dbas = self.duplicitats[id]['CLAMIDIA']
                            for dba in dbas:
                                d16 = dat + timedelta(weeks=16)
                                if dat <= dba <= d16:
                                    publica = 0
                        if 'Tricomones' in self.duplicitats[id]:
                            dbas = self.duplicitats[id]['Tricomones']
                            for dba in dbas:
                                d16 = dat + timedelta(weeks=16)
                                if dat <= dba <= d16:
                                    publica = 0
                elif tipus == 'ITS INESP':
                    publica = 1
                    if id in self.duplicitats:      
                        for cim in self.duplicitats[id]:
                            if cim == 'ITS' or cim == 'ITS INESP':
                                ok = 1
                            else:
                                dbas = self.duplicitats[id][cim]
                                for dba in dbas:
                                    d16 = dat + timedelta(weeks=16)
                                    if dat <= dba <= d16:
                                        publica = 0
                else:
                    publica = 1
                if publica == 1 and tipus != 'ITS':
                    self.its_dx[(periode, dat, up, up2, abs, u.ageConverter(ed,5), u.sexConverter(sexe), tipus)] += 1
      


    def export(self):
        """."""
        cols = ("periode varchar(6)", "data date", "up varchar(10)", "up_ass varchar(10)", "abs varchar(10)",  "edat varchar(10)", "sexe varchar(10)", "tipus varchar(100)", "recompte int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)

        upload = []
        for (periode, dat, up, up2, abs, edat, sexe, tipus), n in self.its_dx.items():
            n = int (n)
            upload.append([periode, dat, up, up2, abs, edat, sexe, tipus, n])
        u.listToTable(upload, tb, db)
                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    its_dades()
    
    u.printTime("Final")    