# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import csv,os,sys
from datetime import datetime, timedelta

import sisapUtils as u
import hashlib as h

zona = True


TODAY = d.datetime.now().date()
print TODAY

db = 'redics'

age_groups = ['infants', 'kids', 'adults', 'elderly.young', 'elderly.old']
health_status = ['healthy','comorbidities']

case_indicators = ['n.detected', 'n.severe', 'n.icu', 'cf.icu']
surveillance_indicators = ['n.test', 'test.positive', 'p.test.symp.num', 'p.positive.symp.num','p.test.symp.den', 'p.positive.symp.den']

def get_edat(edat):
    """."""
    grup = 'infants' if edat < 5 else  'kids' if edat <15 else 'adults' if edat < 50 else 'elderly.young' if edat <70 else 'elderly.old' if edat >=70 else 'error'
    return grup


class projecteAA(object):
    """."""

    def __init__(self):
        """."""
        self.demo_d, self.report_d, self.surv_d = c.Counter(), c.Counter(), c.Counter()
        self.get_periode()
        self.get_cataleg()
        self.get_hash()
        self.get_resis()
        self.get_ingressats()
        self.get_dbs()
        self.get_dbs_2019()
        self.get_rca()
        self.get_simptomes()
        self.get_master()
        self.get_pcr()
        self.export_data()
        
    def get_periode(self):
        """.""" 
        sql = "select min(cas_data), max(cas_data) from sisap_covid_pac_master where  to_char(cas_data, 'YYYYMMDD') >= '20200601' and to_char(cas_data,'YYYYMMDD') <='20200801'"
        for min, max in u.getAll(sql, db):
            self.ini = min
            self.fi = max
    
    def get_cataleg(self):
        """.Agafem codis de territoris"""
        u.printTime("cataleg")
        self.cataleg_loc, self.cataleg_abs = {}, {} 
        sql = """select up from sisap_covid_cat_territori {}""".format('  where comarca_c=33' if zona else '')
        for up,  in u.getAll(sql, db):
            self.cataleg_abs[str(up)] = True      
        sql = """select localitat_c from sisap_covid_cat_localitat {}""".format('  where comarca_c=33' if zona else '')
        for loc,  in u.getAll(sql, db):  
            self.cataleg_loc[str(loc)] = True  
    
    def get_hash(self):
        """conversor de hash"""
        u.printTime("hash")
        self.hashI = {}
        sql = """ select hash, hash_ics
                    from sisap_covid_pac_id"""
        for hash,hashi in u.getAll(sql, db):
            self.hashI[hashi] = hash
    
    def get_resis(self):
        """Obtenim els pacients en residencia de pac_resis"""
        u.printTime("resis")
        sql = """select hash 
                from sisap_covid_res_master"""
        self.resis = set([hash for hash, in u.getAll(sql, db)])

    def get_ingressats(self):
        """pac_ingressats"""
        u.printTime("ingressos")
        self.ingressos = {}
        sql = """select hash,data, decode(tipus_d, 'Convencional','C','U')
                from preduffa.SISAP_COVID_PAC_INGRESSATS 
                 where tipus_d like ('C%') and to_char(data,'YYYYMMDD')>='20200601'"""
        for hash, dat, tipus in u.getAll(sql, db):
            self.ingressos[(hash, tipus)] = True

    
    def get_dbs(self):
        """."""
        u.printTime("dbs")
        sql = """select c_cip,c_up_abs, c_edat_anys,
                    PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA,  ps_bronquitis_cronica_data, PS_CARDIOPATIA_ISQUEMICA_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA,
                    PS_ACV_MCV_DATA,  ps_valvulopatia_data,PS_NEOPLASIA_M_DATA, 
                    PS_OBESITAT_DATA
                from dbs"""
        self.factors = {}
        self.in_dbs_true = {}
        self.com = c.Counter()
        for (cip, abs, edat,
              dm1, dm2, mpoc,
             asma, bronquitis, ci, ic, fa,
             avc, valv, neo, obesitat
             ) in u.getAll(sql, db):
            if str(abs) in self.cataleg_abs:
                if cip in self.hashI:
                    cip1 = self.hashI[cip]
                    self.in_dbs_true[cip1] = True
                    if obesitat:
                        self.factors[(cip1, "obesitat")] = 1
                grup = get_edat(edat)
                cip2 = self.hashI[cip] if cip in self.hashI else '0'
                if cip2 not in self.resis:
                    self.com[(grup, "pob")] += 1
                    if dm1 or dm2 or asma or mpoc or bronquitis or avc or fa or ci or ic or valv or neo or obesitat:
                        self.com[(grup, "pato")] += 1
        
        for (grup, tip), n in self.com.items():
            if tip == 'pob':
                pato = self.com[(grup, 'pato')]
                perc = float(pato)/float(n)
                self.demo_d[('pop.c', grup, 'comorbidities')] = perc
 
    def get_dbs_2019(self):
        """Obtenim dades de dbs"""
        u.printTime("DBS2019")
        SIDICS_DB = ("dbs", "x0002")
        sql = """select c_cip,
                    PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA,  ps_bronquitis_cronica_data, PS_CARDIOPATIA_ISQUEMICA_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA,
                    PS_ACV_MCV_DATA,  ps_valvulopatia_data,PS_NEOPLASIA_M_DATA, 
                    PS_OBESITAT_DATA
                from dbs_2019"""
        self.factors = {}
        self.in_dbs_true = {}
        for (cip,
              dm1, dm2, mpoc,
             asma, bronquitis, ci, ic, fa,
             avc, valv, neo, obesitat
             ) in u.getAll(sql, SIDICS_DB):
            if cip in self.hashI:
                cip1 = self.hashI[(cip)]
                if cip1 not in self.in_dbs_true:
                    self.in_dbs_true[(cip1)] = True
                    if obesitat:
                        self.factors[(cip1, "obesitat")] = 1

    def get_rca(self):
        """."""
        u.printTime("rca")
        self.pob =c.Counter()
        self.pob_rca = {}
        sql = """select hash, sexe, data_naixement, localitat
                from preduffa.sisap_covid_pac_rca 
                where situacio='A'"""
        for hash, sexe, naix, loc in u.getAll(sql, db):
            if str(loc) in self.cataleg_loc:
                if hash not in self.resis:
                    edat = u.yearsBetween(naix, TODAY)
                    grup = get_edat(edat)
                    self.demo_d[('pop.total', grup, 'all')] += 1
                    self.pob_rca[hash] = {'edat': grup}
     
    def get_simptomes(self):
        u.printTime("simptomes")
        """Ho trec de sectors"""
        self.simptomes = {}
        sql = """select diag_cod_u, max(diag_te_sintomes) 
               from prstb306 
               where to_char(diag_dde,'YYYYMMDD')>'20200523' and to_char(diag_dde,'YYYYMMDD')<'20200803'
               group by diag_cod_u"""
        for sector in u.sectors:
            print sector
            for cip,  simpt in u.getAll(sql, sector):
                hash = h.sha1(cip).hexdigest().upper()        
                self.simptomes[hash]= simpt

    
    def get_master(self):
        """Agafem casos de pac_master"""
        u.printTime("master")
        sql = """select hash, cas_data, edat, localitat, fr_diabetis, fr_patol_resp, fr_mcv, fr_neoplasia, exitus
                from preduffa.sisap_covid_pac_master  
                where estat='Confirmat' and origen_d like '%PCR%' and resi is null and to_char(cas_data, 'YYYYMMDD') >= '20200601' and to_char(cas_data,'YYYYMMDD') <='20200731'"""
        for hash, dat, edat, loc, dm, respi, mcv, neo, exitus in u.getAll(sql, db):
            if str(loc) in self.cataleg_loc:
                obesitat = 1 if (hash, 'obesitat') in self.factors else 0
                grup = get_edat(edat)
                status, indicator = 'healthy', 'n.detected'
                if obesitat == 1 or dm == 1 or respi == 1 or mcv == 1 or neo == 1:
                    status = 'comorbidities'
                indicator = 'n.severe' if (hash, 'C') in self.ingressos else indicator
                indicator = 'n.icu' if (hash, 'U') in self.ingressos else indicator
                self.report_d[(indicator, grup, status, dat)] += 1
                if indicator == 'n.icu':
                    if exitus:
                         self.report_d[('cf.icu', grup, status, dat)] += 1
                if hash in self.simptomes:
                    self.surv_d[('p.positive.symp.den', grup, 'all', dat)] += 1
                    si_simp = self.simptomes[hash]
                    if si_simp == 'S':
                        self.surv_d[('p.positive.symp.num', grup, 'all', dat)] += 1

 
    def get_pcr(self):
        """PCRs"""
        u.printTime("PCR")
        tested = c.Counter()
        hash_dat = {}
        sql = """select hash, data, max(decode(estat_cod, 0, 1, 0)) 
               from sisap_covid_pac_prv_aux               
              where prova = 'PCR' and 
                     origen <> 'IA' and to_char(data, 'YYYYMMDD') >= '20200601' and to_char(data,'YYYYMMDD') <='20200731'
               group by hash, data"""
        for hash, dat, pos in u.getAll(sql, db):
            if hash in self.pob_rca:
                grup = self.pob_rca[hash]['edat']
                self.surv_d[('n.test', grup, 'all', dat)]+=1
                if pos:
                    self.surv_d[( 'test.positive', grup, 'all', dat)]+=1
                if hash in self.simptomes:
                    self.surv_d[('p.test.symp.den', grup, 'all', dat)]+= 1
                    si_simp = self.simptomes[hash]
                    if si_simp == 'S':
                        self.surv_d[('p.test.symp.num', grup, 'all', dat)]+=1
                
    def export_data(self):
        """."""
        u.printTime("export reported cases")
        upload = []
        for dat in u.dateRange(self.ini, self.fi):
            for indicador in case_indicators:
                for age in age_groups:
                    for status in health_status:
                        recomptes = self.report_d[(indicador, age, status, dat)] if (indicador, age, status, dat) in self.report_d else 0
                        upload.append([indicador, age, status, dat, recomptes])
                
        file = u.tempFolder + "model_segria_reported_cases.csv"
        u.writeCSV(file, upload, sep='@')
        
        u.printTime("export surveillance")
        upload = []
        for dat in u.dateRange(self.ini, self.fi):
            for indicador in surveillance_indicators:
                for age in age_groups:
                    recomptes = self.surv_d[(indicador, age, 'all', dat)] if (indicador, age, 'all', dat) in self.surv_d else 0
                    upload.append([indicador, age, 'all' , dat, recomptes])
        file = u.tempFolder + "model_segria_surveillance.csv"
        u.writeCSV(file, upload, sep='@')
        
        
        u.printTime("export demographics")
        upload = []
        for (tip, grup, pob), n in self.demo_d.items():
            upload.append([tip, grup, pob, n])
        file = u.tempFolder + "model_segria_demographic.csv"
        u.writeCSV(file, upload, sep='@')
    
if __name__ == '__main__':
    u.printTime("Inici")
    
    projecteAA()
    
    u.printTime("Final")