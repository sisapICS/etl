# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u

resta = d.datetime.now().hour < 21
TODAY = d.datetime.now().date() - d.timedelta(days=1 * resta)

db = 'redics'
tb = 'sisap_covid_rec_resis'


class article_jacobo(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_hash()
        self.get_dbs()
        self.get_dbs_2019()
        self.get_resis()
        self.get_medea()
        self.get_estats()
        self.get_master()
        self.export_data()
        self.dona_grants()
        
  
    def get_hash(self):
        """conversor de hash"""
        u.printTime("hash")
        self.hashI = {}
        sql = """ select hash, hash_ics
                    from sisap_covid_pac_id"""
        for hash,hashi in u.getAll(sql, 'redics'):
            self.hashI[hashi] = hash
     
    def get_dbs(self):
        """Obtenim dades de dbs"""
        u.printTime("DBS")
        self.factors_header = ("nacionalitat", "CHARTLSTON_V", "CHARTLSTON_D")
        sql = """select hash,
                    c_nacionalitat,
                    PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    v_barthel_data, v_barthel_valor,
                    v_pfeifer_data, v_pfeifer_valor
                from dbs a, sisap_covid_pac_id b 
                where a.c_cip = b.hash_ics"""
        self.factors = {}
        self.in_dbs_true = {}
        for (cip,
             nac,
             hta, dm1, dm2, mpoc,
             ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             barthel_d, barthel_v,
             pfeifer_d, pfeifer_v) in u.getAll(sql, db):
            self.factors[(cip, "in_dbs")] = 1
            self.in_dbs_true[(cip)] = True
            if nac:
                self.factors[(cip, "nacionalitat")] = nac
            if hta:
                self.factors[(cip, "hta")] = hta
            if dm1:
                self.factors[(cip, "dm")] = dm1
            if dm2:
                self.factors[(cip, "dm")] = dm2
            if mpoc:
                self.factors[(cip, "mpoc")] = mpoc
            if ci:
                self.factors[(cip, "ci")] = ci
            if mcv:
                self.factors[(cip, "mcv")] = mcv
            if ic:
                self.factors[(cip, "ic")] = ic
            if acfa:
                self.factors[(cip, "acfa")] = acfa
            if valv:
                self.factors[(cip, "valv")] = valv
            if hepat:
                self.factors[(cip, "hepat")] = hepat
            if vhb:
                self.factors[(cip, "vhb")] = vhb
            if vhc:
                self.factors[(cip, "vhc")] = vhc
            if neo:
                self.factors[(cip, "neo")] = neo
            if mrc:
                self.factors[(cip, "mrc")] = mrc
            if obes:
                self.factors[(cip, "obes")] = obes
            if vih:
                self.factors[(cip, "vih")] = vih
            if sida:
                self.factors[(cip, "sida")] = sida
            if demencia:
                self.factors[(cip, "demencia")] = demencia
            if artrosi:
                self.factors[(cip, "artrosi")] = artrosi 
            if barthel_d:
                self.factors[(cip, "barthel_d")] = barthel_d
                self.factors[(cip, "barthel_v")] = barthel_v
            if pfeifer_d:
                self.factors[(cip, "pfeifer_d")] = pfeifer_d
                self.factors[(cip, "pfeifer_v")] = pfeifer_v                
            
    
    def get_dbs_2019(self):
        """Obtenim dades de dbs"""
        u.printTime("DBS2019")
        SIDICS_DB = ("dbs", "x0002")
        
        sql = """select c_cip,
                    c_nacionalitat,
                    c_gma_codi, c_gma_complexitat,
                    PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    v_barthel_data, v_barthel_valor,
                    v_pfeifer_data, v_pfeifer_valor
                from dbs_2019"""
        self.factors19 = {}
        for (cip,
             nac,
             gma_c, gma_cmplx,
             hta, dm1, dm2, mpoc,
             ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             barthel_d, barthel_v,
             pfeifer_d, pfeifer_v) in u.getAll(sql, SIDICS_DB):
            if cip in self.hashI:
                cipCov = self.hashI[cip]
                if cipCov not in self.in_dbs_true:
                    self.factors19[(cipCov, "in_dbs")] = 1 
                    if gma_c:
                        self.factors19[(cipCov, "gma_c")] = gma_c
                        self.factors19[(cipCov, "gma_cmplx")] = gma_cmplx
                    if nac:
                        self.factors19[(cipCov, "nacionalitat")] = nac
                    if hta:
                        self.factors19[(cipCov, "hta")] = hta
                    if dm1:
                        self.factors19[(cipCov, "dm")] = dm1
                    if dm2:
                        self.factors19[(cipCov, "dm")] = dm2
                    if mpoc:
                        self.factors19[(cipCov, "mpoc")] = mpoc
                    if ci:
                        self.factors19[(cipCov, "ci")] = ci
                    if mcv:
                        self.factors19[(cipCov, "mcv")] = mcv
                    if ic:
                        self.factors19[(cipCov, "ic")] = ic
                    if acfa:
                        self.factors19[(cipCov, "acfa")] = acfa
                    if valv:
                        self.factors19[(cipCov, "valv")] = valv
                    if hepat:
                        self.factors19[(cipCov, "hepat")] = hepat
                    if vhb:
                        self.factors19[(cipCov, "vhb")] = vhb
                    if vhc:
                        self.factors19[(cipCov, "vhc")] = vhc
                    if neo:
                        self.factors19[(cipCov, "neo")] = neo
                    if mrc:
                        self.factors19[(cipCov, "mrc")] = mrc
                    if obes:
                        self.factors19[(cipCov, "obes")] = obes
                    if vih:
                        self.factors19[(cipCov, "vih")] = vih
                    if sida:
                        self.factors19[(cipCov, "sida")] = sida
                    if demencia:
                        self.factors19[(cipCov, "demencia")] = demencia
                    if artrosi:
                        self.factors19[(cipCov, "artrosi")] = artrosi 
                    if barthel_d:
                        self.factors19[(cipCov, "barthel_d")] = barthel_d
                        self.factors19[(cipCov, "barthel_v")] = barthel_v
                    if pfeifer_d:
                        self.factors19[(cipCov, "pfeifer_d")] = pfeifer_d
                        self.factors19[(cipCov, "pfeifer_v")] = pfeifer_v     
                        
    def get_resis(self):
        """Agafem pacients que hagin estat algun cop en residències"""
        u.printTime("resis")
        tip_res = {}
        sql = "select cod, tipus from sisap_covid_cat_residencia"
        for cod, tip in u.getAll(sql, db):
            tip_res[cod] = tip
        
        self.pac_res = {}
        sql = "select hash, residencia, entrada, sortida from sisap_covid_res_master"
        for hash, res, entrada, sortida in u.getAll(sql, db):
            res_tip = tip_res[res] if res in tip_res else None
            self.pac_res[hash] = {'res': res, 'entrada': entrada, 'sortida': sortida, 'tip': res_tip}
    
    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        u.printTime("medea")
        self.censal = {}
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              db)}
        sql = ("select usua_cip, sector_censal from md_poblacio", db)
        for id, sector in u.getAll(*sql):
            if id in self.hashI:
                hash = self.hashI[id] 
                if sector in valors:
                    self.censal[hash] = valors[sector]
    
    def get_estats(self):
        """."""
        codis = {"ER0001": "PCC", "ER0002": "MACA"}
        sql = "select es_cod_u, es_cod \
               from prstb715 a"
        self.estats = {}
        for cip, cod in u.getAll(sql, db):
            if cip in self.hashI:
                cipCov = self.hashI[cip]
                v = codis[cod]
                self.estats[(cipCov, v)] = True
    
    
    def get_master(self):
        """obtenim dades de master"""
        u.printTime("master")
        self.upload = []
        sql = """
                select hash, edat, sexe, localitat, abs, up, resi, hotel, estat, origen_d, cas_data, 
                dx_cod, dx_dde,dx_dba, dx_sit, pcr_data, pcr_res, pcr_n,pcr_ia_n, test_n, test_ia_n,exitus, 
                gma_codi, gma_complexitat
                from sisap_covid_pac_master
                where estat in ('Possible', 'Confirmat') and edat>64 and to_char(cas_data,'YYYYMMDD')<'20200501'
              """
        for (hash, edat,sexe,localitat, abs, up, resi, hotel, estat, origen_d, cas_data, 
                dx_cod, dx_dde,dx_dba, dx_sit, pcr_data, pcr_res, pcr_n,pcr_ia_n, test_n, test_ia_n, exitus,
                gma_codi, gma_complexitat) in u.getAll(sql, db): 
            entrada = None
            sortida = None
            tipus_res = None
            nac = None
            hta, dm, mpoc = None, None, None
            ci = None
            mcv, ic, acfa, valv = None, None, None, None
            hepat, vhb, vhc, neo, mrc = None, None, None, None, None
            obes, vih, sida = None, None, None
            demencia, artrosi = None, None
            medea = None
            in_dbs = 0
            barthel_d, barthel_v, pfeifer_d, pfeifer_v = None, None,None,None
            maca = 1 if (hash, 'MACA') in self.estats else 0
            pcc = 1 if (hash, 'PCC') in self.estats else 0
            if maca == 1:
                pcc = 0
            if hash in self.censal:
                medea = self.censal[hash]
            if hash in self.pac_res:
                resi = self.pac_res[hash]['res']
                entrada = self.pac_res[hash]['entrada']
                sortida = self.pac_res[hash]['sortida']
                tipus_res = self.pac_res[hash]['tip']
            if hash in self.in_dbs_true:
                in_dbs = self.factors[(hash, "in_dbs")] if (hash, "in_dbs") in self.factors else 0
                nac = self.factors[(hash, "nacionalitat")] if (hash, "nacionalitat") in self.factors else None
                hta = self.factors[(hash, "hta")] if (hash, "hta") in self.factors else None
                dm = self.factors[(hash, "dm")] if (hash, "dm") in self.factors else None
                mpoc = self.factors[(hash, "mpoc")] if (hash, "mpoc") in self.factors else None
                ci = self.factors[(hash, "ci")] if (hash, "ci") in self.factors else None
                mcv = self.factors[(hash, "mcv")] if (hash, "mcv") in self.factors else None
                ic = self.factors[(hash, "ic")] if (hash, "ic") in self.factors else None
                acfa = self.factors[(hash, "acfa")] if (hash, "acfa") in self.factors else None
                valv = self.factors[(hash, "valv")] if (hash, "valv") in self.factors else None
                hepat = self.factors[(hash, "hepat")] if (hash, "hepat") in self.factors else None
                vhb = self.factors[(hash, "vhb")] if (hash, "vhb") in self.factors else None
                vhc = self.factors[(hash, "vhc")] if (hash, "vhc") in self.factors else None
                neo = self.factors[(hash, "neo")] if (hash, "neo") in self.factors else None
                mrc = self.factors[(hash, "mrc")] if (hash, "mrc") in self.factors else None
                obes = self.factors[(hash, "obes")] if (hash, "obes") in self.factors else None
                vih = self.factors[(hash, "vih")] if (hash, "vih") in self.factors else None
                sida = self.factors[(hash, "sida")] if (hash, "sida") in self.factors else None
                demencia = self.factors[(hash, "demencia")] if (hash, "demencia") in self.factors else None
                artrosi = self.factors[(hash, "artrosi")] if (hash, "artrosi") in self.factors else None
                barthel_d = self.factors[(hash, "barthel_d")] if (hash, "barthel_d") in self.factors else None
                barthel_v = self.factors[(hash, "barthel_v")] if (hash, "barthel_v") in self.factors else None
                pfeifer_d = self.factors[(hash, "pfeifer_d")] if (hash, "pfeifer_d") in self.factors else None
                pfeifer_v = self.factors[(hash, "pfeifer_v")] if (hash, "pfeifer_v") in self.factors else None
            else:
                in_dbs = self.factors19[(hash, "in_dbs")]  if (hash, "in_dbs") in self.factors19 else 0
                nac = self.factors19[(hash, "nacionalitat")] if (hash, "nacionalitat") in self.factors19 else None
                gma_codi = self.factors19[(hash, "gma_c")] if (hash, "gma_c") in self.factors19 else gma_codi
                gma_complexitat = self.factors19[(hash, "gma_cmplx")] if (hash, "gma_cmplx") in self.factors19 else gma_complexitat
                hta = self.factors19[(hash, "hta")] if (hash, "hta") in self.factors19 else None
                dm = self.factors19[(hash, "dm")] if (hash, "dm") in self.factors19 else None
                mpoc = self.factors19[(hash, "mpoc")] if (hash, "mpoc") in self.factors19 else None
                ci = self.factors19[(hash, "ci")] if (hash, "ci") in self.factors19 else None
                mcv = self.factors19[(hash, "mcv")] if (hash, "mcv") in self.factors19 else None
                ic = self.factors19[(hash, "ic")] if (hash, "ic") in self.factors19 else None
                acfa = self.factors19[(hash, "acfa")] if (hash, "acfa") in self.factors19 else None
                valv = self.factors19[(hash, "valv")] if (hash, "valv") in self.factors19 else None
                hepat = self.factors19[(hash, "hepat")] if (hash, "hepat") in self.factors19 else None
                vhb = self.factors19[(hash, "vhb")] if (hash, "vhb") in self.factors19 else None
                vhc = self.factors19[(hash, "vhc")] if (hash, "vhc") in self.factors19 else None
                neo = self.factors19[(hash, "neo")] if (hash, "neo") in self.factors19 else None
                mrc = self.factors19[(hash, "mrc")] if (hash, "mrc") in self.factors19 else None
                obes = self.factors19[(hash, "obes")] if (hash, "obes") in self.factors19 else None
                vih = self.factors19[(hash, "vih")] if (hash, "vih") in self.factors19 else None
                sida = self.factors19[(hash, "sida")] if (hash, "sida") in self.factors19 else None
                demencia = self.factors19[(hash, "demencia")] if (hash, "demencia") in self.factors19 else None
                artrosi = self.factors19[(hash, "artrosi")] if (hash, "artrosi") in self.factors19 else None
                barthel_d = self.factors19[(hash, "barthel_d")] if (hash, "barthel_d") in self.factors19 else None
                barthel_v = self.factors19[(hash, "barthel_v")] if (hash, "barthel_v") in self.factors19 else None
                pfeifer_d = self.factors19[(hash, "pfeifer_d")] if (hash, "pfeifer_d") in self.factors19 else None
                pfeifer_v = self.factors19[(hash, "pfeifer_v")] if (hash, "pfeifer_v") in self.factors19 else None
            self.upload.append([hash, edat,sexe, nac, localitat, abs, up, medea, resi, entrada, sortida, tipus_res, estat, origen_d, cas_data, 
                dx_cod, dx_dde,dx_dba, dx_sit, pcr_data, pcr_res, pcr_n,pcr_ia_n, test_n, test_ia_n, exitus, 
               gma_codi, gma_complexitat,  pcc, maca,
                 in_dbs, hta, dm, mpoc, ci, mcv, ic, acfa, valv, hepat, vhb, vhc, neo, mrc, obes, vih, sida,
                 demencia, artrosi,
                 barthel_d,barthel_v,pfeifer_d,pfeifer_v])

    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("hash varchar2(40)", "edat int",
            "sexe varchar2(1)", "nacionalitat varchar2(100)", "localitat varchar2(7)", "abs int",
            "up varchar2(5)", "medea number(6, 3)", "resi varchar2(10)", "entrada_resi date", "sortida_resi date", "tipus_resi int",
             "estat varchar2(16)", "origen_d varchar2(40)","cas_data date",
            "dx_cod varchar2(16)", "dx_dde date", "dx_dba date",
            "dx_sit varchar2(16)",  "pcr_data date", "pcr_res varchar2(30)", "pcr_n int","pcr_ia_n int", "test_n int", "test_ia_n int",
            "exitus date",
            "gma_codi varchar2(3)", "gma_complexitat number(6, 3)", "pcc int", "maca int",
            "in_dbs int",
            "FR_hta date",  "FR_dm date", "FR_mpoc date",  "FR_ci date", "FR_mcv date",
            "FR_ic date", "FR_acfa date", "FR_valv date", "FR_hepat date", "FR_vhb date", "FR_vhc date", "FR_neo date", "FR_mrc date", "FR_obes date",
            "FR_vih date", "FR_sida date", "FR_demencia date", "FR_artrosi date", 
            "V_barthel_data date",  "V_barthel_valor int","V_pfeifer_data date",  "V_pfeifer_valor int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
    
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)
    
if __name__ == '__main__':
    u.printTime("Inici")
    
    article_jacobo()
    
    u.printTime("Final")