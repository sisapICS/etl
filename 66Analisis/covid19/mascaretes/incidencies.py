# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta
from time import time,sleep

import sisapUtils as u

db = "permanent"
tb = "sisap_covid_mascaretes"

cohorts_naix = ['2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020']

class mascaretes(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_periode()
        self.get_casos()
        self.get_cohorts()
        self.get_calcul()
        self.export_data()
		    
    def get_periode(self):
        """.agafem periode"""
        u.printTime("periode")
        self.periode = {}
        sql = """SELECT CAS_DATA_PDIA, count(*) FROM dwsisap.dbc_metriques where cas_data_pdia is not null GROUP BY CAS_DATA_PDIA"""
        for  data, n in u.getAll(sql, 'exadata'):
            self.periode[data] = n
    
    def get_casos(self):
        """.agafem casos per any"""
        u.printTime("casos")
        self.casos = {}
        sql = """SELECT to_char(DATA_NAIXEMENT,'YYYY'), CAS_DATA_PDIA, count(*) FROM dwsisap.dbc_metriques WHERE to_char(DATA_NAIXEMENT,'YYYY') in ('2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020') GROUP BY to_char(DATA_NAIXEMENT,'YYYY'), CAS_DATA_PDIA"""
        for  anys, data, n in u.getAll(sql, 'exadata'):
            self.casos[(anys,data)] = n    

    def get_cohorts(self):
        """."""
        u.printTime("cohorts")
        self.cohorts = {}
        sql = """SELECT to_char(DATA_NAIXEMENT, 'YYYY'), count(*) FROM DWSISAP.DBC_VACUNA WHERE es_rca=1 GROUP BY to_char(DATA_NAIXEMENT, 'YYYY')"""
        for anys, rec in u.getAll(sql, 'exadata'):
            self.cohorts[anys] = rec
    
    def get_calcul(self):
        """."""
        u.printTime("càlcul mascaretes")
        self.upload = []
        for cohort in cohorts_naix:
            pob = self.cohorts[cohort]
            for dat, n in self.periode.items():
                d7 = dat - timedelta(days=6)
                observats = self.casos[(cohort, dat)] if (cohort,dat) in self.casos else 0
                observats7 = observats
                for single_date in u.dateRange(d7, dat):
                    observ = self.casos[(cohort, single_date)] if (cohort,single_date) in self.casos else 0
                    observats7 += observ
                ia7 = (float(observats7)/float(pob))*100000
                self.upload.append([cohort, dat, observats, observats7, ia7])
            
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("cohort varchar(4)", "data date",
                "n_dia int", "n_7d int", "ia7 double")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    mascaretes()
    
    u.printTime("Final")