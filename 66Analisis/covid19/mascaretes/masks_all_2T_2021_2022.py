# -*- coding: utf8 -*-

import hashlib as h

"""
Mascaretes refaig amb dades menys fines
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = 'mascaretes_positius_21_22_2T'
db = 'permanent'

u.printTime("select 2021_2022 2T")
upload = []
sql = "select persona_id, to_char(data_prova,'YYYY-MM-DD'), to_char(data_resultat,'YYYY-MM-DD') from dwsisap_escola.clinica_positiu"
for id, datap, dataR in u.getAll(sql, 'exadata'):
    upload.append([id.encode('utf-8'), datap, dataR])

u.printTime("export")
cols = ("persona_id varchar(40)","data_prova varchar(40)", "data_resultat varchar(40)")
u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
u.listToTable(upload, tb, db)


