---
title: "`r paste0('Mascaretes ', params$title)`"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
  word_document:
    toc: yes
    toc_depth: '6'
params:
   title: "Article"
   ini.curs: !r as.Date("2022-01-10")
   fi.curs: !r as.Date("2022-02-10")
   cas_var: "data_prova"
   curs.var: "curs_c1"
   flt.cas_previ: FALSE
   flt.medea.U: FALSE
   flt.medea.R: FALSE
---

### Paràmetres informe

```{r}
# - CURS: `r params$curs`
# 
# - TAULA: `r params$taula`
# - TAULA POSITIUS: `r params$taulapos`
# - TAULA PROVES: `r params$taulaproves`
```

- Data Inici Curs: `r params$ini.curs`
- Data Fi Curs: `r params$fi.curs`

- Positiu Prova Variable: **`r params$cas_var`**

- Filtrar CAS PREVI: `r params$flt.cas_previ`
- Filtrar URBÀ: `r params$flt.medea.U`
- Filtrar RURAL: `r params$flt.medea.R`

***

### Actualització Informe

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

***

```{r parametres markdown}
inici <- Sys.time()

setwd("D:/SISAP/sisap/66Analisis/covid19/mascaretes/data/")

nfetch <- -1
rowid.var <- FALSE
savehashrds.var <- FALSE
saverds.var <- FALSE

warning.var <- FALSE
message.var <- FALSE

eval.library.var <- TRUE
eval.functions.var <- FALSE
eval.import.var <- TRUE
eval.datamanager.var <- TRUE
eval.flowchart.var <- TRUE

eval.brots.var <- TRUE
eval.attackrateproves.var <- FALSE
eval.attackrate.var <- TRUE
eval.attackrate2.var <- FALSE

eval.univariada.var <- FALSE

eval.corbes.var <- TRUE
eval.analisi.var <- FALSE
eval.incidencia.var <- TRUE

eval.annex.var <- FALSE # Tarda mucho en ejecutarse!!!

eval.timeexecution.var <- TRUE

var.fig.height <- 4
var.fig.width <- 6
```

```{r parametres estudi}
suppressWarnings(suppressPackageStartupMessages(library('lubridate')))

ini.curs <- params$ini.curs
fi.curs <- params$fi.curs

# Trimestres
  t1 <- c(ini.curs, as.Date(paste0("2020-12-21")))
  t2 <- c(as.Date(paste0("2021-01-11")), as.Date(paste0("2021-03-26")))
  t3 <- c(as.Date(paste0("2021-04-06")), as.Date(paste0("2021-06-22")))
  t4 <- c(as.Date("2021-09-13"), as.Date("2021-12-22"))
  t5 <- c(as.Date("2022-01-10"), as.Date("2022-02-10"))
```

```{r library, child="library.Rmd", eval=eval.library.var}

```

```{r functions, child="functions.Rmd", eval=eval.functions.var}

```

# Metodologia

## Criteris d'inclusió

## Període d'inclusió

***

```{r import, child="import.Rmd", eval=eval.import.var}

```

```{r datamanager, child="datamanager.Rmd", eval=eval.datamanager.var}

```

```{r flowchart, child="flowchart.Rmd", eval=eval.flowchart.var}

```

```{r brots, child="brots.Rmd", eval=eval.brots.var}

```

```{r attack rate proves, child="provesattackrate.Rmd", eval=eval.attackrateproves.var}

```

```{r attack rate, child="attackrate.Rmd", eval=eval.attackrate.var}

```

```{r attack rate 2, child="attackrate2.Rmd", eval=eval.attackrate2.var}
# Taxa d'attack de covid amb la fòrmula npos-1 / nproves-1
```

```{r descriptiva univariada, child="descriptiva_univariada.Rmd", eval=eval.univariada.var}

```

```{r corbes, child="corbes.Rmd", eval=eval.corbes.var}

```

```{r analisi, child="incidencia.Rmd", eval=eval.incidencia.var}

```

```{r annex, child="annex.Rmd", eval=eval.annex.var}

```

---

```{r time execution, eval=eval.timeexecution.var}

final <- Sys.time()
print(round(final - inici,2))
```
