***
***

# Data Manager

## Curs 2020-2021

### Afegir nova informació

Afegir a la cohort la informació socioeconòmica i d'infecció.

####  Infeccions

S'afegeix la informació d'infecció a la cohort.

```{r}
setkey(dt1, 'persona_id')
setkey(dt1.pos, 'persona_id')
dt1 <- merge(dt1,
             dt1.pos,
             by.x = 'persona_id',
             by.y = 'persona_id',
             all.x = TRUE)
```

####  Indicadors Socioeconòmics

S'afegeix la informació socioeconòmica.

```{r}
setkey(dt1, 'up')
setkey(dt.i, 'up')
dt1 <- merge(dt1,
             dt.i,
             by.x = 'up',
             by.y = 'up',
             all.x = TRUE)
#dt.aquas <- dt[, j=list(n=.N, aquas_m=mean(aquas)), .(up)]
#dt.aquas[is.na(aquas_m),]
# dt

dt1.dm <- dt1
```

Hi ha UP's que no tenen informació dels indicadors socioeconòmics.

Llista de UP's sense MEDEA/AQUAS

```{r}
dt1[is.na(medea_c), .N, by=.(up)][order(up)]
```

### Creació noves variables

Creació de les noves variables.

```{r, eval=FALSE}
#### ANY CURS (Curs segons any de naixement)

#Aquesta variable es el curs del nen segons el seu any de naixement.

# ANY CURS
  dt1.dm[any_curs %in% c("P4", "P5"), any_curs_c1 := 0][any_curs %in% c("PRIMER", "SEGON"), any_curs_c1 := 1]
  dt1.dm[, any_curs_c1:=factor(any_curs_c1, labels=c("P4-P5","PRIMER-SEGON"))]
  #datatable(dt1.dm[, .N, by=.(any_curs_c1)])
  
# ANY_CURS segons CURS
  dt1.dm[curs %in% c("EINFLOE 2C 1") & any_curs=="P3", any_curs_curs := "EINFLOE 2C 1"]
  dt1.dm[curs %in% c("EINFLOE 2C 1") & any_curs!="P3", any_curs_curs := "NO EINFLOE 2C 1"]
  dt1.dm[curs %in% c("EINFLOE 2C 1") & any_curs=="P4", any_curs_curs := "EINFLOE 2C 1"]
  
  dt1.dm[curs %in% c("EINFLOE 2C 2") & any_curs=="P4", any_curs_curs := "EINFLOE 2C 2"]
  dt1.dm[curs %in% c("EINFLOE 2C 2") & any_curs!="P4", any_curs_curs := "NO EINFLOE 2C 2"]
  dt1.dm[curs %in% c("EINFLOE 2C 2") & any_curs=="P5", any_curs_curs := "EINFLOE 2C 2"]
  
  dt1.dm[curs %in% c("EINFLOE 2C 3") & any_curs=="P5", any_curs_curs := "EINFLOE 2C 3"]
  dt1.dm[curs %in% c("EINFLOE 2C 3") & any_curs!="P5", any_curs_curs := "NO EINFLOE 2C 3"]
  dt1.dm[curs %in% c("EINFLOE 2C 3") & any_curs=="PRIMER", any_curs_curs := "EINFLOE 2C 3"]
  
  dt1.dm[curs %in% c("EPRILOE 1") & any_curs=="PRIMER", any_curs_curs := "EPRILOE 1"]
  dt1.dm[curs %in% c("EPRILOE 1") & any_curs!="PRIMER", any_curs_curs := "NO EPRILOE 1"]
  dt1.dm[curs %in% c("EPRILOE 1") & any_curs=="SEGON", any_curs_curs := "EPRILOE 1"]
  
  dt1.dm[curs %in% c("EPRILOE 2") & any_curs=="SEGON", any_curs_curs := "EPRILOE 2"]
  dt1.dm[curs %in% c("EPRILOE 2") & any_curs!="SEGON", any_curs_curs := "NO EPRILOE 2"]
  dt1.dm[curs %in% c("EPRILOE 2") & any_curs=="TERCER", any_curs_curs := "EPRILOE 2"]
  
  dt1.dm[curs %in% c("EPRILOE 3") & any_curs=="TERCER", any_curs_curs := "EPRILOE 3"]
  dt1.dm[curs %in% c("EPRILOE 3") & any_curs!="TERCER", any_curs_curs := "NO EPRILOE 3"]
  dt1.dm[curs %in% c("EPRILOE 3") & any_curs=="QUART", any_curs_curs := "EPRILOE 3"]
  
  dt1.dm[curs %in% c("EPRILOE 4") & any_curs=="QUART", any_curs_curs := "EPRILOE 4"]
  dt1.dm[curs %in% c("EPRILOE 4") & any_curs!="QUART", any_curs_curs := "NO EPRILOE 4"]
  dt1.dm[curs %in% c("EPRILOE 4") & any_curs=="CINQUE", any_curs_curs := "EPRILOE 4"]
  
  dt1.dm[curs %in% c("EPRILOE 5") & any_curs=="CINQUE", any_curs_curs := "EPRILOE 5"]
  dt1.dm[curs %in% c("EPRILOE 5") & any_curs!="CINQUE", any_curs_curs := "NO EPRILOE 5"]
  dt1.dm[curs %in% c("EPRILOE 5") & any_curs=="SISE", any_curs_curs := "EPRILOE 5"]
  
  dt1.dm[curs %in% c("EPRILOE 6") & any_curs=="SISE", any_curs_curs := "EPRILOE 6"]
  dt1.dm[curs %in% c("EPRILOE 6") & any_curs!="SISE", any_curs_curs := "NO EPRILOE 6"]
  table(dt1.dm[,.(any_curs_curs)])
  table(dt1.dm[,.(curs,any_curs)])
```

#### CURS

Creació variable curs que junta la informació dels cursos P4, P5 i la informació de Primer-Segon.

```{r}
dt1.dm[curs %in% c("EINFLOE 2C 2", "EINFLOE 2C 3"), curs_c1 := 0][curs %in% c("EPRILOE 1", "EPRILOE 2"), curs_c1 := 1]
dt1.dm[, curs_c1:=factor(curs_c1, labels=c("EINFLOE 2C 2-3","EPRILOE 1-2"))]
#datatable(dt1.dm[, .N, by=.(curs_c1)])
```

```{r}
dt1.dm[, curs_any := "2020-2021"]
```

***

#### EDAT

Creació variable **Edat**

```{r}
# Edat per RDD

dt1.dm[, edat_continuous :=  as.numeric(as.Date("2020-12-31") - data_naix)]
dt1.dm[, edat_continuous_meses := edat_continuous/30.5]

borrar <- dt1.dm[, .N, .(curs,edat_continuous_meses)]
```



```{r}
dt1.dm[, edat := year(ini.curs) - year(data_naix)]
dt1.dm[, .N, by=.(edat)][order(edat),]
dt1.dm[, j=list(.N, media = mean(edat), mediana=median(edat)), by=.(curs)][,media:=round(media,1)][order(curs),]
```

Creació variable **Mes de naixement**

```{r}
dt1.dm[, edat_m := format(data_naix, "%m")]
#a <- dt1.dm[, .N, by=.(curs,edat_m)]
```

***

#### MEDEA

Creació variable de recodificacions de I. Medea.

```{r}
# MEDEA
  # dt.medea <- dt[, .N, .(medea_c1)] dt[, .N, .(medea_c)]
    dt1.dm[, medea_c1 := ifelse(medea_c %in% c('0R', '1R', '2R'), "0-2R", medea_c)][is.na(medea_c), medea_c1:= NA]

  # RURAL
    dt1.dm[, rural := ifelse(medea_c %in% c('0R', '1R', '2R'), "Sí", "No")][is.na(medea_c), rural:= NA]
    #dt.medea <- dt[, .N, .(rural, medea_c)]
```

***

#### CAS

Els positius posteriors a ala data d'execució del informe es pasen a NA.

```{r}
#dt1.dm[pdia_primer_positiu > Sys.Date(), pdia_primer_positiu := NA]
dt1.dm[data_prova > Sys.Date(), data_prova := NA]
```

Creació variable de CAS.

```{r}

dt1.dm[, cas := ifelse(!is.na(eval(as.name(params$cas_var))), "Sí", "No")]
```

***

##### CAS any escolar

Creació de la variable CAS de l'estudi, es a dir, de l'any escolar: `r paste0('(', ini.curs, ' - ', fi.curs, ')')`

```{r}

dt1.dm[, cas_curs := "No"]
dt1.dm[eval(as.name(params$cas_var)) >= as.Date("2020-09-14") & eval(as.name(params$cas_var)) <= as.Date("2021-06-22") + 10, cas_curs := "Sí"]
# Validacio: table(dt1.dm[,.(cas, cas_curs)])

# dt1.dm[, cas_curs_pdia := "No"]
# dt1.dm[pdia_primer_positiu >= ini.curs & pdia_primer_positiu <= as.Date("2021-06-22"), cas_curs_pdia := "Sí"]
# dt1.dm[, cas_curs_prova := "No"]
# dt1.dm[data_prova >= ini.curs & data_prova <= as.Date("2021-06-22"), cas_curs_prova := "Sí"]
# table(dt1.dm[,.(cas_curs_pdia, cas_curs_prova)])
# table(dt1.dm[,.(cas_curs, cas_curs_pdia)])
# table(dt1.dm[,.(cas_curs, cas_curs_prova)])
```

```{r, eval=FALSE}
#### Ingrés Hospitalari
dt1.dm[, ingres_primer_cat := ifelse(!is.na(ingres_primer), "Sí", "No")]
```

```{r, eval=FALSE}
#### Ingrés UCI
dt1.dm[, ingres_uci_primer_cat := ifelse(!is.na(ingres_uci_primer), "Sí", "No")]
```

```{r, eval=FALSE}
#### Exitus COVID
dt1.dm[, exitus_covid_cat := ifelse(!is.na(exitus_covid), "Sí", "No")]
```

***

#### Nivell Escola

Creació de variables nivell Escola.

```{r}
dt1.escola <- dt1.dm[, j=list(#esc_nanycurs = uniqueN(any_curs),
                              esc_ncurs = uniqueN(curs),
                              esc_ngce = uniqueN(gce_id),
                              esc_nnens = .N,
                              #esc_ncens = sum(cens_escola=="1")/.N,
                              #esc_nindbs = round(sum(in_dbs=="1")/.N,2),
                              esc_ncas_estudi = round(sum(eval(as.name(params$cas_var)) >= ini.curs, na.rm = TRUE)/.N,2),
                              #esc_ncas_previ = round(sum(pdia_primer_positiu < ini.curs, na.rm = TRUE)/.N,2), 
                              esc_p_urba = round((sum(medea_c1 %in% c('1U','2U','3U','4U'), na.rm = TRUE)/.N)*100,0)),
                    by= escola_id]

dt1.escola <- dt1.escola[, esc_urba:='No'][esc_p_urba>=50, esc_urba:='Sí']
# datatable(dt1.escola,
#           filter = "top")
```

S'han creat les següents variables: `r names(dt1.escola)`

```{r}

theme_set(theme_minimal() + theme(legend.position = 'bottom'
                                  #legend.title=element_blank(),
                                  # axis.title.x=element_blank())
                                  ))
gg <- ggplot(dt1.escola, aes(esc_p_urba)) + 
        geom_bar(fill="dark green", alpha = 0.5) +
        labs(x = "Percentatge alumnes 'urbans' (%)",
             y = "Número d'escoles")
gg
```

####  Nivell Curs

Creació de variables nivell Curs (Escola/Curs).

```{r}
dt1.escolacurs <- dt1.dm[, j=list(curs_nnens = .N,
                                 #curs_ncens = sum(cens_escola=="1")/.N,
                                 #curs_nanycurs = round(sum(in_dbs=="1")/.N,2),
                                 #curs_nindbs = round(sum(in_dbs=="1")/.N,2),
                                 curs_pcas_estudi = round(sum(eval(as.name(params$cas_var)) >= as.Date("2020-09-14"), na.rm = TRUE)/.N,2)
                                 #curs_pcas_previ = round(sum(pdia_primer_positiu < ini.curs, na.rm = TRUE)/.N,2)
                                 ), 
                   by= .(escola_id, curs)]
# datatable(dt1.escolacurs,
#           filter = "top")
```

S'han creat les següents variables: `r names(dt1.escolacurs)`

#### Nivell GCE

Creació de variables nivell GCE (Escola/Curs/GCE).

```{r}

dt1.gce <- dt1.dm[, j=list(gce_nesc = uniqueN(escola_id),
                           gce_ncurs = uniqueN(curs)
                           ),
                    by= .(gce_id)]

dt1.escolacursgce <- dt1.dm[, j=list(gce_nnens = .N,
                                     #gce_ncursok = sum(curs==any_curs_curs),
                                     #gce_nindbs = sum(in_dbs=="1"),
                                     #gce_pindbs = round(sum(in_dbs=="1")/.N,2),
                                     gce_ncas_estudi = sum(eval(as.name(params$cas_var)) >= as.Date("2020-09-14"), na.rm = TRUE),
                                     gce_pcas_estudi = round(sum(eval(as.name(params$cas_var)) >= as.Date("2020-09-14"), na.rm = TRUE)/.N,2)
                                     ),
                                     #gce_ncas_previ = sum(pdia_primer_positiu < as.Date("2020-09-14"), na.rm = TRUE),
                                     #gce_pcas_previ = round(sum(pdia_primer_positiu < as.Date("2020-09-14"), na.rm = TRUE)/.N,2)), 
                   by= .(escola_id, curs, gce_id)]

dt1.escolacursgce <- merge(dt1.escolacursgce,
                           dt1.gce,
                           by.x = "gce_id",
                           by.y = "gce_id")

dt1.escolacursgce[, gce_nnens_c1:=as.numeric(gce_nnens)][as.numeric(gce_nnens)>=5, gce_nnens_c1:=5]
dt1.escolacursgce[, gce_nnens_c1:=factor(gce_nnens_c1, labels=c("1","2","3","4",">=5"))]

dt1.escolacursgce[, gce_ncas_estudi_c1:=gce_ncas_estudi][gce_ncas_estudi>=5, gce_ncas_estudi_c1:=5]
dt1.escolacursgce[, gce_ncas_estudi_c1:=factor(gce_ncas_estudi_c1, labels=c("0","1","2","3","4",">=5"))]

#dt1.escolacursgce[, gce_pcursok:=round((gce_ncursok/gce_nnens)*100,2)]

dt1.escolacursgce <- dt1.escolacursgce[,.(escola_id, curs, gce_id,
                                          gce_nesc, gce_ncurs,
                                          gce_nnens, gce_nnens_c1, #gce_ncursok, gce_pcursok,
                                          #gce_nindbs, gce_pindbs,
                                          gce_ncas_estudi, gce_ncas_estudi_c1, gce_pcas_estudi)]
                                          #gce_ncas_previ, gce_pcas_previ)]

# datatable(dt.escolacursgce,
#           filter = "top")
```

S'han creat les següents variables: `r names(dt1.escolacursgce)`

A continuació s'explora el **número de cursos per GCE**.

```{r}
theme_set(theme_minimal() + theme(legend.position = 'bottom'
                                  #legend.title=element_blank(),
                                  # axis.title.x=element_blank())
                                  ))
gg <- ggplot(dt1.escolacursgce, aes(factor(gce_ncurs))) + 
        geom_bar(aes(y = round(((..count..)/sum(..count..))*100,1)), fill="dark green", alpha=0.5) +
        labs(x = "Número de cursos per GCE",
             y = "Percentatge de GCE (%)")
gg
```

```{r}
df <- dt1.escolacursgce[, gce_ncurs_c := factor(gce_ncurs)]
res <- compareGroups(curs ~ gce_ncurs + gce_ncurs_c
                     , df
                     , method = 1
                     , max.xlev = 100
                     , max.ylev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT, format = "html")

df <- dt1.dm[, j=list(gce_nesc = uniqueN(escola_id),
                      gce_ncurs = uniqueN(curs)
                      ),
                    by= .(medea_c, gce_id)][, gce_ncurs_c := factor(gce_ncurs)]
res <- compareGroups(medea_c ~ gce_ncurs + gce_ncurs_c
                     , df
                     , method = 1
                     , max.xlev = 100
                     , max.ylev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT, format = "html")
```

***

A continuació s'explora el **número de pacients per GCE** estratificat per curs.

```{r}
df <- dt1.escolacursgce[, gce_nnens:=factor(gce_nnens)]
res <- compareGroups(curs ~ gce_nnens
                     , df
                     , method = 1
                     , max.xlev = 100
                     , max.ylev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT, format = "html")
```

```{r}
# df <- dt1.escolacursgce[((curs=='EINFLOE 2C 3') |
#                         (curs=='EPRILOE 1')),]
# res <- compareGroups(gce_nnens_c1 ~ gce_ncas_estudi
#                      , df
#                      , method = 1
#                      , max.xlev = 100
#                      , max.ylev = 100
#                      , include.miss = TRUE)
# cT <- createTable(res
#                   , show.descr = TRUE
#                   , show.all = FALSE
#                   , show.p.overall = FALSE
#                   , show.n = TRUE
#                   )
# export2md(cT, format = "html")
# export2md(strataTable(cT, "curs"))
# 
# df <- dt1.escolacursgce[((curs=='EINFLOE 2C 3') |
#                         (curs=='EPRILOE 1')),]
# res <- compareGroups(gce_nnens_c1 ~ gce_ncas_estudi_c1
#                      , df
#                      , method = 1
#                      , max.xlev = 100
#                      , max.ylev = 100
#                      , include.miss = TRUE)
# cT <- createTable(res
#                   , show.descr = TRUE
#                   , show.all = FALSE
#                   , show.p.overall = FALSE
#                   , show.n = TRUE
#                   )
# export2md(cT, format = "html")
# export2md(strataTable(cT, "curs"))
```

***

### Afegir nivell Escola, Curs i GCE

####  Nivell Escola

S'afegeix la informació del nivell Escola a la taula nivel de pacient.

```{r}
setkey(dt1.escola, 'escola_id')
setkey(dt1.dm, 'escola_id')
dt1.dm <- merge(dt1.dm,
                dt1.escola[escola_id!="",],
                by.x = 'escola_id',
                by.y = 'escola_id',
                all.x = TRUE)
```

####  Nivell Curs

S'afegeix la informació del nivell Curs (Escola/Curs) a la taula nivel de pacient.

```{r}
setkeyv(dt1.escolacurs, c('escola_id','curs'))
setkeyv(dt1.dm, c('escola_id','curs'))
dt1.dm <- merge(dt1.dm,
                dt1.escolacurs[escola_id!="",],
                by.x = c('escola_id','curs'),
                by.y = c('escola_id','curs'),
                all.x = TRUE)
```

####  Nivell GCE

S'afegeix la informació del nivell GCE (Escola/Curs/GCE) a la taula nivel de pacient.

```{r}
setkeyv(dt1.escolacursgce, c('escola_id','curs','gce_id'))
setkeyv(dt1.dm, c('escola_id','curs','gce_id'))
dt1.dm <- merge(dt1.dm,
                dt1.escolacursgce[escola_id!="",],
                by.x = c('escola_id','curs','gce_id'),
                by.y = c('escola_id','curs','gce_id'),
                all.x = TRUE)
```


```{r}

# setnames(dt1.dm, "cas_curs", "cas_curs_old")
# setnames(dt1.dm, "casdataprova_curs", "cas_curs")
dt1.dm <- dt1.dm[,.(curs_any,
                    escola_id, # esc_nanycurs, esc_nindbs, esc_ncas_previ,
                    esc_ncurs, esc_ngce, esc_nnens, esc_ncas_estudi, esc_p_urba, esc_urba,
                    curs, # curs_nindbs, curs_pcas_previ, any_curs, any_curs_c1, any_curs_curs,
                    curs_c1, curs_nnens, 
                    curs_pcas_estudi, 
                    gce_id, # gce_ncursok, gce_pcursok, gce_nindbs, gce_ncas_previ, gce_pcas_previ,
                    gce_ncurs, gce_nnens, gce_nnens_c1,
                    gce_ncas_estudi, gce_ncas_estudi_c1, gce_pcas_estudi, 
                    #municipi,
                    up, 
                    rural, medea_c, medea_c1, aquas,
                    persona_id, hash, 
                    data_naix, 
                    edat, edat_m, edat_continuous, edat_continuous_meses,
                    sexe,
                    #gma_codi, gma_complexitat, 
                    #pdia_primer_positiu, 
                    data_prova, data_resultat,
                    cas, cas_curs
                    #ingres_primer, ingres_primer_cat,
                    #ingres_ultim, ingres_uci_primer_cat,
                    #ingres_uci_primer, ingres_uci_ultim, 
                    #exitus_covid, exitus_covid_cat,
                    #in_dbs,
                    #fr_dm_data, fr_asma_data, fr_obes_data, fr_dm, fr_asma, fr_obes
                   )]
```

```{r, eval=FALSE}
### Proves

S'afegeixen les columnes any_curs, escola_id, curs i gce_id a la taules de proves.

**Nota: Tots els registres de proves tenen registre a la taula de nens.**

```{r}

# setkey(dt.proves, "hash")
# setkey(dt1, "hash")
# dt.proves.dm <- merge(dt.proves,
#                       dt[, .(hash, any_curs, escola_id, curs, gce_id)],
#                       by.x = 'hash',
#                       by.y = 'hash',
#                       all.x = TRUE)


#### Nivell Escola


# dt.proves.agr.escola <- dt.proves.dm[, j=list(esc_nproves = .N,
#                                               esc_nprovespcr = sum(tipus_prova=="PCR"),
#                                               esc_nprovestar = sum(tipus_prova=="Antigen")),
#                    by= escola_id]
# #datatable(dt.proves.agr.escola,
# #          filter = "top")


#### Nivell Escola/Curs


# dt.proves.agr.escolacurs <- dt.proves.dm[, j=list(curs_nproves = .N,
#                                                   curs_nprovespcr = sum(tipus_prova=="PCR"),
#                                                   curs_nprovestar = sum(tipus_prova=="Antigen")), 
#                    by= .(escola_id, curs)]


#### Nivell Escola/Curs/GCE


# dt.proves.agr.escolacursgce <- dt.proves.dm[, j=list(gce_nproves = .N,
#                                                      gce_nprovespcr = sum(tipus_prova=="PCR"),
#                                                      gce_nprovestar = sum(tipus_prova=="Antigen")), 
#                    by= .(escola_id, curs, gce_id)]
```

***
***

## Curs 2021-2022

### Afegir nova informació

Afegir a la cohort la informació socioeconòmica i d'infecció.

####  Infeccions

S'afegeix la informació d'infecció a la cohort.

```{r}
setkey(dt2, 'persona_id')
setkey(dt2.pos, 'persona_id')
dt2 <- merge(dt2,
             dt2.pos,
             by.x = 'persona_id',
             by.y = 'persona_id',
             all.x = TRUE)
```

####  Indicadors Socioeconòmics

S'afegeix la informació socioeconòmica.

```{r}
setkey(dt2, 'up')
setkey(dt.i, 'up')
dt2 <- merge(dt2,
             dt.i,
             by.x = 'up',
             by.y = 'up',
             all.x = TRUE)
#dt.aquas <- dt[, j=list(n=.N, aquas_m=mean(aquas)), .(up)]
#dt.aquas[is.na(aquas_m),]
# dt

dt2.dm <- dt2
```

Hi ha UP's que no tenen informació dels indicadors socioeconòmics.

Llista de UP's sense MEDEA/AQUAS

```{r}
dt2[is.na(medea_c), .N, by=.(up)][order(up)]
```

### Creació noves variables

Creació de les noves variables.

```{r, eval=FALSE}
#### ANY CURS (Curs segons any de naixement)

#Aquesta variable es el curs del nen segons el seu any de naixement.

# ANY CURS
  dt2.dm[any_curs %in% c("P4", "P5"), any_curs_c1 := 0][any_curs %in% c("PRIMER", "SEGON"), any_curs_c1 := 1]
  dt2.dm[, any_curs_c1:=factor(any_curs_c1, labels=c("P4-P5","PRIMER-SEGON"))]
  #datatable(dt2.dm[, .N, by=.(any_curs_c1)])
  
# ANY_CURS segons CURS
  dt2.dm[curs %in% c("EINFLOE 2C 1") & any_curs=="P3", any_curs_curs := "EINFLOE 2C 1"]
  dt2.dm[curs %in% c("EINFLOE 2C 1") & any_curs!="P3", any_curs_curs := "NO EINFLOE 2C 1"]
  dt2.dm[curs %in% c("EINFLOE 2C 1") & any_curs=="P4", any_curs_curs := "EINFLOE 2C 1"]
  
  dt2.dm[curs %in% c("EINFLOE 2C 2") & any_curs=="P4", any_curs_curs := "EINFLOE 2C 2"]
  dt2.dm[curs %in% c("EINFLOE 2C 2") & any_curs!="P4", any_curs_curs := "NO EINFLOE 2C 2"]
  dt2.dm[curs %in% c("EINFLOE 2C 2") & any_curs=="P5", any_curs_curs := "EINFLOE 2C 2"]
  
  dt2.dm[curs %in% c("EINFLOE 2C 3") & any_curs=="P5", any_curs_curs := "EINFLOE 2C 3"]
  dt2.dm[curs %in% c("EINFLOE 2C 3") & any_curs!="P5", any_curs_curs := "NO EINFLOE 2C 3"]
  dt2.dm[curs %in% c("EINFLOE 2C 3") & any_curs=="PRIMER", any_curs_curs := "EINFLOE 2C 3"]
  
  dt2.dm[curs %in% c("EPRILOE 1") & any_curs=="PRIMER", any_curs_curs := "EPRILOE 1"]
  dt2.dm[curs %in% c("EPRILOE 1") & any_curs!="PRIMER", any_curs_curs := "NO EPRILOE 1"]
  dt2.dm[curs %in% c("EPRILOE 1") & any_curs=="SEGON", any_curs_curs := "EPRILOE 1"]
  
  dt2.dm[curs %in% c("EPRILOE 2") & any_curs=="SEGON", any_curs_curs := "EPRILOE 2"]
  dt2.dm[curs %in% c("EPRILOE 2") & any_curs!="SEGON", any_curs_curs := "NO EPRILOE 2"]
  dt2.dm[curs %in% c("EPRILOE 2") & any_curs=="TERCER", any_curs_curs := "EPRILOE 2"]
  
  dt2.dm[curs %in% c("EPRILOE 3") & any_curs=="TERCER", any_curs_curs := "EPRILOE 3"]
  dt2.dm[curs %in% c("EPRILOE 3") & any_curs!="TERCER", any_curs_curs := "NO EPRILOE 3"]
  dt2.dm[curs %in% c("EPRILOE 3") & any_curs=="QUART", any_curs_curs := "EPRILOE 3"]
  
  dt2.dm[curs %in% c("EPRILOE 4") & any_curs=="QUART", any_curs_curs := "EPRILOE 4"]
  dt2.dm[curs %in% c("EPRILOE 4") & any_curs!="QUART", any_curs_curs := "NO EPRILOE 4"]
  dt2.dm[curs %in% c("EPRILOE 4") & any_curs=="CINQUE", any_curs_curs := "EPRILOE 4"]
  
  dt2.dm[curs %in% c("EPRILOE 5") & any_curs=="CINQUE", any_curs_curs := "EPRILOE 5"]
  dt2.dm[curs %in% c("EPRILOE 5") & any_curs!="CINQUE", any_curs_curs := "NO EPRILOE 5"]
  dt2.dm[curs %in% c("EPRILOE 5") & any_curs=="SISE", any_curs_curs := "EPRILOE 5"]
  
  dt2.dm[curs %in% c("EPRILOE 6") & any_curs=="SISE", any_curs_curs := "EPRILOE 6"]
  dt2.dm[curs %in% c("EPRILOE 6") & any_curs!="SISE", any_curs_curs := "NO EPRILOE 6"]
  table(dt2.dm[,.(any_curs_curs)])
  table(dt2.dm[,.(curs,any_curs)])
```

#### CURS

Creació variable curs que junta la informació dels cursos P4, P5 i la informació de Primer-Segon.

```{r}
dt2.dm[curs %in% c("EINFLOE 2C 2", "EINFLOE 2C 3"), curs_c1 := 0][curs %in% c("EPRILOE 1", "EPRILOE 2"), curs_c1 := 1]
dt2.dm[, curs_c1:=factor(curs_c1, labels=c("EINFLOE 2C 2-3","EPRILOE 1-2"))]
#datatable(dt2.dm[, .N, by=.(curs_c1)])
```


```{r}
dt2.dm[, curs_any := "2021-2022"]
```

***

#### EDAT

Creació variable **Edat**


```{r}
# Edat per RDD

dt2.dm[, edat_continuous :=  as.numeric(as.Date("2021-12-31") - data_naix)]
dt2.dm[, edat_continuous_meses := edat_continuous/30.5]
```

```{r}
dt2.dm[, edat := year(as.Date("2021-09-13")) - year(data_naix)]
dt2.dm[, .N, by=.(edat)][order(edat),]
dt2.dm[, j=list(.N, media = mean(edat), mediana=median(edat)), by=.(curs)][,media:=round(media,1)][order(curs),]
```

Creació variable **Mes de naixement**

```{r}
dt2.dm[, edat_m := format(data_naix, "%m")]
#a <- dt2.dm[, .N, by=.(curs,edat_m)]
```

***

#### MEDEA

Creació variable de recodificacions de I. Medea.

```{r}
# MEDEA
  # dt.medea <- dt[, .N, .(medea_c1)] dt[, .N, .(medea_c)]
    dt2.dm[, medea_c1 := ifelse(medea_c %in% c('0R', '1R', '2R'), "0-2R", medea_c)][is.na(medea_c), medea_c1:= NA]

  # RURAL
    dt2.dm[, rural := ifelse(medea_c %in% c('0R', '1R', '2R'), "Sí", "No")][is.na(medea_c), rural:= NA]
    #dt.medea <- dt[, .N, .(rural, medea_c)]
```

***

#### CAS

Els positius posteriors a ala data d'execució del informe es pasen a NA.

```{r}
#dt2.dm[pdia_primer_positiu > Sys.Date(), pdia_primer_positiu := NA]
dt2.dm[data_prova > Sys.Date(), data_prova := NA]
```

```{r}

dt2.dm[, cas := ifelse(!is.na(eval(as.name(params$cas_var))), "Sí", "No")]
```

***

##### CAS any escolar

Creació de la variable CAS de l'estudi, es a dir, de l'any escolar: `r paste0('(', ini.curs, ' - ', fi.curs, ')')`

```{r}

dt2.dm[, cas_curs := "No"]
dt2.dm[eval(as.name(params$cas_var)) >= as.Date("2021-09-13") & eval(as.name(params$cas_var)) <= as.Date("2021-12-22") + 10, cas_curs := "Sí"]
# Validacio: table(dt2.dm[,.(cas, cas_curs)])

# dt2.dm[, cas_curs_pdia := "No"]
# dt2.dm[pdia_primer_positiu >= ini.curs & pdia_primer_positiu <= as.Date("2021-06-22"), cas_curs_pdia := "Sí"]
# dt2.dm[, cas_curs_prova := "No"]
# dt2.dm[data_prova >= ini.curs & data_prova <= as.Date("2021-06-22"), cas_curs_prova := "Sí"]
# table(dt2.dm[,.(cas_curs_pdia, cas_curs_prova)])
# table(dt2.dm[,.(cas_curs, cas_curs_pdia)])
# table(dt2.dm[,.(cas_curs, cas_curs_prova)])
```

```{r, eval=FALSE}
#### Ingrés Hospitalari
dt2.dm[, ingres_primer_cat := ifelse(!is.na(ingres_primer), "Sí", "No")]
```

```{r, eval=FALSE}
#### Ingrés UCI
dt2.dm[, ingres_uci_primer_cat := ifelse(!is.na(ingres_uci_primer), "Sí", "No")]
```

```{r, eval=FALSE}
#### Exitus COVID
dt2.dm[, exitus_covid_cat := ifelse(!is.na(exitus_covid), "Sí", "No")]
```

***

#### Nivell Escola

Creació de variables nivell Escola.

```{r}
dt2.escola <- dt2.dm[, j=list(#esc_nanycurs = uniqueN(any_curs),
                              esc_ncurs = uniqueN(curs),
                              esc_ngce = uniqueN(gce_id),
                              esc_nnens = .N,
                              #esc_ncens = sum(cens_escola=="1")/.N,
                              #esc_nindbs = round(sum(in_dbs=="1")/.N,2),
                              esc_ncas_estudi = round(sum(eval(as.name(params$cas_var)) >= ini.curs, na.rm = TRUE)/.N,2),
                              #esc_ncas_previ = round(sum(pdia_primer_positiu < ini.curs, na.rm = TRUE)/.N,2), 
                              esc_p_urba = round((sum(medea_c1 %in% c('1U','2U','3U','4U'), na.rm = TRUE)/.N)*100,0)),
                    by= escola_id]

dt2.escola <- dt2.escola[, esc_urba:='No'][esc_p_urba>=50, esc_urba:='Sí']
# datatable(dt2.escola,
#           filter = "top")
```

S'han creat les següents variables: `r names(dt2.escola)`

```{r}

theme_set(theme_minimal() + theme(legend.position = 'bottom'
                                  #legend.title=element_blank(),
                                  # axis.title.x=element_blank())
                                  ))
gg <- ggplot(dt2.escola, aes(esc_p_urba)) + 
        geom_bar(fill="dark green", alpha = 0.5) +
        labs(x = "Percentatge alumnes 'urbans' (%)",
             y = "Número d'escoles")
gg
```

####  Nivell Curs

Creació de variables nivell Curs (Escola/Curs).

```{r}
dt2.escolacurs <- dt2.dm[, j=list(curs_nnens = .N,
                                 #curs_ncens = sum(cens_escola=="1")/.N,
                                 #curs_nanycurs = round(sum(in_dbs=="1")/.N,2),
                                 #curs_nindbs = round(sum(in_dbs=="1")/.N,2),
                                 curs_pcas_estudi = round(sum(eval(as.name(params$cas_var)) >= as.Date("2020-09-14"), na.rm = TRUE)/.N,2)
                                 #curs_pcas_previ = round(sum(pdia_primer_positiu < ini.curs, na.rm = TRUE)/.N,2)
                                 ), 
                   by= .(escola_id, curs)]
# datatable(dt2.escolacurs,
#           filter = "top")
```

S'han creat les següents variables: `r names(dt2.escolacurs)`

#### Nivell GCE

Creació de variables nivell GCE (Escola/Curs/GCE).

```{r}

dt2.gce <- dt2.dm[, j=list(gce_nesc = uniqueN(escola_id),
                           gce_ncurs = uniqueN(curs)
                           ),
                    by= .(gce_id)]

dt2.escolacursgce <- dt2.dm[, j=list(gce_nnens = .N,
                                     #gce_ncursok = sum(curs==any_curs_curs),
                                     #gce_nindbs = sum(in_dbs=="1"),
                                     #gce_pindbs = round(sum(in_dbs=="1")/.N,2),
                                     gce_ncas_estudi = sum(eval(as.name(params$cas_var)) >= as.Date("2020-09-14"), na.rm = TRUE),
                                     gce_pcas_estudi = round(sum(eval(as.name(params$cas_var)) >= as.Date("2020-09-14"), na.rm = TRUE)/.N,2)
                                     ),
                                     #gce_ncas_previ = sum(pdia_primer_positiu < as.Date("2020-09-14"), na.rm = TRUE),
                                     #gce_pcas_previ = round(sum(pdia_primer_positiu < as.Date("2020-09-14"), na.rm = TRUE)/.N,2)), 
                   by= .(escola_id, curs, gce_id)]

dt2.escolacursgce <- merge(dt2.escolacursgce,
                           dt2.gce,
                           by.x = "gce_id",
                           by.y = "gce_id")

dt2.escolacursgce[, gce_nnens_c1:=as.numeric(gce_nnens)][as.numeric(gce_nnens)>=5, gce_nnens_c1:=5]
dt2.escolacursgce[, gce_nnens_c1:=factor(gce_nnens_c1, labels=c("1","2","3","4",">=5"))]

dt2.escolacursgce[, gce_ncas_estudi_c1:=gce_ncas_estudi][gce_ncas_estudi>=5, gce_ncas_estudi_c1:=5]
dt2.escolacursgce[, gce_ncas_estudi_c1:=factor(gce_ncas_estudi_c1, labels=c("0","1","2","3","4",">=5"))]

#dt2.escolacursgce[, gce_pcursok:=round((gce_ncursok/gce_nnens)*100,2)]

dt2.escolacursgce <- dt2.escolacursgce[,.(escola_id, curs, gce_id,
                                          gce_nesc, gce_ncurs,
                                          gce_nnens, gce_nnens_c1, #gce_ncursok, gce_pcursok,
                                          #gce_nindbs, gce_pindbs,
                                          gce_ncas_estudi, gce_ncas_estudi_c1, gce_pcas_estudi)]
                                          #gce_ncas_previ, gce_pcas_previ)]

# datatable(dt.escolacursgce,
#           filter = "top")
```

S'han creat les següents variables: `r names(dt2.escolacursgce)`

A continuació s'explora el **número de cursos per GCE**.

```{r}
theme_set(theme_minimal() + theme(legend.position = 'bottom'
                                  #legend.title=element_blank(),
                                  # axis.title.x=element_blank())
                                  ))
gg <- ggplot(dt2.escolacursgce, aes(factor(gce_ncurs))) + 
        geom_bar(aes(y = round(((..count..)/sum(..count..))*100,1)), fill="dark green", alpha=0.5) +
        labs(x = "Número de cursos per GCE",
             y = "Percentatge de GCE (%)")
gg
```

```{r}
df <- dt2.escolacursgce[, gce_ncurs_c := factor(gce_ncurs)]
res <- compareGroups(curs ~ gce_ncurs + gce_ncurs_c
                     , df
                     , method = 1
                     , max.xlev = 100
                     , max.ylev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT, format = "html")

df <- dt2.dm[, j=list(gce_nesc = uniqueN(escola_id),
                      gce_ncurs = uniqueN(curs)
                      ),
                    by= .(medea_c, gce_id)][, gce_ncurs_c := factor(gce_ncurs)]
res <- compareGroups(medea_c ~ gce_ncurs + gce_ncurs_c
                     , df
                     , method = 1
                     , max.xlev = 100
                     , max.ylev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT, format = "html")
```

***

A continuació s'explora el **número de pacients per GCE estratificat** per curs.

```{r}
df <- dt2.escolacursgce[, gce_nnens:=factor(gce_nnens)]
res <- compareGroups(curs ~ gce_nnens
                     , df
                     , method = 1
                     , max.xlev = 100
                     , max.ylev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT, format = "html")
```

```{r}
# df <- dt2.escolacursgce[((curs=='EINFLOE 2C 3') |
#                         (curs=='EPRILOE 1')),]
# res <- compareGroups(gce_nnens_c1 ~ gce_ncas_estudi
#                      , df
#                      , method = 1
#                      , max.xlev = 100
#                      , max.ylev = 100
#                      , include.miss = TRUE)
# cT <- createTable(res
#                   , show.descr = TRUE
#                   , show.all = FALSE
#                   , show.p.overall = FALSE
#                   , show.n = TRUE
#                   )
# export2md(cT, format = "html")
# export2md(strataTable(cT, "curs"))
# 
# df <- dt2.escolacursgce[((curs=='EINFLOE 2C 3') |
#                         (curs=='EPRILOE 1')),]
# res <- compareGroups(gce_nnens_c1 ~ gce_ncas_estudi_c1
#                      , df
#                      , method = 1
#                      , max.xlev = 100
#                      , max.ylev = 100
#                      , include.miss = TRUE)
# cT <- createTable(res
#                   , show.descr = TRUE
#                   , show.all = FALSE
#                   , show.p.overall = FALSE
#                   , show.n = TRUE
#                   )
# export2md(cT, format = "html")
# export2md(strataTable(cT, "curs"))
```

***

### Afegir nivell Escola, Curs i GCE

####  Nivell Escola

S'afegeix la informació del nivell Escola a la taula nivel de pacient.

```{r}
setkey(dt2.escola, 'escola_id')
setkey(dt2.dm, 'escola_id')
dt2.dm <- merge(dt2.dm,
                dt2.escola[escola_id!="",],
                by.x = 'escola_id',
                by.y = 'escola_id',
                all.x = TRUE)
```

####  Nivell Curs

S'afegeix la informació del nivell Curs (Escola/Curs) a la taula nivel de pacient.

```{r}
setkeyv(dt2.escolacurs, c('escola_id','curs'))
setkeyv(dt2.dm, c('escola_id','curs'))
dt2.dm <- merge(dt2.dm,
                dt2.escolacurs[escola_id!="",],
                by.x = c('escola_id','curs'),
                by.y = c('escola_id','curs'),
                all.x = TRUE)
```

####  Nivell GCE

S'afegeix la informació del nivell GCE (Escola/Curs/GCE) a la taula nivel de pacient.

```{r}
setkeyv(dt2.escolacursgce, c('escola_id','curs','gce_id'))
setkeyv(dt2.dm, c('escola_id','curs','gce_id'))
dt2.dm <- merge(dt2.dm,
                dt2.escolacursgce[escola_id!="",],
                by.x = c('escola_id','curs','gce_id'),
                by.y = c('escola_id','curs','gce_id'),
                all.x = TRUE)
```


```{r}

# setnames(dt2.dm, "cas_curs", "cas_curs_old")
# setnames(dt2.dm, "casdataprova_curs", "cas_curs")
dt2.dm <- dt2.dm[,.(curs_any,
                    escola_id, # esc_nanycurs, esc_nindbs, esc_ncas_previ,
                    esc_ncurs, esc_ngce, esc_nnens, esc_ncas_estudi, esc_p_urba, esc_urba,
                    curs, # curs_nindbs, curs_pcas_previ, any_curs, any_curs_c1, any_curs_curs,
                    curs_c1, curs_nnens, 
                    curs_pcas_estudi, 
                    gce_id, # gce_ncursok, gce_pcursok, gce_nindbs, gce_ncas_previ, gce_pcas_previ,
                    gce_ncurs, gce_nnens, gce_nnens_c1,
                    gce_ncas_estudi, gce_ncas_estudi_c1, gce_pcas_estudi, 
                    #municipi,
                    up, 
                    rural, medea_c, medea_c1, aquas,
                    persona_id, hash, 
                    data_naix, 
                    edat, edat_m, edat_continuous, edat_continuous_meses,
                    sexe,
                    #gma_codi, gma_complexitat, 
                    #pdia_primer_positiu, 
                    data_prova, data_resultat,
                    cas, cas_curs
                    #ingres_primer, ingres_primer_cat,
                    #ingres_ultim, ingres_uci_primer_cat,
                    #ingres_uci_primer, ingres_uci_ultim, 
                    #exitus_covid, exitus_covid_cat,
                    #in_dbs,
                    #fr_dm_data, fr_asma_data, fr_obes_data, fr_dm, fr_asma, fr_obes
                   )]
```

```{r, eval=FALSE}
### Proves

S'afegeixen les columnes any_curs, escola_id, curs i gce_id a la taules de proves.

**Nota: Tots els registres de proves tenen registre a la taula de nens.**

```{r}

# setkey(dt.proves, "hash")
# setkey(dt2, "hash")
# dt.proves.dm <- merge(dt.proves,
#                       dt[, .(hash, any_curs, escola_id, curs, gce_id)],
#                       by.x = 'hash',
#                       by.y = 'hash',
#                       all.x = TRUE)


#### Nivell Escola


# dt.proves.agr.escola <- dt.proves.dm[, j=list(esc_nproves = .N,
#                                               esc_nprovespcr = sum(tipus_prova=="PCR"),
#                                               esc_nprovestar = sum(tipus_prova=="Antigen")),
#                    by= escola_id]
# #datatable(dt.proves.agr.escola,
# #          filter = "top")


#### Nivell Escola/Curs


# dt.proves.agr.escolacurs <- dt.proves.dm[, j=list(curs_nproves = .N,
#                                                   curs_nprovespcr = sum(tipus_prova=="PCR"),
#                                                   curs_nprovestar = sum(tipus_prova=="Antigen")), 
#                    by= .(escola_id, curs)]


#### Nivell Escola/Curs/GCE


# dt.proves.agr.escolacursgce <- dt.proves.dm[, j=list(gce_nproves = .N,
#                                                      gce_nprovespcr = sum(tipus_prova=="PCR"),
#                                                      gce_nprovestar = sum(tipus_prova=="Antigen")), 
#                    by= .(escola_id, curs, gce_id)]
```

***
***

## Proves

### Curs 2020-2021

S'afegeixen les columnes any_curs, escola_id, curs i gce_id a la taules de proves.

**Nota: Tots els registres de proves tenen registre a la taula de nens.**

```{r}

setkey(dt1.proves, "hash")
setkey(dt1.dm, "hash")
dt1.proves.dm <- merge(dt1.proves,
                      dt1.dm[, .(hash, curs_any, escola_id, curs, gce_id)],
                      by.x = 'hash',
                      by.y = 'hash',
                      all.x = TRUE)
#dt1.proves.dm <- dt1.proves
```

### Curs 2021-2022

S'afegeixen les columnes any_curs, escola_id, curs i gce_id a la taules de proves.

**Nota: Tots els registres de proves tenen registre a la taula de nens.**

```{r}

setkey(dt2.proves, "hash")
setkey(dt2.dm, "hash")
dt2.proves.dm <- merge(dt2.proves,
                      dt2.dm[, .(hash, curs_any, escola_id, curs, gce_id)],
                      by.x = 'hash',
                      by.y = 'hash',
                      all.x = TRUE)
#dt2.proves.dm <- dt2.proves
```

```{r}

dt1.proves.dm[, curs_any := "2020-2021"]
dt2.proves.dm[, curs_any := "2021-2022"]

dt.proves.dm <- rbind(dt1.proves.dm,
                      dt2.proves.dm)
```

```{r data manager gc}
gc.var <- gc()
```