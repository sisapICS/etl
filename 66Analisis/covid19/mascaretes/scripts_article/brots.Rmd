# Brots

A continuació es defineixen els brots, el cas index , data inici brot a nivel de GCE.

## Nou Mètode

- Seleccionem tots el positius del període: 
- Utilitzem una funció trobada a internet (create_date_periods: https://community.rstudio.com/t/grouping-dates-together-into-periods/65044/6)
- Establim el número de brot
- Creem la data inici de brot per a casa brot



```{r}

dt.dm_positiu.curs <- dt.flt.curs[cas_curs == "Sí", .(hash, curs_any, curs_brots, escola_id, gce_id, eval(as.name(params$cas_var)))]
setnames(dt.dm_positiu.curs, "V6", params$cas_var)
dt.dm_positiu.curs[, .(min(data_prova),max(data_prova)), .(curs_any)]

create_date_periods <- function(dates, time_period = 10) {
  # TODO: add some error checking
  
  # create a vector to hold the results
  return_vector <- structure(rep(NA_real_, length(dates)), class = "Date")
  
  # if any date in the vector is still missing, keep going
  while(any(is.na(return_vector))) {
    
    # set minimum date amongst the values that are missing
    min_date <- min(dates[is.na(return_vector)])
    
    # if the date falls in range of interest, set it to the minimum date
    return_vector[dates >= min_date & dates <= min_date + time_period] <- min_date
  }
  
  return(return_vector)
}

dt.dm_positiu.curs[, period_start := create_date_periods(data_prova, time_period = 10), by = "gce_id"]

dt.dm_positiu.curs <- dt.dm_positiu.curs[order(curs_any,gce_id,data_prova),][period_start==lag(period_start), period_start := NA, by=.(curs_any,gce_id)]

dt.dm_positiu.curs[, cas_inicial:=ifelse(data_prova==period_start,1,0)][is.na(cas_inicial), cas_inicial:=0]

dt.dm_positiu.curs[order(curs_any, eval(as.name(params$cas_var))), brot := cumsum(cas_inicial), .(curs_any,gce_id)]

dt.dm_positiu.curs[, data_inici_brot := min(eval(as.name(params$cas_var))), c("curs_any","gce_id", "brot")]

dt.dm_positiu.curs[curs_any=="2021-2022", .N, brot] # Hi ha valor BROT=0 (N=420) D3E7A157AC849CCBB9EE6778669DD887A73B7359, els paso a 1

dt.dm_positiu.curs[brot==0, brot:=1]

dt.dm_positiu.curs <- dt.dm_positiu.curs[(curs_any=='2020-2021' & data_inici_brot >= t1[1] & data_inici_brot <= t1[2]) | 
                                         (curs_any=='2020-2021' & data_inici_brot >= t2[1] & data_inici_brot <= t2[2]) |
                                         (curs_any=='2020-2021' & data_inici_brot >= t3[1] & data_inici_brot <= t3[2]) |
                                         (curs_any=='2021-2022' & data_inici_brot >= t4[1] & data_inici_brot <= t4[2]),]

calc_casos_coexistint <- function(dt, vars = c("curs_any", "gce_id", "escola_id", "data_inici_brot", "brot")){
  resultat <- dt[, .(
  alumnes_coexistint = .N
  ), by = vars]
}

brots_gce.curs <- calc_casos_coexistint(unique(dt.dm_positiu.curs[, c("hash", "curs_any", "gce_id", "escola_id", "data_inici_brot", "brot")]))
brots_gcecurs.curs <- calc_casos_coexistint(unique(dt.dm_positiu.curs[, c("hash", "curs_any", "gce_id", "escola_id", "curs_brots", "data_inici_brot", "brot")]),
                                       vars = c("curs_brots", "curs_any", "gce_id", "escola_id", "data_inici_brot", "brot"))

# Table 1 Article
gcecurs.curs <- dt.flt.curs[, .N, c("curs_any", "curs_brots", "gce_id", "escola_id")][, .N, c("curs_any","curs_brots")]
dt.table1.curs <- merge(gcecurs.curs,
                        brots_gcecurs.curs[, .(`Index cases` = .N,
                                               `N sense secondary cases` =sum(alumnes_coexistint == 1),
                                               `All cases` = sum(alumnes_coexistint)
                                               ), .(curs_any,curs_brots)],
                        by.x = c("curs_any", "curs_brots"),
                        by.y = c("curs_any", "curs_brots"))
#datatable(dt.table1.curs)

dt.table1.curs[, `% sense secondary cases`:=round((`N sense secondary cases`/`Index cases`)*100,1)]
dt.table1.curs[, `% with secondary cases`:=round(((`Index cases` - `N sense secondary cases`)/`Index cases`)*100,1)]

dt.table1.20.curs <- dcast(melt(dt.table1.curs[curs_any=='2020-2021',], id.vars = c("curs_brots")), variable ~ curs_brots)
dt.table1.21.curs <- dcast(melt(dt.table1.curs[curs_any=='2021-2022',], id.vars = c("curs_brots")), variable ~ curs_brots)
dt.table1.21.curs

# 95%CI No secondary cases
  dt.table1.curs <- dt.table1.curs[,`% sense secondary cases SD`:= sqrt(((`% sense secondary cases`)*(100-`% sense secondary cases`))/`Index cases`)]
  dt.table1.curs <- dt.table1.curs[,`% sense secondary cases LI`:=round((`% sense secondary cases`-(1.96*`% sense secondary cases SD`)),2)]
  dt.table1.curs <- dt.table1.curs[,`% sense secondary cases LS`:=round((`% sense secondary cases`+(1.96*`% sense secondary cases SD`)),2)]
  dt.table1.curs[curs_any=='2021-2022',]
  
# 95%CI With Secondary cases
  dt.table1.curs <- dt.table1.curs[,`% with secondary cases SD`:= sqrt(((`% with secondary cases`)*(100-`% with secondary cases`))/`Index cases`)]
  dt.table1.curs <- dt.table1.curs[,`% with secondary cases LI`:=round((`% with secondary cases`-(1.96*`% with secondary cases SD`)),2)]
  dt.table1.curs <- dt.table1.curs[,`% with secondary cases LS`:=round((`% with secondary cases`+(1.96*`% with secondary cases SD`)),2)]
  dt.table1.curs[curs_any=='2021-2022',]  
  #fwrite(dt.table1.curs[curs_any=='2021-2022',], "D:/SISAP/sisap/66Analisis/covid19/mascaretes/results/table_sensesecondatycases.csv")
  
# 95%CI No secondary cases Figure  
theme_set(theme_minimal() + theme(legend.position = 'bottom',
                                  legend.title=element_blank(),
                                  axis.title.x=element_blank()))
dt.table1.curs.plot <- dt.table1.curs[curs_any=="2021-2022",]
dt.table1.curs.plot[curs_brots=="EINFLOE 2C 1", curs_plot:="P3"]
dt.table1.curs.plot[curs_brots=="EINFLOE 2C 2", curs_plot:="P4"]
dt.table1.curs.plot[curs_brots=="EINFLOE 2C 3", curs_plot:="P5"]
dt.table1.curs.plot[curs_brots=="EPRILOE 1", curs_plot:="1"]
dt.table1.curs.plot[curs_brots=="EPRILOE 2", curs_plot:="2"]
dt.table1.curs.plot[curs_brots=="EPRILOE 3", curs_plot:="3"]
dt.table1.curs.plot[curs_brots=="EPRILOE 4", curs_plot:="4"]
dt.table1.curs.plot[curs_brots=="EPRILOE 5", curs_plot:="5"]
dt.table1.curs.plot[curs_brots=="EPRILOE 6", curs_plot:="6"]
dt.table1.curs.plot[, curs_plot:= factor(curs_plot, levels=c("P3","P4","P5","1","2","3","4","5", "6"))]

dt.table1.curs.plot[curs_brots=="EINFLOE 2C 1", curs_plotn:=1]
dt.table1.curs.plot[curs_brots=="EINFLOE 2C 2", curs_plotn:=2]
dt.table1.curs.plot[curs_brots=="EINFLOE 2C 3", curs_plotn:=3]
dt.table1.curs.plot[curs_brots=="EPRILOE 1", curs_plotn:=4]
dt.table1.curs.plot[curs_brots=="EPRILOE 2", curs_plotn:=5]
dt.table1.curs.plot[curs_brots=="EPRILOE 3", curs_plotn:=6]
dt.table1.curs.plot[curs_brots=="EPRILOE 4", curs_plotn:=7]
dt.table1.curs.plot[curs_brots=="EPRILOE 5", curs_plotn:=8]
dt.table1.curs.plot[curs_brots=="EPRILOE 6", curs_plotn:=9]

gg <- ggplot(dt.table1.curs.plot, 
             aes(x=curs_plotn, y=`% with secondary cases`)) +
  geom_point() +
  geom_smooth(method='lm', formula = y ~ poly(x,2)) +
  geom_line() +
  geom_errorbar(aes(ymin=`% with secondary cases LI`, ymax=`% with secondary cases LS`), colour="dark green", width=0.4) +
  scale_color_manual(values = c("dark green","black")) + 
  labs(y = "% with secondary cases") +
  #+ scale_y_continuous(limits=c(1,8))
  #scale_y_continuous(limits=c(0,25))
  scale_x_continuous(breaks=c(1:9),labels=c("P3","P4","P5", "1", "2", "3","4","5","6"))
gg  

lm1 <- lm(`% with secondary cases` ~curs_plotn, data=dt.table1.curs.plot)
summary(lm1)

lm2 <- lm(`% with secondary cases` ~ poly(curs_plotn,2), data=dt.table1.curs.plot)
summary(lm2)

#fwrite(dt.table1.21.curs, "D:/SISAP/sisap/66Analisis/covid19/mascaretes/results/table1_20202022_2T.csv")
# brots_gcecurs.curs[data_inici_brot >= t1[1] & data_inici_brot <= t1[2], trimestre:='2020-1T']
# brots_gcecurs.curs[data_inici_brot >= t2[1] & data_inici_brot <= t2[2], trimestre:='2020-2T']
# brots_gcecurs.curs[data_inici_brot >= t3[1] & data_inici_brot <= t3[2], trimestre:='2020-3T']
# brots_gcecurs.curs[data_inici_brot >= t4[1] & data_inici_brot <= t4[2], trimestre:='2021-1T']
# 
# dt.table1.curstrimestre <- merge(gcecurs.curs,
#                         brots_gcecurs.curs[, .(`Index cases` = .N,
#                                                `All cases` = sum(alumnes_coexistint)
#                                                ), .(curs_any,trimestre,curs_brots)],
#                         by.x = c("curs_any", "curs_brots"),
#                         by.y = c("curs_any", "curs_brots"))
# datatable(dt.table1.curstrimestre)
# dt.table1.201t.curs <- dcast(melt(dt.table1.curstrimestre[curs_any=='2020-2021' & trimestre=='2020-1T',], id.vars = c("curs_brots")), variable ~ curs_brots)
# dt.table1.202t.curs <- dcast(melt(dt.table1.curstrimestre[curs_any=='2020-2021' & trimestre=='2020-2T',], id.vars = c("curs_brots")), variable ~ curs_brots)
# dt.table1.203t.curs <- dcast(melt(dt.table1.curstrimestre[curs_any=='2020-2021' & trimestre=='2020-3T',], id.vars = c("curs_brots")), variable ~ curs_brots)

```

## Mètode Antic

```{r, eval=FALSE}
# REVISAR este caso: 291BF5F115D1D63B3E8894085DC861EB71027618 gce de 2 cursos

# dt.flt.curs[, curs_all:=curs]
# dt.flt.curs[, curs:=curs_maxgce]

# 10 DIES
  dt.dm_positiu.curs <- dt.flt.curs[cas_curs == "Sí", .(hash, curs_any, curs_brots, escola_id, gce_id, eval(as.name(params$cas_var)))]
  setnames(dt.dm_positiu.curs, "V6", params$cas_var)
  dt.dm_positiu.curs[order(curs_any,eval(as.name(params$cas_var))), data_prova_menys_1 := shift(eval(as.name(params$cas_var)), 1), .(curs_any,gce_id)]
  dt.dm_positiu.curs[, data_prova_diferencia := eval(as.name(params$cas_var)) - data_prova_menys_1]
  dt.dm_positiu.curs[, cas_inicial := ifelse(is.na(data_prova_diferencia) | data_prova_diferencia > 10, 1, 0)]
  dt.dm_positiu.curs[order(curs_any, eval(as.name(params$cas_var))), brot := cumsum(cas_inicial), .(curs_any,gce_id)]
  dt.dm_positiu.curs[, data_inici_brot := min(eval(as.name(params$cas_var))), c("curs_any","gce_id", "brot")]

  #z <- dt.dm_positiu.curs[curs_any=='2021-2022', .N, data_inici_brot]
  
# 14 DIES
  # dt.dm_positiu.curs[, cas_inicial_14d := ifelse(is.na(data_prova_diferencia) | data_prova_diferencia > 14, 1, 0)]
  # dt.dm_positiu.curs[order(eval(as.name(params$cas_var))), brot_14d := cumsum(cas_inicial_14d), gce_id]
  # dt.dm_positiu.curs[, data_inici_brot_14d := min(eval(as.name(params$cas_var))), c("gce_id", "brot_14d")]

# Seleccionem els brots amb cas índex abans de la fi del curs
  #dt.dm_positiu.curs <- dt.dm_positiu.curs[data_inici_brot < fi.curs]
  dt.dm_positiu.curs <- dt.dm_positiu.curs[(curs_any=='2020-2021' & data_inici_brot >= t1[1] & data_inici_brot <= t1[2]) | 
                                           (curs_any=='2020-2021' & data_inici_brot >= t2[1] & data_inici_brot <= t2[2]) |
                                           (curs_any=='2020-2021' & data_inici_brot >= t3[1] & data_inici_brot <= t3[2]) |
                                           (curs_any=='2021-2022' & data_inici_brot >= t4[1] & data_inici_brot <= t4[2]),]
  
  #z <- dt.dm_positiu.curs[curs_any=='2021-2022', .N, data_inici_brot]
  
  #dt.flt <- dt.flt[cas_curs == "No" | (cas_curs == "Sí" & hash %in% dt.dm_positiu[, hash])]
```


```{r, eval=FALSE}
# Afegir dades dels brots a la taula dels nens.

dt.brots.curs <- merge(dt.flt.curs,
                       dt.dm_positiu.curs[,.(hash,
                                             curs_any,
                                             data_prova_menys_1, data_prova_diferencia, 
                                             cas_inicial, brot, data_inici_brot)],
                                             #cas_inicial_14d, brot_14d, data_inici_brot_14d)],
                      by.x="hash",
                      by.y="hash",
                      all.x =TRUE
                      )
```

## Brot **10 dies**

Càlcul de brots utilitzant 10 dies com temps on trobar els casos secundaris. 

```{r, eval=FALSE}
calc_casos_coexistint <- function(dt, vars = c("curs_any", "gce_id", "escola_id", "data_inici_brot", "brot")){
  resultat <- dt[, .(
  alumnes_coexistint = .N
  ), by = vars]
}
gcecurs.curs <- dt.flt.curs[, .N, c("curs_any", "curs_brots", "gce_id", "escola_id")][, .N, c("curs_any","curs_brots")]
setnames(gcecurs.curs, "N", "N_GCE")

brots_gce.curs <- calc_casos_coexistint(unique(dt.dm_positiu.curs[, c("hash", "curs_any", "gce_id", "escola_id", "data_inici_brot", "brot")]))
brots_gcecurs.curs <- calc_casos_coexistint(unique(dt.dm_positiu.curs[, c("hash", "curs_any", "gce_id", "escola_id", "curs_brots", "data_inici_brot", "brot")]),
                                       vars = c("curs_brots", "curs_any", "gce_id", "escola_id", "data_inici_brot", "brot"))

brots_gcecurs.curs[data_inici_brot >= t1[1] & data_inici_brot <= t1[2], trimestre:='2020-1T']
brots_gcecurs.curs[data_inici_brot >= t2[1] & data_inici_brot <= t2[2], trimestre:='2020-2T']
brots_gcecurs.curs[data_inici_brot >= t3[1] & data_inici_brot <= t3[2], trimestre:='2020-3T']
brots_gcecurs.curs[data_inici_brot >= t4[1] & data_inici_brot <= t4[2], trimestre:='2021-1T']
brots_gcecurs.curs[,.N, trimestre]
```

***

Taules amb la descripció dels brots a nivell de GCE. Les taules només contenen informació dels GCE amb brot.

```{r, eval=FALSE}

table1::table1(~ brot + as.factor(brot) + alumnes_coexistint | curs,
               brots_gcecurs.curs[curs_any=="2020-2021"],
               caption = "CURS 2020-2021: GCE amb brots")

table1::table1(~ brot + as.factor(brot) + alumnes_coexistint | curs,
               brots_gcecurs.curs[trimestre=="2020-1T",],
               caption = "CURS 2020-2021 1T: GCE amb brots")

table1::table1(~ brot + as.factor(brot) + alumnes_coexistint | curs,
               brots_gcecurs.curs[trimestre=="2020-2T",],
               caption = "CURS 2020-2021 2T: GCE amb brots")

table1::table1(~ brot + as.factor(brot) + alumnes_coexistint | curs,
               brots_gcecurs.curs[trimestre=="2020-3T",],
               caption = "CURS 2020-2021 3T: GCE amb brots")

table1::table1(~ brot + as.factor(brot) + alumnes_coexistint | curs,
               brots_gcecurs.curs[curs_any=="2021-2022"],
               caption = "CURS 2021-2022: GCE amb brots")
```

***

### Table 1 article

Informació per fer la taula de l'article.

```{r, eval=FALSE}

dt.table1.curs <- merge(gcecurs.curs,
                        brots_gcecurs.curs[, .(`Index cases` = .N,
                                               `All cases` = sum(alumnes_coexistint)
                                               ), .(curs_any,curs_brots)],
                        by.x = c("curs_any", "curs_brots"),
                        by.y = c("curs_any", "curs_brots"))
datatable(dt.table1.curs)

dt.table1.20.curs <- dcast(melt(dt.table1.curs[curs_any=='2020-2021',], id.vars = c("curs_brots")), variable ~ curs_brots)
dt.table1.21.curs <- dcast(melt(dt.table1.curs[curs_any=='2021-2022',], id.vars = c("curs_brots")), variable ~ curs_brots)

dt.table1.curstrimestre <- merge(gcecurs.curs,
                        brots_gcecurs.curs[, .(`Index cases` = .N,
                                               `All cases` = sum(alumnes_coexistint)
                                               ), .(curs_any,trimestre,curs_brots)],
                        by.x = c("curs_any", "curs_brots"),
                        by.y = c("curs_any", "curs_brots"))
datatable(dt.table1.curstrimestre)
dt.table1.201t.curs <- dcast(melt(dt.table1.curstrimestre[curs_any=='2020-2021' & trimestre=='2020-1T',], id.vars = c("curs_brots")), variable ~ curs_brots)
dt.table1.202t.curs <- dcast(melt(dt.table1.curstrimestre[curs_any=='2020-2021' & trimestre=='2020-2T',], id.vars = c("curs_brots")), variable ~ curs_brots)
dt.table1.203t.curs <- dcast(melt(dt.table1.curstrimestre[curs_any=='2020-2021' & trimestre=='2020-3T',], id.vars = c("curs_brots")), variable ~ curs_brots)
```

***

A continuació es presenta informació dels casos dels brots a nivel de GCE.

```{r, eval=FALSE}
dt.brot.curs.10d.curs <- merge(gcecurs.curs,
                               brots_gcecurs.curs[, .(
  `N GCE amb BROTS` = .N,
  `N GCE sense cas secundari` = sum(alumnes_coexistint == 1),
  `N GCE amb algun cas secundari` = sum(alumnes_coexistint != 1),
  `% GCE sense cas secundari` = round(mean(alumnes_coexistint == 1)*100,2),
  `% GCE amb cas secundari` = round(mean(alumnes_coexistint > 1)*100,2),
  `Nombre de casos secundaris (N)` = sum(alumnes_coexistint - 1),
  `Nombre de casos secundaris (Mínim)` = min(alumnes_coexistint - 1),
  `Nombre de casos secundaris (Mitjana)` = round(mean(alumnes_coexistint - 1),2),
  `Nombre de casos secundaris (P50)` = quantile(alumnes_coexistint - 1, probs = .5),
  `Nombre de casos secundaris (P25)` = quantile(alumnes_coexistint - 1, probs = .25),
  `Nombre de casos secundaris (P75)` = quantile(alumnes_coexistint - 1, probs = .75),
  `Nombre de casos secundaris (Màxim)` = max(alumnes_coexistint - 1),
  `Nombre de casos amb només un cas secundari` = sum(alumnes_coexistint == 2)
  ), .(curs_any,curs)],
by.x = c("curs_any", "curs"),
by.y = c("curs_any", "curs"))

dt.brot.curs.10d.curs <- dt.brot.curs.10d.curs[,`% GCE amb brots` := round((`N GCE amb BROTS`/N_GCE)*100,2)]

dt.brot.curs.10d.t1.curs <- dt.brot.curs.10d.curs[curs_any == '2020-2021',
                                                  .(curs_any, curs, N_GCE, 
                                                     `N GCE amb BROTS`, `% GCE amb brots`, `Nombre de casos secundaris (N)`,
                                                     `N GCE sense cas secundari`, `% GCE sense cas secundari`,
                                                     `N GCE amb algun cas secundari`, `% GCE amb cas secundari`,
                                                     `Nombre de casos amb només un cas secundari`)]



dt.brot.curs.10d.t1.curs <- dt.brot.curs.10d.curs[curs_any == '2021-2022',
                                                  .(curs_any, curs, N_GCE, 
                                                     `N GCE amb BROTS`, `% GCE amb brots`, `Nombre de casos secundaris (N)`,
                                                     `N GCE sense cas secundari`, `% GCE sense cas secundari`,
                                                     `N GCE amb algun cas secundari`, `% GCE amb cas secundari`,
                                                     `Nombre de casos amb només un cas secundari`)]
datatable(dt.brot.curs.10d.t1.curs)
```

```{r, eval=FALSE}
dt.brot.curs.10d.curs <- merge(gcecurs.curs,
                               brots_gcecurs.curs[, .(
  `N GCE amb BROTS` = .N,
  `N GCE sense cas secundari` = sum(alumnes_coexistint == 1),
  `N GCE amb algun cas secundari` = sum(alumnes_coexistint != 1),
  `% GCE sense cas secundari` = round(mean(alumnes_coexistint == 1)*100,2),
  `% GCE amb cas secundari` = round(mean(alumnes_coexistint > 1)*100,2),
  `Nombre de casos secundaris (N)` = sum(alumnes_coexistint - 1),
  `Nombre de casos secundaris (Mínim)` = min(alumnes_coexistint - 1),
  `Nombre de casos secundaris (Mitjana)` = round(mean(alumnes_coexistint - 1),2),
  `Nombre de casos secundaris (P50)` = quantile(alumnes_coexistint - 1, probs = .5),
  `Nombre de casos secundaris (P25)` = quantile(alumnes_coexistint - 1, probs = .25),
  `Nombre de casos secundaris (P75)` = quantile(alumnes_coexistint - 1, probs = .75),
  `Nombre de casos secundaris (Màxim)` = max(alumnes_coexistint - 1),
  `Nombre de casos amb només un cas secundari` = sum(alumnes_coexistint == 2)
  ), .(curs_any,trimestre,curs)],
by.x = c("curs_any", "curs"),
by.y = c("curs_any", "curs"))

dt.brot.curs.10d.curs <- dt.brot.curs.10d.curs[,`% GCE amb brots` := round((`N GCE amb BROTS`/N_GCE)*100,2)]

dt.brot.curs.10d.t1.curs <- dt.brot.curs.10d.curs[curs_any == '2020-2021' & trimestre=='2020-1T',
                                                  .(trimestre, curs, N_GCE, 
                                                     `N GCE amb BROTS`, `% GCE amb brots`, `Nombre de casos secundaris (N)`,
                                                     `N GCE sense cas secundari`, `% GCE sense cas secundari`,
                                                     `N GCE amb algun cas secundari`, `% GCE amb cas secundari`,
                                                     `Nombre de casos amb només un cas secundari`)]
datatable(dt.brot.curs.10d.t1.curs)

dt.brot.curs.10d.t1.curs <- dt.brot.curs.10d.curs[curs_any == '2020-2021' & trimestre=='2020-2T',
                                                  .(trimestre, curs, N_GCE, 
                                                     `N GCE amb BROTS`, `% GCE amb brots`, `Nombre de casos secundaris (N)`,
                                                     `N GCE sense cas secundari`, `% GCE sense cas secundari`,
                                                     `N GCE amb algun cas secundari`, `% GCE amb cas secundari`,
                                                     `Nombre de casos amb només un cas secundari`)]
datatable(dt.brot.curs.10d.t1.curs)

dt.brot.curs.10d.t1.curs <- dt.brot.curs.10d.curs[curs_any == '2020-2021' & trimestre=='2020-3T',
                                                  .(trimestre, curs, N_GCE, 
                                                     `N GCE amb BROTS`, `% GCE amb brots`, `Nombre de casos secundaris (N)`,
                                                     `N GCE sense cas secundari`, `% GCE sense cas secundari`,
                                                     `N GCE amb algun cas secundari`, `% GCE amb cas secundari`,
                                                     `Nombre de casos amb només un cas secundari`)]
datatable(dt.brot.curs.10d.t1.curs)

dt.brot.curs.10d.t1.curs <- dt.brot.curs.10d.curs[curs_any == '2021-2022' & trimestre=='2021-1T',
                                                  .(trimestre, curs, N_GCE, 
                                                     `N GCE amb BROTS`, `% GCE amb brots`, `Nombre de casos secundaris (N)`,
                                                     `N GCE sense cas secundari`, `% GCE sense cas secundari`,
                                                     `N GCE amb algun cas secundari`, `% GCE amb cas secundari`,
                                                     `Nombre de casos amb només un cas secundari`)]
datatable(dt.brot.curs.10d.t1.curs)


# dt.brot.curs.10d.t2.curs <- dt.brot.curs.10d.curs[,.(curs,
#                                                      `Nombre de casos secundaris (Mínim)`, `Nombre de casos secundaris (Mitjana)`, `Nombre de casos secundaris (P50)`, 
#                                                      `Nombre de casos secundaris (P25)`, `Nombre de casos secundaris (P75)`, `Nombre de casos secundaris (Màxim)`)]
# datatable(dt.brot.curs.10d.t2.curs)
```

```{r brots gc}
gc.var <- gc()
```