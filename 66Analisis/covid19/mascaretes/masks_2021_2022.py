# -*- coding: utf8 -*-

import hashlib as h

"""
Mascaretes cursos p5 i primer de any 2020 - 2021
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


tb = 'mascaretes_cohorts_21_22'
tb2 = 'mascaretes_positius_21_22'
db = 'permanent'


def curs_anys(anys):
    if anys == '2018':
        c2020 = 'P3'
    elif anys == '2017':
        c2020 = 'P4'
    elif anys == '2016':
        c2020 = 'P5'
    elif anys == '2015':
        c2020 = 'PRIMER'
    elif anys == '2014':
        c2020 = 'SEGON'
    elif anys == '2013':
        c2020 = 'TERCER'
    elif anys == '2012':
        c2020 = 'QUART'
    elif anys == '2011':
        c2020 = 'CINQUE'
    elif anys == '2010':
        c2020 = 'SISE'
    elif anys == '2009':
        c2020 = 'ESO1'
    else:
        c2020 = 'altres'
    return c2020
    

class escoles(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_gce()
        self.get_cursos()
        self.get_grups()
        self.get_cens()
        self.get_cip()
        self.get_dbs()
        self.get_dbs2020()
        self.get_positius()
        self.get_cohorts()
        #self.get_proves()
        self.export_files()
        
    def get_gce(self):
        """obtenim el gce lligat al centre"""
        u.printTime("gce")
        self.tipus_gce = {}
        sql = """select id,  centre_id from dwsisap_escola.grups_gce"""
        for gce, centre in u.getAll(sql, 'exadata'):
            self.tipus_gce[gce] = centre
      
    def get_cursos(self):
        """obtenim el curs"""
        u.printTime("nivells i cursos")
        ensenyament = {}
        
        sql = """SELECT id, codi FROM dwsisap_escola.GRUPS_ENSENYAMENT WHERE codi IN ('EPRILOE','EINFLOE 2C')"""
        for ensy, codniv in u.getAll(sql, 'exadata'):
            ensenyament[ensy] = codniv
         
        nivell = {}
        sql = """SELECT * FROM dwsisap_escola.grups_nivell"""
        for id, codi, ensy_id in u.getAll(sql, 'exadata'):
            codensy = ensenyament[ensy_id] if ensy_id in ensenyament else '99'
            if codensy != '99':
                key = str(codensy) + ' ' + str(codi)
                nivell[id] = key
        
        self.curs = {}
        sql = """SELECT * FROM dwsisap_escola.grups_curs"""
        for id, centre, nivell_id in u.getAll(sql, 'exadata'):
            codi_curs = nivell[nivell_id] if nivell_id in nivell  else '99'
            if codi_curs != '99':
                self.curs[id] = codi_curs
    
    def get_grups(self):
        """obtenim la persona lligada al gce"""
        u.printTime("grups")
        self.alumnes = {}
        self.gce_ = {}
        sql = """select gce_id,persona_id, curs_id from dwsisap_escola.grups_alumne"""
        for  gce_id, person_id , curs_id in u.getAll(sql, 'exadata'):
            centre = self.tipus_gce[gce_id] if gce_id in self.tipus_gce else None
            self.alumnes[person_id] = centre
            codi_curs = self.curs[curs_id] if curs_id in self.curs else '99'
            if codi_curs != '99':
                self.gce_[person_id] = {'gce': gce_id, 'curs': codi_curs}
            
    def get_cens(self):
        """cens actual"""
        u.printTime("cens")
        self.es_cens = {}
        sql = """SELECT cip, id FROM DWSISAP_ESCOLA.PERSONES_PERSONA"""
        for cip, id in u.getAll(sql, 'exadata'):
            centre = self.alumnes[id] if id in self.alumnes else None
            gce = self.gce_[id]['gce'] if id in self.gce_ else None
            curs = self.gce_[id]['curs'] if id in self.gce_ else '99'
            if curs != '99':
                self.es_cens[cip] = {'c': centre, 'gce': gce, 'curs': curs, 'id':id}
    
    def get_cip(self):
        """hash to cip"""
        u.printTime("cip")
        self.hash_to_cip = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.hash_to_cip[hash] = cip
       
    def get_dbs(self):
        """Dades del dbs"""
        u.printTime("DBS")
        self.factors = {}
        self.in_dbs_true = {}
        sql = """select c_cip,c_gma_codi, c_gma_complexitat, PS_DIABETIS1_DATA, PS_ASMA_DATA, PS_NEOPLASIA_M_DATA,
                    PS_OBESITAT_DATA, F_DIAB_INSULINA, F_MPOC_ASMA
                from DWSISAP.DBS"""
        for (cip, gma, gma1, dm1,
             asma, neos, obesitat,insulina, f_mpoc) in u.getAll(sql, 'exadata'):   
            self.factors[(cip, "in_dbs")] = 1
            self.in_dbs_true[(cip)] = True
            self.factors[(cip, "gma")] = gma
            self.factors[(cip, "gma1")] = gma1
            if dm1:
                self.factors[(cip, "dm")] = dm1
            if asma:
                self.factors[(cip, "asma")] = asma
            if obesitat:
                self.factors[(cip, "obesitat")] = obesitat
            if neos:
                self.factors[(cip, "neos")] = neos     
    
    def get_dbs2020(self):
        """Dades del dbs"""
        u.printTime("DBS 2020")
        
        SIDICS_DB = ("dbs", "x0002")
        sql = """select c_cip,c_gma_codi, c_gma_complexitat, PS_DIABETIS1_DATA, PS_ASMA_DATA, PS_NEOPLASIA_M_DATA,
                    PS_OBESITAT_DATA, F_DIAB_INSULINA, F_MPOC_ASMA
                    from dbs.dbs_2020"""
        for (cip1, gma, gma1, dm1,
             asma, neos, obesitat,insulina, f_mpoc) in u.getAll(sql, SIDICS_DB):
            if cip1 in self.hash_to_cip:
                cip2 = self.hash_to_cip[cip1]
                cip = h.sha1(cip2).hexdigest().upper()
                if cip not in self.in_dbs_true:
                    self.factors[(cip, "in_dbs")] = 1
                    self.in_dbs_true[(cip)] = True
                    self.factors[(cip, "gma")] = gma
                    self.factors[(cip, "gma1")] = gma1
                    if dm1:
                        self.factors[(cip, "dm")] = dm1
                    if asma:
                        self.factors[(cip, "asma")] = asma
                    if obesitat:
                        self.factors[(cip, "obesitat")] = obesitat
    
    def get_positius(self):
        """."""
        u.printTime("positius")
        self.positius = []
        sql = "select persona_id, to_char(data_prova,'YYYY-MM-DD'), to_char(data_resultat,'YYYY-MM-DD') from dwsisap_escola.clinica_positiu"
        for id, datap, dataR in u.getAll(sql, 'exadata'):
            self.positius.append([id.encode('utf-8'), datap, dataR])
    
    def get_cohorts(self):
        """agafem nascuts 2015 (primer) i 2016 (P5) pel curs 2021 - 2022"""
        u.printTime("cohort")
        self.pacients = []
        self.coles = {}
        sql = """SELECT 
                    a.hash, a.rca_cip, a.data_naixement, a.sexe, a.rca_up, a.RCA_MUNICIPI, 
                    pdia_primer_positiu, ingres_primer, ingres_ultim, ingres_uci_primer, ingres_uci_ultim, exitus_covid, to_char(a.data_naixement, 'YYYY')
                FROM 
                    dwsisap.dbc_vacuna a
                left JOIN 
                    dwsisap.DBC_METRIQUES b
                ON 
                    a.hash=b.HASH
                WHERE 
                    to_char(a.data_naixement, 'YYYY') in ('2014','2015', '2016','2017', '2013','2012','2011','2010','2018')"""
        for hash, cip, naix, sexe, up, municipi,data_cas, ingres1, ingres2, uci1, uci2, exitus, curs in u.getAll(sql, 'exadata'):
            self.coles[hash] = True
            cens = 0
            escola, gce, codi_curs, id_persona = None, None, None, None
            if cip in self.es_cens:
                cens=1
                escola=self.es_cens[cip]['c']
                gce=self.es_cens[cip]['gce']
                id_persona=self.es_cens[cip]['id']
                codi_curs=self.es_cens[cip]['curs']
                dm, asma, obesitat, neos = None, None, None, None
                insulina, f_mpoc, gma, gma1 = None, None, None, None
                in_dbs = 0
                curs1 = curs_anys(curs)
                if hash in self.in_dbs_true:
                    in_dbs = self.factors[(hash, "in_dbs")] if (hash, "in_dbs") in self.factors else 0
                    gma = self.factors[(hash, "gma")] if (hash, "gma") in self.factors else None
                    gma1 = self.factors[(hash, "gma1")] if (hash, "gma1") in self.factors else None
                    dm = self.factors[(hash, "dm")] if (hash, "dm") in self.factors else None
                    asma = self.factors[(hash, "asma")] if (hash, "asma") in self.factors else None
                    obesitat = self.factors[(hash, "obesitat")] if (hash, "obesitat") in self.factors else None
                self.pacients.append([hash.encode('utf-8'), id_persona, curs1, naix, sexe,  up, municipi, escola, codi_curs, gce, cens, 
                        gma, gma1, data_cas, ingres1, ingres2, uci1, uci2, exitus,
                        in_dbs, dm, asma,  obesitat])           
    
    def get_proves(self):
        """proves"""
        u.printTime("Proves")
        self.upload = []
        sql = """select hash,data, prova, resultat_des, motiu, entorn,simptomes
                        from preduffa.sisap_covid_pac_prv_raw 
        where (prova like ('%PCR%') or prova like ('%Antigen%')) and to_char(data, 'YYYYMMDD')>'20200913'"""
        for id, data, prova, res, motiu, entorn, simpt in u.getAll(sql, 'redics'):
            if id in self.coles:
                self.upload.append([id.encode('utf-8'), data, prova, res, motiu, entorn, simpt])
    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        cols = ("hash varchar(40)", "persona_id varchar(40)","any_curs varchar(40)", "data_naix date", "sexe varchar(10)",  "up varchar(10)", "municipi varchar(10)","escola_id varchar(50)", "curs varchar(50)","gce_id varchar(40)", "cens_escola int",  "gma_codi varchar(10)", "gma_complexitat double",  "pdia_primer_positiu date",
                "ingres_primer date", "ingres_ultim date", "ingres_uci_primer date", "ingres_uci_ultim date","exitus_covid date",
                "in_dbs int",
            "FR_dm date", "FR_asma date",  "FR_obes date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pacients, tb, db)     
        
        cols = ("persona_id varchar(40)","data_prova varchar(40)", "data_resultat varchar(40)")
        u.createTable(tb2, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.positius, tb2, db)
        
        
if __name__ == '__main__':
    u.printTime("Inici")
     
    escoles()
    
    u.printTime("Final")    