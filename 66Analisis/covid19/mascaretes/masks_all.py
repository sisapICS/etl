# -*- coding: utf8 -*-

import hashlib as h

"""
Mascaretes refaig amb dades menys fines
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = 'maskoles_21_22'
tb2 = 'maskoles_20_21'
db = 'permanent'

u.printTime("select 2021_2022")
upload = []
sql = """SELECT hash, pp.id, pp.data_naixement, pp.sexe_id, eap, gg.centre_id,
            a.gce_id, ge.codi, gn.codi  FROM dwsisap_escola.grups_alumne a
            INNER JOIN dwsisap_escola.GRUPS_CURS gc 
            ON a.curs_id =gc.id 
            INNER JOIN dwsisap_escola.GRUPS_NIVELL gn 
            ON gc.nivell_id = gn.id
            INNER JOIN dwsisap_escola.GRUPS_ENSENYAMENT ge 
            ON gn.ensenyament_id=ge.ID 
            INNER JOIN dwsisap_escola.PERSONES_PERSONA pp 
            ON a.PERSONA_ID =pp.id
            INNER JOIN dwsisap.RCA_CIP_NIA rcn2 
            ON pp.cip=rcn2.cip
            INNER JOIN DWSISAP_ESCOLA.GRUPS_GCE gg 
            ON a.gce_id=gg.id
            WHERE ge.codi IN ('EPRILOE','EINFLOE 2C')
            AND pp.cip IS NOT NULL"""
for hash, persona_id,naix, sexe, eap,  centre, gce, nivell, ensenyament in u.getAll(sql, 'exadata'):
    key = str(nivell) + ' ' + str(ensenyament)
    upload.append([hash.encode('utf-8'), persona_id, naix, sexe, eap,  centre, gce, key])

u.printTime("export")
cols = ("hash varchar(40)", "persona_id varchar(40)", "naix date", "sexe varchar(1)", "up varchar(10)", 
            "escola_id varchar(50)", "gce_id varchar(40)", "curs varchar(50)" )
u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
u.listToTable(upload, tb, db)     



u.printTime("select 2020_2021")
upload = []
sql = """SELECT hash, pp.id, pp.data_naixement, pp.sexe_id, eap,gg.centre_id,
            a.gce_id, ge.codi, gn.codi  FROM dwsisap_escola.grups_alumne_H a
            INNER JOIN dwsisap_escola.GRUPS_CURS_H gc 
            ON a.curs_id =gc.id 
            INNER JOIN dwsisap_escola.GRUPS_NIVELL_H gn 
            ON gc.nivell_id = gn.id
            INNER JOIN dwsisap_escola.GRUPS_ENSENYAMENT_H ge 
            ON gn.ensenyament_id=ge.ID 
            INNER JOIN dwsisap_escola.PERSONES_PERSONA_H pp 
            ON a.PERSONA_ID =pp.id
            INNER JOIN dwsisap.RCA_CIP_NIA rcn2 
            ON pp.cip=rcn2.cip
            INNER JOIN DWSISAP_ESCOLA.GRUPS_GCE_H gg 
            ON a.gce_id=gg.id
            WHERE ge.codi IN ('EPRILOE','EINFLOE 2C')
            AND to_char(a.dia_H, 'YYYYMMDD')='20210703' AND to_char(gc.dia_H, 'YYYYMMDD')='20210703'
            AND to_char(gn.dia_H, 'YYYYMMDD')='20210703' AND to_char(ge.dia_H, 'YYYYMMDD')='20210703'
            AND to_char(pp.dia_H, 'YYYYMMDD')='20210703' AND to_char(gg.dia_H, 'YYYYMMDD')='20210703'
            AND pp.cip IS NOT NULL"""
for hash, persona_id,naix, sexe, eap,  centre, gce, nivell, ensenyament in u.getAll(sql, 'exadata'):
    key = str(nivell) + ' ' + str(ensenyament)
    upload.append([hash.encode('utf-8'), persona_id, naix, sexe, eap,  centre, gce, key])

u.printTime("export")
cols = ("hash varchar(40)", "persona_id varchar(40)", "naix date", "sexe varchar(1)", "up varchar(10)", 
            "escola_id varchar(50)", "gce_id varchar(40)", "curs varchar(50)" )
u.createTable(tb2, "({})".format(", ".join(cols)), db, rm=True)
u.listToTable(upload, tb2, db) 
    
            