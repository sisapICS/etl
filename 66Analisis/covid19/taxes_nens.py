# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta
from time import time,sleep

import sisapUtils as u

db = "redics"

TODAY = d.datetime.now().date() 
print TODAY

def get_edat(inici, fi):
    """."""
    edat = u.yearsBetween(inici, fi)
    grup = '0-2' if edat < 3 else  '3-5' if edat <6 else '6-11' if edat < 12 else '12-15' if edat <16 else '16-17' if edat <18 else '>17'
    return (edat, grup)


class nens(object):
    """."""

    def __init__(self):
        """."""
        self.dades = c.Counter()
        self.get_dates()
        self.get_cataleg()
        self.get_rca()
        self.get_master()
        self.get_unio()
        self.export_data()
        self.dona_grants()
  
    def get_dates(self):
        """dates de web_master"""
        u.printTime("dates")
        sql = "select min(cas_data), max(cas_data) from sisap_covid_pac_master"
        for mind, maxd in u.getAll(sql, 'redics'):
            self.ini= mind
            self.fi = maxd + timedelta(days=1)
    
    def get_cataleg(self):
        """.Agafem codis de territoris"""
        u.printTime("cataleg")
        self.cataleg = {}   
        sql = """select abs_cod, regio_cod, regio_des from sisap_covid_cat_aga"""
        for  abs, reg, reg_des  in u.getAll(sql, db):
            self.cataleg[abs] = {'rs': reg, 'desc': reg_des}
    
    def get_rca(self):
        """Agafem pob"""
        u.printTime("RCA")
        sql = """select hash, data_naixement, 
                      case when sexe = '1' then 'D' else 'H' end, 
                      abs, localitat
               from sisap_covid_pac_rca where situacio='A'"""
        for hash, naix, sexe, abs, loc in u.getAll(sql, db):
            edat, grup = get_edat(naix, TODAY)
            if str(abs) in self.cataleg:
                regio = self.cataleg[str(abs)]['rs']
                desc = self.cataleg[str(abs)]['desc']
            else:
                regio = 'SE'
                desc = 'SE'
            if 0 <= int(edat) <18:
                for dat in u.dateRange(self.ini, self.fi):
                    self.dades[(dat, grup, regio, desc, 'pob')] += 1
     
    def get_master(self):
        """agafem de master"""
        u.printTime("master")
        sql = """select cas_data, sexe, edat, abs 
                from sisap_covid_pac_master  
                where estat='Confirmat' and origen_d like '%PCR%' and edat <18"""
        for data,sexe,edat,abs in u.getAll(sql, db):
            edat = int(edat)
            grup = '0-2' if edat < 3 else  '3-5' if edat <6 else '6-11' if edat < 12 else '12-15' if edat <16 else '16-17' if edat <18 else '>17'
            if abs in self.cataleg:
                regio = self.cataleg[abs]['rs']
                desc = self.cataleg[str(abs)]['desc']
            else:
                regio = 'SE'
                desc = 'SE'
            self.dades[(data,grup, str(regio),desc, 'casos')] += 1
        
    def get_unio(self):
        """unio"""
        u.printTime("unio")
        self.upload = []
        for (dat, grup, regio, desc, t2), rec in self.dades.items():
            if t2 == 'pob':
                casos = self.dades[(dat,grup, str(regio),desc, 'casos' )] if (dat,grup, str(regio), desc, 'casos') in self.dades else 0
                self.upload.append([dat, str(regio), desc, grup, int(casos), int(rec)])
   
    def export_data(self):
        """."""
        u.printTime("export")
        self.tb = 'sisap_covid_agr_nens'
        cols = ("data date", "regio_c varchar2(10)", "regio varchar2(100)", "grup varchar2(10)",  "casos int", "poblacio int")
        u.createTable(self.tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, self.tb, db)
    
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(self.tb,user),db)     
                

if __name__ == '__main__':
    u.printTime("Inici")

    nens()

    u.printTime("Final")

