---
title: "Impacte de la COVID-19 en l'EQA"
output: html_notebook
---

# Objectiu

Comparar l'increment de puntuació de l'EQA i longitudinalitat entre gener i desembre de 2019 i 2020.


```{r message=FALSE, warning=FALSE}
library(data.table)
library(lubridate)
library(ggplot2)
library(forecast)
library(RMySQL)
library(knitr)
library(dplyr)
library(kableExtra)
```

# EQA

## Dades

```{r include=FALSE}
# parametres de connexió
drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password = "",host = "10.80.217.68", port=3307, dbname="permanent")

sql <- "select * from cat_control_indicadors"
cataleg <- data.table(dbGetQuery(con, sql))

## desconexió

dbDisconnect(con)
drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password = "",host = "10.80.217.68", port=3307, dbname="nodrizas")
sql <- "select * from cat_centres"
cat_centres <- data.table(dbGetQuery(con, sql))
dbDisconnect(con)
```

```{r}
khalix_A1920 <- fread("EQADULTS.txt", sep = "{", header = F)
khalix_A2012 <- fread("EQADULTS_12_2020.txt", sep = "{", header = F)

khalix <- rbind(khalix_A1920, khalix_A2012)

```


```{r}
khalix <- khalix[, c(1, 2, 3, 4, 8)]
names(khalix) <- c("indicador", "periode", "br", "res", "N")

# Excloure indicadors
# eqd <- unique(khalix$indicador[which(khalix$indicador %like% "EQD")])
# grip <- c("EQA0712", "EQA0501", "EQA0502")
# ind_raro <- "EQA0237B"
# obesitat <- "EQA1303"
# osteoporosis <- "EQA0228"
# act_fisica <- "EQA0306"
# khalix <- khalix[!indicador %in% c(eqd, grip, ind_raro, obesitat, osteoporosis, act_fisica)]

# Exclusió NO ICS i pediàtriques
khalix <- merge(khalix, cat_centres[, c("amb_codi", "amb_desc", "ics_codi", "ics_desc", "medea")], by.x = "br", by.y = "ics_codi", all.x = T)
khalix <- khalix[amb_codi != "00" & substr(br, 1, 2) != "BN"]

# Crear variables descriptives
khalix[, rural := factor(ifelse(medea %in% c("0R", "1R", "2R"), "Rural", "Urbà"))]
khalix[, medea := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), labels = c(rep("Rural", 3), "1U", "2U", "3U", "4U"))]

# Preparar dades
khalix_dcast <- dcast(khalix, indicador + periode + br + ics_desc + amb_codi + amb_desc + rural + medea ~ res, value.var = "N")
khalix_dcast[, "NUM"][is.na(khalix_dcast[, "NUM"])] <- 0

dades <- khalix_dcast[periode %in% c("A1901", "A1912", "A2001", "A2012")]

dades[, Any := factor(substr(periode, 1, 3), levels = c("A19", "A20"), labels = c("Any 2019", "Any 2020"))]
dades[, Mes := factor(substr(periode, 4, 5), levels = c("01", "12"), labels = c("Gener", "Desembre"))]
dades[, resultat := NUM/DEN]

dades_dcast <- dcast(dades, indicador + br + ics_desc + amb_codi + amb_desc + rural + medea + Any ~ Mes, value.var = "resultat")
dades_dcast[, diferencia := (Desembre - Gener)/Gener]

dades_dcast <- dades_dcast[diferencia != Inf]
```

## Descripció

```{r eval=FALSE, include=FALSE}
summary(dades)
```


```{r eval=FALSE, include=FALSE}
tapply(dades_dcast[diferencia != Inf]$diferencia, dades_dcast$Any, summary)
```


```{r warning=FALSE}
lapply(unique(dades_dcast$indicador), function(x){
  ggplot(dades_dcast[indicador == x], aes(Any, diferencia)) +
  geom_boxplot() +
    geom_hline(yintercept = 0, color = "red", linetype = 2) +
    theme_classic() +
    labs(title = paste0(x, " - ", cataleg[indicador == x]$literal), x = "", y = "Diferència entre Gener i Desembre")
})

```

```{r warning=FALSE}
lapply(unique(dades_dcast$indicador), function(x){
  ggplot(dades_dcast[indicador == x], aes(Any, diferencia)) +
  geom_boxplot() +
    geom_hline(yintercept = 0, color = "red", linetype = 2) +
    theme_classic() +
    labs(title = paste0(x, " - ", cataleg[indicador == x]$literal), x = "", y = "Diferència entre Gener i Desembre") + facet_wrap(~ medea)
})

```

## Paired T-test

```{r}
dades_dcast_dcast <- dcast(dades_dcast, indicador + br + ics_desc + amb_codi + amb_desc + rural + medea ~ Any, value.var = "diferencia")
```

```{r}
a <- lapply(unique(dades_dcast$indicador), function(x){
  print(x)
  if (nrow(dades_dcast_dcast[indicador == x & !(is.na(`Any 2019`) | is.na(`Any 2020`))]) != 0){
    dades_dcast_dcast[indicador == x  & !(is.na(`Any 2019`) | is.na(`Any 2020`)) , .(
      indicador = x,
      indicador_literal = paste0(x, " - ", cataleg[indicador == x]$literal),
      `Diferencia Any 2019` = mean(`Any 2019`), 
      `Diferencia Any 2020` = mean(`Any 2020`), 
      value = t.test(`Any 2019`, `Any 2020`, paired = T, alternative = "greater")$estimate,
      pvalue = t.test(`Any 2019`, `Any 2020`, paired = T, alternative = "greater")$p.value)]
  }

})
t <- do.call("rbind", a)
# fwrite(, "Diferències_gen_dec_2019_2020.csv", sep = ";", dec = ".", row.names = F)
```

```{r}
tt <- dades_dcast[, lapply(.SD, mean, na.rm = T), .SDcols = c("Gener", "Desembre"), c("indicador", "Any")]
tt[, `Mitjana Gener-Desembre` := paste0(round(Gener, 4), "-", round(Desembre, 4))]
tt_dcast <- dcast(tt, indicador ~ Any, value.var = "Mitjana Gener-Desembre")
```

```{r}
ttt <- merge(tt_dcast, t, by = "indicador")
```

```{r}
fwrite(ttt, "Diferències_gen_dec_2019_2020.csv", sep = ";", dec = ".", row.names = F)

```

# Longitudinalitat

## Dades

```{r message=FALSE, warning=FALSE, include=FALSE}
drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password = "",host = "10.80.217.68", port=3307, dbname="permanent")
sql = "select * from permanent.long_covid_up"
long <- data.table(dbGetQuery(con, sql))

sql = "select * from nodrizas.cat_centres"
cat_centres <- data.table(dbGetQuery(con, sql))
dbDisconnect(con)
```

```{r}
long <- merge(long, cat_centres[, c("amb_codi", "amb_desc", "ics_codi", "ics_desc", "medea")], by.x = "up", by.y = "ics_codi", all.x = T)
long <- long[amb_codi != "00" & substr(up, 1, 2) != "BN"]

# Crear variables descriptives
long[, rural := factor(ifelse(medea %in% c("0R", "1R", "2R"), "Rural", "Urbà"))]
long[, medea := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), labels = c(rep("Rural", 3), "1U", "2U", "3U", "4U"))]

# Preparar dades
long_dcast <- dcast(long, indicador + periode + up + ics_desc + amb_codi + amb_desc + rural + medea ~ analisi, value.var = "resultat")
long_dcast[, "NUM"][is.na(long_dcast[, "NUM"])] <- 0

dades_long <- long_dcast[periode %in% c("201901", "201912", "202001", "202012")]

dades_long[, Any := factor(substr(periode, 1, 4), levels = c("2019", "2020"), labels = c("Any 2019", "Any 2020"))]
dades_long[, Mes := factor(substr(periode, 5, 6), levels = c("01", "12"), labels = c("Gener", "Desembre"))]
dades_long[, resultat := NUM/DEN]

dades_long_dcast <- dcast(dades_long, indicador + up + ics_desc + amb_codi + amb_desc + rural + medea + Any ~ Mes, value.var = "resultat")
dades_long_dcast[, diferencia := (Desembre - Gener)/Gener]

dades_long_dcast <- dades_long_dcast[diferencia != Inf]
```

## Descripció


```{r warning=FALSE}
lapply(unique(dades_long_dcast$indicador), function(x){
  ggplot(dades_long_dcast[indicador == x], aes(Any, diferencia)) +
  geom_boxplot() +
    geom_hline(yintercept = 0, color = "red", linetype = 2) +
    theme_classic() +
    labs(title = x, x = "", y = "Diferència entre Gener i Desembre")
})

```

```{r warning=FALSE}
lapply(unique(dades_long_dcast$indicador), function(x){
  ggplot(dades_long_dcast[indicador == x], aes(Any, diferencia)) +
  geom_boxplot() +
    geom_hline(yintercept = 0, color = "red", linetype = 2) +
    theme_classic() +
    labs(title = x, x = "", y = "Diferència entre Gener i Desembre") + facet_wrap(~ medea)
})

```

## Paired T-test

```{r}
dades_long_dcast_dcast <- dcast(dades_long_dcast, indicador + up + ics_desc + amb_codi + amb_desc + rural + medea ~ Any, value.var = "diferencia")
```

```{r}
a <- lapply(unique(dades_long_dcast_dcast$indicador), function(x){
  print(x)
  if (nrow(dades_long_dcast_dcast[indicador == x & !(is.na(`Any 2019`) | is.na(`Any 2020`))]) != 0){
    dades_long_dcast_dcast[indicador == x  & !(is.na(`Any 2019`) | is.na(`Any 2020`)) , .(
      indicador = x,
      `Diferencia Any 2019` = mean(`Any 2019`), 
      `Diferencia Any 2020` = mean(`Any 2020`), 
      value = t.test(`Any 2019`, `Any 2020`, paired = T, alternative = "greater")$estimate,
      pvalue = t.test(`Any 2019`, `Any 2020`, paired = T, alternative = "greater")$p.value)]
  }

})
t <- do.call("rbind", a)
# fwrite(, "Diferències_gen_dec_2019_2020.csv", sep = ";", dec = ".", row.names = F)
```

```{r}
tt <- dades_long_dcast[, lapply(.SD, mean, na.rm = T), .SDcols = c("Gener", "Desembre"), c("indicador", "Any")]
tt[, `Mitjana Gener-Desembre` := paste0(round(Gener, 4), "-", round(Desembre, 4))]
tt_dcast <- dcast(tt, indicador ~ Any, value.var = "Mitjana Gener-Desembre")
```

```{r}
ttt <- merge(tt_dcast, t, by = "indicador")
```

```{r}
fwrite(ttt, "Diferències_long_gen_dec_2019_2020.csv", sep = ";", dec = ".", row.names = F)

```

