indicador_a_analitzar <- "EQA0209"
dades <- khalix_dcast[indicador == indicador_a_analitzar]
dades[, resultat :=(NUM/DEN)]
cols <- c("resultat", "DEN")
dades[order(periode), paste0("change", cols) := lapply(.SD, perc_var), .SDcols = cols, setdiff(names(dades), c("periode", "DEN", "NUM", "resultat"))]

dg <- dades[,.( mitjana_resultat = mean(resultat, na.rm = T)), 
            by = c("periode", "indicador", "medea")]
dg[, actual := ifelse(periode %in% c("A2001", "A2002", "A2003", "A2004"), "Any 2020", "Any 2019")]
dg[, periode_any := substr(periode, 2, 3)]
dg[, periode_mes := substr(periode, 4, 5)]

ggplot(dg, aes(periode_mes, mitjana_resultat, linetype = actual, group = periode_any)) +
  geom_line() +
  # geom_ribbon(aes(ymin = lwr, ymax = uppr), alpha = .5) +  
  theme_classic() + 
  theme(
    plot.title = element_text(hjust = 0.5)) +
  scale_y_continuous(labels = scales::percent_format(accuracy = .01)) +
  scale_linetype_manual(values = c("Any 2020" = 1, "Any 2019" = 2)) +
  scale_x_discrete(breaks = c("01", "04", "07", "10"), labels = c("Gen", "Abr", "Jul", "Oct")) + 
  labs(title = paste0("Comparació de l'evolució de l'indicador", indicador_a_analitzar, "\nsegons MEDEA"), x = "Mes de l'any", y = "Valor de l'indicador", color = "Indicadors", linetype = "") +
  facet_wrap(~ medea)



efecte <- dades[changeresultat != Inf, .SD, .SDcols = c("br", "medea", "indicador", "periode", "changeresultat")]
efecte[, any := as.character(paste0("Any 20", substr(periode, 2, 3)))]
efecte[, mes := as.numeric(as.character(substr(periode, 4, 5)))]
efecte <- efecte[mes %in% c(1, 2, 3, 4, 5, 6)]
efecte_dcast <- dcast(efecte, br + medea + indicador + mes ~ any, value.var = "changeresultat")

efecte_dcast_result <- efecte_dcast[, lapply(.SD, mean, na.rm = T), .SDcols = c("Any 2019", "Any 2020"), by = c("indicador", "medea", "mes")]


efecte_dcast_pvalue <- efecte_dcast[!is.na(`Any 2020`) & !is.na(`Any 2019`), .(
  pvalue = t.test(`Any 2019`, `Any 2020` , paired = T)$p.value), by = c("indicador", "mes", "medea")]
t_efecte <- merge(efecte_dcast_result, efecte_dcast_pvalue, by = c("indicador", "medea", "mes"))
t_efecte[, Significatiu := ifelse(pvalue <= 0.05, "*", "")]
t_efecte[, `Diferència` := paste0(round((`Any 2020`- `Any 2019`), 2), "%")]

t_efecte[, Mes := factor(mes, levels = 1:6, labels = c("Gener", "Febrer", "Març", "Abril", "Maig", "Juny"))]
