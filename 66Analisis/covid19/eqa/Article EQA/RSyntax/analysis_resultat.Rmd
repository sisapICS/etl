---
title: "Impacte de la COVID-19 en l'EQA"
output: html_notebook
---

# Objectius

* Comparar l'evolució dels indicadors de l'EQA durant els mesos d'inici de l'epidèmia de la COVID-19 amb l'evolucio de l'any anterior.


```{r}
library(data.table)
library(lubridate)
library(ggplot2)
library(forecast)
library(RMySQL)
library(knitr)
library(dplyr)
library(kableExtra)

# Functions needed
perc_var <- function(x) ((x/shift(x))-1)*100
```

```{r include=FALSE}
# parametres de connexió
drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password = "",host = "10.80.217.68", port=3307, dbname="permanent")

sql <- "select * from cat_control_indicadors"
cataleg <- data.table(dbGetQuery(con, sql))

## desconexió

dbDisconnect(con)
drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password = "",host = "10.80.217.68", port=3307, dbname="nodrizas")

sql <- "select * from cat_centres"
cat_centres <- data.table(dbGetQuery(con, sql))

con <- dbConnect(drv, user = "sisap", password = "",host = "10.80.217.68", port=3307, dbname="permanent")

sql <- "select * from permanent.mst_variables_analisi"
variables_explicatives <- data.table(dbGetQuery(con, sql))


## desconexió

dbDisconnect(con)
```

```{r}
library("readxl")
etiquetes <- read_excel("EQA_per_a_article_den.xlsx")
etiquetes_dt <- as.data.table(etiquetes)
names(etiquetes_dt)[1] <- "indicador_to_merge"

etiquetes_dt_den <- etiquetes_dt[denominador == "*"]
etiquetes_dt_den[, `títol` := `text denominador`]
etiquetes_dt_den[indicador_to_merge == "EQA0202", `literal curt` := "TAO"]
etiquetes_dt_den[, Grup := ifelse(indicador_to_merge %in% c("EQA0230", "EQA0229", "EQA0232"), "Patologia aguda", "Seguiment")]

etiquetes_dt <- rbind(etiquetes_dt, etiquetes_dt_den)
```



```{r}
khalix <- fread("eqadults_2019_2020/eqadults_2019_2020.txt", sep = "{", header = F)
khalix <- khalix[, c(1, 2, 3, 4, 8)]
names(khalix) <- c("indicador", "periode", "br", "res", "N")
# 
khalix_nou <- fread("eqadults_2019_2020/EQDM27C3.txt", sep = "{", header = F)
# khalix_nou <- khalix_nou[, c(1, 2, 3, 4, 8)]
# names(khalix_nou) <- c("indicador", "periode", "br", "res", "N")
# 
# khalix <- rbind(khalix, khalix_nou)
# Excloure indicadors
eqd <- unique(khalix$indicador[which(khalix$indicador %like% "EQD")])
grip <- c("EQA0712", "EQA0501", "EQA0502")
ind_raro <- "EQA0237B"
obesitat <- "EQA1303"
osteoporosis <- "EQA0228"
act_fisica <- "EQA0306"
khalix <- khalix[!indicador %in% c(eqd, grip, ind_raro, obesitat, osteoporosis, act_fisica)]
# Eliminar indicadors proposats per revisors
khalix <- khalix[!indicador %in% c("EQA0226", "EQA0230", "EQA0231", "EQA0224", "EQA0229", "EQA0232")]
etiquetes_dt <- etiquetes_dt[!indicador_to_merge %in% c(eqd, grip, ind_raro, obesitat, osteoporosis, act_fisica, "EQA0226", "EQA0230", "EQA0231", "EQA0224", "EQA0229", "EQA0232")]

# Exclusió NO ICS i pediàtriques
khalix <- merge(khalix, cat_centres[, c("amb_codi", "amb_desc", "ics_codi", "ics_desc", "medea")], by.x = "br", by.y = "ics_codi", all.x = T)
khalix <- khalix[amb_codi != "00" & substr(br, 1, 2) != "BN"]

# Crear variables descriptives
khalix[, rural := factor(ifelse(medea %in% c("0R", "1R", "2R"), "Rural", "Urbà"))]
khalix[, medea := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), labels = c(rep("Rural", 3), "1U", "2U", "3U", "4U"))]

# Preparar dades
khalix_dcast <- dcast(khalix, indicador + periode + br + ics_desc + amb_codi + amb_desc + rural + medea ~ res, value.var = "N")
khalix_dcast[, "NUM"][is.na(khalix_dcast[, "NUM"])] <- 0


khalix_dcast_global <- khalix_dcast[, .(NUM = sum(NUM, na.rm = T),
                                        DEN = sum(DEN, na.rm = T)),
                                    by = c("indicador", "periode")]
```

```{r}
aa <- lapply(list(khalix_dcast, khalix_dcast_global), function(x){
  x[, resultat :=(NUM/DEN)]
  x[order(periode)]
  cols <- c("resultat", "DEN")
  x[, paste0("change", cols) := lapply(.SD, perc_var), .SDcols = cols, setdiff(names(x), c("periode", "DEN", "NUM", "resultat"))]
})

khalix_dcast[, indicador_to_merge := substr(indicador, 1, 7)]
# khalix_dcast[indicador == "EQDM27C3", indicador_to_merge :="EQDM27C3"]
khalix_dcast_etiquetes <- merge(khalix_dcast, etiquetes_dt, by = "indicador_to_merge", all.y = T)
khalix_dcast_etiquetes[, indicador_text_curt := paste0(indicador, " - ", `literal curt`)]
khalix_dcast_etiquetes[Grup %in% c("Patologia aguda", "Seguiment"), changeresultat := changeDEN]
khalix_dcast_etiquetes[Grup %in% c("Patologia aguda", "Seguiment"), resultat := DEN]
khalix_dcast_etiquetes <- khalix_dcast_etiquetes[indicador_to_merge != "EQDM27C3"]
```

# Metodologia

```{r}
variables_explicatives_analisi <- variables_explicatives[br %in% unique(khalix_dcast_etiquetes[, br])]
variables_explicatives_analisi <- variables_explicatives_analisi[!edat %in% c("EC01", "EC24", "EC59", "EC1014"), 
                                               .(poblacio = sum(poblacio, na.rm = T),
                                               num_edat = sum(sum_edat, na.rm = T),
                                               num_dona = sum(sum_dona, na.rm = T),
                                               num_institucionalitzats = sum(institucionalitzats, na.rm = T),
                                               num_immigrants_renta_baixa = sum(immigrants_renta_baixa, na.rm = T),
                                               num_pensionistes = sum(pensionistes, na.rm = T),
                                               num_pcc = sum(pensionistes, na.rm = T),
                                               num_maca = sum(maca, na.rm = T),
                                               num_atdom = sum(atdom, na.rm  =T),
                                               num_gma = sum(num_gma, na.rm = T), den_gma = sum(den_gma, na.rm = T),
                                               num_medea = sum(num_medea, na.rm = T), den_medea = sum(den_medea, na.rm = T)), by = "br"]
variables_explicatives_analisi[, ":=" (
  poblacio = poblacio,
  edat = num_edat/poblacio,
  sexe = num_dona/poblacio*100,
  institucionalitzats = num_institucionalitzats/poblacio*100,
  immigrants_renta_baixa = num_immigrants_renta_baixa/poblacio*100,
  pensionistes = num_pensionistes/poblacio*100,
  pcc = num_pcc/poblacio*100,
  maca = num_maca/poblacio*100,
  atdom = num_atdom/poblacio*100,
  gma = num_gma/den_gma,
  medea = num_medea/den_medea)]
```

```{r}
descripcio_eap <- 
  do.call("rbind",
          lapply(c("poblacio", "edat", "sexe", "institucionalitzats", "immigrants_renta_baixa", "pensionistes", "pcc", "maca", "atdom", "gma"), function(x){
            if (is.numeric(variables_explicatives_analisi[, get(x)])){
              t <- cbind(
                data.table(
                " " = x,
                "Nombre d'EAP" = nrow(unique(variables_explicatives_analisi[!is.na(x), "br"])),
                "Mitjana" = mean(variables_explicatives_analisi[, get(x)], na.rm = T),
                "DE" = sd(variables_explicatives_analisi[, get(x)], na.rm = T),
                "Coeficient de Variació" = mean(variables_explicatives_analisi[, get(x)], na.rm = T)/sd(variables_explicatives_analisi[, get(x)], na.rm = T),
                "Mínim" = quantile(variables_explicatives_analisi[, get(x)], probs = 0, na.rm = T),
                "P25" = quantile(variables_explicatives_analisi[, get(x)], probs = 0.25, na.rm = T),
                "P50" = quantile(variables_explicatives_analisi[, get(x)], probs = 0.5, na.rm = T),
                "P60" = quantile(variables_explicatives_analisi[, get(x)], probs = 0.6, na.rm = T),
                "P70" = quantile(variables_explicatives_analisi[, get(x)], probs = 0.7, na.rm = T),
                "P75" = quantile(variables_explicatives_analisi[, get(x)], probs = 0.75, na.rm = T),
                "P80" = quantile(variables_explicatives_analisi[, get(x)], probs = 0.8, na.rm = T),
                "Màxim" = quantile(variables_explicatives_analisi[, get(x)], probs = 1, na.rm = T))
              )
            }
          }
                 )
          ) %>%
  kable(caption = "Descripció", digits = 2)
descripcio_eap %>%
  kable_styling(full_width = T) %>%
     column_spec(1, bold = T)
```


```{r}
dades_eap <- unique(khalix_dcast_etiquetes[!is.na(medea), c("br", "medea")])
eap <- unique(khalix_dcast_etiquetes[, c("br", "medea")])[, .N, medea][!is.na(medea)]
eap[, p := paste0(format(round(N/sum(N)*100, 2), decimal.mark = ",", big.mark = "."), "%")]
eap %>%
  kable(caption = "EAP participants") %>%
  kable_styling(full_width = T) %>%
     column_spec(1, bold = T)
```

```{r}
indicadors <- unique(khalix_dcast_etiquetes[, c("títol", "Grup")])
# indicadors[, N := .N, Grup]
indicadors[, c("Grup", "títol")][order(Grup)] %>%
  kable(caption = "Indicadors analitzats") %>%
  kable_styling(full_width = T) %>%
     column_spec(1, bold = T) %>%
  collapse_rows(1)

```

# Resultats

```{r}
n <- length(unique(khalix_dcast_etiquetes$br))
dg <- khalix_dcast_etiquetes[,.( mitjana_resultat = mean(resultat, na.rm = T),
                                 de_resultat = sd(resultat, na.rm = T),
                                 mitjana_changeresultat = mean(changeresultat, na.rm = T),
                                 de_changeresultat = sd(changeresultat, na.rm = T),
                                 cv = sd(resultat, na.rm = T)/mean(resultat, na.rm = T)*100), c("periode", "indicador", "literal curt", "Grup")]
dg[, lwr_resultat := mitjana_resultat - qt(0.975, df = n-1)*de_resultat/sqrt(n)]
dg[, uppr_resultat := mitjana_resultat + qt(0.975, df = n-1)*de_resultat/sqrt(n)]
dg[, lwr_changeresultat := mitjana_changeresultat - qt(0.975, df = n-1)*de_changeresultat/sqrt(n)]
dg[, uppr_changeresultat := mitjana_changeresultat + qt(0.975, df = n-1)*de_changeresultat/sqrt(n)]


dg[, actual := ifelse(periode %in% c("A2001", "A2002", "A2003", "A2004"), "Any 2020", "Any 2019")]
dg[, periode_any := substr(periode, 2, 3)]
dg[, periode_mes := substr(periode, 4, 5)]
```

## Comparació de l'evolució del resultat dels EAP

```{r fig.height=5, fig.width=10}

a <- lapply(unique(dg[!Grup %in% c("Seguiment", "Patologia aguda"), Grup]), function(l){
  # cat("  \n#",  l,  "  \n")
 
g <- ggplot(dg[Grup == l], aes(periode_mes, mitjana_resultat, linetype = actual, group = periode_any)) +
    geom_line() +
    # geom_ribbon(aes(ymin = lwr, ymax = uppr), alpha = .5) +  
    theme_classic() + 
    theme(
      plot.title = element_text(hjust = 0.5)) +
    scale_y_continuous(labels = scales::percent_format(accuracy = .01)) +
    scale_linetype_manual(values = c("Any 2020" = 1, "Any 2019" = 2)) +
    scale_x_discrete(breaks = c("01", "04", "07", "10"), labels = c("Gen", "Abr", "Jul", "Oct")) + 
    labs(title = l, x = "Mes de l'any", y = "Valor de l'indicador", color = "Indicadors", linetype = "") +
    facet_wrap(~ `literal curt`, scales = "free")
  
  print(g)


# cat("  \n")

})
```


```{r fig.height=5, fig.width=10}
a <- lapply(unique(dg[Grup %in% c("Seguiment", "Patologia aguda"), Grup]), function(l){
  # cat("  \n#",  l,  "  \n")
 
g <- ggplot(dg[Grup == l], aes(periode_mes, mitjana_resultat, linetype = actual, group = periode_any)) +
    geom_line() +
    # geom_ribbon(aes(ymin = lwr, ymax = uppr), alpha = .5) +  
    theme_classic() + 
    theme(
      plot.title = element_text(hjust = 0.5)) +
    # scale_y_continuous(labels = scales::percent_format(accuracy = .01)) +
    scale_linetype_manual(values = c("Any 2020" = 1, "Any 2019" = 2)) +
    scale_x_discrete(breaks = c("01", "04", "07", "10"), labels = c("Gen", "Abr", "Jul", "Oct")) + 
    labs(title = l, x = "Mes de l'any", y = "Valor de l'indicador", color = "Indicadors", linetype = "") +
    facet_wrap(~ `literal curt`, scales = "free")
  
  print(g)


# cat("  \n")

})
```


## Efecte de la COVID-19 sobre el resultat dels indicadors de l'EQA Adults

```{r}
efecte <- khalix_dcast_etiquetes[changeresultat != Inf, .SD, .SDcols = c("br", "medea", "rural", "indicador", "títol", "Grup", "periode", "changeresultat")]
efecte[, any := as.character(paste0("Any 20", substr(periode, 2, 3)))]
efecte[, mes := as.numeric(as.character(substr(periode, 4, 5)))]
efecte <- efecte[mes %in% c(1, 2, 3, 4)]
efecte_dcast <- dcast(efecte, br + medea + rural + indicador + `títol` + Grup + mes ~ any, value.var = "changeresultat")

efecte_dcast_result <- efecte_dcast[, lapply(.SD, mean, na.rm = T), .SDcols = c("Any 2019", "Any 2020"), by = c("indicador", "títol", "Grup", "mes")]
efecte_dcast_pvalue <- efecte_dcast[!is.na(`Any 2020`) & !is.na(`Any 2019`), .(
  pvalue = t.test(`Any 2020`, `Any 2019` , paired = T)$p.value,
  ic = paste0("[CI95%: ", round(t.test(`Any 2020`, `Any 2019` , paired = T)$conf.int[1], 2), "% - ", round(t.test(`Any 2020`, `Any 2019` , paired = T)$conf.int[2], 2), "%]")), by = c("indicador", "títol", "Grup", "mes")]
t_efecte <- merge(efecte_dcast_result, efecte_dcast_pvalue, by = c("indicador", "títol", "Grup", "mes"))
t_efecte[, Significatiu := ifelse(pvalue <= 0.05, "*", "")]
t_efecte[, `Diferència` := paste0(round((`Any 2020`- `Any 2019`), 2), "% " , ic)]

t_efecte[, Mes := factor(mes, levels = 1:4, labels = c("Gener", "Febrer", "Març", "Abril"))]

```

```{r fig.height=5, fig.width=10, results="asis"}

a <- lapply(unique(t_efecte[, Grup]), function(l){
  cat("  \n#",  l,  "  \n")
  
  t <- t_efecte[Grup == l, c("títol", "Mes", "Any 2019", "Any 2020", "Diferència", "pvalue", "Significatiu")]
  
  knitr::kable(t) %>%
     kable_styling(full_width = T) %>%
     column_spec(1, bold = T) %>%
    collapse_rows(1) %>%
    print()
   cat("  \n")
 })
t <- t_efecte[, c("Grup", "títol", "Mes", "Any 2019", "Any 2020", "Diferència", "pvalue", "Significatiu")]
fwrite(t, "Taula_sumplementaria.csv", sep = ";", dec = ",", row.names = F)
```

### Resum

```{r}
t_efecte_resum <- t_efecte[, .(
  empitjorament = paste0(format(sum(Significatiu == "*" & `Any 2019`>`Any 2020`), big.mark = ".", decimal.mark = ","), "/", .N, " (", format(round(mean(Significatiu == "*" & `Any 2019`>`Any 2020`)*100, 2), big.mark = ".", decimal.mark = ","), "%)"),
  millora = paste0(format(sum(Significatiu == "*" & `Any 2019`<`Any 2020`), big.mark = ".", decimal.mark = ","), "/", .N, " (", format(round(mean(Significatiu == "*" & `Any 2019`<`Any 2020`)*100, 2), big.mark = ".", decimal.mark = ","), "%)")), by = c("Mes", "Grup")]
t_efecte_resum <- dcast(t_efecte_resum, Grup ~ Mes, value.var = c("empitjorament", "millora"))

t_efecte_resum %>% kable(caption = "Percentatge d'indicadors amb percentatge de variació significatiu respecte el 2019") %>%
     kable_styling(full_width = T) %>%
     column_spec(1, bold = T) 
```

### MEDEA

```{r}
efecte_dcast_result_medea <- efecte_dcast[, lapply(.SD, mean, na.rm = T), .SDcols = c("Any 2019", "Any 2020"), by = c("medea", "indicador", "títol", "Grup", "mes")]
efecte_dcast_pvalue_medea <- efecte_dcast[!is.na(`Any 2020`) & !is.na(`Any 2019`), .(
  pvalue = t.test(`Any 2019`, `Any 2020` , paired = T)$p.value), by = c("medea", "indicador", "títol", "Grup", "mes")]
t_efecte_medea <- merge(efecte_dcast_result_medea, efecte_dcast_pvalue_medea, by = c("medea", "indicador", "títol", "Grup", "mes"))
t_efecte_medea[, Significatiu := ifelse(pvalue <= 0.05, "*", "")]

t_efecte_medea[, Mes := factor(mes, levels = 1:4, labels = c("Gener", "Febrer", "Març", "Abril"))]
```

```{r}
t_efecte_medea_resum <- t_efecte_medea[, .(
  empitjorament = paste0(format(sum(Significatiu == "*" & `Any 2019`>`Any 2020`), big.mark = ".", decimal.mark = ","), "/", .N, " (", format(round(mean(Significatiu == "*" & `Any 2019`>`Any 2020`)*100, 2), big.mark = ".", decimal.mark = ","), "%)"),
  millora = paste0(format(sum(Significatiu == "*" & `Any 2019`<`Any 2020`), big.mark = ".", decimal.mark = ","), "/", .N, " (", format(round(mean(Significatiu == "*" & `Any 2019`<`Any 2020`)*100, 2), big.mark = ".", decimal.mark = ","), "%)")), by = c("medea", "Mes", "Grup")]
t_efecte_medea_resum <- dcast(t_efecte_medea_resum, Grup + medea ~ Mes, value.var = c("empitjorament", "millora"))

t_efecte_medea_resum[!is.na(medea)] %>% kable(caption = "Percentatge d'indicadors amb percentatge de variació significatiu respecte el 2019 segons MEDEA") %>%
     kable_styling(full_width = T) %>%
     column_spec(1, bold = T) %>% 
  collapse_rows(1)
  
```

### rural

```{r}
efecte_dcast_result_rural <- efecte_dcast[, lapply(.SD, mean, na.rm = T), .SDcols = c("Any 2019", "Any 2020"), by = c("rural", "indicador", "títol", "Grup", "mes")]
efecte_dcast_pvalue_rural <- efecte_dcast[!is.na(`Any 2020`) & !is.na(`Any 2019`), .(
  pvalue = t.test(`Any 2019`, `Any 2020` , paired = T)$p.value), by = c("rural", "indicador", "títol", "Grup", "mes")]
t_efecte_rural <- merge(efecte_dcast_result_rural, efecte_dcast_pvalue_rural, by = c("rural", "indicador", "títol", "Grup", "mes"))
t_efecte_rural[, Significatiu := ifelse(pvalue <= 0.05, "*", "")]

t_efecte_rural[, Mes := factor(mes, levels = 1:4, labels = c("Gener", "Febrer", "Març", "Abril"))]
```

```{r}
t_efecte_rural_resum <- t_efecte_rural[, .(
  empitjorament = paste0(format(sum(Significatiu == "*" & `Any 2019`>`Any 2020`), big.mark = ".", decimal.mark = ","), "/", .N, " (", format(round(mean(Significatiu == "*" & `Any 2019`>`Any 2020`)*100, 2), big.mark = ".", decimal.mark = ","), "%)"),
  millora = paste0(format(sum(Significatiu == "*" & `Any 2019`<`Any 2020`), big.mark = ".", decimal.mark = ","), "/", .N, " (", format(round(mean(Significatiu == "*" & `Any 2019`<`Any 2020`)*100, 2), big.mark = ".", decimal.mark = ","), "%)")), by = c("rural", "Mes", "Grup")]
t_efecte_rural_resum <- dcast(t_efecte_rural_resum, Grup + rural ~ Mes, value.var = c("empitjorament", "millora"))

t_efecte_rural_resum[!is.na(rural)] %>% kable(caption = "Percentatge d'indicadors amb percentatge de variació significatiu respecte el 2019 segons rural") %>%
     kable_styling(full_width = T) %>%
     column_spec(1, bold = T) %>% 
  collapse_rows(1)
```

## Taula resum de l'empitjorament

```{r}
a <- lapply(list(t_efecte, t_efecte_medea, t_efecte_rural), function(x){
  t <- x[, paste0(format(round(mean(Significatiu == "*" & `Any 2019`>`Any 2020`)*100, 2), big.mark = ".", decimal.mark = ","), "%"), 
    by = setdiff(names(x), c("indicador_to_merge", "indicador", "mes", "Any 2019", "Any 2020", "pvalue", "Significatiu", "Diferència", "literal", "pare", "indicador_text", "literal curt", "títol"))]
})
resum_empitj_medea <- dcast(a[[2]][!is.na(medea)], Grup + Mes ~ medea, value.var = "V1")
resum_empitj <- merge(a[[1]], resum_empitj_medea, by = c("Grup", "Mes"))
```

```{r}
resum_empitj %>%
  kable(caption = "Percentatge d'indicadors amb resultats significativament pitjor respecte el 2019") %>%
     kable_styling(full_width = T) %>%
     column_spec(1, bold = T) %>% 
  collapse_rows(1)
```

## Gràfic resum de l'empitjorament

```{r}
a <- lapply(list(t_efecte, t_efecte_medea, t_efecte_rural), function(x){
  t <- x[, mean(Significatiu == "*" & `Any 2019`>`Any 2020`), 
    by = setdiff(names(x), c("indicador_to_merge", "indicador", "mes", "Any 2019", "Any 2020", "pvalue", "Significatiu", "Diferència", "literal", "pare", "indicador_text", "literal curt", "títol"))]
})
dg <- a[[2]]
```

```{r}
ggplot(dg[Grup != "Patologia aguda"], aes(Mes, V1, fill = medea)) +
  geom_bar(stat = "identity", position = "dodge", color = "#0D0D0D") + 
  theme_classic() + 
  scale_fill_manual("MEDEA", values = c("Rural" = "#FEFEFE", "1U" = "#CCCCCC", "2U" = "#A5A5A5", "3U" = "#595959", "4U" = "#0D0D0D")) + 
  scale_y_continuous(labels = scales::percent_format(accuracy = 1)) +
  facet_wrap(~ Grup) + labs(y = "Porcentaje de efecto negativo", x = "")

```

```{r fig.height=4, fig.width=10}
dg[, ruralitat := ifelse(medea == "Rural", "Rural", "Urbà")]
ggplot(dg[Grup != "Patologia aguda"], aes(medea, V1, fill = ruralitat)) +
  geom_bar(stat = "identity", position = "dodge", color = "#0D0D0D") + 
  theme_classic() + 
  theme(
    legend.position = "bottom"
  ) +
  scale_fill_manual("", values = c("Rural" = "#FEFEFE", "Urbà" = "#A5A5A5")) +
  scale_y_continuous(labels = scales::percent_format(accuracy = 1)) +
  facet_grid(Mes ~ Grup) + labs(y = "Porcentaje de efecto negativo", x = "")

```

```{r fig.height=4, fig.width=10}
dg[, ruralitat := ifelse(medea == "Rural", "Rural", "Urbà")]
ggplot(dg[Grup != "Patologia aguda"], aes(medea, V1, fill = ruralitat)) +
  geom_bar(stat = "identity", position = "dodge", color = "#0D0D0D") + 
  theme_classic() + 
  theme(
    legend.position = "none"
  ) +
  scale_fill_manual("", values = c("Rural" = "#FEFEFE", "Urbà" = "#A5A5A5")) +
  scale_y_continuous(breaks = seq(0, 10, 2)) +
  facet_grid(Mes ~ Grup) + labs(y = "Número de indicadores de efecto negativo", x = "")

```

```{r fig.height=6}
dg[, ruralitat := ifelse(medea == "Rural", "Rural", "Urbà")]
ggplot(dg[Grup != "Patologia aguda"], aes(medea, V1, fill = ruralitat)) +
  geom_bar(stat = "identity", position = "dodge", color = "#0D0D0D") + 
  theme_classic() + 
  theme(
    legend.position = "bottom"
  ) + 
  scale_fill_manual("MEDEA", values = c("Rural" = "#FEFEFE", "Urbà" = "#A5A5A5")) +
  scale_y_continuous(labels = scales::percent_format(accuracy = 1)) +
  facet_grid(Grup ~ Mes) + labs(y = "Porcentaje de efecto negativo", x = "")

```

```{r fig.height=6}
dg[, ruralitat := ifelse(medea == "Rural", "Rural", "Urbà")]
ggplot(dg[Grup != "Patologia aguda"], aes(medea, V1, fill = ruralitat)) +
  geom_bar(stat = "identity", position = "dodge", color = "#0D0D0D") + 
  theme_classic() + 
  theme(
    legend.position = "bottom"
  ) + 
  scale_fill_manual("MEDEA", values = c("Rural" = "#FEFEFE", "Urbà" = "#A5A5A5")) +
  scale_y_continuous(breaks = seq(0, 12, 2)) +
  facet_grid(Grup ~ Mes) + labs(y = "Nombre d'indicadors d'efecte negatiu", x = "")

```

<!-- ## Comparació de l'evolució del CV dels EAP -->

<!-- ```{r fig.height=5, fig.width=10} -->

<!-- a <- lapply(unique(etiquetes_dt[indicador_to_merge %in% unique(dg[, indicador_to_merge]), Grup]), function(l){ -->
<!--   # cat("  \n#",  l,  "  \n") -->

<!-- g <- ggplot(dg_etiquetes[indicador %in% unique(etiquetes_dt[Grup == l, indicador_to_merge])], aes(periode, cv)) + -->
<!--     geom_line(group = "identity") + -->
<!--     theme_classic() +  -->
<!--   geom_vline(xintercept = 14) + -->
<!--     theme( -->
<!--       axis.text.x = element_text(angle = 90, vjust = .5), -->
<!--       plot.title = element_text(hjust = 0.5)) + -->
<!--     # scale_y_continuous(labels = scales::percent_format(accuracy = 1)) + -->
<!--     # scale_linetype_manual(values = c("Any 2020" = 1, "Any 2019" = 2)) + -->
<!--     # scale_x_discrete(breaks = c("01", "04", "07", "10"), labels = c("Gen", "Abr", "Jul", "Oct")) +  -->
<!--     labs(title = l, x = "Mes de l'any", y = "Coeficient de variació", color = "Indicadors", linetype = "") + -->
<!--     facet_wrap(~ indicador_text_curt, scales = "free") -->

<!--   print(g) -->


<!-- # cat("  \n") -->

<!-- }) -->
<!-- ``` -->


