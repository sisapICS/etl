# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

homes21 = exemple(c, 'CRONICA;AYR21###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
dones21 = exemple(c, 'CRONICA;AYR21###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;DONA')


homes20 = exemple(c, 'CRONICA;AYR20###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
dones20 = exemple(c, 'CRONICA;AYR20###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;DONA')

homes19 = exemple(c, 'CRONICA;AYR19###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
dones19 = exemple(c, 'CRONICA;AYR19###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;DONA')

homes18 = exemple(c, 'CRONICA;AYR18###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
dones18 = exemple(c, 'CRONICA;AYR18###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;DONA')


c.close()

print('export')

file = u.tempFolder + "CRONICA_home_2021.txt"
with open(file, 'w') as f:
   f.write(homes21)

file = u.tempFolder + "CRONICA_dona_2021.txt"
with open(file, 'w') as f:
   f.write(dones21)

file = u.tempFolder + "CRONICA_home_2020.txt"
with open(file, 'w') as f:
   f.write(homes20)

file = u.tempFolder + "CRONICA_dona_2020.txt"
with open(file, 'w') as f:
   f.write(dones20)
   
file = u.tempFolder + "CRONICA_home_2019.txt"
with open(file, 'w') as f:
   f.write(homes19)

file = u.tempFolder + "CRONICA_dona_2019.txt"
with open(file, 'w') as f:
   f.write(dones19)
   
file = u.tempFolder + "CRONICA_home_2018.txt"
with open(file, 'w') as f:
   f.write(homes18)

file = u.tempFolder + "CRONICA_dona_2018.txt"
with open(file, 'w') as f:
   f.write(dones18)
