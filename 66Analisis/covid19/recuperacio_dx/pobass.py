# -*- coding: utf8 -*-

from lvclient import LvClient


def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

POB_homes22 = exemple(c, 'POBASS;AYR22###;AMBITOS###;NACIONS;EDATS1###;USUTIP;HOME')
POB_dones22 = exemple(c, 'POBASS;AYR22###;AMBITOS###;NACIONS;EDATS1###;USUTIP;DONA')

POB_homes23 = exemple(c, 'POBASS;AYR23###;AMBITOS###;NACIONS;EDATS1###;USUTIP;HOME')
POB_dones23 = exemple(c, 'POBASS;AYR23###;AMBITOS###;NACIONS;EDATS1###;USUTIP;DONA')

c.close()

print('export')

file = "pob_home_2022.txt"
with open(file, 'w') as f:
   f.write(POB_homes22)

file = "pob_dona_2022.txt"
with open(file, 'w') as f:
   f.write(POB_dones22)

file = "pob_home_2023.txt"
with open(file, 'w') as f:
   f.write(POB_homes23)

file = "pob_dona_2023.txt"
with open(file, 'w') as f:
   f.write(POB_dones23)


