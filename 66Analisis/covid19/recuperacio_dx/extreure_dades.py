# -*- coding: utf8 -*-

"""
Refredats
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = 'rec_recuperacio_dx'
db = 'permanent'

dx_file =  "neo_pell.txt"

class melanoma(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_codis()
        self.get_assignada()
        self.get_neos()
        self.export()

    def get_codis(self):
        """."""
        u.printTime("codis")
        self.codis = c.defaultdict(set)
        self.agrupadors = {}
        sql = "select criteri_codi, agrupador_desc from eqa_criteris where agrupador in (1, 18, 58, 12, 13, 47, 21, 62)"
        for cim10, agr in u.getAll(sql, 'nodrizas'):
            self.codis[cim10].add(agr)
            self.agrupadors[cim10] = agr
      
        self.tots = tuple(self.codis)
        
    
    def get_assignada(self):
        """."""
        u.printTime("assignada")
        self.pob = {}
        sql = 'select id_cip, usua_sexe, usua_data_naixement from assignada'
        for id, sexe, naix in u.getAll(sql, 'import'):
            self.pob[id] = {'sexe': sexe, 'naix': naix}
    
    
    def get_neos(self):
        """."""
        u.printTime("problemes")

        self.recomptes = c.Counter()

        
        sql = "select id_cip, pr_dde, date_format(pr_dde,'%Y%m'),  pr_cod_ps, pr_up \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_cod_ps in {}  \
                            and extract(year_month from pr_dde) >'201312' and \
                            extract(year_month from pr_dde) <'202201' and \
                            pr_data_baixa = 0 group by id_cip,pr_dde, date_format(pr_dde,'%Y%m'), pr_cod_ps, pr_up".format(self.tots)
        for id, dat, periode,codi, up in u.getAll(sql, 'import'):
            sexe = self.pob[id]['sexe'] if id in self.pob else None
            naix = self.pob[id]['naix'] if id in self.pob else None
            tipus = self.agrupadors[codi]
            try:
                ed = u.yearsBetween(naix, dat)
            except:
                ed = None
            if ed != None:
                self.recomptes[(periode, up, u.ageConverter(ed,5), u.sexConverter(sexe), tipus)] += 1
      


    def export(self):
        """."""
        cols = ("periode varchar(6)", "up varchar(10)", "edat varchar(10)", "sexe varchar(10)", "tipus varchar(300)", "recompte int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)

        upload = []
        for (periode, up, edat, sexe, tipus), n in self.recomptes.items():
            n = int (n)
            upload.append([periode, up, edat, sexe, tipus, n])
        u.listToTable(upload, tb, db)
                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    melanoma()
    
    u.printTime("Final")                 
