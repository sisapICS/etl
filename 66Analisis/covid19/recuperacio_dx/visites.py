# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

visites21 = exemple(c, 'VISCMBDAP###;AYR21###;AMBITOS;CATPROF;EDATS5;TIPPROF###;SEXE')
visites20 = exemple(c, 'VISCMBDAP###;AYR20###;AMBITOS;CATPROF;EDATS5;TIPPROF###;SEXE')
visites19 = exemple(c, 'VISCMBDAP###;AYR19###;AMBITOS;CATPROF;EDATS5;TIPPROF###;SEXE')


c.close()

print('export')

file = u.tempFolder + "visites21_.txt"
with open(file, 'w') as f:
   f.write(visites21)

file = u.tempFolder + "visites20_.txt"
with open(file, 'w') as f:
   f.write(visites20)

file = u.tempFolder + "visites19_.txt"
with open(file, 'w') as f:
   f.write(visites19)
