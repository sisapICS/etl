# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

homes21T4 = exemple(c, 'T82;A17T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones21T4 = exemple(c, 'T82;A17T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes21T3 = exemple(c, 'T82;A17T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones21T3 = exemple(c, 'T82;A17T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes21T2 = exemple(c, 'T82;A17T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones21T2 = exemple(c, 'T82;A17T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes21T1 = exemple(c, 'T82;A17T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones21T1 = exemple(c, 'T82;A17T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')


homes20T4 = exemple(c, 'T82;A16T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones20T4 = exemple(c, 'T82;A16T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes20T3 = exemple(c, 'T82;A16T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones20T3 = exemple(c, 'T82;A16T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes20T2 = exemple(c, 'T82;A16T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones20T2 = exemple(c, 'T82;A16T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes20T1 = exemple(c, 'T82;A16T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones20T1 = exemple(c, 'T82;A16T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')

homes19T4 = exemple(c, 'T82;A15T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones19T4 = exemple(c, 'T82;A15T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes19T3 = exemple(c, 'T82;A15T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones19T3 = exemple(c, 'T82;A15T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes19T2 = exemple(c, 'T82;A15T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones19T2 = exemple(c, 'T82;A15T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes19T1 = exemple(c, 'T82;A15T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones19T1 = exemple(c, 'T82;A15T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')

homes18T4 = exemple(c, 'T82;A14T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones18T4 = exemple(c, 'T82;A14T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes18T3 = exemple(c, 'T82;A14T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones18T3 = exemple(c, 'T82;A14T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes18T2 = exemple(c, 'T82;A14T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones18T2 = exemple(c, 'T82;A14T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes18T1 = exemple(c, 'T82;A14T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones18T1 = exemple(c, 'T82;A14T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')



c.close()

print('export')

file = u.tempFolder + "Obesitat_home_2017_T4.txt"
with open(file, 'w') as f:
   f.write(homes21T4)

file = u.tempFolder + "Obesitat_dona_2017_T4.txt"
with open(file, 'w') as f:
   f.write(dones21T4)
   
file = u.tempFolder + "Obesitat_home_2017_T3.txt"
with open(file, 'w') as f:
   f.write(homes21T3)

file = u.tempFolder + "Obesitat_dona_2017_T3.txt"
with open(file, 'w') as f:
   f.write(dones21T3)
   
file = u.tempFolder + "Obesitat_home_2017_T2.txt"
with open(file, 'w') as f:
   f.write(homes21T2)

file = u.tempFolder + "Obesitat_dona_2017_T2.txt"
with open(file, 'w') as f:
   f.write(dones21T2)
   
file = u.tempFolder + "Obesitat_home_2017_T1.txt"
with open(file, 'w') as f:
   f.write(homes21T1)

file = u.tempFolder + "Obesitat_dona_2017_T1.txt"
with open(file, 'w') as f:
   f.write(dones21T1)
   
file = u.tempFolder + "Obesitat_home_2016_T4.txt"
with open(file, 'w') as f:
   f.write(homes20T4)

file = u.tempFolder + "Obesitat_dona_2016_T4.txt"
with open(file, 'w') as f:
   f.write(dones20T4)
   
file = u.tempFolder + "Obesitat_home_2016_T3.txt"
with open(file, 'w') as f:
   f.write(homes20T3)

file = u.tempFolder + "Obesitat_dona_2016_T3.txt"
with open(file, 'w') as f:
   f.write(dones20T3)
   
file = u.tempFolder + "Obesitat_home_2016_T2.txt"
with open(file, 'w') as f:
   f.write(homes20T2)

file = u.tempFolder + "Obesitat_dona_2016_T2.txt"
with open(file, 'w') as f:
   f.write(dones20T2)
   
file = u.tempFolder + "Obesitat_home_2016_T1.txt"
with open(file, 'w') as f:
   f.write(homes20T1)

file = u.tempFolder + "Obesitat_dona_2016_T1.txt"
with open(file, 'w') as f:
   f.write(dones20T1)
   
file = u.tempFolder + "Obesitat_home_2015_T4.txt"
with open(file, 'w') as f:
   f.write(homes19T4)

file = u.tempFolder + "Obesitat_dona_2015_T4.txt"
with open(file, 'w') as f:
   f.write(dones19T4)
   
file = u.tempFolder + "Obesitat_home_2015_T3.txt"
with open(file, 'w') as f:
   f.write(homes19T3)

file = u.tempFolder + "Obesitat_dona_2015_T3.txt"
with open(file, 'w') as f:
   f.write(dones19T3)
   
file = u.tempFolder + "Obesitat_home_2015_T2.txt"
with open(file, 'w') as f:
   f.write(homes19T2)

file = u.tempFolder + "Obesitat_dona_2015_T2.txt"
with open(file, 'w') as f:
   f.write(dones19T2)
   
file = u.tempFolder + "Obesitat_home_2015_T1.txt"
with open(file, 'w') as f:
   f.write(homes19T1)

file = u.tempFolder + "Obesitat_dona_2015_T1.txt"
with open(file, 'w') as f:
   f.write(dones19T1)
   
file = u.tempFolder + "Obesitat_home_2014_T4.txt"
with open(file, 'w') as f:
   f.write(homes18T4)

file = u.tempFolder + "Obesitat_dona_2014_T4.txt"
with open(file, 'w') as f:
   f.write(dones18T4)
   
file = u.tempFolder + "Obesitat_home_2014_T3.txt"
with open(file, 'w') as f:
   f.write(homes18T3)

file = u.tempFolder + "Obesitat_dona_2014_T3.txt"
with open(file, 'w') as f:
   f.write(dones18T3)
   
file = u.tempFolder + "Obesitat_home_2014_T2.txt"
with open(file, 'w') as f:
   f.write(homes18T2)

file = u.tempFolder + "Obesitat_dona_2014_T2.txt"
with open(file, 'w') as f:
   f.write(dones18T2)
   
file = u.tempFolder + "Obesitat_home_2014_T1.txt"
with open(file, 'w') as f:
   f.write(homes18T1)

file = u.tempFolder + "Obesitat_dona_2014_T1.txt"
with open(file, 'w') as f:
   f.write(dones18T1)