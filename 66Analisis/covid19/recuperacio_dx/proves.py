# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

espiroh21 = exemple(c, 'ORCLINIC;AYR21###;AMBITOS###;CPD2516;EDATS5###;DERORIGEN###;HOME')
espirod21 = exemple(c, 'ORCLINIC;AYR21###;AMBITOS###;CPD2516;EDATS5###;DERORIGEN###;DONA')

espiroh20 = exemple(c, 'ORCLINIC;AYR20###;AMBITOS###;CPD2516;EDATS5###;DERORIGEN###;HOME')
espirod20 = exemple(c, 'ORCLINIC;AYR20###;AMBITOS###;CPD2516;EDATS5###;DERORIGEN###;DONA')

espiroh19 = exemple(c, 'ORCLINIC;AYR19###;AMBITOS###;CPD2516;EDATS5###;DERORIGEN###;HOME')
espirod19 = exemple(c, 'ORCLINIC;AYR19###;AMBITOS###;CPD2516;EDATS5###;DERORIGEN###;DONA')

c.close()

print('export')

file = u.tempFolder + "espiros_home_2021.txt"
with open(file, 'w') as f:
   f.write(espiroh21)

file = u.tempFolder + "espiros_dona_2021.txt"
with open(file, 'w') as f:
   f.write(espirod21)
   
file = u.tempFolder + "espiros_home_2020.txt"
with open(file, 'w') as f:
   f.write(espiroh20)

file = u.tempFolder + "espiros_dona_2020.txt"
with open(file, 'w') as f:
   f.write(espirod20)
   
file = u.tempFolder + "espiros_home_2019.txt"
with open(file, 'w') as f:
   f.write(espiroh19)

file = u.tempFolder + "espiros_dona_2019.txt"
with open(file, 'w') as f:
   f.write(espirod19)

