# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)
"""
homes21T4 = exemple(c, 'T82;A21T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones21T4 = exemple(c, 'T82;A21T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes21T3 = exemple(c, 'T82;A21T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones21T3 = exemple(c, 'T82;A21T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes21T2 = exemple(c, 'T82;A21T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones21T2 = exemple(c, 'T82;A21T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes21T1 = exemple(c, 'T82;A21T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones21T1 = exemple(c, 'T82;A21T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')


homes20T4 = exemple(c, 'T82;A20T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones20T4 = exemple(c, 'T82;A20T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes20T3 = exemple(c, 'T82;A20T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones20T3 = exemple(c, 'T82;A20T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes20T2 = exemple(c, 'T82;A20T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones20T2 = exemple(c, 'T82;A20T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes20T1 = exemple(c, 'T82;A20T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones20T1 = exemple(c, 'T82;A20T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')

homes19T4 = exemple(c, 'T82;A19T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones19T4 = exemple(c, 'T82;A19T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes19T3 = exemple(c, 'T82;A19T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones19T3 = exemple(c, 'T82;A19T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes19T2 = exemple(c, 'T82;A19T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones19T2 = exemple(c, 'T82;A19T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes19T1 = exemple(c, 'T82;A19T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones19T1 = exemple(c, 'T82;A19T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')

homes18T4 = exemple(c, 'T82;A18T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones18T4 = exemple(c, 'T82;A18T4;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes18T3 = exemple(c, 'T82;A18T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones18T3 = exemple(c, 'T82;A18T3;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes18T2 = exemple(c, 'T82;A18T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones18T2 = exemple(c, 'T82;A18T2;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
homes18T1 = exemple(c, 'T82;A18T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;HOME')
dones18T1 = exemple(c, 'T82;A18T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;DONA')
"""
OBESITAT23T1 = exemple(c, 'T82;A23T1;AMBITOS###;DIAGACTIU;EDATS5###;NOIMP;SEXE')

pobassat = exemple(c, 'POBASSAT;A2303;AMBITOS###;NACIONS;EDATS1###;USUTIP;SEXE')


c.close()

print('export')

file = u.tempFolder + "OBESITAT23T1.txt"
with open(file, 'w') as f:
   f.write(OBESITAT23T1)


file = u.tempFolder + "pobassat.txt"
with open(file, 'w') as f:
   f.write(pobassat)


"""
file = u.tempFolder + "Obesitat_home_2021T4.txt"
with open(file, 'w') as f:
   f.write(homes21T4)

file = u.tempFolder + "Obesitat_dona_2021T4.txt"
with open(file, 'w') as f:
   f.write(dones21T4)
   
file = u.tempFolder + "Obesitat_home_2021T3.txt"
with open(file, 'w') as f:
   f.write(homes21T3)

file = u.tempFolder + "Obesitat_dona_2021T3.txt"
with open(file, 'w') as f:
   f.write(dones21T3)
   
file = u.tempFolder + "Obesitat_home_2021T2.txt"
with open(file, 'w') as f:
   f.write(homes21T2)

file = u.tempFolder + "Obesitat_dona_2021T2.txt"
with open(file, 'w') as f:
   f.write(dones21T2)
   
file = u.tempFolder + "Obesitat_home_2021T1.txt"
with open(file, 'w') as f:
   f.write(homes21T1)

file = u.tempFolder + "Obesitat_dona_2021T1.txt"
with open(file, 'w') as f:
   f.write(dones21T1)
   
file = u.tempFolder + "Obesitat_home_2020T4.txt"
with open(file, 'w') as f:
   f.write(homes20T4)

file = u.tempFolder + "Obesitat_dona_2020T4.txt"
with open(file, 'w') as f:
   f.write(dones20T4)
   
file = u.tempFolder + "Obesitat_home_2020T3.txt"
with open(file, 'w') as f:
   f.write(homes20T3)

file = u.tempFolder + "Obesitat_dona_2020T3.txt"
with open(file, 'w') as f:
   f.write(dones20T3)
   
file = u.tempFolder + "Obesitat_home_2020T2.txt"
with open(file, 'w') as f:
   f.write(homes20T2)

file = u.tempFolder + "Obesitat_dona_2020T2.txt"
with open(file, 'w') as f:
   f.write(dones20T2)
   
file = u.tempFolder + "Obesitat_home_2020T1.txt"
with open(file, 'w') as f:
   f.write(homes20T1)

file = u.tempFolder + "Obesitat_dona_2020T1.txt"
with open(file, 'w') as f:
   f.write(dones20T1)
   
file = u.tempFolder + "Obesitat_home_2019T4.txt"
with open(file, 'w') as f:
   f.write(homes19T4)

file = u.tempFolder + "Obesitat_dona_2019T4.txt"
with open(file, 'w') as f:
   f.write(dones19T4)
   
file = u.tempFolder + "Obesitat_home_2019T3.txt"
with open(file, 'w') as f:
   f.write(homes19T3)

file = u.tempFolder + "Obesitat_dona_2019T3.txt"
with open(file, 'w') as f:
   f.write(dones19T3)
   
file = u.tempFolder + "Obesitat_home_2019T2.txt"
with open(file, 'w') as f:
   f.write(homes19T2)

file = u.tempFolder + "Obesitat_dona_2019T2.txt"
with open(file, 'w') as f:
   f.write(dones19T2)
   
file = u.tempFolder + "Obesitat_home_2019T1.txt"
with open(file, 'w') as f:
   f.write(homes19T1)

file = u.tempFolder + "Obesitat_dona_2019T1.txt"
with open(file, 'w') as f:
   f.write(dones19T1)
   
file = u.tempFolder + "Obesitat_home_2018T4.txt"
with open(file, 'w') as f:
   f.write(homes18T4)

file = u.tempFolder + "Obesitat_dona_2018T4.txt"
with open(file, 'w') as f:
   f.write(dones18T4)
   
file = u.tempFolder + "Obesitat_home_2018T3.txt"
with open(file, 'w') as f:
   f.write(homes18T3)

file = u.tempFolder + "Obesitat_dona_2018T3.txt"
with open(file, 'w') as f:
   f.write(dones18T3)
   
file = u.tempFolder + "Obesitat_home_2018T2.txt"
with open(file, 'w') as f:
   f.write(homes18T2)

file = u.tempFolder + "Obesitat_dona_2018T2.txt"
with open(file, 'w') as f:
   f.write(dones18T2)
   
file = u.tempFolder + "Obesitat_home_2018T1.txt"
with open(file, 'w') as f:
   f.write(homes18T1)

file = u.tempFolder + "Obesitat_dona_2018T1.txt"
with open(file, 'w') as f:
   f.write(dones18T1)
"""