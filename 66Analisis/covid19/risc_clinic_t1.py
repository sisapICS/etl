# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import csv,os,sys
from datetime import datetime, timedelta

import sisapUtils as u
import hashlib as h

zona = True


TODAY = d.datetime.now().date()
print TODAY

db = 'redics'

def get_hash(cip):
    """."""
    return h.sha1(cip).hexdigest().upper()

class taula1_risc(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_hashos()
        self.get_cataleg()
        self.get_pob()
        self.get_gma()
        self.get_medea()
        self.get_resis()
        self.get_rca()
        self.export_fitxer()
        
    def get_centres(self):
        """.""" 
        self.centres = {}
        agas = {}
        sql = "select abs_cod,aga_cod,aga_des from sisap_covid_cat_aga"
        for abs, aga, aga_des in u.getAll(sql, 'redics'):
            agas[int(abs)] = {'cod': aga, 'desc': aga_des}

        
        sql = "select abs_cod,eap from  sisap_covid_cat_abs"
        for abs, up in u.getAll(sql, 'redics'):
            if int(abs) in agas:
                cod = agas[int(abs)]['cod']
                desc = agas[int(abs)]['desc']
                self.centres[up] = {'aga': cod, 'aga_desc':desc}
 
    
    def get_hashos(self):
        """."""
        u.printTime("hashos")
        self.idcip_to_hash = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        for id, sec, hash in u.getAll(sql, "import"):
            self.idcip_to_hash[id] = {'hash': hash, 'sec': sec} 
    
    def get_cataleg(self):
        """.Agafem codis de territoris"""
        u.printTime("cataleg")
        self.cataleg = {}   
        sql = """select abs_cod, aga_cod, aga_des from sisap_covid_cat_aga"""
        for  abs, aga, aga_des  in u.getAll(sql, db):
            self.cataleg[abs] = {'AGA': aga, 'desc': aga_des}
    
    def get_pob(self):
        """Agafem poblacio """
        u.printTime("pob")
        self.dades = {}
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = "select id_cip_sec, codi_sector, upABS, upOrigen, sexe, edat, nacionalitat, institucionalitzat from assignada_tot"
        for id, sec, up, upO, sexe, edat, nac, insti in u.getAll(sql, 'nodrizas'):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            nac1 = 1 if nac in renta else 0
            if up in self.centres:
                aga = self.centres[up]['aga'] if up in self.centres else 'SE'
                aga_desc = self.centres[up]['aga_desc'] if up in self.centres else 'SE'
            else:
                aga = self.centres[upO]['aga'] if up in self.centres else 'SE'
                aga_desc = self.centres[upO]['aga_desc'] if up in self.centres else 'SE'
            self.dades[idh] = {
                              'sector': sec, 'up': up, 'aga': aga, 'aga_desc': aga_desc,'sex': sexe,'edat': edat,
                              'renta': nac1, 'gma_cod': None, 'gma_cmplx': None,'gma_num': None, 'medea': 9999999,  'insti': insti }    
    
    def get_gma(self):
        """Obté els tres valors de GMA."""
        u.printTime("gma")
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        for id, cod, cmplx, num in u.getAll(*sql):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            if idh in self.dades:
                self.dades[idh]['gma_cod'] = cod
                self.dades[idh]['gma_cmplx'] = cmplx
                self.dades[idh]['gma_num'] = num

    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        u.printTime("medea")
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            if idh in self.dades and sector in valors:
                self.dades[idh]['medea'] = valors[sector]
    
    def get_resis(self):
        """Obtenim els pacients en residencia de pac_resis"""
        u.printTime("resis")
        sql = """select hash 
                from sisap_covid_res_master"""
        self.resis = set([hash for hash, in u.getAll(sql, db)])

    def get_rca(self):
        """."""
        u.printTime("rca")
        self.pob = []
        sql = """select hash, sexe, data_naixement, abs
                from preduffa.sisap_covid_pac_rca 
                where situacio='A'"""
        for hash, sexe, naix, abs in u.getAll(sql, db):
            abs = str(abs)
            edat = u.yearsBetween(naix, TODAY)
            aga = self.cataleg[abs]['AGA'] if abs in self.cataleg else 'SE'
            aga_desc = self.cataleg[abs]['desc'] if abs in self.cataleg else 'SE'
            resis = 1 if hash in self.resis else 0
            self.pob.append([hash, aga, aga_desc, edat, sexe, resis])    
   
    def export_fitxer(self):
        """obtenim fitxer"""
        u.printTime("export")
        columns = ["id varchar(100)",   "aga varchar(10)", "aga_desc varchar(30)",
                    "edat int", "sexe varchar(10)",  'resis double']
        u.createTable('taula_risc_1', "({})".format(", ".join(columns)), 'permanent', rm=True)
        u.listToTable(self.pob, 'taula_risc_1', 'permanent')
        
        u.printTime("export 2")
        upload = [(id, d['up'],d['aga'],d['aga_desc'], d['sex'], d['edat'],d['renta'], d['gma_cod'], d['gma_cmplx'],d['gma_num'], d['medea'],  d['insti'])
                 for id, d in self.dades.items()]

        columns = ["id varchar(100)",   "up varchar(5)", "aga varchar(10)", "aga_desc varchar(300)","sexe varchar(1)",
                    "edat int", "nac1 int",  'gma_codi varchar(3)', 'gma_complexitat double',
                   'gma_num_croniques int', 'medea_seccio double', 
                   'institucionalitzat int']
        u.createTable('taula_risc_1b', "({})".format(", ".join(columns)), 'permanent', rm=True)
        u.listToTable(upload, 'taula_risc_1b', 'permanent')
        
           
if __name__ == '__main__':
    u.printTime("Inici")
    
    taula1_risc()
    
    u.printTime("Final")