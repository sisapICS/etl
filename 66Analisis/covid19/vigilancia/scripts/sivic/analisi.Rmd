***
***

# Resultats

## Càlcul Taxa incidència

### Global

Càlcul taxa d'incidència COVID-19 global

```{r}
# Creació setmana.
  dt.sivic[, data_setmana := isoweek(data)]

# Creació dia de la setmana.
  dt.sivic[, dia_setmana := wday(data)]

# Compare Groups

# Agregació per setmana
  dt.sivic.agr.setmana <- dt.sivic[, j = list(min_data = min(data),
                                              max_data = max(data),
                                              mostres_total = sum(mostres_total, na.rm = TRUE),
                                              mostres_positiu = sum(mostres_positiu, na.rm = TRUE),
                                              positius = sum(positiu, na.rm = TRUE),
                                              ili_n = sum(ili_n, na.rm = TRUE)
                                              ), .(data_setmana)]
  
# Càlcul Positius IC95% 
  dt.sivic.agr.setmana <- dt.sivic.agr.setmana[,positius_p := round((positius/mostres_total)*100,2)]
  dt.sivic.agr.setmana <- dt.sivic.agr.setmana[,positius_se := round(sqrt(((positius_p)*(100 - positius_p))/mostres_total),3)]
  #dt.sivic.agr.setmana <- dt.sivic.agr.setmana[,positius_se:= sqrt(((positius_p)*(100-positius_p))/525)]
  dt.sivic.agr.setmana <- dt.sivic.agr.setmana[,positius_p_li := round((positius_p - (1.96*positius_se)),2)]
  dt.sivic.agr.setmana <- dt.sivic.agr.setmana[,positius_p_ls := round((positius_p + (1.96*positius_se)),2)]
  
# Càlcul Covid N
  dt.sivic.agr.setmana <- dt.sivic.agr.setmana[,covid_n := round((positius_p*ili_n)/100,0)]
  dt.sivic.agr.setmana <- dt.sivic.agr.setmana[,covid_n_li := round((positius_p_li*ili_n)/100,0)]
  dt.sivic.agr.setmana <- dt.sivic.agr.setmana[,covid_n_ls := round((positius_p_ls*ili_n)/100,0)]
  
# Agregar Poblacio
  dt.poblacio[, total := 'Total']
  dt.poblacio.agr <- dt.poblacio[, j = list(pob_total = sum(pob_total),
                                            pob_ecap = sum(pob_ecap)), .(total)]
  
# Afegir població
  dt.sivic.agr.setmana[, total := 'Total']
  dt.sivic.agr.setmana <- merge(dt.sivic.agr.setmana, # N=20
                                dt.poblacio.agr, # N= 1
                                by = "total"
                                )
  
# Càlcul Taxa
  dt.sivic.agr.setmana <- dt.sivic.agr.setmana[,covid_taxa := round((covid_n/pob_ecap)*100000,1)]
  dt.sivic.agr.setmana <- dt.sivic.agr.setmana[,covid_taxa_li := round((covid_n_li/pob_ecap)*100000,1)]
  dt.sivic.agr.setmana <- dt.sivic.agr.setmana[,covid_taxa_ls := round((covid_n_ls/pob_ecap)*100000,1)]
  
# Taula  
  datatable(dt.sivic.agr.setmana[data_setmana >= 14 & data_setmana < 52,][order(-data_setmana)])
  
# Exportar  
  fwrite(dt.sivic.agr.setmana[data_setmana >= 14 & data_setmana < 52,],
         paste0("D:/SISAP/sisap/66Analisis/covid19/vigilancia/results/sivic/sivic_global_setmana",params$setmana,".csv"),
         sep = ";",
         dec = ".")
  
  
```

### >= 60 anys

Càlcul taxa d'incidència COVID-19 per pacients >= 60 anys

```{r}
# > 60

# Agregació per setmana
  dt.sivic.60.agr.setmana <- dt.sivic[edat_codi == 6, j = list(min_data = min(data),
                                                               max_data = max(data),
                                                               mostres_total = sum(mostres_total, na.rm = TRUE),
                                                               mostres_positiu = sum(mostres_positiu, na.rm = TRUE),
                                                               positius = sum(positiu, na.rm = TRUE),
                                                               ili_n = sum(ili_n, na.rm = TRUE)
                                                               ), .(data_setmana)]
  
# Càlcul Positius IC95% 
  dt.sivic.60.agr.setmana <- dt.sivic.60.agr.setmana[,positius_p := round((positius/mostres_total)*100,2)]
  dt.sivic.60.agr.setmana <- dt.sivic.60.agr.setmana[,positius_se := round(sqrt(((positius_p)*(100 - positius_p))/mostres_total),3)]
  #dt.sivic.60.agr.setmana <- dt.sivic.60.agr.setmana[,positius_se:= sqrt(((positius_p)*(100-positius_p))/125)]
  dt.sivic.60.agr.setmana <- dt.sivic.60.agr.setmana[,positius_p_li := round((positius_p - (1.96*positius_se)),2)]
  dt.sivic.60.agr.setmana <- dt.sivic.60.agr.setmana[,positius_p_ls := round((positius_p + (1.96*positius_se)),2)]
  
# Càlcul Covid N
  dt.sivic.60.agr.setmana <- dt.sivic.60.agr.setmana[,covid_n := round((positius_p*ili_n)/100,0)]
  dt.sivic.60.agr.setmana <- dt.sivic.60.agr.setmana[,covid_n_li := round((positius_p_li*ili_n)/100,0)]
  dt.sivic.60.agr.setmana <- dt.sivic.60.agr.setmana[,covid_n_ls := round((positius_p_ls*ili_n)/100,0)]
  
# Agregar Poblacio
  dt.poblacio[, total := 'Total']
  dt.poblacio.agr <- dt.poblacio[edat_codi == 6, j = list(pob_total = sum(pob_total),
                                                          pob_ecap = sum(pob_ecap)), .(total)]
  
# Afegir població
  dt.sivic.60.agr.setmana[, total := 'Total']
  dt.sivic.60.agr.setmana <- merge(dt.sivic.60.agr.setmana, # N=20
                                   dt.poblacio.agr, # N= 1
                                   by = "total"
                                   )
  
# Càlcul Taxa
  dt.sivic.60.agr.setmana <- dt.sivic.60.agr.setmana[,covid_taxa := round((covid_n/pob_ecap)*100000,1)]
  dt.sivic.60.agr.setmana <- dt.sivic.60.agr.setmana[,covid_taxa_li := round((covid_n_li/pob_ecap)*100000,1)]
  dt.sivic.60.agr.setmana <- dt.sivic.60.agr.setmana[,covid_taxa_ls := round((covid_n_ls/pob_ecap)*100000,1)]
  
# Taula  
  datatable(dt.sivic.60.agr.setmana[data_setmana >= 14 & data_setmana < 52,][order(-data_setmana)]) 
  
# Exportar  
  fwrite(dt.sivic.60.agr.setmana[data_setmana >= 14 & data_setmana < 52,],
         paste0("D:/SISAP/sisap/66Analisis/covid19/vigilancia/results/sivic/sivic_60_setmana",
                params$setmana, ".csv"),
         sep = ";",
         dec = ".")
```

***

## Representativitat Periode

### Edat

```{r}
# Representavitat
  dt.representativitat <- dt.sivic[data_setmana >= 19 & data_setmana <= params$setmana,]

  # Agregació per edat
  dt.sivic.agr.edat <- dt.representativitat[, j = list(min_data = min(data),
                                                       max_data = max(data),
                                                       mostres_total = sum(mostres_total, na.rm = TRUE),
                                                       mostres_positiu = sum(mostres_positiu, na.rm = TRUE),
                                                       positius = sum(positiu, na.rm = TRUE),
                                                       ili_n = sum(ili_n, na.rm = TRUE)
                                                      ), .(edat)]
dt.sivic.agr.edat[, total := 'Total']
dt.sivic.agr.edat[, edat := factor(edat, levels = c("0","1 i 2","3 i 4","5 a 14","15 a 44","45 a 59","60 a 69","70 a 79","80 o més"))]

dt.rep.micro <- dt.sivic.agr.edat[,{
    micro_den = sum(mostres_total)
    .SD[,.(micro_num = sum(mostres_total),
           micro_frac = round((sum(mostres_total)/micro_den)*100,2)),by = edat]
},by = .(total)]
#dt.rep.micro

dt.sivic.agr.edat[, total := 'Total']
dt.rep.ili <- dt.sivic.agr.edat[,{ili_den = sum(ili_n)
                                  .SD[,.(ili_num = sum(ili_n),
                                         ili_frac = round((sum(ili_n)/ili_den)*100,2)), by = edat]
                                 },by = .(total)]
#dt.rep.ili

dt.rep <- merge(dt.rep.ili,
                dt.rep.micro,
                by = c('edat'))
#dt.rep

dt.ili.piramide <- dt.rep[, .(edat, ili_num, ili_frac)][, poblacio := 'ILI']
setnames(dt.ili.piramide, "ili_num", "num")
setnames(dt.ili.piramide, "ili_frac", "frac")
dt.micro.piramide <- dt.rep[, .(edat, micro_num, micro_frac)][, poblacio := 'Microbiologia']
setnames(dt.micro.piramide, "micro_num", "num")
setnames(dt.micro.piramide, "micro_frac", "frac")
dt.ili.piramide[poblacio == "ILI", poblacio := "Sindròmic"]

dt.piramide <- data.table(rbind(dt.ili.piramide,
                                dt.micro.piramide))[,frac := ifelse(poblacio == "Sindròmic",-frac, frac)]

theme_set(theme_minimal() + theme(legend.position = 'bottom',
                                  legend.title = element_blank()))
gg_rep_edat <- ggplot(dt.piramide,
    aes(x = edat,
        y = frac, fill = poblacio)
    ) +
  # agregar el argumento color = "white", si gustan
    geom_col(position = "stack", alpha = 0.6) + 
    coord_flip() +
  # colores que me gustan
  scale_fill_manual("Població", values = c("Sindròmic" = "midnightblue", "Microbiologia" = "darkred")) +
  # tema minimalista
    #theme(legend.position="bottom") +
    theme_minimal() +
  # leyenda para el fondo
    #theme(
    #  legend.position = "bottom",
    #  plot.caption = element_text(hjust = 0)) +
  # etiquetas en positivo
    scale_y_continuous(labels = abs, limits = c(-max(abs(dt.piramide$frac)),max(abs(dt.piramide$frac)))) +
    #scale_fill_discrete(name = "Població", labels = c("Síndròmic", "Microbiologia")) +
    labs(
      y = "Percentatge",
      x = "Edat",
      title = "EDAT",
      caption = paste0("Periode: Setmames 19 - ",
                       params$setmana, "\nFont: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]"))
gg_rep_edat

# Taula  
  dt.table <- dcast(dt.piramide, edat ~ poblacio, value.var = c("num", "frac"))
  dt.table[, frac_ILI := abs(frac_ILI)]
  dt.table[, frac_dif := round(frac_Microbiologia - frac_ILI,2)]
  datatable(dt.table,
            rownames = FALSE) 
  
# Exportar  
  fwrite(dt.table,
         paste0("D:/SISAP/sisap/66Analisis/covid19/vigilancia/results/sivic/sivic_representativitat_periode_edat",
                params$setmana, ".csv"),
         sep = ";",
         dec = ".")
```

### Regio

```{r}
# Representavitat
  dt.representativitat <- dt.sivic[data_setmana >= 19 & data_setmana <= params$setmana,]

  # Agregació
  dt.sivic.agr.edat <- dt.representativitat[, j = list(min_data = min(data),
                                                       max_data = max(data),
                                                       mostres_total = sum(mostres_total, na.rm = TRUE),
                                                       mostres_positiu = sum(mostres_positiu, na.rm = TRUE),
                                                       positius = sum(positiu, na.rm = TRUE),
                                                       ili_n = sum(ili_n, na.rm = TRUE)
                                                      ), .(regio)]
dt.sivic.agr.edat[, total := 'Total']

dt.rep.micro <- dt.sivic.agr.edat[,{micro_den = sum(mostres_total)
                                    .SD[,.(micro_num = sum(mostres_total),
                                           micro_frac = round((sum(mostres_total)/micro_den)*100,2)), by = regio]
                                    }, by = .(total)]
#dt.rep.micro

dt.sivic.agr.edat[, total := 'Total']
dt.rep.ili <- dt.sivic.agr.edat[,{ili_den = sum(ili_n)
                                  .SD[,.(ili_num = sum(ili_n),
                                         ili_frac = round((sum(ili_n)/ili_den)*100,2)), by = regio]
                                  }, by = .(total)]
#dt.rep.ili

dt.rep <- merge(dt.rep.ili,
                dt.rep.micro,
                by = c('regio'))
#dt.rep

dt.ili.piramide <- dt.rep[, .(regio, ili_num, ili_frac)][, poblacio := 'ILI']
setnames(dt.ili.piramide, "ili_num", "num")
setnames(dt.ili.piramide, "ili_frac", "frac")
dt.micro.piramide <- dt.rep[, .(regio, micro_num, micro_frac)][, poblacio := 'Microbiologia']
setnames(dt.micro.piramide, "micro_num", "num")
setnames(dt.micro.piramide, "micro_frac", "frac")

dt.piramide <- data.table(rbind(dt.ili.piramide,
                                dt.micro.piramide))[,frac := ifelse(poblacio == "ILI",-frac, frac)]

theme_set(theme_minimal() + theme(legend.position = 'bottom',
                                  legend.title = element_blank()))
gg_rep_regio <- ggplot(dt.piramide[regio != "No disponible",],
    aes(x = regio,
        y = frac, fill = poblacio)
    ) +
  # agregar el argumento color = "white", si gustan
    geom_col(position = "stack", alpha = 0.6) + 
    coord_flip() +
  # colores que me gustan
    scale_fill_manual("Població", values = c("midnightblue", "darkred")) +
  # tema minimalista
    #theme(legend.position="bottom") +
    theme_minimal() +
  # leyenda para el fondo
    #theme(
    #  legend.position = "bottom",
    #  plot.caption = element_text(hjust = 0)) +
  # etiquetas en positivo
    scale_y_continuous(labels = abs, limits = c(-max(abs(dt.piramide$frac)),max(abs(dt.piramide$frac)))) +
    labs(
      y = "Percentatge",
      x = "Regió",
      title = "REGIÓ",
      caption = paste0("Periode: Setmames 19 - ",
                       params$setmana, "\nFont: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]"))
gg_rep_regio   


# Taula  
  dt.table <- dcast(dt.piramide, regio ~ poblacio, value.var = c("num", "frac"))
  dt.table[, frac_ILI := abs(frac_ILI)]
  dt.table[, frac_dif := round(frac_Microbiologia - frac_ILI,2)]
  datatable(dt.table,
            rownames = FALSE) 
  
  
# Exportar  
  fwrite(dt.table,
         paste0("D:/SISAP/sisap/66Analisis/covid19/vigilancia/results/sivic/sivic_representativitat_periode_regio",
                params$setmana, ".csv"),
         sep = ";",
         dec = ".")
```

***

## Representativitat Setmana

### Edat

```{r}
# Representavitat
  dt.representativitat <- dt.sivic[data_setmana == params$setmana,]

  # Agregació per edat
  dt.sivic.agr.edat <- dt.representativitat[, j = list(min_data = min(data),
                                                       max_data = max(data),
                                                       mostres_total = sum(mostres_total, na.rm = TRUE),
                                                       mostres_positiu = sum(mostres_positiu, na.rm = TRUE),
                                                       positius = sum(positiu, na.rm = TRUE),
                                                       ili_n = sum(ili_n, na.rm = TRUE)
                                                      ), .(edat)]
dt.sivic.agr.edat[, total := 'Total']
dt.sivic.agr.edat[, edat := factor(edat, levels = c("0","1 i 2","3 i 4","5 a 14","15 a 44","45 a 59","60 a 69","70 a 79","80 o més"))]

dt.rep.micro <- dt.sivic.agr.edat[,{
    micro_den = sum(mostres_total)
    .SD[,.(micro_num = sum(mostres_total),
           micro_frac = round((sum(mostres_total)/micro_den)*100,2)),by = edat]
},by = .(total)]
#dt.rep.micro

dt.sivic.agr.edat[, total := 'Total']
dt.rep.ili <- dt.sivic.agr.edat[,{ili_den = sum(ili_n)
                                  .SD[,.(ili_num = sum(ili_n),
                                         ili_frac = round((sum(ili_n)/ili_den)*100,2)), by = edat]
                                 },by = .(total)]
#dt.rep.ili

dt.rep <- merge(dt.rep.ili,
                dt.rep.micro,
                by = c('edat'))
#dt.rep

dt.ili.piramide <- dt.rep[, .(edat,ili_frac)][, poblacio := 'ILI']
setnames(dt.ili.piramide, "ili_frac", "frac")
dt.micro.piramide <- dt.rep[, .(edat,micro_frac)][, poblacio := 'Microbiologia']
setnames(dt.micro.piramide, "micro_frac", "frac")

dt.piramide <- data.table(rbind(dt.ili.piramide,
                                dt.micro.piramide))[,frac := ifelse(poblacio == "ILI",-frac, frac)]

theme_set(theme_minimal() + theme(legend.position = 'bottom',
                                  legend.title = element_blank()))
gg_rep_edat <- ggplot(dt.piramide,
    aes(x = edat,
        y = frac, fill = poblacio)
    ) +
  # agregar el argumento color = "white", si gustan
    geom_col(position = "stack", alpha = 0.6) + 
    coord_flip() +
  # colores que me gustan
    scale_fill_manual("Població", values = c("midnightblue", "darkred")) +
  # tema minimalista
    #theme(legend.position="bottom") +
    theme_minimal() +
  # leyenda para el fondo
    #theme(
    #  legend.position = "bottom",
    #  plot.caption = element_text(hjust = 0)) +
  # etiquetas en positivo
    scale_y_continuous(labels = abs, limits = c(-max(abs(dt.piramide$frac)),max(abs(dt.piramide$frac)))) +
    labs(
      y = "Percentatge",
      x = "Edat",
      title = "EDAT",
      caption = paste0("Setmana ",
                       params$setmana,
                       ": (", params$setmana_text, ") \nFont: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]"))
gg_rep_edat      

# Taula  
  dt.table <- dcast(dt.piramide, edat ~ poblacio, value.var = "frac")
  dt.table[, ILI := abs(ILI)]
  dt.table[, frac_dif := round(Microbiologia - ILI,2)]
  datatable(dt.table,
            rownames = FALSE) 
  
# Exportar  
  fwrite(dt.table,
         paste0("D:/SISAP/sisap/66Analisis/covid19/vigilancia/results/sivic/sivic_representativitat_setmana_edat",
                params$setmana, ".csv"),
         sep = ";",
         dec = ".")
```

### Regio

```{r}
# Representavitat
  dt.representativitat <- dt.sivic[data_setmana == params$setmana,]

  # Agregació
  dt.sivic.agr.edat <- dt.representativitat[, j = list(min_data = min(data),
                                                       max_data = max(data),
                                                       mostres_total = sum(mostres_total, na.rm = TRUE),
                                                       mostres_positiu = sum(mostres_positiu, na.rm = TRUE),
                                                       positius = sum(positiu, na.rm = TRUE),
                                                       ili_n = sum(ili_n, na.rm = TRUE)
                                                      ), .(regio)]
dt.sivic.agr.edat[, total := 'Total']

dt.rep.micro <- dt.sivic.agr.edat[,{micro_den = sum(mostres_total)
                                    .SD[,.(micro_num = sum(mostres_total),
                                           micro_frac = round((sum(mostres_total)/micro_den)*100,2)), by = regio]
                                    }, by = .(total)]
#dt.rep.micro

dt.sivic.agr.edat[, total := 'Total']
dt.rep.ili <- dt.sivic.agr.edat[,{ili_den = sum(ili_n)
                                  .SD[,.(ili_num = sum(ili_n),
                                         ili_frac = round((sum(ili_n)/ili_den)*100,2)), by = regio]
                                  }, by = .(total)]
#dt.rep.ili

dt.rep <- merge(dt.rep.ili,
                dt.rep.micro,
                by = c('regio'))
#dt.rep

dt.ili.piramide <- dt.rep[, .(regio,ili_frac)][, poblacio := 'ILI']
setnames(dt.ili.piramide, "ili_frac", "frac")
dt.micro.piramide <- dt.rep[, .(regio,micro_frac)][, poblacio := 'Microbiologia']
setnames(dt.micro.piramide, "micro_frac", "frac")

dt.piramide <- data.table(rbind(dt.ili.piramide,
                                dt.micro.piramide))[,frac := ifelse(poblacio == "ILI",-frac, frac)]

theme_set(theme_minimal() + theme(legend.position = 'bottom',
                                  legend.title = element_blank()))
gg_rep_regio <- ggplot(dt.piramide[regio != "No disponible",],
    aes(x = regio,
        y = frac, fill = poblacio)
    ) +
  # agregar el argumento color = "white", si gustan
    geom_col(position = "stack", alpha = 0.6) + 
    coord_flip() +
  # colores que me gustan
    scale_fill_manual("Població", values = c("midnightblue", "darkred")) +
  # tema minimalista
    #theme(legend.position="bottom") +
    theme_minimal() +
  # leyenda para el fondo
    #theme(
    #  legend.position = "bottom",
    #  plot.caption = element_text(hjust = 0)) +
  # etiquetas en positivo
    scale_y_continuous(labels = abs, limits = c(-max(abs(dt.piramide$frac)),max(abs(dt.piramide$frac)))) +
    labs(
      y = "Percentatge",
      x = "Regió",
      title = "REGIÓ",
      caption = paste0("Setmana ",
                       params$setmana,
                       ": (", params$setmana_text, ") \nFont: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]"))
gg_rep_regio   


# Taula  
  dt.table <- dcast(dt.piramide, regio ~ poblacio, value.var = "frac")
  dt.table[, ILI := abs(ILI)]
  dt.table[, frac_dif := round(Microbiologia - ILI,2)]
  datatable(dt.table,
            rownames = FALSE) 
  
# Exportar  
  fwrite(dt.table,
         paste0("D:/SISAP/sisap/66Analisis/covid19/vigilancia/results/sivic/sivic_representativitat_setmana_regio",
                params$setmana, ".csv"),
         sep = ";",
         dec = ".")
```

***

## Representativitat Dilluns/Dimarts

### Edat

```{r}
# Representavitat
  dt.representativitat <- dt.sivic[data_setmana == params$setmana & dia_setmana %in% c(2,3),]

  # Agregació per edat
  dt.sivic.agr.edat <- dt.representativitat[, j = list(min_data = min(data),
                                                       max_data = max(data),
                                                       mostres_total = sum(mostres_total, na.rm = TRUE),
                                                       mostres_positiu = sum(mostres_positiu, na.rm = TRUE),
                                                       positius = sum(positiu, na.rm = TRUE),
                                                       ili_n = sum(ili_n, na.rm = TRUE)
                                                      ), .(edat)]
dt.sivic.agr.edat[, total := 'Total']
dt.sivic.agr.edat[, edat := factor(edat, levels = c("0","1 i 2","3 i 4","5 a 14","15 a 44","45 a 59","60 a 69","70 a 79","80 o més"))]

dt.rep.micro <- dt.sivic.agr.edat[,{
    micro_den = sum(mostres_total)
    .SD[,.(micro_num = sum(mostres_total),
           micro_frac = round((sum(mostres_total)/micro_den)*100,2)),by = edat]
},by = .(total)]
#dt.rep.micro

dt.sivic.agr.edat[, total := 'Total']
dt.rep.ili <- dt.sivic.agr.edat[,{ili_den = sum(ili_n)
                                  .SD[,.(ili_num = sum(ili_n),
                                         ili_frac = round((sum(ili_n)/ili_den)*100,2)), by = edat]
                                 },by = .(total)]
#dt.rep.ili

dt.rep <- merge(dt.rep.ili,
                dt.rep.micro,
                by = c('edat'))
#dt.rep

dt.ili.piramide <- dt.rep[, .(edat,ili_frac)][, poblacio := 'ILI']
setnames(dt.ili.piramide, "ili_frac", "frac")
dt.micro.piramide <- dt.rep[, .(edat,micro_frac)][, poblacio := 'Microbiologia']
setnames(dt.micro.piramide, "micro_frac", "frac")

dt.piramide <- data.table(rbind(dt.ili.piramide,
                                dt.micro.piramide))[,frac := ifelse(poblacio == "ILI",-frac, frac)]

theme_set(theme_minimal() + theme(legend.position = 'bottom',
                                  legend.title = element_blank()))
gg_rep_edat <- ggplot(dt.piramide,
    aes(x = edat,
        y = frac, fill = poblacio)
    ) +
  # agregar el argumento color = "white", si gustan
    geom_col(position = "stack", alpha = 0.6) + 
    coord_flip() +
  # colores que me gustan
    scale_fill_manual("Població", values = c("midnightblue", "darkred")) +
  # tema minimalista
    #theme(legend.position="bottom") +
    theme_minimal() +
  # leyenda para el fondo
    #theme(
    #  legend.position = "bottom",
    #  plot.caption = element_text(hjust = 0)) +
  # etiquetas en positivo
    scale_y_continuous(labels = abs, limits = c(-max(abs(dt.piramide$frac)),max(abs(dt.piramide$frac)))) +
    labs(
      y = "Percentatge",
      x = "Edat",
      title = "EDAT",
      caption = paste0("Setmana ",
                       params$setmana,
                       ": (", params$setmana_text, ") \nFont: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]"))
gg_rep_edat      

# Taula  
  dt.table <- dcast(dt.piramide, edat ~ poblacio, value.var = "frac")
  dt.table[, ILI := abs(ILI)]
  datatable(dt.table,
            rownames = FALSE) 
  
# Exportar  
  fwrite(dt.table,
         paste0("D:/SISAP/sisap/66Analisis/covid19/vigilancia/results/sivic/sivic_representativitat_dillunsdimarts_edat",
                params$setmana, ".csv"),
         sep = ";",
         dec = ".")
```

### Regio

```{r}
# Representavitat
  dt.representativitat <- dt.sivic[data_setmana == params$setmana,]

  # Agregació
  dt.sivic.agr.edat <- dt.representativitat[, j = list(min_data = min(data),
                                                       max_data = max(data),
                                                       mostres_total = sum(mostres_total, na.rm = TRUE),
                                                       mostres_positiu = sum(mostres_positiu, na.rm = TRUE),
                                                       positius = sum(positiu, na.rm = TRUE),
                                                       ili_n = sum(ili_n, na.rm = TRUE)
                                                      ), .(regio)]
dt.sivic.agr.edat[, total := 'Total']

dt.rep.micro <- dt.sivic.agr.edat[,{micro_den = sum(mostres_total)
                                    .SD[,.(micro_num = sum(mostres_total),
                                           micro_frac = round((sum(mostres_total)/micro_den)*100,2)), by = regio]
                                    }, by = .(total)]
#dt.rep.micro

dt.sivic.agr.edat[, total := 'Total']
dt.rep.ili <- dt.sivic.agr.edat[,{ili_den = sum(ili_n)
                                  .SD[,.(ili_num = sum(ili_n),
                                         ili_frac = round((sum(ili_n)/ili_den)*100,2)), by = regio]
                                  }, by = .(total)]
#dt.rep.ili

dt.rep <- merge(dt.rep.ili,
                dt.rep.micro,
                by = c('regio'))
#dt.rep

dt.ili.piramide <- dt.rep[, .(regio,ili_frac)][, poblacio := 'ILI']
setnames(dt.ili.piramide, "ili_frac", "frac")
dt.micro.piramide <- dt.rep[, .(regio,micro_frac)][, poblacio := 'Microbiologia']
setnames(dt.micro.piramide, "micro_frac", "frac")

dt.piramide <- data.table(rbind(dt.ili.piramide,
                                dt.micro.piramide))[,frac := ifelse(poblacio == "ILI",-frac, frac)]

theme_set(theme_minimal() + theme(legend.position = 'bottom',
                                  legend.title = element_blank()))
gg_rep_regio <- ggplot(dt.piramide[regio != "No disponible",],
    aes(x = regio,
        y = frac, fill = poblacio)
    ) +
  # agregar el argumento color = "white", si gustan
    geom_col(position = "stack", alpha = 0.6) + 
    coord_flip() +
  # colores que me gustan
    scale_fill_manual("Població", values = c("midnightblue", "darkred")) +
  # tema minimalista
    #theme(legend.position="bottom") +
    theme_minimal() +
  # leyenda para el fondo
    #theme(
    #  legend.position = "bottom",
    #  plot.caption = element_text(hjust = 0)) +
  # etiquetas en positivo
    scale_y_continuous(labels = abs, limits = c(-max(abs(dt.piramide$frac)),max(abs(dt.piramide$frac)))) +
    labs(
      y = "Percentatge",
      x = "Regió",
      title = "REGIÓ",
      caption = paste0("Setmana ",
                       params$setmana,
                       ": (", params$setmana_text, ") \nFont: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]"))
gg_rep_regio   


# Taula  
  dt.table <- dcast(dt.piramide, regio ~ poblacio, value.var = "frac")
  dt.table[, ILI := abs(ILI)]
  datatable(dt.table,
            rownames = FALSE) 
  
# Exportar  
  fwrite(dt.table,
         paste0("D:/SISAP/sisap/66Analisis/covid19/vigilancia/results/sivic/sivic_representativitat_dillunsdimarts_regio",
                params$setmana, ".csv"),
         sep = ";",
         dec = ".")
```


```{r analisi gc}
gc.var <- gc()
```

