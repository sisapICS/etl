***
***

# Data Manager

```{r}
### SIVIC_ECAP_DIAGNOSTIC
### Filtar dades 2022
### Es crea la columna ILI per fer l'agregació
### Modifiquem format columna data per poder fusionar posteriorment
### Filtrem la informació de les ILI
### Fem agregació dades per ILI + Variables

dt.dx.flt <- dt.dx[year(data) == 2022,]

dt.dx.flt[,ili := 0][diagnostic %in% c('ILI','COVID-19','Grip','Bronquiolitis'), ili := 1]
# Validació: dt.dx[, .N, .(ili,diagnostic)][order(-ili)]

# setnames(dt.dx.flt, 'data', 'datahour')
# dt.dx.flt[, data_t:=as.character(datahour)]
# dt.dx.flt[, data_t:=substr(data_t,1,10)]
# dt.dx.flt[, data:=as.Date(data_t,'%Y-%m-%d')]

dt.dx.flt <- dt.dx.flt[ili == 1,][,ili := NULL]

dt.dx.agr <- dt.dx.flt[, j = list(ili_n = sum(diari)), by = .(data,sexe,edat_codi,edat,regio,isc)]
```

```{r}
### SIVIC_AP_MOSTRES
### Modifiquem el noms de les columnes de contatge

setnames(dt.mostres, 'total', 'mostres_total')
setnames(dt.mostres, 'positiu', 'mostres_positiu')

dt.mostres.agr <- dt.mostres[, .(mostres_total = sum(mostres_total), mostres_positiu = sum(mostres_positiu)), .(data,sexe, edat_codi, edat, regio, isc)]
```

```{r}
### SIVIC_AP_PROVES
### Seleccionem els registres de les mostres de COVID-19
### Eliminen la columna "total" que no ens serveix
### Eliminen la columna "prova", ara és una constant

dt.proves.flt <- dt.proves[prova == 'PCR SARS-CoV-2',]

dt.proves.flt <- dt.proves.flt[, total := NULL]
dt.proves.flt <- dt.proves.flt[, prova := NULL]

dt.proves.agr <- dt.proves.flt[, .(positiu = sum(positiu)), .(data, sexe, edat_codi, edat, regio, isc)]
```

```{r}
### SIVIC_AP_POBLACIO
### Seleccionem la poblacio a 31/12/2021 
### Eliminen la columna "data" que no ens serveix
### Modifiquem el noms de les columnes de contatge

dt.poblacio <- dt.poblacio[, data := NULL]

setnames(dt.poblacio, 'total', 'pob_total')
setnames(dt.poblacio, 'ecap', 'pob_ecap')
```


```{r}
# Fusionar les 4 taules

dt.sivic <- merge(dt.proves.agr, # N=3.634
                  dt.mostres.agr, # N= 3.647
                  by.x = c("data", "sexe", "edat_codi", "edat", "regio", "isc"),
                  by.y = c("data", "sexe", "edat_codi", "edat", "regio", "isc"),
                  all = TRUE)

dt.sivic <- merge(dt.sivic, # N=3.634
                  dt.dx.agr, # N= 118.647 (té moltes més dates)
                  by.x = c("data", "sexe", "edat_codi", "edat", "regio", "isc"),
                  by.y = c("data", "sexe", "edat_codi", "edat", "regio", "isc"),
                  all = TRUE) 
# ¡¡¡OJO, se es all, all.x o all.y cambian mucho los resultados!!!
# Si se pone all.y se pierden muestras analizadas
# Si se pone all.x se pierden las ili que no están en las combinaciones de sivic, pero salen todas las muestras analizadas de la web
# Si se pone all no se pierden las ili que no están en las combinaciones de sivic y salen todas las muestras analizadas de la web

# dt.sivic <- merge(dt.sivic, # N=45.884
#                   dt.poblacio, # N= 398
#                   by.x = c("sexe", "edat_codi", "edat", "regio", "isc"),
#                   by.y = c("sexe", "edat_codi", "edat", "regio", "isc"),
#                   all.y = TRUE)
```

```{r data manager gc}
gc.var <- gc()
```

