***
***

# Importació

Importació de tota la informació necessària per realitzar l'estudi. Es filtren les select d'oracle a dades de l'any 2022.

```{r}
source("C:/Users/ehermosilla/Documents/Keys.R")
```

### SIVIC_ECAP_DIAGNOSTIC

Importació de la taula **SIVIC_ECAP_DIAGNOSTIC** d'exadata.

```{r}
if (params$actualitzar_dades) {
drv <- dbDriver("Oracle")
connect.string <- paste("(DESCRIPTION=",
                        "(ADDRESS=(PROTOCOL=tcp)(HOST=", hostexadata, ")(PORT=", portexadata, "))",
                        "(CONNECT_DATA=(SERVICE_NAME=", dbexadata, ")))", sep = "")
ini <- Sys.time()
con <- dbConnect(drv, username = idexadata, password = pwexadata, dbname = connect.string)
query <- dbSendQuery(con,
                     statement = "select * from dwsisap.sivic_ecap_diagnostic where data >= date '2022-01-01' and edat_codi > -1")
dt.dx <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)
fi <- Sys.time()
saveRDS(dt.dx, "../../data/dt_dx.rds")
} else {
  dt.dx <- readRDS("../../data/dt_dx.rds")
}
#fi - ini

gc.var <- gc()
```

Importació informació:
 
- files n = `r nrow(dt.dx)`
- columnes n = `r ncol(dt.dx)`
- columnes: `r names(dt.dx)`

```{r}
names(dt.dx) <- tolower(names(dt.dx))

dt.dx[, data := as.Date(as.character(as.POSIXct(data)))]

dt.dx[, `:=`(comarca = NULL, municipi = NULL, districte = NULL, aga = NULL)]
```

***

### SIVIC_AP_MOSTRES

Importació de la taula **SIVIC_AP_MOSTRES** d'exadata.

```{r}
if (params$actualitzar_dades) {
drv <- dbDriver("Oracle")
connect.string <- paste("(DESCRIPTION=",
                        "(ADDRESS=(PROTOCOL=tcp)(HOST=", hostexadata, ")(PORT=", portexadata, "))",
                        "(CONNECT_DATA=(SERVICE_NAME=", dbexadata, ")))", sep = "")
ini <- Sys.time()
con <- dbConnect(drv, username = idexadata, password = pwexadata, dbname = connect.string)
query <- dbSendQuery(con,
                     statement = "select * from dwsisap.sivic_ap_mostres where data >= date '2022-01-01' and edat_codi > -1")
dt.mostres <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)
fi <- Sys.time()
saveRDS(dt.mostres, "../../data/dt_mostres.rds")
} else {
  dt.mostres <- readRDS("../../data/dt_mostres.rds")
}
#fi - ini

gc.var <- gc()
```

Importació informació:
 
- files n = `r nrow(dt.mostres)`
- columnes n = `r ncol(dt.mostres)`
- columnes: `r names(dt.mostres)`

```{r}
names(dt.mostres) <- tolower(names(dt.mostres))

dt.mostres[, data := as.Date(as.character(as.POSIXct(data)))]

dt.mostres[, `:=`(comarca = NULL, municipi = NULL, districte = NULL, aga = NULL)]
```

***

### SIVIC_AP_PROVES

Importació de la taula **SIVIC_AP_PROVES** d'exadata.

```{r}
if (params$actualitzar_dades) {
connect.string <- paste("(DESCRIPTION=",
                        "(ADDRESS=(PROTOCOL=tcp)(HOST=", hostexadata, ")(PORT=", portexadata, "))",
                        "(CONNECT_DATA=(SERVICE_NAME=", dbexadata, ")))", sep = "")
ini <- Sys.time()
con <- dbConnect(drv, username = idexadata, password = pwexadata, dbname = connect.string)
query <- dbSendQuery(con,
                     statement = "select * from dwsisap.sivic_ap_proves where data >= date '2022-01-01' and edat_codi > -1")
dt.proves <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)
fi <- Sys.time() 
saveRDS(dt.proves, "../../data/dt_proves.rds")
} else {
  dt.proves <- readRDS("../../data/dt_proves.rds")
}
#fi - ini

gc.var <- gc()
```

Importació informació:
 
- files n = `r nrow(dt.proves)`
- columnes n = `r ncol(dt.proves)`
- columnes: `r names(dt.proves)`

```{r}
names(dt.proves) <- tolower(names(dt.proves))

dt.proves[, data := as.Date(as.character(as.POSIXct(data)))]

dt.proves[, `:=`(comarca = NULL, municipi = NULL, districte = NULL, aga = NULL)]
```

***

### SIVIC_POBLACIO

Importació de la taula **SIVIC_POBLACIO** d'exadata.

```{r}
if (params$actualitzar_dades) {
connect.string <- paste("(DESCRIPTION=",
                        "(ADDRESS=(PROTOCOL=tcp)(HOST=", hostexadata, ")(PORT=", portexadata, "))",
                        "(CONNECT_DATA=(SERVICE_NAME=", dbexadata, ")))", sep = "")
ini <- Sys.time()
con <- dbConnect(drv, username = idexadata, password = pwexadata, dbname = connect.string)
query <- dbSendQuery(con,
                     statement = "select * from dwsisap.sivic_poblacio where data >= date '2021-12-31' and edat_codi > -1")
dt.poblacio <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)
fi <- Sys.time()
saveRDS(dt.poblacio, "../../data/dt_poblacio.rds")
} else {
  dt.poblacio <- readRDS("../../data/dt_poblacio.rds")
}
#fi - ini

gc.var <- gc()
```

Importació informació:
 
- files n = `r nrow(dt.poblacio)`
- columnes n = `r ncol(dt.poblacio)`
- columnes: `r names(dt.poblacio)`

```{r}
names(dt.poblacio) <- tolower(names(dt.poblacio))

dt.poblacio[, data := as.Date(as.character(as.POSIXct(data)))]

dt.poblacio[, `:=`(comarca = NULL, municipi = NULL, districte = NULL, aga = NULL)]
```

```{r}
gc.var <- gc()
```

