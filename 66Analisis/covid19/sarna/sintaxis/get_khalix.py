# -*- coding: utf8 -*-

from lvclient import LvClient

#import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

POB_dones = exemple(c, 'POBASS;AYR23###;AMBITOS###;NACIONS;EDATS1###;USUTIP;DONA')
POB_homes = exemple(c, 'POBASS;AYR23###;AMBITOS###;NACIONS;EDATS1###;USUTIP;HOME')

c.close()

print('export')

file = "D:/pob_dona_2023.txt"
with open(file, 'w') as f:
   f.write(POB_dones)

file = "D:/pob_home_2023.txt"
with open(file, 'w') as f:
   f.write(POB_homes)