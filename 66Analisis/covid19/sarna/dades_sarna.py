# -*- coding: utf8 -*-

"""
Sarna
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = 'escabiosi'
tb2 = "escabiosi_tractaments"
tb3 = "escabiosi_pacient"
db = 'permanent'


class sarna(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_assignada()
        self.get_sarna()
        self.get_permetrina()
        self.export()
   
    def get_assignada(self):
        """."""
        u.printTime("assignada")
        self.pob = {}
        sql = 'select id_cip, usua_sexe, usua_data_naixement, usua_uab_up, usua_abs_codi_abs from assignada'
        for id, sexe, naix, up, abs in u.getAll(sql, 'import'):
            self.pob[id] = {'sexe': sexe, 'naix': naix, 'up': up, 'abs': abs}
    
    
    def get_sarna(self):
        """."""
        u.printTime("problemes")

        self.recomptes = c.Counter()
        self.pacients = []
        
        sql = "select id_cip, pr_dde, date_format(pr_dde,'%Y%m'),  pr_cod_ps, pr_up \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_cod_ps in ('B86','C01-B86') \
                            and extract(year_month from pr_dde) >'201312'  and \
                            pr_data_baixa = 0 group by id_cip,pr_dde, date_format(pr_dde,'%Y%m'), pr_cod_ps, pr_up"
        for id, dat, periode, codi, up in u.getAll(sql, 'import'):
            sexe = self.pob[id]['sexe'] if id in self.pob else None
            naix = self.pob[id]['naix'] if id in self.pob else None
            up2 = self.pob[id]['up'] if id in self.pob else None
            abs = self.pob[id]['abs'] if id in self.pob else None
            try:
                ed = u.yearsBetween(naix, dat)
            except:
                ed = None 
            if ed != None:
                self.recomptes[(dat, up, up2, abs, ed, u.sexConverter(sexe))] += 1
                self.pacients.append([id, dat, up, up2, abs, ed,u.sexConverter(sexe)])
      
    def get_permetrina(self):
        """."""
        u.printTime("tractaments")

        self.recomptesT = c.Counter()

        
        sql = "select id_cip,  ppfmc_pmc_data_ini, date_format(ppfmc_pmc_data_ini,'%Y%m')\
                       from tractaments  \
                            where extract(year_month from ppfmc_pmc_data_ini) >'201312'  and \
                            ppfmc_atccodi in ('P03AC04','P03AC54')"
        for id, dat, periode in u.getAll(sql, 'import'):
            sexe = self.pob[id]['sexe'] if id in self.pob else None
            naix = self.pob[id]['naix'] if id in self.pob else None
            up2 = self.pob[id]['up'] if id in self.pob else None
            try:
                ed = u.yearsBetween(naix, dat)
            except:
                ed = None
            if ed != None:
                self.recomptesT[(dat, up2, ed, u.sexConverter(sexe))] += 1
      

    def export(self):
        """."""
        cols = ("data date", "up varchar(10)", "up_ass varchar(10)", "abs varchar(10)", "edat int", "sexe varchar(10)",  "recompte int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)

        upload = []
        for (periode, up, up2, abs, edat, sexe), n in self.recomptes.items():
            n = int (n)
            upload.append([periode, up, up2, abs, edat, sexe, n])
        u.listToTable(upload, tb, db)
        
        cols = ("data date", "up varchar(10)",  "edat int", "sexe varchar(10)",  "recompte int")
        u.createTable(tb2, "({})".format(", ".join(cols)), db, rm=True)

        upload = []
        for (periode, up, edat, sexe), n in self.recomptesT.items():
            n = int (n)
            upload.append([periode, up, edat, sexe, n])
        u.listToTable(upload, tb2, db)
        
        cols = ("id_cip int", "data date", "up varchar(10)", "up_ass varchar(10)", "abs varchar(10)", "edat int", "sexe varchar(10)")
        u.createTable(tb3, "({})".format(", ".join(cols)), db, rm=True)

        u.listToTable(self.pacients, tb3, db)
        
        
if __name__ == '__main__':
    u.printTime("Inici")
     
    sarna()
    
    u.printTime("Final")                 
