---
title: "Anàlisi contagi GCE"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning = FALSE)
```

```{r message=FALSE, warning=FALSE, include=FALSE}
library(RMySQL)
library(data.table)
library(ggplot2)
library(kableExtra)
library(dplyr)
library(lubridate)
```

```{r message=FALSE, warning=FALSE, include=FALSE}
drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password = "",host = "10.80.217.68", port=3307, dbname="permanent")
sql = "select * from permanent.sisap_escoles_gce_jan27 where perfil = 'Alumne'"
dades <- data.table(dbGetQuery(con, sql))

sql = "select * from permanent.sisap_escoles_resul_jan27"
dades_resultat <- data.table(dbGetQuery(con, sql))

sql = "select * from permanent.cat_centres"
cat_centres <- data.table(dbGetQuery(con, sql))

dbDisconnect(con)
```

```{r}

dades[, naix := as.Date(naix)]

dades[data_prova == "", data_prova := NA]
dades[, data_prova := as.Date(data_prova)]
dades[data_resultat == "", data_resultat := NA]
dades[, data_resultat := as.Date(data_resultat)]
dades[, nivell := factor(nivell, levels = c("EINFLOE", "EPRILOE", "ESO", "BATXLOE", "EE", "CFPS", "CFPM", "CFAM", "PTVA", "ESCS", "CFAS", "PFI", "IFE", "ESCM"), labels = c("Educació infantil", "Educació primària", "Educació secundària obligatòria", "Batxillerat", "Educació especial", "Cicles formatius", "Cicles formatius", "Cicles formatius", "Altres", "Altres", "Cicles formatius", "Altres", "Altres", "Altres"))]
dades <- dades[nivell %in% c("Educació infantil", "Educació primària", "Educació secundària obligatòria", "Batxillerat", "Cicles formatius", "Educació especial")]
```

```{r}
cat_centres[, medea := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), labels = c("Rural", "Rural", "Rural", "1U", "2U", "3U", "4U"))]
```

```{r}
# Eliminem els alumnes no assignats a cap GCE
gce_no_especificat <- fread("D:/projectes/covid19/escoles/positivitat GBE/gce no especificat.txt", sep = "@", header = T)

dades <- dades[sense_cip == 0]
dades <- dades[gce_id != ""]
dades <- dades[!gce_id %in% gce_no_especificat$ID]
# Seleccionem els GCE amb un tamany entre 2 i 35 alumnes.
dades <- dades[gce_id %in% unique(dades[, c("id", "gce_id")])[, .N, gce_id][N > 1 & N<36]$gce_id]
# Unifiquem a un nivell per gce
unique_gce_nivell <- dades[, .N, c("gce_id", "nivell")][, max := max(N), c("gce_id")][N == max, c("gce_id", "nivell")]
dades <- merge(dades[, .SD, .SDcols = setdiff(names(dades), "nivell")], unique_gce_nivell[, c("gce_id", "nivell")], all.x = T)
dades[, edat := as.period(interval(start = naix, end = max(data_prova, na.rm = T)))$year]
dades[, curs := round(quantile(edat, probs = .5), 0), gce_id]
# Seleccionem els GCE amb edat mediana < 21 anys i els que l'edat mediana coincideix amb el nivell educatiu
dades <- dades[curs < 21]
dades <- dades[!(nivell == "Educació infantil" & curs > 5)]
dades <- dades[!(nivell == "Educació primària" & curs > 11)]
dades <- dades[!(nivell == "Educació secundària obligatòria" & curs > 15)]
dades <- dades[!(nivell == "Batxillerat" & curs > 17)]
# Trobem el cas índex per a cada gce i brot
dades_positiu <- dades[positiu == 1]
dades_positiu <- dades[!(data_prova > as.Date("2020-12-21") & data_prova < as.Date("2021-01-11"))]
dades_positiu[order(data_prova), data_prova_menys_1 := shift(data_prova, 1), gce_id]
dades_positiu[, data_prova_diferencia := data_prova - data_prova_menys_1]
dades_positiu[, cas_inicial := ifelse(is.na(data_prova_diferencia) | data_prova_diferencia > 10, 1, 0)]
dades_positiu[order(data_prova), brot := cumsum(cas_inicial), gce_id]
dades_positiu[, data_inici_brot := min(data_prova), c("gce_id", "brot")]

# Seleccionem els brots amb cas índex >= 10 dies
# dades_positiu <- dades_positiu[data_inici_brot < max(dades$data_prova, na.rm = T) - days(10)]

dades <- dades[positiu == 0 | (positiu == 1 & id %in% dades_positiu[, id])]
```

```{r}
dades <- merge(dades, cat_centres[, c("scs_codi", "medea")], by.x = "up", by.y = "scs_codi", all.x = T)
dades_positiu <- merge(dades_positiu, cat_centres[, c("scs_codi", "medea")], by.x = "up", by.y = "scs_codi", all.x = T)

```

```{r}
calc_casos_coexistint <- function(dt, vars = c("gce_id", "centre_id", "up", "data_inici_brot", "brot")){
  resultat <- dt[, .(
  alumnes_coexistint = .N
  ), by = vars]
}
```

```{r}
brots_global <- calc_casos_coexistint(unique(dades_positiu[, c("id", "gce_id", "centre_id", "up", "data_inici_brot", "brot")]))
brots_global[, trimestre := ifelse(data_inici_brot < as.Date("2020-12-22"), "1T", ifelse(data_inici_brot < as.Date("2021-03-27"), "2T", "3T"))]

brots_global_nivell <- calc_casos_coexistint(unique(dades_positiu[, c("id", "gce_id", "centre_id", "up", "data_inici_brot", "brot", "nivell")]), vars = c("gce_id", "centre_id", "up", "data_inici_brot", "brot", "nivell"))
brots_global_nivell[, trimestre := ifelse(data_inici_brot < as.Date("2020-12-22"), "1T", ifelse(data_inici_brot < as.Date("2021-03-27"), "2T", "3T"))]

brots_global_nivell_medea <- calc_casos_coexistint(unique(dades_positiu[, c("id", "gce_id", "centre_id", "up", "data_inici_brot", "brot", "nivell", "medea")]), vars = c("gce_id", "centre_id", "up", "data_inici_brot", "brot", "nivell", "medea"))
brots_global_nivell_medea[, trimestre := ifelse(data_inici_brot < as.Date("2020-12-22"), "1T", ifelse(data_inici_brot < as.Date("2021-03-27"), "2T", "3T"))]
```

##### Barplot per nivell educatiu

```{r}
taula_descriptiva_brots <- rbind(
  brots_global[, .(
  nivell = "Global",
  `% GCE sense cas secundari` = mean(alumnes_coexistint == 1)*100
  ), trimestre], 
brots_global_nivell[, .(
 `% GCE sense cas secundari` = mean(alumnes_coexistint == 1)*100
), c("nivell", "trimestre")])
```

```{r}
ggplot(taula_descriptiva_brots[nivell != "Global"], aes(nivell, `% GCE sense cas secundari`/100, fill = trimestre)) +
  geom_bar(stat = "identity", position="dodge") +
  labs(title = "Percentatge de casos en GCE que no han generat\n cap cas secundari segons nivell educatiu", y = "", x = "", fill = "") +
  scale_y_continuous(labels = scales::percent_format(accuracy = 1)
                     # , breaks = seq(0, 1, 1/10), limits = c(0, 1)
                     ) +
  scale_x_discrete(limits = c("Cicles formatius", "Batxillerat", "Educació secundària obligatòria", "Educació primària", "Educació infantil")) +
  scale_fill_manual(values = c("1T" = "#979dac", "2T" = "#5c677d", "3T" = "#d4d7d7")) +
  theme_classic() +
  theme(
  legend.position = "bottom"
  ) +
  # geom_hline(yintercept = taula_descriptiva_brots[nivell == "Global"]$`% GCE sense cas secundari`/100, aes(color = trimestre), linetype = 2) +
  coord_flip()
```

##### Barplot nivell educatiu i setmana de l'any

```{r}
dades_positiu[, periode := week(data_inici_brot)]
brots_global_nivell_periode <- calc_casos_coexistint(unique(dades_positiu[, c("id", "gce_id", "centre_id", "up", "data_inici_brot", "brot", "nivell", "periode")]), vars = c("gce_id", "centre_id", "up", "data_inici_brot", "brot", "nivell", "periode"))

taula_descriptiva_brots_periode <- brots_global_nivell_periode[, .(
  `N GCE sense cas secundari` = sum(alumnes_coexistint == 1),
  `N GCE amb algun cas secundari` = sum(alumnes_coexistint != 1),
  `N GCE amb més de 5 casos` = sum(alumnes_coexistint >6)
), c("nivell", "periode")]
```

```{r fig.height=8, fig.width=8}
taula_descriptiva_brots_periode_melt <- melt(taula_descriptiva_brots_periode, id.vars = c("nivell", "periode"), measure.vars = c("N GCE sense cas secundari", "N GCE amb algun cas secundari"))
ggplot(taula_descriptiva_brots_periode_melt[periode > 35 & periode <40], aes(as.factor(periode), value, fill = factor(variable, levels = c("N GCE amb algun cas secundari", "N GCE sense cas secundari")))) +
  geom_bar(stat = "Identity", position = "fill") + 
  labs(title = "% de casos en GCE amb algun cas secundari", y = "", x = "Setmana de l'any", fill = "") +
  scale_y_continuous(labels = scales::percent_format(accuracy = 1)
  #                    # breaks = seq(0, 1, 1/5)
                     ) +
  # scale_x_discrete(limits = c("37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "2",  "3")) +
  scale_fill_manual(values = c("N GCE amb algun cas secundari" = "#979dac", "N GCE sense cas secundari" = "#5c677d")) +
  theme_classic() +
  theme(
    axis.text.x = element_text(angle = 90, vjust = .5),
    legend.position = "bottom"
  ) + facet_wrap(~ nivell, ncol = 2) +
  geom_hline(yintercept = taula_descriptiva_brots[nivell == "Global" & trimestre == "2T"]$`% GCE sense cas secundari`/100, color = "#001233", linetype = 2)
  # geom_hline(yintercept = .9, color = "#001233", linetype = 2)
```

```{r}
dades_positiu[, periode := week(data_inici_brot)]
brots_global_nivell_periode <- calc_casos_coexistint(unique(dades_positiu[, c("id", "gce_id", "centre_id", "up", "data_inici_brot", "brot", "nivell", "periode")]), vars = c("gce_id", "centre_id", "up", "data_inici_brot", "brot", "nivell", "periode"))

taula_descriptiva_brots_periode <- brots_global_nivell_periode[, .(
  `N GCE sense cas secundari` = sum(alumnes_coexistint == 1),
  `N GCE amb algun cas secundari` = sum(alumnes_coexistint != 1),
  `N GCE amb més de 5 casos` = sum(alumnes_coexistint >6)
), c("periode")]
```

```{r}
taula_descriptiva_brots_periode_melt <- melt(taula_descriptiva_brots_periode, id.vars = c("periode"), measure.vars = c("N GCE sense cas secundari", "N GCE amb algun cas secundari", "N GCE amb més de 5 casos"))
# ggplot(taula_descriptiva_brots_periode_melt[periode < 30], aes(as.factor(periode), value, fill = factor(variable, levels = c("N GCE amb algun cas secundari", "N GCE sense cas secundari")))) + 
ggplot(taula_descriptiva_brots_periode_melt[variable == "N GCE amb més de 5 casos"], aes(as.factor(periode), value)) +   
  geom_bar(stat = "Identity") + 
  labs(title = "GCE amb més de 5 casos secundaris", y = "", x = "Setmana de l'any", fill = "") +
  # scale_y_continuous(labels = scales::percent_format(accuracy = 1)
  #                    # breaks = seq(0, 1, 1/5)
  #                    ) +
  scale_x_discrete(limits = c("37", "38", "39", "40")) +
  scale_fill_manual(values = c("N GCE amb algun cas secundari" = "#979dac", "N GCE sense cas secundari" = "#5c677d")) +
  theme_classic() +
  theme(
    axis.text.x = element_text(angle = 90, vjust = .5),
    legend.position = "bottom"
  ) 
  # facet_wrap(~ nivell, ncol = 2) +
  # geom_hline(yintercept = taula_descriptiva_brots[nivell == "Global" & trimestre == "2T"]$`% GCE sense cas secundari`/100, color = "#001233", linetype = 2)
  # geom_hline(yintercept = .9, color = "#001233", linetype = 2)
```

##### Taula

```{r}
taula <- rbind(
  brots_global[, .(
  nivell = "Global",
  `Nombre de GCE amb almenys un cas secundari` = paste0(format(sum(alumnes_coexistint > 1), big.mark = ".", decimal.mark = ","), " (", format(round(mean(alumnes_coexistint > 1)*100, 1), decimal.mark = ",", big.mark = "."), "%)"),
  `Nombre de GCE amb >5 cas secundari` = paste0(format(sum(alumnes_coexistint > 6), big.mark = ".", decimal.mark = ","), " (", format(round(mean(alumnes_coexistint > 6)*100, 1), decimal.mark = ",", big.mark = "."), "%)"),
  `Nombre de casos secundaris (Mínim)` = min(alumnes_coexistint[alumnes_coexistint > 1] - 1),
  `Nombre de casos secundaris (Mitjana)` = format(round(mean(alumnes_coexistint[alumnes_coexistint > 1] - 1), 1), decimal.mark = ",", big.mark = "."),
  `Nombre de casos secundaris (P50)` = quantile(alumnes_coexistint[alumnes_coexistint > 1] - 1, probs = .5),
  `Nombre de casos secundaris (P25)` = quantile(alumnes_coexistint[alumnes_coexistint > 1] - 1, probs = .25),
  `Nombre de casos secundaris (P75)` = quantile(alumnes_coexistint[alumnes_coexistint > 1] - 1, probs = .75),
  `Nombre de casos secundaris (Màxim)` = max(alumnes_coexistint[alumnes_coexistint > 1] - 1)
), trimestre], 
brots_global_nivell[, .(
  `Nombre de GCE amb almenys un cas secundari` = paste0(format(sum(alumnes_coexistint > 1), big.mark = ".", decimal.mark = ","), " (", format(round(mean(alumnes_coexistint > 1)*100, 1), decimal.mark = ",", big.mark = "."), "%)"),
   `Nombre de GCE amb >5 cas secundari` = paste0(format(sum(alumnes_coexistint > 6), big.mark = ".", decimal.mark = ","), " (", format(round(mean(alumnes_coexistint > 6)*100, 1), decimal.mark = ",", big.mark = "."), "%)"),
  `Nombre de casos secundaris (Mínim)` = min(alumnes_coexistint[alumnes_coexistint > 1] - 1),
  `Nombre de casos secundaris (Mitjana)` = format(round(mean(alumnes_coexistint[alumnes_coexistint > 1] - 1), 1), decimal.mark = ",", big.mark = "."),
  `Nombre de casos secundaris (P50)` = quantile(alumnes_coexistint[alumnes_coexistint > 1] - 1, probs = .5),
  `Nombre de casos secundaris (P25)` = quantile(alumnes_coexistint[alumnes_coexistint > 1] - 1, probs = .25),
  `Nombre de casos secundaris (P75)` = quantile(alumnes_coexistint[alumnes_coexistint > 1] - 1, probs = .75),
  `Nombre de casos secundaris (Màxim)` = max(alumnes_coexistint[alumnes_coexistint > 1] - 1)
), c("nivell", "trimestre")])
```


```{r}
setorderv(taula, c("nivell", "trimestre")) %>% 
  kable(digits = 2, escape = F, format.args = list(decimal.mark = ',', big.mark = "."), align = c("l", "l", rep("c", 8))) %>%
  kable_styling(full_width = T, position = "center") %>%
  row_spec(0, bold = T) 
```

```{r}
taula <- rbind(
  brots_global[, .(
  nivell = "Global",
  `Nombre de GCE amb almenys un cas coexistint` = paste0(format(sum(alumnes_coexistint > 1), big.mark = ".", decimal.mark = ","), " (", format(round(mean(alumnes_coexistint > 1)*100, 1), decimal.mark = ",", big.mark = "."), "%)"),
  `Nombre de casos coexistint (Mínim)` = min(alumnes_coexistint - 1),
  `Nombre de casos coexistint (Mitjana)` = format(round(mean(alumnes_coexistint - 1), 1), decimal.mark = ",", big.mark = "."),
  `Nombre de casos coexistint (P50)` = quantile(alumnes_coexistint - 1, probs = .5),
  `Nombre de casos coexistint (P25)` = quantile(alumnes_coexistint - 1, probs = .25),
  `Nombre de casos coexistint (P75)` = quantile(alumnes_coexistint - 1, probs = .75),
  `Nombre de casos coexistint (Màxim)` = max(alumnes_coexistint - 1)
), "trimestre"], 
brots_global_nivell[, .(
  `Nombre de GCE amb almenys un cas coexistint` = paste0(format(sum(alumnes_coexistint > 1), big.mark = ".", decimal.mark = ","), " (", format(round(mean(alumnes_coexistint > 1)*100, 1), decimal.mark = ",", big.mark = "."), "%)"),
  `Nombre de casos coexistint (Mínim)` = min(alumnes_coexistint - 1),
  `Nombre de casos coexistint (Mitjana)` = format(round(mean(alumnes_coexistint - 1), 1), decimal.mark = ",", big.mark = "."),
  `Nombre de casos coexistint (P50)` = quantile(alumnes_coexistint - 1, probs = .5),
  `Nombre de casos coexistint (P25)` = quantile(alumnes_coexistint - 1, probs = .25),
  `Nombre de casos coexistint (P75)` = quantile(alumnes_coexistint - 1, probs = .75),
  `Nombre de casos coexistint (Màxim)` = max(alumnes_coexistint - 1)
), c("trimestre", "nivell")])
```


```{r}
setorderv(taula, c("nivell", "trimestre")) %>% 
  kable(digits = 2, escape = F, format.args = list(decimal.mark = ',', big.mark = "."), align = c("l", rep("c", 7))) %>%
  kable_styling(full_width = T, position = "center") %>%
  row_spec(0, bold = T) 
```


