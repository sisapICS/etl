# -*- coding: utf8 -*-

import hashlib as h

"""
escoles sentinella
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


ensenyaments = {"BATXLOE": "Batxillerat",
                "CFAM": "Cicles formatius d'arts plàstiques",
                "CFAS": "Cicles formatius d'arts plàstiques",
                "CFPM": "Cicles formatius professionals",
                "CFPS": "Cicles formatius professionals",
                "EE": "Educació especial",
                "EINFLOE": "Educació infantil",
                "EPRILOE": "Educació primària",
                "ESCM": "Arts escèniques", "ESCS": "Arts escèniques",
                "ESO": "Educació secundària obligatòria",
                "IFE": "Itineraris formatius específics",
                "PFI": "Programes de formació i inserció",
                "PTVA": "Programes de trànsit a la vida adulta"}
                
anys_escolars = ["ACTUAL", "PASSAT"]

sentinelles = ['08001030', '43005911', '08020929', '08019265', '17004116', '08064854', '08039471', '25001321', '08029684', '08030947', '08040552', '08052839', '43011170', '43013099', '08076583', '08068495', '08052906', '17007609',
                '08024871', '25006732', '08042101', '08005394', '08025541']


class escoles_sentinella(object):
    """."""

    def __init__(self):
        """."""
        self.recomptes = c.Counter()
        self.rec_sentinella = c.Counter()
        self.get_perfils()
        self.get_regio()
        self.get_cursos()
        self.get_grups()
        self.get_nens()
        self.get_proves()
        self.get_positius()
        #self.get_brots()
        self.export_data()


    def get_perfils(self):
        """."""
        u.printTime("perfils")
        self.perfils = {}
        sql = "select id, nom from dwsisap_escola.PERSONES_PERFIL"
        for id, nom in u.getAll(sql, 'exadata'):
            self.perfils[id] = nom
    
    def get_regio(self):
        u.printTime("regio")
        self.regions = {}
        sql = "SELECT ID,ABS_ID, regio_des FROM dwsisap_escola.CENTRES_CENTRE a \
                INNER JOIN dwsisap.dbc_centres \
                ON abs_id=abs_cod"
        for escola, abs_c, regio in u.getAll(sql, 'exadata'):
            self.regions[(escola)] = {'abs': abs_c, 'regio': regio}
            
    def get_cursos(self):
        """Nivell"""
        u.printTime("Nivell")
        self.nivells = {}
        sql = """
                select a.id, d.codi from dwsisap_escola.GRUPS_curs a
                inner join dwsisap_escola.grups_nivell b on a.niVell_id=b.id 
                inner join dwsisap_escola.grups_ensenyament c on b.ENSENYAMENT_ID=c.id 
                inner join dwsisap_escola.grups_tipusensenyament d
                on c.TIPUS_ID=d.id
            """
        for curs, nivell in u.getAll(sql, 'exadata'):
            self.nivells[curs] = nivell
    
    def get_grups(self):
        """."""
        u.printTime("grups")
        self.alumnes = {}
        self.personal = {}
        sql = "select curs_id, gce_id,persona_id from dwsisap_escola.grups_alumne"
        for curs, gce_id, person_id in u.getAll(sql, 'exadata'):
            self.alumnes[person_id] = {'curs': curs, 'gce': gce_id}
            niv = self.nivells[curs]
            
        self.tipus_gce = {}
        sql = "select id, nom, centre_id from dwsisap_escola.grups_gce"
        for gce, nom, centre in u.getAll(sql, 'exadata'):
            self.tipus_gce[gce] = {'nom': nom, 'centre': centre}
            
        sql = "select  gce_id,persona_id from dwsisap_escola.grups_personal"
        for gce_id, person_id in u.getAll(sql, 'exadata'):
            self.personal[person_id] = gce_id 

    def get_nens(self):
        """."""
        u.printTime("nens")
        self.nens = {}
        self.profes = {}
        self.nensh = {}
        self.profesh = {}
        self.gce_to_regio = {}
        self.gce_to_escola = {}
        self.gce_to_nivell = {}
        a = 0
        sql = "select id, to_char(data_naixement,'YYYY-MM-DD'), sexe_id, perfil_id, substr(cip, 0, 13) from DWSISAP_ESCOLA.PERSONES_PERSONA"
        for id, naix, sex, perfil, cip2 in u.getAll(sql, 'exadata'):
            try:
                cip = h.sha1(cip2).hexdigest().upper()
            except:
                a += 1
                cip = 'Sense cip'
            if id in self.alumnes:
                curs = self.alumnes[id]['curs']
                gce = self.alumnes[id]['gce']
                centre = self.tipus_gce[gce]['centre'] if gce in self.tipus_gce else None
                regio = self.regions[(centre)]['regio'] if centre in self.regions else None
                nivell = self.nivells[curs] if curs in self.nivells else None
                self.gce_to_regio[gce] = regio
                self.gce_to_escola[gce] = centre
                try:
                    niv_agr = ensenyaments[nivell]
                except KeyError:
                    niv_agr= "Altres"
                self.nens[id] = {'curs': curs, 'gce': gce, 'centre':centre, 'regio':regio, 'nivell': niv_agr, 'sexe':sex}
                self.nensh[cip] = {'curs': curs, 'gce': gce, 'centre':centre, 'regio':regio, 'nivell': niv_agr, 'sexe':sex}
                self.gce_to_escola[gce] = niv_agr
            if id in self.personal:
                gce = self.personal[id]
                centre = self.tipus_gce[gce]['centre'] if gce in self.tipus_gce else None
                regio = self.regions[(centre)]['regio'] if centre in self.regions else None
                self.profes[id] = {'gce': gce, 'centre':centre, 'regio':regio, 'sexe':sex}
                self.profesh[cip] = {'gce': gce, 'centre':centre, 'regio':regio, 'sexe':sex}
        print a
        
    def get_proves(self):
        """proves"""
        u.printTime("proves")
        sql = """select hash,to_char(data,'YYYY-MM'), prova, resultat_des, motiu, entorn,simptomes
                        from preduffa.sisap_covid_pac_prv_raw 
        where (prova like ('%PCR%') or prova like ('%Antigen%')) and (to_char(data, 'YYYYMMDD')>'20210912' and to_char(data, 'YYYYMMDD') < '20211223')
        group by  hash,to_char(data,'YYYY-MM'), prova, resultat_des, motiu, entorn,simptomes"""
        for id, datap, prova, res, motiu, entorn, simpt in u.getAll(sql, 'redics'):
            if id in self.nensh:
                regio = self.nensh[id]['regio']
                nivell = self.nensh[id]['nivell']
                sexe = self.nensh[id]['sexe']
                centre = self.nensh[id]['centre']
                self.recomptes[(datap, 'Curs 2021-2022', 'tests', 'alumnes', regio, nivell, sexe)] += 1
                if centre in sentinelles:
                    self.rec_sentinella[(datap, 'Curs 2021-2022', 'tests', centre, 'alumnes', regio, nivell, sexe)] += 1
            if id in self.profesh:
                regio = self.profesh[id]['regio']
                sexe = self.profesh[id]['sexe']
                centre = self.profesh[id]['centre']
                self.recomptes[(datap, 'Curs 2021-2022', 'tests', 'personal', regio, 'no especificat', sexe)] += 1
                if centre in sentinelles:
                    self.rec_sentinella[(datap, 'Curs 2021-2022', 'tests', centre,  'personal', regio, 'no especificat', sexe)] += 1
    
    
    def get_positius(self):
        """."""
        u.printTime("positius")
        self.gce_afectats =  c.defaultdict(lambda: c.defaultdict(set))
        sql = "select persona_id, to_char(data_prova,'YYYY-MM'), data_prova from dwsisap_escola.clinica_positiu where data_prova <= DATE '2021-12-22'"
        for id, datap, datap2 in u.getAll(sql, 'exadata'):
            if id in self.nens:
                regio = self.nens[id]['regio']
                nivell = self.nens[id]['nivell']
                sexe = self.nens[id]['sexe']
                centre = self.nens[id]['centre']
                gce = self.nens[id]['gce']
                self.recomptes[(datap, 'Curs 2021-2022', 'casos', 'alumnes', regio, nivell, sexe)] += 1
                self.gce_afectats[gce][datap].add(datap2)
                if centre in sentinelles:
                    self.rec_sentinella[(datap, 'Curs 2021-2022', 'casos', centre, 'alumnes', regio, nivell, sexe)] += 1
            if id in self.profes:
                regio = self.profes[id]['regio']
                sexe = self.profes[id]['sexe']
                centre = self.profes[id]['centre']
                self.recomptes[(datap, 'Curs 2021-2022', 'casos', 'personal', regio, 'no especificat', sexe)] += 1
                if centre in sentinelles:
                    self.rec_sentinella[(datap, 'Curs 2021-2022', 'casos', centre,  'personal', regio, 'no especificat', sexe)] += 1
    
    def get_brots(self):
        """."""
        u.printTime("brots")
        brots = 0
        secundaris = 0
        self.index_cases = c.defaultdict(set)
        self.primaris = c.Counter()
        self.secundaris = c.Counter()
        for gce in self.gce_afectats:
            casi=0
            for mes in self.gce_afectats[gce]:
                casos = self.gce_afectats[gce][mes]
                for cas in sorted(casos):
                    print gce, cas
                    casp = 0
                    try:
                        dies = u.daysBetween(casi, cas)
                    except:
                        dies = 999
                    if 0 <= dies <= 10:
                        self.secundaris[(gce, mes)] += 1
                        casp = 2
                    else:
                        self.primaris[(gce, mes)] += 1
                        casp = 1                            
                        casi = cas
                    print casp
                    
        for (gce, mes), n in self.primaris.items():
            regio = self.gce_to_regio[gce]
            nivell = self.gce_to_nivell[gce]
            self.recomptes[(mes, 'ES3', 'num', 'alumnes', regio, nivell, 'NA')] += n
             
        for (gce, mes), n in self.secundaris.items():
            regio = self.gce_to_regio[gce]
            nivell = self.gce_to_nivell[gce]
            self.recomptes[(mes, 'ES4', 'num', 'alumnes', regio, nivell, 'NA')] += n
        
    def export_data(self):
        """."""
        u.printTime("export")
        
        upload = []
        for (datap, ind, tip,  per, regio, nivell, sexe), rec in self.recomptes.items():
            upload.append([datap, ind, tip,  per, regio, nivell, sexe, rec])
        file = u.tempFolder + "ES_global_2021.txt"
        u.writeCSV(file, upload, sep='@')

        upload = []
        for (datap, ind, tip, centre, per, regio, nivell, sexe), rec in self.rec_sentinella.items():
            upload.append([datap, ind, tip, centre, per, regio, nivell, sexe, rec])        
        file = u.tempFolder + "ES_sentinella_2021.txt"
        u.writeCSV(file, upload, sep='@')
        
if __name__ == '__main__':
    u.printTime("Inici")
     
    escoles_sentinella()
    
    u.printTime("Final")        


