# -*- coding: utf8 -*-

"""
update escoles profes
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = 'sisap_escoles_gce_jan03'
tb_profe = 'sisap_escoles_gce_profes'

db = 'permanent'


class escoles(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_profes()
        self.export()
        
    def get_profes(self):
        """."""
        u.printTime("perfils")
        self.perfils = []
        sql = "select persona_id, gce_id from dwsisap_escola.GRUPS_PERSONAL"
        for id, nom in u.getAll(sql, 'exadata'):
            self.perfils.append([id, nom])
    
    def export(self):
        """."""
        u.printTime("export")
        cols = ("id varchar(100)", "gce_id varchar(40)")
        u.createTable(tb_profe, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.perfils, tb_profe, db)
    
if __name__ == '__main__':
    u.printTime("Inici")
     
    escoles()
    
    u.printTime("Final")  

