# -*- coding: utf8 -*-

import hashlib as h

"""
Professions 
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


tb = "sisap_covid_nacionalitat"

db = 'permanent'


class Immigracio(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_comarca()
        self.get_pob()
        self.get_casos()
        self.get_taula()
        self.export_data()
        
    def get_centres(self):
        """Obtenim tipus eqap"""
        u.printTime("cataleg medea")
        self.centres = {}
        sql = """select scs_codi, medea
                from cat_centres"""
        for up, medea in u.getAll(sql, "nodrizas"):
            self.centres[up] = medea  
    
    def get_comarca(self):
        """.Agafem les comarques"""
        u.printTime("cataleg comarques")
        self.comarques = {}
        sql = """select  municipi_c, municipi, comarca_c, comarca from sisap_covid_cat_localitat"""
        for  municipi, desc_municipi, comarca, desc_comarca in u.getAll(sql, 'redics'):
            self.comarques[municipi] = {'desc': desc_municipi, 'comarca': comarca, 'desc_comarca': desc_comarca}    
    
    def get_pob(self):
        u.printTime("pob")
        """."""
        self.pob = {}
        
        sql = """select cip, data_naixement, eap, municipi, nacionalitat
                from DWSISAP.RCA_CIP_NIA"""
        for cip, naix, eap, municipi, nac in u.getAll(sql, 'exadata'):
            self.pob[cip] = {'naix': naix, 'eap':eap, 'nac':nac,  'municipi':municipi}

    
    def get_casos(self):
        u.printTime("casos")
        """"""
        self.casos = {}
        sql = """select hash, 
                cas_data_pdia, ingres_primer, ingres_ultim, ingres_uci_primer, ingres_uci_ultim,
                vac_dosi_1, vac_dosi_2, exitus_covid, to_char(cas_data_pdia, 'YYYYMM')
                from DWSISAP.DBC_METRIQUES"""
        for hash, cas, ingres1, ingres2,  uci1, uci2, vac1, vac2, exitus, periode in u.getAll(sql, 'exadata'):
            self.casos[hash] = {'cas': cas, 'ingres1':ingres1, 'ingres2':ingres2,'uci1':uci1, 'uci2':uci2,'vac1':vac1,'vac2':vac2,'exitus':exitus, 'periode':periode}
 
    def get_taula(self):
        u.printTime("pob")
        """Agafem poblacio entre 16 i 65 anys"""
        self.upload = []
        renta = {}
        sql = """select concat(codi_nac, ''), regio_desc, desc_nac, comunitari, if(renta=0,1,0)
              from cat_nacionalitat"""
        for nac, reg, desc, com, ren in u.getAll(sql, "nodrizas"):
		    renta[nac] = {'reg': reg, 'desc': desc, 'com': com, 'ren': ren}

        sql = """select hash, rca_cip,  data_naixement, edat, sexe,  rca_up, rca_municipi, es_actiu, ministeri_codi,ministeri_desc, te_rebuig_1, rebuig_1_motiu, te_rebuig_2, rebuig_2_motiu, IMMUNITZAT_DATA
                from DWSISAP.DBC_VACUNA"""
        for hash, cip, naix, edat, sexe, up, municipi, actiu, codiM, descM, te_rebuig1, rebuig_1_motiu, te_rebui2, rebuig_2_motiu, immunitzat in u.getAll(sql, 'exadata'):
            nac = self.pob[cip]['nac'] if cip in self.pob else None
            nac1 = renta[nac]['ren'] if nac in renta else 0
            regio = renta[nac]['reg'] if nac in renta else None
            descNac = renta[nac]['desc'] if nac in renta else None
            com = renta[nac]['com'] if nac in renta else 0
            cas = self.casos[hash]['cas'] if hash in self.casos else None
            ingres1 = self.casos[hash]['ingres1'] if hash in self.casos else None
            ingres2 = self.casos[hash]['ingres2'] if hash in self.casos else None
            uci1 = self.casos[hash]['uci1'] if hash in self.casos else None
            uci2 = self.casos[hash]['uci2'] if hash in self.casos else None
            vac1 = self.casos[hash]['vac1'] if hash in self.casos else None
            vac2 = self.casos[hash]['vac2'] if hash in self.casos else None
            exitus = self.casos[hash]['exitus'] if hash in self.casos else None
            periode = self.casos[hash]['periode'] if hash in self.casos else None	
            desc_municipi = self.comarques[municipi]['desc'] if municipi in self.comarques else None
            comarca = self.comarques[municipi]['comarca'] if municipi in self.comarques else None
            desc_comarca = self.comarques[municipi]['desc_comarca'] if municipi in self.comarques else None
            medea = self.centres[up] if up in self.centres else None
            ed = 'Sense info'
            if 0 <= edat < 20:
				ed = 'Menors de 20 anys'
            elif 20 <= edat < 60:
				ed = 'Entre 20 i 59 anys'
            elif edat >= 60:
				ed = 'Majors de 60 anys'
            self.upload.append([hash,  actiu, naix, edat,ed, sexe, nac, descNac, nac1, regio, com, up, medea, municipi, desc_municipi, comarca, desc_comarca, cas, periode, ingres1, ingres2, uci1, uci2, codiM, descM, immunitzat, vac1, vac2, te_rebuig1, rebuig_1_motiu, te_rebui2, rebuig_2_motiu, exitus])
	
    def export_data(self):
        """."""  
        u.printTime("export")
        cols = ("hash varchar(40)", "es_actiu int", "data_naix date", "edat int", "grup_edat varchar(300)", "sexe varchar(10)", "nacionalitat varchar(10)", "nacionalitat_desc varchar(300)",
		"renta_baixa int", "regio varchar(100)", "comunitari int",
        "up varchar(10)", "grup_medea varchar(10)",  "municipi_c varchar(10)", "municipi varchar(500)", "comarca_c varchar(10)", "comarca varchar(500)",
        "data_cas date", "mes_cas int", "ingres_p date", "ingres_u date", "uci_p date", "uci_u date", 
        "Ministeri_codi int", "Ministeri_desc varchar(300)", "immunitzat date", "vac_dosi1 date", "vac_dosi2 date", 
		"te_rebuig_1 int", "rebuig_1_motiu varchar(500)", "te_rebuig_2 int", "rebuig_2_motiu varchar(500)", "exitus_covid date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        file = u.tempFolder + "immigracio.txt"
        #u.writeCSV(file, self.upload, sep='@')

                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Immigracio()