# -*- coding: utf8 -*-

import hashlib as h

"""
Variables socioec 
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


tb = "sisap_covid_socioeconomic"

db = 'permanent'

titol_file =  "ass_titol_desc.txt"

class socioec(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_comarca()
        self.get_cataleg_prof()
        self.get_last_date()
        self.get_professions()
        self.get_titol()
        self.get_pob()
        self.get_casos()
        self.get_taula()
        self.export_data()
        
    def get_centres(self):
        """Obtenim tipus eqap"""
        u.printTime("cataleg medea")
        self.centres = {}
        sql = """select scs_codi, medea, aquas
                from cat_centres"""
        for up, medea, aquas in u.getAll(sql, "nodrizas"):
            self.centres[up] = {'medea': medea, 'aquas': aquas}
    
    def get_comarca(self):
        """.Agafem les comarques"""
        u.printTime("cataleg comarques")
        self.comarques = {}
        sql = """select  municipi_c, municipi, comarca_c, comarca from sisap_covid_cat_localitat"""
        for  municipi, desc_municipi, comarca, desc_comarca in u.getAll(sql, 'redics'):
            self.comarques[municipi] = {'desc': desc_municipi, 'comarca': comarca, 'desc_comarca': desc_comarca}    

    def get_cataleg_prof(self):
        u.printTime("cataleg Prof")
        """catàleg professions"""
        self.cataleg_prof = {}
        
        sql = """select cod_niv5, des_niv5, cod_niv1, des_niv1, cod_niv4, des_niv4 
                from  DWSISAP.SISAP_PRTTB105_cat_cno"""
        for prof, desc, niv1, desc1, niv4, desc4 in u.getAll(sql, 'exadata'):    
            self.cataleg_prof[prof] = {'desc': desc, 'niv1':niv1, 'desc1': desc1, 'niv4': niv4, 'desc4': desc4}
	
    def get_last_date(self):
        u.printTime("last date")
        """obtenim última data de professio"""
        self.last_date = {}
        sql = """select hash, max(ilt_data_baixa) 
                  from DWSISAP.SISAP_PRTTB105 
                  group by hash"""
        for hash, dbaixa in u.getAll(sql, 'exadata'):  
            self.last_date[(hash, dbaixa)] = True
                        

    def get_professions(self):
        u.printTime("professions")
        """Obtenim les professions i ajuntem amb casos"""
        self.professions = {}
        
        sql= """select hash, ilt_data_baixa, ilt_professio
            from DWSISAP.SISAP_PRTTB105"""
        for hash, dbaixa, prof in u.getAll(sql, 'exadata'):  
            if (hash, dbaixa) in self.last_date:
                if prof in self.cataleg_prof:
                    descp = self.cataleg_prof[prof]['desc'] 
                    niv4 = self.cataleg_prof[prof]['niv4'] 
                    desc4 = self.cataleg_prof[prof]['desc4'] 
                    niv1 = self.cataleg_prof[prof]['niv1'] 
                    desc1 = self.cataleg_prof[prof]['desc1']
                    self.professions[(hash)] = {'baixa': dbaixa, 'prof': prof, 'descp': descp, 'niv4': niv4,'desc4':desc4, 'niv1':niv1, 'desc1':desc1}
	
    def get_titol(self):
        """variable ass_titol de rca"""
        u.printTime("RCA titol")
        self.titol = {}
        self.cataleg_tit = {}
        for (tito,  desc_tit) in u.readCSV(titol_file, sep='@'):
            self.cataleg_tit[tito] = desc_tit
		
        sql = """SELECT ass_cip, ass_titol 
            FROM DWCATSALUT.RCA_PACIENTS"""
        for cip, tit in u.getAll(sql, 'exadata'):
            self.titol[cip] = tit
	
    def get_pob(self):
        u.printTime("pob Nacionalitat")
        """."""
        self.pob = {}
        
        sql = """select cip, data_naixement, eap, municipi, nacionalitat
                from DWSISAP.RCA_CIP_NIA"""
        for cip, naix, eap, municipi, nac in u.getAll(sql, 'exadata'):
            tit = self.titol[cip] if cip in self.titol else None
            self.pob[cip] = {'naix': naix, 'eap':eap, 'nac':nac,  'municipi':municipi, 'titol': tit}
   
    def get_casos(self):
        u.printTime("casos")
        """"""
        self.casos = {}
        sql = """select hash, 
                cas_data_pdia, ingres_primer, ingres_ultim, ingres_uci_primer, ingres_uci_ultim,
                vac_dosi_1, vac_dosi_1, exitus_covid, to_char(cas_data_pdia, 'YYYYMM')
                from DWSISAP.DBC_METRIQUES"""
        for hash, cas, ingres1, ingres2,  uci1, uci2, vac1, vac2, exitus, periode in u.getAll(sql, 'exadata'):
            self.casos[hash] = {'cas': cas, 'ingres1':ingres1, 'ingres2':ingres2,'uci1':uci1, 'uci2':uci2,'vac1':vac1,'vac2':vac2,'exitus':exitus, 'periode':periode}
 
    def get_taula(self):
        u.printTime("pob DBC vacunes")
        """."""
        self.upload = []
        renta = {}
        sql = """select concat(codi_nac, ''), regio_desc, desc_nac, comunitari, if(renta=0,1,0)
              from cat_nacionalitat"""
        for nac, reg, desc, com, ren in u.getAll(sql, "nodrizas"):
		    renta[nac] = {'reg': reg, 'desc': desc, 'com': com, 'ren': ren}

        sql = """select hash, rca_cip,  data_naixement, edat, sexe,  rca_up, rca_municipi, es_actiu, ministeri_codi,ministeri_desc, te_rebuig_1, rebuig_1_motiu, te_rebuig_2, rebuig_2_motiu
                from DWSISAP.DBC_VACUNA"""
        for hash, cip, naix, edat, sexe, up, municipi, actiu, codiM, descM, te_rebuig1, rebuig_1_motiu, te_rebui2, rebuig_2_motiu in u.getAll(sql, 'exadata'):
            tit = self.pob[cip]['titol'] if cip in self.pob else None
            desc_tit = self.cataleg_tit[tit] if tit in self.cataleg_tit else None
            nac = self.pob[cip]['nac'] if cip in self.pob else None
            nac1 = renta[nac]['ren'] if nac in renta else 0
            regio = renta[nac]['reg'] if nac in renta else None
            descNac = renta[nac]['desc'] if nac in renta else None
            com = renta[nac]['com'] if nac in renta else 0
            cas = self.casos[hash]['cas'] if hash in self.casos else None
            ingres1 = self.casos[hash]['ingres1'] if hash in self.casos else None
            ingres2 = self.casos[hash]['ingres2'] if hash in self.casos else None
            uci1 = self.casos[hash]['uci1'] if hash in self.casos else None
            uci2 = self.casos[hash]['uci2'] if hash in self.casos else None
            vac1 = self.casos[hash]['vac1'] if hash in self.casos else None
            vac2 = self.casos[hash]['vac2'] if hash in self.casos else None
            exitus = self.casos[hash]['exitus'] if hash in self.casos else None
            periode = self.casos[hash]['periode'] if hash in self.casos else None	
            desc_municipi = self.comarques[municipi]['desc'] if municipi in self.comarques else None
            comarca = self.comarques[municipi]['comarca'] if municipi in self.comarques else None
            desc_comarca = self.comarques[municipi]['desc_comarca'] if municipi in self.comarques else None
            medea = self.centres[up]['medea'] if up in self.centres else None
            aquas = self.centres[up]['aquas'] if up in self.centres else None
            ilt_baixa = self.professions[hash]['baixa'] if hash in self.professions else None
            prof = self.professions[hash]['prof'] if hash in self.professions else None
            descp = self.professions[hash]['descp'] if hash in self.professions else None
            niv4 = self.professions[hash]['niv4'] if hash in self.professions else None
            desc4 = self.professions[hash]['desc4'] if hash in self.professions else None
            niv1 = self.professions[hash]['niv1'] if hash in self.professions else None
            desc1 = self.professions[hash]['desc1'] if hash in self.professions else None
            ed = 'Sense info'
            if 0 <= edat < 20:
				ed = 'Menors de 20 anys'
            elif 20 <= edat < 60:
				ed = 'Entre 20 i 59 anys'
            elif edat >= 60:
				ed = 'Majors de 60 anys'
            self.upload.append([hash, actiu, naix, edat,ed, sexe, tit, desc_tit, nac, descNac, nac1, regio, com, up, medea, aquas, municipi, desc_municipi, comarca, desc_comarca,
			ilt_baixa, prof, descp, niv4, desc4, niv1, desc1, cas, ingres1, ingres2, uci1, uci2, codiM, descM, vac1, vac2, te_rebuig1, rebuig_1_motiu, te_rebui2, rebuig_2_motiu, exitus])
	
    def export_data(self):
        """."""  
        u.printTime("export")
        cols = ("hash varchar(40)", "es_actiu int", "data_naix date", "edat int", "grup_edat varchar(300)", "sexe varchar(10)", "ass_titol varchar(10)", "ass_titol_desc varchar(500)","nac varchar(10)", "nac_desc varchar(300)",
		"nac_renta_baixa int", "nac_regio varchar(100)", "nac_comunitari int",
        "up varchar(10)", "grup_medea varchar(10)", "SE_aquas double", "municipi_c varchar(10)", "municipi varchar(500)", "comarca_c varchar(10)", "comarca varchar(500)",
		"ocu_baixa date", "ocu_niv5 varchar(10)", "ocu_desc5 varchar(300)", "ocu_niv4 varchar(10)", "desc_niv4 varchar(300)", "ocu_niv1 varchar(10)", "desc_niv1 varchar(300)",
        "data_cas date",  "ingres_p date", "ingres_u date", "uci_p date", "uci_u date", 
        "Ministeri_codi int", "Ministeri_desc varchar(300)", "vac_dosi1 date", "vac_dosi2 date", 
		"te_rebuig_1 int", "rebuig_1_motiu varchar(500)", "te_rebuig_2 int", "rebuig_2_motiu varchar(500)", "exitus_covid date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)


                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    socioec()