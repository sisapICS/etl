# -*- coding: utf8 -*-

import hashlib as h

"""
Professions 
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


tb = "perc_prof"

db = 'permanent'


class Professions(object):
    """."""

    def __init__(self):
        """."""
        self.get_last_date()
        self.get_pob()
        self.export_data()
       
    def get_last_date(self):
        u.printTime("last date")
        """obtenim última data de professio"""
        self.last_date = {}
        sql = """select hash, ilt_data_baixa
                  from DWSISAP.SISAP_PRTTB105 
                  where ilt_data_baixa between DATE '2014-01-01' AND DATE '2018-12-31'"""
        for hash, dbaixa in u.getAll(sql, 'exadata'):  
            self.last_date[(hash)] = dbaixa
            
    def get_pob(self):
        u.printTime("pob")
        """Agafem poblacio entre 16 i 65 anys"""
        self.upload = []
        
        sql = """select hash, data_naixement, edat, sexe, situacio, rca_up, rca_municipi
                from DWSISAP.DBC_POBLACIO where es_rca=1 and situacio='A'"""
        for hash,naix, edat, sexe, situacio, up, municipi in u.getAll(sql, 'exadata'):
            dbaixa = self.last_date[hash] if hash in self.last_date else None
            self.upload.append([hash, naix, dbaixa, edat,sexe, up,  municipi])    
  
    def export_data(self):
        """."""  
        u.printTime("export")
        cols = ("hash varchar(40)", "data_naix date", "ilt_baixa date","edat int", "sexe varchar(10)", 
        "up varchar(10)", "municipi_c varchar(10)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        file = u.tempFolder + "professions_cov.txt"
        #u.writeCSV(file, self.upload, sep='@')

                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Professions()
    
    u.printTime("Final") 