# -*- coding: utf8 -*-

import hashlib as h

"""
Professions 
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


tb = "sisap_covid_recerca_professions_des21"

db = 'permanent'


class Professions(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_comarca()
        self.get_pob()
        self.get_casos()
        self.get_cataleg()
        self.get_last_date()
        self.get_professions()
        self.export_data()
        
    def get_centres(self):
        """Obtenim tipus eqap"""
        u.printTime("cataleg medea")
        self.centres = {}
        sql = """select scs_codi, medea
                from cat_centres"""
        for up, medea in u.getAll(sql, "nodrizas"):
            self.centres[up] = medea  
    
    def get_comarca(self):
        """.Agafem les comarques"""
        u.printTime("cataleg comarques")
        self.comarques = {}
        sql = """select  municipi_c, municipi, comarca_c, comarca from sisap_covid_cat_localitat"""
        for  municipi, desc_municipi, comarca, desc_comarca in u.getAll(sql, 'redics'):
            self.comarques[municipi] = {'desc': desc_municipi, 'comarca': comarca, 'desc_comarca': desc_comarca}    
    
    def get_pob(self):
        u.printTime("pob")
        """Agafem poblacio entre 16 i 65 anys"""
        self.pob = {}
        
        sql = """select hash, data_naixement, edat, sexe, situacio, rca_up, rca_municipi
                from DWSISAP.DBC_POBLACIO
                where edat between 16 and 65"""
        for hash,naix, edat, sexe, situacio, up, municipi in u.getAll(sql, 'exadata'):
            self.pob[hash] = {'naix': naix, 'edat':edat, 'sexe':sexe, 'situacio':situacio, 'up':up, 'municipi':municipi}

    
    def get_casos(self):
        u.printTime("casos")
        """Agafem poblacio entre 16 i 65 anys"""
        self.casos = {}
        
        sql = """select hash, 
                cas_data_pdia, ingres_primer, ingres_ultim, ingres_uci_primer, ingres_uci_ultim,
                vac_dosi_1, vac_dosi_2, exitus_covid, to_char(cas_data_pdia, 'YYYYMM')
                from DWSISAP.DBC_METRIQUES"""
        for hash, cas, ingres1, ingres2,  uci1, uci2, vac1, vac2, exitus, periode in u.getAll(sql, 'exadata'):
            self.casos[hash] = { 'cas': cas, 'ingres1':ingres1, 'ingres2':ingres2,'uci1':uci1, 'uci2':uci2,'vac1':vac1,'vac2':vac2,'exitus':exitus, 'periode':periode}

    def get_cataleg(self):
        u.printTime("cataleg")
        """catàleg professions"""
        self.cataleg = {}
        
        sql = """select cod_niv5, des_niv5, cod_niv1, des_niv1, cod_niv4, des_niv4 
                from  DWSISAP.SISAP_PRTTB105_cat_cno"""
        for prof, desc, niv1, desc1, niv4, desc4 in u.getAll(sql, 'exadata'):    
            self.cataleg[prof] = {'desc': desc, 'niv1':niv1, 'desc1': desc1, 'niv4': niv4, 'desc4': desc4}
            
    def get_last_date(self):
        u.printTime("last date")
        """obtenim última data de professio"""
        self.last_date = {}
        sql = """select hash, max(ilt_data_baixa) 
                  from DWSISAP.SISAP_PRTTB105 
                  group by hash"""
        for hash, dbaixa in u.getAll(sql, 'exadata'):  
            self.last_date[(hash, dbaixa)] = True
            
    def get_professions(self):
        u.printTime("professions")
        """Obtenim les professions i ajuntem amb casos"""
        self.upload = []
        
        sql= """select hash, ilt_data_baixa, ilt_professio
            from DWSISAP.SISAP_PRTTB105"""
        for hash, dbaixa, prof in u.getAll(sql, 'exadata'):  
            if (hash, dbaixa) in self.last_date:
                if hash in self.pob:
                    naix = self.pob[hash]['naix']
                    edat = self.pob[hash]['edat']
                    sexe = self.pob[hash]['sexe']
                    situacio = self.pob[hash]['situacio']
                    up = self.pob[hash]['up']
                    municipi = self.pob[hash]['municipi']
                    cas = self.casos[hash]['cas'] if hash in self.casos else None
                    ingres1 = self.casos[hash]['ingres1'] if hash in self.casos else None
                    ingres2 = self.casos[hash]['ingres2'] if hash in self.casos else None
                    uci1 = self.casos[hash]['uci1'] if hash in self.casos else None
                    uci2 = self.casos[hash]['uci2'] if hash in self.casos else None
                    vac1 = self.casos[hash]['vac1'] if hash in self.casos else None
                    vac2 = self.casos[hash]['vac2'] if hash in self.casos else None
                    exitus = self.casos[hash]['exitus'] if hash in self.casos else None
                    periode = self.casos[hash]['periode'] if hash in self.casos else None
                    medea = self.centres[up] if up in self.centres else None
                    desc_municipi = self.comarques[municipi]['desc'] if municipi in self.comarques else None
                    comarca = self.comarques[municipi]['comarca'] if municipi in self.comarques else None
                    desc_comarca = self.comarques[municipi]['desc_comarca'] if municipi in self.comarques else None
                    if prof in self.cataleg:
                        descp = self.cataleg[prof]['desc'] 
                        niv4 = self.cataleg[prof]['niv4'] 
                        desc4 = self.cataleg[prof]['desc4'] 
                        niv1 = self.cataleg[prof]['niv1'] 
                        desc1 = self.cataleg[prof]['desc1']
                        self.upload.append([hash, naix, dbaixa, edat,sexe, prof, descp, niv1, desc1, niv4, desc4, situacio, up, medea, municipi, desc_municipi, comarca, desc_comarca, cas, periode, ingres1, ingres2, uci1, uci2, vac1, vac2, exitus])
 
    def export_data(self):
        """."""  
        u.printTime("export")
        cols = ("hash varchar(40)", "data_naix date", "ilt_baixa date","edat int", "sexe varchar(10)", 
        "codi_prof varchar(10)", "desc_prof varchar(300)", "cod_niv1 varchar(10)", "desc_niv1 varchar(300)", "cod_niv4 varchar(10)", "desc_niv4 varchar(300)",
        "situacio varchar(10)", "up varchar(10)", "grup_medea varchar(10)","municipi_c varchar(10)", "municipi varchar(500)", "comarca_c varchar(10)", "comarca varchar(500)",
        "data_cas date", "mes_cas int", "ingres_p date", "ingres_u date", "uci_p date", "uci_u date", 
        "vac_dosi1 date", "vac_dosi2 date", "exitus_covid date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        file = u.tempFolder + "professions_cov.txt"
        #u.writeCSV(file, self.upload, sep='@')

                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Professions()
    
    u.printTime("Final") 