# -*- coding: utf8 -*-

import hashlib as h

"""
Professions
"""

import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u

"""
Mirem de fer les tendències de les professions. Agafem el nivell4. 
Excloem els militars (niv1=0)
Excloem la gent que té info de professió abans del 2016 
per anàlisi socioecon grup 2 vs 9 excloem metges i infermeres (211, 212)
"""


tb = "sisap_covid_recerca_professions_figura"

db = 'permanent'


class Professions(object):
    """."""

    def __init__(self):
        """."""
        self.get_dates()
        self.get_medea()
        self.get_comarques()
        self.get_cataleg()
        self.get_mes1000()
        self.get_denominadors()
        self.get_casos()
        self.get_dades()
        self.export_data()       
    
    def get_dates(self):
        """agafa totes les dates"""
        u.printTime("dates")
        self.periodes = {}
        sql = """select distinct data_cas from sisap_covid_recerca_professions where data_cas<>0"""
        for dat, in u.getAll(sql, "permanent"):
            self.periodes[dat] = True
    
    def get_medea(self):
        """agafa totes les dates"""
        u.printTime("medea")
        self.medees = {}
        sql = """select distinct grup_medea from sisap_covid_recerca_professions"""
        for med, in u.getAll(sql, "permanent"):
            self.medees[med] = True
            
    def get_comarques(self):
        """agafa totes les dates"""
        u.printTime("comarques")
        self.comarques = {}
        sql = """select distinct comarca_c, comarca from sisap_covid_recerca_professions"""
        for codi, desc in u.getAll(sql, "permanent"):
            self.comarques[codi] = desc
    
    def get_cataleg(self):
        """obtenim descripcions"""
        self.cataleg = {}
        sql = """select distinct cod_niv4, desc_niv4
            from sisap_covid_recerca_professions"""
        for cod, desc in u.getAll(sql, db):
            self.cataleg[(str(cod))] = desc
        
        sql = """select distinct cod_niv1, desc_niv1
            from sisap_covid_recerca_professions"""
        for cod, desc in u.getAll(sql, db):
            self.cataleg[(str(cod))] = desc       
    
    def get_mes1000(self):
        """Agafem professions de més de 1000"""
        u.printTime("Més de mil")
        self.inclusio = {}
        sql = """select  cod_niv4, desc_niv4, count(*)  
                from sisap_covid_recerca_professions 
                group by  cod_niv4, desc_niv4  """
        for cod4, desc4, rec in u.getAll(sql, db):
            if rec>1000:
                self.inclusio[cod4] = rec
 
    def get_denominadors(self):
        u.printTime("pob")
        """Agafem denominadors totals i de les professions de més de 1000 registres. També les de nivell 1 = 2 i 9 sense ser sanitaris (NIV4 211 i 212). Excloem militars"""
        self.den = c.Counter()
        
        sql = """select cod_niv1,desc_niv1, cod_niv4, desc_niv4, grup_medea, comarca_c, count(*)  
                from sisap_covid_recerca_professions 
                group by cod_niv1,desc_niv1, cod_niv4, desc_niv4, grup_medea, comarca_c """
        for cod1, desc1, cod4, desc4, medea, comarca, rec in u.getAll(sql, db):
            if cod1 != '0':
                self.den[('Mitjana', medea, comarca)] += rec
                if cod4 in self.inclusio:
                    self.den[(str(cod4), medea, comarca)] += rec
                if cod1 in ('2','3','4','7','8','9'):
                    if cod4 not in ('211','212'):
                        self.den[(str(cod1), medea, comarca)] += rec
    
    def get_casos(self):
        u.printTime("casos")
        """Agafem casos"""
        self.taxes = c.Counter()
        
        sql = """select data_cas, cod_niv1,cod_niv4, grup_medea, comarca_c, sum(if(data_cas<>0,1,0)) 
                from sisap_covid_recerca_professions 
                where cod_niv1>0 and data_cas<>0
                group by data_cas, cod_niv1,cod_niv4, grup_medea, comarca_c"""
        for data, cod1, cod4, medea, comarca, rec in u.getAll(sql, db):
            if cod1 != '0':
                self.taxes[(data, 'Mitjana', medea, comarca)] += rec
                if cod1 in ('2','3','4','7','8','9'):
                    if cod4 not in ('211','212'):
                        self.taxes[(data, str(cod1), medea, comarca)] += rec
                if str(cod4) in self.den:
                    self.taxes[(data, str(cod4), medea, comarca)] += rec                   
                    
        
    def get_dades(self):
        u.printTime("cataleg")
        """ajuntem per fer la incidència"""
        
        self.upload = []
        
        for (dat),rrr in self.periodes.items():
            d7 = dat - timedelta(days=6)
            for (grup_medea),t in self.medees.items():
                for (comarca), desc_comarca in self.comarques.items():
                    for (tip), denom in self.den.items(): 
                        casos = self.taxes[(dat, tip, grup_medea, comarca)] if (dat, tip, grup_medea, comarca) in self.taxes else 0
                        casos7 = casos
                        desc = self.cataleg[(tip, grup_medea, comarca)] if (tip, grup_medea, comarca) in self.cataleg else 'Mitjana'
                        for single_date in u.dateRange(d7, dat):
                            casosN = self.taxes[(single_date, tip, grup_medea, comarca)] if (single_date, tip, grup_medea, comarca) in self.taxes else 0
                            casos7 += casosN
                        self.upload.append([dat, tip, desc, grup_medea, comarca, desc_comarca,casos, casos7, denom])
            
    def export_data(self):
        """."""  
        u.printTime("export")
        cols = ("data date", "cod varchar(10)", "descripcio varchar(500)", "grup_medea varchar(10)", "comarca_c varchar(10)", "comarca varchar(300)", "casos_dia int", "casos7 int", "poblacio int" )
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)

                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Professions()
    
    u.printTime("Final") 