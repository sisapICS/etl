# -*- coding: utf8 -*-

import hashlib as h

"""
Actualitzem dades de professions. Per no liar, simplement faig update dels paràmteres covid. Afegeixo si tene DM2 (T90), obesitat (T82) o hiperplasia benigna de prostata (Y85)
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


tb = "sisap_covid_recerca_professions_202109"

db = 'permanent'


class Professions(object):
    """."""

    def __init__(self):
        """."""
        self.get_dbs()
        self.get_casos()
        self.get_professions()
        self.export_data()
        
    def get_dbs(self):
        """Dades del dbs"""
        u.printTime("DBS")
        self.factors = {}
        sql = """select c_cip,PS_DIABETIS2_DATA, PS_OBESITAT_DATA, PS_H_B_PROSTATA_DATA
                from DWSISAP.DBS"""
        for (cip, dm2, obesitat, hbp) in u.getAll(sql, 'exadata'): 
            self.factors[(cip, "dm2")] = dm2
            self.factors[(cip, "obesitat")] = obesitat
            self.factors[(cip, "hbp")] = hbp
            
    def get_casos(self):
        u.printTime("casos")
        """Agafem mètriques"""
        self.casos = {}
        
        sql = """select hash, 
                cas_data_pdia, ingres_primer, ingres_ultim, ingres_uci_primer, ingres_uci_ultim,
                vac_dosi_1, vac_dosi_2, exitus_covid, to_char(cas_data_pdia, 'YYYYMM')
                from DWSISAP.DBC_METRIQUES"""
        for hash, cas, ingres1, ingres2,  uci1, uci2, vac1, vac2, exitus, periode in u.getAll(sql, 'exadata'):
            self.casos[hash] = { 'cas': cas, 'ingres1':ingres1, 'ingres2':ingres2,'uci1':uci1, 'uci2':uci2,'vac1':vac1,'vac2':vac2,'exitus':exitus, 'periode':periode}

              
    def get_professions(self):
        u.printTime("professions")
        """Obtenim la base de dades que ja teníem"""
        self.upload = []
        
        sql= """select * from sisap_covid_recerca_professions"""
        for hash, naix, dbaixa, edat,sexe, prof, descp, niv1, desc1, niv4, desc4, situacio, up, medea, municipi, desc_municipi, comarca, desc_comarca, cas, periode, ingres1, ingres2, uci1, uci2, vac1, vac2, exitus in u.getAll(sql, 'permanent'):  
            cas = self.casos[hash]['cas'] if hash in self.casos else cas
            periode = self.casos[hash]['periode'] if hash in self.casos else periode
            ingres1 = self.casos[hash]['ingres1'] if hash in self.casos else ingres1
            ingres2 = self.casos[hash]['ingres2'] if hash in self.casos else ingres2
            uci1 = self.casos[hash]['uci1'] if hash in self.casos else uci1
            uci2 = self.casos[hash]['uci2'] if hash in self.casos else uci2
            vac1 = self.casos[hash]['vac1'] if hash in self.casos else vac1
            vac2 = self.casos[hash]['vac2'] if hash in self.casos else vac2
            exitus = self.casos[hash]['exitus'] if hash in self.casos else exitus
            
            dm2 = self.factors[(hash, "dm2")] if (hash, "dm2") in self.factors else None
            obesitat = self.factors[(hash, "obesitat")] if (hash, "obesitat") in self.factors else None
            hbp = self.factors[(hash, "hbp")] if (hash, "hbp") in self.factors else None
          
            self.upload.append([hash, naix, dbaixa, edat,sexe, prof, descp, niv1, desc1, niv4, desc4, situacio, up, medea, municipi, desc_municipi, comarca, desc_comarca, cas, periode, ingres1, ingres2, uci1, uci2, vac1, vac2, exitus,
            dm2, obesitat, hbp])
 
    def export_data(self):
        """."""  
        u.printTime("export")
        cols = ("hash varchar(40)", "data_naix date", "ilt_baixa date","edat int", "sexe varchar(10)", 
        "codi_prof varchar(10)", "desc_prof varchar(300)", "cod_niv1 varchar(10)", "desc_niv1 varchar(300)", "cod_niv4 varchar(10)", "desc_niv4 varchar(300)",
        "situacio varchar(10)", "up varchar(10)", "grup_medea varchar(10)","municipi_c varchar(10)", "municipi varchar(500)", "comarca_c varchar(10)", "comarca varchar(500)",
        "data_cas date", "mes_cas int", "ingres_p date", "ingres_u date", "uci_p date", "uci_u date", 
        "vac_dosi1 date", "vac_dosi2 date", "exitus_covid date",
        "DM2_DATA date", "OBESITAT_DATA date", "HBP_DATA date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        file = u.tempFolder + "professions_cov_202109.txt"
        u.writeCSV(file, self.upload, sep='@')

                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Professions()
    
    u.printTime("Final") 