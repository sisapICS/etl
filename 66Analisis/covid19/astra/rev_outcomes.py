# -*- coding: utf8 -*-


"""
Revisio Nature, actualitzem outcomes
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

class revision1(object):
    """."""

    def __init__(self):
        """."""
        self.get_cohort()
        self.get_metriques()
        self.export_files()
        
    def get_cohort(self):
        """Agafem la cohort de vacunats amb astra"""
        u.printTime("cohort estudi")
        
        self.pob = {}
        sql = """select 
                    hash 
                from 
                    sisap_covid_recerca_astra"""
        for hash, in u.getAll(sql,'permanent'):
            self.pob[hash] = True  
    
    def get_metriques(self):
        """Agafem metriques per actualitzar"""
        u.printTime("update outcomes")
        self.metriques = []
        sql = """select a.hash, cas_data_cas, pdia_primer_positiu, ingres_primer, ingres_ultim, ingres_uci_primer, Ingres_uci_ultim, exitus_covid, 
                vac_dosi_1, vac_dosi_2, vac_dosi_3, cas_reinfeccio 
                    from dwsisap.rca_cip_nia a left join dwsisap.dbc_metriques b
                    on a.hash=b.hash"""
        for hash, cas, pdia, ingres1, ingres2, uci1, uci2, exitus, vac1, vac2, vac3, reinfeccio in u.getAll(sql, 'exadata'):
            if hash in self.pob:
                self.metriques.append([hash, vac1, vac2, vac3, cas, pdia, reinfeccio, ingres1, ingres2, uci1, uci2, exitus])
    
			
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_recerca_astra_rev"
        cols = ("hash varchar(40)", "vac_1_data date", "vac_2_data date", "vac_3_data date",
                "cas_data date", "pdia_primer_positiu date", "cas_reinfeccio date",
                "ingres_primer date", "ingres_ultim date", "ingres_uci_primer date", "ingres_uci_ultim date","exitus_covid date")
        u.createTable(tb, "({})".format(", ".join(cols)), 'permanent', rm=True)
        u.listToTable(self.metriques, tb, 'permanent')
    
if __name__ == "__main__":
    try:
        revision1()
    except Exception as e:
        print e