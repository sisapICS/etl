# -*- coding: utf8 -*-

import hashlib as h

"""
Astra, pautes homolgues i heterologues
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""

        self.get_astra()
        self.export_files()

    def get_astra(self):
        """Agafem vacunats Astra"""
        self.pacients = []
        sql = """SELECT hash, data_naixement, sexe, rca_up, rca_abs, vacuna_1_data, vacuna_2_data, VACUNA_2_FABRICANT, cas_data FROM dwsisap.dbc_vacuna        
                    WHERE VACUNA_1_fabricant LIKE '%Astra%' AND ( VACUNA_2_FABRICANT LIKE '%Astra%' OR VACUNA_2_FABRICANT LIKE '%Pfizer%')"""
        for hash, naix, sexe, up, abs, vac1, vac2, fabricant, cas  in u.getAll(sql, 'exadata'):
            self.pacients.append([hash, naix, sexe, up, abs, vac2, fabricant, cas])

     
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "astra_proves"
        cols = ("hash varchar(40)", "data_naix date", "sexe varchar(10)",  "up varchar(10)",  "abs varchar(10)", "vac_2_data date", "fabricant_2 varchar(100)", "cas_data date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pacients, tb, db)
                         
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final") 