# -*- coding: utf8 -*-


"""
Buscar efectes adversos 
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

class adversos(object):
    """."""

    def __init__(self):
        """."""
        self.get_cohort()
        self.get_adversos()
        self.export_files()
        
    def get_cohort(self):
        """Agafem la cohort de vacunats amb astra"""
        u.printTime("cohort")
        
        self.pob = {}
        sql = """select 
                    hash 
                from 
                    sisap_covid_recerca_astra"""
        for hash, in u.getAll(sql,'permanent'):
            self.pob[hash] = True    
     
    def get_adversos(self):
        """Agafem efectes adversos"""
        u.printTime("adversos")
        self.upload = []       
        sql = """select 
                    hash, exposicio, exposicio_detall, exposicio_data, exposicio_multiple, 
                    es_trombosi_1, es_trombosi_2, ES_TROMBOSI_3, ES_TROMBOSI_4A, ES_TROMBOSI_4B, ES_TROMBOSI_4C, ES_TROMBOSI_5, ES_TROMBOSI_6, ES_PERIMIOCARDITIS,
                    CASE WHEN (es_trombosi_1 + es_trombosi_2 + ES_TROMBOSI_3 + ES_TROMBOSI_4A + ES_TROMBOSI_4B + ES_TROMBOSI_4C + ES_TROMBOSI_5)> 0 THEN 1 ELSE 0 END AS MTE,
                    CASE WHEN ES_TROMBOSI_6 = 1 AND (es_trombosi_1 + es_trombosi_2 + ES_TROMBOSI_3 + ES_TROMBOSI_4A + ES_TROMBOSI_4B + ES_TROMBOSI_4C + ES_TROMBOSI_5)>0 THEN 1 ELSE 0 END AS MTE_i_TCP,
                    CASE WHEN (es_trombosi_1 + es_trombosi_2 + ES_TROMBOSI_3)>0 OR ( ES_TROMBOSI_6 = 1 AND (es_trombosi_1 + es_trombosi_2 + ES_TROMBOSI_3 + ES_TROMBOSI_4A + ES_TROMBOSI_4B + ES_TROMBOSI_4C + ES_TROMBOSI_5)>0) THEN 1 ELSE 0 end  AS Unusual_o_MTE_i_TCP
                from 
                    dwsisap.COVID19_VACUNA_EA 
                WHERE 
                    exposicio IN ('vac_1','vac_2')"""
        for hash, exposicio, exposicio_detall, exposicio_data, exposicio_multiple, es_trombosi_1, es_trombosi_2, ES_TROMBOSI_3, ES_TROMBOSI_4A, ES_TROMBOSI_4B, ES_TROMBOSI_4C, ES_TROMBOSI_5, ES_TROMBOSI_6, ES_PERIMIOCARDITIS, mte, mte_tcp, unusual in u.getAll(sql, 'exadata'):
            if hash in self.pob:
                self.upload.append([hash, exposicio, exposicio_detall, exposicio_data, exposicio_multiple, es_trombosi_1, es_trombosi_2, ES_TROMBOSI_3, ES_TROMBOSI_4A, ES_TROMBOSI_4B, ES_TROMBOSI_4C, ES_TROMBOSI_5, ES_TROMBOSI_6, mte, mte_tcp, unusual, ES_PERIMIOCARDITIS])
            
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "astra_efectes_adversos_rev"
        cols = ("hash varchar(40)","exposicio varchar(10)", "exposicio_detall varchar(50)", "exposicio_data date", "exposicio_multiple int",
                "es_trombosi_1 int", "es_trombosi_2 int", "es_trombosi_3 int", "es_trombosi_4a int", "es_trombosi_4b int", "es_trombosi_4c int", "es_trombosi_5 int", "es_trombosi_6 int", 
                "MTE int", "MTE_TCP int", "unusual_MTE_TCP int","es_perimiocarditis int")
        u.createTable(tb, "({})".format(", ".join(cols)), 'permanent', rm=True)
        u.listToTable(self.upload, tb, 'permanent')
    
if __name__ == "__main__":
    try:
        adversos()
    except Exception as e:
        print e