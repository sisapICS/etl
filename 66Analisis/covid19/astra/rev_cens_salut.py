# -*- coding: utf8 -*-


"""
Revisio Nature, afegim si són cens_sanitaris
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

class revision1(object):
    """."""

    def __init__(self):
        """."""
        self.get_cohort()
        self.get_metriques()
        self.export_files()
        
    def get_cohort(self):
        """Agafem la cohort de vacunats amb astra"""
        u.printTime("cohort estudi")
        
        self.pob = {}
        sql = """select 
                    hash 
                from 
                    sisap_covid_recerca_astra"""
        for hash, in u.getAll(sql,'permanent'):
            self.pob[hash] = True  
    
    def get_metriques(self):
        """Agafem si son cens salut"""
        u.printTime("update outcomes")
        self.metriques = []
        sql = """select a.hash, es_cens_salut
                    from dwsisap.rca_cip_nia a left join dwsisap.dbc_vacuna b
                    on a.hash=b.hash"""
        for hash, cens in u.getAll(sql, 'exadata'):
            if hash in self.pob:
                self.metriques.append([hash, cens])
    
			
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "astra_salut_rev"
        cols = ("hash varchar(40)", "es_cens_salut int")
        u.createTable(tb, "({})".format(", ".join(cols)), 'permanent', rm=True)
        u.listToTable(self.metriques, tb, 'permanent')
    
if __name__ == "__main__":
    try:
        revision1()
    except Exception as e:
        print e