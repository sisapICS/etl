# -*- coding: utf8 -*-

import hashlib as h

"""
Proves (per revisió de Nature)
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""
        self.upload = []
        self.get_astra()
        self.get_proves()
        self.export_files()
    
    def get_astra(self):
        """astra heterolog"""
        u.printTime("Astra")
        self.astra = {}
        sql = """select hash
                from permanent.sisap_covid_recerca_astra"""
        for hash, in u.getAll(sql, db):
            self.astra[(hash)] = True
    
    def get_proves(self):
        """proves"""
        u.printTime("proves")
        sql = """select hash,data, prova, resultat_des, motiu, entorn,simptomes
                        from preduffa.sisap_covid_pac_prv_raw 
        where (prova like ('%PCR%') or prova like ('%Antigen%')) and to_char(data, 'YYYYMMDD')>'20201231'"""
        for id, data, prova, res, motiu, entorn, simpt in u.getAll(sql, 'redics'):
            if id in self.astra:
                self.upload.append([id, data, prova, res, motiu, entorn, simpt])
   
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_recerca_astra_proves_rev"
        cols = ("hash varchar(40)", "data_prova date", "tipus_prova varchar(100)", "resultat varchar(100)", "motiu varchar(300)", "entorn varchar(300)", "simptomes varchar(40)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db) 

if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final")   