# Anàlisi (Seguiment)

En aquest apartat fem l'analisi actualitzant les columnes: 

- vac_1_data, vac_2_data, vac_3_data, 
- cas_data 
- pdia_primer_positiu
- cas_reinfeccio
- ingres_primer, ingres_ultim
- ingres_uci_primer, ingres_uci_ultim,
- exitus_covid


## Importació

Importació de la taula (**sisap_covid_recerca_astra_rev**) preparada per l'Ermen amb el seguiment: 

```{r}

drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password="",host = "10.80.217.68", port=3307, dbname="permanent")
query <- dbSendQuery(con,
                     statement = "select * from sisap_covid_recerca_astra_rev")
dt.cohorts.seg <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)
```

### Format

```{r}
# VACUNACIÓ
  dt.cohorts.seg[vac_1_data == "0000-00-00", vac_1_data := NA]
  dt.cohorts.seg[vac_2_data == "0000-00-00", vac_2_data := NA]
  dt.cohorts.seg[vac_3_data == "0000-00-00", vac_3_data := NA]
  
# COVID  
  dt.cohorts.seg[cas_data == "0000-00-00", cas_data := NA]
  dt.cohorts.seg[pdia_primer_positiu == "0000-00-00", pdia_primer_positiu := NA]

# INGRES
  dt.cohorts.seg[ingres_primer == "0000-00-00", ingres_primer := NA]
  dt.cohorts.seg[ingres_ultim == "0000-00-00", ingres_ultim := NA]

# UCI
  dt.cohorts.seg[ingres_uci_primer == "0000-00-00", ingres_uci_primer := NA]
  dt.cohorts.seg[ingres_uci_ultim == "0000-00-00", ingres_uci_ultim := NA]

# EXITUS
  dt.cohorts.seg[exitus_covid == "0000-00-00", exitus_covid := NA]
  
dt.cohorts.seg[, c("vac_1_data", "vac_2_data", "vac_3_data", 
                   "cas_data", "pdia_primer_positiu",
                   "ingres_primer", "ingres_ultim",
                   "ingres_uci_primer", "ingres_uci_ultim",
                   "exitus_covid") := lapply(.SD, function(x){
  as.Date(x)
  }), .SDcols = c("vac_1_data", "vac_2_data", "vac_3_data", 
                  "cas_data", "pdia_primer_positiu",
                  "ingres_primer", "ingres_ultim",
                  "ingres_uci_primer", "ingres_uci_ultim",
                  "exitus_covid")]  
```

### Creació Noves Variables

#### CAS

```{r}

dt.cohorts.seg[, cas := ifelse(!is.na(cas_data), "Sí", "No")]
```

##### Temps des de vacunació

De moment no faig aquesta variable

```{r}

# dt.cohorts.seg[, temps_vac1cas := cas_data - vac_1_data]
# dt.cohorts.seg[, temps_vac2cas := cas_data - vac_2_data]
# dt.cohorts.seg[temps_vac2cas>=0, temps_cas := "2 Cas Posterior"]
# dt.cohorts.seg[temps_vac2cas<0 & temps_vac1cas>=0, temps_cas := "1 Cas Entre Vacunes"]
# dt.cohorts.seg[temps_vac2cas<0 & temps_vac1cas<0, temps_cas := "0 Cas Previ"]
# 
# dt.cohorts.seg[, cas_previ_vac2 := "No"]
# dt.cohorts.seg[temps_cas %in% c("0 Cas Previ","1 Cas Entre Vacunes"), cas_previ_vac2 := "Cas Previ 2 Dosi"]
# 
# a <- dt.cohorts.seg[, .N, .(temps_cas)]
# b <- dt.cohorts.seg[,.(pauta, edat_v, vac_1_data, vac_2_data, cas_data, temps_cas)]
# 
# df <- b[edat_v %in% c('19-59','60-69') & pauta %in% c('Homòloga','Heteròloga'),]
# res <- compareGroups(temps_cas ~ pauta
#                      , df
#                      , method = 1
#                      , max.xlev = 100
#                      , include.miss = TRUE
#                      , byrow = TRUE)
# cT <- createTable(res
#                   , show.descr = TRUE
#                   , show.all = FALSE
#                   , show.p.overall = TRUE
#                   , show.n = TRUE
#                   )
# export2md(cT)
# export2md(strataTable(cT, "edat_v"))
# 
# res <- compareGroups(pauta ~ edat_v
#                      , df
#                      , method = 1
#                      , max.xlev = 100
#                      , include.miss = TRUE
#                      , byrow = TRUE)
# cT <- createTable(res
#                   , show.descr = TRUE
#                   , show.all = FALSE
#                   , show.p.overall = TRUE
#                   , show.n = TRUE
#                   )
# export2md(cT)
```

#### Ingrés Hospitalari

```{r}

dt.cohorts.seg[, ingres_primer_cat := ifelse(!is.na(ingres_primer), "Sí", "No")]

dt.cohorts.seg[, ingres_estudi := ifelse(!is.na(ingres_primer) & ingres_primer>=vac_2_data, "Sí", "No")]
```

#### Ingrés UCI

```{r}

dt.cohorts.seg[, ingres_uci_primer_cat := ifelse(!is.na(ingres_uci_primer), "Sí", "No")]

dt.cohorts.seg[, uci_estudi := ifelse(!is.na(ingres_uci_primer) & ingres_uci_primer>=vac_2_data, "Sí", "No")]
```

#### Exitus COVID

```{r}

dt.cohorts.seg[, exitus_covid_cat := ifelse(!is.na(exitus_covid), "Sí", "No")]

dt.cohorts.seg[, exitus_estudi := ifelse(!is.na(exitus_covid) & exitus_covid>=vac_2_data, "Sí", "No")]
```

##### Temps des de vacunació

De moment no faig aquesta variable

```{r}
# #table(dt.dm$exitus_covid_cat)
# dt.dm[, temps_vac1exitus := as.numeric(exitus_covid - vac_1_data)]
# dt.dm[, temps_vac2exitus := as.numeric(exitus_covid - vac_2_data)]
# a1 <- dt.dm[,.N, temps_vac1exitus]
# a2 <- dt.dm[,.N, temps_vac2exitus]
# 
# dt.dm[temps_vac1exitus>=0, temps_vac1exitus_c := "2 Exitus Posterior Vac1"]
# dt.dm[temps_vac1exitus<0, temps_vac1exitus_c := "0 Exitus Previ Vac1"]
# table(dt.dm$temps_vac1exitus_c)
# 
# dt.dm[temps_vac2exitus>=0, temps_vac2exitus_c := "2 Exitus Posterior Vac2"]
# dt.dm[temps_vac2exitus<0 & temps_vac1exitus>=0, temps_vac2exitus_c := "1 Exitus Entre Vacunes"]
# dt.dm[temps_vac2exitus<0 & temps_vac1exitus<0, temps_vac2exitus_c := "0 Exitus Previ Vac2"]
# table(dt.dm$temps_vac2exitus_c)
# table(dt.dm$temps_vac1exitus_c, dt.dm$temps_vac2exitus_c, useNA="always")
# 
# kable(dt.dm[!is.na(exitus_covid), c("hash","vac_1_data","vac_2_data","exitus_covid", "temps_vac1exitus", "temps_vac2exitus")][order(temps_vac1exitus)])
```

### Names

Modifiquem el nom de les columnes per especifiar que són de seguiment.

```{r}

setnames(dt.cohorts.seg, names(dt.cohorts.seg), paste0(names(dt.cohorts.seg), "_seg"))
#names(dt.cohorts.seg)
```

Nou noms de les columnes: `r names(dt.cohorts.seg)`

## Fusió de la taula original amb el seguiment

** Hi ha N=15 persones que no tenen seguiment: posem la informació que ja teniem.**

```{r}
# dt.matching[match_c==1,] N=28.650

dt.matching.seg <- merge(dt.matching[match_c==1,],
                         dt.cohorts.seg,
                         by.x = "hash",
                         by.y = "hash_seg",
                         all.x = TRUE)

dt.matching.seg[is.na(cas_seg), .(cas_data, cas, cas_data_seg, cas_seg)]
dt.matching.seg[is.na(cas_seg), cas_data_seg := cas_data]
dt.matching.seg[is.na(cas_seg), cas_seg := cas]
```

## SMD

No cal, és la mateixa.

## Anàlisi supervivència (KM)

```{r}
# table(dt.matching.seg[,exitus_covid_cat_seg]) # No hi ha EXITUS
# table(dt.matching.seg[,ingres_primer_cat_seg]) # Hi ha 7 INGRESOS
# dt.matching.seg[, .(.N, sum(!is.na(ingres_primer_seg))), pauta]

dt.seg.surv <- dt.matching.seg[, c("hash",
                                   "edat_v", "pauta", "vac_2_data", "vac_3_data_seg", 
                                   "cas_data", "cas",
                                   "cas_data_seg", "cas_seg", "exitus_covid_seg", "match")]

# Data 3 vacuna
  dt.seg.surv[, .(.N, sum(!is.na(vac_3_data_seg))), pauta]
  # Heteròloga	14325	489		
  # Homòloga	14325	318

# Existe un paciente con time_seg negativo (hash=53D23067734B04F496CFDA1242A3EF014E16329B)
  # Se modifica los valores de seguimiento por los originales
  dt.seg.surv[hash=="53D23067734B04F496CFDA1242A3EF014E16329B", cas_data_seg := cas_data]
  dt.seg.surv[hash=="53D23067734B04F496CFDA1242A3EF014E16329B", cas_seg := cas]
  
# Cas o censura
  dt.seg.surv[cas_seg=="No", status_seg:=1]
  dt.seg.surv[cas_seg=="Sí", status_seg:=2]
  dt.seg.surv[vac_3_data_seg < cas_data_seg | is.na(cas_seg), status_seg:=1]
  # km_residents_no_vacunats[data_cas < vac_1dosi | (is.na(vac_1dosi) & covid == "Sí"), status := 2]
  # km_residents_no_vacunats[is.na(vac_1dosi) & is.na(data_cas), status := 1]
  # km_residents_no_vacunats[!is.na(vac_1dosi) & (covid == "No" | data_cas > vac_1dosi), status := 1]

# Seguiment
  dt.seg.surv[, time_seg := pmin(cas_data_seg, data_fi_seg, na.rm = TRUE) - vac_2_data]
  
  dt.seg.surv[, data_min := pmin(cas_data_seg, vac_3_data_seg, na.rm = TRUE)]
  dt.seg.surv[, time_seg := pmin(data_min, data_fi_seg, na.rm = TRUE) - vac_2_data]
  # Validació
    val.vac_3_data_seg <- dt.seg.surv[, .(cas_data_seg, vac_3_data_seg, data_fi_seg, vac_2_data, time_seg, status_seg)] # Todo correcto
```

### Absolute Risk Reduction (ARR)

```{r}
# Hacer pruebas en esta página: https://www2.ccrb.cuhk.edu.hk/stat/confidence%20interval/CI%20for%20ratio.htm

taula_2_arr <- dt.seg.surv[, .(N = .N,
                               casos = sum(status_seg == 2)
                        ), pauta]
taula_2_arr[, ":=" (rate = casos/N)]

taula_arr <- taula_2_arr[pauta == "Homòloga"]$rate - taula_2_arr[pauta == "Heteròloga"]$rate
# taula_sd <- ((((taula_2_arr[pauta == "Homòloga"]$rate)*(1 - taula_2_arr[pauta == "Homòloga"]$rate))/taula_2_arr[pauta == "Homòloga"]$N) +
#              (((taula_2_arr[pauta == "Heteròloga"]$rate)*(1 - taula_2_arr[pauta == "Heteròloga"]$rate))/taula_2_arr[pauta == "Heteròloga"]$N))^1/2 # DA RESULTADO ERRÓNEO

taula_sd <- sqrt((((taula_2_arr[pauta == "Homòloga"]$rate)*(1 - taula_2_arr[pauta == "Homòloga"]$rate))/taula_2_arr[pauta == "Homòloga"]$N) +
             (((taula_2_arr[pauta == "Heteròloga"]$rate)*(1 - taula_2_arr[pauta == "Heteròloga"]$rate))/taula_2_arr[pauta == "Heteròloga"]$N))

taula_arrinf <- taula_arr - (1.96*taula_sd)
taula_arrsup <- taula_arr + (1.96*taula_sd)

ARR <- paste0("Absolute Risk Reduction (ARR): ",
              format(round((taula_arr), 4), decimal.mark = ".", big.mark = ","),
              " IC95%: [",
              format(round((taula_arrinf), 4), decimal.mark = ".", big.mark = ","),
              " - ",
              format(round((taula_arrsup), 4), decimal.mark = ".", big.mark = ","),
              "]")
ARR
```

### Number Needed to Treat (NNT)

```{r}
# Hacer pruebas en esta página: https://www2.ccrb.cuhk.edu.hk/stat/confidence%20interval/CI%20for%20ratio.htm

NNT <- paste0("Number Needed to Treat (NNT): ",
              format(round((1/taula_arr), 1), decimal.mark = ".", big.mark = ","),
              " IC95%: [",
              format(round((1/taula_arrsup), 1), decimal.mark = ".", big.mark = ","),
              " - ",
              format(round((1/taula_arrinf), 1), decimal.mark = ".", big.mark = ","),
              "]")
NNT
```

### Relative Risk Reduction (RRR)

```{r}
taula <- dt.seg.surv[, .(N = .N,
                         casos = sum(status_seg == 2),
                         seguiment = sum(time_seg),
                         seguiment_mig = mean(time_seg)
                        ), pauta]
taula[, ":=" (rate_pers_dia = casos/(as.numeric(seguiment))*10000)]

taula_ir <- taula[pauta == "Heteròloga"]$rate_pers_dia/taula[pauta == "Homòloga"]$rate_pers_dia
#taula_sd <- (1/taula[pauta == "Heteròloga"]$casos + 1/taula[pauta == "Homòloga"]$casos)^1/2
taula_sd <- sqrt(1/taula[pauta == "Heteròloga"]$casos + 1/taula[pauta == "Homòloga"]$casos)

taula_irinf <- exp(log(taula_ir) - 1.96*taula_sd)
taula_irsup <- exp(log(taula_ir) + 1.96*taula_sd)

taula[pauta == "Homòloga", RRR := paste0(format(round((1 - taula_ir)*100, 4), decimal.mark = ".", big.mark = ","),
                                         " IC95%: [",
                                         format(round((1 - taula_irsup)*100, 4), decimal.mark = ".", big.mark = ","), 
                                         " - ",
                                         format(round((1 - taula_irinf)*100, 4), decimal.mark = ".", big.mark = ","),
                                         "]")]
taula[, periode := "Total follow-up"]
datatable(taula)
```

```{r}
dt.seg.surv <- dt.seg.surv[,pauta_surv_seg:=0][pauta=='Heteròloga', pauta_surv_seg:=1]

survfit <- survfit(Surv(time_seg, status_seg) ~ pauta_surv_seg, data = dt.seg.surv)
#summary(survfit)

km_results <- data.table(
  Type  =c(rep("Homòloga", survfit$strata[[1]]), rep("Heteròloga", survfit$strata[[2]])),
  time = survfit$time_seg,
  N_risk = survfit$n.risk,
  N_event = survfit$n.event,
  N_censor= survfit$n.censor,
  Prop_surv = survfit$surv,
  IC95_low = survfit$lower,
  IC95_upp = survfit$upper,
  std_error = survfit$std.err,
  cum_hazard = survfit$cumhaz,
  std_chaz = survfit$std.chaz
)
fwrite(km_results, "D:/SISAP/sisap/66Analisis/covid19/astra/results/seg.km.csv",
                   sep = ";", dec = ".", row.names = F)
```

```{r}
png("D:/SISAP/sisap/66Analisis/covid19/astra/results/seg.km.png", 
     units="mm", width=150, height = 100, res=300)
ggsurvplot(survfit,  size = 1,  # change line size
           linetype = "strata", # change line type by groups
           break.time.by = 21, # break time axis by 250
           palette = "grey",
           conf.int = TRUE, # Add confidence interval
           pval = FALSE, # Add p-value
           pval.coord = c(150, 0.995),
           ylim = c(.90, 1),
           xlim = c(0, 180),
           ggtheme = theme_classic(),
           xlab = "Days after second dose",
           legend.labs = c( "Homologous vaccination", "Heterologous vaccination"),
           title = "Figure 3. Kaplan-Meier plot of COVID-19 infection (primary outcome) \nafter second-dose vaccination according to vaccination schedule",
           font.title = 11,
           subtitle = "Kaplan-Meier estimates of covid-19 diagnosis over time from second-dose vaccination \naccording to vaccination schedule",
           font.subtitle = 9,
           #legend = "bottom",
           legend.title = "",
           legend = c(0.2, 0.2),
           censor = TRUE
           )
```

```{r}
coxph_seg <- coxph(Surv(time_seg, status_seg) ~ pauta_surv_seg, data = dt.seg.surv)
hr <-  summary(coxph_seg)$conf.int
summary(coxph_seg)
hr
test <- cox.zph(coxph_seg)
ggcoxzph(test)

coxph_seg <- coxph(Surv(time_seg, status_seg) ~ pauta_surv_seg + strata(match), data = dt.seg.surv)
summary(coxph_seg)
hr <-  summary(coxph_seg)$conf.int
hr
test <- cox.zph(coxph_seg)
ggcoxzph(test)

coxph_seg <- coxph(Surv(time_seg, status_seg) ~ pauta_surv_seg,
                   robust = TRUE,
                   cluster = match,
                   data = dt.seg.surv)
summary(coxph_seg)
hr <-  summary(coxph_seg)$conf.int
hr
test <- cox.zph(coxph_seg)
ggcoxzph(test)
```

## Backup

```{r}

saveRDS(dt.matching.seg, "D:/SISAP/sisap/66Analisis/covid19/astra/data/bk.dtmatchingseg.rds")
```

