# -*- coding: utf8 -*-

import hashlib as h


"""
Buscar events per controls negatius projecte Astra
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

codis_ps = {
    "Lumbalgia/dorsalgia":  {"C01-M54.42", "C01-M54.5", "M54","M54.9", "C01-M54.9","C01_M54.8","C01_M54.89","C01-M54"},
    }
    
agrupadors = {121: 'ulceres', 122: 'ulceres', 119: 'ulceres' , 141: 'ITU', 711: 'Fractura', 710: 'Fractura'}

class Controls_negatius(object):
    """."""

    def __init__(self):
        """."""
        self.get_codis()
        self.get_hash()
        self.get_cip()
        self.get_cohort()
        self.get_problemes()
        self.export_files()
       
        
    def get_codis(self):
        """."""
        u.printTime("codis")
        
        self.codis = {}
        self.dicc_codis = {}        
        for ps, (codisN) in codis_ps.items():
            for codi in codisN:
                self.dicc_codis[codi] = ps
                self.codis[codi] = True
        sql = """select 
                    criteri_codi, agrupador 
                from
                    nodrizas.eqa_criteris 
                where 
                    agrupador in (121,122,119,141,711,710)"""
        for crit, agr in u.getAll(sql, 'nodrizas'):
            desc = agrupadors[agr]
            self.dicc_codis[crit] = desc
            self.codis[crit] = True
        
        self.codis_select = tuple(set([codi for codi, t in self.codis.items()]))
     
    def get_hash(self):
        """id_cip_sec to hash"""
        u.printTime("hash")
        self.id_to_hash = {}
        
        sql = """select 
                    id_cip_sec, hash_d 
                from 
                    u11"""
        for id, hash in u.getAll(sql, 'import'):
            self.id_to_hash[id] = hash
    
    def get_cip(self):
        """hash to cip"""
        u.printTime("cip")
        self.hash_to_cip = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.hash_to_cip[hash] = cip
    
    def get_cohort(self):
        """Agafem la cohort de vacunats amb astra"""
        u.printTime("cohort")
        
        self.pob = {}
        sql = """select 
                    hash 
                from 
                    sisap_covid_recerca_astra"""
        for hash, in u.getAll(sql,'permanent'):
            self.pob[hash] = True    
     
    def get_problemes(self):
        """Agafem els ps de import"""
        u.printTime("problemes")
        self.upload = []       
        sql = """select 
                    id_cip_sec, pr_cod_ps, pr_dde 
                from 
                    problemes
                where 
                    year(pr_dde) ='2021' and pr_cod_o_ps='C' and pr_data_baixa=0 and pr_cod_ps in {}""".format(self.codis_select)
        for id,  ps, dde in u.getAll(sql, 'import'):
            if id in self.id_to_hash:
                hash = self.id_to_hash[id]
                if hash in self.hash_to_cip:
                    cip2 = self.hash_to_cip[hash]
                    cip = h.sha1(cip2).hexdigest().upper()
                    desc = self.dicc_codis[ps]
                    if cip in self.pob:
                        self.upload.append([cip, desc, dde])
            
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "astra_controls_negatius"
        cols = ("hash varchar(40)","ps varchar(100)", "dde date")
        u.createTable(tb, "({})".format(", ".join(cols)), 'permanent', rm=True)
        u.listToTable(self.upload, tb, 'permanent')
    
if __name__ == "__main__":
    try:
        Controls_negatius()
    except Exception as e:
        print e
