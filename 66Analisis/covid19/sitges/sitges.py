# -*- coding: utf8 -*-

"""
oci nocturn sitges
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import hashlib as h

import sisapUtils as u
from random import shuffle


db = 'permanent'

dx_file = "casos.txt"

class sitges(object):
    """."""

    def __init__(self):
        """."""
        self.get_casos()
        self.get_covid()
        self.get_vacuna()
        self.get_codipostal()
        self.get_pob()
        self.export_files()
        self.select_aleatori()
        self.export_file2()
        
    
    def get_casos(self):
        """Casos"""
        u.printTime("Casos")
        self.casos = []
        self.ups = {}

        for (cip, up, desc, cp,sexe, edat,covid,vacuna, contactes) in u.readCSV(dx_file, sep='@'):
            self.casos.append([cip, up, desc, cp,sexe, edat,covid,vacuna, contactes])
            self.ups[up] = True
            
    def get_covid(self):
        """Agafem gent amb covid previ a 13 maig"""
        u.printTime("covid previ")
        self.covid_previ = {}
        
        sql = """select hash, PDIA_PRIMER_POSITIU
                from DWSISAP.DBC_METRIQUES 
                where  to_char(PDIA_PRIMER_POSITIU, 'YYYYMMDD') <'20210513'"""
        for hash, dia in u.getAll(sql, 'exadata'):
            self.covid_previ[hash] = dia
    
    def get_vacuna(self):
        """Agafem gent amb vacuna a 13 maig"""
        u.printTime("vacuna previ")
        self.vac_previ = {}
        
        sql = """select hash, vac_dosi_1
                from DWSISAP.DBC_METRIQUES 
                where  to_char(vac_dosi_1, 'YYYYMMDD') <'20210520'"""
        for hash, dia in u.getAll(sql, 'exadata'):
            self.vac_previ[hash] = dia
			
    def get_codipostal(self):
        """Agafem gent per a fer matchq no hagi estat a meses nni tingui covid previ"""
        u.printTime("Codi postal")
        self.cpostal = {}
        sql = """SELECT ass_cip, ASS_CDP 
            FROM dwcatsalut.RCA_PACIENTS"""
        for cip, cp in u.getAll(sql, 'exadata'):
            self.cpostal[cip] = cp
            
    def get_pob(self):
        """Agafem gent per a fer matchq no hagi estat a meses nni tingui covid previ"""
        u.printTime("Match")
        self.pob = []
        sql = """select rca_cip, hash,  rca_up, rca_municipi, edat, sexe
            FROM DWSISAP.DBC_VACUNA 
            where es_rca=1"""
        for cip, hash, up, municipi, edat, sexe in u.getAll(sql, 'exadata'):
            if up in self.ups:
                cp = self.cpostal[cip] if cip in self.cpostal else None
                covid_p = 1 if hash in self.covid_previ else 0
                covid_d = self.covid_previ[hash] if hash in self.covid_previ else None
                vac_p = 1 if hash in self.vac_previ else 0
                vac_d = self.vac_previ[hash] if hash in self.vac_previ else None
			  
                self.pob.append([cip, hash, up, cp, edat, sexe, covid_p, covid_d, vac_p, vac_d])
               
                    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_sitges_pob"
        cols = ("cip varchar(20)", "hash varchar(40)", "up varchar(10)", "cp varchar(40)",  "edat int", "sexe varchar(10)",
				"covid int", "data_covid date", "vacuna int", "data_vacuna date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pob, tb, db)
        

        tb = "sisap_sitges_casos"
        cols = ("hash varchar(40)", "up varchar(10)", "desc_up varchar(400)", "cp varchar(40)","sexe varchar(10)","edat int","covid int", "vacuna int", "contactes int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.casos, tb, db)

     

    def select_aleatori(self):
        """seleccionem 5 aleatoris"""
        u.printTime("select aleatori")
        self.controls1 = c.defaultdict(set)
        self.numero = {}
        
        sql = """select a.hash, cip, b.up,a.desc_up, a.cp, b.cp, b.sexe, b.edat, b.covid, b.vacuna, a.contactes from permanent.sisap_sitges_casos a inner join permanent.sisap_sitges_pob b
                    on a.up=b.up and a.cp=b.cp and a.edat=b.edat and a.sexe=b.sexe and a.covid=b.covid and a.vacuna=b.vacuna"""
        for hash, cip, up1, up2, cp1, cp2, sexe, edat, covid, vacuna, contactes in u.getAll(sql, "nodrizas"):
            self.controls1[hash].add(cip)
            self.numero[hash] = contactes
        
        self.controls2 = c.defaultdict(set)    
        for cas in self.controls1:
            cont1 = self.controls1[cas]
            n_c = self.numero[cas]
            cont = list(cont1)
            shuffle(cont)
            for num in range(0, n_c):
                try:
                    control_selected = cont[num]
                except IndexError:
                    control_selected= 'No control'
                self.controls2[cas].add(control_selected)
        
    def export_file2(self):
        """."""
        u.printTime("export controls")
        upload = []
        for cas in self.controls2:
            for cip in self.controls2[cas]:
                upload.append([cas, cip])
                
        file = u.tempFolder + "controls_sitges_cpostal.csv"
        
        u.writeCSV(file, upload, sep='@')
        
        
if __name__ == '__main__':
    u.printTime("Inici")
     
    sitges()
    
    u.printTime("Final") 