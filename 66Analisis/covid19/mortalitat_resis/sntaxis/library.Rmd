
```{r, warning=warning.var, message=message.var}

  suppressWarnings(suppressPackageStartupMessages(library('RMySQL')))
  suppressWarnings(suppressPackageStartupMessages(library('data.table')))
  suppressWarnings(suppressPackageStartupMessages(library('lubridate')))

  suppressWarnings(suppressPackageStartupMessages(library('dplyr')))
  suppressWarnings(suppressPackageStartupMessages(library('ggplot2')))
  suppressWarnings(suppressPackageStartupMessages(library("kableExtra")))
  suppressWarnings(suppressPackageStartupMessages(library("table1")))
  suppressWarnings(suppressPackageStartupMessages(library("compareGroups")))


```

