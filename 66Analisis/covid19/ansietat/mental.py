# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)


homes21 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR21###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
dones21 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR21###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;DONA')

homes20 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR20###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
dones20 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR20###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;DONA')

homes19 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR19###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
dones19 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR19###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;DONA')

homes18 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR18###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
dones18 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR18###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;DONA')

homes17 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR17###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
dones17 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR17###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;DONA')

homes16 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR16###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
dones16 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR16###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;DONA')

homes15 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR15###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
dones15 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR15###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;DONA')

homes14 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR14###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
dones14 = exemple(c, 'P11,P77,P7701,P03,P76,P01,P74;AYR14###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;DONA')


c.close()

print('export')

file = u.tempFolder + "MENTAL_home_2021.txt"
with open(file, 'w') as f:
   f.write(homes21)

file = u.tempFolder + "MENTAL_dona_2021.txt"
with open(file, 'w') as f:
   f.write(dones21)

file = u.tempFolder + "MENTAL_home_2020.txt"
with open(file, 'w') as f:
   f.write(homes20)

file = u.tempFolder + "MENTAL_dona_2020.txt"
with open(file, 'w') as f:
   f.write(dones19)

file = u.tempFolder + "MENTAL_home_2019.txt"
with open(file, 'w') as f:
   f.write(homes19)

file = u.tempFolder + "MENTAL_dona_2019.txt"
with open(file, 'w') as f:
   f.write(dones19)
   
file = u.tempFolder + "MENTAL_home_2018.txt"
with open(file, 'w') as f:
   f.write(homes18)

file = u.tempFolder + "MENTAL_dona_2018.txt"
with open(file, 'w') as f:
   f.write(dones18)

file = u.tempFolder + "MENTAL_home_2017.txt"
with open(file, 'w') as f:
   f.write(homes17)

file = u.tempFolder + "MENTAL_dona_2017.txt"
with open(file, 'w') as f:
   f.write(dones17)
   
file = u.tempFolder + "MENTAL_home_2016.txt"
with open(file, 'w') as f:
   f.write(homes16)

file = u.tempFolder + "MENTAL_dona_2016.txt"
with open(file, 'w') as f:
   f.write(dones16)
   
file = u.tempFolder + "MENTAL_home_2015.txt"
with open(file, 'w') as f:
   f.write(homes15)

file = u.tempFolder + "MENTAL_dona_2015.txt"
with open(file, 'w') as f:
   f.write(dones15)
   
file = u.tempFolder + "MENTAL_home_2014.txt"
with open(file, 'w') as f:
   f.write(homes14)

file = u.tempFolder + "MENTAL_dona_2014.txt"
with open(file, 'w') as f:
   f.write(dones14)
