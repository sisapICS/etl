# -*- coding: utf8 -*-

"""
Ansietat, depressiio
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = 'autisme_pac'
db = 'permanent'

dx_file =  "codis.txt"

class mental(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_mental()
        self.get_assignada()
        self.export()

    def get_mental(self):
        """."""
        u.printTime("problemes")
 
        self.pacients = {}

        sql = "select id_cip_sec, pr_dde, date_format(pr_dde,'%Y%m'),  pr_cod_ps, pr_up \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_cod_ps in ('C01-F84.0','F84.0','F84.1', 'C01-F84.1' ) \
                             and pr_data_baixa = 0 and pr_dba=0"
        for id, dat, periode, codi, up in u.getAll(sql, 'import'):
            self.pacients[(id)] = True
            
    def get_assignada(self):
        """."""
        u.printTime("assignada")
        self.upload = []
        sql = 'select id_cip_sec, sexe, edat, up from assignada_tot'
        for id, sexe, naix, up in u.getAll(sql, 'nodrizas'):
            aut = 1 if id in self.pacients else 0
            self.upload.append([id, sexe, naix, up, aut])
            
    
                
    def export(self):
        """."""
        u.printTime("export")
        cols = ("id_cip_sec int", "sexe varchar(10)", "edat int", "up varchar(5)", "autisme int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
                
        
if __name__ == '__main__':
    u.printTime("Inici")
     
    mental()
    
    u.printTime("Final")                 
