---
title: "R Notebook"
output: html_notebook
---

## TS

### Total

```{r}
dades <- grip_temporades_semblants[`Grup Edat` == "Total" & data < as.Date("2020-02-04"), c("data", "Temporada", "day_after_pic", "acumulat_7")]
dades <- dades[order(data)]
grip_ts <- ts(dades[, acumulat_7], frequency = 365.25)
plot(grip_ts)
```

```{r}
library(forecast)
grip_ts_sense_outliers <- tsclean(grip_ts)
plot(grip_ts_sense_outliers)
```

```{r}
fitARIMA <- stlm(grip_ts_sense_outliers, method = "arima")
                 # , xreg = as.numeric(dades$day_after_pic))
```

```{r}
require(forecast)
velocitat_pred <- forecast(fitARIMA, 
                        # newxreg = seq(0:44)
                        h = 46)
plot(velocitat_pred)
```

<!-- ```{r} -->
<!-- acf(fitARIMA$residuals) -->
<!-- library(FitAR) -->
<!-- # boxresult-LjungBoxTest (fitARIMA$residuals,k=2,StartLag=1) -->
<!-- # plot(boxresult[,3],main= "Ljung-Box Q Test", ylab= "P-values", xlab= "Lag") -->
<!-- qqnorm(fitARIMA$residuals) -->
<!-- qqline(fitARIMA$residuals) -->
<!-- ``` -->


```{r}
dt_predict <- as.data.table(velocitat_pred)

dt_predict[, data := seq(as.Date("2019-02-01"), as.Date("2019-03-18"), 1)]
dt_predict <- merge(dt_predict, grip_diaria[`Grup Edat` == "Total", c("data", "acumulat_7")], by = "data", all.x = T)
```

```{r}
dt_predict[, paste0(c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95"), "_velocitat") := lapply(.SD, function(x) {
  x/shift(x)*100 - 100
  }), .SDcols = c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95")]

dt_predict <- dt_predict[!is.na(`Point Forecast_velocitat`)]
pic_max_total <- unique(grip_diaria[Temporada == "2018-2019" & `Grup Edat` == "Total", pic])
dt_predict[, paste0(c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95"), "_esp") := lapply(.SD, function(x) {
  pic_max_total*cumprod(1+(x/100))
  }), .SDcols = paste0(c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95"), "_velocitat")]


```

```{r}
dg <- melt(dt_predict, id.vars = c("data"), measure.vars = c("acumulat_7", "Point Forecast_esp"))
g_total <- ggplot(dg) +
  geom_line(aes(x = data, y = value, color = variable, group = variable), size = .5) +
  geom_ribbon(data = dt_predict, aes(x = data, ymin = `Lo 80_esp`, ymax = `Hi 80_esp`), fill = "grey", alpha = .3) +
  geom_ribbon(data = dt_predict, aes(x = data, ymin = `Lo 95_esp`, ymax = `Hi 95_esp`), fill = "grey", alpha = .3) + theme_classic()
```

### Per Menors de 14 anys

```{r}
dades <- grip_temporades_semblants[`Grup Edat` == "Menors de 14 anys" & data < as.Date("2020-02-04"), c("data", "Temporada", "day_after_pic", "acumulat_7")]
# & Temporada != "2016-2017"
dades <- dades[order(data)]
grip_ts <- ts(dades[, acumulat_7], frequency = 365.25)
plot(grip_ts)
```

```{r}
library(forecast)
grip_ts_sense_outliers <- tsclean(grip_ts)
plot(grip_ts_sense_outliers)
```

```{r}
fitARIMA <- stlm(grip_ts, method = "arima")
```

```{r}
require(forecast)
velocitat_pred <- forecast(fitARIMA, h = 46)
                           # ,newxreg = seq(0:42),
                        
plot(velocitat_pred)
```

```{r}
dt_predict_nens <- as.data.table(velocitat_pred)



dt_predict_nens[, data := seq(as.Date("2019-02-01"), as.Date("2019-03-18"), 1)]
dt_predict_nens <- merge(dt_predict_nens, grip_diaria[`Grup Edat` == "Menors de 14 anys", c("data", "acumulat_7")], by = "data", all.x = T)
```

```{r}
dt_predict_nens[, paste0(c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95"), "_velocitat") := lapply(.SD, function(x) {
  x/shift(x)*100 - 100
  }), .SDcols = c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95")]

dt_predict_nens <- dt_predict_nens[!is.na(`Point Forecast_velocitat`)]
pic_max_total <- unique(grip_diaria[Temporada == "2018-2019" & `Grup Edat` == "Menors de 14 anys", pic])
dt_predict_nens[, paste0(c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95"), "_esp") := lapply(.SD, function(x) {
  pic_max_total*cumprod(1+(x/100))
  }), .SDcols = paste0(c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95"), "_velocitat")]


```

```{r}
dg <- melt(dt_predict_nens, id.vars = c("data"), measure.vars = c("acumulat_7", "Point Forecast_esp"))
g_nens <- ggplot(dg) +
  geom_line(aes(x = data, y = value, color = variable, group = variable), size = .5) +
  geom_ribbon(data = dt_predict_nens, aes(x = data, ymin = `Lo 80_esp`, ymax = `Hi 80_esp`), fill = "grey", alpha = .3) +
  geom_ribbon(data = dt_predict_nens, aes(x = data, ymin = `Lo 95_esp`, ymax = `Hi 95_esp`), fill = "grey", alpha = .3) + theme_classic()
  # geom_smooth(data = dt_predict, aes(x = data, y = `Lo 80_esp`), method = "loess", se = F, linetype = 2, size = .2) +
  # geom_smooth(data = dt_predict, aes(x = data, y = `Hi 80_esp`), method = "loess", se = F, linetype = 2, size = .2) +
  # geom_smooth(data = dt_predict, aes(x = data, y = `Lo 95_esp`), method = "loess", se = F, linetype = 3, size = .2) +
  # geom_smooth(data = dt_predict, aes(x = data, y = `Hi 95_esp`), method = "loess", se = F, linetype = 3, size = .2) + scale_color_manual(values = c("acumulat_7" = "red", "Point Forecast_esp" = "blue"))
  
  # geom_smooth(aes(x = data, y = value, color = variable, group = variable), method = "loess", se = F) +

```

### Per Entre 15 i 64 anys

```{r}
dades <- grip_temporades_semblants[`Grup Edat` == "Entre 15 i 64 anys" & data < as.Date("2020-02-04"), c("data", "Temporada", "day_after_pic", "acumulat_7")]
dades <- dades[order(data)]
grip_ts <- ts(dades[, acumulat_7], frequency = 365.25)
plot(grip_ts)
```

```{r}
library(forecast)
grip_ts_sense_outliers <- tsclean(grip_ts)
plot(grip_ts_sense_outliers)
```

```{r}
fitARIMA <- stlm(grip_ts_sense_outliers, method = "arima")
                 # , xreg = as.numeric(dades$day_after_pic))
```

```{r}
require(forecast)
velocitat_pred <- forecast(fitARIMA, 
                        # newxreg = seq(0:44)
                        h = 46)
plot(velocitat_pred)
```

```{r}
dt_predict_joves <- as.data.table(velocitat_pred)

dt_predict_joves[, data := seq(as.Date("2019-02-01"), as.Date("2019-03-18"), 1)]
dt_predict_joves <- merge(dt_predict_joves, grip_diaria[`Grup Edat` == "Entre 15 i 64 anys", c("data", "acumulat_7")], by = "data", all.x = T)
```

```{r}
dt_predict_joves[, paste0(c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95"), "_velocitat") := lapply(.SD, function(x) {
  x/shift(x)*100 - 100
  }), .SDcols = c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95")]

dt_predict_joves <- dt_predict_joves[!is.na(`Point Forecast_velocitat`)]
pic_max_total <- unique(grip_diaria[Temporada == "2018-2019" & `Grup Edat` == "Entre 15 i 64 anys", pic])
dt_predict_joves[, paste0(c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95"), "_esp") := lapply(.SD, function(x) {
  pic_max_total*cumprod(1+(x/100))
  }), .SDcols = paste0(c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95"), "_velocitat")]


```

```{r}
dg <- melt(dt_predict_joves, id.vars = c("data"), measure.vars = c("acumulat_7", "Point Forecast_esp"))
g_joves <- ggplot(dg) +
  geom_line(aes(x = data, y = value, color = variable, group = variable), size = .5) +
  geom_ribbon(data = dt_predict_joves, aes(x = data, ymin = `Lo 80_esp`, ymax = `Hi 80_esp`), fill = "grey", alpha = .3) +
  geom_ribbon(data = dt_predict_joves, aes(x = data, ymin = `Lo 95_esp`, ymax = `Hi 95_esp`), fill = "grey", alpha = .3) + theme_classic()
  # geom_smooth(data = dt_predict, aes(x = data, y = `Lo 80_esp`), method = "loess", se = F, linetype = 2, size = .2) +
  # geom_smooth(data = dt_predict, aes(x = data, y = `Hi 80_esp`), method = "loess", se = F, linetype = 2, size = .2) +
  # geom_smooth(data = dt_predict, aes(x = data, y = `Lo 95_esp`), method = "loess", se = F, linetype = 3, size = .2) +
  # geom_smooth(data = dt_predict, aes(x = data, y = `Hi 95_esp`), method = "loess", se = F, linetype = 3, size = .2) + scale_color_manual(values = c("acumulat_7" = "red", "Point Forecast_esp" = "blue"))
  
  # geom_smooth(aes(x = data, y = value, color = variable, group = variable), method = "loess", se = F) +

```

### Per Majors de 64 anys

```{r}
dades <- grip_temporades_semblants[`Grup Edat` == "Majors de 64 anys" & data < as.Date("2020-02-04"), c("data", "Temporada", "day_after_pic", "acumulat_7")]
dades <- dades[order(data)]
grip_ts <- ts(dades[, acumulat_7], frequency = 365.25)
plot(grip_ts)
```

```{r}
library(forecast)
grip_ts_sense_outliers <- tsclean(grip_ts)
plot(grip_ts_sense_outliers)
```

```{r}
fitARIMA <- stlm(grip_ts_sense_outliers, method = "arima")
                 # , xreg = as.numeric(dades$day_after_pic))
```

```{r}
require(forecast)
velocitat_pred <- forecast(fitARIMA, 
                        # newxreg = seq(0:44)
                        h = 46)
plot(velocitat_pred)
```

```{r}
dt_predict_vells <- as.data.table(velocitat_pred)

dt_predict_vells[, data := seq(as.Date("2019-02-01"), as.Date("2019-03-18"), 1)]
dt_predict_vells <- merge(dt_predict_vells, grip_diaria[`Grup Edat` == "Majors de 64 anys", c("data", "acumulat_7")], by = "data", all.x = T)
```

```{r}
dt_predict_vells[, paste0(c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95"), "_velocitat") := lapply(.SD, function(x) {
  x/shift(x)*100 - 100
  }), .SDcols = c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95")]

dt_predict_vells <- dt_predict_vells[!is.na(`Point Forecast_velocitat`)]
pic_max_total <- unique(grip_diaria[Temporada == "2018-2019" & `Grup Edat` == "Majors de 64 anys", pic])
dt_predict_vells[, paste0(c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95"), "_esp") := lapply(.SD, function(x) {
  pic_max_total*cumprod(1+(x/100))
  }), .SDcols = paste0(c("Point Forecast", "Lo 80", "Hi 80", "Lo 95", "Hi 95"), "_velocitat")]


```

```{r}
dg <- melt(dt_predict_vells, id.vars = c("data"), measure.vars = c("acumulat_7", "Point Forecast_esp"))
g_vells <- ggplot(dg) +
  geom_line(aes(x = data, y = value, color = variable, group = variable), size = .5) +
  geom_ribbon(data = dt_predict_vells, aes(x = data, ymin = `Lo 80_esp`, ymax = `Hi 80_esp`), fill = "grey", alpha = .3) +
  geom_ribbon(data = dt_predict_vells, aes(x = data, ymin = `Lo 95_esp`, ymax = `Hi 95_esp`), fill = "grey", alpha = .3) + theme_classic()
  # geom_smooth(data = dt_predict, aes(x = data, y = `Lo 80_esp`), method = "loess", se = F, linetype = 2, size = .2) +
  # geom_smooth(data = dt_predict, aes(x = data, y = `Hi 80_esp`), method = "loess", se = F, linetype = 2, size = .2) +
  # geom_smooth(data = dt_predict, aes(x = data, y = `Lo 95_esp`), method = "loess", se = F, linetype = 3, size = .2) +
  # geom_smooth(data = dt_predict, aes(x = data, y = `Hi 95_esp`), method = "loess", se = F, linetype = 3, size = .2) + scale_color_manual(values = c("acumulat_7" = "red", "Point Forecast_esp" = "blue"))
  
  # geom_smooth(aes(x = data, y = value, color = variable, group = variable), method = "loess", se = F) +

```


# Càlcul de l'excés

```{r}
dt_predict[, paste0(c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp"), "_exces") := lapply(.SD, function(x){
  # ifelse(acumulat_7>`Hi 95_esp`, acumulat_7 - x, 0)
  ifelse(acumulat_7 - x < 0, 0, acumulat_7 - x)
}), .SDcols = c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp")]
dt_predict_nens[, paste0(c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp"), "_exces") := lapply(.SD, function(x){
   # ifelse(acumulat_7>`Hi 95_esp`, acumulat_7 - x, 0)
  ifelse(acumulat_7 - x < 0, 0, acumulat_7 - x)
}), .SDcols = c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp")]
dt_predict_joves[, paste0(c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp"), "_exces") := lapply(.SD, function(x){
   # ifelse(acumulat_7>`Hi 95_esp`, acumulat_7 - x, 0)
  ifelse(acumulat_7 - x < 0, 0, acumulat_7 - x)
}), .SDcols = c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp")]
dt_predict_vells[, paste0(c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp"), "_exces") := lapply(.SD, function(x){
   # ifelse(acumulat_7>`Hi 95_esp`, acumulat_7 - x, 0)
  ifelse(acumulat_7 - x < 0, 0, acumulat_7 - x)
}), .SDcols = c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp")]
```

```{r}
dt_predict[, paste0(c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp"), "_exces_mig") := lapply(.SD, function(x){
  x/7
}), .SDcols = paste0(c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp"), "_exces")]
dt_predict_nens[, paste0(c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp"), "_exces_mig") := lapply(.SD, function(x){
  x/7
}), .SDcols = paste0(c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp"), "_exces")]
dt_predict_joves[, paste0(c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp"), "_exces_mig") := lapply(.SD, function(x){
  x/7
}), .SDcols = paste0(c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp"), "_exces")]
dt_predict_vells[, paste0(c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp"), "_exces_mig") := lapply(.SD, function(x){
  x/7
}), .SDcols = paste0(c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp"), "_exces")]
```













# Comparació amb COVID-19

<!-- ```{r} -->
<!-- library(readxl) -->
<!-- n_registres_perdia_1_ <- read_excel("casos_sospita_clinica.xlsx", col_names = FALSE) -->
<!-- dt_covid <- as.data.table(n_registres_perdia_1_) -->
<!-- names(dt_covid) <- c("data", "num_covid") -->
<!-- dt_covid[, data := as.Date(data)] -->
<!-- dt_covid <- merge(dt_covid, seq_dates, by = "data", all = T) -->
<!-- dt_covid[, c("num_covid")][is.na(dt_covid[, c("num_covid")])] <- 0 -->
<!-- dt_covid[order(data), covid_acumulat_7 := Reduce(`+`, shift(num_covid, 0:6))] -->
<!-- ``` -->

```{r}
dt_covid <- fread("dades/sospita_coronavirus_edat_territori.txt", sep = "@", header = F)
names(dt_covid) <- c("dia", "Regio Sanitaria", "Grup Edat", "N")
dt_covid <- dt_covid[`Regio Sanitaria` != "\xc0MBIT ALI\xc9 A L'ICS"]
dt_covid[, `Grup Edat` := factor(`Grup Edat`, levels = c("Menors de 15 anys", "Entre 15 i 64 anys", "Majors de 64 anys"), labels = c("Menors de 14 anys", "Entre 15 i 64 anys", "Majors de 64 anys"))]
dt_covid[, data := as.Date(dia)]
dt_covid <- dt_covid[, sum(N), c("data", "Grup Edat")]
dt_covid_total <- dt_covid[, sum(V1), "data"]
dt_covid_total[, `Grup Edat` := "Total"]
dt_covid_total <- dt_covid_total[, c("data", "Grup Edat", "V1")]
dt_covid <- rbind(dt_covid, dt_covid_total)
dt_covid_grupEdat <- split(dt_covid, f = dt_covid$`Grup Edat`)
```

```{r}
seq_dates <- data.table(data = seq(as.Date("2020-01-01"), today() - 1, by="days"))
dt_covid_grupEdat_totalDates <- lapply(dt_covid_grupEdat, function(l){
  t <- merge(l, seq_dates, by = "data", all = T)
  t[, c("V1")][is.na(t[, c("V1")])] <- 0
  g_edat <- unique(t[!is.na(`Grup Edat`), `Grup Edat`])
  t[, c("Grup Edat")][is.na(t[, c("Grup Edat")])] <- g_edat
  t[order(data), covid_acumulat_7 := Reduce(`+`, shift(V1, 0:6))]
  t
})
covid_diaria <- do.call("rbind", dt_covid_grupEdat_totalDates)
```

```{r}
dt_predict <- dt_predict[, .SD, .SDcols = c("data", "acumulat_7", "Point Forecast_esp", "Lo 95_esp", "Hi 95_esp", "Point Forecast_esp_exces", "Lo 95_esp_exces", "Hi 95_esp_exces", "Point Forecast_esp_exces_mig", "Lo 95_esp_exces_mig", "Hi 95_esp_exces_mig")]
dt_predict[, `Grup Edat` := "Total"]

dt_predict_nens <- dt_predict_nens[, .SD, .SDcols = c("data", "acumulat_7", "Point Forecast_esp", "Lo 95_esp", "Hi 95_esp", "Point Forecast_esp_exces", "Lo 95_esp_exces", "Hi 95_esp_exces", "Point Forecast_esp_exces_mig", "Lo 95_esp_exces_mig", "Hi 95_esp_exces_mig")]
dt_predict_nens[, `Grup Edat` := "Menors de 14 anys"]

dt_predict_joves <- dt_predict_joves[, .SD, .SDcols = c("data", "acumulat_7", "Point Forecast_esp", "Lo 95_esp", "Hi 95_esp", "Point Forecast_esp_exces", "Lo 95_esp_exces", "Hi 95_esp_exces", "Point Forecast_esp_exces_mig", "Lo 95_esp_exces_mig", "Hi 95_esp_exces_mig")]
dt_predict_joves[, `Grup Edat` := "Entre 15 i 64 anys"]

dt_predict_vells <- dt_predict_vells[, .SD, .SDcols = c("data", "acumulat_7", "Point Forecast_esp", "Lo 95_esp", "Hi 95_esp", "Point Forecast_esp_exces", "Lo 95_esp_exces", "Hi 95_esp_exces", "Point Forecast_esp_exces_mig", "Lo 95_esp_exces_mig", "Hi 95_esp_exces_mig")]
dt_predict_vells[, `Grup Edat` := "Majors de 64 anys"]

dt_predict_all <- do.call("rbind", list(dt_predict, dt_predict_nens, dt_predict_joves, dt_predict_vells))
```

```{r}
grip_taxa <- grip[, c("data", "Grup Edat", "Total", "Tasa")]
grip_taxa[, Poblacio := Total/Tasa*100000]
grip_taxa <- unique(grip_taxa[data %between% c(as.Date("2020-02-04"), as.Date("2020-03-20")), c("data", "Grup Edat", "Poblacio")])
dt_predict_all_pobl <- merge(dt_predict_all, grip_taxa, by = c("data", "Grup Edat"), all.x = T)
```


```{r}
dt_predict_obs_covid <- merge(dt_predict_all_pobl, covid_diaria, by = c("data", "Grup Edat"), all.x = T)
dt_predict_obs_covid[, `Grup Edat` := factor(`Grup Edat`, levels = c("Menors de 14 anys", "Entre 15 i 64 anys", "Majors de 64 anys", "Total"), labels = c("Age younger than 15", "Age between 15 and 64", "Age older than 64", "Total"))]

dt_predict_obs_covid[, paste0(c("Point Forecast_esp_exces", "Lo 95_esp_exces", "Hi 95_esp_exces"), "_mes_covid") := lapply(.SD, function(x){
  x + covid_acumulat_7
}), .SDcols = c("Point Forecast_esp_exces", "Lo 95_esp_exces", "Hi 95_esp_exces")]
```


```{r fig.height=3}
dg <- dt_predict_obs_covid
dg[, `Lo 95_esp` := ifelse(`Lo 95_esp`< 0, 0, `Lo 95_esp`)]
dg[, `Point Forecast_esp` := ifelse(`Point Forecast_esp`< 0, 0, `Point Forecast_esp`)]
dg[, paste0(c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp", "acumulat_7"), "_Taxa") := lapply(.SD, function(x){x/Poblacio*100000}), .SDcols  = c("Point Forecast_esp", "Lo 95_esp", "Hi 95_esp", "acumulat_7")]
dg <- melt(dt_predict_obs_covid, id.vars = c("data", "Grup Edat", "Lo 95_esp", "Hi 95_esp"), measure.vars = c("Point Forecast_esp", "acumulat_7"))
dg[, variable := factor(variable, levels = c("Point Forecast_esp", "acumulat_7"), labels = c("Expected", "Observed"))]


tiff("test_ts.jpg", units="in", width=8, height=6, res=600)
ggplot(dg, aes(data)) +
  geom_line(aes(group = variable, y = value, linetype = variable)) +
  geom_ribbon(data = dt_predict_obs_covid, aes(x = data, ymin = `Lo 95_esp`, ymax = `Hi 95_esp`), fill = "grey", alpha = .5) +
  theme_classic() + labs(title = "", x = "Days", y = "New influenza cases in the previous 7-day period", linetype = "") +
  scale_x_date(breaks = "4 days", date_labels = "%b %d") +
  scale_linetype_manual(values = c("Expected" = 2, "Observed" = 1)) +
  theme(axis.text.x = element_text(angle = 90, vjust = .5),
        legend.position = "bottom") +
  # geom_vline(xintercept = as.Date(c("2020-02-13", "2020-02-25", "2020-03-16")), color = "darkgrey", linetype = 2) + 
  facet_wrap(~`Grup Edat`, scales = "free")
# ggsave(file="figure3.svg", plot=gg, width = 8, height=6)
dev.off()
```

