# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

neos_homes = exemple(c, 'MALIGNA###;AYR21###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
neos_dones = exemple(c, 'MALIGNA###;AYR21###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;DONA')

c.close()

print('export')

file = u.tempFolder + "neos_home_2021.txt"
with open(file, 'w') as f:
   f.write(neos_homes)

file = u.tempFolder + "neos_dona_2021.txt"
with open(file, 'w') as f:
   f.write(neos_dones)
