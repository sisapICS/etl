library("data.table")
library("lubridate")
library("gridExtra")
library("xlsx")

# MEDEA
medea <- fread("C:/Users/nmora/repositori/etl/66Analisis/covid19/neos/dades originals/medea_eap.txt", sep = "@", header = F)
medea[V3 %in% c("0R", "1R", "2R"), V3 := "Rural"]

# Àmbit
sheets <- c("AYR21", "AYR20", "AYR19", "AYR18", "AYR17", "AYR16")
ambit <- do.call("rbind", lapply(sheets, function(sheet){
  file <- data.table(read.xlsx("C:/Users/nmora/repositori/etl/66Analisis/covid19/neos/dades originals/pob amb ambit.xlsx", sheet))
  file <- file[-1]
  file[, Any := paste0("20", substr(sheet, 4, 5))]
  file[, c(1, 4, 5)]
}))
names(ambit) <- c("br", "ambit", "Any")
ambit <- ambit[substr(br, 1, 2) == "BR"]
ambit[, br := as.character(br)]

# Pobass
pobass_arixus <- c("pobass 2016", "pobass 2017", "pobass 2018", "pobass 2019", "pobass 2020", "pobass 2021")
library("openxlsx")
pobass <- do.call("rbind", lapply(pobass_arixus, function(arxiu){
  # Any
  # print(arxiu)
  require(stringr)
  any <- paste0("A", substr(arxiu, 10, 11))
  # Mes
  do.call("rbind", lapply(c("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"), function(mes){
    # print(mes)
    
    # Sexe
    do.call("rbind", lapply(c("Home", "Dona"), function(sexe){
      # print(sexe)
      sheet <- paste0(any, mes, " ", sexe)
      pobass <- tryCatch(data.table(read.xlsx(paste0("C:/Users/nmora/repositori/etl/66Analisis/covid19/neos/dades originals/",arxiu, ".xlsx"), sheet)), error=function(e) NULL)
      
      pobass <- tryCatch(pobass[-1], error=function(e) NULL)
      
      if(!is.null(pobass)){
        pobass[, Any := any]
        pobass[, Mes := mes]
        pobass[, Sexe := sexe]
        pobass
      }
      
    }))
  }))
}))
pobass_melt <- melt(pobass, id.vars = c("X1", "X2", "Any", "Mes", "Sexe"), variable.name = "Edat", value.name = "POBASS")
pobass_melt[, POBASS := as.numeric(as.character(POBASS))]
names(pobass_melt)[c(1, 2)] <- c("br", "br_descriptiu")
pobass_melt[, data := ymd(paste0("20", substr(Any, 2, 3), "-", Mes, "-", "01"))]
pobass_melt[, edat_65 := ifelse(Edat %in%  c(paste0("ED", seq(60, 95, 1)), "ED95M"), "Més de 59 anys",ifelse(Edat %in%  c(paste0("ED", seq(15, 29, 1))),"Entre 15 i 29","Entre 30 i 59"))]
pobass_melt[, Any := paste0("20", substr(Any, 2, 3))]
pobass_melt_ambit <- merge(pobass_melt, ambit, by = c("br", "Any"), all.y = T)
pobass_melt_ambit_medea <- merge(pobass_melt_ambit, medea[, c("V1", "V3")], by.x = "br", by.y = "V1", all.x = T)
names(pobass_melt_ambit_medea)[11] <- "medea"
pobass_melt_ambit_medea <- pobass_melt_ambit_medea[!Edat %in% c("ED0", "ED1", "ED2", "ED3", "ED4", "ED5", "ED6", "ED7", "ED8", "ED9", "ED10", "ED11", "ED12", "ED13", "ED14")]
a <- pobass_melt_ambit_medea[, .N, by=.(Edat, edat_65)]
b <- pobass_melt_ambit_medea[, sum(POBASS), by=.(Any, Mes, Edat, edat_65)]

#ITS
library("RMySQL")

drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password="",host = "10.80.217.68", port=3307, dbname="permanent")
nfetch <- -1
query <- dbSendQuery(con,
                     statement = "select * from permanent.sisap_recerca_its where tipus <>'Contacte' and  left(periode,4) >'2015'")
its <- data.table(fetch(query, n = nfetch))
query <- dbSendQuery(con,
                     statement = "select up_codi_up_ics, up_codi_up_scs, up_data_baixa from import.cat_gcctb008")
cataleg_up_antigues <- data.table(fetch(query, n = nfetch))
dbDisconnect(con)

cataleg_up_antigues_br <- cataleg_up_antigues[up_codi_up_ics %like% "BR"]
cataleg_up_antigues_br_sense_duplicats <- cataleg_up_antigues_br[up_codi_up_scs %in% cataleg_up_antigues_br[, .N, up_codi_up_scs][N == 1]$up_codi_up_scs | (
  up_codi_up_scs %in% cataleg_up_antigues_br[, .N, up_codi_up_scs][N > 1]$up_codi_up_scs & up_data_baixa == "0000-00-00"
)]

its_br <- merge(its, cataleg_up_antigues_br_sense_duplicats[, c("up_codi_up_ics", "up_codi_up_scs")], by.x = "up", by.y = "up_codi_up_scs", all.x = T)

its_adults <- its_br[!edat %in% c("EC01", "EC24", "EC59", "EC1014")]

its_adults[, edat_65 := ifelse(edat %in% c("EC6064","EC6569", "EC7074", "EC7579", "EC8084", "EC8589", "EC9094", "EC95M"), "Més de 59 anys", ifelse(edat %in% c("EC1519", "EC2024", "EC2529"),"Entre 15 i 29", "Entre 30 i 59"))]
its_adults[, CODI := tipus]
its_adults[, Any := substr(periode, 1, 4)]
its_adults[, data := ymd(paste0(Any, "-", substr(periode, 5, 6), "-", "01"))]
names(its_adults) <- c("up", "Periode", "Edat", "sexe", "tipus", "value", "br", "edat_65", "CODI", "Any", "data")

its_ambit <- merge(its_adults, ambit, by = c("br", "Any"), all.y = T)

aa <- its_ambit[, .N, by=.(Edat, edat_65)]

## Global
its_global <- its_ambit[, sum(value, na.rm = T), c("data", "Any")]
pobass_global <- pobass_melt_ambit_medea[, sum(POBASS, na.rm = T), c("data", "Any")]

dades_global <- merge(its_global, pobass_global, by = c("data", "Any"))
names(dades_global)[c(3, 4)] <- c("ITS", "POBASS")
dades_global[, taxa := ITS/POBASS*100000]
dades_global[, edat_65 := "Global"]

## Edat
its_edat <- its_ambit[, sum(value, na.rm = T), c("data", "Any", "edat_65")]
pobass_edat <- pobass_melt_ambit_medea[, sum(POBASS, na.rm = T), c("data", "Any", "edat_65")]

dades_edat <- merge(its_edat, pobass_edat, by = c("data", "Any", "edat_65"))
names(dades_edat)[c(4, 5)] <- c("ITS", "POBASS")
dades_edat[, taxa := ITS/POBASS*100000]

## Sexe
pobass_melt_ambit_medea[, sexe := factor(Sexe, levels = c("Home", "Dona"), labels = c("HOME", "DONA"))]
its_sexe <- its_ambit[, sum(value, na.rm = T), c("data", "Any", "sexe")]
pobass_sexe <- pobass_melt_ambit_medea[, sum(POBASS, na.rm = T), c("data", "Any", "sexe")]

dades_sexe <- merge(its_sexe, pobass_sexe, by = c("data", "Any", "sexe"))
names(dades_sexe)[c(4, 5)] <- c("ITS", "POBASS")
dades_sexe[, taxa := ITS/POBASS*100000]

## MEDEA
its <- merge(its_ambit, medea[, c(1, 3)], by.x = "br", by.y = "V1", all.x = T)
names(its)[13] <- "medea"
its_medea <- its[, sum(value, na.rm = T), c("data", "Any", "medea")]
pobass_medea <- pobass_melt_ambit_medea[, sum(POBASS, na.rm = T), c("data", "Any", "medea")]

dades_medea <- merge(its_medea, pobass_medea, by = c("data", "Any", "medea"))
names(dades_medea)[c(4, 5)] <- c("ITS", "POBASS")
dades_medea[, taxa := ITS/POBASS*100000]

# PER TIPUS DE NEO

## Global

its_ambit[!CODI %in% c("CONDILOMES", "SIFILIS", "GONOCOC", "CLAMIDIA", "VIH/SIDA", "HERPES", "ITS INESP", "URETRITIS INESP"), CODI := "Altres"]

its_global_tipus <- its_ambit[, sum(value, na.rm = T), c("data", "Any", "CODI")]
pobass_global <- pobass_melt_ambit_medea[, sum(POBASS, na.rm = T), c("data", "Any")]

dades_global_tipus <- merge(its_global_tipus, pobass_global, by = c("data", "Any"))
names(dades_global_tipus)[c(4, 5)] <- c("ITS", "POBASS")
dades_global_tipus[, taxa := ITS/POBASS*100000]

# Preparar dades per pujar amb article

## neos_data
dades_global <- dades_global[, c("data", "Any", "edat_65", "ITS", "POBASS", "taxa")]
names(dades_global) <- c("date", "Year", "Variable", "Value", "Population", "Rate")
dades_edat <- dades_edat[, c("data", "Any", "edat_65", "ITS", "POBASS", "taxa")]
names(dades_edat) <- c("date", "Year", "Variable", "Value", "Population", "Rate")
dades_sexe <- dades_sexe[, c("data", "Any", "sexe", "ITS", "POBASS", "taxa")]
names(dades_sexe) <- c("date", "Year", "Variable", "Value", "Population", "Rate")
dades_medea <- dades_medea[, c("data", "Any", "medea", "ITS", "POBASS", "taxa")]
names(dades_medea) <- c("date", "Year", "Variable", "Value", "Population", "Rate")

its_data <- rbind(
  dades_global,
  dades_edat,
  dades_sexe,
  dades_medea
)

its_data[, Variable := factor(Variable, 
                               levels = c("Global", "Entre 15 i 29", "Entre 30 i 59","Més de 59 anys", "DONA", "HOME", "1U", "2U", "3U", "4U", "Rural"), 
                               labels = c("Total", "Between 15 and 29", "Between 30 and 59","Older than 59", "Women", "Men","1st Q (least deprived)", "2nd Q", "3rd Q", "4th Q (most deprived)", "Rural"))]
# save(neos_data, file = "D:/projectes/covid19/diagnostics/article/data/neos_data.RData")

## neos_data_selected
dades_global_tipus <- dades_global_tipus[, c("data", "Any", "CODI", "ITS", "POBASS", "taxa")]
names(dades_global_tipus) <- c("date", "Year", "Variable", "Value", "Population", "Rate")
its_data_selected <- dades_global_tipus
its_data_selected[, Variable := factor(Variable, 
                                        levels = c( "CONDILOMES", "SIFILIS", "GONOCOC", "CLAMIDIA", "VIH/SIDA", "HERPES", "ITS INESP", "URETRITIS INESP", "Altres"), 
                                        labels = c("Genital warts", "Syphilis", "Gonorrhea", "Chlamydia", "HIV",  "Genital herpes", "Non-specific STI", "Non-specific urethritis", "Others"))]
# save(neos_data_selected, file = "C:/Users/nmora/repositori/etl/66Analisis/covid19/neos/dades originals/neos_data_selected.RData")