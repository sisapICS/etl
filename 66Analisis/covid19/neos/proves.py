# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

mamografiesh20 = exemple(c, 'ORCLINIC;AYR21###;AMBITOS###;CRA00008,CRA00009,CRA00010,CRA00012,CRA00472,CRA01085;EDATS5###;DERORIGEN###;HOME')
mamografiesd20 = exemple(c, 'ORCLINIC;AYR21###;AMBITOS###;CRA00008,CRA00009,CRA00010,CRA00012,CRA00472,CRA01085;EDATS5###;DERORIGEN###;DONA')

rxh20 = exemple(c, 'ORCLINIC;AYR21###;AMBITOS###;CRA00001,CRA00002,CRA00007,CRA00590,CRA00591;EDATS5###;DERORIGEN###;HOME')
rxd20 = exemple(c, 'ORCLINIC;AYR21###;AMBITOS###;CRA00001;EDATS5###;DERORIGEN###;DONA')

colonoh20 = exemple(c, 'ORCLINIC;AYR21###;AMBITOS###;CPD00069;EDATS5###;DERORIGEN###;HOME')
colonod20 = exemple(c, 'ORCLINIC;AYR21###;AMBITOS###;CPD00069;EDATS5###;DERORIGEN###;DONA')

c.close()

print('export')

file = u.tempFolder + "Mamografies_home_2021.txt"
with open(file, 'w') as f:
   f.write(mamografiesh20)

file = u.tempFolder + "Mamografies_dona_2021.txt"
with open(file, 'w') as f:
   f.write(mamografiesd20)
   
file = u.tempFolder + "Rx_home_2021.txt"
with open(file, 'w') as f:
   f.write(rxh20)

file = u.tempFolder + "Rx_dona_2021.txt"
with open(file, 'w') as f:
   f.write(rxd20)
   
file = u.tempFolder + "colonos_home_2021.txt"
with open(file, 'w') as f:
   f.write(colonoh20)

file = u.tempFolder + "colonos_dona_2021.txt"
with open(file, 'w') as f:
   f.write(colonod20)

