# -*- coding: utf8 -*-

import hashlib as h
import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'redics'


tb = 'sisap_covid_rec_resis_m'
"""

upload = []
sql = "select * from permanent.sisap_covid_rec_resis_m"
for row in u.getAll(sql, 'permanent'):
    upload.append(row)
    
cols = ("hash varchar2(40)", "edat int", "naixement date",
            "sexe varchar2(1)", "nacionalitat varchar2(100)", "localitat varchar2(7)", "abs int",
            "up varchar2(5)", "medea number(6, 2)", "resident int",
             "estat varchar(16)", "origen_d varchar2(40)","cas_data date",
            "dx_cod varchar2(16)", "dx_dde date", "dx_dba date",
            "dx_sit varchar2(16)",  "pcr_data date", "pcr_res varchar2(30)", "pcr_n int","pcr_ia_n int", "test_n int", "test_ia_n int",
            "exitus date",
            "gma_codi varchar2(3)", "gma_complexitat number(6, 2)", "pcc int", "maca int",
            "in_dbs int",
            "FR_hta date",  "FR_dm date", "FR_mpoc date",  "FR_ci date", "FR_mcv date",
            "FR_ic date", "FR_acfa date", "FR_valv date", "FR_hepat date", "FR_vhb date", "FR_vhc date", "FR_neo date", "FR_mrc date", "FR_obes date",
            "FR_vih date", "FR_sida date", "FR_demencia date", "FR_artrosi date", 
            "V_barthel_data date",  "V_barthel_valor int","V_pfeifer_data date",  "V_pfeifer_valor int")
u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
u.listToTable(upload, tb, db)
"""       
users= ["PREDUPRP",  "PREDUECR", "PREDULMB", "PREDUXMG"]
for user in users:
    u.execute("grant select on {} to {}".format(tb,user),db)
    
