# -*- coding: utf8 -*-

"""
meses
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import hashlib as h

import sisapUtils as u


db = 'permanent'

class meses(object):
    """."""

    def __init__(self):
        """."""
        self.get_comarca()
        self.get_meses()
        self.get_covid()
        self.get_meses_final()
        self.get_caract_covid()
        self.get_pob()
        self.export_files()
        
    def get_comarca(self):
        """comarques"""
        self.comarques = {}
        sql = """select municipi_c, comarca_c, comarca 
            from dwsisap.SISAP_CORONAVIRUS_CAT_LOC"""
        for municipi, comarca_c, desc in u.getAll(sql, 'exadata'):
            self.comarques[municipi] = {'codi': comarca_c, 'desc': desc}
    
    def get_meses(self):
        """Meses"""
        u.printTime("Meses")
        self.meses = {}

        sql = """SELECT ass_cip FROM 
                dwcatsalut.CNS_ELECCIONS_UP"""
        for cip, in u.getAll(sql, 'exadata'):
            self.meses[cip] = True
            
    def get_covid(self):
        """Agafem gent amb covid previ per excloure"""
        u.printTime("covid previ")
        self.covid_previ = {}
        
        sql = """select hash, PDIA_PRIMER_POSITIU
                from DWSISAP.DBC_METRIQUES 
                where  to_char(PDIA_PRIMER_POSITIU, 'YYYYMMDD') <'20210214'"""
        for hash, dia in u.getAll(sql, 'exadata'):
            self.covid_previ[hash] = dia
    
    def get_meses_final(self):
        """final meses"""
        self.final_meses = {}
        sql = """select hash, municipi_id, colegi_id, districte, seccio, mesa, tipus_representacio from 
                dwcatsalut.cns_eleccions_final a inner join DWSISAP.RCA_CIP_NIA on nif=dni"""
        for hash, muni, col, dist,secc, mesa, tip in u.getAll(sql, 'exadata'):
            self.final_meses[hash] = { 'muni': muni, 'col': col, 'dist': dist, 'sec': secc, 'mesa': mesa, 'tip': tip}

            
    def get_caract_covid(self):
        """Agafem gent amb covid previ per excloure"""
        u.printTime("mètriques")
        self.dades_p = {}
        
        sql = """select hash, pdia_primer_positiu, ingres_primer, ingres_uci_primer, vac_dosi_1, vac_dosi_2, exitus_covid
                from DWSISAP.DBC_METRIQUES 
                where PDIA_PRIMER_POSITIU is null or to_char(PDIA_PRIMER_POSITIU, 'YYYYMMDD') >'20210213'"""
        for hash, covid, ingres, uci, vac1, vac2, exitus in u.getAll(sql, 'exadata'):
            self.dades_p[hash] = {'covid': covid, 'ingres': ingres, 'uci': uci, 'vac1': vac1, 'vac2':vac2, 'exitus':exitus}
    
    
    def get_pob(self):
        """Agafem gent per a fer matchq no hagi estat a meses nni tingui covid previ"""
        u.printTime("Match")
        self.pob_2 = []
        self.pob = []
        self.pob_3 = []
        sql = """select rca_cip, hash,  rca_up, rca_municipi, edat, sexe, es_risc_baix, es_risc_moderat, es_risc_alt, edat 
            FROM DWSISAP.DBC_VACUNA 
            where es_rca=1"""
        for cip, hash, up, municipi, edat, sexe,  baix, moderat, alt, edat in u.getAll(sql, 'exadata'):
            comarca_c = self.comarques[municipi]['codi'] if municipi in self.comarques else None
            comarca_d = self.comarques[municipi]['desc'] if municipi in self.comarques else None
            covid = self.dades_p[hash]['covid'] if hash in self.dades_p else None
            ingres = self.dades_p[hash]['ingres'] if hash in self.dades_p else None
            uci = self.dades_p[hash]['uci'] if hash in self.dades_p else None
            vac1 = self.dades_p[hash]['vac1'] if hash in self.dades_p else None
            vac2 = self.dades_p[hash]['vac2'] if hash in self.dades_p else None
            exitus = self.dades_p[hash]['exitus'] if hash in self.dades_p else None
            if 18 <= edat <= 70:
                if cip not in self.meses:
                    if hash not in self.covid_previ:
                        self.pob.append([hash, up, municipi, comarca_c, comarca_d, edat, sexe, baix, moderat, alt, covid, ingres, uci, vac1, vac2, exitus])
                else:
                    covid_previ = 1 if hash in self.covid_previ else 0
                    data_previ = self.covid_previ[hash] if hash in self.covid_previ else None
                    self.pob_2.append([hash, up, municipi, edat, sexe, baix, moderat, alt, covid_previ, data_previ, covid, ingres, uci, vac1, vac2, exitus])
            if hash in self.final_meses:
                muni = self.final_meses[hash]['muni'] 
                col = self.final_meses[hash]['col']
                dist = self.final_meses[hash]['dist']
                sec = self.final_meses[hash]['sec'] 
                mesa = self.final_meses[hash]['mesa'] 
                tip = self.final_meses[hash]['tip'] 
                covid_previ = 1 if hash in self.covid_previ else 0
                data_previ = self.covid_previ[hash] if hash in self.covid_previ else None
                
                self.pob_3.append([hash, up, muni, municipi, comarca_c, comarca_d, edat, sexe, baix, moderat, alt, covid_previ, data_previ, covid, ingres, uci, vac1, vac2, exitus,
                                    col, dist, sec, mesa, tip])
                
                
                
                    
                    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_meses_pob_general"
        cols = ("hash varchar(40)", "up varchar(10)", "municipi varchar(40)", "comarca_codi varchar(40)","comarca_desc varchar(40)", "edat int", "sexe varchar(10)",  
            "es_risc_baix int", "es_risc_moderat int", "es_risc_alt int",
            "data_cas date",  "ingres date", "uci date", "vac_1dosi date", "vac_2dosi date", "exitus date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pob, tb, db)
        
        tb = "sisap_covid_meses_cens"
        cols = ("hash varchar(40)", "up varchar(10)", "municipi varchar(40)", "edat int", "sexe varchar(10)",  
            "es_risc_baix int", "es_risc_moderat int", "es_risc_alt int", "covid_previ14F int", "data_previ date", 
            "data_cas date",  "ingres date", "uci date", "vac_1dosi date", "vac_2dosi date", "exitus date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pob_2, tb, db)
        
         
        tb = "sisap_covid_meses_final"
        cols = ("hash varchar(40)", "up varchar(10)", "municipi varchar(40)", "municipi_rca varchar(40)", "comarca_codi varchar(40)","comarca_desc varchar(40)","edat int", "sexe varchar(10)",  
            "es_risc_baix int", "es_risc_moderat int", "es_risc_alt int", "covid_previ14F int", "data_previ date", 
            "data_cas date",  "ingres date", "uci date", "vac_1dosi date", "vac_2dosi date", "exitus date",
            "colegi varchar(40)", "districte varchar(40)", "seccio varchar(40)", "mesa varchar(40)", "tipus_representacio varchar(300)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pob_3, tb, db)
                        
if __name__ == '__main__':
    u.printTime("Inici")
     
    meses()
    
    u.printTime("Final") 