# -*- coding: utf8 -*-

"""
meses
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import hashlib as h

import sisapUtils as u


db = 'permanent'

class meses(object):
    """."""

    def __init__(self):
        """."""

        self.get_covid()
        self.get_controls()
        self.export_files()
        
    
    def get_covid(self):
        """Agafem gent amb covid previ per excloure"""
        u.printTime("mètriques")
        self.covids = {}
        
        sql = """select hash, pdia_primer_positiu
                from DWSISAP.DBC_METRIQUES 
                where PDIA_PRIMER_POSITIU is null or to_char(PDIA_PRIMER_POSITIU, 'YYYYMMDD') >'20210213'"""
        for hash, dia in u.getAll(sql, 'exadata'):
            self.covids[hash] = dia
    
    def get_controls(self):
        """controls"""
        u.printTime("Poblacio")
        self.upload = []
        file = u.tempFolder + "elleccionsMathed.txt"
        for hash, cohort, cohort1, match in u.readCSV(file):
            data = self.covids[hash] if hash in self.covids else None
            self.upload.append([hash, cohort, cohort1, match, data])
                    
                    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_covid_meses_seguiment"
        cols = ("hash varchar(40)", "cohort varchar(40)", "cohort1 int", "matched int", "data_cas date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
      
                        
if __name__ == '__main__':
    u.printTime("Inici")
     
    meses()
    
    u.printTime("Final")