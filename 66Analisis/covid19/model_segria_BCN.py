# -*- coding: utf8 -*-

import hashlib as h

"""
Variables socioec 
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

db = 'redics'

def get_edat(edat):
    if edat <=14:
        ed = '0-14'
    elif 15 <= edat <=49:
        ed = '15-49'
    elif 50<= edat <= 64:
        ed= '50-64'
    elif edat >64:
        ed = '>64'
    else:
        ed = 'None'
    return ed

class projecteAA(object):
    """."""

    def __init__(self):
        """."""
        self.get_periode()
        #self.get_resis()
        self.get_cataleg()
        #self.get_poblacio()
        #self.get_simptomatics()
        #self.get_casos()
        #self.get_pcr()
        self.get_alumnes()
        #self.get_dbs()
        
    def get_periode(self):
        """.""" 
        sql = "select min(cas_data), max(cas_data) from sisap_covid_pac_master where  to_char(cas_data, 'YYYYMMDD') >= '20200901' and to_char(cas_data,'YYYYMMDD') <='20210228'"
        for min, max in u.getAll(sql, db):
            self.ini = min
            self.fi = max
    
    def get_resis(self):
        """Excloem pacients de resis"""
        u.printTime("inicial")
        self.resis = {}
        sql = """select hash, data, perfil, residencia 
                from dwsisap.residencies_cens 
                where  perfil='Resident'"""
        for id, data, perfil, resi in u.getAll(sql, 'exadata'):
            self.resis[id] = True
    
    def get_cataleg(self):
        """.Agafem codis de territoris"""
        u.printTime("cataleg")
        self.cataleg_mun, self.cataleg_loc = {}, {}
        sql = """SELECT municipi_c FROM 
		    SISAP_COVID_CAT_LOCALITAT 
			WHERE comarca_c=33""" 
        for loc,  in u.getAll(sql, db):
            self.cataleg_mun[loc] = 'Segria'      
            
        sql = """SELECT municipi_c, localitat_c 
		    FROM SISAP_COVID_CAT_LOCALITAT 
			WHERE municipi_C='08019'"""
        for loc, loc2 in u.getAll(sql, db):  
            self.cataleg_mun[loc] = 'Barcelona'  
            self.cataleg_loc[loc2] = True 
            
        self.cataleg_up = {}
        sql = """SELECT up FROM 
		    SISAP_COVID_CAT_TERRITORI 
			WHERE comarca_c=33""" 
        for loc,  in u.getAll(sql, db):
            self.cataleg_up[loc] = 'Segria' 
            
        sql = """SELECT up, localitat_c 
		    FROM SISAP_COVID_CAT_TERRITORI"""
        for up, loc in u.getAll(sql, db):  
            if loc in self.cataleg_loc:
                self.cataleg_up[up] = 'Barcelona'  
		
     
    def get_poblacio(self):
        """poblacio"""
        u.printTime("poblacio")
        self.pob = c.Counter()
        self.pacients_rca = {}
        sql = """SELECT hash, edat, rca_municipi FROM  DWSISAP.DBC_poblacio WHERE situacio='A'"""
        for hash, edat, municipi in u.getAll(sql, 'exadata'):
            if hash not in self.resis:
                if municipi in self.cataleg_mun:
                    zona = self.cataleg_mun[municipi]
                    grup = get_edat(int(edat))
                    self.pob[(zona, grup)] += 1
        upload = []
        for (zona, grup), n in self.pob.items():
            upload.append([zona, grup, n])
        file = u.tempFolder + "aalba/pop.csv"
        u.writeCSV(file, upload, sep='@')
                    
        sql = """SELECT hash, edat, rca_municipi FROM  DWSISAP.DBC_poblacio"""
        for hash, edat, municipi in u.getAll(sql, 'exadata'):
            if hash not in self.resis:
                if municipi in self.cataleg_mun:
                    zona = self.cataleg_mun[municipi]
                    grup = get_edat(int(edat))        
                    self.pacients_rca[hash] = {'zona': zona, 'edat': grup}
   
    def get_simptomatics(self):
        """simptomes"""
        u.printTime("simptomes")
        self.simptomes = c.Counter()
        sql = """SELECT DATA, hash FROM preduffa.sisap_covid_pac_prv_casos WHERE (to_char(data, 'YYYYMMDD') >= '20200901' and to_char(data, 'YYYYMMDD') <='20210228') and simptomatic=1 GROUP BY DATA, hash"""
        for data, hash in u.getAll(sql, 'redics'):
            if hash in self.pacients_rca:
                zona = self.pacients_rca[hash]['zona']
                grup = self.pacients_rca[hash]['edat']
                self.simptomes[(data, zona, grup)] += 1
        upload = []
        for (data, zona, grup), n in self.simptomes.items():        
            upload.append([data, zona, grup, n])
        file = u.tempFolder + "aalba/case_clinic.csv"
        u.writeCSV(file, upload, sep='@')
        
    def get_casos(self):
        """casos"""
        u.printTime("casos")
        self.casos = c.Counter()
        sql = """SELECT hash, CAS_DATA_CAS, edat, rca_municipi FROM DWSISAP.DBC_metriques dv WHERE CAS_DATA_CAS IS NOT null and (to_char(CAS_DATA_CAS, 'YYYYMMDD') >= '20200901' and to_char(CAS_DATA_CAS, 'YYYYMMDD') <='20210228') """
        for hash, data, edat, municipi in u.getAll(sql, 'exadata'):
            if hash not in self.resis:
                if municipi in self.cataleg_mun:
                    zona = self.cataleg_mun[municipi]
                    grup = get_edat(int(edat))
                    self.casos[(data, zona, grup)] += 1
        upload = []
        for (data, zona, grup), n in self.casos.items():        
            upload.append([data, zona, grup, n])
            
        file = u.tempFolder + "aalba/all_cases.csv"
        u.writeCSV(file, upload, sep='@')
     
    def get_pcr(self):
        """PCRs"""
        u.printTime("PCR I TAR")
        self.tested = c.Counter()

        sql = """select hash, data, max(decode(estat_cod, 0, 1, 0)) 
               from sisap_covid_pac_prv_aux               
              where prova in ('PCR', 'Antigen') and 
                     origen <> 'IA' and to_char(data, 'YYYYMMDD') >= '20200901' and to_char(data,'YYYYMMDD') <='20210228'
               group by hash, data"""
        for hash, dat, pos in u.getAll(sql, db):
            if hash in self.pacients_rca:
                grup = self.pacients_rca[hash]['edat']
                zona = self.pacients_rca[hash]['zona']
                self.tested[(dat, zona, grup)]+=1  
        upload = []
        for (data, zona, grup), n in self.tested.items():        
            upload.append([data, zona, grup, n])
            
        file = u.tempFolder + "aalba/tested.csv"
        u.writeCSV(file, upload, sep='@')
     
    def get_alumnes(self):
        """alumnes"""
        u.printTime("Escoles")
        self.positius_alumnes, self.tests_escoles = c.Counter(), c.Counter()
        sql = """select id, data_prova, up, nivell from permanent.sisap_escoles_gce_jan27 where positiu=1 and perfil='Alumne' and nivell in ('EINFLOE', 'EPRILOE', 'ESO', 'BATXLOE')
        and data_prova >= '2020-09-13' and data_prova <='2021-02-28'"""
        for id, data, up, nivell in u.getAll(sql, 'permanent'):
            if up in self.cataleg_up:
                zona = self.cataleg_up[up]
                self.positius_alumnes[zona, data, nivell] += 1
        
        upload = []
        for (zona, data, nivell), n in self.positius_alumnes.items():        
            upload.append([data, zona, nivell, n])
            
        file = u.tempFolder + "aalba/school_case.csv"
        u.writeCSV(file, upload, sep='@')
        
            
        sql = """select a.id, a.data_prova, up, nivell 
                from permanent.sisap_escoles_resul_jan27  a
                inner join  permanent.sisap_escoles_gce_jan27 b on a.id=b.id 
                where  perfil='Alumne' and nivell in ('EINFLOE', 'EPRILOE', 'ESO', 'BATXLOE')
                and a.data_prova >= '2020-09-13' and a.data_prova <='2021-02-28'"""
        for id, dat, up, nivell in u.getAll(sql, 'permanent'):
            if up in self.cataleg_up:
                zona = self.cataleg_up[up]
                self.tests_escoles[zona, dat, nivell] += 1
        
        upload = []
        for (zona, data, nivell), n in self.tests_escoles.items():        
            upload.append([data, zona, nivell, n])
            
        file = u.tempFolder + "aalba/school_test.csv"
        u.writeCSV(file, upload, sep='@')
        
        self.gce = c.Counter()
        sql = """select gce_id, up, nivell from permanent.sisap_escoles_gce_jan27 where perfil='Alumne' and nivell in ('EINFLOE', 'EPRILOE', 'ESO', 'BATXLOE')"""
        for gce, up, nivell in u.getAll(sql, 'permanent'):
            if up in self.cataleg_up:
                zona = self.cataleg_up[up]   
                self.gce[(zona, nivell, gce)] += 1
        
        comptem = c.Counter()        
        for (zona, nivell, gce), n in self.gce.items():
            if 5 < n < 36:
                comptem[(zona, nivell, 'num')] += n
                comptem[(zona, nivell, 'den')] += 1
        upload = []
        for (zona, nivell, tip), n in comptem.items():
            if tip == 'den':
                num = comptem[(zona, nivell, 'num')]
                avg = float(num)/float(n)
                upload.append([zona, nivell,avg])
            
        file = u.tempFolder + "aalba/class_size.csv"
        u.writeCSV(file, upload, sep='@')
            
    def get_dbs(self):
        """."""
        u.printTime("dbs")
        self.comorbidities = c.Counter()
        sql = """select c_cip,c_up_abs, c_edat_anys,
                    PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA,  ps_bronquitis_cronica_data, PS_CARDIOPATIA_ISQUEMICA_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA,
                    PS_ACV_MCV_DATA,  ps_valvulopatia_data,PS_NEOPLASIA_M_DATA, 
                    PS_OBESITAT_DATA
                from dbs"""
        for (cip, abs, edat,
              dm1, dm2, mpoc,
             asma, bronquitis, ci, ic, fa,
             avc, valv, neo, obesitat
             ) in u.getAll(sql, db):
            if str(abs) in self.cataleg_up:
                zona = self.cataleg_up[str(abs)]
                grup = get_edat(edat)
                pato = 0
                if dm1 or dm2 or asma or mpoc or bronquitis or avc or fa or ci or ic or valv or neo or obesitat:
                    pato = 1
                self.comorbidities[(grup, zona, pato)] += 1
                
        upload = []
        for (grup, zona, pato), n in self.comorbidities.items():
            if pato == 0:
                malalties = self.comorbidities[(grup, zona, 1)]
                total = malalties + n
                perc = (float(malalties)/float(total))*100
                upload.append([zona, grup, perc])
                
        file = u.tempFolder + "aalba/comorbidities.csv"
        u.writeCSV(file, upload, sep='@')
        
    
if __name__ == '__main__':
    u.printTime("Inici")
    
    projecteAA()
    
    u.printTime("Final")