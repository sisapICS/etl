---
title: "VRS Vall d'Hebron"
output: html_notebook
---

```{r message=TRUE, warning=TRUE, include=FALSE}
library("data.table")
library("ggplot2")
library("lubridate")
library("kableExtra")
library("forecast")
library("gridExtra")
library("xlsx")
```

# Dades

```{r}
dades <- data.table(read.xlsx("D:/projectes/covid19/vrs_VH/20210713TEMPORADA FLU - VRS_PRUEBA.xlsx", "dades"))
dades <- setorder(dades, Any, Temporada)
dades$index <- 1:nrow(dades)
library(stringr)
dades[, data := as.Date(paste0(Any, str_pad(Setmana, 2, pad = "0"), 1), "%Y%U%u")]
```

```{r}
dades_melt_1 <- melt(dades, id.vars = c("Temporada_NM", "Any", "Setmana", "data"), measure.vars = c("REBUDES_F", "REBUDES_M", "REBUDES_U"))
names(dades_melt_1)[6] <- "Tests"
dades_melt_1[, variable := factor(variable, levels = c("REBUDES_F", "REBUDES_M"), labels = c("Dona", "Home"))]
dades_melt_2 <- melt(dades, id.vars = c("Temporada_NM", "Any", "Setmana", "data"), measure.vars = c("VRS_F", "VRS_M"))
names(dades_melt_2)[6] <- "VRS"
dades_melt_2[, variable := factor(variable, levels = c("VRS_F", "VRS_M"), labels = c("Dona", "Home"))]

dades_melt <- merge(dades_melt_1, dades_melt_2, by = c("Temporada_NM", "Any", "Setmana", "data", "variable"))

# dades_melt[, index := paste0(Temporada_NM, " - ", Setmana)]
```

# Descriptiva

```{r}
ggplot(dades_melt, aes(data, Tests, color = variable, group = variable)) +
  geom_line() +
  theme_classic() +
  labs(color = "") +
  theme(
    legend.position = "bottom"
  )
# + facet_wrap(~ Any, nrow = 1)
```

```{r}
ggplot(dades_melt, aes(data, VRS, color = variable, group = variable)) +
  geom_line() +
  theme_classic() +
  labs(color = "") +
  theme(
    legend.position = "bottom"
  )
# + facet_wrap(~ Any, nrow = 1)
```

```{r}
ggplot(dades, aes(Setmana, VRS_Total.general, color = Temporada_NM, group = Temporada_NM)) +
  geom_line() +
  theme_classic() +
  labs(color = "") +
  theme(
    legend.position = "bottom"
  ) 
```

# Time series

## VRS

```{r}
dades_ts <- dades[!Temporada_NM %in% c("2019-2020", "2020-2021") & Setmana != 53, c("data", "Temporada_NM", "VRS_Total.general")]
dades_ts <- dades_ts[order(data)]

ili_ts <- ts(dades_ts$VRS_Total.general, frequency = 52)

fit <- tslm(ili_ts ~ season, data = dades_ts)
  
noves_dades <- dades[Temporada_NM %in% c("2019-2020", "2020-2021") & Setmana != 53, c("data", "Temporada_NM", "Setmana", "VRS_Total.general")]
noves_dades <- noves_dades[order(data)]

pred <- forecast(fit, h = nrow(noves_dades), level = c(80, 90, 95))
  
dt_predict <- as.data.table(pred)
dt_predict$data <- noves_dades$data
dt_predict <- merge(dt_predict, noves_dades[,  c("data", "Temporada_NM", "Setmana", "VRS_Total.general")], by = "data", all.x = T)
  
```

```{r}
dt_predict[, c("Point Forecast", "Lo 95", "Hi 95") := lapply(.SD, function(x){
  ifelse(x < 0, 0, x)
}), .SDcols = c("Point Forecast", "Lo 95", "Hi 95")]
```



```{r}
dg <- melt(dt_predict, id.vars = c("data", "Temporada_NM", "Setmana", "Lo 95", "Hi 95"), measure.vars = c("VRS_Total.general", "Point Forecast"))
dg[, variable := factor(variable, levels = c("Point Forecast", "VRS_Total.general"), labels = c("Expected", "Observed"))]
```

```{r}
tiff("figure2.tiff", units="in", width=8, height=6, res=600)
ggplot(dg, aes(x = data)) +
# ggplot(dg[data < today()], aes(data)) +
  geom_line(aes(group = variable, y = value, linetype = variable)) +
  geom_ribbon(aes(ymin = `Lo 95`, ymax = `Hi 95`), fill = "grey", alpha = .5) +
  theme_classic() + labs(title = "", x = "", y = "New weekly RSV laboratory-confirmations", linetype = "") +
  scale_x_date(breaks = "2 week") + 
  # geom_vline(xintercept = as.Date(c("2020-03-13", "2020-06-19")), linetype = 2) +
  scale_linetype_manual(values = c("Expected" = 2, "Observed" = 1)) +
  theme(axis.text.x = element_text(angle = 90, vjust = .5),
        legend.position = "bottom") 
dev.off()
```

# Càlcul excés

```{r}
dt_predict[, lack := ifelse(VRS_Total.general < `Lo 95`, 1, 0)]
```

```{r message=FALSE, warning=FALSE}
library(dplyr)
periodes_lack <- dt_predict[lack == 1, 
                              .(start = min(data), 
                                end = max(data),
                                expected = paste0(format(sum(`Point Forecast`), big.mark = ".", decimal.mark = ",", digits = 4)), 
                                observed = paste0(format(sum(VRS_Total.general), big.mark = ".", decimal.mark = ",", digits = 4)), 
                                cases = paste0(format(sum(`Point Forecast` - VRS_Total.general), big.mark = ".", decimal.mark = ",", digits = 4), " [IC95%: ", format(sum(`Lo 95` - VRS_Total.general), big.mark = ".", decimal.mark = ",", digits = 4), " - ", format(sum(`Hi 95` - VRS_Total.general), big.mark = ".", decimal.mark = ",", digits = 4), "]"),
                              perc = paste0(format(sum(`Point Forecast` - VRS_Total.general)/sum(`Point Forecast`)*100, big.mark = ".", decimal.mark = ",", digits = 4), " [IC95%: ", format(sum(`Lo 95` - VRS_Total.general)/sum(`Lo 95`)*100, big.mark = ".", decimal.mark = ",", digits = 4), " - ", format(sum(`Hi 95` - VRS_Total.general)/sum(`Hi 95`)*100, big.mark = ".", decimal.mark = ",", digits = 4), "]")),
                              by = .(lack, g = cumsum(c(0, diff(data)) >7))][order(start)]
periodes_lack[, number_of_days := end - start]
# periodes_defecte <- periodes_defecte[dies != 0]
```


```{r}

setorderv(periodes_lack, c("start"))[, c(3:9)] %>%
  kable(escape = F, caption = "", format.args = list(decimal.mark = ',', big.mark = "."), align = c("l", "l", "c", "c", "c", "c", "c")) %>%
  kable_styling(full_width = T, position = "center") %>%
  row_spec(0, bold = T)
```
# Càlcul excés

```{r}
dt_predict[, excess := ifelse(VRS_Total.general > `Hi 95`, 1, 0)]
```

```{r message=FALSE, warning=FALSE}
library(dplyr)
periodes_excess <- dt_predict[excess == 1, 
                              .(start = min(data), 
                                end = max(data),
                                expected = paste0(format(sum(`Point Forecast`), big.mark = ".", decimal.mark = ",", digits = 4)), 
                                observed = paste0(format(sum(VRS_Total.general), big.mark = ".", decimal.mark = ",", digits = 4)), 
                                cases = paste0(format(sum(`Point Forecast` - VRS_Total.general), big.mark = ".", decimal.mark = ",", digits = 4), " [IC95%: ", format(sum(`Lo 95` - VRS_Total.general), big.mark = ".", decimal.mark = ",", digits = 4), " - ", format(sum(`Hi 95` - VRS_Total.general), big.mark = ".", decimal.mark = ",", digits = 4), "]"),
                              perc = paste0(format(sum(`Point Forecast` - VRS_Total.general)/sum(`Point Forecast`)*100, big.mark = ".", decimal.mark = ",", digits = 4), " [IC95%: ", format(sum(`Lo 95` - VRS_Total.general)/sum(`Lo 95`)*100, big.mark = ".", decimal.mark = ",", digits = 4), " - ", format(sum(`Hi 95` - VRS_Total.general)/sum(`Hi 95`)*100, big.mark = ".", decimal.mark = ",", digits = 4), "]")),
                              by = .(excess, g = cumsum(c(0, diff(data)) >7))][order(start)]
periodes_excess[, number_of_days := end - start]
# periodes_defecte <- periodes_defecte[dies != 0]
```


```{r}

setorderv(periodes_excess, c("start"))[, c(3:9)] %>%
  kable(escape = F, caption = "", format.args = list(decimal.mark = ',', big.mark = "."), align = c("l", "l", "c", "c", "c", "c", "c")) %>%
  kable_styling(full_width = T, position = "center") %>%
  row_spec(0, bold = T)
```
