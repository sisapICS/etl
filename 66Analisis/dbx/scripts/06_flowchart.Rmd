***
***

# Flowchart

Diagrama amb els criteris d'inclusió i excluisió de l'estudi.

```{r}
ini <- Sys.time()
n_reg <- nrow(dt.dbx.dm)
n_cip <- dt.dbx.dm[,uniqueN(cip)]
n_nia <- dt.dbx.dm[,uniqueN(nia)]

dt.taula <- dt.dbx.dm[, .N, any_exitus][order(any_exitus)]
datatable(dt.taula,
          rownames = FALSE,
          filter = 'top',
          options = list(pageLength = nrow(dt.taula)))

n_reg_2017 <- dt.taula[any_exitus == 2017,]$N
n_reg_2018 <- dt.taula[any_exitus == 2018,]$N
n_reg_2019 <- dt.taula[any_exitus == 2019,]$N
n_reg_2020 <- dt.taula[any_exitus == 2020,]$N
n_reg_2021 <- dt.taula[any_exitus == 2021,]$N
n_reg_2022 <- dt.taula[any_exitus == 2022,]$N
n_reg_2023 <- dt.taula[any_exitus == 2023,]$N
```

```{r}
fin <- c(seq(1,20,1))

grViz("
      digraph a_nice_graph
      {
      
      node[fontname = Helvetica,
           fontcolor = black,
           shape = box,
           width = 1,
           style = filled,
           fillcolor = whitesmoke]
      
      '@@1' -> '@@2';
      '@@1' -> '@@3';
      '@@1' -> '@@4';
      '@@1' -> '@@5';
      '@@1' -> '@@6';
      '@@1' -> '@@7';
      '@@1' -> '@@8';
      }

      [1]: paste0('Registres N=', n_reg, '\\n', 'Pacients: N=', n_cip, '\\n', 'Pacients (NIA): N=', n_nia)
      [2]: paste0('2017', '\\n', 'Pacients: N=', n_reg_2017)
      [3]: paste0('2018', '\\n', 'Pacients: N=', n_reg_2018)
      [4]: paste0('2019', '\\n', 'Pacients: N=', n_reg_2019)
      [5]: paste0('2020', '\\n', 'Pacients: N=', n_reg_2020)
      [6]: paste0('2021', '\\n', 'Pacients: N=', n_reg_2021)
      [7]: paste0('2022', '\\n', 'Pacients: N=', n_reg_2022)
      [8]: paste0('2023', '\\n', 'Pacients: N=', n_reg_2023)
      ", height = 400, width = 1000)
```

***

```{r filtre}

#filtre = 
dt.dbx.flt <- dt.dbx.dm
```

```{r}
#rm(list = c("dt.dbx.dm"))
gc.var <- gc()
```

