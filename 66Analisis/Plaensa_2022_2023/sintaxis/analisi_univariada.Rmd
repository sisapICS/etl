### Preguntes individuals de l'enquesta

##### Puntuació mitjana de les preguntes de l'enquesta

```{r}
respostes <- dades_anys[PERIODE==periode]
```


```{r message=FALSE, warning=FALSE}
# Calcul mitjana i CV de cada resposta
dades_puntuacio_mitjana_cv <- melt(respostes[, .('mean' = mean(RESULTAT_CL),
                                                   'median' = sd(RESULTAT)/mean(RESULTAT)), by=INDICADOR])

#dades_puntuacio_mitjana_cv[variable == "median", value_grafic := value]
#dades_puntuacio_mitjana_cv[INDICADOR == "P101" & variable == "mean", value_grafic := value]
#dades_puntuacio_mitjana_cv[INDICADOR != "P101" & variable == "mean", value_grafic := value * 10]
names(dades_puntuacio_mitjana_cv)[3] <- "value_grafic"
dades_puntuacio_mitjana_cv[, value_grafic_complementari := 10 - value_grafic]

dades_puntuacio_mitjana_cv <- merge(dades_puntuacio_mitjana_cv, cataleg, by = "INDICADOR")
```

```{r fig.width=5, fig.height=2}
# Plot puntuació mitjana i CV preguntes 101 i 102

names(dades_puntuacio_mitjana_cv)[2] <- "mesura"

dg <- melt(dades_puntuacio_mitjana_cv, id.vars = c("DESCRIPCIO", "INDICADOR", "mesura"), measure.vars = c("value_grafic", "value_grafic_complementari"))

dg_dcast <- dcast(dg, DESCRIPCIO + INDICADOR  + variable ~ mesura, value.var = "value")

ggplot(dg_dcast[DESCRIPCIO %in% c("P101 Grau de satisfacció global", "P102 Continuaria venint?")], aes(DESCRIPCIO)) +
  geom_bar(stat = "Identity", aes(y = mean, fill = factor(variable, levels = c("value_grafic_complementari", "value_grafic"))), color = "#2F3734", width = .7) +
  geom_text(data = dg_dcast[DESCRIPCIO %in% c("P101 Grau de satisfacció global", "P102 Continuaria venint?") & variable == "value_grafic"], aes(y = 0.1, label = paste0(DESCRIPCIO, " (CV = ", round(median, 2), ")")), hjust = 0, color = "#0F2318", size = 3.5) +
  scale_y_continuous(breaks = seq(0, 10, 1), expand = c(0, 0)) + 
  scale_fill_manual(values = c("value_grafic_complementari" = "white", "value_grafic" = "#A9C7B0")) +
  scale_color_manual(values = c("P101 Grau de satisfacció global" = "#0F2318")) +
  scale_linetype_manual(values = c("P101 Grau de satisfacció global" = 2)) +
  coord_flip() +
  theme_minimal() +
  theme(
    axis.text.y = element_blank(),
    axis.ticks.y = element_blank(),
    panel.grid.major.y = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank(),
    legend.position = "bottom"
  ) + 
  guides(fill = "none") +
  labs(title = "", x = "", y = "", color = "", linetype = "", caption = "CV: Coeficient de variació")
```

```{r fig.width=10, fig.height=7}
# Plot puntuació mitjana i CV preguntes != 101 i 102

ggplot(dg_dcast[!DESCRIPCIO %in% c("P101 Grau de satisfacció global", "P102 Continuaria venint?")], aes(DESCRIPCIO)) +
  geom_bar(stat = "Identity", aes(y = mean, fill = factor(variable, levels = c("value_grafic_complementari", "value_grafic"))), color = "#2F3734") +
  geom_text(data = dg_dcast[!DESCRIPCIO %in% c("P101 Grau de satisfacció global", "P102 Continuaria venint?") & variable == "value_grafic"], aes(y = 0.1, label = paste0(DESCRIPCIO, " (CV = ", round(median, 2), ")")), hjust = 0, color = "#0F2318", size = 3.5) +
  geom_hline(aes(yintercept = dg_dcast[DESCRIPCIO == "P101 Grau de satisfacció global" & variable == "value_grafic", mean], linetype = "P101 Grau de satisfacció global", color = "P101 Grau de satisfacció global")) +
  scale_x_discrete(labels = rev) +
  scale_y_continuous(breaks = seq(0, 10, 1), expand = c(0, 0)) + 
  scale_fill_manual(values = c("value_grafic_complementari" = "white", "value_grafic" = "#A9C7B0")) +
  scale_color_manual(values = c("P101 Grau de satisfacció global" = "#0F2318")) +
  scale_linetype_manual(values = c("P101 Grau de satisfacció global" = 2)) +
  coord_flip() +
  theme_minimal() +
  theme(
    axis.text.y = element_blank(),
    axis.ticks.y = element_blank(),
    panel.grid.major.y = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank(),
    legend.position = "bottom"
  ) + 
  guides(fill = "none") +
  labs(title = "", x = "", y = "", color = "", linetype = "", caption = "CV: Coeficient de variació")
```

```{r}
# Taulta amb Mitjana(CV) per cada pregunta
taula <- dg_dcast[variable == "value_grafic", .(
  Pregunta = DESCRIPCIO,
  `Mitjana (CV)` = paste0(round(mean, 2), " (", round(median, 2), ")")
)]

taula %>%  
  kable(digits = 2, escape = F, format.args = list(decimal.mark = ',', big.mark = "."), align = c("l", "c")) %>%
  kable_styling(full_width = F, position = "center") %>%
  row_spec(0, bold = T, background = "#A9C7B0")
```

```{r}
# Plot puntuació mitjana vs coeficient de variació

ggplot(dg_dcast[!DESCRIPCIO %in% c("P101 Grau de satisfacció global", "P102 Continuaria venint?") & variable == "value_grafic"], aes(x = mean, y = median)) +
  geom_point() +
  geom_text(aes(label = DESCRIPCIO), hjust = -.05, size = 3) +
  geom_hline(yintercept = .1, linetype = 2, color = "#2F3734") +
  geom_vline(xintercept = 7, linetype = 2, color = "#2F3734") +
  theme_minimal() +
  labs(x = "Puntuació mitjana", y = "Coeficient de variació")
```
