from lvclient import LvClient

def exemple(conn, query):
	print("EXEMPLE: {}".format(query))
	res = conn.query(query)
	print('RESULTAT:')
	print(res[:1000])
	print('')

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

exemple(c, 'MII015;A2109;AMB06;NOCLI;PRAC;NOIMP;DIM6SET')
exemple(c, 'MII015;A2109;AMB06;NOCLI;TEOR;NOIMP;DIM6SET')

c.close()
