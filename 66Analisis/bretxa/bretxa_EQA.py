# -*- coding: latin1 -*-

"""
Bretxa i interseccionalitat
"""

import collections as c
import psutil as p
import datetime as d
import difflib as s

import sisapUtils as u



class bretxa(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_idcip()
        self.get_cip()
        self.get_aportacio()
        self.get_EQA()
        self.get_assignada()
        self.export()
        
    def get_idcip(self):
        """id_cip to hash"""
        u.printTime("id_cip i hash")
        self.id_to_hash = {}
        
        sql = """select 
                    id_cip, id_cip_sec, hash_d, codi_sector 
                from 
                    u11"""
        for id, id_sec, hash, sec in u.getAll(sql, 'import'):
            self.id_to_hash[id_sec] = hash
    
    def get_cip(self):
        """cip to hash"""
        u.printTime("cip")
        self.cip_to_hash = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.cip_to_hash[cip] = hash
    
    def get_aportacio(self):
        """indicador de farmacia"""
        u.printTime("farmacia")
        self.farma = {}
        sql = "SELECT c_cip, substr(c_cip,0,13), C_IND_FARMACIA, C_SUBIND_FARMACIA, C_IND_FARMACIA_CALC , C_SUBIND_FARMACIA_CALC , C_IND_EXEMPT_TAXA, C_TITOL FROM dwcatsalut.RCA_POB_OFICIAL rpo WHERE c_any_assegurat='2022'"
        for cip14, cip, indf, subindf, indcalc, subindcalc, exemp, titol in u.getAll(sql, 'exadata'):
            nrenda = None
            if indf=="TSI 001":
                nrenda='Exempte'
            elif indf=="TSI 002" and subindf=='00':
                nrenda='Sense recursos'
            elif indf=="TSI 002" and subindf=='01':
                nrenda='<18.000'
            elif indf=="TSI 002" and subindf=='02':
                nrenda='18.000-100.000'
            elif indf=="TSI 003":
                nrenda='<18.000'
            elif indf=="TSI 004":
                nrenda='18.000-100.000'
            elif indf=="TSI 005":
                nrenda='>100.000'
            elif indf=="TSI 006":
                nrenda='Mutues'
            else:
                nrenda=None
            hash = self.cip_to_hash[cip] if cip in self.cip_to_hash else None
            self.farma[hash] = nrenda
     
    def get_EQA(self):
        """."""
        u.printTime("EQA")
        self.indicadors = []
        self.inclosos = {}
        sql = """SELECT id_cip_sec, ind_codi, grup_codi, up, uba, ates, excl_edat, maca, num, den, excl, ci, clin FROM eqa_ind.mst_indicadors_pacient where id_cip_sec >0"""
        for id, ind_codi, grup_codi, up, uba, ates, excl_edat, maca, num, den, excl, ci, clin in u.getAll(sql, 'nodrizas'):
            self.inclosos[id] = True
            hash =  self.id_to_hash[id]
            self.indicadors.append([hash, ind_codi, grup_codi, up, uba, ates, excl_edat, maca, num, den, excl, ci, clin])
    
    def get_assignada(self):
        """."""
        u.printTime("assignada")
        self.assignada = []
        sql = """select id_cip_sec, data_naix, sexe, nacionalitat, nivell_cobertura, institucionalitzat from assignada_tot"""
        for id, naix, sexe, nac, cobertura, insti in u.getAll(sql, 'nodrizas'):
            if id in self.inclosos:
                hash =  self.id_to_hash[id]
                farmacia = self.farma[hash] if hash in self.farma else None
                self.assignada.append([hash, naix, sexe, nac, cobertura, farmacia, insti])
    
       
    def export(self):
        """."""
        u.printTime("export")
        
        db = "bretxa"
        tb = "indicadors_eqa"
        cols = ("hash varchar(40)", "ind_codi varchar(20)", "grup_codi varchar(20)", "up varchar(10)", "uba varchar(10)", "ates int", "excl_edat int","maca int",
        "num int", "den int", "excl int", "ci int", "clin int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.indicadors, tb, db)     
        
        tb = "poblacio"
        cols = ("hash varchar(40)", "naixement date", "sexe varchar(1)", "nacionalitat varchar(10)", "cobertura varchar(10)", "se_farmacia varchar(40)", "institucionalizat int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.assignada, tb, db)
        
         
if __name__ == '__main__':
    u.printTime("Inici")
    
    bretxa()

    u.printTime("Fi")