---
title: "Hospital de Referència - CMBD-URG (Campanya Gripal)"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
  word_document:
    toc: yes
    toc_depth: '6'
params:
  #taules: ['seguiment_clinic']
  #catalegs: ['cat_seguiment_clinic']
  actualitzar_dades: TRUE
  skim: TRUE
---

# RESUM

***
***
***

### Paràmetres de l'informe

- **actualitzar_dades**: Flag que permet decidir si s'actualitzen les dades del informe
- **skim**: Flag que permet executar o no la library skimr que descriu la informació de la informació importada

***

```{r setup, include=FALSE}
inici <- Sys.time()

knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

```{r parametres de conexió}
source("C:/Users/ehermosilla/Documents/Keys.R")
nfetch <- -1
```

```{r parametres rmarkdown generals}
warning.var <- FALSE
message.var <- FALSE
```

```{r parametres rmarkdown figures}
var.fig.height <- 4
var.fig.width <- 6

eval.fig.var <- FALSE # Flag per eliminar figures del informe
```

```{r parametres markdown evaluate}
eval.library.var <- TRUE

eval.import.var <- TRUE
eval.datamanager.var <- TRUE

eval.flowchart.var <- FALSE

eval.univariada.var <- FALSE
eval.bivariada.var <- FALSE
eval.analisi.var <- FALSE

eval.annex.var <- FALSE

eval.timeexecution.var <- FALSE
```

```{r library, child="02_library.Rmd", eval=eval.library.var}

```

```{r import, child="04_import.Rmd", eval=eval.import.var}

```

```{r datamanager, child="05_datamanager.Rmd", eval=eval.datamanager.var}

```

```{r flowchart, child="06_flowchart.Rmd", eval=eval.flowchart.var}

```

```{r univariada, child="07_descriptiva_univariada.Rmd", eval=eval.univariada.var}

```

```{r bivariada, child="08_descriptiva_bivariada.Rmd", eval=eval.bivariada.var}

```

```{r analisi, child="09_analisi.Rmd", eval=eval.analisi.var}

```

```{r annex, child="99_annex.Rmd", eval=eval.annex.var}

```

***
***

```{r time execution, eval=eval.timeexecution.var}

final <- Sys.time()
print(round(final - inici,2))
```
