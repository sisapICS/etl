***
***

# Anàlisi

Es crea un catàleg únic amb la informació dels 2 catàlegs. Domina el catàleg de Francoise Aguado sobre el de Núria Nadal.

```{r}
setnames(dt.cat_hreferencia_nn.dm.wide, "n_hosp_ref", "n_hosp_ref_nn")
setnames(dt.cat_hreferencia_nn.dm.wide, "hosp_ref_codi_1", "hosp_ref_codi_1_nn")
setnames(dt.cat_hreferencia_nn.dm.wide, "hosp_ref_desc_1", "hosp_ref_desc_1_nn")
setnames(dt.cat_hreferencia_nn.dm.wide, "hosp_ref_codi_2", "hosp_ref_codi_2_nn")
setnames(dt.cat_hreferencia_nn.dm.wide, "hosp_ref_desc_2", "hosp_ref_desc_2_nn")

setnames(dt.cat_hreferencia_fa.dm.wide, "n_hosp_ref", "n_hosp_ref_fa")
setnames(dt.cat_hreferencia_fa.dm.wide, "hosp_ref_codi_1", "hosp_ref_codi_1_fa")
setnames(dt.cat_hreferencia_fa.dm.wide, "hosp_ref_desc_1", "hosp_ref_desc_1_fa")
setnames(dt.cat_hreferencia_fa.dm.wide, "hosp_ref_codi_2", "hosp_ref_codi_2_fa")
setnames(dt.cat_hreferencia_fa.dm.wide, "hosp_ref_desc_2", "hosp_ref_desc_2_fa")

dt.cat_hreferencia <- merge(dt.cat_hreferencia_nn.dm.wide[, .(eap_codi, ics_desc, eap_desc,
                                                              ep, 
                                                              amb_desc, sap_desc,
                                                              n_hosp_ref_nn,
                                                              hosp_ref_codi_1_nn, hosp_ref_desc_1_nn, 
                                                              hosp_ref_codi_2_nn, hosp_ref_desc_2_nn)],
                            dt.cat_hreferencia_fa.dm.wide[, .(eap_codi,
                                                              n_hosp_ref_fa,
                                                              hosp_ref_codi_1_fa, hosp_ref_desc_1_fa, 
                                                              hosp_ref_codi_2_fa, hosp_ref_desc_2_fa)],
                            by = c('eap_codi'),
                            all = TRUE
                            )

SDCols.var = names(dt.cat_hreferencia)
dt.cat_hreferencia[, (SDCols.var) := lapply(.SD, function(x) {factor(x)}), .SDcols = SDCols.var]

dt.cat_hreferencia[, hosp_ref_codi_1 := hosp_ref_codi_1_fa]
dt.cat_hreferencia[is.na(hosp_ref_codi_1), hosp_ref_codi_1 := hosp_ref_codi_1_nn]

dt.cat_hreferencia[, hosp_ref_desc_1 := hosp_ref_desc_1_fa]
dt.cat_hreferencia[is.na(hosp_ref_desc_1), hosp_ref_desc_1 := hosp_ref_desc_1_nn]

dt.cat_hreferencia[, hosp_ref_codi_2 := hosp_ref_codi_2_fa]
dt.cat_hreferencia[is.na(hosp_ref_codi_2), hosp_ref_codi_2 := hosp_ref_codi_2_nn]

dt.cat_hreferencia[, hosp_ref_desc_2 := hosp_ref_desc_2_fa]
dt.cat_hreferencia[is.na(hosp_ref_desc_2), hosp_ref_desc_2 := hosp_ref_desc_2_nn]

SDCols.var = c("hosp_ref_codi_1", "hosp_ref_codi_2", "hosp_ref_desc_1", "hosp_ref_desc_2")
dt.cat_hreferencia[, (SDCols.var) := lapply(.SD, function(x) {factor(x)}), .SDcols = SDCols.var]
```

### Modificacions

```{r modificacions, echo = TRUE}
# Modificacions
  dt.cat_hreferencia[, modificado := 'No']
  dt.cat_hreferencia[as.character(eap_codi) == '00915', hosp_ref_codi_1 := '00759']
  dt.cat_hreferencia[as.character(eap_codi) == '00915', hosp_ref_desc_1 := 'HC Sant Jaume Calella i HC de Blanes']
  dt.cat_hreferencia[as.character(eap_codi) == '00915', modificado := 'Sí']
  
  dt.cat_hreferencia[as.character(eap_codi) == '14423', hosp_ref_codi_1 := '00718']
  dt.cat_hreferencia[as.character(eap_codi) == '14423', hosp_ref_desc_1 := 'Hospital Clínic de Barcelona']
  dt.cat_hreferencia[as.character(eap_codi) == '14423', modificado := 'Sí']
  
  dt.cat_hreferencia[as.character(eap_codi) == '15386', hosp_ref_codi_1 := '00744']
  dt.cat_hreferencia[as.character(eap_codi) == '15386', hosp_ref_desc_1 := 'Hospital de Terrassa']
  dt.cat_hreferencia[as.character(eap_codi) == '15386', modificado := 'Sí']
  
  dt.cat_hreferencia[as.character(eap_codi) == '15443', hosp_ref_codi_1 := '00001']
  dt.cat_hreferencia[as.character(eap_codi) == '15443', hosp_ref_desc_1 := "Hospital Universitari Arnau de Vilanova de Lleida"]
  dt.cat_hreferencia[as.character(eap_codi) == '15443', modificado := 'Sí']
  
  dt.cat_hreferencia[as.character(eap_codi) == '15838', hosp_ref_codi_1 := '01012']
  dt.cat_hreferencia[as.character(eap_codi) == '15838', hosp_ref_desc_1 := "Hospital Comarcal de l'Alt Penedès"]
  dt.cat_hreferencia[as.character(eap_codi) == '15838', modificado := 'Sí']
  
  dt.cat_hreferencia[as.character(eap_codi) == '15839', hosp_ref_codi_1 := '01012']
  dt.cat_hreferencia[as.character(eap_codi) == '15839', hosp_ref_desc_1 := "Hospital Comarcal de l'Alt Penedès"]
  dt.cat_hreferencia[as.character(eap_codi) == '15839', modificado := 'Sí']
  
  dt.cat_hreferencia[as.character(eap_codi) == '00598', hosp_ref_codi_1 := '00746']
  dt.cat_hreferencia[as.character(eap_codi) == '00598', hosp_ref_desc_1 := "Hospital del Mar - H. de l'Esperança"]
  dt.cat_hreferencia[as.character(eap_codi) == '00598', modificado := 'Sí']
  
  dt.cat_hreferencia[as.character(eap_codi) == '01273', hosp_ref_codi_1 := '00746']
  dt.cat_hreferencia[as.character(eap_codi) == '01273', hosp_ref_desc_1 := "Hospital del Mar - H. de l'Esperança"]
  dt.cat_hreferencia[as.character(eap_codi) == '01273', modificado := 'Sí']
  
  dt.cat_hreferencia[as.character(eap_codi) == '08311', hosp_ref_codi_1 := '06046']
  dt.cat_hreferencia[as.character(eap_codi) == '08311', hosp_ref_desc_1 := "Hospital Universitari de la Vall d'Hebron"]
  dt.cat_hreferencia[as.character(eap_codi) == '08311', modificado := 'Sí']
  
  dt.cat_hreferencia[as.character(eap_codi) == '08312', hosp_ref_codi_1 := '06046']
  dt.cat_hreferencia[as.character(eap_codi) == '08312', hosp_ref_desc_1 := "Hospital Universitari de la Vall d'Hebron"]
  dt.cat_hreferencia[as.character(eap_codi) == '08312', modificado := 'Sí']
  
  dt.cat_hreferencia[is.na(ics_desc), modificado := 'Sí']
  dt.cat_hreferencia[is.na(ics_desc), ics_desc := eap_desc]

#dt.cat_hreferencia[modificado == 'Sí',]
```


### Catàleg final (Taula dinàmica)

A continuació es presenta el catàleg final de Hospital de Referència

```{r}
dt.datatable <- dt.cat_hreferencia[, .(eap_codi, ics_desc, 
                                       ep, amb_desc, sap_desc,
                                       hosp_ref_codi_1, hosp_ref_desc_1,
                                       hosp_ref_codi_2, hosp_ref_desc_2,
                                       n_hosp_ref_nn, hosp_ref_desc_1_nn, hosp_ref_desc_2_nn,
                                       n_hosp_ref_fa, hosp_ref_desc_1_fa, hosp_ref_desc_2_fa,
                                       modificado)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

### Exportació de excel

```{r}
writexl::write_xlsx(dt.datatable, "../results/cat_hreferencia_final.xlsx")
```

### Creació taula a Permanent

```{r}
source("C:/Users/ehermosilla/Documents/Keys.R")
drv <- dbDriver("MySQL")
con <- dbConnect(drv,
                 user = idpermanent,
                 password = pwpermanent,
                 host = hostpermanent,
                 port = portpermanent,
                 dbname = dbpermanent)

query <- "DROP TABLE IF EXISTS cat_hospital_ref;"
results <- dbSendQuery(con, query)
dbClearResult(results)

query <- "CREATE TABLE
            cat_hospital_ref (eap_codi VARCHAR(5),
                              ics_desc VARCHAR(100),
                              amb_desc VARCHAR(100),
                              sap_desc VARCHAR(100),
                              hosp_ref_codi_1 VARCHAR(5),
                              hosp_ref_desc_1 VARCHAR(100)
                              );"
# Enviar query a MuSQL per la seva execució
  results <- dbSendQuery(con, query)
  dbClearResult(results)

dbWriteTable(con,
             "cat_hospital_ref",
             dt.datatable[, .(eap_codi, ics_desc,
                              amb_desc, sap_desc,
                              hosp_ref_codi_1, hosp_ref_desc_1)],
             row.names =  FALSE,
             append = TRUE)
var.disconnect <- dbDisconnect(con)
```

```{r analisi gc}

gc.var <- gc()
```

