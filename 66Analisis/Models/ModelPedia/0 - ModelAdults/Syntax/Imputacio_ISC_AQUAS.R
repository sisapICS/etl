## Imputació missings AQUAS

library(data.table)

khalix <- fread("../ModelPedia/0 - ModelAdults/Dades/cataleg.TXT", sep = "{", header = F)[, c("V1", "V3", "V9")]
khalix <- dcast(khalix, V3 ~ V1, value.var = "V9")
names(khalix)[1] <- "BR"

dm <- khalix[!is.na(INDAQUAS)]
m1 <- lm(INDAQUAS ~ MEDEA, data = khalix)
summary(m1)

# new_INDAQUAS <- khalix[is.na(INDAQUAS)]
new_INDAQUAS <- data.table("MEDEA" = -0.5912541959189468531765219435210369027232)
pred1 <- predict(m1, new_INDAQUAS)
new_INDAQUAS$INDAQUAS_pred <- pred1

pred1

summary(m1)
