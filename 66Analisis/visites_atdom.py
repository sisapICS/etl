# -*- coding: utf8 -*-

import hashlib as h

"""
.
"""

import sisapUtils as u


DEBUG = False

TB = "visites_atdom"
DB = "permanent"
upload = []

class visites_atdom(object):
    """."""

    def __init__(self):
        """."""
        self.get_cip()
        self.get_nafres()
        self.get_visites()
        self.get_atdom()
        self.export_data()             
    
    def get_cip(self):
        """hash to cip"""
        u.printTime("cip")
        self.hash_to_cip = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.hash_to_cip[hash] = cip

    def get_nafres(self):
        """Obtenim nafres 2019 de la taula de nafres d exadata"""
        u.printTime("nafres 2019")
        
        self.nafres = {}
        sql = """select
                        hash
                from
                    dwsisap.upp_2019"""
        for hash, in u.getAll(sql, 'exadata'):
            self.nafres[hash]= True
            
    def get_visites(self):
        """visites d exadata"""
        u.printTime("visites_2019")
        
        self.visites = {}
        
        sql = """select pacient,  EXTRACT (YEAR FROM data) ,
                     sum(CASE WHEN SERVEI IN ('MG','PED','INF','INFPD','INFP','INFG','GCAS','INFGR','INFC','ACINF','INFEX','INFEN',
                    'WINF','INFE','URGEN','URG','ATCON','URGINF','URGMG','ESP')AND tipus IN ('9C','9R','9D')  THEN 1 ELSE 0 END) presencial,
                    sum(CASE WHEN SERVEI IN ('MG','PED','INF','INFPD','INFP','INFG','GCAS','INFGR','INFC','ACINF','INFEX','INFEN',
                    'WINF','INFE','URGEN','URG','ATCON','URGINF','URGMG','ESP')AND tipus NOT IN ('9C','9R','9D')  THEN 1 ELSE 0 END) no_presencial
                from 
                    DWSISAP.SISAP_CORONAVIRUS_ACTIVITAT 
                where
                    flag_4cw=0
                    AND modul NOT LIKE 'VC%%'
                    AND EXTRACT (YEAR FROM data)='2019'
                group by
                    pacient,
                    EXTRACT (YEAR FROM data)"""
        for pacient, anys, presencials, no_presencials in u.getAll(sql, 'exadata'):
            self.visites[(pacient)] = {'presencial': presencials, 'no_presencial': no_presencials}
            
    def get_atdom(self):
        """obtenim atdom del 2019 del dbs"""
        u.printTime("atdom dbs")
        
        SIDICS_DB = ("dbs", "x0002")
        sql = """ 
                SELECT c_cip, C_UP, c_data_naix, c_sexe, c_gma_complexitat, 
                CASE WHEN PR_MACA_DATA <>'' THEN  1 ELSE 0 END maca,
                CASE WHEN PR_pcc_DATA <>'' THEN 1 ELSE 0 END pcc,
                CASE WHEN PS_DEMENCIA_DATA  <>'' THEN 1 ELSE 0 END demencia,
                CASE WHEN PS_PSICOSI_DATA  <>'' OR PS_PSICOSI_AFECTIVA_DATA  <>0 THEN 1 ELSE 0 END psicosi,
                CASE WHEN PS_NEOPLASIA_M_DATA <>'' THEN 1 ELSE 0 END neo,
                CASE WHEN  V_TIRS_VALOR>1 THEN 1 ELSE 0 END TIRS
                FROM dbs_2019
                where 
                PS_ATDOM_DATA <> ''
            """
        for hash, up, naix, sexe, gma,  maca, pcc, demencia, psicosi, neo, tirs in u.getAll(sql, SIDICS_DB):
            if hash in self.hash_to_cip:
                cip2 = self.hash_to_cip[hash]
                cip = h.sha1(cip2).hexdigest().upper()
            presencial = self.visites[(cip)]['presencial'] if (cip) in self.visites else 0
            no_presencial = self.visites[(cip)]['no_presencial'] if (cip) in self.visites else 0
            Nnafres = 1 if cip in self.nafres else 0
            upload.append([cip, up, naix, sexe, gma, presencial, no_presencial, maca, pcc, demencia, psicosi, neo, Nnafres, tirs])

    def export_data(self):
        """export"""
        u.printTime("export")

        cols = ("pacient varchar(40)", "up varchar(5)", "data_naix date", "sexe varchar(1)", "gma_complexitat double",
                "presencial int", "no_presencial int", "maca int", "pcc int", "demencia int", "psicosi int", "neo int", "nafres int",
                "risc_social int")
        u.createTable(TB, "({})".format(", ".join(cols)), DB, rm=True)
        u.listToTable(upload, TB, DB) 

if __name__ == '__main__':
    u.printTime("Inici")
     
    visites_atdom()