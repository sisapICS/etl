---
title: "Anàlisi dels pacients long covid"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
  word_document:
    toc: yes
    toc_depth: '6'

---


<style>
 .ocultar {
   display: none;

 }

</style>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
options(knitr.kable.na = '')

```


```{r parametres markdown}
inici <- Sys.time()

warning.var <- FALSE
message.var <- FALSE

eval.library.var <- TRUE
eval.import.var <- TRUE
eval.datamanager.var <- TRUE
eval.analisi.var <- TRUE

```

```{r parametres estudi}

nfetch <- -1

```

```{r library, child="library.Rmd", eval=eval.library.var}

```


```{r import, child="import.Rmd", eval=eval.import.var}

```


```{r datamanager, child="datamanager.Rmd", eval=eval.datamanager.var}

```

```{r resultats, child="analisi.Rmd", eval=eval.analisi.var}

```

