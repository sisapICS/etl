***
***

# Flowchart

Diagrama amb els criteris d'inclusió i excluisió de l'estudi.

```{r}
n_reg <- nrow(dt.dbs.dm)
```

```{r}
# fin <- c(seq(1,20,1))
# 
# grViz("
#       digraph a_nice_graph
#       {
#       
#       node[fontname = Helvetica,
#            fontcolor = black,
#            shape = box,
#            width = 1,
#            style = filled,
#            fillcolor = whitesmoke]
#       
#       '@@1'
#       }
# 
#       [1]: paste0('Registres N=', n_reg, '\\n','Indicadors N=', n_ind, '\\n','Periodes N=', n_periodes, '\\n','UP Origen N=', n_uporigen, '\\n','UP Destí N=', n_updestí, '\\n','Especialitats N=', n_especialitat)
#       ", height = 300, width = 300)
```

***

```{r filtre}
dt.dbs.flt <- dt.dbs.dm[c_sector != "6951",]
```

```{r}
#rm(list = c(""))
gc.var <- gc()
```

