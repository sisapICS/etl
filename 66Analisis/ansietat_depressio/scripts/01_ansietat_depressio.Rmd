---
title: "Prevalença ansietat i depressió amb i sense tractament"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
params:
   actualitzar_dades: FALSE
   skim: TRUE
---

# RESUM

***
***
***

### Paràmetres informe

- TAULES:
  - DBS

- CATÀLEGS:


***

### Actualització informe

- 2023-12-11 Inici informe

***
***

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

```{r parametres markdown}
inici <- Sys.time()

nfetch <- -1

warning.var <- FALSE
message.var <- FALSE

eval.library.var <- TRUE

eval.import.var <- TRUE

eval.datamanager.var <- TRUE

eval.flowchart.var <- TRUE

eval.univariada.var <- FALSE

eval.bivariada.var <- FALSE

eval.analisi.var <- TRUE

eval.annex.var <- FALSE

eval.timeexecution.var <- TRUE

var.fig.height <- 4
var.fig.width <- 6
```

```{r parametres estudi}
```




```{r library, child="02_library.Rmd", eval=eval.library.var}

```

```{r import, child="04_import.Rmd", eval=eval.import.var}

```

```{r datamanager, child="05_datamanager.Rmd", eval=eval.datamanager.var}

```

```{r flowchart, child="06_flowchart.Rmd", eval=eval.flowchart.var}

```

```{r descriptiva univariada, child="descriptiva_univariada.Rmd", eval=eval.univariada.var}

```

```{r descriptiva bivariada, child="descriptiva_bivariada.Rmd", eval=eval.bivariada.var}

```

```{r analisi, child="09_analisi.Rmd", eval=eval.analisi.var}

```



```{r annex, child="annex.Rmd", eval=eval.annex.var}

```

---

```{r time execution, eval=eval.timeexecution.var}

final <- Sys.time()
print(round(final - inici,2))
```
