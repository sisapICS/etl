# -*- coding: utf8 -*-

"""
Mirem quants adolescents tenen telefon i telefon mobil, per peticio de la Montse
"""

import collections as c
import sisapUtils as u

import hashlib as h




class sms_vac(object):
    """."""

    def __init__(self):
        """."""
        self.get_assignada()
        self.export()       
        
    
    def get_assignada(self):
        """Agafem els telfs"""
        u.printTime("telfs")
        
        self.upload = []
        dext, = u.getOne("select data_ext from dextraccio", 'nodrizas')
        print dext
        for sector in u.sectors:
            print sector
            sql = """SELECT usua_cip,to_date(USUA_data_naixement, 'J'), usua_sexe, usua_up, USUA_TELEFON_PRINCIPAL, USUA_TELEFON_SECUNDARI FROM usutb040 WHERE usua_situacio='A' AND usua_uab_up IS NOT null"""
            for hash, naix, sexe, up, telf1, telf2 in u.getAll(sql, sector):
                edat = u.yearsBetween(naix, dext)
                self.upload.append([hash, naix,  edat, sexe, up, telf1, telf2])
                
    def export(self):
        """exportem les taules"""
        u.printTime("export")
        
        db = "permanent"
        tb = "prova_telfs"
        columns = ["hash varchar(40), naix date, edat int, sexe varchar(10), up varchar(10), telf1 varchar(20), telf2 varchar(20)"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
if __name__ == "__main__":
    u.printTime("Inici")
    
    sms_vac()
    
    u.printTime("Fi")


