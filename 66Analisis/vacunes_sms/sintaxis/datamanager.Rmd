
<!-- ```{r} -->
<!-- cat_antigen[criteri_codi == "A00009", agrupador_desc := "Antimeningocòccica"] -->
<!-- cat_antigen[criteri_codi == "A00009", agrupador := 883] -->
<!-- cat_antigen[criteri_codi == "A00032", agrupador_desc := "VacH A+B"] -->
<!-- cat_antigen[criteri_codi == "A00032", agrupador := 153400] -->

<!-- cat_antigen <- unique(cat_antigen) -->
<!-- ``` -->

```{r}
cat_centres[, medea := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), labels = c("Rural", "Rural", "Rural", "1U", "2U", "3U", "4U"))]
cat_centres[, urba := factor(medea, levels = c("Rural", "Rural", "Rural", "1U", "2U", "3U", "4U"), labels = c("Rural", "Rural", "Rural", "Urbà", "Urbà", "Urbà", "Urbà"))]
```


```{r}
vac_dm <- rbind(
  vac[, cat := "vacunats"],
  vac_alta[, cat := "registrats"]
)

vac_dm <- merge(vac_dm, cat_vac[, .(vacuna, des, antigen)], by.x = "cod", by.y = "vacuna", all.x = T)
vac_dm <- merge(vac_dm, cat_centres[, .(ics_codi, scs_codi, amb_codi, medea, urba)], by.x = "up", by.y = "scs_codi", all.x = T)
vac_dm <- merge(vac_dm, cat_antigen, by.x = "antigen", by.y = "criteri_codi", all.x = T, allow.cartesian = T)
vac_dm[, data := as.Date(data)]
vac_dm[, dins_periode := ifelse(data > as.Date("2022-09-07") & data < as.Date("2023-01-01"), "Dins", "Fora")]


```

```{r exclusions}
vac_dm <- vac_dm[edat %in% 4:14]
vac_dm_ics <- vac_dm[amb_codi != "00"]
vac_dm_ics <- vac_dm_ics[!is.na(agrupador_desc)]
vac_dm_ics <- vac_dm_ics[agrupador_desc != "Antigripal"]
# vac_dm_ics <- vac_dm_ics[agrupador_desc != "Polio"]
vac_dm_ics <- vac_dm_ics[agrupador_desc != "Vacuna Rotavirus"]
vac_dm_ics <- vac_dm_ics[agrupador_desc != "Vacuna VPH"]

vac_dm_ics <- vac_dm_ics[agrupador %in% c(15, 313, 312, 311, 36, 49, 314, 34, 548, 883, 782, 698)]

vac_dm_ics[agrupador == 15, VACUNA := "VHB"]
vac_dm_ics[agrupador == 313, VACUNA := "MCC"]
vac_dm_ics[agrupador == 312, VACUNA := "HIB"]
vac_dm_ics[agrupador == 311, VACUNA := "PO"]
vac_dm_ics[agrupador == 36, VACUNA := "XRP"]
vac_dm_ics[agrupador == 49, VACUNA := "DTP"]
vac_dm_ics[agrupador == 314, VACUNA := "VARICE"]
vac_dm_ics[agrupador == 34, VACUNA := "VHA"]
vac_dm_ics[agrupador == 548, VACUNA := "MCB"]
vac_dm_ics[agrupador == 883, VACUNA := "MACWY"]
vac_dm_ics[agrupador == 782, VACUNA := "VPH"]
vac_dm_ics[agrupador == 698, VACUNA := "VNC"]


```

<!-- ```{r} -->

<!-- freq_antigen <- vac_dm_ics[cat == "vacunats", .N, .(agrupador_desc)][order(N, decreasing = T)] -->
<!-- freq_antigen[, p := N/sum(N)*100] -->
<!-- freq_antigen[order(N, decreasing = T), p_cumsum := cumsum(p)] -->
<!-- # freq_antigen[order(N, decreasing = T)] -->

<!-- ggplot(freq_antigen, aes(agrupador_desc, p_cumsum)) + -->
<!--   geom_bar(stat = "identity", fill = "#7E7F9A") + -->
<!--   scale_x_discrete(limits = freq_antigen[order(N, decreasing = T)]$agrupador_desc) + -->
<!--   theme_minimal() + -->
<!--   theme( -->
<!--     axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1) -->
<!--   ) + -->
<!--   labs(x = "Antígen", y = "Freq acumulada") -->
<!-- ``` -->

```{r}
estudi_sms[, SEXE := factor(SEXE, levels = c("D", "H"), labels = c("Femení", "Masculí"))]
estudi_sms[, edat := trunc((`TO_DATE(DATA_NAIX,'J')` %--% as.Date("2022-09-08")) / years(1))]

estudi_sms <- merge(estudi_sms[, .(CIP, SEXE, edat)], up_imput[, .(CIP, UP)], by = "CIP", all.x = T)
estudi_sms <- merge(estudi_sms, cat_centres[, .(ics_codi, scs_codi, amb_codi, medea, aquas, urba)], by.x = "UP", by.y = "scs_codi", all.x = T)
```

```{r}
vacunes_mancants[, PO := NULL]
vacunes_mancants[, DTP := NULL]
vacunes_mancants <- merge(vacunes_mancants, vacunes_mancants_reals, by = "CIP", all.x = T)
vacunes_mancants[is.na(PO), PO := 1]
vacunes_mancants[is.na(DTP), DTP := 1]
```



```{r}
n0 <- nrow(vacunes_mancants)
vacunes_mancants[, vacunes_administrades := apply(.SD, 1, sum), .SDcols = c("VHB", "MCC", "HIB", "PO", "XRP", "DTP", "VARICE", "VHA", "MCB", "MACWY", "VPH", "VNC")]
n_excl_1 <- nrow(vacunes_mancants[vacunes_administrades == 12])
vacunes_mancants_dm <- vacunes_mancants[vacunes_administrades < 12]
n1 <- nrow(vacunes_mancants_dm)
vacunes_mancants_dm_demografiques <- merge(vacunes_mancants_dm, estudi_sms, by = "CIP")
n_excl_2 <- nrow(vacunes_mancants_dm_demografiques[edat > 10])
vacunes_mancants_dm_demografiques <- vacunes_mancants_dm_demografiques[edat < 11]
n2 <- nrow(vacunes_mancants_dm_demografiques)


vacunes_mancants_long <- melt(vacunes_mancants_dm_demografiques, id.vars = c("CIP", "edat", "SEXE", "UP", "medea", "aquas", "urba"), measure.vars = c("VHB", "MCC", "HIB", "PO", "XRP", "DTP", "VARICE", "VHA", "MCB", "MACWY", "VPH", "VNC"), variable.name = "VACUNA", value.name = "TENGUI")
vacunes_mancants_long[VACUNA == "VHB", agrupador := 15]
vacunes_mancants_long[VACUNA == "MCC", agrupador := 313]
vacunes_mancants_long[VACUNA == "HIB", agrupador := 312]
vacunes_mancants_long[VACUNA == "PO", agrupador := 311]
vacunes_mancants_long[VACUNA == "XRP", agrupador := 36]
vacunes_mancants_long[VACUNA == "DTP", agrupador := 49]
vacunes_mancants_long[VACUNA == "VARICE", agrupador := 314]
vacunes_mancants_long[VACUNA == "VHA", agrupador := 34]
vacunes_mancants_long[VACUNA == "MCB", agrupador := 548]
vacunes_mancants_long[VACUNA == "MACWY", agrupador := 883]
vacunes_mancants_long[VACUNA == "VPH", agrupador := 782]
vacunes_mancants_long[VACUNA == "VNC", agrupador := 698]

```


```{r}
estat_vacunacio_311222_long <- melt(estat_vacunacio_311222, id.vars = c("CIP"), measure.vars = c("VHB", "MCC", "HIB", "PO", "XRP", "DTP", "VARICE", "VHA", "MCB", "MACWY", "VPH", "VNC"), variable.name = "VACUNA", value.name = "TENGUI_FINAL")
estat_vacunacio_311222_long[VACUNA == "VHB", agrupador := 15]
estat_vacunacio_311222_long[VACUNA == "MCC", agrupador := 313]
estat_vacunacio_311222_long[VACUNA == "HIB", agrupador := 312]
estat_vacunacio_311222_long[VACUNA == "PO", agrupador := 311]
estat_vacunacio_311222_long[VACUNA == "XRP", agrupador := 36]
estat_vacunacio_311222_long[VACUNA == "DTP", agrupador := 49]
estat_vacunacio_311222_long[VACUNA == "VARICE", agrupador := 314]
estat_vacunacio_311222_long[VACUNA == "VHA", agrupador := 34]
estat_vacunacio_311222_long[VACUNA == "MCB", agrupador := 548]
estat_vacunacio_311222_long[VACUNA == "MACWY", agrupador := 883]
estat_vacunacio_311222_long[VACUNA == "VPH", agrupador := 782]
estat_vacunacio_311222_long[VACUNA == "VNC", agrupador := 698]
```

```{r}
vacunes_mancants_long_PRE_POST <- merge(vacunes_mancants_long, estat_vacunacio_311222_long, by = c("CIP", "VACUNA", "agrupador"), all.x = T)
n_excl_2_bis <- nrow(vacunes_mancants_long_PRE_POST[is.na(TENGUI_FINAL), .N, CIP])
n2_final <- nrow(vacunes_mancants_long_PRE_POST[!is.na(TENGUI_FINAL), .N, CIP])
```



```{r}
# Antigen = unique(dades$agrupador_desc)[, DATA_VACUNA := as.Date(DATA_VACUNA)]
nens_vacunats_periode[, DATA_REGISTRE := as.Date(DATA_REGISTRE)]
nens_vacunats_periode[, DATA_VACUNA := as.Date(DATA_VACUNA)]
nens_vacunats_periode[, vacuna_dins_periode := ifelse(DATA_VACUNA > as.Date("2022-09-07") & DATA_VACUNA <= as.Date("2022-12-31"), 1, 0)]
nens_vacunats_periode[, registre_dins_periode := ifelse(DATA_REGISTRE > as.Date("2022-09-07") & DATA_REGISTRE <= as.Date("2022-12-31"), 1, 0)]

nens_vacunats_periode_dm <- merge(nens_vacunats_periode, cat_vac[, .(vacuna, des, antigen)], by.x = "CODI", by.y = "vacuna", all.x = T)
nens_vacunats_periode_dm <- merge(nens_vacunats_periode_dm, cat_antigen, by.x = "antigen", by.y = "criteri_codi", all.x = T, allow.cartesian = T)

###########


nens_vacunats_periode_dm <- nens_vacunats_periode_dm[!is.na(agrupador)]
nens_vacunats_periode_dm_selected <- nens_vacunats_periode_dm[agrupador %in% c(15, 311, 49, 312, 36, 313, 314, 34, 548, 883, 782, 698)]
n3 <- nrow(nens_vacunats_periode_dm_selected)
n_excl_3 <- nrow(nens_vacunats_periode_dm_selected[registre_dins_periode == 0])

nens_vacunats_periode_dm_selected <- nens_vacunats_periode_dm_selected[registre_dins_periode == 1]
n4 <- nrow(nens_vacunats_periode_dm_selected)
```

```{r}
vacunes_mancants_long_data_registre <- merge(vacunes_mancants_long_PRE_POST[TENGUI == 0], nens_vacunats_periode_dm_selected, by = c("CIP", "agrupador"), all.x = T)

vacunes_mancants_long_data_registre[is.na(vacuna_dins_periode), vacuna_dins_periode := 0]
vacunes_mancants_long_data_registre[is.na(registre_dins_periode), registre_dins_periode := 0]
n5 <- vacunes_mancants_long_data_registre[, sum(registre_dins_periode == 1, na.rm = T)]
n6 <- vacunes_mancants_long_data_registre[, sum(vacuna_dins_periode == 1, na.rm = T)]

n5_tipus <- vacunes_mancants_long_data_registre[, sum(registre_dins_periode == 1, na.rm = T), VACUNA]
```



```{r}
historia_vac_nens_sms[, DATA_REGISTRE := as.Date(DATA_REGISTRE)]
historia_vac_nens_sms[, DATA_VACUNA := as.Date(DATA_VACUNA)]
historia_vac_nens_sms_dm <- merge(historia_vac_nens_sms, cat_vac[, .(vacuna, des, antigen)], by.x = "CODI", by.y = "vacuna", all.x = T)
historia_vac_nens_sms_dm <- merge(historia_vac_nens_sms_dm, cat_antigen, by.x = "antigen", by.y = "criteri_codi", all.x = T, allow.cartesian = T)


historia_vac_nens_sms_dm <- historia_vac_nens_sms_dm[agrupador %in% c(15, 313, 312, 311, 36, 49, 314, 34, 548, 883, 782, 698)]

historia_vac_nens_sms_dm[agrupador == 15, VACUNA := "VHB"]
historia_vac_nens_sms_dm[agrupador == 313, VACUNA := "MCC"]
historia_vac_nens_sms_dm[agrupador == 312, VACUNA := "HIB"]
historia_vac_nens_sms_dm[agrupador == 311, VACUNA := "PO"]
historia_vac_nens_sms_dm[agrupador == 36, VACUNA := "XRP"]
historia_vac_nens_sms_dm[agrupador == 49, VACUNA := "DTP"]
historia_vac_nens_sms_dm[agrupador == 314, VACUNA := "VARICE"]
historia_vac_nens_sms_dm[agrupador == 34, VACUNA := "VHA"]
historia_vac_nens_sms_dm[agrupador == 548, VACUNA := "MCB"]
historia_vac_nens_sms_dm[agrupador == 883, VACUNA := "MACWY"]
historia_vac_nens_sms_dm[agrupador == 782, VACUNA := "VPH"]
historia_vac_nens_sms_dm[agrupador == 698, VACUNA := "VNC"]

historia_vac_nens_sms_dm[, registre_in_periode := ifelse(DATA_REGISTRE > as.Date("2022-09-07") & DATA_REGISTRE <= as.Date("2022-12-31"), 1, 0)]
```



```{r}
historia_vac_nens_sms_duplicats <- historia_vac_nens_sms_dm[, .N, .(CIP, agrupador, agrupador_desc, CODI, VACUNA, DATA_VACUNA, DOSI)][N>1]

aux <- merge(historia_vac_nens_sms_duplicats, historia_vac_nens_sms_dm[, .(CIP, CODI, DATA_VACUNA, DATA_REGISTRE, DOSI, registre_in_periode)], by = c("CIP", "CODI", "DATA_VACUNA", "DOSI"))
aux[, registres_fora_periode := sum(registre_in_periode == 0), by = c("CIP", "CODI", "DATA_VACUNA", "DOSI")]
registres_dins_periode_duplicats <- aux[registre_in_periode != N][registre_in_periode != 0][DATA_VACUNA < as.Date("2022-09-07")]
```

```{r}
vacunes_mancants_long_data_registre_duplicats <- merge(vacunes_mancants_long_data_registre, unique(registres_dins_periode_duplicats[, .(CIP, CODI, agrupador, VACUNA, DATA_VACUNA, DATA_REGISTRE, DOSI, registres_fora_periode)]), by = c("CIP", "CODI", "agrupador", "VACUNA", "DATA_VACUNA", "DATA_REGISTRE", "DOSI"), all.x = T)
```


```{r}
estudi_sms_dm <- estudi_sms[CIP %in% unique(vacunes_mancants_long_data_registre[, CIP])]



dades_demografiques <- rbind(
  estudi_sms_dm[, .(VACUNA = "GLOBAL"), .(UP, CIP, SEXE, edat, medea, aquas, urba)],
  estudi_sms_dm[CIP %in% vacunes_mancants_long_data_registre[!VACUNA %in% c("XRP", "DTP", "MCC", "VNC"), CIP], .(VACUNA = "ALTRES"), .(UP, CIP, SEXE, edat, medea, aquas, urba)],
  unique(vacunes_mancants_long_data_registre[, .(UP, CIP, SEXE, edat, medea, aquas, urba, VACUNA)])
)
```


