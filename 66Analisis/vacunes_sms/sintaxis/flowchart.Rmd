
# Flowchart

```{r}
#### TAULA vacunes_mancants
 grViz("digraph diagraph {

      node[fontname = Helvetica,
           fontcolor = black,
           shape = box,
           width = 1,
           style = filled,
           fillcolor = '#F49097']
       
       '@@1'
       '@@2'
       '@@3'
       '@@4'
       '@@5'

       
       node [shape=none, width=0, height=0, label='']
       w1
       w2
       
       edge []
       w1 -> '@@2';
       w1 -> '@@3';
       w2 -> '@@4';
       w2 -> '@@5';

       
       edge [dir=none]
       '@@1' -> w1;
       '@@3' -> w2;
       

       
 {rank=same; w1 -> '@@2'}
 {rank=same; w2 -> '@@4'}
       }
    
      [1]: paste0('vacunes_mancants', '\\n', 'Pacients: ', n0)
      [2]: paste0('Tenen totes les', '\\n', 'vacunes posades', '\\n', 'Pacients: ', n_excl_1)
      [3]: paste0('Pacients amb alguna', '\\n', 'vacuna mancant: ', '\\n', n1)
      [4]: paste0('Edat > 10 anys', '\\n', 'Pacients: ', n_excl_2)
      [5]: paste0('Pacients amb alguna', '\\n', 'vacuna mancant', '\\n', 'cumpleixen CI', '\\n', n2)

     ")



```



