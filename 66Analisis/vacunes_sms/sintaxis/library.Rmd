
```{r}
options(java.parameters = "-Xmx8000m")
```


```{r}
library(RMySQL)
library(data.table)
library(lubridate)
library(ggplot2)
library(kableExtra)
library(segmented)
library(table1)
library(DiagrammeR)
library(DiagrammeRsvg)
library(stringi)
library(epitools)
library(compareGroups)
```

