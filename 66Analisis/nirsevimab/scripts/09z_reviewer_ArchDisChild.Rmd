---
title: "NIRSEVIMAB - Anàlisi \nPeríode 2023/04/01 - 2023/09/30 - REVIEWER ARCH DIS CHILD"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
params:
  actualitzar_dades: TRUE
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

```{r}
inici <- Sys.time()
```

```{r}
# Importació
  suppressWarnings(suppressPackageStartupMessages(library('RJDBC')))
  suppressWarnings(suppressPackageStartupMessages(library('DBI')))
  
# Data Manager
  suppressWarnings(suppressPackageStartupMessages(library('Hmisc')))
  suppressWarnings(suppressPackageStartupMessages(library('data.table')))
  
  suppressWarnings(suppressPackageStartupMessages(library('tidyverse')))
  
  suppressWarnings(suppressPackageStartupMessages(library('tibble')))
  suppressWarnings(suppressPackageStartupMessages(library('plyr')))
  suppressWarnings(suppressPackageStartupMessages(library('dplyr')))
  suppressWarnings(suppressPackageStartupMessages(library('tidyr')))
  suppressWarnings(suppressPackageStartupMessages(library('forcats')))
  suppressWarnings(suppressPackageStartupMessages(library('encode')))
  
  # Dates
    suppressWarnings(suppressPackageStartupMessages(library('lubridate')))

# Matching
  suppressWarnings(suppressPackageStartupMessages(library('Matching')))
  
# Anàlisi
  suppressWarnings(suppressPackageStartupMessages(library('compareGroups')))
  suppressWarnings(suppressPackageStartupMessages(library('stddiff')))
  suppressWarnings(suppressPackageStartupMessages(library('zoo')))
#   #suppressWarnings(suppressPackageStartupMessages(library('nlme')))
#   #suppressWarnings(suppressPackageStartupMessages(library('statmod')))
#   

# Modelització
#     suppressWarnings(suppressPackageStartupMessages(library('sjPlot')))
#     suppressWarnings(suppressPackageStartupMessages(library('lme4')))
#     suppressWarnings(suppressPackageStartupMessages(library('MASS'))) # Binomial Negative
#     suppressWarnings(suppressPackageStartupMessages(library('pscl'))) # Zero-Inflated Regression
#     suppressWarnings(suppressPackageStartupMessages(library('sandwich')))
#     suppressWarnings(suppressPackageStartupMessages(library('gtsummary')))
      
# # Figures
    suppressWarnings(suppressPackageStartupMessages(library('ggplot2'))) 
    suppressWarnings(suppressPackageStartupMessages(library('scales')))
#   #suppressWarnings(suppressPackageStartupMessages(library('splines')))
#   #suppressWarnings(suppressPackageStartupMessages(library('gridExtra')))
    #source('../functions.r')
# 
# # Taules
    suppressWarnings(suppressPackageStartupMessages(library('DT')))
```

***
***

```{r}
dt.cohort.dm <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/data/dt.cohort.dm.rds")
dt.cohort.dm.ana <- dt.cohort.dm[periode_estudi == 'Periode 1',]

dt.cohort.flt <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/data/dt.cohort.flt.rds")
dt.cohort.flt.ana <- dt.cohort.flt[periode_estudi == 'Periode 1',]
```

# Pacients Without a valid health identifier number

```{r}
dt.cohort.dm.ana[, ce1 := 0][!(eap_codi != '' & eap_codi != '01933' & !(situacio %in% c('F'))), ce1 := 1]
dt.cohort.dm.ana[, ce2 := 0][!(!(situacio %in% c('T'))), ce2 := 1]
dt.cohort.dm.ana[, ce3 := 0][!(data_defuncio >= as.Date('2023-10-01') | is.na(data_defuncio)), ce3 := 1]

dt.cohort.dm.ana <- dt.cohort.dm.ana[(ce1 == 1 & ce2 == 0) | (ce1 == 0 & ce2 == 0 & ce3 == 0),]
dt.cohort.dm.ana[, .N, .(ce1, ce2, ce3)][order(ce1, ce2, ce3)]

cols <- c("ce1",
          "nirse",
          "rural",
          "medea_c2",
          "aquas", "aquas",
          "month_naixement_f",
          "edat_20231001", "edat_20231001", "sexe",
          "espanyol",
          "edat_nirse")
df <- dt.cohort.dm.ana[,
                    .SD,
                    .SDcols = cols]
cG <- compareGroups(ce1 ~ nirse + rural + medea_c2 + aquas + aquas + month_naixement_f + edat_20231001 + edat_20231001 + sexe + espanyol + edat_nirse + edat_nirse,
                    df, 
                    method = c(1,1,1,1,2,1,1,2,1,1,1,2),
                    max.xlev = 20)
cT <- createTable(cG,
                  show.descr = TRUE,
                  show.n = TRUE,
                  show.all = FALSE,
                  hide.no = c("Dona", "No"),
                  show.ratio = FALSE)
export2md(cT
          , caption = "Comparació Criteri Excluisió 1"
          , format = "html")
```

# Pacients Sense informació d'AP

```{r}
cols <- c("dbs",
          "nirse",
          "rural",
          "medea_c2",
          "aquas", "aquas",
          "month_naixement_f",
          "edat_20231001", "edat_20231001", "sexe",
          "espanyol",
          "edat_nirse")
df <- dt.cohort.flt.ana[,
                    .SD,
                    .SDcols = cols]
cG <- compareGroups(dbs ~ nirse + rural + medea_c2 + aquas + aquas + month_naixement_f + edat_20231001 + edat_20231001 + sexe + espanyol + edat_nirse + edat_nirse,
                    df, 
                    method = c(1,1,1,1,2,1,1,2,1,1,1,2),
                    max.xlev = 20)
cT <- createTable(cG,
                  show.descr = TRUE,
                  show.n = TRUE,
                  show.all = FALSE,
                  hide.no = c("Dona", "No"),
                  show.ratio = FALSE)
export2md(cT
          , caption = "Comparació DBS"
          , format = "html")
```

# Bronquiolitis Inespecífiques

```{r}
dt.diagnostics.ecap.dm.wide <- readRDS("../data/dt.diagnostics.ecap.dm.wide.rds")
dt.diagnostics.ecap.dm.wide[, .N, bronquiolitis]
dt.diagnostics.ecap.dm.wide[bronquiolitis == 1, .N, .(bronquiolitis, bronquiolitis_vrs, bronquiolitis_novrs)]

dt.urgencies.dm <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/data/dt.urgencies.dm.rds")

dt.urgencies.dm[, .N, bronquiolitis]
dt.urgencies.dm[bronquiolitis == 1, .N, .(bronquiolitis, bronquiolitisvrs, bronquiolitis_novrs)]
```

# Figura R1

```{r}
options(java.parameters = "-Xmx32g")

Sys.setenv(TZ = 'Europe/Madrid')
Sys.setenv(ORA_SDTZ = 'Europe/Madrid')

source("C:/Users/ehermosilla/Documents/Keys.R")
```

```{r}
params$actualitzar_dades <- TRUE
if (params$actualitzar_dades) {
  ini <- Sys.time()
  driver_path = "C:/Users/ehermosilla/ojdbc8.jar"
  driver = JDBC("oracle.jdbc.driver.OracleDriver", driver_path)
  service_name  = "excdox01srv"
  dsn = paste0("jdbc:oracle:thin:@//", hostexadata, ":", portexadata, "/", service_name)
  connection = dbConnect(driver,
                         dsn,
                         idexadata,
                         pwexadata)
  query = "SELECT
            data,
            sexe,
            edat,
            regio,
            isc,
            abs_codi,
            diari
          FROM
            dwsisap.sivic_ecap_diagnostic
          WHERE
            diagnostic = 'Bronquiolitis' and
            data >= DATE '2023-09-28' and data <= DATE '2024-02-03'"
              
  dt.sivic_ecap_dx.raw = data.table(dbGetQuery(connection,
                                               query))
  var.disconnect <- dbDisconnect(connection)
  saveRDS(dt.sivic_ecap_dx.raw, "D:/SISAP/sisap/66Analisis/nirsevimab/data/dt.sivic_ecap_dx.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.sivic_ecap_dx.raw <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/data/dt.sivic_ecap_dx.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}
```

```{r}
setnames(dt.sivic_ecap_dx.raw, tolower(names(dt.sivic_ecap_dx.raw)))

#dt.sivic_ecap_dx.raw[, data := as.Date(data, '%Y-%m-%d')]

dt.sivic_ecap_dx.raw[, diari := as.numeric(diari)]

#dates <- seq(from = as.Date('2023-10-01'), to = as.Date('2024-01-31'), by = "day")
#dt.dates <- data.table(data = dates)
                    
dt.data.agr <- dt.sivic_ecap_dx.raw[, .(n = sum(diari)), data][order(data)]

dt.data.agr <- dt.data.agr[, nmovil7 := zoo::rollmean(n, k = 7, fill = NA)]
```

```{r}
figura <- ggplot(dt.data.agr[data >= as.Date('2023-10-01') & data <= as.Date('2024-01-31'),],
                 aes(x = as.Date(data), y = nmovil7)) +
                 geom_line() + 
                 scale_x_date(labels = date_format("%Y-%m")) +
                 labs(x = "Study Period (days)", y = "Bronchiolitis, (n)", linetype = "") +
                 theme_minimal()

tiff("../results/bronchiolitis_studyperiod.tiff", units = "mm", width = 250, height = 150, res = 300)
figura
dev.off()
```

```{r}
fi <- Sys.time()
fi - inici
```