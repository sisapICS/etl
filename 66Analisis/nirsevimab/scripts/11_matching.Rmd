---
title: "NIRSEVIMAB - Matching"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

```{r}
inici <- Sys.time()
```

## Library

```{r}
# Data Manager
  suppressWarnings(suppressPackageStartupMessages(library('Hmisc')))
  suppressWarnings(suppressPackageStartupMessages(library('data.table')))
  
  suppressWarnings(suppressPackageStartupMessages(library('tidyverse')))
  
  suppressWarnings(suppressPackageStartupMessages(library('tibble')))
  suppressWarnings(suppressPackageStartupMessages(library('plyr')))
  suppressWarnings(suppressPackageStartupMessages(library('dplyr')))
  suppressWarnings(suppressPackageStartupMessages(library('tidyr')))
  suppressWarnings(suppressPackageStartupMessages(library('forcats')))
  suppressWarnings(suppressPackageStartupMessages(library('encode')))

# Matching
  suppressWarnings(suppressPackageStartupMessages(library('Matching')))
  suppressWarnings(suppressPackageStartupMessages(library('MatchIt')))
  
# Anàlisi
  suppressWarnings(suppressPackageStartupMessages(library('compareGroups')))
  suppressWarnings(suppressPackageStartupMessages(library('stddiff')))
#   #suppressWarnings(suppressPackageStartupMessages(library('nlme')))
#   #suppressWarnings(suppressPackageStartupMessages(library('statmod')))
#   

# Modelització
  suppressWarnings(suppressPackageStartupMessages(library('splines')))
  suppressWarnings(suppressPackageStartupMessages(library('lmtest')))
#     suppressWarnings(suppressPackageStartupMessages(library('sjPlot')))
#     suppressWarnings(suppressPackageStartupMessages(library('lme4')))
#     suppressWarnings(suppressPackageStartupMessages(library('MASS'))) # Binomial Negative
#     suppressWarnings(suppressPackageStartupMessages(library('pscl'))) # Zero-Inflated Regression
#     suppressWarnings(suppressPackageStartupMessages(library('sandwich')))
#     suppressWarnings(suppressPackageStartupMessages(library('gtsummary')))
      
# Figures
  suppressWarnings(suppressPackageStartupMessages(library('ggplot2')))
  suppressWarnings(suppressPackageStartupMessages(library('ggformula'))) 
#   suppressWarnings(suppressPackageStartupMessages(library('scales')))
#   #suppressWarnings(suppressPackageStartupMessages(library('splines')))
#   #suppressWarnings(suppressPackageStartupMessages(library('gridExtra')))
    #source('../functions.r')
# 
# # Taules
    suppressWarnings(suppressPackageStartupMessages(library('DT')))
```

***

## Importació

S'importa la bbdd de l'article

```{r}
dt.cohort.ana <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/data/dt.cohort.flt.rds")
```

## Selecció de variables

Selecció de les variables necesàries per er el matching

```{r}
#names(dt.cohort.ana)
# dt.cohort.ana <- dt.cohort.ana[, .(hash, dbs, ics_desc,
#                                    rural, medea_c, medea_c2,
#                                    aquas,
#                                    edat_20231001,
#                                    sexe,
#                                    codi_nacionalitat, espanyol,
#                                    nirse)]
```

## Comparació Exposició NIRSEVIMAB

Comparació de les variables condidates per fer el matching

### Taula

```{r}
cols <- c("nirse",
          "rural",
          "medea_c2",
          "aquas",
          "edat_20231001", "sexe",
          "espanyol")
df <- dt.cohort.ana[,
                    .SD,
                    .SDcols = cols]
cG <- compareGroups(nirse ~ .,
                    df, 
                    method = 1,
                    max.xlev = 20)
cT <- createTable(cG,
                  show.descr = TRUE,
                  show.n = TRUE,
                  show.all = FALSE,
                  hide.no = c("Dona", "No"),
                  show.ratio = FALSE)
export2md(cT
          , caption = "Comparació Nirsevimab"
          , format = "html")
```

## Data Manager

Cal preparar les variables abans de fer el matching amb la funciño xxx de la llibreria XXXX

- Totes les variables han de ser numèriques


```{r}
dt.cohort.ana[nirse == 'No Nirsevimab', nirse_n := 0][nirse == 'Nirsevimab', nirse_n := 1]

dt.cohort.ana[, rural_n := as.numeric(rural)]
dt.cohort.ana[, medea_c_n := as.numeric(medea_c)]
dt.cohort.ana[, medea_c2_n := as.numeric(medea_c2)]
dt.cohort.ana[, espanyol_n := as.numeric(espanyol)]
dt.cohort.ana[sexe == 'Home', sexe_n := 0][sexe == 'Dona', sexe_n := 1]

dt.cohort.ana[, aquas_q4_n := cut(aquas,
                                  breaks = quantile(aquas,
                                                    probs = seq(0, 1, 0.25)),
                                  labels = FALSE,
                                  include.lowest = TRUE)]
dt.cohort.ana[, .(mean(aquas)), aquas_q4_n][order(aquas_q4_n)]

dt.cohort.ana[, aquas_q4_f := factor(aquas_q4_n)]
```

***
***

## Propensity Score

### Explorar el efectode las variables continuas

#### Edat

```{r}
a <- dt.cohort.ana[, .N, .(edat_20231001, nirse)]
a <- dcast(a, edat_20231001 ~ nirse, value.var = "N")
a[, p := round((Nirsevimab/(Nirsevimab + `No Nirsevimab`)), 2)]
a
ggplot(a,
       aes(x = edat_20231001, y = p)) +
       geom_line() +
       geom_smooth(method = "lm", se = FALSE, color = "red") +
       geom_smooth(method = "loess", se = FALSE, color = "blue") +
       #geom_spline(aes(x = edat_20231001, y = p, color = "green"), nknots = 4) +
       stat_spline() +
       #geom_smooth(method = "glm", formula = p ~ bs(edat_20231001, df = 3), color = "green", se = FALSE) + 
       theme_minimal()
```

#### Aquas

```{r}
a <- dt.cohort.ana[, .N, .(aquas, nirse)]
a <- dcast(a, aquas ~ nirse, value.var = "N")
a[, p := round((Nirsevimab/(Nirsevimab + `No Nirsevimab`)), 2)]
a
ggplot(a,
       aes(x = aquas, y = p)) +
       geom_point() +
       #geom_smooth(method = "lm", se = FALSE, color = "red") +
       geom_smooth(method = "loess", se = FALSE, color = "green") +
       geom_smooth(formula = y ~ poly(x,2), method = "lm", se = FALSE, color = "red") +
       geom_smooth(formula = y ~ poly(x,3), method = "lm", se = FALSE, color = "blue") +
       #stat_spline() +
       theme_minimal()
```

### Estimación de modelos

```{r}
#str(dt.cohort.ana)
dt.cohort.ana.ps <- dt.cohort.ana
m.ps.0 <- glm(nirse ~ edat_20231001 + sexe + medea_c2 + aquas + espanyol, 
              family = binomial(link = "logit"),
              data = dt.cohort.ana.ps)
summary(m.ps.0)
dt.cohort.ana.ps[, ps.m.0 := m.ps.0$fitted.values]
ggplot(dt.cohort.ana.ps,
       aes(ps.m.0)) +
  geom_histogram() +
  facet_grid(. ~nirse) +
  theme_minimal()
```

```{r}
m.ps.1 <- glm(nirse ~ I(edat_20231001^2) + sexe + medea_c2 + aquas + espanyol, 
              family = binomial(link = "logit"),
              data = dt.cohort.ana.ps)
summary(m.ps.1)
dt.cohort.ana.ps[, ps.m.1 := m.ps.1$fitted.values]

resultado_test <- lrtest(m.ps.0, m.ps.1)
resultado_test

ggplot(dt.cohort.ana.ps,
       aes(ps.m.1)) +
  geom_histogram() +
  facet_grid(. ~nirse) +
  theme_minimal()
```

```{r}
m.ps.2 <- glm(nirse ~ bs(edat_20231001, df = 3) + sexe + medea_c2 + aquas + espanyol, 
              family = binomial(link = "logit"),
              data = dt.cohort.ana.ps)
summary(m.ps.2)
dt.cohort.ana.ps[, ps.m.2 := m.ps.2$fitted.values]

resultado_test <- lrtest(m.ps.1, m.ps.2)
resultado_test

ggplot(dt.cohort.ana.ps,
       aes(ps.m.2)) +
  geom_histogram() +
  facet_grid(. ~nirse) +
  theme_minimal()
```

```{r}
m.ps.3 <- glm(nirse ~ bs(edat_20231001, df = 3) + sexe + medea_c2 + bs(aquas, df = 3) + espanyol, 
              family = binomial(link = "logit"),
              data = dt.cohort.ana.ps)
summary(m.ps.3)
dt.cohort.ana.ps[, ps.m.3 := m.ps.3$fitted.values]

resultado_test <- lrtest(m.ps.2, m.ps.3)
resultado_test

ggplot(dt.cohort.ana.ps,
       aes(ps.m.3)) +
  geom_histogram() +
  facet_grid(. ~nirse) +
  theme_minimal()
```

***
***

## Matching

### A mano

```{r}
# Está en el archivo 11b_matching_hand.Rmd
```

### Library Match

#### 1:1 Replace FALSE

```{r}
set.seed(123456)
dt.cohort.ana = dt.cohort.ana[sample(1:nrow(dt.cohort.ana), replace = FALSE), ]

X <- cbind('sexe' = dt.cohort.ana$sexe_n
           , 'edat' = dt.cohort.ana$edat_20231001
           , 'rural' = dt.cohort.ana$rural_n
           , 'espanyol' = dt.cohort.ana$espanyol_n
           #, 'aquas' = dt.cohort.ana$aquas
           ,'aquas' = dt.cohort.ana$aquas_q4_n
           #,'dbs' = dt.cohort.ana$dbs
           )
```

```{r}
ini <- Sys.time()
matching <- Match(Y = NULL, 
                  Tr = dt.cohort.ana$nirse_n,
                  X = X, 
                  exact = c(T, F, T, T, T),
                  replace = F,
                  M = 1,
                  caliper = 0.05) # 0,20 = 1/5
fi <- Sys.time()
fi - ini
summary(matching)
#str(matching)
```

```{r}
a <- data.table(matching$index.control)
a[, N := 1]
b <- a[, .N, V1]

dt.cohort.ana$match.m1 <- NA
dt.cohort.ana$match.m1[c(matching$index.treated,
                         matching$index.control)] <- sprintf("%d",
                                                             rep(1:length(matching$index.treated), times = 2))
dt.cohort.ana[, .N, match.m1]

dt.cohort.ana$match.m1_c <- 0
dt.cohort.ana[!is.na(match.m1), match.m1_c := 1]
dt.cohort.ana[, .N, match.m1_c]

dt.cohort.ana.wide <- dt.cohort.ana[, .(match.m1, match.m1_c, nirse, edat_20231001)]
dt.cohort.ana.wide1 <- dcast(dt.cohort.ana.wide[match.m1_c == 1,], match.m1 ~ nirse, value.var = "edat_20231001")
dt.cohort.ana.wide1[, edat_diff := `No Nirsevimab` - `Nirsevimab`]
summary(dt.cohort.ana.wide1[, edat_diff])

setnames(dt.cohort.ana, c('match.m1', 'match.m1_c'), c('match_f1005', 'match_f1005_c'))

saveRDS(dt.cohort.ana,
        paste0("../data/dt.cohort.matching_f1005.rds"))
```

```{r}
cols <- c("nirse",
          "rural",
          "medea_c2",
          "aquas", "aquas_q4_f",
          "edat_20231001", "sexe",
          "espanyol",
          "dbs")
df <- dt.cohort.ana[match_f1005_c == 1,
                    .SD,
                    .SDcols = cols]
cG <- compareGroups(nirse ~ .,
                    df, 
                    method = 1,
                    max.xlev = 20)
cT <- createTable(cG,
                  show.descr = TRUE,
                  show.n = TRUE,
                  show.all = FALSE,
                  hide.no = c("Dona", "No", "Urbà"),
                  show.ratio = FALSE)
export2md(cT
          , caption = "Comparació Nirsevimab (Matching)"
          , format = "html")
#export2word(cT,
#            "../results/matching_table_1.docx")
```

```{r matching smd}
dt.smd <- dt.cohort.ana[match_f1005_c == 1,]

tsmd <- do.call("rbind", 
                 lapply(c("rural",
                          "medea_c2",
                          "aquas", "aquas_q4_f",
                          "edat_20231001",
                          "sexe",
                          "espanyol",
                          "dbs"
                          ),
                          function(x){
                     d <- dt.smd[, .SD, .SDcols = c("nirse_n", x)]
    if (is.integer(dt.smd[, get(x)]) | is.numeric(dt.smd[, get(x)])) {
      smd <- tryCatch(stddiff.numeric(data = d, gcol = 1, vcol = 2)[,"stddiff"], error = function(e) NA)
      t <- data.table(V1 = c(smd))
      t[, Variable := x]
      t[, c("Variable", "V1")]
  } 
  else {
    smd <- tryCatch(stddiff.category(data = d, gcol = 1, vcol = 2)[1,"stddiff"], error = function(e) NA)
      t <- data.table(V1 = c(smd))
      t[, Variable := x]
      t[, c("Variable", "V1")]
  }
}))
dt.smd.all <- data.table(tsmd)[order(-V1)]
datatable(dt.smd.all)
#fwrite(data.table(tsmd), "../results/matching_table_smd.csv")
```

***

#### 1:1 Replace TRUE

```{r}
# set.seed(123456)
# dt.cohort.ana = dt.cohort.ana[sample(1:nrow(dt.cohort.ana), replace = FALSE), ]
# X <- cbind('sexe' = dt.cohort.ana$sexe_n
#            , 'edat' = dt.cohort.ana$edat_20231001
#            , 'rural' = dt.cohort.ana$rural_n
#            , 'espanyol' = dt.cohort.ana$espanyol_n
#            #, 'aquas' = dt.cohort.ana$aquas
#            ,'aquas' = dt.cohort.ana$aquas_q4_n
#            #,'dbs' = dt.cohort.ana$dbs
#            )
```

```{r}
ini <- Sys.time()
matching <- Match(Y = NULL, 
                  Tr = dt.cohort.ana$nirse_n,
                  X = X, 
                  exact = c(T, F, T, T, T),
                  replace = T,
                  M = 1,
                  caliper = 0.05) # 0,20 = 1/5
fi <- Sys.time()
fi - ini
summary(matching)
#str(matching)

a <- data.table(matching$index.treated, matching$index.control)
uniqueN(a$V1)
uniqueN(a$V2)
```

```{r}
#a
# setnames(a, 'V2', 'var_nrow_treated')
# setnames(a, 'V1', 'var_nrow_control')
# #a
# dt.cohort.ana[, var_nrow := 1:.N]
# 
# dt.casos <- merge(dt.cohort.ana[, .(var_nrow, hash, nirse, edat_20231001)],
#                   a,
#                   by.x = 'var_nrow',
#                   by.y = 'var_nrow_treated'
#                   )
# dt.casos
# setnames(dt.casos, 'var_nrow', 'match')
# dt.casos[match == 2,]
# dt.casos[, var_nrow_control := NULL]
# dt.casos <- unique(dt.casos)
# 
# 
# dt.controles <- merge(dt.cohort.ana[, .(var_nrow, hash, nirse, edat_20231001)],
#                       a,
#                       by.x = 'var_nrow',
#                       by.y = 'var_nrow_control',
#                       all.y = TRUE)
# dt.controles
# setnames(dt.controles, 'var_nrow_treated', 'match')
# dt.controles[match == 2,]
# dt.controles[, var_nrow := NULL]
# dt.controles
# uniqueN(dt.controles[, match])
#         
# dt.matching <- rbind(dt.casos,
#                      dt.controles)
# dt.matching[match == 2,]
# hash.list <- unique(dt.matching[, hash])
# 
# a <- dt.matching[, .N, hash]
# dt.matching[hash == 'FC865741A13A972CB7788BA8140D523A2D706DF6',]
# 
# 
# 
# dt.cohort.ana.wide <- dt.matching[, .(match, nirse, edat_20231001)]
# dt.cohort.ana.wide1 <- dcast(dt.cohort.ana.wide, match ~ nirse, value.var = "edat_20231001", fun.aggregate = mean)
# dt.cohort.ana.wide1[, edat_diff := `No Nirsevimab` - `Nirsevimab`]
# summary(dt.cohort.ana.wide1[, edat_diff])
# dt.matching[match == 5175, ]
# 
# 
# 
# dt.borrar <- dt.cohort.ana[hash %in% c('FA5C1583673F6B473F00E651D8DBBE9E6B6A9421',
#                                        'A68D4F415B6E9545BEC5413FBA9537D1ABA03AB5',
#                                        'FDE0A43683A97E80BC36FC2B6E305EFF64A0BF3B')]
# 
# # Pacients Nirsevimab no matching
#   dt.nirse.drop <- dt.cohort.ana[!(hash %in% hash.list),][, .(hash, rural, aquas_q4_f, sexe, edat_20231001, espanyol)]
#   dt.nirse.drop
#   
#   #295CF4381706BF5DB6336F125751AAB578618F0A
#   dt.cohort.ana[rural == 'Rural' & aquas_q4_f == '1' & sexe == 'Home' & edat_20231001 == 4 & espanyol == "Sí",] # Todos Nirsemivab
#   dt.borrar <- dt.cohort.ana[rural == 'Rural' & aquas_q4_f == '1' & sexe == 'Home' & espanyol == "Sí",] # 
# 
# 
# setnames(dt.cohort.ana, c('match.m1', 'match.m1_c'), c('match_f1005', 'match_f1005_c'))
# 
# saveRDS(dt.cohort.ana,
#         paste0("../data/dt.cohort.matching_f1005.rds"))
```

```{r}
cols <- c("nirse",
          "rural",
          "medea_c2",
          "aquas", "aquas_q4_f",
          "edat_20231001", "sexe",
          "espanyol",
          "dbs")
df <- dt.cohort.ana[match_f1005_c == 1,
                    .SD,
                    .SDcols = cols]
cG <- compareGroups(nirse ~ .,
                    df, 
                    method = 1,
                    max.xlev = 20)
cT <- createTable(cG,
                  show.descr = TRUE,
                  show.n = TRUE,
                  show.all = FALSE,
                  hide.no = c("Dona", "No", "Urbà"),
                  show.ratio = FALSE)
export2md(cT
          , caption = "Comparació Nirsevimab (Matching)"
          , format = "html")
#export2word(cT,
#            "../results/matching_table_1.docx")
```

```{r matching smd}
dt.smd <- dt.cohort.ana[match_f1005_c == 1,]

tsmd <- do.call("rbind", 
                 lapply(c("rural",
                          "medea_c2",
                          "aquas", "aquas_q4_f",
                          "edat_20231001",
                          "sexe",
                          "espanyol",
                          "dbs"
                          ),
                          function(x){
                     d <- dt.smd[, .SD, .SDcols = c("nirse_n", x)]
    if (is.integer(dt.smd[, get(x)]) | is.numeric(dt.smd[, get(x)])) {
      smd <- tryCatch(stddiff.numeric(data = d, gcol = 1, vcol = 2)[,"stddiff"], error = function(e) NA)
      t <- data.table(V1 = c(smd))
      t[, Variable := x]
      t[, c("Variable", "V1")]
  } 
  else {
    smd <- tryCatch(stddiff.category(data = d, gcol = 1, vcol = 2)[1,"stddiff"], error = function(e) NA)
      t <- data.table(V1 = c(smd))
      t[, Variable := x]
      t[, c("Variable", "V1")]
  }
}))
dt.smd.all <- data.table(tsmd)[order(-V1)]
datatable(dt.smd.all)
#fwrite(data.table(tsmd), "../results/matching_table_smd.csv")
```

***

#### 1:n

```{r}
dt.cohort.ana = dt.cohort.ana[sample(1:nrow(dt.cohort.ana), replace = FALSE), ]

X <- cbind('sexe' = dt.cohort.ana$sexe_n
           , 'edat' = dt.cohort.ana$edat_20231001
           , 'rural' = dt.cohort.ana$rural_n
           , 'espanyol' = dt.cohort.ana$espanyol_n
           #, 'aquas' = dt.cohort.ana$aquas
           ,'aquas' = dt.cohort.ana$aquas_q4_n
           #,'dbs' = dt.cohort.ana$dbs
           )
```

```{r}
dt.cohort.ana[nirse_n == 0, nirse_n_inv := 1][nirse_n == 1, nirse_n_inv := 0]
dt.cohort.ana[, .N,  .(nirse_n,  nirse_n_inv)]

set.seed(123456)
ini <- Sys.time()
matching <- Match(Y = NULL, 
                  Tr = dt.cohort.ana$nirse_n_inv,
                  X = X, 
                  exact = c(T, F, T, T, T),
                  replace = T,
                  M = 20,
                  caliper = 0.1) # 0,20 = 1/5
fi <- Sys.time()
fi - ini
summary(matching)
#str(matching)

a <- data.table(matching$index.treated, matching$index.control)
uniqueN(a$V1)
uniqueN(a$V2)
```


```{r}
a
setnames(a, 'V2', 'var_nrow_treated')
setnames(a, 'V1', 'var_nrow_control')
a
dt.cohort.ana[, var_nrow := 1:.N]

dt.casos <- merge(dt.cohort.ana[, .(var_nrow, hash, nirse, edat_20231001)],
                  a,
                  by.x = 'var_nrow',
                  by.y = 'var_nrow_treated'
                  )
dt.casos
setnames(dt.casos, 'var_nrow', 'match')
dt.casos[match == 2,]
dt.casos[, var_nrow_control := NULL]
dt.casos <- unique(dt.casos)


dt.controles <- merge(dt.cohort.ana[, .(var_nrow, hash, nirse, edat_20231001)],
                      a,
                      by.x = 'var_nrow',
                      by.y = 'var_nrow_control',
                      all.y = TRUE)
dt.controles
setnames(dt.controles, 'var_nrow_treated', 'match')
dt.controles[match == 2,]
dt.controles[, var_nrow := NULL]
dt.controles
uniqueN(dt.controles[, match])
        
dt.matching <- rbind(dt.casos,
                     dt.controles)
dt.matching[match == 2,]
hash.list <- unique(dt.matching[, hash])

a <- dt.matching[, .N, hash]
dt.matching[hash == 'FC865741A13A972CB7788BA8140D523A2D706DF6',]



dt.cohort.ana.wide <- dt.matching[, .(match, nirse, edat_20231001)]
dt.cohort.ana.wide1 <- dcast(dt.cohort.ana.wide, match ~ nirse, value.var = "edat_20231001", fun.aggregate = mean)
dt.cohort.ana.wide1[, edat_diff := `No Nirsevimab` - `Nirsevimab`]
summary(dt.cohort.ana.wide1[, edat_diff])
dt.matching[match == 5175, ]



dt.borrar <- dt.cohort.ana[hash %in% c('FA5C1583673F6B473F00E651D8DBBE9E6B6A9421',
                                       'A68D4F415B6E9545BEC5413FBA9537D1ABA03AB5',
                                       'FDE0A43683A97E80BC36FC2B6E305EFF64A0BF3B')]

# Pacients Nirsevimab no matching
  dt.nirse.drop <- dt.cohort.ana[!(hash %in% hash.list),][, .(hash, rural, aquas_q4_f, sexe, edat_20231001, espanyol)]
  dt.nirse.drop
  
  #295CF4381706BF5DB6336F125751AAB578618F0A
  dt.cohort.ana[rural == 'Rural' & aquas_q4_f == '1' & sexe == 'Home' & edat_20231001 == 4 & espanyol == "Sí",] # Todos Nirsemivab
  dt.borrar <- dt.cohort.ana[rural == 'Rural' & aquas_q4_f == '1' & sexe == 'Home' & espanyol == "Sí",] # 
```

```{r}
dt.matching[match == 2,]
dt.matching[, .N, nirse]
```


***

### Library MatchIt

#### Propensity Score

```{r}
# dt.cohort.ana.ps = dt.cohort.ana.ps[sample(1:nrow(dt.cohort.ana), replace = FALSE), ]
# m.ps.out <- matchit(as.factor(nirse_n) ~ I(edat_20231001^2) + sexe + rural + aquas + espanyol,
#                     data = dt.cohort.ana.ps,
#                     method = "nearest",
#                     distance = "glm",
#                     caliper = 0.02,
#                     ratio = 1)
# summary(m.ps.out)
# plot(m.ps.out, type = "jitter", interactive = FALSE)
# plot(summary(m.ps.out), abs = FALSE)
# 
# 
# fn_bal <- function(dta, variable) {
#   dta$variable <- dta[, variable]
#   if (variable == 'w3income') dta$variable <- dta$variable / 10^3
#   dta$catholic <- as.factor(dta$catholic)
#   support <- c(min(dta$variable), max(dta$variable))
#   ggplot(dta, aes(x = distance, y = variable, color = catholic)) +
#     geom_point(alpha = 0.2, size = 1.3) +
#     geom_smooth(method = "loess", se = F) +
#     xlab("Propensity score") +
#     ylab(variable) +
#     theme_bw() +
#     ylim(support)
# }
# 
# library(gridExtra)
# grid.arrange(
#    fn_bal(dta_m, "w3income"),
#    fn_bal(dta_m, "p5numpla") + theme(legend.position = "none"),
#    fn_bal(dta_m, "p5hmage"),
#    fn_bal(dta_m, "w3momed_hsb") + theme(legend.position = "none"),
#    fn_bal(dta_m, "race_white"),
#    nrow = 3, widths = c(1, 0.8)
# )
```

#### No Propensity Score

```{r}
# # No hace falta seed (siempre es el mismo resultado)
# dt.cohort.ana = dt.cohort.ana[sample(1:nrow(dt.cohort.ana), replace = FALSE), ]
# m.out <- matchit(as.factor(nirse_n) ~ edat_20231001 + sexe_n + medea_c2_n + espanyol,
#                  data = dt.cohort.ana,
#                  exact = c("sexe_n", "medea_c2_n", "espanyol"),
#                  method = "nearest",
#                  caliper = 0.02,
#                  ratio = 1)
# summary(m.out)
# data_match1 <- match.data(m.out)
# plot(m.out, type = "jitter", interactive = FALSE)
# plot(summary(m.out), abs = FALSE)
```

```{r}
# dt.cohort.ana.wide <- data_match1[, .(subclass, nirse, edat_20231001)]
# dt.cohort.ana.wide1 <- dcast(dt.cohort.ana.wide, subclass ~ nirse, value.var = "edat_20231001")
# dt.cohort.ana.wide1[, edat_diff := `No Nirsevimab` - `Nirsevimab`]
# summary(dt.cohort.ana.wide1[, edat_diff])
```


















```{r}
# ini <- Sys.time()
# saveRDS(dt.cohort.ana,
#         paste0("../data/dt.cohort.matching.rds"))
# fi <- Sys.time()
# fi - ini
```

```{r}
gc.var <- gc()
```


