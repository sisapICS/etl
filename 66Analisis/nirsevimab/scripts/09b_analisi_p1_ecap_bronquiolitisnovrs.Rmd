---
title: "NIRSEVIMAB -  Bronquiolitis VRS (comunitària)"
subtitle: "Període 2023/04/01 - 2023/09/30"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

```{r}
inici <- Sys.time()
```

```{r}
# Data Manager
  suppressWarnings(suppressPackageStartupMessages(library('Hmisc')))
  suppressWarnings(suppressPackageStartupMessages(library('data.table')))
  
  suppressWarnings(suppressPackageStartupMessages(library('tidyverse')))
  
  suppressWarnings(suppressPackageStartupMessages(library('tibble')))
  suppressWarnings(suppressPackageStartupMessages(library('plyr')))
  suppressWarnings(suppressPackageStartupMessages(library('dplyr')))
  suppressWarnings(suppressPackageStartupMessages(library('tidyr')))
  suppressWarnings(suppressPackageStartupMessages(library('forcats')))
  suppressWarnings(suppressPackageStartupMessages(library('encode')))

# Matching
  suppressWarnings(suppressPackageStartupMessages(library('Matching')))
  
# Anàlisi
  suppressWarnings(suppressPackageStartupMessages(library('compareGroups')))
  suppressWarnings(suppressPackageStartupMessages(library('stddiff')))
#   #suppressWarnings(suppressPackageStartupMessages(library('nlme')))
#   #suppressWarnings(suppressPackageStartupMessages(library('statmod')))
  suppressWarnings(suppressPackageStartupMessages(library('epitools')))
#   

# Modelització
  suppressWarnings(suppressPackageStartupMessages(library('survival')))
#     suppressWarnings(suppressPackageStartupMessages(library('lme4')))
#     suppressWarnings(suppressPackageStartupMessages(library('MASS'))) # Binomial Negative
#     suppressWarnings(suppressPackageStartupMessages(library('pscl'))) # Zero-Inflated Regression
#     suppressWarnings(suppressPackageStartupMessages(library('sandwich')))
#     suppressWarnings(suppressPackageStartupMessages(library('gtsummary')))
      
# # Figures
    suppressWarnings(suppressPackageStartupMessages(library('ggplot2'))) 
#   suppressWarnings(suppressPackageStartupMessages(library('scales')))
#   #suppressWarnings(suppressPackageStartupMessages(library('splines')))
#   #suppressWarnings(suppressPackageStartupMessages(library('gridExtra')))
    suppressWarnings(suppressPackageStartupMessages(library('survminer')))
# 
# # Taules
    suppressWarnings(suppressPackageStartupMessages(library('DT')))
```

***
***

## Data Manager

```{r}
#dt.urgencies.dm <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/data/dt.urgencies.dm.rds")
dt.diagnostics.ecap.dm.wide <- readRDS("../data/dt.diagnostics.ecap.dm.wide.rds")
a <- dt.diagnostics.ecap.dm.wide[, .N, .(bronquiolitis, bronquiolitis_vrs, bronquiolitis_novrs)]

dt.cohort.flt <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/data/dt.cohort.flt.rds")
dt.cohort.ana <- dt.cohort.flt[dbs == 1,]
```

```{r}
data_inici <- as.Date('2023-10-01')
data_fi <- as.Date('2024-01-31')
```

```{r}
# Selecció de columnes
  dt.cohort.ana <- dt.cohort.ana[, .(hash, data_defuncio, data_nirse, rural, medea_c2, sexe, edat_20231001, edat_20231001_c, espanyol)]
```

```{r}
# Expandir los pacientes expuestos
  dt.cohort.ana <- dt.cohort.ana[, nirse_n := 0]
  dt.temp <- dt.cohort.ana[!is.na(data_nirse),]
  dt.temp <- dt.temp[, nirse_n := 1]

  dt.cohort.ana <- rbind(dt.cohort.ana,
                         dt.temp,
                         fill = TRUE) # 24.955 > 47.351

  dt.cohort.ana[hash == '0002533174E7C2CF8BD912DC18CC97017C2A212E',] # Pacient immunitzat
  dt.cohort.ana[hash == '000804B794CC0644FF43699D224093CB33944AA8',] # Pacient immunitzat
```

```{r}
# Set data_entry and data_cens
  # Data Entry
    dt.cohort.ana[nirse_n == 0, data_entry := data_inici]
    dt.cohort.ana[nirse_n == 1, data_entry := data_nirse]
    dt.cohort.ana[hash == '0002533174E7C2CF8BD912DC18CC97017C2A212E',] # Pacient immunitzat
    dt.cohort.ana[hash == '000804B794CC0644FF43699D224093CB33944AA8',] # Pacient immunitzat
    
  # Data Cens
    dt.cohort.ana[is.na(data_nirse) & nirse_n == 0, data_cens := data_fi]
    dt.cohort.ana[!is.na(data_nirse) & nirse_n == 0, data_cens := data_nirse - 1]
    
    dt.cohort.ana[nirse_n == 1, data_cens := data_fi]
    dt.cohort.ana[hash == '0002533174E7C2CF8BD912DC18CC97017C2A212E',] # Pacient immunitzat
    dt.cohort.ana[hash == '000804B794CC0644FF43699D224093CB33944AA8',] # Pacient immunitzat

    
    # Elimnar si data_cens < data_entry
    a <- dt.cohort.ana[data_cens < data_entry,] # N=1 immunització el dia de l'inici de l'estudi 2023-10-01
    dt.cohort.ana <- dt.cohort.ana[data_cens >= data_entry,] # 47.351 > 47.350

  # Añadir otras posibles censuras (exitus, etc ..)
```


```{r}
# Event
    dt.ecap.bronquiolitis <- dt.diagnostics.ecap.dm.wide[bronquiolitis_novrs == 1, .(hash, bronquiolitis_novrs, data_dx)]
    dt.ecap.bronquiolitis[hash == '943EFDD874E4884E725A643B85463ED371812EC4',]
    dt.ecap.bronquiolitis <- dt.ecap.bronquiolitis[order(hash, data_dx)]
    dt.ecap.bronquiolitis <- dt.ecap.bronquiolitis[, .SD[1], hash]
    dt.ecap.bronquiolitis[hash == '943EFDD874E4884E725A643B85463ED371812EC4',] # Pacient amb > 1 bronuiolitis
    
    dt.cohort.ana <- merge(dt.cohort.ana,
                           dt.ecap.bronquiolitis,
                           by = 'hash',
                           all.x = TRUE) # 49.507
    
  # 
    dt.cohort.ana <- dt.cohort.ana[bronquiolitis_novrs == 1, data_event := data_dx]
    dt.cohort.ana[hash == '000000C8566EAF77D9A2C5697D08301BD430FFC3',] # Paciente no Vacunado sin bronquiolitis
    dt.cohort.ana[hash == '00623325762E732D036607F1B057D5B4015B87DF',] # Paciente no Vacunado con bronquiolitis
    dt.cohort.ana[hash == '000804B794CC0644FF43699D224093CB33944AA8',] # Paciente Vacunado sin bronquiolitis
    dt.cohort.ana[hash == '0065E9D7094D1E775F857C357086F984433EA9BE',] # Paciente vacunado con Bronquiolitis después vacuna  
    dt.cohort.ana[hash == '011DD8EBCB00FC908A2E86831E2AE4CD2CA569DF',] # Paciente vacunado con Bronquiolitis antes vacuna  
  
    dt.cohort.ana <- dt.cohort.ana[, data_cens := pmin(data_cens, data_event, data_defuncio, na.rm = TRUE)]

    
  # Establir l'event
    dt.cohort.ana[, bronquiolitis_model := 0][data_cens == data_event, bronquiolitis_model := 1]
    dt.cohort.ana[hash == '000000C8566EAF77D9A2C5697D08301BD430FFC3',] # Paciente no Vacunado sin bronquiolitis
    dt.cohort.ana[hash == '00623325762E732D036607F1B057D5B4015B87DF',] # Paciente no Vacunado con bronquiolitis
    dt.cohort.ana[hash == '000804B794CC0644FF43699D224093CB33944AA8',] # Paciente Vacunado sin bronquiolitis
    dt.cohort.ana[hash == '002F7B7530CA48AD4E100A4883627E3EA55577E9',] # Paciente vacunado con Bronquiolitis después vacuna  
    dt.cohort.ana[hash == '37768DF96FA865EE586B34A63B172F1DF05DDA1A',] # Paciente vacunado con Bronquiolitis antes vacuna  

  # Eliminar los registros de pacientes vacunados con el event antes de la inmunització
    dt.cohort.ana <- dt.cohort.ana[data_cens >= data_entry,] # 49.507 > 49.361
    dt.cohort.ana[hash == '011DD8EBCB00FC908A2E86831E2AE4CD2CA569DF',] # Paciente vacunado con Bronquiolitis antes vacuna  
    
  # Eliminar las fechas de vacunación de los registros iniciales de cada paciente
    dt.cohort.ana[nirse_n == 0, data_nirse := NA]
    dt.cohort.ana[,nirse_model := 0][!is.na(data_nirse), nirse_model := 1]
    dt.cohort.ana[hash == '000000C8566EAF77D9A2C5697D08301BD430FFC3',] # Paciente no Vacunado sin bronquiolitis
    dt.cohort.ana[hash == '00623325762E732D036607F1B057D5B4015B87DF',] # Paciente no Vacunado con bronquiolitis
    dt.cohort.ana[hash == '000804B794CC0644FF43699D224093CB33944AA8',] # Paciente Vacunado sin bronquiolitis
    dt.cohort.ana[hash == '002F7B7530CA48AD4E100A4883627E3EA55577E9',] # Paciente vacunado con Bronquiolitis después vacuna  
    dt.cohort.ana[hash == '37768DF96FA865EE586B34A63B172F1DF05DDA1A',] # Paciente vacunado con Bronquiolitis antes vacuna  
    
    dt.cohort.ana <- dt.cohort.ana[, .(hash, nirse_model, data_entry, data_cens, bronquiolitis_model,
                                       rural, medea_c2, sexe, edat_20231001, edat_20231001_c, espanyol)]
```

```{r}
# Exportar arxiu a txt per ferr proves amb STATA
fwrite(dt.cohort.ana, "../data/p1_ecap_bronquiolitisnovrs.txt", sep = '@')
```

***
***

# Poisson Regression

## Naive

```{r}

dt.cohort.ana[, logfu := log(as.numeric(data_cens - data_entry))]
dt.cohort.ana[logfu == -Inf, logfu := NA]

m.p.naive <- glm(bronquiolitis_model ~ nirse_model + offset(logfu),
             dt.cohort.ana,
             family = poisson(link = "log"))
summary(m.p.naive)
exp(cbind(coef(m.p.naive), confint(m.p.naive)))
```

# Cox Regression

## Time-since-nirsevimab scale

```{r}
dt.cohort.ana[, temps := as.numeric(data_cens) - as.numeric(data_entry)]
surv_obj <- with(dt.cohort.ana, Surv(temps,
                                     event = bronquiolitis_model))
modelo_cox <- coxph(surv_obj ~ nirse_model, data = dt.cohort.ana)
summary(modelo_cox)
```

## Calendar-time scale

```{r}
surv_obj <- with(dt.cohort.ana, Surv(as.numeric(data_entry), as.numeric(data_cens),
                                     event = bronquiolitis_model))
modelo_cox <- coxph(surv_obj ~ nirse_model, data = dt.cohort.ana)
summary(modelo_cox)
# n= 49057, number of events= 2151 (169 observations deleted due to missingness) >>> temps == 0 data_entry == data_cens
```


```{r}
dt.cohort.ana[, medea_c2 := factor(medea_c2, levels = c('1U','2U','3U','4U','Rural'))]
surv_obj <- with(dt.cohort.ana, Surv(as.numeric(data_entry), as.numeric(data_cens),
                                     event = bronquiolitis_model))
modelo_cox <- coxph(surv_obj ~ nirse_model
                               #+ medea_c2
                               + edat_20231001
                               #+ pspline(edat_20231001)
                               #+ sexe
                               + espanyol,
                    data = dt.cohort.ana)
summary(modelo_cox)
```

## Kaplan-Meier

```{r}
surv_obj_fit <- survfit(Surv(as.numeric(data_entry) - as.numeric(data_entry), as.numeric(data_cens) - as.numeric(data_entry), event = bronquiolitis_model) ~ nirse_model,
                        data = dt.cohort.ana)

surv_obj_fit <- survfit(Surv(temps , event = bronquiolitis_model) ~ nirse_model,
                        data = dt.cohort.ana)

ggsurvplot(surv_obj_fit,
           conf.int = TRUE,
           ylim = c(.85, 1),
           xlim = c(0, 120),
           break.time.by = 15,
           palette = "grey",
           ggtheme = theme_classic(),
           legend.labs = c("Control", "Nirsevimab"),
           title = "Bronquiolitis no VRS (Comunitària)",
           subtitle = "Nascuts Abril-Setembre",
           legend = "bottom"
           #, risk.table = TRUE
           ) +
           labs(caption = "Elaboració: Sistemes d'informació dels Serveis d'Atenció Primària (SISAP)")

ggsurvplot(surv_obj_fit,
           fun = "event",
           xlim = c(0, 120),
           break.time.by = 15,
           palette = "grey",
           ggtheme = theme_classic(),
           legend.title = "",
           legend.labs = c("Control", "Nirsevimab"),
           legend = "bottom",
           title = "Bronquiolitis no VRS (Comunitària)",
           subtitle = "Nascuts Abril-Setembre"
           ) +
           labs(caption = "Elaboració: Sistemes d'informació dels Serveis d'Atenció Primària (SISAP)")
ggsave("../resuls/bronqiolitisnovrs_ecap.png", width = 6, height = 4)

dt.cohort.ana[data_cens < as.Date('2024-01-01', '%Y-%m-%d') & bronquiolitis_model == 1, .N, data_cens][order(data_cens)]
# ,  size = 1,  # change line size
#            linetype = "strata", # change line type by groups
#            break.time.by = 2, # break time axis by 250
#            
#             # Add confidence interval
#            pval = TRUE, # Add p-value
# 
#            # 
# 
# 
#            #, censor = TRUE
#            )
```
