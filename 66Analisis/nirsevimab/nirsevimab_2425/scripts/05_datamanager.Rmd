
# Data Manager `r if (params$eval.tabset) '{.tabset}' else ''`

## Catàleg `r if (params$eval.tabset) '{.tabset}' else ''`

### Catèleg de Centres `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
dt.cat_centres.dm <- dt.cat_centres.raw
```

#### Medea `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
dt.cat_centres.dm[, medea_c := factor(medea_c,
                                      levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"))]

dt.cat_centres.dm[medea_c %in% c("0R", "1R", "2R"), rural := "Rural"]
dt.cat_centres.dm[medea_c %in% c("1U", "2U", "3U", "4U"), rural := "Urbà"]
dt.cat_centres.dm[, rural := factor(rural,
                                         levels = c("Rural", "Urbà"),
                                         labels = c("Rural", "Urbà"))]

dt.cat_centres.dm[, medea_c2 := medea_c]
dt.cat_centres.dm[medea_c %in% c("0R", "1R", "2R"), medea_c2 := "Rural"]
dt.cat_centres.dm[, medea_c2 := factor(medea_c2,
                                       levels = c("Rural", "1U", "2U", "3U", "4U"),
                                       labels = c("Rural", "1U", "2U", "3U", "4U"))]
```

***
***

## Dades `r if (params$eval.tabset) '{.tabset}' else ''`

### cohort `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
dt.cohort.dm <- dt.cohort.raw

# Mes Naixement
  dt.cohort.dm[, month_naixement := month(data_naixement)]
  dt.cohort.dm[, month_naixement_f := factor(month_naixement,
                                             levels = c(1:12),
                                             labels = c("January",
                                                        "February",
                                                        "March",
                                                        "April",
                                                        "May",
                                                        "June",
                                                        "July",
                                                        "August",
                                                        "September",
                                                        "October",
                                                        "November",
                                                        "December"))]
  dt.cohort.dm[, .N, month_naixement_f][order(month_naixement_f)]
               
# Edat
  dt.cohort.dm[, edat_20240331 :=  as.numeric(as.Date('2025-03-31') - data_naixement)]
  dt.cohort.dm[, edat_20240331_c := cut(edat_20240331,
                                        quantile(edat_20240331,
                                                 probs = c(0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100) / 100),
                                        include.lowest = TRUE)]
  
  dt.cohort.dm[, edat_20231001 :=  as.numeric(as.Date('2024-10-01') - data_naixement)]
  dt.cohort.dm[, edat_20231001_c := cut(edat_20231001, 
                                        quantile(edat_20231001,
                                                 probs = c(0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100) / 100),
                                        include.lowest = TRUE)]
# Defunció
  dt.cohort.dm[, defuncio := 0]
  dt.cohort.dm[!is.na(data_defuncio), defuncio := 1]
  dt.cohort.dm[, defuncio := factor(defuncio, levels = c(0,1), labels = c('No', 'Sí'))]
  dt.cohort.dm[, .N, defuncio]
  
# Exitus
  dt.cohort.dm[, exitus := 0]
  dt.cohort.dm[situacio == 'D', exitus := 1]
  dt.cohort.dm[, exitus := factor(exitus, levels = c(0,1), labels = c('No', 'Sí'))]
  dt.cohort.dm[, .N, exitus]
  dt.cohort.dm[situacio == 'D', data_exitus := data_situacio]
  dt.cohort.dm[, .N, data_exitus]
  
  dt.cohort.dm[, edat_exitus := as.numeric(data_exitus - data_naixement)]
  dt.cohort.dm[, .N, edat_exitus][order(edat_exitus)]
  
# Nacionalitat
  dt.cohort.dm[, .N, codi_nacionalitat]
  dt.cohort.dm[is.na(codi_nacionalitat), codi_nacionalitat := 724]
  dt.cohort.dm[codi_nacionalitat == 728, codi_nacionalitat := 724]
  
  # Espanyol
    dt.cohort.dm[, espanyol := 0]
    dt.cohort.dm[codi_nacionalitat == 724, espanyol := 1]
    dt.cohort.dm[, espanyol := factor(espanyol, levels = c(0,1), labels = c('No', 'Sí'))]
    dt.cohort.dm[, .N, espanyol]
  
  # Nacionalitat descripció
    dt.cohort.dm <- merge(dt.cohort.dm,
                          dt.cat_nacionalitat.raw[, .(codi_nac, desc_nac)],
                          by.x = 'codi_nacionalitat',
                          by.y = 'codi_nac',
                          all.x =  TRUE)
    dt.cohort.dm[, .N, desc_nac][order(-N)]
    
  # Regió
    dt.cohort.dm <- merge(dt.cohort.dm,
                          dt.cat_nacionalitat.raw[, .(codi_nac, regio_desc)],
                          by.x = 'codi_nacionalitat',
                          by.y = 'codi_nac',
                          all.x =  TRUE)
    dt.cohort.dm[, .N, regio_desc][order(-N)]


# Nirsevimab
    
  # Nirsevimab
    dt.cohort.dm[, nirse := factor(nirse, levels = c(0,1), labels = c('No Nirsevimab', 'Nirsevimab'))]
    
  # Edat
    dt.cohort.dm[, edat_nirse := as.numeric(data_nirse - data_naixement)]
    dt.cohort.dm[, .N, edat_nirse][order(edat_nirse)]

# Període
  dt.cohort.dm[, periode_estudi := 'Periode 1']
  dt.cohort.dm[data_naixement >= as.Date('2024-10-01'), periode_estudi := 'Periode 2']
  dt.cohort.dm[, periode_estudi := factor(periode_estudi, levels = c('Periode 1', 'Periode 2'))]
  dt.cohort.dm[, .N, periode_estudi]
  a <- dt.cohort.dm[periode_estudi == 'Periode 1', .N, data_nirse][order(data_nirse)]
```

```{r}
dt.cohort.dm[periode_estudi == 'Periode 1', .N, nirse]
dt.cohort.dm[periode_estudi == 'Periode 1' & data_nirse >= as.Date('2025-02-01', '%Y-%m-%d'), nirse := "No Nirsevimab"]
dt.cohort.dm[periode_estudi == 'Periode 1' & data_nirse >= as.Date('2025-02-01', '%Y-%m-%d'), edat_nirse := NA]
dt.cohort.dm[periode_estudi == 'Periode 1' & data_nirse >= as.Date('2025-02-01', '%Y-%m-%d'), producte_nirse := NA]
dt.cohort.dm[periode_estudi == 'Periode 1' & data_nirse >= as.Date('2025-02-01', '%Y-%m-%d'), data_nirse := NA]
dt.cohort.dm[periode_estudi == 'Periode 1', .N, nirse]
```

***

### diagnòstics ecap `r if (params$eval.tabset) '{.tabset}' else ''`

#### trasposar la informació `r if (params$eval.tabset) '{.tabset}' else ''`

Es transformen les dades de format long a wide per aconseguir una columna per a cada diagnòstic

```{r}
dt.diagnostics.ecap.dm <- dt.diagnostics.ecap.raw
dt.diagnostics.ecap.dm[, a := 1]

dt.diagnostics.ecap.dm.wide <- dcast(dt.diagnostics.ecap.dm, hash + data_dx ~ dx, value.var = "a")

dt.diagnostics.ecap.dm.wide[is.na(dt.diagnostics.ecap.dm.wide)] <- 0


setnames(dt.diagnostics.ecap.dm.wide, c('Bronquiolitis', 'Bronquiolitis VRS', 'Pneumònia', 'Pneumònia vírica'),
                                      c('bronquiolitis', 'bronquiolitis_vrs', 'pneumonia', 'pneumonia_virica'))

dt.diagnostics.ecap.dm.wide[bronquiolitis_vrs == 1, bronquiolitis := 1]

dt.diagnostics.ecap.dm.wide[, bronquiolitis_novrs := 0]
dt.diagnostics.ecap.dm.wide[bronquiolitis == 1 & bronquiolitis_vrs == 0, bronquiolitis_novrs := 1]
dt.diagnostics.ecap.dm.wide[, .N, .(bronquiolitis, bronquiolitis_vrs, bronquiolitis_novrs)]
```

***

### multitets `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
dt.multitests.dm <- dt.multitests.raw
```

***

### urgencies `r if (params$eval.tabset) '{.tabset}' else ''`

#### numero urgències `r if (params$eval.tabset) '{.tabset}' else ''`

Aconseguir el número urgències

```{r}
dt.urgencies.dm <- dt.urgencies.raw

dt.urgencies.dm[, urgencies := 1]

dt.urgencies.n <- dt.urgencies.dm[, .(urgencies_n = .N), .(hash)]
```

#### darrera urgència `r if (params$eval.tabset) '{.tabset}' else ''`

Aconseguir la darrera urgència (per a cada outcome)

```{r}
dt.urgencies.dm <- dt.urgencies.raw

dt.urgencies.dm[, urgencies := 1]
dt.urgencies.dm[, bronquiolitis_novrs := 0]
dt.urgencies.dm[bronquiolitis == 1 & bronquiolitisvrs == 0, bronquiolitis_novrs := 1]

#dt.urgencies.dm[, .N, .(tipus_des, subtipus_des)]
dt.urgencies.dm <- dt.urgencies.dm[, .(hash, tipus_cod, data_urgencies, urgencies, bronquiolitis, bronquiolitisvrs, bronquiolitis_novrs, c_s_alta)]
a <- dt.urgencies.dm[, .N, .(tipus_cod, bronquiolitis, bronquiolitis_novrs, bronquiolitisvrs)]
```

```{r}
dt.urgencies.dm.last <- dt.urgencies.dm[, .SD[which.max(data_urgencies)], .(hash)]

dt.urgencies.dm.bronquiolitis <- dt.urgencies.dm[bronquiolitis == 1, ]
dt.urgencies.dm.last.bronquiolitis <- dt.urgencies.dm.bronquiolitis[, .SD[which.max(data_urgencies)], .(hash)]
setnames(dt.urgencies.dm.last.bronquiolitis, c('data_urgencies', 'bronquiolitis'), c('data_bronquiolitis', 'bronquiolitis'))
dt.urgencies.dm.last.bronquiolitis <- dt.urgencies.dm.last.bronquiolitis[, .(hash, data_bronquiolitis, bronquiolitis)]

dt.urgencies.dm.bronquiolitis.amb.ingres <- dt.urgencies.dm[bronquiolitis == 1 & c_s_alta %in% c('21', '22', '23', '24'), ]
dt.urgencies.dm.bronquiolitis.amb.ingres <- dt.urgencies.dm.bronquiolitis.amb.ingres[, .SD[which.max(data_urgencies)], .(hash)]
setnames(dt.urgencies.dm.bronquiolitis.amb.ingres, c('data_urgencies', 'bronquiolitis'), c('data_bronquiolitis.amb.ingres', 'bronquiolitis.amb.ingres'))
dt.urgencies.dm.bronquiolitis.amb.ingres <- dt.urgencies.dm.bronquiolitis.amb.ingres[, .(hash, data_bronquiolitis.amb.ingres, bronquiolitis.amb.ingres)]
```

***

### ingrés hospitalari `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
dt.hospitalitzacions.dm <- dt.hospitalitzacions.raw

dt.hospitalitzacions.dm[, bronquiolitis_novrs := 0]
dt.hospitalitzacions.dm[bronquiolitis == 1 & bronquiolitisvrs == 0, bronquiolitis_novrs := 1]

dt.hospitalitzacions.dm[, bronquiolitis_altres := 0]
dt.hospitalitzacions.dm[bronquiolitisaltres == 1 & bronquiolitisvrs == 0, bronquiolitis_altres := 1]

a <- dt.hospitalitzacions.dm[, .N, .(bronquiolitis, bronquiolitisvrs, bronquiolitis_altres)]
```

***

### controls negatius `r if (params$eval.tabset) '{.tabset}' else ''`

#### trasposar la informació `r if (params$eval.tabset) '{.tabset}' else ''`

Es transformen les dades de format long a wide per aconseguir una columna per a cada diagnòstic

```{r}
dt.controls_neg.ecap.dm <- dt.controls_neg.ecap.raw
dt.controls_neg.ecap.dm[, dx_control_neg := tolower(dx_control_neg)]
dt.controls_neg.ecap.dm[, a := 1]

dt.controls_neg.ecap.dm.wide <- dcast(dt.controls_neg.ecap.dm, hash + data_control_neg ~ dx_control_neg, value.var = "a")

dt.controls_neg.ecap.dm.wide[is.na(dt.controls_neg.ecap.dm.wide)] <- 0


setnames(dt.controls_neg.ecap.dm.wide, c('enteritis i diarrees', 'faringoamigdalitis estreptocòccica', 'febre aftosa', 'tos ferina', 'varicel·la'),
                                       c('enteris_diarrea', 'faringo', 'faftosa', 'tosferina', 'varicella'))
#dt.controls_neg.ecap.dm.wide
```

***

### dbs `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
dt.dbs.dm <- dt.dbs.raw

dt.dbs.dm <- dt.dbs.dm[, dbs := 1]
```

***
***

### Afegir informació `r if (params$eval.tabset) '{.tabset}' else ''`

#### catàleg de centres `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
# S'afegit la informació dels catàlegs. 
  dt.cohort.dm <- merge(dt.cohort.dm,
                        dt.cat_centres.dm[, .(scs_codi, ics_desc, eap_ep, amb_desc, 
                                              rural, medea_c, medea_c2, aquas)],
                        by.x = "eap_codi",
                        by.y = "scs_codi",
                        all.x = TRUE)
  a <- dt.cohort.dm[, .N, ics_desc]
  
  # EAP Sardenya
    dt.cohort.dm[eap_codi == '01933', ics_desc := 'EAP Barcelona 7B - Sardenya']
    dt.cohort.dm[eap_codi == '01933', eap_ep := '06076']
    dt.cohort.dm[eap_codi == '01933', amb_desc := "ÀMBIT ALIÉ A L'ICS"]
    
  dt.cohort.dm[, ics := 'No ICS'][eap_ep == '0208', ics := 'ICS'][, ics := factor(ics, levels = c('No ICS', 'ICS'))]
```

#### dbs `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
# S'afegit la informació del dbs
  dt.cohort.dm <- merge(dt.cohort.dm,
                        dt.dbs.dm,
                        by.x = "hash",
                        by.y = "hash",
                        all.x = TRUE)
  dt.cohort.dm[is.na(dbs), dbs := 0]
  dt.cohort.dm[, .N, dbs]
```

```{r}
#names(dt.cohort.dm)
# df <- dt.cohort.dm[periode_estudi == 'Periode 1',]
# res <- compareGroups(dbs ~ nirse + periode_estudi
#                      , df
#                      , max.ylev = 50
#                      , include.miss = TRUE)
# cT <- createTable(res
#                   , show.descr = TRUE
#                   , show.all = TRUE
#                   , show.p.overall = TRUE
#                   , show.n = TRUE
#                   , 
#                   )
# export2md(cT,
#           format = "html",
#           caption = "Comparació Pacients DBS - Taula RESUM")
```

#### Urgències `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
# dt.cohort.dm <- merge(dt.cohort.dm,
#                       dt.urgencies.n,
#                       by = 'hash',
#                       all.x = TRUE)
# dt.cohort.dm[is.na(urgencies_n), urgencies_n := 0]
# dt.cohort.dm[, .N, urgencies_n][order(-N)]
```

```{r, eval = FALSE}
dt.cohort.dm <- merge(dt.cohort.dm,
                      dt.urgencies.dm.last.bronquiolitis,
                      by = 'hash',
                      all.x = TRUE)
dt.cohort.dm[is.na(bronquiolitis), bronquiolitis := 0]
dt.cohort.dm[, nirse_bronquiolitis := nirse]
dt.cohort.dm[data_nirse > data_bronquiolitis, nirse_bronquiolitis := "No Nirsevimab"]

# dt.cohort.ana <- dt.cohort.dm[, .(hash, nirse_bronquiolitis, bronquiolitis)]
# resultado_fisher <- fisher.test(table(dt.cohort.ana$nirse_bronquiolitis, dt.cohort.ana$bronquiolitis))
# resultado_fisher
# resultado_rr <- riskratio(table(dt.cohort.ana$nirse_bronquiolitis, dt.cohort.ana$bronquiolitis))
# resultado_rr


dt.cohort.dm <- merge(dt.cohort.dm,
                      dt.urgencies.dm.bronquiolitis.amb.ingres,
                      by = 'hash',
                      all.x = TRUE)
dt.cohort.dm[is.na(bronquiolitis.amb.ingres), bronquiolitis.amb.ingres := 0]
dt.cohort.dm[, nirse_bronquiolitis.amb.ingres := nirse]
dt.cohort.dm[data_nirse > data_bronquiolitis.amb.ingres, nirse_bronquiolitis.amb.ingres := "No Nirsevimab"]


# dt.cohort.ana <- dt.cohort.dm[, .(hash, nirse_bronquiolitis.amb.ingres, bronquiolitis.amb.ingres)]
# resultado_fisher <- fisher.test(table(dt.cohort.ana$nirse_bronquiolitis.amb.ingres, dt.cohort.ana$bronquiolitis.amb.ingres))
# resultado_fisher
# resultado_rr <- riskratio(table(dt.cohort.ana$nirse_bronquiolitis.amb.ingres, dt.cohort.ana$bronquiolitis.amb.ingres))
# resultado_rr
# 
# dt.cohort.ana <- dt.cohort.dm[data_naixement < as.Date('2023-10-01'), .(hash, nirse_bronquiolitis.amb.ingres, bronquiolitis.amb.ingres)]
# resultado_fisher <- fisher.test(table(dt.cohort.ana$nirse_bronquiolitis.amb.ingres, dt.cohort.ana$bronquiolitis.amb.ingres))
# resultado_fisher
# resultado_rr <- riskratio(table(dt.cohort.ana$nirse_bronquiolitis.amb.ingres, dt.cohort.ana$bronquiolitis.amb.ingres))
# resultado_rr
```

```{r orden columnas}
dt.cohort.dm <- dt.cohort.dm[, .(hash,
                                 dbs,
                                 abs_codi,
                                 eap_codi, ics_desc,
                                 eap_ep, ics,
                                 amb_desc,
                                 rural, medea_c, medea_c2, aquas,
                                 data_naixement, month_naixement, month_naixement_f,
                                 edat_20231001, edat_20231001_c,
                                 edat_20240331, edat_20240331_c,
                                 sexe,
                                 codi_nacionalitat, espanyol, desc_nac, regio_desc,
                                 situacio, data_situacio, 
                                 defuncio, data_defuncio, 
                                 exitus, data_exitus, edat_exitus, 
                                 periode_estudi,
                                 nirse, data_nirse, edat_nirse, producte_nirse
                                 # ,
                                 # data_bronquiolitis, bronquiolitis,
                                 # nirse_bronquiolitis, nirse_bronquiolitis.amb.ingres, data_bronquiolitis.amb.ingres, bronquiolitis.amb.ingres
                                 )]
```


```{r eval = FALSE}
dt.cohort.df <- dt.cohort.dm[periode_estudi == 'Periode 1',]
dt.cohort.df[, bronquiolitis := factor(bronquiolitis)]
dt.cohort.df[, bronquiolitis.amb.ingres := factor(bronquiolitis.amb.ingres)]
df <- dt.cohort.df
res <- compareGroups(nirse ~ bronquiolitis + bronquiolitis.amb.ingres
                     , df
                     , max.xlev = 100
                     , max.ylev = 5
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = TRUE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "")

dt.cohort.df <- dt.cohort.dm[periode_estudi == 'Periode 2',]
dt.cohort.df[, bronquiolitis := factor(bronquiolitis)]
dt.cohort.df[, bronquiolitis.amb.ingres := factor(bronquiolitis.amb.ingres)]
df <- dt.cohort.df
res <- compareGroups(nirse ~ bronquiolitis + bronquiolitis.amb.ingres
                     , df
                     , max.xlev = 100
                     , max.ylev = 5
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = TRUE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "")

dt.cohort.df <- dt.cohort.dm[periode_estudi == 'Periode 2' &
                             data_naixement >= as.Date('2024-10-01', '%Y-%m-%d') & data_naixement <= as.Date('2024-10-31', '%Y-%m-%d'),]
dt.cohort.df[, bronquiolitis := factor(bronquiolitis)]
dt.cohort.df[, bronquiolitis.amb.ingres := factor(bronquiolitis.amb.ingres)]
df <- dt.cohort.df
res <- compareGroups(nirse ~ bronquiolitis + bronquiolitis.amb.ingres
                     , df
                     , max.xlev = 100
                     , max.ylev = 5
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = TRUE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "")

dt.eap.den <- dt.cohort.dm[, .(den = .N), .(ics_desc, periode_estudi)]
dt.eap.num <- dt.cohort.dm[nirse == 'Nirsevimab', .(num = .N), .(ics_desc, periode_estudi)]
dt.eap <- merge(dt.eap.den,
                dt.eap.num,
                by = c('ics_desc', 'periode_estudi'))
dt.eap[, taxa := round((num/den)*100,2)]
dt.eap.wide <- dcast(dt.eap, ics_desc ~ periode_estudi, value.var = 'taxa')
setnames(dt.eap.wide, c('ics_desc', 'taxa_p1', 'taxa_p2'))
dt.eap.wide.den <- dcast(dt.eap, ics_desc ~ periode_estudi, value.var = 'den')
setnames(dt.eap.wide.den, c('ics_desc', 'den_p1', 'den_p2'))
dt.eap.wide <- merge(dt.eap.wide,
                     dt.eap.wide.den)
dt.eap.wide[, diff := taxa_p2 - taxa_p1]
extremos <- tail(arrange(dt.eap.wide, -diff))
ggplot(dt.eap.wide,
       aes(taxa_p1, taxa_p2)) +
  geom_point() +
  scale_x_continuous(limits = c(0, 100)) +
  scale_y_continuous(limits = c(0, 100)) +
  geom_abline(slope = 1, intercept = 0, color = "red", linetype = "dashed") +
  theme_minimal()
fwrite(dt.eap.wide, "../results/eap_taxa_nirsevimab.txt", sep = '@')
```

```{r data manager gc}
gc.var <- gc()
```

