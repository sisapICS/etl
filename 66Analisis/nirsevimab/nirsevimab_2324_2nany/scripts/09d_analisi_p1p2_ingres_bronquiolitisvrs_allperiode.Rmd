---
title: "NIRSEVIMAB 2023-2024 - 2n Any - Ingrés per Bronquiolitis VRS"
subtitle: "Període 2023/04/01 - 2023/09/30"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

```{r}
inici <- Sys.time()
```

# Library

```{r}
# Data Manager
  suppressWarnings(suppressPackageStartupMessages(library('Hmisc')))
  suppressWarnings(suppressPackageStartupMessages(library('data.table')))
  
  suppressWarnings(suppressPackageStartupMessages(library('tidyverse')))
  
  suppressWarnings(suppressPackageStartupMessages(library('tibble')))
  suppressWarnings(suppressPackageStartupMessages(library('plyr')))
  suppressWarnings(suppressPackageStartupMessages(library('dplyr')))
  suppressWarnings(suppressPackageStartupMessages(library('tidyr')))
  suppressWarnings(suppressPackageStartupMessages(library('forcats')))
  suppressWarnings(suppressPackageStartupMessages(library('encode')))

# Matching
  suppressWarnings(suppressPackageStartupMessages(library('Matching')))
  
# Anàlisi
  suppressWarnings(suppressPackageStartupMessages(library('compareGroups')))
  suppressWarnings(suppressPackageStartupMessages(library('stddiff')))
#   #suppressWarnings(suppressPackageStartupMessages(library('nlme')))
#   #suppressWarnings(suppressPackageStartupMessages(library('statmod')))
  suppressWarnings(suppressPackageStartupMessages(library('epitools')))
#   

# Modelització
  suppressWarnings(suppressPackageStartupMessages(library('survival')))
  suppressWarnings(suppressPackageStartupMessages(library('survminer')))
#     suppressWarnings(suppressPackageStartupMessages(library('lme4')))
#     suppressWarnings(suppressPackageStartupMessages(library('MASS'))) # Binomial Negative
#     suppressWarnings(suppressPackageStartupMessages(library('pscl'))) # Zero-Inflated Regression
#     suppressWarnings(suppressPackageStartupMessages(library('sandwich')))
#     suppressWarnings(suppressPackageStartupMessages(library('gtsummary')))
      
# Figures
  suppressWarnings(suppressPackageStartupMessages(library('ggplot2'))) 
#   suppressWarnings(suppressPackageStartupMessages(library('scales')))
#   #suppressWarnings(suppressPackageStartupMessages(library('splines')))
#   #suppressWarnings(suppressPackageStartupMessages(library('gridExtra')))

# Taules
  suppressWarnings(suppressPackageStartupMessages(library('DT')))
```

# Data Manager

```{r}
dt.hospitalitzacions.dm <- readRDS("../data/dt.hospitalitzacions.dm.rds")

dt.cohort.flt <- readRDS("../data/dt.cohort.flt.rds")
dt.cohort.ana <- dt.cohort.flt
```

```{r}
data_inici_temp2324 <- as.Date('2023-10-01')
data_inici <- as.Date('2024-10-01')
data_fi <- as.Date('2025-01-31')
```

```{r}
# Selecció de columnes
  dt.cohort.ana <- dt.cohort.ana[, .(hash,
                                     data_defuncio,
                                     data_nirse,
                                     rural, medea_c2, 
                                     aquas,
                                     sexe, edat_20241001, edat_20241001_c,
                                     espanyol,
                                     data_naixement, month_naixement_f,
                                     periode_estudi)]
```

```{r}
dt.cohort.ana[, nirse := 0][!is.na(data_nirse), nirse := 1]
dt.cohort.ana[, .N, nirse]

dt.cohort.ana[, nirse_20241001 := 0][data_nirse <= as.Date('2024-10-01'), nirse_20241001 := 1]
dt.cohort.ana[, .N, nirse_20241001]

dt.cohort.ana[, nirse_20240331 := 0][data_nirse <= as.Date('2024-03-31'), nirse_20240331 := 1]
dt.cohort.ana[, .N, nirse_20240331]

dt.cohort.ana[, .N, .(nirse_20240331, nirse_20241001, nirse)][order(nirse_20240331, nirse_20241001, nirse)]
```

## Hospitalització

### Tot el periode

```{r}

dt.hosp.bronquiolitis <- dt.hospitalitzacions.dm[bronquiolitisvrs == 1, .(hash, bronquiolitisvrs, data_ingres)]
hash.val <- dt.hosp.bronquiolitis[,hash][28]
dt.hosp.bronquiolitis[hash == hash.val,]
dt.hosp.bronquiolitis <- dt.hosp.bronquiolitis[order(hash, data_ingres)]
dt.hosp.bronquiolitis <- dt.hosp.bronquiolitis[, .SD[1], hash]
dt.hosp.bronquiolitis[hash == hash.val,]
```

### Pre Temporada VRS

```{r}

dt.hosp.bronquiolitis_ltdatainici <- dt.hospitalitzacions.dm[bronquiolitisvrs == 1 & data_ingres < data_inici, .(hash, bronquiolitisvrs, data_ingres)]
hash.val <- dt.hosp.bronquiolitis_ltdatainici[,hash][28]
dt.hosp.bronquiolitis_ltdatainici[hash == hash.val,]
dt.hosp.bronquiolitis_ltdatainici <- dt.hosp.bronquiolitis_ltdatainici[order(hash, data_ingres)]
dt.hosp.bronquiolitis_ltdatainici <- dt.hosp.bronquiolitis_ltdatainici[, .SD[1], hash]
dt.hosp.bronquiolitis_ltdatainici[hash == hash.val,]
setnames(dt.hosp.bronquiolitis_ltdatainici, c('hash', 'bronquiolitisvrs_pre', 'bronquiolitisvrs_pre_data_ingres'))
```


### Temporada VRS

```{r}

dt.hosp.bronquiolitis_gedatainici <- dt.hospitalitzacions.dm[bronquiolitisvrs == 1 & data_ingres >= data_inici, .(hash, bronquiolitisvrs, data_ingres)]
hash.val <- dt.hosp.bronquiolitis_gedatainici[,hash][28]
dt.hosp.bronquiolitis_gedatainici[hash == hash.val,]
dt.hosp.bronquiolitis_gedatainici <- dt.hosp.bronquiolitis_gedatainici[order(hash, data_ingres)]
dt.hosp.bronquiolitis_gedatainici <- dt.hosp.bronquiolitis_gedatainici[, .SD[1], hash]
dt.hosp.bronquiolitis_gedatainici[hash == hash.val,]
setnames(dt.hosp.bronquiolitis_gedatainici, c('hash', 'bronquiolitisvrs_post', 'bronquiolitisvrs_post_data_ingres'))
```

# Temporada 2024-2025 (Sense Expandir)

## Data Manager

### Data Entry

```{r}
# Set data_entry and data_cens
  # Data Entry
    dt.cohort.ana[, data_entry := data_inici]
```

### Data Cens

```{r}

dt.cohort.ana[, data_cens := data_fi]

  # Elimnar si data_cens < data_entry
    a <- dt.cohort.ana[data_cens < data_entry,] # N= 0 
    dt.cohort.ana <- dt.cohort.ana[data_cens >= data_entry,] # 
```

### Data Event

```{r}
dt.cohort.ana <- merge(dt.cohort.ana,
                       dt.hosp.bronquiolitis_ltdatainici,
                       by = 'hash',
                       all.x = TRUE) #

dt.cohort.ana <- merge(dt.cohort.ana,
                       dt.hosp.bronquiolitis_gedatainici,
                       by = 'hash',
                       all.x = TRUE) #
dt.cohort.ana[, .N, .(bronquiolitisvrs_pre, bronquiolitisvrs_post)]
dt.cohort.ana[, .N, .(nirse, nirse_20240331, bronquiolitisvrs_pre, bronquiolitisvrs_post)][order(nirse, nirse_20240331, bronquiolitisvrs_pre, bronquiolitisvrs_post)]

  # Data Censura
    dt.cohort.ana <- dt.cohort.ana[bronquiolitisvrs_post == 1, data_event := bronquiolitisvrs_post_data_ingres]
    dt.cohort.ana[, .(hash, nirse, bronquiolitisvrs_post, data_cens, data_event, data_defuncio)][order(bronquiolitisvrs_post)]
    dt.cohort.ana <- dt.cohort.ana[, data_cens := pmin(data_cens, data_event, data_defuncio, na.rm = TRUE)]

  # Establir l'event
    dt.cohort.ana[, bronquiolitis_model := 0][data_cens == data_event, bronquiolitis_model := 1]
    dt.cohort.ana[, .(hash, nirse, 
                      bronquiolitisvrs_pre, bronquiolitisvrs_post, bronquiolitis_model, 
                      data_cens, data_event, data_defuncio)][order(bronquiolitisvrs_post)]

  # Eliminar los registros de pacientes vacunados con el event antes de la inmunització (EXITUS (N=12))
    dt.cohort.ana[data_cens < data_entry,][, .(hash, 
                                               nirse, bronquiolitisvrs_pre, bronquiolitisvrs_post, bronquiolitis_model, 
                                               data_entry, data_cens, data_event, data_defuncio)][order(bronquiolitis_model)]
    dt.cohort.ana <- dt.cohort.ana[data_cens >= data_entry,] # 27.036 > 27.024
```


### Exposure

```{r}
dt.cohort.ana[, nirse_model := nirse]
```


```{r}
dt.cohort.ana <- dt.cohort.ana[, .(hash,
                                   nirse_model,
                                   data_entry, data_cens,
                                   bronquiolitisvrs_pre, bronquiolitisvrs_post,
                                   data_event, bronquiolitis_model,
                                   rural, medea_c2, sexe, edat_20241001, edat_20241001_c, espanyol,
                                   data_naixement, month_naixement_f, periode_estudi,
                                   aquas)]
#dt.cohort.ana
```

### Time

```{r}
dt.cohort.ana[, temps := as.numeric(data_cens) - as.numeric(data_entry)]
```


## Análisis

```{r}
dt.cohort.ana <- dt.cohort.ana[is.na(bronquiolitisvrs_pre),]
```

### Taula

```{r}
dt.cohort.ana[, bronquiolitis_model_f := factor(bronquiolitis_model,
                                                levels = c(0,1),
                                                labels = c("No", "Sí"))]
dt.cohort.ana[, bronquiolitis_model_edat := as.numeric(data_event - data_naixement)]
dt.cohort.ana[bronquiolitis_model_f == "No", bronquiolitis_model_edat := NA] # 118 (61.2)	136 (55.5) 0.085	128
dt.cohort.ana[data_entry == data_cens, bronquiolitis_model_edat := NA] # 119 (61.3)	135 (55.6) 0.122
a <- dt.cohort.ana[, .N, .(bronquiolitis_model_f, bronquiolitis_model_edat)]

cols <- c("nirse_model",
          "bronquiolitis_model_f",
          "bronquiolitis_model_edat",
           "month_naixement_f")
df <- dt.cohort.ana[,
                    .SD,
                    .SDcols = cols]
cG <- compareGroups(nirse_model ~ bronquiolitis_model_f + bronquiolitis_model_edat + bronquiolitis_model_edat + month_naixement_f,
                    method = c(1,1,2),
                    df, 
                    max.xlev = 20)
cT <- createTable(cG,
                  show.descr = TRUE,
                  show.n = TRUE,
                  show.all = FALSE,
                  show.ratio = FALSE)
export2md(cT
          , caption = ""
          , format = "html")
```

### Kaplan-Meier

#### Estimacions

```{r}
surv_obj_fit <- survfit(Surv(as.numeric(data_entry) - as.numeric(data_entry), 
                             as.numeric(data_cens) - as.numeric(data_entry),
                             event = bronquiolitis_model) ~ nirse_model,
                             data = dt.cohort.ana)

surv_obj_fit <- survfit(Surv(temps , event = bronquiolitis_model) ~ nirse_model,
                        data = dt.cohort.ana)
```

```{r}
dt.inciencia <- dt.cohort.ana[, .(person_time = sum(temps),
                                  n_events = sum(bronquiolitis_model)), nirse_model]
dt.inciencia[, incidencia := round(((n_events/person_time)*100000),2)]
dt.inciencia
```

### Poisson Regression

#### Naive

```{r}

dt.cohort.ana[, logfu := log(as.numeric(data_cens - data_entry))]
dt.cohort.ana[logfu == -Inf, logfu := NA]

m.p.naive <- glm(bronquiolitis_model ~ nirse_model + offset(logfu),
             dt.cohort.ana,
             family = poisson(link = "log"))
summary(m.p.naive)
exp(cbind(coef(m.p.naive), confint(m.p.naive)))
```

### Cox Regression

#### Time-since-nirsevimab scale

```{r}

surv_obj <- with(dt.cohort.ana, Surv(temps,
                                     event = bronquiolitis_model))
modelo_cox <- coxph(surv_obj ~ nirse_model, data = dt.cohort.ana)
summary(modelo_cox)
```

#### Calendar-time scale

##### Crude

```{r}
surv_obj <- with(dt.cohort.ana, Surv(as.numeric(data_entry), as.numeric(data_cens),
                                     event = bronquiolitis_model))
modelo_cox <- coxph(surv_obj ~ nirse_model, data = dt.cohort.ana)
summary(modelo_cox)
```

### Adjusted

```{r}
dt.cohort.ana[, medea_c2 := factor(medea_c2, levels = c('1U','2U','3U','4U','Rural'))]
surv_obj <- with(dt.cohort.ana, Surv(as.numeric(data_entry), as.numeric(data_cens),
                                     event = bronquiolitis_model))
modelo_cox <- coxph(surv_obj ~ nirse_model
                               #+ medea_c2
                               + month_naixement_f
                               #+ edat_20241001
                               #+ sexe
                               + espanyol,
                    data = dt.cohort.ana)
summary(modelo_cox)
```

#### Residuos schoenfeld

```{r}
test <- cox.zph(modelo_cox)
test
a <- ggcoxzph(test)[1]
tiff("../results/article_ing_bronquiolitisvrs_schoenfeld_nirse.tiff", units = "mm", width = 250, height = 150, res = 300)
a
dev.off()

a <- ggcoxzph(test)[2]
tiff("../results/article_ing_bronquiolitisvrs_schoenfeld_edat.tiff", units = "mm", width = 250, height = 150, res = 300)
a
dev.off()

a <- ggcoxzph(test)[3]
tiff("../results/article_ing_bronquiolitisvrs_schoenfeld_espanyol.tiff", units = "mm", width = 250, height = 150, res = 300)
a
dev.off()
```

#### Stratificated

```{r}
cols <- c("nirse_model",
          "bronquiolitis_model_f",
          "bronquiolitis_model_edat",
           "month_naixement_f")
df <- dt.cohort.ana[,
                    .SD,
                    .SDcols = cols]
cG <- compareGroups(nirse_model ~ bronquiolitis_model_f + bronquiolitis_model_edat + bronquiolitis_model_edat + month_naixement_f,
                    method = c(1,1,2),
                    df, 
                    max.xlev = 20)
cT <- createTable(cG,
                  show.descr = TRUE,
                  show.n = TRUE,
                  show.all = FALSE,
                  show.ratio = FALSE)
export2md(cT
          , caption = ""
          , format = "html")
```

```{r}
dt.cohort.ana[, .N, month_naixement_f]

surv_obj <- with(dt.cohort.ana, Surv(temps,
                                     event = bronquiolitis_model))
modelo_cox.strata <- coxph(surv_obj ~ nirse_model
                               #+ medea_c2
                            #+ edat_20241001
                               #+ sexe
                            + espanyol
                            + strata(month_naixement_f),
                            dt.cohort.ana
                            #,ties = c("efron","breslow","exact")[1]
                           )
summary(modelo_cox.strata)
```

```{r}
dt.cohort.ana[, .N, .(month_naixement_f, nirse_model, bronquiolitis_model)][order(month_naixement_f, nirse_model)]
```


```{r}
surv_obj <- with(dt.cohort.ana[month_naixement_f == 'April',], Surv(temps,
                                                                    event = bronquiolitis_model))
modelo_cox.strata.april <- coxph(surv_obj ~ nirse_model
                                  #+ medea_c2
                                  + edat_20240331
                                  #+ sexe
                                  + espanyol,
                                  dt.cohort.ana[month_naixement_f == 'April',])
summary(modelo_cox.strata.april)
```


```{r}
surv_obj <- with(dt.cohort.ana[month_naixement_f == 'May',], Surv(temps,
                                                                    event = bronquiolitis_model))
modelo_cox.strata.may <- coxph(surv_obj ~ nirse_model
                                  #+ medea_c2
                                  + edat_20240331
                                  #+ sexe
                                  + espanyol,
                                  dt.cohort.ana[month_naixement_f == 'May',])
summary(modelo_cox.strata.may) # n= 7749, number of events= 1
```


```{r}
surv_obj <- with(dt.cohort.ana[month_naixement_f == 'June',], Surv(temps,
                                                                    event = bronquiolitis_model))
modelo_cox.strata.june <- coxph(surv_obj ~ nirse_model
                                  #+ medea_c2
                                  + edat_20240331
                                  #+ sexe
                                  + espanyol,
                                  dt.cohort.ana[month_naixement_f == 'June',])
summary(modelo_cox.strata.june)
```

```{r}
surv_obj <- with(dt.cohort.ana[month_naixement_f == 'July',], Surv(temps,
                                                                    event = bronquiolitis_model))
modelo_cox.strata.july <- coxph(surv_obj ~ nirse_model
                                  #+ medea_c2
                                  + edat_20240331
                                  #+ sexe
                                  + espanyol,
                                  dt.cohort.ana[month_naixement_f == 'July',])
summary(modelo_cox.strata.july)
```

```{r}
surv_obj <- with(dt.cohort.ana[month_naixement_f == 'August',], Surv(temps,
                                                                    event = bronquiolitis_model))
modelo_cox.strata.august <- coxph(surv_obj ~ nirse_model
                                  #+ medea_c2
                                  + edat_20240331
                                  #+ sexe
                                  + espanyol,
                                  dt.cohort.ana[month_naixement_f == 'August',])
summary(modelo_cox.strata.august)
```

```{r}
#dt.cohort.ana[, .N, month_naixement_f][order(month_naixement_f)]
surv_obj <- with(dt.cohort.ana[month_naixement_f == 'September',], Surv(temps,
                                                                    event = bronquiolitis_model))
modelo_cox.strata.september <- coxph(surv_obj ~ nirse_model
                                  #+ medea_c2
                                  + edat_20240331
                                  #+ sexe
                                  + espanyol,
                                  dt.cohort.ana[month_naixement_f == 'September',])
summary(modelo_cox.strata.september)
```










# Expandiendo (RESIVAR A PARTIR DE AQUI)

```{r}
# # Expandir los pacientes expuestos
#   dt.cohort.ana <- dt.cohort.ana[, nirse_n := 0]
#   dt.temp <- dt.cohort.ana[!is.na(data_nirse),]
#   dt.temp <- dt.temp[, nirse_n := 1]
# 
#   dt.cohort.ana <- rbind(dt.cohort.ana,
#                          dt.temp,
#                          fill = TRUE) # 52.317
# 
# 
#   dt.cohort.ana[hash == dt.cohort.ana[nirse_n == 1,hash][28],] # Paciente Vacunado
#   dt.cohort.ana[hash == dt.cohort.ana[nirse_n == 1,hash][52],] # Paciente Vacunado
```


```{r}

```


```{r}
# Set data_entry and data_cens
  # Data Entry
    dt.cohort.ana[nirse_n == 0, data_entry := data_inici]
    dt.cohort.ana[nirse_n == 1, data_entry := data_nirse]
    dt.cohort.ana[hash == dt.cohort.ana[nirse_n == 1,hash][28],] # Paciente Vacunado
    dt.cohort.ana[hash == dt.cohort.ana[nirse_n == 1,hash][52],] # Paciente Vacunado
    
  # Data Cens
    dt.cohort.ana[is.na(data_nirse) & nirse_n == 0, data_cens := data_fi]
    dt.cohort.ana[!is.na(data_nirse) & nirse_n == 0, data_cens := data_nirse - 1]
    
    dt.cohort.ana[nirse_n == 1, data_cens := data_fi]
    dt.cohort.ana[hash == dt.cohort.ana[nirse_n == 1,hash][28],] # Paciente Vacunado
    dt.cohort.ana[hash == dt.cohort.ana[nirse_n == 1,hash][52],] # Paciente Vacunado

    
    # Elimnar si data_cens < data_entry
    a <- dt.cohort.ana[data_cens < data_entry,] # N= 1
    dt.cohort.ana <- dt.cohort.ana[data_cens >= data_entry,] # 

  # Añadir otras posibles censuras (exitus, etc ..)
```


```{r}
# 
    
    dt.hosp.bronquiolitis <- dt.hospitalitzacions.dm[bronquiolitisvrs == 1, .(hash, bronquiolitisvrs, data_ingres)]
    hash.val <- dt.hosp.bronquiolitis[,hash][28]
    dt.hosp.bronquiolitis[hash == hash.val,]
    dt.hosp.bronquiolitis <- dt.hosp.bronquiolitis[order(hash, data_ingres)]
    dt.hosp.bronquiolitis <- dt.hosp.bronquiolitis[, .SD[1], hash]
    dt.hosp.bronquiolitis[hash == hash.val,]
    
    dt.cohort.ana <- merge(dt.cohort.ana,
                           dt.hosp.bronquiolitis,
                           by = 'hash',
                           all.x = TRUE) # 45.507 > 45.507
    
  # Data Censura
    dt.cohort.ana <- dt.cohort.ana[bronquiolitisvrs == 1, data_event := data_ingres]
    dt.cohort.ana[hash == '000000C8566EAF77D9A2C5697D08301BD430FFC3',] # Paciente no Vacunado sin bronquiolitis
    dt.cohort.ana[hash == '00623325762E732D036607F1B057D5B4015B87DF',] # Paciente no Vacunado con bronquiolitis
    dt.cohort.ana[hash == '000804B794CC0644FF43699D224093CB33944AA8',] # Paciente Vacunado sin bronquiolitis
    dt.cohort.ana[hash == '45D1F0784289A4ECC0F3C975BE6FDC67FC81F8F7',] # Paciente vacunado con Bronquiolitis después vacuna  
    dt.cohort.ana[hash == '011DD8EBCB00FC908A2E86831E2AE4CD2CA569DF',] # Paciente vacunado con Bronquiolitis antes vacuna
    dt.cohort.ana[hash == 'E4B6499C16E4B35833CC4EBBF6F91C10880BD35D',] # Pacient EXITUS
  
    dt.cohort.ana <- dt.cohort.ana[, data_cens := pmin(data_cens, data_event, data_defuncio, na.rm = TRUE)]
    dt.cohort.ana[hash == 'E4B6499C16E4B35833CC4EBBF6F91C10880BD35D',] # Pacient EXITUS
    
  # Establir l'event
    dt.cohort.ana[, bronquiolitis_model := 0][data_cens == data_event, bronquiolitis_model := 1]
    dt.cohort.ana[hash == '000000C8566EAF77D9A2C5697D08301BD430FFC3',] # Paciente no Vacunado sin bronquiolitis
    dt.cohort.ana[hash == '00623325762E732D036607F1B057D5B4015B87DF',] # Paciente no Vacunado con bronquiolitis
    dt.cohort.ana[hash == '000804B794CC0644FF43699D224093CB33944AA8',] # Paciente Vacunado sin bronquiolitis
    dt.cohort.ana[hash == '45D1F0784289A4ECC0F3C975BE6FDC67FC81F8F7',] # Paciente vacunado con Bronquiolitis después vacuna  
    dt.cohort.ana[hash == '011DD8EBCB00FC908A2E86831E2AE4CD2CA569DF',] # Paciente vacunado con Bronquiolitis antes vacuna  

  # Eliminar los registros de pacientes vacunados con el event antes de la inmunització
    dt.cohort.ana <- dt.cohort.ana[data_cens >= data_entry,] # 49.507 > 49.361
    dt.cohort.ana[hash == '011DD8EBCB00FC908A2E86831E2AE4CD2CA569DF',] # Paciente vacunado con Bronquiolitis antes vacuna  
    
  # Eliminar las fechas de vacunación de los registros iniciales de cada paciente
    dt.cohort.ana[nirse_n == 0, data_nirse := NA]
    dt.cohort.ana[,nirse_model := 0][!is.na(data_nirse), nirse_model := 1]
    dt.cohort.ana[hash == '000000C8566EAF77D9A2C5697D08301BD430FFC3',] # Paciente no Vacunado sin bronquiolitis
    dt.cohort.ana[hash == '00623325762E732D036607F1B057D5B4015B87DF',] # Paciente no Vacunado con bronquiolitis
    dt.cohort.ana[hash == '000804B794CC0644FF43699D224093CB33944AA8',] # Paciente Vacunado sin bronquiolitis
    dt.cohort.ana[hash == '45D1F0784289A4ECC0F3C975BE6FDC67FC81F8F7',] # Paciente vacunado con Bronquiolitis después vacuna  
    dt.cohort.ana[hash == '011DD8EBCB00FC908A2E86831E2AE4CD2CA569DF',] # Paciente vacunado con Bronquiolitis antes vacuna  
    
    dt.cohort.ana <- dt.cohort.ana[, .(hash,
                                       nirse_model,
                                       data_entry, data_cens,
                                       data_event, bronquiolitis_model,
                                       rural, medea_c2, sexe, edat_20231001, edat_20231001_c, espanyol,
                                       data_naixement, month_naixement_f, aquas)]
```

```{r}
dt.cohort.ana[, bronquiolitis_model_f := factor(bronquiolitis_model,
                                                levels = c(0,1),
                                                labels = c("No", "Sí"))]
dt.cohort.ana[, bronquiolitis_model_edat := as.numeric(data_event - data_naixement)]
dt.cohort.ana[bronquiolitis_model_f == "No", bronquiolitis_model_edat := NA] # 118 (61.2)	136 (55.5) 0.085	128
dt.cohort.ana[data_entry == data_cens, bronquiolitis_model_edat := NA] # 119 (61.3)	135 (55.6) 0.122
a <- dt.cohort.ana[, .N, .(bronquiolitis_model_f, bronquiolitis_model_edat)]

cols <- c("nirse_model",
          "bronquiolitis_model_f",
          "bronquiolitis_model_edat")
df <- dt.cohort.ana[,
                    .SD,
                    .SDcols = cols]
cG <- compareGroups(nirse_model ~ bronquiolitis_model_f + bronquiolitis_model_edat + bronquiolitis_model_edat,
                    method = c(1,1,2),
                    df, 
                    max.xlev = 20)
cT <- createTable(cG,
                  show.descr = TRUE,
                  show.n = TRUE,
                  show.all = FALSE,
                  show.ratio = FALSE)
export2md(cT
          , caption = ""
          , format = "html")
```

```{r}
# Exportar arxiu a txt per ferr proves amb STATA
fwrite(dt.cohort.ana, "../data/p1_hosp_bronquiolitisvrs.txt", sep = '@')
```

***
***

# Poisson Regression

## Naive

```{r}

dt.cohort.ana[, logfu := log(as.numeric(data_cens - data_entry))]
dt.cohort.ana[logfu == -Inf, logfu := NA]

m.p.naive <- glm(bronquiolitis_model ~ nirse_model + offset(logfu),
             dt.cohort.ana,
             family = poisson(link = "log"))
summary(m.p.naive)
exp(cbind(coef(m.p.naive), confint(m.p.naive)))
```

# Cox Regression

## Time-since-nirsevimab scale

```{r}
dt.cohort.ana[, temps := as.numeric(data_cens) - as.numeric(data_entry)]
surv_obj <- with(dt.cohort.ana, Surv(temps,
                                     event = bronquiolitis_model))
modelo_cox <- coxph(surv_obj ~ nirse_model, data = dt.cohort.ana)
summary(modelo_cox)
```

## Calendar-time scale

```{r}
surv_obj <- with(dt.cohort.ana, Surv(as.numeric(data_entry), as.numeric(data_cens),
                                     event = bronquiolitis_model))
modelo_cox <- coxph(surv_obj ~ nirse_model, data = dt.cohort.ana)
summary(modelo_cox)
```

### Modelo Ajustado

```{r}
dt.cohort.ana[, medea_c2 := factor(medea_c2, levels = c('1U','2U','3U','4U','Rural'))]
surv_obj <- with(dt.cohort.ana, Surv(as.numeric(data_entry), as.numeric(data_cens),
                                     event = bronquiolitis_model))
modelo_cox <- coxph(surv_obj ~ nirse_model
                               #+ medea_c2
                               + edat_20231001
                               #+ sexe
                               + espanyol,
                    data = dt.cohort.ana)
summary(modelo_cox)
test <- cox.zph(modelo_cox)
test
a <- ggcoxzph(test)[1]
tiff("../results/article_ing_bronquiolitisvrs_schoenfeld_nirse.tiff", units = "mm", width = 250, height = 150, res = 300)
a
dev.off()

a <- ggcoxzph(test)[2]
tiff("../results/article_ing_bronquiolitisvrs_schoenfeld_edat.tiff", units = "mm", width = 250, height = 150, res = 300)
a
dev.off()

a <- ggcoxzph(test)[3]
tiff("../results/article_ing_bronquiolitisvrs_schoenfeld_espanyol.tiff", units = "mm", width = 250, height = 150, res = 300)
a
dev.off()
```

#### Modelo Stratificado

```{r}
surv_obj <- with(dt.cohort.ana, Surv(temps,
                                     event = bronquiolitis_model))
modelo_cox.strata <- coxph(surv_obj ~ nirse_model
                               #+ medea_c2
                            + edat_20231001
                               #+ sexe
                            + espanyol
                            + strata(month_naixement_f),
                            dt.cohort.ana
                            #,ties = c("efron","breslow","exact")[1]
                           )
summary(modelo_cox.strata)
```

```{r}
dt.cohort.ana[, .N, .(month_naixement_f, bronquiolitis_model)][order(month_naixement_f)]
```


```{r}
surv_obj <- with(dt.cohort.ana[month_naixement_f == 'April',], Surv(temps,
                                                                    event = bronquiolitis_model))
modelo_cox.strata.april <- coxph(surv_obj ~ nirse_model
                                  #+ medea_c2
                                  + edat_20231001
                                  #+ sexe
                                  + espanyol,
                                  dt.cohort.ana[month_naixement_f == 'April',])
summary(modelo_cox.strata.april)
```


```{r}
surv_obj <- with(dt.cohort.ana[month_naixement_f == 'May',], Surv(temps,
                                                                    event = bronquiolitis_model))
modelo_cox.strata.may <- coxph(surv_obj ~ nirse_model
                                  #+ medea_c2
                                  + edat_20231001
                                  #+ sexe
                                  + espanyol,
                                  dt.cohort.ana[month_naixement_f == 'May',])
summary(modelo_cox.strata.may) # n= 7749, number of events= 1
```


```{r}
surv_obj <- with(dt.cohort.ana[month_naixement_f == 'June',], Surv(temps,
                                                                    event = bronquiolitis_model))
modelo_cox.strata.june <- coxph(surv_obj ~ nirse_model
                                  #+ medea_c2
                                  + edat_20231001
                                  #+ sexe
                                  + espanyol,
                                  dt.cohort.ana[month_naixement_f == 'June',])
summary(modelo_cox.strata.june)
```

```{r}
surv_obj <- with(dt.cohort.ana[month_naixement_f == 'July',], Surv(temps,
                                                                    event = bronquiolitis_model))
modelo_cox.strata.july <- coxph(surv_obj ~ nirse_model
                                  #+ medea_c2
                                  + edat_20231001
                                  #+ sexe
                                  + espanyol,
                                  dt.cohort.ana[month_naixement_f == 'July',])
summary(modelo_cox.strata.july)
```

```{r}
surv_obj <- with(dt.cohort.ana[month_naixement_f == 'August',], Surv(temps,
                                                                    event = bronquiolitis_model))
modelo_cox.strata.august <- coxph(surv_obj ~ nirse_model
                                  #+ medea_c2
                                  + edat_20231001
                                  #+ sexe
                                  + espanyol,
                                  dt.cohort.ana[month_naixement_f == 'August',])
summary(modelo_cox.strata.august)
```

```{r}
#dt.cohort.ana[, .N, month_naixement_f][order(month_naixement_f)]
surv_obj <- with(dt.cohort.ana[month_naixement_f == 'September',], Surv(temps,
                                                                    event = bronquiolitis_model))
modelo_cox.strata.september <- coxph(surv_obj ~ nirse_model
                                  #+ medea_c2
                                  + edat_20231001
                                  #+ sexe
                                  + espanyol,
                                  dt.cohort.ana[month_naixement_f == 'September',])
summary(modelo_cox.strata.september)
```

### Modelo Interección ICS*NAcionalitat

```{r}
surv_obj <- with(dt.cohort.ana, Surv(temps,
                                     event = bronquiolitis_model))
modelo_cox.nointeraction <- coxph(surv_obj ~ nirse_model
                                  #+ medea_c2
                                  + aquas
                                  + edat_20231001
                                  #+ sexe
                                  + espanyol
                                  ,
                                  dt.cohort.ana)
summary(modelo_cox.nointeraction)

modelo_cox.interaction <- coxph(surv_obj ~ nirse_model
                                + medea_c2
                                + edat_20231001
                                #+ sexe
                                + espanyol
                                #+ espanyol*medea_c2
                                + espanyol*aquas,
                                dt.cohort.ana)
summary(modelo_cox.interaction)
anova(modelo_cox.interaction,modelo_cox.nointeraction)
anova(modelo_cox.nointeraction,modelo_cox.interaction)
```

## Kaplan-Meier

### Estimacions

```{r}
surv_obj_fit <- survfit(Surv(as.numeric(data_entry) - as.numeric(data_entry), 
                             as.numeric(data_cens) - as.numeric(data_entry),
                             event = bronquiolitis_model) ~ nirse_model,
                             data = dt.cohort.ana)

surv_obj_fit <- survfit(Surv(temps , event = bronquiolitis_model) ~ nirse_model,
                        data = dt.cohort.ana)
```

```{r}
dt.inciencia <- dt.cohort.ana[, .(person_time = sum(temps),
                                  n_events = sum(bronquiolitis_model)), nirse_model]
dt.inciencia[, incidencia := round(((n_events/person_time)*100000),2)]
dt.inciencia
```


```{r}
surv_obj_fit_res <- data.table(
  Type  = c(rep("No Nirsevimab", surv_obj_fit$strata[[1]]), rep("Nirsevimab", surv_obj_fit$strata[[2]])),
  time = surv_obj_fit$time,
  N_risk = surv_obj_fit$n.risk,
  N_event = surv_obj_fit$n.event,
  N_censor = surv_obj_fit$n.censor,
  Prop_surv = surv_obj_fit$surv,
  IC95_low = surv_obj_fit$lower,
  IC95_upp = surv_obj_fit$upper,
  std_error = surv_obj_fit$std.err,
  cum_hazard = surv_obj_fit$cumhaz,
  std_chaz = surv_obj_fit$std.chaz
)
surv_obj_fit_res
```

### Figures

```{r}
ggsurvplot(surv_obj_fit,
           conf.int = TRUE,
           pval.coord = c(0, .85),
           ylim = c(.85, 1),
           xlim = c(0, 120),
           break.time.by = 15,
           palette = "grey",
           ggtheme = theme_classic(),
           legend.labs = c("Control", "Nirsevimab"),
           title = "Hospital admission",
           #subtitle = "Nascuts Abril-Setembre",
           legend = "bottom"
           , risk.table = TRUE
           )
           # + labs(caption = "Elaboració: Sistemes d'informació dels Serveis d'Atenció Primària (SISAP)")
```

```{r}
# Source: https://stackoverflow.com/questions/61371184/specifying-custom-time-points-for-survival-plot
a <- ggsurvplot(surv_obj_fit,
           fun = "event",
           #xlim = c(0, 122),  # Amplié el límite para incluir el último break
           xlim = c(0, 500),  # Amplié el límite para incluir el último break
           #break.time.by = 12.2,
           break.time.by = 60,
           palette = "grey",
           ggtheme = theme_classic(),
           legend.title = "",
           xlab = "Days",
           legend.labs = c("Control", "Nirsevimab"),
           legend = "bottom",
           title = "Hospital admission",
           cumevents = TRUE,
           cumevents.title = "",
           #fontsize = 0.5,
           tables.height = 0.30
)
# extract ggplot object from ggsurvplot
p <- a$plot 
p
p <- p + scale_x_continuous(breaks = c(0, 12, 24, 36, 48, 60, 72, 84, 96, 108, 122))

# extract table object from ggsurvplot
tab <- a$table
tab$layers = NULL # clear labels
tab <- tab + 
  geom_text(aes(x = time, y = rev(strata), label = llabels), data = tab$data[tab$data$time %in% c(0, 12, 24, 36, 48, 60, 72, 84, 96, 108, 122),]) +
  scale_x_continuous(breaks = c(0, 12, 24, 36, 48, 60, 72, 84, 96, 108, 122))

# extract cumevents object from ggsurvplot
tab2 <- a$cumevents
tab2$layers = NULL # clear labels
tab2 <- tab2 + 
  geom_text(aes(x = time, y = rev(strata), label = cum.n.event),
            data = tab$data[tab$data$time %in% c(0, 12, 24, 36, 48, 60, 72, 84, 96, 108, 122),],
            size = 3) +
  scale_x_continuous(breaks = c(0, 12, 24, 36, 48, 60, 72, 84, 96, 108, 122))


# Add plots back
a$plot <- p
#a$table <- tab
a$cumevents <- tab2


#tiff("../results/article_survplot_ing_bronquiolitisvrs.tiff", units = "mm", width = 250, height = 150, res = 300)
a
dev.off()
```

