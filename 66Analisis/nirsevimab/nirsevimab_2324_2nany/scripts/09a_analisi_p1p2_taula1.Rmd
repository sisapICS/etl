---
title: "NIRSEVIMAB 2023-2024 - 2n Any"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

```{r}
inici <- Sys.time()
```

```{r}
# Data Manager
  suppressWarnings(suppressPackageStartupMessages(library('Hmisc')))
  suppressWarnings(suppressPackageStartupMessages(library('data.table')))
  
  suppressWarnings(suppressPackageStartupMessages(library('tidyverse')))
  
  suppressWarnings(suppressPackageStartupMessages(library('tibble')))
  suppressWarnings(suppressPackageStartupMessages(library('plyr')))
  suppressWarnings(suppressPackageStartupMessages(library('dplyr')))
  suppressWarnings(suppressPackageStartupMessages(library('tidyr')))
  suppressWarnings(suppressPackageStartupMessages(library('forcats')))
  suppressWarnings(suppressPackageStartupMessages(library('encode')))

# Matching
  suppressWarnings(suppressPackageStartupMessages(library('Matching')))
  
# Anàlisi
  suppressWarnings(suppressPackageStartupMessages(library('compareGroups')))
  suppressWarnings(suppressPackageStartupMessages(library('stddiff')))
#   #suppressWarnings(suppressPackageStartupMessages(library('nlme')))
#   #suppressWarnings(suppressPackageStartupMessages(library('statmod')))
#   

# Modelització
#     suppressWarnings(suppressPackageStartupMessages(library('sjPlot')))
#     suppressWarnings(suppressPackageStartupMessages(library('lme4')))
#     suppressWarnings(suppressPackageStartupMessages(library('MASS'))) # Binomial Negative
#     suppressWarnings(suppressPackageStartupMessages(library('pscl'))) # Zero-Inflated Regression
#     suppressWarnings(suppressPackageStartupMessages(library('sandwich')))
#     suppressWarnings(suppressPackageStartupMessages(library('gtsummary')))
      
# # Figures
    suppressWarnings(suppressPackageStartupMessages(library('ggplot2'))) 
#   suppressWarnings(suppressPackageStartupMessages(library('scales')))
#   #suppressWarnings(suppressPackageStartupMessages(library('splines')))
#   #suppressWarnings(suppressPackageStartupMessages(library('gridExtra')))
    #source('../functions.r')
# 
# # Taules
    suppressWarnings(suppressPackageStartupMessages(library('DT')))
```

***
***

```{r}
dt.cohort.flt <- readRDS("../data/dt.cohort.flt.rds")
dt.cohort.ana <- dt.cohort.flt
```

# SMD `r if (params$eval.tabset) '{.tabset}' else ''`

## Abans Matching `r if (params$eval.tabset) '{.tabset}' else ''`

### Taules `r if (params$eval.tabset) '{.tabset}' else ''`

```{r compare groups}
cols <- c("nirse",
          "periode_estudi",
          "rural",
          "medea_c2",
          "aquas", "aquas",
          "month_naixement_f",
          "edat_20241001", "edat_20241001", "sexe",
          "espanyol",
          "dbs_visites_any_n",
          "edat_nirse",
          "temps_nirse")
df <- dt.cohort.ana[,
                    .SD,
                    .SDcols = cols]
cG <- compareGroups(nirse ~ rural + medea_c2 + aquas + aquas + month_naixement_f + periode_estudi + edat_20241001 + edat_20241001 + sexe + espanyol + dbs_visites_any_n + dbs_visites_any_n + edat_nirse + edat_nirse + temps_nirse,
                    df, 
                    method = c(1,1,1,2,1,1,1,2,1,1,1,2,1,2,2),
                    max.xlev = 20)
cT <- createTable(cG,
                  show.descr = TRUE,
                  show.n = TRUE,
                  show.all = FALSE,
                  hide.no = c("Dona", "No"),
                  show.ratio = FALSE)
export2md(cT
          , caption = "Taula 1"
          , format = "html")
export2word(cT, file = '../results/table1.docx')
```

```{r}
# dt.cat.var.label.raw <- data.table(read_xlsx("../data/catalegs/SMD - Nom de les variables (labels).xlsx",
#                                              sheet = "Hoja 1",
#                                              range = 'A2:F67'))
# dt.cat.var.label.flt <- dt.cat.var.label.raw[smd == 1, ]
```

```{r càlcul SMD}
# Nota: L'exposició ha de sser una variable numèrica 0/1!!!
  dt.cohort.ana[nirse == 'No Nirsevimab', nirse_smd := 0][nirse == 'Nirsevimab', nirse_smd := 1]
  dt.cohort.ana[, .N, .(nirse, nirse_smd)]

dt.smd <- dt.cohort.ana
dt.smd[, month_naixement_t := as.character(month_naixement_f)]

tsmd <- do.call("rbind", 
                 lapply(c("rural", "medea_c2",
                          "month_naixement_t", "periode_estudi",
                          "aquas",
                          "edat_20241001", "sexe",
                          "espanyol",
                          "dbs_visites_any_n"
                          ),
                          function(x){
                     d <- dt.smd[, .SD, .SDcols = c("nirse_smd", x)]
    if (is.integer(dt.smd[, get(x)]) | is.numeric(dt.smd[, get(x)])) {
      smd <- tryCatch(stddiff.numeric(data = d, gcol = 1, vcol = 2)[,"stddiff"], error = function(e) NA)
      t <- data.table(V1 = c(smd))
      t[, Variable := x]
      t[, c("Variable", "V1")]
  } 
  else {
    smd <- tryCatch(stddiff.category(data = d, gcol = 1, vcol = 2)[1,"stddiff"], error = function(e) NA)
      t <- data.table(V1 = c(smd))
      t[, Variable := x]
      t[, c("Variable", "V1")]
  }
}))
dt.smd.all <- data.table(tsmd)[order(-V1)]
datatable(dt.smd.all)
#fwrite(data.table(tsmd), "../results/matching_table_smd.csv")

```

### Figures

```{r}
# dt.smd.ggplot <- merge(dt.smd.matching,
#                        dt.cat.var.label.flt,
#                        by.x = 'Variable',
#                        by.y = 'Variable')
# dt.smd.ggplot <- dt.smd.ggplot[order(ordre),]
```

```{r, fig.height=4, fig.width=7}
dt.smd.all[Variable == 'month_naixement_t', desc := 'Month of birth']
dt.smd.all[Variable == 'periode_estudi', desc := 'VRS Season']
dt.smd.all[Variable == 'edat_20241001', desc := 'Age']
dt.smd.all[Variable == 'espanyol', desc := 'Nationality (Spanish)']
#dt.smd.all[Variable == 'dbs_visites_any_n', desc := 'dbs_visites_any_n']
dt.smd.all[Variable == 'aquas', desc := 'Socioeconomic status']
dt.smd.all[Variable == 'rural', desc := 'Rural']
dt.smd.all[Variable == 'sexe', desc := 'Sex']

fig <- ggplot(dt.smd.all[Variable %in% c('month_naixement_t', 'periode_estudi','edat_20241001', 'sexe', 'espanyol', 
                                         #'dbs_visites_any_n',
                                         'rural', 'aquas')],
              aes(x = reorder(desc, V1), y = V1)) +
  geom_point(size = 3) +
  coord_flip() +
  theme_classic() +
  labs(x = "", y = "SMD") +
  geom_hline(yintercept = 0.1, linetype = 2) + ylim(0, 0.5)

ggsave("../results/figure_smd.png", plot = fig, width = 6, height = 4, dpi = 300)
```


```{r}
fi <- Sys.time()
fi - inici
```