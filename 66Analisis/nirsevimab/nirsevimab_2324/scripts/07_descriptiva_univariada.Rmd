***
***

# Descriptiva Univariada `r if (params$eval.tabset) '{.tabset}' else ''`

## Nivell Pacient `r if (params$eval.tabset) '{.tabset}' else ''`

### Taula Resum `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
df <- dt.cohort.flt
res <- compareGroups(nirse ~ . -eap_ep -codi_nacionalitat
                     , df
                     , max.xlev = 100
                     , max.ylev = 5
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = FALSE
                  , show.all = TRUE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Taula RESUM")
```

### Data Naixement `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
ggplot(dt.cohort.flt,
       aes(x = data_naixement)) +
       geom_histogram(fill = 'grey', color = "white") + 
       theme_minimal()
```

### Edat `r if (params$eval.tabset) '{.tabset}' else ''`

Edat al final de l'estudi, es només per comparar l'exat entre les dues cohorts.

```{r}
summary(dt.cohort.flt[, edat_20240331])
```

### Sexe `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
ggplot(dt.cohort.flt,
       aes(x = as.factor(sexe))) +
       geom_bar() + 
       coord_flip() +
       theme_minimal()
```

### Nacionalitat `r if (params$eval.tabset) '{.tabset}' else ''`

#### Espanyol `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
summary(dt.cohort.flt[, as.factor(espanyol)])
```

#### Descripció `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
ggplot(dt.cohort.flt[codi_nacionalitat != 724,],
       aes(x = desc_nac)) +
       geom_bar() + 
       coord_flip() +
       theme_minimal()
```

#### Regió `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
ggplot(dt.cohort.flt,
       aes(x = regio_desc)) +
       geom_bar() + 
       coord_flip() +
       theme_minimal()

ggplot(dt.cohort.flt[codi_nacionalitat != 724],
       aes(x = reorder(regio_desc, table(regio_desc)[regio_desc]))) +
       geom_bar() + 
       coord_flip() +
       theme_minimal()
```

### Situació `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
ggplot(dt.cohort.flt[situacio != 'A',],
       aes(x = reorder(situacio, table(situacio)[situacio]))) +
       geom_bar() + 
       coord_flip() +
       theme_minimal()
```

#### Defunció `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
#summary(dt.cohort.flt[, as.factor(exitus)])
```

```{r}
summary(dt.cohort.flt[, as.factor(data_defuncio)])
```

#### Exitus `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
summary(dt.cohort.flt[, as.factor(exitus)])
```

```{r}
summary(dt.cohort.flt[, as.factor(data_exitus)])
```

```{r}
summary(dt.cohort.flt[, as.factor(edat_exitus)])
```

### Periode estudi `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
ggplot(dt.cohort.flt,
       aes(x = reorder(periode_estudi, table(periode_estudi)[periode_estudi]))) +
       geom_bar() + 
       coord_flip() +
       theme_minimal()
```

### Nirsevimab `r if (params$eval.tabset) '{.tabset}' else ''`

#### Nirsevimab `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
summary(dt.cohort.flt[, as.factor(nirse)])
```

#### Data Nirsevimab `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
ggplot(dt.cohort.flt,
       aes(x = data_nirse)) +
       geom_histogram(fill = 'grey', color = "white") + 
       theme_minimal()

# ggplot(dt.cohort.flt[periode_estudi == 'Periode 2',],
#        aes(x = data_nirse)) +
#        geom_histogram(fill = 'grey', color = "white") + 
#        theme_minimal()
```

```{r}
dt.fig.nirsevimab <- dt.cohort.flt
dt.fig.nirsevimab <- dt.fig.nirsevimab[, .(den = .N), data_nirse]
dt.fig.nirsevimab[, N := sum(den)]
dt.fig.nirsevimab <- dt.fig.nirsevimab[order(data_nirse), cs := cumsum(den)][order(data_nirse)]
dt.fig.nirsevimab[, cobertura_nirse := round((cs/N)*100,2)]
dt.fig.nirsevimab[order(data_nirse), sequencia := seq(1, .N, 1),]
dt.fig.nirsevimab <- dt.fig.nirsevimab[, .(sequencia, data_nirse, cobertura_nirse)]

figura <- ggplot(dt.fig.nirsevimab, aes(data_nirse, cobertura_nirse)) +
  geom_ribbon(aes(ymin = 0, ymax = cobertura_nirse), fill = "lightgray", alpha = 0.5) +  # Sombreado del área bajo la línea
  geom_line(size = 0.8) +
  theme_classic() +
  scale_x_date(labels = date_format("%Y-%m")) +
  scale_y_continuous(limits = c(0,100)) +
  geom_segment(aes(x = as.Date('2023-11-01', '%Y-%m-%d'), y = 0,
                   xend = as.Date('2023-11-01', '%Y-%m-%d'), yend = 76.26),
                   linetype = "dashed") +
  geom_segment(aes(x = as.Date('2024-01-31', '%Y-%m-%d'), y = 0,
                   xend = as.Date('2024-01-31', '%Y-%m-%d'), yend = 87.19),
                   linetype = "dashed") +  
  geom_text(aes(x = as.Date('2023-10-31'),
                y = dt.fig.nirsevimab[data_nirse == as.Date('2023-10-31'),][[3]],
                label = dt.fig.nirsevimab[data_nirse == as.Date('2023-10-31'),][[3]]),
                vjust = -1, hjust = 0.4) +
  geom_text(aes(x = as.Date('2024-01-31'),
                y = dt.fig.nirsevimab[data_nirse == as.Date('2024-01-31'),][[3]],
                label = dt.fig.nirsevimab[data_nirse == as.Date('2024-01-31'),][[3]]),
                vjust = -1, hjust = 1) +
  labs(x = "Days", y = "Nirsevimab coverage", linetype = "")
  theme(legend.position = "bottom")
figura  
  
tiff("../results/vaccination_coverage.tiff", units = "mm", width = 250, height = 150, res = 300)
figura
dev.off()
```

#### Edat Nirsevimab `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
summary(dt.cohort.flt[, edat_nirse])
ggplot(dt.cohort.flt,
       aes(x = edat_nirse)) +
       geom_histogram(fill = 'grey', color = "white", bins = 150) + 
       theme_minimal()
```

                                 
```{r}
#rm(list = c("dt.agr"))
gc.var <- gc()
```


