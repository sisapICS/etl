***
***

# Descriptiva Bivariada

## Cas

### Taula Resum

```{r}
df <- dt.cohort.flt
res <- compareGroups(cas ~ . -hash -abs_codi -eap_codi -ics_desc -eap_ep -amb_desc
                     , df
                     , max.xlev = 100
                     , max.ylev = 5
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Comparació CAS - Taula RESUM")
```

```{r}
#rm(list=c("dt.biv","df","res","cT"))
gc.var <- gc()
```


