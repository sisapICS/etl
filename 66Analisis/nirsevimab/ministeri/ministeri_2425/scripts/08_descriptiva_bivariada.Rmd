
# Descriptiva Bivariada `r if (params$eval.tabset) '{.tabset}' else ''`

## Cohort `r if (params$eval.tabset) '{.tabset}' else ''`

### Cas `r if (params$eval.tabset) '{.tabset}' else ''`

#### Taula Resum `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
df <- dt.cohort.flt
res <- compareGroups(cas ~ . -hash -abs_codi -eap_codi -ics_desc -eap_ep -amb_desc -hos_inf_cod -codi_nacionalitat -identificadorcaso
                     , df
                     , max.xlev = 100
                     , max.ylev = 5
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Comparació CAS - Taula RESUM")
```

## Casos `r if (params$eval.tabset) '{.tabset}' else ''`

### Inmunizacion `r if (params$eval.tabset) '{.tabset}' else ''`

#### Taula Resum `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
df <- dt.cohort.flt[cas == 'Cas',]
res <- compareGroups(inmunizacion ~ . -hash -abs_codi -eap_codi -ics_desc -eap_ep -amb_desc -hos_inf_cod -codi_nacionalitat -identificadorcaso -localitat
                     , df
                     , max.xlev = 100
                     , max.ylev = 5
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Comparació INMONITZACIÓN - Taula RESUM")
```


### Inmunizacion `r if (params$eval.tabset) '{.tabset}' else ''`

#### Taula Resum `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
df <- dt.cohort.flt[cas == 'Cas',]
res <- compareGroups(inmunizacion ~ . -hash -abs_codi -eap_codi -ics_desc -eap_ep -amb_desc -hos_inf_cod -codi_nacionalitat -identificadorcaso -localitat
                     , df
                     , max.xlev = 100
                     , max.ylev = 5
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Comparació INMONITZACIÓN - Taula RESUM")
```

```{r}
#rm(list=c("dt.biv","df","res","cT"))
gc.var <- gc()
```


