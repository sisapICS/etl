
library('rmarkdown')
library('compareGroups')

render("01_nirsevimab_ministeri.Rmd", 
       output_format = "html_document",
       output_file = paste('nirsevimab_ministeri_2425',
                           format(Sys.Date(),'%Y%m%d'),
                           format(Sys.time(),'%H%M'), sep = "_"),
       output_dir = "../results/",
       params = list(bbdd = 'nirse_ministerio_2025',
                     actualitzar_dades = TRUE,
                     skim = TRUE,
                     eval.tabset = TRUE,
                     create.table = TRUE)
)
gc()