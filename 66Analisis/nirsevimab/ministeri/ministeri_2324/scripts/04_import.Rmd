***
***
***

# Importació

```{r}
options(java.parameters = "-Xmx32g")

Sys.setenv(TZ = 'Europe/Madrid')
Sys.setenv(ORA_SDTZ = 'Europe/Madrid')

source("C:/Users/ehermosilla/Documents/Keys.R")
```

## Dades

### Casos

S'importa la informació de la taula **casos** de la bbdd **nirse_ministerio** de SISAP (backup).

```{r}

if (params$actualitzar_dades) {
  ini <- Sys.time()
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idpermanent,
                   password = "", 
                   host = hostpermanent,
                   port = portpermanent,
                   dbname = "nirse_ministerio")
  query <- dbSendQuery(con,
                       statement = "select
                                      *
                                    from
                                      casos")
  dt.casos.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  saveRDS(dt.casos.raw, "D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/casos.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.casos.raw <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/casos.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}
```

Informació importada:

- files n = `r nrow(dt.casos.raw)`
- columnes n = `r ncol(dt.casos.raw)`
- columnes: `r names(dt.casos.raw)`

```{r}

names(dt.casos.raw) <- tolower(names(dt.casos.raw))

setnames(dt.casos.raw,
         c('centre', 'ingres', 'data_vrs'),
         c('hospital_codih', 'data_ingres', 'data_vrs_text'))
```

```{r}
# Dates
  # Data VRS
  dt.casos.raw[, data_vrs_text := substring(data_vrs_text,1,9)]
  dt.casos.raw[, dia := substr(data_vrs_text, 1, 2)]
  
  meses_abreviados <- c("JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")
  dt.casos.raw[, mes := match(substr(data_vrs_text, 4, 6), meses_abreviados)]

  dt.casos.raw[, any := substr(data_vrs_text, 8, 9)]

  dt.casos.raw[, data_vrs := as.Date(paste0(dia, "-", mes, "-", any), format = "%d-%m-%y")]

  # Obtener las columnas que empiezan con "data_"
    cols <- grep("^data_i", names(dt.casos.raw), value = TRUE)

  # Convertir las columnas de character a tipo date
    dt.casos.raw[, (cols) := lapply(.SD, function(x) as.Date(x, format = "%Y-%m-%d")), .SDcols = cols]
```

```{r}
dt.casos.raw <- dt.casos.raw[, .(hash,
                                 hospital_codih,
                                 data_ingres,
                                 data_vrs,
                                 virus,
                                 viruses)]
```

#### Summary

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.casos.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

***

### cohort

S'importa la informació de la taula **cohort** de la bbdd **nirse_ministerio** de SISAP (backup).

```{r}

if (params$actualitzar_dades) {
  ini <- Sys.time()
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idpermanent,
                   password = "", 
                   host = hostpermanent,
                   port = portpermanent,
                   dbname = "nirse_ministerio")
  query <- dbSendQuery(con,
                       statement = "select
                                      *
                                    from
                                      cohort")
  dt.cohort.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  saveRDS(dt.cohort.raw, "D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/cohort.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.cohort.raw <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/cohort.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}
```

Informació importada:

- files n = `r nrow(dt.cohort.raw)`
- columnes n = `r ncol(dt.cohort.raw)`
- columnes: `r names(dt.cohort.raw)`

```{r}

names(dt.cohort.raw) <- tolower(names(dt.cohort.raw))

setnames(dt.cohort.raw, c('naixement', 'abs', 'eap', 'nacionalitat'),
                        c('data_naixement', 'abs_codi', 'eap_codi', 'codi_nacionalitat'))
```

```{r}
# Dates
  # Obtener las columnas que empiezan con "data_"
    cols <- grep("^data_", names(dt.cohort.raw), value = TRUE)

  # Convertir las columnas de character a tipo date
    dt.cohort.raw[, (cols) := lapply(.SD, function(x) as.Date(x, format = "%Y-%m-%d")), .SDcols = cols]
    
# Numeric
  dt.cohort.raw[, codi_nacionalitat := as.numeric(codi_nacionalitat)]  
```


#### Summary

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.cohort.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

***

### multitest

S'importa la informació de la taula **multitest** de la bbdd **nirse_ministerio** de SISAP (backup).

```{r}

if (params$actualitzar_dades) {
  ini <- Sys.time()
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idpermanent,
                   password = "", 
                   host = hostpermanent,
                   port = portpermanent,
                   dbname = "nirse_ministerio")
  query <- dbSendQuery(con,
                       statement = "select
                                      *
                                    from
                                      multitests")
  dt.multitests.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  saveRDS(dt.multitests.raw, "D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/multitests.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.multitests.raw <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/multitests.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}
```

Informació importada:

- files n = `r nrow(dt.multitests.raw)`
- columnes n = `r ncol(dt.multitests.raw)`
- columnes: `r names(dt.multitests.raw)`

```{r}

names(dt.multitests.raw) <- tolower(names(dt.multitests.raw))
setnames(dt.multitests.raw, c('data'), c('data_multitest'))
```

```{r}
# Dates
  # Obtener las columnas que empiezan con "data_"
    cols <- grep("^data_", names(dt.multitests.raw), value = TRUE)

  # Convertir las columnas de character a tipo date
    dt.multitests.raw[, (cols) := lapply(.SD, function(x) as.Date(x, format = "%Y-%m-%d")), .SDcols = cols]
```

#### Summary

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.multitests.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

***

### urgencies

S'importa la informació de la taula **urgencies** de la bbdd **nirse_ministerio** de SISAP (backup).

```{r}

if (params$actualitzar_dades) {
  ini <- Sys.time()
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idpermanent,
                   password = "", 
                   host = hostpermanent,
                   port = portpermanent,
                   dbname = "nirse_ministerio")
  query <- dbSendQuery(con,
                       statement = "select
                                      *
                                    from
                                      urgencies")
  dt.urgencies.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  saveRDS(dt.urgencies.raw, "D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/urgencies.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.urgencies.raw <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/urgencies.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}
```

Informació importada:

- files n = `r nrow(dt.urgencies.raw)`
- columnes n = `r ncol(dt.urgencies.raw)`
- columnes: `r names(dt.urgencies.raw)`

```{r}

names(dt.urgencies.raw) <- tolower(names(dt.urgencies.raw))
setnames(dt.urgencies.raw, c('up', 'up_des', 'data'), c('up_urgencies_cod', 'up_urgencies_des', 'data_urgencies'))
```

```{r}
# Dates
  # Obtener las columnas que empiezan con "data_"
    cols <- grep("^data_", names(dt.urgencies.raw), value = TRUE)

  # Convertir las columnas de character a tipo date
    dt.urgencies.raw[, (cols) := lapply(.SD, function(x) as.Date(x, format = "%Y-%m-%d")), .SDcols = cols]
```

#### Summary

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.urgencies.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

***

### hospitalitzacions

S'importa la informació de la taula **cmbdh** de la bbdd **nirse_ministerio** de SISAP (backup).

```{r}

if (params$actualitzar_dades) {
  ini <- Sys.time()
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idpermanent,
                   password = "", 
                   host = hostpermanent,
                   port = portpermanent,
                   dbname = "nirse_ministerio")
  query <- dbSendQuery(con,
                       statement = "select
                                      *
                                    from
                                      cmbdh")
  dt.hospitalitzacions.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  saveRDS(dt.hospitalitzacions.raw, "D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/hospitalitzacions.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.hospitalitzacions.raw <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/hospitalitzacions.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}
```

Informació importada:

- files n = `r nrow(dt.hospitalitzacions.raw)`
- columnes n = `r ncol(dt.hospitalitzacions.raw)`
- columnes: `r names(dt.hospitalitzacions.raw)`

```{r}

names(dt.hospitalitzacions.raw) <- tolower(names(dt.hospitalitzacions.raw))
setnames(dt.hospitalitzacions.raw, c('up'), c('up_hospital_cod'))
```

```{r}
# Dates
  # Obtener las columnas que empiezan con "data_"
    cols <- grep("^data_", names(dt.hospitalitzacions.raw), value = TRUE)

  # Convertir las columnas de character a tipo date
    dt.hospitalitzacions.raw[, (cols) := lapply(.SD, function(x) as.Date(x, format = "%Y-%m-%d")), .SDcols = cols]
```

#### Summary

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.hospitalitzacions.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

***
***

## Matching

### Casos

```{r}

if (params$actualitzar_dades) {
  ini <- Sys.time()
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idpermanent,
                   password = "", 
                   host = hostpermanent,
                   port = portpermanent,
                   dbname = "nirse_ministerio")
  query <- dbSendQuery(con,
                       statement = "select
                                      *
                                    from
                                      dades_casos")
  dt.matching.casos.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  saveRDS(dt.matching.casos.raw, "D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/matching.casos.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.matching.casos.raw <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/matching.casos.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}
```

Informació importada:

- files n = `r nrow(dt.matching.casos.raw)`
- columnes n = `r ncol(dt.matching.casos.raw)`
- columnes: `r names(dt.matching.casos.raw)`

```{r}

names(dt.matching.casos.raw) <- tolower(names(dt.matching.casos.raw))
```

```{r}
# # Dates
#   # Data VRS
#   dt.casos.raw[, data_vrs_text := substring(data_vrs_text,1,9)]
#   dt.casos.raw[, dia := substr(data_vrs_text, 1, 2)]
#   
#   meses_abreviados <- c("JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")
#   dt.casos.raw[, mes := match(substr(data_vrs_text, 4, 6), meses_abreviados)]
# 
#   dt.casos.raw[, any := substr(data_vrs_text, 8, 9)]
# 
#   dt.casos.raw[, data_vrs := as.Date(paste0(dia, "-", mes, "-", any), format = "%d-%m-%y")]
# 
#   # Obtener las columnas que empiezan con "data_"
#     cols <- grep("^data_i", names(dt.casos.raw), value = TRUE)
# 
#   # Convertir las columnas de character a tipo date
#     dt.casos.raw[, (cols) := lapply(.SD, function(x) as.Date(x, format = "%Y-%m-%d")), .SDcols = cols]
```

```{r}
# dt.casos.raw <- dt.casos.raw[, .(hash,
#                                  hospital_codih,
#                                  data_ingres,
#                                  data_vrs,
#                                  virus,
#                                  viruses)]
```

#### Summary

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.matching.casos.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

***

### Controls

```{r}

if (params$actualitzar_dades) {
  ini <- Sys.time()
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idpermanent,
                   password = "", 
                   host = hostpermanent,
                   port = portpermanent,
                   dbname = "nirse_ministerio")
  query <- dbSendQuery(con,
                       statement = "select
                                      *
                                    from
                                      dades_controls")
  dt.matching.controls.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  saveRDS(dt.matching.controls.raw, "D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/matching.controls.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.matching.controls.raw <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/matching.controls.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}
```

Informació importada:

- files n = `r nrow(dt.matching.controls.raw)`
- columnes n = `r ncol(dt.matching.controls.raw)`
- columnes: `r names(dt.matching.controls.raw)`

```{r}

names(dt.matching.controls.raw) <- tolower(names(dt.matching.controls.raw))
```

```{r}
# # Dates
#   # Data VRS
#   dt.casos.raw[, data_vrs_text := substring(data_vrs_text,1,9)]
#   dt.casos.raw[, dia := substr(data_vrs_text, 1, 2)]
#   
#   meses_abreviados <- c("JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")
#   dt.casos.raw[, mes := match(substr(data_vrs_text, 4, 6), meses_abreviados)]
# 
#   dt.casos.raw[, any := substr(data_vrs_text, 8, 9)]
# 
#   dt.casos.raw[, data_vrs := as.Date(paste0(dia, "-", mes, "-", any), format = "%d-%m-%y")]
# 
#   # Obtener las columnas que empiezan con "data_"
#     cols <- grep("^data_i", names(dt.casos.raw), value = TRUE)
# 
#   # Convertir las columnas de character a tipo date
#     dt.casos.raw[, (cols) := lapply(.SD, function(x) as.Date(x, format = "%Y-%m-%d")), .SDcols = cols]
```

```{r}
# dt.casos.raw <- dt.casos.raw[, .(hash,
#                                  hospital_codih,
#                                  data_ingres,
#                                  data_vrs,
#                                  virus,
#                                  viruses)]
```

#### Summary

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.matching.controls.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

***
***

## Catàlegs

### Catèleg de Centres

S'importa el catàleg de centres de nodrizas.

```{r cat_centres}

drv <- dbDriver("MySQL")
con <- dbConnect(drv,
                 user = idsisap,
                 password = "", 
                 host = hostsisap,
                 port = portsisap,
                 dbname = "nodrizas")
query <- dbSendQuery(con,
                     statement = "select
                                    ep,
                                    amb_codi, amb_desc,
                                    sap_codi, sap_desc,
                                    scs_codi, ics_codi, ics_desc,
                                    tip_eap, adults, nens,
                                    medea, aquas
                                  from
                                    cat_centres")
dt.cat_centres.raw <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)

setnames(dt.cat_centres.raw, "ep", "eap_ep")
setnames(dt.cat_centres.raw, "medea", "medea_c")
#setnames(dt.cat_centres.raw, "ics_desc", "up_origen_desc")
```

Informació importada:
 
 - files n = `r nrow(dt.cat_centres.raw)`
 - columnes n = `r ncol(dt.cat_centres.raw)`
 - columnes: `r names(dt.cat_centres.raw)`

```{r}
dt.cat_centres.raw[amb_desc == 'distinct', amb_desc := 'TARRAGONA']
```

#### Summary

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.cat_centres.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

***

### Catèleg de Nacionalitats

S'importa el catàleg de nacionalitats de nodrizas.

```{r cataleg nacionalitat}

drv <- dbDriver("MySQL")
con <- dbConnect(drv,
                 user = idsisap,
                 password = "", 
                 host = hostsisap,
                 port = portsisap,
                 dbname = "nodrizas")
query <- dbSendQuery(con,
                     statement = "select
                                    *
                                  from
                                    cat_nacionalitat")
dt.cat_nacionalitat.raw <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)
```

Informació importada:
 
 - files n = `r nrow(dt.cat_nacionalitat.raw)`
 - columnes n = `r ncol(dt.cat_nacionalitat.raw)`
 - columnes: `r names(dt.cat_nacionalitat.raw)`


#### Summary

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.cat_nacionalitat.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

***

### Catèleg de Hospital de Referència SISAP

S'importa el catàleg de Hospital de Referència fet per SISAP.

```{r}
if (params$actualitzar_dades) {
  ini <- Sys.time()
  driver_path = "C:/Users/ehermosilla/ojdbc8.jar"
  driver = JDBC("oracle.jdbc.driver.OracleDriver", driver_path)
  service_name  = "excdox01srv"
  dsn = paste0("jdbc:oracle:thin:@//", hostexadata, ":", portexadata, "/", service_name)
  connection = dbConnect(driver,
                         dsn,
                         idexadata,
                         pwexadata)
  query = "SELECT
              abs_cod,
              hos_inf_cod,
              hos_inf_des
           FROM
              dwsisap.dbc_centres"
  dt.cat_hospital_referencia.raw = data.table(dbGetQuery(connection,
                                              query))
  var.disconnect <- dbDisconnect(connection)
  saveRDS(dt.cat_hospital_referencia.raw, "D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/dt.dbs.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.cat_hospital_referencia.raw <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/ministeri/data/dt.dbs.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}
```

Informació importada:
 
 - files n = `r nrow(dt.cat_hospital_referencia.raw)`
 - columnes n = `r ncol(dt.cat_hospital_referencia.raw)`
 - columnes: `r names(dt.cat_hospital_referencia.raw)`

```{r}
names(dt.cat_hospital_referencia.raw) <- tolower(names(dt.cat_hospital_referencia.raw))
```

#### Summary

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.cat_hospital_referencia.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```


### Hospital SIVIC

```{r}
# 00718 H08000307	Hospital Clínic de Barcelona
# 00634 H08000875	Hospital de Sant Joan de Déu
# 00148 H08000924	Hospital Universitari de Bellvitge
# 00272 H08002022	Hospital Universitari Germans Trias i Pujol de Badalona
# 00146 H08002142	Hospital de Viladecans
# 06046 H08810319	Hospital Universitari Vall d'Hebron 
# 00100 H17001484	Hospital Universitari de Girona Dr. Josep Trueta
# 00001 H25001621	Hospital Universitari Arnau de Vilanova de Lleida
# 00039 H43001903	Hospital Universitari Joan XXIII de Tarragona
# 00086 H43001974	Hospital de Tortosa Verge de la Cinta

dt.cat.sivic.hospital.raw <- data.table(hosp_cod = c('00718','00765','00148','00272','00146',
                                                     '06046','00100','00001','00039','00086'),
                                        hosp_cod_spublica = c('H08000307','H08000875','H08000924','H08002022','H08002142',
                                                              'H08810319','H17001484','H25001621','H43001903','H43001974'),
                                        hosp_des_spublica = c("Hospital Clínic de Barcelona",
                                                              "Hospital de Sant Joan de Déu (Esplugues de Llobregat)",
                                                              "Hospital Universitari de Bellvitge",
                                                              "Hospital Universitari Germans Trias i Pujol de Badalona",
                                                              "Hospital de Viladecans",
                                                              "Hospital Universitari Vall d'Hebron",
                                                              "Hospital Universitari de Girona Dr. Josep Trueta",
                                                              "Hospital Universitari Arnau de Vilanova de Lleida",
                                                              "Hospital Universitari Joan XXIII de Tarragona",
                                                              "Hospital de Tortosa Verge de la Cinta"))
```

```{r}
gc.var <- gc()
fi <- Sys.time()
fi - ini
```

