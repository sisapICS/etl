# Aquest script és per aconseguir 9 nous controls, ja que a la 1ª versió tenim 9 controls repetits

dt.cas <- dt.ini[cas == 'Cas' & data_naixement < as.Date('2023-10-01', '%Y-%m-%d'),]
dt.control <- dt.ini[cas != 'Cas' & data_naixement < as.Date('2023-10-01', '%Y-%m-%d') & bronquiolitis == 'No',]

set.seed(123456)

cas <- c()
controls <- c()

for (i in 1:nrow(dt.cas)) {
  aux <- dt.cas[i, .(hash, provincia, medea_c2, data_naixement, sexe, hospital_codih)]
  elegibles <- dt.control[(data_naixement >= aux$data_naixement - 2) & (data_naixement <= aux$data_naixement + 2)
                                   & hos_inf_cod == aux$hospital_codih
                                   & provincia == aux$provincia]
  elegibles <- elegibles[!elegibles$hash %in% unlist(controls)]
  if (nrow(elegibles) > 0) {
    controls[[dt.cas[i]$hash]] <- elegibles[sample(1:.N, 4, replace = F)]$hash
    cas[[dt.cas[i]$hash]] <- dt.cas[i]$hash
  } else {
    print(i)
    elegibles <- dt.control[(data_naixement >= aux$data_naixement - 2) & (data_naixement <= aux$data_naixement + 2)
                                      & provincia == aux$provincia]
    #elegibles <- elegibles[!elegibles$hash %in% unlist(controls)]
    controls[[dt.cas[i]$hash]] <- elegibles[sample(1:.N, 4, replace = F)]$hash
    cas[[dt.cas[i]$hash]] <- dt.cas[i]$hash
  }
} 
controls
cas

dt.con <- data.table(as.data.frame(controls))
setnames(dt.con, c('cas1','cas2','cas3','cas4','cas5','cas6','cas7','cas8','cas9','cas10',
                     'cas11','cas12','cas13','cas14','cas15','cas16','cas17','cas18','cas19','cas20',
                     'cas21','cas22','cas23','cas24','cas25','cas26','cas27','cas28','cas29','cas30',
                     'cas31','cas32','cas33','cas34','cas35','cas36','cas37','cas38','cas39','cas40'))
dt.con[, cc := 'control']
dt.con <- melt(dt.con, id.vars = c("cc"),
               measure.vars = c('cas1','cas2','cas3','cas4','cas5','cas6','cas7','cas8','cas9','cas10',
                                'cas11','cas12','cas13','cas14','cas15','cas16','cas17','cas18','cas19','cas20',
                                'cas21','cas22','cas23','cas24','cas25','cas26','cas27','cas28','cas29','cas30',
                                'cas31','cas32','cas33','cas34','cas35','cas36','cas37','cas38','cas39','cas40'))
setnames(dt.con, c('cc', 'variable', 'hash_control'))
dt.con

dt.m.cas <- data.table(as.data.frame(cas))
setnames(dt.m.cas, c('cas1','cas2','cas3','cas4','cas5','cas6','cas7','cas8','cas9','cas10',
                   'cas11','cas12','cas13','cas14','cas15','cas16','cas17','cas18','cas19','cas20',
                   'cas21','cas22','cas23','cas24','cas25','cas26','cas27','cas28','cas29','cas30',
                   'cas31','cas32','cas33','cas34','cas35','cas36','cas37','cas38','cas39','cas40'))
dt.m.cas[, cc := 'cas']
dt.m.cas <- melt(dt.m.cas, id.vars = c("cc"),
               measure.vars = c('cas1','cas2','cas3','cas4','cas5','cas6','cas7','cas8','cas9','cas10',
                                'cas11','cas12','cas13','cas14','cas15','cas16','cas17','cas18','cas19','cas20',
                                'cas21','cas22','cas23','cas24','cas25','cas26','cas27','cas28','cas29','cas30',
                                'cas31','cas32','cas33','cas34','cas35','cas36','cas37','cas38','cas39','cas40'))
setnames(dt.m.cas, c('cc', 'variable', 'hash_cas'))
dt.m.cas

dt.fin <- merge(dt.m.cas[, .(variable, hash_cas)],
                dt.con[, .(variable, hash_control)],
                by = 'variable')
dt.fin
a <- dt.fin[, .N, hash_cas]
b <- dt.fin[, .N, hash_control]

dt.matching.old <- readRDS("D:/SISAP/sisap/66Analisis/nirsevimab/data/backup/matching_hand_controls.rds")

a <- dt.tot[cas == 'Cas', .N, hash]
b <- dt.tot[cas == 'Control', .N, hash]






z <- merge(dt.matching.control,
           dt.fin,
           by.x = c('hash'),
           by.y = c('hash_control'),
           all.x = TRUE)
z <- z[is.na(hash_cas),]


dt.fin[variable == 'cas1',]
dt.matching.control[idcas == 1,]

dt.fin[variable == 'cas11',]
dt.fin[variable == 'cas9',]

# ----------------------------------------------------------------------------------------------------------------

dt.cas <- dt.ini[cas == 'Cas' & data_naixement >= as.Date('2023-10-01', '%Y-%m-%d'),]
dt.control <- dt.ini[cas != 'Cas' & data_naixement >= as.Date('2023-10-01', '%Y-%m-%d') & bronquiolitis == 'No',]

set.seed(123456)

cas <- c()
controls <- c()

for (i in 1:nrow(dt.cas)) {
  aux <- dt.cas[i, .(hash, provincia, medea_c2, data_naixement, sexe, hospital_codih)]
  elegibles <- dt.control[(data_naixement >= aux$data_naixement - 2) & (data_naixement <= aux$data_naixement + 2)
                          & hos_inf_cod == aux$hospital_codih
                          & provincia == aux$provincia]
  elegibles <- elegibles[!elegibles$hash %in% unlist(controls)]
  if (nrow(elegibles) > 0) {
    controls[[dt.cas[i]$hash]] <- elegibles[sample(1:.N, 4, replace = F)]$hash
    cas[[dt.cas[i]$hash]] <- dt.cas[i]$hash
  } else {
    print(i)
    elegibles <- dt.control[(data_naixement >= aux$data_naixement - 2) & (data_naixement <= aux$data_naixement + 2)
                            & provincia == aux$provincia]
    #elegibles <- elegibles[!elegibles$hash %in% unlist(controls)]
    controls[[dt.cas[i]$hash]] <- elegibles[sample(1:.N, 4, replace = F)]$hash
    cas[[dt.cas[i]$hash]] <- dt.cas[i]$hash
  }
} 
controls
cas

dt.con <- data.table(as.data.frame(controls))
setnames(dt.con, c('cas41','cas42','cas43','cas44','cas45','cas46','cas47','cas48','cas49','cas50',
                   'cas51','cas52','cas53','cas54','cas55','cas56','cas57','cas58','cas59','cas60',
                   'cas61','cas62','cas63','cas64','cas65','cas66','cas67','cas68','cas69','cas70',
                   'cas71','cas72','cas73','cas74','cas75','cas76','cas77','cas78','cas79','cas80',
                   'cas81','cas82','cas83','cas84','cas85','cas86','cas87','cas88'))
dt.con[, cc := 'control']
dt.con <- melt(dt.con, id.vars = c("cc"),
               measure.vars = c('cas41','cas42','cas43','cas44','cas45','cas46','cas47','cas48','cas49','cas50',
                                'cas51','cas52','cas53','cas54','cas55','cas56','cas57','cas58','cas59','cas60',
                                'cas61','cas62','cas63','cas64','cas65','cas66','cas67','cas68','cas69','cas70',
                                'cas71','cas72','cas73','cas74','cas75','cas76','cas77','cas78','cas79','cas80',
                                'cas81','cas82','cas83','cas84','cas85','cas86','cas87','cas88'))
setnames(dt.con, c('cc', 'variable', 'hash_control'))
dt.con

dt.m.cas <- data.table(as.data.frame(cas))
setnames(dt.m.cas, c('cas41','cas42','cas43','cas44','cas45','cas46','cas47','cas48','cas49','cas50',
                     'cas51','cas52','cas53','cas54','cas55','cas56','cas57','cas58','cas59','cas60',
                     'cas61','cas62','cas63','cas64','cas65','cas66','cas67','cas68','cas69','cas70',
                     'cas71','cas72','cas73','cas74','cas75','cas76','cas77','cas78','cas79','cas80',
                     'cas81','cas82','cas83','cas84','cas85','cas86','cas87','cas88'))
dt.m.cas[, cc := 'cas']
dt.m.cas <- melt(dt.m.cas, id.vars = c("cc"),
                 measure.vars = c('cas41','cas42','cas43','cas44','cas45','cas46','cas47','cas48','cas49','cas50',
                                  'cas51','cas52','cas53','cas54','cas55','cas56','cas57','cas58','cas59','cas60',
                                  'cas61','cas62','cas63','cas64','cas65','cas66','cas67','cas68','cas69','cas70',
                                  'cas71','cas72','cas73','cas74','cas75','cas76','cas77','cas78','cas79','cas80',
                                  'cas81','cas82','cas83','cas84','cas85','cas86','cas87','cas88'))
setnames(dt.m.cas, c('cc', 'variable', 'hash_cas'))
dt.m.cas

dt.fin <- merge(dt.m.cas[, .(variable, hash_cas)],
                dt.con[, .(variable, hash_control)],
                by = 'variable')
dt.fin
a <- dt.fin[, .N, hash_cas]
b <- dt.fin[, .N, hash_control]

dt.fin[variable == 'cas69',]


# LLista de nous hash controls
# DF3B5C983BA501DA571A609A559201EE3A7DA2B1 9_3
# 48F01F7AFC21B57FE6CF014330164F6B80DB2AB7 11_3
# 098C17964E9B5BFA4830F3099A71EDEA56E5ADA7 20_2
# 3451600DB5C41C838307151F08C978375689C04C 47_3
# 39AF47D21F30DE7BEC292CBFFA8329F06AD550FE 49_3
# 7737A20876E24C8CEB7A72E4DF26480CFA98A707 69_2
# AB9C49952D25BC1870C9EE64325B41B803983A8A 86_1
# 9B8BD0BF6107791CCB0006E4ECE7440D59BE0782 86_2
# 2D558BA07F62BD4BA1874430CE767D4FB39D900F 86_3

hash.nous.controls <- c('DF3B5C983BA501DA571A609A559201EE3A7DA2B1',
                        '48F01F7AFC21B57FE6CF014330164F6B80DB2AB7',
                        '098C17964E9B5BFA4830F3099A71EDEA56E5ADA7',
                        '3451600DB5C41C838307151F08C978375689C04C',
                        '39AF47D21F30DE7BEC292CBFFA8329F06AD550FE',
                        '7737A20876E24C8CEB7A72E4DF26480CFA98A707',
                        'AB9C49952D25BC1870C9EE64325B41B803983A8A',
                        '9B8BD0BF6107791CCB0006E4ECE7440D59BE0782',
                        '2D558BA07F62BD4BA1874430CE767D4FB39D900F')

dt.nous.controls <- dt.ini[hash %in% hash.nous.controls, .(hash, provincia, data_ingres, data_vrs)]
dt.nous.controls[, cas := 'Control']

dt.nous.controls[hash == 'DF3B5C983BA501DA571A609A559201EE3A7DA2B1', idcas := 9]
dt.nous.controls[hash == 'DF3B5C983BA501DA571A609A559201EE3A7DA2B1', id := '9_3']

dt.nous.controls[hash == '48F01F7AFC21B57FE6CF014330164F6B80DB2AB7', idcas := 11]
dt.nous.controls[hash == '48F01F7AFC21B57FE6CF014330164F6B80DB2AB7', id := '11_3']

dt.nous.controls[hash == '098C17964E9B5BFA4830F3099A71EDEA56E5ADA7', idcas := 20]
dt.nous.controls[hash == '098C17964E9B5BFA4830F3099A71EDEA56E5ADA7', id := '20_2']

dt.nous.controls[hash == '3451600DB5C41C838307151F08C978375689C04C', idcas := 47]
dt.nous.controls[hash == '3451600DB5C41C838307151F08C978375689C04C', id := '47_3']

dt.nous.controls[hash == '39AF47D21F30DE7BEC292CBFFA8329F06AD550FE', idcas := 49]
dt.nous.controls[hash == '39AF47D21F30DE7BEC292CBFFA8329F06AD550FE', id := '49_3']

dt.nous.controls[hash == '7737A20876E24C8CEB7A72E4DF26480CFA98A707', idcas := 69]
dt.nous.controls[hash == '7737A20876E24C8CEB7A72E4DF26480CFA98A707', id := '69_2']

dt.nous.controls[hash == 'AB9C49952D25BC1870C9EE64325B41B803983A8A', idcas := 86]
dt.nous.controls[hash == 'AB9C49952D25BC1870C9EE64325B41B803983A8A', id := '86_1']

dt.nous.controls[hash == '9B8BD0BF6107791CCB0006E4ECE7440D59BE0782', idcas := 86]
dt.nous.controls[hash == '9B8BD0BF6107791CCB0006E4ECE7440D59BE0782', id := '86_2']

dt.nous.controls[hash == '2D558BA07F62BD4BA1874430CE767D4FB39D900F', idcas := 86]
dt.nous.controls[hash == '2D558BA07F62BD4BA1874430CE767D4FB39D900F', id := '86_3']

dt.nous.controls <- dt.nous.controls[, .(hash, cas, idcas, id, provincia, data_ingres, data_vrs)]
dt.nous.controls[order(idcas, id),]


## Creació taula a bbdd nirse_ministerio

suppressWarnings(suppressPackageStartupMessages(library('RMySQL')))
source("C:/Users/ehermosilla/Documents/Keys.R")
drv <- dbDriver("MySQL")
con <- dbConnect(drv,
                 user = idpermanent,
                 password = "",
                 host = hostpermanent,
                 port = portpermanent,
                 dbname = "nirse_ministerio")
tabla <- "CREATE TABLE matching_9nouscontrols (
            hash VARCHAR(40),
            cas VARCHAR(10),
            idcas INT,
            id VARCHAR(5),
            provincia VARCHAR(15),
            data_ingres DATE,
            data_vrs DATE
         )"
dbSendQuery(con, tabla)
dbWriteTable(con, "matching_9nouscontrols", dt.nous.controls[, .(hash, cas, idcas, id, provincia, data_ingres, data_vrs)], row.names = FALSE, append = TRUE)
dbDisconnect(con)

