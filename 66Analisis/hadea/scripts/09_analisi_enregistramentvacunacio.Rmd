
# Anàlisi Enregistrament/Vacunació {.tabset}

```{r}
dt.cohort.flt <- readRDS("../data/dt.cohort.flt.rds")
```

## Cohort 4 anys {.tabset}

```{r}
dt.ana <- dt.cohort.flt[cohort == 'Cohort 4 anys' & enviar == 'Sí' & !is.na(missatge_c),]
dt.ana[, .N, enregistratvacunat]
```

### Tosts els enviaments {.tabset}

#### Taula Resum

```{r}
df <- dt.ana
res <- compareGroups(enregistratvacunat ~ . -hash -eap -sector -c_up -c_up_origen -c_up_abs -ics_desc -eap_ep -c_data_naix -c_nacionalitat
                     , df
                     , max.xlev = 100
                     , include.miss = TRUE
                     , byrow = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = TRUE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Comparació ENREGISTRAMENT/VACUNACIÓ - Cohort 4 ANYS - Taula RESUM")
```

#### Temps fins Enregistrament o Vacuanció {.tabset}

##### Sense Censurats

```{r}
dt.survival <- dt.ana[enregistratvacunat == 'Sí' & !is.na(missatge_c) & enregistratvacunat_temps >= 0,]
a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
a
#summary(a)
ggsurvplot(a,
           pval = TRUE,
           conf.int = TRUE)
```

##### Amb censurats

```{r}
dt.survival <- dt.ana[!is.na(missatge_c),]
a <- dt.survival[, .N, enregistratvacunat_temps]

dt.survival[enregistratvacunat == 'No', enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[!is.na(missatge_c) & enregistratvacunat_temps >= 0,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.05*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.05*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```

##### Amb censura a x dies {.tabset}

###### 15 dies

```{r}
val <- 15
dt.survival <- dt.ana[!is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat := 'No']
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat_temps := val]
dt.survival <- dt.survival[enregistratvacunat_temps <= val,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.01*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.01*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```

###### 30 dies

```{r}
val <- 30
dt.survival <- dt.ana[!is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat := 'No']
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat_temps := val]
dt.survival <- dt.survival[enregistratvacunat_temps <= val,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.01*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.01*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```

###### 45 dies

```{r}
val <- 45
dt.survival <- dt.ana[!is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat := 'No']
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat_temps := val]
dt.survival <- dt.survival[enregistratvacunat_temps <= val,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.01*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.01*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```

###### 60 dies

```{r}
val <- 60
dt.survival <- dt.ana[!is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat := 'No']
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat_temps := val]
dt.survival <- dt.survival[enregistratvacunat_temps <= val,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.01*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.01*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```

### Enviament 2020-05 {.tabset}

#### Taula Resum

```{r}
df <- dt.ana[referencia == '2020-05',]
res <- compareGroups(enregistratvacunat ~ . -hash -eap -sector -c_up -c_up_origen -c_up_abs -ics_desc -eap_ep -c_data_naix -c_nacionalitat
                     , df
                     , max.xlev = 100
                     , include.miss = TRUE
                     , byrow = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = TRUE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Comparació ENREGISTRAMENT/VACUNACIÓ - Cohort 4 ANYS - Enviament 2020-05 - Taula RESUM")
```

#### Temps fins Enregistrament o Vacuanció {.tabset}

##### Sense Censurats

```{r}
dt.survival <- dt.ana[referencia == '2020-05' & enregistratvacunat == 'Sí' & !is.na(missatge_c) & enregistratvacunat_temps >= 0,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
a
#summary(a)
ggsurvplot(a,
           pval = TRUE,
           conf.int = TRUE)
```

##### Amb censurats {.tabset}

```{r}
dt.survival <- dt.ana[referencia == '2020-05' & !is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[!is.na(missatge_c) & enregistratvacunat_temps >= 0,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.05*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.05*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```

##### Amb censura a x dies {.tabset}

###### 15 dies

```{r}
val <- 15
dt.survival <- dt.ana[referencia == '2020-05' & !is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat := 'No']
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat_temps := val]
dt.survival <- dt.survival[enregistratvacunat_temps <= val,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.01*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.01*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```

###### 30 dies

```{r}
val <- 30
dt.survival <- dt.ana[referencia == '2020-05' & !is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat := 'No']
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat_temps := val]
dt.survival <- dt.survival[enregistratvacunat_temps <= val,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.01*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.01*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```

###### 45 dies

```{r}
val <- 45
dt.survival <- dt.ana[referencia == '2020-05' & !is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat := 'No']
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat_temps := val]
dt.survival <- dt.survival[enregistratvacunat_temps <= val,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.01*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.01*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```

###### 60 dies

```{r}
val <- 60
dt.survival <- dt.ana[referencia == '2020-05' & !is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat := 'No']
dt.survival <- dt.survival[enregistratvacunat_temps > val, enregistratvacunat_temps := val]
dt.survival <- dt.survival[enregistratvacunat_temps <= val,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.01*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.01*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```

### Enviament 2020-06 {.tabset}

#### Taula Resum

```{r}
df <- dt.ana[referencia == '2020-06',]
res <- compareGroups(enregistratvacunat ~ . -hash -eap -sector -c_up -c_up_origen -c_up_abs -ics_desc -eap_ep -c_data_naix -c_nacionalitat
                     , df
                     , max.xlev = 100
                     , include.miss = TRUE
                     , byrow = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = TRUE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Comparació ENREGISTRAMENT/VACUNACIÓ - Cohort 4 ANYS - Enviament 2020-06 - Taula RESUM")
```

#### Temps fins Enregistrament o Vacuanció {.tabset}

##### Sense Censurats

```{r}
dt.survival <- dt.ana[referencia == '2020-06' & enregistratvacunat == 'Sí' & !is.na(missatge_c) & enregistratvacunat_temps >= 0,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
a
#summary(a)
ggsurvplot(a,
           pval = TRUE,
           conf.int = TRUE)
```

##### Amb censurats

```{r}
dt.survival <- dt.ana[referencia == '2020-06' & !is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[!is.na(missatge_c) & enregistratvacunat_temps >= 0,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.05*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.05*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```

### Enviament 2020-07 {.tabset}

#### Taula Resum

```{r}
df <- dt.ana[referencia == '2020-07',]
res <- compareGroups(enregistratvacunat ~ . -hash -eap -sector -c_up -c_up_origen -c_up_abs -ics_desc -eap_ep -c_data_naix -c_nacionalitat
                     , df
                     , max.xlev = 100
                     , include.miss = TRUE
                     , byrow = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = TRUE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Comparació ENREGISTRAMENT/VACUNACIÓ - Cohort 4 ANYS - Enviament 2020-07 - Taula RESUM")
```

#### Temps fins Enregistrament o Vacuanció {.tabset}

##### Sense Censurats

```{r}
dt.survival <- dt.ana[referencia == '2020-07' & enregistratvacunat == 'Sí' & !is.na(missatge_c) & enregistratvacunat_temps >= 0,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
a
#summary(a)
ggsurvplot(a,
           pval = TRUE,
           conf.int = TRUE)
```

##### Amb censurats

```{r}
dt.survival <- dt.ana[referencia == '2020-07' & !is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[!is.na(missatge_c) & enregistratvacunat_temps >= 0,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.05*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.05*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```

### Enviament 2020-08 {.tabset}

#### Taula Resum

```{r}
df <- dt.ana[referencia == '2020-08',]
res <- compareGroups(enregistratvacunat ~ . -hash -eap -sector -c_up -c_up_origen -c_up_abs -ics_desc -eap_ep -c_data_naix -c_nacionalitat
                     , df
                     , max.xlev = 100
                     , include.miss = TRUE
                     , byrow = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = TRUE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Comparació ENREGISTRAMENT/VACUNACIÓ - Cohort 4 ANYS - Enviament 2020-08 - Taula RESUM")
```

#### Temps fins Enregistrament o Vacuanció {.tabset}

##### Sense Censurats

```{r}
dt.survival <- dt.ana[referencia == '2020-08' & enregistratvacunat == 'Sí' & !is.na(missatge_c) & enregistratvacunat_temps >= 0,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
a
#summary(a)
ggsurvplot(a,
           pval = TRUE,
           conf.int = TRUE)
```

##### Amb censurats

```{r}
dt.survival <- dt.ana[referencia == '2020-08' & !is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[!is.na(missatge_c) & enregistratvacunat_temps >= 0,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.05*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.05*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```


### Enviament 2020-09 {.tabset}

#### Taula Resum

```{r}
df <- dt.ana[referencia == '2020-09',]
res <- compareGroups(enregistratvacunat ~ . -hash -eap -sector -c_up -c_up_origen -c_up_abs -ics_desc -eap_ep -c_data_naix -c_nacionalitat
                     , df
                     , max.xlev = 100
                     , include.miss = TRUE
                     , byrow = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = TRUE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Comparació ENREGISTRAMENT/VACUNACIÓ - Cohort 4 ANYS - Enviament 2020-09 - Taula RESUM")
```

#### Temps fins Enregistrament o Vacuanció {.tabset}

##### Sense Censurats

```{r}
dt.survival <- dt.ana[referencia == '2020-09' & enregistratvacunat == 'Sí' & !is.na(missatge_c) & enregistratvacunat_temps >= 0,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
a
#summary(a)
ggsurvplot(a,
           pval = TRUE,
           conf.int = TRUE)
```

##### Amb censurats

```{r}
dt.survival <- dt.ana[referencia == '2020-09' & !is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[!is.na(missatge_c) & enregistratvacunat_temps >= 0,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.06*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.05*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```


### Enviament 2020-10 {.tabset}

#### Taula Resum

```{r}
df <- dt.ana[referencia == '2020-10',]
res <- compareGroups(enregistratvacunat ~ . -hash -eap -sector -c_up -c_up_origen -c_up_abs -ics_desc -eap_ep -c_data_naix -c_nacionalitat
                     , df
                     , max.xlev = 100
                     , include.miss = TRUE
                     , byrow = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = TRUE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Comparació ENREGISTRAMENT/VACUNACIÓ - Cohort 4 ANYS - Enviament 2020-10 - Taula RESUM")
```

#### Temps fins Enregistrament o Vacuanció {.tabset}

##### Sense Censurats

```{r}
dt.survival <- dt.ana[referencia == '2020-10' & enregistratvacunat == 'Sí' & !is.na(missatge_c) & enregistratvacunat_temps >= 0,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
a
#summary(a)
ggsurvplot(a,
           pval = TRUE,
           conf.int = TRUE)
```

##### Amb censurats

```{r}
dt.survival <- dt.ana[referencia == '2020-10' & !is.na(missatge_c),]
dt.survival[is.na(enregistratvacunat_temps), enregistratvacunat_temps := as.numeric(Sys.Date() - data_enviament)]
dt.survival <- dt.survival[!is.na(missatge_c) & enregistratvacunat_temps >= 0,]

a <- survfit(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c, data = dt.survival)
ggsurvplot(a,
           ylim = c(min(a$surv) - (0.05*min(a$surv)), 1),
           pval = TRUE,
           pval.coord = c(1, min(a$surv) - (0.05*min(a$surv))),
           conf.int = TRUE
           )
surv_diff <- survdiff(Surv(enregistratvacunat_temps, enregistratvacunat == 'Sí') ~ missatge_c,
                      data = dt.survival)
surv_diff
```

## Cohort 10 anys {.tabset}

### Enviat {.tabset}

```{r}
dt.ana <- dt.cohort.flt[cohort == 'Cohort 5-10 anys' & enviar == 'Sí' & !is.na(missatge_c),]
```

#### Taula Resum

```{r}
df <- dt.ana
res <- compareGroups(enregistratvacunat ~ . -hash -eap -sector -c_up -c_up_origen -c_up_abs -ics_desc -eap_ep -c_data_naix -c_nacionalitat
                     , df
                     , max.xlev = 100
                     , include.miss = TRUE
                     , byrow = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = TRUE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Comparació ENREGISTRAMENT/VACUNACIÓ - Cohort 5-10 ANYS - Taula RESUM")
```

```{r}
#rm(list=c("dt.biv","df","res","cT"))
gc.var <- gc()
```


