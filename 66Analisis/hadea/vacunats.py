# -*- coding: latin1 -*-

"""
HADEA
"""

import collections as c
import psutil as p
import datetime as d
import difflib as s

import sisapUtils as u


REFERENCIA = d.datetime(2024, 5, 26)

referencies_mesos = {'2020-06': {'dat': d.datetime(2024, 6, 30), 'calcul': d.datetime(2024, 7, 1), 'enviament': d.datetime(2024, 7, 3)},
                     '2020-07': {'dat': d.datetime(2024, 8, 4), 'calcul': d.datetime(2024, 8, 5), 'enviament': d.datetime(2024, 8, 7)},
                     '2020-08': {'dat':d.datetime(2024, 9, 8), 'calcul': d.datetime(2024, 9, 9), 'enviament': d.datetime(2024, 9, 11)},
                     '2020-09': {'dat':d.datetime(2024, 10, 6), 'calcul': d.datetime(2024, 10, 7), 'enviament': d.datetime(2024, 10, 9)},
                     '2020-10': {'dat':d.datetime(2024, 11, 10), 'calcul': d.datetime(2024, 11, 11), 'enviament': d.datetime(2024, 11, 13)}}

class HADEA(object):
    """."""

    def __init__(self):
        """."""
        self.get_hash()
        self.get_vacunes()
        self.get_exclusions()
        self.get_poblacio()
        self.upload_data()

    def get_hash(self):
        """hash a partir del nia"""
        u.printTime("hashos")
        self.hashos = {}

        sql = """select nia, hash from dwsisap.dbc_poblacio"""
        for nia, hash in u.getAll(sql, 'exadata'):
            self.hashos[nia] = hash
            
            
    def get_vacunes(self):
        """."""
        u.printTime("vacunats ecap")
        self.vacunes = c.defaultdict(lambda: c.defaultdict(set))
        self.vacunes_mensual = c.defaultdict(lambda: c.defaultdict(set))
        for sector in u.sectors:
            print sector
            sql = "select c.usua_nia, a.va_u_cod_an, a.va_u_data_alta, a.va_u_data_vac, a.va_u_dosi \
                   from prstb051 a \
                   inner join usutb011 b on a.va_u_usua_cip = b.cip_cip_anterior \
                   inner join usutb040 c on b.cip_usuari_cip = c.usua_cip \
                   where a.va_u_cod_an in ('A00022') and \
                         a.va_u_data_baixa is null"
            for nia, codi, dat_car, dat, dosi in u.getAll(sql, sector):
                if dat_car > REFERENCIA:
                    self.vacunes[nia][dat_car].add(dat.date())
        
        u.printTime("vacunats hc3")
        sql = "select pac_nia, tva_sid, vac_dad_trunc, data_carrega \
               from bislt_pro_h.ft_vac \
               where icl_bad is null and \
                     (vac_mnv = 'ND' or vac_mnv is null) and \
                     tva_sid in ('386012008')"
        for nia, codi, dat, dat_car in u.getAll(sql, 'exadata'):
            if dat_car > REFERENCIA:
                if nia in self.vacunes:
                    ok = 1
                else:
                    self.vacunes[nia][dat_car].add(dat.date())
        
        
    def get_exclusions(self):
        """."""
        u.printTime("exclusions problemes")
        agrupadors = (108, 109, 40, 557, 529, 677, 787, 39)
        sql = "select distinct criteri_codi, ps_tancat \
               from eqa_criteris \
               where agrupador in {}".format(agrupadors)
        codis = {codi: int(tancat) for (codi, tancat)
                 in u.getAll(sql, 'nodrizas')}
        
        self.exclusions = set()
        for sector in u.sectors:
            print sector
            sql = "select c.usua_nia, a.pr_cod_ps, decode(a.pr_dba, null, 0, 1) \
                   from prstb015 a \
                   inner join usutb011 b on a.pr_cod_u = b.cip_cip_anterior \
                   inner join usutb040 c on b.cip_usuari_cip = c.usua_cip \
                   where a.pr_cod_ps in {} and \
                         a.pr_data_baixa is null".format(tuple(codis))
        
            for nia, codi, tancat in u.getAll(sql, sector):
                if tancat == codis[codi]:
                    self.exclusions.add(nia)
                    
        u.printTime("exclusions immunitzats")
        sql = "select distinct criteri_codi \
               from eqa_criteris \
               where agrupador = 516"
        codis = [codi for codi,
                 in u.getAll(sql, 'nodrizas')]
        sql = "select c.usua_nia \
               from prstb052 a \
               inner join usutb011 b on a.imu_usua_cip = b.cip_cip_anterior \
               inner join usutb040 c on b.cip_usuari_cip = c.usua_cip \
               where a.imu_cod_ps in {} and \
                     a.imu_data_baixa is null".format(tuple(codis))
        for sector in u.sectors:
            print sector
            for nia, in u.getAll(sql, sector):
                self.exclusions.add(nia)

    def get_poblacio(self):
        """."""
        u.printTime("població")
        sql = "select nia, missatge_c \
               from dwsisap.hadea_raw \
               where missatge_c in (1,2,3)"
        self.upload = []
        for nia, missatge in u.getAll(sql, 'exadata'):  # noqa
            exclos = 1 * (nia in self.exclusions)
            hash = self.hashos[nia] if nia in self.hashos else None
            if nia in self.vacunes:
                for data_carrega in self.vacunes[nia]:
                    for dosis_data in self.vacunes[nia][data_carrega]:
                        self.upload.append([hash, missatge, data_carrega, dosis_data, exclos])
            if exclos == 1 and nia not in self.vacunes:
                self.upload.append([hash, missatge, None, None, exclos])
                
        sql = "select referencia, nia, missatge_c \
               from dwsisap.hadea_mensual a inner join dwsisap.rca_cip_nia rcn \
               on a.cip=rcn.cip"
        self.upload_mensual = []
        for referencia, nia, missatge in u.getAll(sql, 'exadata'):  # noqa
            exclos = 1 * (nia in self.exclusions)
            hash = self.hashos[nia] if nia in self.hashos else None
            ref = referencies_mesos[referencia]['dat']
            d_calcul = referencies_mesos[referencia]['calcul']
            d_enviament = referencies_mesos[referencia]['enviament']
            if nia in self.vacunes:
                for data_carrega in self.vacunes[nia]:
                    if data_carrega > ref:
                        for dosis_data in self.vacunes[nia][data_carrega]:
                            self.upload_mensual.append([referencia, d_calcul.date(), d_enviament.date(), hash, missatge, data_carrega.date(), dosis_data, exclos])
            if exclos == 1 and nia not in self.vacunes:
                self.upload_mensual.append([referencia, d_calcul.date(), d_enviament.date(), hash, missatge, None, None, exclos])

    def upload_data(self):
        """."""
        u.printTime("export")
        
        db = "hadea"
        tb = "vacunats"
        cols = ("hash varchar(40)", "missatge int", "data_alta date", "dosis_data date", "exclos int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        tb = "vacunats_mensual"
        cols = ("referencia varchar(10)", "data_calcul date", "data_enviament date", "hash varchar(40)", "missatge int", "data_alta date", "dosis_data date", "exclos int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload_mensual, tb, db)

if __name__ == "__main__":
    HADEA()
