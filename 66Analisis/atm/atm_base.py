# -*- coding: utf8 -*-

"""
ATM
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = 'sisap_odn_atm'
tb_ps = 'sisap_odn_atm_ps'
db = 'permanent'

codi_atm = 'C01-M26.609'

class melanoma(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_centres()
        self.get_atm()
        self.get_hashos()
        self.get_pob()
        self.get_gma()
        self.get_medea()
        self.get_problemes()
        self.export_dades()

    def get_centres(self):
        """EAP ICS."""
        u.printTime("centres")
        sql = ("select scs_codi, ics_codi \
                from cat_centres", "nodrizas")
        self.centres = {up: br for (up, br) in u.getAll(*sql)}

    def get_atm(self):
        """."""
        u.printTime("ATM")
        self.dx_atm = {}
        sql = "select id_cip_sec, pr_dde, pr_dba \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_cod_ps = '{}' and \
                            pr_data_baixa = 0".format(codi_atm)
        for id, dde, dba in u.getAll(sql, 'import'):
            self.dx_atm[id] = {'dde': dde, 'dba': dba}
            
    def get_hashos(self):
        """."""
        u.printTime("hash")
        self.idcip_to_hash = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        for id, sec, hash in u.getAll(sql, "import"):
            self.idcip_to_hash[id] = {'hash': hash, 'sec': sec} 
            
    def get_pob(self):
        """Agafem poblacio """
        u.printTime("pob")
        self.dades = {}
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = "select id_cip_sec, codi_sector, up, sexe, edat, nacionalitat, institucionalitzat from assignada_tot"
        for id, sec, up, sexe, edat, nac, insti in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
                idh = hash + ':' + sec
                nac1 = 1 if nac in renta else 0
                atm_dx = 1 if id in self.dx_atm else 0
                atm_dde = self.dx_atm[id]['dde'] if id in self.dx_atm else None
                atm_dba = self.dx_atm[id]['dba'] if id in self.dx_atm else None
                if atm_dx == 1:
                    self.dades[idh] = {
                              'id': id, 'up': up, 'sex': sexe,'edat': edat,
                              'renta': nac1, 'gma_cod': None, 'gma_cmplx': None,'gma_num': None, 'medea': None, 'insti': insti, 'dde': atm_dde, 
                              'dba': atm_dba}
  
    def get_gma(self):
        """Obté els tres valors de GMA."""
        u.printTime("gma")
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        for id, cod, cmplx, num in u.getAll(*sql):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            if idh in self.dades:
                self.dades[idh]['gma_cod'] = cod
                self.dades[idh]['gma_cmplx'] = cmplx
                self.dades[idh]['gma_num'] = num

    def get_medea(self):
        u.printTime("medea")
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            if idh in self.dades and sector in valors:
                self.dades[idh]['medea'] = valors[sector]
                
    def get_problemes(self):
        """."""
        self.upload_ps = []
        sql = "select id_cip_sec, pr_dde, pr_dba, pr_cod_ps \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_data_baixa = 0"
        for id, dde, dba, ps in u.getAll(sql, 'import'):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            if idh in self.dades:
                self.upload_ps.append([idh, id, dde, dba, ps])
    
    def export_dades(self):
        """."""
        u.printTime("export")
        upload = [(id, d['id'],d['up'], d['sex'], d['edat'],d['renta'], d['gma_cod'], d['gma_cmplx'],d['gma_num'], d['medea'], d['insti'], d['dde'],
                    d['dba'])
                 for id, d in self.dades.items()]

        columns = ["id varchar(100)", "id_sec int",   "up varchar(5)", "sexe varchar(1)",
                    "edat int", "nac1 int",  'gma_codi varchar(3)', 'gma_complexitat double',
                   'gma_num_croniques int', 'medea_seccio double',
                   'institucionalitzat int', 'atm_dde date', "atm_dba date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb, db)
        
        columns = ["id varchar(100)", "id_sec int",  'dde date', "dba date", "codi_cim varchar(40)"]
        u.createTable(tb_ps, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload_ps, tb_ps, db)
  
                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    melanoma()
    
    u.printTime("Final")   