### Peticions Jornada

Peticions de Manolo per a la Jornada del dia 27/10/2022.

```{r}
dt.matching.raw <- readRDS(paste0("D:/SISAP/sisap/66Analisis/Planificat/data/", "dt.matching_20221025", ".rds"))

dt.matching.raw <- dt.matching.raw[order(match, -planificat)]
dt.matching.raw[, fase1 := c(NA, fase[-.N]), by =  match][match_c == 0, fase1 := NA]
dt.matching.raw[is.na(fase1) , fase1 := as.numeric(fase)][match_c == 0, fase1 := NA]
dt.matching.raw[ , fase1 := factor(fase1, levels = c(1:5), labels = c("Fase 1 (Pilots)", "Fase 2", "Fase 3", "Fase 4", "Fase 5"))]
dt.matching.raw[, .N, .(fase, fase1)][order(fase1,fase),]
dt.matching.raw[, fase := NULL]
setnames(dt.matching.raw, "fase1", "fase")

dt.dbs.actual <- readRDS("D:/SISAP/sisap/66Analisis/Planificat/data/dbs_actual.rds")
setnames(dt.dbs.actual, tolower(names(dt.dbs.actual)))
dt.dbs.actual.sel <- dt.dbs.actual[, .(c_cip, v_col_total_data, v_ecg_amb_data, v_hba1c_data, v_hba1c_valor)]
dt.dbs.actual.sel[, v_col_total_post := 0][v_col_total_data >= as.Date('2022/02/01'), v_col_total_post := 1]
dt.dbs.actual.sel[, v_col_total_post := factor(v_col_total_post, levels = c(0:1), labels = c("No", "Sí"))]
dt.dbs.actual.sel[, v_ecg_amb_post := 0][v_ecg_amb_data >= as.Date('2022/02/01'), v_ecg_amb_post := 1]
dt.dbs.actual.sel[, v_ecg_amb_post := factor(v_ecg_amb_post, levels = c(0:1), labels = c("No", "Sí"))]
dt.dbs.actual.sel[, v_hba1c_post := 0][v_hba1c_data >= as.Date('2022/02/01'), v_hba1c_post := 1]
dt.dbs.actual.sel[, v_hba1c_post := factor(v_hba1c_post, levels = c(0:1), labels = c("No", "Sí"))]
dt.dbs.actual.sel[, v_hba1c_lt8_post := 0][v_hba1c_data >= as.Date('2022/02/01') & v_hba1c_valor < 8, v_hba1c_lt8_post := 1]
dt.dbs.actual.sel[, v_hba1c_lt8_post := factor(v_hba1c_lt8_post, levels = c(0:1), labels = c("No controlado", "Controlado"))]

dt.matching.raw.pet <- merge(dt.matching.raw,
                             dt.dbs.actual.sel,
                             by = 'c_cip',
                             all.x = TRUE)

dt.matching.raw.pet[, planificat := factor(planificat, levels = c(0:1), labels = c("Control", "Intervención"))]

dt.matching.raw.pet[fase == 'Fase 1 (Pilots)',
                    .N, planificat]

dt.matching.raw.pet[fase == 'Fase 1 (Pilots)' & 
                    match_c == 1, .N, planificat]

dt.matching.raw.pet[fase == 'Fase 1 (Pilots)' & 
                    match_c == 1 &
                    estat == "10 - Programat", .N, planificat]
```

#### Visites MG

```{r}
match_id <- unique(dt.matching.raw.pet[match_c == 1 &
                                       fase == 'Fase 1 (Pilots)' &
                                       estat == "10 - Programat" & 
                                       estat_data < as.Date('2022/05/01') &
                                       !is.na(v_col_total_post) & !is.na(v_ecg_amb_post) & !is.na(v_hba1c_post), match])
match_id <- dt.matching.raw.pet[match_c == 1 & match %in% match_id & !is.na(v_col_total_post) & !is.na(v_ecg_amb_post) & !is.na(v_hba1c_post), .N, match][N > 1]$match
df <- dt.matching.raw.pet[match_c == 1 & match %in% match_id, .(planificat, c_edat_anys, grup, estat, nvis_mg_realitzades_pre, nvis_mg_realitzades_post)]
cG <- compareGroups(planificat ~ nvis_mg_realitzades_pre + nvis_mg_realitzades_post +
                                 c_edat_anys + grup + estat,
                    df, 
                    max.xlev = 20)
cT <- createTable(cG, show.descr = TRUE, show.all = FALSE)
export2md(cT
          , caption = ""
          , format = "html")
```

#### Variables

```{r}
match_id <- unique(dt.matching.raw.pet[match_c == 1 & fase == 'Fase 1 (Pilots)' & estat == "10 - Programat" & estat_data < as.Date('2022/05/01') & !is.na(v_col_total_post) & !is.na(v_ecg_amb_post) & !is.na(v_hba1c_post), match])
match_id <- dt.matching.raw.pet[match_c == 1 & match %in% match_id & !is.na(v_col_total_post) & !is.na(v_ecg_amb_post) & !is.na(v_hba1c_post), .N, match][N > 1]$match
df <- dt.matching.raw.pet[match_c == 1 & match %in% match_id, .(planificat, v_col_total_post, v_ecg_amb_post, v_hba1c_post)]
cG <- compareGroups(planificat ~ v_col_total_post + v_ecg_amb_post + v_hba1c_post,
                    df, 
                    max.xlev = 20)
cT <- createTable(cG, show.descr = TRUE, show.all = FALSE, hide.no = "No")
export2md(cT
          , caption = ""
          , format = "html")
```

#### Diabetics

```{r}
match_id <- unique(dt.matching.raw.pet[match_c == 1 &
                                       fase == 'Fase 1 (Pilots)' &
                                       estat == "10 - Programat" &
                                       estat_data < as.Date('2022/05/01') &
                                      !is.na(v_col_total_post) & !is.na(v_ecg_amb_post) & !is.na(v_hba1c_post) &
                                       grup %in% c("1 - IC (amb o sense CI) amb DM2 i MPOC",
                                                   "2 - IC (amb o sense CI) amb DM2",
                                                   "5 - CI (sense IC) amb DM2 i MPOC",
                                                   "6 - CI (sense IC) amb DM2",
                                                   "9 - DM2 i MPOC",
                                                   "10 - DM2"), match])
match_id <- dt.matching.raw.pet[match_c == 1 & match %in% match_id & !is.na(v_col_total_post) & !is.na(v_ecg_amb_post) & !is.na(v_hba1c_post), .N, match][N > 1]$match
df <- dt.matching.raw.pet[match_c == 1 & match %in% match_id, .(planificat, v_hba1c_post, v_hba1c_lt8_post)]
cG <- compareGroups(planificat ~ v_hba1c_post + v_hba1c_lt8_post,
                    df, 
                    max.xlev = 20)
cT <- createTable(cG, show.descr = TRUE, show.all = FALSE, hide.no = c("No"))
export2md(cT
          , caption = ""
          , format = "html")
```

#### Visites INF

```{r}
dt.matching.raw.pet1 <- merge(dt.matching.raw.pet,
                              dt.vis.pax.pre[, .(hash, nvis_inf_realitzades_pre)],
                              by.x = 'c_cip',
                              by.y = 'hash',
                              all.x = TRUE)
dt.matching.raw.pet1 <- merge(dt.matching.raw.pet1,
                              dt.vis.pax.post[, .(hash, nvis_inf_realitzades_post)],
                              by.x = 'c_cip',
                              by.y = 'hash',
                              all.x = TRUE)  

match_id <- unique(dt.matching.raw.pet[match_c == 1 & fase == 'Fase 1 (Pilots)' & estat == "10 - Programat" & estat_data < as.Date('2022/05/01') & !is.na(v_col_total_post) & !is.na(v_ecg_amb_post) & !is.na(v_hba1c_post), match])
match_id <- dt.matching.raw.pet[match_c == 1 & match %in% match_id & !is.na(v_col_total_post) & !is.na(v_ecg_amb_post) & !is.na(v_hba1c_post), .N, match][N > 1]$match
df <- dt.matching.raw.pet1[match_c == 1 & match %in% match_id, .(planificat, nvis_inf_realitzades_pre, nvis_inf_realitzades_post)]
cG <- compareGroups(planificat ~ nvis_inf_realitzades_pre + nvis_inf_realitzades_post,
                    df, 
                    max.xlev = 20)
cT <- createTable(cG, show.descr = TRUE, show.all = FALSE, hide.no = "No")
export2md(cT
          , caption = ""
          , format = "html")

dt.matching.raw.pet1 <- merge(dt.matching.raw.pet1,
                              dt.vis.pax.pre[, .(hash, nvis_mg_pre, nvis_inf_pre)],
                              by.x = 'c_cip',
                              by.y = 'hash',
                              all.x = TRUE)
dt.matching.raw.pet1 <- merge(dt.matching.raw.pet1,
                              dt.vis.pax.post[, .(hash, nvis_mg_post, nvis_inf_post)],
                              by.x = 'c_cip',
                              by.y = 'hash',
                              all.x = TRUE)

a <- dt.matching.raw.pet1[, .(rea = sum(nvis_mg_realitzades_post, na.rm = TRUE),
                              prog = sum(nvis_mg_programades_post, na.rm = TRUE),
                              nn = sum(nvis_mg_n_post, na.rm = TRUE),
                              total = sum(nvis_mg_post, na.rm = TRUE)), planificat]
a[, porc := round((rea/total)*100,2)]
```

```{r}
gc.var <- gc()
```


