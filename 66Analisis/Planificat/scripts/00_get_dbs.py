# -*- coding: utf-8 -*-

# 2022/04/26 Descarreguem tota la informació de la taula DBS de redICS per seguretat
# ¡¡¡¡ OJO, tarda 3h !!!!

import sisapUtils as u
from datetime import datetime

ts = datetime.now()
print('comencem')
sql = """SELECT
            *
         FROM 
            dbs
        """        
upload = []

cols = u.getTableColumns('dbs', 'redics')
print(cols)

for row in u.getAll(sql, 'redics'):
    upload.append(row)

file = u.tempFolder + 'Planificat_dbs.csv'
print(file)
u.writeCSV(file, [cols , upload])
