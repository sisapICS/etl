# -*- coding: utf-8 -*-

"""
.
"""

import sisaptools as t
import sisapUtils as u
from datetime import datetime

#DATABASE = ("exadata", "data")
DROP_TABLE = False  # danger!

ts = datetime.now()

class planificat_eqa_adults(object):

    def __init__(self):
        '''.'''

        self.get_u11()
        self.get_eqa()
        self.get_upload()
        self.export_upload()


    def get_u11(self):
        '''.'''
        self.u11 = {}
        i = 0
        for id_cip, id_cip_sec, codi_sector, hash in u.readCSV('D:/SISAP/sisap/66Analisis/Planificat/data/u11_202204.csv', sep='@', header = True):
            i = i + 1
            self.u11[id_cip_sec] = {'hash': hash}
            # if i == 100000:
            #     break

        print('u11: time {} / nrow {}'.format(datetime.now() - ts, len(self.u11)))
        #print(self.u11)

    def get_eqa(self):
        '''.'''
        self.eqa = []
        i = 0
        for id_cip_sec, ind_codi, grup_codi, up, uba, upinf, ubainf, ates, trasplantat, excl_edat, institucionalitzat, maca, num, excl, ci, clin, llistat in u.readCSV('D:/SISAP/sisap/66Analisis/Planificat/data/eqa_202204.csv', sep='@', header = True):
            i = i + 1
            self.eqa.append((id_cip_sec, ind_codi, grup_codi, up, uba, upinf, ubainf, ates, trasplantat, excl_edat, institucionalitzat, maca, num, excl, ci, clin, llistat))
            # if i == 100000:
            #     break            
                
        print('eqa: time {} / nrow {}'.format(datetime.now() - ts, len(self.eqa)))
        #print(self.eqa)

    def get_upload(self):
        """."""
        self.upload = []
        for id_cip_sec, ind_codi, grup_codi, up, uba, upinf, ubainf, ates, trasplantat, excl_edat, institucionalitzat, maca, num, excl, ci, clin, llistat in self.eqa:
            hash = self.u11[id_cip_sec]['hash'] if id_cip_sec in self.u11 else None
            self.upload.append(('202204', hash, ind_codi, grup_codi, up, uba, upinf, ubainf, ates, trasplantat, excl_edat, institucionalitzat, maca, num, excl, ci, clin, llistat))
        print('upload: time {} / nrow {}'.format(datetime.now() - ts, len(self.upload)))
        #print(self.upload)

    def export_upload(self):
        """ . """
        db = 'exadata'
        tb = "SISAP_PLANIFICAT_EQA_ADULTS"
        cols = ("PERIODE VARCHAR2(10)",
                "HASH VARCHAR2(40)",
                "IND_CODI VARCHAR2(10)",
                "GRUP_CODI VARCHAR2(10)",
                "UP VARCHAR2(5)",
                "UBA VARCHAR2(7)",
                "UPINF VARCHAR2(5)",
                "UBAINF VARCHAR2(7)",
                "ATES INT",
                "TRANSPLANTAT INT",
                "EXCL_EDAT INT",
                "INSTITUCIONALITZAT INT",
                "MACA INT",
                "NUM INT",
                "EXCL INT",
                "CI INT",
                "CLIN INT",
                "LLISTAT INT")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        try:
            u.listToTable(self.upload, tb, db)
        except:
            n = 100
            for minidata in [self.upload[i:i + n] for i in xrange(0, len(self.upload), n)]:
                try:
                    u.listToTable(minidata, tb, db)
                except:
                    z = 10
                    for microdata in [minidata[j:j + z] for j in xrange(0, len(minidata), z)]:
                        u.listToTable(microdata, tb, db)       
        users = ['DWSISAP_ROL', 'DWEHERMOSILLA']
        for user in users:
            u.execute("grant select on {} to {}".format(tb, user), db)


if __name__ == "__main__":
    planificat_eqa_adults()