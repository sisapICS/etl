
```{r}
ini.import <- Sys.time()
source("C:/Users/ehermosilla/Documents/Keys.R")
Sys.setenv(TZ = 'Europe/Madrid')
Sys.setenv(ORA_SDTZ = 'Europe/Madrid')
```

## Planificat

Importació de la informació pròpia del projecte.

### SEGUENT_VISITA_CRONICS

Importació taula per fer el projecte d'investigació.

```{r}
if (params$actualitzar_dades) {drv <- dbDriver("Oracle")
                               connect.string <- paste("(DESCRIPTION=",
                                                       "(ADDRESS=(PROTOCOL=tcp)(HOST=", hostpdp,
                                                       ")(PORT=", portpdp,
                                                       "))", "(CONNECT_DATA=(SERVICE_NAME=", dbpdp, ")))", sep = "")
                               con <- dbConnect(drv,
                                                username = idpdp,
                                                password = pwpdp,
                                                dbname = connect.string)
                               query <- dbSendQuery(con,
                                                    statement = "select * from seguent_visita_cronics")
                               dt.planificat.seguent_visita_cronics <- data.table(fetch(query, n = nfetch))
                               var.disconnect <- dbDisconnect(con)
                               saveRDS(dt.planificat.seguent_visita_cronics, 
                                       paste0("../data/planificat/planificat_seguent_visita_cronics_", format(Sys.Date(), '%Y%m%m'), ".rds"))
} else {
  dt.planificat.seguent_visita_cronics <- readRDS("../data/planificat/planificat_seguent_visita_cronics_20230808.rds")
}

gc.var <- gc()
```

```{r}
names(dt.planificat.seguent_visita_cronics) <- tolower(names(dt.planificat.seguent_visita_cronics))
```

Informació importada:

- files n = `r nrow(dt.planificat.seguent_visita_cronics)`
- columnes n = `r ncol(dt.planificat.seguent_visita_cronics)`
- columnes: `r names(dt.planificat.seguent_visita_cronics)`


Selecció de columnes:

Es seleccionen les columnes necessaries per l'estudi.

```{r}
dt.planificat.seguent_visita_cronics <- dt.planificat.seguent_visita_cronics[,.(c_cip, c_sector,
                                                                                dx,
                                                                                grup
                                              )]

gc.var <- gc()

saveRDS(dt.planificat.seguent_visita_cronics, 
        "../data/planificat/planificat_seguent_visita_cronics_20230808.rds")
```

### PLANIFICAT_AUDIT_TOTAL

Importació taula per fer el projecte d'investigació.

```{r}
if (params$actualitzar_dades) {drv <- dbDriver("Oracle")
                               connect.string <- paste("(DESCRIPTION=",
                                                       "(ADDRESS=(PROTOCOL=tcp)(HOST=", hostpdp,
                                                       ")(PORT=", portpdp,
                                                       "))", "(CONNECT_DATA=(SERVICE_NAME=", dbpdp, ")))", sep = "")
                               con <- dbConnect(drv,
                                                username = idpdp,
                                                password = pwpdp,
                                                dbname = connect.string)
                               query <- dbSendQuery(con,
                                                    statement = paste0("select * from ", params$taules[1]))
                               dt.planificat.audit <- data.table(fetch(query, n = nfetch))
                               var.disconnect <- dbDisconnect(con)
                               saveRDS(dt.planificat.audit, 
                                       paste0("../data/planificat_audit_", format(Sys.Date(), '%Y%m%m'), ".rds"))
} else {
  dt.planificat.audit <- readRDS("../data/planificat/planificat_audit_20230808.rds")
}

gc.var <- gc()
```

```{r}
names(dt.planificat.audit) <- tolower(names(dt.planificat.audit))

setnames(dt.planificat.audit, "estat_data", "estat_datahora")
```

Informació importada:

- files n = `r nrow(dt.planificat.audit)`
- columnes n = `r ncol(dt.planificat.audit)`
- columnes: `r names(dt.planificat.audit)`


Selecció de columnes:

Es seleccionen les columnes necessaries per l'estudi, ja que ara mateix la taula té 88 columnes i la majoria es per fer funcionar Planificat.

```{r}
dt.planificat.audit <- dt.planificat.audit[,.(hash,
                                              up, uba, c_sector,
                                              estat, estat_login, estat_datahora,
                                              fadm_data_recomanada,
                                              fadm_grup
                                              )]

gc.var <- gc()
```

Validacions:

```{r}
# Validaciones
  # HASH
    dt.planificat.audit.agr.hash <- dt.planificat.audit[, .N, hash] # N=987.925
    #nrow(dt.planificat.audit[, .N, hash][N > 1])
    dt.planificat.audit[, .N, hash][N > 10,][order(-N)][1:10,]
    # 230D488A04D360D01BCD757AD562509437EA41A7	451			
    # BF2B5953B3DBCD5428FBD221AF546269EEE69CB3	146			
    # 685A6D4EC81BF7CECB6EE44DB10D06356DAB2992	122			
    # ACDA98C772FDE035E8DE2FA8DEBABB3D67BBA916	42			
    # 040BE7DD98F5743AD90FC578D7A062F97E7E324E	39			
    # B7273715E1FC992FC7D27094F072E35F235CB582	34			
    # 6286E2B7A4F2629B330C4235A098B8B67956DF8B	33			
    # CD99B67AC61C05DCAA1F6A82D7AB61269D5FD6F3	32			
    # A64CBE74BD09A93FB339B0614568DF2481A2CAA2	30			
    # D5A37A2462ABD140299345A87F7D7B9A8F24D7A8	29
    a <- dt.planificat.audit[hash == "D5A37A2462ABD140299345A87F7D7B9A8F24D7A8",]

  # UP
    dt.planificat.audit.agr.up <- dt.planificat.audit[, .N, up] # N = 392 Totes les UP
    
  # UBA
    dt.planificat.audit.agr.upuba <- dt.planificat.audit[, .N, .(up,uba)] # N = 5.371 Profesionales
    
  # C_SECTOR
    dt.planificat.audit.agr.sector <- dt.planificat.audit[, .N, c_sector] # N = 22 Hay prisiones
    
  # ESTAT
    dt.planificat.audit.agr.estat <- dt.planificat.audit[, .N, estat] # N = 8 estados
   #  1 1645960 Revisar programació
   #  2 23025 Exclusió sanitaria
   #  3 9599 Pendent petició analítica
   #  4 148469 LLest per programar
   #  5 2958 Exclusió Administrativa
   #  6 37370 Tasques pendents
   #  7 15072 Retornat Administratiu
   # 10 100482 Programat

  # ESTAT_LOGIN
    dt.planificat.audit.agr.estatlogin <- dt.planificat.audit[, .N, estat_login] # N = 4.731 login diferents
    
  # ESTAT_DATA
    dt.planificat.audit.agr.estat_datahora <- dt.planificat.audit[, .N, estat_datahora]
    dt.planificat.audit[, estat_data := as.Date(substr(estat_datahora,1,10), '%Y/%m/%d')]
    dt.planificat.audit.agr.estat_data <- dt.planificat.audit[, .N, as.Date(substr(estat_datahora,1,10), '%Y/%m/%d')]
    dt.planificat.audit.agr.estat_data.estat <- dt.planificat.audit[, .N, .(estat,as.Date(substr(estat_datahora,1,10), '%Y/%m/%d'))]
    
  # fadm_data_recomanada
    dt.planificat.audit.agr.fadm_data_recomanada <- dt.planificat.audit[, .N, fadm_data_recomanada]
    a <- dt.planificat.audit[!is.na(fadm_data_recomanada),]
    
  # data_recomanada_chk: 2023/08/08 Se elimina, 99% missing 
    #dt.planificat.audit.agr.fadm_data_recomanada_chk <- dt.planificat.audit[, .N, data_recomanada_chk]
    
  # GRUP
    dt.planificat.audit.agr.fadm_grup <- dt.planificat.audit[, .N, fadm_grup]
    dt.planificat.audit.agr.fadm_grup.estat <- dt.planificat.audit[, .N, .(estat,fadm_grup)]
```

```{r}
saveRDS(dt.planificat.audit[,.(hash,
                               up, uba, c_sector,
                               estat, estat_login, estat_datahora, estat_data,
                               fadm_data_recomanada,
                               fadm_grup)], 
        paste0("../data/planificat/planificat_audit_", format(Sys.Date(), '%Y%m%m'), "_colflt",".rds"))
```

```{r}
dt.planificat.audit <- readRDS("../data/planificat/planificat_audit_20230808_colflt.rds")
```

```{r}
fi <- Sys.time()
fi - ini.import #
gc.var <- gc()
```

