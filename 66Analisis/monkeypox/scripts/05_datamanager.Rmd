***
***

# Data Manager

## ECAP

### Població

```{r}
dt.ecap.pob # 582
dt.ecap.pob[, nacionalitat_c1 := "Extranger"][nacionalitat %in% c("", "724"), nacionalitat_c1 := "Nacional"]
dt.ecap.pob[, .N, nacionalitat_c1]

dt.ecap.pob[, last_visit_year := substr(last_visit, 1,4)]
dt.ecap.pob[, .N, last_visit_year][order(last_visit_year)]
```

### Casos

```{r}
dt.ecap.casos[, .N, pcr] # 582

setnames(dt.ecap.casos, "diagnostic", "diagnostic_data")
dt.ecap.casos[, diagnostic_data := as.Date(diagnostic_data, '%Y-%m-%d')]
dt.ecap.casos[, diagnostic := 0][!is.na(diagnostic_data), diagnostic := 1]
dt.ecap.casos[, .N, diagnostic] # 1205

dt.ecap.casos[, .N, .(diagnostic, pcr)] # 
# diagnostic pcr N
# 0	0	7789677		
# 1	0	1053		
# 0	1	430		
# 1	1	152
```

### Proves

```{r}
dt.ecap.proves # 
```

### Juntar tota la informació

```{r}
setkey(dt.ecap.casos, 'id')
setkey(dt.ecap.u11, 'id_cip_sec')
dt.ecap.dm <- merge(dt.ecap.casos,
                    unique(dt.ecap.u11[, .(id_cip_sec, hash_d)]),
                    by.x = 'id',
                    by.y = 'id_cip_sec',
                    all.x = TRUE)

setkey(dt.ecap.dm, 'hash_d')
setkey(dt.pdptb101.raw, 'hash_redics')
dt.ecap.dm <- merge(dt.ecap.dm,
                    dt.pdptb101.raw,
                    by.x = 'hash_d',
                    by.y = 'hash_redics',
                    all.x = TRUE)

setkey(dt.ecap.pob, 'id_cip_sec')
dt.ecap.dm <- merge(dt.ecap.dm,
                    dt.ecap.pob,
                    by.x = 'id',
                    by.y = 'id_cip_sec',
                    all.x = TRUE)

#♥ Filtrem els pacients
  dt.ecap.flt <- dt.ecap.dm[pcr == 1 | diagnostic == 1, .(id, hash_d, hash_covid,
                                                          up,
                                                          naix, edat, sexe,
                                                          nacionalitat, nacionalitat_c1,
                                                          institucionalitzat,
                                                          last_visit, last_visit_year,
                                                          pcr, diagnostic_data, diagnostic)]
```


## MonkeyPox

### Prep

Data manager de la informació de ...

```{r}
dt.prep.dm <- dt.prep

setnames(dt.prep.dm, "naix", "data_naix")

dt.prep.dm[data_naix == "0000-00-00", data_naix := ""]
dt.prep.dm[prep == 0, prep_data := ""]

dt.prep.dm[, data_naix := as.Date(data_naix, '%Y-%m-%d')]
dt.prep.dm[, prep_data := as.Date(prep_data, '%Y-%m-%d')]

dt.prep.dm[, prep := factor(prep, levels = c(0,1), labels = c("No", "Sí"))]
dt.prep.dm[, hos := factor(hos)]
```

***

### Vacunes

Data manager de la informació de les vacunes

```{r}
a <- dt.vac[, .N, hash]
#dt.vac[hash == ""]

dt.vac.dm <- dt.vac

# SDCols <- c("vac2_data", "vac2_tipus", "vac2_dosis", "vac2_via")
# dt.vac.dm[vac2 == 0, SDCols := lapply(.SD, function(x){
#   x := ""
# }), SDCols]

dt.vac.dm[vac2 == 0, vac2_data := ""]
dt.vac.dm[vac2 == 0, vac2_tipus := ""]
dt.vac.dm[vac2 == 0, vac2_dosis := ""]
dt.vac.dm[vac2 == 0, vac2_via := ""]

dt.vac.dm[, vac1_data := as.Date(vac1_data, '%Y-%m-%d')]
dt.vac.dm[, vac2_data := as.Date(vac2_data, '%Y-%m-%d')]
dt.vac.dm[, vac_diff := as.numeric(difftime(vac2_data, vac1_data, units = "days"))]
a <- dt.vac.dm[, .N, vac_diff]
dt.vac.dm[vac_diff <= 0]

dt.vac.dm[, vac1 := factor(vac1, levels = c(0, 1, 9),
                                 labels = c("No", "Sí", "Desconocido"))]
dt.vac.dm[, vac1_ind := factor(vac1_ind,
                               levels = c(1, 2, 9),
                               labels = c("Vacunación pre-exposición", "Vacunación pos-exposición", "Desconocido"))]
dt.vac.dm[, vac1_tipus := factor(vac1_tipus,
                                 levels = c(1, 2, 9),
                                 labels = c("IMVANEX", "JYNNEOS", "Desconocido"))]
dt.vac.dm[, vac1_dosis := factor(vac1_dosis,
                                 levels = c(1, 2, 9),
                                 labels = c("0,1ml", "0,5ml", "Desconocido"))]
dt.vac.dm[, vac1_via := factor(vac1_via,
                               levels = c(1, 2, 9),
                               labels = c("Subcutánea", "Intradérmica", "Desconocido"))]

dt.vac.dm[, vac2 := factor(vac2, levels = c(0, 1, 9),
                                 labels = c("No", "Sí", "Desconocido"))]
dt.vac.dm[, vac2_tipus := factor(vac2_tipus,
                                 levels = c(1, 2, 9),
                                 labels = c("IMVANEX", "JYNNEOS", "Desconocido"))]
dt.vac.dm[, vac2_dosis := factor(vac2_dosis,
                                 levels = c(1, 2, 9),
                                 labels = c("0,1ml", "0,5ml", "Desconocido"))]
dt.vac.dm[, vac2_via := factor(vac2_via,
                               levels = c(1, 2, 9),
                               labels = c("Subcutánea", "Intradérmica", "Desconocido"))]
```

***

### Casos

Data manager de la informació dels casos

```{r}
a <- dt.cas[, .N, hash]
#dt.cas[hash == ""]

dt.cas.dm <- dt.cas

a <- dt.cas.dm[, .N, .(simpt, simpt_data)]

dt.cas.dm[simpt != 1, simpt_data := ""]
dt.cas.dm[exitus != 1, exitus_data := ""]

dt.cas.dm[, test_data := as.Date(test_data, '%Y-%m-%d')]
dt.cas.dm[, simpt_data := as.Date(simpt_data, '%Y-%m-%d')]
dt.cas.dm[, exitus_data := as.Date(exitus_data, '%Y-%m-%d')]

dt.cas.dm[, resident_cat := factor(resident_cat)]

dt.cas.dm[, mpx := factor(mpx,
                          levels = c(0, 1, 9),
                          labels = c("No", "Sí", "Desconocido"))]
dt.cas.dm[, simpt := factor(simpt,
                            levels = c(0, 1, 9),
                            labels = c("No", "Sí", "Desconocido"))]
dt.cas.dm[, hx := factor(hx,
                         levels = c(0, 1, 9),
                         labels = c("No", "Sí", "Desconocido"))]
dt.cas.dm[, uci := factor(uci,
                          levels = c(0, 1, 9),
                          labels = c("No", "Sí", "Desconocido"))]
dt.cas.dm[, .N, exitus]
dt.cas.dm[, exitus := factor(exitus,
                             levels = c(0, 1, 9),
                             labels = c("No", "Sí", "Desconocido"))]
dt.cas.dm[, vacinf := factor(vacinf,
                             levels = c(1, 2, 9),
                             labels = c("No", "Sí", "Desconocido"))]
```

***

### DBS

Data manager informació del DBS

```{r}
a <- dt.dbs[, .N, hash]
# Paciente repetido: 53B0493C3C6835861729546870EE4147D2FCBE64
#dt.dbs[hash == "53B0493C3C6835861729546870EE4147D2FCBE64"]
```

```{r}
dt.dbs[, any_dbs := as.factor(any_dbs)]
dt.dbs[, data_naix := as.Date(data_naix, '%Y-%m-%d')]
```

```{r}
vars <- names(dt.dbs)[names(dt.dbs) %in%  c("pcc", "maca")]
dt.dbs[, toupper(vars) := lapply(.SD, function(x){
  factor(ifelse(x != '0000-00-00', "Sí", "No"))
}
), .SDcols = vars]
dt.dbs[, (vars) := NULL]
```

```{r}
fr <- names(dt.dbs)[which(substr(names(dt.dbs), 1, 2) == "fr")]
dt.dbs[, toupper(fr) := lapply(.SD, function(x){
  factor(ifelse(x != '0000-00-00', "Sí", "No"))
}
), .SDcols = fr]
dt.dbs[, (fr) := NULL]
```

```{r}
f <- names(dt.dbs)[which(substr(names(dt.dbs), 1, 2) == "f_")]
dt.dbs[, toupper(f) := lapply(.SD, function(x){
  factor(ifelse(x != "", "Sí", "No"))
}
), .SDcols = f]
dt.dbs[, (f) := NULL]
```


```{r}
names(dt.dbs) <- tolower(names(dt.dbs))
dt.dbs.dm <- dt.dbs

gc.var <- gc()
```

***

## Afegir Taules/Catàlegs

Afegim el taules i catàlegs a la taula de vacunats

```{r}
dt.monkeypox <- merge(dt.prep.dm[hash != "", ],
                      dt.vac.dm[hash != "", ],
                      by.x = c("hash"),
                      by.y = c("hash"),
                      all.x = TRUE)

dt.monkeypox <- merge(dt.monkeypox,
                      dt.cas.dm[hash != "", ],
                      by.x = c("hash"),
                      by.y = c("hash"),
                      all.x = TRUE)

dt.monkeypox <- merge(dt.monkeypox,
                      dt.dbs.dm[hash != "", ],
                      by.x = c("hash"),
                      by.y = c("hash"),
                      all.x = TRUE)
```

```{r}

```

```{r data manager gc}
gc.var <- gc()
```

