# -*- coding: utf8 -*-

"""
Per obtenir positius a monkeypox
"""

import collections as c
import sisapUtils as u

import hashlib as h

vac_file =  u.tempFolder + "mkpox_vacuna_def.txt"
vac_file2 =  u.tempFolder + "mkpox_vac_check.txt"
cas_file =  u.tempFolder + "mkpox_casos_def.txt"
prep_file = u.tempFolder + "mkpox_PREP_def.txt"


class monkey(object):
    """."""

    def __init__(self):
        """."""
        self.inclosos = {}
        #self.get_cip()
        self.get_poblacio()
        #self.get_vacunats()
        #self.get_casos()
        self.get_prep()
        #self.get_dbs()
        #self.get_dbs2021()
        #self.get_dbs2022()
        self.export()
    
    def get_cip(self):
        """hash to cip"""
        u.printTime("cip")
        self.hash_to_cip = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.hash_to_cip[hash] = cip
    
    def get_poblacio(self):
        """obtenim poblacio RCA i els no RCA de ecap"""
        u.printTime("poblacio")
        self.id_cip = {}
        self.id_dni = {}
        self.id_cip13 = {}
        self.hash_to = {}
        sql = """SELECT hash, cip, substr(cip, 0, 13), dni, data_naixement, data_defuncio, case when sexe=0 then 'H' when sexe=1 then 'M' else '' end, situacio, nacionalitat,  abs, eap, up_residencia  FROM dwsisap.RCA_CIP_NIA rcn """
        for hash, cip, cip13, dni, naix, defuncio, sexe, situacio, nac, abs, eap, up in u.getAll(sql, 'exadata'):
            self.id_cip[cip] = {'hash': hash, 'naix': naix, 'defuncio': defuncio, 'sexe': sexe, 'situacio': situacio, 'nac': nac, 'abs': abs, 'eap': eap, 'up': up}
            self.id_dni[dni] = {'hash': hash, 'naix': naix, 'defuncio': defuncio, 'sexe': sexe, 'situacio': situacio, 'nac': nac, 'abs': abs, 'eap': eap, 'up': up}
            self.id_cip13[cip13] = {'hash': hash, 'naix': naix, 'defuncio': defuncio, 'sexe': sexe, 'situacio': situacio, 'nac': nac, 'abs': abs, 'eap': eap, 'up': up}
            self.hash_to[hash] = {'cip': cip, 'dni': dni}
        
        sql = """SELECT hash, cip, substr(cip, 0, 13), document, data_naixement, case when sexe='D' then  'M' else sexe end, situacio, nacionalitat, ecap_abs, ecap_up FROM dwsisap.dbc_poblacio WHERE es_rca=0"""
        for hash, cip, cip13, dni, naix, sexe, situacio, nac, abs,  up in u.getAll(sql, 'exadata'):
            self.id_cip[cip] = {'hash': hash, 'naix': naix, 'defuncio': None, 'sexe': sexe, 'situacio': situacio, 'nac': nac, 'abs': abs, 'eap': None, 'up': up}
            self.id_dni[dni] = {'hash': hash, 'naix': naix, 'defuncio': None, 'sexe': sexe, 'situacio': situacio, 'nac': nac, 'abs': abs, 'eap': None, 'up': up}
            self.id_cip13[cip13] = {'hash': hash, 'naix': naix, 'defuncio': None, 'sexe': sexe, 'situacio': situacio, 'nac': nac, 'abs': abs, 'eap': None, 'up': up}
            self.hash_to[hash] = {'cip': cip, 'dni': dni}
    
    def get_vacunats(self):
        """obtenim vacunats de fitxer de la Montse"""
        u.printTime("vacunes")

        self.vacunes, self.vacunesM = [], []
        self.doble_registre = []
        for (cip, dni, vac1, vac1_ind, vac1_data, vac1_tipus, vac1_dosis, vac1_via, vac2, vac2_data, vac2_tipus, vac2_dosis, vac2_via, vacinf ) in u.readCSV(vac_file, sep='@'):
            if dni in self.id_dni:
                hash = self.id_dni[dni]['hash']
            elif cip in self.id_cip:
                hash = self.id_cip[cip]['hash']
            elif cip in self.id_cip13:
                hash = self.id_cip13[cip]['hash']
            else:
                hash = None
            if cip == '':
                cip = self.hash_to[hash]['cip'] if hash in self.hash_to else None
            if dni == '':
                dni = self.hash_to[hash]['dni'] if hash in self.hash_to else None
            self.vacunes.append([hash, vac1, vac1_ind, vac1_data, vac1_tipus, vac1_dosis, vac1_via, vac2, vac2_data, vac2_tipus, vac2_dosis, vac2_via, 'vacunes'])
            self.vacunesM.append([hash, cip, dni, vac1, vac1_ind, vac1_data, vac1_tipus, vac1_dosis, vac1_via, vac2, vac2_data, vac2_tipus, vac2_dosis, vac2_via])
            if hash != None:
                self.inclosos[hash] = True
                
        for (cipsns, dni, cip, vac1_data,vac2_data) in u.readCSV(vac_file2, sep='@'):
            if dni in self.id_dni:
                hash = self.id_dni[dni]['hash']
            elif cip in self.id_cip:
                hash = self.id_cip[cip]['hash']
            elif cip in self.id_cip13:
                hash = self.id_cip13[cip]['hash']
            else:
                hash = None
            print hash
            if hash != None:
                if hash in self.inclosos:
                    vac1, vac2 = 0, 0
                    if vac1_data != '--':
                        vac1=1
                        if vac2_data != '--':
                            vac2 = 1
                    self.doble_registre.append([hash, vac1, vac1_data, vac2, vac2_data])
                else:
                    vac1, vac2 = 0, 0
                    if vac1_data != '--':
                        vac1=1
                        if vac2_data != '--':
                            vac2 = 1
                        print vac1, vac1_data, vac2, vac2_data
                        self.inclosos[hash] = True
                        self.vacunes.append([hash, vac1, None, vac1_data, None, None, None, vac2, vac2_data, None, None, None, 'prep'])
                        self.vacunesM.append([hash, cip, dni, vac1, None, vac1_data, None, None, None, vac2, vac2_data, None, None, None])
                    
    
    def get_casos(self):
        """obtenim casos de fitxer de la Montse"""
        u.printTime("casos")
        self.casos, self.casosM = [], []
        for (cip, dni, id, mpx, test_data, simpt, simpt_data, hx, uci, exitus, exitus_data, vacinf, resident_cat ) in u.readCSV(cas_file, sep='@'):
            if dni in self.id_dni:
                hash = self.id_dni[dni]['hash']
                defuncio = self.id_dni[dni]['defuncio']
            elif cip in self.id_cip:
                hash = self.id_cip[cip]['hash']
                defuncio = self.id_cip[cip]['defuncio']
            elif cip in self.id_cip13:
                hash = self.id_cip13[cip]['hash']
                defuncio = self.id_cip13[cip]['defuncio']
            else:
                hash = None
                defuncio = None
            dni = self.hash_to[hash]['dni'] if hash in self.hash_to else None
            self.casos.append([hash,mpx, test_data, simpt, simpt_data, hx, uci, exitus, exitus_data, vacinf, resident_cat, defuncio])
            self.casosM.append([hash, cip, dni, mpx, test_data, simpt, simpt_data, hx, uci, exitus, exitus_data, vacinf])
            if hash != None:
                self.inclosos[hash] = True
                
    def get_prep(self):
        """obtenim Prep dels fitxers"""
        u.printTime("Prep")
        self.prep_vih = []
        self.prep_M = []
        for (cip_sns, cip, dni, naix, prep, prep_data, hos) in u.readCSV(prep_file, sep='@'):
            if dni in self.id_dni:
                hash = self.id_dni[dni]['hash']
                sexe = self.id_dni[dni]['sexe']
                if naix == '--':
                    naix = self.id_dni[dni]['naix']
            elif cip in self.id_cip:
                hash = self.id_cip[cip]['hash']
                sexe = self.id_cip[cip]['sexe']
                if naix == '--':
                    naix = self.id_cip[cip]['naix']
            elif cip in self.id_cip13:
                hash = self.id_cip13[cip]['hash']
                sexe = self.id_cip13[cip]['sexe']
                if naix == '--':
                    naix = self.id_cip13[cip]['naix']
            else:
                hash = None
                sexe = None
            self.prep_vih.append([hash,naix, sexe, prep, prep_data, hos])
            self.prep_M.append([hash, cip_sns, cip, dni, naix, sexe, prep, prep_data, hos])
            if hash != None:
                self.inclosos[hash] = True


    def get_dbs(self):
        """Dades del dbs"""
        u.printTime("DBS")
        self.in_dbs = []
        self.in_dbs_true = {}
        sql = """select c_cip, c_up, c_data_naix, c_sexe, c_nacionalitat, c_gma_codi, c_gma_complexitat, PR_PCC_DATA, PR_MACA_DATA, PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    V_RCV_DATA, V_RCV_VALOR,
                     F_HTA_IECA_ARA2, F_HTA_CALCIOA, F_HTA_BETABLOQ, F_HTA_DIURETICS, F_HTA_ALFABLOQ, F_HTA_ALTRES, F_HTA_COMBINACIONS,
                    F_DIAB_ADO, F_DIAB_INSULINA, F_MPOC_ASMA,F_ANTIAGREGANTS_ANTICOA, F_AINE, F_ANALGESICS, 
                    F_ANTIDEPRESSIUS, F_ANSIO_HIPN, F_ANTIPSICOTICS, F_ANTIULCEROSOS, F_ANTIESP_URI, F_CORTIC_SISTEM,
                    F_ANTIEPILEPTICS,F_HIPOLIPEMIANTS, F_MHDA_VIH
                from DWSISAP.DBS"""
        for (cip, up, naix, sexe, nac, gma, gma1, pcc, maca,
             hta, dm1, dm2, mpoc,
             asma, bc, ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             rcv_dat, rcv_val,
             ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
             ado, insulina, f_mpoc, antiag, aine, analg,
             antidep, ansiol, antipsic, ulcer, anties, cortis,
             epil, hipol, mhda) in u.getAll(sql, 'exadata'):
            if cip in self.inclosos:
                self.in_dbs.append([2023, cip, up, naix, sexe, nac, gma, gma1, pcc, maca,
                 hta, dm1, dm2, mpoc,
                 asma, bc, ci,
                 mcv, ic, acfa, valv,
                 hepat, vhb, vhc, neo, mrc,
                 obes, vih, sida,
                 demencia, artrosi,
                 ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
                 ado, insulina, f_mpoc, antiag, aine, analg,
                 antidep, ansiol, antipsic, ulcer, anties, cortis,
                 epil, hipol, mhda])
                self.in_dbs_true[cip] = True
             
    def get_dbs2021(self):
        """Dades del dbs de 2021 pels morts durant 2022"""
        u.printTime("DBS 2021")
        
        SIDICS_DB = ("dbs", "x0002")
        sql = """select c_cip, c_up, c_data_naix, c_sexe, c_nacionalitat, c_gma_codi, c_gma_complexitat, PR_PCC_DATA, PR_MACA_DATA, PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    V_RCV_DATA, V_RCV_VALOR,
                     F_HTA_IECA_ARA2, F_HTA_CALCIOA, F_HTA_BETABLOQ, F_HTA_DIURETICS, F_HTA_ALFABLOQ, F_HTA_ALTRES, F_HTA_COMBINACIONS,
                    F_DIAB_ADO, F_DIAB_INSULINA, F_MPOC_ASMA,F_ANTIAGREGANTS_ANTICOA, F_AINE, F_ANALGESICS, 
                    F_ANTIDEPRESSIUS, F_ANSIO_HIPN, F_ANTIPSICOTICS, F_ANTIULCEROSOS, F_ANTIESP_URI, F_CORTIC_SISTEM,
                    F_ANTIEPILEPTICS,F_HIPOLIPEMIANTS, F_MHDA_VIH
                from dbs.dbs_2021"""
        for (cip1, up, naix, sexe, nac, gma, gma1, pcc, maca,
             hta, dm1, dm2, mpoc,
             asma, bc, ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             rcv_dat, rcv_val,
             ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
             ado, insulina, f_mpoc, antiag, aine, analg,
             antidep, ansiol, antipsic, ulcer, anties, cortis,
             epil, hipol, mhda) in u.getAll(sql, SIDICS_DB):
            if cip1 in self.hash_to_cip:
                cip2 = self.hash_to_cip[cip1]
                cip = h.sha1(cip2).hexdigest().upper()
                if cip in self.inclosos:
                    if cip not in self.in_dbs_true:
                        self.in_dbs_true[cip] = True
                        self.in_dbs.append([2021, cip, up, naix, sexe, nac, gma, gma1, pcc, maca,
                         hta, dm1, dm2, mpoc,
                         asma, bc, ci,
                         mcv, ic, acfa, valv,
                         hepat, vhb, vhc, neo, mrc,
                         obes, vih, sida,
                         demencia, artrosi,
                         ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
                         ado, insulina, f_mpoc, antiag, aine, analg,
                         antidep, ansiol, antipsic, ulcer, anties, cortis,
                         epil, hipol, mhda])
    
    def get_dbs2022(self):
        """Dades del dbs de desembre de 2022 per tenir més talls"""
        u.printTime("DBS 2022")
        
        SIDICS_DB = ("dbs", "x0002")
        sql = """select c_cip, c_up, c_data_naix, c_sexe, c_nacionalitat, c_gma_codi, c_gma_complexitat, PR_PCC_DATA, PR_MACA_DATA, PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                    V_RCV_DATA, V_RCV_VALOR,
                     F_HTA_IECA_ARA2, F_HTA_CALCIOA, F_HTA_BETABLOQ, F_HTA_DIURETICS, F_HTA_ALFABLOQ, F_HTA_ALTRES, F_HTA_COMBINACIONS,
                    F_DIAB_ADO, F_DIAB_INSULINA, F_MPOC_ASMA,F_ANTIAGREGANTS_ANTICOA, F_AINE, F_ANALGESICS, 
                    F_ANTIDEPRESSIUS, F_ANSIO_HIPN, F_ANTIPSICOTICS, F_ANTIULCEROSOS, F_ANTIESP_URI, F_CORTIC_SISTEM,
                    F_ANTIEPILEPTICS,F_HIPOLIPEMIANTS, F_MHDA_VIH
                from dbs.dbs_2022"""
        for (cip1, up, naix, sexe, nac, gma, gma1, pcc, maca,
             hta, dm1, dm2, mpoc,
             asma, bc, ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi,
             rcv_dat, rcv_val,
             ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
             ado, insulina, f_mpoc, antiag, aine, analg,
             antidep, ansiol, antipsic, ulcer, anties, cortis,
             epil, hipol, mhda) in u.getAll(sql, SIDICS_DB):
            if cip1 in self.hash_to_cip:
                cip2 = self.hash_to_cip[cip1]
                cip = h.sha1(cip2).hexdigest().upper()
                if cip in self.inclosos:
                    if cip not in self.in_dbs_true:
                        self.in_dbs_true[cip] = True
                        self.in_dbs.append([2022, cip, up, naix, sexe, nac, gma, gma1, pcc, maca,
                         hta, dm1, dm2, mpoc,
                         asma, bc, ci,
                         mcv, ic, acfa, valv,
                         hepat, vhb, vhc, neo, mrc,
                         obes, vih, sida,
                         demencia, artrosi,
                         ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
                         ado, insulina, f_mpoc, antiag, aine, analg,
                         antidep, ansiol, antipsic, ulcer, anties, cortis,
                         epil, hipol, mhda])
    
    def export(self):
        """exportem les taules"""
        u.printTime("export")
        
        db = "permanent"
        tb = "mkpox_vacunes"
        columns = ["hash varchar(40), vac1 int, vac1_ind int, vac1_data date, vac1_tipus int, vac1_dosis int, vac1_via int, vac2 int, vac2_data date, vac2_tipus int, vac2_dosis int, vac2_via int, fitxer varchar(50)"]
        #u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        #u.listToTable(self.vacunes, tb, db)
        
        tb = "mkpox_vacunes_doble"
        columns = ["hash varchar(40), vac1 int, vac1_data date, vac2 int, vac2_data date"]
        #u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        #u.listToTable(self.doble_registre, tb, db)
        
        tb = "mkpox_vacunes_M"
        columns = ["hash varchar(40), cip varchar(40), dni varchar(40), vac1 int, vac1_indicacion int, vac1_fecha date, vac1_tipo int, vac1_dosis int, vac1_via int, vac2 int, vac2_fecha date, vac2_tipo int, vac2_dosis int, vac2_via int"]
        #u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        #u.listToTable(self.vacunesM, tb, db) 
        
        tb = "mkpox_casos"
        columns = ["hash varchar(40), mpx int, test_data date, simpt int, simpt_data date, hx int, uci int, exitus int, exitus_data date, vacinf int, resident_cat varchar(10), data_defuncio date"]
        #u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        #u.listToTable(self.casos, tb, db)
        
        tb = "mkpox_casos_M"
        columns = ["hash varchar(40), cip varchar(40), dni varchar(40), mpx int, test_fecha date, sint int, sint_fecha date, hosp int, uci int, fall int, fall_fecha date, vacinf int"]
        #u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        #u.listToTable(self.casosM, tb, db)

        tb = "mkpox_PREP"
        columns = ["hash varchar(40), naix date, sexe varchar(10), prep int, prep_data date, hos varchar(20)"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.prep_vih, tb, db)
        
        tb = "mkpox_PREP_M"
        columns = ["hash varchar(40), cip_sns varchar(40),cip varchar(40), dni varchar(40), naix date, sexe varchar(10), prep int, prep_data date, hos varchar(20)"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.prep_M, tb, db)

        tb = "mkpox_dbs"
        cols = ("any_dbs int", "hash varchar(40)", "up varchar(5)", "data_naix date", "sexe varchar(1)", "nacionalitat varchar(100)",
           "gma_codi varchar(10)", "gma_complexitat double", "pcc date", "maca date", 
            "FR_hta date",  "FR_dm1 date", "FR_dm2 date", "FR_mpoc date", "FR_asma date", "FR_bc date", "FR_ci date", "FR_mcv date",
            "FR_ic date", "FR_acfa date", "FR_valv date", "FR_hepat date", "FR_vhb date", "FR_vhc date", "FR_neo date", "FR_mrc date", "FR_obes date",
            "FR_vih date", "FR_sida date", "FR_demencia date", "FR_artrosi date",
            "F_HTA_IECA_ARA2 varchar(500)", "F_HTA_CALCIOA varchar(500)","F_HTA_BETABLOQ varchar(500)","F_HTA_DIURETICS varchar(500)",
            "F_HTA_ALFABLOQ varchar(500)", "F_HTA_ALTRES varchar(500)", "F_HTA_COMBINACIONS varchar(500)",
             "F_DIAB_ADO varchar(500)", "F_DIAB_INSULINA varchar(500)", "F_MPOC_ASMA varchar(500)", 
             "F_ANTIAGREGANTS_ANTICOA varchar(500)", "F_AINE varchar(500)", "F_ANALGESICS varchar(500)",
             "F_ANTIDEPRESSIUS varchar(500)", "F_ANSIO_HIPN varchar(500)", "F_ANTIPISCOTICS varchar(500)",
             "F_ANTIULCEROSOS varchar(500)", "F_ANTIESP_URI varchar(500)", "F_CORTIC_SISTEM varchar(500)",
             "F_ANTIEPILEPTICS varchar(500)", "F_HIPOLIPEMIANTS varchar(500)", "F_MHDA_VIH varchar(500)")
        #u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        #u.listToTable(self.in_dbs, tb, db)
        
if __name__ == "__main__":
    u.printTime("Inici")
    
    monkey()
    
    u.printTime("Fi")
