# -*- coding: utf8 -*-

"""
Per obtenir positius a monkeypox
"""

import collections as c
import sisapUtils as u


DEBUG = False

TB = "MPX_casos"
TB2 = "MPX_proves"
DB = "permanent"


class IC(object):
    """."""

    def __init__(self):
        """."""
        self.get_pcr()
        self.get_diagnostic()
        self.get_poblacio()
        self.upload_data()

    def get_pcr(self):
        """."""
        u.printTime("pcr")
        centres = tuple([up for up, in u.getAll("select scs_codi from cat_centres", "nodrizas")])  # noqa
        self.pcr_mpx =  c.defaultdict(set)
        sql = "select id_cip_sec, cr_data_reg, cr_codi_lab, \
                      cr_codi_prova_ics, cr_res_lab, codi_up \
               from {} \
               where cr_codi_prova_ics in ('016026','016027','016028','016029','016030')"
        jobs = ([sql.format(table), "import"] for table in u.getSubTables("laboratori"))  # noqa
        resultat = u.multiprocess(_get_data, jobs, 8)
        self._iterate_workers(resultat, self.pcr_mpx)


    def get_diagnostic(self):
        """."""
        u.printTime("dx")
        sql = "select id_cip_sec, pr_dde from problemes  where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_cod_ps in ('C01-B04') and \
                            pr_data_baixa = 0 "
        self.diagnostic = {id: dde for (id, dde) in _get_data([sql, "import"])}  # noqa

    def _iterate_workers(self, resultat, objecte):
        """."""
        for worker in resultat:
            for row in worker:
                id = row[0]
                dades = row[1:] if len(row) > 2 else row[1]
                if id not in objecte or dades > objecte[id]:
                    objecte[id].add(dades)

    def get_poblacio(self):
        """."""
        u.printTime("pob")
        self.resultat = []
        sql = "select id_cip_sec, data_naix, edat, sexe, up from assignada_tot"
        for id, edat, edat2, sexe, up in _get_data([sql, "nodrizas"]):
            pcr_dat = 0
            if id in self.pcr_mpx:
                pcr_dat = 1
            dx = self.diagnostic[id] if id in self.diagnostic else None
            self.resultat.append([id, edat, edat2, sexe, pcr_dat, dx])
        
        self.upload = []
        for id, registres in self.pcr_mpx.items():
            for row in registres:
                pcr_dat = row[0]
                pcr_prova = row[2]
                pcr_res = row[3]
                pcr_up = row[4]
                self.upload.append([id, pcr_dat, pcr_prova, pcr_res, pcr_up])
                 
    def upload_data(self):
        """."""
        u.printTime("upload")
        cols = "(id int, naix date, edat int, sexe varchar(1), pcr int,  diagnostic date null)"
        u.createTable(TB, cols, DB, rm=True)
        u.listToTable(self.resultat, TB, DB)
        
        cols = "(id int, pcr_dat date, pcr_prova varchar(10), pcr_res varchar(100), up varchar(100))"
        u.createTable(TB2, cols, DB, rm=True)
        u.listToTable(self.upload, TB2, DB)


def _get_data(param):
    """."""
    if DEBUG:
        param[0] += " limit 1000"
    return [row for row in u.getAll(*param)]


if __name__ == "__main__":
    IC()
