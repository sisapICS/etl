import os

currentWD = os.getcwd().replace("\\","/").split("etl")[0]
dirPath = currentWD + "etl/66Analisis/flash/wp6/rct/catalegs_pdp"
os.chdir(dirPath)


with open('flash_rct_catalegs.sql', 'w') as file:
    # Write the commands to create FLASHINDICATORS
    file.write("DROP TABLE FLASHINDICADORS;\n")
    file.write("CREATE TABLE FLASHINDICADORS ( indicador varchar2(7) );\n")

    with open('./FLASHINDICADORS.txt','r') as indFile:
        indicators = [ind.replace("\n","") for ind in indFile]
    
    for ind in indicators:
        file.write("INSERT INTO FLASHINDICADORS (indicador) VALUES ('{}');\n".format(ind))

    file.write("COMMIT;\n\n")

    # Write the commands to create FLASHUPS
    file.write("DROP TABLE FLASHUPS;\n")
    file.write("CREATE TABLE FLASHUPS ( up varchar2(5), intervencio int );\n")

    with open('./FLASHUPS.txt','r') as upFile:
        ups = {line.split("\t")[0]:line.split("\t")[1].replace("\n","") for line in upFile}
    
    for k, i in ups.items():
        file.write("INSERT INTO FLASHUPS (up, intervencio) VALUES ('{}', {});\n".format(k, i))

    file.write("COMMIT;\n\n")

    # Write the commands to create FLASHPROFEXCLOSOS
    file.write("DROP TABLE FLASHPROFEXCLOSOS;\n")
    file.write("CREATE TABLE FLASHPROFEXCLOSOS ( login varchar2(12), up varchar2(10), uab varchar2(10), tipus varchar2(10) );\n")

    with open('./FLASHPROFEXCLOSOS.txt','r') as proFile:
        profs = [prof.replace("\n","") for prof in proFile]
    
    for prof in profs:
        file.write("INSERT INTO FLASHPROFEXCLOSOS (login) VALUES ('{}');\n".format(prof))

    file.write("COMMIT;")