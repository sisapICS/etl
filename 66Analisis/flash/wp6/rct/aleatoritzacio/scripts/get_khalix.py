# -*- coding: utf8 -*-

from lvclient import LvClient
import sisapUtils as u

#import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

EQAXXX = exemple(c, 'EQA0212,EQA0205,EQA0235,EQA0213,EQD0240,EQA0201,EQA0220; A2410; AMBITOS###; NUM,DEN; EDATS5; NOINSAT; SEXE')

c.close()

print('export')

file = u.tempFolder + "Indicadors_2410.txt"
with open(file, 'w') as f:
   f.write(EQAXXX)
