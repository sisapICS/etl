##### Character

```{r}
if (params$skim) {
  if ("character" %in% skim_type) {
  dt.datatable <- dt.skim[skim_type == "character", c("skim_variable", 
                                                      "n_missing", "complete_rate",
                                                      "character.min", "character.max", "character.empty", "character.n_unique", "character.whitespace")]
  datatable(dt.datatable,
            rownames = FALSE,
            filter = "top",
            options = list(ordering = T,
                           pageLength = nrow(dt.datatable),
                           columnDefs = list(list(className = 'dt-center', targets = 0:7)))) %>%
            formatRound(columns = c('complete_rate'), digits = 2)
  } else {print("No hi ha columnes tipus CHARACTER")}
} else {print("No s'executa SKIM: volum de dades molt gran")}  
```

##### Numeric

```{r}
if (params$skim) {
if ("numeric" %in% skim_type) {
    dt.datatable <- dt.skim[skim_type == "numeric", c("skim_variable", 
                                                      "n_missing", "complete_rate",
                                                      "numeric.mean", "numeric.sd",
                                                      "numeric.p0", "numeric.p25", "numeric.p50", "numeric.p75", "numeric.p100", "numeric.hist")]
    datatable(dt.datatable,
              rownames = FALSE,
              filter = "top",
              options = list(ordering = T,
                             pageLength = nrow(dt.datatable),
                             columnDefs = list(list(className = 'dt-center', targets = 0:10)))) %>%
              formatRound(columns = c("complete_rate","numeric.mean", "numeric.sd"), digits = 2)
  } else {print("No hi ha columnes tipus NUMERIC")}
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

##### Data

```{r}
if (params$skim) {
  if ("Date" %in% skim_type) {
      # dt.datatable <- dt.skim[skim_type %in% c("Date", "POSIXct"), c("skim_variable", 
      #                                                                "n_missing", "complete_rate",
      #                                                                "POSIXct.min", "POSIXct.max", "POSIXct.median", "POSIXct.n_unique")]
      dt.datatable <- dt.skim[skim_type == "Date", c("skim_variable", 
                                                          "n_missing", "complete_rate",
                                                          "Date.min", "Date.max", "Date.median", "Date.n_unique")]      
      datatable(dt.datatable,
                rownames = FALSE,
                filter = "top",
                options = list(ordering = T,
                               pageLength = nrow(dt.datatable),
                               columnDefs = list(list(className = 'dt-center', targets = 0:6)))) %>%
                formatRound(columns = c("complete_rate"), digits = 2)
  } else {print("No hi ha columnes tipus DATA")}
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```
