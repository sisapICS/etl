# -*- coding: utf8 -*-

"""
neurodesenvolupament
"""

import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u


db = 'neuroD'

dx_file =  "codis_dx.txt"

class its_dades(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_idcip()
        self.get_assignada()
        self.get_dx()
        self.export()

    def get_idcip(self):
        """id_cip to hash"""
        u.printTime("id_cip i hash")
        self.id_to_hash = {}
        
        sql = """select 
                    id_cip, id_cip_sec, hash_d, codi_sector 
                from 
                    u11"""
        for id, id_sec, hash, sec in u.getAll(sql, 'import'):
            self.id_to_hash[id] = hash
    
    def get_assignada(self):
        """."""
        u.printTime("assignada")
        self.pob = []
        self.inclosos = {}
        sql = 'select id_cip, up, abs, up_rca, data_naix, sexe, nacionalitat, ates from assignada_tot where edat < 16'
        for id_cip, up, abs, up_rca, data_naix, sexe, nacionalitat, ates  in u.getAll(sql, 'nodrizas'):
            hash = self.id_to_hash[id_cip] if id_cip in self.id_to_hash else None
            self.pob.append([hash, up, abs, up_rca, data_naix, sexe, nacionalitat, ates])
            self.inclosos[hash] = True
    
    
    def get_dx(self):
        """."""
        u.printTime("problemes")
        dx_codis = []
        tip_dx = {}
        self.diagnostics = []
        for (dx,  desc, grup) in u.readCSV(dx_file, sep='@'):
            dx_codis.append(dx)
            tip_dx[dx] = {'desc': desc, 'grup':grup}
        in_crit = tuple(dx_codis)

        sql = "select id_cip, pr_dde,  pr_cod_ps, pr_dba \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_cod_ps in {}  and \
                            pr_data_baixa = 0".format(in_crit)
        for id, dde, codi, dba in u.getAll(sql, 'import'):
            desc = tip_dx[codi]['desc']
            grup = tip_dx[codi]['grup']
            hash = self.id_to_hash[id] if id in self.id_to_hash else None
            if hash in self.inclosos:
                self.diagnostics.append([hash, grup, codi, desc, dde, dba])
        

    def export(self):
        """."""
        u.printTime("export")
        tb = "cohort"
        cols = ("hash varchar(40)","up varchar(10)", "abs varchar(10)", "up_rca varchar(10)", "data_naix date", "sexe varchar(5)", "nacionalitat varchar(10)", "ates int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pob, tb, db)
        
        tb = "diagnostics"
        cols = ("hash varchar(40)","grup varchar(300)", "codi varchar(50)", "descripcio varchar(300)", "dde date", "dba date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.diagnostics, tb, db)
                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    its_dades()
    
    u.printTime("Final")    