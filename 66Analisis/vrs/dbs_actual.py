# -*- coding: latin1 -*-

"""
Projecte Nirse
"""

import collections as c
import psutil as p
import datetime as d
import difflib as s

import sisapUtils as u



class Nirse(object):
    """."""
    
    def __init__(self):
        """."""
        self.dbs()
        self.export()
        
    def dbs(self):
        """id_cip to hash"""
        u.printTime("dbs")
        self.pob = []
        
        sql = """select 
                    c_cip, c_up
                from 
                    dwsisap.dbs where c_edat_anys <2"""
        for id, up in u.getAll(sql, 'exadata'):
            self.pob.append([id, up])
    
   
    def export(self):
        """."""
        u.printTime("export")
        
        db = "nirse2"
        tb = "dbs"
        cols = ("hash varchar(40)", "up varchar(10)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pob, tb, db)     
        
         
if __name__ == '__main__':
    u.printTime("Inici")
    
    Nirse()

    u.printTime("Fi")