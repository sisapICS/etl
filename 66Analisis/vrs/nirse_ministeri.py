# -*- coding: latin1 -*-

"""
Projecte Nirse Ministeri
"""

import collections as c
import psutil as p
import datetime as d
import difflib as s

import sisapUtils as u



class Nirse(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_poblacio()
        self.get_casos()
        self.get_multitests()
        self.get_cmbdH()
        self.export()
    
    
    def get_poblacio(self):
        """."""
        u.printTime("DBC Poblacio")
        self.dades = []
        self.en_campanya_hash = {}
        self.en_campanya_nia = {}
        self.en_campanya_cip = {}
        sql = """select hash, nia, substr(cip, 0, 13), data_naixement, decode(sexe, '0', 'Home', '1', 'Dona', 'No disponible'),  nacionalitat, abs, eap, situacio, data_situacio, data_defuncio, localitat from dwsisap.RCA_CIP_NIA 
            where data_naixement between DATE '2023-04-01' AND DATE '2024-03-19'"""
        for hash, nia, cip, naix, sexe, nac, abs,eap,situacio, data_situacio, data_defuncio, localitat in u.getAll(sql, 'exadata'):
            self.en_campanya_hash[hash] = True
            self.en_campanya_nia[nia] = hash
            self.en_campanya_cip[cip] = hash
            self.dades.append([hash, naix, sexe, nac, abs,eap, localitat, situacio, data_situacio, data_defuncio])
    
    def get_casos(self):
        """."""
        u.printTime("Casos: taula SP")
        self.casos = []
        
        sql = """SELECT centre, substr(cip, 0, 13), to_date(fechahosp, 'DD-MM-YYYY'), data_prova,
                CASE WHEN enterovirus =1 THEN 'Enterovirus' ELSE ' ' END AS entero,
                CASE WHEN adenovirus=1 THEN 'Adenovirus' ELSE ' ' END AS adenovirus,
                CASE WHEN metapneumovirus=1 THEN 'Metapneumovirus' ELSE '' END AS metap,
                CASE WHEN bocavirus=1 THEN 'Bocavirus' ELSE ' ' END AS bocavirus,
                CASE WHEN coronavirus229E =1 OR coronavirusNL63=1 OR coronavirusoc43=1 THEN 'Coronavirus no sars' ELSE '' END AS corona,
                CASE WHEN sarscov2=1 THEN 'SARS-COV-2' ELSE ' ' END AS sarscov2,
                CASE WHEN gripe=1 THEN 'Gripe' ELSE '' END AS grip,
                CASE WHEN parainfluenza=1 THEN 'Parainfluenza' ELSE ' ' END AS parainfluenza,
                CASE WHEN rinovirus=1 THEN 'Rinovirus' ELSE ' ' END AS rinovirus,
                CASE WHEN enterovirus=1 OR adenovirus=1 OR metapneumovirus=1 OR bocavirus=1 OR coronavirus229E =1 OR coronavirusNL63=1 OR coronavirusoc43=1 OR sarscov2=1 OR gripe=1 OR rinovirus=1 OR enterovirus=1 THEN 1 ELSE 0 END AS virus,
                to_date(fechaltahosp, 'DD-MM-YYYY'), 
                uci
                from	DWSPUBLICA.IRAG_SINDRO_SISAP_VRS 
                WHERE vrs=1 AND  to_date(fechahosp, 'DD-MM-YYYY') between DATE '2023-10-01' and DATE '2024-03-31'"""
        for centre, cip, data_ingres, data_vrs, entero, adeno, metap, bocavirus, corona, sarscov2, grip, parainfluenza, rinovirus, virus, alta, uci in u.getAll(sql, 'exadata'):
            if cip in self.en_campanya_cip:
                hash = self.en_campanya_cip[cip]
                viruses = None
                if virus == 1:
                    viruses = str(entero) + ' ' + str(adeno) + ' ' + str(metap) + ' ' + str(bocavirus) + ' ' + str(corona) + ' ' + str(sarscov2) + ' ' + str(grip) + ' ' + str(parainfluenza) + ' ' + str(rinovirus)
                self.casos.append([hash, centre, data_ingres, data_vrs, virus, viruses, alta, uci])
                
    def get_multitests(self):
        """."""
        u.printTime("multitests ecap")
        self.multitests = []
        
        sql = """SELECT cip, data, tr_adenovirus, tr_vrs, tr_infla, tr_inflb, tr_sarscov2 from dwsisap.sivic_multitest_master where  data > DATE '2023-09-30'"""
        for cip, data, adeno, vrs, gripA, gripB, sarscov2 in u.getAll(sql, 'exadata'):
            if cip in self.en_campanya_cip:
                hash = self.en_campanya_cip[cip]
                self.multitests.append([hash, data, adeno, vrs, gripA, gripB, sarscov2])

    def get_cmbdH(self):
        """."""
        u.printTime("CMBDH")
        self.cmbdH = []
        
        sql ="""SELECT nia, up_assistencia, c_ingres, pr_ingres, pr_interna, data_ingres, data_alta, data_ingres_uci, data_alta_uci, ingres_uci, DIES_T_UCI, dies_est, c_alta, dp, poap, ds1, poa1, ds2, poa2 FROM dwcatsalut.TF_CMBDHA
                where data_ingres  > DATE '2023-09-30'"""
        for nia, up, c_ingres, pr_ingres, pr_interna, data_ingres, data_alta, data_ingres_uci, data_alta_uci, ingres_uci, DIES_T_UCI, dies_est, c_alta, dp, poap, ds1, poa1, ds2, poa2 in u.getAll(sql, 'exadata'):
            if nia in self.en_campanya_nia:
                hash = self.en_campanya_nia[nia]
                bronquiolitis, bronquiolitisVRS, bronquiolitisAltres = 0, 0, 0
                if dp in ('J21','J210','J211','J218','J219', 'J121','J205','J22') or ds1 in ('J21','J210','J211','J218','J219', 'J121','J205','J22') or ds2 in ('J21','J210','J211','J218','J219','J121','J205','J22'):
                    bronquiolitis = 1
                if dp == 'J210' or ds1 == 'J210' or ds2 == 'J210':
                    bronquiolitisVRS = 1
                if dp in ('J211','J218') or ds1 in ('J211','J218') or ds2 in ('J211','J218'):
                    bronquiolitisAltres = 1
                self.cmbdH.append([hash, up, c_ingres, pr_ingres, pr_interna, data_ingres, data_alta, data_ingres_uci, data_alta_uci, ingres_uci, DIES_T_UCI, dies_est, c_alta, dp, poap, ds1, poa1, ds2, poa2, bronquiolitis, bronquiolitisVRS, bronquiolitisAltres])
    
    
    def export(self):
        """."""
        u.printTime("export")
        
        db = "nirse_ministerio"
        
        tb = "cohort"
        cols = ("hash varchar(40)", "naixement date", "sexe varchar(40)", "nacionalitat varchar(10)", "abs varchar(10)", "eap varchar(10)", "localitat varchar(50)",
        "situacio varchar(10)", "data_situacio date", "data_defuncio date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.dades, tb, db)     
        
        tb = "casos"
        cols = ("hash varchar(40)", "centre varchar(50)", "ingres date",  "data_vrs varchar(100)", "virus int", "viruses varchar(500)", "alta date", "uci int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.casos, tb, db)
        
        tb = "multitests"
        cols = ("hash varchar(40)", "data date", "adeno int", "vrs int", "gripA int", "gripB int", "sarscov2 int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.multitests, tb, db) 

        tb = "cmbdh"
        cols = ("hash varchar(40)", "up varchar(10)", "c_ingres varchar(40)", "pr_ingres varchar(40)", "pr_interna varchar(40)", "data_ingres date", "data_alta date", "data_ingres_uci date",
        "data_alta_uci date", "ingres_uci varchar(40)", "DIES_T_UCI int", "dies_est int", "c_alta varchar(40)", "dp varchar(100)", "poap varchar(100)", "ds1 varchar(100)", "poa1 varchar(100)", "ds2 varchar(100)", "poa2 varchar(100)",
        "bronquiolitis int", "bronquiolitisVRS int", "bronquiolitisAltres int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.cmbdH, tb, db)
         
if __name__ == '__main__':
    u.printTime("Inici")
    
    Nirse()

    u.printTime("Fi")