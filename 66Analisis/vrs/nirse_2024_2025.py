# -*- coding: latin1 -*-

"""
Projecte Nirse, nova cohort 2024 -2025
"""

import collections as c
import psutil as p
import datetime as d
import difflib as s

import sisapUtils as u



class Nirse(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_idcip()
        self.get_hashos()
        self.get_centres()
        self.get_nirse()
        self.get_poblacio()
        self.get_diagnostics()
        self.get_multitests()
        self.get_urgencies()
        self.get_cmbdH()
        self.get_controls_negatius()
        self.export()
        
    def get_idcip(self):
        """id_cip to hash"""
        u.printTime("id_cip i hash")
        self.id_to_hash = {}
        
        sql = """select 
                    id_cip, id_cip_sec, hash_d, codi_sector 
                from 
                    u11"""
        for id, id_sec, hash, sec in u.getAll(sql, 'import'):
            self.id_to_hash[id] = hash
    
    def get_hashos(self):
        """Hashos"""
        u.printTime("hashos")
        
        self.red_to_exa = {}
        sql = """select hash_redics, hash_covid from dwsisap.PDPTB101_RELACIO"""
        for h_redics, h_covid in u.getAll(sql, 'exadata'):
            self.red_to_exa[h_redics] = h_covid
    
    def get_centres(self):
        """."""
        u.printTime("RUP")
        self.centres = {}
        sql ="""select to_number(up_cod), up_des, tip_cod, tip_des, subtip_cod, subtip_des FROM dwsisap.DBC_RUP"""
        for up, des, tip, tip_des, subtip, subtip_des in u.getAll(sql, 'exadata'):
            self.centres[up] = {'des':des, 'tip':tip,'tip_des':tip_des, 'subtip': subtip,'subtip_des': subtip_des}
    
    def get_nirse(self):
        """ En teoria inclou els nirse que hi ha a ecap amb la variabla es_hc3"""
        u.printTime("Nirse")
        self.nirse = {}
        sql = """select cip, data, producte, es_hc3 from dwsisap.vrs_vacuna where campanya='2024'"""
        for cip, data, producte, hc3 in u.getAll(sql, 'exadata'):
            self.nirse[cip] = {'data':data, 'producte':producte, 'hc3': hc3}
            
        
    def get_poblacio(self):
        """."""
        u.printTime("DBC Poblacio")
        self.dades = []
        self.en_campanya_hash = {}
        self.en_campanya_nia = {}
        self.en_campanya_cip = {}
        inclosos_sivic = {}
        sql = """SELECT cip, motiu FROM dwsisap.VRS_CAMPANYA vc WHERE campanya='2024'"""
        for cip, motiu in u.getAll(sql, 'exadata'):
            inclosos_sivic[cip] = motiu
            
        sql = """select hash, nia, substr(cip, 0, 13), data_naixement, decode(sexe, '0', 'Home', '1', 'Dona', 'No disponible'),  nacionalitat, abs, eap, situacio, data_situacio, data_defuncio from dwsisap.RCA_CIP_NIA 
            where data_naixement between DATE '2024-04-01' AND DATE '2025-03-31'"""
        for hash, nia, cip, naix, sexe, nac, abs,eap,situacio, data_situacio, data_defuncio in u.getAll(sql, 'exadata'):
            if cip in inclosos_sivic:
                self.en_campanya_hash[hash] = True
                self.en_campanya_nia[(nia)] = hash
                self.en_campanya_cip[cip] = hash
                nirse = 1 if cip in self.nirse else 0
                dat_nirse = self.nirse[cip]['data'] if cip in self.nirse else None
                producte = self.nirse[cip]['producte'] if cip in self.nirse else None
                hc3 = self.nirse[cip]['hc3'] if cip in self.nirse else None
                self.dades.append([hash, naix, sexe, nac, abs,eap,situacio, data_situacio, data_defuncio, nirse, dat_nirse, hc3, producte])
       
    def get_diagnostics(self):
        """."""
        u.printTime("diagnostics ecap")
        self.outcomes = []
        
        sql = """SELECT hash, diagnostic, data, codi FROM dwsisap.SIVIC_ECAP_MASTER WHERE diagnostic IN ('Bronquiolitis', 'Pneum�nia v�rica','Pneum�nia') and data > DATE '2024-09-30'"""
        for hash, dx, data,codi in u.getAll(sql, 'exadata'):
            if hash in self.en_campanya_hash:
                if dx == 'Bronquiolitis' and codi == 'C01-J21.0':
                    dx = 'Bronquiolitis VRS'
                self.outcomes.append([hash, dx, data])
    
    def get_multitests(self):
        """."""
        u.printTime("multitests ecap")
        self.multitests = []
        
        sql = """SELECT cip, data, tr_adenovirus, tr_vrs, tr_infla, tr_inflb, tr_sarscov2 from dwsisap.sivic_multitest_master where  data > DATE '2024-09-30'"""
        for cip, data, adeno, vrs, gripA, gripB, sarscov2 in u.getAll(sql, 'exadata'):
            if cip in self.en_campanya_cip:
                hash = self.en_campanya_cip[cip]
                self.multitests.append([hash, data, adeno, vrs, gripA, gripB, sarscov2])
                
    def get_urgencies(self):
        """."""
        u.printTime("urgÃ¨ncies hx - cuap")
        self.urgencies = []
        
        sql = """SELECT c_nia, to_number(c_up),  c_motiu, dd1, dd2, dd3, dd4, c_data_entr, c_data_triatge, C_DATA_ALTA, f_bronquiolitis, f_grip, f_bronquitis, c_s_alta FROM DWSISAP.CMBD_URG where c_data_alta > DATE '2024-09-30'"""
        for nia, up, motiu, d1, d2, d3,d4, data_entr, data_triatge, data_alta, bronquiolitis, grip, bronquitis, c_s_alta in u.getAll(sql, 'exadata'):
            if nia in self.en_campanya_nia:
                hash = self.en_campanya_nia[(nia)]
                up_des = self.centres[up]['des'] if up in self.centres else None
                tip = self.centres[up]['tip'] if up in self.centres else None
                tip_des = self.centres[up]['tip_des'] if up in self.centres else None
                subtip = self.centres[up]['subtip'] if up in self.centres else None
                subtip_des = self.centres[up]['subtip_des'] if up in self.centres else None
                bronquiolitisVRS = 0
                if motiu == 'J210' or d1 == 'J210' or d2 == 'J210':
                    bronquiolitisVRS = 1
                self.urgencies.append([hash, up, up_des, tip, tip_des, subtip, subtip_des, motiu, d1, d2, d3,d4, data_entr, data_triatge, data_alta, bronquiolitis, bronquiolitisVRS, grip, bronquitis,  c_s_alta])
       
    def get_cmbdH(self):
        """."""
        u.printTime("CMBDH")
        self.cmbdH = []
        
        sql ="""SELECT nia, up_assistencia, c_ingres, pr_ingres, pr_interna, data_ingres, data_alta, data_ingres_uci, data_alta_uci, ingres_uci, DIES_T_UCI, dies_est, c_alta, dp, poap, ds1, poa1, ds2, poa2 FROM dwcatsalut.TF_CMBDHA
                where data_ingres  > DATE '2024-09-30'"""
        for nia, up, c_ingres, pr_ingres, pr_interna, data_ingres, data_alta, data_ingres_uci, data_alta_uci, ingres_uci, DIES_T_UCI, dies_est, c_alta, dp, poap, ds1, poa1, ds2, poa2 in u.getAll(sql, 'exadata'):
            if nia in self.en_campanya_nia:
                hash = self.en_campanya_nia[(nia)]
                bronquiolitis, bronquiolitisVRS, bronquiolitisAltres = 0, 0, 0
                if dp in ('J21','J210','J211','J218','J219') or ds1 in ('J21','J210','J211','J218','J219') or ds2 in ('J21','J210','J211','J218','J219'):
                    bronquiolitis = 1
                if dp == 'J210' or ds1 == 'J210' or ds2 == 'J210':
                    bronquiolitisVRS = 1
                if dp in ('J211','J218') or ds1 in ('J211','J218') or ds2 in ('J211','J218'):
                    bronquiolitisAltres = 1
                self.cmbdH.append([hash, up, c_ingres, pr_ingres, pr_interna, data_ingres, data_alta, data_ingres_uci, data_alta_uci, ingres_uci, DIES_T_UCI, dies_est, c_alta, dp, poap, ds1, poa1, ds2, poa2, bronquiolitis, bronquiolitisVRS, bronquiolitisAltres])
    
    def get_controls_negatius(self):
        """."""
        u.printTime("diagnostics ecap 2")
        self.controls_neg = []
        
        sql = """SELECT hash, diagnostic, data FROM dwsisap.sivic_ecap_master
            WHERE diagnostic IN ('Faringoamigdalitis estreptoc�ccica','Impetigen','Tinya','Varicel�la','Tos ferina','Herpangina','Febre aftosa','Escarlatina','Faringoamigdalitis','Escabiosi','Enteritis i diarrees','Otitis') and data > DATE '2024-09-30'"""
        for hash, dx, data in u.getAll(sql, 'exadata'):
            if hash in self.en_campanya_hash:
                self.controls_neg.append([hash, dx, data])
    
    def export(self):
        """."""
        u.printTime("export")
        
        db = "nirse24_25"
        tb = "cohort"
        cols = ("hash varchar(40)", "naixement date", "sexe varchar(40)", "nacionalitat varchar(10)", "abs varchar(10)", "eap varchar(10)",
        "situacio varchar(10)", "data_situacio date", "data_defuncio date", "nirse int", "data_nirse date", "es_hc3 int", "producte_nirse varchar(100)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.dades, tb, db)     
        
        tb = "diagnostics_ecap"
        cols = ("hash varchar(40)", "diagnostic varchar(100)", "data date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.outcomes, tb, db)
        
        tb = "multitests"
        cols = ("hash varchar(40)", "data date", "adeno int", "vrs int", "gripA int", "gripB int", "sarscov2 int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.multitests, tb, db) 
        
        tb = "urgencies"
        cols = ("hash varchar(40)", "up varchar(10)", "up_des varchar(300)", "tipus_cod varchar(10)", "tipus_des varchar(300)", "subtipus_cod varchar(10)", 
        "subtipus_des varchar(300)", "motiu varchar(100)", "d1 varchar(100)", "d2  varchar(100)", "d3  varchar(100)", "d4  varchar(100)", "data_entrada date", 
        "data_triatge date", "data_alta date", "bronquiolitis int", "bronquiolitisVRS int", 
        "grip int", "bronquitis int",  "c_s_alta varchar(50)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.urgencies, tb, db)
       
        
        tb = "cmbdh"
        cols = ("hash varchar(40)", "up varchar(10)", "c_ingres varchar(40)", "pr_ingres varchar(40)", "pr_interna varchar(40)", "data_ingres date", "data_alta date", "data_ingres_uci date",
        "data_alta_uci date", "ingres_uci varchar(40)", "DIES_T_UCI int", "dies_est int", "c_alta varchar(40)", "dp varchar(100)", "poap varchar(100)", "ds1 varchar(100)", "poa1 varchar(100)", "ds2 varchar(100)", "poa2 varchar(100)",
        "bronquiolitis int", "bronquiolitisVRS int", "bronquiolitisAltres int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.cmbdH, tb, db)
        
        tb = "controls_negatius"
        cols = ("hash varchar(40)", "diagnostic varchar(100)", "data date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.controls_neg, tb, db)
        
         
if __name__ == '__main__':
    u.printTime("Inici")
    
    Nirse()

    u.printTime("Fi")