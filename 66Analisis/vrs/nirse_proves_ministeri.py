# -*- coding: latin1 -*-

"""
Projecte Nirse Ministeri: multitests casos i controls
"""

import collections as c
import psutil as p
import datetime as d
import difflib as s
import hashlib as h

import sisapUtils as u

class Nirse(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_casos()
        self.get_multitests()
        self.export()
        
    def get_casos(self):
        """."""
        u.printTime("Multitests")
        
        self.casos_controls = {}

        sql = """select hash, cas, idcas, id, data_ingres from matching"""
        for hash, cas, idcas, id, ingres in u.getAll(sql, 'nirse_ministerio'):
            self.casos_controls[hash] ={'cas': cas, 'idcas': idcas, 'id': id, 'ingres':ingres}
        
        
    def get_multitests(self):
        """."""
        u.printTime("Multitests")
        
        self.proves_casos = []
        self.proves_controls = []
        
        sql ="""select hash, data, vrs from multitests"""
        for hash, data, vrs in u.getAll(sql, 'nirse_ministerio'):
            if hash in self.casos_controls:
                cas = self.casos_controls[hash]['cas']
                idcas = self.casos_controls[hash]['idcas']
                id = self.casos_controls[hash]['id']
                ingres = self.casos_controls[hash]['ingres']
                if cas == 'Cas':
                    if data < ingres:
                        self.proves_casos.append([hash, id, vrs, data,2])
                if cas == 'Control':
                    self.proves_controls.append([hash, id, vrs, data,2])    
    
    def export(self):
        """."""
        u.printTime("export")
        
        db = "nirse_ministerio"
        
        tb = "dades_proves_casos"
        cols = ("hash varchar(40)", "identificadorcaso int", "testpcr int", "test_fecha date", "Test_tipo int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.proves_casos, tb, db)  
        
        tb = "dades_proves_controls"
        cols = ("hash varchar(40)", "identificadorcontrol varchar(20)", "testpcr int", "test_fecha date", "Test_tipo int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.proves_controls, tb, db)    
     
         
if __name__ == '__main__':
    u.printTime("Inici")
    
    Nirse()

    u.printTime("Fi")