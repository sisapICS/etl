# -*- coding: latin1 -*-

"""
Projecte Nirse
"""

import collections as c
import psutil as p
import datetime as d
import difflib as s

import sisapUtils as u



class Nirse(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_idcip()
        self.get_hashos()
        self.get_campanya()
        self.get_nirse()
        self.get_nirse_ecap()
        self.get_poblacio()
        self.export()
        
    def get_idcip(self):
        """id_cip to hash"""
        u.printTime("id_cip i hash")
        self.id_to_hash = {}
        
        sql = """select 
                    id_cip, id_cip_sec, hash_d, codi_sector 
                from 
                    u11"""
        for id, id_sec, hash, sec in u.getAll(sql, 'import'):
            self.id_to_hash[id] = hash
    
    def get_hashos(self):
        """Hashos"""
        u.printTime("hashos")
        
        self.red_to_exa = {}
        sql = """select hash_redics, hash_covid from dwsisap.PDPTB101_RELACIO"""
        for h_redics, h_covid in u.getAll(sql, 'exadata'):
            self.red_to_exa[h_redics] = h_covid

    def get_campanya(self):
        """mirem la campanya que fa francesc"""
        u.printTime("campanya sivic")
        
        self.campanya = {}
        sql = """SELECT substr(cip, 0, 13), motiu FROM dwsisap.VRS_CAMPANYA vc WHERE campanya='2023'"""
        for cip, motiu in u.getAll(sql, 'exadata'):
            self.campanya[cip] = motiu
    
    def get_nirse(self):
        """."""
        u.printTime("Nirse")
        self.nirse = {}
        sql = """select cip, data, producte from dwsisap.vrs_vacuna where campanya='2023'"""
        for cip, data, producte in u.getAll(sql, 'exadata'):
            self.nirse[cip] = {'data':data, 'producte':producte}
    
    def get_nirse_ecap(self):
        """."""
        u.printTime("Nirse ECAP")
        self.nirse_ecap = {}
        sql = """select  id_cip, va_u_data_vac, va_u_cod from vacunes where va_u_cod in  ('P00162', 'P00163') and va_u_data_vac >= '2023-10-01'"""
        for id, dat, producte in u.getAll(sql, 'import'):
            hash = self.id_to_hash[id] if id in self.id_to_hash else None
            hash_covid = self.red_to_exa[hash] if hash in self.red_to_exa else None
            self.nirse_ecap[hash_covid] = {'data':dat, 'producte':producte}
            
        
    def get_poblacio(self):
        """."""
        u.printTime("DBC Poblacio")
        self.dades = []
        sql = """select hash, nia, substr(cip, 0, 13), case when data_naixement BETWEEN DATE '2023-04-01' AND DATE '2023-09-30' THEN 'primera cohort' else 'segona cohort' end as cohorts,data_naixement, decode(sexe, '0', 'Home', '1', 'Dona', 'No disponible'),  nacionalitat, abs, eap, situacio, data_situacio, data_defuncio from dwsisap.RCA_CIP_NIA 
            where data_naixement between DATE '2023-04-01' AND DATE '2024-03-31'"""
        for hash, nia, cip, cohort, naix, sexe, nac, abs,eap,situacio, data_situacio, data_defuncio in u.getAll(sql, 'exadata'):
            nirse = 1 if cip in self.nirse else 0
            dat_nirse = self.nirse[cip]['data'] if cip in self.nirse else None
            producte = self.nirse[cip]['producte'] if cip in self.nirse else None
            campanya_sivic = 1 if cip in self.campanya else 0
            motiu_campanya = self.campanya[cip] if cip in self.campanya else 0
            if nirse == 0:
                if hash in self.nirse_ecap:
                    nirse = 1
                    dat_nirse = self.nirse_ecap[hash]['data']      
            self.dades.append([hash, cohort, campanya_sivic, motiu_campanya, naix, sexe, nac, abs,eap,situacio, data_situacio, data_defuncio, nirse, dat_nirse])
       
   
    def export(self):
        """."""
        u.printTime("export")
        
        db = "nirse_ministerio"
        tb = "cobertures_202404"
        cols = ("hash varchar(40)", "cohort varchar(100)", "campanya_sivic int", "motiu_campanya int", "naixement date", "sexe varchar(40)", "nacionalitat varchar(10)", "abs varchar(10)", "eap varchar(10)",
        "situacio varchar(10)", "data_situacio date", "data_defuncio date", "nirse int", "data_nirse date", "producte_nirse varchar(100)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.dades, tb, db)     
        
         
if __name__ == '__main__':
    u.printTime("Inici")
    
    Nirse()

    u.printTime("Fi")