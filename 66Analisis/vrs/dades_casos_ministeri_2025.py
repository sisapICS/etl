# -*- coding: latin1 -*-

"""
Projecte Nirse Ministeri: variables dels casos
"""

import collections as c
import psutil as p
import datetime as d
import difflib as s
import hashlib as h

import sisapUtils as u

class Nirse(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_idcip()
        self.get_hashos()
        self.get_poblacio()
        self.get_lactancia()
        self.get_dx()
        self.get_casos()
        self.export()
        
    def get_idcip(self):
        """id_cip to hash"""
        u.printTime("id_cip i hash")
        self.id_to_hash = {}
        self.id_sec_to_hash = {}
        sql = """select 
                    id_cip, id_cip_sec, hash_d, codi_sector 
                from 
                    u11"""
        for id, id_sec, hash, sec in u.getAll(sql, 'import'):
            self.id_to_hash[id] = hash
            self.id_sec_to_hash[id_sec] = hash
    
    def get_hashos(self):
        """Hashos"""
        u.printTime("hashos")
        
        self.red_to_exa = {}
        sql = """select hash_redics, hash_covid from dwsisap.PDPTB101_RELACIO"""
        for h_redics, h_covid in u.getAll(sql, 'exadata'):
            self.red_to_exa[h_redics] = h_covid
    
    def get_poblacio(self):
        """."""
        u.printTime("DBC Poblacio")
        self.poblacio = {}
        
        sql = """select hash, if(naixement <'2023-10-01', 1, 2), naixement, sexe, nacionalitat, abs, data_situacio, if(data_defuncio ='', 0, 1),data_defuncio, localitat from cohort"""
        for hash, cohort, naixement, sexe, nacionalitat, abs, data_situacio, defuncio, data_defuncio, localitat in u.getAll(sql, 'nirse_ministerio_2025'):
            self.poblacio[hash] = {'cohort': cohort, 'naix': naixement, 'sexe': sexe, 'exitus': defuncio, 'exitus_data': data_defuncio, 'localitat': localitat}   
    
    def get_lactancia(self):
        """."""
        
        self.lactancia = {}
        sql = """select id_cip_sec, dat from ped_variables where agrupador in (764, 765)"""
        for id, dat in u.getAll(sql, 'nodrizas'):
            hash_redics = self.id_sec_to_hash[id] if id in self.id_sec_to_hash else None
            hash = self.red_to_exa[hash_redics] if hash_redics in self.red_to_exa else None
            if hash in self.poblacio:
                self.lactancia[hash] = dat
    
    def get_dx(self):
        """."""
        u.printTime("problemes")
        dx_file =  "codis_dx.txt"
        dx_codis = []
        tip_dx = {}
        self.diagnostics = {}
        for (grup, dx) in u.readCSV(dx_file, sep='@'):
            dx_codis.append(dx)
            tip_dx[dx] = grup
        in_crit = tuple(dx_codis)

        sql = "select id_cip, pr_dde,  pr_cod_ps, pr_dba \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_cod_ps in {}  and \
                            pr_data_baixa = 0".format(in_crit)
        for id, dde, codi, dba in u.getAll(sql, 'import'):
            hash_redics = self.id_to_hash[id] if id in self.id_to_hash else None
            hash = self.red_to_exa[hash_redics] if hash_redics in self.red_to_exa else None
            grup = tip_dx[codi]
            if hash in self.poblacio:
                if (hash, grup) in self.diagnostics:
                    data_ant = self.diagnostics[(hash, grup)]
                    if dde < data_ant:
                        self.diagnostics[(hash, grup)] = dde
                else:
                    self.diagnostics[(hash, grup)] = dde
    
    def get_casos(self):
        """."""
        u.printTime("Casos")
        self.casos = []
        sql = """select identificadorcaso, hash, hospitalizacion from casos"""
        for id, hash, ingres in u.getAll(sql, 'nirse_ministerio_2025'):
            displasia = 0
            displasia_dde = self.diagnostics[(hash, 'displasia')] if (hash, 'displasia') in self.diagnostics else ''
            if displasia_dde != '':
                if displasia_dde < ingres:
                    displasia = 1
                else:
                    displasia_dde = ''
            cardiopatia = 0
            cardiopatia_dde = self.diagnostics[(hash, 'cardiopatia')] if (hash, 'cardiopatia') in self.diagnostics else ''
            if cardiopatia_dde != '':
                if cardiopatia_dde < ingres:
                    cardiopatia = 1
                else:
                    cardiopatia_dde = ''
            tmetabolic = 0
            tmetabolic_dde = self.diagnostics[(hash, 'trastorno metabolico')] if (hash, 'trastorno metabolico') in self.diagnostics else ''
            if tmetabolic_dde != '':
                if tmetabolic_dde < ingres:
                    tmetabolic = 1
                else:
                    tmetabolic_dde = ''
            Down = 0
            Down_dde = self.diagnostics[(hash, 'Down')] if (hash, 'Down') in self.diagnostics else ''
            if Down_dde != '':
                if Down_dde < ingres:
                    Down = 1
                else:
                    Down_dde = ''
            fibrosisquistica = 0
            fibrosisquistica_dde = self.diagnostics[(hash, 'fibrosisquistica')] if (hash, 'fibrosisquistica') in self.diagnostics else ''
            if fibrosisquistica_dde != '':
                if fibrosisquistica_dde < ingres:
                    fibrosisquistica = 1
                else:
                    fibrosisquistica_dde = ''
            inmunodef = 0
            inmunodef_dde = self.diagnostics[(hash, 'inmunodef')] if (hash, 'inmunodef') in self.diagnostics else ''
            if inmunodef_dde != '':
                if inmunodef_dde < ingres:
                    inmunodef = 1
                else:
                    inmunodef_dde = ''
            lac_materna = 1 if hash in self.lactancia else 9
            lac_materna_dat = self.lactancia[hash] if hash in self.lactancia else ''
            self.casos.append([hash, id, displasia, cardiopatia, tmetabolic, Down, fibrosisquistica, 9, inmunodef,
                                    displasia_dde, cardiopatia_dde, tmetabolic_dde, Down_dde, fibrosisquistica_dde, None, inmunodef_dde, 9, None, lac_materna,lac_materna_dat])

    
    
    def export(self):
        """."""
        u.printTime("export")
        
        db = "nirse_ministerio_2025"
        
        tb = "dades_casos"
        cols = ("hash varchar(40)", "identificadorcaso int", "displasia int", "cardiopatia int", "tmetabolico int", "sdown int", "fibrosisquistica int",
        "otras int", "inmunodef int", "displasia_fecha date", "cardiopatia_fecha date", "tmetabolico_fecha date", "sdown_fecha date", "fibrosisquistica_fecha date", "otras_fecha date", "inmunodef_fecha date",
        "paliativos int", "paliativos_fecha date", "lactancia int", "lactancia_fecha date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.casos, tb, db)     
        
        
       
         
if __name__ == '__main__':
    u.printTime("Inici")
    
    Nirse()

    u.printTime("Fi")