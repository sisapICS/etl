# -*- coding: latin1 -*-

"""
Projecte Nirse Ministeri: creem taula
"""

import collections as c
import psutil as p
import datetime as d
import difflib as s
import hashlib as h

import sisapUtils as u

class Nirse(object):
    """."""
    
    def __init__(self):
        """."""
        self.export()
        #self.export_cips()
        
    
    def export(self):
        """."""
        u.printTime("export")
        
        db = "exadata2"
        
        casos_d = []
        
        
        sql = """select identificadorcaso, displasia ,cardiopatia, tmetabolico, sdown, fibrosisquistica,
        otras, inmunodef, displasia_fecha, cardiopatia_fecha, tmetabolico_fecha, sdown_fecha, fibrosisquistica_fecha, otras_fecha, inmunodef_fecha,
        paliativos, paliativos_fecha, lactancia, lactancia_fecha from dades_casos"""
        for identificadorcaso, displasia ,cardiopatia, tmetabolico, sdown, fibrosisquistica,        otras, inmunodef, displasia_fecha, cardiopatia_fecha, tmetabolico_fecha, sdown_fecha, fibrosisquistica_fecha, otras_fecha, inmunodef_fecha,   paliativos, paliativos_fecha, lactancia, lactancia_fecha  in u.getAll(sql, 'nirse_ministerio_2025'):
            casos_d.append([identificadorcaso,  displasia, cardiopatia, 
            tmetabolico, sdown, fibrosisquistica, otras, inmunodef, displasia_fecha, cardiopatia_fecha, tmetabolico_fecha, sdown_fecha, fibrosisquistica_fecha, otras_fecha, inmunodef_fecha,
            paliativos, paliativos_fecha, lactancia, lactancia_fecha])
        
        tb = "ministerio_datos_casos_2025"
        cols = ( "identificadorcaso int",  "displasia int", "cardiopatia int", "tmetabolico int", "sdown int", "fibrosisquistica int",
        "otras int", "inmunodef int", "displasia_fecha date", "cardiopatia_fecha date", "tmetabolico_fecha date", "sdown_fecha date", "fibrosisquistica_fecha date", "otras_fecha date", "inmunodef_fecha date",
        "paliativos int", "paliativos_fecha date", "lactancia int", "lactancia_fecha date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(casos_d, tb, db)     
        
        users= ['DWSISAP_ROL','DWSPUBLICA']
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db) 
        
        controls_d = []
        sql = """select identificadorcaso, identificadorcontrol, criterios,  fdn,  provincia, immunizacion, immunizacion_fecha, sexe, 
        if(if(edad_gestacional>100,edad_gestacional/10, edad_gestacional)>43,43,if(edad_gestacional>100,edad_gestacional/10, edad_gestacional)) as edad_gestacional, 
         if(if(CAST(replace(peso_nacimiento,',','') AS UNSIGNED)<500,CAST(replace(left(peso_nacimiento,3),',','') AS unsigned)*100,CAST(replace(peso_nacimiento,',','') AS unsigned)) <400, 
            if(CAST(replace(peso_nacimiento,',','') AS UNSIGNED)<500,CAST(replace(left(peso_nacimiento,3),',','') AS unsigned)*100,CAST(replace(peso_nacimiento,',','') AS unsigned))*10, if(CAST(replace(peso_nacimiento,',','') AS UNSIGNED)<500,CAST(replace(left(peso_nacimiento,3),',','') AS unsigned)*100,CAST(replace(peso_nacimiento,',','') AS unsigned)))as pes, 
            gestacionmultiple, primerhijo, displasia, 
        cardiopatia, tmetabolico, sdown, fibrosisquistica, otras, inmunodef, displasia_fecha, cardiopatia_fecha, tmetabolico_fecha, sdown_fecha, fibrosisquistica_fecha, otras_fecha, inmunodef_fecha,
        paliativos, paliativos_fecha, lactancia, lactancia_fecha, hospitalizacionprevia, hospitalizacionprevia_fecha from dades_controls_nous"""
        for  identificadorcaso, identificadorcontrol, criterios,  fdn,  provincia, immunizacion, immunizacion_fecha, sexe, edad_gestacional, peso_nacimiento, gestacionmultiple, primerhijo, displasia,         cardiopatia, tmetabolico, sdown, fibrosisquistica, otras, inmunodef, displasia_fecha, cardiopatia_fecha, tmetabolico_fecha, sdown_fecha, fibrosisquistica_fecha, otras_fecha, inmunodef_fecha,        paliativos, paliativos_fecha, lactancia, lactancia_fecha, hospitalizacionprevia, hospitalizacionprevia_fecha in u.getAll(sql, 'nirse_ministerio_2025'):
            controls_d.append([identificadorcaso, identificadorcontrol, criterios,  fdn,  provincia, immunizacion, immunizacion_fecha, sexe, edad_gestacional, peso_nacimiento, gestacionmultiple, primerhijo, displasia, 
        cardiopatia, tmetabolico, sdown, fibrosisquistica, otras, inmunodef, displasia_fecha, cardiopatia_fecha, tmetabolico_fecha, sdown_fecha, fibrosisquistica_fecha, otras_fecha, inmunodef_fecha,
        paliativos, paliativos_fecha, lactancia, lactancia_fecha, hospitalizacionprevia, hospitalizacionprevia_fecha])
        
        
        tb = "ministerio_datos_controles_2025"
        cols = ("identificadorcaso int", "identificadorcontrol varchar(20)", "criterios int",  "fdn date",  "provincia int", "inmunizacion int", "inmunizacion_fecha date", 
        "sexo varchar(20)", "edad_gestacional int", "peso_nacimiento int", "gestacionmultiple int", "primerhijo int", "displasia int", "cardiopatia int", "tmetabolico int", "sdown int", "fibrosisquistica int",
        "otras int", "inmunodef int", "displasia_fecha date", "cardiopatia_fecha date", "tmetabolico_fecha date", "sdown_fecha date", "fibrosisquistica_fecha date", "otras_fecha date", "inmunodef_fecha date",
        "paliativos int", "paliativos_fecha date", "lactancia int", "lactancia_fecha date", "hospitalizacionprevia int", "hospitalizacionprevia_fecha date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        
        u.listToTable(controls_d, tb, db)     
        
        users= ['DWSISAP_ROL', 'DWSPUBLICA']
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db) 
        
        #proves_casos = []
        #sql = """select identificadorcaso, testpcr, test_fecha, test_tipo from dades_proves_casos"""
        #for identificadorcaso, testpcr, test_fecha, test_tipo in u.getAll(sql, 'nirse_ministerio'):
        #    proves_casos.append([identificadorcaso, testpcr, test_fecha, test_tipo])
        
        #tb = "ministerio_pruebas_casos"
        #cols = ("identificadorcaso int", "testpcr int", "test_fecha date", "test_tipo int")
        #u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        #u.listToTable(proves_casos, tb, db)
        
        #users= ['DWSISAP_ROL','DWSPUBLICA']
        #for user in users:
        #    u.execute("grant select on {} to {}".format(tb,user),db) 
        
        proves_controls = []
        #sql = """select identificadorcontrol, testpcr, test_fecha, test_tipo from dades_proves_controls"""
        #for identificadorcontrol, testpcr, test_fecha, test_tipo in u.getAll(sql, 'nirse_ministerio'):
        #    proves_controls.append([identificadorcontrol, testpcr, test_fecha, test_tipo])
        
        #tb = "ministerio_pruebas_controles"
        #cols = ("identificadorcontrol varchar2(20)", "testpcr int", "test_fecha date", "test_tipo int")
        #u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        #u.listToTable(proves_controls, tb, db)
        
        #users= ['DWSISAP_ROL','DWSPUBLICA']
        #for user in users:
        #    u.execute("grant select on {} to {}".format(tb,user),db) 
       
    def export_cips(self):
        """."""
        u.printTime("export")
        
        db = "exadata2"
        
        dx_file =  "pacients_inclosos.txt"
        inclosos = []
        for (id, cip13) in u.readCSV(dx_file, sep='@'):
            inclosos.append([id, cip13])
        
        tb = "ministerio_controles_2025"
        cols = ("identificador varchar2(20)", "cip varchar(40)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(inclosos, tb, db)
        
        users= ['DWSISAP_ROL','DWSPUBLICA']
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db) 
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    Nirse()

    u.printTime("Fi")