# -*- coding: latin1 -*-

"""
Projecte Nirse Ministeri: variables dels casos
"""

import collections as c
import psutil as p
import datetime as d
import difflib as s
import hashlib as h

import sisapUtils as u

class Nirse(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_idcip()
        self.get_hashos()
        self.get_nirse()
        self.get_nirse_ecap()
        self.get_poblacio()
        self.get_neixer()
        self.get_lactancia()
        self.get_dx()
        self.get_cmbdH()
        self.get_casos()
        self.get_cmbdH()
        self.export()
        
    def get_idcip(self):
        """id_cip to hash"""
        u.printTime("id_cip i hash")
        self.id_to_hash = {}
        self.id_sec_to_hash = {}
        sql = """select 
                    id_cip, id_cip_sec, hash_d, codi_sector 
                from 
                    u11"""
        for id, id_sec, hash, sec in u.getAll(sql, 'import'):
            self.id_to_hash[id] = hash
            self.id_sec_to_hash[id_sec] = hash
    
    def get_hashos(self):
        """Hashos"""
        u.printTime("hashos")
        
        self.red_to_exa = {}
        sql = """select hash_redics, hash_covid from dwsisap.PDPTB101_RELACIO"""
        for h_redics, h_covid in u.getAll(sql, 'exadata'):
            self.red_to_exa[h_redics] = h_covid
    
    def get_nirse(self):
        """."""
        u.printTime("Nirse")
        self.nirse = {}
        sql = """select cip, data, producte from dwsisap.vrs_vacuna where campanya='2023'"""
        for cip, data, producte in u.getAll(sql, 'exadata'):
            hash = h.sha1(cip).hexdigest().upper()
            self.nirse[hash] = {'data':data, 'producte':producte}
    
    def get_nirse_ecap(self):
        """."""
        u.printTime("Nirse ECAP")
        self.nirse_ecap = {}
        sql = """select  id_cip, va_u_data_vac, va_u_cod from vacunes where va_u_cod in  ('P00162', 'P00163') and va_u_data_vac >= '2023-10-01'"""
        for id, dat, producte in u.getAll(sql, 'import'):
            hash = self.id_to_hash[id] if id in self.id_to_hash else None
            hash_covid = self.red_to_exa[hash] if hash in self.red_to_exa else None
            self.nirse_ecap[hash_covid] = {'data':dat, 'producte':producte}
            
    def get_poblacio(self):
        """."""
        u.printTime("DBC Poblacio")
        self.poblacio = {}
        
        sql = """select hash, if(naixement <'2023-10-01', 1, 2), naixement, sexe, nacionalitat, abs, data_situacio, if(data_defuncio ='', 0, 1),data_defuncio, localitat from cohort"""
        for hash, cohort, naixement, sexe, nacionalitat, abs, data_situacio, defuncio, data_defuncio, localitat in u.getAll(sql, 'nirse_ministerio'):
            self.poblacio[hash] = {'cohort': cohort, 'naix': naixement, 'sexe': sexe, 'exitus': defuncio, 'exitus_data': data_defuncio, 'localitat': localitat}   
    
    def get_neixer(self):
        """."""
        u.printTime("dades naixements de ECAP")
        
        self.gestacio = {}
        sql = """select id_cip, val_data, val_var, val_val from import.nen11 where val_var in ('PEPNA06', 'PEPNA02')"""
        for id, data, var, val in u.getAll(sql, 'nodrizas'):
            hash_redics = self.id_to_hash[id] if id in self.id_to_hash else None
            hash = self.red_to_exa[hash_redics] if hash_redics in self.red_to_exa else None
            variable = None
            if var == 'PEPNA06':
                variable = 'Pes'
            elif var == 'PEPNA02':
                variable = 'SG'
            if hash in self.poblacio:
                self.gestacio[(hash, variable)] = {'data': data, 'val':val}
    
    def get_lactancia(self):
        """."""
        
        self.lactancia = {}
        sql = """select id_cip_sec, dat from ped_variables where agrupador in (764, 765)"""
        for id, dat in u.getAll(sql, 'nodrizas'):
            hash_redics = self.id_sec_to_hash[id] if id in self.id_sec_to_hash else None
            hash = self.red_to_exa[hash_redics] if hash_redics in self.red_to_exa else None
            if hash in self.poblacio:
                self.lactancia[hash] = dat
    
    def get_dx(self):
        """."""
        u.printTime("problemes")
        dx_file =  "codis_dx.txt"
        dx_codis = []
        tip_dx = {}
        self.diagnostics = {}
        for (grup, dx) in u.readCSV(dx_file, sep='@'):
            dx_codis.append(dx)
            tip_dx[dx] = grup
        in_crit = tuple(dx_codis)

        sql = "select id_cip, pr_dde,  pr_cod_ps, pr_dba \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_cod_ps in {}  and \
                            pr_data_baixa = 0".format(in_crit)
        for id, dde, codi, dba in u.getAll(sql, 'import'):
            hash_redics = self.id_to_hash[id] if id in self.id_to_hash else None
            hash = self.red_to_exa[hash_redics] if hash_redics in self.red_to_exa else None
            grup = tip_dx[codi]
            if hash in self.poblacio:
                if (hash, grup) in self.diagnostics:
                    data_ant = self.diagnostics[(hash, grup)]
                    if dde < data_ant:
                        self.diagnostics[(hash, grup)] = dde
                else:
                    self.diagnostics[(hash, grup)] = dde
    
    def get_cmbdH(self):
        """."""
        u.printTime("CMBDH")
        self.cmbdh = {}
        
        sql ="""SELECT hash, data_ingres, bronquiolitis from cmbdh"""
        for hash, data_ingres, bronquiolitis in u.getAll(sql, 'nirse_ministerio'):
            if bronquiolitis == 0:
                if hash in self.cmbdh:
                    data_ant = self.cmbdh[hash]
                    if data_ingres < data_ant:
                        self.cmbdh[hash] = data_ingres
                else:
                    self.cmbdh[hash] = data_ingres

    def get_casos(self):
        """."""
        u.printTime("Casos")
        self.casos = []
        sql = """select id, a.hash, a.data_ingres, a.data_vrs, virus, replace(replace(viruses,' ',''),'None',''), alta, uci,  provincia  from matching a inner join casos b on a.hash=b.hash and a.data_ingres=b.ingres where cas='Cas'"""
        for id, hash, ingres, data_vrs, virus, viruses, alta, uci, provincia2 in u.getAll(sql, 'nirse_ministerio'):
            naix = self.poblacio[hash]['naix']
            estudi = self.poblacio[hash]['cohort']
            nirse = 1 if hash in self.nirse else 0
            dat_nirse = self.nirse[hash]['data'] if hash in self.nirse else None
            if nirse == 0:
                if hash in self.nirse_ecap:
                    nirse = 1
                    dat_nirse = self.nirse_ecap[hash]['data'] 
            sex = self.poblacio[hash]['sexe']
            sexe = None
            if sex == 'Home':
                sexe = 'Masculino'
            elif sex == 'Dona':
                sexe = 'Femenino'
            sg = self.gestacio[(hash, 'SG')]['val'] if (hash, 'SG') in self.gestacio else None
            pes = self.gestacio[(hash, 'Pes')]['val'] if (hash, 'Pes') in self.gestacio else None
            displasia = 0
            displasia_dde = self.diagnostics[(hash, 'displasia')] if (hash, 'displasia') in self.diagnostics else ''
            if displasia_dde != '':
                if displasia_dde < ingres:
                    displasia = 1
                else:
                    displasia_dde = ''
            cardiopatia = 0
            cardiopatia_dde = self.diagnostics[(hash, 'cardiopatia')] if (hash, 'cardiopatia') in self.diagnostics else ''
            if cardiopatia_dde != '':
                if cardiopatia_dde < ingres:
                    cardiopatia = 1
                else:
                    cardiopatia_dde = ''
            tmetabolic = 0
            tmetabolic_dde = self.diagnostics[(hash, 'trastorno metabolico')] if (hash, 'trastorno metabolico') in self.diagnostics else ''
            if tmetabolic_dde != '':
                if tmetabolic_dde < ingres:
                    tmetabolic = 1
                else:
                    tmetabolic_dde = ''
            Down = 0
            Down_dde = self.diagnostics[(hash, 'Down')] if (hash, 'Down') in self.diagnostics else ''
            if Down_dde != '':
                if Down_dde < ingres:
                    Down = 1
                else:
                    Down_dde = ''
            fibrosisquistica = 0
            fibrosisquistica_dde = self.diagnostics[(hash, 'fibrosisquistica')] if (hash, 'fibrosisquistica') in self.diagnostics else ''
            if fibrosisquistica_dde != '':
                if fibrosisquistica_dde < ingres:
                    fibrosisquistica = 1
                else:
                    fibrosisquistica_dde = ''
            inmunodef = 0
            inmunodef_dde = self.diagnostics[(hash, 'inmunodef')] if (hash, 'inmunodef') in self.diagnostics else ''
            if inmunodef_dde != '':
                if inmunodef_dde < ingres:
                    inmunodef = 1
                else:
                    inmunodef_dde = ''
            lac_materna = 1 if hash in self.lactancia else 9
            lac_materna_dat = self.lactancia[hash] if hash in self.lactancia else ''
            ingres_data = self.cmbdh[hash] if hash in self.cmbdh else ''
            ingres_c = 0
            if ingres_data  != '':
                if ingres_data < ingres:
                    ingres_c = 1
            if ingres_c == 0:
                ingres_data = None
            data_defuncio = self.poblacio[hash]['exitus_data'] if hash in self.poblacio else None
            exitus = self.poblacio[hash]['exitus'] if hash in self.poblacio else 0
            if provincia2 == 'Girona':
                provincia = 17
            elif provincia2 == 'Lleida':
                provincia = 25
            elif provincia2 == 'Barcelona':
                provincia = 8
            elif provincia2 == 'Tarragona':
                provincia = 43
            else:
                provincia = 999
            self.casos.append([hash, id, 1, ingres, naix, estudi, provincia, nirse, dat_nirse, sexe, sg, pes, 9, 9, displasia, cardiopatia, tmetabolic, Down, fibrosisquistica, 9, inmunodef,
                                    displasia_dde, cardiopatia_dde, tmetabolic_dde, Down_dde, fibrosisquistica_dde, None, inmunodef_dde, 9, None, lac_materna, lac_materna_dat, ingres_c, ingres_data, None,
                                    data_vrs, virus, viruses, uci, 9, 9, exitus, data_defuncio, alta])

    
    
    def export(self):
        """."""
        u.printTime("export")
        
        db = "nirse_ministerio"
        
        tb = "dades_casos"
        cols = ("hash varchar(40)", "identificadorcaso int", "criterios int", "hospitalizacion date", "fdn date", "estudio int", "provincia int", "immunizacion int", "immunizacion_fecha date", 
        "sexe varchar(20)", "edad_gestacional int", "peso_nacimiento varchar(20)", "gestacionmultiple int", "primerhijo int", "displasia int", "cardiopatia int", "tmetabolico int", "sdown int", "fibrosisquistica int",
        "otras int", "inmunodef int", "displasia_fecha date", "cardiopatia_fecha date", "tmetabolico_fecha date", "sdown_fecha date", "fibrosisquistica_fecha date", "otras_fecha date", "inmunodef_fecha date",
        "paliativos int", "paliativos_fecha date", "lactancia int", "lactancia_fecha date", "hospitalizacionprevia int", "hospitalizacionprevia_fecha date", "hospitalizacionvrs_fecha date", "pcrvrs_fecha date",
        "dxotropatogeno int", "dxotropatogeno_descrip varchar(500)", "uci int", "ventilacionnoinvasiva int", "ventilacionnoinvasiva2 int", "fallecimiento int", "fallecimiento_fecha date", "altahospi_fecha date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.casos, tb, db)     
        
        
       
         
if __name__ == '__main__':
    u.printTime("Inici")
    
    Nirse()

    u.printTime("Fi")