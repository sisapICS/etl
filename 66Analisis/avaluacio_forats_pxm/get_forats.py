# -*- coding: utf8 -*-

import sisapUtils as u
import collections as c

from datetime import datetime, timedelta

DATA_INICI = '2021-01-01'  # Solo hay datos desde 202301
DATA_FI = '2023-12-31'
DESTI_TB = "ESTUDI_PXM"
DESTI_DB = 'exadata'

up_estudi = set([
    '00445', '00299', '06156', '00490', '00466', '00439', '04376', '00169',
    '00480', '00438', '07084', '00006', '00175', '00464', '00355', '08117',
    '00363', '00055', '00294', '00356', '00338', '00474', '00020', '00094',
    '07962', '00015', '00390', '00343', '00021', '00460', '00444', '00462',
    '00336', '00441', '00337', '00116', '00052', '00093', '00151', '00029',
    '07927', '00089', '00498', '00177', '00122', '03527', '06009', '00111',
    '00158', '00479', '04374', '00066', '00125', '00017', '04548', '01327',
    '04704', '04055', '00105', '00695', '00345', '00288', '00117', '00195',
    '00164', '01485', '00005', '00450', '00295', '01273', '00282', '14276',
    '00342', '00702', '00283', '00437', '01121', '00488', '00374', '00397',
    '01122', '04863', '00448', '00181', '00186', '00155', '00171', '00008',
    '00160', '00457',  # desde aqui faltaban en extracción longview
    '00088', '00087', '00201', '01928', '00040', '00042', '00060', '01097',
    '00044', '08118', '00202', '00014', '00018', '00004', '04547', '00385',
    '00382', '00362', '01919', '00103', '00114', '00121', '00277', '00477',
    '00478', '00353', '00372', '01077', '00461', '00465'    
    ])


class Main(object):
    """control de flujo de ejecución"""

    def __init__(self):
        """Metodo de inicialización"""
        # self.get_trams(data_inici=DATA_INICI, data_fi=DATA_FI)  # (9C,9D) NO-tiparé
        self.get_moduls()
        print('init OK')
        self.upload_desti(desti_tb=DESTI_TB, desti_db=DESTI_DB)

    def get_meses(self, data_inici, data_fi):
        """ torna la llista [int, int, ...] en format YYYYMM entre datas """
        start_date = datetime.strptime(data_inici, "%Y-%m-%d")
        end_date = datetime.strptime(data_fi, "%Y-%m-%d")

        # Generar la lista de meses en formato YYYYMM
        meses = []
        current_date = start_date

        while current_date <= end_date:
            meses.append(int(current_date.strftime("%Y%m")))
            # Avanzar al siguiente mes
            next_month = current_date.month + 1
            next_year = current_date.year + (next_month // 13)
            next_month = 1 if next_month == 13 else next_month
            current_date = datetime(next_year, next_month, 1)

        return(meses)

    def upload_desti(self, desti_tb, desti_db):
        """test de taula_desti"""
        prefijo = "FORATS_LLIURES_"
        recomptes = self.get_forats(DATA_INICI, DATA_FI)
        # meses = [int('2024' + str(mes).zfill(2)) for mes in range(1, 13)]
        # meses.append(202501)
        meses = self.get_meses(DATA_INICI, DATA_FI)
        for mes in meses:
            print(mes, recomptes.get(('MF', '00441', mes)))
        upload = [
            ("{ind}{servei}".format(ind=prefijo, servei=key[0]),
            key[1],
            str(key[2])) +
            tuple((
                recomptes[key]['lliures'],
                recomptes[key]['totals'],
                (recomptes[key]['lliures'] /
                float(recomptes[key]['totals'])) * 100
                )
            )
            for key in recomptes]
        u.listToTable(upload, desti_tb, desti_db)
        sql = """
            select * from {} where CODI = 'FORATS_LLIURES_MF' AND UP = '00441'
        """.format(desti_tb)
        for row in u.getAll(sql, desti_db):
            print(row)

    def get_trams(self, data_inici, data_fi):
        """trae los id y tipos de tramos"""
        trams = c.defaultdict(dict)
        sql = """
            SELECT
                capa_id_modul,
                capa_id_tram,
                to_date(capa_data, 'J') AS capa_data,
                capa_tipus_visita
            FROM
                vistb407
            WHERE
                capa_id_modul = 22717 AND
                to_date(capa_data, 'J') BETWEEN DATE '{dini}' AND DATE '{dfi}'
        """.format(dini=data_inici, dfi=data_fi)
        for sector in u.sectors:
            if sector == '6102':
                for id_modul, id_tram, data, tipus in u.getAll(sql, sector):
                    trams[sector][(id_modul, id_tram, data)] = tipus
        print(trams)

    def get_moduls(self):
        """trae los modulos y si son UBA"""
        sql = """
            SELECT
                codi_sector,
                MODU_ID_MODUL,
                modu_codi_up,
                MODU_CODI_UAB,
                -- modu_centre_codi_centre,
                -- modu_centre_classe_centre,
                case modu_servei_codi_servei when 'MG' then 'MF' else 'INF' end as servei
                -- modu_codi_modul,
                -- MODU_LLOC_CODI_LLOC
            FROM
                sisap_cat_vistb027
            where
                -- modu_codi_uab is not null AND
                modu_servei_codi_servei in (
                    'MG',                                    -- 'MF'
                    'INF', 'INFG', 'INFGR', 'INFMG', 'ENF'   -- 'INF'
                    )
                and ((modu_act_agenda = 1 AND MODU_SUBACT_AGENDA IN (1, 2))
                     OR modu_codi_uab is not null)
        """
        self.moduls = c.defaultdict(lambda: {"up": None, "uba": None, "servei": None})

        for sector, id_modul, up, uba, servei in u.getAll(sql, "exadata"):
            if up in up_estudi:
                self.moduls[(sector, id_modul)] = {"up": up,
                                                   "uba": uba,
                                                   "servei": servei}
        print("Moduls OK")

    def get_forats(self, data_inici, data_fi):
        """trae los huecos libres y totales de cada modulo, YYYYMM"""
        sql = """
            SELECT
                codi_sector,
                AG_ID_MODUL,
                -- AG_ID_TRAM,
                to_char(to_date(ag_data, 'J'), 'YYYYMM') * 1 AS ag_data,
                sum(decode(ag_cip,'LLIURE',1,0)) AS forats_lliures,
                count(*) AS forats_totals,
                avg(decode(ag_cip,'LLIURE',1,0)) AS percent
            FROM
                DWSISAP.vistb408
            WHERE
                to_date(ag_data, 'J') BETWEEN DATE '{dini}' AND DATE '{dfi}'
            GROUP BY
                codi_sector,
                ag_id_modul,
                -- ag_id_tram,
                to_char(to_date(ag_data, 'J'), 'YYYYMM') * 1
        """.format(dini=data_inici, dfi=data_fi)
        conteos = c.defaultdict(lambda: {"lliures": 0, "totals": 0, "moduls": 0})
        for sector, id_modul, data, lliures, totals, percent in u.getAll(sql, 'exadata'):
            dades = self.moduls.get((sector, id_modul))
            if not dades:
                continue  # salta si no existe en self.moduls
            up, servei, uba = dades["up"], dades["servei"], dades["uba"]
            conteos[(servei, up, data)].update({
                "lliures": conteos[(servei, up, data)]["lliures"] + lliures,
                "totals": conteos[(servei, up, data)]["totals"] + totals,
                "moduls": conteos[(servei, up, data)]["moduls"] + 1
            })
        print("forats OK")
        return(conteos)


if __name__ == '__main__':
    Main()
