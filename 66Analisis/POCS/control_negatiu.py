# -*- coding: utf8 -*-

import hashlib as h


"""
Buscar controls negatius pels pocs
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

codis_ps = {
    "Lumbalgia/dorsalgia":  {"C01-M54.42", "C01-M54.5", "M54","M54.9", "C01-M54.9","C01_M54.8","C01_M54.89","C01-M54"},
    }
    
agrupadors = {121: 'ulceres', 122: 'ulceres', 119: 'ulceres' , 141: 'ITU', 711: 'Fractura', 710: 'Fractura'}

class Controls_negatius(object):
    """."""

    def __init__(self):
        """."""
        self.get_codis()
        self.get_hashos_2()
        self.get_hash()
        self.get_cohort()
        self.get_problemes()
        self.export_files()
       
        
    def get_codis(self):
        """."""
        u.printTime("codis")
        
        self.codis = {}
        self.dicc_codis = {}        
        for ps, (codisN) in codis_ps.items():
            for codi in codisN:
                self.dicc_codis[codi] = ps
                self.codis[codi] = True
        sql = """select 
                    criteri_codi, agrupador 
                from
                    nodrizas.eqa_criteris 
                where 
                    agrupador in (121,122,119,141,711,710)"""
        for crit, agr in u.getAll(sql, 'nodrizas'):
            desc = agrupadors[agr]
            self.dicc_codis[crit] = desc
            self.codis[crit] = True
        
        self.codis_select = tuple(set([codi for codi, t in self.codis.items()]))
     
    def get_hashos_2(self):
        """Hashos covid i redics"""
        u.printTime("hashos redics i exadata")
        
        self.exa_to_red = {}
        sql = """select hash_redics, hash_covid from dwsisap.PDPTB101_RELACIO"""
        for h_redics, h_covid in u.getAll(sql, 'exadata'):
            self.exa_to_red[h_covid] = h_redics
    
    def get_hash(self):
        """id_cip_sec to hash"""
        u.printTime("hash")
        self.id_to_hash = {}
        
        sql = """select 
                    id_cip_sec, hash_d 
                from 
                    u11"""
        for id, hash in u.getAll(sql, 'import'):
            self.id_to_hash[id] = hash
    
    def get_cohort(self):
        """Agafemla cohort de pocs"""
        u.printTime("cohort")
        
        self.pob = {}
        sql = """select 
                    c_cip 
                FROM DWSISAP.DBS_POCS a
                        WHERE c_edat_anys>=75"""
        for cip, in u.getAll(sql,'exadata'):
            hash = self.exa_to_red[cip] if cip in self.exa_to_red else None
            self.pob[hash] = True    
     
    def get_problemes(self):
        """Agafem els ps de import"""
        u.printTime("problemes")
        self.upload = []       
        sql = """select 
                    id_cip_sec, pr_cod_ps, pr_dde 
                from 
                    problemes
                where 
                    year(pr_dde) ='2023' and pr_cod_o_ps='C' and pr_data_baixa=0 and pr_cod_ps in {}""".format(self.codis_select)
        for id,  ps, dde in u.getAll(sql, 'import'):
            hash = self.id_to_hash[id] if id in self.id_to_hash else None
            desc = self.dicc_codis[ps]
            if hash in self.pob:
                self.upload.append([hash, desc, dde])
            
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "pocs_controls_negatius"
        cols = ("hash varchar(40)","ps varchar(100)", "dde date")
        u.createTable(tb, "({})".format(", ".join(cols)), 'permanent', rm=True)
        u.listToTable(self.upload, tb, 'permanent')
    
if __name__ == "__main__":
    try:
        Controls_negatius()
    except Exception as e:
        print e
