# -*- coding: utf8 -*-

"""
Anàlisi de mortalitat per risc mensual, amb dades del dbs de pocs guardat
Falten els següents paràmetres al DBS del 2021 que es podrien incloure buscant-los a variables:

OR PS_FRAGIL_DATA IS NOT NULL -- fragilitat
OR V_FRAGIL_VIG_valor>0.2 --- fragilitat
OR VC_VIU_SOL_VALOR='Viu sol/a' -- viu sol
OR PS_INGBAI_DATA IS NOT NULL  -- p social

No excoc institucionalitzats tot i que en anàlisi hauríem d'excloure

"""
import hashlib as h

import collections as c

import sisapUtils as u


class pocs(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_hashos()
        self.get_exitus()
        self.get_dbs21()
        self.export()
        
    def get_hashos(self):
        """Hashos"""
        u.printTime("hashos")
        
        self.exa_to_red = {}
        sql = """select hash_redics, hash_covid from dwsisap.PDPTB101_RELACIO"""
        for h_redics, h_covid in u.getAll(sql, 'exadata'):
            self.exa_to_red[h_covid] = h_redics
            
    def get_exitus(self):
        """exitus"""
        u.printTime("exitus")
        self.exitus22 = {}
        
        sql = """SELECT substr(cip, 0, 13),data_exitus FROM dwsisap.DBX d WHERE data_exitus BETWEEN DATE '2023-01-01' AND DATE '2023-12-31'"""
        for cip2, dat in u.getAll(sql, 'exadata'):
            cip = h.sha1(cip2).hexdigest().upper()
            hash = self.exa_to_red[cip] if cip in self.exa_to_red else None
            if hash != None:
                self.exitus22[hash] = dat
    
    def get_dbs21(self):
        """Agafem gent a risc de POCS amb dbs 2021"""
        u.printTime("risc pocs")
        self.upload = []
        
        sql = """SELECT 
                    c_cip,
                    c_sector
                FROM 
                    SELECT * FROM dwsisap.DBS_POCS a
                WHERE 
                    c_edat_anys>=75) x"""
        for (cip, sector) in u.getAll(sql, 'exadata'):
            hash = self.exa_to_red[cip] if cip in self.exa_to_red else None
            exitus = self.exitus22[hash] if hash in self.exitus22 else None
            self.upload.append([hash, sector, exitus])
                        
    def export(self):
        """export"""
        u.printTime("export")
        
        db = "permanent"
        tb = "defuncions_2023"
        columns = ["hash varchar(40)", "sector varchar(5)", "exitus date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
if __name__ == '__main__' :
        u.printTime("Inici")
        
        pocs()

        u.printTime("Fi")
