***
***
***

# Flowchart {.tabset}

Diagrama amb els criteris d'inclusió i excluisió de l'estudi.

<!-- ## Intern -->

<!-- ```{r} -->
<!-- dt.cohort.dm <- readRDS("../data/dt.cohort.dm.rds") -->
<!-- ``` -->

<!-- ```{r} -->
<!-- dt.temp <- dt.cohort.dm -->
<!-- n1.reg <- nrow(dt.cohort.dm) -->
<!-- n1.pax <- uniqueN(dt.cohort.dm[, hash]) -->

<!-- dt.temp <- dt.cohort.dm[cohort == 'Cohort 4 anys',] -->
<!-- n2a.reg <- nrow(dt.temp) -->
<!-- n2a.pax <- uniqueN(dt.temp[, hash]) -->

<!-- dt.temp <- dt.cohort.dm[cohort == 'Cohort 5-10 anys',] -->
<!-- n2b.reg <- nrow(dt.temp) -->
<!-- n2b.pax <- uniqueN(dt.temp[, hash]) -->

<!-- dt.temp <- dt.cohort.dm[cohort == 'Cohort 4 anys' & enviar == 'No',] -->
<!-- n3a.reg <- nrow(dt.temp) -->
<!-- n3a.pax <- uniqueN(dt.temp[, hash]) -->

<!-- dt.temp <- dt.cohort.dm[cohort == 'Cohort 4 anys' & enviar == 'Sí',] -->
<!-- n3b.reg <- nrow(dt.temp) -->
<!-- n3b.pax <- uniqueN(dt.temp[, hash]) -->

<!-- dt.temp <- dt.cohort.dm[cohort == 'Cohort 5-10 anys' & enviar == 'No',] -->
<!-- n3c.reg <- nrow(dt.temp) -->
<!-- n3c.pax <- uniqueN(dt.temp[, hash]) -->

<!-- dt.temp <- dt.cohort.dm[cohort == 'Cohort 5-10 anys' & enviar == 'Sí',] -->
<!-- n3d.reg <- nrow(dt.temp) -->
<!-- n3d.pax <- uniqueN(dt.temp[, hash]) -->

<!-- dt.temp <- dt.cohort.dm[cohort == 'Cohort 4 anys' & enviar == 'Sí' & missatge_c == 'Missatge 1',] -->
<!-- n4a.reg <- nrow(dt.temp) -->
<!-- n4a.pax <- uniqueN(dt.temp[, hash]) -->

<!-- dt.temp <- dt.cohort.dm[cohort == 'Cohort 4 anys' & enviar == 'Sí' & missatge_c == 'Missatge 2',] -->
<!-- n4b.reg <- nrow(dt.temp) -->
<!-- n4b.pax <- uniqueN(dt.temp[, hash]) -->

<!-- # dt.temp <- dt.cohort.dm[cohort == 'Cohort 4 anys' & enviar == 'Sí' & missatge_c == 'Missatge 1',] -->
<!-- # n4a.reg <- nrow(dt.temp) -->
<!-- # n4a.pax <- uniqueN(dt.temp[, hash]) -->
<!-- #  -->
<!-- # dt.temp <- dt.cohort.dm[cohort == 'Cohort 4 anys' & enviar == 'Sí' & missatge_c == 'Missatge 2',] -->
<!-- # n4b.reg <- nrow(dt.temp) -->
<!-- # n4b.pax <- uniqueN(dt.temp[, hash]) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- fin <- c(seq(1,20,1)) -->

<!-- grViz(" -->
<!--       digraph a_nice_graph -->
<!--       { -->

<!--       node[fontname = Helvetica, -->
<!--            fontcolor = black, -->
<!--            shape = box, -->
<!--            width = 1, -->
<!--            style = filled, -->
<!--            fillcolor = whitesmoke] -->

<!--       '@@1' -> '@@2' -->
<!--       '@@1' -> '@@3' -->
<!--       '@@2' -> '@@4' -->
<!--       '@@2' -> '@@5' -->
<!--       '@@3' -> '@@6' -->
<!--       '@@3' -> '@@7' -->
<!--       '@@5' -> '@@8' -->
<!--       '@@5' -> '@@9' -->
<!--       } -->

<!--       [1]: paste0('COHORTs 4-10 anys', '\\n', 'Registres N=', n1.reg, '\\n', 'Pacients N=', n1.pax) -->
<!--       [2]: paste0('4 Anys', '\\n', 'Registres N=', n2a.reg, '\\n', 'Pacients N=', n2a.pax) -->
<!--       [3]: paste0('5-10 anys', '\\n', 'Registres N=', n2b.reg, '\\n', 'Pacients N=', n2b.pax) -->
<!--       [4]: paste0('4 anys SMS NO Enviat', '\\n', 'Registres N=', n3a.reg, '\\n', 'Pacients N=', n3a.pax) -->
<!--       [5]: paste0('4 anys SMS Enviat', '\\n', 'Registres N=', n3b.reg, '\\n', 'Pacients N=', n3b.pax) -->
<!--       [6]: paste0('5-10 anys SMS NO Enviat', '\\n', 'Registres N=', n3c.reg, '\\n', 'Pacients N=', n3c.pax) -->
<!--       [7]: paste0('5-10 anys SMS Enviat', '\\n', 'Registres N=', n3d.reg, '\\n', 'Pacients N=', n3d.pax) -->
<!--       [8]: paste0('Missatge A', '\\n', 'Registres N=', n4a.reg, '\\n', 'Pacients N=', n4a.pax) -->
<!--       [9]: paste0('Missatge B', '\\n', 'Registres N=', n4b.reg, '\\n', 'Pacients N=', n4b.pax) -->
<!--       ", height = 700, width = 900) -->
<!-- ``` -->

```{r filtre}

#filtre = 
dt.fin.flt <- dt.fin.dm

saveRDS(dt.fin.flt, "../data/dt.fin.flt.rds")
```

```{r}
#rm(list = c(""))
gc.var <- gc()
```

