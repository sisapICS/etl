# -*- coding: utf8 -*-

"""
POCS per 2023, fem amb dbs23
Falten els següents paràmetres al DBS del 2021 que es podrien incloure buscant-los a variables:

OR PS_FRAGIL_DATA IS NOT NULL -- fragilitat
OR V_FRAGIL_VIG_valor>0.2 --- fragilitat
OR VC_VIU_SOL_VALOR='Viu sol/a' -- viu sol
OR PS_INGBAI_DATA IS NOT NULL  -- p social

No excoc institucionalitzats tot i que en anàlisi hauríem d'excloure

"""
import hashlib as h

import collections as c

import sisapUtils as u


class pocs(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_hashos()
        self.get_nia()
        self.get_exitus()
        self.get_pocs23()
        self.get_check()
        self.get_dbs()
        self.get_urgencies()
        self.export()
        
    def get_hashos(self):
        """Hashos"""
        u.printTime("hashos")
        
        self.exa_to_red = {}
        sql = """select hash_redics, hash_covid from dwsisap.PDPTB101_RELACIO"""
        for h_redics, h_covid in u.getAll(sql, 'exadata'):
            self.exa_to_red[h_covid] = h_redics
            
    def get_nia(self):
        """Nia"""
        u.printTime("Nia")
        self.nia_to_cip = {}
        sql = """select cip, nia
                   from dwsisap.rca_cip_nia"""
        for cip, nia in u.getAll(sql, 'exadata'):
            self.nia_to_cip[nia] = cip[:13]

    
                    
    def get_exitus(self):
        """exitus"""
        u.printTime("exitus")
        self.dbx = {}
        self.exitus22 = {}
        
        sql = """SELECT  substr(cip, 0, 13),data_exitus FROM dwsisap.DBX d WHERE data_exitus BETWEEN DATE '2023-01-01' AND DATE '2023-04-30'"""
        for cip2, dat in u.getAll(sql, 'exadata'):
            cip = h.sha1(cip2).hexdigest().upper()
            hash = self.exa_to_red[cip] if cip in self.exa_to_red else None
            if hash != None:
                self.dbx[hash] = True
        
        sql = """SELECT substr(cip, 0, 13),data_exitus FROM dwsisap.DBX d WHERE data_exitus BETWEEN DATE '2023-05-01' AND DATE '2023-09-30'"""
        for cip2, dat in u.getAll(sql, 'exadata'):
            cip = h.sha1(cip2).hexdigest().upper()
            hash = self.exa_to_red[cip] if cip in self.exa_to_red else None
            if hash != None:
                self.exitus22[hash] = dat
                
    def get_pocs23(self):
        """Centres"""
        u.printTime("pocs 2023")
        self.poc_act = {}

        sql = """SELECT au_cod_u, AU_DAT_ACT FROM PRSTB016 WHERE au_cod_AC='POCS' AND au_dat_act BETWEEN DATE '2023-05-01' AND DATE '2023-09-30'"""
        for id, dat in u.getAll(sql, 'redics'):
            self.poc_act[id] = dat
        
    def get_check(self):
        """Centres"""
        u.printTime("check_sisap")
        self.check = {}

        sql = """select cip, revisat from POCSINC2023 where revisat is not null"""
        for id, dat in u.getAll(sql, 'pdp'):
            self.check[id] = dat
    
    def get_dbs(self):
        """Agafem gent a risc de POCS """
        u.printTime("risc pocs")
        self.upload = []
        self.poblacio_risc = {}
        sql = """SELECT x.*,
                        CASE WHEN fragilitat=1 THEN '3.molt alt' WHEN patologia=1 AND fragilitat=0 THEN '2.alt' WHEN patologia=0 AND fragilitat=0 THEN '1.baix' END risc2 from
                        (SELECT
                        c_cip,
                        c_sector,
                        c_up,
                        c_metge,
                        a.C_INFERMERA,
                        c_edat_anys edat,
                        c_sexe,
                        c_institucionalitzat,
                        CASE WHEN PS_DEPENDENCIA_DATA IS NOT NULL
                        OR V_BARTHEL_VALOR<=60
                        OR PR_MACA_DATA IS NOT NULL
                        OR PR_PCC_DATA IS NOT NULL
                        OR PS_ATDOM_DATA IS NOT NULL
                        OR PS_GENT_GRAN_VULNERABLE_DATA IS NOT NULL
                        OR PS_FRAGIL_DATA IS NOT NULL
                        OR VC_DEPENDENCIA_VALOR IN ('Gran dependència','Dependència severa','Dependència severa nivell 2','Gran dependència nivell 2','Gran dependència nivell 1','Dependència severa nivell 1')
                        OR VC_GERONTOP_VALOR IN ('Fràgil i accepta intervenció','Fràgil i no accepta intervenció','Sí fràgil (Aquest ítem no aplica a partir del 30/03/2023')
                        OR V_FRAGIL_VIG_valor>0.2
                        OR PC_FRGL_M_A_DATA IS NOT NULL OR PC_FRGL_PG_DATA IS NOT NULL THEN 1 ELSE 0 end fragilitat,
                        CASE WHEN PS_DEMENCIA_DATA IS NOT NULL OR
                        PS_MPOC_ENFISEMA_DATA IS NOT NULL OR
                        PS_CARDIOPATIA_ISQUEMICA_DATA IS NOT NULL OR
                        PS_INSUF_CARDIACA_DATA IS NOT NULL OR
                        PS_RENAL_CRO_DATA IS NOT NULL OR
                        PS_M_NEURO_DATA is NOT NULL THEN 1 ELSE 0 END patologia,
                        CASE WHEN PS_VIU_SOL_DATA IS NOT NULL
                        OR VC_VIU_SOL_VALOR='Viu sol/a' THEN 1 ELSE 0 END viu_sol,
                        CASE WHEN a.PS_RESIDENCIA_HABITATGE_DATA IS NOT NULL
                        OR PS_INGBAI_DATA IS NOT NULL THEN 1 ELSE 0 END p_social,
                        PS_DEPENDENCIA_DATA , v_barthel_valor, pr_maca_data, pr_pcc_data, ps_atdom_data, ps_gent_gran_vulnerable_data, ps_fragil_data, vc_dependencia_valor,
                        vc_gerontop_valor, v_fragil_vig_valor, PC_FRGL_M_A_DATA, PC_FRGL_PG_DATA, PS_DEMENCIA_DATA, ps_mpoc_enfisema_data, ps_cardiopatia_isquemica_data, ps_insuf_cardiaca_data,
                        ps_renal_cro_data, ps_m_neuro_data, ps_viu_sol_data, vc_viu_sol_valor, PS_RESIDENCIA_HABITATGE_DATA, PS_INGBAI_DATA
                FROM DWSISAP.DBS_POCS a
                        WHERE c_edat_anys>=75) x"""
        for (cip, sector, up, metge, inf,  edat, sexe, resi, fragilitat, patologia, viu_sol, p_social, PS_DEPENDENCIA_DATA,v_barthel_valor, pr_maca_data, pr_pcc_data, ps_atdom_data, ps_gent_gran_vulnerable_data, ps_fragil_data, vc_dependencia_valor, vc_gerontop_valor,
                V_FRAGIL_VIG_valor, PC_FRGL_M_A_DATA, PC_FRGL_PG_DATA,PS_DEMENCIA_DATA,ps_mpoc_enfisema_data,ps_cardiopatia_isquemica_data,ps_insuf_cardiaca_data,ps_renal_cro_data,ps_m_neuro_data,ps_viu_sol_data,vc_viu_sol_valor, PS_RESIDENCIA_HABITATGE_DATA,
                PS_INGBAI_DATA,  risc) in u.getAll(sql, 'exadata'):
            hash = self.exa_to_red[cip] if cip in self.exa_to_red else None
            if hash not in self.dbx:
                self.poblacio_risc[hash] = True
                exitus = self.exitus22[hash] if hash in self.exitus22 else None
                pocs_fet = self.poc_act[hash] if hash in self.poc_act else None
                check = self.check[hash] if hash in self.check else None    
                self.upload.append([hash, sector, up, metge, inf, edat, sexe, resi, fragilitat, patologia, risc, viu_sol, p_social, PS_DEPENDENCIA_DATA,v_barthel_valor, pr_maca_data, pr_pcc_data, ps_atdom_data, ps_gent_gran_vulnerable_data, ps_fragil_data,vc_dependencia_valor, vc_gerontop_valor,
                PC_FRGL_M_A_DATA, PC_FRGL_PG_DATA,PS_DEMENCIA_DATA,ps_mpoc_enfisema_data,ps_cardiopatia_isquemica_data,ps_insuf_cardiaca_data,ps_renal_cro_data,ps_m_neuro_data,ps_viu_sol_data,vc_viu_sol_valor, PS_RESIDENCIA_HABITATGE_DATA, 
                PS_INGBAI_DATA, V_FRAGIL_VIG_valor, pocs_fet, check, exitus])
                         
    def get_urgencies(self):
        """urgencies"""
        u.printTime("urgencies")
        self.urgencies = []
        
        sql = """select a.c_nia, a.c_data_alta_f, C_S_ALTA, CASE WHEN C_S_ALTA IN (21,22,23,24) THEN 1 ELSE 0 END AS ingres
               from dwcatsalut.cs_urgencies a where c_nia < 99999999
               AND c_data_alta_f BETWEEN DATE '2023-05-01' AND DATE '2023-09-30'"""
        for nia, data, motiu_a, ingres in u.getAll(sql, 'exadata'):
            cip2 = self.nia_to_cip[nia] if nia in self.nia_to_cip else ''
            cip = h.sha1(cip2).hexdigest().upper()
            hash = self.exa_to_red[cip] if cip in self.exa_to_red else None 
            if hash != None:
                if hash in self.poblacio_risc:
                    self.urgencies.append([hash, data, ingres])
        
        db = "permanent"
        tb = "urgencies_23"
        columns = ["hash varchar(40)","data date", "ingres int"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.urgencies, tb, db)
        
    def export(self):
        """export"""
        u.printTime("export")
        
        db = "permanent"
        tb = "pocs_2023"
        columns = ["hash varchar(40)", "sector varchar(5)", "up varchar(5)", "metge varchar(5)", "inf varchar(5)", "edat int", "sexe varchar(10)", "resi varchar(50)", "fragilitat int", "patologia int", "risc varchar(50)", "viu_sol int", "p_social int", 
               "PS_DEPENDENCIA_DATA date","v_barthel_valor double", "pr_maca_data date", "pr_pcc_data date", "ps_atdom_data date", "ps_gent_gran_vulnerable_data date", "ps_fragil_data date", "vc_dependencia_valor varchar(100)", "vc_gerontop_valor varchar(100)",
                "PC_FRGL_M_A_DATA date", "PC_FRGL_PG_DATA date", "PS_DEMENCIA_DATA date", "ps_mpoc_enfisema_data date", "ps_cardiopatia_isquemica_data date", "ps_insuf_cardiaca_data date", "ps_renal_cro_data date", "ps_m_neuro_data date",
                "ps_viu_sol_data date", "vc_viu_sol_valor varchar(100)", "PS_RESIDENCIA_HABITATGE_DATA date", 
                "PS_INGBAI_DATA date", "v_fragil_vig_valor double", "pocs_fet date", "check_sisap date","exitus date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
if __name__ == '__main__' :
        u.printTime("Inici")
        
        pocs()

        u.printTime("Fi")
