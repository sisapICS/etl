# -*- coding: utf8 -*-

"""
POCS per 2022, fem amb dbs2021
Falten els següents paràmetres al DBS del 2021 que es podrien incloure buscant-los a variables:

OR PS_FRAGIL_DATA IS NOT NULL -- fragilitat
OR V_FRAGIL_VIG_valor>0.2 --- fragilitat
OR VC_VIU_SOL_VALOR='Viu sol/a' -- viu sol
OR PS_INGBAI_DATA IS NOT NULL  -- p social

No excoc institucionalitzats tot i que en anàlisi hauríem d'excloure

"""
import hashlib as h

import collections as c

import sisapUtils as u


class pocs(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_hashos()
        self.get_nia()
        self.get_urgencies()
        self.get_exitus()
        self.get_pocs22()
        self.get_dbs21()
        self.export()
        
    def get_hashos(self):
        """Hashos"""
        u.printTime("hashos")
        
        self.exa_to_red = {}
        sql = """select hash_redics, hash_covid from dwsisap.PDPTB101_RELACIO"""
        for h_redics, h_covid in u.getAll(sql, 'exadata'):
            self.exa_to_red[h_covid] = h_redics
            
    def get_nia(self):
        """Nia"""
        u.printTime("Nia")
        self.nia_to_cip = {}
        sql = """select cip, nia
                   from dwsisap.rca_cip_nia"""
        for cip, nia in u.getAll(sql, 'exadata'):
            self.nia_to_cip[nia] = cip[:13]

    def get_urgencies(self):
        """urgencies"""
        u.printTime("urgencies")
        self.urgencies = c.defaultdict(set)
        
        sql = """select a.c_nia, a.c_data_alta_f
               from dwcatsalut.cs_urgencies a where c_nia < 99999999
               AND c_data_alta_f BETWEEN DATE '2022-05-01' AND DATE '2022-09-30'"""
        for nia, data in u.getAll(sql, 'exadata'):
            cip2 = self.nia_to_cip[nia] if nia in self.nia_to_cip else ''
            cip = h.sha1(cip2).hexdigest().upper()
            hash = self.exa_to_red[cip] if cip in self.exa_to_red else None 
            if hash != None:
                self.urgencies[hash].add(data)
                    
    def get_exitus(self):
        """exitus"""
        u.printTime("exitus")
        self.dbx = {}
        self.exitus22 = {}
        
        sql = """SELECT  substr(cip, 0, 13),data_exitus FROM dwsisap.DBX d WHERE data_exitus BETWEEN DATE '2022-01-01' AND DATE '2022-04-30'"""
        for cip2, dat in u.getAll(sql, 'exadata'):
            cip = h.sha1(cip2).hexdigest().upper()
            hash = self.exa_to_red[cip] if cip in self.exa_to_red else None
            if hash != None:
                self.dbx[hash] = True
        
        sql = """SELECT substr(cip, 0, 13),data_exitus FROM dwsisap.DBX d WHERE data_exitus BETWEEN DATE '2022-05-01' AND DATE '2022-09-30'"""
        for cip2, dat in u.getAll(sql, 'exadata'):
            cip = h.sha1(cip2).hexdigest().upper()
            hash = self.exa_to_red[cip] if cip in self.exa_to_red else None
            if hash != None:
                self.exitus22[hash] = dat
                
    def get_pocs22(self):
        """Centres"""
        u.printTime("pocs 2022")
        self.poc_act = {}

        sql = """SELECT au_cod_u, AU_DAT_ACT FROM PRSTB016 WHERE au_cod_AC='POCS' AND au_dat_act BETWEEN DATE '2022-05-01' AND DATE '2022-09-30'"""
        for id, dat in u.getAll(sql, 'redics'):
            self.poc_act[id] = dat
    
    def get_dbs21(self):
        """Agafem gent a risc de POCS amb dbs 2021"""
        u.printTime("risc pocs")
        self.upload = []
        
        SIDICS_DB = ("dbs", "x0002")
        sql = """SELECT x.*,
                    if(fragilitat=1,'3.molt alt', if(patologia=1 AND fragilitat=0, '2.alt',if(patologia=0 AND fragilitat=0, '1.baix', 'err'))) as risc2 from
                    (SELECT
                    c_cip,
                    c_sector,
                    c_up,
                    c_metge,
                    a.C_INFERMERA,
                    c_edat_anys edat,
                    c_sexe,
                    c_institucionalitzat,
                    if(PS_DEPENDENCIA_DATA <>0
                    OR V_BARTHEL_VALOR between 0.1 and 60
                    OR PR_MACA_DATA <>0
                    OR PR_PCC_DATA <>0
                    OR PS_ATDOM_DATA <>0
                    OR PS_GENT_GRAN_VULNERABLE_DATA <>0
                    OR VC_DEPENDENCIA_VALOR IN ('Gran dependència','Dependència severa','Dependència severa nivell 2','Gran dependència nivell 2','Gran dependència nivell 1','Dependència severa nivell 1')
                    OR VC_GERONTOP_VALOR IN ('Fràgil i accepta intervenció','Fràgil i no accepta intervenció','Sí fràgil')
                    OR PC_FRGL_M_A_DATA <>0 OR PC_FRGL_PG_DATA <>0, 1,0) fragilitat,
                    if( PS_DEMENCIA_DATA <>0 OR
                    PS_MPOC_ENFISEMA_DATA <>0 OR
                    PS_CARDIOPATIA_ISQUEMICA_DATA <>0 OR
                    PS_INSUF_CARDIACA_DATA <>0 OR
                    PS_RENAL_CRO_DATA <>0 OR
                    PS_M_NEURO_DATA <>0,1, 0) patologia,
                    if(PS_VIU_SOL_DATA <>0,1,0) viu_sol,
                    if(a.PS_RESIDENCIA_HABITATGE_DATA <>0,1,0) p_social,
                    PS_DEPENDENCIA_DATA , v_barthel_valor, pr_maca_data, pr_pcc_data, ps_atdom_data, ps_gent_gran_vulnerable_data,  vc_dependencia_valor,
                    vc_gerontop_valor,  PC_FRGL_M_A_DATA, PC_FRGL_PG_DATA, PS_DEMENCIA_DATA, ps_mpoc_enfisema_data, ps_cardiopatia_isquemica_data, ps_insuf_cardiaca_data,
                    ps_renal_cro_data, ps_m_neuro_data, ps_viu_sol_data, PS_RESIDENCIA_HABITATGE_DATA
                FROM 
                    dbs_2021 a
                WHERE 
                    c_edat_anys>=75) x"""
        for (cip, sector, up, metge, inf,  edat, sexe, resi, fragilitat, patologia, viu_sol, p_social, PS_DEPENDENCIA_DATA,v_barthel_valor, pr_maca_data, pr_pcc_data, ps_atdom_data, ps_gent_gran_vulnerable_data, vc_dependencia_valor, vc_gerontop_valor,
                PC_FRGL_M_A_DATA, PC_FRGL_PG_DATA,PS_DEMENCIA_DATA,ps_mpoc_enfisema_data,ps_cardiopatia_isquemica_data,ps_insuf_cardiaca_data,ps_renal_cro_data,ps_m_neuro_data,ps_viu_sol_data,PS_RESIDENCIA_HABITATGE_DATA,risc) in u.getAll(sql, SIDICS_DB):
            if cip not in self.dbx:
                exitus = self.exitus22[cip] if cip in self.exitus22 else None
                pocs_fet = self.poc_act[cip] if cip in self.poc_act else None
                n_urg = 0
                d_urg = None
                if cip in self.urgencies:
                    for dat in self.urgencies[cip]:
                        n_urg += 1
                        if pocs_fet != None:
                            if dat >= pocs_fet:
                                if d_urg == None or dat < d_urg:
                                    d_urg = dat
                        else:
                            if d_urg == None or dat < d_urg:
                                d_urg = dat        
                self.upload.append([cip, sector, up, metge, inf, edat, sexe, resi, fragilitat, patologia, risc, viu_sol, p_social, PS_DEPENDENCIA_DATA,v_barthel_valor, pr_maca_data, pr_pcc_data, ps_atdom_data, ps_gent_gran_vulnerable_data, vc_dependencia_valor, vc_gerontop_valor,
                PC_FRGL_M_A_DATA, PC_FRGL_PG_DATA,PS_DEMENCIA_DATA,ps_mpoc_enfisema_data,ps_cardiopatia_isquemica_data,ps_insuf_cardiaca_data,ps_renal_cro_data,ps_m_neuro_data,ps_viu_sol_data,PS_RESIDENCIA_HABITATGE_DATA, pocs_fet, exitus, d_urg, n_urg])
                        
    def export(self):
        """export"""
        u.printTime("export")
        
        db = "permanent"
        tb = "pocs_2022"
        columns = ["hash varchar(40)", "sector varchar(5)", "up varchar(5)", "metge varchar(5)", "inf varchar(5)", "edat int", "sexe varchar(10)", "resi varchar(50)", "fragilitat int", "patologia int", "risc varchar(50)", "viu_sol int", "p_social int", 
               "PS_DEPENDENCIA_DATA date","v_barthel_valor double", "pr_maca_data date", "pr_pcc_data date", "ps_atdom_data date", "ps_gent_gran_vulnerable_data date", "vc_dependencia_valor varchar(100)", "vc_gerontop_valor varchar(100)",
                "PC_FRGL_M_A_DATA date", "PC_FRGL_PG_DATA date", "PS_DEMENCIA_DATA date", "ps_mpoc_enfisema_data date", "ps_cardiopatia_isquemica_data date", "ps_insuf_cardiaca_data date", "ps_renal_cro_data date", "ps_m_neuro_data date",
                "ps_viu_sol_data date", "PS_RESIDENCIA_HABITATGE_DATA date", "pocs_fet date", "exitus date", "urgencies_hx date", "n_urgencies int"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
if __name__ == '__main__' :
        u.printTime("Inici")
        
        pocs()

        u.printTime("Fi")
