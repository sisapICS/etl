# -*- coding: utf8 -*-

"""
Anàlisi de mortalitat per risc mensual, amb dades del dbs2021
Falten els següents paràmetres al DBS del 2021 que es podrien incloure buscant-los a variables:

OR PS_FRAGIL_DATA IS NOT NULL -- fragilitat
OR V_FRAGIL_VIG_valor>0.2 --- fragilitat
OR VC_VIU_SOL_VALOR='Viu sol/a' -- viu sol
OR PS_INGBAI_DATA IS NOT NULL  -- p social

No excoc institucionalitzats tot i que en anàlisi hauríem d'excloure

"""
import hashlib as h

import collections as c

import sisapUtils as u


class pocs(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_hashos()
        self.get_exitus()
        self.get_dbs21()
        self.export()
        
    def get_hashos(self):
        """Hashos"""
        u.printTime("hashos")
        
        self.exa_to_red = {}
        sql = """select hash_redics, hash_covid from dwsisap.PDPTB101_RELACIO"""
        for h_redics, h_covid in u.getAll(sql, 'exadata'):
            self.exa_to_red[h_covid] = h_redics
            
    def get_exitus(self):
        """exitus"""
        u.printTime("exitus")
        self.exitus22 = {}
        
        sql = """SELECT substr(cip, 0, 13),data_exitus FROM dwsisap.DBX d WHERE data_exitus BETWEEN DATE '2022-01-01' AND DATE '2022-12-31'"""
        for cip2, dat in u.getAll(sql, 'exadata'):
            cip = h.sha1(cip2).hexdigest().upper()
            hash = self.exa_to_red[cip] if cip in self.exa_to_red else None
            if hash != None:
                self.exitus22[hash] = dat
    
    def get_dbs21(self):
        """Agafem gent a risc de POCS amb dbs 2021"""
        u.printTime("risc pocs")
        self.upload = []
        
        SIDICS_DB = ("dbs", "x0002")
        sql = """SELECT x.*,
                    if(fragilitat=1,'3.molt alt', if(patologia=1 AND fragilitat=0, '2.alt',if(patologia=0 AND fragilitat=0, '1.baix', 'err'))) as risc2 from
                    (SELECT
                    c_cip,
                    c_sector,
                    c_up,
                    c_metge,
                    a.C_INFERMERA,
                    c_edat_anys edat,
                    c_sexe,
                    c_institucionalitzat,
                    if(PS_DEPENDENCIA_DATA <>0
                    OR V_BARTHEL_VALOR between 0.1 and 60
                    OR PR_MACA_DATA <>0
                    OR PR_PCC_DATA <>0
                    OR PS_ATDOM_DATA <>0
                    OR PS_GENT_GRAN_VULNERABLE_DATA <>0
                    OR VC_DEPENDENCIA_VALOR IN ('Gran dependència','Dependència severa','Dependència severa nivell 2','Gran dependència nivell 2','Gran dependència nivell 1','Dependència severa nivell 1')
                    OR VC_GERONTOP_VALOR IN ('Fràgil i accepta intervenció','Fràgil i no accepta intervenció','Sí fràgil')
                    OR PC_FRGL_M_A_DATA <>0 OR PC_FRGL_PG_DATA <>0, 1,0) fragilitat,
                    if( PS_DEMENCIA_DATA <>0 OR
                    PS_MPOC_ENFISEMA_DATA <>0 OR
                    PS_CARDIOPATIA_ISQUEMICA_DATA <>0 OR
                    PS_INSUF_CARDIACA_DATA <>0 OR
                    PS_RENAL_CRO_DATA <>0 OR
                    PS_M_NEURO_DATA <>0,1, 0) patologia,
                    if(PS_VIU_SOL_DATA <>0,1,0) viu_sol,
                    if(a.PS_RESIDENCIA_HABITATGE_DATA <>0,1,0) p_social,
                    PS_DEPENDENCIA_DATA , v_barthel_valor, pr_maca_data, pr_pcc_data, ps_atdom_data, ps_gent_gran_vulnerable_data,  vc_dependencia_valor,
                    vc_gerontop_valor,  PC_FRGL_M_A_DATA, PC_FRGL_PG_DATA, PS_DEMENCIA_DATA, ps_mpoc_enfisema_data, ps_cardiopatia_isquemica_data, ps_insuf_cardiaca_data,
                    ps_renal_cro_data, ps_m_neuro_data, ps_viu_sol_data, PS_RESIDENCIA_HABITATGE_DATA
                FROM 
                    dbs_2021 a
                WHERE 
                    c_edat_anys>=75) x"""
        for (cip, sector, up, metge, inf,  edat, sexe, resi, fragilitat, patologia, viu_sol, p_social, PS_DEPENDENCIA_DATA,v_barthel_valor, pr_maca_data, pr_pcc_data, ps_atdom_data, ps_gent_gran_vulnerable_data, vc_dependencia_valor, vc_gerontop_valor,
                PC_FRGL_M_A_DATA, PC_FRGL_PG_DATA,PS_DEMENCIA_DATA,ps_mpoc_enfisema_data,ps_cardiopatia_isquemica_data,ps_insuf_cardiaca_data,ps_renal_cro_data,ps_m_neuro_data,ps_viu_sol_data,PS_RESIDENCIA_HABITATGE_DATA,risc) in u.getAll(sql, SIDICS_DB):
            exitus = self.exitus22[cip] if cip in self.exitus22 else None
            self.upload.append([cip, sector, edat, sexe, resi,  risc,  exitus])
                        
    def export(self):
        """export"""
        u.printTime("export")
        
        db = "permanent"
        tb = "defuncions_2022"
        columns = ["hash varchar(40)", "sector varchar(5)", "edat int", "sexe varchar(10)", "resi varchar(50)", "risc int", "exitus date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
if __name__ == '__main__' :
        u.printTime("Inici")
        
        pocs()

        u.printTime("Fi")
