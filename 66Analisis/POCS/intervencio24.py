# -*- coding: utf8 -*-

"""
COBERTURA POCS
"""
import hashlib as h

import collections as c

import sisapUtils as u


class pocs(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_pocs()
        self.pob_risc()
        
    def get_pocs(self):
        """Centres"""
        u.printTime("pocs 2024")
        self.poc_act = {}

        sql = """SELECT au_cod_u, AU_DAT_ACT FROM PRSTB016 WHERE au_cod_AC='POCS' AND au_dat_act BETWEEN DATE '2024-05-01' AND DATE '2024-09-30'"""
        for id, dat in u.getAll(sql, 'redics'):
            self.poc_act[id] = True
            
    def pob_risc(self):
        """Centres"""
        u.printTime("Poblacio a risc 2023")
        recomptes = c.Counter()
        
        sql = "select c_cip, risc2 FROM pdp.pocs"
        for id, risc in u.getAll(sql, 'pdp'):
            intervencio22 = 1 if id in self.poc_act else 0
            recomptes[(risc, 'pob')] +=1
            recomptes[(risc, 'poc')] += intervencio22
        
        for (risc, tip), n in recomptes.items():
            print risc, tip, n
        
if __name__ == '__main__' :
        u.printTime("Inici")
        
        pocs()

        u.printTime("Fi")
