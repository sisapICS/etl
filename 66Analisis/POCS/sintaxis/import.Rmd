


```{r}
options(java.parameters = "-Xmx8048m")
library(RJDBC)
server<-"excdox-scan.cpd4.intranet.gencat.cat"
port<-"1522"
sid<-"excdox01srv"
# Create connection driver and open connection
jdbcDriver <- JDBC(driverClass="oracle.jdbc.OracleDriver", classPath="C:/Users/nmora/OneDrive - Generalitat de Catalunya/Documents/ojdbc11.jar")
jdbcStr = paste("jdbc:oracle:thin:@", server, ":", port, "/", sid, sep="")
jdbcConnection = dbConnect(jdbcDriver, jdbcStr, "DWECOMA", "ranagustavo2020")



# Query on the Oracle instance name.
dbs_pocs <- as.data.table(dbGetQuery(jdbcConnection,"SELECT hash_redics,  c_up, c_data_naix, c_sexe, c_nacionalitat, c_gma_codi, c_gma_complexitat, C_VISITES_ANY, PR_PCC_DATA, PR_MACA_DATA, 
                     PS_HTA_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                     PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                     PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                     PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                     PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                     PS_DEMENCIA_DATA, PS_ARTROSI_DATA,
                     V_RCV_DATA, V_RCV_VALOR,
                     F_HTA_IECA_ARA2, F_HTA_CALCIOA, F_HTA_BETABLOQ, F_HTA_DIURETICS, F_HTA_ALFABLOQ, F_HTA_ALTRES, F_HTA_COMBINACIONS,
                     F_DIAB_ADO, F_DIAB_INSULINA, F_MPOC_ASMA,F_ANTIAGREGANTS_ANTICOA, F_AINE, F_ANALGESICS,
                     F_ANTIDEPRESSIUS, F_ANSIO_HIPN, F_ANTIPSICOTICS, F_ANTIULCEROSOS, F_ANTIESP_URI, F_CORTIC_SISTEM,
                     F_ANTIEPILEPTICS,F_HIPOLIPEMIANTS
  FROM DWSISAP.DBS_POCS poc INNER JOIN dwsisap.PDPTB101_RELACIO pr
  ON c_cip=hash_covid
  WHERE  c_edat_anys>=75"))


dbDisconnect(jdbcConnection)
```

```{r}
saveRDS(dbs_pocs, "../dades/dbs_pocs.RDS")
```


```{r}
drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password="",host = "10.80.217.68", 
                 # port=3309,
                 port=3307,
                 dbname="permanent")
nfetch <- -1
query <- dbSendQuery(con,
                     statement = "select * from permanent.cat_centres")
cat_centres <- data.table(fetch(query, n = nfetch))
# query <- dbSendQuery(con,
#                      statement = "select * from permanent.pocs_2022")
# pocs_2022 <- data.table(fetch(query, n = nfetch))
query <- dbSendQuery(con,
                     statement = "select * from permanent.pocs_2023")
pocs_2023 <- data.table(fetch(query, n = nfetch))
query <- dbSendQuery(con,
                     statement = "select * from permanent.urgencies_23")
urgencies_2023 <- data.table(fetch(query, n = nfetch))
query <- dbSendQuery(con,
                     statement = "select * from permanent.pocs_controls_negatius")
pocs_controls_negatius <- data.table(fetch(query, n = nfetch))
query <- dbSendQuery(con,
                     statement = "select * from permanent.pocs_deshidratacio")
pocs_deshidratacio <- data.table(fetch(query, n = nfetch))

dbDisconnect(con)
```

```{r eval=FALSE, include=FALSE}
Encoding(pocs_2023$vc_dependencia_valor) <- "latin1"
Encoding(pocs_2023$vc_gerontop_valor) <- "latin1"
```


```{r}
saveRDS(cat_centres, "../dades/cat_centres.RDS")
saveRDS(pocs_2023, "../dades/pocs_2023.RDS")
saveRDS(urgencies_2023, "../dades/urgencies_2023.RDS")
saveRDS(pocs_controls_negatius, "../dades/pocs_controls_negatius.RDS")
saveRDS(pocs_deshidratacio, "../dades/pocs_deshidratacio.RDS")
```


