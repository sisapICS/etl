---
title: "COVID19 - Algoritme IA"
author: "SISAP"
date: "`r Sys.Date()`"
output:
  html_document:
    toc: yes
    toc_float: true
    toc_depth: 4
  word_document:
    toc: yes
    toc_depth: '4'
---

```{r, include=FALSE}

import.original.var <- FALSE
# Especifica si cal utilitzar les dades originals de les taules, ara mateix les dades són del 02/06/2020
```

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)

options(scipen=999)
options(width = 300)
```

```{r parametres}
warning.var <- FALSE

eval.logos <- TRUE
eval.cq <- TRUE
eval.dflux <- TRUE
eval.criterios <- TRUE
eval.cpoblacio <- TRUE
eval.exploraciopcr <- TRUE
eval.annex <- TRUE
eval.annex.chunks <- TRUE # Per reduir temps d'execució: especifica si s'evaluen les comparacions de l'annex
eval.exportbddd <- TRUE
```

```{r child="logos.Rmd", eval=eval.logos}
```

```{r child="library.Rmd"}
```

```{r child="myconexions.Rmd"}
```


# Material

A continuació hi ha els links al material de l'estudi.

- [Funcionament de l'Algoritme](https://drive.google.com/file/d/1su9UCK9mp9WAAKqUsCYegLl8Dg5353bJ/view)


- Entrades del Blog de l'ecap relacionades amb el qüestionari:

  - [Com es pot fer el seguiment dels pacients COVID-19 (26/03/2020)](  https://ecapics.wordpress.com/2020/03/26/com-es-pot-fer-el-seguiment-dels-pacients-afectats-per-covid-19/)



- **[Estudio Nacional de sero-Epidemiología de la Infección por SARS-CoV-2 en España (ENE-Covid)](https://www.mscbs.gob.es/ciudadanos/ene-covid/home.htm)**


- [Document tècnic](https://docs.google.com/document/d/1QoAy3xmgykvmOiMfrEBKl8L55-A0jpeyuOi29vwbLjc/edit?usp=sharing)

  Inclou els dubtes/comentaris als diferents informes realitzats.

  
- Article:
  **[Aplicació d’intel·ligència artificial a la història clínica electrònica d’atenció primària per a diagnòstic i predicció en COVID-19]( https://docs.google.com/document/d/1wEw_9HfmWAa1HlNZs38x5y4vUU1HaVgEjxY5ZrNuaLc/edit?usp=sharing)**


# Importació

Les dades per l'estudi estàn a la taules:

- **SISAP_COVID_PAC_MASTER** a **redICS**
- **SISAP_COVID_PAC_XML_SEG** a **redICS**
- **SISAP_COVID_PAC_PRV_EXT** a **redICS**

## sisap_covid_pac_xml_seg

Taula que conté la informació del qüestionari XML "Seguiment COVID-19"

```{r}

if (import.original.var==TRUE) {
  con <- dbConnect(redics.drv, 
                   username="PREDUEHE", password="nanare34",
                   dbname=redics.connect.string)
  query <- "select * from preduffa.sisap_covid_pac_xml_seg"
  dt.xmlseg <- data.table(dbGetQuery(con,query))
  con.disc.var <- dbDisconnect(con)
  setnames(dt.xmlseg,tolower(names(dt.xmlseg)))
  
  saveRDS(dt.xmlseg,paste0("dt.xmlseg_", format(Sys.Date(),'%Y%m%d'),".rds"))
} else dt.xmlseg <- readRDS("../data/dt.xmlseg_20200602.rds")

#dt.xmlseg
```

Dades a data 02/06/2020:

- Número de registres: `r nrow(dt.xmlseg)`
- Nùmero de pacientes: `r nrow(unique(dt.xmlseg[,.(hash)]))`
- Nùmero de columnes: `r ncol(dt.xmlseg)`

```{r child="cq.Rmd", eval=eval.cq}

```

## sisap_covid_pac_master

Taula que conté la informació dels pacients COVID-19 (confirmats, possibles i contactes)

```{r}

if (import.original.var==TRUE) {
  con <- dbConnect(redics.drv, 
                   username=redics.user, password=redics.password,
                   dbname=redics.connect.string)
  query <- "select * from preduffa.sisap_covid_pac_master"
  dt.master <- data.table(dbGetQuery(con,query))
  con.disc.var <- dbDisconnect(con)
  setnames(dt.master,tolower(names(dt.master)))

  saveRDS(dt.master,paste0("dt.master_", format(Sys.Date(),'%Y%m%d'),".rds"))
} else dt.master <- readRDS("../data/dt.master_20200602.rds")

#names(dt.master)
setnames(dt.master,"up","up_master")
dt.master.flt <- dt.master[,c("hash","up_master","edat", "sexe","estat","cas_data","pcr_res","pcr_data")]
#dt.master.flt
```

- Número de registres: `r nrow(dt.master)`
- Nùmero de pacientes: `r nrow(unique(dt.master[,.(hash)]))`
- Nùmero de columnes: `r ncol(dt.master)`

## sisap_covid_pac_prv_ext (sisap_coronavirus_proves)

```{r}
#2020/06/18 N=857646
#['HASH', 'PROVA', 'ORIGEN', 'DATA', 'RESULTAT_COD', 'RESULTAT_DES', 'ESTAT_COD', 'ESTAT_DES']

dt.prv <- fread("D:/SISAP/peticions/COVID19/algoritme_ia/data/sisap_covid_pac_prv_ext.txt", sep="@")
setnames(dt.prv,c('hash','prova','origen','data_prova','resultat_cod','resultat_des','estat_cod','estat_res'))
#dt.prv

dt.prv[,data_prova:=as.Date(data_prova,'%Y-%m-%d')]

dt.prv.flt <- dt.prv[prova=='PCR' & estat_cod <9,]
```

- Número de registres: `r nrow(dt.prv)`
- Nùmero de pacientes: `r nrow(unique(dt.prv[,.(hash)]))`
- Nùmero de columnes: `r ncol(dt.prv)`

Es seleccionen els registres amb **prova='PCR' i estat_cod<9**:

- Número de registres: `r nrow(dt.prv.flt)`
- Nùmero de pacientes: `r nrow(unique(dt.prv.flt[,.(hash)]))`
- Nùmero de columnes: `r ncol(dt.prv.flt)`

## Catàleg CENTRES

Taula que conté les UP de l'ICS i la variable MEDEA.

```{r importació cataleg, include=FALSE}

if (import.original.var==TRUE) {
  con <- dbConnect(drv = sisap.drv 
                   , user = sisap.user
                   , password=sisap.password
                   , host = sisap.host
                   , port=sisap.port
                   , dbname="nodrizas")
  query <- "select ep, scs_codi, ics_desc, medea from cat_centres"
  dt.catcentres <- data.table(dbGetQuery(con, query))
  disc <- dbDisconnect(con)

  setnames(dt.catcentres,c('ep','up','up_desc','medea'))

  saveRDS(dt.catcentres, paste0("dt.catcentres_", format(Sys.Date(),'%Y%m%d'),".rds"))
} else dt.catcentres <- readRDS("../data/dt.catcentres_20200602.rds")
```


```{r merge}
setkey(dt.xmlseg, up)
setkey(dt.catcentres, up)
dt.xmlseg <- merge(dt.xmlseg
                   , dt.catcentres
                   , by.x = 'up'
                   , by.y = 'up'
                   , all.x = TRUE)

# MEDEA
  dt.xmlseg[,medea:=factor(medea)]
  
  
setkey(dt.xmlseg, hash)
setkey(dt.master.flt, hash)
dt.xmlseg <- merge(dt.xmlseg
                   , dt.master.flt
                   , by.x = 'hash'
                   , by.y = 'hash'
                   , all.x = TRUE)

# ESTAT
  dt.xmlseg[,estat:=factor(estat)]
  # dt.xmlseg[,.N, by=.(estat)][order(estat)]

# CAS_DATA
  dt.xmlseg[, cas_data:=as.Date(cas_data,format="%Y-%m-%d")]
  
# PCR
  dt.xmlseg[, pcr_res:=factor(pcr_res)]
  dt.xmlseg[, pcr_data:=as.Date(pcr_data,format="%Y-%m-%d")]

# CAS
  #dt.xmlseg[, .N, by=.(estat)]
  
  # Confirmat
    dt.xmlseg[,cas_c:=ifelse(estat=='Confirmat','Sí','No')][is.na(estat), cas_c:='No']
    #dt.xmlseg[, .N, by=.(cas_c)]
    
  # Possible
    dt.xmlseg[,cas_p:=ifelse(estat=='Possible','Sí','No')][is.na(estat), cas_p:='No']
    #dt.xmlseg[, .N, by=.(cas_p)]
    
  # Confirmat i/o possible
    dt.xmlseg[,cas:=ifelse(estat=='Confirmat' | estat=='Possible','Sí','No')][is.na(estat), cas:='No']
    #dt.xmlseg[, .N, by=.(cas)]

# Afegir PCR als pacients sense informació
  dt.pcrna <- dt.xmlseg[is.na(pcr_res),]
  setkey(dt.pcrna, hash)
  setkey(dt.prv.flt, hash)
  dt.pcrna <- merge(dt.pcrna
                     , dt.prv.flt
                     , by.x = 'hash'
                     , by.y = 'hash'
                     , all.x = TRUE)
  dt.pcrna.flt <- dt.pcrna[, ddif_prova:=as.numeric(xml_data_test - data_prova)]
  dt.pcrna.flt <- dt.pcrna.flt[ddif_prova >= -4 & ddif_prova <= 15,]
  
  # Selecció registre
    dt.pcrna.pax <- dt.pcrna.flt[order(hash,ddif_prova)][,.SD[1], by=hash][,.(hash,data_prova,estat_res)]

  # Afegir la informació a xml
    setkey(dt.xmlseg, hash)
    setkey(dt.pcrna.pax, hash)
    dt.xmlseg <- merge(dt.xmlseg
                       , dt.pcrna.pax
                       , by.x = 'hash'
                       , by.y = 'hash'
                       , all.x = TRUE)
    #dt.xmlseg[,.N,.(pcr_res)]
    # Positiu	33090			
    # NA	63879			
    # Negatiu	23879			
    # No concloent	756			
    # No valorable	62	
    dt.xmlseg[,pcr_res_nou:=ifelse(is.na(pcr_res),estat_res,pcr_res)]
    #dt.xmlseg[,.N,.(pcr_res_nou)]
    # 4	33090			
    # NA	61211			
    # Negatiu	2636			
    # 1	23879			
    # 2	756			
    # 3	62			
    # No concloent	11			
    # Positiu	21
    dt.xmlseg[pcr_res_nou==4,pcr_res_nou:='Positiu']
    dt.xmlseg[pcr_res_nou==3,pcr_res_nou:='No valorable']
    dt.xmlseg[pcr_res_nou==2,pcr_res_nou:='No concloent']
    dt.xmlseg[pcr_res_nou==1,pcr_res_nou:='Negatiu']
    #dt.xmlseg[,.N,.(pcr_res_nou)]
    # Positiu	33111	vs 33090		
    # NA	61211 vs 63879
    # Negatiu	26515 vs 23879
    # No concloent	767 vs 756
    # No valorable	62 vs 62	
    
    # PCR DATA
      #dt.xmlseg[,.N,.(pcr_data)]
      dt.xmlseg[,pcr_data_nou:=ifelse(is.na(pcr_data),as.character(data_prova),as.character(pcr_data))]
      dt.xmlseg[,pcr_data:=as.Date(pcr_data, '%Y-%m-%d')]
      dt.xmlseg[,pcr_data_nou:=as.Date(pcr_data_nou, '%Y-%m-%d')]
      #dt.xmlseg[,.N,.(pcr_data_nou)]
      #dt.xmlseg[,a:=pcr_data-pcr_data_nou]
      #dt.xmlseg[, .N, by=.(a)]
      

# Afegir MEDEA a MASTER per a la comparació població
  setkey(dt.master.flt, up_master)
  dt.master.flt <- merge(dt.master.flt
                        , dt.catcentres
                        , by.x = 'up_master'
                        , by.y = 'up'
                        , all.x = TRUE)
  #dt.master.flt[,.N,by=.(medea)]
  #dt.master.flt[,.N,by=.(up_master)]
```


```{r child="criteris.Rmd", echo=FALSE, message=FALSE, warning=FALSE, eval=eval.criterios}

```


```{r seleccio primer registre}

# dt.xmlseg.flt[,.N, by=hash]
# dt.xmlseg.flt[hash=='E3D82F4082AEAD6CBFBB05F6F6A882A99EF9312A']
dt.xmlseg.pax <- dt.xmlseg.flt[order(hash,xml_data_test)][,.SD[1], by=hash]
#dt.xmlseg.pax[hash=='E3D82F4082AEAD6CBFBB05F6F6A882A99EF9312A']
```


```{r child="diagramaflux.Rmd", echo=FALSE, message=FALSE, warning=FALSE, eval=eval.dflux}

```


```{r seleccio de columnes}
# Ordre columnes
  # names(dt.xmlseg.pax)
  dt.xmlseg.pax1 <- dt.xmlseg.pax[ ,
                                   .(hash
                                    , xml_data_test
                                    , xml_edat
                                    , xml_sahs_2
                                    , xml_tipusvisita
                                    , xml_modatencio        
                                    # MEDEA,
                                    , xml_estatgeneral
                                    , xml_estatmental
                                    , xml_tos
                                    , xml_febre_sino
                                    #, xml_diesfebre
                                    #, xml_febre_cedeix
                                    , xml_dispnea
                                    #, xml_diesdispnea
                                    #, xml_parlatallada
                                    , xml_dolorpleuritic         
                                    , xml_hemoptisi
                                    , xml_vomits
                                    , xml_diarrea
                                    , nsimptomesestat, nsimptomes
                                    , xml_altgust
                                    , xml_interpretacio, iadx, contacte
                                    , xml_recomanacio
                                    # , servei
                                    , medea
                                    , edat
                                    , sexe
                                    , estat
                                    , cas_data
                                    , cas_c
                                    #, cas_p
                                    #, cas
                                    , pcr_res, pcr_res_nou
                                    , pcr_data, pcr_data_nou
                                    , ddif_pcr
                                  )]

#dt.xmlseg.pax1[, num_obs := Reduce(`+`, lapply(.SD,function(x) !is.na(x)))]
#dt.xmlseg.pax1[, .N, by=.(num_obs)]
#a <- dt.xmlseg.pax1[num_obs==15,]
```


```{r child="comparaciopoblacio.Rmd", echo=FALSE, message=FALSE, warning=FALSE, eval=eval.cpoblacio}

```

# <span style="color: blue;">Anàlisi CONTACTES</span>.

## Descriptiva univariada

Taula 5: Característiques dels pacients classificats com a CONTACTES per l'algoritme.

```{r contactes - descriptiva univariada, warning=warning.var}

dt.xmlseg.contacte <- dt.xmlseg.pax1[iadx=="Contacte",]

df <- dt.xmlseg.contacte
res <- compareGroups(xml_modatencio  ~ . -hash -xml_data_test
                                         -xml_interpretacio -contacte -iadx -xml_recomanacio
                                         -edat -sexe
                                         -cas_data
                     , df
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = FALSE
                  , show.all = TRUE
                  , show.p.overall = FALSE
                  )
export2md(cT
          , caption = "")
```


# Anàlisi COVID-19

## Descriptiva univariada

Taula 6: Característiques dels pacients classificats com a COVID-19 per l'algoritme.

```{r covid-19 - descriptiva univariada, warning=warning.var}

dt.xmlseg.covid19 <- dt.xmlseg.pax1[iadx=="COVID-19",]

df <- dt.xmlseg.covid19
res <- compareGroups(xml_modatencio  ~ . -hash -xml_data_test
                                         -contacte -iadx -xml_recomanacio
                                         -edat -sexe
                     , df
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = FALSE
                  , show.all = TRUE
                  , show.p.overall = FALSE
                  )
export2md(cT
          , caption = "")
```


# Anàlisi NO & SOSPITA

## Descriptiva univariada

Taula 7: Descriptiva de les variables del XML seguiment.

```{r descriptiva univariada, warning=warning.var}

dt.xmlseg.se <- dt.xmlseg.pax1[iadx=="No" | iadx=="Sospita",]

dt.xmlseg.se[,xml_edat:=as.numeric(xml_edat)]
df <- dt.xmlseg.se
res <- compareGroups(xml_modatencio  ~ . -hash -xml_data_test
                                         -edat -sexe
                                         #-pcr -pcr_res -pcr_data -timetopcr -timetopcrad
                     , df
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = FALSE
                  , show.all = TRUE
                  , show.p.overall = FALSE
                  )
export2md(cT
          , caption = "")
```

### Data XML

Descriptiva Data XML

```{r, warning=warning.var}

ggplot(dt.xmlseg.se, aes(x=xml_data_test)) +
  geom_bar(fill='orange', color="white") + 
  geom_vline(xintercept=as.Date('2020-03-26'), lty=4) +
  geom_vline(xintercept=as.Date('2020-04-30'), lty=4)
# +
#   facet_wrap(~ ci.pestudi, ncol=1)
```


### Edat

Descriptiva edat

```{r, warning=warning.var}
#ggplot(dt.xmlseg.se, aes(x=xml_edat)) + geom_histogram(fill='orange', color="white")
#ggplot(dt.xmlseg.se, aes(xml_edat)) + 
#    geom_bar(aes(fill=as.factor(xml_sahs_2)), position="fill") +
#    coord_flip()
```

### Interpretacio

Descriptiva dels possibles resultats de l'algorime (camp **interpretació**)

Taula 8: Número i percentatge dels resultats de l'algoritme (variable interpretació)

```{r, warning=warning.var, message=FALSE}

df <- data.frame(dt.xmlseg.se)
res <- compareGroups(xml_modatencio ~ xml_interpretacio,
                     df,
                     max.xlev = 1000)
export2md(createTable(res
                      , show.all = TRUE
                      , show.p.overall = FALSE
                      , show.descr = FALSE)
          , caption = "")
```

### Recomanació

Descriptiva de les possibles recomanacions d'actuacions de l'algoritme (camp **recomanació**)

Taula 9: Número i percentatge de les recomanacions d'actuacions de l'algoritme (variable recomanació)

```{r, warning=warning.var, message=FALSE}

df <- data.frame(dt.xmlseg.se)
res <- compareGroups(xml_modatencio ~ xml_recomanacio,
                     df,
                     max.xlev = 1000)
export2md(createTable(res
                      , show.all = TRUE
                      , show.p.overall = FALSE
                      , show.descr = FALSE)
          , caption = "")

#dt.xmlseg[,.N, by=.(iadx,xml_recomanacio)][order(iadx)]
```

## Comparació NO vs SOSPITA COVID-19

Taula 10: Comparació característiques dels pacientes segons classificació algoritme

```{r analisi no vs sospita, warning=warning.var}
dt.xmlseg.nosospita <- dt.xmlseg.pax1[iadx=="No" | iadx=="Sospita"]

df <- dt.xmlseg.nosospita
res <- compareGroups(iadx  ~ . -hash -xml_data_test
                               #-cas_data
                               -edat -sexe
                               #-pcr 
                               -pcr_data 
                               #-pcr_res -timetopcr -timetopcrad
                     , df
                     , max.xlev = 100
                     , include.miss = TRUE
                     , byrow = FALSE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT
          , caption = "")
```

# Anàlisi RESULTAT PCR

Taula 11: Número i percentatge de pcr positives segons variables XML seguiment

```{r analisi resultat pcr, warning=warning.var}
df <- dt.xmlseg.se
res <- compareGroups(pcr_res_nou  ~ . -hash -xml_data_test
                                -estat
                                -edat -sexe
                                -cas_data
                                -pcr_data -pcr_res -pcr_data_nou #-timetopcr -timetopcrad
                     , df
                     , include.miss = TRUE
                     , byrow = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT
          , caption = "")
```

# Anàlisi CAS

## CONFIRMAT

Taula 12: Número i percentatge de casos confirmats segons variables XML seguiment

```{r analisi cas confirmat, warning=warning.var}
df <- dt.xmlseg.se
res <- compareGroups(cas_c  ~ . -hash -xml_data_test
                                -estat
                                -edat -sexe
                                -cas_data
                                -pcr_data -pcr_res #-timetopcr -timetopcrad
                     , df
                     , include.miss = TRUE
                     , byrow = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT
          , caption = "")
```


```{r child="annex.Rmd", eval=eval.annex}

```

```{r child="exportbbdd.Rmd", eval=eval.exportbddd}

```

