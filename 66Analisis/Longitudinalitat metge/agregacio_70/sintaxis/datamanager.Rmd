
```{r}
cont_aggr_eap <- cont[ ,  c(lapply(mget(c("r1", "r2", "r3", "r4")), function(x) {mean(x>=0.7)*100}), 
                            lapply(mget(c("r1", "r2", "r3", "r4")), function(x) {mean(x)*100})), by = .(up)]
colnames(cont_aggr_eap)[2:5] <- paste0(colnames(cont_aggr_eap)[2:5], "_70")
colnames(cont_aggr_eap)[6:9] <- paste0(colnames(cont_aggr_eap)[6:9], "_mean")


cont_aggr_eap <- merge(cont_aggr_eap, cat_centres[, .(scs_codi, ics_codi, amb_codi, medea)], by.x = "up", by.y = "scs_codi", all.x = T)
cont_aggr_eap_ics <- cont_aggr_eap[amb_codi != "00"]
cont_aggr_eap_ics <- merge(cont_aggr_eap_ics, long_previ_internacionals, by = "ics_codi", all.x = T)
```

```{r}
mapa_up <- MAPA_MASTER_ETIQUETA_UP[ENTITAT_CODI %in% cat_centres[amb_codi != "00" & tip_eap %in% c("A", "M")]$scs_codi]
```


```{r}
khalix_up <- merge(khalix_dcast_mf_aggr_br_selected, cat_centres[, .SD, .SDcols = c("scs_codi", "ics_codi", "amb_codi")], by = "ics_codi", all.x = T)
khalix_up <- khalix_up[amb_codi != "00"]
```
