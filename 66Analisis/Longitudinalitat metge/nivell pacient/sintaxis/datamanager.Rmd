
# Fonts de dades

```{r parametres}
data_calcul <- as.Date("2021-12-31")
```

## long_temps_junt

```{r junt}
temps_21_dm <- copy(temps_21)



temps_21_dm[, ates := as.factor(ates)]
temps_21_dm[, edat := trunc((data_naix %--% data_calcul) / years(1))]
temps_21_dm[, edat_15 := ifelse(edat < 15, "<15 anys", "> 14 anys")]
temps_21_dm[, sexe := factor(sexe, levels = c("D", "H"), labels = c("Dona", "Home"))]

temps_21_dm[primera_visitaM == "0000-00-00", primera_visitaM := NA]
temps_21_dm[, primera_visitaM := as.Date(primera_visitaM)]
temps_21_dm[, anys_desde_primera_visita := trunc((primera_visitaM %--% data_calcul) / years(1))]
temps_21_dm[, anys_desde_primera_visita_cat := cut(anys_desde_primera_visita, breaks = c(0, 1, 2, 4, 6, 11, 16, 20), labels = c("0", "1", "2-3", "4-5", "6-10", "11-15", ">15"), include.lowest = T, right = F)]

temps_21_dm[ultima_visitaM == "0000-00-00", ultima_visitaM := NA]
temps_21_dm[, ultima_visitaM := as.Date(ultima_visitaM)]
temps_21_dm[, mesos_desde_ultima_visita_propi := trunc((ultima_visitaM %--% data_calcul) / months(1))]
```

```{r}
table1(~ ates + edat + edat_15 + sexe + anys_desde_primera_visita + anys_desde_primera_visita_cat + mesos_desde_ultima_visita_propi, data = temps_21_dm)
```

```{r}
summary(temps_21_dm[, .(primera_visitaM, ultima_visitaM)])
```

## long_temps_dbs

```{r}
dbs_dm <- copy(dbs)

dbs_dm[, c_ultima_visita_eap := as.Date(c_ultima_visita_eap)]
dbs_dm[, mesos_desde_ultima_visita := trunc((c_ultima_visita_eap %--% data_calcul) / months(1))]
dbs_dm[, c_institucionalitzat := factor(ifelse(c_institucionalitzat == 0, 0, 1), levels = 0:1, labels = c("No", "Sí"))]
dbs_dm[c_nacionalitat == "", c_nacionalitat := "ESPANYA"]


vars_ps <- names(dbs_dm)[which(toupper(sub("\\_.*", "", names(dbs_dm))) == "PS")]
dbs_dm[, toupper(vars_ps) := lapply(.SD, function(x){
  factor(ifelse(x == "0000-00-00", 0, 1), levels = 0:1, labels = c("No", "Sí"))
}), .SDcols = vars_ps]

vars_f <- names(dbs_dm)[which(toupper(sub("\\_.*", "", names(dbs_dm))) == "F")]
dbs_dm[, toupper(vars_f) := lapply(.SD, function(x){
  factor(ifelse(x == "", 0, 1), levels = 0:1, labels = c("No", "Sí"))
}), .SDcols = vars_f]

dbs_dm <- dbs_dm[, .SD, .SDcols = c("id_cip_sec", "gma_codi", "gma_complexitat", "gma_n_croniques", "c_ultima_visita_eap", "mesos_desde_ultima_visita", "c_visites_any", "c_nacionalitat", "c_institucionalitzat", toupper(vars_ps), toupper(vars_f))]
```

```{r}
table1(~ . , data = dbs_dm[, .SD, .SDcols = c("gma_complexitat", "gma_n_croniques", "mesos_desde_ultima_visita", "c_visites_any", "c_nacionalitat", "c_institucionalitzat", toupper(vars_ps), toupper(vars_f))])
```

## defuncions

```{r}
defuncions_dm <- copy(defuncions)
defuncions_dm[, data_defuncio := as.Date(usua_data_situacio)]

temps_21_defuncions_dm <- merge(temps_21_dm, defuncions_dm[, .(id_cip_sec, data_defuncio)], by = "id_cip_sec", all.x = T)
```


## long_temps_cont

```{r}
cont_dm <- copy(cont[, .(id_cip_sec, up, servei, nprof, vprof, totals, r1, r2, r3, r4)])
```

```{r}
table1(~ . , data = cont_dm[, .(servei, nprof, vprof, totals, r1, r2, r3, r4)])
```

# Merge

## long_temps_junt + long_temps_dbs

```{r}
junt_dbs_dm <- merge(temps_21_dm, dbs_dm[c_visites_any>0], by = "id_cip_sec")
junt_dbs_dm <- junt_dbs_dm[edat>14]

junt_dbs_dm[, na_primera_visita := ifelse(is.na(primera_visitaM), "NA primera visita", "No NA primera visita")]
junt_dbs_dm[, nacionalitat_esp := factor(ifelse(c_nacionalitat == "ESPANYA", "Espanyola", "Altres"))]

```

