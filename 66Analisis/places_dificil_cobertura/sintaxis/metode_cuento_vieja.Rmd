
## Indicador sintètic

Per crear l'indicador sintètic s'ha creat un indicador binari per identificar els EAP que superen el P80 de cada un dels indicadors.

<!-- ```{r} -->
<!-- indicadors <- indicadors[, -c("dist_mesproper120", "dist_mespropercat", "dist_mespropercat56", "CARASSIS08", "ENVELLIMENT_75", "ENVELLIMENT_02", "sobrecarrega014", "sobrecarrega")] -->
<!-- ``` -->


```{r eval=FALSE, include=FALSE}
indicadors[, paste0(c("dist_mespropercat3456", "distancia_consultori", "ENVELLIMENT_65", "sobrecarrega14", "guardies_total"), "_P80") := lapply(.SD, function(x) ifelse(x < quantile(x, .8, na.rm = T) | is.na(x), 0, 1)), .SDcols = c("dist_mespropercat3456", "distancia_consultori", "ENVELLIMENT_65", "sobrecarrega14", "guardies_total")]
indicadors[, dist_mespropercat3456_P80 := ifelse(dist_mespropercat3456 < 25000 | is.na(dist_mespropercat3456), 0, 1)]
indicadors[, distancia_consultori_P80 := ifelse(distancia_consultori < 30 | is.na(distancia_consultori), 0, 1)]
indicadors[, sobrecarrega14_P80 := ifelse(sobrecarrega14 < quantile(sobrecarrega14, .9, na.rm = T) | is.na(sobrecarrega14), 0, 1)]
# indicadors[, sobrecarrega14_P80_new := ifelse(sobrecarrega14_bis_aga_new < quantile(sobrecarrega14_bis_aga_new, .9, na.rm = T) | is.na(sobrecarrega14_bis_aga_new), 0, 1)]
indicadors[, guardies_total_P80 := ifelse(guardies_total == 0 | is.na(guardies_total), 0, 1)]
```

```{r}
indicadors[, dist_mespropercat3456_P80 := ifelse(dist_mespropercat3456 < 25000 | is.na(dist_mespropercat3456), 0, 1)]
indicadors[, sobrecarrega14_P80 := ifelse(sobrecarrega14 < 10.67 | is.na(sobrecarrega14), 0, 1)]
indicadors[, guardies_total_P80 := ifelse(guardies_total == 0 | is.na(guardies_total), 0, 1)]
indicadors[, ENVELLIMENT_65_P80 := ifelse(ENVELLIMENT_65 < 25.8914 | is.na(ENVELLIMENT_65), 0, 1)]
indicadors[, distancia_consultori_P80 := ifelse(distancia_consultori < 30 | is.na(distancia_consultori), 0, 1)]
```


#### Figura 7. Distribució del nombre d'EAP que superen el P80 per indicador

```{r eval=FALSE, fig.height=5, fig.width=8, message=FALSE, warning=FALSE, include=FALSE}
dg <- melt(indicadors, id.vars = c("ics_codi", "amb_desc"), measure.vars = c("dist_mespropercat3456_P80", "distancia_consultori_P80", "ENVELLIMENT_65_P80", "sobrecarrega14_P80", "guardies_total_P80", "alta_ruralitat"))
dg[, value := factor(value, levels = c("0", "No", "1", "Sí"), labels = c("No", "No", "Sí", "Sí"))]


dg <- dg[, .(
  value = mean(value == "Sí", na.rm = T)
), by = .(variable)]

ggplot(dg, aes(x = variable)) +
  geom_bar(aes(y = value), stat = "Identity", fill = "#067fc8d9", alpha = .9) + 
  scale_x_discrete(limits = c("alta_ruralitat", "dist_mespropercat3456_P80", "sobrecarrega14_P80", "guardies_total_P80", "ENVELLIMENT_65_P80", "distancia_consultori_P80"),
                   labels = c("Alta ruralitat", "Distància a\nhospital\nmés proper", "Sobrecàrrega\nestacional", "Guàrides totals", "Índex d'envelliment", "Distància a\nconsultori")) +
  scale_y_continuous(labels = scales::percent_format(accuracy = 1)) +
  theme_minimal() +
  theme(
    # axis.text.x = element_text(angle = 90, vjust = .5, hjust = 1)
  ) +
  labs(y = "Percentatge d'EAP", title = "", x = "") +
  geom_text(aes(y = value + .01, label = paste0(round(value*100, 2), "%")))
```

```{r eval=FALSE, fig.height=5, fig.width=8, message=FALSE, warning=FALSE, include=FALSE}
dg <- melt(indicadors, id.vars = c("ics_codi", "amb_desc"), measure.vars = c("dist_mespropercat3456_P80", "distancia_consultori_P80", "ENVELLIMENT_65_P80", "sobrecarrega14_P80", "guardies_total_P80", "alta_ruralitat"))
dg[, value := factor(value, levels = c("0", "No", "1", "Sí"), labels = c("No", "No", "Sí", "Sí"))]


dg <- dg[, .(
  value = mean(value == "Sí")
), by = .(variable, metode = amb_desc)]

ggplot(dg[metode > 0], aes(x = variable, y = value)) +
  geom_bar(stat = "Identity", fill = "#390099", alpha = .9) + 
  scale_x_discrete(limits = c("alta_ruralitat", "dist_mespropercat3456_P80", "distancia_consultori_P80", "ENVELLIMENT_65_P80", "sobrecarrega14_P80", "p_tss_P80", "guardies_total_P80"), 
                   labels = c("Alta ruralitat", "Distància a\nhospital\nmés proper", "Distància a\nconsultori", "Índex d'envelliment", "Sobrecàrrega\nestacional", "Percentatge\nde vacants no\ncobertes pels TSS", "Guàrdies Totals")) +
  scale_y_continuous(labels = scales::percent_format(accuracy = 1)) +
  theme_minimal() +
  theme(
    panel.grid.major.y = element_blank(),
    panel.grid.minor.y = element_blank(),
    axis.text.x = element_text(angle = 90, vjust = .5, hjust = 1)
  ) +
  facet_wrap(~ metode, ncol = 3) + labs(y = "Nombre d'EAP", title = "B") 
```

A cada indicador se li ha assignat una ponderació segons els resultats del PCA. Després s'ha sumat el nombre d'items que superen el P80, ponderant pels pesos.

#### Taula 1: Pesos assignats a cada indicador

```{r}
indicadors[, dist_mespropercat3456_P80_w := 3*dist_mespropercat3456_P80]
indicadors[, guardies_total_P80_w := 2*guardies_total_P80]
indicadors[, ENVELLIMENT_65_P80_w := 1*ENVELLIMENT_65_P80]
indicadors[, sobrecarrega14_P80_w := 2*sobrecarrega14_P80]
# indicadors[, p_tss_P80_w := 0*p_tss_P80]
indicadors[, distancia_consultori_P80_w := 1*distancia_consultori_P80]




indicadors[, items := apply(.SD, 1, function(x) sum(x)), .SDcols = c("dist_mespropercat3456_P80_w", "distancia_consultori_P80_w", "ENVELLIMENT_65_P80_w", "sobrecarrega14_P80_w", "guardies_total_P80_w")]
indicadors[, items := items + 3*(alta_ruralitat == "Sí")]

# kable(pesos[order(pes, decreasing = T)]) %>%
#   kable_styling(bootstrap_options = c("striped", "hover", "condensed", "responsive"), full_width = F)
```

#### Figura 8. Distribució del resultat sintètic.

```{r fig.width=10}
dg <- indicadors[, .N, items]
dg[order(items, decreasing = T), N_cumsum := cumsum(N)]
dg[, p := N_cumsum/sum(N)*100]
dg[, items_cat := cut(items, breaks = c(0, 2, 3, 6, 12), include.lowest = T, right = 1, labels = c("No", "Low", "Medium", "High"))]
# , fill = "#390099"
ggplot(dg, aes(x =  reorder(items, -items))) +
  geom_bar(stat = "identity", alpha = .9, aes(y = p, fill = items_cat)) +
  geom_line(aes(y = p, group = 1)) +
  geom_point(aes(y = p)) +
  geom_text(aes(y = p + 5, label = round(p, 2)), size = 4) +
  geom_hline(yintercept = 24.31, linetype = 2, color = "#6E8387", size = .7) +
  scale_y_continuous(breaks = seq(0, 100, 20)) +
  scale_fill_manual(values = c("No" = "#067fc8d9", "Low" = "#FFE285", "Medium" = "#F8A827", "High" = "#FE7B72")) +
  theme_minimal() +
  theme(
    legend.position = "top"
  ) +
  labs(x = "Puntuació de l'indicador sintètic", y = "Percentatge acumulat d'EAP", fill = "Indicador sintètic categoritzat")
```

*Es defineixen EAP de difícil cobertura segons l'indicador sintètic aquells que tenen una puntuació més gran de 2 punts*

```{r}
indicadors[, dificil_cobertura := factor(ifelse(items > 3, "Sí", "No"), levels = c("No", "Sí"), labels = c("No", "Sí"))]
indicadors[, dificil_cobertura_cat := cut(items, breaks = c(0, 2, 3, 6, 12), include.lowest = T, right = 1, labels = c("No", "Low", "Medium", "High"))]
# indicadors[, dificil_cobertura_3456 := factor(ifelse(items > 2, "Sí", "No"), levels = c("No", "Sí"), labels = c("No", "Sí"))]
```


