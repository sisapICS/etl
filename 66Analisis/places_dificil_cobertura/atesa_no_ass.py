# -*- coding: utf8 -*-

"""
Calculem atesa no assignada de fora del AGA. Excloem institucionalitzats
"""
import hashlib as h

import collections as c

import sisapUtils as u

db = "permanent"
tb = "atesa_no_assignada_2023"
tb2 = "validacio_sarria_2023"


class atesa_no_assignada(object):
    """."""

    def __init__(self):
        """."""
        
        self.atesa_no_a = c.Counter()
        self.get_centres()
        self.get_exitus()
        self.get_poblacio()
        self.get_visites()
        self.get_taula()
        
    def get_centres(self):
        """Centres"""
        u.printTime("Centres")
        self.centres = {}

        sql = """select scs_codi, ics_codi, aga from nodrizas.cat_centres"""
        for up, br, aga in u.getAll(sql, 'nodrizas'):
            self.centres[up] = aga
            
    def get_exitus(self):
        """exitus"""
        u.printTime("exitus")
        self.dbx = {}
        
        sql = """SELECT substr(cip, 0, 13),data_exitus FROM dwsisap.DBX d WHERE data_exitus BETWEEN DATE '2023-01-01' AND DATE '2023-12-31'"""
        for cip2, dat in u.getAll(sql, 'exadata'):
            cip = h.sha1(cip2).hexdigest().upper()
            self.dbx[cip] = True

    def get_poblacio(self):
        """Població"""
        u.printTime("Població")    
        self.poblacio = {}
        self.resis = {}
        sql ="""SELECT c_cip, c_sector, c_up, c_visites_any, c_edat_anys,  CASE WHEN c_institucionalitzat IS NULL THEN 0 ELSE 1 END AS insti FROM dwsisap.dbs"""
        for cip, sector, up, visites, edat, insti in u.getAll(sql, 'exadata'):
            aga = self.centres[up] if up in self.centres else None
            self.poblacio[cip] = {'sector': sector, 'up': up, 'aga': aga}
            if insti == 1:
                self.resis[cip] = True
            if insti == 0:
                if edat > 14:
                    self.atesa_no_a[(up, 'ass')] += 1
                    if visites > 0:
                        self.atesa_no_a[(up, 'atesa')] += 1
        
    def get_visites(self):
        """Visites"""
        u.printTime("Visites")
        self.visites = {}
        self.eap_estranys = []
        sql = """SELECT PACIENT, SECTOR, UP, SISAP_SERVEI_CLASS, data FROM dwsisap.SISAP_MASTER_VISITES smv WHERE SITUACIO ='R' AND DATA BETWEEN DATE '2023-01-01' AND DATE '2023-12-31' and up is not null AND EDAT>14"""
        for cip, sector, up, servei, data in u.getAll(sql, 'exadata'):
            if up in self.centres:
                if cip not in self.resis:
                    if cip not in self.dbx:
                        aga = self.centres[up]
                        ates_no_assS = 1
                        ates_no_assA = 1
                        if cip in self.poblacio:
                            upA, sectorA, agaA = self.poblacio[cip]['up'], self.poblacio[cip]['sector'], self.poblacio[cip]['aga']
                            if up == upA:
                                ates_no_assS = 0
                                ates_no_assA = 0
                            else:
                                if sector == sectorA:
                                    ates_no_assS = 0
                                if aga == agaA:
                                    ates_no_assA = 0
                        if (cip, up, 'tot') not in self.visites:
                            self.atesa_no_a[(up, 'totS')] += ates_no_assS
                            self.atesa_no_a[(up, 'totA')] += ates_no_assA
                            self.visites[(cip, up, 'tot')] = True
                        if servei == 'MF':
                            if (cip, up, 'MF') not in self.visites:
                                self.atesa_no_a[(up, 'MFS')] += ates_no_assS
                                self.atesa_no_a[(up, 'MFA')] += ates_no_assA
                                self.visites[(cip, up, 'MF')] = True
                        if up in ('00479','00464') and ates_no_assA == 1:
                            self.eap_estranys.append([cip, up, servei,data])
            
    def get_taula(self):
        """dades"""
        
        u.printTime("taula")
        upload = []
        
        for (up, tip), n in self.atesa_no_a.items():
            if tip == 'ass':
                totalS = self.atesa_no_a[(up, 'totS')]
                totalA = self.atesa_no_a[(up, 'totA')]
                mf = self.atesa_no_a[(up, 'MFA')]
                atesa = self.atesa_no_a[(up, 'atesa')]
                upload.append([up, n, atesa, totalS, totalA, mf])
                
        columns = ["up varchar(5)", "assignada int", "atesa int", "atesa_no_tot_sector int", "atesa_no_tot_aga int", "atesa_no_mf_AGA int"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb, db)
        
        columns = ["pacient varchar(40)", "up varchar(5)", "servei varchar(50)", "data date"]
        u.createTable(tb2, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.eap_estranys, tb2, db)
        
if __name__ == '__main__' :
        u.printTime("Inici")
        
        atesa_no_assignada()

        u.printTime("Fi")