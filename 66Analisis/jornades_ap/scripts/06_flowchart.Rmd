
# Flowchart `r if (params$eval.tabset) '{.tabset}' else ''`

<!-- Diagrama amb els criteris d'inclusió i excluisió de l'estudi. -->

<!-- ```{r} -->
<!-- dt.cat_centres.dm <- readRDS('../data/dt.cat_centres.dm.rds') -->
<!-- ``` -->


<!-- ```{r} -->
<!-- dt.temp <- dt.cat_centres.dm -->
<!-- n1.centres <- nrow(dt.cat_centres.dm) -->

<!-- dt.temp <- dt.cat_centres.dm[tip_eap %in% c('A', 'M'),] -->
<!-- n2a.eap <- nrow(dt.temp) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- fin <- c(seq(1,20,1)) -->

<!-- grViz(" -->
<!--       digraph a_nice_graph -->
<!--       { -->

<!--       node[fontname = Helvetica, -->
<!--            fontcolor = black, -->
<!--            shape = box, -->
<!--            width = 1, -->
<!--            style = filled, -->
<!--            fillcolor = whitesmoke] -->

<!--       '@@1' -> '@@2' -->
<!--       } -->

<!--       [1]: paste0('Catàleg CENTRES SISAP', '\\n', 'Centres N=', n1.centres) -->
<!--       [2]: paste0('EAP (Adult/Mixte)', '\\n', 'EAPs N=', n2a.eap) -->

<!--       ", height = 400, width = 400) -->
<!-- ``` -->

<!-- ```{r filtre} -->

<!-- #filtre =  -->
<!-- dt.cat_centres.flt <- dt.cat_centres.dm[tip_eap %in% c('A', 'M'),] -->

<!-- saveRDS(dt.cat_centres.flt, "../data/dt.cat_centres.flt.rds") -->
<!-- ``` -->

<!-- ```{r} -->
<!-- #rm(list = c("")) -->
<!-- gc.var <- gc() -->
<!-- ``` -->

