# coding: utf8

"""
Procés per crear la base de dades a la carpeta permanent per a anàlisi per a Lídia segura sobre abús de substàncies.
Quines variables afecten a l'abús de substàncies??
"""

anys = [2017, 2018]

import collections as c
import datetime as d

import sisapUtils as u

db = 'permanent'
tb = 'mst_abus_substancies'

abusos_subst= "('P15','P16','P19.1','P19.2','P19.3','P19.4','P19.5','P19.0','P18','P18.1','P19')"

class abus_substancies(object):
    """."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_abus_substancies()
        self.get_poblacio()
        self.upload_data()        
        
    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: br for (up, br) in u.getAll(*sql)}
        
    def get_abus_substancies(self):
        """Obtenir els codis cim10 sobre abús de substàncies"""
        
        pabus, cim10_to_ciap = {}, {}
        sql = "select codi_cim10, left(codi_ciap_M,3) from import.cat_md_ct_cim10_ciap where codi_ciap_m in {}".format(abusos_subst)
        for cim10, ciap in u.getAll(sql, "import"):
            pabus[cim10] = True
            cim10_to_ciap[cim10] = {'ciap': ciap}
            
        in_crit = tuple(pabus)
        self.pabussubst = c.defaultdict(set)
        sql = ("select id_cip_sec, pr_cod_ps from problemes,  nodrizas.dextraccio \
                where  pr_cod_ps in {} \
                   and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext) and (pr_dba = 0 or pr_dba > data_ext)".format(in_crit),
               "import")
        for id, ps in u.getAll(*sql):
            ciap2 = cim10_to_ciap[ps]['ciap']
            self.pabussubst[id].add(ciap2)

    def get_poblacio(self):
        """Obtenim el % de cadascun dels abusos de cada EAP, en població adulta"""
        self.dades = c.Counter()
        sql= "select id_cip_sec, up, sexe, edat, ates from assignada_tot where edat > 14"
        for id, up, sexe, edat, ates in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                br = self.centres[up]
                edat_k = u.ageConverter(edat)
                sexe_k = u.sexConverter(sexe)
                self.dades[(up, br, ates, edat_k, sexe_k, 'den')]+= 1
                if id in self.pabussubst:
                    for ciap2 in self.pabussubst[id]:
                        ciap2 = 'P15' if ciap2 == 'P16' else ciap2
                        self.dades[(up, br, ates, edat_k, sexe_k, ciap2)]+= 1
    
    def upload_data(self):
        """."""
        upload = []
        for (up, br, ates, edat, sexe, tipus), n in self.dades.items():
            if tipus == 'den':
                upload.append([up, br, ates, edat, sexe, n, self.dades[(up, br,  ates, edat, sexe,'P15')] if (up, br,  ates, edat, sexe,'P15') in self.dades else 0, 
                self.dades[(up, br,  ates, edat, sexe,'P18')] if (up, br,  ates, edat, sexe,'P18') in self.dades else 0, self.dades[(up, br,  ates, edat, sexe,'P19')] if (up, br,  ates, edat, sexe,'P19') in self.dades else 0])
        u.listToTable(upload, tb, db)
                

if __name__ == "__main__":
    u.printTime("Inici")

    columns =  ["up varchar(5)", "br varchar(5)", "ates int", "edat varchar(10)", "sexe varchar(5)", "denominador int", "p15 double", "p18 double", "p19 double"]     
    u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
    
    abus_substancies()
    
    u.printTime("Fi")