---
title: "Anàlisi de les cerques i programacions de la pantalla de PxM"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
crossref:
  fig-title: "**Figure**"
output:
  bookdown::html_document2:
    toc: yes
    toc_float: false
    toc_depth: 6
    body_placement: left 
  bookdown::pdf_document2:
    toc: yes
    toc_float: false
    toc_depth: 3
always_allow_html: yes
---

<style>
body {
  position: absolute;
  max-width: 100%;
  left: 2px;
  text-align: justify}
</style>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
options(knitr.kable.na = '')
```

```{r library, child="library.Rmd", eval=TRUE}

```

```{r import.data, results='asis'}
data <- readRDS('../Data/cerques_PxM_20240201_20240331_clean.rds')

cat('Període importat:\n From:', format(as.POSIXct(min(data$DATA_ACCES), "%Y-%m-%d %H:%M:%S")), '- To:', format(as.POSIXct(max(data$DATA_ACCES), "%Y-%m-%d %H:%M:%S")))
```

# Cerques sense selecció de motiu
```{r cerques.no.seleccio}
# Cerques fetes on no s'ha programat res:

dt <- data[!is.na(MOTIU_ESCRIT) & is.na(MOTIU_DESC)]
dt <- dt[, c('DATA_ACCES', 'SECTOR_ECAP', 'MOTIU_ESCRIT', 'VISITA_NAVEGA')]

dt <- dt[, .('N' = .N, 'Navega' = sum(VISITA_NAVEGA=='S', na.rm = TRUE)), by=MOTIU_ESCRIT][, `Navega%` := round(Navega/N*100,2)]
setorder(dt, -N)

datatable(dt, filter = 'top', options = list(
  pageLength = 10, autoWidth = TRUE
))
```

# Seleccions de motiu sense programació
```{r seleccio.no.programacio}
# Motius seleccionats i no programats

dt <- data[!is.na(MOTIU_DESC)]
dt <- dt[, c('DATA_ACCES', 'SECTOR_ECAP', 'MOTIU_ESCRIT', 'MOTIU_CODI', 'MOTIU_DESC', 'VISITA_PROGRAMA', 'ES_PROGRAMA_PXM')]

dt <- dt[, .('N' = .N, 'Programa' = sum(VISITA_PROGRAMA=='N', na.rm = TRUE), 'Programa_PxM' = sum(ES_PROGRAMA_PXM=='1')), by=c('MOTIU_CODI', 'MOTIU_DESC')][, c('Programa%', 'Programa_PxM%') := .(round(Programa/N*100,2),round(Programa_PxM/N*100,2))]
setorder(dt, -N)

datatable(dt, filter = 'top', options = list(
  pageLength = 10, autoWidth = TRUE
))
```

# Programació segons flux horari
```{r programacio.flux}
# Motius programats dins i fora flux

dt <- data[!is.na(ID_VISITA) & !is.na(MOTIU_DESC)]

dt <- dt[, .('N' = .N, 
             'Dintre_Flux' = sum(flux_horari==1, na.rm = TRUE), 
             'Programa_PxM' = sum(ES_PROGRAMA_PXM=='1'), 
             'Programa_PxM_Dintre_Flux' = sum(flux_horari[ES_PROGRAMA_PXM=='1'], na.rm=TRUE)), by=c('MOTIU_CODI', 'MOTIU_DESC', 'SERVEI')][, c('Dintre_Flux%', 'Programa_PxM%', 'Programa_PxM_Dintre_Flux%') := .(
          round(Dintre_Flux/N*100,2),
          round(Programa_PxM/N*100,2), 
          round(Programa_PxM_Dintre_Flux/N*100,2))]
setorder(dt, -N)

dt[, SERVEI := as.factor(SERVEI)]

datatable(dt, filter = 'top', colnames = c('Motiu_Codi', 'Motiu_Desc', 'Servei', 'N', 'Dintre_Flux', 'Programa_PxM', 'Programa_PxM & \nDintre_Flux', '% \nDintre_Flux', '% \nPrograma_PxM', '% \nPrograma_PxM & \nDintre_Flux'),
          options = list( pageLength = 10, autoWidth = TRUE))
```

# Programació segons flux professional
```{r programacio.professional}
# Motius programats dins i fora flux

dt <- data[!is.na(ID_VISITA) & !is.na(MOTIU_DESC)]

dt <- dt[, .('N' = .N, 
             'Dintre_Flux' = sum(flux_prof==1, na.rm = TRUE), 
             'Programa_PxM' = sum(ES_PROGRAMA_PXM=='1'), 
             'Programa_PxM_FluxProf' = sum(flux_prof[ES_PROGRAMA_PXM=='1'], na.rm=TRUE),
             'Programa_ECAP_FluxProf' = sum(flux_prof[ES_PROGRAMA_PXM=='0'], na.rm=TRUE)), by=c('MOTIU_CODI', 'MOTIU_DESC', 'SERVEI')][, c('Dintre_Flux%', 'Programa_PxM%', 'Programa_PxM_FluxProf%', 'Programa_ECAP_FluxProf%') := .(
          round(Dintre_Flux/N*100,2),
          round(Programa_PxM/N*100,2), 
          round(Programa_PxM_FluxProf/N*100,2),
          round(Programa_ECAP_FluxProf/N*100,2))]
setorder(dt, -N)

dt[, SERVEI := as.factor(SERVEI)]

datatable(dt, filter = 'top', colnames = c('Motiu_Codi', 'Motiu_Desc', 'Servei', 'N', 'Dintre_Flux', 'Programa_PxM', 'Programa_PxM & \nFluxProf', 'Programa_ECAP & \nFluxProf', '% \nDintre_Flux', '% \nPrograma_PxM', '% \nPrograma_PxM & \nFluxProf', '% \nPrograma_ECAP & \nFluxProf'),
          options = list( pageLength = 10, autoWidth = TRUE))
```


# Programació segons flux tipus visita
```{r programacio.tipvisi}
# Motius programats dins i fora flux

dt <- data[!is.na(ID_VISITA) & !is.na(MOTIU_DESC)]

dt <- dt[, .('N' = .N, 
             'Programa_PxM%' = round(sum(ES_PROGRAMA_PXM=='1')/.N*100,2), 
             'Dintre_FluxTipVisi' = sum(flux_tipvisi==1, na.rm = TRUE), 
             'Programa_PxM_FluxTipVisi' = sum(flux_tipvisi[ES_PROGRAMA_PXM=='1'], na.rm=TRUE),
             'Programa_ECAP_FluxTipVisi' = sum(flux_tipvisi[ES_PROGRAMA_PXM=='0'], na.rm=TRUE)), by=c('MOTIU_CODI', 'MOTIU_DESC', 'PRESTACIO')][, c('Dintre_Flux%', 'Programa_PxM_FluxTipVisi%', 'Programa_ECAP_FluxTipVisi%') := .(
          round(Dintre_FluxTipVisi/N*100,2),
          round(Programa_PxM_FluxTipVisi/N*100,2),
          round(Programa_ECAP_FluxTipVisi/N*100,2))]
setorder(dt, -N)

dt[, PRESTACIO := as.factor(PRESTACIO)]

datatable(dt, filter = 'top', colnames = c('Motiu_Codi', 'Motiu_Desc', 'Prestació', 'N', '% \nPrograma_PxM', 'Dintre_FluxTipVisi', 'Programa_PxM & \nFluxTipVisi', 'Programa_ECAP & \nFluxTipVisi', '% \nDintre_FluxTipVisi', '% \nPrograma_PxM & \nFluxTipVisi', '% \nPrograma_ECAP & \nFluxTipVisi'),
          options = list( pageLength = 10, autoWidth = TRUE))
```

```{r}
# Errors:
dt <- data[flux_tipvisi==0][, .N, by=c('MOTIU_CODI', 'MOTIU_DESC', 'PRESTACIO', 'TIPUS_CLASS')][, '%N':= round(N/sum(N)*100,2), by='MOTIU_DESC']

setorder(dt, -N)

datatable(dt, filter = 'top', colnames = c('MOTIU_CODI', 'MOTIU_DESC', 'Prestacio_Flux', 'Prestacio_Programada', 'N', '%N'),
          options = list(pageLength = 10, autoWidth = TRUE))
```

# Programació segons flux - QUI COM QUAN
```{r programacio.quicomquan}
# Motius programats dins i fora flux

dt <- data[!is.na(ID_VISITA) & !is.na(MOTIU_DESC)]

dt <- dt[, .(.N, 'Qui' = sum(flux_prof, na.rm=TRUE), 'Com' = sum(flux_tipvisi, na.rm=TRUE), 'Quan' = sum(flux_horari, na.rm=TRUE), 'QuiComQuan'= sum(flux_horari[flux_tipvisi == 1 & flux_prof == 1], na.rm=TRUE),
       'Qui_PxM' = sum(flux_prof[ES_PROGRAMA_PXM=='1'], na.rm=TRUE), 'Com_Pxm' = sum(flux_tipvisi[ES_PROGRAMA_PXM=='1'], na.rm=TRUE), 'Quan_Pxm' = sum(flux_horari[ES_PROGRAMA_PXM=='1'], na.rm=TRUE), 'QuiComQuan_PxM'= sum(flux_horari[flux_tipvisi == 1 & flux_prof == 1 & ES_PROGRAMA_PXM=='1'], na.rm=TRUE)), by=c('MOTIU_CODI', 'MOTIU_DESC')]

dt2 <- dt[, .('Qui%' = round(Qui/N*100, 2), 'Com%' = round(Com/N*100, 2), 'Quan%' = round(Quan/N*100, 2), 'QuiComQuan%' = round(QuiComQuan/N*100, 2),
       'Qui_PxM%' = round(Qui_PxM/N*100, 2), 'Com_Pxm%' = round(Com_Pxm/N*100, 2), 'Quan_Pxm%' = round(Quan_Pxm/N*100, 2), 'QuiComQuan_PxM%' = round(QuiComQuan_PxM/N*100, 2)), by=c('MOTIU_CODI', 'MOTIU_DESC')]

setorder(dt, -N)

datatable(dt, filter = 'top',
          options = list( pageLength = 10, autoWidth = TRUE))

datatable(dt2, filter = 'top',
          options = list( pageLength = 10, autoWidth = TRUE))
```





