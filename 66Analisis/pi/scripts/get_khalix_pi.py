# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

path = "D:/SISAP/sisap/66Analisis/pi/data/"

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

pi_a2022_homes = exemple(c, 'ORCLINIC###;AYR22###;AMBITOS###;CLINIQUES#2;EDATS5###;DERORIGEN###;HOME')
pi_a2022_dones = exemple(c, 'ORCLINIC###;AYR22###;AMBITOS###;CLINIQUES#2;EDATS5###;DERORIGEN###;DONA')

pi_a2021_homes = exemple(c, 'ORCLINIC###;AYR21###;AMBITOS###;CLINIQUES#2;EDATS5###;DERORIGEN###;HOME')
pi_a2021_dones = exemple(c, 'ORCLINIC###;AYR21###;AMBITOS###;CLINIQUES#2;EDATS5###;DERORIGEN###;DONA')

pi_a2020_homes = exemple(c, 'ORCLINIC###;AYR20###;AMBITOS###;CLINIQUES#2;EDATS5###;DERORIGEN###;HOME')
pi_a2020_dones = exemple(c, 'ORCLINIC###;AYR20###;AMBITOS###;CLINIQUES#2;EDATS5###;DERORIGEN###;DONA')

pi_a2019_homes = exemple(c, 'ORCLINIC###;AYR19###;AMBITOS###;CLINIQUES#2;EDATS5###;DERORIGEN###;HOME')
pi_a2019_dones = exemple(c, 'ORCLINIC###;AYR19###;AMBITOS###;CLINIQUES#2;EDATS5###;DERORIGEN###;DONA')

c.close()

print('export')

# 2022
file = path + "pi_2022_homes.txt"
with open(file, 'w') as f:
   f.write(pi_a2022_homes)

file = path + "pi_2022_dones.txt"
with open(file, 'w') as f:
   f.write(pi_a2022_dones)

# 2021
file = path + "pi_2021_homes.txt"
with open(file, 'w') as f:
   f.write(pi_a2021_homes)

file = path + "pi_2021_dones.txt"
with open(file, 'w') as f:
   f.write(pi_a2021_dones)

# 2020
file = path + "pi_2020_homes.txt"
with open(file, 'w') as f:
   f.write(pi_a2020_homes)

file = path + "pi_2020_dones.txt"
with open(file, 'w') as f:
   f.write(pi_a2020_dones)

# 2019
file = path + "pi_2019_homes.txt"
with open(file, 'w') as f:
   f.write(pi_a2019_homes)

file = path + "pi_2019_dones.txt"
with open(file, 'w') as f:
   f.write(pi_a2019_dones)   