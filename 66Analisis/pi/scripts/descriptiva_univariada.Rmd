***
***

# Descriptiva Univariada

## PI

Taula resum

```{r, warning=warning.var}

df <- dt.pi.flt
res <- compareGroups(any ~ ep + codi_origen + cost + codi_prova
                     , df
                     , method = 1
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = TRUE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "")
```

***

```{r}
ggplot(dt.pi.cost.dm, aes(x=cost)) +
              geom_histogram(, fill="darkgreen", color="white") +
              theme_minimal()
```

***
***

## MAPA

### Resultat

```{r, eval=eval.fig.var}
gg1 <- ggplot(dt.mapa.flt, aes(x=resultat)) +
              geom_histogram(, fill="darkgreen", color="white") +
              theme_minimal()

gg2 <- ggplot(dt.mapa.flt, aes(x=resultat)) +
              geom_boxplot(fill="darkgreen") +
              theme_minimal()

grid.arrange(gg1, gg2)
```

***

## Punts

```{r, eval=eval.fig.var}
gg1 <- ggplot(dt.mapa.flt, aes(x=punts)) +
              geom_histogram(, fill="darkgreen", color="white") +
              theme_minimal()

gg2 <- ggplot(dt.mapa.flt, aes(x=punts)) +
              geom_boxplot(fill="darkgreen") +
              theme_minimal()

grid.arrange(gg1, gg2)
```

***

## Punts Màxim

```{r, eval=eval.fig.var}
gg1 <- ggplot(dt.mapa.flt, aes(x=punts_max.x)) +
              geom_histogram(, fill="darkgreen", color="white") +
              theme_minimal()

gg2 <- ggplot(dt.mapa.flt, aes(x=punts_max.x)) +
              geom_boxplot(fill="darkgreen") +
              theme_minimal()

grid.arrange(gg1, gg2)
```

***

## Número indicadors

Número indicadors dins de les etiquetes

```{r, eval=eval.fig.var}
gg1 <- ggplot(dt.mapa.flt, aes(x=num_indicadors.x)) +
              geom_histogram(, fill="darkgreen", color="white") +
              theme_minimal()

gg2 <- ggplot(dt.mapa.flt, aes(x=num_indicadors.x)) +
              geom_boxplot(fill="darkgreen") +
              theme_minimal()

grid.arrange(gg1, gg2)
```

***

## COST

### PI

#### Taula Resum

```{r}
df <- dt.pi.costnew.raw
df <- df %>% mutate(taula="Cost Producte Intermedi")
cG <- compareGroups(taula ~ .,
                    df,
                    )
cT <- createTable(cG,
                  show.p.overall = FALSE)
export2md(cT,
          format = "html",
          caption = "Cost Producte Intermedi")
```

#### Variables

##### Número de proves

```{r}
ggplot(dt.pi.costnew.raw,
       aes(x=n_proves)) +
  geom_histogram(, fill="dark green", color="white") +
  theme_minimal() +
  labs(caption = "Sistemes d'informació dels Serveis d'Atenció Primària (SISAP)")
```

##### Cost ECOFIN

```{r}
ggplot(dt.pi.costnew.raw,
       aes(x=cost_prova_ecofin)) +
  geom_histogram(, fill="dark green", color="white") +
  theme_minimal() +
  labs(caption = "Sistemes d'informació dels Serveis d'Atenció Primària (SISAP)")
```

##### Cost PPII

```{r}
ggplot(dt.pi.costnew.raw,
       aes(x=cost_prova_ppii)) +
  geom_histogram(, fill="dark green", color="white") +
  theme_minimal() +
  labs(caption = "Sistemes d'informació dels Serveis d'Atenció Primària (SISAP)")
```

***

### LABORATORI

#### Taula Resum

```{r}
df <- dt.pi.lab.cost.raw
df <- df %>% mutate(taula="Cost Laboratori 2021")
cG <- compareGroups(taula ~ .,
                    df,
                    )
cT <- createTable(cG,
                  show.p.overall = FALSE)
export2md(cT,
          format = "html",
          caption = "Cost Laboratori 2021")

cG <- compareGroups(laboratori_tipus ~ . -taula,
                    df,
                    )
cT <- createTable(cG,
                  show.p.overall = FALSE)
export2md(cT,
          format = "html",
          caption = "Cost Laboratori 2021")
```
, aes()
#### Variables

##### Número de proves

```{r}
ggplot(dt.pi.lab.cost.raw,
       aes(x=n_proves)) +
  geom_histogram(, fill="dark green", color="white") +
  theme_minimal() +
  labs(caption = "Sistemes d'informació dels Serveis d'Atenció Primària (SISAP)")

ggplot(dt.pi.lab.cost.raw,
       aes(x=n_proves)) +
  geom_histogram(, fill="dark green", color="white") +
  facet_grid(laboratori_tipus ~ ., scale="free") + 
  theme_minimal() +
  labs(caption = "Sistemes d'informació dels Serveis d'Atenció Primària (SISAP)")
```

##### Cost

```{r}
ggplot(dt.pi.lab.cost.raw,
       aes(x=cost_proves)) +
  geom_histogram(, fill="dark green", color="white") +
  theme_minimal() +
  labs(caption = "Sistemes d'informació dels Serveis d'Atenció Primària (SISAP)")

ggplot(dt.pi.lab.cost.raw,
       aes(x=cost_proves)) +
  geom_histogram(, fill="dark green", color="white") +
  facet_grid(laboratori_tipus ~ ., scale="free") + 
  theme_minimal() +
  labs(caption = "Sistemes d'informació dels Serveis d'Atenció Primària (SISAP)")
```

##### Unitats Relatives de Valor (URV)

```{r}
ggplot(dt.pi.lab.cost.raw,
       aes(x=urv_proves)) +
  geom_histogram(, fill="dark green", color="white") + 
  theme_minimal() +
  labs(caption = "Sistemes d'informació dels Serveis d'Atenció Primària (SISAP)")

ggplot(dt.pi.lab.cost.raw,
       aes(x=urv_proves)) +
  geom_histogram(, fill="dark green", color="white") +
  facet_grid(laboratori_tipus ~ ., scale="free") + 
  theme_minimal() +
  labs(caption = "Sistemes d'informació dels Serveis d'Atenció Primària (SISAP)")
```


```{r}
gc.var <- gc()
```


