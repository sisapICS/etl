***
***
***


# Resultats

## Nivell 0

### Global

```{r}
dt.pi.ana <- dt.pi.flt[codi_prova_nchar==9 & time %in% c('A2102','A2202'),]
dt.pi.ana.long <- dt.pi.ana[,.(n=sum(n)), .(time, codi_origen, codi_prova)]
dt.pi.ana.wide <- dcast.data.table(dt.pi.ana.long, codi_prova + codi_origen~ time, value.var = "n")
datatable(dt.pi.ana.wide)

dt.pi.ana <- dt.pi.flt[codi_prova_nchar==9,]
dt.pi.ana.long <- dt.pi.ana[,.(n=sum(n)), .(time, codi_prova)]
dt.pi.ana.wide <- dcast.data.table(dt.pi.ana.long, codi_prova ~ time, value.var = "n")
datatable(dt.pi.ana.wide)

ggplot(dt.pi.ana.long,
       aes(x=time, y=n, group=codi_prova)) +
       geom_line() +
       geom_point()
```

## Nivell 2

```{r}
dt.pi.ana <- dt.pi.flt[codi_prova_nchar==5 & time %in% c('A2102','A2202'),]
dt.pi.ana.long <- dt.pi.ana[,.(n=sum(n)), .(time, codi_prova)]
dt.pi.ana.wide <- dcast.data.table(dt.pi.ana.long, codi_prova ~ time, value.var = "n")
datatable(dt.pi.ana.wide)

dt.pi.ana <- dt.pi.flt[codi_prova_nchar==5, ]
dt.pi.ana.long <- dt.pi.ana[,.(n=sum(n)), .(time, codi_prova)]
dt.pi.ana.wide <- dcast.data.table(dt.pi.ana.long, codi_prova ~ time, value.var = "n")
datatable(dt.pi.ana.wide)

ggplot(dt.pi.ana.long,
       aes(x=time, y=n, group=codi_prova)) +
       geom_line() +
       geom_point()
```


```{r}
gc.var <- gc()
```

