***
***

# Flowchart

```{r}
#dt.dbs.dm <- readRDS("../data/dt.dm.rds")
#dt.dbs.flt <- dt.dbs.dm
```

Diagrama amb els criteris d'inclusió i excluisió de l'estudi.

```{r}
#ini <- Sys.time()
n_reg <- nrow(dt.dm)
```

```{r,}
fin <- c(seq(1,20,1))

grViz("
      digraph a_nice_graph
      {
      
      node[fontname = Helvetica,
           fontcolor = black,
           shape = box,
           width = 1,
           style = filled,
           fillcolor = whitesmoke]
      
      '@@1'
      }

      [1]: paste0('DEMORA - Registres N=', n_reg, '\\n')
      ", height = 500, width = 500)
```

***

```{r filtre}

#filtre = 
dt.flt <- dt.dm
```

```{r}
#rm(list = c("dt.dbs.dm"))
gc.var <- gc()
```

