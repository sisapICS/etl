---
title: "Demora"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
params:
  actualitzar_dades: FALSE
---

### Paràmetres informe

Sense paràmetres

***

### Actualització informe

- 2022-11-21 Inici informe

***
***

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```


```{r parametres markdown}
inici <- Sys.time()

nfetch <- -1
rowid.var <- FALSE
savehashrds.var <- FALSE
saverds.var <- FALSE

warning.var <- FALSE
message.var <- FALSE

eval.library.var <- TRUE
eval.functions.var <- FALSE
eval.import.var <- TRUE
eval.datamanager.var <- TRUE
# eval.cq.var <- FALSE
eval.flowchart.var <- TRUE
# 
eval.univariada.var <- TRUE
eval.bivariada.var <- TRUE
# 
# eval.seguiment.var <- FALSE
# 
# eval.evaluacio.var <- FALSE
# 
eval.annex.var <- FALSE

eval.timeexecution.var <- TRUE

var.fig.height <- 4
var.fig.width <- 6
```

```{r parametres estudi}

```

```{r library, child="02_library.Rmd", eval=eval.library.var}

```

```{r functions, child="functions.Rmd", eval=eval.functions.var}

```

```{r import, child="04_import.Rmd", eval=eval.import.var}

```

```{r data manager, child="05_datamanager.Rmd", eval=eval.datamanager.var}

```

```{r flowchart, child="06_flowchart.Rmd", eval=eval.flowchart.var}

```

```{r descriptiva univariada, child="07_descriptiva_univariada.Rmd", eval=eval.univariada.var}

```

```{r descriptiva bivariada, child="08_descriptiva_bivariada.Rmd", eval=eval.bivariada.var}

```

```{r annex, child="annex.Rmd", eval=eval.annex.var}

```

---

```{r time execution, eval=eval.timeexecution.var}

final <- Sys.time()
print(round(final - inici,2))
```
