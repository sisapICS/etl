```{r}
# MEDEA
medea <- fread("C:/Users/nmora/repositori/etl/66Analisis/covid19/neos/dades originals/medea_eap.txt", sep = "@", header = F)
medea[V3 %in% c("0R", "1R", "2R"), V3 := "Rural"]
names(medea)[3] <- "medea"

# Àmbit
sheets <- c("AYR21", "AYR20", "AYR19", "AYR18", "AYR17", "AYR16", "AYR15", "AYR14")
ambit <- do.call("rbind", lapply(sheets, function(sheet){
  file <- data.table(read.xlsx("C:/Users/nmora/repositori/etl/66Analisis/covid19/neos/dades originals/pob amb ambit.xlsx", sheet))
  file <- file[-1]
  file[, Any := paste0("20", substr(sheet, 4, 5))]
  file[, c(1, 4, 5)]
}))
names(ambit) <- c("br", "ambit", "Any")
ambit <- ambit[substr(br, 1, 2) == "BR"]
ambit[, br := as.character(br)]

# Pobass (fins 2020)
pobass_arxius <- c("pobass 2014", "pobass 2015", "pobass 2016", "pobass 2017", "pobass 2018", "pobass 2019", "pobass 2020")
library("openxlsx")
pobass <- do.call("rbind", lapply(pobass_arxius, function(arxiu){
  # Any
  # print(arxiu)
  require(stringr)
  any <- paste0("A", substr(arxiu, 10, 11))
  # Mes
  do.call("rbind", lapply(c("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"), function(mes){
    # print(mes)
    
    # Sexe
    do.call("rbind", lapply(c("Home", "Dona"), function(sexe){
      # print(sexe)
      sheet <- paste0(any, mes, " ", sexe)
      pobass <- tryCatch(data.table(read.xlsx(paste0("C:/Users/nmora/repositori/etl/66Analisis/covid19/neos/dades originals/",arxiu, ".xlsx"), sheet)), error=function(e) NULL)
      
      pobass <- tryCatch(pobass[-1], error=function(e) NULL)
      
      if(!is.null(pobass)){
        pobass[, Any := any]
        pobass[, Mes := mes]
        pobass[, sexe := toupper(sexe)]
        pobass
      }
      
    }))
  }))
}))
pobass_melt <- melt(pobass, id.vars = c("X1", "X2", "Any", "Mes", "sexe"), variable.name = "Edat", value.name = "POBASS")
pobass_melt[, POBASS := as.numeric(as.character(POBASS))]
names(pobass_melt)[c(1, 2)] <- c("br", "br_descriptiu")
pobass_melt[, data := ymd(paste0("20", substr(Any, 2, 3), "-", Mes, "-", "01"))]
pobass_melt[, edat_65 := ifelse(Edat %in%  c(paste0("ED", seq(65, 95, 1)), "ED95M"), "Més de 65 anys", "Menys de 65 anys")]
pobass_melt[Edat %in%  c(paste0("ED", seq(15, 44, 1))), edat_memoria := "15-44"]
pobass_melt[Edat %in%  c(paste0("ED", seq(45, 644, 1))), edat_memoria := "45-64"]
pobass_melt[Edat %in%  c(paste0("ED", seq(65, 74, 1))), edat_memoria := "65-74"]
pobass_melt[Edat %in%  c(paste0("ED", seq(75, 95, 1)), "ED95M"), edat_memoria := "75 i més"]
pobass_melt[, Any := paste0("20", substr(Any, 2, 3))]
pobass_melt_ambit <- merge(pobass_melt, ambit, by = c("br", "Any"), all.y = T)
pobass_melt_ambit_medea <- merge(pobass_melt_ambit, medea[, c("V1", "medea")], by.x = "br", by.y = "V1", all.x = T)
# names(pobass_melt_ambit_medea)[12] <- "medea"
# pobass_melt_ambit_medea <- pobass_melt_ambit_medea[!Edat %in% c("ED0", "ED1", "ED2", "ED3", "ED4", "ED5", "ED6", "ED7", "ED8", "ED9", "ED10", "ED11", "ED12", "ED13", "ED14")]

# Pobass (2021)
pobass_arxius <- c("pob_dona_2021.txt", "pob_home_2021.txt")
pobass <- do.call("rbind", lapply(pobass_arxius, function(arxiu){
  fread(paste0("C:/Users/nmora/repositori/etl/66Analisis/covid19/neos/dades originals/actualitzacio_20211207/neos/neos/", arxiu),
        sep = "{", header = F)
}))
pobass[, data := ymd(paste0("20", substr(V2, 2, 3), "-", substr(V2, 4, 5), "-", "01"))]
pobass[, edat_65 := ifelse(V5 %in%  c(paste0("ED", seq(65, 95, 1)), "ED95M"), "Més de 65 anys", "Menys de 65 anys")]
pobass[V5 %in%  c(paste0("ED", seq(15, 44, 1))), edat_memoria := "15-44"]
pobass[V5 %in%  c(paste0("ED", seq(45, 644, 1))), edat_memoria := "45-64"]
pobass[V5 %in%  c(paste0("ED", seq(65, 74, 1))), edat_memoria := "65-74"]
pobass[V5 %in%  c(paste0("ED", seq(75, 95, 1)), "ED95M"), edat_memoria := "75 i més"]
pobass[, Any := paste0("20", substr(V2, 2, 3))]
pobass <- pobass[, c(2, 3, 5, 7, 8, 9, 10, 11, 12)]
names(pobass)[1:5] <- c("Periode", "br", "Edat", "sexe", "POBASS")
pobass_ambit <- merge(pobass, ambit, by = c("br", "Any"), all.y = T)
pobass_ambit_medea <- merge(pobass_ambit, medea[, c("V1", "medea")], by.x = "br", by.y = "V1", all.x = T)

# names(pobass_ambit_medea)[11] <- "medea"
# pobass_ambit_medea <- pobass_ambit_medea[!Edat %in% c("ED0", "ED1", "ED2", "ED3", "ED4", "ED5", "ED6", "ED7", "ED8", "ED9", "ED10", "ED11", "ED12", "ED13", "ED14")]

# pobass_ambit_medea[, Sexe := factor(sexe, levels = c("HOME", "DONA"), labels = c("Home", "Dona"))]

pobass_ambit_medea <- rbind(pobass_melt_ambit_medea[, .SD, .SDcols = setdiff(names(pobass_ambit_medea), c("Periode"))],
                            pobass_ambit_medea[, .SD, .SDcols = setdiff(names(pobass_ambit_medea), c("Periode"))])
pobass_ambit_medea[, edat_adult_inf := ifelse(Edat %in% c("ED0", "ED1", "ED2", "ED3", "ED4", "ED5", "ED6", "ED7", "ED8", "ED9", "ED10", "ED11", "ED12", "ED13", "ED14"), "Població infantil", "Població adulta")]
pobass_ambit_medea[, rs := ambit]
pobass_ambit_medea[ambit %in% c("Metropolitana Nord", "Barcelona Ciutat", "Costa de Ponent"), rs := "Barcelona"]
pobass_ambit_medea[ambit %in% c("Alt Pirineu"), rs := "Alt Pirineu-Aran"]
pobass_ambit_medea[, Periode := paste0("A", substr(Any, start = 3, stop = 4), "T", quarter(data))]
pobass_ambit_medea_T <- pobass_ambit_medea[, .(
  data = max(data)), by = .(Periode)]
pobass_ambit_medea_T <- merge(pobass_ambit_medea, pobass_ambit_medea_T, all.y = T)
```

```{r}
# Incidència
dx_arxius <- apply(expand.grid("caries", c("dona", "home"), seq(2014, 2021, 1)), 1, paste, collapse="_")
dx_incidencia <- llegir_dades_dx(dx_arxius, ruta = "C:/Users/nmora/repositori/etl/66Analisis/obesitat/dades/caries/caries/")
```

```{r}
dx_incidencia[, rs := ambit]
dx_incidencia[ambit %in% c("Metropolitana Nord", "Barcelona Ciutat", "Costa de Ponent"), rs := "Barcelona"]
dx_incidencia[ambit %in% c("Alt Pirineu"), rs := "Alt Pirineu-Aran"]

dx_incidencia_var <- agrupar_variable(dades  = dx_incidencia, poblacio = pobass_ambit_medea, periode = "data", variable = c("edat_adult_inf"))

dx_incidencia_var_medea <- agrupar_variable_medea(dades  = dx_incidencia, poblacio = pobass_ambit_medea, periode = "data", variable = c("edat_adult_inf"), variable_sec = "medea")

dx_incidencia_var_sexe <- agrupar_variable_medea(dades  = dx_incidencia, poblacio = pobass_ambit_medea, periode = "data", variable = c("edat_adult_inf"), variable_sec = "sexe")
```



```{r}
# Prevalença
dx_arxius <- apply(expand.grid("caries", c("dona", "home"), seq(2014, 2021, 1), c("T1", "T2", "T3", "T4")), 1, paste, collapse="_")
dx_prevalenca <- llegir_dades_dx(dx_arxius, ruta = "C:/Users/nmora/repositori/etl/66Analisis/obesitat/dades/caries/caries/")
```

```{r}
dx_prevalenca[, rs := ambit]
dx_prevalenca[ambit %in% c("Metropolitana Nord", "Barcelona Ciutat", "Costa de Ponent"), rs := "Barcelona"]
dx_prevalenca[ambit %in% c("Alt Pirineu"), rs := "Alt Pirineu-Aran"]

dx_prevalenca_var <- agrupar_variable(dades  = dx_prevalenca, poblacio = pobass_ambit_medea_T, periode = "Periode", variable = c("edat_adult_inf"))

dx_prevalenca_var_medea <- agrupar_variable_medea(dades  = dx_prevalenca, poblacio = pobass_ambit_medea_T, periode = "Periode", variable = c("edat_adult_inf"), variable_sec = "medea")

dx_prevalenca_var_sexe <- agrupar_variable_medea(dades  = dx_prevalenca, poblacio = pobass_ambit_medea_T, periode = "Periode", variable = c("edat_adult_inf"), variable_sec = "sexe")
```



