
```{r}
llegir_dades_dx <- function(dx_arxius, ruta){
  dx <- do.call("rbind", lapply(dx_arxius, function(arxiu){
  fread(paste0(ruta, arxiu, ".txt"),
        sep = "{", header = F)
}))
  dx[, Any := paste0("20", substr(V2, 2, 3))]
  dx <- dx[, c(1, 2, 3, 5, 7, 8, 9)]
  names(dx) <- c("CODI", "Periode", "br", "Edat", "sexe", "value", "Any")
  dx[, data := ymd(paste0(Any, "-", substr(Periode, 4, 5), "-", "01"))]
  dx_ambit <- merge(dx, ambit, by = c("br", "Any"), all.y = T)
  return(dx_ambit[Edat %in% c("EC01", "EC24", "EC59", "EC1014")])
}
```

```{r}
agrupar_variable<- function(dades, poblacio, periode, variable){
  ## Global
  dx_global <- dades[, sum(value, na.rm = T), c("CODI", periode, "Any")]
  pobass_global <- poblacio[, sum(POBASS, na.rm = T), c(periode, "Any")]
  dades_global <- merge(dx_global, pobass_global, by = c(periode, "Any"))
  names(dades_global)[c(4, 5)] <- c("Dx", "POBASS")
  dades_global[, taxa := Dx/POBASS*100000]
  dades_global[, Edat := "Global"]
  dades_global <- dades_global[, .SD, .SDcols = c(periode, "Any", "Edat", "CODI", "Dx", "POBASS", "taxa")]
  names(dades_global) <- c("date", "Year", "Variable", "Dx", "Value", "Population", "Rate")
  
  dades_var <- do.call("rbind",
    lapply(variable, function(x){
    dades_var <- dades[, sum(value, na.rm = T), c("CODI", periode, "Any", x)]
    pobass_var <- poblacio[, sum(POBASS, na.rm = T), c(periode, "Any", x)]
    dades_var_pobass <- merge(dades_var, pobass_var, by = c(periode, "Any", x))
    names(dades_var_pobass)[c(5, 6)] <- c("Dx", "POBASS")
    dades_var_pobass[, taxa := Dx/POBASS*100000]
    dades_var_pobass <- dades_var_pobass[, .SD, .SDcols = c(periode, "Any", x, "CODI", "Dx", "POBASS", "taxa")]
    names(dades_var_pobass) <- c("date", "Year", "Variable", "Dx", "Value", "Population", "Rate")
    dades_var_pobass
  }))
  
  dades_final <- rbind(dades_global, dades_var)
  return(dades_final)
  
}
```

```{r}
agrupar_variable_medea  <- function(dades, poblacio, periode, variable, variable_sec){
  dades_var <- do.call("rbind",
    lapply(variable, function(x){
      dades_medea <- merge(dades, medea, by.x = "br", by.y = "V1", all.x = T)
      dades_var <- dades_medea[, sum(value, na.rm = T), c("CODI", periode, "Any", x, variable_sec)]
      pobass_var <- poblacio[, sum(POBASS, na.rm = T), c(periode, "Any", x, variable_sec)]
      dades_var_pobass <- merge(dades_var, pobass_var, by = c(periode, "Any", x, variable_sec))
      names(dades_var_pobass)[c(6, 7)] <- c("Dx", "POBASS")
      dades_var_pobass[, taxa := Dx/POBASS*100000]
      dades_var_pobass <- dades_var_pobass[, .SD, .SDcols = c(periode, "Any", x, variable_sec, "CODI", "Dx", "POBASS", "taxa")]
      names(dades_var_pobass) <- c("date", "Year", "Variable", "Variable_sec", "Dx", "Value", "Population", "Rate")
      dades_var_pobass
  }))
  
  dades_final <- dades_var
  return(dades_final)
  
}
```



```{r}
fit_ts_control <- function(d, frequencia){
  d <- d[!is.na(date)]
  dx_ts <- ts(d[order(date)][Year < 2019]$Rate, frequency = frequencia)
  dxfit <- tslm(dx_ts ~ trend + season)
  pred <- forecast(dxfit, h =  length(unique(d[Year > 2018, date])), level = c(80, 90, 95))
  dt_predict <- as.data.table(pred)
  dt_predict[, date :=  unique(d[Year > 2018, date])]
  dt_predict[, Variable := unique(d$Variable)]
  dt_predict_observed <- merge(dt_predict, d[, c("date", "Variable", "Value", "Population", "Rate")], by = c("date", "Variable"))
    dt_predict_observed[, Dx := unique(d$Dx)]

  dt_predict_observed
}
```

```{r}
fit_ts <- function(d, frequencia){
  d <- d[!is.na(date)]
  dx_ts <- ts(as.ts(d[order(date)][Year < 2020]$Rate), frequency = frequencia)
  dxfit <- tslm(dx_ts ~ trend + season)
  pred <- forecast(dxfit, h =  length(unique(d[Year > 2019, date])), level = c(80, 90, 95))
  dt_predict <- as.data.table(pred)
  dt_predict[, date :=  unique(d[Year > 2019, date])]
  dt_predict[, Variable := unique(d$Variable)]
  dt_predict_observed <- merge(dt_predict, d[, c("date", "Variable", "Value", "Population", "Rate")], by = c("date", "Variable"))
  dt_predict_observed[, Dx := unique(d$Dx)]
  dt_predict_observed
}
```

```{r fig.height=8, fig.width=8}
generate_plot <- function(d){
  ggplot(d, aes(x = date)) +
  geom_line(aes(y = Rate, linetype = "Incidència observada", color = "Incidència observada")) +
  geom_point(aes(y = Rate)) +
  geom_line(aes(y = `Point Forecast`, linetype = "Incidència esperada", color = "Incidència esperada")) +
  geom_ribbon(aes(ymin = `Lo 95`, ymax = `Hi 95`, fill = "IC95% incidència esperada"), alpha = .2) +
  theme_minimal() +
  labs(x = "", y = "Incidència mensula de l'obesitat", linetype = "", color = "", fill = "") +
  scale_linetype_manual(values = c("Incidència esperada" = 2, "Incidència observada" = 1)) +
  scale_color_manual(values = c("Incidència esperada" = "darkgreen", "Incidència observada" = "black")) +
  scale_fill_manual(values = c("IC95% incidència esperada" = "darkgreen")) +
  scale_x_date(breaks = "3 month", date_labels = "%Y-%b") +
  theme(axis.text.x = element_text(angle = 90, vjust = .5),
        legend.position = "bottom") +
  guides(colour = guide_legend(ncol = 1)) +
  facet_wrap(~ Variable, scales = "free_y", nrow = 2)}

```

