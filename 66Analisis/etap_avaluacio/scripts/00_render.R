
library('rmarkdown')
library('compareGroups')

render("01_etap_avaluacio.Rmd", 
       output_format = "html_document",
       output_file = paste('ETAP_Avaluacio_Representativitat',
                           format(Sys.Date(),'%Y%m%d'),
                           format(Sys.time(),'%H%M'), sep = "_"),
       output_dir = "../results/",
       params = list(taules = c('DWSISAP.ETAP_ENQUESTA'),
                     actualitzar_dades = TRUE,
                     skim = TRUE)
)
gc()