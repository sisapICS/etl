```{r eval=FALSE, include=FALSE}
drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password="",host = "10.80.217.68", 
                 # port=3309,
                 port=3307,
                 dbname="permanent")
nfetch <- -1
query <- dbSendQuery(con,
                     statement = "select * from permanent.cat_centres")
cat_centres <- data.table(fetch(query, n = nfetch))
# query <- dbSendQuery(con,
#                      statement = "select * from permanent.sivic_multitests")
# sivic_multitests <- data.table(fetch(query, n = nfetch))
query <- dbSendQuery(con,
                     statement = "select * from permanent.sisap_bronquiolitis")
sisap_bronquiolitis <- data.table(fetch(query, n = nfetch))

dbDisconnect(con)
```

```{r eval=FALSE, include=FALSE}
saveRDS(cat_centres, "../dades/cat_cetres.RDS")
saveRDS(sivic_multitests, "../dades/sivic_multitests.RDS")
saveRDS(sisap_bronquiolitis, "../dades/sisap_bronquiolitis.RDS")
```

```{r}
require("RJDBC")
options(java.parameters = "-Xmx16g")
server<-"excdox-scan.cpd4.intranet.gencat.cat"
port<-"1522"
sid<-"excdox01srv"
# Create connection driver and open connection
jdbcDriver <- JDBC(driverClass="oracle.jdbc.OracleDriver", classPath="C:/Users/nmora/Documents/ojdbc8.jar")
jdbcStr = paste("jdbc:oracle:thin:@", server, ":", port, "/", sid, sep="")
jdbcConnection = dbConnect(jdbcDriver, jdbcStr, "dwecoma", "ranagustavo2020")



# Query on the Oracle instance name.
poblacions <- as.data.table(dbGetQuery(jdbcConnection,"SELECT C_ANY_ASSEGURAT, C_DATA_NAIXEMENT FROM dwcatsalut.rca_pob_oficial
WHERE C_ANY_ASSEGURAT IN ('2021','2022','2023')"))
diagnostics_sivic <- as.data.table(dbGetQuery(jdbcConnection,"SELECT * FROM DWSISAP.SIVIC_ECAP_DIAGNOSTIC WHERE DIAGNOSTIC = 'Bronquiolitis'"))

sivic_multitest <- as.data.table(dbGetQuery(jdbcConnection,"SELECT data_naixement, sexe, data, tr_adenovirus, tr_vrs, tr_infla, tr_inflb, tr_sarscov2 from dwsisap.sivic_multitest_master a left JOIN
dwsisap.RCA_CIP_NIA rcn
ON
substr(a.CIP, 0, 13) =substr(rcn.CIP, 0, 13)"))

dbDisconnect(jdbcConnection)
```

```{r}
poblacions[, C_DATA_NAIXEMENT := as.Date(C_DATA_NAIXEMENT)]
poblacions[, data_inici := as.Date(paste0(C_ANY_ASSEGURAT, "-01-01"))]
poblacions[, age_year := trunc((C_DATA_NAIXEMENT %--% data_inici) / years(1))]
poblacions[, age_month := interval(C_DATA_NAIXEMENT, data_inici) %/% months(1)]
poblacions[, age_grup := cut(age_year, breaks = c(0, 1, 3, 5, 10, 15, max(age_year, na.rm = T)), include.lowest = T, right = F, labels = c("0", "1-2 anys", "3-4 anys", "5-9 anys", "10-14 anys", ">14 anys"))]
poblacions[age_grup == "0" & age_month < 6, age_grup := "0-6 mesos"]
poblacions[age_grup == "0" & age_month >= 6, age_grup := "6-12 mesos"]
poblacions[, age_grup := factor(age_grup, levels = c("0-6 mesos", "6-12 mesos", "1-2 anys", "3-4 anys", "5-9 anys", "10-14 anys", ">14 anys"))]
poblacions_aggr <- poblacions[, .N, .(C_ANY_ASSEGURAT, age_grup)]
poblacions_aggr[, C_ANY_ASSEGURAT := as.numeric(C_ANY_ASSEGURAT)]
saveRDS(poblacions_aggr, "../dades/poblacions.RDS")
```


```{r}
dades <- diagnostics_sivic

dades[, data := as.Date(DATA)]
# dades_edat_04 <- rbind(
#   dades[, .SD, .SDcols = c("data", "DIAGNOSTIC", "EDAT_CODI", "EDAT", "SEXE", "COMARCA", "MUNICIPI", "DISTRICTE", "REGIO", "AGA", "ISC", "DIARI")],
#   dades[EDAT_CODI %in% c(0, 1, 2), .(DIARI = sum(DIARI, na.rm = T), 
#         EDAT_CODI = "998",
#         EDAT = "0 a 4"), by = .(data, DIAGNOSTIC, SEXE, COMARCA, MUNICIPI, DISTRICTE, REGIO, AGA, ISC)][, .SD, .SDcols = c("data", "DIAGNOSTIC", "EDAT_CODI", "EDAT", "SEXE", "COMARCA", "MUNICIPI", "DISTRICTE", "REGIO", "AGA", "ISC", "DIARI")]
# )
dades_edat_04 <- dades

dades_ili_tot <- rbind(
  # ILI = ILI + bronquiolitis 
  ## Global
  dades_edat_04[DIAGNOSTIC %in% c("ILI", "Bronquiolitis"), .(RECOMPTE = sum(as.numeric(DIARI), na.rm = T),
            DIAGNOSTIC = "ILI",
            EDAT_CODI = "999",
            EDAT = "GLOBAL",
            REGIO = "GLOBAL",
            ISC = 999), by = data][, c("data", "EDAT_CODI", "EDAT", "REGIO", "ISC", "DIAGNOSTIC", "RECOMPTE")],
  ## Edat
  dades_edat_04[DIAGNOSTIC %in% c("ILI", "Bronquiolitis"), .(RECOMPTE = sum(as.numeric(DIARI), na.rm = T),
            DIAGNOSTIC = "ILI",
            REGIO = "GLOBAL",
            ISC = 999), by = .(data, EDAT_CODI, EDAT)][, c("data", "EDAT_CODI", "EDAT", "REGIO", "ISC", "DIAGNOSTIC", "RECOMPTE")],
  ## Regió
  dades_edat_04[DIAGNOSTIC %in% c("ILI", "Bronquiolitis"), .(RECOMPTE = sum(as.numeric(DIARI), na.rm = T),
            DIAGNOSTIC = "ILI",
            EDAT_CODI = "999",
            EDAT = "GLOBAL",
            ISC = 999), by = .(data, REGIO)][, c("data", "EDAT_CODI", "EDAT", "REGIO", "ISC", "DIAGNOSTIC", "RECOMPTE")],
  ## ISC
  dades_edat_04[DIAGNOSTIC %in% c("ILI", "Bronquiolitis"), .(RECOMPTE = sum(as.numeric(DIARI), na.rm = T),
            DIAGNOSTIC = "ILI",
            EDAT_CODI = "999",
            EDAT = "GLOBAL",
            REGIO = "GLOBAL"), by = .(data, ISC)][, c("data", "EDAT_CODI", "EDAT", "REGIO", "ISC", "DIAGNOSTIC", "RECOMPTE")],
  
  # ILI_TOT = ILI + bronquiolitis + GRIP + COVID_19
  ## Global
  dades_edat_04[DIAGNOSTIC %in% c("ILI", "Bronquiolitis", "Grip", "COVID-19"), .(RECOMPTE = sum(as.numeric(DIARI), na.rm = T),
            DIAGNOSTIC = "ILI_TOT",
            EDAT_CODI = "999",
            EDAT = "GLOBAL",
            REGIO = "GLOBAL",
            ISC = 999), by = data][, c("data", "EDAT_CODI", "EDAT", "REGIO", "ISC", "DIAGNOSTIC", "RECOMPTE")],
  ## Edat
  dades_edat_04[DIAGNOSTIC %in% c("ILI", "Bronquiolitis", "Grip", "COVID-19"), .(RECOMPTE = sum(as.numeric(DIARI), na.rm = T),
            DIAGNOSTIC = "ILI_TOT",
            REGIO = "GLOBAL",
            ISC = 999), by = .(data, EDAT_CODI, EDAT)][, c("data", "EDAT_CODI", "EDAT", "REGIO", "ISC", "DIAGNOSTIC", "RECOMPTE")],
  ## Regió
  dades_edat_04[DIAGNOSTIC %in% c("ILI", "Bronquiolitis", "Grip", "COVID-19"), .(RECOMPTE = sum(as.numeric(DIARI), na.rm = T),
            DIAGNOSTIC = "ILI_TOT",
            EDAT_CODI = "999",
            EDAT = "GLOBAL",
            ISC = 999), by = .(data, REGIO)][, c("data", "EDAT_CODI", "EDAT", "REGIO", "ISC", "DIAGNOSTIC", "RECOMPTE")],
  ## ISC
  dades_edat_04[DIAGNOSTIC %in% c("ILI", "Bronquiolitis", "Grip", "COVID-19"), .(RECOMPTE = sum(as.numeric(DIARI), na.rm = T),
            DIAGNOSTIC = "ILI_TOT",
            EDAT_CODI = "999",
            EDAT = "GLOBAL",
            REGIO = "GLOBAL"), by = .(data, ISC)][, c("data", "EDAT_CODI", "EDAT", "REGIO", "ISC", "DIAGNOSTIC", "RECOMPTE")],
  
  # Altres diagnòstics
  ## Global
  dades_edat_04[DIAGNOSTIC != "ILI", .(RECOMPTE = sum(as.numeric(DIARI), na.rm = T),
            EDAT_CODI = "999",
            EDAT = "GLOBAL",
            REGIO = "GLOBAL",
            ISC = 999), by = .(data, DIAGNOSTIC)][, c("data", "EDAT_CODI", "EDAT", "REGIO", "ISC", "DIAGNOSTIC", "RECOMPTE")],
  ## Edat
  dades_edat_04[DIAGNOSTIC != "ILI", .(
            RECOMPTE = sum(as.numeric(DIARI), na.rm = T),
            REGIO = "GLOBAL",
            ISC = 999), by = .(data, EDAT_CODI, EDAT, DIAGNOSTIC)][, c("data", "EDAT_CODI", "EDAT", "REGIO", "ISC", "DIAGNOSTIC", "RECOMPTE")],
  ## Regió
  dades_edat_04[DIAGNOSTIC != "ILI", .(RECOMPTE = sum(as.numeric(DIARI), na.rm = T),
            EDAT_CODI = "999",
            EDAT = "GLOBAL",
            ISC = 999), by = .(data, REGIO, DIAGNOSTIC)][, c("data", "EDAT_CODI", "EDAT", "REGIO", "ISC", "DIAGNOSTIC", "RECOMPTE")],
  ## ISC
  dades_edat_04[DIAGNOSTIC != "ILI", .(RECOMPTE = sum(as.numeric(DIARI), na.rm = T),
            EDAT_CODI = "999",
            EDAT = "GLOBAL",
            REGIO = "GLOBAL"), by = .(data, ISC, DIAGNOSTIC)][, c("data", "EDAT_CODI", "EDAT", "REGIO", "ISC", "DIAGNOSTIC", "RECOMPTE")]
)


seq_dates <- rbind(
  # Edat
  data.table(expand.grid(
  "data" = seq(min(dades_ili_tot$data), max(dades_ili_tot$data), by="days"), 
  EDAT = unique(dades_ili_tot$EDAT),
  REGIO = "GLOBAL",
  ISC = 999,
  DIAGNOSTIC = unique(dades_ili_tot$DIAGNOSTIC))),
  # Regió
  data.table(expand.grid(
  "data" = seq(min(dades_ili_tot$data), max(dades_ili_tot$data), by="days"), 
  EDAT = "GLOBAL",
  REGIO = unique(dades_ili_tot[REGIO != "GLOBAL"]$REGIO),
  ISC = 999,
  DIAGNOSTIC = unique(dades_ili_tot$DIAGNOSTIC)
  )),
  # ISC
  data.table(expand.grid(
  "data" = seq(min(dades_ili_tot$data), max(dades_ili_tot$data), by="days"), 
  EDAT = "GLOBAL",
  REGIO = "GLOBAL",
  ISC = unique(dades_ili_tot[ISC != 999]$ISC),
  DIAGNOSTIC = unique(dades_ili_tot$DIAGNOSTIC)
  ))
  )

dades_aggr_data_all <- merge(dades_ili_tot[, -"EDAT_CODI"], seq_dates, all = T, by = c("data", "EDAT", "REGIO", "ISC", "DIAGNOSTIC"))
dades_aggr_data_all[is.na(RECOMPTE), RECOMPTE := 0]
dades_aggr_data_all[order(data), RECOMPTE_7 := Reduce(`+`, shift(RECOMPTE, 0:6)), by = c("EDAT", "REGIO", "ISC", "DIAGNOSTIC")]
dades_aggr_data_all[, Temporada := str_c(year(data %m+% months(-8)),"-",year(data %m+% months(4)))]
dades_aggr_data_all[, setmana := factor(isoweek(data), levels = c(35:52, 1:34))]
dades_aggr_data_all[, temporada_actual := ifelse(Temporada == "2023-2024", "Temporada actual", "Temporades històriques")]
dades_aggr_data_all <- merge(dades_aggr_data_all, unique(dades_ili_tot[, c("EDAT", "EDAT_CODI")]), by = "EDAT")

dades_aggr_data_all <- dades_aggr_data_all[DIAGNOSTIC == "Bronquiolitis"]
```


```{r}
sivic_multitest[, naixement := as.Date(DATA_NAIXEMENT)]
sivic_multitest[, data_test := as.Date(DATA)]
sivic_multitest[, data_test_any := year(data_test)]
require("stringr")
sivic_multitest[, Temporada := str_c(year(data_test %m+% months(-8)),"-", year(data_test %m+% months(4)))]
sivic_multitest[, C_ANY_ASSEGURAT := as.numeric(substr(Temporada, 1, 4))]
sivic_multitest[, age_year := trunc((naixement %--% data_test) / years(1))]
sivic_multitest[, age_month := interval(naixement, data_test) %/% months(1)]
sivic_multitest[, age_grup := cut(age_year, breaks = c(0, 1, 3, 5, 10, 15,200), include.lowest = T, right = F, labels = c("0", "1-2 anys", "3-4 anys", "5-9 anys", "10-14 anys", ">14 anys"))]
sivic_multitest[age_grup == "0" & age_month < 6, age_grup := "0-6 mesos"]
sivic_multitest[age_grup == "0" & age_month >= 6, age_grup := "6-12 mesos"]
sivic_multitest[, age_grup := factor(age_grup, levels = c("0-6 mesos", "6-12 mesos", "1-2 anys", "3-4 anys", "5-9 anys", "10-14 anys", ">14 anys"))]
# sivic_multitest[, sexe := factor(sexe, levels = c("Dona", "Home"), labels = c("Femení", "Masculí"))]
sivic_multitest[, c("adeno", "vrs", "gripA", "gripB", "sarscov2") := lapply(.SD, function(x){
  factor(x, levels = c(0, 1), labels = c("Negatiu", "Positiu"))
}), .SDcols = c("TR_ADENOVIRUS", "TR_VRS", "TR_INFLA", "TR_INFLB", "TR_SARSCOV2")]
```

```{r}
dades <- melt(sivic_multitest, id.vars = c("naixement", "age_year", "age_grup", "data_test", "data_test_any", "Temporada", "C_ANY_ASSEGURAT"), measure.vars = c("adeno", "vrs", "gripA", "gripB", "sarscov2"))
```

```{r}
dades <- merge(dades, poblacions, by = c("C_ANY_ASSEGURAT", "age_grup"), all.x = T)
dades[data_test_any == 2024]
dades <- dades[!is.na(N)]
```