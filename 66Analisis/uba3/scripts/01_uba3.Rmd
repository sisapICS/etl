---
title: "UBA3"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
# params:
#    taules: ['PLANIFICAT_AUDIT_TOTAL']
#    catalegs: ['ESTAT_CRONICS_CAT', 'SISAP_COVID_CAT_UP', 'VISITA_CRONICS_CAT','CENTRES_PILOTS']
---

### RESUM

- EL 51% dels logins no tenen EAP a Capitol1, i aquest percentatge depèn de la categoria professional.

- Hi ha un 0,5% (més o menys) sense informació a nom, cognom1 i cognom2, i up_login.

- Hi ha 3 categories professionals: Administratius, Auxiliar administratius i Zeladors.

- Hi han EAP al catàleg de SISAP (N=303) que no estàn al catàleg d'UBA-3 (N=297):

  - BN001 LÍNIA PEDIÀTRICA SANT ANDREU
  - BN002 LÍNIA PEDIÀTRICA RIO DE JANEIRO	
  - BN003 LÍNIA PEDIÀTRICA MARAGALL	
  - BN007 LÍNIA PEDIÀTRICA LA MARINA	
  - BN010 LINIA PEDIÀTRICA SANTS-NUMÀNCIA	
  - BN015 LÍNIA PEDIÀTRICA CAP PARE CLARET	
  - BN017 LÍNIA PEDIÀTRICA REUS
 
Són totes Línies Pediàtriques
 
***
***

### Paràmetres informe

***

### Actualització informe

- 2021-07-11 Inici informe.
- 2021-07-13 Modificacions catàleg.
- 2021-07-15 Nou catàleg (2 tuales).
- 2021-07-18 Eliminar TCAIs.

***
***

### Notes


#### Taules

Dades:

- 


Catàlegs:

- **cat_administratius**: Catàleg de logins dels professionals relacionats amb les UBA3. Clave Primaria: LOGIN. Un professional pot tener n logins.

- **cat_administratius_uas**: Catàleg de UAS dels profesionals relacionat amb les UBA3. Clave Primaria: DNI/UP/UAS.

***
***

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```


```{r parametres markdown}
inici <- Sys.time()

nfetch <- -1
rowid.var <- FALSE
savehashrds.var <- FALSE
saverds.var <- FALSE

warning.var <- FALSE
message.var <- FALSE

eval.library.var <- TRUE
eval.functions.var <- TRUE
eval.import.var <- TRUE
eval.cq.var <- TRUE
eval.datamanager.var <- TRUE
eval.flowchart.var <- TRUE

eval.univariada.var <- FALSE
eval.bivariada.var <- TRUE

eval.annex.var <- FALSE

eval.timeexecution.var <- TRUE

var.fig.height <- 4
var.fig.width <- 6
```

```{r parametres estudi}
```

```{r library, child="02_library.Rmd", eval=eval.library.var}

```

```{r functions, child="03_functions.Rmd", eval=eval.functions.var}

```

```{r import, child="04_import.Rmd", eval=eval.import.var}

```

```{r cq, child="05_cq.Rmd", eval=eval.cq.var}

```

```{r datamanager, child="06_datamanager.Rmd", eval=eval.datamanager.var}

```

```{r flowchart, child="07_flowchart.Rmd", eval=eval.flowchart.var}

```

```{r descriptiva univariada, child="descriptiva_univariada.Rmd", eval=eval.univariada.var}

```

```{r descriptiva bivariada, child="09_bivariada.Rmd", eval=eval.bivariada.var}

```

```{r annex, child="annex.Rmd", eval=eval.annex.var}

```

---

```{r time execution, eval=eval.timeexecution.var}

final <- Sys.time()
print(round(final - inici,2))
```
