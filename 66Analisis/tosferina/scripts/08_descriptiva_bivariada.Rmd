***
***

# Descriptiva Bivariada

## EAP

```{r}
dt <- dt.tosferina.eap.data.agr[year(data) == 2024 & data <= as.Date('2024-02-26','%Y-%m-%d'),]
dt <- dt[, sum(n_tosferina), .(regio_des, scs_codi, ics_desc)]
setnames(dt, 'V1', 'tosferina_n')
dt <- dt[order(-tosferina_n),]
# dt <- merge(dt.cat_centres.dm[tip_eap %in% c('A', 'M'), .(eap_ep, scs_codi, ics_desc)],
#             dt,
#             by.x = 'scs_codi',
#             by.y = 'scs_codi',
#             all.x = TRUE)
# dt[is.na(tosferina_n), tosferina_n := 0]
write_xlsx(dt, "../results/tosferina_eap_2024.xlsx")
sum(dt[, .(tosferina_n)])
```



```{r}
#rm(list=c("dt.biv","df","res","cT"))
gc.var <- gc()
```


