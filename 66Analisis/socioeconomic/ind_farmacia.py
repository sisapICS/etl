# coding: utf8

import collections as c
import datetime as d

import sisapUtils as u


tb = "ind_farmacia"
db = "permanent"


class nivell_renda(object):
    """."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_hash()
        self.get_cip()
        self.get_aportacio()
        self.get_diabetis()
        self.get_pob()
        self.export_data()
        
    def get_hash(self):
        """hash to id_cip"""
        u.printTime("hash")
        self.id_to_hash = {}
        
        sql = """select 
                    id_cip, codi_sector,  hash_d 
                from 
                    u11"""
        for id, sec, hash in u.getAll(sql, 'import'):
            self.id_to_hash[(hash)] = id
    
    def get_cip(self):
        """cip to hash"""
        u.printTime("cip")
        self.cip_to_hash = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.cip_to_hash[cip] = hash
    
    def get_aportacio(self):
        """indicador de farmacia"""
        u.printTime("farmacia")
        self.farma = {}
        sql = "SELECT c_cip, substr(c_cip,0,13), C_IND_FARMACIA, C_SUBIND_FARMACIA, C_IND_FARMACIA_CALC , C_SUBIND_FARMACIA_CALC , C_IND_EXEMPT_TAXA, C_TITOL FROM dwcatsalut.RCA_POB_OFICIAL rpo WHERE c_any_assegurat='2022'"
        for cip14, cip, indf, subindf, indcalc, subindcalc, exemp, titol in u.getAll(sql, 'exadata'):
            nrenda = None
            if indf=="TSI 001":
                nrenda='Exempte'
            elif indf=="TSI 002" and subindf=='00':
                nrenda='Sense recursos'
            elif indf=="TSI 002" and subindf=='01':
                nrenda='<18.000'
            elif indf=="TSI 002" and subindf=='02':
                nrenda='18.000-100.000'
            elif indf=="TSI 003":
                nrenda='<18.000'
            elif indf=="TSI 004":
                nrenda='18.000-100.000'
            elif indf=="TSI 005":
                nrenda='>100.000'
            elif indf=="TSI 006":
                nrenda='Mutues'
            else:
                nrenda=None
            nrenda1 = nrenda
            if nrenda=='Sense recursos':
                nrenda1='<18.000'
            if nrenda=='Mutues':
                nrenda1='18.000-100.000'
            hash = self.cip_to_hash[cip] if cip in self.cip_to_hash else None
            id_cip = self.id_to_hash[(hash)] if (hash) in self.id_to_hash else None
            self.farma[id_cip] = {'ind': indf, 'subind': subindf, 'nrenda': nrenda, 'nrenda1':nrenda1, 'indcalc': indcalc, 'subindcalc': subindcalc, 'exemp': exemp, 'titol':titol}
            
    def get_diabetis(self):
        u.printTime("pob")
        """Diabetis"""
        self.dm2 = {}
        sql = """select id_cip_sec from nodrizas.eqa_problemes where ps=18"""
        for id, in u.getAll(sql, 'nodrizas'):
            self.dm2[id] = True
        
    
    def get_pob(self):
        u.printTime("pob")
        """Agafem poblacio"""
        self.upload = []
        
        sql = """select id_cip, id_cip_sec, edat, sexe, up, ates from assignada_tot"""
        for id, id2,  edat, sexe, up, ates in u.getAll(sql, 'nodrizas'):
            f1 = self.farma[id]['ind'] if id in self.farma else None
            f2 = self.farma[id]['subind'] if id in self.farma else None
            f3 = self.farma[id]['nrenda'] if id in self.farma else None
            f4 = self.farma[id]['nrenda1'] if id in self.farma else None
            f5 = self.farma[id]['indcalc'] if id in self.farma else None
            f6 = self.farma[id]['subindcalc'] if id in self.farma else None
            f7 = self.farma[id]['exemp'] if id in self.farma else None
            f8 = self.farma[id]['titol'] if id in self.farma else None
            dm = 1 if id2 in self.dm2 else 0
            self.upload.append([id2, up, edat, sexe, ates, dm, f1, f2, f3, f4, f5, f6, f7,f8])    
    
    
    def export_data(self):
        """."""  
        u.printTime("export")
        cols = ("id_cip_sec int",  "up varchar(10)", "edat int", "sexe varchar(10)", "ates int", "diabetis2 int",
        "ind_farmacia varchar(100)", "subind_farmacia varchar(100)", "nrenda varchar(300)",  "nrenda1 varchar(300)",
        "ind_farmacia_calc varchar(100)", "subind_farmacia_calc varchar(100)", "ind_exempt_taxa varchar(100)", "titol varchar(100)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        file = u.tempFolder + "ind_farm.txt"
        #u.writeCSV(file, self.upload, sep='@')
        
        
if __name__ == "__main__":
    u.printTime("Inici")
     
    nivell_renda()
    
    u.printTime("Final") 
