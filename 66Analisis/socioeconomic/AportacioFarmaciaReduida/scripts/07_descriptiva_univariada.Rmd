***
***

# Descriptiva Univariada

```{r}
dt.eap.rca.all.flt <- readRDS("../data/dt.eap.rca.all.flt.rds")
```

### Taula Resum

### Indicadors de Farmàcia

```{r}
df <- dt.eap.rca.all.flt
res <- compareGroups(rural ~ ind_afarmred + ind_afarmred_ge15 + ind_afarmred_ge65
                     , df
                     , max.xlev = 100
                     , max.ylev = 5
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = FALSE
                  , show.all = TRUE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "Taula RESUM")
```

```{r}
#rm(list = c("dt.agr"))
gc.var <- gc()
```


