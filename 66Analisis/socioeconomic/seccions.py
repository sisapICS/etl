# coding: utf8

import collections as c
import datetime as d

import sisapUtils as u


tb = "seccio_rca"
db = "permanent"


class seccio_censal(object):
    """."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_hash()
        self.get_cip()
        self.get_aportacio()
        self.get_pob()
        self.export_data()
        
    def get_hash(self):
        """hash to id_cip"""
        u.printTime("hash")
        self.id_to_hash = {}
        
        sql = """select 
                    id_cip, codi_sector,  hash_d 
                from 
                    u11"""
        for id, sec, hash in u.getAll(sql, 'import'):
            self.id_to_hash[(hash)] = id
    
    def get_cip(self):
        """cip to hash"""
        u.printTime("cip")
        self.cip_to_hash = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.cip_to_hash[cip] = hash
    
    def get_aportacio(self):
        """indicador de farmacia"""
        u.printTime("farmacia")
        self.rcapob = {}
        sql = "SELECT C_ANY_ASSEGURAT , substr(c_cip,0,13), versio, seccio, te_seccio FROM dwsisap.rca_seccio_censal WHERE versio=2021 AND C_ANY_ASSEGURAT=2023"
        for any_ass, cip, versio, seccio, te_seccio in u.getAll(sql, 'exadata'):
            hash = self.cip_to_hash[cip] if cip in self.cip_to_hash else None
            id_cip = self.id_to_hash[(hash)] if (hash) in self.id_to_hash else None
            self.rcapob[id_cip] = {'any': any_ass, 'versio': versio, 'seccio': seccio, 'te_seccio': te_seccio}
            
    def get_pob(self):
        u.printTime("pob")
        """Agafem poblacio"""
        self.upload = []
        
        sql = """select id_cip, id_cip_sec, edat, sexe, up, ates from assignada_tot"""
        for id, id2,  edat, sexe, up, ates in u.getAll(sql, 'nodrizas'):
            f1 = self.rcapob[id]['any'] if id in self.rcapob else None
            f2 = self.rcapob[id]['versio'] if id in self.rcapob else None
            f3 = self.rcapob[id]['seccio'] if id in self.rcapob else None
            f4 = self.rcapob[id]['te_seccio'] if id in self.rcapob else None
            self.upload.append([id2, up, edat, sexe, ates, f1, f2, f3, f4])    
    
    
    def export_data(self):
        """."""  
        u.printTime("export")
        cols = ("id_cip_sec int",  "up varchar(10)", "edat int", "sexe varchar(10)", "ates int", 
        "any_assegurat int", "versio int", "seccio varchar(100)", "te_seccio int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)        
        
if __name__ == "__main__":
    u.printTime("Inici")
     
    seccio_censal()
    
    u.printTime("Final") 
