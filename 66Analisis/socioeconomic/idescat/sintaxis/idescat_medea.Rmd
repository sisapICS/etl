---
title: "Anàlisi de la correlació entre l'ISC de l'IDESCAT i el MEDEA"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 3
  word_document:
    toc: yes
    toc_depth: '3'
---


<style>
 .ocultar {
   display: none;

 }

</style>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
options(knitr.kable.na = '')

```


```{r parametres markdown}
inici <- Sys.time()

warning.var <- FALSE
message.var <- FALSE

```

```{r parametres estudi}

nfetch <- -1

```

```{r library, child="library.Rmd"}

```


```{r import, child="import.Rmd"}

```


```{r datamanager, child="datamanager.Rmd"}

```

## Resultats

```{r analisi, child="analisi.Rmd"}

```

