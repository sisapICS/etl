# Anàlisi

<!-- # Diferències entre la distribució del MEDEA segons si tenim dades IDESCAT o no. -->

<!-- ```{r} -->
<!-- medea_seccio_censal_AC[, amb_agrupacio_censal := ifelse(is.na(AC), "No", "Sí")] -->
<!-- ``` -->

<!-- ```{r} -->
<!-- ggplot(medea_seccio_censal_AC, aes(x = MEDEA)) + -->
<!--   geom_histogram(aes(fill = amb_agrupacio_censal), color = "white") + -->
<!--   geom_vline(data = medea_seccio_censal_AC[, mean(MEDEA), amb_agrupacio_censal], aes(xintercept = V1), linetype = 2, color = "#2F3734", size = .8) + -->
<!--   scale_fill_manual(values = c("No" = "#D81159", "Sí" = "#A9C7B0")) + -->
<!--   theme_minimal() + -->
<!--   theme( -->
<!--     legend.position = "bottom", -->
<!--     strip.text = element_blank() -->
<!--   ) + -->
<!--   facet_wrap(~ amb_agrupacio_censal, ncol = 1, scales = "free_y") + -->
<!--   labs(y = "Nombre de seccions censals", x = "MEDEA secció censal", fill = "Hi ha agrupació censal") -->
<!-- ``` -->

<!-- # Diferències entre la distribució del ISC segons si tenim dades a MEDEA o no. -->

<!-- ```{r} -->
<!-- cat_sec_censals_isc[, amb_MEDEA := ifelse(SECTOR %in% medea_seccio_censal[,SECTOR], "No", "Sí")] -->
<!-- ``` -->

<!-- ```{r} -->
<!-- ggplot(cat_sec_censals_isc, aes(x = ist)) + -->
<!--   geom_histogram(aes(fill = amb_MEDEA), color = "white") + -->
<!--   geom_vline(data = cat_sec_censals_isc[, mean(ist), amb_MEDEA], aes(xintercept = V1), linetype = 2, color = "#2F3734", size = .8) + -->
<!--   scale_fill_manual(values = c("No" = "#D81159", "Sí" = "#A9C7B0")) + -->
<!--   theme_minimal() + -->
<!--   theme( -->
<!--     legend.position = "bottom", -->
<!--     strip.text = element_blank() -->
<!--   ) + -->
<!--   facet_wrap(~ amb_MEDEA, ncol = 1, scales = "free_y") + -->
<!--   labs(y = "Nombre de seccions censals", x = "ISC agrupació censal", fill = "Hi ha MEDEA") -->
<!-- ``` -->


<!-- ```{r} -->
<!-- agrupacions_censals <- cat_sec_censals_isc[, .(perc_seccions_censals_merged = mean(SECTOR %in% medea_seccio_censal$SECTOR)), .(AC, AC_nom, ist)] -->
<!-- ``` -->

<!-- ```{r} -->
<!-- agrupacions_censals[, amb_medea := ifelse(perc_seccions_censals_merged == 0, "Sense MEDEA", "Amb MEDEA")] -->
<!-- ggplot(agrupacions_censals, aes(x = ist)) + -->
<!--   geom_histogram(aes(fill = amb_medea), color = "white") + -->
<!--   geom_vline(data = agrupacions_censals[, mean(ist), amb_medea], aes(xintercept = V1), linetype = 2, color = "#2F3734", size = .8) + -->
<!--   scale_fill_manual(values = c("Sense MEDEA" = "#D81159", "Amb MEDEA" = "#A9C7B0")) + -->
<!--   theme_minimal() + -->
<!--   theme( -->
<!--     legend.position = "bottom", -->
<!--     strip.text = element_blank() -->
<!--   ) + -->
<!--   facet_wrap(~ amb_medea, ncol = 1, scales = "free_y") + -->
<!--   labs(y = "Nombre d'agrupacions censals", x = "ISC agrupació censal", fill = "") -->
<!-- ``` -->

<!-- ```{r} -->
<!-- agrupacions_censals[, summary(perc_seccions_censals_merged)] -->
<!-- ``` -->

<!-- ```{r} -->
<!-- agrupacions_censals[, amb_medea_100 := ifelse(perc_seccions_censals_merged == 1, "Totes amb MEDEA", "Algunes sense MEDEA")] -->
<!-- ggplot(agrupacions_censals, aes(x = ist)) + -->
<!--   geom_histogram(aes(fill = amb_medea_100), color = "white") + -->
<!--   geom_vline(data = agrupacions_censals[, mean(ist), amb_medea_100], aes(xintercept = V1), linetype = 2, color = "#2F3734", size = .8) + -->
<!--   scale_fill_manual(values = c("Algunes sense MEDEA" = "#D81159", "Totes amb MEDEA" = "#A9C7B0")) + -->
<!--   theme_minimal() + -->
<!--   theme( -->
<!--     legend.position = "bottom", -->
<!--     strip.text = element_blank() -->
<!--   ) + -->
<!--   facet_wrap(~ amb_medea_100, ncol = 1, scales = "free_y") + -->
<!--   labs(y = "Nombre d'agrupacions censals", x = "ISC agrupació censal", fill = "") -->
<!-- ``` -->


<!-- ```{r} -->
<!-- agrupacions_censals[, amb_medea_50 := ifelse(perc_seccions_censals_merged >= .5, "Almenys el 50% de les SC amb MEDEA", "Menys del 50% de les SC amb MEDEA")] -->
<!-- ggplot(agrupacions_censals, aes(x = ist)) + -->
<!--   geom_histogram(aes(fill = amb_medea_50), color = "white") + -->
<!--   geom_vline(data = agrupacions_censals[, mean(ist), amb_medea_50], aes(xintercept = V1), linetype = 2, color = "#2F3734", size = .8) + -->
<!--   scale_fill_manual(values = c("Menys del 50% de les SC amb MEDEA" = "#D81159", "Almenys el 50% de les SC amb MEDEA" = "#A9C7B0")) + -->
<!--   theme_minimal() + -->
<!--   theme( -->
<!--     legend.position = "bottom", -->
<!--     strip.text = element_blank() -->
<!--   ) + -->
<!--   facet_wrap(~ amb_medea_50, ncol = 1, scales = "free_y") + -->
<!--   labs(y = "Nombre d'agrupacions censals", x = "ISC agrupació censal", fill = "") -->
<!-- ``` -->

<!-- ```{r} -->
<!-- agrupacions_censals[, amb_medea_75 := ifelse(perc_seccions_censals_merged >= .75, "Almenys el 75% de les SC amb MEDEA", "Menys del 75% de les SC amb MEDEA")] -->
<!-- ggplot(agrupacions_censals, aes(x = ist)) + -->
<!--   geom_histogram(aes(fill = amb_medea_75), color = "white") + -->
<!--   geom_vline(data = agrupacions_censals[, mean(ist), amb_medea_75], aes(xintercept = V1), linetype = 2, color = "#2F3734", size = .8) + -->
<!--   scale_fill_manual(values = c("Menys del 75% de les SC amb MEDEA" = "#D81159", "Almenys el 75% de les SC amb MEDEA" = "#A9C7B0")) + -->
<!--   theme_minimal() + -->
<!--   theme( -->
<!--     legend.position = "bottom", -->
<!--     strip.text = element_blank() -->
<!--   ) + -->
<!--   facet_wrap(~ amb_medea_75, ncol = 1, scales = "free_y") + -->
<!--   labs(y = "Nombre d'agrupacions censals", x = "ISC agrupació censal", fill = "") -->
<!-- ``` -->


<!-- # Correlació MEDEA i ISC IDESCAT -->

<!-- ## Amb totes les seccions censals amb merge a MEDEA -->

<!-- ```{r} -->
<!-- taula_1 <- rbind( -->
<!--   unique(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea == "Amb MEDEA"]$AC, .SD, .SDcols = c("AC", "RURALITAT")])[, .( -->
<!--   `Selecció` = "Totes les agregacions censals amb alguna secció censal amb merge a les dues fonts de dades", -->
<!--   Unitat = "Agrupació censal", -->
<!--   .N), RURALITAT], -->
<!--   unique(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea == "Amb MEDEA"]$AC, .SD, .SDcols = c("Municipi_curt", "RURALITAT")])[, .( -->
<!--   `Selecció` = "Totes les agregacions censals amb alguna secció censal amb merge a les dues fonts de dades", -->
<!--   Unitat = "Municipi", -->
<!--   .N), RURALITAT], -->
<!--   unique(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea == "Amb MEDEA"]$AC, .SD, .SDcols = c("SECTOR", "RURALITAT")])[, .( -->
<!--   `Selecció` = "Totes les agregacions censals amb alguna secció censal amb merge a les dues fonts de dades", -->
<!--   Unitat = "Secció censal", -->
<!--   .N), RURALITAT]) -->
<!-- ``` -->

<!-- ### Unitat d'anàlisi: Secció censal -->

<!-- ```{r} -->
<!-- medea_seccio_censal_AC_ISC <- merge(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea == "Amb MEDEA"]$AC], isc_idescat[, .SD, .SDcols = c("Codi", "ist")], by.x = "AC", by.y = "Codi", all.x = T) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- medea_seccio_censal_AC_ISC[, RURALITAT_plot := ifelse(is.na(RURALITAT), "Urbà", "Rural")] -->
<!-- ggplot(medea_seccio_censal_AC_ISC) + -->
<!--   geom_point(aes(ist, MEDEA)) + -->
<!--   geom_text(data = medea_seccio_censal_AC_ISC[, round(cor(MEDEA, ist), 5), RURALITAT_plot], aes(label = paste0("R^2 == ", V1)), parse = T, x = c(120, 120), y = c(3, 5)) + -->
<!--   geom_smooth(method = "lm", color = "#A9C7B0", aes(ist, MEDEA)) + -->
<!--   theme_minimal() + -->
<!--   facet_wrap(~ RURALITAT_plot, scales = "free") + -->
<!--   labs(x = "ISC Idescat", y = "MEDEA secció censal", title = "Correlació entre ISC Idescat i MEDEA secció censal", subtitle = "Totes les agregacions censals amb alguna secció censal amb merge a les dues fonts de dades") -->
<!-- ``` -->

<!-- ### Unitat d'anàlisi: Agrupació censal -->

<!-- ```{r} -->
<!-- medea_seccio_censal_AC_ISC_aggr_AC <- rbind( -->
<!--   medea_seccio_censal_AC_ISC[is.na(RURALITAT)][, .( -->
<!--   RURALITAT_plot = "Urbà", -->
<!--   V1 = mean(MEDEA)), .(AC, AC_nom, ist)], -->
<!--   medea_seccio_censal_AC_ISC[!is.na(RURALITAT)][, .( -->
<!--   RURALITAT_plot = "Rural", -->
<!--   V1 = mean(MEDEA)), .(AC, AC_nom, ist)]) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- ggplot(medea_seccio_censal_AC_ISC_aggr_AC) + -->
<!--   geom_point(aes(ist, V1)) + -->
<!--   geom_text(data = medea_seccio_censal_AC_ISC_aggr_AC[, round(cor(V1, ist), 5), RURALITAT_plot], aes(label = paste0("R^2 == ", V1)), parse = T, x = c(120, 120), y = c(2, 4)) + -->
<!--   geom_smooth(method = "lm", color = "#A9C7B0", aes(ist, V1)) + -->
<!--   theme_minimal() + -->
<!--   facet_wrap(~ RURALITAT_plot, scales = "free") + -->
<!--   labs(x = "ISC Idescat", y = "Mitjana MEDEA secció censal", title = "Correlació entre ISC Idescat i MEDEA secció censal", subtitle = "Totes les agregacions censals amb alguna secció censal amb merge a les dues fonts de dades") -->
<!-- ``` -->

<!-- ## Amb totes les agregacions censals que tinguin, al menys, el 75% de les seccions censals amb MEDEA -->

<!-- ```{r} -->
<!-- taula_2 <- rbind( -->
<!--   unique(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea_75 == "Almenys el 75% de les SC amb MEDEA"]$AC, .SD, .SDcols = c("AC", "RURALITAT")])[, .( -->
<!--   `Selecció` = "Totes les agregacions censals amb, al menys, el 75% de les seccions censals\na les dues fonts de dades", -->
<!--   Unitat = "Agrupació censal", -->
<!--   .N), RURALITAT], -->
<!--   unique(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea_75 == "Almenys el 75% de les SC amb MEDEA"]$AC, .SD, .SDcols = c("Municipi_curt", "RURALITAT")])[, .( -->
<!--   `Selecció` = "Totes les agregacions censals amb, al menys, el 75% de les seccions censals\na les dues fonts de dades", -->
<!--   Unitat = "Municipi", -->
<!--   .N), RURALITAT], -->
<!--   unique(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea_75 == "Almenys el 75% de les SC amb MEDEA"]$AC, .SD, .SDcols = c("SECTOR", "RURALITAT")])[, .( -->
<!--   `Selecció` = "Totes les agregacions censals amb, al menys, el 75% de les seccions censals\na les dues fonts de dades", -->
<!--   Unitat = "Secció censal", -->
<!--   .N), RURALITAT]) -->
<!-- ``` -->

<!-- ### Unitat d'anàlisi: Secció censal -->

<!-- ```{r} -->
<!-- medea_seccio_censal_AC_ISC_75 <- merge(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea_75 == "Almenys el 75% de les SC amb MEDEA"]$AC], isc_idescat[, .SD, .SDcols = c("Codi", "ist")], by.x = "AC", by.y = "Codi", all.x = T) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- medea_seccio_censal_AC_ISC_75[, RURALITAT_plot := ifelse(is.na(RURALITAT), "Urbà", "Rural")] -->
<!-- ggplot(medea_seccio_censal_AC_ISC_75) + -->
<!--   geom_point(aes(ist, MEDEA)) + -->
<!--   geom_text(data = medea_seccio_censal_AC_ISC_75[, round(cor(MEDEA, ist), 2), RURALITAT_plot], aes(label = paste0("R^2 == ", V1)), parse = T, x = c(120, 120), y = c(3, 5)) + -->
<!--   geom_smooth(method = "lm", color = "#A9C7B0", aes(ist, MEDEA)) + -->
<!--   theme_minimal() + -->
<!--   facet_wrap(~ RURALITAT_plot, scales = "free") + -->
<!--   labs(x = "ISC Idescat", y = "MEDEA secció censal", title = "Correlació entre ISC Idescat i MEDEA secció censal", subtitle = "Totes les agregacions censals amb, al menys, el 75% de les seccions censals\na les dues fonts de dades") -->
<!-- ``` -->

<!-- ### Unitat d'anàlisi: Agrupació censal -->

<!-- ```{r} -->
<!-- medea_seccio_censal_AC_ISC_aggr_AC_75 <- rbind( -->
<!--   medea_seccio_censal_AC_ISC_75[is.na(RURALITAT)][, .( -->
<!--   RURALITAT_plot = "Urbà", -->
<!--   V1 = mean(MEDEA)), .(AC, AC_nom, ist)], -->
<!--   medea_seccio_censal_AC_ISC_75[!is.na(RURALITAT)][, .( -->
<!--   RURALITAT_plot = "Rural", -->
<!--   V1 = mean(MEDEA)), .(AC, AC_nom, ist)]) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- ggplot(medea_seccio_censal_AC_ISC_aggr_AC_75) + -->
<!--   geom_point(aes(ist, V1)) + -->
<!--   geom_text(data = medea_seccio_censal_AC_ISC_aggr_AC_75[, round(cor(V1, ist), 2), RURALITAT_plot], aes(label = paste0("R^2 == ", V1)), parse = T, x = c(120, 120), y = c(1.5, 4)) + -->
<!--   geom_smooth(method = "lm", color = "#A9C7B0", aes(ist, V1)) + -->
<!--   theme_minimal() + -->
<!--   facet_wrap(~ RURALITAT_plot, scales = "free") + -->
<!--   labs(x = "ISC Idescat", y = "Mitjana MEDEA secció censal", title = "Correlació entre ISC Idescat i MEDEA secció censal", subtitle = "Totes les agregacions censals amb, al menys, el 75% de les seccions censals\na les dues fonts de dades") -->
<!-- ``` -->

<!-- ## Amb totes les agregacions censals que tinguin, al menys, el 50% de les seccions censals amb MEDEA -->

<!-- ```{r} -->
<!-- taula_3 <- rbind( -->
<!--   unique(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea_50 == "Almenys el 50% de les SC amb MEDEA"]$AC, .SD, .SDcols = c("AC", "RURALITAT")])[, .( -->
<!--   `Selecció` = "Totes les agregacions censals amb, al menys, el 50% de les seccions censals\na les dues fonts de dades", -->
<!--   Unitat = "Agrupació censal", -->
<!--   .N), RURALITAT], -->
<!--   unique(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea_50 == "Almenys el 50% de les SC amb MEDEA"]$AC, .SD, .SDcols = c("Municipi_curt", "RURALITAT")])[, .( -->
<!--   `Selecció` = "Totes les agregacions censals amb, al menys, el 50% de les seccions censals\na les dues fonts de dades", -->
<!--   Unitat = "Municipi", -->
<!--   .N), RURALITAT], -->
<!--   unique(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea_50 == "Almenys el 50% de les SC amb MEDEA"]$AC, .SD, .SDcols = c("SECTOR", "RURALITAT")])[, .( -->
<!--   `Selecció` = "Totes les agregacions censals amb, al menys, el 50% de les seccions censals\na les dues fonts de dades", -->
<!--   Unitat = "Secció censal", -->
<!--   .N), RURALITAT]) -->
<!-- ``` -->

<!-- ### Unitat d'anàlisi: Secció censal -->

<!-- ```{r} -->
<!-- medea_seccio_censal_AC_ISC_50 <- merge(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea_50 == "Almenys el 50% de les SC amb MEDEA"]$AC], isc_idescat[, .SD, .SDcols = c("Codi", "ist")], by.x = "AC", by.y = "Codi", all.x = T) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- medea_seccio_censal_AC_ISC_50[, RURALITAT_plot := ifelse(is.na(RURALITAT), "Urbà", "Rural")] -->
<!-- ggplot(medea_seccio_censal_AC_ISC_50) + -->
<!--   geom_point(aes(ist, MEDEA)) + -->
<!--   geom_text(data = medea_seccio_censal_AC_ISC_50[, round(cor(MEDEA, ist), 2), RURALITAT_plot], aes(label = paste0("R^2 == ", V1)), parse = T, x = c(120, 120), y = c(3, 5)) + -->
<!--   geom_smooth(method = "lm", color = "#A9C7B0", aes(ist, MEDEA)) + -->
<!--   theme_minimal() + -->
<!--   facet_wrap(~ RURALITAT_plot, scales = "free") + -->
<!--   labs(x = "ISC Idescat", y = "MEDEA secció censal", title = "Correlació entre ISC Idescat i MEDEA secció censal", subtitle = "Totes les agregacions censals amb, al menys, el 50% de les seccions censals\na les dues fonts de dades") -->
<!-- ``` -->

<!-- ### Unitat d'anàlisi: Agrupació censal -->

<!-- ```{r} -->
<!-- medea_seccio_censal_AC_ISC_aggr_AC_50 <- rbind( -->
<!--   medea_seccio_censal_AC_ISC_50[is.na(RURALITAT)][, .( -->
<!--   RURALITAT_plot = "Urbà", -->
<!--   V1 = mean(VALOR)), .(AC, AC_nom, V3)], -->
<!--   medea_seccio_censal_AC_ISC_50[!is.na(RURALITAT)][, .( -->
<!--   RURALITAT_plot = "Rural", -->
<!--   V1 = mean(VALOR)), .(AC, AC_nom, V3)]) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- ggplot(medea_seccio_censal_AC_ISC_aggr_AC_50) + -->
<!--   geom_point(aes(V3, V1)) + -->
<!--   geom_text(data = medea_seccio_censal_AC_ISC_aggr_AC_50[, round(cor(V1, V3), 2), RURALITAT_plot], aes(label = paste0("R^2 == ", V1)), parse = T, x = c(120, 120), y = c(1.5, 4)) + -->
<!--   geom_smooth(method = "lm", color = "#A9C7B0", aes(V3, V1)) + -->
<!--   theme_minimal() + -->
<!--   facet_wrap(~ RURALITAT_plot, scales = "free") + -->
<!--   labs(x = "ISC Idescat", y = "Mitjana MEDEA secció censal", title = "Correlació entre ISC Idescat i MEDEA secció censal", subtitle = "Totes les agregacions censals amb, al menys, el 50% de les seccions censals\na les dues fonts de dades") -->
<!-- ``` -->

<!-- ## Amb totes les agregacions censals que tinguin el 100% de les seccions censals amb MEDEA -->

<!-- ```{r} -->
<!-- taula_4 <- rbind( -->
<!--   unique(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea_100 == "Totes amb MEDEA"]$AC, .SD, .SDcols = c("AC", "RURALITAT")])[, .( -->
<!--   `Selecció` = "Totes les agregacions censals amb totes les seccions censals\na les dues fonts de dades", -->
<!--   Unitat = "Agrupació censal", -->
<!--   .N), RURALITAT], -->
<!--   unique(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea_50 == "Totes amb MEDEA"]$AC, .SD, .SDcols = c("Municipi_curt", "RURALITAT")])[, .( -->
<!--   `Selecció` = "Totes les agregacions censals amb totes les seccions censals\na les dues fonts de dades", -->
<!--   Unitat = "Municipi", -->
<!--   .N), RURALITAT], -->
<!--   unique(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea_50 == "Totes amb MEDEA"]$AC, .SD, .SDcols = c("SECTOR", "RURALITAT")])[, .( -->
<!--   `Selecció` = "Totes les agregacions censals amb totes les seccions censals\na les dues fonts de dades", -->
<!--   Unitat = "Secció censal", -->
<!--   .N), RURALITAT]) -->
<!-- ``` -->

<!-- ### Unitat d'anàlisi: Secció censal -->

<!-- ```{r} -->
<!-- medea_seccio_censal_AC_ISC_100 <- merge(medea_seccio_censal_AC[AC %in% agrupacions_censals[amb_medea_100 == "Totes amb MEDEA"]$AC], isc_idescat[, .SD, .SDcols = c("V1", "V3")], by.x = "AC", by.y = "V1", all.x = T) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- medea_seccio_censal_AC_ISC_100[, RURALITAT_plot := ifelse(is.na(RURALITAT), "Urbà", "Rural")] -->
<!-- ggplot(medea_seccio_censal_AC_ISC_100) + -->
<!--   geom_point(aes(V3, VALOR)) + -->
<!--   geom_text(data = medea_seccio_censal_AC_ISC_100[, round(cor(VALOR, V3), 2), RURALITAT_plot], aes(label = paste0("R^2 == ", V1)), parse = T, x = c(120, 120), y = c(3, 5)) + -->
<!--   geom_smooth(method = "lm", color = "#A9C7B0", aes(V3, VALOR)) + -->
<!--   theme_minimal() + -->
<!--   facet_wrap(~ RURALITAT_plot, scales = "free") + -->
<!--   labs(x = "ISC Idescat", y = "MEDEA secció censal", title = "Correlació entre ISC Idescat i MEDEA secció censal", subtitle = "Totes les agregacions censals amb totes les seccions censals\na les dues fonts de dades") -->
<!-- ``` -->

<!-- ### Unitat d'anàlisi: Agrupació censal -->

<!-- ```{r} -->
<!-- medea_seccio_censal_AC_ISC_aggr_AC_100 <- rbind( -->
<!--   medea_seccio_censal_AC_ISC_100[is.na(RURALITAT)][, .( -->
<!--   RURALITAT_plot = "Urbà", -->
<!--   V1 = mean(VALOR)), .(AC, AC_nom, V3)], -->
<!--   medea_seccio_censal_AC_ISC_100[!is.na(RURALITAT)][, .( -->
<!--   RURALITAT_plot = "Rural", -->
<!--   V1 = mean(VALOR)), .(AC, AC_nom, V3)]) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- ggplot(medea_seccio_censal_AC_ISC_aggr_AC_100) + -->
<!--   geom_point(aes(V3, V1)) + -->
<!--   geom_text(data = medea_seccio_censal_AC_ISC_aggr_AC_100[, round(cor(V1, V3), 2), RURALITAT_plot], aes(label = paste0("R^2 == ", V1)), parse = T, x = c(120, 120), y = c(1.5, 4)) + -->
<!--   geom_smooth(method = "lm", color = "#A9C7B0", aes(V3, V1)) + -->
<!--   theme_minimal() + -->
<!--   facet_wrap(~ RURALITAT_plot, scales = "free") + -->
<!--   labs(x = "ISC Idescat", y = "Mitjana MEDEA secció censal", title = "Correlació entre ISC Idescat i MEDEA secció censal", subtitle = "Totes les agregacions censals amb totes les seccions censals\na les dues fonts de dades") -->
<!-- ``` -->


## Resum


```{r}
taula <- dcast(rbind(taula_1, taula_2, taula_3, taula_4), `Selecció` + Unitat ~ RURALITAT, value.var = "N")
taula %>%
  kable(digits = 2, row.names = F, col.names = c("Selecció", "Unitat", "Rural", "Urbà"), escape = F, format.args = list(decimal.mark = ',', big.mark = "."), align = c("l", "l", "c", "c")) %>%
  kable_styling(full_width = F, position = "center") %>%
  collapse_rows(columns = 1, valign = "top") %>%
  add_header_above(c(" " = 2, "Ruralitat" = 2))

```



```{r}
medea_seccio_censal_AC_ISC_tot <- rbind(
  medea_seccio_censal_AC_ISC[, .SD, .SDcols = c("AC", "SECTOR", "VALOR", "V3", "RURALITAT_plot")][, `Selecció` := "Totes les agregacions censals amb alguna secció censal amb merge a les dues fonts de dades"],
  medea_seccio_censal_AC_ISC_75[, .SD, .SDcols = c("AC", "SECTOR", "VALOR", "V3", "RURALITAT_plot")][, `Selecció` := "Totes les agregacions censals amb, al menys, el 75% de les seccions censals\na les dues fonts de dades"],
  medea_seccio_censal_AC_ISC_50[, .SD, .SDcols = c("AC", "SECTOR", "VALOR", "V3", "RURALITAT_plot")][, `Selecció` := "Totes les agregacions censals amb, al menys, el 50% de les seccions censals\na les dues fonts de dades"],
  medea_seccio_censal_AC_ISC_100[, .SD, .SDcols = c("AC", "SECTOR", "VALOR", "V3", "RURALITAT_plot")][, `Selecció` := "Totes les agregacions censals amb totes les seccions censals\na les dues fonts de dades"]
)

medea_seccio_censal_AC_ISC_tot_cor <- medea_seccio_censal_AC_ISC_tot[, .(Cor_seccio_censal = cor(VALOR, V3)), .(`Selecció`, RURALITAT_plot)]
```

```{r}
medea_seccio_censal_AC_ISC_aggr_AC_tot <- rbind(
  medea_seccio_censal_AC_ISC_aggr_AC[, .SD, .SDcols = c("AC", "V1", "V3", "RURALITAT_plot")][, `Selecció` := "Totes les agregacions censals amb alguna secció censal amb merge a les dues fonts de dades"],
  medea_seccio_censal_AC_ISC_aggr_AC_75[, .SD, .SDcols = c("AC", "V1", "V3", "RURALITAT_plot")][, `Selecció` := "Totes les agregacions censals amb, al menys, el 75% de les seccions censals\na les dues fonts de dades"],
  medea_seccio_censal_AC_ISC_aggr_AC_50[, .SD, .SDcols = c("AC", "V1", "V3", "RURALITAT_plot")][, `Selecció` := "Totes les agregacions censals amb, al menys, el 50% de les seccions censals\na les dues fonts de dades"],
  medea_seccio_censal_AC_ISC_aggr_AC_100[, .SD, .SDcols = c("AC", "V1", "V3", "RURALITAT_plot")][, `Selecció` := "Totes les agregacions censals amb totes les seccions censals\na les dues fonts de dades"]
)

medea_seccio_censal_AC_ISC_AC_aggr_tot_cor <- medea_seccio_censal_AC_ISC_aggr_AC_tot[, .(Cor_seccio_censal = cor(V1, V3)), .(`Selecció`, RURALITAT_plot)]
```

```{r}
taula <- dcast(rbind(
  medea_seccio_censal_AC_ISC_tot_cor[, unitat := "Secció censal"],
  medea_seccio_censal_AC_ISC_AC_aggr_tot_cor[, unitat := "Agregació censal"]
), `Selecció` + RURALITAT_plot ~ unitat, value.var = "Cor_seccio_censal")
taula %>%
  kable(digits = 2, row.names = F, col.names = c("Selecció", "Ruralitat", "Agregació censal", "Selecció censal"), escape = F, format.args = list(decimal.mark = ',', big.mark = "."), align = c("l", "l", "c", "c")) %>%
  kable_styling(full_width = F, position = "center") %>%
  collapse_rows(columns = 1:2, valign = "top") %>%
  add_header_above(c(" " = 2, "Unitat anàlisi" = 2))
```


## Correlació entre ISC Idescat i indicadors socieconòmics dels EAP

Un cop assignada l'agrupació censal a cada secció censal, es calcula la mitjana ponderada de l'ISC de l'Idescat per cada EAP, en funció de la distribució dels pacients a les diferents agrupacions censals.

```{r}
dades_finals[, ISC_ponderat := IDESCAT*perc_rec/100]
dades_finals_up <- dades_finals[, .(IDESCAT = sum(ISC_ponderat, na.rm = T)), .(up, ics_codi, ics_desc, medea, medea_num, aquas)]

```


### ICS Idescat vs MEDEA

```{r}
dades_finals_up[, medea_rural := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), labels = c("Rural", "Rural", "Rural", "1U", "2U", "3U", "4U"))]
ggplot(dades_finals_up, aes(x = medea_rural, y = IDESCAT)) + 
  geom_boxplot(fill = "#A9C7B0") + 
  theme_minimal() +
  labs(x = "MEDEA", y = "ISC Idescat ponderat")
```

```{r fig.height=6}

ggplot(dades_finals_up, aes(x = IDESCAT)) + 
  geom_histogram(color = "black", fill = "#A9C7B0") + 
  theme_minimal() +
  facet_wrap(~ medea_rural, ncol = 1, scales = "free_y") +
   labs(x = "ISC Idescat ponderat", y = "N UP")
```


```{r}
dades_finals_up[, rural := ifelse(medea_rural == "Rural", "Rural", "Urbà")]
ggplot(dades_finals_up) +
  geom_point(aes(IDESCAT, medea_num)) + 
  geom_text(data = dades_finals_up[, round(cor(IDESCAT, medea_num), 5), rural], aes(label = paste0("R^2 == ", V1)), parse = T, x = c(90, 110), y = c(1.5, 2.7)) +
  geom_smooth(method = "lm", color = "#A9C7B0", aes(IDESCAT, medea_num)) +
  theme_minimal() +
  facet_wrap(~ rural, scales = "free") +
  labs(x = "ISC Idescat ponderat", y = "MEDEA EAP", title = "Correlació entre ISC Idescat ponderat i MEDEA UP")
```

```{r fig.height=6}
ggplot(dades_finals_up) +
  geom_point(aes(IDESCAT, medea_num)) + 
  geom_text(data = dades_finals_up[, round(cor(IDESCAT, medea_num), 2), medea_rural], aes(label = paste0("R^2 == ", V1)), parse = T, x = c(90, 110, 95, 95, 95), y = c(1.5, 0, 0.75, 1, 2.5)) +
  geom_smooth(method = "lm", color = "#A9C7B0", aes(IDESCAT, medea_num)) +
  theme_minimal() +
  facet_wrap(~ medea_rural, scales = "free", ncol = 2) +
  labs(x = "ISC Idescat ponderat", y = "MEDEA EAP", title = "Correlació entre ISC Idescat ponderat i MEDEA UP")
```

```{r}
t_medea <- dades_finals_up[, .(R2_medea_num = round(cor(IDESCAT, medea_num), 2)), medea_rural][order(medea_rural)]
```

### ICS Idescat vs AQUAS

```{r}
dades_finals_up[, rural := ifelse(medea_rural == "Rural", "Rural", "Urbà")]
ggplot(dades_finals_up) +
  geom_point(aes(IDESCAT, aquas)) + 
  geom_text(data = dades_finals_up[, round(cor(IDESCAT, aquas), 5), rural], aes(label = paste0("R^2 == ", V1)), parse = T, x = c(90, 110), y = c(70, 90)) +
  geom_smooth(method = "lm", color = "#A9C7B0", aes(IDESCAT, aquas)) +
  theme_minimal() +
  facet_wrap(~ rural, scales = "free") +
  labs(x = "ISC Idescat ponderat", y = "AQUAS EAP", title = "Correlació entre ISC Idescat ponderat i AQUAS UP")
```

```{r fig.height=6}
ggplot(dades_finals_up) +
  geom_point(aes(IDESCAT, aquas)) + 
  geom_text(data = dades_finals_up[, round(cor(IDESCAT, aquas), 2), medea_rural], aes(label = paste0("R^2 == ", V1)), parse = T, x = c(90, 110, 95, 95, 95), y = c(70, 35, 60, 60, 90)) +
  geom_smooth(method = "lm", color = "#A9C7B0", aes(IDESCAT, aquas)) +
  theme_minimal() +
  facet_wrap(~ medea_rural, scales = "free", ncol = 2) +
  labs(x = "ISC Idescat ponderat", y = "MEDEA EAP", title = "Correlació entre ISC Idescat ponderat i MEDEA UP")
```

```{r}
t_aquas <- dades_finals_up[, .(R2_aquas = round(cor(IDESCAT, aquas), 2)), medea_rural][order(medea_rural)]

t <- merge(t_medea, t_aquas, by = "medea_rural")
```

### ICS Idescat vs E/O

```{r}
dades_finals_up_eo <- merge(dades_finals_up, diag_e_o, by = "ics_codi", all.x = T)
```


```{r}
ggplot(dades_finals_up_eo[!is.na(dx)]) +
  geom_point(aes(IDESCAT, EO)) + 
  # geom_text(data = dades_finals_up_eo[!is.na(dx), round(cor(IDESCAT, aquas, use = "pairwise.complete.obs"), 2), .(rural, dx)], aes(label = paste0("R^2 == ", V1)), parse = T, x = rep(100, 8), y = c(1.25, 1.35, 1.35, 1.35, 2.5, 1.5)) +
  geom_smooth(method = "lm", color = "#A9C7B0", aes(IDESCAT, EO)) +
  theme_minimal() +
  facet_wrap(~ rural + dx, scales = "free") +
  labs(x = "ISC Idescat ponderat", y = "AQUAS EAP", title = "Correlació entre ISC Idescat ponderat i E/O ajustats")
```

```{r}
t_dx_isc <- dades_finals_up_eo[!is.na(dx), .(R2_ISC = round(cor(IDESCAT, EO, use = "pairwise.complete.obs"), 2)), .(rural, dx)][order(rural)]
t_dx_medea <- dades_finals_up_eo[!is.na(dx), .(R2_medea = round(cor(medea_num, EO, use = "pairwise.complete.obs"), 2)), .(rural, dx)][order(rural)]

t_dx <- merge(t_dx_isc, t_dx_medea, by = c("rural", "dx"))
t_dx
```


```{r fig.height=8}
ggplot(dades_finals_up_eo[!is.na(dx)]) +
  geom_point(aes(IDESCAT, EO)) + 
  geom_smooth(method = "lm", color = "#A9C7B0", aes(IDESCAT, EO)) +
  theme_minimal() +
  facet_wrap(~ medea_rural + dx, scales = "free", ncol = 3) +
  labs(x = "ISC Idescat ponderat", y = "AQUAS EAP", title = "Correlació entre ISC Idescat ponderat i E/O ajustats")
```

```{r}
t_dx <- dades_finals_up_eo[!is.na(dx), .(R2 = round(cor(IDESCAT, EO, use = "pairwise.complete.obs"), 2)), .(medea_rural, dx)][order(medea_rural)]
t <- merge(t, dcast(t_dx, medea_rural ~ dx, value.var = "R2"), by = "medea_rural")
t
```