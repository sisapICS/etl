# Anàlisi descriptiva bivariant

## Correlació entre variables explicatives

Sobre la població atesa

```{r}
aux <- merge(explicatives_eap, cat_centres.dm[, c("ics_codi", "aquas")], by = "ics_codi")
M = cor(aux[variable == "Atesa", c(3:11)], use = "complete.obs")
corrplot(M, method = 'number', tl.col = "black") # colorful number
```

Sobre la població assignada

```{r}
M = cor(aux[variable == "Assignada", c(3:11)], use = "complete.obs")
corrplot(M, method = 'number', tl.col = "black") # colorful number
```

### Correlació entre explicatives i prevalences

```{r}
aux <- merge(diagactiu_eap, cat_centres[, c("ics_codi", "medea", "aquas", "medea_b", "rural")], by = "ics_codi", all.x = T)
aux <- merge(aux, explicatives_eap, by = "ics_codi", all.x = T, allow.cartesian=TRUE)

dades_model <- aux

R2_atesa <- dades_model[variable == "Atesa", lapply(.SD, function(x) cor(prev_atesa, x, use = "complete.obs")), .SDcols = c("aquas", "dones", "edat", "edat_65", "immigrants", "pensionistes", "pensionistes_65", "medea.y", "ratio_professions", "gma"), by = "codi"]
R2_assignada <- dades_model[variable == "Assignada", lapply(.SD, function(x) cor(prev_assignada, x, use = "complete.obs")), .SDcols = c("aquas", "dones", "edat", "edat_65", "immigrants", "pensionistes", "pensionistes_65", "medea.y", "ratio_professions", "gma"), by = "codi"]
```

```{r}
R2_atesa <- merge(R2_atesa, cat_codis.dm[, c("codi", "MIN(DESC_CIAP_M)")], by = "codi")
R2_assignada <- merge(R2_assignada, cat_codis.dm[, c("codi", "MIN(DESC_CIAP_M)")], by = "codi")
```


