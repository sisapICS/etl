---
title: "Malalties de rics i malalties de pobres"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
  word_document:
    toc: yes
    toc_depth: '6'
---

### Actualització Informe

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```


```{r parametres markdown}
inici <- Sys.time()

setwd("C:/Users/nmora/repositori/etl/66Analisis/socioeconomic/Prevalences rics vs pobres/scripts/")

warning.var <- FALSE
message.var <- FALSE

eval.library.var <- TRUE
eval.import.var <- TRUE
eval.datamanager.var <- TRUE
eval.univariada.var <- FALSE
eval.bivariada.var <- FALSE
eval.lm.var <- FALSE
```

```{r parametres estudi}

nfetch <- -1
# set.seed(12345678) # Especificació d'una llavors per obtenir mateixos resultats cada vegada que es fa el Matching

# data_fi <- as.Date("2021-10-13")
```

```{r library, child="library.Rmd", eval=eval.library.var}

```

# Metodologia

## Dades

```{r import, child="import.Rmd", eval=eval.import.var}

```

```{r datamanager, child="datamanager.Rmd", eval=eval.datamanager.var}

```

## Resultats

```{r descriptiva univariada, child="analisi_descriptiva_uni.Rmd", eval=eval.univariada.var}

```



