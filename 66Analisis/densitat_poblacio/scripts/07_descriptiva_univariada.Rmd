
# Descriptiva Univariada {.tabset}

```{r}
dt.cat_centres.flt <- readRDS("../data/dt.cat_centres.flt.rds")
```

## Densitat {.tabset}

```{r}
summary(dt.cat_centres.flt[, .(densitat)])

dt.datatable <- dt.cat_centres.flt[is.na(densitat), .(amb_desc, sap_desc, abs_cod, scs_codi, ics_desc, medea_c, aquas, densitat, area_km2)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

### Figures

```{r}
ggplot(dt.cat_centres.flt,
       aes(x = densitat)) +
  geom_histogram(color = 'white', fill = 'orange') +
  theme_minimal()
```

## Superfície {.tabset}

```{r}
summary(dt.cat_centres.flt[, .(area_km2)])

dt.datatable <- dt.cat_centres.flt[is.na(area_km2), .(amb_desc, sap_desc, abs_cod, scs_codi, ics_desc, medea_c, aquas, densitat, area_km2)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

### Figures

```{r}
ggplot(dt.cat_centres.flt,
       aes(x = area_km2)) +
  geom_histogram(color = 'white', fill = 'orange') +
  theme_minimal()
```

## Número de Consultoris

```{r}
summary(dt.cat_centres.flt[, .(nconsultoris)])
```

```{r}
ggplot(dt.cat_centres.flt,
       aes(x = nconsultoris)) +
  geom_histogram(color = 'white', fill = 'orange') +
  theme_minimal()
```

```{r}
#rm(list = c("dt.agr"))
gc.var <- gc()
```


