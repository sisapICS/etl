***
***

# Flowchart

Diagrama amb els criteris d'inclusió i excluisió de l'estudi.

```{r}
n1.reg <- nrow(dt.dm)
n1.codis <- uniqueN(dt.dm[, codi])
n1.tipus <- uniqueN(dt.dm[, tipus])
n1.abs <- uniqueN(dt.dm[, abs])
n1.eap <- uniqueN(dt.dm[, scs_codi])

dt.temp <- dt.dm[tipus == "ASSISTÈNCIA SALUT MENTAL",]
n2.reg <- nrow(dt.temp)
n2.vis <- sum(dt.temp[, visites])
n2.codis <- uniqueN(dt.temp[, codi])
n2.abs <- uniqueN(dt.temp[, abs])
n2.eap <- uniqueN(dt.temp[, scs_codi])
n2.mvis <- round(mean(dt.temp[, visites]),0)

dt.temp <- dt.dm[tipus == "ATENCIÓ CONTINUADA",]
n3.reg <- nrow(dt.temp)
n3.vis <- sum(dt.temp[, visites])
n3.codis <- uniqueN(dt.temp[, codi])
n3.abs <- uniqueN(dt.temp[, abs])
n3.eap <- uniqueN(dt.temp[, scs_codi])
n3.mvis <- round(mean(dt.temp[, visites]),0)

dt.temp <- dt.dm[tipus == "PADES",]
n4.reg <- nrow(dt.temp)
n4.vis <- sum(dt.temp[, visites])
n4.codis <- uniqueN(dt.temp[, codi])
n4.abs <- uniqueN(dt.temp[, abs])
n4.eap <- uniqueN(dt.temp[, scs_codi])
n4.mvis <- round(mean(dt.temp[, visites]),0)

dt.temp <- dt.dm[tipus == "UNITAT DE REPRODUCCIÓ I SEXUALITAT",]
n5.reg <- nrow(dt.temp)
n5.vis <- sum(dt.temp[, visites])
n5.codis <- uniqueN(dt.temp[, codi])
n5.abs <- uniqueN(dt.temp[, abs])
n5.eap <- uniqueN(dt.temp[, scs_codi])
n5.mvis <- round(mean(dt.temp[, visites]),0)

dt.temp <- dt.dm[tipus == "REHABILITACIÓ",]
n6.reg <- nrow(dt.temp)
n6.vis <- sum(dt.temp[, visites])
n6.codis <- uniqueN(dt.temp[, codi])
n6.abs <- uniqueN(dt.temp[, abs])
n6.eap <- uniqueN(dt.temp[, scs_codi])
n6.mvis <- round(mean(dt.temp[, visites]),0)

dt.dm[, .N, descripcio_tipus]

dt.temp <- dt.dm[tipus == "ATENCIÓ CONTINUADA" & descripcio_tipus == "CUAP",]
n3.1.reg <- nrow(dt.temp)
n3.1.vis <- sum(dt.temp[, visites])
n3.1.codis <- uniqueN(dt.temp[, codi])
n3.1.abs <- uniqueN(dt.temp[, abs])
n3.1.eap <- uniqueN(dt.temp[, scs_codi])
n3.1.mvis <- round(mean(dt.temp[, visites]),0)

dt.temp <- dt.dm[tipus == "ATENCIÓ CONTINUADA" & descripcio_tipus == "PAC",]
n3.2.reg <- nrow(dt.temp)
n3.2.vis <- sum(dt.temp[, visites])
n3.2.codis <- uniqueN(dt.temp[, codi])
n3.2.abs <- uniqueN(dt.temp[, abs])
n3.2.eap <- uniqueN(dt.temp[, scs_codi])
n3.2.mvis <- round(mean(dt.temp[, visites]),0)

dt.temp <- dt.dm[tipus == "ATENCIÓ CONTINUADA" & descripcio_tipus == "CAC",]
n3.3.reg <- nrow(dt.temp)
n3.3.vis <- sum(dt.temp[, visites])
n3.3.codis <- uniqueN(dt.temp[, codi])
n3.3.abs <- uniqueN(dt.temp[, abs])
n3.3.eap <- uniqueN(dt.temp[, scs_codi])
n3.3.mvis <- round(mean(dt.temp[, visites]),0)

dt.temp <- dt.dm[tipus == "ATENCIÓ CONTINUADA" & descripcio_tipus == "DEA",]
n3.4.reg <- nrow(dt.temp)
n3.4.vis <- sum(dt.temp[, visites])
n3.4.codis <- uniqueN(dt.temp[, codi])
n3.4.abs <- uniqueN(dt.temp[, abs])
n3.4.eap <- uniqueN(dt.temp[, scs_codi])
n3.4.mvis <- round(mean(dt.temp[, visites]),0)

dt.temp <- dt.dm[tipus == "ATENCIÓ CONTINUADA" & descripcio_tipus == "Atenció Continuada",]
n3.5.reg <- nrow(dt.temp)
n3.5.vis <- sum(dt.temp[, visites])
n3.5.codis <- uniqueN(dt.temp[, codi])
n3.5.abs <- uniqueN(dt.temp[, abs])
n3.5.eap <- uniqueN(dt.temp[, scs_codi])
n3.5.mvis <- round(mean(dt.temp[, visites]),0)
```

```{r}
fin <- c(seq(1,20,1))

grViz("
      digraph a_nice_graph
      {
      
      node[fontname = Helvetica,
           fontcolor = black,
           shape = box,
           width = 1,
           style = filled,
           fillcolor = whitesmoke]
      
      '@@1' -> '@@2';
      '@@1' -> '@@3';
      '@@1' -> '@@4';
      '@@1' -> '@@5';
      '@@1' -> '@@6';
      '@@3' -> '@@7';
      '@@3' -> '@@8';
      '@@3' -> '@@9';
      '@@3' -> '@@10';
      '@@3' -> '@@11';
      }

      [1]: paste0('Registres N=', n1.reg, '\\n', 'Centres N=', n1.codis, '\\n', 'Tipus N=', n1.tipus, '\\n', 'ABS N=', n1.abs, '\\n', 'EAP N=', n1.eap)
      [2]: paste0('ASSISTÈNCIA SALUT \\n MENTAL', '\\n', 'Registres N=', n2.reg, '\\n', 'Visites N=', n2.vis, '\\n', 'Centres N=', n2.codis, '\\n', 'ABS N=', n2.abs, '\\n', 'EAP N=', n2.eap,'\\n', 'Mitjana Visites ', n2.mvis)
      [3]: paste0('ATENCIÓ CONTINUADA', '\\n', 'Registres N=', n3.reg, '\\n', 'Visites N=', n3.vis, '\\n', 'Centres N=', n3.codis, '\\n', 'ABS N=', n3.abs, '\\n', 'EAP N=', n3.eap, '\\n', 'Mitjana Visites ', n3.mvis)
      [4]: paste0('PADES', '\\n', 'Registres N=', n4.reg, '\\n', 'Visites N=', n4.vis, '\\n', 'Centres N=', n4.codis, '\\n', 'ABS N=', n4.abs, '\\n', 'EAP N=', n4.eap, '\\n', 'Mitjana Visites ', n4.mvis)
      [5]: paste0('UNITAT DE REPRODUCCIÓ \\n I SEXUALITAT', '\\n', 'Registres N=', n5.reg, '\\n', 'Visites N=', n5.vis, '\\n', 'Centres N=', n5.codis, '\\n', 'ABS N=', n5.abs, '\\n', 'EAP N=', n5.eap, '\\n', 'Mitjana Visites ', n5.mvis)
      [6]: paste0('REHABILITACIÓ', '\\n', 'Registres N=', n6.reg, '\\n', 'Visites N=', n6.vis, '\\n', 'Centres N=', n6.codis, '\\n', 'ABS N=', n6.abs, '\\n', 'EAP N=', n6.eap, '\\n','Mitjana Visites ', n6.mvis)
      [7]: paste0('CUAP', '\\n', 'Registres N=', n3.1.reg, '\\n', 'Visites N=', n3.1.vis, '\\n', 'Centres N=', n3.1.codis, '\\n', 'ABS N=', n3.1.abs, '\\n', 'EAP N=', n3.1.eap, '\\n', 'Mitjana Visites ', n3.1.mvis)
      [8]: paste0('PAC', '\\n', 'Registres N=', n3.2.reg, '\\n', 'Visites N=', n3.2.vis, '\\n', 'Centres N=', n3.2.codis, '\\n', 'ABS N=', n3.2.abs, '\\n', 'EAP N=', n3.2.eap, '\\n', 'Mitjana Visites ', n3.2.mvis)
      [9]: paste0('CAC', '\\n', 'Registres N=', n3.3.reg, '\\n', 'Visites N=', n3.3.vis, '\\n', 'Centres N=', n3.3.codis, '\\n', 'ABS N=', n3.3.abs, '\\n', 'EAP N=', n3.3.eap, '\\n', 'Mitjana Visites ', n3.3.mvis)
      [10]: paste0('DEA', '\\n', 'Registres N=', n3.4.reg, '\\n', 'Visites N=', n3.4.vis, '\\n', 'Centres N=', n3.4.codis, '\\n', 'ABS N=', n3.4.abs, '\\n', 'EAP N=', n3.4.eap, '\\n', 'Mitjana Visites ', n3.4.mvis)
      [11]: paste0('A. Continuada', '\\n', 'Registres N=', n3.5.reg, '\\n', 'Visites N=', n3.5.vis, '\\n', 'Centres N=', n3.5.codis, '\\n', 'ABS N=', n3.5.abs, '\\n', 'EAP N=', n3.5.eap, '\\n', 'Mitjana Visites ', n3.5.mvis)
      ", height = 600, width = 1200)
```

```{r}

dt.flt <- dt.dm
```

```{r}
gc.var <- gc()
```

