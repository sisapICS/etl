
library('rmarkdown')


render("D:/SISAP/sisap/66Analisis/fluxes/scripts/exploracio/01_fluxes_exploracio.Rmd", 
       output_format = "html_document",
       output_file = paste('fluxes_exploracio',
                           format(Sys.Date(),'%Y%m%d'),
                           format(Sys.time(),'%H%M'), sep = "_"),
       output_dir = "D:/SISAP/sisap/66Analisis/fluxes/results/exploracio",
       params = list(actualitzar_dades = FALSE,
                     sample = FALSE,
                     skim = TRUE,
                     cG.desc.uni = TRUE)
)
