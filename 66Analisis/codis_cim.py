# -*- coding: utf8 -*-

"""
peticio cie 10
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

class Codis_cim(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_centres()
        self.get_refredats()

    def get_centres(self):
        """."""
        u.printTime("centres")
        self.centres = {}
        sql = """select scs_codi,ics_desc from cat_centres where ep='0208'"""
        for up, desc in u.getAll(sql, 'nodrizas'):
            self.centres[up] = desc
    
    def get_refredats(self):
        """."""
        u.printTime("problemes")
        recomptes = c.Counter()
        sql = "select id_cip, pr_dde, year(pr_dde), pr_cod_ps, pr_up, count(*) \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and \
                            pr_cod_ps ='C01-C96.4' and \
                            pr_data_baixa = 0 group by id_cip,pr_dde,  year(pr_dde), pr_cod_ps, pr_up"
                        
        for id, dat,anys, codi, up, recompte in u.getAll(sql, 'import'):
            if up in self.centres:
                recomptes[anys] += recompte

        upload = []
        for (anys), n in recomptes.items():
            upload.append([anys, n])
        
        print upload

                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Codis_cim()
    
    u.printTime("Final")