***
***
***

# IMPORTACIÓ `r if (params$eval.tabset) '{.tabset}' else ''`

Importació de la informació necessària per la realització d'aquest informe

```{r}
options(java.parameters = "-Xmx32g")

Sys.setenv(TZ = 'Europe/Madrid')
Sys.setenv(ORA_SDTZ = 'Europe/Madrid')

source("C:/Users/ehermosilla/Documents/Keys.R")
```

```{r}
ini.import <- Sys.time()
```

## Dades `r if (params$eval.tabset) '{.tabset}' else ''`

Importació de les dades necessàries per  fer l'anàlisi.

### Activitats `r if (params$eval.tabset) '{.tabset}' else ''`

Importació de la informació de les activitats.

```{r}
if (params$actualitzar_dades) {
  ini <- Sys.time()
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idsisap,
                   password = pwsisap,
                   host = hostsisap,
                   port = 3309,
                   dbname = "nodrizas")
  query <- dbSendQuery(con,
                       statement = "select
                                      *
                                    from
                                      activitats_grupals_nous_professionals_b")
  dt.activitat.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  fi <- Sys.time()
  fi - ini # 
  saveRDS(dt.activitat.raw, "../data/dt.activitat.raw.rds")
  if (params$sample) {dt.activitat.raw <- dt.grupal4.raw[sample(1000)]}
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.activitat.raw <- readRDS("../data/dt.activitat.raw.rds")
  if (params$sample) {dt.activitat.raw <- dt.grupal4.raw[sample(1000)]}
  fi <- Sys.time()
  fi - ini # 
  gc.var <- gc()
}
```

```{r}
names(dt.activitat.raw) <- tolower(names(dt.activitat.raw))
setnames(dt.activitat.raw, paste0('g_', names(dt.activitat.raw)))
```

Informació importada:

- files n = `r nrow(dt.activitat.raw)`
- columnes n = `r ncol(dt.activitat.raw)`
- columnes: `r names(dt.activitat.raw)`

#### Summary `r if (params$eval.tabset) '{.tabset}' else ''`

```{r grupal4 control de qualitat}
# cols <- c('id_cip', 'id_cip_sec', 'grup_num')
# dt.activitat.raw[, (cols) := lapply(.SD, as.character), .SDcols = cols]
# 
# dt.activitat.raw[, (names(dt.activitat.raw)) := lapply(.SD, function(x) ifelse(x == '', NA, x)), .SDcols = names(dt.activitat.raw)]
# 
# cols <- c('id_cip', 'id_cip_sec')
# dt.activitat.raw[, (cols) := NULL, .SDcols = cols]
```

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.activitat.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

***

### Títol `r if (params$eval.tabset) '{.tabset}' else ''`

***

### Pacients `r if (params$eval.tabset) '{.tabset}' else ''`

Importació taula pacients_activitats_nous_professionals.

```{r}
if (params$actualitzar_dades) {
  ini <- Sys.time()
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idsisap,
                   password = pwsisap,
                   host = hostsisap,
                   port = 3309,
                   dbname = "nodrizas")
  query <- dbSendQuery(con,
                       statement = "select
                                      *
                                    from
                                      pacients_activitats_nous_professionals_b")
  dt.pacients.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  fi <- Sys.time()
  fi - ini # 
  saveRDS(dt.pacients.raw, "../data/dt.pacients.raw.rds")
  if (params$sample) {dt.pacients.raw <- dt.pacients.raw[sample(1000)]}
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.pacients.raw <- readRDS("../data/dt.pacients.raw.rds")
  if (params$sample) {dt.pacients.raw <- dt.pacients.raw[sample(1000)]}
  fi <- Sys.time()
  fi - ini # 
  gc.var <- gc()
}
```

```{r}
names(dt.pacients.raw) <- tolower(names(dt.pacients.raw))
setnames(dt.pacients.raw, paste0('p_', names(dt.pacients.raw)))
```

Informació importada:

- files n = `r nrow(dt.pacients.raw)`
- columnes n = `r ncol(dt.pacients.raw)`
- columnes: `r names(dt.pacients.raw)`

#### Summary `r if (params$eval.tabset) '{.tabset}' else ''`

```{r control de qualitat}
# dt.pacients.raw[, (names(dt.pacients.raw)) := lapply(.SD, function(x) ifelse(x == '', NA, x)), .SDcols = names(dt.pacients.raw)]
# 
# cols <- c('id_cip', 'id_cip_sec', 'codi_grup', 'codi_intervencio')
# dt.pacients.raw[, (cols) := lapply(.SD, as.character), .SDcols = cols]
# 
# #dt.rebec.pob.raw[, codi_sector := NULL]
```

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.pacients.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

### Problemes de Salut `r if (params$eval.tabset) '{.tabset}' else ''`

<!-- Importació de la taula de problemes del SISAP. -->

<!-- ```{r} -->
<!-- if (params$actualitzar_dades_problemes) { -->
<!--   ini <- Sys.time() -->
<!--   drv <- dbDriver("MySQL") -->
<!--   con <- dbConnect(drv, -->
<!--                    user = idsisap, -->
<!--                    password = pwsisap, -->
<!--                    host = hostsisap, -->
<!--                    port = portsisap, -->
<!--                    dbname = "import") -->
<!--   query <- dbSendQuery(con, -->
<!--                        statement = "select -->
<!--                                       id_cip, id_cip_sec, -->
<!--                                       pr_cod_ps, -->
<!--                                       pr_dde, -->
<!--                                       pr_dba -->
<!--                                     from -->
<!--                                       problemes a -->
<!--                                     where exists (select 1 from nodrizas.pacients_activitats_nous_professionals b -->
<!--                                                   where a.id_cip_sec = b.id_cip_sec and -->
<!--                                                         procedencia = 'grupal4' and -->
<!--                                                         rol_creador = 'BEN' and -->
<!--                                                         year(data_intervencio) = 2023) and -->
<!--                                                         (pr_dba = 0 or year(pr_dba) >= 2023)") -->
<!--   dt.problemes.raw <- data.table(fetch(query, n = nfetch)) -->
<!--   var.disconnect <- dbDisconnect(con) -->
<!--   fi <- Sys.time() -->
<!--   fi - ini # -->
<!--   saveRDS(dt.problemes.raw, "../data/dt.problemes.raw.rds") -->
<!--   if (params$sample) {dt.problemes.raw <- dt.problemes.raw[sample(1000)]} -->
<!--   gc.var <- gc() -->
<!-- } else { -->
<!--   ini <- Sys.time() -->
<!--   dt.problemes.raw <- readRDS("../data/dt.problemes.raw.rds") -->
<!--   if (params$sample) {dt.problemes.raw <- dt.problemes.raw[sample(1000)]} -->
<!--   fi <- Sys.time() -->
<!--   fi - ini # -->
<!--   gc.var <- gc() -->
<!-- } -->
<!-- ``` -->

<!-- ```{r} -->
<!-- names(dt.problemes.raw) <- tolower(names(dt.problemes.raw)) -->
<!-- ``` -->

<!-- #### Summary {.tabset} -->

<!-- ```{r problemes control de qualitat} -->
<!-- cols <- c('id_cip', 'id_cip_sec') -->
<!-- dt.problemes.raw[, (cols) := lapply(.SD, as.character), .SDcols = cols] -->
<!-- ``` -->

<!-- ```{r} -->
<!-- if (params$skim) { -->
<!--   dt.skim <- data.table(skim(dt.problemes.raw)) -->
<!--   skim_type <- unique(dt.skim[, skim_type]) -->
<!-- } else {print("No s'executa SKIM: volum de dades molt gran")} -->
<!-- ``` -->

<!-- ```{r, child="skim.Rmd"} -->

<!-- ``` -->

### Variables `r if (params$eval.tabset) '{.tabset}' else ''`

<!-- Importació de la taula de VARIABLES de SISAP. -->

<!-- ```{r variables import} -->
<!-- if (params$actualitzar_dades_variables) { -->
<!--   ini <- Sys.time() -->
<!--   drv <- dbDriver("MySQL") -->
<!--   con <- dbConnect(drv, -->
<!--                    user = idsisap, -->
<!--                    password = pwsisap, -->
<!--                    host = hostsisap, -->
<!--                    port = portsisap, -->
<!--                    dbname = "import") -->
<!--   query <- dbSendQuery(con, -->
<!--                        statement = "select -->
<!--                                       id_cip, id_cip_sec, -->
<!--                                       vu_dat_act, -->
<!--                                       vu_val, -->
<!--                                       vu_up, -->
<!--                                       vu_usu -->
<!--                                     from -->
<!--                                       variables a -->
<!--                                     where exists (select 1 from nodrizas.pacients_activitats_nous_professionals b -->
<!--                                                   where a.id_cip_sec = b.id_cip_sec and -->
<!--                                                         procedencia = 'grupal4' and -->
<!--                                                         rol_creador = 'BEN' and -->
<!--                                                         vu_cod_vs = 'EZ5101')") -->
<!--   dt.variables.raw <- data.table(fetch(query, n = nfetch)) -->
<!--   var.disconnect <- dbDisconnect(con) -->
<!--   fi <- Sys.time() -->
<!--   fi - ini # -->
<!--   saveRDS(dt.variables.raw, "../data/dt.variables.raw.rds") -->
<!--   if (params$sample) {dt.variables.raw <- dt.variables.raw[sample(1000)]} -->
<!--   gc.var <- gc() -->
<!-- } else { -->
<!--   ini <- Sys.time() -->
<!--   dt.variables.raw <- readRDS("../data/dt.variables.raw.rds") -->
<!--   if (params$sample) {dt.variables.raw <- dt.variables.raw[sample(1000)]} -->
<!--   fi <- Sys.time() -->
<!--   fi - ini # -->
<!--   gc.var <- gc() -->
<!-- } -->
<!-- ``` -->

<!-- ```{r} -->
<!-- names(dt.variables.raw) <- tolower(names(dt.variables.raw)) -->
<!-- ``` -->

<!-- #### Summary {.tabset} -->

<!-- ```{r variables control de qualitat} -->
<!-- cols <- c('id_cip', 'id_cip_sec') -->
<!-- dt.variables.raw[, (cols) := lapply(.SD, as.character), .SDcols = cols] -->
<!-- ``` -->

<!-- ```{r} -->
<!-- if (params$skim) { -->
<!--   dt.skim <- data.table(skim(dt.variables.raw)) -->
<!--   skim_type <- unique(dt.skim[, skim_type]) -->
<!-- } else {print("No s'executa SKIM: volum de dades molt gran")} -->
<!-- ``` -->

<!-- ```{r, child="skim.Rmd"} -->

<!-- ``` -->

### Master `r if (params$eval.tabset) '{.tabset}' else ''`

Importació de la taula creada per Leo específicament per aquest informe.

```{r variables import}
if (params$actualitzar_dades_variables) {
  ini <- Sys.time()
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idsisap,
                   password = pwsisap,
                   host = hostsisap,
                   port = portsisap,
                   dbname = "import")
  query <- dbSendQuery(con,
                       statement = "with
	catprof as (
		select distinct
			ide_usuari,
			REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
			REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
				case substr(ide_categ_prof, 1,4) when 'MEDI' then 'METG' else substr(ide_categ_prof, 1,4) end,
			'À', 'A'), 'È', 'E'), 'Ì', 'I'), 'Ò', 'O'), 'Ù', 'U'),
      		'Á', 'A'), 'É', 'E'), 'Í', 'I'), 'Ó', 'O'), 'Ú', 'U') as catprof
      	from import.cat_pritb992),
  	grups as (
		select
			catprof,
			codi_sector,
			grup_num,
			grup_codi_up,
			grup_assistents,
			grup_data_alta,
			grup_usu_alta,
			grup_usu_modif,
			grup_data_ini,
			grup_data_fi,
			grup_tipus,
			grup_tipus_activitat,
			grup_activitat,
			grup_hora_ini,
			grup_hora_fi,
			grup_validada,
			grup_num_sessions,
			grup_titol,
			grup_cod_o_ps,
			grup_cod_ps,
			grup_poblacio
		from
			import.grupal4 grups left join catprof on grups.grup_usu_alta = catprof.IDE_USUARI
		-- where
		--	grup_data_alta between date '2023-01-01' and date '2024-10-17'
			),
	sessions as (
		select
			codi_sector,
			inp_num_grup,
			count(distinct inp_num_int) as distinct_pacients,
			count(distinct inp_data) as distinct_sessions,
			count(*) as pacients_sessions,
			min(inp_data) as min_data_sessions,
			max(inp_data) as max_data_sessions
		from import.grupal2
		group by codi_sector, inp_num_grup),
	pacients as (
		select
			g3.codi_sector,
			pagr_num_grup,
			concat(pagr_situacio, case when ates is null then 9 else ates end) as situacion,
			count(*) as n
		from
			import.grupal3 g3 left join nodrizas.assignada_tot at on g3.id_cip_sec = at.id_cip_sec 
		group by codi_sector, pagr_num_grup,  concat(pagr_situacio, case when ates is null then 9 else ates end)),
	pacients_agg as (
		SELECT
		    codi_sector,
		    pagr_num_grup,
		    SUM(CASE WHEN situacion = 'P0' THEN n ELSE 0 END) AS situacion_P0,
		    SUM(CASE WHEN situacion = 'P1' THEN n ELSE 0 END) AS situacion_P1,
		    SUM(CASE WHEN situacion = 'P9' THEN n ELSE 0 END) AS situacion_P9,
		    SUM(CASE WHEN situacion = 'S0' THEN n ELSE 0 END) AS situacion_S0,
		    SUM(CASE WHEN situacion = 'S1' THEN n ELSE 0 END) AS situacion_S1,
		    SUM(CASE WHEN situacion = 'S9' THEN n ELSE 0 END) AS situacion_S9,
		    SUM(CASE WHEN situacion = 'E0' THEN n ELSE 0 END) AS situacion_E0,
		    SUM(CASE WHEN situacion = 'E1' THEN n ELSE 0 END) AS situacion_E1,
		    SUM(CASE WHEN situacion = 'E9' THEN n ELSE 0 END) AS situacion_E9,
		    SUM(CASE WHEN situacion = 'D0' THEN n ELSE 0 END) AS situacion_D0,
		    SUM(CASE WHEN situacion = 'D1' THEN n ELSE 0 END) AS situacion_D1,
		    SUM(CASE WHEN situacion = 'D9' THEN n ELSE 0 END) AS situacion_D9
		FROM
		    pacients
		GROUP BY
		    codi_sector,
		    pagr_num_grup
		)
select
	grups.*,
	distinct_pacients, distinct_sessions, pacients_sessions, min_data_sessions, max_data_sessions,
	situacion_P0, situacion_P1, situacion_P9,
	situacion_S0, situacion_S1, situacion_S9,
	situacion_E0, situacion_E1, situacion_E9,
	situacion_D0, situacion_D1, situacion_D9
from
	grups
	left join sessions
		on grups.codi_sector = sessions.codi_sector
		and grups.grup_num = sessions.inp_num_grup
	left join pacients_agg
		on grups.codi_sector = pacients_agg.codi_sector
		and grups.grup_num = pacients_agg.pagr_num_grup	
")
  dt.master.grupals.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  fi <- Sys.time()
  fi - ini #
  saveRDS(dt.master.grupals.raw, "../data/dt.master.grupals.raw.rds")
  if (params$sample) {dt.master.grupals.raw <- dt.master.grupals.raw[sample(1000)]}
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.master.grupals.raw <- readRDS("../data/dt.master.grupals.raw.rds")
  if (params$sample) {dt.master.grupals.raw <- dt.master.grupals.raw[sample(1000)]}
  fi <- Sys.time()
  fi - ini #
  gc.var <- gc()
}
```

```{r}
names(dt.master.grupals.raw) <- tolower(names(dt.master.grupals.raw))
```

Informació importada:

- files n = `r nrow(dt.master.grupals.raw)`
- columnes n = `r ncol(dt.master.grupals.raw)`
- columnes: `r names(dt.master.grupals.raw)`

#### Summary {.tabset}

```{r}

```

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.master.grupals.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}
```

```{r, child="skim.Rmd"}

```

***
***

## Catàlegs `r if (params$eval.tabset) '{.tabset}' else ''`

### Centres SISAP `r if (params$eval.tabset) '{.tabset}' else ''`

S'importa el catàleg de centres de nodrizas.

```{r cat_centres}

if (params$actualitzar_dades) {
  ini <- Sys.time()
  source("C:/Users/ehermosilla/Documents/Keys.R") 
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idsisap,
                   password = pwsisap,
                   host = hostsisap,
                   port = portsisap,
                   dbname = "nodrizas")
  query <- dbSendQuery(con,
                       statement = "select
                                      ep,
                                      rs,
                                      aga,
                                      amb_codi, amb_desc,
                                      sap_codi, sap_desc,
                                      scs_codi, ics_codi, ics_desc,
                                      abs,
                                      tip_eap,
                                      medea, aquas
                                    from
                                      cat_centres
                                    where
                                      scs_codi != '14423'")
  dt.cat_centres.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  fi <- Sys.time()
  fi - ini # 
  saveRDS(dt.cat_centres.raw, paste0("../data/cat_centres.rds"))
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.cat_centres.raw <- readRDS("../data/cat_centres.rds")
  fi <- Sys.time()
  fi - ini # 
  gc.var <- gc()
}
```

**Informació importada:**
 
 - files n = `r nrow(dt.cat_centres.raw)`
 - columnes n = `r ncol(dt.cat_centres.raw)`
 - columnes: `r names(dt.cat_centres.raw)`

```{r}
setnames(dt.cat_centres.raw, "medea", "medea_c")

cols <- c('abs')
dt.cat_centres.raw[, (cols) := lapply(.SD, as.character), .SDcols = cols]

cols <- c('ep', 'rs', 'aga', 'abs')
setnames(dt.cat_centres.raw, cols, paste0(cols, "_codi"))
names(dt.cat_centres.raw) <- tolower(names(dt.cat_centres.raw))
```

#### Summary `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.cat_centres.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

```{r}
fi <- Sys.time()
fi - ini.import #
gc.var <- gc()
```