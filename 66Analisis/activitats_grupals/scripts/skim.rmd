##### Character

```{r}
if (params$skim) {
  if ("character" %in% skim_type) {
  dt.datatable <- dt.skim[skim_type == "character", skim_variable:character.whitespace]
  datatable(dt.datatable,
            rownames = FALSE,
            filter = "top",
            options = list(ordering = T,
                           pageLength = nrow(dt.datatable),
                           columnDefs = list(list(className = 'dt-center', targets = 0:7)))) %>%
            formatRound(columns = c('complete_rate'), digits = 2)
  } else {print("No hi ha columnes tipus CHARACTER")}
} else {print("No s'executa SKIM: volum de dades molt gran")}  
```

##### Numeric

```{r}
if (params$skim) {
if ("numeric" %in% skim_type) {
    dt.datatable <- dt.skim[skim_type == "numeric", c("skim_variable", 
                                                      "n_missing", "complete_rate",
                                                      "numeric.mean", "numeric.sd",
                                                      "numeric.p0", "numeric.p25", "numeric.p50", "numeric.p75", "numeric.p100", "numeric.hist")]
    datatable(dt.datatable,
              rownames = FALSE,
              filter = "top",
              options = list(ordering = T,
                             pageLength = nrow(dt.datatable),
                             columnDefs = list(list(className = 'dt-center', targets = 0:10)))) %>%
              formatRound(columns = c("complete_rate","numeric.mean", "numeric.sd"), digits = 2)
  } else {print("No hi ha columnes tipus NUMERIC")}
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

##### Factor

```{r}
if (params$skim) {
if ("factor" %in% skim_type) {
    dt.datatable <- dt.skim[skim_type == "factor", c("skim_variable", 
                                                     "n_missing", "complete_rate",
                                                     "factor.ordered", "factor.n_unique", "factor.top_counts")]
    datatable(dt.datatable,
              rownames = FALSE,
              filter = "top",
              options = list(ordering = T,
                             pageLength = nrow(dt.datatable))) %>% 
                             #columnDefs = list(list(className = 'dt-center', targets = 0:6)))) %>%
              formatRound(columns = c("complete_rate"), digits = 3)
  } else {print("No hi ha columnes tipus FACTOR")}
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

##### Data

```{r}
if (params$skim) {
  if ("POSIXct" %in% skim_type) {
      dt.datatable <- dt.skim[skim_type == "POSIXct", c("skim_variable", 
                                                        "n_missing", "complete_rate",
                                                        "POSIXct.min", "POSIXct.max", "POSIXct.median", "POSIXct.n_unique")]
      datatable(dt.datatable,
                rownames = FALSE,
                filter = "top",
                options = list(ordering = T,
                               pageLength = nrow(dt.datatable),
                               columnDefs = list(list(className = 'dt-center', targets = 0:7)))) %>%
                formatRound(columns = c("complete_rate"), digits = 2)
  } else {print("No hi ha columnes tipus DATA")}
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

<!-- ##### Logical -->

<!-- ```{r} -->
<!-- if (params$skim) { -->
<!-- if ("logical" %in% skim_type) { -->
<!--     dt.datatable <- dt.skim[skim_type == "logical", c("skim_variable",  -->
<!--                                                   "n_missing", "complete_rate", -->
<!--                                                   "logical.mean", "logical.count")] -->
<!--     datatable(dt.datatable, -->
<!--               rownames = FALSE, -->
<!--               filter = "top", -->
<!--               options = list(ordering = T, -->
<!--                              pageLength = nrow(dt.datatable), -->
<!--                              columnDefs = list(list(className = 'dt-center', targets = 0:4)))) %>% -->
<!--               formatRound(columns = c('complete_rate', "logical.mean"), digits = 2) -->
<!--   } else {print("No hi ha columnes tipus LOGICAL")} -->
<!-- } else {print("No s'executa SKIM: volum de dades molt gran")}     -->
<!-- ``` -->

<!-- ##### List -->

<!-- ```{r} -->
<!-- if (params$skim) { -->
<!--   if ("list" %in% skim_type) { -->
<!--       dt.datatable <- dt.skim[skim_type == "list", c("skim_variable",  -->
<!--                                                      "n_missing", "complete_rate", -->
<!--                                                      "list.n_unique", "list.min_length", "list.max_length")] -->
<!--       datatable(dt.datatable, -->
<!--                 rownames = FALSE, -->
<!--                 filter = "top", -->
<!--                 options = list(ordering = T, -->
<!--                                pageLength = nrow(dt.datatable), -->
<!--                                columnDefs = list(list(className = 'dt-center', targets = 0:5)))) %>% -->
<!--                 formatRound(columns = c('complete_rate'), digits = 2) -->
<!--   } else {print("No hi ha columnes tipus LIST")} -->
<!-- } else {print("No s'executa SKIM: volum de dades molt gran")}     -->
<!-- ``` -->