***
***

# Anàlisi

```{r}
dt.ana <- dt.flt
```

```{r}
dt.data <- dt.ana[, .N, .(pacient, data)] # Existen pacientes con > 1 visita al día: dt.ana[pacient == "3477940F283A5692A453511CEC20F64531B6CB19",]
setnames(dt.data, c("pacient", "data.uac", "nvis.uac"))
dt.flt.4post[is.na(sisap_servei_class), sisap_servei_class := "Missing"]

dt.ana2 <- merge(dt.data,
                 dt.flt.4post, # Totes les visites
                 by = c("pacient"),
                 all.x = TRUE)
dt.ana2[, time_diff := as.numeric(difftime(data, data.uac, units = "days"))]
dt.time_diff <- dt.ana2[, .N, time_diff][order(time_diff)]

dt.ana2 <- dt.ana2[time_diff >= 1 & time_diff <= 7, ]

pacient.var <- "3477940F283A5692A453511CEC20F64531B6CB19" # Pacient >1 visita al día
pacient.var <- "6008A76C7650839907CF16A30566474BF3AB6185" # Pacient no AUC
pacient.var <- "B6C39B6AF17E57AA937C7CF4EB107DF73E6A57A1" # Pacient AUC amb 1 visita mateix dia
pacient.var <- "CCB9DDFA119B368C8F56378AD350F6F26E752996"
pacient.var <- c("3477940F283A5692A453511CEC20F64531B6CB19",
                 "6008A76C7650839907CF16A30566474BF3AB6185",
                 "B6C39B6AF17E57AA937C7CF4EB107DF73E6A57A1",
                 "CCB9DDFA119B368C8F56378AD350F6F26E752996")

dt.flt1 <- merge(dt.flt,
                 dt.ana2,
                 by.x = c("pacient", "data"),
                 by.y = c("pacient", "data.uac"),
                 all.x = TRUE)
```

```{r}
for (i in pacient.var) {
 datatable(dt.dm[pacient == i,])
 datatable(dt.flt[pacient == i,])
 datatable(dt.ana2[pacient == i,])
 datatable(dt.flt1[pacient == i,])
}
```

```{r}
# Càlcul de denominador

  ## Global
     dt.den <- dt.flt[, .(den = .N), .(sisap_servei_class)]
     dt.den[, .(sum(den)), sisap_servei_class]

  ## EAP
     dt.den.up <- dt.flt[, .(den = .N), .(ics_desc, sisap_servei_class)]
     dt.den.up[, .(sum(den)), sisap_servei_class]

  ## Motiu
     dt.den.motiu <- dt.flt[, motius_nlp.x := motius_nlp]
     dt.den.motiu <- dt.den.motiu %>% separate(col = motius_nlp.x,
                                               into = c("V1","V2","V3","V4","V5","V6","V7","V8","V9","V10","V11","V12"), sep = "; ")

     dt.den.motiu <- melt(dt.den.motiu,
                          id.vars = c("pacient", "data", "sisap_servei_class"),
                          measure.vars = c("V1","V2", "V3", "V4", "V5", "V6", "V7", "V8", "V9", "V10", "V11", "V12"))
     
     dt.den.motiu <- dt.den.motiu[, .(den = .N), .(value, sisap_servei_class)][!is.na(value),]
     dt.den.motiu[, .(sum(den)), sisap_servei_class]
     # MF	239887	OK	239441	
     # INF	168596	<<<<<< 220554 		
     # URG	482 OK 482
     
     # dcast(dt.flt[,.N,.(sisap_servei_class, usuari_text_c)], 
     #       sisap_servei_class ~ usuari_text_c ,
     #       value.var = "N")
     #       
     #       dcast(DT.m1, family_id + age_mother ~ child, value.var = "dob")
     
  ## EAP and Motiu
     dt.den.eapmotiu <- dt.flt[, motius_nlp.x := motius_nlp]
     dt.den.eapmotiu <- dt.den.eapmotiu %>% separate(col = motius_nlp.x,
                                                    into = c("V1","V2","V3","V4","V5","V6","V7","V8","V9","V10","V11","V12"), sep = "; ")

     dt.den.eapmotiu <- melt(dt.den.eapmotiu,
                             id.vars = c("ics_desc", "pacient", "data", "sisap_servei_class"),
                             measure.vars = c("V1","V2", "V3", "V4", "V5", "V6", "V7", "V8", "V9", "V10", "V11", "V12"))
     
     dt.den.eapmotiu <- dt.den.eapmotiu[, .(den = .N), .(ics_desc, value, sisap_servei_class)][!is.na(value),]
     dt.den.eapmotiu[, .(sum(den)), sisap_servei_class]
          
```

### Global

#### Visita a MF

```{r}
 
dt.flt1.agr <- dt.flt1[, .N, .(pacient, data, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.flt1.agr[, .N, .(sisap_servei_class.x, sisap_servei_class.y)][order(sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.datatable[, .(num = sum(N)), .(sisap_servei_class.x, sisap_servei_class.y)]

dt.datatable <- merge(dt.den,
                      dt.datatable,
                      by.x = c("sisap_servei_class"),
                      by.y = c("sisap_servei_class.x"),
                      all.x = TRUE)
dt.datatable[, prop := round((num/den)*100, 2)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

#### Primera visita a MF

```{r}
 
dt.flt1.first <- dt.flt1[order(pacient,data,data.y),]
dt.flt1.first <- dt.flt1.first[,.SD[1], .(pacient, data)]
dt.datatable <- dt.flt1.first[, .N, .(sisap_servei_class.x, sisap_servei_class.y)][order(sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.datatable[, .(num = sum(N)), .(sisap_servei_class.x, sisap_servei_class.y)]

dt.datatable <- merge(dt.den,
                      dt.datatable,
                      by.x = c("sisap_servei_class"),
                      by.y = c("sisap_servei_class.x"),
                      all.x = TRUE)
dt.datatable[, prop := round((num/den)*100, 2)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

### Segons EAP

#### Visita a MF

##### Taula

```{r}
dt.flt1 <- dt.flt1[, motius_nlp := motius_nlp.x]
dt.flt2 <- dt.flt1 %>% separate(col = motius_nlp.x,
                                into = c("V1","V2","V3","V4","V5","V6","V7","V8","V9","V10","V11","V12"), sep = "; ")

dt.flt2.melt.up <- melt(dt.flt2,
                        id.vars = c("ics_desc.x", "pacient", "data", "sisap_servei_class.x", "sisap_servei_class.y"),
                        measure.vars = c("V1","V2", "V3", "V4", "V5", "V6", "V7", "V8", "V9", "V10", "V11", "V12"))

dt.flt2.melt.up[, value2 := value]
dt.flt2.melt.up <- dt.flt2.melt.up %>%  separate(col = value2, into = c("nom0","nom"), sep = ": ")
dt.flt2.melt.up[, c("variable", "nom0") := NULL]

# Afegir variables del catàleg
  dt.flt2.melt.up <- merge(dt.flt2.melt.up,
                           dt.cat.motiunpl.raw,
                           by = c("nom"),
                           all.x = TRUE)

dt.flt1.agr <- dt.flt2.melt.up[, .N, .(ics_desc.x, pacient, data, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.flt1.agr[, .N, .(ics_desc.x, sisap_servei_class.x, sisap_servei_class.y)][order(ics_desc.x, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.datatable[, .(num = sum(N)), .(ics_desc.x, sisap_servei_class.x, sisap_servei_class.y)]

dt.datatable <- merge(dt.den.up,
                      dt.datatable,
                      by.x = c("ics_desc", "sisap_servei_class"),
                      by.y = c("ics_desc.x", "sisap_servei_class.x"),
                      all.x = TRUE)
dt.datatable[, ind2_p := round((num/den)*100, 2)]
datatable(dt.datatable[sisap_servei_class == "INF" & sisap_servei_class.y == "MF",],
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable[sisap_servei_class == "INF" & sisap_servei_class.y == "MF",])))
fwrite(dt.datatable, "../results/ind2_eap.txt", sep = "@") # Validado!!! > Todo OK
```

##### Figures

```{r}
ggplot(dt.datatable[sisap_servei_class == "INF" & sisap_servei_class.y == "MF",],
       aes(x = ind2_p)) +
  geom_histogram(color = "white", fill = "orange") +
  #geom_jitter() +
  theme_minimal()
```

#### Primera visita a MF

```{r}

dt.flt1.first <- dt.flt1[order(pacient, data, data.y),]
dt.flt1.first <- dt.flt1.first[, .SD[1], .(pacient, data)]
dt.flt1.first <- dt.flt1.first[, motius_nlp := motius_nlp.x]
dt.flt2 <- dt.flt1.first %>% separate(col = motius_nlp.x,
                                      into = c("V1","V2","V3","V4","V5","V6","V7","V8","V9","V10","V11","V12"), sep = "; ")

dt.flt2.melt.up.first <- melt(dt.flt2,
                              id.vars = c("ics_desc.x", "pacient", "data", "sisap_servei_class.x", "sisap_servei_class.y"),
                              measure.vars = c("V1","V2", "V3", "V4", "V5", "V6", "V7", "V8", "V9", "V10", "V11", "V12"))

dt.flt2.melt.up.first[, value2 := value]
dt.flt2.melt.up.first <- dt.flt2.melt.up.first %>%  separate(col = value2, into = c("nom0","nom"), sep = ": ")
dt.flt2.melt.up.first[, c("variable", "nom0") := NULL]

# Afegir variables del catàleg
  dt.flt2.melt.up.first <- merge(dt.flt2.melt.up.first,
                                 dt.cat.motiunpl.raw,
                                 by = c("nom"),
                                 all.x = TRUE)

dt.flt1.agr <- dt.flt2.melt.up.first[, .N, .(ics_desc.x, pacient, data, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.flt1.agr[, .N, .(ics_desc.x, sisap_servei_class.x, sisap_servei_class.y)][order(ics_desc.x, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.datatable[, .(num = sum(N)), .(ics_desc.x, sisap_servei_class.x, sisap_servei_class.y)]

dt.datatable <- merge(dt.den.up,
                      dt.datatable,
                      by.x = c("ics_desc", "sisap_servei_class"),
                      by.y = c("ics_desc.x", "sisap_servei_class.x"),
                      all.x = TRUE)
dt.datatable[, ind2_p := round((num/den)*100, 2)]
datatable(dt.datatable[sisap_servei_class == "INF" & sisap_servei_class.y == "MF",],
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable[sisap_servei_class == "INF" & sisap_servei_class.y == "MF",])))
fwrite(dt.datatable, "../results/ind2_eap_first.txt", sep = "@")
```

##### Figures

```{r}
ggplot(dt.datatable[sisap_servei_class == "INF" & sisap_servei_class.y == "MF",],
       aes(x = ind2_p)) +
  geom_histogram(color = "white", fill = "orange") +
  #geom_jitter() +
  theme_minimal()
```

### Segons MOTIU NPL

#### Nom

##### Visita a MF

###### Taula

```{r}
#dt.flt1 <- dt.flt1[, motius_nlp := motius_nlp.x]
dt.flt2 <- dt.flt1 %>% separate(col = motius_nlp.x,
                                into = c("V1","V2","V3","V4","V5","V6","V7","V8","V9","V10","V11","V12"), sep = "; ")

dt.flt2.melt <- melt(dt.flt2,
                        id.vars = c("pacient", "data", "sisap_servei_class.x", "sisap_servei_class.y"),
                        measure.vars = c("V1","V2", "V3", "V4", "V5", "V6", "V7", "V8", "V9", "V10", "V11", "V12"))

dt.flt2.melt[, value2 := value]
dt.flt2.melt <- dt.flt2.melt %>%  separate(col = value2, into = c("nom0","nom"), sep = ": ")
dt.flt2.melt[, c("variable", "nom0") := NULL]

# Afegir variables del catàleg
  dt.flt2.melt <- merge(dt.flt2.melt,
                        dt.cat.motiunpl.raw,
                        by = c("nom"),
                        all.x = TRUE)

dt.flt1.agr <- dt.flt2.melt[, .N, .(value, pacient, data, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.flt1.agr[, .N, .(value, sisap_servei_class.x, sisap_servei_class.y)][order(value, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.datatable[, .(num = sum(N)), .(value, sisap_servei_class.x, sisap_servei_class.y)]

dt.datatable <- merge(dt.den.motiu,
                      dt.datatable,
                      by.x = c("value", "sisap_servei_class"),
                      by.y = c("value", "sisap_servei_class.x"),
                      all.x = TRUE)
dt.datatable[, ind2_p := round((num/den)*100, 2)]

dt.datatable[, ind1_num := den]
dt.datatable[, ind1_den := nrow(dt.flt)]
dt.datatable[, ind1_p := round((ind1_num/ind1_den)*100, 2)]

dt.datatable <- dt.datatable[, .(value, sisap_servei_class, ind1_num, ind1_den, ind1_p, sisap_servei_class.y, num, den, ind2_p)]
datatable(dt.datatable[sisap_servei_class == "INF" & sisap_servei_class.y == "MF",],
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable[sisap_servei_class == "INF" & sisap_servei_class.y == "MF",])))
#fwrite(dt.datatable, "../results/ind2_motiu.txt", sep = "@") # OK Validación
```


##### Primera Visita a MF

###### Taula

```{r}
dt.flt1.first <- dt.flt1[order(pacient, data, data.y),]
dt.flt1.first <- dt.flt1.first[, .SD[1], .(pacient, data)]
dt.flt1.first <- dt.flt1.first[, motius_nlp := motius_nlp.x]
dt.flt2 <- dt.flt1.first %>% separate(col = motius_nlp.x,
                                      into = c("V1","V2","V3","V4","V5","V6","V7","V8","V9","V10","V11","V12"), sep = "; ")

dt.flt2.melt <- melt(dt.flt2,
                        id.vars = c("pacient", "data", "sisap_servei_class.x", "sisap_servei_class.y"),
                        measure.vars = c("V1","V2", "V3", "V4", "V5", "V6", "V7", "V8", "V9", "V10", "V11", "V12"))

dt.flt2.melt[, value2 := value]
dt.flt2.melt <- dt.flt2.melt %>%  separate(col = value2, into = c("nom0","nom"), sep = ": ")
dt.flt2.melt[, c("variable", "nom0") := NULL]

# Afegir variables del catàleg
  dt.flt2.melt <- merge(dt.flt2.melt,
                        dt.cat.motiunpl.raw,
                        by = c("nom"),
                        all.x = TRUE)

dt.flt1.agr <- dt.flt2.melt[, .N, .(value, pacient, data, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.flt1.agr[, .N, .(value, sisap_servei_class.x, sisap_servei_class.y)][order(value, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.datatable[, .(num = sum(N)), .(value, sisap_servei_class.x, sisap_servei_class.y)]

dt.den.motiu.final <- dt.den.motiu[, .(den = sum(den)), value]

dt.datatable <- merge(dt.den.motiu,
                      dt.datatable,
                      by.x = c("value", "sisap_servei_class"),
                      by.y = c("value", "sisap_servei_class.x"),
                      all.x = TRUE)
dt.datatable[, ind2_p := round((num/den)*100, 2)]

dt.datatable[, ind1_num := den]

dt.datatable <- merge(dt.den.motiu.final,
                      dt.datatable,
                      by.x = c("value"),
                      by.y = c("value"),
                      all.x = TRUE)
setnames(dt.datatable, "den.x", "ind1_den")
setnames(dt.datatable, "den.y", "ind2_den")
setnames(dt.datatable, "num", "ind2_num")
dt.datatable[, ind1_p := round((ind1_num/ind1_den)*100, 2)]

dt.datatable <- dt.datatable[, .(value, sisap_servei_class, ind1_num, ind1_den, ind1_p, sisap_servei_class.y, ind2_num, ind2_den, ind2_p)]

datatable(dt.datatable[sisap_servei_class == "INF" & sisap_servei_class.y == "MF",],
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable[sisap_servei_class == "INF" & sisap_servei_class.y == "MF",])))
fwrite(dt.datatable[sisap_servei_class == "INF" & sisap_servei_class.y == "MF",],
       "../results/manolo_global.txt", sep = "@")
```


###### Taula

```{r}
dt.flt1.first <- dt.flt1[order(pacient, data, data.y),]
dt.flt1.first <- dt.flt1.first[, .SD[1], .(pacient, data)]
dt.flt1.first <- dt.flt1.first[, motius_nlp := motius_nlp.x]
dt.flt2 <- dt.flt1.first %>% separate(col = motius_nlp.x,
                                      into = c("V1","V2","V3","V4","V5","V6","V7","V8","V9","V10","V11","V12"), sep = "; ")

dt.flt2.melt <- melt(dt.flt2,
                        id.vars = c("pacient", "data", "sisap_servei_class.x", "sisap_servei_class.y"),
                        measure.vars = c("V1","V2", "V3", "V4", "V5", "V6", "V7", "V8", "V9", "V10", "V11", "V12"))

dt.flt2.melt[, value2 := value]
dt.flt2.melt <- dt.flt2.melt %>%  separate(col = value2, into = c("nom0","nom"), sep = ": ")
dt.flt2.melt[, c("variable", "nom0") := NULL]

# Afegir variables del catàleg
  dt.flt2.melt <- merge(dt.flt2.melt,
                        dt.cat.motiunpl.raw,
                        by = c("nom"),
                        all.x = TRUE)

dt.flt2.melt[, mf := "No"][sisap_servei_class.y == "MF", mf := "Sí"]
dt.flt1.agr <- dt.flt2.melt[, .N, .(categoria, tema, value, pacient, data, sisap_servei_class.x, mf)]
dt.datatable <- dt.flt1.agr[, .N, .(categoria, tema, value, sisap_servei_class.x, mf)][order(value, sisap_servei_class.x, mf)]
dt.datatable <- dt.datatable[, .(num = sum(N)), .(categoria, tema, value, sisap_servei_class.x, mf)]

dt.den.motiu.final <- dt.den.motiu[, .(den = sum(den)), value]

dt.datatable <- merge(dt.den.motiu,
                      dt.datatable,
                      by.x = c("value", "sisap_servei_class"),
                      by.y = c("value", "sisap_servei_class.x"),
                      all.x = TRUE)
dt.datatable[, ind2_p := round((num/den)*100, 2)]

dt.datatable[, ind1_num := den]

dt.datatable <- merge(dt.den.motiu.final,
                      dt.datatable,
                      by.x = c("value"),
                      by.y = c("value"),
                      all.x = TRUE)
setnames(dt.datatable, "den.x", "ind1_den")
setnames(dt.datatable, "den.y", "ind2_den")
setnames(dt.datatable, "num", "ind2_num")
dt.datatable[, ind1_p := round((ind1_num/ind1_den)*100, 2)]
fwrite(dt.datatable,
       "../results/semfyc_leo.txt", sep = "@")

dt.datatable <- dt.datatable[, .(categoria, tema, value, sisap_servei_class, ind1_num, ind1_den, ind1_p, mf, ind2_num, ind2_den, ind2_p)]

datatable(dt.datatable[sisap_servei_class == "INF",],
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable[sisap_servei_class == "INF",])))

```

### Segons EAP i MOTIU

#### Nom

##### Visita a MF

###### Taula

```{r}

setnames(dt.den.eapmotiu, "den", "ind1_den")

dt.ind1.raw <- dt.flt2.melt.up[, .N, .(ics_desc.x, value, sisap_servei_class.x)]
dt.ind1 <- dt.ind1.raw[,{ind1_num = sum(N)
                         .SD[,.(N = N, 
                                ind1_p = round((sum(N)/ind1_num)*100,2)), by = .(sisap_servei_class.x)]
                         }, by = .(ics_desc.x, value)]
setnames(dt.ind1, c("ics_desc.x", "value", "sisap_servei_class.x", "ind1_num", "ind1_p"))


dt.ind2.raw <- dt.flt2.melt.up[, .N, .(ics_desc.x, value, pacient, data, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.ind2.raw[, .N, .(ics_desc.x, value, sisap_servei_class.x, sisap_servei_class.y)][order(ics_desc.x, value, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.datatable[, .(ind2_num = sum(N)), .(ics_desc.x, value, sisap_servei_class.x, sisap_servei_class.y)]

dt.datatable <- merge(dt.ind1,
                      dt.datatable,
                      by.x = c("ics_desc.x", "value", "sisap_servei_class.x"),
                      by.y = c("ics_desc.x", "value", "sisap_servei_class.x"),
                      all.x = TRUE)
dt.datatable[, ind2_den := ind1_num]
dt.datatable[, ind2_p := round((ind2_num/ind2_den)*100, 2)]
fwrite(dt.datatable[!is.na(value),], "../results/ind2_motiueap.txt", sep = "@") # Validació ok >> REVISAR
dt.datatable <- dt.datatable[!is.na(value) & sisap_servei_class.x == "INF" & sisap_servei_class.y == "MF",]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(25)))

```

###### Figuras

####### Indicador 1

```{r, fig.height=70, fig.width=120}
dt.ggplot <- dt.datatable[sisap_servei_class.x == "INF" & sisap_servei_class.y == "MF",]
dt.ggplot.nlp.n <- dt.ggplot[, .N, value]
dt.ggplot.nlp.n[, N_q := cut(N, quantile(N, probs = 0:4/4), include.lowest = TRUE, labels = FALSE)]
dt.ggplot.nlp.n[, N_q := factor(N_q, levels = c(1:4), labels = c("N EAP= 1-3", "N EAP=4-12", "N EAP=13-59", "N EAP > 60"))]

dt.ggplot <- merge(dt.ggplot,
                   dt.ggplot.nlp.n,
                   by = c("value"))

ggplot(dt.ggplot,
       aes(x = ind1_p, y = fct_reorder(value, ind1_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal() +
  facet_wrap(N_q ~ ., ncol = 4, scales = "free_y")
```

```{r, fig.height=12, fig.width=8}
ggplot(dt.ggplot[as.numeric(N_q) == 4,],
       aes(x = ind1_p, y = fct_reorder(value, ind1_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal() +
  ggtitle("N EAP > 60")
```

####### Indicador 2

```{r, fig.height=30, fig.width=8}
ggplot(dt.ggplot,
       aes(x = ind2_p, y = fct_reorder(value, ind2_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal()

ggplot(dt.ggplot[as.numeric(N_q) == 4,],
       aes(x = ind2_p, y = fct_reorder(value, ind2_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal() +
  ggtitle("N EAP > 60")
```

####### Correlació

```{r, fig.height=8, fig.width=15}
#motiu_nlp.list <- unique(dt.datatable[!is.na(value) & sisap_servei_class.x == "INF" & sisap_servei_class.y == "MF" & ind1_num > 80,]$value)
ggplot(dt.ggplot[sisap_servei_class.x == "INF" & sisap_servei_class.y == "MF" & as.numeric(N_q) == 4,],
       aes(ind1_p, ind2_p)) +
  geom_point(color = "orange", alpha = 0.5) +
  theme_minimal() +
  facet_wrap(value ~ .)
```


##### Primera Visita a MF

###### Taula

```{r}
dt.ind1.raw <- dt.flt2.melt.up.first[, .N, .(ics_desc.x, value, sisap_servei_class.x)]
dt.ind1 <- dt.ind1.raw[,{ind1_num = sum(N)
                         .SD[,.(N = N, 
                                ind1_p = round((sum(N)/ind1_num)*100,2)), by = .(sisap_servei_class.x)]
                         }, by = .(ics_desc.x, value)]
setnames(dt.ind1, c("ics_desc.x", "value", "sisap_servei_class.x", "ind1_num", "ind1_p"))


dt.ind2.raw <- dt.flt2.melt.up.first[, .N, .(ics_desc.x, value, pacient, data, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.ind2.raw[, .N, .(ics_desc.x, value, sisap_servei_class.x, sisap_servei_class.y)][order(ics_desc.x, value, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.datatable[, .(ind2_num = sum(N)), .(ics_desc.x, value, sisap_servei_class.x, sisap_servei_class.y)]

dt.datatable <- merge(dt.ind1,
                      dt.datatable,
                      by.x = c("ics_desc.x", "value", "sisap_servei_class.x"),
                      by.y = c("ics_desc.x", "value", "sisap_servei_class.x"),
                      all.x = TRUE)
dt.datatable[, ind2_den := ind1_num]
dt.datatable[, ind2_p := round((ind2_num/ind2_den)*100, 2)]
fwrite(dt.datatable[!is.na(value),], "../results/ind2_motiueap_first.txt", sep = "@") # Validació ok >> REVISAR
dt.datatable <- dt.datatable[!is.na(value) & sisap_servei_class.x == "INF" & sisap_servei_class.y == "MF",]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(25)))

fwrite(dt.datatable[ics_desc.x == "EAP LLEIDA-7 - ONZE DE SETEMBRE",],
       "../results/manolo_eap.txt", sep = "@")
```

####### Excel Manolo

```{r}
# #Tema EAP LLEIDA ONZE SETEMBRE
#   #dt.lleida.onzesetembre <- dt.datatable[ics_desc.x == "EAP LLEIDA-7 - ONZE DE SETEMBRE", ]
# 
#   dt.flt2.melt.up.first[, ics := "ICS"]
#   dt.ind2.lleida.onzesetembre.raw <- dt.flt2.melt.up.first[, .N, .(ics, value, pacient, data, sisap_servei_class.x, sisap_servei_class.y)]
# 
#   
#   dt.datatable <- dt.ind2.lleida.onzesetembre.raw[, .N, .(ics, value, sisap_servei_class.x, sisap_servei_class.y)][order(ics, value, sisap_servei_class.x, sisap_servei_class.y)]
#   dt.datatable <- dt.datatable[, .(ind2_num = sum(N)), .(ics, value, sisap_servei_class.x, sisap_servei_class.y)]
# 
#   dt.datatable <- merge(dt.den.motiu,
#                         dt.datatable,
#                         by.x = c("value", "sisap_servei_class"),
#                         by.y = c("value", "sisap_servei_class.x"),
#                         all.x = TRUE)
#   dt.datatable[, ind1_num := den]
#   dt.datatable[, ind2_den := ind1_num]
#   dt.datatable[, ind2_p := round((ind2_num/ind2_den)*100, 2)]
  
```

###### Figuras

####### Indicador 1

```{r, fig.height=70, fig.width=120}
dt.ggplot <- dt.datatable[sisap_servei_class.x == "INF" & sisap_servei_class.y == "MF",]
dt.ggplot.nlp.n <- dt.ggplot[, .N, value]
dt.ggplot.nlp.n[, N_q := cut(N, quantile(N, probs = 0:4/4), include.lowest = TRUE, labels = FALSE)]
dt.ggplot.nlp.n[, N_q := factor(N_q, levels = c(1:4), labels = c("N EAP= 1-3", "N EAP=4-12", "N EAP=13-59", "N EAP > 60"))]

dt.ggplot <- merge(dt.ggplot,
                   dt.ggplot.nlp.n,
                   by = c("value"))

ggplot(dt.ggplot,
       aes(x = ind1_p, y = fct_reorder(value, ind1_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal() +
  facet_wrap(N_q ~ ., ncol = 4, scales = "free_y")
```


```{r, fig.height=12, fig.width=8}
ggplot(dt.ggplot[as.numeric(N_q) == 4,],
       aes(x = ind1_p, y = fct_reorder(value, ind1_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal() +
  ggtitle("N EAP > 60")

ggplot(dt.ggplot[N >= 200,],
       aes(x = ind1_p, y = fct_reorder(value, ind1_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal() +
  ggtitle("N EAP >= 200")
```

####### Indicador 2

```{r, fig.height=30, fig.width=8}
ggplot(dt.ggplot,
       aes(x = ind2_p, y = fct_reorder(value, ind2_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal()
```

```{r, fig.height=30, fig.width=8}
ggplot(dt.ggplot[as.numeric(N_q) == 4,],
       aes(x = ind2_p, y = fct_reorder(value, ind2_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal() +
  ggtitle("N EAP > 60")
```


```{r, fig.height=8, fig.width=8}
ggplot(dt.ggplot[N >= 200,],
       aes(x = ind2_p, y = fct_reorder(value, ind2_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal() +
  ggtitle("N EAP >= 200")
```

####### Correlació

```{r, fig.height=8, fig.width=15}
#motiu_nlp.list <- unique(dt.datatable[!is.na(value) & sisap_servei_class.x == "INF" & sisap_servei_class.y == "MF" & ind1_num > 80,]$value)
ggplot(dt.ggplot[sisap_servei_class.x == "INF" & sisap_servei_class.y == "MF" & as.numeric(N_q) == 4,],
       aes(ind1_p, ind2_p)) +
  geom_point(color = "orange", alpha = 0.5) +
  theme_minimal() +
  facet_wrap(value ~ .)
```

### Segons EAP i MOTIU (Categoria)

#### Nom

##### Primera Visita a MF

###### Taula

```{r}

dt.ind1.raw <- dt.flt2.melt.up.first[, .N, .(ics_desc.x, categoria, sisap_servei_class.x)]
dt.ind1 <- dt.ind1.raw[,{ind1_num = sum(N)
                         .SD[,.(N = N, 
                                ind1_p = round((sum(N)/ind1_num)*100,2)), by = .(sisap_servei_class.x)]
                         }, by = .(ics_desc.x, categoria)]
setnames(dt.ind1, c("ics_desc.x", "categoria", "sisap_servei_class.x", "ind1_num", "ind1_p"))


dt.ind2.raw <- dt.flt2.melt.up.first[, .N, .(ics_desc.x, categoria, pacient, data, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.ind2.raw[, .N, .(ics_desc.x, categoria, sisap_servei_class.x, sisap_servei_class.y)][order(ics_desc.x, categoria, sisap_servei_class.x, sisap_servei_class.y)]
dt.datatable <- dt.datatable[, .(ind2_num = sum(N)), .(ics_desc.x, categoria, sisap_servei_class.x, sisap_servei_class.y)]

dt.datatable <- merge(dt.ind1,
                      dt.datatable,
                      by.x = c("ics_desc.x", "categoria", "sisap_servei_class.x"),
                      by.y = c("ics_desc.x", "categoria", "sisap_servei_class.x"),
                      all.x = TRUE)
dt.datatable[, ind2_den := ind1_num]
dt.datatable[, ind2_p := round((ind2_num/ind2_den)*100, 2)]
#fwrite(dt.datatable[!is.na(value),], "../results/ind2_motiueap_first.txt", sep = "@") # Validació ok >> REVISAR
dt.datatable <- dt.datatable[!is.na(categoria) & sisap_servei_class.x == "INF" & sisap_servei_class.y == "MF",]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(25)))

```

###### Figuras

####### Indicador 1

```{r, fig.height=8, fig.width=8}
dt.ggplot <- dt.datatable[sisap_servei_class.x == "INF" & sisap_servei_class.y == "MF",]
dt.ggplot.nlp.n <- dt.ggplot[, .N, categoria]
dt.ggplot.nlp.n[, N_q := cut(N, quantile(N, probs = 0:4/4), include.lowest = TRUE, labels = FALSE)]
#dt.ggplot.nlp.n[, .(min=min(N), max=max(N)), N_q]
dt.ggplot.nlp.n[, N_q := factor(N_q, levels = c(1:4), labels = c("N EAP= <=29", "N EAP=96-188", "N EAP=264-268", "N >= 273"))]

dt.ggplot <- merge(dt.ggplot,
                   dt.ggplot.nlp.n,
                   by = c("categoria"))

ggplot(dt.ggplot,
       aes(x = ind1_p, y = fct_reorder(categoria, ind1_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal()
```


```{r, fig.height=8, fig.width=8}
ggplot(dt.ggplot[N >= 200,],
       aes(x = ind1_p, y = fct_reorder(categoria, ind1_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal() +
  ggtitle("N EAP >= 200")
```

####### Indicador 2

```{r, fig.height=8, fig.width=8}
ggplot(dt.ggplot,
       aes(x = ind2_p, y = fct_reorder(categoria, ind2_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal()
```

```{r, fig.height=8, fig.width=8}
ggplot(dt.ggplot[N >= 200,],
       aes(x = ind2_p, y = fct_reorder(categoria, ind2_p))) +
  geom_jitter(color = "orange", alpha = 0.5) +
  geom_boxplot(alpha = 0.5) +
  theme_minimal() +
  ggtitle("N EAP >= 200")
```

####### Correlació

```{r, fig.height=8, fig.width=8}
#motiu_nlp.list <- unique(dt.datatable[!is.na(value) & sisap_servei_class.x == "INF" & sisap_servei_class.y == "MF" & ind1_num > 80,]$value)
ggplot(dt.ggplot[sisap_servei_class.x == "INF" & sisap_servei_class.y == "MF",],
       aes(ind1_p, ind2_p)) +
  geom_point(color = "orange", alpha = 0.5) +
  theme_minimal() +
  facet_wrap(categoria ~ .)
```


```{r}
gc.var <- gc()
```


