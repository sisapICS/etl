***
***

# Descriptiva Univariada

## Taula resum

Taula resum amb la desciptiva de totes les variables disponibles.

```{r, warning=warning.var}
#if (params$cG.desc.uni) {
  df <- dt.flt[, taula := "Visites"]
  res <- compareGroups(taula ~ . -pacient -ep -up
                       , df
                       , method = 1
                       , max.xlev = 100
                       , include.miss = TRUE)
  cT <- createTable(res
                    , show.descr = FALSE
                    , show.all = TRUE
                    , show.p.overall = FALSE
                    , show.n = TRUE
                    )
  export2md(cT,
            format = "html",
            caption = "")
  rm(list = c("df","res","cT"))
#}
```

## Edat

```{r}
ggplot(dt.flt,
       aes(edat)) +
  geom_histogram(, color = 'white', fill = 'orange') +
  theme_minimal()
```

## Servei

```{r}
ggplot(dt.flt,
       aes(servei)) +
  geom_bar(alpha = 0.3) +
  theme_minimal() +
  coord_flip()

dt.datatable <- dt.flt[, .N, servei][order(-N)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```


## Tipus

```{r}
ggplot(dt.flt,
       aes(tipus)) +
  geom_bar(alpha = 0.3) +
  theme_minimal() +
  coord_flip()

dt.datatable <- dt.flt[, .N, tipus][order(-N)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = 25))
```

## Tipus Class

```{r}
ggplot(dt.flt,
       aes(tipus_class)) +
  geom_bar(alpha = 0.3) +
  theme_minimal() +
  coord_flip()

dt.datatable <- dt.flt[, .N, tipus_class][order(-N)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

## Citació

```{r}
# ggplot(dt.flt,
#        aes(citacio)) +
#   geom_bar(alpha = 0.3) +
#   theme_minimal() +
#   coord_flip()

dt.datatable <- dt.flt[, .N, citacio][order(-N)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

## Situació

```{r}
dt.datatable <- dt.flt[, .N, situacio][order(-N)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

## Origen

```{r}
dt.datatable <- dt.flt[, .N, origen][order(-N)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

## Sisap_servei_class

```{r}
ggplot(dt.flt,
       aes(sisap_servei_class)) +
  geom_bar(alpha = 0.3) +
  theme_minimal() +
  coord_flip()

dt.datatable <- dt.flt[, .N, sisap_servei_class][order(-N)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

## Motiu

### Class

```{r}
ggplot(dt.flt,
       aes(motiu_class)) +
  geom_bar(alpha = 0.3) +
  theme_minimal() +
  coord_flip()

dt.datatable <- dt.flt[, .N, motiu_class][order(-N)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

### Prior

<span style="color:red"> Nota: Revisar!!: hi ha motius escrits diferents que són el mateix </span>

```{r}
dt.flt.motiu_prior <- dt.flt
dt.flt.motiu_prior <- dt.flt.motiu_prior[, motiu_prior2 := motiu_prior]
dt.flt.motiu_prior <- dt.flt.motiu_prior %>% separate(col = motiu_prior2, into = c("V1","V2","V3","V4","V5","V6","V7","V8"), sep = ";")
dt.flt.motiu_prior.melt <- melt(dt.flt.motiu_prior,
                                id.vars = c("pacient", "motiu_class"),
                                measure.vars = c("V1","V2", "V3", "V4", "V5", "V6", "V7", "V8"))
dt.flt.motiu_prior.melt <- dt.flt.motiu_prior.melt[value != "",] # S'eliminen els registres buits pq motiu_prior és ";dsfdsf" i crea un registre buit
dt.datatable <- dt.flt.motiu_prior.melt[, .N, .(motiu_class, value)][order(-N)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = 25))
```

## NLP

### NOM

```{r}
# Creació data table Motiu NPL nivellmotiu
  dt.flt.nlp <- dt.flt[!is.na(motius_nlp),]
  dt.flt.nlp <- dt.flt.nlp[, motius_nlp2 := motius_nlp]
  dt.flt.nlp <- dt.flt.nlp %>% separate(col = motius_nlp2, into = c("V1","V2","V3","V4","V5","V6","V7","V8","V9","V10","V11","V12"), sep = "; ")
  dt.flt.nlp.melt <- melt(dt.flt.nlp,
                          id.vars = c("pacient"),
                          measure.vars = c("V1","V2", "V3", "V4", "V5", "V6", "V7", "V8","V9","V10","V11","V12"))
  dt.flt.nlp.melt <- dt.flt.nlp.melt[!is.na(value)]

  
  
# Creació columna motiu npl per afegir la informació del catàleg
  dt.flt.nlp.melt[, value2 := value]
  a <- dt.flt.nlp.melt[, .N, value]
  dt.flt.nlp.melt <- dt.flt.nlp.melt %>%  separate(col = value2, into = c("nom0","nom"), sep = ": ")
  dt.flt.nlp.melt[, c("variable", "nom0") := NULL]

# Afegir variables del catàleg
  dt.flt.nlp.melt <- merge(dt.flt.nlp.melt,
                           dt.cat.motiunpl.raw,
                           by = c("nom"),
                           all.x = TRUE)
```

```{r}

dt.datatable <- dt.flt.nlp.melt[, .(.N, prop = round(.N/nrow(dt.flt.nlp.melt)*100,2)), .(value, `icpc-3`, nom,
                                                                                         categoria, superclase, clase, 
                                                                                         tema_codi, tema, 
                                                                                         subtitle_codi, subtitle_desc
                                                                                         )][order(-N)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = 25))

fwrite(dt.datatable,
       "../results/semfyc_leo2.txt", sep = "@")
```


```{r}
gc.var <- gc()
```


