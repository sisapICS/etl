***
***

# Import

## `r params$arxius[1]`

```{r}

dt <- data.table(read_excel(paste0("D:/SISAP/sisap/66Analisis/programacioxmotius/data/", params$arxiu)))
```

Informació importada:

- files n = `r nrow(dt)`
- columnes n = `r ncol(dt)`
- columnes: `r names(dt)`

```{r}
names(dt) <- tolower(names(dt))
setnames(dt, c("up","up_desc","sap_desc","amb_desc",
               "mes", "any",
               "sisap_servei_class","servei_no_metge","motiu_class",
               "n"))
```

Quantitat de missing:

```{r}
df <- data.frame(dt)
df <- df %>% mutate(taula=params$arxius[1])
res <- compareGroups(taula ~ .
                     , df
                     , max.xlev = 100
                     , method = 4
                     , include.label = FALSE)
export2md(missingTable(res
                       , show.p.overall=FALSE
                       ),
          format = "html",
          caption = "")
```

Control de qüalitat Valors:

```{r}
f.cq.table2(dt)
```

```{r}
dt[, up:=substr(paste0("0000",up), nchar(paste0("0000",up))-4, nchar(paste0("0000",up)))]

dt[, c("up", "up_desc", "sap_desc", "amb_desc", "mes", "any", "sisap_servei_class", "servei_no_metge", "motiu_class") := lapply(.SD, function(x){
  as.factor(x)
  }), .SDcols = c("up", "up_desc", "sap_desc", "amb_desc", "mes", "any", "sisap_servei_class", "servei_no_metge", "motiu_class")]
```

```{r}
gc.var <- gc()
```

