***
***

# Anàlisi Indicador 2

A continuació s'analitza el indicador 2: 

- % de PPM codificat amb servei destí infermeria (n visites amb codi amb servei destí infermeria / n visites amb codi)*100

```{r}
dt.ana <- dt.flt[motiu_class=="CODI",]
```

## Global

```{r}

dt.ana[, global:="Global"]
dt.ana.global <- dt.ana[,{
    den = sum(n)
    .SD[,.(frac=sum(n)/den),by=sisap_servei_class]
},by=.(global, mesyear)]

ggplot(dt.ana.global[sisap_servei_class=="INF",], aes(x = mesyear)) +
  geom_line(aes(y = frac)) +
  scale_y_continuous(labels = scales::percent) +
  labs(caption="Dates: 2020-2022/02\nFont: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "",
       y = "PPM2: % PPM codificat \n amb servei destí infermeria") +  
  theme_minimal() +
  theme(legend.position = "bottom", legend.title = element_blank(), 
        axis.title.x = element_blank(), 
        plot.caption = element_text(size = 8, colour = "grey35")) +
  annotate("rect",
           xmin = as.Date("2020-01-01"), xmax = as.Date("2020-12-31"), 
           ymin = -Inf, ymax = Inf,  fill = "darkred", alpha=.1) +
  annotate("rect",
           xmin = as.Date("2021-01-01"), xmax = as.Date("2021-12-31"), 
           ymin = -Inf, ymax = Inf,  fill = "darkgreen", alpha=.1) +
  annotate("rect",
           xmin = as.Date("2022-01-01"), xmax = as.Date("2022-06-30"), 
           ymin = -Inf, ymax = Inf,  fill = "darkblue", alpha=.1)
```

***

## Estratificat Àmbit

```{r}

dt.ana.plot <- dt.ana[,{
    den = sum(n)
    .SD[,.(frac=sum(n)/den),by=sisap_servei_class]
},by=.(amb_desc, mesyear)]

ggplot(dt.ana.plot[sisap_servei_class=="INF",], aes(x = mesyear)) +
  geom_line(aes(y = frac, color="Percentatge PPM codificat")) +
  geom_line(data=dt.ana.global[sisap_servei_class=="INF",], aes(x=mesyear, y=frac, color="Global ICS")) +
  scale_y_continuous(labels = scales::percent) +
  scale_color_manual(values=c("Percentatge PPM codificat"="black","Global ICS"="darkred")) +
  scale_x_date(breaks = "1 year", date_labels = "%Y", expand = c(0,0)) + 
  labs(caption="Escala: Mateixa per a cada estrat\nDates: 2020-2022/02\nFont: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "",
       y = "PPM2: % PPM codificat \n amb servei destí infermeria") +
  theme_minimal() +
  theme(legend.position = "bottom", legend.title = element_blank(), 
        axis.title.x = element_blank(),
        axis.text.x = element_text(size=7),
        plot.caption = element_text(size = 8, colour = "grey35"),
        strip.text.x = element_text(size = 8)) +
  facet_wrap(. ~ amb_desc, ncol=5) +
  annotate("rect",
           xmin = as.Date("2020-01-01"), xmax = as.Date("2020-12-31"), 
           ymin = -Inf, ymax = Inf,  fill = "darkred", alpha=.1) +
  annotate("rect",
           xmin = as.Date("2021-01-01"), xmax = as.Date("2021-12-31"), 
           ymin = -Inf, ymax = Inf,  fill = "darkgreen", alpha=.1) +
  annotate("rect",
           xmin = as.Date("2022-01-01"), xmax = as.Date("2022-06-30"), 
           ymin = -Inf, ymax = Inf,  fill = "darkblue", alpha=.1)

ggplot(dt.ana.plot[sisap_servei_class=="INF",], aes(x = mesyear)) +
  geom_line(aes(y = frac, color="Percentatge PPM codificat")) +
  geom_line(data=dt.ana.global[sisap_servei_class=="INF",], aes(x=mesyear, y=frac, color="Global ICS")) +
  scale_y_continuous(labels = scales::percent) +
  scale_color_manual(values=c("Percentatge PPM codificat"="black","Global ICS"="darkred")) +
  scale_x_date(breaks = "1 year", date_labels = "%Y", expand = c(0,0)) + 
  labs(caption="Escala: Específica per a cada estrat\nDates: 2020-2022/02\nFont: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "",
       y = "PPM2: % PPM codificat \n amb servei destí infermeria") +
  theme_minimal() +
  theme(legend.position = "bottom", legend.title = element_blank(), legend.text = element_text(size=7),
        axis.title.x = element_blank(),
        axis.text.x = element_text(size=7),
        axis.text.y = element_text(size=7), 
        plot.caption = element_text(size = 8, colour = "grey35"),
        strip.text.x = element_text(size = 6)) +
  facet_wrap(. ~ amb_desc, ncol=5, scales = "free_y") +
  annotate("rect",
           xmin = as.Date("2020-01-01"), xmax = as.Date("2020-12-31"), 
           ymin = -Inf, ymax = Inf,  fill = "darkred", alpha=.1) +
  annotate("rect",
           xmin = as.Date("2021-01-01"), xmax = as.Date("2021-12-31"), 
           ymin = -Inf, ymax = Inf,  fill = "darkgreen", alpha=.1) +
  annotate("rect",
           xmin = as.Date("2022-01-01"), xmax = as.Date("2022-06-30"), 
           ymin = -Inf, ymax = Inf,  fill = "darkblue", alpha=.1)
```

***

## Estratificat UP

```{r}

dt.ana.plot.expand <- expand.grid(unique(dt.ana$any),
                                  unique(dt.ana$up),
                                  unique(dt.ana$sisap_servei_class))

dt.ana.plot <- dt.ana[,{
    den = sum(n)
    .SD[,.(frac=sum(n)/den),by=sisap_servei_class]
},by=.(amb_desc, up, any)]

setnames(dt.ana.plot.expand, c("any","up", "sisap_servei_class"))
dt.ana.plot <- data.table(merge(dt.ana.plot.expand,
                     dt.ana.plot,
                     by.x=c("any","up","sisap_servei_class"),
                     by.y=c("any","up","sisap_servei_class"),
                     all.x = TRUE))

dt.ana.plot <- dt.ana.plot[sisap_servei_class=="INF",]
dt.ana.plot <- dt.ana.plot[is.na(frac), frac:=0]

dt.ambdescup <- dt.ana[, .N, by=.(amb_desc, up)]

dt.ana.plot <- data.table(merge(dt.ana.plot,
                                 dt.ambdescup,
                                 by.x = c("up"),
                                 by.y = c("up"),
                                 all.x = TRUE))[,amb_desc.x:=NULL][,N:=NULL]
setnames(dt.ana.plot, "amb_desc.y", "amb_desc")
```

### Taula descriptiva

```{r}
df <- dt.ana.plot
t <- dt.ana.plot[, .(
                  N = .N,
                  Mitjana = mean(frac*100, na.rm = T),
                  DE = sd(frac*100, na.rm = T),
                  Minim = min(frac*100, na.rm = T),
                  P5 = quantile(frac*100, probs = .05, na.rm = T),
                  P25 = quantile(frac*100, probs = .25, na.rm = T),
                  Mediana = quantile(frac*100, probs = .5, na.rm = T),
                  P75 = quantile(frac*100, probs = .75, na.rm = T),
                  P95 = quantile(frac*100, probs = .95, na.rm = T),
                  Maxim = max(frac*100, na.rm = T)
                  ),
                  by = .(any,amb_desc)][order(any, -Mediana)]

names(t) <- c("Any",
              "",
              "N",
              "Mitjana",
              paste0("DE", footnote_marker_alphabet(1)),
              "Mínim",
              paste0("P5", footnote_marker_alphabet(2)),
              paste0("P25", footnote_marker_alphabet(3)),
              "Mediana",
              paste0("P75", footnote_marker_alphabet(4)),
              paste0("P95", footnote_marker_alphabet(5)),
              "Màxim")
t[Any==2020,] %>%
  kable(digits = 2, row.names = F, escape = F, caption = "", align = c("l", "c", "c", "c", "c", "c", "c", "c"), format.args = list(decimal.mark = ',', big.mark = ".")) %>%
  kable_styling(bootstrap_options = c("hover", "condensed", "responsive"), full_width = T) %>%
  row_spec(0, bold = T, align = "c", background = "#011936", color = "white") %>%
  column_spec(1, bold = F, border_right = F, background = "#D0D5DA") %>%
  footnote(alphabet = c("Desviació Estàndard; ", "Percentil 5; ", "Percentil 25; ", "Percentil 75; ", "Percentil 95; "))

t[Any==2021,] %>%
  kable(digits = 2, row.names = F, escape = F, caption = "", align = c("l", "c", "c", "c", "c", "c", "c", "c"), format.args = list(decimal.mark = ',', big.mark = ".")) %>%
  kable_styling(bootstrap_options = c("hover", "condensed", "responsive"), full_width = T) %>%
  row_spec(0, bold = T, align = "c", background = "#011936", color = "white") %>%
  column_spec(1, bold = F, border_right = F, background = "#D0D5DA") %>%
  footnote(alphabet = c("Desviació Estàndard; ", "Percentil 5; ", "Percentil 25; ", "Percentil 75; ", "Percentil 95; "))

t[Any==2022,] %>%
  kable(digits = 2, row.names = F, escape = F, caption = "", align = c("l", "c", "c", "c", "c", "c", "c", "c"), format.args = list(decimal.mark = ',', big.mark = ".")) %>%
  kable_styling(bootstrap_options = c("hover", "condensed", "responsive"), full_width = T) %>%
  row_spec(0, bold = T, align = "c", background = "#011936", color = "white") %>%
  column_spec(1, bold = F, border_right = F, background = "#D0D5DA") %>%
  footnote(alphabet = c("Desviació Estàndard; ", "Percentil 5; ", "Percentil 25; ", "Percentil 75; ", "Percentil 95; "))
```

### Figura (Box-Plot)

```{r}
ggplot(dt.ana.plot, aes(x=reorder(amb_desc,frac, fun=median), y=frac)) +
  geom_boxplot(fill="darkgreen", alpha=0.3) +
  coord_flip() +
  scale_y_continuous(labels = scales::percent) +
  labs(caption="Escala: Mateixa per a cada estrat\nDates: 2020-2022/02\nFont: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "",
       y = "PPM2: % PPM codificat \n amb servei destí infermeria") +
  theme_minimal() +
  theme(legend.position = "none", legend.title = element_blank(), legend.text = element_blank(),
        axis.title.x = element_blank(),
        axis.text.x = element_text(size=7),
        axis.text.y = element_text(size=7), 
        plot.caption = element_text(size = 8, colour = "grey35"),
        strip.text.x = element_text()) +
  facet_wrap(. ~ any)

ggplot(dt.ana.plot, aes(x=any, y=frac)) +
  geom_boxplot(fill="darkgreen", alpha=0.3) +
  #coord_flip() +
  scale_y_continuous(labels = scales::percent) +
  labs(caption="Escala: Específica per a cada estrat\nDates: 2020-2022/02\nFont: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "",
       y = "PPM2: % PPM codificat \n amb servei destí infermeria") +
  theme_minimal() +
  theme(legend.position = "none", legend.title = element_blank(), legend.text = element_blank(),
        axis.title.x = element_blank(),
        axis.text.x = element_text(size=7),
        axis.text.y = element_text(size=7), 
        plot.caption = element_text(size = 8, colour = "grey35"),
        strip.text.x = element_text(size = 6)) +
  facet_wrap(. ~ amb_desc, scales = "free", ncol=5)
```

### Taula dades

Taula amb les dades de l'indicador per UP.

```{r}
# 00092 Centre amb molt poca N >>> Revisar expand!!! 2022/03/18
dt.xdt <- dt.ana.plot[,frac:=round(frac*100,3)][, .(any, amb_desc, up, frac)]
datatable(dt.xdt[, .(any, amb_desc, up, frac)],
          rownames = FALSE,
          filter = 'top',
          options = list())
```

***

```{r}
gc.var <- gc()
```
