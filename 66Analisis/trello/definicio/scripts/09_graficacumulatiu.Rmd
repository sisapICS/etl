***
***

# Gràfic Acumulatiu

```{r}
# Importar la informació de la bbdd permanet
source("C:/Users/ehermosilla/Documents/Keys.R")
drv <- dbDriver("MySQL")
con <- dbConnect(drv,
                 user = idpermanent,
                 password = pwpermanent,
                 host = hostpermanent,
                 port = portpermanent,
                 dbname = dbpermanent)
query <- dbSendQuery(con,
                     statement = "select * from trello_definicio")
dt <- data.table(fetch(query, n = nfetch))
dt[, data := as.Date(data)]

# # Importar la informació de l'arxiu rds amb nova informació
# dt.rds <- readRDS("../data/dt.graficAcumulatiu.trello.20230310.rds")
# setnames(dt.rds, c("data", "dia", "backlog", "doing_def", "doing_exp", "doing_vis", "doing_ana", "stuck", "done"))
# dt.rds <- dt.rds[dia == 5,]
# 
# # Afegior registres a la taula de permanet
# dbWriteTable(con,
#              "trello_definicio", 
#              dt.rds, 
#              row.names =  FALSE,
#              append = TRUE)
```

```{r}

#dt.graficAcumulatiu.excel <- read_excel("../data/kanban - gràfic acumulat.xlsx",
#                                        sheet = "data23")
#names(dt.graficAcumulatiu.excel)

dt.graficAcumulatiu.trello <- rbind(dt.graficAcumulatiu.backlog,
                                    dt.graficAcumulatiu.d,
                                    dt.graficAcumulatiu.e,
                                    dt.graficAcumulatiu.a,
                                    dt.graficAcumulatiu.v,
                                    dt.graficAcumulatiu.stuck,
                                    dt.graficAcumulatiu.done)
dt.graficAcumulatiu.trello
dt.graficAcumulatiu.trello1 <- dcast(dt.graficAcumulatiu.trello, date ~ unit, value.var = "n_cards")

#names(dt)
setnames(dt.graficAcumulatiu.trello1,
         c("data", "doing_ana", "backlog", "doing_def", "done", "doing_exp", "stuck", "doing_vis"))
dt.graficAcumulatiu.trello1[, dia := NA]
dt.graficAcumulatiu.trello1[, dia := as.integer(dia)]
dt.graficAcumulatiu.trello1 <- dt.graficAcumulatiu.trello1[, c("data", "dia",
                                                               "backlog", 
                                                               "doing_def", "doing_exp", "doing_vis", "doing_ana",
                                                               "stuck", "done")]
dt.graficAcumulatiu.trello <- rbind(dt,
                                    dt.graficAcumulatiu.trello1) %>% data.table()
dt.graficAcumulatiu.trello <- dt.graficAcumulatiu.trello[order(data,dia),][, dia_lag := lag(dia, n = 1)][is.na(dia), dia := dia_lag + 1][,dia_lag := NULL]
dt.topermanent <- dt.graficAcumulatiu.trello[data == Sys.Date(),]


# Fem backup de la taula de permanent

    query <- "CREATE TABLE trello_definicio_backup2 like trello_definicio_backup;"
    results <- dbSendQuery(con, query)
    dbClearResult(results)

  # Insert dades        
    query <- "INSERT INTO trello_definicio_backup2 SELECT * FROM trello_definicio_backup;"
    results <- dbSendQuery(con, query)
    dbClearResult(results)
    
    query <- "DROP TABLE trello_definicio_backup;"
    results <- dbSendQuery(con, query)
    dbClearResult(results)
    
  # Afegir registre a la taula de permanent
    
    query <- "select max(data) as data from trello_definicio;"
    results <- dbGetQuery(con, query)
    if (results[1,1] != dt.topermanent[, data]) {dbWriteTable(con,
                                                              "trello_definicio", 
                                                              dt.topermanent, 
                                                              row.names =  FALSE,
                                                              append = TRUE)}

  # Backup en formato rds en local
    saveRDS(dt.graficAcumulatiu.trello,
            "../data/dt.graficAcumulatiu.trello.rds")
    dt.graficAcumulatiu.trello <- readRDS("../data/dt.graficAcumulatiu.trello.rds")

    query <- "CREATE TABLE trello_definicio_backup like trello_definicio;"
    results <- dbSendQuery(con, query)
    dbClearResult(results)

  # Insert dades        
    query <- "INSERT INTO trello_definicio_backup SELECT * FROM trello_definicio;"
    results <- dbSendQuery(con, query)
    dbClearResult(results)

    query <- "DROP TABLE trello_definicio_backup2;"
    results <- dbSendQuery(con, query)
    dbClearResult(results)
    



dt.graficAcumulatiu.trello.long <- melt(dt.graficAcumulatiu.trello,
                                        id.vars = c("data", "dia"),
                                        measure.vars = c("backlog", 
                                                         "doing_def", "doing_exp", "doing_vis", "doing_ana",
                                                         "stuck", "done"))
dt.graficAcumulatiu.trello.long <- dt.graficAcumulatiu.trello.long[, variable := factor(variable,
                                                                                        levels = c("done",
                                                                                                   "stuck",
                                                                                                   "doing_ana",
                                                                                                   "doing_vis",
                                                                                                   "doing_exp",
                                                                                                   "doing_def",
                                                                                                   "backlog"))]
ggplot(dt.graficAcumulatiu.trello.long,
       aes(x = data, y = value, fill = variable)) + 
    geom_area() +
    theme_minimal()

ggplot(dt.graficAcumulatiu.trello.long[year(data) >= 2023,],
       aes(x = data, y = value, fill = variable)) + 
    geom_area() +
    theme_minimal()

ggplot(dt.graficAcumulatiu.trello.long[variable != "done" & data >= Sys.Date() - 365,],
       aes(x = data, y = value, fill = variable)) + 
    geom_area() +
    theme_minimal()

ggplot(dt.graficAcumulatiu.trello.long[variable != "done" & year(data) >= 2023,],
       aes(x = data, y = value, fill = variable)) + 
    geom_area() +
    theme_minimal()
```

```{r}
var.disconnect <- dbDisconnect(con)
```

```{r}
gc.var <- gc()
```

