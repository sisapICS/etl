***
***

# Gràfic IN/OUT

Gràfic que compara el número de tajetes que han entrat amb el número de tarjetes tancades

```{r}

dt.ggplot.inout <- dt.cards.flt[, .(idList, id, create.year, create.yearmonth, done.yearmonth)]
dt.ggplot.inout.in <- dt.ggplot.inout[, .N, .(create.year, create.yearmonth)][order(-create.yearmonth)]
setnames(dt.ggplot.inout.in, c("year", "yearmonth", "n.in"))
dt.ggplot.inout.out <- dt.ggplot.inout[, .N, done.yearmonth][order(-done.yearmonth)]
setnames(dt.ggplot.inout.out, c("yearmonth", "n.out"))
dt.ggplot.inout <- merge(dt.ggplot.inout.in,
                         dt.ggplot.inout.out,
                         by = "yearmonth")
dt.ggplot.inout <- dt.ggplot.inout[, .(year, yearmonth, n.in, n.out)]
dt.ggplot.inout[, inout.dif := n.out - n.in]
dt.ggplot.inout[inout.dif < 0, ingt := "Més entrades que sortides"][inout.dif > 0, ingt := "Més sortides que entrades"]

ggplot(dt.ggplot.inout[!is.na(yearmonth),],
       aes(x = inout.dif, y = yearmonth, fill = ingt)) +
  geom_bar(stat = "identity") +
  theme_minimal() +
  xlab("Diferències de entrades i sortides (N Sortides - N Entrades)") +
  ylab("") +
  theme(legend.position = "bottom") +
  scale_fill_discrete(name = "")

ggplot(dt.ggplot.inout[year == 2023 & !is.na(yearmonth),],
       aes(x = inout.dif, y = yearmonth, fill = ingt)) +
  geom_bar(stat = "identity") +
  theme_minimal() +
  xlab("Diferències de entrades i sortides (N Sortides - N Entrades)") +
  ylab("") +
  theme(legend.position = "bottom") +
  scale_fill_discrete(name = "")
```



```{r}
gc.var <- gc()
```

