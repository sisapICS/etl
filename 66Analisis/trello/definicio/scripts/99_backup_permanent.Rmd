# Backup a Permanent

Backup de les dades del ggogle drive de Mireia al MySQL de SISAP, bbdd Permanent, per desprè actualitzar les dades.


```{r library, warning=warning.var, message=message.var}

# Importació
  suppressWarnings(suppressPackageStartupMessages(library('readxl')))
  suppressWarnings(suppressPackageStartupMessages(library('RMySQL')))
```

```{r importacio}
# Dades 2022
# Importem les dades de l'arxiu excel de google drive
dt.2022 <- data.table(read_excel("../data/kanban - gràfic acumulat.xlsx",
                                 sheet = "data22",
                                 range = 'A1:I59'))
#names(dt)

# Modifiquem els noms de les columnes per no tenir problemes al crear la taula a permanent
setnames(dt.2022, c("data", "dia", "backlog", "doing_def", "doing_exp", "doing_vis", "doing_ana", "stuck", "done"))
#dt

# Dades 2022
# Importem les dades de l'arxiu excel de google drive
dt.2023 <- data.table(read_excel("../data/kanban - gràfic acumulat.xlsx",
                                 sheet = "data23"))
#names(dt)

# Modifiquem els noms de les columnes per no tenir problemes al crear la taula a permanent
setnames(dt.2023, c("data", "dia", "backlog", "doing_def", "doing_exp", "doing_vis", "doing_ana", "stuck", "done"))

# Juntar les dades dels diferents anys
dt <- rbind(dt.2022,
            dt.2023)
#dt
```

```{r conexio}
source("C:/Users/ehermosilla/Documents/Keys.R")
drv <- dbDriver("MySQL")
con <- dbConnect(drv,
                 user = idsisap,
                 password = pwsisap,
                 host = hostsisap,
                 port = portsisap,
                 dbname = "permanent")
```

```{r creació taula}
#names(dt)
query <- "CREATE TABLE 
            trello_definicio (data DATE,
                              dia INT,
                              backlog INT,
                              doing_def INT,
                              doing_exp INT,
                              doing_vis INT,
                              doing_ana INT,
                              stuck INT,
                              done INT
                              );"

# Enviar query a MuSQL per la seva execució
results <- dbSendQuery(con, query)
dbClearResult(results)
```

```{r}
dbWriteTable(con,
             "trello_definicio", 
             dt, 
             row.names =  FALSE,
             append = TRUE)
var.disconnect <- dbDisconnect(con)
```

```{r}
gc.var <- gc()
```

