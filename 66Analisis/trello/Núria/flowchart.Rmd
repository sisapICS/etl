
# Flowchart

```{r}
#### TAULA vacunes_mancants
 grViz("digraph diagraph {

      node[fontname = Helvetica,
           fontcolor = black,
           shape = box,
           width = 1,
           style = filled,
           fillcolor = '#F49097']
       
       '@@1'
       '@@2'
       '@@3'
       '@@4'
       '@@5'
       '@@6'
       '@@7'
       '@@8'
      
       
       edge []
       '@@1' -> '@@2';
       '@@2' -> '@@3';
       '@@1' -> '@@4';
       '@@1' -> '@@5';
       '@@4' -> '@@6';
       '@@5' -> '@@7';
       '@@6' -> '@@8';
       
     
      {rank=same; '@@2' -> '@@1'}
   

       }
    
      [1]: paste0('PRE')
      [2]: paste0('Es necessita més informació per prioritzar')
      [3]: paste0('Descartats')
      [4]: paste0('Bklg')
      [5]: paste0('PRE EXPLOTACIÓ')
      [6]: paste0('In progress: Definició')
      [7]: paste0('Preparat per: Explotació')
      [8]: paste0('Preparat per: Visualització')

     ")



```



