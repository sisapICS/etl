
```{r}
my_token <- get_token(app = 'TrelloR', 
                      key = "f4680901fcb92ffe0718f64670a00fe8",
                      secret = "84c4ee0c5efa1fe66cc60f4a609d6d64850dbb7524c638df74d7b190040317c5",
                      expiration = "never")
```

```{r}
load("TrelloToken.RData")

```


# Taulell

```{r}
dt.boards <- data.table(get_my_boards(my_token))

# Boards SISAP: 56dffc70661e78d9fbf62768
dt.board.definicio <- dt.boards[id == "56e7d9e1b66564c1f6f318ba"]  
# url = "https://trello.com/b/O2OOLgVb/agenda"
# board.id = get_id_board(url, token = my_token)
board.id = "56e7d9e1b66564c1f6f318ba"
```

# Llistes

```{r Lists, message = FALSE}

dt.lists = data.table(get_board_lists("57dbc7ed567da528a7745592", token = my_token))
```

# Targetes

```{r}
dt.cards <- data.table(get_board_cards(board.id, token = my_token, limit = 5000))
```
# Etiquetes

```{r labels}
dt.labels <- data.table(get_board_labels(board.id, token = my_token, limit = 1000))
```


# Persones

```{r members}
dt.members <- data.table(get_board_members(board.id, token = my_token))
```
# Accions

```{r}
dt.actions <- data.table(get_board_actions(board.id, token = my_token, limit = 10000, paging = TRUE, ))
```

