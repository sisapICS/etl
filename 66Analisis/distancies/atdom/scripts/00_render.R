
library('rmarkdown')


render("D:/SISAP/sisap/66Analisis/distancies/atdom/scripts/01_distancies_atdom.Rmd", 
       output_format = "html_document",
       output_file = paste('distancies_atdom',
                           format(Sys.Date(),'%Y%m%d'),
                           format(Sys.time(),'%H%M'), sep = "_"),
       output_dir = "D:/SISAP/sisap/66Analisis/distancies/atdom/results/",
       params = list(actualitzar_dades = FALSE,
                     sample = FALSE,
                     skim = TRUE,
                     cG.desc.uni = TRUE)
)
