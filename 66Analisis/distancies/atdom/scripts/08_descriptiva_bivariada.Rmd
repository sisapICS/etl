***
***

# Descriptiva Bivariada

## Regió Sanitaria

```{r, warning=warning.var}
dt.datatable <- dt.dist.flt[, .(n_distancies = .N,
                                length_driving_min = min(length_driving, na.rm = TRUE),
                                length_driving_max = max(length_driving, na.rm = TRUE),
                                length_walking_min = min(length_walking, na.rm = TRUE),
                                length_walking_max = max(length_walking, na.rm = TRUE)),
                       .(regio_sanitaria)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

### Distàncies

#### Cotxe

```{r, fig.height=65, fig.width=50}
#dt.flt[, .(m_dis = median(length_driving)), des_cap][order(-m_dis)]
# dt.gglot <- dt.dist.flt[length_driving > 0 & assig_resi == "Sí", ]
# dt.datatable <- merge(dt.cat_centres_exadata.raw[, .(up_cod, up_des)],
#                       dt.gglot,
#                       by.x = "up_cod",
#                       by.y = "up",
#                       all = TRUE)
# dt.datatable[, up_fig := paste0(up_cod, " - ", up_des)]
# ggplot(dt.datatable,
#        aes(x = fct_reorder(up_fig, length_driving, median), y = length_driving)) +
#   geom_jitter(alpha = 0.7, color = "orange") +
#   geom_boxplot(outlier.shape = NA, alpha = 0.3) +
#   facet_wrap(regio_sanitaria ~ ., scales = "free_y") +
#   theme_minimal() +
#   coord_flip() +
#   ggtitle(paste0("EAP Assignació = EAP Residencia", " N=", nrow(dt.datatable)))
```

```{r, fig.height=65, fig.width=50}
#dt.flt[, .(m_dis = median(length_driving)), des_cap][order(-m_dis)]
# dt.gglot <- dt.dist.flt[length_driving > 0 & dist_resi == "Sí", ]
# dt.datatable <- merge(dt.cat_centres_exadata.raw[, .(up_cod, up_des)],
#                       dt.gglot,
#                       by.x = "up_cod",
#                       by.y = "up",
#                       all = TRUE)
# dt.datatable[, up_fig := paste0(up_cod, " - ", up_des)]
# ggplot(dt.datatable,
#        aes(x = fct_reorder(up_fig, length_driving, median), y = length_driving)) +
#   geom_jitter(alpha = 0.7, color = "orange") +
#   geom_boxplot(outlier.shape = NA, alpha = 0.3) +
#   facet_wrap(regio_sanitaria ~ ., scales = "free_y") +  
#   theme_minimal() +
#   coord_flip() +
#   ggtitle(paste0("EAP Distancia = EAP Residencia", " N=", nrow(dt.datatable)))
```

```{r}
gc.var <- gc()
```


