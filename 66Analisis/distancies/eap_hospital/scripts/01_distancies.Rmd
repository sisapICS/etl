---
title: "DISTANCIES: EAP -> HOSPITAL"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
  word_document:
    toc: yes
    toc_depth: '6'
params:
  actualitzar_dades: TRUE
  sample: FALSE
  skim: TRUE
  cG.desc.uni: TRUE
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

```{r parametres de conexió}
source("C:/Users/ehermosilla/Documents/Keys.R")
```

```{r parametres markdown}
inici <- Sys.time()

nfetch <- -1

warning.var <- FALSE
message.var <- FALSE

eval.fig.var <- FALSE # Flag per eliminar figures del informe

eval.library.var <- TRUE
eval.functions.var <- FALSE

eval.import.var <- TRUE
eval.datamanager.var <- TRUE

eval.flowchart.var <- TRUE

eval.univariada.var <- TRUE
eval.bivariada.var <- TRUE

eval.analisi.var <- TRUE

eval.annex.var <- FALSE

eval.timeexecution.var <- TRUE

var.fig.height <- 4
var.fig.width <- 6
```

```{r library, child="02_library.Rmd", eval=eval.library.var}

```

```{r functions, child="03_functions.Rmd", eval=eval.functions.var, include=FALSE}

```

```{r import, child="04_import.Rmd", eval=eval.import.var}

```

```{r datamanager, child="05_datamanager.Rmd", eval=eval.datamanager.var}

```

```{r flowchart, child="06_flowchart.Rmd", eval=eval.flowchart.var}

```

```{r univariada, child="07_descriptiva_univariada.Rmd", eval=eval.univariada.var}

```

```{r bivariada, child="08_descriptiva_bivariada.Rmd", eval=eval.bivariada.var}

```

```{r analisi, child="09_analisi.Rmd", eval=eval.analisi.var}

```

```{r annex, child="annex.Rmd", eval=eval.annex.var}

```

***
***

```{r time execution, eval=eval.timeexecution.var}

final <- Sys.time()
print(round(final - inici,2))
```
