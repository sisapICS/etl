***
***

# Data Manager {.tabset}

## Dades {.tabset}

### Distàncies {.tabset}

```{r}

dt.distancies.dm <- dt.distancies.raw
```

#### Coordenades

```{r, echo = TRUE}
names(dt.distancies.dm)
#[1] "c_any_assegurat"  "c_cip"            "c_up_territorial" "te_coordenades"   "nivell_codi"      "nivell_nom"       "mapa_codi"        "mapa_nom"        
#[9] "mapa_persona"     "mapa_centre"      "mapa_tot"         "te_walk"          "length_walking"   "te_drive"         "length_driving" 

#c_up_territorial
dt.distancies.dm[, te_coordenades := factor(te_coordenades,
                                            levels = c(0,1),
                                            labels = c('No té coordenades','Té coordenades'))]
```

#### Walk

```{r, echo = TRUE}
dt.distancies.dm[, te_walk := factor(te_walk,
                                     levels = c(0,1),
                                     labels = c('No té distància a peu','Té distància a peu'))]
dt.distancies.dm[, .N, te_walk]
dt.dist.agr.walk <- dt.distancies.dm[, .N, length_walking] # Valor NA != 0 , 0 es que el proceso devuelve 0, lo que significaria que vive a 0 metros del EAP
#dt.dist.agr.walk

# dt.distancies.dm <- dt.distancies.dm[length_walking == 0, length_walking := NA] <---------------------------------------OJO

dt.distancies.dm <- dt.distancies.dm[length_walking == 0, length_walking_c := "0"]
dt.distancies.dm <- dt.distancies.dm[length_walking > 0 & length_walking <= 2500, length_walking_c := "1-2500"]
dt.distancies.dm <- dt.distancies.dm[length_walking > 2500 & length_walking <= 10000, length_walking_c := "2500-10000"]
dt.distancies.dm <- dt.distancies.dm[length_walking > 10000 & length_walking <= 30000, length_walking_c := "10000-30000"]
dt.distancies.dm <- dt.distancies.dm[length_walking > 30000, length_walking_c := ">30000"]
dt.distancies.dm[, length_walking_c := factor(length_walking_c,
                                              levels = c("0", "1-2500", "2500-10000", "10000-30000", ">30000"))]
dt.distancies.dm[, .N, length_walking_c][order(length_walking_c)]
```

#### Drive

```{r, echo = TRUE}
dt.distancies.dm[, te_drive := factor(te_drive,
                                      levels = c(0,1),
                                      labels = c('No té distància en cotxe','Té distància en cotxe'))]
dt.distancies.dm[, .N, te_drive]
dt.dist.agr.drive <- dt.distancies.dm[, .N, length_driving] # Valor NA != 0 , 0 es que el proceso devuelve 0, lo que significaria que vive a 0 metros del EAP
#dt.dist.agr.drive

dt.distancies.dm <- dt.distancies.dm[length_driving == 0, length_driving_c := "0"]
dt.distancies.dm <- dt.distancies.dm[length_driving > 0 & length_driving <= 2500, length_driving_c := "1-2500"]
dt.distancies.dm <- dt.distancies.dm[length_driving > 2500 & length_driving <= 10000, length_driving_c := "2500-10000"]
dt.distancies.dm <- dt.distancies.dm[length_driving > 10000 & length_driving <= 30000, length_driving_c := "10000-30000"]
dt.distancies.dm <- dt.distancies.dm[length_driving > 30000, length_driving_c := ">30000"]
dt.distancies.dm[, length_driving_c := factor(length_driving_c,
                                         levels = c("0", "1-2500", "2500-10000", "10000-30000", ">30000"))]
dt.distancies.dm[, .N, length_driving_c][order(length_driving_c)]
```

```{r, echo = TRUE}
dt.datatable <- dt.distancies.dm[, .N, .(length_walking_c, length_driving_c)][order(length_walking_c, length_driving_c)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

#### Nivell

```{r, echo = TRUE}
dt.dist.agr.nivell_codi <- dt.distancies.dm[, .N, nivell_codi]
dt.dist.agr.nivell_codi

dt.dist.agr.nivell_nom <- dt.distancies.dm[, .N, nivell_nom]
dt.dist.agr.nivell_nom
```

#### Mapa

```{r, echo = TRUE}
dt.dist.agr.mapa_codi <- dt.distancies.dm[, .N, mapa_codi]
dt.dist.agr.mapa_codi

dt.dist.agr.mapa_nom <- dt.distancies.dm[, .N, mapa_nom]
dt.dist.agr.mapa_nom

dt.distancies.dm[, mapa_persona := factor(mapa_persona,
                                          levels = c(0,1),
                                          labels = c("No", "Sí"))]
dt.dist.agr.mapa_persona <- dt.distancies.dm[, .N, mapa_persona]
dt.dist.agr.mapa_persona

dt.distancies.dm[, mapa_centre := factor(mapa_centre,
                                         levels = c(0,1),
                                         labels = c("No", "Sí"))]
dt.dist.agr.mapa_centre <- dt.distancies.dm[, .N, mapa_centre]
dt.dist.agr.mapa_centre

dt.distancies.dm[, mapa_tot := factor(mapa_tot,
                                      levels = c(0,1),
                                      labels = c("No", "Sí"))]
dt.dist.agr.mapa_tot <- dt.distancies.dm[, .N, mapa_tot]
dt.dist.agr.mapa_tot
```

***

### Llocs de difícil cobertura {.tabset}

```{r}
dt.llocdificilcoberura.dm <- dt.llocdificilcoberura.raw
```

```{r, echo = TRUE}
dt.llocdificilcoberura.dm[, dificil_cobertura_c := factor(dificil_cobertura_c,
                                                          levels = c("No", "Low", "Medium", "High"))]

dt.llocdificilcoberura.dm[dificil_cobertura_c %in% c("No", "Low"), dificil_cobertura_c2 := "No-Low"]
dt.llocdificilcoberura.dm[dificil_cobertura_c %in% c("Medium", "High"), dificil_cobertura_c2 := "Medium-High"]
dt.llocdificilcoberura.dm[, dificil_cobertura_c2 := factor(dificil_cobertura_c2,
                                                           levels = c("No-Low", "Medium-High"))]

dt.llocdificilcoberura.dm[, dificil_cobertura := factor(dificil_cobertura,
                                                        levels = c("No", "Sí"))]
```

***
***

## Catàlegs {.tabset}

### Centres SISAP {.tabset}

```{r}
dt.cat_centres.dm <- dt.cat_centres.raw
```

```{r, echo = TRUE}

dt.cat_centres.dm[, medea_c :=  factor(medea_c,
                                       levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"))]

dt.cat_centres.dm[, medea_c2 := medea_c]
dt.cat_centres.dm[medea_c %in%  c("1U", "2U", "3U", "4U"), medea_c2 := "U"]
dt.cat_centres.dm[, medea_c2 :=  factor(medea_c2,
                                        levels = c("0R", "1R", "2R", "U"))]
```


***
***

## Afegir informació {.tabset}

#### Catàlegs

```{r}
dt.distancies.dm <- merge(dt.distancies.dm,
                          dt.cat_centres.dm,
                          by.x = c("c_up_territorial"),
                          by.y = c("scs_codi"),
                          all.x = TRUE)
# 
# dt.dm <- merge(dt.dm,
#                dt.cat_centres_exadata.raw,
#                by.x = c("up_cap"),
#                by.y = c("up_cod"),
#                all.x = TRUE)
```

#### LLocs de difícil cobertura

```{r}
dt.distancies.dm <- merge(dt.distancies.dm,
                          dt.llocdificilcoberura.dm[, .(ics_codi, dificil_cobertura, dificil_cobertura_c, dificil_cobertura_c2)],
                          by = "ics_codi",
                          all.x = TRUE)
# setkey(dt.poblacio.raw, 'cip')
# dt.dist.dm <- merge(dt.distancies.dm,
#                     dt.poblacio.raw[, .(cip, hash, edat, sexe, situacio, situacio_desc, data_situacio, data_defuncio, nacionalitat)],
#                     by.x = c("c_cip"),
#                     by.y = c("cip"),
#                     all.x = TRUE)
```

```{r data manager gc}
gc.var <- gc()
```

