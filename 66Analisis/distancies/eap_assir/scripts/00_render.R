
library('rmarkdown')


render("D:/SISAP/sisap/66Analisis/distancies/eap_hospital/scripts/01_distancies.Rmd", 
       output_format = "html_document",
       output_file = paste('distancies',
                           format(Sys.Date(),'%Y%m%d'),
                           format(Sys.time(),'%H%M'), sep = "_"),
       output_dir = "D:/SISAP/sisap/66Analisis/distancies/eap_hospital/results/",
       params = list(actualitzar_dades = FALSE,
                     sample = FALSE,
                     skim = TRUE,
                     cG.desc.uni = TRUE)
)
