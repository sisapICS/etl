***
***

# Descriptiva Bivariada

## TIPO EAP

Comparación según el Tipo EAP (Adulto (A), Mixto (M) o Infantil (N))

```{r, warning=warning.var}

dt.datatable <- dt.flt[,
                       .(n_distancies = .N,
                         distancia_min = min(length_driving, na.rm = TRUE),
                         distancia_max = max(length_driving, na.rm = TRUE),
                         n_eap = uniqueN(up_cap),
                         n_hospitals = uniqueN(up_hosp)),
                       .(es_ics, tip_eap)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

```{r}
gc.var <- gc()
```


