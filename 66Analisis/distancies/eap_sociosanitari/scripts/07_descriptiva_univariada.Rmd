***
***

# Descriptiva Univariada

## Taula resum

Taula resum amb la desciptiva de totes les variables disponibles.

```{r, warning=warning.var}
if (params$cG.desc.uni) {
  df <- dt.dis.flt[, taula := "Distancies"]
  res <- compareGroups(taula ~ .
                       , df
                       , method = 1
                       , max.xlev = 100
                       , include.miss = TRUE)
  cT <- createTable(res
                    , show.descr = FALSE
                    , show.all = TRUE
                    , show.p.overall = FALSE
                    , show.n = TRUE
                    )
  export2md(cT,
            format = "html",
            caption = "")
  rm(list = c("df","res","cT"))
}
```

## MAPA

```{r}
dt.datatable <- dt.dis.flt[,
                           .(n_distancies = .N,
                             dist_na = sum(is.na(distancia)),
                             dist_0 = sum(distancia == 0, na.rm = TRUE),
                             dist_min = min(distancia, na.rm = TRUE),
                             dist_max = max(distancia, na.rm = TRUE),
                             dist_drive_na = sum(is.na(drive)),
                             dist_drive_0 = sum(drive == 0, na.rm = TRUE),
                             dist_drive_min = min(drive, na.rm = TRUE),
                             dist_drive_max = max(drive, na.rm = TRUE),
                             dist_walk_na = sum(is.na(walk)),
                             dist_walk_0 = sum(walk == 0, na.rm = TRUE),
                             dist_walk_min = min(walk, na.rm = TRUE),
                             dist_walk_max = max(walk, na.rm = TRUE)),
                           .(mapa_cod, mapa_des)]
dt.datatable[, mapa_cod := as.factor(mapa_cod)]
dt.datatable[, mapa_des := as.factor(mapa_des)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

## EAP

```{r}
dt.datatable <- dt.dis.flt[,
                           .(n_distancies = .N,
                             eap_te_coord = min(eap_te_coord),
                             eap_te_mapa = min(eap_te_mapa),
                             dist_na = sum(is.na(distancia)),
                             dist_0 = sum(distancia == 0, na.rm = TRUE),
                             dist_min = min(distancia, na.rm = TRUE),
                             dist_max = max(distancia, na.rm = TRUE),
                             dist_drive_na = sum(is.na(drive)),
                             dist_drive_0 = sum(drive == 0, na.rm = TRUE),
                             dist_drive_min = min(drive, na.rm = TRUE),
                             dist_drive_max = max(drive, na.rm = TRUE),
                             dist_walk_na = sum(is.na(walk)),
                             dist_walk_0 = sum(walk == 0, na.rm = TRUE),
                             dist_walk_min = min(walk, na.rm = TRUE),
                             dist_walk_max = max(walk, na.rm = TRUE)),
                           .(eap_cod, eap_des, mapa_des)]
dt.datatable[, eap_cod := as.factor(eap_cod)]
dt.datatable[, eap_des := as.factor(eap_des)]
dt.datatable[, mapa_des := as.factor(mapa_des)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

## SOCIOSANITARI

```{r}
dt.datatable <- dt.dis.flt[,
                           .(n_distancies = .N,
                             dist_na = sum(is.na(distancia)),
                             dist_0 = sum(distancia == 0, na.rm = TRUE),
                             dist_min = min(distancia, na.rm = TRUE),
                             dist_max = max(distancia, na.rm = TRUE),
                             dist_drive_na = sum(is.na(drive)),
                             dist_drive_0 = sum(drive == 0, na.rm = TRUE),
                             dist_drive_min = min(drive, na.rm = TRUE),
                             dist_drive_max = max(drive, na.rm = TRUE),
                             dist_walk_na = sum(is.na(walk)),
                             dist_walk_0 = sum(walk == 0, na.rm = TRUE),
                             dist_walk_min = min(walk, na.rm = TRUE),
                             dist_walk_max = max(walk, na.rm = TRUE)),
                           .(disp_cod, disp_des, mapa_des)]
dt.datatable[, disp_cod := as.factor(disp_cod)]
dt.datatable[, disp_des := as.factor(disp_des)]
dt.datatable[, mapa_des := as.factor(mapa_des)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

```{r}
dt.dis.flt[, taula := NULL]
gc.var <- gc()
```


