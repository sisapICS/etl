# -*- coding: utf8 -*-

import hashlib as h

"""
carregar taula
"""

import collections as c
from datetime import datetime
import sys



import sisapUtils as u

"""
file = u.tempFolder + "maxims.txt"
tb = 'DWSISAP.SIVIC_MAXIMS'
db = 'exadata2'

upload = []
for (REGIO,EDAT_CODI,EDAT,DIAGNOSTIC,ISC,setmana,RECOMPTE) in u.readCSV(file, sep=';'):
    upload.append([REGIO,EDAT_CODI,EDAT,DIAGNOSTIC,ISC,setmana,RECOMPTE])

cols = ("regio varchar2(255)","edat_codi int", "edat varchar2(255)",  "diagnostic varchar2(255)","isc int", "setmana int",
      "max_historic int") 
u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)      
u.listToTable(upload, tb, db)    

users= ['DWSISAP_ROL', 'DWSIVIC', 'DWAMERCADE']
for user in users:
    u.execute("grant select on {} to {}".format(tb,user),db)
"""

file = u.tempFolder + "prediccions.txt"
tb = 'DWSISAP.SIVIC_PREDICCIONS'
db = 'exadata2'

upload = []
for (REGIO,EDAT_CODI,EDAT,DIAGNOSTIC, isc, puntual,inf,sup,dia_de_la_temporada,dia_de_lany) in u.readCSV(file, sep=';'):
    upload.append([REGIO,EDAT_CODI,EDAT,DIAGNOSTIC, isc, puntual,inf,sup,dia_de_la_temporada,dia_de_lany])

cols = ("regio varchar2(255)", "edat_codi int", "edat varchar2(255)", "diagnostic varchar2(255)","isc int", "puntual number", "low number", "high number", "dia_temporada int", "dia_any int"
       ) 
u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)      
u.listToTable(upload, tb, db)    

users= ['DWSISAP_ROL', 'DWSIVIC', 'DWAMERCADE']
for user in users:
    u.execute("grant select on {} to {}".format(tb,user),db) 

"""
file = u.tempFolder + "virus.txt"
tb = 'DWSISAP.SIVIC_PROVESVIRUS'

db = 'exadata2'


upload = []
for (idprova, prova, virus, idvirus, desc_prova, color_prova, color_virus) in u.readCSV(file, sep='@'):
    usar = 1
    if desc_prova == 'Influenza A':
        usar = 0
    upload.append([idprova, prova, desc_prova, idvirus, color_prova, usar])

cols = ("id int", "prova_id varchar2(300)", "descripcio varchar2(300)", "virus_id int", "color varchar2(50)", "usar int") 
#u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)      
#u.listToTable(upload, tb, db)    

users= ['DWSISAP_ROL', 'DWSIVIC', 'DWAMERCADE']
for user in users:
    u.execute("grant select on {} to {}".format(tb,user),db)  
    
file = u.tempFolder + "virus.txt"
tb = 'DWSISAP.SIVIC_VIRUS'
db = 'exadata2'


viruses = {}
upload = []
for (idprova, prova, virus, idvirus, desc_prova, color_prova, color_virus) in u.readCSV(file, sep='@'):
    viruses[(idvirus,virus)] = color_virus
    
for (idvirus,virus), color in viruses.items():
    upload.append([idvirus, virus, color])

cols = ("id int", "virus varchar2(300)", "color varchar2(50)") 
#u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)      
#u.listToTable(upload, tb, db)    

users= ['DWSISAP_ROL', 'DWSIVIC', 'DWAMERCADE']
for user in users:
    u.execute("grant select on {} to {}".format(tb,user),db) 
    

tb = 'DWSISAP.SIVIC_MAXIMS'

users= ['DWSISAP_ROL', 'DWSIVIC', 'DWAMERCADE']
for user in users:
    u.execute("grant select on {} to {}".format(tb,user),db) 
"""