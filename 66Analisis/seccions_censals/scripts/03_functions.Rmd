```{r, include=FALSE}
f.cq.table2 <- function(x) {
dt.nval <- x %>% summarize_all(n_distinct) %>% data.table %>% mutate(var := 'nvaldif') %>% melt(id.vars = c("var"))
setnames(dt.nval,'value','nvaldif')
dt.min <- x %>% summarize_all(min, na.rm = TRUE) %>% data.table %>% mutate(var := 'min') %>% melt(id.vars = c("var"))
setnames(dt.min,'value','min')
dt.max <- x %>% summarize_all(max, na.rm = TRUE) %>% data.table %>% mutate(var := 'max') %>% melt(id.vars = c("var"))
setnames(dt.max,'value','max')

dt.taula <- merge(dt.nval
                  , dt.min
                  , by.x = 'variable'
                  , by.y = 'variable')

dt.taula <- merge(dt.taula
                  , dt.max
                  , by.x = 'variable'
                  , by.y = 'variable') %>% data.table()

dt.taula1 <- dt.taula[,.(variable,nvaldif,min,max)]
datatable(dt.taula1,
          rownames = FALSE,
          colnames = c("Variable", "Número valors diferents", "Valor mínim", "Valor màxim"),
          filter = 'top',
          options = list(pageLength = nrow(dt.taula1)))
}
```

