
library('rmarkdown')


render("D:/SISAP/sisap/66Analisis/visites_atdom/scripts/01_visites_atdom.Rmd", 
       output_file = paste('visites_atdom',
                           format(Sys.Date(),'%Y%m%d'),
                           format(Sys.time(),'%H%M'), sep = "_"),
       output_format = "html_document",
       output_dir = "D:/SISAP/sisap/66Analisis/visites_atdom/results"
)
