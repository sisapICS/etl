# Anàlisi descriptiva univariant

A continuació es presenten les dades obtingudes de l'enquesta.

## Variables demogràfiques

```{r, fig.align='center'}
table1::label(dades.dm$Sexe) <- cataleg[Etiqueta_3 == "Sexe"]$Etiqueta_1
table1::label(dades.dm$Edat) <- cataleg[Etiqueta_3 == "Edat"]$Etiqueta_1
table1::label(dades.dm$Antig) <- cataleg[Etiqueta_3 == "Antig"]$Etiqueta_1
table1::label(dades.dm$Horari_treb) <- cataleg[Etiqueta_3 == "Horari_treb"]$Etiqueta_1
table1::label(dades.dm$Cat_prof) <- cataleg[Etiqueta_3 == "Cat_prof"]$Etiqueta_1
table1::label(dades.dm$Comand) <- cataleg[Etiqueta_3 == "Comand"]$Etiqueta_1
table1::label(dades.dm$Ambit) <- cataleg[Etiqueta_3 == "Ambit"]$Etiqueta_1

table1::table1(~., data = dades.dm[, .SD, .SDcols = vars_demografiques], caption = "")
```

## Preguntes enquesta

```{r}
t <- as.data.table(t(
       do.call("rbind",
             list(
               N = dades.dm[, lapply(.SD, function(x) sum(!is.na(x))), .SDcols = vars_enquesta_num],
               Mean = dades.dm[, lapply(.SD, mean, na.rm = T), .SDcols = vars_enquesta_num],
               SD = dades.dm[, lapply(.SD, sd, na.rm = T), .SDcols = vars_enquesta_num],
               Median = dades.dm[, lapply(.SD, quantile, .5, na.rm = T), .SDcols = vars_enquesta_num],
               P25 = dades.dm[, lapply(.SD, quantile, .25, na.rm = T), .SDcols = vars_enquesta_num],
               P75 = dades.dm[, lapply(.SD, quantile, .75, na.rm = T), .SDcols = vars_enquesta_num],
               Min = dades.dm[, lapply(.SD, min, na.rm = T), .SDcols = vars_enquesta_num],
               Max = dades.dm[, lapply(.SD, max, na.rm = T), .SDcols = vars_enquesta_num],
               NAs = dades.dm[, lapply(.SD, function(x) mean(is.na(x), na.rm = T)), .SDcols = vars_enquesta_num]
               )
             )
       )
)
t_desc_enquesta_num <- 
  t[, .(
    Etiqueta_3 = vars_enquesta_num,
    N = format(V1, digits = 0),
    `Mean (SD)` =  paste0(format(V2, digits = 2), " (", format(V3, digits = 2), ")"),
    `Median` =  format(V4, digits = 2),
    `P25 - P75` =  paste0(format(V5, digits = 2), "-", format(V6, digits = 2)),
    `Min - Max` =  paste0(format(V7, digits = 2), "-", format(V8, digits = 2)),
    `% NA`= paste0(format(V9, digits = 2), "%")
  )
  ]

t_desc_enquesta_num <- merge(cataleg[, c("Etiqueta_1", "Etiqueta_2", "Etiqueta_3")], 
                         t_desc_enquesta_num,
                         by = "Etiqueta_3")
```

```{r}
options(knitr.kable.NA = '')
t_desc_enquesta_num[vars_enquesta_num, -1] %>%
  kable(digits = 2, col.names = c("Pregunta", "", "N", "Mean (SD)", "Median", "P25-P75", "Min-Max", "% NA"), row.names = F, escape = F, caption = "", align = c("l", "l", "c", "c", "c", "c", "c", "c")) %>%
  kable_styling(bootstrap_options = c("hover", "condensed", "responsive"), full_width = T) %>%
  row_spec(0, bold = T, align = "c") %>%
  column_spec(1:2, bold = F, border_right = F)
```

```{r, fig.align='center'}


table1::label(dades.dm$p1) <- cataleg[Etiqueta_3 == "p1"]$Etiqueta_1
table1::label(dades.dm$p2) <- cataleg[Etiqueta_3 == "p2"]$Etiqueta_1
table1::label(dades.dm$p3) <- cataleg[Etiqueta_3 == "p3"]$Etiqueta_1
table1::label(dades.dm$p4) <- cataleg[Etiqueta_3 == "p4"]$Etiqueta_1
table1::label(dades.dm$p5) <- cataleg[Etiqueta_3 == "p5"]$Etiqueta_1
table1::label(dades.dm$p6) <- cataleg[Etiqueta_3 == "p6"]$Etiqueta_1
table1::label(dades.dm$p7) <- cataleg[Etiqueta_3 == "p7"]$Etiqueta_1
table1::label(dades.dm$p8) <- cataleg[Etiqueta_3 == "p8"]$Etiqueta_1
table1::label(dades.dm$p9) <- cataleg[Etiqueta_3 == "p9"]$Etiqueta_1
table1::label(dades.dm$p10) <- cataleg[Etiqueta_3 == "p10"]$Etiqueta_1
table1::label(dades.dm$p11) <- cataleg[Etiqueta_3 == "p11"]$Etiqueta_1
table1::label(dades.dm$p12) <- cataleg[Etiqueta_3 == "p12"]$Etiqueta_1

table1::table1(~., data = dades.dm[, .SD, .SDcols = vars_enquesta_bin], caption = "")
```

### Figures

#### Gràfics de barres

```{r message=FALSE, warning=FALSE}
ggplot(dades.dm, aes(as.factor(Augm_H_setm))) +
    geom_bar(aes(y = (..count..)/sum(..count..)), fill = "#9aafd5") + 
    scale_y_continuous(labels = scales::percent_format(accuracy = 1)) +
    # scale_x_discrete(limits = 1:5) +
    ylab("") +
    xlab(paste0(cataleg[Etiqueta_3 == "Augm_H_setm" & Etiqueta_1 != "Response", Etiqueta_1],cataleg[Etiqueta_3 == "Augm_H_setm" & Etiqueta_2 != "Response", Etiqueta_2])) +
    theme_classic() +
    theme(
      axis.title.x = element_text(size = 10, hjust = 0)
    ) 

ggplot(dades.dm, aes(as.factor(Trasllat_LLT))) +
    geom_bar(aes(y = (..count..)/sum(..count..)), fill = "#9aafd5") + 
    scale_y_continuous(labels = scales::percent_format(accuracy = 1)) +
    # scale_x_discrete(limits = 1:5, drop=FALSE) +
    ylab("") +
    xlab(paste0(cataleg[Etiqueta_3 == "Trasllat_LLT" & Etiqueta_1 != "Response", Etiqueta_1],cataleg[Etiqueta_3 == "Trasllat_LLT" & Etiqueta_2 != "Response", Etiqueta_2])) +
    theme_classic() +
    theme(
      axis.title.x = element_text(size = 10, hjust = 0)
    ) 
```

```{r fig.height=15, fig.width=8, message=FALSE, warning=FALSE}
dg <- melt(dades.dm[, c("24b", "24a", "27h", "27g", "24q", "27i", "27a", "27b", "27c")], variable.name = "Etiqueta_3")
dg <- merge(dg, cataleg, by = "Etiqueta_3")
dg <- dg[, .N, c("Etiqueta_2", "value")]
dg[, N_tot := sum(N), Etiqueta_2]
dg[, p := N/N_tot*100, Etiqueta_2]

ggplot(dg, aes(x = value, y = p)) + 
  geom_bar(stat = "identity", fill = "#9aafd5") +
  # scale_fill_gradient2("Percentatge de resposta", low = "green", high = "#9aafd5", mid = "white") +
  theme_classic() +
  theme(
    axis.title = element_text(hjust = 1),
    title = element_text(hjust = 0, size = 10),
    legend.position = "bottom"
  ) +
  xlab("") +
  ylab("") +
  ggtitle("En la feina que has realitzat durant aquest període...") + 
  facet_wrap(~Etiqueta_2, ncol = 2)
```

```{r message=FALSE, warning=FALSE}
ggplot(dades.dm, aes(as.factor(Contacte_COVID))) +
    geom_bar(aes(y = (..count..)/sum(..count..)), fill = "#9aafd5") + 
    scale_y_continuous(labels = scales::percent_format(accuracy = 1)) +
    # scale_x_discrete(limits = 1:5) +
    ylab("") +
    xlab(paste0(cataleg[Etiqueta_3 == "Contacte_COVID" & Etiqueta_1 != "Response", Etiqueta_1],cataleg[Etiqueta_3 == "Contacte_COVID" & Etiqueta_2 != "Response", Etiqueta_2])) +
    theme_classic() +
    theme(
      axis.title.x = element_text(size = 10, hjust = 0)
    ) 
```


```{r fig.height=15, fig.width=8, message=FALSE, warning=FALSE}
dg <- melt(dades.dm[, c("25f", "25d", "26a", "29a", "29c", "25c")], variable.name = "Etiqueta_3")
dg <- merge(dg, cataleg, by = "Etiqueta_3")
dg <- dg[, .N, c("Etiqueta_2", "value")]
dg[, N_tot := sum(N), Etiqueta_2]
dg[, p := N/N_tot*100, Etiqueta_2]

ggplot(dg, aes(x = value, y = p)) + 
  geom_bar(stat = "identity", fill = "#9aafd5") +
  # scale_fill_gradient2("Percentatge de resposta", low = "green", high = "#9aafd5", mid = "white") +
  theme_classic() +
  theme(
    axis.title = element_text(hjust = 1),
    title = element_text(hjust = 0, size = 10),
    legend.position = "bottom"
  ) +
  xlab("") +
  ylab("") +
  ggtitle("En la feina que has realitzat durant el període d'emergència sanitària...") + 
  facet_wrap(~Etiqueta_2, ncol = 2)
```

```{r message=FALSE, warning=FALSE}
ggplot(dades.dm, aes(as.factor(Satisf))) +
    geom_bar(aes(y = (..count..)/sum(..count..)), fill = "#9aafd5") + 
    scale_y_continuous(labels = scales::percent_format(accuracy = 1)) +
    # scale_x_discrete(limits = 1:5) +
    ylab("") +
    xlab(paste0(cataleg[Etiqueta_3 == "Satisf" & Etiqueta_1 != "Response", Etiqueta_1],cataleg[Etiqueta_3 == "Satisf" & Etiqueta_2 != "Response", Etiqueta_2])) +
    theme_classic() +
    theme(
      axis.title.x = element_text(size = 10, hjust = 0)
    ) 
```

```{r fig.height=15, fig.width=8, message=FALSE, warning=FALSE}
dg <- melt(dades.dm[, c("25m", "25i", "26e", "29b", "25b", "25g")], variable.name = "Etiqueta_3")
dg <- merge(dg, cataleg, by = "Etiqueta_3")
dg <- dg[, .N, c("Etiqueta_2", "value")]
dg[, N_tot := sum(N), Etiqueta_2]
dg[, p := N/N_tot*100, Etiqueta_2]

ggplot(dg, aes(x = value, y = p)) + 
  geom_bar(stat = "identity", fill = "#9aafd5") +
  # scale_fill_gradient2("Percentatge de resposta", low = "green", high = "#9aafd5", mid = "white") +
  theme_classic() +
  theme(
    axis.title = element_text(hjust = 1),
    title = element_text(hjust = 0, size = 10),
    legend.position = "bottom"
  ) +
  xlab("") +
  ylab("") +
  ggtitle("En la feina que has realitzat durant el període d'emergència sanitària...") + 
  facet_wrap(~Etiqueta_2, ncol = 2)
```


#### Heatmap

```{r fig.width=11, message=FALSE, warning=FALSE}
dg <- melt(dades.dm[, c("24b", "24a", "27h", "27g", "24q", "27i", "27a", "27b", "27c"
)], variable.name = "Etiqueta_3")
dg <- merge(dg, cataleg, by = "Etiqueta_3")
dg <- dg[, .N, c("Etiqueta_2", "value")]
dg[, N_tot := sum(N), Etiqueta_2]
dg[, p := N/N_tot*100, Etiqueta_2]



ggplot(dg, aes(x = value, y = Etiqueta_2, fill = p)) + 
  geom_tile() +
  scale_fill_gradient2("Percentatge de resposta", low = "green", high = "#9aafd5", mid = "white") +
  theme_classic() +
  theme(
    axis.title = element_text(hjust = 1),
    title = element_text(hjust = 0, size = 10),
    legend.position = "bottom"
  ) +
  xlab("") +
  ylab("") +
  ggtitle("En la feina que has realitzat durant el període d'emergència sanitària...")
```

```{r fig.width=11, message=FALSE, warning=FALSE}
dg <- melt(dades.dm[, c("25f", "25d", "26a", "29a", "29c", "25c")], variable.name = "Etiqueta_3")
dg <- merge(dg, cataleg, by = "Etiqueta_3")
dg <- dg[, .N, c("Etiqueta_2", "value")]
dg[, N_tot := sum(N), Etiqueta_2]
dg[, p := N/N_tot*100, Etiqueta_2]



ggplot(dg, aes(x = value, y = Etiqueta_2, fill = p)) + 
  geom_tile() +
  scale_fill_gradient2("Percentatge de resposta", low = "green", high = "#9aafd5", mid = "white") +
  theme_classic() +
  theme(
    axis.title = element_text(hjust = 1),
    title = element_text(hjust = 0, size = 10),
    legend.position = "bottom"
  ) +
  xlab("") +
  ylab("") +
  ggtitle("En la feina que has realitzat durant el període d'emergència sanitària...")
```


```{r fig.width=11, message=FALSE, warning=FALSE}
dg <- melt(dades.dm[, c("25m", "25i", "26e", "29b", "25b", "25g")], variable.name = "Etiqueta_3")
dg <- merge(dg, cataleg, by = "Etiqueta_3")
dg <- dg[, .N, c("Etiqueta_2", "value")]
dg[, N_tot := sum(N), Etiqueta_2]
dg[, p := N/N_tot*100, Etiqueta_2]



ggplot(dg, aes(x = value, y = Etiqueta_2, fill = p)) + 
  geom_tile() +
  scale_fill_gradient2("Percentatge de resposta", low = "green", high = "#9aafd5", mid = "white") +
  theme_classic() +
  theme(
    axis.title = element_text(hjust = 1),
    title = element_text(hjust = 0, size = 10),
    legend.position = "bottom"
  ) +
  xlab("") +
  ylab("") +
  ggtitle("En la feina que has realitzat durant el període d'emergència sanitària...")
```

## Puntuacions calculades

```{r}
t <- as.data.table(t(
       do.call("rbind",
             list(
               N = dades.dm[, lapply(.SD, function(x) sum(!is.na(x))), .SDcols = vars_calculades],
               Mean = dades.dm[, lapply(.SD, mean, na.rm = T), .SDcols = vars_calculades],
               SD = dades.dm[, lapply(.SD, sd, na.rm = T), .SDcols = vars_calculades],
               Median = dades.dm[, lapply(.SD, quantile, .5, na.rm = T), .SDcols = vars_calculades],
               P25 = dades.dm[, lapply(.SD, quantile, .25, na.rm = T), .SDcols = vars_calculades],
               P75 = dades.dm[, lapply(.SD, quantile, .75, na.rm = T), .SDcols = vars_calculades],
               Min = dades.dm[, lapply(.SD, min, na.rm = T), .SDcols = vars_calculades],
               Max = dades.dm[, lapply(.SD, max, na.rm = T), .SDcols = vars_calculades],
               NAs = dades.dm[, lapply(.SD, function(x) sum(is.na(x))), .SDcols = vars_calculades]
               )
             )
       )
)
t_desc_calc <- 
  t[, .(
    Etiqueta_3 = vars_calculades,
    N = format(V1, digits = 0),
    `Mean (SD)` =  paste0(format(V2, digits = 2), " (", format(V3, digits = 2), ")"),
    `Median` =  format(V4, digits = 2),
    `P25 - P75` =  paste0(format(V5, digits = 2), "-", format(V6, digits = 2)),
    `Min - Max` =  paste0(format(V7, digits = 2), "-", format(V8, digits = 2))
  )
  ]

```

```{r}
options(knitr.kable.NA = '')
t_desc_calc[] %>%
  kable(digits = 2, col.names = c("Pregunta", "N", "Mean (SD)", "Median", "P25-P75", "Min-Max"), row.names = F, escape = F, caption = "", align = c("l", "c", "c", "c", "c", "c")) %>%
  kable_styling(bootstrap_options = c("hover", "condensed", "responsive"), full_width = T) %>%
  row_spec(0, bold = T, align = "c") %>%
  column_spec(1:3, bold = F, border_right = F)
```

```{r fig.width=10, message=FALSE, warning=FALSE}
dg <- melt(dades.dm[, .SD, .SDcols = vars_calculades], variable.name = "Etiqueta_3")




ggplot(dg, aes(y = value, x = Etiqueta_3)) + 
  geom_boxplot(color = "#9aafd5") +
  theme_classic() +
  theme(
    axis.title = element_text(hjust = 1),
    title = element_text(hjust = 0, size = 10)
  ) +
  xlab("") +
  ylab("") +
  ggtitle("Puntuacions")
```


```{r, fig.align='center'}
table1::table1(~., data = dades.dm[, .SD, .SDcols = vars_calculades_rang], caption = "")
```



