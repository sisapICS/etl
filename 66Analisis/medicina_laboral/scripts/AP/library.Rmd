
```{r, warning=warning.var, message=message.var}

# Importació
  suppressWarnings(suppressPackageStartupMessages(library('RMySQL')))
  suppressWarnings(suppressPackageStartupMessages(library('openxlsx')))
  suppressWarnings(suppressPackageStartupMessages(library('data.table')))

# Data Manager
  
# Anàlisi descriptiva univariant
  suppressWarnings(suppressPackageStartupMessages(library('table1')))
  suppressWarnings(suppressPackageStartupMessages(library('ggplot2')))
  suppressWarnings(suppressPackageStartupMessages(library('tidyr')))
  suppressWarnings(suppressPackageStartupMessages(library('kableExtra')))
  
# Gràfics radials
  suppressWarnings(suppressPackageStartupMessages(library('fmsb')))
  suppressWarnings(suppressPackageStartupMessages(library('colormap')))  

# Anàlisi descriptiva bivariant
  suppressWarnings(suppressPackageStartupMessages(library('compareGroups')))  
  
# RMarkdown
  suppressWarnings(suppressPackageStartupMessages(library('knitr')))


```

