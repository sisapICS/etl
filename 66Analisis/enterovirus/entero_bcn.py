# -*- coding: utf8 -*-

import hashlib as h

"""
Mascaretes refaig amb dades menys fines
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

db = "permanent"

def get_tip(cim):
    if cim in ('C01-B08.5', 'B08.5'):
        tip = 'Herpangina'
    elif cim in ('C01-B08.4','B08.4'):
        tip = 'Febre aftosa'
    else:
        tip = None
    return(tip)

def _get_grup(edat):
    """."""
    if edat is None or edat < 0:
        grup = "No disponible"
    elif edat > 89:
        grup = "90 o més"
    elif edat < 3:
        grup = "0 a 2"
    elif edat < 6:
        grup =  "3 a 5"
    elif edat < 10:
        grup =  "6 a 9"
    elif edat < 15:
        grup = "10 a 14"
    elif edat < 20:
        grup = "15 a 19"
    else:
        baix = 10 * m.floor(edat / 10)
        alt = baix + 9
        grup = (baix, "{} a {}".format(baix, alt))
    return(grup)    
    
class enterovirus(object):
    """."""

    def __init__(self):
        """."""
        self.recomptes = c.Counter()
        self.get_centres()
        self.get_assignada()
        self.get_enterov()
        self.export()

    def get_centres(self):
        """Centres de BCN"""
        u.printTime("centres")
        self.centres = {}
        
        sql = "select scs_codi, sap_desc, ics_desc from nodrizas.cat_centres cc where amb_codi='03' and tip_eap in ('M','N')"
        for up, sap, amb in u.getAll(sql, 'nodrizas'):
            self.centres[up] = {'sap': sap, 'amb': amb}
    
    def get_assignada(self):
        """."""
        u.printTime("assignada")
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        
        self.pob = {}
        sql = 'select id_cip, usua_sexe, usua_data_naixement, usua_uab_up from assignada'
        for id, sexe, naix, up in u.getAll(sql, 'import'):
            try:
                ed = u.yearsBetween(naix, dext)
            except:
                ed = None
            if ed < 15:
                if up in self.centres:
                    amb = self.centres[up]['amb']
                    sap = self.centres[up]['sap']
                    grup = _get_grup(ed)
                    self.recomptes[sap, up, amb, grup, sexe, 'poblacio infantil'] += 1
            self.pob[id] = {'naix': naix, 'sexe': sexe}
            
    def get_enterov(self):
        """."""
        u.printTime("problemes")
        sql = "select id_cip, pr_dde, year(pr_dde),  pr_cod_ps, pr_up \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_cod_ps in ('C01-B08.5', 'B08.5', 'C01-B08.4','B08.4')  \
                            and extract(year_month from pr_dde) >'201712' and \
                            extract(year_month from pr_dde) <'202201' and \
                            pr_data_baixa = 0 group by id_cip,pr_dde, date_format(pr_dde,'%Y%m'), pr_cod_ps, pr_up"
        for id, dat, periode,codi, up in u.getAll(sql, 'import'):
            sexe = self.pob[id]['sexe'] if id in self.pob else None
            naix = self.pob[id]['naix'] if id in self.pob else None
            tipus = get_tip(codi)
            try:
                ed = u.yearsBetween(naix, dat)
            except:
                ed = None
            if ed <15:
                if up in self.centres:
                    amb = self.centres[up]['amb']
                    sap = self.centres[up]['sap']
                    grup = _get_grup(ed)
                    variable = tipus + '_' + str(periode)
                    self.recomptes[sap, up, amb, grup, sexe, variable] += 1

    def export(self):
        """."""
        tb = "enterovirus_bcn"
        cols = ("sap varchar(200)", "up varchar(5)", "eap varchar(150)", "grup varchar(20)", "sexe varchar(10)", "variable varchar(150)", "recomptes int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)

        upload = []
        for (sap, up, amb, grup, sexe, variable), n in self.recomptes.items():
            n = int (n)
            upload.append([sap, up, amb, grup, sexe, variable, n])
        u.listToTable(upload, tb, db)
    
           
if __name__ == '__main__':
    u.printTime("Inici")
    
    enterovirus()
    
    u.printTime("Final")
    