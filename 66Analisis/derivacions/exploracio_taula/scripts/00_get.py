# -*- coding: utf-8 -*-

# 2023/10/096 Descarreguem tota la informació de la taula dwsisap.master_derivacions
# TIME: 

import sisapUtils as u
from datetime import datetime

ts = datetime.now()
print('comencem')
sql = """SELECT
            *
         FROM 
            dwsisap.master_derivacions
         WHERE
            extract(year from oc_data) = 2023
        """        
upload = []

cols = u.getTableColumns('master_derivacions', 'exadata')
print(cols)

for row in u.getAll(sql, 'exadata'):
    upload.append(row)
#print(upload)

file = u.tempFolder + 'master_derivacions.csv'
print(file)
u.writeCSV(file, [cols , upload])
tf = datetime.now()
print(tf - ts)
