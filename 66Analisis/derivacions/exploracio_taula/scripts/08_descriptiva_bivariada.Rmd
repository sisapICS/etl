***
***

# Descriptiva Bivariada

## Tipus fluxe

Ja no cal, es filtren les derivacions PIPA en el flowchart. A l'annex hi ha la comparació dels diferents fluxes.

## Presencialitat

### Taula Resum

```{r}

#dt.biv <- dt.flt[sample(250000)]
dt.biv <- dt.flt
df <- dt.biv
ini <- Sys.time()
res <- compareGroups(agrupador_prova ~ es_ics +
                                        oc_data + oc_year +
                                        oc_collegiat_missing +
                                        oc_dni_professional_missing +
                                        inf_prioritat + 
                                        inf_servei_d_codi_missing + inf_servei_d_codi_desc_missing + 
                                        espec_inout + 
                                        inf_espec_sire_missing + der_espe_sire_missing + especialitat_final_missing +
                                        der_cod_pr_missing +
                                        der_data_prog + data_prog_missing + prog_year +
                                        oc_demora + 
                                        data_real_missing +
                                        data_baix_missing +
                                        agrupador_rebuig + #gpi_motiu_baixa_desc +
                                        der_estat + 
                                        flag_compta_gpi_rel_peticio + flag_compta_gpi_rel_peticio_c +
                                        flag_peticio_anullada + 
                                        flag_peticio_sollicitada + 
                                        flag_peticio_tramitada +
                                        flag_peticio_programada +
                                        flag_peticio_realitzada +
                                        flag_peticio_rebutjada +
                                        flag_peticio_pendent
                     , df
                     , method = 1
                     , max.xlev = 100
                     , include.miss = FALSE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT
          , format = "html"
          #, caption = "Anàlisi derivació presencial"
          )
fi <- Sys.time()
fi - ini # Sample 1.3174 mins / All 24.88133 mins
```


### EAP Origen

#### Taula

```{r}

dt.agr  <- dt.flt[, .N, .(oc_up_ori_desc, agrupador_prova)]
dt.agr <- dcast(dt.agr, oc_up_ori_desc ~ agrupador_prova, value.var = "N")
dt.agr[is.na(`No presencial`), `No presencial`:=0]
dt.agr[, Total:= `No presencial` + `Primera presencial`]
dt.agr[, presencial_p:=round((`Primera presencial`/Total)*100,2)]
dt.agr[, nopresencial_p:=round((`No presencial`/Total)*100,2)]
dt.agr[, nopresencial_p:=-nopresencial_p]
datatable(dt.agr[,.(oc_up_ori_desc, Total, `No presencial`, nopresencial_p, `Primera presencial`, presencial_p)],
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.agr)))

```

#### Figura

```{r}

ggplot(dt.agr, aes(x = reorder(oc_up_ori_desc, presencial_p))) +
  geom_col(aes(y = presencial_p), fill = "#5d8402") +
  geom_col(aes(y = nopresencial_p), fill = "#817d79") +
  theme_minimal() +
  theme(axis.text.y = element_text(size=4)) +
  coord_flip()

tiff("D:/SISAP/sisap/66Analisis/derivacions/results/f.presencialitat.eaporigen.tiff",
     units="in", width=5, height=15, res=300)
ggplot(dt.agr, aes(x = reorder(oc_up_ori_desc, presencial_p))) +
  geom_col(aes(y = presencial_p), fill = "#5d8402") +
  geom_col(aes(y = nopresencial_p), fill = "#817d79") + 
  theme_minimal() +
  theme(axis.text.y = element_text(size=4)) +
  coord_flip()
dev.off()
```

### Regió EAP Origen

#### Taula

```{r}

dt.agr  <- dt.flt[, .N, .(oc_up_ori, oc_up_ori_desc, agrupador_prova)]
dt.agr <- merge(dt.agr
                , dt.cat.redics #[,.(up_cod,es_ics, tip_cod, subtip_cod, abs_cod, sector_cod, aga_cod, regio_cod, ep_cod)],
                , by.x = "oc_up_ori"
                , by.y = "up_cod")
dt.agr[, .N, regio_des]

dt.agr  <- dt.agr[, .(N=sum(N)), .(regio_des, agrupador_prova)]
dt.agr <- dcast(dt.agr, regio_des ~ agrupador_prova, value.var = "N")

dt.agr[is.na(`No presencial`), `No presencial`:=0]
dt.agr[, Total:= `No presencial` + `Primera presencial`]
dt.agr[, presencial_p:=round((`Primera presencial`/Total)*100,2)]
dt.agr[, nopresencial_p:=round((`No presencial`/Total)*100,2)]
datatable(dt.agr[,.(regio_des, Total, `No presencial`, nopresencial_p, `Primera presencial`, presencial_p)],
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.agr)))
```

#### Figura

```{r}

dt.agr[, nopresencial_p:=-nopresencial_p]
ggplot(dt.agr, aes(x = reorder(regio_des, presencial_p))) +
  geom_col(aes(y = presencial_p), fill = "#5d8402") +
  geom_col(aes(y = nopresencial_p), fill = "#817d79") +
  scale_y_continuous(limits = c(-100,100)) +
  theme_minimal() +
  theme(axis.text.y = element_text(size=8)) +
  coord_flip()

tiff("D:/SISAP/sisap/66Analisis/derivacions/results/f.presencialitat.regioeaporigen.tiff",
     units="in", width=5, height=6, res=300)
ggplot(dt.agr, aes(x = reorder(regio_des, presencial_p))) +
  geom_col(aes(y = presencial_p), fill = "#5d8402") +
  geom_col(aes(y = nopresencial_p), fill = "#817d79") +
  scale_y_continuous(limits = c(-100,100)) +
  theme_minimal() +
  theme(axis.text.y = element_text(size=8)) +
  coord_flip()
dev.off()
```

***

## Prioritat

### Taula Resum

```{r}

#dt.biv <- dt.flt[sample(250000)]
dt.biv <- dt.flt
df <- dt.biv
ini <- Sys.time()
res <- compareGroups(inf_prioritat ~  es_ics +
                                      agrupador_prova + 
                                      oc_data + oc_year +
                                      oc_collegiat_missing +
                                      oc_dni_professional_missing +
                                      inf_servei_d_codi_missing + inf_servei_d_codi_desc_missing + 
                                      espec_inout + 
                                      inf_espec_sire_missing + der_espe_sire_missing + especialitat_final_missing +
                                      der_cod_pr_missing +
                                      der_data_prog + data_prog_missing + prog_year +
                                      oc_demora + 
                                      data_real_missing +
                                      data_baix_missing +
                                      agrupador_rebuig + #gpi_motiu_baixa_desc +
                                      der_estat + 
                                      flag_compta_gpi_rel_peticio + flag_compta_gpi_rel_peticio_c +
                                      flag_peticio_anullada + 
                                      flag_peticio_sollicitada + 
                                      flag_peticio_tramitada +
                                      flag_peticio_programada +
                                      flag_peticio_realitzada +
                                      flag_peticio_rebutjada +
                                      flag_peticio_pendent

                     , df
                     , method = 4
                     , max.xlev = 100
                     , include.miss = FALSE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT
          , format = "html"
          #, caption = "Anàlisi derivacions segons prioritat"
          )
fi <- Sys.time()
fi - ini # 
```

***

## Data Programació missing

### Taula Resum

```{r}

#dt.biv <- dt.flt[sample(250000)]
dt.biv <- dt.flt
df <- dt.biv
ini <- Sys.time()
res <- compareGroups(data_prog_missing ~ es_ics +
                                      agrupador_prova + 
                                      oc_data + oc_year +
                                      oc_collegiat_missing +
                                      oc_dni_professional_missing +
                                      inf_servei_d_codi_missing + inf_servei_d_codi_desc_missing + 
                                      espec_inout + 
                                      inf_espec_sire_missing + der_espe_sire_missing + especialitat_final_missing + #especialitat_final
                                      der_cod_pr_missing +
                                      der_data_prog + data_prog_missing + prog_year +
                                      oc_demora + 
                                      data_real_missing +
                                      data_baix_missing +
                                      agrupador_rebuig + #gpi_motiu_baixa_desc +
                                      der_estat + 
                                      flag_compta_gpi_rel_peticio + flag_compta_gpi_rel_peticio_c +
                                      flag_peticio_anullada + 
                                      flag_peticio_sollicitada + 
                                      flag_peticio_tramitada +
                                      flag_peticio_programada +
                                      flag_peticio_realitzada +
                                      flag_peticio_rebutjada +
                                      flag_peticio_pendent
                     , df
                     , method = 4
                     , max.xlev = 100
                     , include.miss = FALSE
                     , byrow = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT
          , format = "html"
          , caption = "Anàlisi Data Programació Missing (Nota: % per fila)"
          )
fi <- Sys.time()
fi - ini # Sample: 59.18979 secs / All: 23.96887 mins
```

```{r}
rm(list=c("dt.biv","df","res","cT"))
gc.var <- gc()
```


