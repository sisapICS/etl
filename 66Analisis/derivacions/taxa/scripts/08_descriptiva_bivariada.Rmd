***
***

# Descriptiva Bivariada

## Àmbit

```{r, fig.height=4, fig.width=8}
ggplot(dt.data.table,
       aes(x = taxa, y = amb_desc)) +
  geom_jitter(color = 'orange', alpha = 0.5) +
  geom_boxplot(outlier.shape = NA, alpha = 0.2) + 
  theme_minimal() +
  xlab("Taxa derivacions població atesa (x10000)") +
  ylab("") +
  ggtitle("Taxa de derivacions en població atesa segons AMBIT",
          subtitle = "")
```

## Sap

```{r, fig.height=12, fig.width=8}

dt.data.table[, taxa_mediana := median(taxa), sap_desc]

ggplot(dt.data.table,
       aes(x = taxa, y = fct_reorder(sap_desc, taxa_mediana))) +
  geom_jitter(color = 'orange', alpha = 0.5) +
  geom_boxplot(outlier.shape = NA, alpha = 0.2) + 
  theme_minimal() +
  xlab("Taxa derivacions població atesa (x10000)") +
  ylab("") +
  ggtitle("Taxa de derivacions en població atesa segons SAP",
          subtitle = "")
```

```{r, fig.height=12, fig.width=8}

dt.data.table[, taxa_mediana := median(taxa), sap_desc]

ggplot(dt.data.table,
       aes(x = taxa, y = fct_reorder(sap_desc, taxa_mediana))) +
  geom_jitter(color = 'orange', alpha = 0.5) +
  geom_boxplot(outlier.shape = NA, alpha = 0.2) + 
  theme_minimal() +
  xlab("Taxa derivacions població atesa (x10000)") +
  ylab("") +
  ggtitle("Taxa de derivacions en població atesa segons SAP",
          subtitle = "") +
  #facet_grid(vars(amb_desc), scales = "free_y", space = "free")
  facet_col(vars(amb_desc), scales = "free_y", space = "free")
```

## Hospital de Referència

### Hospital de Referència

```{r, fig.height=10, fig.width=12}

dt.data.table[, taxa_mediana := median(taxa), hosp_ref_desc_1]

ggplot(dt.data.table,
       aes(x = taxa, y = fct_reorder(hosp_ref_desc_1, taxa_mediana))) +
  geom_jitter(color = 'orange', alpha = 0.5) +
  geom_boxplot(outlier.shape = NA, alpha = 0.2) + 
  theme_minimal() +
  xlab("Taxa derivacions població atesa (x10000)") +
  ylab("") +
  ggtitle("Taxa de derivacions en població atesa segons HOSPITAL DE REFERÈNCIA",
          subtitle = "")
```

#### Àmbit

```{r, fig.height=14, fig.width=12}

dt.data.table[, taxa_mediana := median(taxa), hosp_ref_desc_1]

ggplot(dt.data.table,
       aes(x = taxa, y = fct_reorder(hosp_ref_desc_1, taxa_mediana))) +
  geom_jitter(color = 'orange', alpha = 0.5) +
  geom_boxplot(outlier.shape = NA, alpha = 0.2) + 
  theme_minimal() +
  xlab("Taxa derivacions població atesa (x10000)") +
  ylab("") +
  ggtitle("Taxa de derivacions en població atesa segons HOSPITAL DE REFERÈNCIA",
          subtitle = "") +
  #facet_grid(vars(amb_desc), scales = "free_y", space = "free")
  facet_col(vars(amb_desc), scales = "free_y", space = "free")
```

#### Sap

```{r, fig.height=36, fig.width=14}

dt.data.table[, taxa_mediana := median(taxa), hosp_ref_desc_1]

ggplot(dt.data.table,
       aes(x = taxa, y = fct_reorder(hosp_ref_desc_1, taxa_mediana))) +
  geom_jitter(color = 'orange', alpha = 0.5) +
  geom_boxplot(outlier.shape = NA, alpha = 0.2) + 
  theme_minimal() +
  xlab("Taxa derivacions població atesa (x10000)") +
  ylab("") +
  ggtitle("Taxa de derivacions en població atesa segons HOSPITAL DE REFERÈNCIA",
          subtitle = "") +
  #facet_grid(vars(amb_desc), scales = "free_y", space = "free")
  facet_col(vars(sap_desc), scales = "free_y", space = "free")
```

#### ICS

```{r, fig.height=12, fig.width=10}

dt.data.table[, taxa_mediana := median(taxa), hosp_ref_desc_1]

ggplot(dt.data.table,
       aes(x = taxa, y = fct_reorder(hosp_ref_desc_1, taxa_mediana))) +
  geom_jitter(color = 'orange', alpha = 0.5) +
  geom_boxplot(outlier.shape = NA, alpha = 0.2) + 
  theme_minimal() +
  xlab("Taxa derivacions població atesa (x10000)") +
  ylab("") +
  ggtitle("Taxa de derivacions en població atesa \nsegons HOSPITAL DE REFERÈNCIA",
          subtitle = "") +
  #facet_grid(vars(amb_desc), scales = "free_y", space = "free")
  facet_col(vars(hosp_ref_epics_1), scales = "free_y", space = "free")
```

### Hospital de Referència ICS

```{r, fig.height=4, fig.width=8}

dt.data.table[, taxa_mediana := median(taxa), hosp_ref_epics_1]

ggplot(dt.data.table,
       aes(x = taxa, y = fct_reorder(hosp_ref_epics_1, taxa_mediana))) +
  geom_jitter(color = 'orange', alpha = 0.5) +
  geom_boxplot(outlier.shape = NA, alpha = 0.2) + 
  theme_minimal() +
  xlab("Taxa derivacions població atesa (x10000)") +
  ylab("") +
  ggtitle("Taxa de derivacions en població atesa \nsegons HOSPITAL DE REFERÈNCIA ICS verses NO ICS",
          subtitle = "")
```

***

## Nivell Hospital de Referència

```{r, fig.height=4, fig.width=8}

dt.data.table[, taxa_mediana := median(taxa), hosp_nivell_desc]

ggplot(dt.data.table,
       aes(x = taxa, y = fct_reorder(hosp_nivell_desc, taxa_mediana))) +
  geom_jitter(color = 'orange', alpha = 0.5) +
  geom_boxplot(outlier.shape = NA, alpha = 0.2) + 
  theme_minimal() +
  xlab("Taxa derivacions població atesa (x10000)") +
  ylab("") +
  ggtitle("Taxa de derivacions en població atesa \nsegons NIVELL HOSPITAL DE REFERÈNCIA",
          subtitle = "")
```

### ICS

```{r, fig.height=6, fig.width=8}

dt.data.table[, taxa_mediana := median(taxa), hosp_nivell_desc]

ggplot(dt.data.table,
       aes(x = taxa, y = fct_reorder(hosp_nivell_desc, taxa_mediana))) +
  geom_jitter(color = 'orange', alpha = 0.5) +
  geom_boxplot(outlier.shape = NA, alpha = 0.2) + 
  theme_minimal() +
  xlab("Taxa derivacions població atesa (x10000)") +
  ylab("") +
  ggtitle("Taxa de derivacions en població atesa \nsegons HOSPITAL DE REFERÈNCIA",
          subtitle = "") +
  #facet_grid(vars(amb_desc), scales = "free_y", space = "free")
  facet_col(vars(hosp_ref_epics_1), scales = "free_y", space = "free")

ggplot(dt.data.table,
       aes(x = taxa, y = fct_reorder(hosp_ref_epics_1, taxa_mediana))) +
  geom_jitter(color = 'orange', alpha = 0.5) +
  geom_boxplot(outlier.shape = NA, alpha = 0.2) + 
  theme_minimal() +
  xlab("Taxa derivacions població atesa (x10000)") +
  ylab("") +
  ggtitle("Taxa de derivacions en població atesa \nsegons HOSPITAL DE REFERÈNCIA",
          subtitle = "") +
  #facet_grid(vars(amb_desc), scales = "free_y", space = "free")
  facet_col(vars(hosp_nivell_desc), scales = "free_y", space = "free")
```

### Àmbit

```{r, fig.height=12, fig.width=10}

dt.data.table[, taxa_mediana := median(taxa), hosp_nivell_desc]

ggplot(dt.data.table,
       aes(x = taxa, y = fct_reorder(hosp_nivell_desc, taxa_mediana))) +
  geom_jitter(color = 'orange', alpha = 0.5) +
  geom_boxplot(outlier.shape = NA, alpha = 0.2) + 
  theme_minimal() +
  xlab("Taxa derivacions població atesa (x10000)") +
  ylab("") +
  ggtitle("Taxa de derivacions en població atesa segons HOSPITAL DE REFERÈNCIA",
          subtitle = "") +
  #facet_grid(vars(amb_desc), scales = "free_y", space = "free")
  facet_col(vars(amb_desc), scales = "free_y", space = "free")
```

***

## Combinación EAP - Hospital de Referència

```{r}
df <- dt.data.table
res <- compareGroups(eaphosp_ics ~ taxa + taxa
                     , df
                     , method = c(1,2)
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "")
```

***

## EAP - Hospital de Referència mateixa Entitat proveidora 

```{r}
df <- dt.data.table
res <- compareGroups(epsame ~ taxa + taxa
                     , df
                     , method = c(1,2)
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "")
```


```{r}
#rm(list=c("dt.biv","df","res","cT"))
gc.var <- gc()
```


