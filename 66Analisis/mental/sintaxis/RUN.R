library('rmarkdown')

setwd("C:/Users/nmora/repositori/etl/66Analisis/obesitat/sintaxis/")
Sys.setenv(RSTUDIO_PANDOC= "C:/Program Files/RStudio/bin/pandoc")

render("C:/Users/nmora/repositori/etl/66Analisis/obesitat/sintaxis/analisi_obesitat.Rmd", 
       output_format = "html_document",
       output_file = paste('Analisi_patologia_dental',
                           format(Sys.Date(),'%Y%m%d'),
                           format(Sys.time(),'%H%M'), sep="_"),
       output_dir = "../resultats/")
