***
***
***

# IMPORTACIÓ

Importació de la informació necessària per la realització d'aquest informe

```{r}
ini.import <- Sys.time()
```

## Dades

Importació de les dades necessàries per  fer l'anàlisi.

### Arxiu excel

Importació de l'arxiu excel ** proporcionat via mail el dia 28/06/2023.

```{r}
dt.raw <- data.table(read_excel("../data/Enquesta estudiants Grau de Medicina de la UAB sobre les pràctiques a l'Atenció Primària(1-337)_Excelrespostesmentoratge260623.xlsx",
                                sheet = "Sheet1",
                                range = "A1:M338")
                    )

setnames(dt.raw, tolower(names(dt.raw)))
```

Informació importada:

- files n = `r nrow(dt.raw)`
- columnes n = `r ncol(dt.raw)`
- columnes: `r names(dt.raw)`

#### Summary

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

```{r}
fi <- Sys.time()
fi - ini.import #
gc.var <- gc()
```