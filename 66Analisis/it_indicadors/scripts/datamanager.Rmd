# Data Manager

### Creació noves variables

No s'han creat noves variables.

```{r}
# # Resident
# #  dt[, resident := ifelse(resident == 1, "Sí", "No")]
# 
# # MEDEA
#   # dt.medea <- dt[, .N, .(medea_c)]
#   # RURAL
#     dt.dm[, rural := ifelse(medea_c %in% c('0R', '1R', '2R'), "Sí", "No")]
#     #dt.medea <- dt[, .N, .(rural, medea_c)]
# 
#   # MEDEA
#     dt.dm[, medea_c1 := medea_c][medea_c %in% c('0R', '1R', '2R'), medea_c1:="R"]
```

### Selecció d'informació

**S'han seleccionat només els indicadors "pare" TOT**

```{r, selecció}

dt.flt <- dt[str_sub(ind,-3)=="TOT",]
```

- files n = `r nrow(dt.flt)`

### Long to Wide Format

```{r}

dt.flt.wide <- dt.flt %>% spread(analisi, n)
setnames(dt.flt.wide, tolower(names(dt.flt.wide)))

# Poner 0 a las numeradores NA
  dt.flt.wide[is.na(num), num:=0]
```

### Agregar nivell UP

```{r}

#dt.agr <- dt.flt.wide[, j=list(num=sum(num), den=sum(den)), by=.(ind,time,up_codi)]
```

### Afegir variables explicatives

```{r afegir nova informació}

setkeyv(dt.flt.wide, c('up_codi','edat_c','sexe'))
setkeyv(dt.vars, c('br','edat', 'sexe'))
dt.dm <- merge(dt.flt.wide,
               dt.vars[ates==1,],
               by.x = c('up_codi','edat_c','sexe'),
               by.y = c('br','edat', 'sexe'),
               #allow.cartesian=TRUE,
               all.x = TRUE)
```

### Afegir I. MEDEA i I. AQUAS

```{r afegir medea aquas}

setkey(dt.dm, 'up')
setkey(dt.i, 'up')
dt.temp <- merge(dt.dm,
                 dt.i,
                 by.x = c('up'),
                 by.y = c('up'),
                 all.x = TRUE)
```

```{r data manager gc}
gc.var <- gc()
```

