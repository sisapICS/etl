
## Indicador IT003

```{r}

dt.glm <- dt.dm[ind=="IT003TOT" & time=="A2108", ]
dt.glm[, res:=round((num/den), 2)]
dt.it003 <- dt.glm[, j=list(num=sum(num), den=sum(den)), by=.(edat_c, sexe)][, res:=round((num/den), 2)]
dt.it003 <- dt.it003 %>% pivot_wider(names_from = sexe, values_from = c(num, den, res))
dt.it003 <- dt.it003[, c("edat_c",
                         "num_DONA","den_DONA","res_DONA",
                         "num_HOME","den_HOME","res_HOME")]
dt.it003 <- dt.it003 %>% mutate(ratio:=round((res_DONA/res_HOME), 2))
kable(dt.it003)

dt.it003 <- dt.glm[, j=list(num=sum(num), den=sum(den)), by=.(ind)][, res:=round((num/den), 2)]
```

### Modelització

#### Estructura EAP

```{r}

# Agregació dades a nivell UP
dt.glm <- dt.dm[ind=="IT003TOT" & time=="A2108", ]
#edat_c %in% c("EC1519", "EC2024", "EC2529", "EC3034", "EC3539", "EC4044",
#                                                      "EC4549", "EC5054", "EC5559", "EC6064", "EC6569", "EC7074")
dt.glm <- dt.glm[,j=list(num=sum(num), den=sum(den),
                         poblacio=sum(poblacio),
                         dona=sum(sum_dona),
                         edat=sum(sum_edat),
                         institucionalitzats=sum(institucionalitzats),
                         immigrants_renta_baixa=sum(immigrants_renta_baixa),
                         pensionistes=sum(pensionistes),
                         pcc=sum(pcc),
                         maca=sum(maca),
                         atdom=sum(atdom),
                         den_gma=sum(den_gma),
                         num_gma=sum(num_gma),
                         den_medea=sum(den_medea),
                         num_medea=sum(num_medea)), by=.(up, ind)]

dt.glm[, res:=round((num/den), 2)]
dt.it003 <- dt.glm[, j=list(num=sum(num), den=sum(den)), by=.(ind)][, res:=round((num/den), 2)]

# Creació variables
dt.glm[, ":=" (edat = edat/poblacio,
               dona = dona/poblacio,
               institucionalitzats = institucionalitzats/poblacio*100,
               immigrants_renta_baixa = immigrants_renta_baixa/poblacio*100,
               pensionistes = pensionistes/poblacio*100,
               pcc = pcc/poblacio*100,
               maca = maca/poblacio*100,
               atdom = atdom/poblacio*100,
               gma = num_gma/den_gma,
               medea = num_medea/den_medea)]

a <- dt.glm[,.(res,edat,dona,gma,institucionalitzats,immigrants_renta_baixa, pensionistes,pcc, maca,atdom)]
ggplot(a) + geom_autopoint() + facet_matrix(vars(res:dona))
ggplot(a) + geom_autopoint() + facet_matrix(vars(res, gma, immigrants_renta_baixa, pensionistes))
ggplot(a) + geom_autopoint() + facet_matrix(vars(res,institucionalitzats,pcc:atdom))
```

```{r}
# MODELS

## LM

  # FULL    
    lm.full <- lm(res ~ edat + dona + 
                        medea + 
                        gma +
                        institucionalitzats + immigrants_renta_baixa + pensionistes + pcc + maca + atdom,
                  data = dt.glm)
    it003.step.model <- stepAIC(lm.full, direction = "both", 
                          trace = FALSE)
    summary(it003.step.model)
    pred <- predict(it003.step.model, newdata = dt.glm, type = "response")
    dt.glm[, pred0 := pred]
    cor(dt.glm[, res], dt.glm[, pred0], use = "pairwise.complete.obs")^2
    ggplot(dt.glm, aes(x=pred0, y=res)) + geom_point()
```

#### Estructura EAP, EDAT i SEXE

```{r}

# Agregació dades a nivell UP
dt.glm <- dt.dm[ind=="IT003TOT" & time=="A2108", ]
#edat_c %in% c("EC1519", "EC2024", "EC2529", "EC3034", "EC3539", "EC4044",
#                                                      "EC4549", "EC5054", "EC5559", "EC6064", "EC6569", "EC7074")

dt.glm[, res:=round((num/den), 2)]
dt.it003 <- dt.glm[, j=list(num=sum(num), den=sum(den)), by=.(ind)][, res:=round((num/den), 2)]

# Creació variables
dt.glm[, ":=" (institucionalitzats = institucionalitzats/poblacio*100,
               immigrants_renta_baixa = immigrants_renta_baixa/poblacio*100,
               pensionistes = pensionistes/poblacio*100,
               pcc = pcc/poblacio*100,
               maca = maca/poblacio*100,
               atdom = atdom/poblacio*100,
               gma = num_gma/den_gma,
               medea = num_medea/den_medea)]

a <- dt.glm[,.(res,edat_c,sexe,gma,institucionalitzats,immigrants_renta_baixa, pensionistes,pcc, maca,atdom)]
ggplot(a) + geom_autopoint() + facet_matrix(vars(res:sexe))
ggplot(a) + geom_autopoint() + facet_matrix(vars(res, gma, immigrants_renta_baixa, pensionistes))
ggplot(a) + geom_autopoint() + facet_matrix(vars(res,institucionalitzats,pcc:atdom))
```

```{r}
# MODELS

## LM

  # FULL    
    lm.full <- lm(res ~ edat_c + sexe + edat_c*sexe +
                        medea + 
                        gma +
                        institucionalitzats + immigrants_renta_baixa + pensionistes + pcc + maca + atdom,
                  data = dt.glm)
    it003.es.step.model <- stepAIC(lm.full, direction = "both", 
                                   trace = FALSE)
    summary(it003.es.step.model)
    pred <- predict(it003.es.step.model, newdata = dt.glm, type = "response")
    dt.glm[, pred0 := pred]
    cor(dt.glm[, res], dt.glm[, pred0], use = "pairwise.complete.obs")^2
    ggplot(dt.glm, aes(x=pred0, y=res, color=sexe)) + geom_point() + 
      facet_wrap(edat_c ~ ., ncol=3, scale="free_y")
```
