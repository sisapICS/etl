# coding: latin1

"""
Vacunes segons any de naixement
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u
import sisaptools as t


db = ("vacunes")

codis_vacunes = { 'Malaltia neumocòccica invasora 13 valent(VNC13)': 977,
                  'Malaltia meningocòccica invasora serogrup B': 548,
                  }   

class vacunesCohorts(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """ExecuciÃ³ seqÃ¼encial."""
        self.get_hash()
        self.get_vacunes()
        self.get_pob()        
        self.export_data()
        
    def get_hash(self):
        """hash to id_cip_sec"""
        u.printTime("id_cip_sec")
        self.id_to_hash = {}
        self.idsec_to_hash = {}
        
        sql = """select 
                    id_cip, id_cip_sec, hash_d, codi_sector 
                from 
                    u11"""
        for id, id_sec, hash, sec in u.getAll(sql, 'import'):
            self.id_to_hash[id] = hash
            self.idsec_to_hash[id_sec] = hash
       
    def get_vacunes(self):
        """aconseguim vacunes"""
        self.vacunats = []
        sql = 'select id_cip_sec, agrupador, datamin, dosis from ped_vacunes where agrupador in (548,977)'
        for id, agr, data, dosi in u.getAll(sql, 'nodrizas'):
            hash = self.idsec_to_hash[id] if id in self.idsec_to_hash else None
            self.vacunats.append([hash, agr, data, dosi])

        sql = 'select id_cip_sec, agrupador, datamin, dosis from eqa_vacunes where agrupador in (548,977)'
        for id, agr, data, dosi in u.getAll(sql, 'nodrizas'):
            hash = self.idsec_to_hash[id] if id in self.idsec_to_hash else None
            self.vacunats.append([hash, agr, data, dosi])
                
    def get_pob(self):
        """agafem poblaciÃ³ segons criteris edat"""
        self.resultats = []
        sql = "select id_cip_sec, sexe, data_naix, up, ates from assignada_tot"
        for id, sexe, naix, up, ates in u.getAll(sql, 'nodrizas'):
            hash = self.idsec_to_hash[id] if id in self.idsec_to_hash else None
            self.resultats.append([hash, naix, sexe,up, ates])
                            
    def export_data(self):
        """."""
        
        tb = "mst_assignada"
        cols = ("hash varchar(40)", "datanaix date", "sexe varchar(10)", "up varchar(10)","ates int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.resultats, tb, db)
        
        tb = "mst_vacunes"
        cols = ("hash varchar(40)", "vacuna int", "data date", "dosi int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.vacunats, tb, db)
    

   
if __name__ == '__main__':
    u.printTime("Inici")
        
    vacunesCohorts()
    
    u.printTime("Fi")