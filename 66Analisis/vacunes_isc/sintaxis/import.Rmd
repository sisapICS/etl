
```{r}
maia <- fread("../dades/consluta_vacunals.csv", sep = ";", header = T, dec = ",")
Encoding(maia$literal) <- "UTF-8"
Encoding(maia$medea_cat) <- "UTF-8"
```



```{r message=FALSE, warning=FALSE, include=FALSE}
drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password="",host = "10.80.217.68", port=3307, dbname="permanent")
nfetch <- -1
query <- dbSendQuery(con,
                     statement = "select * from permanent.mst_cohorts_vacunals where codi_vacuna = 548 and ates = '1'")
mst_cohorts_vacunals <- data.table(fetch(query, n = nfetch))
query <- dbSendQuery(con,
                     statement = "select * from permanent.cat_centres")
cat_centres <- data.table(fetch(query, n = nfetch))

dbDisconnect(con)

```

