# coding: latin1

"""
Vacunes segons any de naixement
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u
import sisaptools as t


db = ("vacunes")

codis_vacunes = { 'Malaltia neumocòccica invasora 13 valent(VNC13)': 977,
                  'Malaltia meningocòccica invasora serogrup B': 548,
                  }   

class vacunesCohorts(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """ExecuciÃ³ seqÃ¼encial."""
        self.get_hash()
        self.get_assignada()
        self.get_pob() 
        self.get_vacunes()       
        self.export_data()
        
    def get_hash(self):
        """hash to id_cip_sec"""
        u.printTime("id_cip_sec")
        self.id_to_hash = {}
        self.idsec_to_hash = {}
        
        sql = """select 
                    id_cip, id_cip_sec, hash_d, codi_sector 
                from 
                    u11"""
        for id, id_sec, hash, sec in u.getAll(sql, 'import'):
            self.id_to_hash[id] = hash
            self.idsec_to_hash[id_sec] = hash
       
    def get_assignada(self):
        """aconseguim """
        u.printTime("import assignada")
        self.pob_total = {}

        sql = "select id_cip_sec, usua_data_naixement, usua_sexe from assignada"
        for id, naix, sexe in u.getAll(sql, 'import'):
            self.pob_total[id] = {'naix': naix, 'sexe': sexe}
    
    def get_pob(self):
        """agafem població segons criteris edat"""
        u.printTime("assignada històrica")
        
        self.resultats = []
        
        sql = "select id_cip_sec, sexe, data_naix, up, ates from assignada_tot"
        for id, sexe, naix, up, ates in u.getAll(sql, 'nodrizas'):
            hash = self.idsec_to_hash[id] if id in self.idsec_to_hash else None
            self.resultats.append([2023, hash, naix, sexe,up, ates])
            
        sql =  "select id_cip_sec, dataany, up, ates from assignadahistorica where dataany>2012"
        for id, dany, up, ates in u.getAll(sql, 'import'):
            naix, sexe = None, None
            if id in self.pob_total:
                naix = self.pob_total[id]['naix']
                sexe = self.pob_total[id]['sexe']
            hash = self.idsec_to_hash[id] if id in self.idsec_to_hash else None
            self.resultats.append([dany, hash, naix, sexe,up, ates])
                
    
    def get_vacunes(self):
        """."""
        u.printTime("vacunes")
        
        vac_desc = {}
        codis_dx = []
        sql = 'select vacuna, agrupador from nodrizas.eqa_criteris a inner join import.cat_prstb040_new cpn on criteri_codi=antigen where agrupador in (977, 548)'
        for cod, agrd in u.getAll(sql, 'nodrizas'):
            codis_dx.append(cod)
            vac_desc[cod]=agrd
        in_crit = tuple(codis_dx)
        
        self.vacunes = []
        sql = """select 
                    id_cip_sec, va_u_cod,  va_u_data_vac, va_u_dosi 
                from 
                    vacunes 
                where 
                    va_u_data_baixa=0 and va_u_cod in {}""".format(in_crit)
        for id, vacs, data, dosi in u.getAll(sql, 'import'):
            hash = self.idsec_to_hash[id] if id in self.idsec_to_hash else None
            desc = vac_desc[vacs]
            self.vacunes.append([hash, desc, data, dosi])                
    
                            
    def export_data(self):
        """."""
        
        tb = "mst_assignada_anual"
        cols = ("dataany int", "hash varchar(40)", "datanaix date", "sexe varchar(10)", "up varchar(10)","ates int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.resultats, tb, db)
        
        tb = "mst_vacunes_tot"
        cols = ("hash varchar(40)", "vacuna varchar(100)", "data date", "dosi int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.vacunes, tb, db)
    

   
if __name__ == '__main__':
    u.printTime("Inici")
        
    vacunesCohorts()
    
    u.printTime("Fi")