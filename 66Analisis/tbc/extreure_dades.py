# -*- coding: utf8 -*-

"""
TBC per Jacobo, a nivell pcient. demanen TBC de 2016 a 2020 i mirar visites amb codis dx
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


dx_file =  "codis.txt"

simptomes = ['R05','R093','R042','R509','R61','R042','R079','R591','R599','R531','R630','R634','R52','R454','R0781','R091']

class tbc_(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_hashos()
        self.get_assignada()
        self.get_tbc()
        self.get_cmbdap()
        self.export()

    def get_cip(self):
        """hash to cip"""
        u.printTime("cip")
        self.hash_to_cip = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.hash_to_cip[hash] = cip
    
    
    def get_hashos(self):
        """."""
        u.printTime("u11")
        self.idcip_to_hash = {}
        sql = "select id_cip, codi_sector, hash_d from u11"
        for id, sec, hash in u.getAll(sql, "import"):
            self.idcip_to_hash[id] = {'hash': hash, 'sec': sec} 
    
    def get_assignada(self):
        """."""
        u.printTime("assignada")
        self.pob = {}
        sql = 'select id_cip, usua_sexe, usua_data_naixement, usua_uab_up, usua_abs_codi_abs  from assignada'
        for id, sexe, naix, up, abs in u.getAll(sql, 'import'):
            self.pob[id] = {'sexe': sexe, 'naix': naix, 'up': up, 'abs': abs}
            
    def get_tbc(self):
        """Pacients amb TBC de 2016 a 2020"""
        u.printTime("problemes")
        dx_codis = []
        tip_dx = {}
        self.pacients = []
        for (dx,  tip) in u.readCSV(dx_file, sep='@'):
            dx_codis.append(dx)
            tip_dx[dx] = tip
        in_crit = tuple(dx_codis)
        
        self.pac_tbc = c.defaultdict(set)
        sql = "select id_cip, pr_dde, pr_cod_ps, pr_up \
                       from problemes  \
                            where pr_cod_o_ps = 'C' and pr_hist = 1 and \
                            pr_cod_ps in {} \
                            and extract(year_month from pr_dde) >'201512' and  extract(year_month from pr_dde) <'202101' and \
                            pr_data_baixa = 0 group by id_cip,pr_dde,pr_cod_ps, pr_up".format(in_crit)
        for id, dat, codi, up in u.getAll(sql, 'import'):
            sexe = self.pob[id]['sexe'] if id in self.pob else None
            naix = self.pob[id]['naix'] if id in self.pob else None
            abs = self.pob[id]['abs'] if id in self.pob else None
            tipus = tip_dx[codi]
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            self.pacients.append([hash, sec, u.sexConverter(sexe), naix, tipus, codi, dat, abs])
            self.pac_tbc[(hash,sec)].add(dat)
                
    
    def get_cmbdap(self):
        """Treure visites cmbdAP"""
        u.printTime("cmbdap")
        
        self.visites = []
        sql = """SELECT cip_usuari_cip, codi_sector, cp_ecap_dvis, cp_d1, cp_d2, cp_d3, cp_d4, cp_d5,cp_d6, cp_ecap_categoria, cp_t_act, cp_up FROM cmbd_ap a
                INNER JOIN usutb011 b
                ON a.cp_ecap_cip=b.cip_cip_anterior AND codi_sector=cip_sector_codi_sector
                WHERE cp_ecap_dvis > DATE '2015-01-01' AND cp_ecap_dvis < DATE '2021-01-01'"""
        for hash, sector, dvis, d1,d2,d3,d4,d5,d6, categ, tip, up in u.getAll(sql, "redics"):
            if (hash,sector) in self.pac_tbc:
                dats = self.pac_tbc[(hash,sector)]
                for dat in dats:
                    dd = u.monthsBetween(dvis, dat)
                    if 0 <= dd <= 6:
                        print hash, sector
                        simp = 1 if (d1 in simptomes or d2 in simptomes or d3 in simptomes or d4 in simptomes or d5 in simptomes or d6 in simptomes) else 0
                        self.visites.append([hash,sector, dvis, simp, d1, d2, d3, d4, d5, d6, categ, tip, up])

    
    def export(self):
        """."""
        file = u.tempFolder + "tbc_2016_2020.txt"
        u.writeCSV(file, self.pacients, sep='@')
        
        file = u.tempFolder + "tbc_Visites.txt"
        u.writeCSV(file, self.visites, sep='@')
                
        
if __name__ == '__main__':
    u.printTime("Inici")
     
    tbc_()
    
    u.printTime("Final")                 
