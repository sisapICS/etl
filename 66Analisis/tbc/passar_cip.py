# -*- coding: utf8 -*-

"""
TBC per Jacobo, passar de hash a cip
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

import sisaptools as su

file = u.tempFolder + "tbc_2016_2020.txt"
file2 = u.tempFolder + "tbc_Visites.txt"


class tbc_(object):
    """."""

    def __init__(self):
        """."""
        
        #self.get_cip()
        #self.get_files()
        self.export()

    def get_cip(self):
        """hash to cip"""
        u.printTime("cip")
        self.hash_to_cip = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.hash_to_cip[hash] = cip

            
    def get_files(self):
        """Pacients amb TBC de 2016 a 2020"""
        u.printTime("files")
        self.dades1 = []
        self.dades2 = []
        for hash, sec, sexe, naix, tipus, codi, dat, abs in u.readCSV(file, sep='@'):
            cip = self.hash_to_cip[hash] if hash  in self.hash_to_cip else None
            self.dades1.append([cip, sexe, naix, tipus, codi, dat, abs])
            
        for hash,sector, dvis, simp, d1, d2, d3, d4, d5, d6, categ, tip, up in u.readCSV(file2, sep='@'):
            cip = self.hash_to_cip[hash] if hash in self.hash_to_cip else None
            self.dades2.append([cip, dvis, simp, d1, d2, d3, d4, d5, d6, categ, tip, up])
       
    def export(self):
        """."""
        u.printTime("export")
        file = "tbc_2016_2020_c.txt"
        filee = u.tempFolder + file
        #u.writeCSV(filee, self.dades1, sep='@')
        
        with su.SFTP("sisap") as sftp:
            sftp.put(filee, "pidirac/{}".format(file))

        
        filee = u.tempFolder + "tbc_visites_c.txt"
        #u.writeCSV(filee, self.dades2, sep='@')
                
        
if __name__ == '__main__':
    u.printTime("Inici")
     
    tbc_()
    
    u.printTime("Final")                 
