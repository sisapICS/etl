***
***

## Catàleg

### Catèleg de Centres SISAP

```{r}
dt.cat_centres.dm <- dt.cat_centres.raw
```

#### Medea

```{r}
dt.cat_centres.dm[, medea_c := factor(medea_c,
                                           levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"))]

dt.cat_centres.dm[medea_c %in% c("0R", "1R", "2R"), rural := "Rural"]
dt.cat_centres.dm[medea_c %in% c("1U", "2U", "3U", "4U"), rural := "Urbà"]
dt.cat_centres.dm[, rural := factor(rural,
                                         levels = c("Rural", "Urbà"),
                                         labels = c("Rural", "Urbà"))]

dt.cat_centres.dm[, medea_c2 := medea_c]
dt.cat_centres.dm[medea_c %in% c("0R", "1R", "2R"), medea_c2 := "Rural"]
dt.cat_centres.dm[, medea_c_2 := factor(medea_c2,
                                             levels = c("Rural", "1U", "2U", "3U", "4U"),
                                              labels = c("Rural", "1U", "2U", "3U", "4U"))]
```

### Catèleg de Centres DBC

```{r}
dt.cat_centres_dbc.dm <- dt.cat_centres_dbc.raw
dt.cat_centres_dbc.dm[, abs_cod := as.numeric(abs_cod)]
```

***

## Assignada

A continuació es detalla el procés de manipulació de les dades per aconseguir la informació necessaria per realitzar l'exploració.

```{r, echo = TRUE}
dt.assignada.dm <- dt.assignada.raw
gc.var <- gc()
```

### Afegir informació

S'afegit la informació dels catàlegs.

```{r}
 
dt.assignada.dm <- merge(dt.assignada.dm,
                         dt.cat_centres.dm,
                         by.x = "up",
                         by.y = "scs_codi",
                         all.x = TRUE)
```


```{r orden columnas}
#|

```

```{r data manager gc}
#rm(list = c("dt.ind.der.raw", "dt.korra.raw"))
gc.var <- gc()
```

