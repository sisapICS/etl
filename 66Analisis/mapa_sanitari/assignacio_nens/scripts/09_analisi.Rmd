***

# Anàlisi

```{r}

dt.assignada.ana <- dt.assignada.flt[, .(tip_eap, up, abs)]

# a <- dt.assignada.ana[up == "00002",]
# b <- a[, .N, .(tip_eap, up, up_rca)]
# b[, nchar_up_rca := nchar(up_rca)]
```

```{r}
#names(dt.assignada.ana)

dt.assignada.ana.up.agr <- dt.assignada.ana[, .N, .(tip_eap, up, abs)]

dt.assignada.ana.up.agr <- dt.assignada.ana.up.agr[,{
den = sum(N)
.SD[,.(N = N, porcentaje = sum(N)/den), by = abs]
}, by = .(up)]

dt.assignada.ana.up.agr <- dt.assignada.ana.up.agr[order(up, porcentaje),][, porcentaje := round(porcentaje*100,1)]

dt.assignada.ana.up.agr <- merge(dt.assignada.ana.up.agr,
                                 dt.cat_centres.dm[, .(scs_codi, ics_desc, tip_eap)],
                                 by.x = "up",
                                 by.y = "scs_codi",
                                 all.x = TRUE)
setnames(dt.assignada.ana.up.agr, 'ics_desc', 'up_desc')

dt.abs <- dt.cat_centres.dm[abs != 0, .(abs, scs_codi, ics_desc)]
setnames(dt.abs, c('abs', 'abs_scs_codi', 'abs_ics_desc'))

dt.assignada.ana.up.agr <- merge(dt.assignada.ana.up.agr,
                                 dt.abs,
                                 by.x = "abs",
                                 by.y = "abs",
                                 all.x = TRUE)

dt.assignada.ana.up.agr <- merge(dt.assignada.ana.up.agr,
                                 dt.cat_centres_dbc.dm,
                                 by.x = "abs",
                                 by.y = "abs_cod",
                                 all.x = TRUE)

dt.assignada.ana.up.agr <- dt.assignada.ana.up.agr[, .(tip_eap, up, up_desc, abs, abs_des, abs_scs_codi, abs_ics_desc, N, porcentaje)]
dt.assignada.ana.up.agr <- dt.assignada.ana.up.agr[order(tip_eap, up, -porcentaje)]
dt.assignada.ana.up.agr
```

## Exportació resultados

Exportació dels resultats en format excel per enviar a Mapa Sanitari

```{r}
write_xlsx(dt.assignada.ana.up.agr[abs != 0,], "D:/SISAP/sisap/66Analisis/mapa_sanitari/assignacio_nens/results/sisap_mapa_sanitari_assignacio_nens.xlsx")
```

```{r}
gc.var <- gc()
```


