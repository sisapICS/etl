
library('rmarkdown')
library('compareGroups')

render("01_prescripcio_hospital.Rmd", 
       output_format = "html_document",
       output_file = paste('prescripcio_hospital',
                           format(Sys.Date(),'%Y%m%d'),
                           format(Sys.time(),'%H%M'), sep = "_"),
       output_dir = "../results/",
       params = list(taules = c('tractaments_s6844'),
                     actualitzar_dades = FALSE,
                     skim = TRUE,
                     data = '2022-01-01',
                     tractament = 'ibp',
                     import.eval = FALSE,
                     datamanager.eval = FALSE)
)
gc()