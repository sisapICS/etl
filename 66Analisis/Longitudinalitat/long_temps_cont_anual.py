# -*- coding: utf8 -*-

"""
Repliquem indicadors individuals pel 2021, pero anuals
"""

import collections as c

import sisapUtils as u

u.printTime("inici")

periodes_calcul = {'202112': '202101'}

db = "permanent"

class Continuitat(object):
    """."""

    def __init__(self):
        """."""

        self.get_centres()
        self.get_id_cip_sec()
        self.get_usuaris()
        self.get_pob()
        for self.period in periodes_calcul:
            print self.period
            self.p_fi = periodes_calcul[self.period]
            self.get_visites()
            self.get_taula()
            self.process_sequencia()
            self.get_calculs()

    def get_centres(self):
        """."""
        sql = "select scs_codi from cat_centres"
        self.centres = {up: True for up, in u.getAll(sql, 'nodrizas')}

    def get_id_cip_sec(self):
        """hash to id_cip sec"""
        u.printTime("id_cip")
        self.hash_to_id = {}
        
        sql = """select 
                    id_cip, codi_sector,  hash_d 
                from 
                    u11"""
        for id, sec, hash in u.getAll(sql, 'import'):
            self.hash_to_id[(id)] = hash
    
    def get_usuaris(self):
        """."""
        self.usuaris = {}
        sql = "select left(ide_dni, 8), ide_categ_prof_c \
                from cat_pritb992 where ide_categ_prof_c in ('10999','10117')"
        for usu, categ in u.getAll(sql, "import"):
            self.usuaris[(usu)] = categ

    def get_pob(self):
        """."""
        u.printTime("Assignada")
        SIDICS_DB = ("dbs", "x0002")
        self.pob = {}
        sql = """select c_cip, C_SECTOR, C_up, c_metge, c_up, c_infermera
                from dbs.dbs_2021 where c_edat_anys > 14"""
        for hash, sector, up, uba, upinf, ubainf in u.getAll(sql, SIDICS_DB):
            self.pob[hash]  = {'up':up, 'uba':uba, 'upinf': upinf, 'ubainf': ubainf}

    def get_visites(self):
        """Agafem visites dels dos últims anys"""
        u.printTime("Visites")
        self.visitespacients = c.Counter()
        self.data = c.defaultdict(lambda: c.defaultdict(list))
        sql = "select id_cip, codi_sector, visi_up, left(visi_dni_prov_resp, 8), visi_data_visita, s_espe_codi_especialitat, visi_servei_codi_servei \
                    from visites \
                    where visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_dni_prov_resp<>'' and visi_col_prov_resp like ('1%') \
                     and extract(year_month from visi_data_visita)>='{0}' and extract(year_month from visi_data_visita)<= '{1}' ".format(self.p_fi, self.period)
        for id, _sector, up, col, dat, _categoria, _serveis in u.getAll(sql, "import"):
            if up in self.centres:
                if (col) in self.usuaris:
                    self.visitespacients[(id, col)] += 1
                    identify = str(id) 
                    self.data[identify][dat].append(col)
  
    def get_taula(self):
        """Calculem les tres dades necessàries per calcular els indicadors. A nivell de pacient necessitem:
            1) Nombre de professionals diferents
            2) Nombre de visites totals
            3) Nombre visites professional majoritari
            4) El sumatori dels quadrats de les visites de cada professional (per a calcular COC)
        """
        u.printTime("Càlculs")
        self.pacients = {}
        for (id, dummy_col), rec in self.visitespacients.items():
            Rq2 = rec * rec
            if (id) in self.pacients:
                self.pacients[(id)]['totals'] += rec
                self.pacients[(id)]['nprof'] += 1
                self.pacients[(id)]['rq2'] += Rq2
                majoritari = self.pacients[(id)]['vprof']
                if rec > majoritari:
                    self.pacients[(id)]['vprof'] = rec
            else:
                self.pacients[(id)] = {'totals': rec, 'nprof': 1, 'vprof': rec, 'rq2': Rq2}
        
    def process_sequencia(self):
        """Ordenem les visites per ordre temporal per poder calcular el de sequencia"""
        u.printTime("Seqüència")
        resultat = []
        for (id), dates in self.data.items():
            pac = ((id, dat, cols) for dat, cols in dates.items())
            colegiat = {}
            for i, (id, dat, cols) in enumerate(sorted(pac, key=lambda x: x[1]), start=1):
                for col in cols:
                    if col == colegiat:
                        seq = 1
                    else:
                        seq = 0
                        colegiat = col
                    identities = id
                    resultat.append((id, dat, col, i, seq))
        Stb = "Long_sequencia"
        Scolumns = ["id_cip_sec int",  "data date", "colegiat varchar(10)", "ordre int", "sequencia int"]
        u.createTable(Stb, "({})".format(", ".join(Scolumns)), db, rm=True)
        u.listToTable(resultat, Stb, db)

        self.indicador_sequencia = c.Counter()
        sql = "select id_cip_sec,  sequencia from {}".format(Stb)
        for id, seq in u.getAll(sql, db):
            self.indicador_sequencia[id,  'num'] += seq
            self.indicador_sequencia[id,  'den'] += 1
    
    def get_calculs(self):
        """Calculem els indicadors a nivell de pacient
        Aqui filtrem per població assignada i pacients entre 3 i 300 visites"""
        u.printTime("Més càlculs")
        upload = []
        for (id), dad in self.pacients.items():
            totals, nprof, vprof, rq2 = dad['totals'], dad['nprof'], dad['vprof'], dad['rq2']
            n1 = 1 - (nprof/(totals + 0.1))
            d1 = 1 - (1/(totals + 0.1))
            n3 = self.indicador_sequencia[id, 'num']
            d3 = self.indicador_sequencia[id,  'den']
            r1 = float(n1/d1)
            r2 = float(vprof) / float(totals)
            d3 = d3 - 1
            try:
                r3 = float(n3)/float(d3)
            except ZeroDivisionError:
                r3 = 0
            n4 = rq2 - totals
            d4 = totals * (totals - 1)
            try:
                r4 = float(n4) / float(d4)
            except ZeroDivisionError:
                r4 = 0
            hash = self.hash_to_id[id]
            if hash in self.pob:
                up, uba, upinf, ubainf = self.pob[hash]['up'], self.pob[hash]['uba'], self.pob[hash]['upinf'], self.pob[hash]['ubainf']
                upload.append([hash, up, uba, upinf, ubainf,  nprof, vprof, totals, n1, d1, n3, r1, r2, r3, r4])

              
        u.listToTable(upload, tb, db)

    

if __name__ == '__main__' :
        tb = "long_temps_cont_a"
        columns = ["hash varchar(40)", "up varchar(5)", "uba varchar(5)",  "upinf varchar(5)", "ubainf varchar(5)",  "nprof int", "vprof int", "totals int", "n1 double", "d1 double", "n3 double", "r1 double", "r2 double", "r3 double", "r4 double"]

        sql = "select scs_codi, ics_codi from cat_centres"
        centres = {up: br for up, br in u.getAll(sql, 'nodrizas')}

        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        Continuitat()

        u.printTime("Fi")