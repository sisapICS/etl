
```{r eval=FALSE, include=FALSE}
frequentacio <- frequentacio[V6 == "VISASSIG", .(tipprof = V1, 
                                 ics_codi = V3,
                                 freq = V8)]
frequentacio[, tipprof := factor(tipprof, levels = c("VISMF", "VISINF"), labels = c("MF", "INF"))]



longA <- longA[, .(ics_codi = V3,
                   V4,
                   tipprof = V6,
                   value = V8)]
longA <- dcast(longA, ics_codi + tipprof ~ V4, value.var = "value")
longA[, longA := NUM/DEN*100]
longA <- longA[tipprof %in% c("TIPPROF1", "TIPPROF4")]
longA[, tipprof := factor(tipprof, levels = c("TIPPROF1", "TIPPROF4"), labels = c("MF", "INF"))]
```

```{r}
visites_2023 <- visites_2023[V6 == "VISASSIG", .(visites = sum(V8)), .(tipprof = V1, periode = V2, ics_codi = V3)]
visites_2023[, tipprof := factor(tipprof, levels = c("VISMF", "VISINF"), labels = c("MF", "INF"))]
visites_2023[, data := format(as.Date(paste0("20", substr(periode, 2, 3), "-", substr(periode, 4, 5), "-", "01")), "%Y-%m")]
```

```{r}
pobass_2023 <- pobass_2023[, .(pobass = sum(V8)), .(ics_codi = V3)]
```

```{r}
frequentacio <- merge(visites_2023, pobass_2023, by = "ics_codi", all.x = T)
frequentacio[, freq := visites/pobass*100]
```




```{r}
# frequentacio <- frequentacio[V6 == "VISASSIG", .(tipprof = V1, 
#                                                  periode = V2,
#                                                  ics_codi = V3,
#                                                  freq = V8)]
# frequentacio[, tipprof := factor(tipprof, levels = c("VISMF", "VISINF"), labels = c("MF", "INF"))]
# frequentacio[, data := format(as.Date(paste0("20", substr(periode, 2, 3), "-", substr(periode, 4, 5), "-", "01")), "%Y-%m")]
longA <- longA[, .(periode = V2,
                   ics_codi = V3,
                   V4,
                   tipprof = V6,
                   value = V8)]
longA <- dcast(longA, periode + ics_codi + tipprof ~ V4, value.var = "value")
longA[is.na(NUM), NUM := 0]
longA[, longA := NUM/DEN*100]
longA <- longA[tipprof %in% c("TIPPROF1", "TIPPROF4")]
longA[, tipprof := factor(tipprof, levels = c("TIPPROF1", "TIPPROF4"), labels = c("MF", "INF"))]
longA[, data := format(as.Date(paste0("20", substr(periode, 2, 3), "-", substr(periode, 4, 5), "-", "01")), "%Y-%m")]

visurg <- visurg[V5 == "ANUAL", .(periode = V2,
                                  ics_codi = V3,
                                  V4,
                                  tipprof = V6,
                                  value = V8)]
visurg <- dcast(visurg, periode + ics_codi + tipprof ~ V4, value.var = "value")
visurg[is.na(NUM), NUM := 0]
visurg[, visurg := NUM/DEN*100]
visurg <- visurg[tipprof %in% c("TIPPROF1", "TIPPROF4")]
visurg[, tipprof := factor(tipprof, levels = c("TIPPROF1", "TIPPROF4"), labels = c("MF", "INF"))]
visurg[, data := format(as.Date(paste0("20", substr(periode, 2, 3), "-", substr(periode, 4, 5), "-", "01")), "%Y-%m")]

```



```{r}
korra <- korra[br %in% cat_centres$ics_codi]
korra <- korra[!edat %in% c("EC01",
                            "EC24",
                            "EC59",
                            "EC1014")]
korra <- merge(
  korra[, lapply(.SD, sum, na.rm = T), .SDcols = c("poblacio", "sum_dona", "sum_edat", "institucionalitzats", "immigrants_renta_baixa", "pensionistes", "pens_menys_65", "pcc", "maca", "atdom", "den_gma", "num_gma", "den_medea", "num_medea", "prof_grup9", "prof_grup2", "den_seccio", "ist", "pob_ocupada", "treb_baixa_qualitat", "estudis_baixos", "jove_sense_estudis", "imm_renda_baixa_idescat", "renda_minima"), by = .(ics_codi = br)], 
  korra[ates == 1][, lapply(.SD, sum, na.rm = T), .SDcols = c("poblacio", "sum_dona", "sum_edat", "institucionalitzats", "immigrants_renta_baixa", "pensionistes", "pens_menys_65", "pcc", "maca", "atdom", "den_gma", "num_gma", "den_medea", "num_medea", "prof_grup9", "prof_grup2", "den_seccio", "ist", "pob_ocupada", "treb_baixa_qualitat", "estudis_baixos", "jove_sense_estudis", "imm_renda_baixa_idescat", "renda_minima"), by = .(ics_codi = br)], by = "ics_codi"
  )

korra <- korra[, ":=" (
  dones_assignada = sum_dona.x/poblacio.x*100,
  edat_assignada = sum_edat.x/poblacio.x,
  institucionalitzats_assignada = institucionalitzats.x/poblacio.x*100,
  immigrants_renta_baixa_assignada = immigrants_renta_baixa.x/poblacio.x*100,
  pensionistes_assignada = pensionistes.x/poblacio.x*100,
  pens_menys_65_assignada = pens_menys_65.x/poblacio.x*100,
  pcc_assignada = pcc.x/poblacio.x*100,
  maca_assignada = maca.x/poblacio.x*100,
  atdom_assignada = atdom.x/poblacio.x*100,
  gma_assignada = num_gma.x/den_gma.x,
  medea_c_assignada = num_medea.x/den_medea.x,
  ist_assignada = ist.x/den_seccio.x,
  pob_ocupada_assignada = pob_ocupada.x/den_seccio.x,
  treb_baixa_qualitat_assignada = treb_baixa_qualitat.x/den_seccio.x,
  estudis_baixos_assignada = estudis_baixos.x/den_seccio.x,
  jove_sense_estudis_assignada = jove_sense_estudis.x/den_seccio.x,
  imm_renda_baixa_idescat_assignada = imm_renda_baixa_idescat.x/den_seccio.x,
  renda_minima_assignada = renda_minima.x/den_seccio.x,
  
  dones_atesa = sum_dona.y/poblacio.y*100,
  edat_atesa = sum_edat.y/poblacio.y,
  institucionalitzats_atesa = institucionalitzats.y/poblacio.y*100,
  immigrants_renta_baixa_atesa = immigrants_renta_baixa.y/poblacio.y*100,
  pensionistes_atesa = pensionistes.y/poblacio.y*100,
  pens_menys_65_atesa = pens_menys_65.y/poblacio.y*100,
  pcc_atesa = pcc.y/poblacio.y*100,
  maca_atesa = maca.y/poblacio.y*100,
  atdom_atesa = atdom.y/poblacio.y*100,
  gma_atesa = num_gma.y/den_gma.y,
  medea_c_atesa = num_medea.y/den_medea.y,
  ist_atesa = ist.y/den_seccio.y,
  pob_ocupada_atesa = pob_ocupada.y/den_seccio.y,
  treb_baixa_qualitat_atesa = treb_baixa_qualitat.y/den_seccio.y,
  estudis_baixos_atesa = estudis_baixos.y/den_seccio.y,
  jove_sense_estudis_atesa = jove_sense_estudis.y/den_seccio.y,
  imm_renda_baixa_idescat_atesa = imm_renda_baixa_idescat.y/den_seccio.y,
  renda_minima_atesa = renda_minima.y/den_seccio.y
)]

korra_assignada <- korra[, .(ics_codi, dones_assignada, edat_assignada, institucionalitzats_assignada, immigrants_renta_baixa_assignada, pensionistes_assignada, pens_menys_65_assignada, pcc_assignada, maca_assignada, atdom_assignada, gma_assignada, medea_c_assignada, ist_assignada, pob_ocupada_assignada, treb_baixa_qualitat_assignada, estudis_baixos_assignada, jove_sense_estudis_assignada, imm_renda_baixa_idescat_assignada, renda_minima_assignada)]
names(korra_assignada) <- c("ics_codi", "dones", "edat", "institucionalitzats", "immigrants_renta_baixa", "pensionistes", "pens_menys_65", "pcc", "maca", "atdom", "gma", "medea_c", "ist", "pob_ocupada", "treb_baixa_qualitat", "estudis_baixos", "jove_sense_estudis", "imm_renda_baixa_idescat", "renda_minima")
korra_assignada[, atesa := "assignada"]

korra_atesa <- korra[, .(ics_codi, dones_atesa, edat_atesa, institucionalitzats_atesa, immigrants_renta_baixa_atesa, pensionistes_atesa, pens_menys_65_atesa, pcc_atesa, maca_atesa, atdom_atesa, gma_atesa, medea_c_atesa, ist_atesa, pob_ocupada_atesa, treb_baixa_qualitat_atesa, estudis_baixos_atesa, jove_sense_estudis_atesa, imm_renda_baixa_idescat_atesa, renda_minima_atesa)]
names(korra_atesa) <- c("ics_codi", "dones", "edat", "institucionalitzats", "immigrants_renta_baixa", "pensionistes", "pens_menys_65", "pcc", "maca", "atdom", "gma", "medea_c", "ist", "pob_ocupada", "treb_baixa_qualitat", "estudis_baixos", "jove_sense_estudis", "imm_renda_baixa_idescat", "renda_minima")
korra_atesa[, atesa := "atesa"]

korra <- rbind(korra_assignada, korra_atesa)
korra[, atesa := factor(atesa, levels = c("atesa", "assignada"))]


```

```{r eval=FALSE, include=FALSE}
dades <- merge(frequentacio, longA[, .(ics_codi, tipprof, longA)], by = c("ics_codi", "tipprof"))
dades <- merge(dades, cat_centres[, .(ics_codi, amb_codi, amb_desc, medea, tip_eap)], by = "ics_codi", all.x = T)
dades <- merge(dades, korra, by = c("ics_codi"), all.x = T)

dades <- dades[amb_codi != "00"]
dades <- dades[tip_eap != "N"]

dades[, medea := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"))]
dades[, rural := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), labels = c("Rural", "Rural", "Rural", "Urbà", "Urbà", "Urbà", "Urbà"))]

fwrite(dades, "../dades/dades_202402.txt", sep = "{", row.names = F)
```

```{r}
dades <- merge(frequentacio[, .(data, ics_codi, tipprof, freq)], longA[, .(data, ics_codi, tipprof, longA)], by = c("data", "ics_codi", "tipprof"), all.x = T, all.y = T)
dades <- merge(dades, visurg[, .(data, ics_codi, tipprof, visurg)], by = c("data", "ics_codi", "tipprof"), all.x = T, all.y = T)
dades <- merge(dades, cat_centres[, .(ics_codi, amb_codi, amb_desc, medea, tip_eap)], by = "ics_codi", all.x = T)
dades <- merge(dades, korra[atesa == "atesa"], by = c("ics_codi"), all.x = T)

dades <- dades[amb_codi != "00"]
dades <- dades[tip_eap != "N"]

dades[, medea := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"))]
dades[, rural := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), labels = c("Rural", "Rural", "Rural", "Urbà", "Urbà", "Urbà", "Urbà"))]
dades[, medea_u := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), labels = c("Rural", "Rural", "Rural", "1U", "2U", "3U", "4U"))]

fwrite(dades, "../dades/dades_2023.txt", sep = "{", row.names = F)
```

