
```{r eval=FALSE, include=FALSE}
dades <- fread("../dades/dades_202402.txt", sep = "{", header = T)
```

```{r}
dades <- fread("../dades/dades_2023.txt", sep = "{", header = T)
```


```{r}
taula <- dades[, 
                 lapply(.SD, function(x){
                   paste0(format(round(mean(x, na.rm = T), 2), decimal.mark = ",", big.mark = "."), 
                          " [",
                          format(round(quantile(x, .25, na.rm = T), 2), decimal.mark = ",", big.mark = "."),
                          "; ",
                          format(round(quantile(x, .75, na.rm = T), 2), decimal.mark = ",", big.mark = "."),
                          "]"
                          )
                   }
                   ),
                 by = .(tipprof, data),
                 .SDcols = c("longA", "freq", "visurg")
] 
taula[order(tipprof)][, -1] %>% 
  kable(col.names = c("Data", "LONG_A", "Freqüentació", "VISURG"),
        caption = "Taula descriptiva dels indicadors de longitudinalitat i freqüentació per tipus de professional")  %>%
  kable_classic(full_width = F) %>% 
  pack_rows("INF", 1, 12) %>%
  pack_rows("MF", 13, 24)
```

```{r}
aux <- unique(dades[, .(ics_codi, medea, rural)])
taula <- rbind(
  aux[, .(
    variable = "MEDEA",
    .N), .(cat = medea)],
  aux[, .(
    variable = "Ruralitat",
    .N), .(cat = rural)])
taula[, total := sum(N), variable]
taula[, valor := paste0(N, 
                        " (",
                        format(round(N/total*100, 2), decimal.mark = ",", big.mark = "."),
                        "%",
                        ")"
                        )]
taula[, .(cat, valor)][order(cat)] %>% 
  kable(col.names = c("Tipus d'EAP", "N (%)"), caption = "Taula descriptiva sobre el tipus d'EAP")  %>%
  kable_classic(full_width = F)
  
```


```{r}
aux <- unique(dades[, .(ics_codi, atesa, dones, edat, institucionalitzats, immigrants_renta_baixa, pensionistes, pens_menys_65, pcc, maca, atdom, gma, medea_c, ist, pob_ocupada, treb_baixa_qualitat, estudis_baixos, jove_sense_estudis, imm_renda_baixa_idescat, renda_minima)])
aux <- aux[, 
                 lapply(.SD, function(x){
                   paste0(format(round(mean(x), 2), decimal.mark = ",", big.mark = "."), 
                          " [",
                          format(round(quantile(x, .25), 2), decimal.mark = ",", big.mark = "."),
                          "; ",
                          format(round(quantile(x, .75), 2), decimal.mark = ",", big.mark = "."),
                          "]"
                          )
                   }
                   ),
                 by = .(atesa),
                 .SDcols = c("dones", "edat", "institucionalitzats", "immigrants_renta_baixa", "pensionistes", "pens_menys_65", "pcc", "maca", "atdom", "gma", "medea_c", "ist", "pob_ocupada", "treb_baixa_qualitat", "estudis_baixos", "jove_sense_estudis", "imm_renda_baixa_idescat", "renda_minima")
] 
taula <- as.data.table(t(aux[, -1]), keep.rownames = T)
names(taula) <- c("", unlist(aux[, 1]))
taula %>% 
  kable(col.names = c("Variables Korra", "Sobre població atesa"), caption = "Taula descriptiva de les variables Korra segons el tipus de població")  %>%
  kable_classic(full_width = F)
  
```
