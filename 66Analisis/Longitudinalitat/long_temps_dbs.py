# coding: utf8

import collections as c
import datetime as d

import sisapUtils as u


tb = "dbs_2023"
db = "longitudinalitat"


class Long_temps(object):
    """."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_dbs2023()
        self.export_files()
            
    def get_dbs2023(self):
        """Dades del dbs"""
        u.printTime("DBS 2021")
        self.dades_dbs = []
        SIDICS_DB = ("dbs", "x0002")
        sql = """select c_cip, C_SECTOR, c_up, C_edat_anys, c_sexe, C_GMA_CODI, C_GMA_COMPLEXITAT, C_GMA_N_CRONIQUES, C_ULTIMA_VISITA_EAP, C_VISITES_ANY, C_NACIONALITAT, C_INSTITUCIONALITZAT, PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA, PS_DEPRESSIO_DATA, PS_ANSIETAT_DATA,
                    V_RCV_DATA, V_RCV_VALOR,
                     F_HTA_IECA_ARA2, F_HTA_CALCIOA, F_HTA_BETABLOQ, F_HTA_DIURETICS, F_HTA_ALFABLOQ, F_HTA_ALTRES, F_HTA_COMBINACIONS,
                    F_DIAB_ADO, F_DIAB_INSULINA, F_MPOC_ASMA,F_ANTIAGREGANTS_ANTICOA, F_AINE, F_ANALGESICS, 
                    F_ANTIDEPRESSIUS, F_ANSIO_HIPN, F_ANTIPSICOTICS, F_ANTIULCEROSOS, F_ANTIESP_URI, F_CORTIC_SISTEM,
                    F_ANTIEPILEPTICS,F_HIPOLIPEMIANTS, F_MHDA_VIH
                from dbs.dbs_2023"""
        for (hash, sector, up, edat, sexe, gma_codi, gma_cplx, gma_croniques, ultima_vis, visites_any, nac, insti,
             hta, dm1, dm2, mpoc,
             asma, bc, ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi, depressio, ansietat,
             rcv_dat, rcv_val,
             ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
             ado, insulina, f_mpoc, antiag, aine, analg,
             antidep, ansiol, antipsic, ulcer, anties, cortis,
             epil, hipol, mhda) in u.getAll(sql, SIDICS_DB):
            self.dades_dbs.append([hash, up, edat,  sexe, gma_codi, gma_cplx, gma_croniques, ultima_vis, visites_any, nac, insti,
                            hta, dm1, dm2,  mpoc,asma, bc, ci, mcv, ic, acfa, valv, hepat, vhb, vhc, neo, mrc, obes, vih, sida,
                                     demencia, artrosi, depressio, ansietat,
                                     ieca, calcioa, betb, diur, alfab, hta_alt, hta_comb,
                                     ado, insulina, f_mpoc, antiag, aine, analg,
                                     antidep, ansiol, antipsic, ulcer, anties, cortis,
                                     epil, hipol])
       
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
       
        cols = ("id_cip_sec varchar(40)", "up varchar(5)", "edat int", "sexe varchar(10)", "gma_codi int", "gma_complexitat int", "gma_n_croniques int", "c_ultima_visita_eap date","c_visites_any int", 
        "c_nacionalitat varchar(50)", "c_institucionalitzat int", 
            "ps_hta date", "PS_dm1 date", "PS_dm2 date","PS_mpoc date", "PS_asma date", "PS_bc date", "PS_ci date", "PS_mcv date",
            "PS_ic date", "PS_acfa date", "PS_valv date", "PS_hepat date", "PS_vhb date", "PS_vhc date", "PS_neo date", "PS_mrc date", "PS_obes date",
            "PS_vih date", "PS_sida date", "PS_demencia date", "PS_artrosi date", "PS_DEPRESSIO date", "ps_ansietat date",
            "F_HTA_IECA_ARA2 varchar(500)", "F_HTA_CALCIOA varchar(500)","F_HTA_BETABLOQ varchar(500)","F_HTA_DIURETICS varchar(500)",
            "F_HTA_ALFABLOQ varchar(500)", "F_HTA_ALTRES varchar(500)", "F_HTA_COMBINACIONS varchar(500)",
             "F_DIAB_ADO varchar(500)", "F_DIAB_INSULINA varchar(500)", "F_MPOC_ASMA varchar(500)", 
             "F_ANTIAGREGANTS_ANTICOA varchar(500)", "F_AINE varchar(500)", "F_ANALGESICS varchar(500)",
             "F_ANTIDEPRESSIUS varchar(500)", "F_ANSIO_HIPN varchar(500)", "F_ANTIPISCOTICS varchar(500)",
             "F_ANTIULCEROSOS varchar(500)", "F_ANTIESP_URI varchar(500)", "F_CORTIC_SISTEM varchar(500)",
             "F_ANTIEPILEPTICS varchar(500)", "F_HIPOLIPEMIANTS varchar(500)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.dades_dbs, tb, db)
                           
if __name__ == '__main__':
    u.printTime("Inici")
     
    Long_temps()

    u.printTime("Fi")