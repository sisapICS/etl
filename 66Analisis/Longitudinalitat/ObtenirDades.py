# coding: utf8

"""
Procés per crear la base de dades a la carpeta permanent per a anàlisi de la Longitudinalitat a nivell UP
Les dades s'extreuen a través del DS de SIDIAP, projecte [SISAP] Longitudinalitat i 
a la base de dades long_cont
"""

anys = [2017, 2018]

import collections as c
import datetime as d

import sisapUtils as u

tb = "long_up"
tb_pac = "long_pacient"
db = "permanent"
sidiap = ("long_cont", "nym_proj")

inici = "01012017"
fi = "20181231"

ind_long = ['CONT0001', 'CONT0002', 'CONT0003', 'CONT0004', 'CONT0002d', 'nprof']

class LongCont(object):
    """."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_long()
        self.get_hashos()
        self.get_visites()
        self.get_pob_hist()
        self.get_atdom()
        self.get_pob()
        self.get_gma()
        self.get_medea()
        self.export_dades()
 
    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: br for (up, br) in u.getAll(*sql)}
    
    def get_long(self):
        """De la taula longitudinalitat de sidiap"""
        self.long = {}
        sql = "select id, cod, val from longitudinalitat where dat ={}".format(fi)
        for id, cod, val in u.getAll(sql, sidiap):
            if cod in ind_long:
                if cod == 'CONT0002d':
                    cod = 'nvisites'
                self.long[(id, cod)] = val

    def get_hashos(self):
        """."""
        self.idcip_to_hash = {}
        sql = "select id_cip, codi_sector, hash_d from u11"
        for id, sec, hash in u.getAll(sql, "import"):
            self.idcip_to_hash[id] = {'hash': hash, 'sec': sec}                

    def get_visites(self):
        """Agafem visites dels dos últims anys de sisap..."""          
        self.visitespacients = c.Counter()
        for table in u.getSubTables('visites'):
            dat, = u.getOne('select year(visi_data_visita) from {} limit 1'.format(table), "import")
            if dat in anys:
                sql = "select id_cip, visi_up, visi_col_prov_resp, visi_data_visita \
                        from {} \
                        where visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_servei_codi_servei = 'MG' and visi_col_prov_resp <> 0".format(table)
                for id, up, col, dat in u.getAll(sql, "import"):
                    if up in self.centres:
                        hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
                        self.visitespacients[(sec, hash)] += 1
    
    def get_pob_hist(self):
        """Agafem població assignada de la històrica"""
        self.ass = {}
        sql = "select id_cip, up, ates from assignadahistorica where dataany='2018'"
        for id, up, ates in u.getAll(sql, "import"):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            self.ass[(sec, hash)] = {'up': up, 'ates': ates}
   
    def get_atdom(self):
        """Pacients en ATDOM a final de període."""
        self.atdoms = {}
        sql = "select id from diagnostics where agr = 'ATDOM' and (dat<={0} and (dbaixa='' or dbaixa>{0}) )".format(fi)
        for id, in u.getAll(sql, sidiap):
           self.atdoms[id]= True
    
    def get_pob(self):
        """Agafem poblacio de sidiap pero la filtrem per assignadahistorica"""
        self.dades = {}
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = "select id, id_persistent, situacio, sector, up, sexe, edat, nacionalitat, insti from poblacio where edat>14 and sortida={}".format(fi)
        for id1, id, sit, sec, up, sexe, edat, nac, insti in u.getAll(sql, sidiap):
            if up in self.centres:
                if sit == 'A':
                    hash, sec = id.split(':')[0], id.split(':')[1]
                    atd = 1 if id1 in self.atdoms else 0
                    nac1 = 1 if nac in renta else 0
                    cont001 = self.long[(id1, 'CONT0001')] if (id1, 'CONT0001') in self.long else None
                    cont002 = self.long[(id1, 'CONT0002')] if (id1, 'CONT0002') in self.long else None
                    cont003 = self.long[(id1, 'CONT0003')] if (id1, 'CONT0003') in self.long else None
                    cont004 = self.long[(id1, 'CONT0004')] if (id1, 'CONT0004') in self.long else None
                    nvisites = self.long[(id1, 'nvisites')] if (id1, 'nvisites') in self.long else None
                    nprof = self.long[(id1, 'nprof')] if (id1, 'nprof') in self.long else None
                    nvis = 0
                    if (sec, hash) in self.visitespacients:
                        nvis = self.visitespacients[(sec, hash)]
                    if nvisites == None:
                        nvisites=nvis
                    self.dades[id] = {
                              'sector': sec, 'up': up, 'sex': sexe,'edat': edat,
                              'renta': nac1, 'gma_cod': None, 'gma_cmplx': None,'gma_num': None, 'medea': None, 'insti': insti, 'atdom': atd, 
                              'C1': cont001, 'C2': cont002, 'C3': cont003, 'C4': cont004, 'nvisites': nvisites, 'nprof': nprof}
    
    def get_gma(self):
        """Obté els tres valors de GMA."""
        sql = ("select id_cip, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        for id, cod, cmplx, num in u.getAll(*sql):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            if idh in self.dades:
                self.dades[idh]['gma_cod'] = cod
                self.dades[idh]['gma_cmplx'] = cmplx
                self.dades[idh]['gma_num'] = num

    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            if idh in self.dades and sector in valors:
                self.dades[idh]['medea'] = valors[sector]
    
   
    def export_dades(self):
        """."""
        upload = [(id, d['up'], d['sex'], d['edat'],d['renta'], d['gma_cod'], d['gma_cmplx'],d['gma_num'], d['medea'], d['insti'], d['atdom'],
                    d['C1'], d['C2'], d['C3'], d['C4'], d['nvisites'], d['nprof'] )
                 for id, d in self.dades.items()]

        columns = ["id varchar(100)",   "up varchar(5)", "sexe varchar(1)",
                    "edat int", "nac1 int",  'gma_codi varchar(3)', 'gma_complexitat double',
                   'gma_num_croniques int', 'medea_seccio double',
                   'institucionalitzat int', 'atdom int', "cont001 double", "cont002 double", "cont003 double", "cont004 double",
                     "nvisites int", "nprof int"]
        u.createTable(tb_pac, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb_pac, db)
    
if __name__ == "__main__":
    LongCont()

    