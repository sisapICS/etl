# -*- coding: latin1 -*-

"""
Agafem totes les dades i les ajuntem en una taula
"""

import collections as c
import psutil as p

import sisapUtils as u


tb = "long_temps_pacient_final"
db = "permanent"
imp = "import"

SERVEIS_GC = ("INFG", "UGC", "GCAS")


class Longitudinalitat(object):
    """."""

    def __init__(self):
        """."""
        self.get_long_cont()
        self.get_long_contA()
        self.get_herfindahl()
        self.get_temps()
        self.get_slicc()
        self.get_dbs21()


    def get_long_cont(self):
        """."""
        u.printTime("Long")

        self.long = {}
        sql = """select hash, nprof, totals, r1, r2, r3, r4 from permanent.long_temps_cont"""
        for hash, nprof,vprof, r1, r2,r3,r4 in u.getAll(sql, db):
            self.long[hash]  = {'nprof': nprof, 'vprof':vprof, 'r1': r1, 'r2':r2, 'r3':r3, 'r4':r4}

    def get_long_contA(self):
        """."""
        u.printTime("Long anual")

        self.longA = {}
        sql = """select hash, nprof, totals, r1, r2, r3, r4 from permanent.long_temps_cont_a"""
        for hash, nprof,vprof, r1, r2,r3,r4 in u.getAll(sql, db):
            self.longA[hash]  = {'nprof': nprof, 'vprof':vprof, 'r1': r1, 'r2':r2, 'r3':r3, 'r4':r4}
            
    def get_herfindahl(self):
        """."""
        u.printTime("Index herfindahl")

        self.long_HH = {}
        sql = """select id_cip_sec, totals, nprof, rq2 from permanent.long_temps_herfindahl"""
        for hash, totals, nprof, rq2 in u.getAll(sql, db):
            self.long_HH[hash]  = {'totals':totals, 'nprof':nprof, 'rq2':rq2}
            
    def get_temps(self):
        """."""
        u.printTime("temps")
        self.temps21 = {}
        sql = """select id_cip_sec, ates, data_naix, primera_visitaM , ultima_visitaM from permanent.long_temps_21"""
        for id, ates, naix, primera, ultima in u.getAll(sql, db):
            self.temps21[id] = {'ates': ates, 'naix': naix, 'primera': primera, 'ultima':ultima}
            
    
    def get_slicc(self):
        u.printTime("SLICC")
        self.dades = c.Counter()
        sql = "select pac_id, pac_up, pac_uba, pac_ubainf, \
                          modul_serv_homol, modul_eap, \
                          prof_responsable, prof_delegat, modul_relacio, \
                          prof_rol_gestor, modul_serv_gestor, \
                          right(year(vis_data),2),date_format(vis_data,'%m') \
                   from permanent.long_temps_slicc \
                   where \
                         prof_numcol <> '' and \
                         vis_laborable = 1 and modul_serv_homol='MG'"
        for (id, up, uba, ubainf,  serv, eap, resp,
                 deleg, rel, gc_rol, gc_serv, anys, mes) in u.getAll(sql, db):
            gc = (gc_rol == 1 or gc_serv == 1)
            self.dades[(id, 'totals')] += 1
            if resp == 1 or gc == 1 or deleg == 1:
                self.dades[(id, 'ref')] +=1
            if resp ==1:
                self.dades[(id, 'resp')] +=1
            if deleg == 1:
                self.dades[(id, 'deleg')] +=1           
            
    def get_dbs21(self):
        u.printTime("DBS")
        
        self.upload = []
        sql = "select id_cip_sec  from long_temps_dbs where edat>14"
        for id,  in u.getAll(sql, db):
            nprof = self.long[id]['nprof'] if id in self.long else 0
            vprof = self.long[id]['vprof'] if id in self.long else 0
            r1 = self.long[id]['r1'] if id in self.long else 0
            r2 = self.long[id]['r2'] if id in self.long else 0
            r3 = self.long[id]['r3'] if id in self.long else 0
            r4 = self.long[id]['r4'] if id in self.long else 0
            nprofA = self.longA[id]['nprof'] if id in self.longA else 0
            vprofA = self.longA[id]['vprof'] if id in self.longA else 0
            r1A = self.longA[id]['r1'] if id in self.longA else 0
            r2A = self.longA[id]['r2'] if id in self.longA else 0
            r3A = self.longA[id]['r3'] if id in self.longA else 0
            r4A = self.longA[id]['r4'] if id in self.longA else 0
            tot_hh = self.long_HH[id]['totals'] if id in self.long_HH else 0
            nprofH = self.long_HH[id]['nprof'] if id in self.long_HH else 0
            rq2 = self.long_HH[id]['rq2'] if id in self.long_HH else 0
            tot_slicc = self.dades[(id, 'totals')] if (id, 'totals') in self.dades else 0
            ref_slicc = self.dades[(id, 'ref')] if (id, 'ref') in self.dades else 0
            resp_slicc = self.dades[(id, 'resp')] if (id, 'resp') in self.dades else 0
            deleg_slicc = self.dades[(id, 'deleg')] if (id, 'deleg') in self.dades else 0
            ates = self.temps21[id]['ates'] if id in self.temps21 else 0
            naix = self.temps21[id]['naix'] if id in self.temps21 else None
            primera = self.temps21[id]['primera'] if id in self.temps21 else None
            ultima = self.temps21[id]['ultima'] if id in self.temps21 else None
            self.upload.append([id, ates, naix, primera, ultima, nprof, vprof, r1, r2, r3, r4, nprofA, vprofA, r1A, r2A, r3A, r4A,  rq2, tot_slicc, ref_slicc, resp_slicc, deleg_slicc])
            
        u.listToTable(self.upload, tb, db)
            
if __name__ == "__main__":

    columns = ["id_cip_sec varchar(40)", "ates int", "naixement date", "primera_visita date", "ultima_visita date", "nprof2 int", "visites_tot2 int", "cont1 double", "cont2 double", "cont3 double", "cont4 double", "nprof1 int", "visites_tot1 int",
            "cont1A double", "cont2A double", "cont3A double", "cont4A double", "HH double", "visites_slicc_tot int", "visites_slicc_ref int", "visites_slicc_resp int", "visites_slicc_deleg int"]

    u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
    Longitudinalitat()
