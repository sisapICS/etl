# coding: utf8

"""
Procés per treure dades de longitudinalitat de inf i comparar en pacient amb i sense UPP
"""

anys = [2017, 2018]

import collections as c
import datetime as d

import sisapUtils as u

tb_pac = "long_pacient_upp_tip"
db = "permanent"


class LongCont(object):
    """."""

    def __init__(self):
        """Execució seqüencial."""
       
        self.get_hashos()
        self.get_upp()
        self.export_dades()
 
    
    def get_hashos(self):
        """."""
        self.idcip_to_hash = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        for id, sec, hash in u.getAll(sql, "import"):
            self.idcip_to_hash[id] = {'hash': hash, 'sec': sec} 
    
    def get_upp(self):
        """Agrupadors UPP és el 91 (obert) i 92 (tancat). però uso només els codis cim i els busco a import"""
        agrs = []
        criters = {}
        self.upload = []
        sql = "select agrupador,criteri_codi from eqa_criteris where agrupador in (91, 833)"
        for agrupador, criteri in u.getAll(sql, 'nodrizas'):
            agrs.append(criteri)
            criters[criteri] = agrupador
        in_crit = tuple(agrs)
        sql = "select id_cip_sec, pr_dde,if(pr_dba = 0,data_ext,pr_dba), pr_cod_ps from problemes, nodrizas.dextraccio \
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext) and pr_cod_ps in {0} \
        and pr_dde > date_add(data_ext, interval -3 year)".format(in_crit)
        for id, dde, dba, ps in u.getAll(sql, 'import'):
            if ps in criters:
                agr = criters[ps]
                hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
                idh = hash + ':' + sec
                self.upload.append([idh, dde, dba, agr])
    
                    
    def export_dades(self):
        """."""
        columns = ["id varchar(100)",    "dini_upp date", "dfi_upp date", "agr int"]
        u.createTable(tb_pac, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb_pac, db)
    
if __name__ == "__main__":
    LongCont()

    