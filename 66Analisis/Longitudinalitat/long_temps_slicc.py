# -*- coding: latin1 -*-

"""
Repliquem nodriza dels antics indicadors de longitudinalitat pel tema del càlcul del SLICC
"""

import collections as c
import psutil as p

import sisapUtils as u


tb = "long_temps_slicc"
db = "permanent"
imp = "import"

SERVEIS_GC = ("INFG", "UGC", "GCAS")


class Longitudinalitat(object):
    """."""

    def __init__(self):
        """."""
        self.create_tables()
        self.get_pob()
        self.get_id_cip_sec()
        self.get_especialitats()
        self.get_professionals()
        self.get_moduls()
        self.get_responsable()
        self.get_delegats()
        self.get_gestors()
        self.get_tipus()
        self.get_serveis()
        self.get_centres()
        self.get_visites()

    def create_tables(self):
        """."""
        cols = ("pac_id varchar(40)", "pac_up varchar(5)", "pac_uba varchar(5)",
                "pac_ubainf varchar(5)", 
                "prof_dni varchar(10)", "prof_numcol varchar(10)",
                "prof_categ varchar(5)", "prof_responsable int",
                "prof_delegat int", "prof_rol_gestor int",
                "modul_serv varchar(5)", "modul_espe varchar(5)",
                "modul_serv_homol varchar(5)", "modul_serv_gestor int",
                "modul_codi varchar(5)", "modul_up varchar(5)",
                "modul_eap int", "modul_uba varchar(5)",
                "modul_relacio varchar(1)", "vis_data date",
                "vis_laborable int", "vis_tipus varchar(4)",
                "vis_cita varchar(1)", "vis_tipus_homol varchar(5)",
                "vis_etiqueta varchar(4)")

        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)

    def get_pob(self):
        """."""
        u.printTime("Assignada")
        SIDICS_DB = ("dbs", "x0002")
        self.pob = {}
        sql = """select c_cip, C_SECTOR, C_up, c_metge, c_up, c_infermera
                from dbs.dbs_2021 where c_edat_anys > 14"""
        for hash, sector, up, uba, upinf, ubainf in u.getAll(sql, SIDICS_DB):
            self.pob[hash]  = {'up':up, 'uba':uba, 'upinf': upinf, 'ubainf': ubainf}

        
    def get_id_cip_sec(self):
        """hash to id_cip sec"""
        u.printTime("id")
        self.hash_to_id = {}
        
        sql = """select 
                    id_cip, codi_sector,  hash_d 
                from 
                    u11"""
        for id, sec, hash in u.getAll(sql, 'import'):
            self.hash_to_id[(id)] = hash
    
    
    def get_especialitats(self):
        """."""
        u.printTime("espe")
        sql = "select espe_codi_especialitat, espe_especialitat \
               from cat_pritb023"
        self.especialitats = {cod: des for (cod, des) in u.getAll(sql, imp)}

    def get_professionals(self):
        """."""
        u.printTime("prof")
        sql = "select codi_sector, prov_dni_proveidor, prov_categoria \
               from cat_pritb031"
        self.professionals = {(sector, dni): categ for (sector, dni, categ)
                              in u.getAll(sql, imp)}

    def get_moduls(self):
        """."""
        u.printTime("moduls")
        sql = "select modu_centre_codi_centre, modu_centre_classe_centre, \
                      modu_servei_codi_servei, modu_codi_modul, modu_codi_uab \
               from cat_vistb027"
        self.moduls = {row[:4]: row[4] for row in u.getAll(sql, imp)}

    def get_responsable(self):
        """."""
        u.printTime("responsable")
        sql = "select lloc_codi_up, lloc_codi_lloc_de_treball, lloc_numcol \
               from cat_pritb025"
        llocs = {(up, lloc): col for (up, lloc, col)
                 in u.getAll(sql, imp)}
        sql = "select {}, '{}', {}, {} from {}"
        self.responsable = c.defaultdict(set)
        for param in (("uab_codi_up", "M", "uab_codi_uab",
                       "uab_lloc_de_tr_codi_lloc_de_tr", "cat_vistb039"),
                      ("uni_codi_up", "I", "uni_codi_unitat",
                       "uni_ambit_treball", "cat_vistb059")):
            this = sql.format(*param)
            for up, tipus, uba, lloc in u.getAll(this, imp):
                if uba and lloc and (up, lloc) in llocs:
                    self.responsable[(up, tipus, uba)].add(llocs[(up, lloc)])

    def get_delegats(self):
        """."""
        u.printTime("delegats")
        sql = "select codi_sector, delm_numcol, delm_numcol_del \
               from cat_pritb777"
        self.delegats = c.defaultdict(list)
        for sector, delegador, delegat in u.getAll(sql, imp):
            self.delegats[(sector, delegador)].append(delegat)

    def get_gestors(self):
        """."""
        u.printTime("gestors")
        sql = "select a.codi_sector, ide_numcol \
              from cat_pritb799 a inner join cat_pritb992 b \
              on a.rol_usu = b.ide_usuari and a.codi_sector = b.codi_sector \
              where rol_rol = 'ECAP_GEST' and ide_numcol <> ''"
        self.gestors = set([row for row in u.getAll(sql, imp)])

    def get_tipus(self):
        """."""
        u.printTime("tipus")
        sql = "select codi_sector, tv_tipus, tv_cita, tv_homol, tv_model_nou \
               from cat_vistb206"
        self.tipus = {(sector, tipus, cita): tipus if model == "S" else homol
                      for (sector, tipus, cita, homol, model)
                      in u.getAll(sql, imp)}

    def get_serveis(self):
        """."""
        u.printTime("serveis")
        sql = "select codi_sector, s_codi_servei, s_descripcio, \
                      s_espe_codi_especialitat, s_codi_servei_hom \
               from cat_pritb103"
        self.serveis = {row[:2]: row[3:] for row in u.getAll(sql, imp)}

    def get_centres(self):
        """."""
        u.printTime("centres")
        sql = "select scs_codi from cat_centres"
        self.centres = set([up for up, in u.getAll(sql, db)])

    def get_visites(self):
        """."""
        sql = "select id_cip, \
                      codi_sector, \
                      visi_data_visita, \
                      visi_up, \
                      visi_dni_prov_resp, \
                      visi_col_prov_resp, \
                      visi_centre_codi_centre, \
                      visi_centre_classe_centre, \
                      visi_servei_codi_servei, \
                      visi_modul_codi_modul, \
                      visi_rel_proveidor, \
                      visi_tipus_visita, \
                      visi_tipus_citacio, \
                      visi_etiqueta \
               from visites \
               where \
                        (extract(year_month from visi_data_visita)>='202101' and extract(year_month from visi_data_visita)<= '202112' ) and\
                     visi_situacio_visita = 'R'"
                                 
        upload = []
        for (id_cip, sector, dat, up, dni, col, codi, classe, servei,
             modul, rel, tipus, citacio, etiqueta) in u.getAll(sql, imp):
            id = self.hash_to_id[id_cip] 
            if id in self.pob:
                up_pob, uba, upinf, ubainf = self.pob[id]['up'], self.pob[id]['uba'], self.pob[id]['upinf'], self.pob[id]['ubainf']
                categ = self.professionals.get((sector, dni))
                responsables = self.responsable[(up, "M", uba)] | self.responsable[(up, "I", ubainf)]
                delegats = []
                for resp in responsables:
                    delegats.extend(self.delegats.get((sector, resp), []))
                ubamod = self.moduls.get((codi, classe, servei, modul))
                lab = 1 * u.isWorkingDay(dat)
                v_homol = self.tipus.get((sector, tipus, citacio))
                espe, s_homol = self.serveis.get((sector, servei), [None, None])
                this = (id, up_pob, uba, ubainf,
                        dni, col, categ,
                        9 if not col else (1 * (col in responsables)),
                        9 if not col else (1 * (col in delegats)),
                        9 if not col else (1 * ((sector, col) in self.gestors)),
                        servei, espe, s_homol,
                        1 * (servei in SERVEIS_GC),
                        modul, up, 1 * (up in self.centres), ubamod, rel,
                        dat, lab, tipus, citacio, v_homol, etiqueta)
                upload.append(this)
        u.listToTable(upload, tb, db)


if __name__ == "__main__":
    Longitudinalitat()
