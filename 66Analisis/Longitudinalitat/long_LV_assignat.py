# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

LONG = exemple(c, 'LONG0002;A2211;AMBITOS###;AGRESULT;NOCAT;TIPPROF###;DIM6SET')

c.close()

print('export')

file = u.tempFolder + "LONG_2022_10.txt"
with open(file, 'w') as f:
   f.write(LONG)
