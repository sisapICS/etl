# coding: utf8

import collections as c
import datetime as d

import sisapUtils as u



db = "permanent"


class Long_temps(object):
    """."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_id_cip_sec()
        self.get_hash()
        self.get_long()
        self.export_files()
        
    def get_id_cip_sec(self):
        """hash to id_cip sec"""
        u.printTime("id_cip")
        self.hash_to_id = {}
        
        sql = """select 
                    id_cip_sec, codi_sector,  hash_d 
                from 
                    long_u11"""
        for id, sec, hash in u.getAll(sql, 'permanent'):
            self.hash_to_id[(hash, sec)] = id
            
    def get_hash(self):
        """id cip sec to hash de permanent"""
        u.printTime("hash")
        self.id_to_hash = {}
        
        sql = """select 
                    id_cip_sec, codi_sector,  hash_d 
                from 
                    u11"""
        for id, sec, hash in u.getAll(sql, 'import'):
            self.id_to_hash[(id)] = {'hash': hash, 'sec': sec}
    
    def get_long(self):
        """Agafem dades de long"""
        u.printTime("long")
        self.upload = []
        self.dades_morts = {}
        sql = """select id_cip_sec, up, uba, upinf, ubainf, servei, nprof, vprof, totals, n1, d1, n3, r1, r2, r3, r4 from mst_long_cont_pacient"""
        for id,  up, uba, upinf, ubainf, servei, nprof, vprof, totals, n1, d1, n3, r1, r2,r3, r4 in u.getAll(sql, 'permanent'):
            hash = self.id_to_hash[(id)]['hash']
            sector = self.id_to_hash[(id)]['sec']
            if (hash, sector) in self.hash_to_id:
                id_cip_sec = self.hash_to_id[(hash, sector)]
                self.upload.append([id_cip_sec, up, uba, upinf, ubainf, servei, nprof, vprof, totals, n1, d1, n3, r1, r2,r3, r4])
            
   
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        
        tb = "long_temps_cont"
        columns = ["id_cip_sec int",  "up varchar(5)", "uba varchar(5)",  "upinf varchar(5)", "ubainf varchar(5)", "servei varchar(10)", "nprof int", "vprof int", "totals int", "n1 double", "d1 double", "n3 double", "r1 double", "r2 double", "r3 double", "r4 double"]

        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)       
    
if __name__ == '__main__':
    u.printTime("Inici")
     
    Long_temps()

    u.printTime("Fi")