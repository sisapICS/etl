---
title: "Longitudinalitat - Canvis Agenda Urgències"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
  word_document:
    toc: yes
    toc_depth: '6'
params:
  skim: TRUE
---

# RESUM

***
***
***

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

```{r parametres de conexió}
source("C:/Users/ehermosilla/Documents/Keys.R")
```

```{r parametres markdown 1}
inici <- Sys.time()

nfetch <- -1

warning.var <- FALSE
message.var <- FALSE
eval.var <- FALSE # Flag per executar chunks que tarden molt i són descriptius: missings, etc ..

eval.fig.var <- TRUE
```

```{r parametres markdown 2}
eval.library.var <- TRUE

eval.import.var <- TRUE
eval.datamanager.var <- TRUE

eval.flowchart.var <- TRUE

eval.univariada.var <- TRUE
eval.bivariada.var <- FALSE

eval.analisi.var <- TRUE

eval.annex.var <- FALSE

eval.timeexecution.var <- TRUE

var.fig.height <- 4
var.fig.width <- 6
```

```{r library, child="02_library.Rmd", eval=eval.library.var}

```

```{r import, child="04_import.Rmd", eval=eval.import.var}

```

```{r datamanager, child="05_datamanager.Rmd", eval=eval.datamanager.var}

```

```{r flowchart, child="06_flowchart.Rmd", eval=eval.flowchart.var}

```

```{r univariada, child="07_descriptiva_univariada.Rmd", eval=eval.univariada.var}

```

```{r bivariada, child="08_descriptiva_bivariada.Rmd", eval=eval.bivariada.var}

```

```{r analisi, child="09_analisi.Rmd", eval=eval.analisi.var}

```

```{r time execution, eval=eval.timeexecution.var}

final <- Sys.time()
print(round(final - inici,2))
```
