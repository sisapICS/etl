```{r}
phy_up_uba <- mst_long_phy[, .(
  `N pacients PHY` = sum(den),
  phy = sum(num)/sum(den)), by = .(up, uba)]
cont_up_uba <- mst_long_cont_pacient[servei == "MG", .(
  `N pacients CONT` = .N,
  r1 = mean(r1),
  r2 = mean(r2),
  r3 = mean(r3),
  r4 = mean(r4)), .(up, uba)]
cont_a_up_uba <- mst_long_cont_pacient_a[servei == "MG", .(
  `N pacients CONT A` = .N,
  r1_a = mean(r1),
  r2_a = mean(r2),
  r3_a = mean(r3),
  r4_a = mean(r4)), .(up, uba)]
```

```{r}
dades_up_uba <- merge(phy_up_uba, cont_up_uba, by = c("up", "uba"), all.x = T, all.y = T)
dades_up_uba <- merge(dades_up_uba, cont_a_up_uba, by = c("up", "uba"), all.x = T, all.y = T)
```

```{r}
dades_up_uba <- merge(dades_up_uba, cat_centres[amb_codi != "00", .(ics_codi, scs_codi, amb_codi, amb_desc, medea)], by.x = "up", by.y = "scs_codi", all.y = T)
```

```{r}
dades_up_uba[, medea_5c := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), labels = c("Rural", "Rural", "Rural", "1U", "2U", "3U", "4U"))]
```

```{r}
dades_up_uba_long <- melt(dades_up_uba, id.vars = c("up", "uba", "medea", "medea_5c", "amb_desc", "phy", "r1_a", "r2_a", "r3_a", "r4_a"), measure.vars = c("r1", "r2", "r3", "r4"), variable.name = "cont_var", value.name = "cont_value")
dades_up_uba_long <- melt(dades_up_uba_long, id.vars = c("up", "uba", "medea", "medea_5c", "amb_desc", "phy", "cont_var", "cont_value"), measure.vars = c("r1_a", "r2_a", "r3_a", "r4_a"), variable.name = "cont_a_var", value.name = "cont_a_value")
```


```{r}
phy_up <- mst_long_phy[, .(
  `N pacients PHY` = sum(den),
  phy = sum(num)/sum(den)), by = .(up)]
cont_up <- mst_long_cont_pacient[servei == "MG", .(
  `N pacients CONT` = .N,
  r1 = mean(r1),
  r2 = mean(r2),
  r3 = mean(r3),
  r4 = mean(r4)), .(up)]
cont_a_up <- mst_long_cont_pacient_a[servei == "MG", .(
  `N pacients CONT A` = .N,
  r1_a = mean(r1),
  r2_a = mean(r2),
  r3_a = mean(r3),
  r4_a = mean(r4)), .(up)]
```

```{r}
dades_up <- merge(phy_up, cont_up, by = c("up"), all.x = T, all.y = T)
dades_up <- merge(dades_up, cont_a_up, by = c("up"), all.x = T, all.y = T)
```

```{r}
dades_up <- merge(dades_up, cat_centres[amb_codi != "00", .(ics_codi, scs_codi, amb_codi, amb_desc, medea)], by.x = "up", by.y = "scs_codi", all.y = T)
```

```{r}
dades_up[, medea_5c := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), labels = c("Rural", "Rural", "Rural", "1U", "2U", "3U", "4U"))]
```

```{r}
dades_up_long <- melt(dades_up, id.vars = c("up", "medea", "medea_5c", "amb_desc", "phy", "r1_a", "r2_a", "r3_a", "r4_a"), measure.vars = c("r1", "r2", "r3", "r4"), variable.name = "cont_var", value.name = "cont_value")
dades_up_long <- melt(dades_up_long, id.vars = c("up", "medea", "medea_5c", "amb_desc", "phy", "cont_var", "cont_value"), measure.vars = c("r1_a", "r2_a", "r3_a", "r4_a"), variable.name = "cont_a_var", value.name = "cont_a_value")
```
