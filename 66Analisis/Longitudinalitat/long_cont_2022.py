# -*- coding: utf8 -*-

"""
Repliquem indicadors individuals pel 2022 amb dades del master de visiets
"""

import collections as c

import sisapUtils as u

u.printTime("inici")


db = "exadata2"

class Continuitat(object):
    """."""

    def __init__(self):
        """."""

        self.get_centres()
        self.get_nia()
        self.get_pob()
        self.get_visites()
        self.get_taula()
        self.process_sequencia()
        self.get_calculs()

    def get_centres(self):
        """."""
        self.centres = {}
        sql = "select scs_codi, ics_codi from cat_centres"
        for up, br in u.getAll(sql, 'nodrizas'):
            self.centres[up] = True
            
        sql = "select distinct c_up from dwsisap.dbs_2022"
        for up, in u.getAll(sql, 'exadata'):
            self.centres[up] = True

    def get_nia(self):
        """hash to id_cip sec"""
        u.printTime("nia")
        self.hash_to_nia = {}
        
        sql = """SELECT 
                    hash, nia 
                FROM 
                    dwsisap.RCA_CIP_NIA"""
        for hash, nia in u.getAll(sql, 'exadata'):
            self.hash_to_nia[(hash)] = nia
    
    def get_pob(self):
        """."""
        u.printTime("Assignada")
        self.pob = {}
        sql = """select NIA, C_up, c_metge
                from DWSISAP.dbs_2022 where c_edat_anys > 14"""
        for nia, up, uba in u.getAll(sql, 'exadata'):
            self.pob[nia]  = {'up':up, 'uba':uba}

    def get_visites(self):
        """Agafem visites dels dos últims anys"""
        u.printTime("Visites")
        self.visitespacients = c.Counter()
        self.data = c.defaultdict(lambda: c.defaultdict(list))
        sql = """SELECT 
                    pacient, up,   substr(professional,0,8), DATA FROM  dwsisap.SISAP_MASTER_VISITES 
                WHERE 
                    SITUACIO = 'R' AND PROFESSIONAL IS NOT NULL AND sisap_catprof_codi IN ('10999', '10117', '10116')
                AND 
                    DATA BETWEEN DATE '2021-01-01' AND DATE '2022-12-31'"""
        for hash, up, professional, dat in u.getAll(sql, "exadata"):
            if up in self.centres:
                nia = self.hash_to_nia[(hash)] if hash in self.hash_to_nia else None
                self.visitespacients[(nia, professional)] += 1
                identify = str(nia) 
                self.data[identify][dat].append(professional)
  
    def get_taula(self):
        """Calculem les tres dades necessàries per calcular els indicadors. A nivell de pacient necessitem:
            1) Nombre de professionals diferents
            2) Nombre de visites totals
            3) Nombre visites professional majoritari
            4) El sumatori dels quadrats de les visites de cada professional (per a calcular COC)
        """
        u.printTime("Càlculs")
        self.pacients = {}
        for (id, dummy_col), rec in self.visitespacients.items():
            Rq2 = rec * rec
            if (id) in self.pacients:
                self.pacients[(id)]['totals'] += rec
                self.pacients[(id)]['nprof'] += 1
                self.pacients[(id)]['rq2'] += Rq2
                majoritari = self.pacients[(id)]['vprof']
                if rec > majoritari:
                    self.pacients[(id)]['vprof'] = rec
            else:
                self.pacients[(id)] = {'totals': rec, 'nprof': 1, 'vprof': rec, 'rq2': Rq2}
        
    def process_sequencia(self):
        """Ordenem les visites per ordre temporal per poder calcular el de sequencia"""
        u.printTime("Seqüència")
        resultat = []
        for (id), dates in self.data.items():
            pac = ((id, dat, cols) for dat, cols in dates.items())
            colegiat = {}
            for i, (id, dat, cols) in enumerate(sorted(pac, key=lambda x: x[1]), start=1):
                for col in cols:
                    if col == colegiat:
                        seq = 1
                    else:
                        seq = 0
                        colegiat = col
                    identities = id
                    resultat.append((id, dat, col, i, seq))
        Stb = "Long_sequencia"
        Scolumns = ["id_cip_sec int",  "data date", "colegiat varchar(20)", "ordre int", "sequencia int"]
        u.createTable(Stb, "({})".format(", ".join(Scolumns)), 'permanent', rm=True)
        u.listToTable(resultat, Stb, 'permanent')

        self.indicador_sequencia = c.Counter()
        sql = "select id_cip_sec,  sequencia from {}".format(Stb)
        for id, seq in u.getAll(sql, 'permanent'):
            self.indicador_sequencia[id,  'num'] += seq
            self.indicador_sequencia[id,  'den'] += 1
        u.execute("drop table {}".format(Stb),'permanent')
    
    def get_calculs(self):
        """Calculem els indicadors a nivell de pacient
        Aqui filtrem per població assignada i pacients entre 3 i 300 visites"""
        u.printTime("Més càlculs")
        upload = []
        for (id), dad in self.pacients.items():
            totals, nprof, vprof, rq2 = dad['totals'], dad['nprof'], dad['vprof'], dad['rq2']
            if 3 <= totals <= 300:
                n1 = 1 - (nprof/(totals + 0.1))
                d1 = 1 - (1/(totals + 0.1))
                n3 = self.indicador_sequencia[id, 'num']
                d3 = self.indicador_sequencia[id, 'den']
                r1 = float(n1/d1)
                r2 = float(vprof) / float(totals)
                d3 = d3 - 1
                r3 = float(n3)/float(d3)
                n4 = rq2 - totals
                d4 = totals * (totals - 1)
                r4 = float(n4) / float(d4)
                if id in self.pob:
                    up, uba= self.pob[id]['up'], self.pob[id]['uba']
                    upload.append([id, up, uba,   nprof, totals, r1, r2, r3, r4])
        
        u.listToTable(upload, tb, db)

    

if __name__ == '__main__' :
        tb = "sisap_longitudinalitat_2022"
        columns = ["nia int", "up varchar2(5)", "uba varchar2(5)",  "professionals int",  "visites int",  "CONT1 number(6, 4)", "CONT2 number(6, 4)", "CONT3 number(6, 4)", "CONT4 number(6, 4)"]

        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        
        users= ['DWECOMA','DWAQUAS']
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)
            
        Continuitat()

        u.printTime("Fi")