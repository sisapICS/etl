# -*- coding: utf8 -*-

"""
Herfindahl index. Sumatori del % de visites de cada professional al quadrat Càlcul anual
"""

import collections as c

import sisapUtils as u


db = "permanent"
tbup = "long_herfindahl"

u.printTime("inici")

periodes_calcul = {'202112': '202101'}


class Continuitat(object):
    """."""

    def __init__(self):
        """."""

        self.get_centres()
        self.get_id_cip_sec()
        self.get_usuaris()
        self.get_pob()
        for self.period in periodes_calcul:
            print self.period
            self.p_fi = periodes_calcul[self.period]
            self.get_visites()
            self.get_taula()
            self.get_calculs()

    def get_centres(self):
        """."""
        sql = "select scs_codi from cat_centres"
        self.centres = {up: True for up, in u.getAll(sql, 'nodrizas')}

    def get_id_cip_sec(self):
        """hash to id_cip sec"""
        u.printTime("id_cip")
        self.hash_to_id = {}
        
        sql = """select 
                    id_cip, codi_sector,  hash_d 
                from 
                    u11"""
        for id, sec, hash in u.getAll(sql, 'import'):
            self.hash_to_id[(id)] = hash
    
    def get_usuaris(self):
        """."""
        self.usuaris = {}
        sql = "select left(ide_dni, 8), ide_categ_prof_c \
                from cat_pritb992 where ide_categ_prof_c in ('10999','10117')"
        for usu, categ in u.getAll(sql, "import"):
            self.usuaris[(usu)] = categ

    def get_pob(self):
        """."""
        u.printTime("Assignada")
        SIDICS_DB = ("dbs", "x0002")
        self.pob = {}
        sql = """select c_cip, C_SECTOR, C_up, c_metge, c_up, c_infermera
                from dbs.dbs_2021 where c_edat_anys > 14"""
        for hash, sector, up, uba, upinf, ubainf in u.getAll(sql, SIDICS_DB):
            self.pob[hash]  = {'up':up, 'uba':uba, 'upinf': upinf, 'ubainf': ubainf}

    def get_visites(self):
        """Agafem visites dels dos últims anys"""
        u.printTime("Visites")
        self.visitespacients = c.Counter()
        self.data = c.defaultdict(lambda: c.defaultdict(list))
        sql = "select id_cip, codi_sector, visi_up, left(visi_dni_prov_resp, 8), visi_data_visita, s_espe_codi_especialitat, visi_servei_codi_servei \
                    from visites \
                    where visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_dni_prov_resp<>'' and visi_col_prov_resp like ('1%') \
                     and extract(year_month from visi_data_visita)>='{0}' and extract(year_month from visi_data_visita)<= '{1}' ".format(self.p_fi, self.period)
        for id, _sector, up, col, dat, _categoria, _serveis in u.getAll(sql, "import"):
            if up in self.centres:
                if (col) in self.usuaris:
                    self.visitespacients[(id, col)] += 1
                    identify = str(id) 
                    self.data[identify][dat].append(col)
  
    def get_taula(self):
        """Calculem les tres dades necessàries per calcular indicador. A nivell de pacient necessitem:
        """
        u.printTime("Càlculs")
        self.pacients = {}
        for (id,  dummy_col), rec in self.visitespacients.items():
            if (id) in self.pacients:
                self.pacients[(id)]['totals'] += rec
                self.pacients[(id)]['nprof'] += 1
            else:
                self.pacients[(id)] = {'totals': rec, 'nprof': 1, 'rq2': 0}
        
        for (id,  dummy_col), rec in self.visitespacients.items():        
            vis_totals = self.pacients[(id)]['totals']
            perc_ = float(rec)/float(vis_totals)
            Rq2 = float(perc_) * float(perc_)
            self.pacients[(id)]['rq2'] += Rq2
        
    def get_calculs(self):
        """Calculem els indicadors a nivell de pacient
        Aqui filtrem per població assignada i pacients entre 3 i 300 visites"""
        u.printTime("Més càlculs")
        upload = []
        for (id), dad in self.pacients.items():
            totals, nprof, rq2 = dad['totals'], dad['nprof'], dad['rq2']
            hash = self.hash_to_id[id]
            if hash in self.pob:
                up, uba = self.pob[hash]['up'], self.pob[hash]['uba']
                upload.append([hash, self.period, up, uba,  totals, nprof, rq2])

        u.listToTable(upload, tb, db)

    

if __name__ == '__main__' :
        tb = "long_temps_herfindahl"
        columns = ["id_cip_sec varchar(40)", "periode varchar(10)", "up varchar(5)", "uba varchar(5)",  "totals int", "nprof int", "rq2 double"]

        sql = "select scs_codi, ics_codi from cat_centres"
        centres = {up: br for up, br in u.getAll(sql, 'nodrizas')}

        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        Continuitat()

        u.printTime("Fi")