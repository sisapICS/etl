# coding: utf8

import collections as c
import datetime as d

import sisapUtils as u



db = "permanent"


class Long_temps(object):
    """."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_id_cip_sec()
        self.get_hash()
        self.get_deaths()
        self.get_rips()
        self.get_long_temps()
        self.export_files()
        
    def get_id_cip_sec(self):
        """hash to id_cip sec"""
        u.printTime("id_cip")
        self.hash_to_id = {}
        
        sql = """select 
                    id_cip_sec, codi_sector,  hash_d 
                from 
                    long_u11"""
        for id, sec, hash in u.getAll(sql, 'permanent'):
            self.hash_to_id[(hash, sec)] = id
            
    def get_hash(self):
        """id cip sec to hash de permanent"""
        u.printTime("hash")
        self.id_to_hash = {}
        
        sql = """select 
                    id_cip_sec, codi_sector,  hash_d 
                from 
                    u11"""
        for id, sec, hash in u.getAll(sql, 'import'):
            self.id_to_hash[(id)] = {'hash': hash, 'sec': sec}
    
    def get_deaths(self):
        """Agafem dades dels morts"""
        u.printTime("assignada")
        
        self.dades_morts = {}
        sql = """select id_cip_sec, usua_data_naixement, usua_situacio, usua_data_situacio from assignada where usua_situacio='D'"""
        for id, naix, sit, dsit in u.getAll(sql, 'import'):
            hash = self.id_to_hash[(id)]['hash']
            sector = self.id_to_hash[(id)]['sec']
            if (hash, sector) in self.hash_to_id:
                id_cip_sec = self.hash_to_id[(hash, sector)]
                self.dades_morts[(id_cip_sec)] = {'naix': naix, 'sit': sit, 'dsit': dsit}
            
    def get_rips(self):
        """Agafem dades de long_te,ps_rip per ajuntar"""
        u.printTime("long_temps_rip")
        self.upload  = []
        sql = """select id_cip_sec, up, uba, edat, sexe, numcolM, primera_visitaM, ultima_visitaM from long_temps_rip"""
        for id, up, uba, edat, sexe, numcolM, primera, ultima in u.getAll(sql, 'permanent'):
            naix = self.dades_morts[id]['naix'] if id in self.dades_morts else None
            sit = self.dades_morts[id]['sit'] if id in self.dades_morts else None
            dsit = self.dades_morts[id]['dsit'] if id in self.dades_morts else None
            self.upload.append([id, up, uba, 99, naix, sexe, sit, dsit, numcolM, primera, ultima])
    
    def get_long_temps(self):
        """Agafem les dades de longtemps"""
        sql = """select id_cip_sec, up, uba, ates, data_naix, sexe, numcolM, primera_visitaM, ultima_visitaM from long_temps"""
        for id, up, uba, ates, naix, sexe, numcol, primera, ultima in u.getAll(sql, 'permanent'):
            self.upload.append([id, up, uba, ates , naix, sexe, 'A', None,  numcol, primera, ultima])
    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        
        tb = "long_temps_junt"
        cols = ("id_cip_sec int", "up varchar(5)", "uba varchar(5)", "ates int", "naix date","sexe varchar(10)", "situacio varchar(10)",  "data_defuncio date", "numcolM varchar(10)",  "primera_visitaM date",  "ultima_visitaM date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)       
    
if __name__ == '__main__':
    u.printTime("Inici")
     
    Long_temps()

    u.printTime("Fi")