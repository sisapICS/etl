# -*- coding: utf8 -*-

"""
Guardem les taules a nivell de pacient dels resultats dels indicadors CONT i CONTA

"""

import collections as c

import sisapUtils as u

db = "longitudinalitat"


u.printTime("inici")



class Continuitat(object):
    """."""

    def __init__(self):
        """."""

        self.get_hash()
        self.get_taules()
        self.export()
        
    def get_hash(self):
        """hash to id_cip_sec"""
        u.printTime("id_cip_sec")
        self.idsec_to_hash = {}
        
        sql = """select 
                    id_cip, id_cip_sec, hash_d, codi_sector 
                from 
                    u11"""
        for id, id_sec, hash, sec in u.getAll(sql, 'import'):
            self.idsec_to_hash[id_sec] = hash
    
    def get_taules(self):
        """."""
        u.printTime("Taules origen")
        self.long, self.longA = [],[]
        
        sql = "select * from mst_long_cont_pacient"
        for id, up, uba, upinf, ubainf, servei, nprof, vprof, totals, n1, d1,n3,r1,r2,r3,r4 in u.getAll(sql, 'altres'):
            hash = self.idsec_to_hash[id] if id in self.idsec_to_hash else None
            self.long.append([hash, up, uba, upinf, ubainf, servei, nprof, vprof, totals, n1, d1,n3,r1,r2,r3,r4])
            
        sql = "select * from mst_long_cont_pacient_a"
        for id, up, uba, upinf, ubainf, servei, nprof, vprof, totals, n1, d1,n3,r1,r2,r3,r4 in u.getAll(sql, 'altres'):
            hash = self.idsec_to_hash[id] if id in self.idsec_to_hash else None
            self.longA.append([hash, up, uba, upinf, ubainf, servei, nprof, vprof, totals, n1, d1,n3,r1,r2,r3,r4])
    
    def export(self):
        """."""
        u.printTime("export")

        tb = "mst_long_cont_pacient"
        columns = ["hash varchar(40)", "up varchar(5)", "uba varchar(5)",  "upinf varchar(5)", "ubainf varchar(5)", "servei varchar(10)", "nprof int", "vprof int", "totals int", "n1 double", "d1 double", "n3 double", "r1 double", "r2 double", "r3 double", "r4 double"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.long, tb, db)
        
        tb = "mst_long_cont_pacient_a"
        columns = ["hash varchar(40)", "up varchar(5)", "uba varchar(5)",  "upinf varchar(5)", "ubainf varchar(5)", "servei varchar(10)", "nprof int", "vprof int", "totals int", "n1 double", "d1 double", "n3 double", "r1 double", "r2 double", "r3 double", "r4 double"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.longA, tb, db)
        
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    Continuitat()

    u.printTime("Fi")