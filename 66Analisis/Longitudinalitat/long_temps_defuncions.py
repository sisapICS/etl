# -*- coding: utf8 -*-

"""
Agafem defuncions
"""

import collections as c

import sisapUtils as u

u.printTime("inici")

db = "permanent"

class defuncions(object):
    """."""

    def __init__(self):
        """."""

        self.get_id_cip_sec()
        self.get_defuncions()
        self.get_pob()
        self.export_files()

    def get_id_cip_sec(self):
        """hash to id_cip sec"""
        u.printTime("id_cip")
        self.hash_to_id = {}
        
        sql = """select 
                    id_cip, codi_sector,  hash_d 
                from 
                    u11"""
        for id, sec, hash in u.getAll(sql, 'import'):
            self.hash_to_id[(id)] = hash
    
    def get_defuncions(self):
        """Defuncions"""
        u.printTime("Defuncions")
        self.defuncions = {}
        sql = "select id_cip, usua_data_situacio from import.assignada  where usua_situacio='D'"
        for id, dat in u.getAll(sql, "import"):
            hash = self.hash_to_id[(id)] if id in self.hash_to_id else None
            self.defuncions[(hash)] = dat
            
    def get_pob(self):
        """."""
        u.printTime("Assignada")
        SIDICS_DB = ("dbs", "x0002")
        self.upload = []
        sql = """select c_cip, C_SECTOR from  dbs.dbs_2021"""
        for hash, sector in u.getAll(sql, SIDICS_DB):
            if hash in self.defuncions:
                data = self.defuncions[(hash)]
                self.upload.append([hash, data])
    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        
        tb = "long_temps_defuncions"
        cols = ("hash varchar(40)", "data_defuncio date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)  
    

if __name__ == '__main__' :
        
        defuncions()

        u.printTime("Fi")