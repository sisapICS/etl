# -*- coding: utf8 -*-

"""
Longiudinalitat com a temps
"""

import collections as c

import sisapUtils as u

DEBUG = False

db='permanent'

class Continuitat(object):
    """."""

    def __init__(self):
        """."""

        self.get_responsable()
        self.get_pob()
        self.get_visites()
        self.get_taula()
        self.export_files()

    def get_responsable(self):
        """."""
        u.printTime("Responsable")
        sql = "select lloc_codi_up, lloc_codi_lloc_de_treball, lloc_numcol \
               from cat_pritb025 where lloc_data_baixa = '4712-01-01'"
        llocs = {(up, lloc): col for (up, lloc, col)
                 in u.getAll(sql, 'import')}
        sql = "select {}, '{}', {}, {} from {} where {}"
        self.responsable = {}
        for param in (("uab_codi_up", "M", "uab_codi_uab",
                       "uab_lloc_de_tr_codi_lloc_de_tr", "cat_vistb039", "uab_data_baixa= '4712-01-01'"),
                      ("uni_codi_up", "I", "uni_codi_unitat",
                       "uni_ambit_treball", "cat_vistb059", "uni_data_baixa= 0")):
            this = sql.format(*param)
            for up, tipus, uba, lloc in u.getAll(this, 'import'):
                if uba and lloc and (up, lloc) in llocs:
                    col = llocs[(up, lloc)]
                    self.responsable[(up, tipus, uba)]= col

    def get_pob(self):
        """."""
        u.printTime("Assignada")
        sql = "select id_cip_sec, up, uba, upinf, ubainf from assignada_tot where ates=1 and codi_sector='6837'"
        self.pob = {id: (up, uba, upinf, ubainf) for (id, up, uba, upinf, ubainf) in u.getAll(sql, 'nodrizas')}

    def get_visites(self):
        """Agafem visites de tota la vida"""
        u.printTime("Visites")
        self.visitespacients = {}
        sql = "select id_cip_sec, codi_sector, visi_col_prov_resp, visi_data_visita \
                from {} \
                where visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_col_prov_resp<>''"
                
        jobs = ([sql.format(table), "import"] for table in u.getSubTables("visites"))  # noqa
        resultat = u.multiprocess(_get_data, jobs, 4)
        self._iterate_workers(resultat, self.visitespacients)
        
    def _iterate_workers(self, resultat, objecte):
        """."""
        for worker in resultat:
            for id, sector, col, data in worker:
                if id in self.pob:
                    if (id, col) in objecte:
                        d1 = objecte[(id,col)]
                        if data < d1:
                            objecte[(id,col)]= data
                    else:
                        objecte[(id, col)]=data

    def get_taula(self):
        """Posem a cada pacient segons professional assignat a la seva up i uba la primera visita"""
        u.printTime("Càlculs")
        self.upload = []
        for id, n in self.pob.items():
            up, uba, upinf, ubainf = self.pob[id][0],self.pob[id][1],self.pob[id][2],self.pob[id][3]
            colM = self.responsable[(up, 'M', uba)] if (up, 'M', uba) in self.responsable else None
            colI = self.responsable[(upinf, 'I', ubainf)] if (upinf, 'I', ubainf) in self.responsable else None
            primera_visitaM = self.visitespacients[(id, colM)] if (id, colM) in self.visitespacients else None
            primera_visitaI = self.visitespacients[(id, colI)] if (id, colI) in self.visitespacients else None
            self.upload.append([id, up, uba, colM, primera_visitaM, upinf, ubainf, colI, primera_visitaI])
    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        
        tb = "long_temps"
        cols = ("id_cip_sec int", "up varchar(5)", "uba varchar(5)", "numcolM varchar(10)",  "primera_visitaM date", "upinf varchar(5)", "ubainf varchar(5)", "numcolI varchar(10)",  "primera_visitaI date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)       

def _get_data(param):
    """."""
    if DEBUG:
        param[0] += " limit 1000"
    return [row for row in u.getAll(*param)]

if __name__ == '__main__':
    u.printTime("Inici")
     
    Continuitat()

    u.printTime("Fi")