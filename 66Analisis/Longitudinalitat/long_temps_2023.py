# -*- coding: utf8 -*-

"""
Com que calcular el temps des de la primera visita es molt costos en termes de calcul, fem a partir de dates assignació de metge i de pacient
"""

import collections as c
import sisapUtils as u
import sisaptools as t

DEBUG = False

db='longitudinalitat'

class Continuitat(object):
    """."""

    def __init__(self):
        """."""
        self.get_hash()
        self.get_responsable()
        self.get_assignada()
        self.get_pob()
        self.export_files()
        
    def get_hash(self):
        """hash to id_cip_sec"""
        u.printTime("id_cip_sec")
        self.idsec_to_hash = {}
        
        sql = """select 
                    id_cip, id_cip_sec, hash_d, codi_sector 
                from 
                    u11"""
        for id, id_sec, hash, sec in u.getAll(sql, 'import'):
            self.idsec_to_hash[id_sec] = hash
            

    def get_responsable(self):
        """."""
        u.printTime("Responsable")
        llocs = {}
        sql = "select lloc_codi_up, lloc_codi_lloc_de_treball, lloc_numcol, lloc_data_inici \
               from cat_pritb025 where lloc_data_baixa = '4712-01-01'"
        for up, lloc, col, dat in u.getAll(sql, 'import'):
            ncol = col[0:8]
            llocs[(up, lloc)] = {'ncol': ncol, 'dat': dat}
        sql = "select {}, '{}', {}, {} from {} where {}"
        self.responsable = {}
        for param in (("uab_codi_up", "M", "uab_codi_uab",
                       "uab_lloc_de_tr_codi_lloc_de_tr", "cat_vistb039", "uab_data_baixa= '4712-01-01'"),
                      ("uni_codi_up", "I", "uni_codi_unitat",
                       "uni_ambit_treball", "cat_vistb059", "uni_data_baixa= 0")):
            this = sql.format(*param)
            for up, tipus, uba, lloc in u.getAll(this, 'import'):
                if uba and lloc and (up, lloc) in llocs:
                    col = llocs[(up, lloc)]['ncol']
                    dat = llocs[(up, lloc)]['dat']
                    self.responsable[(up, tipus, uba)]= {'ncol': col, 'dat': dat}
        
    def get_assignada(self):
        """."""
        u.printTime("usutb040")
        self.usu40 = {}
        
        sql = """select id_cip_sec, if(usua_data_assignacio=0,usua_data_alta,usua_data_assignacio) from assignada"""
        for id, data_a in u.getAll(sql, 'import'):
            hash = self.idsec_to_hash[id]
            self.usu40[hash] = data_a
            
    def get_pob(self):
        """."""
        u.printTime("Assignada")
        
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        
        self.pob = []
        sql = """select c_cip, c_up, c_metge, c_up, C_INFERMERA from dbs.dbs_2023"""
        for hash, up, uba, upinf, ubainf in u.getAll(sql, ("sidics", "x0002")):
            datM = self.responsable[(up, 'M', uba)]['dat'] if (up, 'M', uba) in self.responsable else None
            colM = self.responsable[(up, 'M', uba)]['ncol'] if (up, 'M', uba) in self.responsable else None
            datI = self.responsable[(upinf, 'I', ubainf)]['dat'] if (upinf, 'I', ubainf) in self.responsable else None
            colI = self.responsable[(upinf, 'I', ubainf)]['ncol'] if (upinf, 'I', ubainf) in self.responsable else None
            dat_pac = self.usu40[hash] if hash in self.usu40 else None               
            self.pob.append([hash, up, uba, upinf, ubainf, colM, colI,  datM, datI, dat_pac])
            
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "long_temps_2023"
        cols = ["hash varchar(40)", "up varchar(5)", "uba varchar(5)", "upinf varchar(5)", "ubainf varchar(5)", 
                    "numcolM varchar(10)", "numcolI varchar(10)",  "visita_M date", "visita_I date", "visita_pac date"]
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pob, tb, db)  
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    Continuitat()

    u.printTime("Fi")