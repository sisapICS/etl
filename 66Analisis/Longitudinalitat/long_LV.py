# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

LONG23 = exemple(c, 'CONT0002;AYR23###;AMBITOS###;NUM,DEN;NOCAT;TIPPROF###;DIM6SET')
LONG22 = exemple(c, 'CONT0002;AYR22###;AMBITOS###;NUM,DEN;NOCAT;TIPPROF###;DIM6SET')
LONG21 = exemple(c, 'CONT0002;AYR21###;AMBITOS###;NUM,DEN;NOCAT;TIPPROF###;DIM6SET')
LONG20 = exemple(c, 'CONT0002;AYR20###;AMBITOS###;NUM,DEN;NOCAT;TIPPROF###;DIM6SET')
LONG19 = exemple(c, 'CONT0002;AYR19###;AMBITOS###;NUM,DEN;NOCAT;TIPPROF###;DIM6SET')

c.close()

print('export')

file = u.tempFolder + "LONG_2023.txt"
with open(file, 'w') as f:
   f.write(LONG23)

file = u.tempFolder + "LONG_2022.txt"
with open(file, 'w') as f:
   f.write(LONG22)
   
file = u.tempFolder + "LONG_2021.txt"
with open(file, 'w') as f:
   f.write(LONG20)
   
file = u.tempFolder + "LONG_2020.txt"
with open(file, 'w') as f:
   f.write(LONG21)
   
file = u.tempFolder + "LONG_2019.txt"
with open(file, 'w') as f:
   f.write(LONG19)
