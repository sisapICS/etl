# -*- coding: utf8 -*-

"""
Mirem visites durat 2022 dels pacients dbs del 2021, per veure si fan més o menys visites o més o menys absentisme
"""

import collections as c

import sisapUtils as u

u.printTime("inici")

db = "permanent"

class visites(object):
    """."""

    def __init__(self):
        """."""

        self.get_hash()
        self.get_pob()
        self.get_visites()
        self.export_files()

    def get_hash(self):
        """hash to id_cip sec"""
        u.printTime("id_cip")
        self.covid_to_hash = {}
        
        sql = """select 
                    hash_redics, hash_covid
                from 
                    dwsisap.PDPTB101_RELACIO """
        for id, hash in u.getAll(sql, 'exadata'):
            self.covid_to_hash[(hash)] = id
    
    def get_pob(self):
        """."""
        u.printTime("Assignada")
        SIDICS_DB = ("dbs", "x0002")
        self.pob = {}
        sql = """select c_cip, C_SECTOR from  dbs.dbs_2021 where c_edat_anys > 14"""
        for hash, sector in u.getAll(sql, SIDICS_DB):
            self.pob[hash] = True
    
    def get_visites(self):
        """Visites de master"""
        u.printTime("Visites")
        self.upload = []
        sql = """SELECT pacient, DATA,  citacio, forcada, peticio, tipus_class, TIPUS_CLASS_DESC, ATTRIBUTS_AGENDA_DESC , sisap_servei_codi, situacio
                FROM dwsisap.SISAP_MASTER_VISITES where DATA BETWEEN DATE '2022-01-01' AND DATE '2022-12-31'"""
        for id, data, citacio, forcada, peticio, tipus_class, tipus_class_dessc, atributs, servei, situacio in u.getAll(sql, "exadata"):
            hash = self.covid_to_hash[(id)] if id in self.covid_to_hash else None
            if hash in self.pob:
                self.upload.append([hash, data, citacio, forcada, peticio, tipus_class, tipus_class_dessc, atributs, servei, situacio])

    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        
        tb = "long_temps_visites"
        cols = ("hash varchar(40)", "data_visita date, citacio varchar(10), forcada varchar(10), peticio, date, tipus_class varchar(10), tipus_class_desc varchar(100), atributs varchar(100), servei varchar(10), situacio varchar(10)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)  
    

if __name__ == '__main__' :
        
        visites()

        u.printTime("Fi")