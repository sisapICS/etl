# -*- coding: utf8 -*-

import hashlib as h

"""
Longitudinalitat per planificat
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


class long_plan(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_u11()
        self.longitudinalitat()
        self.get_planificat()
        self.export()
        
    def get_u11(self):
        """."""
        u.printTime("u11")
        
        self.hash_to_id = {}
        sql = "select id_cip_sec, hash_d, codi_sector from u11"
        for id, hash, sector in u.getAll(sql, 'import'):
            self.hash_to_id[(hash, sector)] = id
            
    def longitudinalitat(self):
        """."""
        u.printTime("long")
        
        self.long = {}
        sql = "select id_cip_sec, up, r2 from mst_long_cont_pacient_A where servei='MG'"
        for id, up, r2 in u.getAll(sql, 'altres'):
            self.long[(id)] = {'up': up, 'r2':r2}
            
    
    def get_planificat(self):
        """planificat"""
        u.printTime("planificat")
        
        a = 0
        b = 0
        self.upload = []
        self.hashos = []
        sql = "select c_cip, c_sector, grup from pdp.seguent_visita_cronics"
        for hash, sector, grup in u.getAll(sql, 'pdp'):
            try:
                idcip = self.hash_to_id[(hash, sector)]
            except KeyError:
                a += 1
                idcip = None
            if idcip in self.long:
                up = self.long[(idcip)]['up']
                r2 = self.long[(idcip)]['r2']
                self.upload.append([idcip, grup, up, r2])
            else:
                b +=1
                self.hashos.append([hash, grup])
        print a
        print b
        
    def export(self):
        """."""
        tb = "long_planificat"
        db = "permanent"
        
        cols = ("id_cip_sec int", "grup int", "up_ass varchar(5)", "r2 double")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)

        u.listToTable(self.upload, tb, db)
        
        tb = "long_planificat_hash"
        
        cols = ("hash varchar(40)", "grup int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)

        u.listToTable(self.hashos, tb, db)
        
if __name__ == '__main__':
    u.printTime("Inici")
     
    long_plan()
    
    u.printTime("Final")        
