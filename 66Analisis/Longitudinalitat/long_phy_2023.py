# -*- coding: utf8 -*-

"""
PHY
(Continuity for Physician)

"""

import collections as c

import sisapUtils as u

db = "permanent"
tbup = "long_phy"

u.printTime("inici")



class Continuitat(object):
    """."""

    def __init__(self):
        """."""

        self.get_centres()
        self.get_id_cip_sec()
        self.get_usuaris()
        self.get_pob()
        self.get_visites()
        self.export()

    def get_centres(self):
        """."""
        sql = "select scs_codi from cat_centres"
        self.centres = {up: True for up, in u.getAll(sql, 'nodrizas')}

    def get_id_cip_sec(self):
        """hash to id_cip sec"""
        u.printTime("id_cip")
        self.id_to_hash = {}
        
        sql = """select 
                    id_cip_sec, codi_sector,  hash_d 
                from 
                    u11"""
        for id, sec, hash in u.getAll(sql, 'import'):
            self.id_to_hash[(id)] = hash
    
    def get_usuaris(self):
        """."""
        self.usuaris = {}
        self.uba_to_dni = {}
        self.dni_to_uba = {}
        sql = "select left(ide_dni, 8), ide_categ_prof_c \
                from cat_pritb992 where ide_categ_prof_c in ('10999','10117','10116')"
        for usu, categ in u.getAll(sql, "import"):
            self.usuaris[(usu)] = categ
        
        sql = """select uab_codi_up, uab_codi_uab, lloc_codi_lloc_de_treball, lloc_numcol, left(lloc_proveidor_dni_proveidor, 8) 
                       from import.cat_pritb025 v25
                inner join 
                    import.cat_vistb039 v39
                on 
                    v25.lloc_codi_up=v39.uab_codi_up and lloc_codi_lloc_de_treball=uab_lloc_de_tr_codi_lloc_de_tr"""
        for up, uba, lloc, numcol, dni in u.getAll(sql, 'import'):
            if dni in self.usuaris:
                self.uba_to_dni[(up, uba)] = dni
                self.dni_to_uba[dni] = {'up': up, 'uba':uba}

        
    def get_pob(self):
        """."""
        u.printTime("Assignada")
       
        self.pob = {}
        sql = """select id_cip_sec, up, uba, upinf, ubainf from assignada_tot where edat > 14"""
        for id, up, uba, upinf, ubainf in u.getAll(sql, 'nodrizas'):
            hash = self.id_to_hash[(id)] if id in self.id_to_hash else None
            dni = self.uba_to_dni[(up,uba)] if (up,uba) in self.uba_to_dni else None
            self.pob[hash]  = {'up':up, 'uba':uba, 'upinf': upinf, 'ubainf': ubainf, 'dni': dni}
            

    def get_visites(self):
        """Agafem visites de últim any"""
        u.printTime("Visites")
        self.phy = c.Counter()

        sql = "select id_cip_sec, codi_sector, visi_up, left(visi_dni_prov_resp, 8), visi_data_visita, s_espe_codi_especialitat, visi_servei_codi_servei \
                    from visites1 \
                    where visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_dni_prov_resp<>'' and visi_col_prov_resp like ('1%')"
        for id, _sector, up, dni, dat, _categoria, _serveis in u.getAll(sql, "import"):
            if up in self.centres:
                if dni in self.dni_to_uba:
                    up_metge = self.dni_to_uba[dni]['up']
                    uba_metge = self.dni_to_uba[dni]['uba']
                    hash = self.id_to_hash[(id)] if id in self.id_to_hash else None
                    if hash in self.pob:
                        pac_assignat = 0
                        dni_pac = self.pob[hash]['dni']
                        if dni == dni_pac:
                            pac_assignat = 1
                        self.phy[(dni, up_metge, uba_metge, 'den')] += 1
                        self.phy[(dni, up_metge, uba_metge, 'num')] += pac_assignat
  
    def export(self):
        """exportem
        """
        u.printTime("export")
        self.upload = []
        for (dni, up_metge, uba_metge, tip), rec in self.phy.items():
            if tip == 'den':
                num = self.phy[(dni, up_metge, uba_metge, 'num')]
                self.upload.append([dni, up_metge, uba_metge, num, rec])

        tb = "mst_long_phy"
        db = "longitudinalitat"
        columns = ["dni varchar(40)", "up varchar(5)", "uba varchar(5)", "num int", "den int"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)

if __name__ == '__main__' :

        u.printTime("inici")
         
        Continuitat()

        u.printTime("Fi")