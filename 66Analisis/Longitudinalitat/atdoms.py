# coding: utf8

"""
Mirem de treure els EAP amb uba de atdom
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

db = 'permanent'
tb = 'long_atdom'

class uba_atdom(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_atdom()
        self.get_calculs()
        self.upload_data()

    def get_centres(self):
        """EAP ICS i no ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres where ep='0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}
       
    def get_atdom(self):
        """traiem números a nivell up"""
        self.recomptes = Counter()
        self.atdom_up = Counter()
        sql = 'select c_up,c_metge, case when ps_atdom_data is not null then 1 else 0 end as atdom from dbs where c_institucionalitzat is null'
        for up, uba, atdom in u.getAll(sql, 'redics'):
            if up in self.centres:
                self.recomptes[(up, uba, 'total')] += 1
                self.recomptes[(up, uba, 'atdom')] += atdom
                self.atdom_up[up] += atdom
                        
    def get_calculs(self):
        """."""
        self.upload = []
        for (up, uba, tip), rec in self.recomptes.items():
            if tip == 'total':
                atd = self.recomptes[(up, uba, 'atdom')]
                atdoms = self.atdom_up[up]
                try:
                    perc_atd = float(atd)/float(atdoms)
                except ZeroDivisionError:
                    perc_atd = 0
                try:    
                    perc_atd_uba = float(atd)/float(rec)
                except ZeroDivisionError:
                    perc_atd_uba = 0
                self.upload.append([up, uba, atd, rec, perc_atd_uba, perc_atd])           
    
    def upload_data(self):
        """."""
        columns = ["up varchar(5)", "uba varchar(5)", "atdom double", "assignats double", "perc_atd double", "perc_atd_up double"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        
        u.listToTable(self.upload, tb, db)
        
if __name__ == '__main__':
    uba_atdom()
    