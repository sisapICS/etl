# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

path = "D:/SISAP/sisap/66Analisis/dm2/analisi_territorial/data/"

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

# EQDM2 - Epidemiologia de la DM2 a Catalunya ( és una grupador, hi ha subindicadors)
EQDM2 = exemple(c, 'EQDM2###;AYR24###;AMBITOS#99;AGRESULT;EDATS5;NOINSAT;SEXE')

# EQAG02 - Diabetes mellitus tipus 2( és una grupador, hi ha subindicadors)
EQAG02 = exemple(c, 'EQAG02#1;AYR24###;AMBITOS#99;AGRESOLTR;EDATS5;NOINSAT;SEXE')

# EQA0602 - Revisió bucodental en DM2 amb mal control glicèmic
EQA0602 = exemple(c, 'EQA0602;AYR24###;AMBITOS#99;AGRESOLTR;EDATS5;NOINSAT;SEXE')

# NUTRES10 - Percentatge població diabètica tipus 2 i HbAC2 > 8 en seguiment per dietista i milloria metabòlica
NUTRES10 = exemple(c, 'NUTRES10;AYR24###;AMBITOS#99;AGRESULT;EDATS5;NOINSAT;SEXE')

# NUTPAT10 - Percentatge de població diabètica tipus 2 amb hemoglobina glicada > 10 amb valoració del dietista
NUTPAT10 = exemple(c, 'NUTPAT10;AYR24###;AMBITOS#99;AGRESULT;EDATS5;NOINSAT;SEXE')

# NUTPAT14 - Percentatge de població diabètica tipus 2 amb hemoglobina glicada 8-10 amb valoració del dietista
NUTPAT14 = exemple(c, 'NUTPAT14;AYR24###;AMBITOS#99;AGRESULT;EDATS5;NOINSAT;SEXE')

# MEDADIAB - Control excessiu DM2 en població envellida i polimedicada fràgil
NUTPAT14 = exemple(c, 'NUTPAT14;AYR24###;AMBITOS#99;AGRESULT;EDATS5;NOINSAT;SEXE')

# FARM0007 - Prescripció inadequada antidiabètics per funció renal
FARM0007 = exemple(c, 'FARM0007;AYR24###;AMBITOS#99;AGRESULT;NOCAT;NOIMP;DIM6SET')

# FARM0008 - Control metabòlic excessiu DM2 en població envellida
FARM0008 = exemple(c, 'FARM0008;AYR24###;AMBITOS#99;AGRESULT;NOCAT;NOIMP;DIM6SET')

# FARM0009 - Prescripció antidiabètics sense funció renal el darrer any
FARM0009 = exemple(c, 'FARM0009;AYR24###;AMBITOS#99;AGRESULT;NOCAT;NOIMP;DIM6SET')

# AGRDIABOP - Utilització d’antidiabètics (DIAB) per prescripció ( és una grupador, hi ha subindicadors)
AGRDIABOP = exemple(c, 'AGRDIABOP###;AYR24###;AMBITOS#99;AGRESULT;EDATS5;NOINSAT;SEXE')

#EQA procesos DM2:
# EQA3112 - DM2: Cribratge microalbuminúria
EQA3112 = exemple(c, 'EQA3112;AYR24###;AMBITOS#99;AGRESOLTR;EQADIM;DM2;DIM6SET')

# EQA3101 - Prescripció inadequada de tires en pacients amb DM2 en tractament amb dieta i/o metformina
EQA3101 = exemple(c, 'EQA3101;AYR24###;AMBITOS#99;AGRESOLTR;EQADIM;DM2;DIM6SET')

# EQA3113 - Antiagregants en DM2 sense MCV
EQA3113 = exemple(c, 'EQA3113;AYR24###;AMBITOS#99;AGRESOLTR;EQADIM;DM2;DIM6SET')

# EQA3114 - Prescripció de metformina o sulfonilurees en DM2 i FG<30
EQA3114 = exemple(c, 'EQA3114;AYR24###;AMBITOS#99;AGRESOLTR;EQADIM;DM2;DIM6SET')

# EQA3111 - Taxa estandarditzada de mortalitat en pacients amb DM2 ( aquest no sé si està bé o no)
EQA3111 = exemple(c, 'EQA3111;AYR24###;AMBITOS#99;AGRESOLTR;EQADIM;DM2;DIM6SET')

c.close()

print('export')

# EQDM2
file = path + "EQDM2.txt"
with open(file, 'w') as f:
   f.write(EQDM2)

# EQAG02
file = path + "EQAG02.txt"
with open(file, 'w') as f:
   f.write(EQAG02)

# EQA0602
file = path + "EQA0602.txt"
with open(file, 'w') as f:
   f.write(EQA0602)

# NUTRES10
file = path + "NUTRES10.txt"
with open(file, 'w') as f:
   f.write(NUTRES10)

# NUTPAT10
file = path + "NUTPAT10.txt"
with open(file, 'w') as f:
   f.write(NUTPAT10)

# NUTPAT14
file = path + "NUTPAT14.txt"
with open(file, 'w') as f:
   f.write(NUTPAT14)  

# FARM0007
file = path + "FARM0007.txt"
with open(file, 'w') as f:
   f.write(FARM0007)

# FARM0008
file = path + "FARM0008.txt"
with open(file, 'w') as f:
   f.write(FARM0008)

# FARM0009
file = path + "FARM0009.txt"
with open(file, 'w') as f:
   f.write(FARM0009)

# AGRDIABOP
file = path + "AGRDIABOP.txt"
with open(file, 'w') as f:
   f.write(AGRDIABOP)

# EQA3112
file = path + "EQA3112.txt"
with open(file, 'w') as f:
   f.write(EQA3112)

# EQA3101
file = path + "EQA3101.txt"
with open(file, 'w') as f:
   f.write(EQA3101)

# EQA3113
file = path + "EQA3113.txt"
with open(file, 'w') as f:
   f.write(EQA3113)

# EQA3114
file = path + "EQA3114.txt"
with open(file, 'w') as f:
   f.write(EQA3114)

# EQA3111
file = path + "EQA3111.txt"
with open(file, 'w') as f:
   f.write(EQA3111)