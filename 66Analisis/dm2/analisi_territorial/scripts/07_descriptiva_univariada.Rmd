
# Descriptiva Univariada {.tabset}

```{r}
dt.indicadors.flt <- readRDS("../data/dt.indicadors.flt.rds")
```

## Indicadors {.tabset}

```{r}
dt.indicadors.flt[, .N, amb_khalix_cod]
dt.indicadors.flt[, .N, ind_cod]
dt.indicadors.flt[, .N, periode]
```

```{r}
gc.var <- gc()
```


