
```{r, warning=warning.var, message=message.var}

# Importació
  #suppressWarnings(suppressPackageStartupMessages(library('ROracle')))
  suppressWarnings(suppressPackageStartupMessages(library('RMySQL')))
  # suppressWarnings(suppressPackageStartupMessages(library('readxl')))

# Diagrama de flux
  suppressWarnings(suppressPackageStartupMessages(library('DiagrammeR')))
  suppressWarnings(suppressPackageStartupMessages(library('DiagrammeRsvg')))
  suppressWarnings(suppressPackageStartupMessages(library('rsvg')))
  
# Data Manager
  suppressWarnings(suppressPackageStartupMessages(library('data.table')))
  # suppressWarnings(suppressPackageStartupMessages(library('tibble')))
  # suppressWarnings(suppressPackageStartupMessages(library('plyr')))
  suppressWarnings(suppressPackageStartupMessages(library('dplyr')))
  # suppressWarnings(suppressPackageStartupMessages(library('tidyr')))
  # suppressWarnings(suppressPackageStartupMessages(library('forcats')))
  # suppressWarnings(suppressPackageStartupMessages(library('encode')))


  
  # Dates
    suppressWarnings(suppressPackageStartupMessages(library('lubridate')))

  # String
    suppressWarnings(suppressPackageStartupMessages(library('stringr')))

# Anàlisi
  suppressWarnings(suppressPackageStartupMessages(library('compareGroups')))

  #suppressWarnings(suppressPackageStartupMessages(library('tidymodels')))
  
  # Stepwisee
    #suppressWarnings(suppressPackageStartupMessages(library('MASS')))
    
  # Series temporals
    # suppressWarnings(suppressPackageStartupMessages(library('tseries')))
    # suppressWarnings(suppressPackageStartupMessages(library('forecast')))

# Figures
  suppressWarnings(suppressPackageStartupMessages(library('ggplot2'))) 
  #suppressWarnings(suppressPackageStartupMessages(library('scales')))
  # suppressWarnings(suppressPackageStartupMessages(library('splines')))
  #suppressWarnings(suppressPackageStartupMessages(library('gridExtra')))
  #suppressWarnings(suppressPackageStartupMessages(library('ggpubr')))

# Taules
  suppressWarnings(suppressPackageStartupMessages(library('DT')))
  suppressWarnings(suppressPackageStartupMessages(library('kableExtra'))) 
  
# RMarkdown
  # suppressWarnings(suppressPackageStartupMessages(library('knitr')))
  
# Exportació
  suppressWarnings(suppressPackageStartupMessages(library('writexl')))
```

