***
***

# Descriptiva Bivariada

```{r}
dt.ana <- dt.ind.der.flt
```


Nota: <span style="color:red"> Pendent!! </span>.


```{r}
#| echo = TRUE
dt.ana.agr <- dt.ana[analisis == "NUM",
                     .(numerador = sum(n)),
                     .(up_desti_desc, periode)]
  
dt.updesti <- dcast(dt.ana.agr, up_desti_desc ~ periode, value.var = "numerador")
#dt.ind.02.uporigen$nespmissing <- apply(dt.ind.02.uporigen, 1, function(x) {sum(is.na(x))})
#dt.ind.02.uporigen[, .N, nespmissing]
```

```{r}
#rm(list=c("dt.biv","df","res","cT"))
gc.var <- gc()
```


