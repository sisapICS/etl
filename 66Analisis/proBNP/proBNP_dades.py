# -*- coding: utf8 -*-

"""
.
"""

import sisapUtils as u


DEBUG = False

TB = "probnp"
DB = "permanent"


class IC(object):
    """."""

    def __init__(self):
        """."""
        self.get_probnp()
        self.get_ecocardio()
        self.get_diagnostic()
        self.get_centres()
        self.get_poblacio()
        self.upload_data()

    def get_probnp(self):
        """."""
        centres = tuple([up for up, in u.getAll("select scs_codi from cat_centres", "nodrizas")])  # noqa
        self.probnp = {}
        sql = "select id_cip_sec, cr_data_reg, cr_codi_lab, \
                      cr_codi_prova_ics, cr_res_lab \
               from {} \
               where cr_codi_prova_ics in ('W14585', '004192', 'W14566','009799','W14572') and \
                     codi_up in {}"
        jobs = ([sql.format(table, centres), "import"] for table in u.getSubTables("laboratori"))  # noqa
        resultat = u.multiprocess(_get_data, jobs, 8)
        self._iterate_workers(resultat, self.probnp)

    def get_ecocardio(self):
        """."""
        self.ecocardio = {}
        sql = "select id_cip_sec, oc_data \
               from nod_proves \
               where inf_codi_prova in ('RA00416', 'RA00459', 'RA00468', 'TO010', 'TO011')"  # noqa
        jobs = [[sql, "nodrizas"]]
        sqls = [("variables",
                 "select id_cip_sec, vu_dat_act \
                  from {} \
                  where vu_cod_vs in ('EK402', 'VK4060', 'K406', 'ECO02')"),
                ("activitats",
                 "select id_cip_sec, au_dat_act \
                  from {} \
                  where au_cod_ac = '351'")]
        for ambit, sql in sqls:
            jobs.extend(([sql.format(table), "import"] for table in u.getSubTables(ambit)))  # noqa
        resultat = u.multiprocess(_get_data, jobs, 8)
        self._iterate_workers(resultat, self.ecocardio)

    def get_diagnostic(self):
        """."""
        sql = "select id_cip_sec, dde from eqa_problemes where ps = 21"
        self.diagnostic = {id: dde for (id, dde) in _get_data([sql, "nodrizas"])}  # noqa

    def _iterate_workers(self, resultat, objecte):
        """."""
        for worker in resultat:
            for row in worker:
                id = row[0]
                dades = row[1:] if len(row) > 2 else row[1]
                if id not in objecte or dades > objecte[id]:
                    objecte[id] = dades

    def get_centres(self):
        """."""
        sql = "select scs_codi, amb_desc, sap_desc, ics_codi, ics_desc \
               from nodrizas.cat_centres \
               where ep = '0208'"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_poblacio(self):
        """."""
        self.resultat = []
        sql = "select id_cip_sec, data_naix, sexe, up from assignada_tot where ates = 1"
        for id, edat, sexe, up in _get_data([sql, "nodrizas"]):
            if id in self.probnp and up in self.centres:
                this = [id, edat, sexe]
                this.extend(self.centres[up])
                this.extend(self.probnp[id])
                this.append(self.ecocardio.get(id))
                this.append(self.diagnostic.get(id))
                self.resultat.append(this)

    def upload_data(self):
        """."""
        cols = "(id int, naix date, sexe varchar(1), ambit varchar(255), sap varchar(255), \
                 br varchar(5), eap varchar(255), data date, \
                 laboratori varchar(5), codi varchar(25), \
                 resultat varchar(255), ecocardio date null, \
                 diagnostic date null)"
        u.createTable(TB, cols, DB, rm=True)
        u.listToTable(self.resultat, TB, DB)


def _get_data(param):
    """."""
    if DEBUG:
        param[0] += " limit 1000"
    return [row for row in u.getAll(*param)]


if __name__ == "__main__":
    IC()
