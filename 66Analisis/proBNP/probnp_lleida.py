# -*- coding: utf8 -*-

"""
.
"""

import sisapUtils as u

import collections as c

DEBUG = False

TB = "probnp_taxa_lleida"
DB = "permanent"


class IC(object):
    """."""

    def __init__(self):
        """."""
        self.get_probnp()
        self.get_centres()
        self.get_poblacio()
        self.upload_data()

    def get_probnp(self):
        """."""
        u.printTime("Lab")
        centres = tuple([up for up, in u.getAll("select scs_codi from cat_centres", "nodrizas")])  # noqa
        self.probnp = c.defaultdict(set)
        sql = "select id_cip_sec, cr_data_reg, cr_codi_lab, \
                      cr_codi_prova_ics, cr_res_lab \
               from {} \
               where cr_codi_prova_ics in ('W14585', '004192', 'W14566','009799','W14572') and \
                     codi_up in {}"
        jobs = ([sql.format(table, centres), "import"] for table in u.getSubTables("laboratori"))  # noqa
        resultat = u.multiprocess(_get_data, jobs, 8)
        self._iterate_workers(resultat, self.probnp)


    def _iterate_workers(self, resultat, objecte):
        """."""
        for worker in resultat:
            for row in worker:
                id = row[0]
                dat = row[1]
                objecte[id].add(dat)

    def get_centres(self):
        """."""
        u.printTime("centres")
        sql = "select scs_codi, amb_desc, sap_desc, ics_codi, ics_desc \
               from nodrizas.cat_centres \
               where ep = '0208' and amb_codi='01'"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_poblacio(self):
        """."""
        u.printTime("Pob")
        self.resultat = []
        sql = "select id_cip_sec, data_naix, sexe, up from assignada_tot"
        for id, edat, sexe, up in _get_data([sql, "nodrizas"]):
            if id in self.probnp and up in self.centres:
                dats = self.probnp[id]
                for dat in dats:
                    print dat
                    self.resultat.append([id, edat, sexe, up, dat])

    def upload_data(self):
        """."""
        u.printTime("export")
        cols = "(id int, naix date, sexe varchar(1), \
                 up varchar(5),  data date)"
        u.createTable(TB, cols, DB, rm=True)
        u.listToTable(self.resultat, TB, DB)


def _get_data(param):
    """."""
    if DEBUG:
        param[0] += " limit 1000"
    return [row for row in u.getAll(*param)]


if __name__ == "__main__":
    u.printTime("Inici")
    IC()
    u.printTime("Final")