# Flowchart

## Dades

Flowchart amb els criteris d'inclusió i d'exclusió.

```{r}

nrow(dt.dm[probnp == "Sí",])


```

```{r}
fin <- c(seq(1,20,1))

grViz("
      digraph a_nice_graph
      {
      
      node[fontname = Helvetica,
           fontcolor = black,
           shape = box,
           width = 1,
           style = filled,
           fillcolor = whitesmoke]
      
      '@@1' -> '@@2';
      }
      
      [1]: paste0('Taula proBNP', '\\n', 'N = ', nrow(dt.dm))
      [2]: paste0('proBNP valor text', '\\n', 'N = ', n2)
      ", height = 200, width = 800)
```

```{r}
# Filtre FINAL
  filtre <- paste0("probnp_val_tipus=='Valor numèric'")

  dt.flt <- dt.dm[eval(parse(text = filtre)),]
```

***

## Taxe de Lleida

Flowchart amb els criteris d'inclusió i d'exclusió.

```{r}

n1 <- nrow(dt.taxa.lleida.dm)

```

```{r}
fin <- c(seq(1,20,1))

grViz("
      digraph a_nice_graph
      {
      
      node[fontname = Helvetica,
           fontcolor = black,
           shape = box,
           width = 1,
           style = filled,
           fillcolor = whitesmoke]
      
      '@@1';
      }
      
      [1]: paste0('Taula proBNP Taxe LLeida', '\\n', 'N = ', nrow(dt.taxa.lleida.dm))
      ", height = 200, width = 800)
```

```{r}
# Filtre FINAL
  dt.taxa.lleida.flt <- dt.taxa.lleida.dm
```

***

## Taxe de proBNP

Flowchart amb els criteris d'inclusió i d'exclusió.

```{r}

n1 <- nrow(dt.probnp.dm)
n2 <- nrow(dt.probnp.dm[probnp_edat > 14,])
```

```{r}
fin <- c(seq(1,20,1))

grViz("
      digraph a_nice_graph
      {
      
      node[fontname = Helvetica,
           fontcolor = black,
           shape = box,
           width = 1,
           style = filled,
           fillcolor = whitesmoke]
      
      '@@1' -> '@@2';
      }
      
      [1]: paste0('Taula proBNP', '\\n', 'N = ', n1)
      [2]: paste0('Edat proBNP > 14 anys', '\\n', 'N = ', n2)
      ", height = 200, width = 800)
```

```{r}
# Filtre FINAL
  dt.probnp.flt <- dt.probnp.dm
```