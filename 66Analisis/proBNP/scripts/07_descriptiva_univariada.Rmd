# Descriptiva univariada

## Taula resum

Taula resum.

```{r descriptiva univariada taula resum, warning=warning.var}

df <- dt.flt
res <- compareGroups(probnp_val_cutoff1 ~ ambit + sap +
                                          sexe + probnp_edat + probnp_edat_c + 
                                          probnp_data + probnp_year + probnp_lab + probnp_codi +
                                          probnp_val + probnp_val_cutoff1 + probnp_val_cutoff2 +
                                          ecocardio_data + ecocardio + ecocardio_postprobnp + ecocardio_temps_probnp + 
                                          ic + ic_data + ic_postprobnp + ic_temps_probnp + ic_temps_ecocardio
                     , df
                     , method = 1
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = FALSE
                  , show.all = TRUE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT)
```

---

## Ambit

```{r}
table1(~ ambit,
         data = dt.flt)
ggplot(dt.flt, aes(x = ambit)) + geom_bar(fill = 'grey', color = "white") + coord_flip() + theme_classic()
```

## Sap

```{r}
table1(~ sap,
         data = dt.flt)
ggplot(dt.flt, aes(x = sap)) + geom_bar(fill = 'grey', color = "white") + coord_flip() + theme_classic()
#ggplot(dt.flt, aes(x=sap)) + geom_bar(fill='grey', color="white") + coord_flip() + facet_wrap( ~ ambit) + theme_classic()
```

## Edat

Descriptiva edat

```{r, warning=warning.var}
table1(~ probnp_edat + probnp_edat_c,
         data = dt.flt)
ggplot(dt.flt, aes(x = probnp_edat)) + geom_histogram(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = probnp_edat_c)) + geom_bar(fill = 'grey', color = "white") + theme_classic()
```

## Sexe

```{r}
table1(~ sexe,
         data = dt.flt)
ggplot(dt.flt, aes(x = sexe)) + geom_bar(fill = 'grey', color = "white") + theme_classic()
```

## proBNP

```{r, warning=warning.var}
table1(~ probnp_codi +
         probnp_year +
         probnp_val + probnp_val_cutoff1 + probnp_val_cutoff2,
         data = dt.flt)
ggplot(dt.flt, aes(x = probnp_data)) + geom_histogram(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = probnp_year)) + geom_bar(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = probnp_year)) + geom_bar(fill = 'grey', color = "white") + 
       theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1)) +  facet_wrap(~ambit)
ggplot(dt.flt, aes(x = probnp_codi)) + geom_bar(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = probnp_val)) + geom_histogram(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = log(probnp_val))) + geom_histogram(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt[probnp_val_tipus == "Valor numèric"], aes(x = probnp_val_tipus, y = log(probnp_val))) +
  geom_boxplot() +
  geom_jitter(color = "grey", alpha = 0.05) + theme_classic()
ggplot(dt.flt, aes(x = probnp_val_cutoff1)) + geom_bar(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = probnp_val_cutoff2)) + geom_bar(fill = 'grey', color = "white") + theme_classic()
```

## Ecocardio

```{r, warning=warning.var}
table1(~ ecocardio + ecocardio_postprobnp + ecocardio_temps_probnp,
         data = dt.flt)
ggplot(dt.flt, aes(x = ecocardio)) + geom_bar(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = ecocardio_data)) + geom_histogram(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = ecocardio_postprobnp)) + geom_bar(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = ecocardio_temps_probnp)) + geom_histogram(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = ecocardio_temps_probnp)) + geom_histogram(fill = 'grey', color = "white") + facet_grid(~ecocardio_postprobnp, scales = "free_x") + theme_classic()
```

## IC

```{r, warning=warning.var}
table1(~ ic + ic_postprobnp + ic_temps_probnp + ic_temps_ecocardio,
         data = dt.flt)
ggplot(dt.flt, aes(x = ic)) + geom_bar(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = ic_data)) + geom_histogram(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = ic_postprobnp)) + geom_bar(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = ic_temps_probnp)) + geom_histogram(fill = 'grey', color = "white") + theme_classic()
ggplot(dt.flt, aes(x = ic_temps_probnp)) + geom_histogram(fill = 'grey', color = "white") + facet_grid(~ic_postprobnp, scales = "free_x") + theme_classic()
ggplot(dt.flt, aes(x = ic_temps_ecocardio)) + geom_histogram(fill = 'grey', color = "white") + facet_grid(~ic_postprobnp, scales = "free_x") + theme_classic()
```
                                           + 
                                          
