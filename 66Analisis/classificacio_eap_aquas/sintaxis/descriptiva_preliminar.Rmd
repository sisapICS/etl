
```{r}
 
```


```{r fig.height=6, fig.width=10}
vars <- c("institucionalitzats", "immigrants_renta_baixa", "pensionistes", "pens_menys_65", "pcc", "maca", "atdom", "prof_grup9", "prof_grup2")
lapply(vars, function(x){
  print(x)
  ggplot(dades, aes(x = edat, y = get(x)/poblacio*100, fill = sexe)) +
  geom_boxplot() +
  scale_x_discrete(limits = c(paste0("EC", c(1519, 2024, 2529, 3034, 3539, 4044, 4549, 5054, 5559, 6064, 6569, 7074, 7579, 8084, 8589, 9094)), "EC95M")) +
  theme_minimal() +
  theme(
    legend.position = "top"
  ) +
    labs(x = "", y = paste(x)) +
  coord_flip() +
  facet_wrap(~medea)
  
})

```
```{r}
ggplot(dades, aes(x = edat, y = immigrants_renta_baixa/poblacio*100, fill = sexe)) +
  geom_boxplot() +
  scale_x_discrete(limits = c(paste0("EC", c(1519, 2024, 2529, 3034, 3539, 4044, 4549, 5054, 5559, 6064, 6569, 7074, 7579, 8084, 8589, 9094)), "EC95M")) 
```

