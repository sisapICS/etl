# -*- coding: utf8 -*-

"""
petició carmen: visites en nou_nats
"""

import collections as c

import sisapUtils as u

u.printTime("inici")

db = "permanent"

class visites15d(object):
    """."""

    def __init__(self):
        """."""

        self.get_visites()
        self.get_assignada()
        
        
    def get_visites(self):
        """Visites de import"""
        u.printTime("Visites")
        self.visites = {}
        sql = """SELECT id_cip_sec, min(visi_data_visita)
                FROM visites1 where visi_data_visita>'2022-12-31' and visi_situacio_visita='R'
                group by id_cip_sec"""
        for id, data in u.getAll(sql, "import"):
            self.visites[id] = data
    
    def get_assignada(self):
        """Visites de import"""
        u.printTime("assignada")
        self.recomptes = c.Counter()
        sql = """SELECT id_cip_sec, edat, data_naix from assignada_tot where data_naix between '2023-01-01' and '2023-03-31'"""
        for id, edat, naix in u.getAll(sql, "nodrizas"):
            visita15 = 0
            if id in self.visites:
                visi = self.visites[id]
                dies = u.daysBetween(naix, visi)
                if 0 <= dies <= 7:
                    visita15= 1
                print visi, naix, dies, visita15    
            self.recomptes['visi'] += visita15
            self.recomptes['pob'] += 1
        
        for tip, n in self.recomptes.items():
            print tip, n
            

if __name__ == '__main__' :
        
        visites15d()

        u.printTime("Fi")