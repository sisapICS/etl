
# -*- coding: utf-8 -*-

"""
.
"""

import collections as c
import csv
import datetime as d
import itertools as it
import os
import future.standard_library as f
f.install_aliases()
import urllib.request as w  # noqa
import sys


import sisapUtils as u




url = "https://momo.isciii.es/public/momo/data"
response = w.urlopen(url)
upload = []
for _row in response:
    if sys.version_info[0] == 3:
        _row = _row.decode()
    row = _row.replace("\"", "").split(",")
    if row[2] == "9" and row[4] == "all":
        data = d.datetime.strptime(row[8], "%Y-%m-%d").date()
        upload.append([data, row[7], row[9]])

cols = ("data date", "edat varchar2(16)", "obs number")

file = u.tempFolder + "dades_momo.txt"
        
u.writeCSV(file, upload, sep='@')
#u.createTable('SISAP_COVID_AGR_MOMO', "({})".format(", ".join(cols)), 'redics', rm=True)      
#u.listToTable(upload, 'SISAP_COVID_AGR_MOMO', 'redics')  