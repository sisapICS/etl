
```{r vacunes covid}
library(RJDBC)
server<-"excdox-scan.cpd4.intranet.gencat.cat"
port<-"1522"
sid<-"excdox01srv"
# Create connection driver and open connection
jdbcDriver <- JDBC(driverClass="oracle.jdbc.OracleDriver", classPath="C:/Oracle/OJDBC-Full/ojdbc6.jar")
jdbcStr = paste("jdbc:oracle:thin:@", server, ":", port, "/", sid, sep="")
jdbcConnection = dbConnect(jdbcDriver, jdbcStr, "DWECOMA", "ranagustavo2020")



vacunes <- as.data.table(dbGetQuery(jdbcConnection,"SELECT * FROM dwsisap.sivic_vacunes_vacuna cv2 WHERE campanya IN (1,2,6)"))




dbDisconnect(jdbcConnection)
```

```{r}

vacunes[EDAT_CODI %in% 0:4, g_edat := "0-44"]
vacunes[EDAT_CODI %in% 5:7, g_edat := "45-79"]
vacunes[EDAT_CODI == 8, g_edat := "80 o més"]
vacunes[, data := as.Date(DATA)]

vacunes_dm <- vacunes[, .(
  vacunes = sum(RECOMPTE)), by = .(data, ABS = ABS_CODI, g_edat)]

vacunes_aggr <- rbind(
  vacunes_dm[, .(
    Classificacio = "GLOBAL",
    Categoria = "Global",
    VACUNES = sum(vacunes, na.rm = T)
    ), by = .(data)],
  vacunes_dm[!is.na(g_edat), .(
    Classificacio = "EDAT",
    VACUNES = sum(vacunes, na.rm = T)
  ), by = .(data, Categoria = g_edat)]
  )

all_dates <- data.table(
  expand.grid(data = seq(as.Date("2012-01-01"), today(), by = "1 day"),
              Categoria = unique(vacunes_aggr$Categoria))
)
all_dates <- merge(all_dates, unique(vacunes_aggr[, .SD, .SDcols = c("Classificacio", "Categoria")]), by = c("Categoria"))
vacunes_aggr_all_dates <- merge(vacunes_aggr, all_dates, by = c("data", "Classificacio", "Categoria"), all.y = T)



vacunes_aggr_all_dates[is.na(VACUNES), VACUNES := 0]
vacunes_aggr_all_dates[order(data), VACUNES_MA7 := Reduce(`+`, shift(VACUNES, 0:6))/7, by = .(Classificacio, Categoria)]
vacunes_aggr_all_dates[order(data), vacunes_ma7_lag_15 := shift(VACUNES_MA7, n = 15, type = "lag"), .(Classificacio, Categoria)]

```



[Executar datamanager_ii de la mateixa carpeta]


```{r}
dades_estiu_21_22_vacunes <- merge(dades_estiu_21_22[Classificacio %in% c("GLOBAL", "EDAT")], vacunes_aggr_all_dates, by = c("data", "Classificacio", "Categoria"), all.x = T)

dades_estiu_21_22_vacunes[, vacunes_z := (VACUNES - mean(VACUNES, na.rm = T))/sd(VACUNES, na.rm = T), by = .(TIPUS_DEFUNCIONS, Categoria, Classificacio, Any)]
dades_estiu_21_22_vacunes[, vacunes_7_z := (VACUNES_MA7 - mean(VACUNES_MA7, na.rm = T))/sd(VACUNES_MA7, na.rm = T), by = .(TIPUS_DEFUNCIONS, Categoria, Classificacio, Any)]
dades_estiu_21_22_vacunes[, vacunes_7_lag_15_z := (vacunes_ma7_lag_15 - mean(vacunes_ma7_lag_15, na.rm = T))/sd(vacunes_ma7_lag_15, na.rm = T), by = .(TIPUS_DEFUNCIONS, Categoria, Classificacio, Any)]

```

