
```{r, warning=warning.var, message=message.var}

# Importació
  # if (params$actualitzar_dades) {suppressWarnings(suppressPackageStartupMessages(library('ROracle')))
  #                                suppressWarnings(suppressPackageStartupMessages(library('data.table')))
  #   } else {
  suppressWarnings(suppressPackageStartupMessages(library('RMySQL')))

# Codificació (SHA1)
  #suppressWarnings(suppressPackageStartupMessages(library('digest')))

# EDA
  suppressWarnings(suppressPackageStartupMessages(library('skimr')))

# Diagrama de flux
  suppressWarnings(suppressPackageStartupMessages(library('DiagrammeR')))
  suppressWarnings(suppressPackageStartupMessages(library('DiagrammeRsvg')))
  suppressWarnings(suppressPackageStartupMessages(library('rsvg')))
  
# Data Manager
  #suppressWarnings(suppressPackageStartupMessages(library('lubridate')))
  suppressWarnings(suppressPackageStartupMessages(library('data.table')))
  # suppressWarnings(suppressPackageStartupMessages(library('tibble')))
  # suppressWarnings(suppressPackageStartupMessages(library('plyr')))
  suppressWarnings(suppressPackageStartupMessages(library('dplyr')))
  suppressWarnings(suppressPackageStartupMessages(library('tidyr')))
  # suppressWarnings(suppressPackageStartupMessages(library('forcats')))
  # suppressWarnings(suppressPackageStartupMessages(library('encode')))
  

  # String
    suppressWarnings(suppressPackageStartupMessages(library('stringr')))
    
# Matching
  #suppressWarnings(suppressPackageStartupMessages(library('Matching')))
  
# Anàlisi
  suppressWarnings(suppressPackageStartupMessages(library('compareGroups')))
  #suppressWarnings(suppressPackageStartupMessages(library('stddiff')))
#   #suppressWarnings(suppressPackageStartupMessages(library('nlme')))
#   #suppressWarnings(suppressPackageStartupMessages(library('statmod')))
#   
#   # Modelització
#     suppressWarnings(suppressPackageStartupMessages(library('sjPlot')))
#     #suppressWarnings(suppressPackageStartupMessages(library('lme4')))
#     #suppressWarnings(suppressPackageStartupMessages(library('MASS'))) # Binomial Negative
#     #suppressWarnings(suppressPackageStartupMessages(library('pscl'))) # Zero-Inflated Regression
# 
# # Figures
    suppressWarnings(suppressPackageStartupMessages(library('ggplot2'))) 
#   suppressWarnings(suppressPackageStartupMessages(library('scales')))
#   #suppressWarnings(suppressPackageStartupMessages(library('splines')))
#   #suppressWarnings(suppressPackageStartupMessages(library('gridExtra')))
# 
# # Taules
    suppressWarnings(suppressPackageStartupMessages(library('DT')))
#   suppressWarnings(suppressPackageStartupMessages(library('kableExtra')))
#   suppressWarnings(suppressPackageStartupMessages(library('table1'))) 
#   
# # RMarkdown
#   suppressWarnings(suppressPackageStartupMessages(library('knitr')))
  
# Exportació
  #suppressWarnings(suppressPackageStartupMessages(library('writexl')))
#}
```

