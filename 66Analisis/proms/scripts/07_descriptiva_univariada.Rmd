***
***

# Descriptiva Univariada

## Taula Resum

Taula resum amb la desciptiva de totes les variables disponibles.

### Tots els resgistres

```{r, warning=warning.var}
#summary(dt.var.flt)

# df <- dt.var.flt[, taula := "Proms"]
# res <- compareGroups(taula ~ . -id_cip -vu_usu -ide_numcol -ide_dni -ics_codi -ics_desc
#                      , df
#                      , method = 1
#                      , max.xlev = 100
#                      , include.miss = FALSE)
# cT <- createTable(res
#                   , show.descr = FALSE
#                   , show.all = TRUE
#                   , show.p.overall = FALSE
#                   , show.n = TRUE
#                   )
# export2md(cT,
#           format = "html",
#           caption = "")
# rm(list = c("df","res","cT"))
```

### Resgistres amb PROMS

```{r, warning=warning.var}
#summary(dt.var.flt)
# dt.df <- dt.var.flt[proms == "Sí", ]
# df <- dt.df[, taula := "Proms"]
# res <- compareGroups(up_pilot ~ . -id_cip- vu_usu -ics_codi
#                      , df
#                      , method = 1
#                      , max.xlev = 100
#                      , include.miss = TRUE)
# cT <- createTable(res
#                   , show.descr = TRUE
#                   , show.all = FALSE
#                   , show.p.overall = FALSE
#                   , show.n = TRUE
#                   )
# export2md(cT,
#           format = "html",
#           caption = "")
# rm(list = c("df","res","cT"))
```

## Variables

Descriptiva de les variables de la taula

### Tots els registres

#### Codi/Descripció

```{r, fig.width = 10, fig.height = 5}
dt.var.flt[, codi_desc := paste0(vs_des, " (", vu_cod_vs, ")")]
ggplot(dt.var.flt, aes(x = codi_desc)) + 
  geom_bar() +
  facet_grid(. ~ escala) +
  coord_flip() +
  theme_minimal()
```

#### Data

```{r, fig.width = 10, fig.height = 5}
dt.ggplot <- dt.var.flt[, .N, .(vu_dat_act)]
ggplot(dt.ggplot, aes(x = vu_dat_act, y = N)) + 
  geom_line() +
  theme_minimal() +
  facet_wrap(. ~ year(vu_dat_act), scales = "free", ncol = 2) +
  geom_vline(xintercept = as.numeric(as.Date("2022-12-14")), linetype = "dotted", 
              color = "red", size = 1)
```

#### Valor

```{r, fig.width = 10, fig.height = 6}
ggplot(dt.var.flt, aes(x = vu_val, y = codi_desc)) + 
  geom_jitter(alpha = 0.2, color = "orange" ) +
  geom_boxplot(alpha = 0.4) +
  facet_grid(. ~ escala, scales = "free_x") +
  theme_minimal()
```

#### Professional

```{r}
dt.datatable <- dt.var.flt[, .N, .(prof_tipus)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

#### PROMS

##### Tots

```{r}
dt.datatable <- dt.var.flt[, .N, .(escala, codi_desc)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

##### Escala de depressió PHQ9

```{r}
dt.datatable <- dt.var.flt[escala == "Escala de depressió PHQ9", .N, .(escala, codi_desc)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

##### Escala IPSS

```{r}
#dt.var.flt[, .N, .(escala)]
dt.datatable <- dt.var.flt[escala == 'Escala IPSS', .N, .(escala, codi_desc)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

##### Escala d'ansietat GAD-7

```{r}
#dt.var.flt[, .N, .(escala)]
dt.datatable <- dt.var.flt[escala == "Escala d'ansietat GAD-7", .N, .(escala, codi_desc)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

##### EuroQol

```{r}
dt.datatable <- dt.var.flt[escala == "EuroQol", .N, .(escala, codi_desc)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

***

### Registres amb PROMS

```{r}
dt.var.flt.proms <- dt.var.flt[proms == "Sí", ]
```

#### Codi/Descripció

```{r, fig.width = 10, fig.height = 5}
dt.var.flt.proms[, codi_desc := paste0(vs_des, " (", vu_cod_vs, ")")]
ggplot(dt.var.flt.proms, aes(x = codi_desc)) + 
  geom_bar() +
  facet_grid(. ~ escala) +
  coord_flip() +
  theme_minimal()
```

#### Data

```{r, fig.width = 10, fig.height = 5}
dt.ggplot <- dt.var.flt.proms[, .N, .(vu_dat_act)]
ggplot(dt.ggplot, aes(x = vu_dat_act, y = N)) + 
  geom_line() +
  theme_minimal() +
  facet_wrap(. ~ year(vu_dat_act), scales = "free", ncol = 2) +
  geom_vline(xintercept = as.numeric(as.Date("2022-12-14")), linetype = "dotted", 
              color = "red", size = 1)
```

#### Valor

```{r, fig.width = 10, fig.height = 6}
ggplot(dt.var.flt.proms, aes(x = vu_val, y = codi_desc)) + 
  geom_jitter(alpha = 0.2, color = "orange" ) +
  geom_boxplot(alpha = 0.4) +
  facet_grid(. ~ escala, scales = "free_x") +
  theme_minimal()
```

#### Professional

```{r}
dt.datatable <- dt.var.flt.proms[, .N, .(prof_tipus)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

#### PROMS

##### Tots

```{r}
dt.datatable <- dt.var.flt.proms[, .N, .(escala, codi_desc)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

##### Escala de depressió PHQ9


```{r}
dt.datatable <- dt.var.flt.proms[escala == "Escala de depressió PHQ9", .N, .(escala, codi_desc)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

##### Escala IPSS


```{r}
#dt.var.flt[, .N, .(escala)]
dt.datatable <- dt.var.flt.proms[escala == 'Escala IPSS', .N, .(escala, codi_desc)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

##### Escala d'ansietat GAD-7

```{r}
#dt.var.flt[, .N, .(escala)]
dt.datatable <- dt.var.flt.proms[escala == "Escala d'ansietat GAD-7", .N, .(escala, codi_desc)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

##### EuroQol


```{r}
dt.datatable <- dt.var.flt.proms[escala == "EuroQol", .N, .(escala, codi_desc)]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

```{r}
gc.var <- gc()
```


