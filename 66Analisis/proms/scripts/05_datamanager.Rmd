***
***

# Data Manager

## Variables

```{r}
dt.var.dm <- dt.var.raw

dt.var.dm[, up_pilot := "No"][vu_up %in% c('00104','00378','01886','00054','04376','01077','00494','00030','00149','00089','00017'), up_pilot := "Sí"]
#dt.var.dm[, .N, up_pilot]
dt.var.dm[, vu_dat_act := as.Date(vu_dat_act, '%Y-%m-%d')]
dt.var.dm[, vu_cod_vs := as.factor(vu_cod_vs)]
```


## Tokens

```{r}
dt.tokens.dm <- dt.tokens.raw

dt.tokens.dm[, vu_dat_act := as.Date(vu_dat_act, '%Y-%m-%d')]
dt.tokens.dm[, vu_cod_vs := as.factor(vu_cod_vs)]
```

## Catàlegs

### Variables

```{r}
dt.cat_var.dm <- dt.cat_var.raw

dt.cat_var.dm <- dt.cat_var.dm[, .(n_sector = .N), .(vs_cod, vs_des)]
dt.cat_var.dm <- dt.cat_var.dm[!(vs_des %in% c("Intenció de participar?",
                                               "Cribratge colon fet",
                                               "Motius exclusió cribratge", 
                                               "Consentiment informat oral",
                                               "Consell PIUC",
                                               "Cigarrets dia",
                                               "Tipus d'interven. sobre la pràct. d'e.f.")),]

dt.cat_var.dm <- dt.cat_var.dm[vs_des == 'EurpQol. Estat de salut avui', vs_des := 'EuroQol. Estat de salut avui']
dt.cat_var.dm <- dt.cat_var.dm[, .(n_sector = sum(n_sector)), .(vs_cod, vs_des)]

dt.cat_var.dm <- dt.cat_var.dm[vs_cod %in% c('VP5403', 'VP5405'), escala := "Escala de depressió PHQ9"]
dt.cat_var.dm <- dt.cat_var.dm[vs_cod %in% c('5162', '5171','VY4001'), escala := "Escala IPSS"]
dt.cat_var.dm <- dt.cat_var.dm[vs_cod %in% c('VP70C0'), escala := "Escala d'ansietat GAD-7"]
dt.cat_var.dm <- dt.cat_var.dm[vs_cod %in% c('X00004', 'X00005', 'X00006', 'X00007', 'X00008', 'X00009', 'XP0012'), escala := "EuroQol"]

dt.cat_var.dm[, vs_cod := as.factor(vs_cod)]
dt.cat_var.dm[, vs_des := as.factor(vs_des)]
dt.cat_var.dm[, n_sector := as.factor(n_sector)]
dt.cat_var.dm[, escala := as.factor(escala)]
                    
dt.datatable <- dt.cat_var.dm
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable)))
```

### Professionals

```{r}
dt.cat_prof.dm <- dt.cat_prof.raw
```

### Centres

```{r}
dt.cat_centres.dm <- dt.cat_centres.raw
dt.cat_centres.dm <- dt.cat_centres.dm[, cat_up_pilot := "No"][scs_codi %in% c('00104','00378','01886','00054','04376','01077','00494','00030','00149','00089','00017'), cat_up_pilot := "Sí"]
#dt.cat_centres.dm[, .N, cat_up_pilot]
```

***

## Afegir Taules/Catàlegs

Afegim el taules i catàlegs a la taula de vacunats

```{r}
dt.var.dm <- merge(dt.var.dm,
                   dt.tokens.dm[, c("id_cip", "vu_cod_vs", "vu_dat_act", "vu_token", "vu_id_proms")],
                   by.x = c("id_cip", "vu_cod_vs", "vu_dat_act"),
                   by.y = c("id_cip", "vu_cod_vs", "vu_dat_act"),
                   all.x = TRUE)
dt.var.dm[, token := "No"][nchar(vu_token) == 36, token := "Sí"]
dt.var.dm[, proms := "No"][nchar(vu_id_proms) == 36, proms := "Sí"]
#dt.var.dm[, .N, token]
#a<-dt.var.dm[, .N, .(token, vu_token)]
#dt.var.dm[, .N, proms]

dt.var.dm <- merge(dt.var.dm,
                   dt.cat_centres.dm,
                   by.x = c("vu_up"),
                   by.y = c("scs_codi"),
                   all.x = TRUE)

dt.var.dm <- merge(dt.var.dm,
                   dt.cat_var.dm,
                   by.x = c("vu_cod_vs"),
                   by.y = c("vs_cod"),
                   all.x = TRUE)

dt.var.dm <- merge(dt.var.dm,
                   dt.cat_prof.dm,
                   by.x = c("vu_usu"),
                   by.y = c("ide_usuari"),
                   all.x = TRUE)
```

```{r}
dt.var.dm[is.na(ics_desc), up_pilot := "No EAP"]
dt.var.dm[ , .N, up_pilot]
dt.var.dm[is.na(prof_tipus), prof_tipus := "Missing"]
dt.var.dm[is.na(ep), ep := "Missing"]
```

```{r data manager gc}
gc.var <- gc()
```

