
```{r}
dades <- merge(ass_embaras_iassir05_analisi,
               cat_centres, by.x = "up", by.y = "scs_codi", all.x = T)

require(stringr)
cat_nacionalitat[, codi_nac := str_pad(codi_nac, width = 3, pad = "0")]
dades <- merge(dades, 
               cat_nacionalitat, by.x = "nacionalitat", by.y = "codi_nac", all.x = T)
# dades <- merge(dades, 
#                unique(ass_centres[, .(amb_codi, amb_desc, up_assir)]), by.x = "visi_up_assir", by.y = "up_assir", all.x = T)
Encoding(dades$desc_nac) <- "latin1"
Encoding(dades$amb_desc) <- "latin1"
dades[is.na(desc_nac), desc_nac := "ESPANYA"]
```

```{r}
dades[, inici := as.Date(inici)]
dades[, fi_original := as.Date(fi_original)]
dades[, fi := as.Date(fi)]
```

```{r}
dades[, edat_cat := cut(edat, breaks = c(0, 20, 25, 30, 40, 50, 100), include.lowest = F, right = T, labels = c("<20 anys", "20-25 anys", "25-30 anys", "30-40 anys", "40-50 anys", ">50 anys"))]
dades[, ruralitat := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), labels = c("Rural", "Rural", "Rural", "Urbà", "Urbà", "Urbà", "Urbà"))]
dades[, medea := factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), labels = c("Rural", "Rural", "Rural", "1U", "2U", "3U", "4U"))]
```




