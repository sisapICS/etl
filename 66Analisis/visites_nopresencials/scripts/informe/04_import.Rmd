***
***

# Importació

Importació de la informació per executar el projecte.

## Dades

Importació de les dades necessàries per  fer l'anàlisi.

```{r}
ini.import <- Sys.time()
```

### DBS

Importació de la taula DBS de redICS.

```{r}
if (params$actualitzar_dades) {
  ini <- Sys.time()
  source("C:/Users/ehermosilla/Documents/Keys.R")
  drv <- dbDriver("Oracle")
  connect.string <- paste("(DESCRIPTION=",
                          "(ADDRESS=(PROTOCOL=tcp)(HOST=", hostredics, ")(PORT=", portredics, "))",
                          "(CONNECT_DATA=(SERVICE_NAME=", dbredics, ")))", sep = "")
  con <- dbConnect(drv, username = idredics, password = pwredics, dbname = connect.string)
  query <- dbSendQuery(con,
                       statement = "select C_CIP,
                                           C_UP,
                                           C_EDAT_ANYS, C_GRUP_EDAT,
                                           C_SEXE,
                                           C_GMA_COMPLEXITAT, C_GMA_N_CRONIQUES, C_GMA_CODI,
                                           C_INSTITUCIONALITZAT,
                                           C_NACIONALITAT,
                                           VI_A_9c9R, VI_A_9D, VI_A_9E_ECTA, VI_A_9E_NOECTA, VI_A_9T,
                                           VI_I_9c9R, VI_I_9D, VI_I_9E_ECTA, VI_I_9E_NOECTA, VI_I_9T,
                                           VI_M_9c9R, VI_M_9D, VI_M_9E_ECTA, VI_M_9E_NOECTA, VI_M_9T,
                                           EP_CVA_EPISODIS, EP_GEA_EPISODIS, EP_INTOXICACIO_EPISODIS,
                                           EP_ITU_EPISODIS , EP_OTITIS_MITJANA_EPISODIS, EP_TRAUMATISME_EPISODIS,
                                           PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                                           PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                                           PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                                           PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                                           PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,PS_DEMENCIA_DATA, PS_ARTROSI_DATA, 
                                           PS_ANSIETAT_DATA , PS_DEPRESSIO_DATA , PS_FIBROMIALGIA_DATA from DBS")
  dt.dbs <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  fi <- Sys.time()
  fi - ini # 
  saveRDS(dt.dbs, paste0("../data/visites_nopresencials_", format(Sys.Date(),'%Y%m%d'),".rds"))
  saveRDS(dt.dbs[sample(100000)], paste0("../data/visites_nopresencials_sample_", format(Sys.Date(),'%Y%m%d'),".rds"))
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.dbs <- readRDS("../data/visites_nopresencials_20230116.rds")
  #dt.dbs <- readRDS("../data/visites_nopresencials_sample_20221123.rds")
  fi <- Sys.time()
  fi - ini # 
  gc.var <- gc()
}
```


```{r}
names(dt.dbs) <- tolower(names(dt.dbs))
```

Informació importada:

- files n = `r nrow(dt.dbs)`
- columnes n = `r ncol(dt.dbs)`
- columnes: `r names(dt.dbs)`

#### Summary

```{r}
ini <- Sys.time()
dt.skim <- data.table(skim(dt.dbs))
fi <- Sys.time()
fi - ini # 
```

##### Character

```{r}
dt.datatable <- dt.skim[skim_type == "character", skim_variable:character.whitespace]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable),
                         columnDefs = list(list(className = 'dt-center', targets = 0:7)))) %>%
          formatRound(columns = c('complete_rate'), digits = 2)
```

##### Numeric

```{r}
dt.datatable <- dt.skim[skim_type == "numeric", c("skim_variable", 
                                                  "n_missing", "complete_rate",
                                                  "numeric.mean", "numeric.sd",
                                                  "numeric.p0", "numeric.p25", "numeric.p50", "numeric.p75", "numeric.p100", "numeric.hist")]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable),
                         columnDefs = list(list(className = 'dt-center', targets = 0:10)))) %>%
          formatRound(columns = c("complete_rate","numeric.mean", "numeric.sd"), digits = 2)
```

##### Data

```{r}
dt.datatable <- dt.skim[skim_type == "POSIXct", c("skim_variable", 
                                                  "n_missing", "complete_rate",
                                                  "POSIXct.min", "POSIXct.max", "POSIXct.median", "POSIXct.n_unique")]
datatable(dt.datatable,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.datatable),
                         columnDefs = list(list(className = 'dt-center', targets = 0:6)))) %>%
          formatRound(columns = c("complete_rate"), digits = 2)
```

***

## Catàlegs

### Catèleg de Centres SISAP

S'importa el catàleg de centres de nodrizas.

```{r cat_centres}
source("C:/Users/ehermosilla/Documents/Keys.R")
drv <- dbDriver("MySQL")
con <- dbConnect(drv,
                 user = idsisap,
                 password = pwsisap,
                 host = hostsisap,
                 port = portsisap,
                 dbname = "nodrizas")
query <- dbSendQuery(con,
                     statement = "select ep, scs_codi, ics_codi, ics_desc, tip_eap, medea, aquas from cat_centres")
dt.cat_centres.raw <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)

setnames(dt.cat_centres.raw, "medea", "medea_c")
```

**Informació importada:**
 
 - files n = `r nrow(dt.cat_centres.raw)`
 - columnes n = `r ncol(dt.cat_centres.raw)`
 - columnes: `r names(dt.cat_centres.raw)`


**Quantitat de missing:**

```{r}
df <- data.frame(dt.cat_centres.raw)
df <- df %>% mutate(taula = "Catàleg Centres/EAP")
res <- compareGroups(taula ~ .
                     , df
                     , method = 1
                     , include.label = FALSE)
export2md(missingTable(res
                       , show.p.overall = FALSE
                       ),
          caption = "",
          format = "html")
```


### Catèleg de Centres DBC

S'importa el catàleg de centres de nodrizas.

```{r cat centres dbs}
if (params$actualitzar_dades) {
  ini <- Sys.time()
  source("C:/Users/ehermosilla/Documents/Keys.R")
  drv <- dbDriver("Oracle")
  connect.string <- paste("(DESCRIPTION=",
                          "(ADDRESS=(PROTOCOL=tcp)(HOST=", hostexadata, ")(PORT=", portexadata, "))",
                          "(CONNECT_DATA=(SERVICE_NAME=", dbexadata, ")))", sep = "")
  con <- dbConnect(drv, username = idexadata, password = pwexadata, dbname = connect.string)
  query <- dbSendQuery(con,
                       statement = "select * from dwsisap.dbc_centres")
  dt.dbc.centres.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  fi <- Sys.time()
  fi - ini # 
  saveRDS(dt.dbc.centres.raw, paste0("../data/dbc.centres_", format(Sys.Date(),'%Y%m%d'),".rds"))
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.dbc.centres.raw <- readRDS("../data/dbc.centres_20230116.rds")
  fi <- Sys.time()
  fi - ini # 
  gc.var <- gc()
}
```

**Informació importada:**
 
 - files n = `r nrow(dt.dbc.centres.raw)`
 - columnes n = `r ncol(dt.dbc.centres.raw)`
 - columnes: `r names(dt.dbc.centres.raw)`


```{r}
names(dt.dbc.centres.raw) <- tolower(names(dt.dbc.centres.raw))
```

**Quantitat de missing:**

```{r}
df <- data.frame(dt.dbc.centres.raw)
df <- df %>% mutate(taula = "Catàleg DBC Centres/EAP")
res <- compareGroups(taula ~ .
                     , df
                     , method = 1
                     , include.label = FALSE)
export2md(missingTable(res
                       , show.p.overall = FALSE
                       ),
          caption = "",
          format = "html")
```


***
***

```{r}
fi <- Sys.time()
fi - ini.import #
gc.var <- gc()
```