
```{r}
library(DiagrammeR)
library(DiagrammeRsvg)
library(rsvg)

#### TAULA vacunes_mancants
g <-  grViz("digraph diagraph {
      bgcolor = transparent
 
      node[fontname = Helvetica,
           fontcolor = '#151F28',
           fontsize = 10,
           shape = box,
           width = 1,
           style = filled,
           fillcolor = 'white']
       
       '@@1'
       '@@2'
       '@@3'
       '@@4'
       
       
       node [shape=none, width=0, height=0, label='']
       w1
       w2


       
       edge []
       w1 -> '@@2';
       w2 -> '@@3'
       w2 -> '@@4'
      
       
       edge [dir=none]
       '@@1' -> w1;
       w1 -> w2;
      

       
      {rank=same; w1 -> '@@2'}
      {rank=same; w2 -> '@@3'}

       

       }
    
      [1]: paste0('Cohort inicial', '\\n', format(N0, big.mark = '.', decimal.mark = ','))
      [2]: paste0('Dies de tractament < 30 dies: ', format(excl_1, big.mark = '.', decimal.mark = ','), '\\n')
      [3]: paste0('Pacients no atesos: ', format(excl_2, big.mark = '.', decimal.mark = ','), '\\n')
      [4]: paste0('Cohort final', '\\n', format(N1, big.mark = '.', decimal.mark = ','))



     ")
g
g %>%
    export_svg %>% charToRaw %>% rsvg_png("flowchart.png")

```
