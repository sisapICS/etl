# -*- coding: utf8 -*-

"""
Bronquiolitis i VRS en nens
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


tb = 'sisap_bronquiolitis'
db = 'permanent'

codis = '("C01-J21", "C01-J21.0", "C01-J21.1", "C01-J21.8", "C01-J21.9" )'

class Bronquiolitis(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_assignada()
        self.get_refredats()

    
    def get_assignada(self):
        """."""
        u.printTime("poblacio")
        self.pob = {}
        sql = 'select id_cip, usua_sexe, usua_data_naixement from assignada'
        for id, sexe, naix in u.getAll(sql, 'import'):
            self.pob[id] = {'sexe': sexe, 'naix': naix}
    
    def get_refredats(self):
        """."""
        u.printTime("problemes")
        
        sql = "select id_cip, pr_dde, pr_cod_ps, pr_th \
           from problemes \
           where pr_cod_o_ps = 'C' and \
                 pr_cod_ps in {} and \
                 extract(year_month from pr_dde) >'201109' and \
                 pr_data_baixa = 0 \
                 group by id_cip, pr_dde, pr_cod_ps, pr_th".format(codis)
        
        upload = []
        for id, dde, ps, th in u.getAll(sql, 'import'):
            sexe = self.pob[id]['sexe'] if id in self.pob else None
            naix = self.pob[id]['naix'] if id in self.pob else None 
            upload.append([naix, sexe, dde])
        cols = ("data_naix date", "sexe varchar(10)", "data_diagnostic date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(upload, tb, db)

                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Bronquiolitis()
    
    u.printTime("Final")                 
