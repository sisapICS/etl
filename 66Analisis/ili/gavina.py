# -*- coding: utf8 -*-

"""
Gavina, prediccio grip
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u




codis_ps = {
    "Bronquiolitis":  {"J21.9", "J21", "B97.4", "J21.0", "C01-B97.4", "C01-J21.0", "C01-J21.9", "C01-J21"},
    "Grip identificada": {"C01-J09", "C01-J09.X", "J09", "C01-J09.X1", "C01-J09.X2", "C01-J09.X3", "C01-J09.X9", "C01-J10", "C01-J10.0", "C01-J10.8", "J10", "J10.0", "C01-J10.00", "C01-J10.01", "C01-J10.08", "J10.1", "C01-J10.1", "C01-J10.2", "J10.8", "C01-J10.81", "C01-J10.82", "C01-J10.83", "C01-J10.89"},
    "Grip no identificada":  {"C01-J11.8",  "J11", "J11.0", "C01-J11", "C01-J11.0", "C01-J11.00", "C01-J11.08", "J11.1", "C01-J11.1", "C01-J11.2", "J11.8", "C01-J11.81", "C01-J11.82", "C01-J11.83", "C01-J11.89"},
    "ILI": {"B25.0", "B30.2", "J00", "J12", "J12.0", "J12.1", "J12.2", "J12.3", "J12.8", "J12.9", "J17.1", "J20.3", "J20.4", "J20.5", "J20.6", "J20.7", "J21.0", "J21.1", "P23.0", "U04", "U04.9", "C01-B25.0", "C01-B30.2", "C01-B33.4", "C01-B97.21", "C01-J00", "C01-J12", "C01-J12.0", "C01-J12.1", "C01-J12.2", "C01-J12.3", "C01-J12.8", "C01-J12.81", "C01-J12.89", "C01-J12.9", "C01-J20.3", "C01-J20.4", "C01-J20.5", "C01-J20.6", "C01-J20.7", "C01-J21.0", "C01-J21.1", "C01-P23.0"},
    "Adenovirus": {"B97.0", "C01-B97.0","B34.0", "C01-B34.0","J12.0", "C01-J12.0"},
    "Febre":  {"R50.9", "C01-R50.9"},
    "VRS GAVINA": {"J21.1", "C01-J21.1", "B97.4", "J21.0", "C01-B97.4", "C01-J21.0", "C01-Z29.11", "Z29.11", "J20.5", "C01-J20.5"},
    "IRA GAVINA": {"J00", "C01-J00", "J02", "C01-J02", "J02.8", "C01-J02.8", "J02.9", "C01-J02.9", "J20", "C01-J20", "J20.6", "C01-J20.6", "J21", "C01-J21", "J21.9", "C01-J21.9", "J22", "C01-J22"},
    "Pneumònia":  {"C01-J12.89", "C01-J18.1", "C01-J18.9", "C01-J15.9", "C01-J15.211", "C01-J15.6", "C01-J16.8", "C01-J18.8", "C01-J12.2", "C01-J15.7", "C01-J10.00", "C01-J12.81", "C01-J15.4", "C01-J15.29", "C01-J11.00", "C01-J10.08", "C01-J13", "C01-J12.3", "C01-J18.0", "C01-J14", "C01-J18.2", "C01-J15.8", "C01-J15.1", "C01-J12.1", "C01-J12.0", "C01-J12.9"},
    }
    

class Diagnosticat(object):
    """."""

    def __init__(self):
        """."""
        #self.get_codis()
        #self.diagnostics = c.Counter()
        self.up_centres = []
        for self.sector in u.sectors:
            print self.sector
            self.get_centres()
            #self.get_assignada()
            #self.get_refredats()
        #self.export_data()
        
        
    def get_codis(self):
        """."""
        u.printTime("codis")
        
        self.codis = {}      
        for ps, (codisN) in codis_ps.items():
            for codi in codisN:
                self.codis[codi] = True
        self.codis_select = tuple(set([codi for codi, t in self.codis.items()]))
        print self.codis_select
        
    def get_centres(self):
        """."""
        u.printTime("centres")

        sql =  """ select amb.amb_codi_amb,amb.amb_desc_amb,sap.dap_codi_dap, sap.dap_desc_dap,
                cent_codi_centre, cent_classe_centre, cent_nom_centre,to_char(to_date(cent_data_baixa, 'J'), 'YYYY-MM-DD') ,  
                cent_codi_up, UPB.up_codi_up_ics, up_desc_up_ics, e_p_cod_ep, abs_codi_abs
                from pritb010 ups 
                left join
                gcctb008 UPB
                on ups.cent_codi_up=UPB.up_codi_up_scs
                left join
                gcctb007 br
                on UPB.up_codi_up_ics=br.up_codi_up_ics
                left join
                gcctb006   sap
                on
                br.dap_codi_dap=sap.DAP_CODI_DAP 
                left join 
                gcctb005 amb 
                on sap.amb_codi_amb=amb.amb_codi_amb
                left join
                rittb001 abs
                on ups.cent_codi_up=abs.abs_codi_up"""
        for ambc, ambd,sapc,sapd,codi, classe, nom, dat, up, br, nom2, ep, abs in u.getAll(sql, self.sector):
            if dat == '4712-01-01':
                dat = None
            self.up_centres.append([self.sector, ambc, ambd,sapc,sapd,codi, classe, nom, dat, up, br, nom2, ep, abs])
            
        file = u.tempFolder + "cat_centres.txt"
        u.writeCSV(file, self.up_centres, sep='|')
    
    def get_assignada(self):
        """."""
        u.printTime("assignada")
        
        self.pob = {}
        sql = "select usua_cip, usua_sexe, to_date(usua_data_naixement, 'J') from usutb040"
        for id, sexe, naix in u.getAll(sql, self.sector):
            self.pob[id] = {'sexe': sexe, 'naix': naix}
        
    
    def get_refredats(self):
        """."""
        u.printTime("problemes")
                
        sql = "select cip_usuari_cip, to_date(pr_dde), pr_cod_ps, pr_v_cen, pr_v_cla  \
           from prstb015, usutb011 \
            where pr_cod_u=cip_cip_anterior and pr_cod_o_ps = 'C' and \
                 pr_cod_ps in {} and \
                 to_char(pr_dde, 'YYYYMM') >'200912' and \
                 to_char(pr_dde, 'YYYYMM') <'202001' and \
                 pr_data_baixa is null \
                 group by cip_usuari_cip, pr_dde, pr_cod_ps,  pr_v_cen, pr_v_cla ".format(self.codis_select)
        for id,  dde, ps, centre, classe in u.getAll(sql, self.sector):
            sexe = self.pob[id]['sexe'] if id in self.pob else None
            naix = self.pob[id]['naix'] if id in self.pob else None
            try:
                ed = u.yearsBetween(naix, dde)
            except:
                ed = None   
            self.diagnostics[(self.sector, dde, centre, classe, ed, sexe, ps)] += 1
    
    def export_data(self):
        """."""    
        u.printTime("export")
        
        upload = []
        for (sector, dde, centre, classe, ed, sexe, ps), rec in self.diagnostics.items():
            upload.append([sector, dde, centre, classe, ed, sexe, ps, rec])
        file = u.tempFolder + "gavina_ps.txt"
        u.writeCSV(file, upload, sep='@')
        
        file = u.tempFolder + "cat_centres.txt"
        u.writeCSV(file, self.up_centres, sep='|')
    
if __name__ == "__main__":
    try:
        Diagnosticat()
    except Exception as e:
        print e