# -*- coding: utf8 -*-

"""
Busquem els TAR fets a les UP
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import hashlib as h

import sisapUtils as u
from random import shuffle


db = 'permanent'
class sistema_ili(object):
    """."""

    def __init__(self):
        """."""
        self.get_dbs()
        #self.get_rca()
        #self.get_tar()
        self.get_tar_simpt()
        self.export_files()
        
    def get_dbs(self):
        """obtenir pacients del dbs per up ecap"""
        u.printTime("Obtenim dades DBS")
        self.dbs = {}
        sql = """SELECT 
                        c_cip, c_up, c_up_origen,c_up_abs, c_metge, c_data_naix, c_sexe 
                FROM 
                    dwsisap.dbs"""
        for hash, up, up_o, abs, uba, naix, sexe in u.getAll(sql, 'exadata'):
            self.dbs[(hash)] = {'up': up, 'up_o': up_o, 'abs': abs, 'uba': uba, 'naix': naix, 'sexe':sexe}
            
    def get_rca(self):
        """obtenir pacients rca"""
        u.printTime("Rca")
        self.rca = {}
        sql = """SELECT 
                    hash, UP_RESIDENCIA 
                FROM 
                    dwsisap.RCA_CIP_NIA """
        for hash, up_res in u.getAll(sql, 'exadata'):
            self.rca[(hash)]= up_res
    
    def get_tar(self):
        """obtenim TAR dels pacients (IA ecap)"""
        u.printTime("Obtenim dades TAR")
        self.upload = []
        sql = """SELECT 
                    hash, DATA, resultat_cod, estat_cod, estat_des 
                FROM 
                    dwsisap.SISAP_CORONAVIRUS_PROVES 
                WHERE 
                    prova LIKE '%Antigen%' AND origen='IA'"""
        for hash, data, resul, estat, estat_des in u.getAll(sql, 'exadata'):
            up = self.dbs[(hash)]['up'] if (hash) in self.dbs else None
            uba = self.dbs[(hash)]['uba'] if (hash) in self.dbs else None
            naix = self.dbs[(hash)]['naix'] if (hash) in self.dbs else None
            sexe = self.dbs[(hash)]['sexe'] if (hash) in self.dbs else None
            self.upload.append([hash, up, uba, naix, sexe, data, resul, estat, estat_des])
    
    def get_tar_simpt(self):
        """obtenim TAR simptomatics"""
        u.printTime("Obtenim dades TAR")
        self.upload = []
        sql = """SELECT 
                        hash, alta, to_char(alta, 'hh24:MM:SS'),DATA, up, tipus, resultat, motiu, localitzacio, entorn, simptomes,INICI_SIMPTOMES 
                FROM 
                    dwsisap.SISAP_CORONAVIRUS_ANTIGEN"""
        for hash, alta, hora, data, up, tipus, resultat, motiu, loc, entorn, simptomes, inici  in u.getAll(sql, 'exadata'):
            up = self.dbs[(hash)]['up'] if (hash) in self.dbs else None
            uba = self.dbs[(hash)]['uba'] if (hash) in self.dbs else None
            naix = self.dbs[(hash)]['naix'] if (hash) in self.dbs else None
            sexe = self.dbs[(hash)]['sexe'] if (hash) in self.dbs else None
            self.upload.append([hash, up, uba, naix, sexe, alta, hora, data, up, tipus, resultat, motiu, loc, entorn, simptomes, inici])
            
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_sentinelles_tar"

        cols = ("hash varchar(40)",  "up_ecap varchar(10)", "uba varchar(10)",
                "naix date", "sexe varchar(1)", "data_tar date", "resultat_cod int", "estat_cod int", "estat_des varchar(10)")
        #u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        #u.listToTable(self.upload, tb, db)
        
        u.printTime("export")
        tb = "sisap_sentinelles_tar_simpt"

        cols = ("hash varchar(40)",  "up_ecap varchar(10)", "uba varchar(10)",
                "naix date", "sexe varchar(1)", "alta datetime", "hora varchar(20)", "data_tar date", "up_tar varchar(10)", "tipus varchar(50)",
                    "resultat varchar(50)", "motiu varchar(100)", "localitzacio varchar(100)", "entorn varchar(100)",
                    "simptomes varchar(10)", "inici_simptomes date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
            
if __name__ == '__main__':
    u.printTime("Inici")
     
    sistema_ili()
    
    u.printTime("Final") 