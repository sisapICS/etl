# -*- coding: utf8 -*-

"""
Gavina, prediccio grip
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

class Diagnosticat(object):
    """."""

    def __init__(self):
        """."""
        self.get_assignada()
        self.get_visites()
        self.export_data()        
       
    def get_assignada(self):
        """."""
        u.printTime("assignada")
        
        self.pob = {}
        sql = "select id_cip_sec, usua_data_naixement, usua_sexe from assignada"
        for id, naix, sexe in u.getAll(sql, 'import'):
            self.pob[id] = {'sexe': sexe, 'naix': naix}
    
    def get_visites(self):
        """."""
        u.printTime("visites")
        self.visites_fetes = c.Counter()
        for partition in u.getTablePartitions('visites', 'import'):
            sql = "select year(visi_data_visita) from {} WHERE visi_situacio_visita = 'R' and visi_data_baixa=0 limit 1".format(partition)
            for anys, in u.getAll(sql, 'import'):
                if str(anys) == '2012':
                    print partition
                    sql = """select 
                                id_cip_sec, codi_sector, visi_data_visita, s_espe_codi_especialitat, visi_lloc_visita,visi_up, visi_centre_codi_centre, visi_centre_classe_centre, visi_servei_codi_servei, visi_tipus_visita
                            from 
                                {}
                            where 
                                visi_situacio_visita = 'R' and visi_data_baixa=0""".format(partition)
                    for id, sector, dat, espe, lloc, up, centre, classe, servei, tipus in u.getAll(sql, 'import'):
                        sexe = self.pob[id]['sexe'] if id in self.pob else None
                        naix = self.pob[id]['naix'] if id in self.pob else None
                        try:
                            ed = u.yearsBetween(naix, dat)
                        except:
                            ed = None  
                        self.visites_fetes[(sector, sexe, ed, dat, espe, lloc, up, centre, classe, servei, tipus)] += 1
    
    def export_data(self):
        """."""    
        u.printTime("export")
        upload = []
        for (sector, sexe, ed, dat, espe, lloc, up, centre, classe, servei, tipus), rec in self.visites_fetes.items():
            upload.append([sector, sexe, ed, dat, espe, lloc, up, centre, classe, servei, tipus, rec])
        file = u.tempFolder + "Gavina_visites_2012.txt"
        u.writeCSV(file, upload, sep='|')
    
if __name__ == "__main__":
    try:
        Diagnosticat()
    except Exception as e:
        print e