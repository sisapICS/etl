
# -*- coding: utf8 -*-

"""
ILIs per analisi sentinella
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import hashlib as h

import sisapUtils as u
from random import shuffle

upload = []

sql="""SELECT up, es_ecap, grup, sexe, DATA,
        sum(dx_covid) dx_covid, sum(dx_ili) dx_ili, sum(dx_grip) dx_grip, sum(pneumonia) dx_pneumonia FROM dwsisap.DADESCOVID_MASTER dm 
        GROUP BY 
        up, es_ecap, grup, sexe, DATA"""
for up, ecap, grup, sexe, dat, covid, ili, grip, pneumo in u.getAll(sql, 'exadata'):
    upload.append([up, ecap, grup, sexe, dat, covid, ili, grip, pneumo])
    
db = "permanent"
tb = "ili_sentinella"

cols = ("up varchar(10)", "ecap int", "grup varchar(10)", "sexe varchar(10)",
"data date", "dx_covid int", "dx_ili int", "dx_grip int", "dx_pneumonia int")
u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
u.listToTable(upload, tb, db)
    