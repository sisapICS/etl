# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import csv,os,sys
from datetime import datetime, timedelta

import sisapUtils as u

db = 'redics'
tb = 'sisap_covid_dades_tar_dia'

path = u.tempFolder + "dades_TAR/" 


class upload_txt(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_files()
        self.export_data()
        self.dona_grants()  
    
    def get_files(self):
        """Agafem estimacions"""
        u.printTime("fitxers")
        self.upload = []
        n = 0
        for file1 in os.listdir(path):
            n += 1
            print file1
            tipus = file1[10:20]
            print tipus
            file = path + file1
            for data, aga, casos, tar, pob in u.readCSV(file, sep="@"):
                self.upload.append([str(tipus), data, str(aga), int(casos), int(tar), int(pob)])
    
    def export_data(self):
        """."""
        u.printTime("export")
        cols = ("data_extraccio varchar2(20)", "data varchar2(10)", "AGA varchar2(10)", "casos_TAR int", "TAR int", "poblacio int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)
               
    
    
if __name__ == '__main__':
    u.printTime("Inici")
    
    upload_txt()
    
    u.printTime("Final")