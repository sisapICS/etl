# -*- coding: latin1 -*-

"""
Multitests
"""

import collections as c
import psutil as p
import datetime as d
import difflib as s

import sisapUtils as u



class Multitests(object):
    """."""

    def __init__(self):
        """."""
        self.get_multitest()
        self.export()


    def get_multitest(self):
        """."""
        SIDICS_DB = ("sidics", "x0002")
        sql = """select 
                    xml_cip, xml_data_alta, xml_up, camp_codi, camp_valor 
                from 
                    sisap_xml
                where 
                    xml_origen = 'GABINETS' and 
                    xml_tipus_orig = 'XML0000124'"""
        tests = ("TR_Adenovirus", "TR_VRS", "TR_InflA", "TR_InflB", "TR_SarsCov2")
        self.multitests = {}

        for id, dat, up, codi, valor in u.getAll(sql, SIDICS_DB):
            if valor:
                if codi == "DATA_Test":
                    dtest = d.datetime.strptime(valor, "%d/%m/%Y")
                    self.multitests[(id,dat,up, 'dtest')] = dtest
                elif codi == "SexePacient":
                    self.multitests[(id,dat, up, 'sexe')] = valor
                elif codi == "DataNaixPacient":
                    dnaix = d.datetime.strptime(valor, "%d/%m/%Y")
                    self.multitests[(id,dat,up, 'naix')] = dnaix
                elif codi in tests:
                    self.multitests[(id,dat,up, codi)] = valor
                else:
                    ok = 1

        
    def export(self):
        """."""
        u.printTime("export")
        
        upload = []
        
        for (id,dat, up, codi), val in self.multitests.items():
            if codi == 'dtest':
                naix = self.multitests[(id,dat, up, 'naix')] if (id,dat, up,'naix') in self.multitests else None
                sexe = self.multitests[(id,dat, up,'sexe')] if (id,dat, up,'sexe') in self.multitests else None
                adeno = self.multitests[(id,dat,up, 'TR_Adenovirus')] if (id,dat, up,'TR_Adenovirus') in self.multitests else None
                vrs = self.multitests[(id,dat, up,'TR_VRS')] if (id,dat, up,'TR_VRS') in self.multitests else None
                gripA = self.multitests[(id,dat, up, 'TR_InflA')] if (id,dat, up,'TR_InflA') in self.multitests else None
                gripB = self.multitests[(id,dat, up,'TR_InflB')] if (id,dat, up,'TR_InflB') in self.multitests else None
                sars = self.multitests[(id,dat, up,'TR_SarsCov2')] if (id,dat, up,'TR_SarsCov2') in self.multitests else None
                upload.append([id, naix, sexe, up,val,  adeno, vrs, gripA, gripB, sars])
                
        db = "permanent"
        tb = "sivic_multitests"
        cols = ("hash varchar(40)", "naixement date", "sexe varchar(40)", "up varchar(10)", "data_test date", "adeno int", "vrs int", "gripA int", "gripB int", "sarscov2 int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(upload, tb, db)



if __name__ == '__main__':
    u.printTime("Inici")
    
    Multitests()

    u.printTime("Fi")
    