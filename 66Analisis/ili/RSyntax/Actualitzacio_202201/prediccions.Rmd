## Time series

* Per estimar, utilitzem les dades desde la temporada 2014-2015. 
* Excloem les temporades posteriors a la pandèmia de la covid.
* Utilitzem temporada 2018-2019 per validar l'estimació.

```{r}
data_minim <- dades_aggr_data[, min(data), Temporada][Temporada == "2014-2015"]$V1
# data_max <- dades_vrs_aggr_data[Temporada == "2017-2018" & day(data) == day(today()) & month(data) == month(today())]$data
data_max <- dades_aggr_data[, min(data), Temporada][Temporada == "2019-2020"]$V1
dades_ts <- dades_aggr_data[data >= data_minim & data < data_max]
dades_ts <- dades_ts[order(data)]
```

### 1 - Estimació de les ILI, excloent els dx de grip i covid.

```{r}
ili_ts <- ts(dades_ts[!is.na(DX_ILI_7), DX_ILI_7], frequency = 365)

ilifit <- tslm(ili_ts ~ trend + season)
# pred <- forecast(ilifit, newdata = dades_vrs_aggr_data[data >=as.Date("2020-01-01")], level = c(80, 90, 95))
pred <- forecast(ilifit, h = nrow(dades_aggr_data[Temporada %in% "2019-2020"]), level = c(80, 90, 95))
```

```{r}
dt_predict <- as.data.table(pred)
dt_predict[, data :=  seq(dades_aggr_data[Temporada %in% c("2019-2020")][, min(data)] , to = dades_aggr_data[Temporada %in% c("2019-2020")][, max(data)], 1)]
dt_predict_1 <- dt_predict
dt_predict_1[order(data), dia_de_la_temporada := sequence(.N)]
```

```{r}
taula <- dt_predict_1[, .SD, .SDcols = c("Point Forecast", "Lo 95", "Hi 95", "dia_de_la_temporada")]
taula[, EDAT_CODI := 999]
taula[, DESC_ENTITAT := "Global"]
```


```{r}
dades_aggr_data[order(data), dia_de_la_temporada := sequence(.N), Temporada]
```

```{r}
dg <- merge(dades_aggr_data[, .SD, .SDcols = c("data", "DX_GRIP_7", "DX_ILI_7", "DX_COVID_7", "dia_de_la_temporada", "Temporada")], 
            dt_predict_1[, .SD, .SDcols = c("dia_de_la_temporada", "Point Forecast", "Lo 95", "Hi 95")],
            by = "dia_de_la_temporada", all.x = T)
```


```{r}
g_1 <- ggplot(dg[Temporada %in% c("2019-2020", "2020-2021", "2021-2022")], aes(x = data)) + 
  geom_line(aes(y = DX_ILI_7+DX_GRIP_7+DX_COVID_7, color = "ILI + GRIP + COVID observats", linetype = "ILI + GRIP + COVID observats")) + 
  geom_line(aes(y = `Point Forecast`, color = "Estimació Dx ILI", linetype = "Estimació Dx ILI")) +
  geom_ribbon(aes(ymin = `Lo 95`, ymax = `Hi 95`, fill = "IC95% Estimació ILI"), alpha = .2) +
  scale_color_manual(values = c("ILI + GRIP + COVID observats" = "#81B29A", "Estimació Dx ILI" = "#3D405B"), limits = c(c("ILI + GRIP + COVID observats", "Estimació Dx ILI"))) +
  scale_linetype_manual(values = c("ILI + GRIP + COVID observats" = 1, "Estimació Dx ILI" = 1), limits = c(c("ILI + GRIP + COVID observats", "Estimació Dx ILI"))) +
  scale_fill_manual(values = c("IC95% Estimació ILI" = "#3D405B", "IC95% Mitjana de les temporades gripals històriques" = "#FFB703")) + 
  theme_minimal() + 
  theme(legend.position = "bottom",
        plot.caption = element_text(size = 8, colour = "grey35")) +
  guides(colour = guide_legend(ncol = 1),
         linetype = "none",
         fill = guide_legend(ncol = 1)) +
  labs(y = "Nombre de casos (7 dies)", x = "", color = "", linetype = "", fill = "", title = "Evolució diària de les IRA", caption = "Font: SISAP")
g_1
```

#### Per grups d'edat

```{r}
data_minim <- dades_aggr_data_edat[, min(data), Temporada][Temporada == "2014-2015"]$V1
data_max <- dades_aggr_data_edat[, min(data), Temporada][Temporada == "2019-2020"]$V1
dades_ts <- dades_aggr_data_edat[data >= data_minim & data < data_max][!is.na(EDAT_CODI)]
dades_ts <- dades_ts[order(data)]
```

```{r}
dt_predict_edat <- do.call("rbind", 
                           lapply(split(dades_ts, dades_ts$EDAT_CODI), estimar_ts, D = dades_aggr_data_edat))
```

```{r}
taula <- rbind(taula, 
               dt_predict_edat[, 
                               DESC_ENTITAT := "Global"][, .SD, .SDcols = c("Point Forecast", "Lo 95", "Hi 95", "dia_de_la_temporada", "EDAT_CODI", "DESC_ENTITAT")])
```

```{r}
dades_aggr_data_edat[order(data), dia_de_la_temporada := sequence(.N), c("Temporada", "EDAT_CODI")]
```

```{r}
dg <- merge(dades_aggr_data_edat[, .SD, .SDcols = c("data", "EDAT_CODI", "DX_GRIP_7", "DX_ILI_7", "DX_COVID_7", "dia_de_la_temporada", "Temporada")], 
            dt_predict_edat[, .SD, .SDcols = c("dia_de_la_temporada", "EDAT_CODI", "Point Forecast", "Lo 95", "Hi 95")],
            by = c("dia_de_la_temporada", "EDAT_CODI"), all.x = T)
```


```{r fig.height=10, fig.width=10}
g_1 <- ggplot(dg[Temporada %in% c("2019-2020", "2020-2021", "2021-2022") & !is.na(EDAT_CODI)], aes(x = data)) + 
  geom_line(aes(y = DX_ILI_7+DX_GRIP_7+DX_COVID_7, color = "ILI + GRIP + COVID observats", linetype = "ILI + GRIP + COVID observats")) + 
  geom_line(aes(y = `Point Forecast`, color = "Estimació Dx ILI", linetype = "Estimació Dx ILI")) +
  geom_ribbon(aes(ymin = `Lo 95`, ymax = `Hi 95`, fill = "IC95% Estimació ILI"), alpha = .2) +
  scale_color_manual(values = c("ILI + GRIP + COVID observats" = "#81B29A", "Estimació Dx ILI" = "#3D405B"), limits = c(c("ILI + GRIP + COVID observats", "Estimació Dx ILI"))) +
  scale_linetype_manual(values = c("ILI + GRIP + COVID observats" = 1, "Estimació Dx ILI" = 1), limits = c(c("ILI + GRIP + COVID observats", "Estimació Dx ILI"))) +
  scale_fill_manual(values = c("IC95% Estimació ILI" = "#3D405B", "IC95% Mitjana de les temporades gripals històriques" = "#FFB703")) + 
  theme_minimal() + 
  theme(legend.position = "bottom",
        plot.caption = element_text(size = 8, colour = "grey35")) +
  guides(colour = guide_legend(ncol = 1),
         linetype = "none",
         fill = guide_legend(ncol = 1)) +
  labs(y = "Nombre de casos (7 dies)", x = "", color = "", linetype = "", fill = "", title = "Evolució diària de les IRA", caption = "Font: SISAP") + facet_wrap(~ EDAT_CODI, scale = "free")
g_1
```

#### Per RS

```{r}
dades_aggr_data_rs[, EDAT_CODI := REGIO_DES]
data_minim <- dades_aggr_data_rs[, min(data), Temporada][Temporada == "2014-2015"]$V1
data_max <- dades_aggr_data_rs[, min(data), Temporada][Temporada == "2019-2020"]$V1
dades_ts <- dades_aggr_data_rs[data >= data_minim & data < data_max][!is.na(REGIO_DES)]
dades_ts <- dades_ts[order(data)]
```

```{r}
dt_predict_rs <- do.call("rbind", 
                           lapply(split(dades_ts, dades_ts$EDAT_CODI), estimar_ts, D = dades_aggr_data_rs))
```

```{r}
taula <- rbind(taula, 
               dt_predict_rs[, 
                               ":=" (DESC_ENTITAT = EDAT_CODI,
                                     EDAT_CODI = "999")][, .SD, .SDcols = c("Point Forecast", "Lo 95", "Hi 95", "dia_de_la_temporada", "EDAT_CODI", "DESC_ENTITAT")])
taula[, TIPUS_ENTITAT := "REGIO"]
taula[DESC_ENTITAT == "Global", TIPUS_ENTITAT := "GLOBAL"]
taula[, DIAGNOSTIC := "ILI_NO_GRIP_NO_COVID"]

taula <- taula[, c("TIPUS_ENTITAT", "DESC_ENTITAT", "EDAT_CODI", "DIAGNOSTIC", "dia_de_la_temporada", "Point Forecast", "Lo 95", "Hi 95")]
```

```{r}
dades_aggr_data_rs[order(data), dia_de_la_temporada := sequence(.N), c("Temporada", "EDAT_CODI")]
```

```{r}
dg <- merge(dades_aggr_data_rs[, .SD, .SDcols = c("data", "EDAT_CODI", "DX_GRIP_7", "DX_ILI_7", "DX_COVID_7", "dia_de_la_temporada", "Temporada")], 
            dt_predict_rs[, .SD, .SDcols = c("dia_de_la_temporada", "DESC_ENTITAT", "Point Forecast", "Lo 95", "Hi 95")],
            by.x = c("dia_de_la_temporada", "EDAT_CODI"), by.y = c("dia_de_la_temporada", "DESC_ENTITAT"), all.x = T)
```


```{r fig.height=10, fig.width=10}
g_1 <- ggplot(dg[Temporada %in% c("2019-2020", "2020-2021", "2021-2022") & !is.na(EDAT_CODI)], aes(x = data)) + 
  geom_line(aes(y = DX_ILI_7+DX_GRIP_7+DX_COVID_7, color = "ILI + GRIP + COVID observats", linetype = "ILI + GRIP + COVID observats")) + 
  geom_line(aes(y = `Point Forecast`, color = "Estimació Dx ILI", linetype = "Estimació Dx ILI")) +
  geom_ribbon(aes(ymin = `Lo 95`, ymax = `Hi 95`, fill = "IC95% Estimació ILI"), alpha = .2) +
  scale_color_manual(values = c("ILI + GRIP + COVID observats" = "#81B29A", "Estimació Dx ILI" = "#3D405B"), limits = c(c("ILI + GRIP + COVID observats", "Estimació Dx ILI"))) +
  scale_linetype_manual(values = c("ILI + GRIP + COVID observats" = 1, "Estimació Dx ILI" = 1), limits = c(c("ILI + GRIP + COVID observats", "Estimació Dx ILI"))) +
  scale_fill_manual(values = c("IC95% Estimació ILI" = "#3D405B", "IC95% Mitjana de les temporades gripals històriques" = "#FFB703")) + 
  theme_minimal() + 
  theme(legend.position = "bottom",
        plot.caption = element_text(size = 8, colour = "grey35")) +
  guides(colour = guide_legend(ncol = 1),
         linetype = "none",
         fill = guide_legend(ncol = 1)) +
  labs(y = "Nombre de casos (7 dies)", x = "", color = "", linetype = "", fill = "", title = "Evolució diària de les IRA", caption = "Font: SISAP") + facet_wrap(~ EDAT_CODI, scale = "free")
g_1
```