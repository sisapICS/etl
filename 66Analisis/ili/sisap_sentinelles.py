# -*- coding: utf8 -*-

"""
Obtenir dades per veure si podem seleccionar eaps per sentinella ILI/covid
"""

import collections as c
import datetime as d
import sys
from datetime import datetime
import hashlib as h

import sisapUtils as u
from random import shuffle


db = 'permanent'
class sistema_ili(object):
    """."""

    def __init__(self):
        """."""
        self.get_pacients()
        self.export_files()
        
    def get_pacients(self):
        """obtenir pacients amb data de covid"""
        u.printTime("Obtenim els casos")
        self.upload = []
        sql = """SELECT 
                    HASH, es_rca, situacio, DATA_NAIXEMENT, EDAT, SEXE, RCA_UP, RCA_ABS, es_resident, CAS_DATA 
                FROM 
                    dwsisap.DBC_VAcuna"""
        for hash, rca, sit, naix, edat,sexe, up, abs, resdient, cas in u.getAll(sql, 'exadata'):
            self.upload.append([hash, rca, sit, naix, edat,sexe, up, abs, resdient, cas])
            
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "sisap_sentinelles"

        cols = ("hash varchar(40)", "rca int", "situacio varchar(10)", 
				"naix_rca date", "edat int", "sexe varchar(1)", "rca_up varchar(10)", "rca_abs varchar(10)","es_resident int","cas_data date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)
            
if __name__ == '__main__':
    u.printTime("Inici")
     
    sistema_ili()
    
    u.printTime("Final") 