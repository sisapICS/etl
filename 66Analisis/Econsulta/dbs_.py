# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u

db = 'permanent'
tb19 = 'econsulta_dx_2019'
tb20 = 'econsulta_dx_2020'

dx_file =  "dx_covid.txt"


class dx_econs(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_dbs()
        self.get_dbs_2019()
   
    def get_dbs(self):
        """Obtenim dades de dbs"""
        u.printTime("DBS")
        sql = """select c_cip,
                    c_sector,
                    PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA
                from dbs"""
        upload = []
        for (cip,
             sector,
             hta, dm1, dm2, mpoc,
             asma, bc, ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi
             ) in u.getAll(sql, "redics"):
            idh = cip + ':' + sector
            upload.append([idh,hta, dm1, dm2, mpoc,asma, bc, ci,mcv, ic, acfa, valv,hepat, vhb, vhc, neo, mrc,obes, vih, sida,demencia, artrosi])
    
 
        columns = ["id varchar(100)", "hta date", "dm1 date", "dm2 date", "mpoc date", "asma date",  "bc date",
        "ci date","mcv date", "ic date", "acfa date", "valv date","hepat date", "vhb date", "vhc date", "neo date", "mrc date","obes date", "vih date"
        , "sida date","demencia date", "artrosi date"]
        u.createTable(tb20, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb20, db)
    
    def get_dbs_2019(self):
        """Obtenim dades de dbs"""
        u.printTime("DBS2019")
        SIDICS_DB = ("dbs", "x0002")
        
        sql = """select c_cip,
                    c_sector,
                    PS_HTA_DATA, PS_DIABETIS1_DATA, PS_DIABETIS2_DATA, PS_MPOC_ENFISEMA_DATA,
                    PS_ASMA_DATA, PS_BRONQUITIS_CRONICA_DATA, PS_CARDIOPATIA_ISQUEMICA_DATA,
                    PS_ACV_MCV_DATA, PS_INSUF_CARDIACA_DATA, PS_AC_FA_DATA, PS_VALVULOPATIA_DATA,
                    PS_HEPATOPATIA_DATA, PS_VHB_DATA, PS_VHC_DATA, PS_NEOPLASIA_M_DATA, PS_RENAL_CRO_DATA,
                    PS_OBESITAT_DATA, PS_VIH_DATA, PS_SIDA_DATA,
                    PS_DEMENCIA_DATA, PS_ARTROSI_DATA
                from dbs_2019"""
        upload = []
        for (cip,
             sector,
             hta, dm1, dm2, mpoc,
             asma, bc, ci,
             mcv, ic, acfa, valv,
             hepat, vhb, vhc, neo, mrc,
             obes, vih, sida,
             demencia, artrosi
             ) in u.getAll(sql, SIDICS_DB):
            idh = cip + ':' + sector
            upload.append([idh,hta, dm1, dm2, mpoc,asma, bc, ci,mcv, ic, acfa, valv,hepat, vhb, vhc, neo, mrc,obes, vih, sida,demencia, artrosi])
    
 
        columns = ["id varchar(100)", "hta date", "dm1 date", "dm2 date", "mpoc date", "asma date",  "bc date",
        "ci date","mcv date", "ic date", "acfa date", "valv date","hepat date", "vhb date", "vhc date", "neo date", "mrc date","obes date", "vih date"
        , "sida date","demencia date", "artrosi date"]
        u.createTable(tb19, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb19, db)
        
       
if __name__ == '__main__':
    u.printTime("Inici")
    
    dx_econs()
    
    u.printTime("Final")