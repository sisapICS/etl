---
title: 'Anàlisi eConsulta: Model'
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)

library(data.table)
library(knitr)
library(kableExtra)
library(ggplot2)
library(hrbrthemes)
library(dplyr)

load("../Dades/assignada_bivar_inici.RData")
load("../Dades/assignada_bivar_reben.RData")
load("../Dades/assignada_bivar_pre.RData")
load("../Dades/assignada_bivar_post.RData")


```

# El model

```{r}
vars <- c("edat", "edat_f", "sexe_f", "immigrant_renta_baixa", "gma_cmplx_num", "gma_pniv_f", "pcc_f", "maca_f", "nivell_cobertura_f", "medea_cat", "ruralitat", "Nombre de consultes", "Mitjana de missatges")

coeficients <- do.call("rbind", lapply(c("assignada_bivar_pre", "assignada_bivar_post"), function(x){
  # print(x)
  lapply(c("Han iniciat alguna conversa", "Només reben"), function(xx){
    # print(xx)
    
    dm <- get(x)[perfil %in% c(xx, "No utilitzador"), .SD, .SDcols = c("perfil", vars)]
    dm[, resposta := ifelse(perfil == xx, 1, 0)]
    selected_model <- glm(resposta ~ edat + sexe_f + immigrant_renta_baixa + gma_cmplx_num + nivell_cobertura_f + medea_cat, data = dm, family = "binomial")
    # selected_model <- step(model, direction = "backward", trace = F)
    coeficients <- data.table(
      # cbind(
      selected_model$coefficients
                                    # , confint(selected_model))
      )
    pvalues <- data.table(coef(summary(selected_model))[,"Pr(>|z|)"])
    t <- cbind(coeficients = coeficients, pvalues = pvalues)
    t[, tot_junt := linebreak(paste0(format(round(coeficients.V1, 4), decimal.mark = ","), "<br>(", format(round(pvalues.V1, 4), decimal.mark = ","), ")"))]
    t[, Variables := names(selected_model$coefficients)]
    t[, Periode := x]
    t[, Perfil := xx]
    t <- t[, c("Perfil", "Periode", "Variables", "tot_junt")]
  })
}))
```

```{r message=FALSE, warning=FALSE}
taula_coef <- do.call("rbind", list(coeficients[[1]], coeficients[[2]], coeficients[[3]], coeficients[[4]]))
t <- dcast(taula_coef, Perfil + Variables ~ Periode)
```


```{r}
t %>% 
  kable(digits = 2, row.names = F, escape = F, caption = "Taula: Coeficients dels models logístics", align = c("l", "l", "c", "c"), format.args = list(decimal.mark = ',', big.mark = ".")) %>%
  kable_styling(bootstrap_options = c("hover", "condensed", "responsive"), full_width = T, position = "center") %>%
  row_spec(0, bold = T, align = "c", background = "#337ab7", color = "white", font_size = 12) %>%
  column_spec(1:2, bold = F, border_right = F, background = "#E7F0F7") %>%
  column_spec(1:4, width = "5cm") %>%
  collapse_rows(columns = c(1))

  
```

