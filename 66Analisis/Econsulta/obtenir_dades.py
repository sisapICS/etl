# -*- coding: utf8 -*-

"""
Peticio econsulta pel sidiap
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

db = "permanent"

class EConsultaPac(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_centres()
        self.get_gma()
        self.get_medea()
        self.get_hashos()
        self.get_assignats()
        self.get_econsulta()
        self.get_LMS()
        self.get_visites()
        self.get_its()
        
    def get_centres(self):
        """."""
        upload = []
        sql = "select scs_codi, ics_codi, ics_desc, ep, medea from cat_centres"
        for up, br, desc, ep, medea in u.getAll(sql, 'nodrizas'):
            upload.append([up, br, desc, ep, medea])
        
    
    def get_gma(self):
        """."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron, gma_pniv \
                from gma", "import")
        self.gma = {(id): (cod, cmplx, num, pniv) for (id, cod, cmplx, num, pniv) 
                    in  u.getAll(*sql)}
    
    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        sql = ("select sector, valor \
                from sisap_medea", "redics")
        valors = {sector: str(valor) for (sector, valor)
                  in u.getAll(*sql)}
        self.Imedea = {}                  
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if str(sector) in valors:
                self.Imedea[(id)] = valors[sector] 
    
    def get_hashos(self):
        """."""
        self.idcip_to_hash = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        for id, sec, hash in u.getAll(sql, "import"):
            self.idcip_to_hash[id] = {'hash': hash, 'sec': sec}     
    
    def get_assignats(self):
        """Assignada"""
        u.printTime("Assignada")
        upload = []
        self.assignats = {}
        sql = "select id_cip_sec, up, edat, sexe, nacionalitat, pcc, maca, nivell_cobertura, lms_peticio, lms_connexio from assignada_tot"
        for id, up, edat, sexe, nac, pcc, maca, ncob, lms_pet, lms_con in u.getAll(sql, 'nodrizas'):
            gmac, gmax, medea = None, None, None
            if (id) in self.Imedea:
                medea = self.Imedea[(id)]
            if (id) in self.gma:
                gmac = self.gma[(id)][0]
                gmax = self.gma[(id)][1]
                gmap = self.gma[(id)][3]
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            upload.append([idh, up, edat, sexe, nac, medea, gmac, gmax, gmap, pcc, maca, ncob, lms_pet, lms_con])
            self.assignats[id] = True
        
        
        tb = "econsulta_assignada_2020"
        columns = ["id varchar(100)", "up varchar(5)", "edat int", "sexe varchar(5)", "nac varchar(10)", "medea varchar(10)",
                    "gma_codi varchar(10)", "gma_cmplx varchar(10)", "gma_pniv int","pcc int", "maca int", "nivell_cobertura varchar(10)",
                     "lms_pet date", "lms_con date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb, db)
                
    def get_econsulta(self):  
        u.printTime("econsulta")
        converses = []
        sql = "select id_cip_sec, codi_sector, conv_id, conv_autor, \
                      conv_desti, conv_estat, conv_dini \
               from econsulta_conv"
        for id, sector, conv_id, conv_autor, conv_desti, conv_estat, conv_dini in u.getAll(sql, 'import'): 
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            converses.append([idh, sector, conv_id, conv_autor, conv_desti, conv_estat, conv_dini])
            
        tb = "econsulta_converses"
        columns = ["id varchar(100)", "sector varchar(5)", "id_conversa varchar(20)", "conv_autor varchar(20)", "conv_desti varchar(20)",
                    "conv_estat varchar(10)", "cond_dini date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(converses, tb, db)
        
        missatges = []
        sql = "select codi_sector, msg_conv_id, msg_id, msg_autor, \
                      msg_desti, msg_data, date_format(msg_data,'%Y%m') \
               from econsulta_msg"
        for sector, id_c, id, autor, desti, data, period in u.getAll(sql, 'import'):
            missatges.append([sector, id_c, id, autor, desti, data])
            
        tb = "econsulta_missatges"
        columns = ["sector varchar(5)", "id_conversa varchar(20)", "id_missatge varchar(20)", "autor varchar(20)", "desti varchar(20)","data date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(missatges, tb, db)    
        
    def get_LMS(self):
        """Ve de un fitxer local que ens van passar al novembre 2019"""
        u.printTime("LMS")
        upload = []
        nies = {}
        sql = ("select id_cip_sec, usua_nia from crg", "import")
        for id, nia in u.getAll(*sql):
            nia = str(nia)
            nies[nia] = id
        for (nia, pes, cid) in u.readCSV(LMS_file, sep=';'):
            if str(nia) in nies:
                id = nies[nia]
                if id in self.assignats:
                    upload.append([id, pes, cid])   
        
    def get_visites(self):
        """."""
        u.printTime("Visites")
        visites = []
        for table in u.getSubTables('visites'):
            dat, = u.getOne('select  extract(year_month from visi_data_visita) from {} limit 1'.format(table), 'import')
            if dat >= 201806:
                print table
                sql = "select  codi_sector, id_cip_sec,  if(visi_etiqueta='ECTA', '9Ec', if(visi_etiqueta='VITA','9Ev', visi_tipus_visita)), \
                visi_data_visita,  visi_tipus_citacio, visi_forcada_s_n, visi_etiqueta, visi_internet, visi_lloc_visita,\
                visi_dia_peticio \
               from {}, nodrizas.dextraccio where visi_data_baixa = 0 and visi_situacio_visita ='R' and visi_servei_codi_servei='MG'".format(table)
                for sector, id, tipus, periode, citacio, forcada, etiqueta, internet, lloc, dia_pet in u.getAll(sql, 'import'):
                    hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
                    idh = hash + ':' + sec
                    visites.append([sector, idh, tipus, periode, citacio, forcada, etiqueta, internet, lloc, dia_pet]) 
        tb = "econsulta_visites_MG"
        columns = ["sector varchar(5)", "id varchar(100)", "tipus varchar(10)", "data date",  "citacio varchar(10)", "forcada varchar(5)",
                    "etiqueta varchar(100)", "internet varchar(30)", "lloc varchar(10)", "peticio date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(visites, tb, db)  
        
    
    def get_its(self):
        """Its de la taula import baixes"""
        u.printTime("Baixes")
        its = []
        sql = "select id_cip_sec, ilt_data_baixa, ilt_data_alta, year(ilt_data_baixa) from baixes"
        for id, baixa, alta, anys in u.getAll(sql, 'import'):
            if anys >= 2018:
                if id in self.assignats:
                    hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
                    idh = hash + ':' + sec
                    its.append([idh, baixa, alta])
        tb = "econsulta_it"
        columns = ["id varchar(100)", "data_baixa date", "data_alta date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(its, tb, db)  
           
if __name__ == '__main__':
    u.printTime("Inici")
    
    EConsultaPac()
    
    u.printTime("Final")
    