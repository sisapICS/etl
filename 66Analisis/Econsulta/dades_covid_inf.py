# -*- coding: utf8 -*-

"""
Peticio econsulta manolo: veure si persones q usen econsulta tenien visites abans
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

db = "permanent"

class EConsultaPac(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_centres()
        self.get_gma()
        self.get_medea()
        self.get_hashos()
        self.get_assignats()
        self.get_visites()
        
    def get_centres(self):
        """."""
        upload = []
        sql = "select scs_codi, ics_codi, ics_desc, ep, medea from cat_centres"
        for up, br, desc, ep, medea in u.getAll(sql, 'nodrizas'):
            upload.append([up, br, desc, ep, medea])
        
    
    def get_gma(self):
        """."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron, gma_pniv \
                from gma", "import")
        self.gma = {(id): (cod, cmplx, num, pniv) for (id, cod, cmplx, num, pniv) 
                    in  u.getAll(*sql)}
    
    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        sql = ("select sector, valor \
                from sisap_medea", "redics")
        valors = {sector: str(valor) for (sector, valor)
                  in u.getAll(*sql)}
        self.Imedea = {}                  
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if str(sector) in valors:
                self.Imedea[(id)] = valors[sector] 
    
    def get_hashos(self):
        """."""
        self.idcip_to_hash = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        for id, sec, hash in u.getAll(sql, "import"):
            self.idcip_to_hash[id] = {'hash': hash, 'sec': sec}     
    
    def get_assignats(self):
        """Assignada"""
        u.printTime("Assignada")
        upload = []
        self.assignats = {}
        sql = "select id_cip_sec, up, edat, sexe, nacionalitat, pcc, maca, nivell_cobertura, lms_peticio, lms_connexio from assignada_tot"
        for id, up, edat, sexe, nac, pcc, maca, ncob, lms_pet, lms_con in u.getAll(sql, 'nodrizas'):
            gmac, gmax, medea = None, None, None
            if (id) in self.Imedea:
                medea = self.Imedea[(id)]
            if (id) in self.gma:
                gmac = self.gma[(id)][0]
                gmax = self.gma[(id)][1]
                gmap = self.gma[(id)][3]
            hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
            idh = hash + ':' + sec
            upload.append([idh, up, edat, sexe, nac, medea, gmac, gmax, gmap, pcc, maca, ncob, lms_pet, lms_con])
            self.assignats[id] = True
        
        
        tb = "econsulta_covid_ass"
        columns = ["id varchar(100)", "up varchar(5)", "edat int", "sexe varchar(5)", "nac varchar(10)", "medea varchar(10)",
                    "gma_codi varchar(10)", "gma_cmplx varchar(10)", "gma_pniv int","pcc int", "maca int", "nivell_cobertura varchar(10)",
                     "lms_pet date", "lms_con date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb, db)
               
        
    def get_visites(self):
        """."""
        u.printTime("Visites")
        visites = []
        for table in u.getSubTables('visites'):
            dat, = u.getOne('select  extract(year_month from visi_data_visita) from {} limit 1'.format(table), 'import')
            if dat > 201712:
                print table
                sql = "select  codi_sector, id_cip_sec,  if(visi_etiqueta='ECTA', '9Ec', if(visi_etiqueta='VITA','9Ev', visi_tipus_visita)), \
                visi_data_visita,  visi_tipus_citacio, visi_forcada_s_n, visi_etiqueta, visi_internet, visi_lloc_visita,\
                visi_dia_peticio \
               from {}, nodrizas.dextraccio where visi_data_baixa = 0 and visi_situacio_visita ='R' and visi_servei_codi_servei in ('INF', 'ENF', 'INFG', 'INFP','INF','INFGR', 'GCAS')".format(table)
                for sector, id, tipus, periode, citacio, forcada, etiqueta, internet, lloc, dia_pet in u.getAll(sql, 'import'):
                    hash, sec = self.idcip_to_hash[id]['hash'],self.idcip_to_hash[id]['sec']
                    idh = hash + ':' + sec
                    visites.append([sector, idh, tipus, periode, citacio, forcada, etiqueta, internet, lloc, dia_pet]) 
        tb = "econsulta_covid_visitesINF"
        columns = ["sector varchar(5)", "id varchar(100)", "tipus varchar(10)", "data date",  "citacio varchar(10)", "forcada varchar(5)",
                    "etiqueta varchar(100)", "internet varchar(30)", "lloc varchar(10)", "peticio date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(visites, tb, db)  
        
    
           
if __name__ == '__main__':
    u.printTime("Inici")
    
    EConsultaPac()
    
    u.printTime("Final")
    