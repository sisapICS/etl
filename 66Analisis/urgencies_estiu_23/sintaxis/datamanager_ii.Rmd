
<<<<<<< HEAD
```{r eval=FALSE, include=FALSE}
urgencies <- fread("../dades/dades/urgencies.txt", sep = "@", header = T)
urgencies[, data := as.Date(data)]
=======
```{r}
urgencies <- fread("../dades/dades/urgencies.txt", sep = "@", header = T)
>>>>>>> 84cd213bc63fb61028806c32e339b9395a628f23
```



<<<<<<< HEAD

```{r eval=FALSE, include=FALSE}
temperatures <- fread("../dades/dades/temperatures.txt", sep = "@", header = T)
temperatures[, data := as.Date(data)]
cat_estacio <- fread("../dades/dades/cat_estacio.txt", sep = "@", header = T)
```

```{r}
=======

```{r}
temperatures <- fread("../dades/dades/temperatures.txt", sep = "@", header = T)
cat_estacio <- fread("../dades/dades/cat_estacio.txt", sep = "@", header = T)
```

```{r}
>>>>>>> 84cd213bc63fb61028806c32e339b9395a628f23
temperatures <- merge(temperatures, cat_estacio, by.x = "ESTACIO", by.y = "CODI_ESTACIO", all.x = T)
temperatures <- temperatures[ALTITUD<1500]

temperatures_aggr <- rbind(
  temperatures[, .(
    REGIO = "GLOBAL",
    MINIMA = mean(MINIMA, na.rm = T),
    MAXIMA = mean(MAXIMA, na.rm = T),
    MITJANA = mean(MITJANA, na.rm = T)
  ), by = .(data)],
  temperatures[!is.na(REGIO), .(
    MINIMA = mean(MINIMA, na.rm = T),
    MAXIMA = mean(MAXIMA, na.rm = T),
    MITJANA = mean(MITJANA, na.rm = T)
  ), by = .(data, REGIO)]
  )


all_dates <- data.table(
  expand.grid(data = seq(as.Date("2023-05-01"), as.Date("2023-09-04"), by = "1 day"),
              REGIO = unique(temperatures_aggr$REGIO))
)

temperatures_aggr_all_dates <- merge(temperatures_aggr, all_dates, by = c("data", "REGIO"), all.y = T)
<<<<<<< HEAD


=======


>>>>>>> 84cd213bc63fb61028806c32e339b9395a628f23
temperatures_aggr_all_dates[order(data), mitjana_7 := Reduce(`+`, shift(MITJANA, 0:6))/7, by = .(REGIO)]
temperatures_aggr_all_dates[order(data), maxim_7 := Reduce(`+`, shift(MAXIMA, 0:6))/7, by = .(REGIO)]
temperatures_aggr_all_dates[order(data), minim_7 := Reduce(`+`, shift(MINIMA, 0:6))/7, by = .(REGIO)]
```

```{r}
dades <- merge(urgencies, temperatures_aggr_all_dates, by = c("data", "REGIO"), all.x = T)
dades[, estiu := ifelse(month(data) %in% 6:8, 1, 0)]
dades[, Any := year(data)]
dades[, mes_any := paste0(year(data), str_pad(month(data), width = 2, pad = "0"), str_pad(day(data), width = 2, pad = "0"))]
dades[, mes_dia := paste0(str_pad(month(data), width = 2, pad = "0"), str_pad(day(data), width = 2, pad = "0"))]
dades[, EDAT := factor(EDAT, levels = c("0-4", "5-14", "15-44", "45-59", "60-69", "70-79", ">=80", "TOTAL"))]

```

<<<<<<< HEAD
```{r eval=FALSE, include=FALSE}
ira <- fread("../dades/dades/IRA.txt", sep = "@", header = T)
ira[, data := as.Date(DATA)]
```

```{r}
=======
```{r}
ira <- fread("../dades/dades/IRA.txt", sep = "@", header = T)
ira[, data := as.Date(DATA)]
>>>>>>> 84cd213bc63fb61028806c32e339b9395a628f23
ira[EDAT %in% c("0", "1 i 2", "3 i 4"), EDAT_short := "0-4"]
ira[EDAT %in% c("5 a 9", "10 a 14"), EDAT_short := "5-14"]
ira[EDAT %in% c("15 a 19", "20 a 24", "25 a 29", "30 a 34", "35 a 39", "40 a 44"), EDAT_short := "15-44"]
ira[EDAT %in% c("45 a 49", "50 a 54", "55 a 59"), EDAT_short := "45-59"]
ira[EDAT %in% c("60 a 64", "65 a 69"), EDAT_short := "60-69"]
ira[EDAT %in% c("70 a 74", "75 a 79"), EDAT_short := "70-79"]
ira[EDAT == "80 o més", EDAT_short := ">=80"]
ira[, EDAT_short := factor(EDAT_short, levels = c("0-4", "5-14", "15-44", "45-59", "60-69", "70-79", ">=80", "TOTAL"))]

```

```{r}
ira_aggr <- rbind(
  ira[, .(
    REGIO = "GLOBAL",
    EDAT = "TOTAL",
    IRA = sum(IRA, na.rm = T)
  ), by = .(data)],
  ira[, .(
    REGIO = "GLOBAL",
    IRA = sum(IRA, na.rm = T)
  ), by = .(data, EDAT = EDAT_short)],
  ira[, .(
    EDAT = "TOTAL",
    IRA = sum(IRA, na.rm = T)
  ), by = .(data, REGIO)],
  ira[, .(
    IRA = sum(IRA, na.rm = T)
  ), by = .(data, REGIO, EDAT = EDAT_short)]
)
```


```{r}
dades <- merge(dades, ira_aggr, by = c("data", "REGIO", "EDAT"), all.x = T)
dades[is.na(IRA), IRA := 0]
dades[order(data), IRA_7 := Reduce(`+`, shift(IRA, 0:6))/7, by = .(REGIO, EDAT)]

<<<<<<< HEAD
```

```{r}
dades <- dades[month(data) %in% 6:8]
```


=======
```



>>>>>>> 84cd213bc63fb61028806c32e339b9395a628f23
```{r}
dades[, mitjana_7_z := (mitjana_7 - mean(mitjana_7, na.rm = T))/sd(mitjana_7, na.rm = T), by = .(EDAT, REGIO)]
dades[, maxim_7_z := (maxim_7 - mean(maxim_7, na.rm = T))/sd(maxim_7, na.rm = T), by = .(EDAT, REGIO)]

dades[, ut_m7_z := (UT_M7 - mean(UT_M7, na.rm = T))/sd(UT_M7, na.rm = T), by = .(EDAT, REGIO)]
dades[, ui_m7_z := (UI_M7 - mean(UI_M7, na.rm = T))/sd(UI_M7, na.rm = T), by = .(EDAT, REGIO)]
dades[, ira_m7_z := (IRA_7 - mean(IRA_7, na.rm = T))/sd(IRA_7, na.rm = T), by = .(EDAT, REGIO)]
```

<<<<<<< HEAD
```{r eval=FALSE, include=FALSE}
fwrite(dades, "../dades/dades_urgencies_ta_regio_edat_global.txt", sep = "@")
```
=======
```{r}
fwrite(dades, "../dades/dades/dades_urgencies_ta_regio_edat_global.txt", sep = "@")
```


>>>>>>> 84cd213bc63fb61028806c32e339b9395a628f23
