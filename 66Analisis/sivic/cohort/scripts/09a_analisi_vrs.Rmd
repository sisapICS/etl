***
***

# Análisi VRS

```{r}
dt.a <- dt.flt
```

###  Característiques basals

```{r}
SDCols.var <- c("vrs",
                "edat", "c_grup_edat_c3", "c_grup_edat_c4", "edat_le2", "edat_ge65",
                "sexe")
df <- dt.a[seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(vrs ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
export2word(cT, "../results/taula_vrs_basal.docx")
```

### Outcomes

#### 1 Setmana

```{r}
dt.a <- dt.flt[vrs == "Sí",]
SDCols.1s.var <- names(dt.a)[grepl("_1s" , names(dt.a))]
SDCols.1s.var <- setdiff(SDCols.1s.var, SDCols.seg.var)
SDCols.var <- c("vrs",
                SDCols.1s.var)
df <- dt.a[seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(vrs ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

#### 2 Setmanes

```{r}
SDCols.2s.var <- names(dt.a)[grepl("_2s" , names(dt.a))]
SDCols.2s.var <- setdiff(SDCols.2s.var, SDCols.seg.var)
SDCols.var <- c("vrs",
                SDCols.2s.var)
df <- dt.a[seguiment_2s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(vrs ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

#### 4 Setmanes

```{r}
SDCols.4s.var <- names(dt.a)[grepl("_4s" , names(dt.a))]
SDCols.4s.var <- setdiff(SDCols.4s.var, SDCols.seg.var)
SDCols.var <- c("vrs",
                SDCols.4s.var)
df <- dt.a[seguiment_4s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(vrs ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

#### 12 Setmanes

```{r}
SDCols.12s.var <- names(dt.a)[grepl("_12s" , names(dt.a))]
SDCols.12s.var <- setdiff(SDCols.12s.var, SDCols.seg.var)
SDCols.var <- c("vrs",
                SDCols.12s.var)
df <- dt.a[seguiment_12s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(vrs ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```


```{r}
gc.var <- gc()
```

