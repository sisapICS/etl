***
***

# Descriptiva BIVARIADA

```{r}

```



```{r}
dt.a <- dt.flt
```

## Resultat (Negatiu/Grip/Covid/VRS/Altres)

Comparació segons resultat

###  Característiques basals

```{r}
SDCols.var <- c("resultat",
                "edat", "c_grup_edat_c3", "c_grup_edat_c4", "edat_le2", "edat_ge60", "edat_ge65",
                "sexe")
df <- dt.a[seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
#export2word(cT, "../results/taula_resultat_basal.docx")
```

### Seguiment

```{r}
SDCols.seg.var <- names(dt.a)[grepl("seguiment" , names(dt.a))]
SDCols.var <- c("resultat",
                SDCols.seg.var)
df <- dt.a[seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

### Outcomes

#### 1 Setmana

```{r}
SDCols.1s.var <- names(dt.a)[grepl("_1s" , names(dt.a))]
SDCols.1s.var <- setdiff(SDCols.1s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.1s.var)
df <- dt.a[seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

#### 2 Setmanes

```{r}
SDCols.2s.var <- names(dt.a)[grepl("_2s" , names(dt.a))]
SDCols.2s.var <- setdiff(SDCols.2s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.2s.var)
df <- dt.a[seguiment_2s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

#### 4 Setmanes

```{r}
SDCols.4s.var <- names(dt.a)[grepl("_4s" , names(dt.a))]
SDCols.4s.var <- setdiff(SDCols.4s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.4s.var)
df <- dt.a[seguiment_4s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

#### 12 Setmanes

```{r}
SDCols.12s.var <- names(dt.a)[grepl("_12s" , names(dt.a))]
SDCols.12s.var <- setdiff(SDCols.12s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.12s.var)
df <- dt.a[seguiment_12s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

### Edat

```{r}
ggplot(dt.a,
       aes(edat)) +
  geom_histogram(fill = 'orange', color = "white") +
  facet_grid(resultat ~ ., scales = "free") +
  theme_minimal()
```

### Sexe

```{r}
cols <- c("resultat",
          "sexe")

df <- dt.a[, ..cols]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = TRUE
                  )
export2md(cT, format = "html")
```

### Data

```{r}
dt.ggplot <- dt.a[, .N, .(resultat, data)]
ggplot(dt.ggplot,
       aes(data, N)) +
  geom_line() +
  facet_grid(resultat ~ .) +
  theme_minimal()
```
***
***

### Estratificat

#### Pacients <= 2 anys

##### 1 Setmana

```{r}
SDCols.1s.var <- names(dt.a)[grepl("_1s" , names(dt.a))]
SDCols.1s.var <- setdiff(SDCols.1s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.1s.var)
df <- dt.a[edat_le2 == "<=2a" & seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 2 Setmanes

```{r}
SDCols.2s.var <- names(dt.a)[grepl("_2s" , names(dt.a))]
SDCols.2s.var <- setdiff(SDCols.2s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.2s.var)
df <- dt.a[edat_le2 == "<=2a" & seguiment_2s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 4 Setmanes

```{r}
SDCols.4s.var <- names(dt.a)[grepl("_4s" , names(dt.a))]
SDCols.4s.var <- setdiff(SDCols.4s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.4s.var)
df <- dt.a[edat_le2 == "<=2a" & seguiment_4s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 12 Setmanes

```{r}
SDCols.12s.var <- names(dt.a)[grepl("_12s" , names(dt.a))]
SDCols.12s.var <- setdiff(SDCols.12s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.12s.var)
df <- dt.a[edat_le2 == "<=2a" & seguiment_12s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

#### Pacients 3 - 4 anys

##### 1 Setmana

```{r}
SDCols.1s.var <- names(dt.a)[grepl("_1s" , names(dt.a))]
SDCols.1s.var <- setdiff(SDCols.1s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.1s.var)
df <- dt.a[c_grup_edat_c3 == "3-4" & seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 2 Setmanes

```{r}
SDCols.2s.var <- names(dt.a)[grepl("_2s" , names(dt.a))]
SDCols.2s.var <- setdiff(SDCols.2s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.2s.var)
df <- dt.a[c_grup_edat_c3 == "3-4" & seguiment_2s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 4 Setmanes

```{r}
SDCols.4s.var <- names(dt.a)[grepl("_4s" , names(dt.a))]
SDCols.4s.var <- setdiff(SDCols.4s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.4s.var)
df <- dt.a[c_grup_edat_c3 == "3-4" & seguiment_4s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 12 Setmanes

```{r}
SDCols.12s.var <- names(dt.a)[grepl("_12s" , names(dt.a))]
SDCols.12s.var <- setdiff(SDCols.12s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.12s.var)
df <- dt.a[c_grup_edat_c3 == "3-4" & seguiment_12s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

#### Pacients 5 - 14 anys

##### 1 Setmana

```{r}
SDCols.1s.var <- names(dt.a)[grepl("_1s" , names(dt.a))]
SDCols.1s.var <- setdiff(SDCols.1s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.1s.var)
df <- dt.a[c_grup_edat_c3 == "5-14" & seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 2 Setmanes

```{r}
SDCols.2s.var <- names(dt.a)[grepl("_2s" , names(dt.a))]
SDCols.2s.var <- setdiff(SDCols.2s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.2s.var)
df <- dt.a[c_grup_edat_c3 == "5-14" & seguiment_2s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 4 Setmanes

```{r}
SDCols.4s.var <- names(dt.a)[grepl("_4s" , names(dt.a))]
SDCols.4s.var <- setdiff(SDCols.4s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.4s.var)
df <- dt.a[c_grup_edat_c3 == "5-14" & seguiment_4s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 12 Setmanes

```{r}
SDCols.12s.var <- names(dt.a)[grepl("_12s" , names(dt.a))]
SDCols.12s.var <- setdiff(SDCols.12s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.12s.var)
df <- dt.a[c_grup_edat_c3 == "5-14" & seguiment_12s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

#### Pacients 3 - 14 anys

##### 1 Setmana

```{r}
SDCols.1s.var <- names(dt.a)[grepl("_1s" , names(dt.a))]
SDCols.1s.var <- setdiff(SDCols.1s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.1s.var)
df <- dt.a[c_grup_edat_c4 == "3-14" & seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 2 Setmanes

```{r}
SDCols.2s.var <- names(dt.a)[grepl("_2s" , names(dt.a))]
SDCols.2s.var <- setdiff(SDCols.2s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.2s.var)
df <- dt.a[c_grup_edat_c4 == "3-14" & seguiment_2s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 4 Setmanes

```{r}
SDCols.4s.var <- names(dt.a)[grepl("_4s" , names(dt.a))]
SDCols.4s.var <- setdiff(SDCols.4s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.4s.var)
df <- dt.a[c_grup_edat_c4 == "3-14" & seguiment_4s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 12 Setmanes

```{r}
SDCols.12s.var <- names(dt.a)[grepl("_12s" , names(dt.a))]
SDCols.12s.var <- setdiff(SDCols.12s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.12s.var)
df <- dt.a[c_grup_edat_c4 == "3-14" & seguiment_12s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

#### Pacients 15 - 59 anys

##### 1 Setmana

```{r}
SDCols.1s.var <- names(dt.a)[grepl("_1s" , names(dt.a))]
SDCols.1s.var <- setdiff(SDCols.1s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.1s.var)
df <- dt.a[c_grup_edat_c4 == "15-59" & seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 2 Setmanes

```{r}
SDCols.2s.var <- names(dt.a)[grepl("_2s" , names(dt.a))]
SDCols.2s.var <- setdiff(SDCols.2s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.2s.var)
df <- dt.a[c_grup_edat_c4 == "15-59" & seguiment_2s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 4 Setmanes

```{r}
SDCols.4s.var <- names(dt.a)[grepl("_4s" , names(dt.a))]
SDCols.4s.var <- setdiff(SDCols.4s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.4s.var)
df <- dt.a[c_grup_edat_c4 == "15-59" & seguiment_4s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 12 Setmanes

```{r}
SDCols.12s.var <- names(dt.a)[grepl("_12s" , names(dt.a))]
SDCols.12s.var <- setdiff(SDCols.12s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.12s.var)
df <- dt.a[c_grup_edat_c4 == "15-59" & seguiment_12s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

***

#### Pacients 60 - 69 anys

##### 1 Setmana

```{r}
SDCols.1s.var <- names(dt.a)[grepl("_1s" , names(dt.a))]
SDCols.1s.var <- setdiff(SDCols.1s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.1s.var)
df <- dt.a[c_grup_edat_c4 == "60-69" & seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 2 Setmanes

```{r}
SDCols.2s.var <- names(dt.a)[grepl("_2s" , names(dt.a))]
SDCols.2s.var <- setdiff(SDCols.2s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.2s.var)
df <- dt.a[c_grup_edat_c4 == "60-69" & seguiment_2s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 4 Setmanes

```{r}
SDCols.4s.var <- names(dt.a)[grepl("_4s" , names(dt.a))]
SDCols.4s.var <- setdiff(SDCols.4s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.4s.var)
df <- dt.a[c_grup_edat_c4 == "60-69" & seguiment_4s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 12 Setmanes

```{r}
SDCols.12s.var <- names(dt.a)[grepl("_12s" , names(dt.a))]
SDCols.12s.var <- setdiff(SDCols.12s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.12s.var)
df <- dt.a[c_grup_edat_c4 == "60-69" & seguiment_12s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

***

#### Pacients 70 - 79 anys

##### 1 Setmana

```{r}
SDCols.1s.var <- names(dt.a)[grepl("_1s" , names(dt.a))]
SDCols.1s.var <- setdiff(SDCols.1s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.1s.var)
df <- dt.a[c_grup_edat_c4 == "70-79" & seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 2 Setmanes

```{r}
SDCols.2s.var <- names(dt.a)[grepl("_2s" , names(dt.a))]
SDCols.2s.var <- setdiff(SDCols.2s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.2s.var)
df <- dt.a[c_grup_edat_c4 == "70-79" & seguiment_2s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 4 Setmanes

```{r}
SDCols.4s.var <- names(dt.a)[grepl("_4s" , names(dt.a))]
SDCols.4s.var <- setdiff(SDCols.4s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.4s.var)
df <- dt.a[c_grup_edat_c4 == "70-79" & seguiment_4s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 12 Setmanes

```{r}
SDCols.12s.var <- names(dt.a)[grepl("_12s" , names(dt.a))]
SDCols.12s.var <- setdiff(SDCols.12s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.12s.var)
df <- dt.a[c_grup_edat_c4 == "70-79" & seguiment_12s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```


***

#### Pacients >= 80 anys

##### 1 Setmana

```{r}
SDCols.1s.var <- names(dt.a)[grepl("_1s" , names(dt.a))]
SDCols.1s.var <- setdiff(SDCols.1s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.1s.var)
df <- dt.a[c_grup_edat_c4 == ">=80" & seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 2 Setmanes

```{r}
SDCols.2s.var <- names(dt.a)[grepl("_2s" , names(dt.a))]
SDCols.2s.var <- setdiff(SDCols.2s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.2s.var)
df <- dt.a[c_grup_edat_c4 == ">=80" & seguiment_2s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 4 Setmanes

```{r}
SDCols.4s.var <- names(dt.a)[grepl("_4s" , names(dt.a))]
SDCols.4s.var <- setdiff(SDCols.4s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.4s.var)
df <- dt.a[c_grup_edat_c4 == ">=80" & seguiment_4s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 12 Setmanes

```{r}
SDCols.12s.var <- names(dt.a)[grepl("_12s" , names(dt.a))]
SDCols.12s.var <- setdiff(SDCols.12s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.12s.var)
df <- dt.a[c_grup_edat_c4 == ">=80" & seguiment_12s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

***

####  Pacients >= 60 anys

##### 1 Setmana

```{r}
SDCols.1s.var <- names(dt.a)[grepl("_1s" , names(dt.a))]
SDCols.1s.var <- setdiff(SDCols.1s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.1s.var)
df <- dt.a[edat_ge60 == ">=60a" & seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 2 Setmanes

```{r}
SDCols.2s.var <- names(dt.a)[grepl("_2s" , names(dt.a))]
SDCols.2s.var <- setdiff(SDCols.2s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.2s.var)
df <- dt.a[edat_ge60 == ">=60a" & seguiment_2s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 4 Setmanes

```{r}
SDCols.4s.var <- names(dt.a)[grepl("_4s" , names(dt.a))]
SDCols.4s.var <- setdiff(SDCols.4s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.4s.var)
df <- dt.a[edat_ge60 == ">=60a" & seguiment_4s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 12 Setmanes

```{r}
SDCols.12s.var <- names(dt.a)[grepl("_12s" , names(dt.a))]
SDCols.12s.var <- setdiff(SDCols.12s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.12s.var)
df <- dt.a[edat_ge60 == ">=60a" & seguiment_12s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

***

####  Pacients >= 65 anys

##### 1 Setmana

```{r}
SDCols.1s.var <- names(dt.a)[grepl("_1s" , names(dt.a))]
SDCols.1s.var <- setdiff(SDCols.1s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.1s.var)
df <- dt.a[edat_ge65 == ">=65a" & seguiment_1s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 2 Setmanes

```{r}
SDCols.2s.var <- names(dt.a)[grepl("_2s" , names(dt.a))]
SDCols.2s.var <- setdiff(SDCols.2s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.2s.var)
df <- dt.a[edat_ge65 == ">=65a" & seguiment_2s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 4 Setmanes

```{r}
SDCols.4s.var <- names(dt.a)[grepl("_4s" , names(dt.a))]
SDCols.4s.var <- setdiff(SDCols.4s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.4s.var)
df <- dt.a[edat_ge65 == ">=65a" & seguiment_4s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

##### 12 Setmanes

```{r}
SDCols.12s.var <- names(dt.a)[grepl("_12s" , names(dt.a))]
SDCols.12s.var <- setdiff(SDCols.12s.var, SDCols.seg.var)
SDCols.var <- c("resultat",
                SDCols.12s.var)
df <- dt.a[edat_ge65 == ">=65a" & seguiment_12s == "Sí", .SD, .SDcols = SDCols.var]
res <- compareGroups(resultat ~ .
                     , df
                     , method = 1
                     , max.ylev = 10
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = TRUE
                  , show.all = FALSE
                  , show.p.overall = TRUE
                  , show.n = FALSE
                  , hide.no = 'No'
                  )
export2md(cT, format = "html")
```

```{r}
gc.var <- gc()
```

***
***

