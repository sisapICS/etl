# -*- coding: utf8 -*-

import hashlib as h

"""
Pujar llindars sivic
"""

import collections as c
from datetime import datetime
import sys



import sisapUtils as u


file = u.tempFolder + "llindars.txt"
tb = 'DWSISAP.SIVIC_LLINDARS_EPI'
db = 'exadata2'

upload = []
for (ID, ANY_INICI_TEMPORADA, DIAGNOSTIC, LLINDAR_DESC, COLOR, VALOR) in u.readCSV(file, sep='@'):
    upload.append([ID, ANY_INICI_TEMPORADA, DIAGNOSTIC, LLINDAR_DESC, COLOR, VALOR])

cols = ("ID INT","ANY_INICI_TEMPORADA int", "DIAGNOSTIC varchar2(255)",  "LLINDAR_DESC varchar2(255)","COLOR varchar2(255)", "VALOR number") 
u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)      
u.listToTable(upload, tb, db)    

users= ['DWSISAP_ROL', 'DWSIVIC', 'DWAMERCADE']
for user in users:
    u.execute("grant select on {} to {}".format(tb,user),db)
