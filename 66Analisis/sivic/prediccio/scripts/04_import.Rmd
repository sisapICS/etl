***
***
***

# Importació {.tabset}

```{r}
options(java.parameters = "-Xmx32g")

Sys.setenv(TZ = 'Europe/Madrid')
Sys.setenv(ORA_SDTZ = 'Europe/Madrid')

source("C:/Users/ehermosilla/Documents/Keys.R")
```

## Dades {.tabset}

### Mortalitat {.tabset}

S'importa la informació de la taula **sivic_mortalitat** de la bbdd DWSISAP d'EXADATA.

```{r}
if (params$actualitzar_dades) {
  ini <- Sys.time()
  source("C:/Users/ehermosilla/Documents/Keys.R")
  driver_path = "C:/Users/ehermosilla/ojdbc8.jar"
  driver = JDBC("oracle.jdbc.driver.OracleDriver", driver_path)
  ini <- Sys.time()
  service_name  = "excdox01srv"
  dsn = paste0("jdbc:oracle:thin:@//", hostexadata, ":", portexadata, "/", service_name)
  connection = dbConnect(driver,
                         dsn,
                         idexadata,
                         pwexadata)
  query <- "select
              *
            from
              dwsisap.SIVIC_MORTALITAT"
  dt.sivic.mortalitat.raw <- data.table(dbGetQuery(connection,
                                                   query))
  var.disconnect <- dbDisconnect(connection)
  saveRDS(dt.sivic.mortalitat.raw, "../data/dt.sivic.mortalitat.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.sivic.mortalitat.raw <- readRDS("../data/dt.sivic.mortalitat.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}
```

Informació importada:

- files n = `r nrow(dt.sivic.mortalitat.raw)`
- columnes n = `r ncol(dt.sivic.mortalitat.raw)`
- columnes: `r names(dt.sivic.mortalitat.raw)`

```{r}

names(dt.sivic.mortalitat.raw) <- tolower(names(dt.sivic.mortalitat.raw))
```

```{r}
# # Dates
#   # Obtener las columnas que empiezan con "data_"
#     cols <- grep("^data_", names(dt.cohort.raw), value = TRUE)
# 
#   # Convertir las columnas de character a tipo date
#     dt.cohort.raw[, (cols) := lapply(.SD, function(x) as.Date(x, format = "%Y-%m-%d")), .SDcols = cols]
#     
# # Numeric
#   dt.cohort.raw[, codi_nacionalitat := as.numeric(codi_nacionalitat)]  
```


#### Summary {.tabset}

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.sivic.mortalitat.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

```{r}
setnames(dt.sivic.mortalitat.raw, 'edat', 'edat_text')
dt.sivic.mortalitat.raw <- dt.sivic.mortalitat.raw[, .(data, sexe, edat_codi, edat_text, regio, isc, abs_codi, diari)]
```

***
***

## Catàlegs {.tabset}

### Catèleg de Centres {.tabset}

S'importa el catàleg de centres de nodrizas.

```{r cat_centres}
if (params$actualitzar_dades) {
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idsisap,
                   password = "", 
                   host = hostsisap,
                   port = portsisap,
                   dbname = "nodrizas")
  query <- dbSendQuery(con,
                       statement = "select
                                      ep,
                                      amb_codi, amb_desc,
                                      sap_codi, sap_desc,
                                      scs_codi, ics_codi, ics_desc,
                                      tip_eap, adults, nens,
                                      medea, aquas
                                    from
                                      cat_centres
                                    where
                                      scs_codi != '14423'")
  dt.cat_centres.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  saveRDS(dt.cat_centres.raw, "../data/dt.cat_centres.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.cat_centres.raw <- readRDS("../data/dt.cat_centres.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}  
#setnames(dt.cat_centres.raw, "ics_desc", "up_origen_desc")
```

Informació importada:
 
 - files n = `r nrow(dt.cat_centres.raw)`
 - columnes n = `r ncol(dt.cat_centres.raw)`
 - columnes: `r names(dt.cat_centres.raw)`

```{r}
names(dt.cat_centres.raw) <- tolower(names(dt.cat_centres.raw))

setnames(dt.cat_centres.raw, "ep", "eap_ep")
setnames(dt.cat_centres.raw, "medea", "medea_c")
#dt.cat_centres.raw[, .N, amb_desc]
#dt.cat_centres.raw[amb_desc == 'distinct', amb_desc := 'TARRAGONA']
```

#### Summary {.tabset}

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.cat_centres.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

```{r}
gc.var <- gc()
fi <- Sys.time()
fi - ini
```

