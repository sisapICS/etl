
suppressWarnings(suppressPackageStartupMessages(library('rmarkdown')))
suppressWarnings(suppressPackageStartupMessages(library('compareGroups')))


render("D:/SISAP/sisap/66analisis/sivic/cohort2324/scripts/01_CohortSIVIC.Rmd", 
       output_format = "html_document",
       output_file = paste('sivic_cohort_2324',
                           format(Sys.Date(),'%Y%m%d'),
                           format(Sys.time(),'%H%M'), sep = "_"),
       output_dir = "../results/",
       params = list(actualitzar_dades = TRUE,
                     skim = TRUE,
                     eval.tabset = TRUE,
                     export.word = FALSE)
)
gc()

# render("09a_analisi_params.Rmd", 
#        output_format = "html_document",
#        output_file = paste('sivic_cohort_2324_vrs',
#                            format(Sys.Date(),'%Y%m%d'),
#                            format(Sys.time(),'%H%M'), sep = "_"),
#        output_dir = "../results/",
#        params = list(virus = "vrs",
#                      eval.tabset = TRUE)
# )
# gc()