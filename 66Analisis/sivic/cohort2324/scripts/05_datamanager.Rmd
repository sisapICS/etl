
# Data Manager `r if (params$eval.tabset) '{.tabset}' else ''`

## Catàlegs `r if (params$eval.tabset) '{.tabset}' else ''`

### Catèleg de Centres SISAP

```{r}

dt.cat_centres.dm <- dt.cat_centres.raw
```

***

## Dades `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}

dt.sivic.cohort.dm <- dt.sivic.cohort.raw
```

### Cohort `r if (params$eval.tabset) '{.tabset}' else ''`

#### Creació noves variables `r if (params$eval.tabset) '{.tabset}' else ''`

Creació de les noves variables.

##### Edat

Calculem només variables d'edat.

```{r, echo = TRUE}
a <- dt.sivic.cohort.dm[, .N, edat]
                   
dt.sivic.cohort.dm[edat == 0, c_grup_edat_c3 := 1]
dt.sivic.cohort.dm[edat >= 1 & edat <= 2, c_grup_edat_c3 := 2]
dt.sivic.cohort.dm[edat >= 3 & edat <= 4, c_grup_edat_c3 := 3]
dt.sivic.cohort.dm[edat >= 5 & edat <= 14, c_grup_edat_c3 := 4]
dt.sivic.cohort.dm[edat >= 15 & edat <= 44, c_grup_edat_c3 := 5]
dt.sivic.cohort.dm[edat >= 45 & edat <= 59, c_grup_edat_c3 := 6]
dt.sivic.cohort.dm[edat >= 60 & edat <= 69, c_grup_edat_c3 := 7]
dt.sivic.cohort.dm[edat >= 70 & edat <= 79, c_grup_edat_c3 := 8]
dt.sivic.cohort.dm[edat >= 80, c_grup_edat_c3 := 9]
dt.sivic.cohort.dm[, .N, c_grup_edat_c3]
dt.sivic.cohort.dm[, c_grup_edat_c3 := factor(c_grup_edat_c3, labels = c("0",
                                                            "1-2",
                                                            "3-4",
                                                            "5-14",
                                                            "15-44",
                                                            "45-59",
                                                            "60-69",
                                                            "70-79",
                                                            ">=80"))]
dt.sivic.cohort.dm[, .(min = min(edat), max = max(edat)), c_grup_edat_c3][order(c_grup_edat_c3)]
gc.var <- gc()

dt.sivic.cohort.dm[edat >= 0 & edat <= 2, c_grup_edat_c4 := 1]
dt.sivic.cohort.dm[edat >= 3 & edat <= 14, c_grup_edat_c4 := 2]
dt.sivic.cohort.dm[edat >= 15 & edat <= 59, c_grup_edat_c4 := 3]
dt.sivic.cohort.dm[edat >= 60 & edat <= 69, c_grup_edat_c4 := 4]
dt.sivic.cohort.dm[edat >= 70 & edat <= 79, c_grup_edat_c4 := 5]
dt.sivic.cohort.dm[edat >= 80, c_grup_edat_c4 := 6]
dt.sivic.cohort.dm[, c_grup_edat_c4 := factor(c_grup_edat_c4, labels = c("0-2",
                                                            "3-14",
                                                            "15-59",
                                                            "60-69",
                                                            "70-79",
                                                            ">=80"))]
dt.sivic.cohort.dm[, .(min = min(edat), max = max(edat)), c_grup_edat_c4][order(c_grup_edat_c4)]
gc.var <- gc()

dt.sivic.cohort.dm[edat == 0, c_grup_edat_c5 := 1]
dt.sivic.cohort.dm[edat == 1, c_grup_edat_c5 := 2]
dt.sivic.cohort.dm[edat == 2, c_grup_edat_c5 := 3]
dt.sivic.cohort.dm[edat >= 3 & edat <= 14, c_grup_edat_c5 := 4]
dt.sivic.cohort.dm[edat >= 15 & edat <= 44, c_grup_edat_c5 := 5]
dt.sivic.cohort.dm[edat >= 45 & edat <= 59, c_grup_edat_c5 := 6]
dt.sivic.cohort.dm[edat >= 60 & edat <= 69, c_grup_edat_c5 := 7]
dt.sivic.cohort.dm[edat >= 70 & edat <= 79, c_grup_edat_c5 := 8]
dt.sivic.cohort.dm[edat >= 80, c_grup_edat_c5 := 9]
dt.sivic.cohort.dm[, .N, c_grup_edat_c5]
dt.sivic.cohort.dm[, c_grup_edat_c5 := factor(c_grup_edat_c5, labels = c("0",
                                                                         "1",
                                                                         "2",
                                                                        "3-14",
                                                                        "15-44",
                                                                        "45-59",
                                                                        "60-69",
                                                                        "70-79",
                                                                        ">=80"))]
dt.sivic.cohort.dm[, .(min = min(edat), max = max(edat)), c_grup_edat_c5][order(c_grup_edat_c5)]
gc.var <- gc()

dt.sivic.cohort.dm[, edat_le2 := ">2a"][edat <= 2, edat_le2 := "<=2a"][, edat_le2 := factor(edat_le2, levels = c(">2a", "<=2a"))]
dt.sivic.cohort.dm[, .(min = min(edat), max = max(edat)), edat_le2][order(edat_le2)]

dt.sivic.cohort.dm[, edat_ge60 := "<60a"][edat >= 60, edat_ge60 := ">=60a"][, edat_ge60 := factor(edat_ge60, levels = c("<60a", ">=60a"))]
dt.sivic.cohort.dm[, .(min = min(edat), max = max(edat)), edat_ge60][order(edat_ge60)]
dt.sivic.cohort.dm[, edat_ge65 := "<65a"][edat >= 65, edat_ge65 := ">=65a"][, edat_ge65 := factor(edat_ge65, levels = c("<65a", ">=65a"))]
dt.sivic.cohort.dm[, .(min = min(edat), max = max(edat)), edat_ge65][order(edat_ge65)]
```

##### Data

```{r, echo = TRUE}
dt.sivic.cohort.dm[, data := as.Date(data)]

dt.sivic.cohort.dm[, data_year := year(data)]
dt.sivic.cohort.dm[, data_year := as.factor(data_year)]

#dt.sivic.cohort.dm[, data_monthday := format(data, format = "%b %d")]
#dt.sivic.cohort.dm[, .N, data_monthday]
```

##### Temporada

```{r, echo = TRUE}
dt.sivic.cohort.dm[, temporada := str_c(year(data %m+% months(-9)),"-",year(data %m+% months(3)))]
#dt.sivic.cohort.dm[, .(min(data),max(data)), temporada][order(temporada)]
dt.sivic.cohort.dm[, .N, temporada]
```

##### Resultat

Ja no tenim aquesta variable, es podria tornar a calcular, només cal establir l'ordre.

```{r, echo = TRUE}
# #dt.sivic.cohort.dm[, .N, resultat]
# dt.sivic.cohort.dm[, resultat := factor(resultat,
#                                         levels = c("Negatiu", "Grip", "COVID", "VRS", "Altres"))]
```

##### Positiu

```{r, echo = TRUE}
# Convertim en factors les variables de positivitat
SDCols.var <- c("positiu")
dt.sivic.cohort.dm[, (SDCols.var) := lapply(.SD, function(x) {factor(x, levels = c(0,1), labels = c("No", "Sí"))}), .SDcols = SDCols.var]
```

##### Virus

```{r, echo = TRUE}
# Convertim en factors les variables de positivitat
SDCols.var <- c("adenovirus", "bocavirus", "coronavirushuma", "enterovirus", "grip", "metapneumovirus", "parainfluenza", "sarscov2", "vrs", "rinovirus")
dt.sivic.cohort.dm[, (SDCols.var) := lapply(.SD, function(x) {factor(x, levels = c(0,1), labels = c("No", "Sí"))}), .SDcols = SDCols.var]
```

```{r}
dt.sivic.cohort.dm[, virus_n := rowSums(.SD == "Sí"), .SDcols = SDCols.var]
dt.sivic.cohort.dm[, .N, virus_n][order(virus_n)]

dt.sivic.cohort.dm[, virus_n_f := virus_n][virus_n >= 3, virus_n_f := 3]
dt.sivic.cohort.dm[, virus_n_f := factor(virus_n_f,
                                         levels = c(0:3),
                                         labels = c('Negatiu', '1', '2', '>=3'))]
dt.sivic.cohort.dm[, .N, .(virus_n_f, virus_n)][order(virus_n_f, virus_n)]
```

##### Seguiment `r if (params$eval.tabset) '{.tabset}' else ''`

###### Seguiment dies

Calculem el mínim de dies entre les diferents fonts hospitalàries: mapa i cmbd-ah.

```{r, echo = TRUE}

dt.sivic.cohort.dm[, hosp_convencional_dies := pmin(mapa_convencional_dies, cmbd_convencional_dies, na.rm = TRUE)]
#a <- dt.sivic.cohort.dm[, .(mapa_convencional_dies, cmbd_convencional_dies, hosp_convencional_dies)]
```


```{r, echo = TRUE}

dt.sivic.cohort.dm[, hosp_critics_dies := pmin(mapa_critics_dies, cmbd_critics_dies, na.rm = TRUE)]
#a <- dt.sivic.cohort.dm[, .(mapa_critics_dies, cmbd_critics_dies, hosp_critics_dies)]
```

###### Seguiment 1/2 Setmana

```{r, echo = TRUE}

dt.sivic.cohort.dm[, seguiment_1s := 0][difftime(Sys.Date(), data, , units = "days") >= 7, seguiment_1s := 1] # dt.sivic.cohort.dm[, .N, seguiment_1s]
dt.sivic.cohort.dm[, seguiment_2s := 0][difftime(Sys.Date(), data, , units = "days") >= 14, seguiment_2s := 1]
     
f.seg <- function(x, dies) {
  columna.ori <- paste0(x,"_dies")
  ifelse(dies == 7, columna.new <- paste0(x,"_1s"), columna.new <- paste0(x,"_2s"))
  dt.sivic.cohort.dm[, (columna.new) := 0][get(columna.ori) > 0 & get(columna.ori) <= dies, (columna.new) := 1]  #[, paste0(x,"_1s") := factor(paste0(x,"_1s"))]
}
f.seg("urgencies", 7)
f.seg("urgencies", 14)

f.seg("mapa_convencional", 7)
f.seg("mapa_convencional", 14)
f.seg("mapa_critics", 7)
f.seg("mapa_critics", 14)

f.seg("cmbd_convencional", 7)
f.seg("cmbd_convencional", 14)
f.seg("cmbd_critics", 7)
f.seg("cmbd_critics", 14)

f.seg("hosp_convencional", 7)
f.seg("hosp_convencional", 14)
f.seg("hosp_critics", 7)
f.seg("hosp_critics", 14)

f.seg("exitus", 7)
f.seg("exitus", 14)

SDCols.var <- c("seguiment_1s", "seguiment_2s",
                "urgencies_1s", "urgencies_2s",
                "mapa_convencional_1s", "mapa_convencional_2s",
                "mapa_critics_1s", "mapa_critics_2s",
                "cmbd_convencional_1s", "cmbd_convencional_2s",
                "cmbd_critics_1s", "cmbd_critics_2s",
                "hosp_convencional_1s", "hosp_convencional_2s",
                "hosp_critics_1s", "hosp_critics_2s",
                "exitus_1s", "exitus_2s")
dt.sivic.cohort.dm[, (SDCols.var) := lapply(.SD, function(x) {factor(x, levels = c(0,1), labels = c("No", "Sí"))}), .SDcols = SDCols.var]

dt.sivic.cohort.dm[, .N, .(temporada, urgencies_1s)][order(temporada, urgencies_1s)]
```

```{r, echo = TRUE}
f.seg.a <- function(x, dies) {
  columna.ori <- paste0(x,"_dies")
  ifelse(dies == 28, columna.new <- paste0(x,"_4s"), columna.new <- paste0(x,"_12s"))
  dt.sivic.cohort.dm[, (columna.new) := 0][get(columna.ori) <= dies, (columna.new) := 1]  #[, paste0(x,"_1s") := factor(paste0(x,"_1s"))]
}
f.seg.a("hosp_convencional", 28)
f.seg.a("hosp_convencional", 84)
f.seg.a("hosp_critics", 28)
f.seg.a("hosp_critics", 84)

# Convertim en factors les variables de seguiment
SDCols.var <- c("seguiment_4s", "seguiment_12s",
                "urgencies_4s", "urgencies_12s",
                "mapa_convencional_4s", "mapa_convencional_12s",
                "mapa_critics_4s", "mapa_critics_12s",
                "cmbd_convencional_4s", "cmbd_convencional_12s",
                "cmbd_critics_4s", "cmbd_critics_12s",
                "hosp_convencional_4s", "hosp_convencional_12s",
                "hosp_critics_4s", "hosp_critics_12s",
                "exitus_4s", "exitus_12s")
dt.sivic.cohort.dm[, (SDCols.var) := lapply(.SD, function(x) {factor(x, levels = c(0,1), labels = c("No", "Sí"))}), .SDcols = SDCols.var]
#dt.sivic.cohort.dm[, .(min(cmbd_convencional_dies),max(cmbd_convencional_dies)), cmbd_convencional_1s][order(cmbd_convencional_1s)]
#a <- dt.sivic.cohort.dm[, .N, .(cmbd_convencional_1s, cmbd_convencional_dies)]
#a <- dt.sivic.cohort.dm[, .N, .(seguiment_1s, seguiment_dies)]
```


```{r}

a <- dt.sivic.cohort.dm[mapa_convencional_1s != cmbd_convencional_1s,]
```

```{r}
# REVISAR!!!

# f.seg <- function(x, dies) {
#   columna.ori <- paste0(x,"_dies")
#   ifelse(dies == 7, columna.new <- paste0(x,"_1s"), columna.new <- paste0(x,"_2s"))
#   dt.sivic.cohort.dm[, (columna.new) := 0][get(columna.ori) <= dies, (columna.new) := 1]
#   #return(dt.sivic.cohort.dm)
# }
# 
# columnas <- c("urgencies", "mapa_convencional", "mapa_critics", "cmbd_convencional", "cmbd_critics", "exitus")
# dies <- c(7, 14)
# 
# resultado <- lapply(columnas, function(x) {
#   lapply(dies, function(y) {
#     f.seg(x, y)
#   })[[2]] # solo nos interesa el último data frame modificado
# })
```

#### Afegir variables `r if (params$eval.tabset) '{.tabset}' else ''`

##### Catàleg Centres EAP

```{r}

```

#### Ordernar columnes

```{r orden columnas, echo = TRUE}

#names(dt.sivic.cohort.dm)
dt.sivic.cohort.dm <- dt.sivic.cohort.dm[, .(cip, 
                   peticio, data, data_year, temporada,
                   edat,
                   c_grup_edat_c3, c_grup_edat_c4, c_grup_edat_c5,
                   edat_le2, edat_ge60, edat_ge65,
                   sexe, 
                   positiu, 
                   adenovirus, bocavirus, coronavirushuma, enterovirus, grip,
                   metapneumovirus, parainfluenza, sarscov2, vrs, rinovirus,
                   virus_n, virus_n_f,
                   #resultat, 
                   seguiment_1s, seguiment_2s, seguiment_4s, seguiment_12s,
                   urgencies_dies, urgencies_1s, urgencies_2s, urgencies_4s, urgencies_12s,
                   mapa_convencional_dies, mapa_convencional_1s, mapa_convencional_2s, mapa_convencional_4s, mapa_convencional_12s,
                   mapa_critics_dies, mapa_critics_1s, mapa_critics_2s, mapa_critics_4s, mapa_critics_12s,
                   cmbd_convencional_dies, cmbd_convencional_1s, cmbd_convencional_2s, cmbd_convencional_4s, cmbd_convencional_12s,
                   cmbd_critics_dies, cmbd_critics_1s, cmbd_critics_2s, cmbd_critics_4s, cmbd_critics_12s,
                   hosp_convencional_dies, hosp_convencional_1s, hosp_convencional_2s, hosp_convencional_4s, hosp_convencional_12s,
                   hosp_critics_dies, hosp_critics_1s, hosp_critics_2s, hosp_critics_4s, hosp_critics_12s,
                   exitus_dies, exitus_1s, exitus_2s, exitus_4s, exitus_12s)]
```

```{r data manager save}
saveRDS(dt.sivic.cohort.dm, "../data/dt.sivic.cohort.dm.rds")
```

```{r data manager gc}

gc.var <- gc()
```

