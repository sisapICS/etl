
library('rmarkdown')


render("../scripts/01_presencialitat_proves.Rmd", 
       output_file = paste('Presencialitat_proves',
                           format(Sys.Date(),'%Y%m%d'),
                           format(Sys.time(),'%H%M'), sep = "_"),
       output_format = "html_document",
       output_dir = "../results"
)
