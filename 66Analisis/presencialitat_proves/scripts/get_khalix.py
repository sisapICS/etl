# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

path = "D:/SISAP/sisap/66Analisis/presencialitat_proves/data/"

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

""" c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)
AGENDQC1 = exemple(c, 'AGENDQC1; A2210; AMBITOS###; NUM, DEN; ANUAL, MENSUAL; TIPPROF###; DIM6SET')
CONT0002 = exemple(c, 'CONT0002; A2210 ; AMBITOS###;	NUM, DEN; NOCAT; TIPPROF###; DIM6SET')
CONT0002A = exemple(c, 'CONT0002A; A2210; AMBITOS###; NUM, DEN; NOCAT; TIPPROF###; DIM6SET')
QACC2D = exemple(c, 'QACC2D; A2210; AMBITOS###; NUM, DEN; ANUAL, MENSUAL; TIPPROF###; DIM6SET')
QACC5D = exemple(c, 'QACC5D; A2210; AMBITOS###; NUM, DEN; ANUAL, MENSUAL; TIPPROF###; DIM6SET')
VISUBA = exemple(c, 'VISUBA; A2210; AMBITOS###; NUM, DEN; ANUAL, MENSUAL; TIPPROF###; DIM6SET')
VISURG = exemple(c, 'VISURG; A2210; AMBITOS###; NUM, DEN; ANUAL, MENSUAL; TIPPROF###; DIM6SET')
QECONS0000 = exemple(c, 'QECONS0000; A2210; AMBITOS###; NUM, DEN; ANUAL; TIPPROF###; DIM6SET')
VVIRTUALS = exemple(c, 'VVIRTUALS; A2210; AMBITOS###; NUM, DEN; ANUAL; TIPPROF###; DIM6SET')

c.close()
print('export')
file = path + "indicadors_khalix.txt"
with open(file, 'w') as f:
   f.write(CONT0002 + '\n' + CONT0002A + '\n' + QACC2D + '\n' + QACC5D + '\n' + VISUBA + '\n' + VISURG + '\n' + AGENDQC1 + '\n' + QECONS0000 + '\n' + VVIRTUALS)


c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)
pi_a2022_homes = exemple(c, 'ORCLINIC###;AYR22;AMBITOS###;CLINIQUES#2;EDATS5###;DERORIGEN###;HOME')
pi_a2022_dones = exemple(c, 'ORCLINIC###;AYR22;AMBITOS###;CLINIQUES#2;EDATS5###;DERORIGEN###;DONA')
c.close()
print('export')
file = path + "indicadors_khalix_proves_homes.txt"
with open(file, 'w') as f:
   f.write(pi_a2022_homes)
file = path + "indicadors_khalix_proves_dones.txt"
with open(file, 'w') as f:
   f.write(pi_a2022_dones) """

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)
pi_2210_homes = exemple(c, 'ORCLINIC###;A2210;AMBITOS###;CLINIQUES#2;EDATS5###;DERORIGEN###;HOME')
pi_2210_dones = exemple(c, 'ORCLINIC###;A2210;AMBITOS###;CLINIQUES#2;EDATS5###;DERORIGEN###;DONA')
c.close()
print('export')
file = path + "indicadors_khalix_proves_homes_2210.txt"
with open(file, 'w') as f:
   f.write(pi_2210_homes)
file = path + "indicadors_khalix_proves_dones_2210.txt"
with open(file, 'w') as f:
   f.write(pi_2210_dones)