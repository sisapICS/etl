# -*- coding: utf8 -*-

"""
Absentisme projecte residents: Nivell visites
"""
import hashlib as h

import collections as c

import sisapUtils as u


class mst_visites(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_visites()
        self.export()
  
    def get_visites(self):
        """."""
        u.printTime("Visites")  
        self.upload = []
        sql = """select
                    PACIENT,
                    EDAT,
                    DATA,
                    HORA,
                    UP,
                    SISAP_SITUACIO_CODI,
                    INTERNET,
                    PETICIO, 
                    ORIGEN,
                    tipus_class_desc
                from 
                    dwsisap.sisap_master_visites
                WHERE
                    DATA BETWEEN DATE '2022-07-01' AND DATE '2023-06-30'
                    and programacio_class = 'P' 
                    and tipus_class in ('C9C', 'C9R') 
                    and sisap_servei_class = 'MF'"""
        for pacient, edat, data, hora, up, situacio, internet, peticio,  origen, tipus in u.getAll(sql, 'exadata'):
            self.upload.append([pacient, edat, data, hora, up, situacio, internet, peticio,  origen, tipus])
                        
    def export(self):
        """export"""
        u.printTime("export")
        
        db = "permanent"
        tb = "absentisme_visites"
        columns = ["hash varchar(40)", "edat int", "data_visita date", "hora_visita varchar(10)", "up varchar(5)", "situacio varchar(5)", "internet varchar(10)", "data_peticio date","origen varchar(10)", "tipus varchar(50)" ]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
if __name__ == '__main__' :
        u.printTime("Inici")
        
        mst_visites()

        u.printTime("Fi")
