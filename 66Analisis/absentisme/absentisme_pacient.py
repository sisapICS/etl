# -*- coding: utf8 -*-

"""
Absentisme projecte residents: Nivell pacient
"""
import hashlib as h

import collections as c

import sisapUtils as u


class mst_pacients(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_dbs()
        self.export()
  
    def get_dbs(self):
        """."""
        u.printTime("dbs")  
        self.upload = []
        sql = """select
                    C_CIP, 
                    C_UP, 
                    C_PROVEIDOR, 
                    C_EDAT_ANYS, 
                    C_DATA_NAIX, 
                    C_SEXE, 
                    C_gma_codi,
                    C_GMA_COMPLEXITAT, 
                    c_gma_n_croniques,
                    C_NACIONALITAT, 
                    C_INSTITUCIONALITZAT
                from 
                    dwsisap.DBS"""
        for pacient, up, ep, edat, naix, sexe, gma, complx, n_cron, nac, insti in u.getAll(sql, 'exadata'):
            self.upload.append([pacient, up, ep, edat, naix, sexe, gma, complx, n_cron, nac, insti ])
                        
    def export(self):
        """export"""
        u.printTime("export")
        
        db = "permanent"
        tb = "absentisme_pacients"
        columns = ["hash varchar(40)", "up varchar(5)", "proveidor varchar(10)", "edat int", "data_naix date", "sexe varchar(10)",  "gma_codi varchar(10)", "gma_complexitat double", "gma_n_croniques int", "nacionalitat varchar(100)", "institucionalitzat varchar(10)" ]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        
        
if __name__ == '__main__' :
        u.printTime("Inici")
        
        mst_pacients()

        u.printTime("Fi")