
# Importació `r if (params$eval.tabset) '{.tabset}' else ''`

Importació de les dades necessàries per fer l'anàlisi.

```{r}
options(java.parameters = "-Xmx32g")

Sys.setenv(TZ = 'Europe/Madrid')
Sys.setenv(ORA_SDTZ = 'Europe/Madrid')

source("C:/Users/ehermosilla/Documents/Keys.R")
```

## Dades `r if (params$eval.tabset) '{.tabset}' else ''`

### Moviments `r if (params$eval.tabset) '{.tabset}' else ''`

S'importa la informació de la taula **vistb050** de la bbdd redICS.

```{r}
if (params$actualitzar_dades) {
  ini <- Sys.time()
  driver_path = "C:/Users/ehermosilla/ojdbc8.jar"
  driver = JDBC("oracle.jdbc.driver.OracleDriver", driver_path)
  service_name  = dbredics
  dsn = paste0("jdbc:oracle:thin:@//", hostredics, ":", portredics, "/", service_name)
  connection = dbConnect(driver,
                         dsn,
                         idredics,
                         pwredics)
  query <- "select
              huab_usua_cip,
              codi_sector,
              huab_ep_codi,
              huab_up_codi,
              huab_uab_codi,
              trunc(huab_data_ass) as huab_data_ass,
              trunc(huab_data_final) as huab_data_final,
              huab_prov_t_doc,
              huab_prov_n_id,
              huab_prov_n_col,
              huab_usuari_ass,
              huab_usuari_final,
              huab_motiu_des,
              huab_motiu_canvi_uab
            from
              vistb050"
  dt.moviments.raw = data.table(dbGetQuery(connection,
                                           query))
  var.disconnect <- dbDisconnect(connection)
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
    
  ini <- Sys.time()
  saveRDS(dt.moviments.raw, "../data/dt.moviments.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.moviments.raw <- readRDS("../data/dt.moviments.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}  
```

Informació importada:

- files n = `r nrow(dt.moviments.raw)`
- columnes n = `r ncol(dt.moviments.raw)`
- columnes: `r names(dt.moviments.raw)`

```{r}
if (params$actualitzar_dades) {
  names(dt.moviments.raw) <- tolower(names(dt.moviments.raw))
}  
```

```{r}
if (params$actualitzar_dades) {
# Dates
  cols <- grep("huab_data_", names(dt.moviments.raw), value = TRUE)
  dt.moviments.raw[, (cols) := lapply(.SD, function(x) as.Date(x, format = "%Y-%m-%d")), .SDcols = cols]
  rm(list = c('cols'))
  gc.var <- gc()

# Numeric
  #cols <- c('') 
  #d[, (cols) := lapply(.SD, function(x) {as.numeric(x)}), .SDcols = cols]
  #rm(list = c('cols'))
  
# Factor
  cols <- c('codi_sector',
            'huab_ep_codi', 'huab_up_codi', 'huab_uab_codi',
            'huab_prov_t_doc','huab_prov_n_id','huab_prov_n_col',
            'huab_usuari_ass', 'huab_usuari_final', 'huab_motiu_canvi_uab') 
  dt.moviments.raw[, (cols) := lapply(.SD, function(x) {as.factor(x)}), .SDcols = cols]
  rm(list = c('cols'))
  gc.var <- gc()
}
```

#### Summary `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.moviments.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

```{r}
if (params$actualitzar_dades) {
  saveRDS(dt.moviments.raw, "../data/dt.moviments.raw.rds")
}  
```

***
***

## Catàlegs `r if (params$eval.tabset) '{.tabset}' else ''`

### Catèleg de Centres `r if (params$eval.tabset) '{.tabset}' else ''`

S'importa el catàleg de centres de nodrizas.

```{r cat_centres}
if (params$actualitzar_dades) {
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idsisap,
                   password = "", 
                   host = hostsisap,
                   port = portsisap,
                   dbname = "nodrizas")
  query <- dbSendQuery(con,
                       statement = "select
                                      ep,
                                      amb_codi, amb_desc,
                                      sap_codi, sap_desc,
                                      scs_codi, ics_codi, ics_desc,
                                      tip_eap, adults, nens,
                                      medea, aquas
                                    from
                                      cat_centres
                                    where
                                      scs_codi != '14423'")
  dt.cat_centres.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  saveRDS(dt.cat_centres.raw, "../data/dt.cat_centres.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.cat_centres.raw <- readRDS("../data/dt.cat_centres.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}  
#setnames(dt.cat_centres.raw, "ics_desc", "up_origen_desc")
```

Informació importada:
 
 - files n = `r nrow(dt.cat_centres.raw)`
 - columnes n = `r ncol(dt.cat_centres.raw)`
 - columnes: `r names(dt.cat_centres.raw)`

```{r}
names(dt.cat_centres.raw) <- tolower(names(dt.cat_centres.raw))

setnames(dt.cat_centres.raw, "ep", "eap_ep")
setnames(dt.cat_centres.raw, "medea", "medea_c")
#dt.cat_centres.raw[, .N, amb_desc]
#dt.cat_centres.raw[amb_desc == 'distinct', amb_desc := 'TARRAGONA']
```

#### Summary `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.cat_centres.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

***

### Catèleg de Nacionalitats `r if (params$eval.tabset) '{.tabset}' else ''`

S'importa el catàleg de nacionalitats de nodrizas.

```{r import cataleg nacionalitat}
if (params$actualitzar_dades) {
  drv <- dbDriver("MySQL")
  con <- dbConnect(drv,
                   user = idsisap,
                   password = "", 
                   host = hostsisap,
                   port = portsisap,
                   dbname = "nodrizas")
  query <- dbSendQuery(con,
                       statement = "select
                                      *
                                    from
                                      cat_nacionalitat")
  dt.cat_nacionalitat.raw <- data.table(fetch(query, n = nfetch))
  var.disconnect <- dbDisconnect(con)
  saveRDS(dt.cat_nacionalitat.raw, "../data/dt.cat_nacionalitat.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
} else {
  ini <- Sys.time()
  dt.cat_nacionalitat.raw <- readRDS("../data/dt.cat_nacionalitat.raw.rds")
  fi <- Sys.time()
  fi - ini
  gc.var <- gc()
}    
  
```

Informació importada:
 
 - files n = `r nrow(dt.cat_nacionalitat.raw)`
 - columnes n = `r ncol(dt.cat_nacionalitat.raw)`
 - columnes: `r names(dt.cat_nacionalitat.raw)`


#### Summary `r if (params$eval.tabset) '{.tabset}' else ''`

```{r}
if (params$skim) {
  dt.skim <- data.table(skim(dt.cat_nacionalitat.raw))
  skim_type <- unique(dt.skim[, skim_type])
} else {print("No s'executa SKIM: volum de dades molt gran")}    
```

```{r, child="skim.Rmd"}

```

```{r}
gc.var <- gc()
fi <- Sys.time()
fi - ini
```

