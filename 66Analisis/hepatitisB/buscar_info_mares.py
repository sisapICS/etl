# -*- coding: latin1 -*-

"""
Fills mare portadora hepatitis B
"""

import collections as c
import psutil as p

import difflib as s

import sisapUtils as u


cas_file = u.tempFolder + "vhb_pacients_rca.txt"
rescat = u.tempFolder + "vhb_pacients_rescat.txt"

db = "permanent"
nod = "nodrizas"
imp = "import"

ps = 13
serologia = 655
vacuna = 15
embaras = 39

class embarassos_hepH(object):
    """."""

    def __init__(self):
        """."""
        self.get_hash()
        self.getMares()
        self.get_assignada()
        self.get_problemes()
        self.get_vacunes()
        self.get_serologies()
        self.get_embarassos()
        self.export()
    
        
    def get_hash(self):
        """hash to id_cip_sec"""
        u.printTime("id_cip_sec")
        self.id_to_hash = {}
        self.idsec_to_hash = {}
        
        sql = """select 
                    id_cip, id_cip_sec, hash_d, codi_sector 
                from 
                    u11"""
        for id, id_sec, hash, sec in u.getAll(sql, 'import'):
            self.id_to_hash[id] = hash
            self.idsec_to_hash[id_sec] = hash
    
    def getMares(self):
        """Busquem les mares primer"""
        u.printTime("mares")

        self.mares = {}
        self.registres= []

        for (id,cip13,hash_covid,hash_redics,mare_edat,mare_nacionalitat_esp,mare_nacionalitat) in u.readCSV(cas_file, sep=';'):
            self.mares[hash_redics] = True
            self.registres.append([id,hash_covid,hash_redics,mare_edat,mare_nacionalitat_esp,mare_nacionalitat])
            
        for (id,cip13,hash_covid,hash_redics,mare_edat,mare_nacionalitat_esp,mare_nacionalitat) in u.readCSV(rescat, sep=';'):
            self.mares[hash_redics] = True
            self.registres.append([id,hash_covid,hash_redics,mare_edat,mare_nacionalitat_esp,mare_nacionalitat])
    
    def get_assignada(self):
        """."""
        u.printTime("sociodemografic")
        
        self.poblacio = []
        
        nacionalitats = {}
        sql = """select codi_nac, desc_nac, regio_desc from cat_nacionalitat"""
        for cod, desc, regio in u.getAll(sql, 'nodrizas'):
            nacionalitats[int(cod)] = {'desc': desc, 'regio':regio}          
        
        sql = """select 
                    usua_cip, usua_uab_up, usua_data_naixement, usua_sexe, usua_situacio, usua_data_situacio, usua_nacionalitat 
                from 
                    usutb040"""
        for hash, up, naix, sexe, situacio, dat_sit, nac in u.getAll(sql, 'redics'):
            if hash in self.mares:
                if nac == None:
                    nac = 724
                desc = nacionalitats[int(nac)]['desc'] if int(nac) in nacionalitats else None
                regio = nacionalitats[int(nac)]['regio'] if int(nac) in nacionalitats else None
                self.poblacio.append([hash, up, naix, sexe, situacio, dat_sit, nac, desc, regio])
    
    def get_problemes(self):
        """."""
        u.printTime("problemes")
        codis_dx = []
        sql = 'select criteri_codi from eqa_criteris where agrupador={}'.format(ps)
        for cod, in u.getAll(sql, nod):
            codis_dx.append(cod)
        in_crit = tuple(codis_dx)
        
        descripcio = {}
        sql = """select ps_cod, ps_des from cat_prstb001 where ps_cod in {}""".format(in_crit)
        for cod, des in u.getAll(sql, 'import'):
            descripcio[cod] = des

        self.dx_hepatitis = []
        sql = """select 
                    id_cip, pr_dde, pr_dba, pr_cod_ps
                from 
                    problemes 
                where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_data_baixa = 0
                    and pr_cod_ps in {}""".format(in_crit)
        for id, dde, dba, pr_cod in u.getAll(sql, imp):
            hash = self.id_to_hash[id] if id in self.id_to_hash else None
            if hash in self.mares:
                desc = descripcio[pr_cod]
                self.dx_hepatitis.append([hash, dde, dba, pr_cod, desc])
    
    def get_vacunes(self):
        """."""
        u.printTime("vacunes")
        
        codis_dx = []
        sql = 'select vacuna from nodrizas.eqa_criteris a inner join import.cat_prstb040_new cpn on criteri_codi=antigen where agrupador={}'.format(vacuna)
        for cod, in u.getAll(sql, nod):
            codis_dx.append(cod)
        in_crit = tuple(codis_dx)
        
        self.vac_hepatitis = []
        sql = """select 
                    id_cip, va_u_cod,  va_u_data_vac, va_u_dosi 
                from 
                    vacunes 
                where 
                    va_u_data_baixa=0 and va_u_cod in {}""".format(in_crit)
        for id, vacs, data, dosi in u.getAll(sql, imp):
            hash = self.id_to_hash[id] if id in self.id_to_hash else None
            if hash in self.mares:
                self.vac_hepatitis.append([hash, vacs, data, dosi])
	
    def get_serologies(self):
        """."""
        u.printTime("serologia")
        codis_dx = []
        self.serologiaB = []
        self.desc = {}
        sql = """select codi, agrupador from cat_dbscat 
                              where taula = 'serologies' and agrupador like '%VHB%'"""
        for codi, agr in u.getAll(sql, imp):
            codis_dx.append(codi)
            self.desc[codi] = agr
        in_crit = tuple(codis_dx)
        sql = """select id_cip_sec, dat, cod, val from nod_serologies where cod in {}""".format(in_crit)
        for id, data, codi, val in u.getAll(sql, "nodrizas"):
            hash = self.idsec_to_hash[id] if id in self.idsec_to_hash else None
            if hash in self.mares:
                agr = self.desc[codi]
                self.serologiaB.append([hash, data, codi, agr, val])
    
    def get_embarassos(self):
        """."""
        u.printTime("embarassos")
        codis_dx = []
        sql = """select criteri_codi from eqa_criteris where agrupador={}""".format(embaras)
        for cod, in u.getAll(sql, nod):
            codis_dx.append(cod)
        in_crit = tuple(codis_dx)

        self.embarassades = []
        sql = """select 
                    id_cip, pr_cod_ps, pr_dde, pr_dba, date_add(pr_dde, interval 42 week), date_format(pr_dde, '%Y%m%d'), date_format(pr_dba, '%Y%m%d'), date_format(date_add(pr_dde, interval 42 week), '%Y%m%d')
                from 
                    problemes 
                where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_data_baixa = 0 
                    and pr_cod_ps in {}""".format(in_crit)
        for id, pr_cod, dde, dba, fi2, dde2, dba2, fi22 in u.getAll(sql, imp):
            hash = self.id_to_hash[id] if id in self.id_to_hash else None
            if hash in self.mares:
                if dba == None:
                    dba = fi2
                if dba2 == None:
                    dba2 = fi22
                try:
                    dies = u.daysBetween(dde,dba)
                except:
                    dies = None
                if dies > 294:
                    dies = 294
                    dba = fi2
                    dba2 = fi22        
                self.embarassades.append([hash, pr_cod, dde, dba, dies])
    
    def export(self):
        """."""
        u.printTime("export")
        
        db = "hepatitisB"
        tb = "mares_embarassos"
        cols = ("hash varchar(40)", "ps varchar(40)", "embaras_ini date", "embaras_fi date", "dies_embaras int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.embarassades, tb, db)
        
        tb = "mares_serologies"
        cols = ("hash varchar(40)", "data date", "codi varchar(40)", "agrupador varchar(200)", "valor int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.serologiaB, tb, db)
        
        tb = "mares_vacunes"
        cols = ("hash varchar(40)", "vacuna varchar(100)","data date", "dosi int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.vac_hepatitis, tb, db)
        
        tb = "mares_hepatitis"
        cols = ("hash varchar(40)", "dde date", "dba date", "codi varchar(40)", "descripcio varchar(100)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.dx_hepatitis, tb, db)
        
        tb = "mares_poblacio"
        cols = ("hash varchar(40)", "up varchar(10)", "data_naix date", "sexe varchar(10)", "situacio varchar(10)", "data_situacio date", "nac varchar(40)", "nac_desc varchar(300)","nac_regio varchar(300)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.poblacio, tb, db)
        
        
        tb = "mares_cohort_inicial"
        cols = ("id int", "hash_covid varchar(40)", "hash_redics varchar(40)", "edat varchar(10)", "espanyola varchar(10)", "nacionalitat varchar(40)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.registres, tb, db)

        
if __name__ == '__main__':
    u.printTime("Inici")
    
    embarassos_hepH()

    u.printTime("Fi")