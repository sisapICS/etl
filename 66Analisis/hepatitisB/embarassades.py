# -*- coding: latin1 -*-

"""
Embarassades, vacunes hepatitis B, dx i serologies
"""

import collections as c
import psutil as p

import sisapUtils as u


tb = "cohort_embarassades"
db = "permanent"
nod = "nodrizas"
imp = "import"

ps = 13
serologia = 655
vacuna = 15
embaras = 39
    

class embarassos_hepH(object):
    """."""

    def __init__(self):
        """."""
        self.get_assignada()
        self.get_problemes()
        self.get_vacunes()
        self.get_serologies()
        self.get_embarassos()
        self.export()

    def get_assignada(self):
        """."""
        u.printTime("sociodemografic")
        
        self.poblacio = {}
        
        sql = """select 
                    id_cip_sec, usua_uab_up, usua_data_naixement, usua_sexe, usua_situacio, usua_data_situacio, usua_nacionalitat 
                from 
                    import.assignada"""
        for id, up, naix, sexe, situacio, dat_sit, nac in u.getAll(sql, imp):
            self.poblacio[id] = {'up': up, 'naix': naix, 'sexe': sexe, 'sit': situacio, 'data_sit': dat_sit, 'nac': nac}
        
    def get_problemes(self):
        """."""
        u.printTime("problemes")
        codis_dx = []
        sql = 'select criteri_codi from eqa_criteris where agrupador={}'.format(ps)
        for cod, in u.getAll(sql, nod):
            codis_dx.append(cod)
        in_crit = tuple(codis_dx)

        self.dx_hepatitis = {}
        sql = """select 
                    id_cip_sec, pr_dde, pr_dba
                from 
                    problemes 
                where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_data_baixa = 0
                    and pr_cod_ps in {}""".format(in_crit)
        for id, dde, dba in u.getAll(sql, imp):
            if id in self.dx_hepatitis:
                dde_2 = self.dx_hepatitis[id]['dde']
                if dde < dde_2:
                    self.dx_hepatitis[id] ={'dde': dde, 'dba': dba}
            else:
                self.dx_hepatitis[id] ={'dde': dde, 'dba': dba}
    
    def get_vacunes(self):
        """."""
        u.printTime("vacunes")
        
        codis_dx = []
        sql = 'select vacuna from nodrizas.eqa_criteris a inner join import.cat_prstb040_new cpn on criteri_codi=antigen where agrupador={}'.format(vacuna)
        for cod, in u.getAll(sql, nod):
            codis_dx.append(cod)
        in_crit = tuple(codis_dx)
        
        self.vac_hepatitis = c.defaultdict(set)
        sql = """select 
                    id_cip_sec, va_u_cod,  va_u_data_vac, va_u_dosi 
                from 
                    vacunes 
                where 
                    va_u_data_baixa=0 and va_u_cod in {}""".format(in_crit)
        for id, vacs, data, dosi in u.getAll(sql, imp):
            self.vac_hepatitis[id].add(data)
	
    def get_serologies(self):
        """."""
        u.printTime("serologia")
        codis_dx = []
        self.serologiaB = c.defaultdict(lambda: c.defaultdict(list))
        sql = """select codi, agrupador from cat_dbscat 
                              where taula = 'serologies' and agrupador like '%VHB%'"""
        for codi, agr in u.getAll(sql, imp):
            codis_dx.append(codi)
        in_crit = tuple(codis_dx)
        sql = "select id_cip_sec, dat, cod, val from nod_serologies where cod in {}""".format(in_crit)
        for id, data, codi, val in u.getAll(sql, "nodrizas"):
            self.serologiaB[id][data].append(val)

    
    def get_embarassos(self):
        """."""
        u.printTime("embarassos")
        codis_dx = []
        sql = 'select criteri_codi from eqa_criteris where agrupador={}'.format(embaras)
        for cod, in u.getAll(sql, nod):
            codis_dx.append(cod)
        in_crit = tuple(codis_dx)

        self.embarassades = []
        sql = """select 
                    id_cip, id_cip_sec, pr_dde, pr_dba, date_add(pr_dde, interval 42 week), date_format(pr_dde, '%Y%m%d'), date_format(pr_dba, '%Y%m%d'), date_format(date_add(pr_dde, interval 42 week), '%Y%m%d')
                from 
                    problemes 
                where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_data_baixa = 0 and year(pr_dde) in ('2018', '2019', '2020', '2021','2022')
                    and pr_cod_ps in {}""".format(in_crit)
        for id2, id, dde, dba, fi2, dde2, dba2, fi22 in u.getAll(sql, imp):
            naix = self.poblacio[id]['naix'] if id in self.poblacio else ''
            up = self.poblacio[id]['up'] if id in self.poblacio else ''
            sexe = self.poblacio[id]['sexe'] if id in self.poblacio else ''
            sit = self.poblacio[id]['sit'] if id in self.poblacio else ''
            dsit = self.poblacio[id]['data_sit'] if id in self.poblacio else ''
            nac = self.poblacio[id]['nac'] if id in self.poblacio else ''
            vac_e = 0
            if dba == None:
                dba = fi2
            if dba2 == None:
                dba2 = fi22
            try:
                dies = u.daysBetween(dde,dba)
            except:
                dies = None
            if dies > 294:
                dies = 294
                dba = fi2
                dba2 = fi22                
            if id in self.vac_hepatitis:
                for dat in self.vac_hepatitis[id]:
                    if dat <= dde:
                        vac_e = 1
            nserol = 0
            npositius = 0
            if id in self.serologiaB:
                for dat2 in self.serologiaB[id]:
                    for val in self.serologiaB[id][dat2]:
                        if dde2 <= dat2 <= dba2:
                            nserol += 1
                        if val == 1:
                            npositius = 1
            dx_hep_ini = self.dx_hepatitis[id]['dde'] if id in self.dx_hepatitis else ''
            dx_hep_fi = self.dx_hepatitis[id]['dba'] if id in self.dx_hepatitis else ''
            
            self.embarassades.append([id, up, naix, sexe, sit, dsit, nac, dde, dba, dies, vac_e, dx_hep_ini, dx_hep_fi, nserol, npositius])
    
    def export(self):
        """."""
        u.printTime("export")
 
        cols = ("id_cip_SEC varchar(40)", "up varchar(5)", "naix date", "sexe varchar(10)", "sit varchar(10)", "data_situacio date",
            "nacionalitat varchar(10)", "embaras_ini date", "embaras_fi date", "dies_embaras int", "vacuna int", "dx_hep_ini date", "dx_hep_fi date", "n_serologies int", "n_positius int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.embarassades, tb, db)
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    embarassos_hepH()

    u.printTime("Fi")
