# -*- coding: latin1 -*-

"""
Fills mare portadora hepatitis B
"""

import collections as c
import psutil as p

import difflib as s

import sisapUtils as u


tb = "permanent"
cas_file = u.tempFolder + "fitxer_mares.txt"
   

class fillsMareVHB(object):
    """."""

    def __init__(self):
        """."""
        self.get_poblacio()
        self.getMares()
        self.export()
    
        
    def get_poblacio(self):
        """obtenim poblacio RCA i els no RCA de ecap"""
        u.printTime("poblacio")
        self.id_cip = {}
        self.id_cip13 = {}
        sql = """SELECT hash, cip, substr(cip, 0, 13), dni, data_naixement, data_defuncio, case when sexe=0 then 'H' when sexe=1 then 'M' else '' end, situacio, nacionalitat,  abs, eap, up_residencia  FROM dwsisap.RCA_CIP_NIA rcn """
        for hash, cip, cip13, dni, naix, defuncio, sexe, situacio, nac, abs, eap, up in u.getAll(sql, 'exadata'):
            self.id_cip[cip] = {'hash': hash, 'naix': naix, 'defuncio': defuncio, 'sexe': sexe, 'situacio': situacio, 'nac': nac, 'abs': abs, 'eap': eap, 'up': up}
            self.id_cip13[cip13] = {'hash': hash, 'naix': naix, 'defuncio': defuncio, 'sexe': sexe, 'situacio': situacio, 'nac': nac, 'abs': abs, 'eap': eap, 'up': up}
        
        sql = """SELECT hash, cip, substr(cip, 0, 13), document, data_naixement, case when sexe='D' then  'M' else sexe end, situacio, nacionalitat, ecap_abs, ecap_up FROM dwsisap.dbc_poblacio WHERE es_rca=0"""
        for hash, cip, cip13, dni, naix, sexe, situacio, nac, abs,  up in u.getAll(sql, 'exadata'):
            self.id_cip[cip] = {'hash': hash, 'naix': naix, 'defuncio': None, 'sexe': sexe, 'situacio': situacio, 'nac': nac, 'abs': abs, 'eap': None, 'up': up}
            self.id_cip13[cip13] = {'hash': hash, 'naix': naix, 'defuncio': None, 'sexe': sexe, 'situacio': situacio, 'nac': nac, 'abs': abs, 'eap': None, 'up': up}
    
    def getMares(self):
        """Busquem les mares primer"""
        u.printTime("mares")

        self.mares = []
        m = 0
        n = 0
        for (id, nom_nen, cognom1_nen, cognom_nen, naix_nen, cip_mare, edat_mare, nac_mare, part, multiple, nbessons, setmana_g, pes_neixer ) in u.readCSV(cas_file, sep='@'):
            m += 1
            if cip_mare in self.id_cip:
                hash = self.id_cip[cip_mare]['hash']
            elif cip_mare in self.id_cip13:
                hash = self.id_cip13[cip_mare]['hash']
            else:
                hash = None
            self.mares.append([id, cip_mare, hash])
            if hash != None:
                n += 1
                        
        print m, n    

    def export(self):
        """exportem les taules"""
        u.printTime("export")
        
        db = "permanent"
        tb = "hepB_mares"
        columns = ["id int, cip varchar(40), hash_covid varchar(40)"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.mares, tb, db)        

        
if __name__ == '__main__':
    u.printTime("Inici")
    
    fillsMareVHB()

    u.printTime("Fi")