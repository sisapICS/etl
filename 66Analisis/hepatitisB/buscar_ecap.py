# -*- coding: latin1 -*-

"""
Buscar a ecap cips que no estan al RCA
"""

import collections as c
import psutil as p

import difflib as s

import sisapUtils as u



cas_file = u.tempFolder + "vhb_pacients_norca.txt"

rescat = u.tempFolder + "vhb_pacients_rescat.txt"
   

class buscar_noms(object):
    """."""

    def __init__(self):
        """."""
        self.get_cip()
        self.get_hash()
        self.get_fitxer()
        self.export()
    
        
    def get_cip(self):
        """
        busquem cips
        """
        u.printTime("Sectors")
        self.persones = {}
        for sector in u.sectors:
            print sector
            sql = """
                SELECT
                    cip_cip_anterior, cip_usuari_cip
                FROM
                    usutb011
            """
            for cip_a, cip in u.getAll(sql, sector):
                self.persones[cip_a] = {'cip':cip, 'sector':sector}
    
    def get_hash(self):
        """cip to hash"""
        u.printTime("hash")
        self.cip_to_hash = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.cip_to_hash[cip] = hash
    
   
    def get_fitxer(self):
        """
        busquem noms
        """
        u.printTime("Busquem Mares")
        self.mares = []
        self.trobades = []
        for (id, cip1, hash_covid, hash_redics, mare_edat, mare_nac1, mare_nac) in u.readCSV(cas_file, sep=';'):
            cip, sector = None, None
            if cip1 in self.persones:
                cip = self.persones[cip1]['cip']
                sector = self.persones[cip1]['sector']
            cip2 = self.persones[cip]['cip'] if cip in self.persones else cip1
            hash =  self.cip_to_hash[cip2] if cip2 in self.cip_to_hash else None
            self.mares.append([id, hash_covid, hash])
            if hash != None:
                self.trobades.append([id, cip1, hash_covid, hash, mare_edat, mare_nac1, mare_nac])
    
    def export(self):
        """exportem les taules"""
        u.printTime("export")
        
        db = "hepatitisB"
        tb = "mares_rescat"
        columns = ["id int",  "hash_covid varchar(40)", "hash_redics varchar(40)"]
        #u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        #u.listToTable(self.mares, tb, db)

        u.writeCSV(rescat, self.trobades, sep=';')        

if __name__ == '__main__':
    u.printTime("Inici")
    
    buscar_noms()

    u.printTime("Fi")