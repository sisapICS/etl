# -*- coding: latin1 -*-

"""
Buscar a ecap cips que no estan al RCA dels nadons
"""

import collections as c
import psutil as p

import difflib as s

import sisapUtils as u



cas_file = u.tempFolder + "vhb_nens_notrobats.txt"

rescat = u.tempFolder + "vhb_nadons_rescat.txt"
   

class buscar_noms(object):
    """."""

    def __init__(self):
        """."""
        self.get_cip()
        self.get_hash()
        self.get_fitxer()
        self.export()
    
        
    def get_cip(self):
        """
        busquem cips
        """
        u.printTime("Sectors")
        self.persones = {}
        self.anteriors = c.defaultdict(set)
        for sector in u.sectors:
            print sector
            sql = """
                SELECT
                    substr(cip_cip_anterior, 0, 11), cip_cip_anterior, substr(cip_usuari_cip, 0, 11), cip_usuari_cip
                FROM
                    usutb011
            """
            for cip_a11, cip_a, cip11, cip in u.getAll(sql, sector):
                self.anteriors[cip_a11].add(cip_a)
                self.persones[cip_a] = {'cip':cip, 'sector':sector}
    
    def get_hash(self):
        """cip to hash"""
        u.printTime("hash")
        self.cip_to_hash = {}
        
        sql = """select usua_cip_cod,  usua_cip
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.cip_to_hash[cip] = hash
    
   
    def get_fitxer(self):
        """
        busquem noms
        """
        u.printTime("Busquem Nadons")
        self.nadons = []
        self.trobades = []
        for (id, cip1) in u.readCSV(cas_file, sep=';'):
            cip, sector, hash = None, None, None
            if cip1 in self.anteriors:
                for cip13 in self.anteriors[cip1]:
                    cip = self.persones[cip13]['cip']
                    sector = self.persones[cip13]['sector']
                    cip2 = self.persones[cip]['cip'] if cip in self.persones else cip13
                    hash =  self.cip_to_hash[cip2] if cip2 in self.cip_to_hash else None
                    self.nadons.append([id, hash])
                    if hash != None:
                        self.trobades.append([id, hash])
    
    def export(self):
        """exportem les taules"""
        u.printTime("export")
        
        db = "hepatitisB"
        tb = "nadons_rescat"
        columns = ["id int",  "hash_redics varchar(40)"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.nadons, tb, db)

        u.writeCSV(rescat, self.trobades, sep=';')        

if __name__ == '__main__':
    u.printTime("Inici")
    
    buscar_noms()

    u.printTime("Fi")