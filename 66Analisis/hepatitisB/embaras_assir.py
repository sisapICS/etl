# -*- coding: latin1 -*-

"""
Fills mare portadora hepatitis B
"""

import collections as c
import psutil as p

import difflib as s

import sisapUtils as u


cas_file = u.tempFolder + "vhb_pacients_rca.txt"
rescat = u.tempFolder + "vhb_pacients_rescat.txt"

db = "permanent"
nod = "nodrizas"
imp = "import"



class embarassos_hepH(object):
    """."""

    def __init__(self):
        """."""
        self.get_hash()
        self.getMares()
        self.get_embarassos()
        self.export()
    
        
    def get_hash(self):
        """hash to id_cip_sec"""
        u.printTime("id_cip_sec")
        self.id_to_hash = {}
        self.idsec_to_hash = {}
        
        sql = """select 
                    id_cip, id_cip_sec, hash_d, codi_sector 
                from 
                    u11"""
        for id, id_sec, hash, sec in u.getAll(sql, 'import'):
            self.id_to_hash[id] = hash
            self.idsec_to_hash[id_sec] = hash
            
    def getMares(self):
        """Busquem les mares primer"""
        u.printTime("mares")

        self.mares = {}

        for (id,cip13,hash_covid,hash_redics,mare_edat,mare_nacionalitat_esp,mare_nacionalitat) in u.readCSV(cas_file, sep=';'):
            self.mares[hash_redics] = True
            
        for (id,cip13,hash_covid,hash_redics,mare_edat,mare_nacionalitat_esp,mare_nacionalitat) in u.readCSV(rescat, sep=';'):
            self.mares[hash_redics] = True
            
    def get_embarassos(self):
        """."""
        u.printTime("embarassos")

        self.embarassades = []
        sql = """select 
                    id_cip_sec, inici, fi, temps, risc, puerperi
                from
                    ass_embarasB"""
        for id, inici, fi, temps, risc, puerperi in u.getAll(sql, nod):
            hash = self.idsec_to_hash[id] if id in self.idsec_to_hash else None
            if hash in self.mares:     
                self.embarassades.append([hash, inici, fi, temps, risc, puerperi])
                
    def export(self):
        """."""
        u.printTime("export")
        
        db = "hepatitisB"
        tb = "mares_embaras_assir"
        cols = ("hash varchar(40)", "inici date", "fi date", "temps int", "risc varchar(40)", "puerperi int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.embarassades, tb, db)
        
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    embarassos_hepH()

    u.printTime("Fi")
    