import re
import sisapUtils as u 
import sisaptools as t
import collections as c

noms = c.Counter()
sql = "select usua_nom, usua_sexe, count(*) as n from usutb040 group by usua_nom, usua_sexe"
for sector in u.sectors:
    print sector
    for nom, sexe, n in u.getAll(sql, sector):
        if sexe in ('D', 'H'):
            nom = re.sub('\s+', ' ', nom.lstrip().rstrip())
            noms[(nom, sexe)] += n
            noms_sexe = c.defaultdict(dict)
            for k, v in noms.items():
                nom = k[0]
                sexe = k[1]
                noms_sexe[nom][sexe] = v

upload = []
for nom in noms_sexe:
    for sexe in noms_sexe[nom]:
        rec = noms_sexe[nom][sexe]
        upload.append([nom,sexe,rec])
        
tb = "sexe_by_nom"
db = "hepatitisB"
cols = ("nom varchar(100)", "sexe varchar(10)","recompte int")
u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
u.listToTable(self.upload, tb, db)