# -*- coding: latin1 -*-

"""
Buscar per nom
"""

import collections as c
import psutil as p

import difflib as s

import sisapUtils as u


tb = "T"
cas_file = u.tempFolder + "nadons_complet.txt"
   

class buscar_noms(object):
    """."""

    def __init__(self):
        """."""
        #self.get_hash()
        #self.get_id_cip_sec()
        #self.get_assignada()
        self.get_persona()
        self.get_fitxer()
    
        
    def get_hash(self):
        """cip to hash"""
        u.printTime("hash")
        self.cip_to_hash = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.cip_to_hash[cip] = hash
    
    def get_id_cip_sec(self):
        """hash to id_cip_sec"""
        u.printTime("id_cip_sec")
        self.hash_to_id = {}
        
        sql = """select 
                    id_cip, hash_d, codi_sector 
                from 
                    u11"""
        for id, hash, sec in u.getAll(sql, 'import'):
            self.hash_to_id[(hash)] = id
            
    def get_assignada(self):
        """Assignada"""
        u.printTime("assignada")
        self.poblacio = {}
        
        sql = """select id_cip, up, uba  from assignada_tot"""
        for id, up, uba in u.getAll(sql, 'nodrizas'):
            self.poblacio[id] = {'up': up, 'uba': uba}
    
    def get_persona(self):
        """
        busquem noms
        """
        u.printTime("Sectors")
        self.persones = {}
        self.anys_persones = c.defaultdict(lambda: c.defaultdict(list))
        for sector in u.sectors:
            print sector
            sql = """
                SELECT
                    usua_dni, usua_nom, usua_cognom1, usua_cognom2,
                    usua_uab_up, usua_situacio, usua_cip, usua_usuari,  to_char(to_date(usua_data_naixement, 'J'), 'YYMMDD')
                FROM
                    usutb040
                where rownum < 2
            """
            for id, nom, cog1, cog2, up, sit, cip, usuari, naix in u.getAll(sql, sector):
                print id, nom, cog1, cog2, up, sit, cip, usuari, naix
                key =  str(nom) +  str(cog1) + str(cog2) + str(naix)
                print key
                self.persones[key] = cip
                #self.anys_persones[anys].append(key)

    def get_fitxer(self):
        """
        busquem noms
        """
        u.printTime("Busquem noms")
        for (cog1, cog2, nom, anys, mes, dia) in u.readCSV(cas_file, sep='@'):
            key = nom.upper() + cog1.upper() + cog2.upper() + str(anys) + str(mes) + str(dia)
            print key
            if key in self.persones:
                cip = self.persones[key]
                hash = self.cip_to_hash[cip] if cip in self.cip_to_hash else None
                id = self.hash_to_id[(hash)] if (hash) in self.hash_to_id else None
                up = self.poblacio[id]['up'] if id in self.poblacio else None
                uba = self.poblacio[id]['uba'] if id in self.poblacio else None
                print nom, cog1, cog2, cip, hash, id,  up, uba
            else:
                u.printTime("aproximat")
                if anys in self.anys_persones:
                    for persona in self.anys_persones[anys]:
                        perc = s.SequenceMatcher(None, key, persona)
                        perc_ = perc.ratio()
                        if perc_ > 0.8:
                            cip = self.persones[persona]
                            hash = self.cip_to_hash[cip] if cip in self.cip_to_hash else None
                            id = self.hash_to_id[(hash)] if (hash) in self.hash_to_id else None
                            up = self.poblacio[id]['up'] if id in self.poblacio else None
                            uba = self.poblacio[id]['uba'] if id in self.poblacio else None
                            print  perc_, nom, cog1, cog2, cip, hash, id,  up, uba
                else:
                    u.printTime("aproximat 2")
                    for usu, dad in self.persones.items():
                        perc = s.SequenceMatcher(None, key, usu)
                        perc_ = perc.ratio()
                        if perc_ > 0.8:
                            cip = self.persones[usu]
                            hash = self.cip_to_hash[cip] if cip in self.cip_to_hash else None
                            id = self.hash_to_id[(hash)] if (hash) in self.hash_to_id else None
                            up = self.poblacio[id]['up'] if id in self.poblacio else None
                            uba = self.poblacio[id]['uba'] if id in self.poblacio else None
                            print  perc_, nom, cog1, cog2, cip, hash, id,  up, uba
                        
                    

        
if __name__ == '__main__':
    u.printTime("Inici")
    
    buscar_noms()

    u.printTime("Fi")