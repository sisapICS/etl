# -*- coding: latin1 -*-

"""
Buscar per nom
"""

import collections as c
import psutil as p

import difflib as s

import sisapUtils as u


cas_file = u.tempFolder + "nadons_complet.txt"
   

class buscar_noms(object):
    """."""

    def __init__(self):
        """."""
        self.get_hash()
        self.get_persona()
        self.get_fitxer()
        self.export()
    
        
    def get_hash(self):
        """cip to hash"""
        u.printTime("hash")
        self.cip_to_hash = {}
        
        sql = """select usua_cip_cod, usua_cip 
                from pdptb101"""
        for hash, cip in u.getAll(sql, 'pdp'):
            self.cip_to_hash[cip] = hash
    
    def get_persona(self):
        """
        busquem noms
        """
        u.printTime("Sectors")
        self.persones = {}
        self.anys_persones = c.defaultdict(lambda: c.defaultdict(list))
        for sector in u.sectors:
            print sector
            sql = """
                SELECT
                    usua_dni, usua_nom, usua_cognom1, usua_cognom2,
                    usua_uab_up, usua_situacio, usua_cip, usua_usuari,  to_char(to_date(usua_data_naixement, 'J'), 'YYMMDD'), to_char(to_date(usua_data_naixement, 'J'), 'YYYY')
                FROM
                    usutb040
            """
            for id, nom, cog1, cog2, up, sit, cip, usuari, naix, anys in u.getAll(sql, sector):
                if anys in ('2019', '2020', '2021','2022'):
                    key =  str(nom) +  str(cog1) + str(cog2) + str(naix)
                    self.persones[key] = cip
                    #self.anys_persones[anys].append(key)

    def get_fitxer(self):
        """
        busquem noms
        """
        self.noms = []
        u.printTime("Busquem noms")
        for (id, nom, cog1, cog2, anys, mes, dia) in u.readCSV(cas_file, sep='@'):
            key = nom.upper() + cog1.upper() + cog2.upper() + str(anys) + str(mes) + str(dia)
            cip, hash = None, None
            if key in self.persones:
                cip = self.persones[key]
                hash = self.cip_to_hash[cip] if cip in self.cip_to_hash else None
            self.noms.append([id, cip, hash])

    def export(self):
        """."""
        u.printTime("export")
        
        db = "hepatitisB"

        tb = "nadons_creuats"
        cols = ("id int", "cip varchar(40)", "hash varchar(40)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.noms, tb, db)                      

        
if __name__ == '__main__':
    u.printTime("Inici")
    
    buscar_noms()

    u.printTime("Fi")