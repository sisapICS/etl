### Model d'ajust per la pregunta P101 Grau de satisfacció global 

```{r}
dades_dm_br_khalix_korra_overall <- dades_dm_br_khalix_korra[indicadors %in% c("AGENDQC1_ANUAL", "CONT0002A", "QACC2D_ANUAL", "QECONS0000_ANUAL", "VISUBA_ANUAL", "VISURG_ANUAL", "VVIRTUALS_ANUAL", "I4CW"), lapply(.SD, sum), .SDcols = c("NUM", "DEN"), by = c("ics_codi", "indicadors", c(paste0("P", 1:23), "P101", "P102"), "medea", "aquas", "poblacio", "edat_mitjana", "dona", "institucionalitzats_perc", "immigrants", "pensionistes_perc", "pens_menys_65_perc", "pcc_perc", "maca_perc", "gma", "prof_grup9_perc", "prof_grup2_perc", "abs")]
dades_dm_br_khalix_korra_overall[, RESULTAT := NUM/DEN]

dades_dm_br_khalix_korra_overall_dcast <- dcast(dades_dm_br_khalix_korra_overall[, -c("NUM", "DEN")], as.formula(paste0("ics_codi + abs + ", paste0("P", c(1:23, "101", "102"), collapse = " + "), " + medea + aquas + poblacio + edat_mitjana + dona + institucionalitzats_perc + immigrants + pensionistes_perc + pens_menys_65_perc + pcc_perc + maca_perc + gma  + prof_grup9_perc + prof_grup2_perc ~ indicadors")), value.var = "RESULTAT")

dades_dm_br_i4cw <- merge(dades_dm_br[, .SD, .SDcols = c("ics_codi", "scs_codi")], ind_4cw[, .SD, .SDcols = c("UP", "I4CW")], by.x = "scs_codi", by.y = "UP", all.y = T)

dades_dm_br_khalix_korra_overall_dcast_i4cw <- merge(dades_dm_br_khalix_korra_overall_dcast, dades_dm_br_i4cw, by = "ics_codi", all.x = T)

# dades_dm_br_khalix_korra_overall_dcast_i4cw_melt <- melt(dades_dm_br_khalix_korra_overall_dcast_i4cw, id.vars = c("ics_codi", "scs_codi", "abs", paste0("P", c(1:23, 101, 102)), "medea", "aquas", "poblacio", "edat_mitjana", "dona", "institucionalitzats_perc", "immigrants", "pensionistes_perc", "pens_menys_65_perc", "pcc_perc", "maca_perc", "gma", "prof_grup9_perc", "prof_grup2_perc"))
# 
# dades_model <- dades_dm_br_khalix_korra_overall_dcast_i4cw_melt

dades_model <- dades_dm_br_khalix_korra_overall_dcast_i4cw[, .SD, .SDcols = setdiff(names(dades_dm_br_khalix_korra_overall_dcast_i4cw), paste0("P", c(1:23, 102)))]
```


```{r}
ggplot(dades_model, aes(x = P101)) +
  geom_histogram(fill = "#A9C7B0", color = "grey30") +
  theme_minimal() +
  labs(title = "Distribució de la pregunta P101 Grau de satisfacció global", x = "", y = "")
```




```{r fig.height=12, fig.width=12}
dades_model <- dades_dm_br_khalix_korra_overall_dcast_i4cw[, .SD, .SDcols = setdiff(names(dades_dm_br_khalix_korra_overall_dcast_i4cw), paste0("P", c(1:23, 102)))]

dades_model <- merge(dades_model, eqa, by.x = "ics_codi", by.y = "codi")
M <- cor(dades_model[, .SD, .SDcols = setdiff(names(dades_model), c("ics_codi", "abs", "scs_codi", "medea"))], use = "pairwise.complete.obs")

corrplot(M, method = 'number', tl.col = "black") # colorful number
```

```{r}
dades_model[, rural := ifelse(medea %like% "R", "Rural", "Urbà")]

```


```{r}
model <- lm(P101 ~ rural + aquas + pens_menys_65_perc + gma + AGENDQC1_ANUAL + CONT0002A + QACC2D_ANUAL + QECONS0000_ANUAL + I4CW + VISUBA_ANUAL, data = dades_model)
summary(model)
```
























































<!-- ```{r} -->
<!-- names(dades_dm_br_i4cw)[c(3, 4)] <- c("NUM", "DEN") -->
<!-- dades_dm_br_i4cw[, indicadors := "I4CW"] -->
<!-- dades_entrega <- rbind(dades_dm_br_khalix_korra_overall[, c("ics_codi", "indicadors", "NUM", "DEN")], -->
<!--                        dades_dm_br_i4cw[, c("ics_codi", "indicadors", "NUM", "DEN")]) -->
<!-- dades_entrega <- merge(dades_entrega, cat_centres[, c("abs", "ics_codi")], all.x = T) -->
<!-- dades_entrega <- dades_entrega[, -"ics_codi"] -->


<!-- dades_entrega[indicadors == "AGENDQC1_ANUAL", nom_indicadors := "Oferta visites presencials"] -->
<!-- dades_entrega[indicadors == "CONT0002A", nom_indicadors := "Longitudinalitat"] -->
<!-- dades_entrega[indicadors == "QACC2D_ANUAL", nom_indicadors := "Accessibilitat"] -->
<!-- dades_entrega[indicadors == "QECONS0000_ANUAL", nom_indicadors := "Percentatge visites eConsulta"] -->
<!-- dades_entrega[indicadors == "VISUBA_ANUAL", nom_indicadors := "Percentatge visites agenda UBA"] -->
<!-- dades_entrega[indicadors == "VISURG_ANUAL", nom_indicadors := "Percentatge visites agenda urgències"] -->
<!-- dades_entrega[indicadors == "VVIRTUALS_ANUAL", nom_indicadors := "Percentatge visites virtuals"] -->
<!-- dades_entrega[indicadors == "I4CW", nom_indicadors := "Percentatge visites 4CW"] -->

<!-- fwrite(dades_entrega[!is.na(abs) & !is.na(nom_indicadors), c("abs", "indicadors", "nom_indicadors", "NUM", "DEN")], "../dades/dades_entega_catsalut.csv", sep = ";", dec = ".", row.names = F) -->
<!-- ``` -->




<!-- ```{r} -->
<!-- vars_explicatives <- c("AGENDQC1_MENSUAL", "CONT0002A", "QACC5D_MENSUAL", "QECONS0000_ANUAL",  "VISUBA_MENSUAL", "VISURG_MENSUAL", "VVIRTUALS_ANUAL") -->

<!-- dades_dm_br_khalix_dcast <- dcast(dades_dm_br_khalix, ics_codi + abs + P101 + P2 ~ indicadors, value.var = "RESULTAT") -->
<!-- dades_model <- dades_dm_br_khalix_dcast[, .SD, .SDcols = c("ics_codi", "abs", "P2", "P101", vars_explicatives)] -->

<!-- ``` -->

<!-- ##### Rànking de la importància de les variables explicatives del model lineal -->
<!-- ###### Model complert -->

<!-- ```{r include=FALSE} -->
<!-- lm_p2 <- lm(P2 ~ ., data = dades_model[, .SD, .SDcols = c("P2", vars_explicatives)]) -->
<!-- # summary(lm_p2) -->
<!-- varimp_p2 <- as.data.table(varImp(lm_p2), keep.rownames = T)  -->

<!-- lm_p101 <- lm(P101 ~ ., data = dades_model[, .SD, .SDcols = c("P101", vars_explicatives)]) -->
<!-- # summary(lm_p101) -->
<!-- varimp_p101 <- as.data.table(varImp(lm_p101), keep.rownames = T)  -->

<!-- varimp <- merge(varimp_p2, varimp_p101, by = "rn") -->
<!-- colnames(varimp)[c(2, 3)] <- c("P2", "P101") -->
<!-- ``` -->

<!-- ```{r fig.height=6, fig.width=6, message=FALSE, warning=FALSE} -->
<!-- dg <- melt(varimp, id.vars = c("rn"), variable.name = "aaa") -->
<!-- dg[, ordre := factor(rn, levels = dg[order(aaa, value, decreasing = F)][aaa == "P101"]$rn)] -->
<!-- ggplot(dg, aes(x = aaa, y = ordre, fill = value)) +  -->
<!--   geom_tile() + -->
<!--   geom_text(aes(label = format(value, digits = 2)), color = "white", size = 3) + -->
<!--   scale_fill_gradient2(low = "#A50B44", mid = "white", high = "#304A36") + -->
<!--   theme_minimal() + -->
<!--   theme( -->
<!--     # axis.text.x = element_blank(), -->
<!--     axis.ticks.x = element_blank(), -->
<!--     panel.grid.major.y = element_blank(), -->
<!--     panel.grid.minor.y = element_blank(), -->
<!--     panel.grid.major.x = element_blank(), -->
<!--     panel.grid.minor.x = element_blank(), -->
<!--     legend.position = "none", -->
<!--     plot.title = element_text(hjust = 1), -->
<!--     plot.caption = element_text(hjust = 1) -->
<!--   ) + -->
<!--    labs(title = "", x = "", y = "", color = "", linetype = "", fill = expression(R^2)) -->
<!-- ``` -->

<!-- ###### Model final -->

<!-- ```{r include=FALSE} -->
<!-- lm_p2 <- lm(P2 ~ ., data = dades_model[, .SD, .SDcols = setdiff(c("P2", vars_explicatives), c("VISURG", "VVIRTUALS"))]) -->
<!-- # summary(lm_p2) -->
<!-- varimp_p2 <- as.data.table(varImp(lm_p2), keep.rownames = T)  -->

<!-- lm_p101 <- lm(P101 ~ ., data = dades_model[, .SD, .SDcols = setdiff(c("P101", vars_explicatives), c("VISURG", "QACC2D", "VVIRTUALS"))]) -->
<!-- summary(lm_p101) -->
<!-- varimp_p101 <- as.data.table(varImp(lm_p101), keep.rownames = T)  -->

<!-- varimp <- merge(varimp_p2, varimp_p101, by = "rn", all = T) -->
<!-- colnames(varimp)[c(2, 3)] <- c("P2", "P101") -->
<!-- ``` -->

<!-- ```{r fig.height=6, fig.width=6, message=FALSE, warning=FALSE} -->
<!-- dg <- melt(varimp, id.vars = c("rn"), variable.name = "aaa") -->
<!-- dg[, ordre := factor(rn, levels = dg[order(aaa, value, decreasing = F, na.last = F)][aaa == "P101"]$rn)] -->
<!-- ggplot(dg, aes(x = aaa, y = ordre, fill = value)) +  -->
<!--   geom_tile() + -->
<!--   geom_text(aes(label = format(value, digits = 2)), color = "white", size = 3) + -->
<!--   scale_fill_gradient2(low = "#A50B44", mid = "white", high = "#304A36", na.value = "white") + -->
<!--   theme_minimal() + -->
<!--   theme( -->
<!--     # axis.text.x = element_blank(), -->
<!--     axis.ticks.x = element_blank(), -->
<!--     panel.grid.major.y = element_blank(), -->
<!--     panel.grid.minor.y = element_blank(), -->
<!--     panel.grid.major.x = element_blank(), -->
<!--     panel.grid.minor.x = element_blank(), -->
<!--     legend.position = "none", -->
<!--     plot.title = element_text(hjust = 1), -->
<!--     plot.caption = element_text(hjust = 1) -->
<!--   ) + -->
<!--    labs(title = "", x = "", y = "", color = "", linetype = "", fill = expression(R^2)) -->
<!-- ``` -->

<!-- ###### Model final ajustat per variables estructurals dels EAP -->

<!-- - Mitjana d'edat de la població atesa adulta. -->
<!-- - Percentatge de dones de la població atesa adulta. -->
<!-- - Percentatge d'immigrants de la població atesa adulta. -->

<!-- ```{r} -->
<!-- dades_dm_br_khalix_korra_dcast <- dcast(dades_dm_br_khalix_korra, ics_codi + abs + P101 + P2 + dona + edat_mitjana + immigrants ~ indicadors, value.var = "RESULTAT") -->


<!-- dades_model_ajustat <- dades_dm_br_khalix_korra_dcast[, .SD, .SDcols = c("ics_codi", "abs", "P2", "P101", vars_explicatives, "dona", "edat_mitjana", "immigrants")] -->

<!-- ``` -->

<!-- ```{r include=FALSE} -->
<!-- lm_p2 <- lm(P2 ~ ., data = dades_model_ajustat[, .SD, .SDcols = setdiff(c("P2", vars_explicatives, "dona", "edat_mitjana", "immigrants"), c("VVIRTUALS", "dona", "VISURG"))]) -->
<!-- summary(lm_p2) -->
<!-- varimp_p2 <- as.data.table(varImp(lm_p2), keep.rownames = T)  -->

<!-- lm_p101 <- lm(P101 ~ ., data = dades_model_ajustat[, .SD, .SDcols = setdiff(c("P101", vars_explicatives, "dona", "edat_mitjana", "immigrants"), c("QACC2D", "VISURG", "VVIRTUALS"))]) -->
<!-- summary(lm_p101) -->
<!-- varimp_p101 <- as.data.table(varImp(lm_p101), keep.rownames = T)  -->

<!-- varimp <- merge(varimp_p2, varimp_p101, by = "rn", all = T) -->
<!-- colnames(varimp)[c(2, 3)] <- c("P2", "P101") -->
<!-- ``` -->

<!-- ```{r fig.height=6, fig.width=6, message=FALSE, warning=FALSE} -->
<!-- dg <- melt(varimp, id.vars = c("rn"), variable.name = "aaa") -->
<!-- dg[, ordre := factor(rn, levels = dg[order(aaa, value, decreasing = F, na.last = F)][aaa == "P101"]$rn)] -->
<!-- ggplot(dg, aes(x = aaa, y = ordre, fill = value)) +  -->
<!--   geom_tile() + -->
<!--   geom_text(aes(label = format(value, digits = 2)), color = "white", size = 3) + -->
<!--   scale_fill_gradient2(low = "#A50B44", mid = "white", high = "#304A36", na.value = "white") + -->
<!--   theme_minimal() + -->
<!--   theme( -->
<!--     # axis.text.x = element_blank(), -->
<!--     axis.ticks.x = element_blank(), -->
<!--     panel.grid.major.y = element_blank(), -->
<!--     panel.grid.minor.y = element_blank(), -->
<!--     panel.grid.major.x = element_blank(), -->
<!--     panel.grid.minor.x = element_blank(), -->
<!--     legend.position = "none", -->
<!--     plot.title = element_text(hjust = 1), -->
<!--     plot.caption = element_text(hjust = 1) -->
<!--   ) + -->
<!--    labs(title = "", x = "", y = "", color = "", linetype = "", fill = expression(R^2)) -->
<!-- ``` -->

<!-- ```{r include=FALSE} -->
<!-- P2 <- dades_model_ajustat[, .( -->
<!--   Variables = names(coefficients(lm(P2 ~ ., data = dades_model_ajustat[, .SD, .SDcols = setdiff(c("P2", vars_explicatives, "dona", "edat_mitjana", "immigrants"), c("VVIRTUALS", "dona", "VISURG"))]))), -->
<!--   P2 = coefficients(lm(P2 ~ ., data = dades_model_ajustat[, .SD, .SDcols = setdiff(c("P2", vars_explicatives, "dona", "edat_mitjana", "immigrants"), c("VVIRTUALS", "dona", "VISURG"))])), -->
<!--   P2_inf = confint(lm(P2 ~ ., data = dades_model_ajustat[, .SD, .SDcols = setdiff(c("P2", vars_explicatives, "dona", "edat_mitjana", "immigrants"), c("VVIRTUALS", "dona", "VISURG"))]))[, 1], -->
<!--   P2_sup = confint(lm(P2 ~ ., data = dades_model_ajustat[, .SD, .SDcols = setdiff(c("P2", vars_explicatives, "dona", "edat_mitjana", "immigrants"), c("VVIRTUALS", "dona", "VISURG"))]))[, 2] -->
<!-- )] -->

<!-- P101<- dades_model_ajustat[, .( -->
<!--   Variables = names(coefficients(lm(P101 ~ ., data = dades_model_ajustat[, .SD, .SDcols = setdiff(c("P101", vars_explicatives, "dona", "edat_mitjana", "immigrants"), c("QACC2D", "VISURG", "VVIRTUALS"))]))), -->
<!--   P101 = coefficients(lm(P101 ~ ., data = dades_model_ajustat[, .SD, .SDcols = setdiff(c("P101", vars_explicatives, "dona", "edat_mitjana", "immigrants"), c("QACC2D", "VISURG", "VVIRTUALS"))])), -->
<!--   P101_inf = confint(lm(P101 ~ ., data = dades_model_ajustat[, .SD, .SDcols = setdiff(c("P101", vars_explicatives, "dona", "edat_mitjana", "immigrants"), c("QACC2D", "VISURG", "VVIRTUALS"))]))[, 1], -->
<!--   P101_sup = confint(lm(P101 ~ ., data = dades_model_ajustat[, .SD, .SDcols = setdiff(c("P101", vars_explicatives, "dona", "edat_mitjana", "immigrants"), c("QACC2D", "VISURG", "VVIRTUALS"))]))[, 2] -->
<!-- )] -->

<!-- coeficients <- merge(P2, P101, by = "Variables", all = T)  -->
<!-- ``` -->

<!-- ```{r} -->
<!-- options(knitr.kable.NA = ' ') -->
<!-- coeficients[c(varimp$rn, "(Intercept)"), ] %>%   -->
<!--   kable(digits = 4, escape = F, col.names = c("", "Estimació puntual", "Límit inferior", "Límit superior", "Estimació puntual", "Límit inferior", "Límit superior"), format.args = list(decimal.mark = ',', big.mark = "."), align = c("l", rep("c", 6))) %>% -->
<!--   kable_styling(full_width = F, position = "center") %>% -->
<!--   row_spec(0, bold = T, background = "#A9C7B0") %>% -->
<!--   column_spec(1, background = "#A9C7B0") %>% -->
<!--   add_header_above(c(" " = 1, "P2" = 3, "P101" = 3), background = "#A9C7B0") -->
<!-- ``` -->



