

```{r}
dades <- as.data.table(read.xlsx("../dades/satisfacció_usuaris_ABS.xlsx", sheet = "Hoja1"))
cataleg <- as.data.table(read.xlsx("../dades/satisfacció_usuaris_ABS.xlsx", sheet = "Hoja2", colNames = F))
ind_4cw <- fread("../dades/4cw.csv", sep = "@", header = T)
ind_4cw[, I4CW := `SUM(FLAG_4CW)`/`COUNT(*)`]
ind_4cw[, UP := str_pad(UP, width = 5, pad = "0")]
```


```{r}
khalix <- fread("../dades/indicadors_khalix.txt", sep = "{", header = F)
khalix[V5 == "ANUAL", V1 := paste0(V1, "_ANUAL")]
khalix[V5 == "MENSUAL", V1 := paste0(V1, "_MENSUAL")]

khalix_dcast <- dcast(khalix, V1 + V2 + V3 + V6 ~ V4, value.var = "V8")

khalix_dcast_mf_inf <- khalix_dcast[V6 %in% c("TIPPROF1", "TIPPROF4")]

khalix_dcast_mf_inf_aggr_br <- khalix_dcast_mf_inf[, lapply(.SD, sum), .SDcols = c("NUM", "DEN"), .(V1, V2, V3, V6)]
khalix_dcast_mf_inf_aggr_br[, RESULTAT := NUM/DEN]
# khalix_dcast_mf_dcast <- dcast(khalix_dcast_mf, V3 ~ V1, value.var = "RESULTAT")
names(khalix_dcast_mf_inf_aggr_br)[c(1, 3, 4)] <- c("indicadors", "ics_codi", "TIPPROF")

# khalix_dcast_mf_dcast[, setdiff(names(khalix_dcast_mf_dcast), "BR") := lapply(.SD, function(x){
#   ifelse(is.na(x), 0, x)
# }), .SDcols = setdiff(names(khalix_dcast_mf_dcast), "BR")]
```

```{r include=FALSE}
drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password="",host = "10.80.217.68", port=3307, dbname="permanent")
nfetch <- -1
query <- dbSendQuery(con,
                     statement = "select abs, ics_codi, scs_codi, medea, aquas from permanent.cat_centres")
cat_centres <- data.table(fetch(query, n = nfetch))  

query <- dbSendQuery(con,
                     statement = "select * from permanent.mst_variables_analisi where ates = '1'")
korra <- data.table(fetch(query, n = nfetch))
dbDisconnect(con)

# cat_centres[, abs := str_pad(abs, 3, pad = "0")]
```

```{r include=FALSE}
server<-"excdox-scan.cpd4.intranet.gencat.cat"
port<-"1522"
sid<-"excdox01srv"
# Create connection driver and open connection
jdbcDriver <- JDBC(driverClass="oracle.jdbc.OracleDriver", classPath="C:/Oracle/OJDBC-Full/ojdbc6.jar")
jdbcStr = paste("jdbc:oracle:thin:@", server, ":", port, "/", sid, sep="")
jdbcConnection = dbConnect(jdbcDriver, jdbcStr, "dwecoma", "ranagustavo2020")



# Query on the Oracle instance name.
dades_anys <- as.data.table(dbGetQuery(jdbcConnection,"SELECT * FROM dwsisap.satisfaccio"))

dbDisconnect(jdbcConnection)
```

```{r}
khalix_20 <- fread("../dades/indicadors_khalix_2020.txt", sep = "{", header = F)

cont0002a_20 <- fread("../dades/long0002A.csv", sep = ";", header = T)
names(cont0002a_20)[2] <- "RESULTAT"


```


```{r}
eqa <- data.table(read.xlsx("C:/Users/nmora/repositori/etl/66Analisis/enquesta_satisfaccio/dades/eqa_adults_201912.xlsx", 1))[, c(1, 3)]
```

