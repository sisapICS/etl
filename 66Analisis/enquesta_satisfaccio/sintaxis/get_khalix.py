# -*- coding: utf8 -*-

from lvclient import LvClient

#import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

#neos_homes = exemple(c, 'MALIGNA###;AYR21###;AMBITOS###;DIAGNOU;EDATS5###;NOIMP;HOME')
CONT0002 = exemple(c, 'CONT0002;A2112 ; AMBITOS###;	NUM, DEN; NOCAT; TIPPROF###; DIM6SET')
CONT0002A = exemple(c, 'CONT0002A;A2112; AMBITOS###; NUM, DEN; NOCAT; TIPPROF###; DIM6SET')
QACC2D = exemple(c, 'QACC2D;A2112; AMBITOS###; NUM, DEN; ANUAL, MENSUAL; TIPPROF###; DIM6SET')
QACC5D = exemple(c, 'QACC5D;A2112; AMBITOS###; NUM, DEN; ANUAL, MENSUAL; TIPPROF###; DIM6SET')
VISUBA = exemple(c, 'VISUBA;A2112;AMBITOS###;NUM,DEN;ANUAL,MENSUAL;TIPPROF###;DIM6SET')
VISURG = exemple(c, 'VISURG;A2112; AMBITOS###; NUM, DEN; ANUAL, MENSUAL; TIPPROF###; DIM6SET')
AGENDQC1 = exemple(c, 'AGENDQC1;A2112; AMBITOS###; NUM, DEN; ANUAL, MENSUAL; TIPPROF###; DIM6SET')
#QECONS0000 = exemple(c, 'QECONS0000;A2112; AMBITOS###; NUM, DEN; ANUAL; TIPPROF###; DIM6SET')
VVIRTUALS = exemple(c, 'VVIRTUALS;A2112; AMBITOS###; NUM, DEN; ANUAL; TIPPROF###; DIM6SET')


c.close()

print('export')

file = "indicadors_khalix_longA_2021.txt"

with open(file, 'w') as f:
   f.write(CONT0002 + '\n' + CONT0002A + '\n' + QACC2D + '\n' + QACC5D + '\n' + VISUBA + '\n' + VISURG + '\n' + AGENDQC1 + '\n' + VVIRTUALS)

