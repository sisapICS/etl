
```{r, warning=warning.var, message=message.var}


suppressWarnings(suppressPackageStartupMessages(library('RMySQL')))
suppressWarnings(suppressPackageStartupMessages(library('RJDBC')))
suppressWarnings(suppressPackageStartupMessages(library('openxlsx')))
suppressWarnings(suppressPackageStartupMessages(library('data.table')))
suppressWarnings(suppressPackageStartupMessages(library('stringr')))

suppressWarnings(suppressPackageStartupMessages(library('table1')))
suppressWarnings(suppressPackageStartupMessages(library('ggplot2')))
suppressWarnings(suppressPackageStartupMessages(library('tidyr')))
suppressWarnings(suppressPackageStartupMessages(library('kableExtra')))
suppressWarnings(suppressPackageStartupMessages(library('caret')))
suppressWarnings(suppressPackageStartupMessages(library('corrplot')))


  
# RMarkdown
  suppressWarnings(suppressPackageStartupMessages(library('knitr')))


```

