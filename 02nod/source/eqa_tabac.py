from sisapUtils import getSubTables, multiprocess, getAll, getOne, createTable, listToTable, grantSelect
from datetime import datetime, timedelta
import sisapUtils as u
import sisaptools as t
import itertools as i

imp = 'import'
jail = 'import_jail'
nod = 'nodrizas'
tables = {'s': 'eqa_tabac', 'c': 'sidiap_tabac'}
aquas = u.IS_MENSUAL and int(u.getKhalixDates()[2]) in (6, 12)

valors = {
    'E': {0: 0, 1: 3},
    'N': {0: 0, 'd': 3},
    'C': {0: 0, 'd': 3},
    'T': {0: 0, 'd': 3},
    'A': {'0': 0, '1': 0, '2': 3, '3': 2, '4': 2, '5': 3, '6': 3},
    'F': {0: 3, 1: 2},
    'Z': {0: 2, 1: 3},
    }

sqls = [
    ('variables', "select id_cip_sec, id_cip, left(vu_cod_vs, 1), date_format(vu_dat_act, '%Y%m%d'), vu_val from {}, nodrizas.dextraccio \
                where vu_cod_vs in ('EP2700', 'NCIR', 'CIGARR', 'TABAC') and vu_dat_act between '19900101' and data_ext"),
    ('activitats', "select id_cip_sec, id_cip, left(au_cod_ac, 1), date_format(au_dat_act,'%Y%m%d'), au_val from {}, nodrizas.dextraccio \
                    where au_cod_ac='ATA2' and au_dat_act between '19900101' and data_ext"),
    ('problemes', "select id_cip_sec, id_cip, 'F', date_format(pr_dde,'%Y%m%d'), 0 from {}, nodrizas.dextraccio \
                    where (pr_cod_ps like 'F17%' or pr_cod_ps like 'C01-F17%') and pr_dde between '19200101' and data_ext and if(pr_dba=0, date_format('29991231', '%Y%m%d'), pr_dba) > pr_dde and pr_hist=1"),
    ('problemes', "select id_cip_sec, id_cip, 'F', date_format(pr_dba,'%Y%m%d'), 1 from {}, nodrizas.dextraccio \
                    where (pr_cod_ps like 'F17%' or pr_cod_ps like 'C01-F17%') and pr_dba between '19200101' and data_ext and pr_dba >= pr_dde and pr_hist=1"),
]


def download_it(params):
    data = []
    for row in getAll(*params):
        data.append(row)
    return data


def getPeriods(id, data, dat_ext):
    result = []
    ha_fumat, dat_previ, val_previ = False, False, False
    for dat, val in data:
        if val > 0:
            ha_fumat = True
        if val == 0 and ha_fumat:
            val = 2
        if val == 3:
            val = 1
        last = -1
        if dat_previ:
            if val != val_previ:
                dat_baixa = datetime.strftime(datetime.strptime(dat, '%Y%m%d') - timedelta(days=1), '%Y%m%d')
                result.append([id, val_previ, dat_previ, dat_baixa, dat_last, 0])
            else:
                last = dat
                dat = dat_previ
                val = val_previ
        dat_previ = dat
        val_previ = val
        dat_last = max([dat, last])
    result.append([id, val_previ, dat_previ, dat_ext, dat_last, 1])
    return result


if __name__ == '__main__':
    jobs = []
    for sql in sqls:
        jobs.extend([(sql[1].format(tab), imp) for tab in getSubTables(sql[0])])
        jobs.append((sql[1].format(sql[0]), jail))
    mega_data = multiprocess(download_it, jobs, 8, close=True)

    tabac = {k: {} for k in tables}
    for data in mega_data:
        for id_cip_sec, id_cip, var, dat, val in data:
            id = {'s': id_cip_sec, 'c': id_cip if id_cip > 0 else None}
            try:
                valor = valors[var][val]
            except KeyError:
                try:
                    valor = valors[var]['d']
                except KeyError:
                    continue
            for k in id:
                if not id[k]:
                    continue
                if id[k] not in tabac[k]:
                    tabac[k][id[k]] = {}
                if dat not in tabac[k][id[k]]:
                    tabac[k][id[k]][dat] = -1
                tabac[k][id[k]][dat] = max(valor, tabac[k][id[k]][dat])

    createTable(tables['s'], '(id_cip_sec int, tab int, dalta date, dbaixa date, dlast date, last int)', nod, rm=True)
    createTable(tables['c'], '(id_cip int, tab int, dalta date, dbaixa date, dlast date, last int)', nod, rm=True)
    dext, = getOne("select date_format(data_ext, '%Y%m%d') from dextraccio", nod)
    for k in tables:
        upload = []
        for id, data in tabac[k].iteritems():
            for row in getPeriods(id, sorted(data.items()), dext):
                upload.append(row)
        listToTable(upload, tables[k], nod)

    if aquas:
        table = "padris_tabac"
        db = "exadata"
        sql = "select usua_cip_cod, usua_cip from pdptb101 \
               where usua_cip_cod like '{}%'"
        digits = [format(digit, "x").upper() for digit in range(16)]
        jobs = [(sql.format("".join(combo)), "pdp")
                for combo in i.product(digits, digits)]
        resultat = u.multiprocess(u.get_data, jobs, 32)
        hash_to_cip = {}
        for worker in resultat:
            for hash, cip in worker:
                hash_to_cip[hash] = cip
        sql = "select id_cip, hash_d from u11"
        id_to_cip = {id: hash_to_cip[hash] for (id, hash)
                     in getAll(sql, "import") if hash in hash_to_cip}
        sql = "select id_cip, dalta, dbaixa, tab from {}".format(tables["c"])
        upload = [(id_to_cip[row[0]],) + row[1:]
                  for row in getAll(sql, "nodrizas")
                  if row[0] in id_to_cip]
        createTable(table, "(cip varchar2(13), inici date, final date, valor int)", db, rm=True)
        t.Database("exadata", "data").list_to_table(upload, table, chunk=10**5)
        grantSelect(table, "DWAQUAS", db)
