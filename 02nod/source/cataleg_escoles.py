# coding: latin1

"""
Creem el catàleg escoles a partir de dos catalegs:
1) escoles - abs que hem tret a partir dels mapes de la Maria
2) nombre alumnes: que ens passen periòdicament.
Passem per assignada per veure en quins abs estan les línies
"""


MODEL = "PRITBEXA"  # TXT o EXADATA o PRITBEXA


import collections as c

import sisapUtils as u
import sisaptools as t
import csv
import sys

FILE_IN = "./dades_noesb/sie_escoles_abs.txt"
CURSOS_IN = "./dades_noesb/sie_cursos.csv"
COORD_IN = "./dades_noesb/sie_coordenades.txt"
db = "nodrizas"

def readCSV2(file,sep='@',encoding='UTF-8', header=False):

    with open(file,'rb') as f:
        primera = True
        c= csv.reader(f,delimiter=sep)
        for row in c:
            if header and primera:
                primera = False
            else:
                yield [unicode(cell, encoding).encode('latin-1') for cell in row]


class escoles(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_relacions()
        if MODEL == 'TXT':
            self.get_cursos()
        else:
            self.get_cursos_exadata()
        self.get_coordenades()
        if MODEL == "TXT":
            self.get_cataleg()
        elif MODEL == "EXADATA":
            self.get_cataleg_exadata()
        elif MODEL == "PRITBEXA":
            self.get_cataleg_pritb_exadata()
        else:
            sys.exit("MODEL desconegut")
        self.upload_data()
        
    def get_centres(self):
        """agafem ABS i mirem quina es la seva UP"""
        self.centres = {}
        self.up_to_br = {}
        self.up_abs = {}
        sql = "select abs, scs_codi, ics_codi, ics_desc from cat_centres"
        for abs, up, br, desc in u.getAll(sql, 'nodrizas'):
            abs = int(abs)
            self.centres[(abs)] =  up 
            self.up_to_br[(up)] = {'br': br, 'desc':desc}
            self.up_abs[up] = abs
        
    def get_relacions(self):
        """Mirem les poblacions infantils ups rca"""
        self.relacions = {}
        sql = 'select up, up_rca, count(*) from nodrizas.assignada_tot where edat < 15 group by up, up_rca'
        for up, up_rca, rec in u.getAll(sql, 'nodrizas'):
            if up != up_rca:
                if rec > 500:
                    self.relacions[up_rca] = up
        
    def get_cursos(self):
        """Agafem cursos de excel de manolo"""
        self.cursos = {}
        for escola, municipi, BATX1, BATX2, P3, P4, P5, EP1, EP2, EP3, EP4, EP5, EP6, ESO1, ESO2, ESO3, ESO4 in u.readCSV(CURSOS_IN, sep=';'):
            self.cursos[str(int(escola))] = municipi, BATX1, BATX2, P3, P4, P5, EP1, EP2, EP3, EP4, EP5, EP6, ESO1, ESO2, ESO3, ESO4

    def get_cursos_exadata(self):
        """Agafem cursos de taula de dades obertes que es crea a cataleg_escoles al X002"""
        self.cursos = {}
        self.info = c.defaultdict(dict)
        sql = """SELECT nom_municipi, codi_centre, codi_estudis, nivell, MATR_CULES_TOTAL 
                    FROM dwsisap.cat_escoles_do
                    WHERE codi_estudis IN ('BATX', 'EINF', 'EPRI', 'ESO')
                    and nivell is not null"""
        for municipi, codi_cen, codi_estudis, nivell, N in u.getAll(sql, 'exadata'):
            self.info[str(int(codi_cen))]['municipi'] = municipi
            codi = codi_estudis + nivell
            self.info[str(int(codi_cen))][codi] = N
        converter = {
            'municipi': 0,
            'BATX1': 1,
            'BATX2': 2,
            'EINF1': 3,
            'EINF2': 4,
            'EINF3': 5,
            'EPRI1': 6,
            'EPRI2': 7,
            'EPRI3': 8,
            'EPRI4': 9,
            'EPRI5': 10,
            'EPRI6': 11,
            'ESO1': 12,
            'ESO2': 13,
            'ESO3': 14,
            'ESO4': 15
        }
        for escola in self.info:
            self.cursos[str(int(escola))] = [0] * 16
            for curs, n in self.info[escola].items():
                if curs == 'municipi':
                    pass
                    # self.cursos[escola][converter[curs]] = int(n)
                elif curs in converter:
                    self.cursos[str(int(escola))][converter[curs]] = int(n)
        self.cursos[str(int(escola))] = tuple(self.cursos[str(int(escola))])

   
    def get_coordenades(self):
        """Agafem coordenades"""
        self.coordenades = {}
        for escola, titulacio, comarca, utmX, utmY, Geox, GeoY in readCSV2(COORD_IN, sep='@'):
            self.coordenades[str(int(escola))] = titulacio, comarca, utmX, utmY, Geox, GeoY
        
   
    def get_cataleg(self):
        """Llegeixo catàleg i li poso la up a partir de la ABS, passo els abs de les línies a les línies"""
        self.upload = []
        for codi, desc, abs, desc_abs in readCSV2(FILE_IN, sep='@'):
            try:
                abs = int(abs)
            except ValueError:
                abs = None
            abs = 302 if abs in (381, 382) else abs  # Montcada
            up = self.centres[(abs)] if (abs) in self.centres else None
            if up in self.relacions:
                up = self.relacions[up]
            up_desc = self.up_to_br[(up)]['desc'] if (up) in self.up_to_br else None
            br = self.up_to_br[(up)]['br'] if (up) in self.up_to_br else None
            try:
                titulacio, comarca, utmX, utmY, Geox, GeoY = self.coordenades[codi][0], self.coordenades[codi][1], self.coordenades[codi][2], self.coordenades[codi][3], self.coordenades[codi][4], self.coordenades[codi][5]
            except KeyError:
                titulacio, comarca, utmX, utmY, Geox, GeoY = None, None, None, None, None, None   
            try:
                self.upload.append([codi, desc, titulacio, utmX, utmY, Geox, GeoY, abs, desc_abs, up, br, up_desc, self.cursos[codi][0], comarca, self.cursos[codi][1],self.cursos[codi][2],self.cursos[codi][3],self.cursos[codi][4],
                   self.cursos[codi][5],self.cursos[codi][6],self.cursos[codi][7],self.cursos[codi][8],self.cursos[codi][9],self.cursos[codi][10],self.cursos[codi][11]
                   ,self.cursos[codi][12],self.cursos[codi][13],self.cursos[codi][14],self.cursos[codi][15]])
            except KeyError:
                self.upload.append([codi, desc, titulacio, utmX, utmY, Geox, GeoY, abs, desc_abs, up, None, None, None, comarca, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None])

    def get_cataleg_exadata(self):
        """Llegeixo catàleg i li poso la up a partir de la ABS, passo els abs de les línies a les línies"""
        self.upload = []
        sql = """select codi_centre, denominaci_completa, nom_naturalesa, coordenades_utm_x,
                        coordenades_utm_y, coordenades_geo_x, coordenades_geo_y,
                        nom_municipi, nom_comarca, abs_cod, abs_des
                from dwsisap.cat_escoles_do a
                inner join dwsisap.centres_escolars_centre b on a.codi_centre = b.centre_c 
                inner join dwsisap.dbc_centres c on b.abs = c.abs_cod"""
        for _codi, desc, titulacio, utmX, utmY, Geox, GeoY, municipi, comarca, abs, desc_abs in u.getAll(sql, "exadata"):
            codi = str(int(_codi))
            try:
                abs = int(abs)
            except ValueError:
                abs = None
            up = self.centres[(abs)] if (abs) in self.centres else None
            if up in self.relacions:
                up = self.relacions[up]
            up_desc = self.up_to_br[(up)]['desc'] if (up) in self.up_to_br else None
            br = self.up_to_br[(up)]['br'] if (up) in self.up_to_br else None
            this = (codi, desc, titulacio, utmX, utmY, Geox, GeoY, abs, desc_abs, up, br, up_desc, municipi, comarca)
            cursos = self.cursos.get(codi, (None,) * len(self.cursos["8003294"]))
            self.upload.append(this + cursos)
    
    def get_cataleg_pritb_exadata(self):
        self.upload = set()
        sql = """SELECT RV_LOW_VALUE  AS codi, RV_DOMAIN AS up, RV_MEANING  AS DESCr 
                    FROM PRITB000 WHERE RV_TABLE ='ESCOLES'"""
        escoles_up = {}
        for codi_e, up, desc in u.getAll(sql, '6837'):
            if up:
                up = up.strip()
            codi_e = str(int(codi_e))
            escoles_up[codi_e] = (up, desc)
        print('n coles', len(escoles_up))
        sql = """select distinct codi_centre, nom_naturalesa, coordenades_utm_x,
                        coordenades_utm_y, coordenades_geo_x, coordenades_geo_y,
                        nom_municipi, nom_comarca, abs_cod, abs_des
                from dwsisap.cat_escoles_do a
                left join dwsisap.centres_escolars_centre b on a.codi_centre = b.centre_c 
                left join dwsisap.dbc_centres c on b.abs = c.abs_cod"""
        counter = 0
        coles = set()
        for _codi, titulacio, utmX, utmY, Geox, GeoY, municipi, comarca, abs, desc_abs in u.getAll(sql, "exadata"):
            try:
                codi = str(int(_codi))
                if codi in escoles_up:
                    counter += 1
                    coles.add(codi)
                    up, desc = escoles_up[codi]
                    try:
                        if abs:
                            abs = int(abs)
                        else:
                            abs = None
                    except ValueError:
                        abs = None
                    up_desc = self.up_to_br[(up)]['desc'] if (up) in self.up_to_br else None
                    br = self.up_to_br[(up)]['br'] if (up) in self.up_to_br else None
                    this = (codi, desc, titulacio, utmX, utmY, Geox, GeoY, abs, desc_abs, up, br, up_desc, municipi, comarca)
                    cursos = self.cursos.get(codi, (None,) * len(self.cursos["8000013"]))[1:]
                    if len(codi) == 7: 
                        codi = '0' + codi
                        this = list(this)
                        this[0] = codi 
                    this = tuple(this)
                    cursos = tuple(cursos)
                    self.upload.add(this + cursos)
            except:
                pass
        self.upload = list(self.upload)
        print(counter)
        print(len(coles))

    def upload_data(self):
        """pugem a taula a nodrizas i redics"""
        table = "cat_escoles"
        columnsU = ["codi_escola varchar(40)", "nom_escola varchar(300)", "titulacio varchar(100)", "Coordenades_UTM_X double", "Coordenades_UTM_Y double",	"Coordenades_GEO_X double", "Coordenades_GEO_Y double",
        "abs varchar(10)", "desc_abs varchar(300)", "up varchar(1500)",  "br varchar(5)","up_desc varchar(300)",
        "municipi varchar(150)", "comarca varchar(150)",
        "N_1_BATX int", "N_2_BATX int","N_1_EI int","N_2_EI int","N_3_EI int","N_1_EP int","N_2_EP int","N_3_EP int","N_4_EP int","N_5_EP int","N_6_EP int","N_1_ESO int",
        "N_2_ESO int","N_3_ESO int","N_4_ESO int"]
        u.createTable(table, "({})".format(", ".join(columnsU)), db, rm=True)
        u.listToTable(self.upload, table, db)
        
        table = "sie_escoles"
        columnsU = ["codi_escola varchar2(40)", "nom_escola varchar2(300)", "titulacio varchar2(100)", "Coordenades_UTM_X number", "Coordenades_UTM_Y number",	"Coordenades_GEO_X number", "Coordenades_GEO_Y number",
        "abs varchar2(10)", "desc_abs varchar2(300)", "up varchar2(1500)", "br varchar2(5)", "up_desc varchar2(300)","municipi varchar2(150)","comarca varchar2(150)",
        "N_1_BATX number", "N_2_BATX number","N_1_EI number","N_2_EI number","N_3_EI number","N_1_EP number","N_2_EP number","N_3_EP number","N_4_EP number","N_5_EP number","N_6_EP number","N_1_ESO number",
        "N_2_ESO number","N_3_ESO number","N_4_ESO number"]
        u.createTable(table, "({})".format(", ".join(columnsU)), 'redics', rm=True)
        u.listToTable(self.upload, table, 'redics')
        u.grantSelect(table,('PREDUMMP','PREDUPRP','PREDUECR','PREDULMB', 'PDP'),'redics')
           
if __name__ == "__main__":
    escoles()