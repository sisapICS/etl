from sisapUtils import createTable, multiprocess, getSubTables, getAll, listToTable, execute
from collections import defaultdict


ori_db = 'import'
ori_tb = 'cmbdap'
des_db = 'nodrizas'
des_tb = 'csl_lligats'
espe = ('10999', '30999', '10888', '10777', '10117', '10116', '05999', '10106', '30888')


def do_it(taula):
    data = defaultdict(int)
    sql = "select id_cip_sec, date_format(cp_ecap_dvis, '%Y%m%d'), cp_ecap_col, if(cp_d1 <> '' or cp_din1 <> '', 1, 0) from {}, nodrizas.dextraccio \
            where cp_ecap_dvis between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext and cp_ecap_categoria in {}".format(taula, str(espe))
    for id, dat, col, num in getAll(sql, ori_db):
        data[id, dat, col] = max(num, data[id, dat, col])

    result = {}
    for (id, dat, col), num in data.items():
        if id not in result:
            result[id] = {'n': num, 'd': 1}
        else:
            result[id]['n'] += num
            result[id]['d'] += 1

    listToTable([(id, values['n'], values['d']) for id, values in result.items()], des_tb, des_db)


if __name__ == '__main__':
    createTable(des_tb, '(id_cip_sec int, num int, den int)', des_db, rm=True)
    multiprocess(do_it, getSubTables(ori_tb), 8)
    execute('alter table {} add index (id_cip_sec)'.format(des_tb), des_db)
