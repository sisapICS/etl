from sisapUtils import *

bd = "nodrizas"
bases= 'eqa_criteris','ped_criteris_var','ass_criteris_var'

conn = connect(bd)
c = conn.cursor()

for base in bases:
    c.execute("select distinct taula from %s" % base)
    r=c.fetchall()
    for t in r:
        nom = t[0]
        taula = "%s_%s" % (base,nom)
        c.execute("drop table if exists %s" % taula)

conn.close()