# coding: latin1

"""
.
"""

import collections as c
import multiprocessing as m

import sisaptools as u
import sisapUtils as u2


TABLE = "assignada_tot"


def worker(taula):
    """."""
    moduls = set(("VESC", "VESCV", "VESCT", "VESCP", "VESCO", "VESCM",
                  "VESCE", "VESC9", "VESC8", "VESC7", "VESC6", "VESC5",
                  "VESC4", "VESC3", "VESC2", "VESC1", "VESC0", "I-COL",
                  "I-VAC", "RODN", "RODN1", "RODN2", "RODN3", "RODN4",
                  "RODN5", "RODON", "RODON1", "RODON2", "RODON3", "RODON4",
                  "RODON5", "REV", "UAAU", "ALTES", "ADM", "TRESC", "PCRP",
                  "TRNTG"))
    etiquetes = set(("ESCO", "VAES", "RODN", "VESC", "VACU", "RODON", "V.ES"))
    serveis = ("UAAU", "ADM", "UAC")
    sql = "select id_cip_sec, visi_data_visita, s_espe_codi_especialitat, \
                  visi_modul_codi_modul, visi_etiqueta, \
                  visi_servei_codi_servei, visi_col_prov_resp, \
                  visi_tipus_visita \
           from {}, nodrizas.dextraccio \
           where visi_data_visita between \
                    adddate(adddate(data_ext, interval -1 year), \
                    interval +1 day) and data_ext and \
                 visi_situacio_visita = 'R' and \
                 visi_data_baixa = 0".format(taula)
    visites = {}
    for id, dat, espe, modul, etiqueta, servei, col, tipus in u.Database("p2262", "import").get_all(sql):  # noqa
        if modul not in moduls and etiqueta not in etiquetes and servei not in serveis:  # noqa
            if id not in visites:
                visites[id] = [dat, 1, 1, None, 0]
            else:
                visites[id][2] += 1
                if dat > visites[id][0]:
                    visites[id][0] = dat
            if espe in ("10106", "10777"):
                if not visites[id][3] or dat > visites[id][3]:
                    visites[id][3] = dat
            if not visites[id][4]:
                if col and col[0] == "3" and espe not in ("EXTRA", "10102") and tipus not in ("9E", "EXTRA", "EXT"):  # noqa
                    visites[id][4] = 1
    return (visites)


class Poblacio(object):
    """."""

    def __init__(self):
        """."""
        self.get_up_origen()
        self.get_abs_to_up()
        self.get_seccio_censal()
        self.get_institucionalitzats()
        self.get_estats()
        self.get_centres()
        self.get_piramides()
        self.get_valids()
        self.get_visites()
        self.create_table()

    def get_up_origen(self):
        """."""
        sql = "select distinct lloc_codi_up up, lloc_uab_codi_uab_assignat__a,\
                               lloc_up_rca rca \
            from cat_pritb025_def \
            where lloc_codi_up in ( \
                      select scs_codi from nodrizas.cat_centres \
                      where tip_eap = 'N' or scs_codi = '00478') and \
                  lloc_codi_up <> lloc_up_rca"
        self.up_origen = {row[:2]: row[2] for row
                          in u.Database("p2262", "import").get_all(sql)}

    def get_abs_to_up(self):
        """."""
        sql = "select abs_codi_abs, abs_codi_up from cat_rittb001"
        self.abs_to_up = {row[0]: row[1] for row
                          in u.Database("p2262", "import").get_all(sql)}

    def get_seccio_censal(self):
        """."""
        sql = "select id_cip_sec, sector_censal from crg \
               where sector_censal <> ''"
        self.seccio_censal = {id: seccio for id, seccio
                              in u.Database("p2262", "import").get_all(sql)}

    def get_institucionalitzats(self):
        """."""
        sql = "select distinct id_cip_sec, b.src_code, 1 \
               from institucionalitzats a \
               inner join cat_sisap_map_grup_rup b \
                     on a.codi_sector = b.sec and a.ug_codi_grup = b.grup \
               where b.sisap_class = 1"
        self.institucionalitzats = {row[0]: row[1:] for row
                                    in u.Database("p2262", "import").get_all(sql)}  # noqa

    def get_estats(self):
        """."""
        self.estats_pcc = set()
        self.estats_maca = set()
        sql = "select id_cip_sec, es_cod from estats, nodrizas.dextraccio \
               where es_dde <= data_ext"
        for id, estat in u.Database("p2262", "import").get_all(sql):
            if estat == "ER0001":
                self.estats_pcc.add(id)
            elif estat == "ER0002":
                self.estats_maca.add(id)

    def get_centres(self):
        """."""
        sql = "select scs_codi, tip_eap from cat_centres"
        self.centres = {up: tip for up, tip
                        in u.Database("p2262", "nodrizas").get_all(sql)}

    def get_piramides(self):
        """."""

        self.piramides = dict()

        sql = """
                SELECT DISTINCT
                    pir_up_origen,
                    pir_uab_origen,
                    pir_codi_modul_desti
                FROM
                    vistb327
                WHERE
                    pir_codi_servei_desti = 'TS'
                ORDER BY 1, 2
            """
        for sector in u2.sectors:
            for up, uba, modul in u2.getAll(sql, sector):
                self.piramides[(up, uba)] = modul

    def get_valids(self):
        """."""
        sql = "select id_cip, usua_uab_up \
               from assignada, nodrizas.dextraccio \
               where usua_situacio = 'A' and \
                     usua_data_naixement <= data_ext"
        recompte = c.Counter()
        for id, up in u.Database("p2262", "import").get_all(sql):
            if up in self.centres:
                recompte[id] += 1
        self.valids = set([k for k, v in recompte.items() if v == 1])

    def get_visites(self):
        """."""
        sql = "show create table visites1"
        tables = u.Database("p2262", "import").get_one(sql)[1]
        jobs = [table[1:-1]
                for table in tables.split("UNION=(")[1][:-1].split(",")]
        pool = m.Pool(len(jobs))
        results = pool.map(worker, jobs, chunksize=1)
        pool.close()
        self.visites = {}
        for visites in results:
            for id, dades in visites.items():
                if id not in self.visites:
                    self.visites[id] = dades[:]
                else:
                    self.visites[id][2] += dades[2]
                    if dades[0] > self.visites[id][0]:
                        self.visites[id][0] = dades[0]
                    if dades[3]:
                        if not self.visites[id][3] or dades[3] > self.visites[id][3]:  # noqa
                            self.visites[id][3] = dades[3]
                    if not self.visites[id][4] and dades[4]:
                        self.visites[id][4] = 1

    def create_table(self):
        """."""
        cols = ("id_cip_sec int", "id_cip int", "codi_sector varchar(4)",
                "ep varchar(4)", "up varchar(5)", "uba varchar(5)",
                "abs int", "upinf varchar(5)", "ubainf varchar(5)",
                "centre_codi varchar(9)", "centre_classe varchar(9)",
                "up_rca varchar(5)", "data_naix date", "edat int",
                "sexe varchar(1)", "nacionalitat varchar(3)",
                "nivell_cobertura varchar(2)", "relacio varchar(1)",
                "lms_peticio date", "lms_connexio date", "uas varchar(5)",
                "upOrigen varchar(5)", "upABS varchar(5)",
                "seccio_censal varchar(10)", "last_visit date", "ates int",
                "n_year int", "last_odn date", "ates_inf int",
                "last_visit_novc date", "ates_novc int", "n_year_novc int",
                "last_odn_novc date", "ates_inf_novc int",
                "up_residencia varchar(13)", "institucionalitzat int",
                "institucionalitzat_ps int", "atdom int", "trasplantat int",
                "maca int", "pcc int", "piramide varchar(5)")
        with u.Database("p2262", "nodrizas") as nodrizas:
            nodrizas.create_table(TABLE, cols, pk=["id_cip_sec"], remove=True)


class Sector(object):
    """."""

    def __init__(self, sector):
        """."""
        self.sector = sector
        self.get_problemes()
        self.get_poblacio()
        self.upload_data()

    def get_problemes(self):
        """."""
        sql = "select criteri_codi, if(agrupador = 45, 'atdom', 'trasplantat')\
               from eqa_criteris \
               where agrupador in (45, 529, 665)"
        codis = {codi: desc for codi, desc
                 in u.Database("p2262", "nodrizas").get_all(sql)}
        codis["C01-Z51.5"] = "maca"
        codis["C01-Z59.3"] = "institucionalitzat"
        sql = "select id_cip_sec, pr_cod_ps \
               from problemes_{}, nodrizas.dextraccio \
               where pr_cod_ps in {} and \
                     pr_dde <= data_ext and \
                     (pr_dba = 0 or pr_dba > data_ext) and \
                     (pr_data_baixa = 0 or pr_data_baixa > data_ext) and \
                     pr_hist = 1".format(self.sector, tuple(codis))
        self.problemes = {key: set() for key in codis.values()}
        for id, ps in u.Database("p2262", "import").get_all(sql):
            self.problemes[codis[ps]].add(id)

    def get_poblacio(self):
        """."""
        maca = poblacio.estats_maca | self.problemes["maca"]
        pcc = poblacio.estats_pcc - maca
        sql = "select id_cip_sec, id_cip, codi_sector, usua_uab_ep, \
                      usua_uab_up, usua_uab_codi_uab, usua_abs_codi_abs, \
                      ass_codi_up, ass_codi_unitat, usua_uab_servei_centre, \
                      usua_uab_servei_centre_classe, usua_up_rca, \
                      date_format(usua_data_naixement,'%Y%m%d'), \
                      (year(data_ext) - year(usua_data_naixement)) - \
                        (date_format(data_ext,'%m%d') < \
                          date_format(usua_data_naixement,'%m%d')), \
                      usua_sexe, usua_nacionalitat, cart_tipus, usua_relacio, \
                      usua_data_peticio_lms, usua_data_connexio_lms, \
                      usua_uas_codi_uas \
               from assignada_{}, nodrizas.dextraccio \
               where usua_situacio = 'A' and \
                     usua_data_naixement <= data_ext".format(self.sector)
        self.upload = []
        for row in u.Database("p2262", "import").get_all(sql):
            id_sec, id_cip = row[:2]
            up, uba, abs = row[4:7]
            edat = row[13]
            if id_cip in poblacio.valids and up in poblacio.centres:
                tipus_up = poblacio.centres[up]
                tipus_pac = "A" if edat > 14 else "N"
                if tipus_up == "M" or tipus_up == tipus_pac:
                    this = list(row)
                    this.append(poblacio.up_origen.get((up, uba), up))
                    this.append(poblacio.abs_to_up.get(abs))
                    this.append(poblacio.seccio_censal.get(id_sec))
                    visites = poblacio.visites.get(id_sec, (None, 0, 0, None, 0))  # noqa
                    this.extend(visites * 2)
                    this.extend(poblacio.institucionalitzats.get(id_sec, (None, 0)))  # noqa
                    this.append(1 * (id_sec in self.problemes["institucionalitzat"]))  # noqa
                    this.append(1 * (id_sec in self.problemes["atdom"]))
                    this.append(1 * (id_sec in self.problemes["trasplantat"]))
                    this.append(1 * (id_sec in maca))
                    this.append(1 * (id_sec in pcc))
                    this.append(poblacio.piramides.get((up, uba), None))
                    self.upload.append(this)

    def upload_data(self):
        """."""
        with u.Database("p2262", "nodrizas") as nodrizas:
            nodrizas.list_to_table(self.upload, TABLE)


if __name__ == "__main__":
    sectors = [k for k, v in
               sorted(u.Database("sidics", "sidics").get_table_partitions("usutb040").items(),  # noqa
                      key=lambda x:x[1], reverse=True)
               if k[1:] in u.constants.SECTORS_ECAP[:-1]]
    poblacio = Poblacio()
    results = []
    pool = m.Pool(8)
    for sector in sectors:
        results.append(pool.apply_async(Sector, args=(sector,)))
    pool.close()
    pool.join()
    for result in results:
        try:
            a = result.get()  # noqa
        except Exception as e:
            raise
    poblacio = None
    sql = "create or replace table cat_linies as \
           select distinct up, uba, uporigen \
           from {} \
           where edat < 12 and up <> uporigen".format(TABLE)
    u.Database("p2262", "nodrizas").execute(sql)
