from sisapUtils import *

db= 'nodrizas','aux'
name= db[0]
bases = 'eqa_criteris','ped_criteris_var','ass_criteris_var'

for base in bases:
    sql= 'select distinct taula from {}'.format(base)
    for nom, in getAll(sql,db):
        taula = base+'_'+nom
        execute('drop table if exists {}'.format(taula),db)
        execute('create table {} like {}'.format(taula,base),db)
        sql= "select distinct index_name from information_schema.statistics where table_schema='{}' and table_name='{}'".format(name,taula)
        for idx, in getAll(sql,db):
            execute('alter table {} drop {}'.format(taula,"PRIMARY KEY" if idx== "PRIMARY" else ("index " + idx)),db)
        execute("insert into {} select * from {} where taula='{}'".format(taula,base,nom),db)
        execute("alter table {} add column inici date,add column final date".format(taula),db)
        if base== 'ped_criteris_var':
            execute("update {},dextraccio set inici=date_add(date_add(data_ext,interval - minim_t month),interval +1 day),final=data_ext where unitat='m'".format(taula),db)
            execute("update {},dextraccio set inici=date_add(date_add(data_ext,interval - minim_t year),interval +1 day),final=data_ext where unitat='a'".format(taula),db)
        elif base== 'ass_criteris_var':
            execute('update {},dextraccio set inici=date_add(date_add(data_ext,interval - minim_t month),interval +1 day),final=data_ext'.format(taula),db)
        else:
            execute('update {},dextraccio set inici=date_add(date_add(data_ext,interval - minimt month),interval +1 day),final=data_ext'.format(taula),db)
        execute("alter table {} add unique (criteri_codi,agrupador,agrupador_desc,inici,final,ps_tancat,inclusio)".format(taula),db)
    try:
        execute("insert into {0}_activitats select * from {0}_actuacions".format(base),db)
    except:
        pass

