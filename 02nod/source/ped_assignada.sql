use nodrizas
;

DROP TABLE IF EXISTS ped_assignada;
CREATE TABLE ped_assignada (
  codi_sector varchar(4),
  id_cip double null,
  id_cip_sec double null,
  up VARCHAR(5) NOT NULL DEFAULT '',
  uba VARCHAR(5) NOT NULL DEFAULT '',
  upinf VARCHAR(5) NOT NULL DEFAULT '',
  ubainf VARCHAR(5) NOT NULL DEFAULT '',
  data_naix DATE NOT NULL DEFAULT 0,
  edat_a double NULL,
  edat_m double NULL,
  edat_d double NULL,
  sexe VARCHAR(1) NOT NULL DEFAULT '',
  ates double null,
  institucionalitzat double null,
  maca double null,
  pcc int,
  UNIQUE prob (id_cip_sec,data_naix)
)
select 
	codi_sector
    ,id_cip
    ,id_cip_sec
	,up
	,uba
    ,upinf
    ,ubainf
	,data_naix
	,edat edat_a
    ,(YEAR(data_ext) - YEAR(data_naix)) * 12 + (MONTH(data_ext) - MONTH(data_naix)) - (DAYOFMONTH(data_ext) < DAYOFMONTH(data_naix)) edat_m
    ,datediff(data_ext,data_naix) edat_d
	,sexe
    ,ates
    ,institucionalitzat_ps institucionalitzat
    ,maca
    ,pcc
from
	assignada_tot
    ,dextraccio
where
    edat < 15
    and data_naix <= data_ext
;
