# coding: utf8

"""
.
"""

import collections as c

import sisapUtils as u


tb = "nod_rhb"
db = "nodrizas"


class RHB(object):
    """."""

    def __init__(self):
        """."""
        self.get_fitxes()
        self.get_visites()
        self.upload_data()

    def get_fitxes(self):
        """."""
        sql = "select fit_id, codi_sector, id_cip_sec, fit_probs_salut, \
                      fit_tipus_prof, fit_tipus_trac, fit_up, \
                      fit_data_inici, fit_data_fi_tract \
               from rhb07"
        self.fitxes = {row[:2]: (row[2], row[3].split("#")[0]) + row[3:]
                       for row in u.getAll(sql, "import")}

    def get_visites(self):
        """."""
        sql = "select vis_fit_id, codi_sector, vis_data, \
                      vis_realitzada, vis_up \
               from rhb09"
        self.master = [row + self.fitxes[row[:2]]
                       for row in u.getAll(sql, "import")
                       if row[:2] in self.fitxes]

    def upload_data(self):
        """."""
        columns = ("vis_fit_id int", "codi_sector varchar(4)", "vis_data date",
                   "vis_realitzada varchar(1)", "vis_up varchar(5)",
                   "id_cip_sec int", "fit_probs_salut_first varchar(10)",
                   "fit_probs_salut_all varchar(100)",
                   "fit_tipus_prof varchar(1)", "fit_tipus_trac varchar(1)",
                   "fit_up varchar(5)", "fit_data_inici date",
                   "fit_data_fi_tract date")
        u.createTable(tb, "({})".format(" ,".join(columns)), db, rm=True)
        u.listToTable(self.master, tb, db)


if __name__ == "__main__":
    RHB()
