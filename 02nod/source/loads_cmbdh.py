
import sisapUtils as u

bd = "nodrizas"

# agrupador 21 ICC dp
sql = """
		insert into nodrizas.eqa_criteris
		select
			replace(taula, 'problemes', 'cmbdh_exadata') as taula,
			inclusio,
			replace(substr(criteri_codi, 5, length(criteri_codi)),'.','') as criteri_codi,
			concat(agrupador_desc, 'ppal') as agrupador_desc,
			case when agrupador = 21 then 625 end as agrupador,
			ps_tancat,
			minimt
		from
			nodrizas.eqa_criteris
		where
			taula = 'problemes' and
			agrupador = 21 and
			substr(criteri_codi,1,4) = 'C01-'"""
u.execute(sql, bd)

# agrupador 21 ICC ds1
sql = """
	insert into nodrizas.eqa_criteris
	select
		replace(taula, 'problemes', 'cmbdh_exadata') as taula,
		inclusio,
		replace(substr(criteri_codi, 5, length(criteri_codi)),'.','') as criteri_codi,
		concat(agrupador_desc, 'ds1') as agrupador_desc,
		case when agrupador = 21 then 627 end as agrupador,
		ps_tancat,
		minimt
	from
		nodrizas.eqa_criteris
	where
		taula = 'problemes' and
		agrupador = 21 and
		substr(criteri_codi,1,4) = 'C01-'"""

u.execute(sql, bd)

# agrupador 709
sql = """
	insert into nodrizas.eqa_criteris
	select
		replace(taula, 'problemes', 'cmbdh_exadata') as taula,
		inclusio,
		replace(substr(criteri_codi, 5, length(criteri_codi)),'.','') as criteri_codi,
		agrupador_desc,
		case when agrupador = 709 then 626 end as agrupador,
		ps_tancat,
		minimt
	from
		nodrizas.eqa_criteris
	where
		taula = 'problemes' and
		agrupador = 709 and
		substr(criteri_codi,1,4) = 'C01-'"""

u.execute(sql, bd)

# agrupador 194

sql = """ insert into nodrizas.eqa_criteris
	select
		replace(taula, 'problemes', 'cmbdh_exadata') as taula,
		inclusio,
		replace(substr(criteri_codi, 5, length(criteri_codi)),'.','') as criteri_codi,
		agrupador_desc,
		case when agrupador = 194 then 619 end as agrupador,
		ps_tancat,
		minimt
	from
		nodrizas.eqa_criteris
	where
		taula = 'problemes' and
		agrupador = 194 and
		substr(criteri_codi,1,4) = 'C01-'"""

u.execute(sql, bd)

# agrupador 50
sql = """
	insert into nodrizas.eqa_criteris
	select
		replace(taula, 'problemes', 'cmbdh_exadata') as taula,
		inclusio,
		replace(substr(criteri_codi, 5, length(criteri_codi)),'.','') as criteri_codi,
		agrupador_desc,
		case when agrupador = 50 then 621 end as agrupador,
		ps_tancat,
		minimt
	from
		nodrizas.eqa_criteris
	where
		taula = 'problemes' and
		agrupador = 50 and
		substr(criteri_codi,1,4) = 'C01-'"""

u.execute(sql, bd)

# agrupador 257
sql = """
	insert into nodrizas.eqa_criteris
	select
		replace(taula, 'problemes', 'cmbdh_exadata') as taula,
		inclusio,
		replace(substr(criteri_codi, 5, length(criteri_codi)),'.','') as criteri_codi,
		agrupador_desc,
		case when agrupador = 257 then 624 end as agrupador,
		ps_tancat,
		minimt
	from
		nodrizas.eqa_criteris
	where
		taula = 'problemes' and
		agrupador = 257 and
		substr(criteri_codi,1,4) = 'C01-'"""

u.execute(sql, bd)

# agrupador 930
sql = """
	insert into nodrizas.eqa_criteris
	select
		replace(taula, 'problemes', 'cmbdh_exadata') as taula,
		inclusio,
		replace(substr(criteri_codi, 5, length(criteri_codi)),'.','') as criteri_codi,
		agrupador_desc,
		case when agrupador = 930 then 620 end as agrupador,
		ps_tancat,
		minimt
	from
		nodrizas.eqa_criteris
	where
		taula = 'problemes' and
		agrupador = 930 and
		substr(criteri_codi,1,4) = 'C01-'"""

u.execute(sql, bd)

# agrupador 7
sql = """
	insert into nodrizas.eqa_criteris
	select
		replace(taula, 'problemes', 'cmbdh_exadata') as taula,
		inclusio,
		replace(substr(criteri_codi, 5, length(criteri_codi)),'.','') as criteri_codi,
		agrupador_desc,
		case when agrupador = 7 then 622 end as agrupador,
		ps_tancat,
		minimt
	from
		nodrizas.eqa_criteris
	where
		taula = 'problemes' and
		agrupador = 7 and
		substr(criteri_codi,1,4) = 'C01-'"""

u.execute(sql, bd)

# agrupador 932 complicacions croniques dm2
sql = """
	insert into nodrizas.eqa_criteris
	select
		replace(taula, 'problemes', 'cmbdh_exadata') as taula,
		inclusio,
		replace(substr(criteri_codi, 5, length(criteri_codi)),'.','') as criteri_codi,
		agrupador_desc,
		case when agrupador = 932 then 933 end as agrupador,
		ps_tancat,
		minimt
	from
		nodrizas.eqa_criteris
	where
		taula = 'problemes' and
		agrupador = 932 and
		substr(criteri_codi,1,4) = 'C01-'"""

u.execute(sql, bd)

# agrupador 933 complicacions agudes dm2
sql = """
	insert into nodrizas.eqa_criteris
	select
		replace(taula, 'problemes', 'cmbdh_exadata') as taula,
		inclusio,
		replace(substr(criteri_codi, 5, length(criteri_codi)),'.','') as criteri_codi,
		agrupador_desc,
		case when agrupador = 934 then 935 end as agrupador,
		ps_tancat,
		minimt
	from
		nodrizas.eqa_criteris
	where
		taula = 'problemes' and
		agrupador = 934 and
		substr(criteri_codi,1,4) = 'C01-'"""

u.execute(sql, bd)

# agrupador 939 ci
sql = """
	insert into nodrizas.eqa_criteris
	select
		replace(taula, 'problemes', 'cmbdh_exadata') as taula,
		inclusio,
		replace(substr(criteri_codi, 5, length(criteri_codi)),'.','') as criteri_codi,
		agrupador_desc,
		case when agrupador = 1 then 939 end as agrupador,
		ps_tancat,
		minimt
	from
		nodrizas.eqa_criteris
	where
		taula = 'problemes' and
		agrupador = 1 and
		substr(criteri_codi,1,4) = 'C01-'"""

u.execute(sql, bd)