from sisapUtils import getAll, createTable, listToTable


db = 'nodrizas'
tb_dx = 'eqa_ingressos_EX'
tb_px = 'eqa_procediments_EX'


data = []
sql = """select a.id_cip_sec, a.id_ingres, a.up, a.dnaix, a.sexe, b.agrupador, 
            a.dingres, a.posicio, b.ps_tancat 
            from nod_cmbdh_dx_exadata a 
            inner join eqa_criteris_cmbdh_exadata b on a.codi = b.criteri_codi
            where a.dingres between b.inici and b.final
            union
            select a.id_cip_sec, a.id_ingres, a.up, a.dnaix, a.sexe, b.agrupador, 
            a.dingres, a.posicio, b.ps_tancat 
            from nod_cmbdh_dx_exadata a 
            inner join eqa_criteris_cmbdh b on a.codi = b.criteri_codi
            where a.dingres between b.inici and b.final"""
for id, ing, up, naix, sexe, agr, dat, pos, tip in getAll(sql, db):
    if tip == 0 or tip == pos:
        data.append([id, ing, up, naix, 'H' if sexe == '0' else 'D', agr, dat])

createTable(tb_dx, '(id_cip_sec int, ingres int, up varchar(5), naix date, sexe varchar(1), agr int, dat date)', db, rm=True)
listToTable(data, tb_dx, db)


data = []
sql = 'select a.id_cip_sec, a.id_ingres, b.agrupador from nod_cmbdh_px_exadata a inner join eqa_criteris_cmbdhpx b on a.codi = b.criteri_codi'
for id, ing, agr in getAll(sql, db):
    data.append([id, ing, agr])

createTable(tb_px, '(id_cip_sec int, ingres int, agr int)', db, rm=True)
listToTable(data, tb_px, db)