use nodrizas
;

drop table if exists eqa_xml;
create table eqa_xml(
id_cip_sec int,
agrupador int,
dat date,
index(id_cip_sec)
)
select
    a.id_cip_sec
    ,agrupador
    ,xml_data_alta dat
from
    import.xml a
    inner join eqa_criteris_xml b on a.xml_tipus_orig = b.criteri_codi
where
    xml_data_alta between inici and final
;
