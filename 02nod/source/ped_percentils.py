# coding: iso-8859-1

from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

debug = False

db = 'nodrizas'
agrupador = 756

def get_sexe():
    """Retorna el sexe dels cada pacient
    """
    sexes = {}
    sql = "select id_cip_sec, sexe from ped_assignada"
    for id, sexe in getAll(sql, 'nodrizas'):
        sexes[id] = sexe
        
    return sexes


def get_percentil_IMC():

    """
    Per obtenir punts de tall de percentils P95 i P97 
    """
    minim = {}
    maxim = {}
    perc = {}
    sex = ['H', 'D']
    ptils = ['P95', 'P97']
    for sexe in sex:
        for percentil in ptils:
            for edat in range(0, 180):
                sql = "select replace(tm_val,',','.'), tm_edat from cat_prstb456 where tm_tipus='IMC' and tm_var='{}' and tm_sexe='{}' and tm_tvar='L' and tm_edat <={} order by tm_edat desc limit 1".format(percentil, sexe, edat)
                for p, ed in getAll(sql, 'import'):
                    minim[(sexe, edat, percentil)] = {'valor': float(p), 'edat': ed}
                sql = "select replace(tm_val,',','.'), tm_edat from cat_prstb456 where tm_tipus='IMC' and tm_var='{}' and tm_sexe='{}' and tm_tvar='L' and tm_edat >={} order by tm_edat asc limit 1".format(percentil, sexe, edat)
                for p, ed in getAll(sql, 'import'):
                    maxim[(sexe, edat, percentil)] = {'valor': float(p), 'edat': ed}
    
    for (sexe, edat, percentil), valors in minim.items():
        min_valor = valors['valor']
        min_edat = valors['edat']
        max_valor = maxim[(sexe, edat, percentil)]['valor']
        max_edat = maxim[(sexe, edat, percentil)]['edat']
        temps = max_edat - min_edat
        vals = max_valor - min_valor
        if temps != 0:
            sumar = vals / temps
            interval_edat = edat - min_edat
            persumar = sumar * interval_edat
            resultat = min_valor + persumar
            perc[sexe, edat, percentil] = resultat
        else:
            perc[sexe, edat, percentil] = min_valor

    return perc

def calcul_perc(sexes, percentils):
    """
    Per cada IMC calculem si és >P95, >p97 o p95 o <
    """
    upload = []
    uploadvar = []
    sql = "select id_cip_sec, val, dat, edat_a, edat_m from ped_variables where agrupador = 19{}".format(' limit 10' if debug else '')
    for id, val, dat, edata, edat in getAll(sql, 'nodrizas'):
        if id in sexes:
            sexe = sexes[id]
            P95 = percentils[sexe, edat, 'P95']
            P97 = percentils[sexe, edat,'P97']
            if float(val) > P97:
                p = '>P97'
            elif P95 < float(val) <= P97:
                p = '>P95'
            else:
                p = '<95' 
            upload.append([id, val, dat, sexe, edat, p])
            uploadvar.append([id, agrupador, p, dat, edata, edat])
     
    return upload, uploadvar
    
if __name__ == '__main__':
    printTime('Inici')
    
    sexes = get_sexe()
    printTime('assignada')
    
    percentils = get_percentil_IMC()
    printTime('percentils')
    
    rows, rowsv = calcul_perc(sexes, percentils)
    printTime('percentils')
    
    table = 'ped_percentils'
    
    createTable(table, '(id_cip_sec int,  val double, data date, sexe varchar(1), edat_m int, percentil varchar(10))', db, rm=True)
    listToTable(rows, table, db)
    listToTable(rowsv, 'ped_variables', db)
    
    printTime('Fi')
        
    
    
    
    