import sisaptools as u


TABLE = "eqa_familiars"


sql = "select id_cip_sec, agrupador, \
              if(ps_tancat = 0, af_pare_edat, af_mare_edat) edat \
       from import.familiars a \
       inner join nodrizas.eqa_criteris b on a.af_cod_ps = b.criteri_codi, \
       nodrizas.dextraccio \
       where af_data_a <= data_ext and af_data_b = '' and \
             taula = 'familiars' and if(ps_tancat = 0, af_pare, af_mare) = 'S'"
cols = ("id_cip_sec int", "agrupador int", "edat int")
with u.Database("p2262", "nodrizas") as nodrizas:
    nodrizas.create_table(TABLE, cols, remove=True)
    nodrizas.execute("insert into {} {}".format(TABLE, sql))
