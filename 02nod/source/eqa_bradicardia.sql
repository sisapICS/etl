use nodrizas;

drop table if exists bradicardia;
create table bradicardia as
select id_cip_sec,214 as ps,dde,pr_gra,incident,n,ninc,edat,0 fc from eqa_problemes where ps=178;

alter table bradicardia add index id (id_cip_sec);

update bradicardia a inner join eqa_variables b on a.id_cip_sec=b.id_cip_sec,dextraccio
set fc=1 where b.agrupador=179 and valor<50 and (data_var between date_add(data_ext,interval - 2 year) and data_ext) 
;

delete a.* from eqa_problemes a where ps=214
;

insert into eqa_problemes
select id_cip_sec,ps,dde,pr_gra,incident,n,ninc,edat from bradicardia where fc=1;

drop table bradicardia;
