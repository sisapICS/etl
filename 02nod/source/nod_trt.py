# -*- coding: utf8 -*-

"""
Calcula, per cada determinació de INR, el Temps en Rang Terapèutic (TRT)
en els 12 mesos previs.
"""

import collections as c

import sisapUtils as u


db = "nodrizas"
tb = "nod_trt"

dies_trt = 365  # TRT de 12 mesos
dies_gap = 60   # Hi ha gap si passen més de 60 dies entre dues determinacions


class TRT(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_inr()
        self.get_inr_rang()
        self.iterate_data()
        self.upload_data()

    def get_inr(self):
        """Captura les determinacions de INR i els rangs terapèutics."""
        self.origen = {}
        sql = "select id_cip_sec, agrupador, data_var, valor \
               from eqa_variables \
               where agrupador in (195, 262, 263)"
        for id, agr, dat, val in u.getAll(sql, "nodrizas"):
            self.origen[(id, agr, dat)] = val

    def get_inr_rang(self):
        """Uneix els INR amb els rangs."""
        self.inr_rang = c.defaultdict(list)
        for (id, agr, dat), val in self.origen.items():
            if agr == 195:
                rinf = self.origen.get((id, 262, dat), 2)
                rsup = self.origen.get((id, 263, dat), 3)
                self.inr_rang[id].append((dat, val, rinf, rsup))

    def iterate_data(self):
        """Itera els pacients i crida el càlcul per cadascun d'ells."""
        self.trt = []
        for id, info in self.inr_rang.items():
            self.__get_degradat(id, info)

    def __get_degradat(self, id, info):
        """
        Calcula els valors diaris estimats d'un pacient,
        i crida el càlcul dels seus TRT.
        """
        ordenat = sorted(info, reverse=True)
        degradat = []
        for i, (dat, val, rinf, rsup) in enumerate(ordenat):
            degradat.append((i + 1, dat, val, rinf <= val <= rsup))
            if (i + 1) < len(info):
                prev_dat, prev_val = ordenat[i + 1][0:2]
                dies = (dat - prev_dat).days
                canvi_dia = (prev_val - val) / dies
                degradat.extend(((None, None, None,
                                  rinf <= (val + (canvi_dia * j)) <= rsup)
                                 for j in xrange(1, dies)))
        self.__get_trt(id, degradat)

    def __get_trt(self, id, degradat):
        """Calcula els diferents temps en rang terapèutic d'un pacient."""
        for i, (pos, dat, val, good) in enumerate(degradat):
            if pos:
                calculable, n_all, n_good, d_not, gaps = False, 0, 0, 0, False
                if len(degradat) >= (i + dies_trt):
                    calculable = True
                    for j in xrange(dies_trt):
                        n_good += degradat[i + j][3]
                        if degradat[i + j][0]:
                            n_all += 1
                            d_not = 0
                        else:
                            d_not += 1
                            if d_not > dies_gap:
                                gaps = True
                    trt = round((100.0 * n_good) / dies_trt, 2)
                this = (id, dat, round(val, 2), pos, 1 * calculable,
                        trt if calculable else -1,
                        n_all if calculable else -1,
                        1 * gaps if calculable else -1)
                self.trt.append(this)

    def upload_data(self):
        """Crea la taula de destí i hi insereix les dades."""
        u.createTable(tb, "(id int, dat date, val double, usar int, \
                            is_calculable int, trt double, n_controls int, \
                            has_gaps int)",
                      db, rm=True)
        u.listToTable(self.trt, tb, db)


if __name__ == "__main__":
    TRT()
