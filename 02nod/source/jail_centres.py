# coding: latin1
import sisaptools as u
from sisapUtils import *
from os import remove
import os

imp = 'import_jail'
nod = 'nodrizas'
table = 'jail_centres'
eap = 'cat_centres'
merge = 'cat_centres_with_jail'
execute('drop table if exists {}'.format(table),nod)
execute('create table {} like {}'.format(table,eap),nod)
new = []
davidFile = tempFolder + 'centres_sisap.txt'
jailFile = 'jail_centres_klx.txt'


sql = "select distinct usua_uab_up from assignada where usua_situacio='A' and usua_uab_up<>''"
existeixen = tuple([up for up,
                    in u.Database("p2262", "import_jail").get_all(sql)])

sql = "SELECT a.up_codi_up_scs, a.up_codi_up_ics, b.up_desc_up_ics \
       FROM import.cat_gcctb008 a \
       INNER JOIN import.cat_gcctb007 b ON a.up_codi_up_ics = b.up_codi_up_ics\
       WHERE a.up_data_baixa = '' AND b.up_data_baixa = '' and \
             a.up_codi_up_scs in {}".format(existeixen)
conversio = {row[0]: row[1:] for row
             in u.Database("p2262", "import").get_all(sql)}

with u.Database("p2262", "nodrizas") as conn:
    for up in existeixen:
        br, lit = conversio[up]
        this = 'insert into {} \
                   (sector, ep, scs_codi, ics_codi, ics_desc, ics_codi_orig) \
                   values ("6951", "0208", "{}", "{}", "{}", "{}")'.format(table, up, br, lit, br)
        conn.execute(this)

execute('drop table if exists {}'.format(merge),nod)    
execute('create table {} like {}'.format(merge,table),nod)
execute('alter table {} ENGINE=MERGE'.format(merge),nod)
execute('alter table {} UNION=({},{})'.format(merge,eap,table),nod)
    
if len(new) > 0:
    sendSISAP('jcamus@gencat.cat','Nuevas UP prisiones','Joan','\r\n'.join(map(str,new)))

davidList = [['ep', 'amb_codi', 'amb_desc', 'sap_codi', 'sap_desc', 'scs_codi', 'ics_codi', 'ics_desc', 'medea']]
sql = 'select ep, amb_codi, amb_desc, sap_codi, sap_desc, scs_codi, ics_codi, ics_desc, medea from {}'.format(merge)
for row in getAll(sql, nod):
    davidList.append(list(row))
writeCSV(davidFile, davidList, sep=';')
dat, = getOne("select date_format(data_ext,'%d %b') from dextraccio", nod)
sendSISAP('dmonterde@gencat.cat', 'Centres SISAP', 'David', 'Adjuntem centres SISAP en data {}.'.format(dat), file=davidFile)
remove(davidFile)

covid_table = "sisap_covid_cat_up_ecap"
covid_db = "redics"
covid_cols = "(up varchar2(5), br varchar2(5), ep varchar2(4), abs varchar2(3), \
               is_linia int, is_eapp int, medea varchar2(2), sector varchar2(4))"
covid_sql = "select scs_codi, ics_codi, ep, if(abs=0, null, lpad(abs, 3, '0')), tip_eap = 'N', 0, medea, sector from cat_centres"
covid_sql += " union select scs_codi, ics_codi, '0208', null, 0, 1, '', sector from jail_centres"
covid_data = list(getAll(covid_sql, "nodrizas"))
createTable(covid_table, covid_cols, covid_db, rm=True)
createTable("sisap_up_ecap", covid_cols, "exadata", rm=True)
listToTable(covid_data, covid_table, covid_db)
listToTable(covid_data, "sisap_up_ecap", "exadata")
grantSelect(covid_table, ("PDP", "PREDUMMP", "PREDUECR", "PREDUPRP"), covid_db)
grantSelect("sisap_up_ecap", "DWSISAP_ROL", "exadata")
calcStatistics(covid_table, covid_db)
calcStatistics("sisap_up_ecap", "exadata")
