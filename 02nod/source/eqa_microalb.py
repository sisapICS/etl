# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict, Counter
import csv
import os
import sys
from time import strftime

printTime('Inici')

debug = False

nod = 'nodrizas'
QAC = 513
variables = 'eqa_variables'

OutFile = tempFolder + 'microALB.txt'
tableMy = 'eqa_microALB'

microALB, ultimV = {}, {}
sql = "select id_cip_sec, data_var, valor, usar from {0} where agrupador={1} {2}".format(variables, QAC, ' limit 10' if debug else '')
for id, data_var, valor, usar in getAll(sql, nod):
    if valor > 30:
        if usar == 1:
            ultimV[(id)] = True
        if (id) in microALB:
            data = microALB[(id)]['data']
            mesos = monthsBetween(data_var, data)
            if -5 <= mesos <= 5:
                microALB[(id)]['num'] += 1
                microALB[(id)]['data'] = data_var
        else:
            microALB[(id)] = {'data': data_var, 'num': 1, 'agr': QAC}

            
with openCSV(OutFile) as c:
    for (id), d in microALB.items():
        num = d['num']
        agr = d['agr']
        if num > 1:
            if id in ultimV:
                c.writerow([id, agr])

execute("drop table if exists {}".format(tableMy), nod)
execute("create table {} (id_cip_sec int, agrupador int, index(agrupador))".format(tableMy), nod)
loadData(OutFile, tableMy, nod)

printTime('Fi')
