from sisapUtils import createTable, getOne, multiprocess, getAll, listToTable
from collections import defaultdict


og = 'import'
tb = {'pac': 'ag_long_pacients', 'vis': 'ag_long_visites'}
db = 'nodrizas'
hm = {tipus: homol for tipus, homol in getAll("select concat(codi_sector, tv_cita, tv_tipus), if(tv_model_nou = 'S', tv_tipus, tv_homol) from cat_vistb206", og)}
gc_serveis = ['INFG', 'UGC', 'GCAS']
gc_numcols = set([numcol for numcol, in getAll("select left(ide_numcol, 8) from cat_pritb799 a inner join cat_pritb992 b on a.rol_usu = b.ide_usuari and a.codi_sector = b.codi_sector \
                                                where rol_rol = 'ECAP_GEST' and ide_numcol <> ''", og)])


def do_it(params):
    taula, pacients = params
    data = []
    sql = "select id_cip_sec, date_format(visi_data_visita, '%Y%m%d'), visi_servei_codi_servei, s_espe_codi_especialitat, concat(codi_sector, visi_tipus_citacio, visi_tipus_visita), \
           visi_rel_proveidor, left(visi_col_prov_resp, 8), visi_up from {}, nodrizas.dextraccio \
           where visi_data_visita between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext and visi_situacio_visita = 'R'".format(taula)
    for id, dat, servei, especialitat, tipus, relacio, numcol, up in getAll(sql, og):
        if id in pacients:
            data.append([id, dat, especialitat, hm[tipus] if tipus in hm else '', relacio, 1 if numcol in pacients[id]['c'] else 0, 1 if numcol in pacients[id]['d'] else 0, 1 if servei in gc_serveis or numcol in gc_numcols else 0, up])
    listToTable(data, tb['vis'], db)


if __name__ == '__main__':
    __delegats = defaultdict(list)
    for nc, ncd in getAll('select delm_numcol, delm_numcol_del from cat_pritb777', og):
        __delegats[nc].append(ncd)
    numcols = defaultdict(list)
    delegats = defaultdict(list)
    for up, uba, tipus, col in getAll("select up, uab, tipus, left(ide_numcol, 8) from cat_professionals where tipus in ('M', 'I')", og):
        numcols[(up, uba, tipus)].append(col)
        if col in __delegats:
            delegats[(up, uba, tipus)].extend(__delegats[col])

    atdom = set([id for id, in getAll('select id_cip_sec from eqa_problemes where ps = 45', 'nodrizas')])
    pacients = set()
    pacient_numcol = {}
    for id, up, uba, upinf, ubainf, pcc, maca in getAll('select id_cip_sec, up, uba, upinf, ubainf, pcc, maca from assignada_tot', db):
        if pcc or maca or id in atdom:
            pacients.add((id,))
            pacient_numcol[id] = {'c': [], 'd': []}
            for row in [(up, uba, 'M'), (upinf, ubainf, 'I')]:
                if row in numcols:
                    pacient_numcol[id]['c'].extend(numcols[row])
                    if row in delegats:
                        pacient_numcol[id]['d'].extend(delegats[row])

    createTable(tb['pac'], '(id_cip_sec int, primary key (id_cip_sec))', db, rm=True)
    listToTable(pacients, tb['pac'], db)

    createTable(tb['vis'], '(id_cip_sec int, dat date, especialitat varchar(5), tipus varchar(4), relacio int, numcol int, delegat int, gcasos int, up varchar(5))', db, rm=True)
    tables = getOne('show create table visites1', og)[1].split('UNION=(')[1][:-1].split(',')
    jobs = [(table, pacient_numcol) for table in tables]
    multiprocess(do_it, jobs, close=True)
    # do_it(jobs[0])
