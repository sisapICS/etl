from sisapUtils import getOne, getAll, createTable, listToTable, exportKhalix
from string import ascii_lowercase
import sys


valors = {(1, 1): 'AMBTS',
          (0, 1): 'SENSETS',
          (1, 0): 'SENSEODO',
          (0, 0): 'SENSETSODO'}
tb_in = 'cat_centres'
db_in = 'nodrizas'
tb_out = 'exp_khalix_centres'
db_out = 'altres'
file = 'CENTRES_CATALEG'
period, = getOne('select year(data_ext) from dextraccio', db_in)

planificat = {}
sql = """SELECT ics_codi, fase FROM centres_pilots"""
for up, fase in getAll(sql, 'pdp'):
    planificat[up] = fase
print(len(planificat))

sql = "select ics_codi, soc, odn from {0}".format(tb_in)
resul = []
for centre, soc, odn in getAll(sql, db_in):
    # resul.append(('TSODOTIP',
    #       'DEF{}'.format(period),
    #       centre,
    #       'NOCLI',
    #       'NOCAT',
    #       'NOIMP',
    #       'DIM6SET',
    #       'S',
    #       valors[(soc, odn)]))
    if centre in planificat:
        resul.append(('PLANIFICAT',
          'DEF{}'.format(period),
          centre,
          'NOCLI',
          'NOCAT',
          'NOIMP',
          'DIM6SET',
          'S',
          planificat[centre]))


cols = ', '.join(('{} varchar(10)'.format(ascii_lowercase[i]) for i in range(9)))
createTable(tb_out, '({})'.format(cols), db_out, rm=True)
listToTable(resul, tb_out, db_out)

error = []
query = 'select * from {}.{}'.format(db_out, tb_out)
error.append(exportKhalix(query, file))
if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
