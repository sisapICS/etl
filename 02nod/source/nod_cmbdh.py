
import sisapUtils as u
from collections import defaultdict

imp_db = 'import'
imp_tb = 'cmbdh_exadata'
nod_db = 'nodrizas'
nod_tb_dx = 'nod_cmbdh_dx_exadata'
nod_tb_px = 'nod_cmbdh_px_exadata'

fields = """(id_cip_sec int, id_cip int, codi_sector varchar(4), nia int, eap varchar(5),
            d_naix date, sexe varchar(1), up_ass varchar(5), d_ingres date, c_ingres varchar(1), 
            pr_ingres varchar(1), pr_interna varchar(255), d_alta date, 
            d_ingres_uci date, d_alta_uci date, c_alta varchar(255), ser_alta varchar(255), 
            t_act varchar(255), dp varchar(255), ds1 varchar(255), ds2 varchar(255), 
            ds3 varchar(255), ds4 varchar(255), ds5 varchar(255), ds6 varchar(255), ds7 varchar(255), 
            ds8 varchar(255), ds9 varchar(255), ds10 varchar(255), ds11 varchar(255), 
            ds12 varchar(255), ds13 varchar(255), ds14 varchar(255),
            pp varchar(255), ps1 varchar(255), ps2 varchar(255), ps3 varchar(255), ps4 varchar(255), 
            ps5 varchar(255), ps6 varchar(255), ps7 varchar(255), ps8 varchar(255), ps9 varchar(255), 
            ps10 varchar(255), ps11 varchar(255), ps12 varchar(255), ps13 varchar(255), 
            ps14 varchar(255), ps15 varchar(255), ps16 varchar(255), ps17 varchar(255), 
            ps18 varchar(255), ps19 varchar(255))"""
u.createTable(imp_tb, fields, imp_db, rm=True)

cip_hash = {}
sql = """SELECT usua_cip, usua_cip_cod FROM pdptb101"""
for cip, hash in u.getAll(sql, 'pdp'):
    cip_hash[cip] = hash

hash_id_cip = defaultdict(defaultdict)
sql = """select id_cip, id_cip_sec, codi_sector, hash_d 
                from nodrizas.eqa_u11_with_jail"""
for id_cip, id_cip_sec, sector, hash in u.getAll(sql, 'nodrizas'):
    hash_id_cip[hash][id_cip_sec] = [id_cip, sector]

upload = []
sql = """SELECT SUBSTR(b.cip, 0, 13), a.NIA,
            a.EAP, a.DATA_NAIXEMENT, a.SEXE, a.UP_ASSISTENCIA, a.DATA_INGRES,
            a.C_INGRES, a.PR_INGRES, a.PR_INTERNA, a.DATA_ALTA, 
            a.DATA_INGRES_uci, a.DATA_ALTA_UCI,
            a.C_ALTA, a.SER_ALTA, a.T_ACT, a.DP, a.ds1,
            a.ds2, a.ds3, a.ds4, a.ds5, a.ds6,
            a.ds7, a.ds8, a.ds9, a.ds10, a.ds11,
            a.ds12, a.ds13, a.ds14, a.pp,
            a.PS1, a.ps2, a.PS3, a.PS4, a.ps5,
            a.ps6, a.ps7, a.ps8, a.ps9, a.ps10,
            a.ps11, a.ps12, a.ps13, a.ps14, a.ps15,
            a.ps16, a.ps17, a.ps18, a.ps19
            FROM dwcatsalut.tf_cmbdha a, DWSISAP.rca_cip_nia b
            WHERE a.nia = b.nia
            and t_act = '11' and pr_ingres != 7
            and pr_ingres != 1
            and DATA_INGRES-DATA_ALTA > 1"""
for (cip, nia, eap, d_naix, sexe, up_ass, d_ingres, c_ingres, pr_ingres, pr_interna, d_alta,
        d_ingres_uci, d_alta_uci, c_alta, ser_alta, t_atc, dp, ds1, ds2, ds3, ds4, ds5, ds6,
        ds7, ds8, ds9, ds10, ds11, ds12, ds13, ds14, pp, ps1, ps2, ps3, ps4, ps5, ps6, ps7, 
        ps8, ps9, ps10, ps11, ps12, ps13, ps14, ps15, ps16, ps17, ps18, ps19) in u.getAll(sql, 'exadata'):
    if cip in cip_hash:
        hash = cip_hash[cip]
        if hash in hash_id_cip:
            for id_cip_sec in hash_id_cip[hash]:
                id_cip, sector = hash_id_cip[hash][id_cip_sec]
                upload.append((id_cip_sec, id_cip, sector, nia, eap, d_naix, sexe, up_ass, d_ingres, c_ingres, pr_ingres, pr_interna, d_alta,
                    d_ingres_uci, d_alta_uci, c_alta, ser_alta, t_atc, dp, ds1, ds2, ds3, ds4, ds5, ds6,
                    ds7, ds8, ds9, ds10, ds11, ds12, ds13, ds14, pp, ps1, ps2, ps3, ps4, ps5, ps6, ps7, 
                    ps8, ps9, ps10, ps11, ps12, ps13, ps14, ps15, ps16, ps17, ps18, ps19))

u.listToTable(upload, imp_tb, imp_db)

fields = '(id_cip_sec int, id_ingres int, up varchar(5), dnaix date, sexe varchar(1), dingres date, dalta date, cingres varchar(1), calta varchar(1), procedencia varchar(1), codi varchar(5), posicio int)'
u.createTable(nod_tb_dx, fields, nod_db, rm=True)
u.createTable(nod_tb_px, fields, nod_db, rm=True)


idcip = defaultdict(set)
sql = 'select usua_nia, id_cip_sec from crg'
for nia, id in u.getAll(sql, imp_db):
    idcip[nia].add(id)

sql = """select nia, eap, d_naix, sexe, d_ingres, d_alta, c_ingres, c_alta, pr_ingres, 
        dp, ds1, ds2, ds3, ds4, ds5, ds6, ds7, ds8, ds9, ds10, ds11, ds12, ds13, ds14,
        pp, ps1, ps2, ps3, ps4, ps5, ps6, ps7,
        ps8, ps9, ps10, ps11, ps12, ps13, ps14, ps15, ps16, ps17, ps18, ps19 from {}""".format(imp_tb)
data_dx, data_px = [], []
ing = 0
for row in u.getAll(sql, imp_db):
    nia = row[0]
    if nia in idcip:
        base = row[1:9]
        dx = row[9:24]
        px = row[24:44]
        ing += 1
        for id in idcip[nia]:
            for d in range(15):
                if dx[d]:
                    data_dx.append((id, ing) + base + (dx[d], d + 1))
            for p in range(20):
                if px[p]:
                    data_px.append((id, ing) + base + (px[p], p + 1))


u.listToTable(data_dx, nod_tb_dx, nod_db)
u.listToTable(data_px, nod_tb_px, nod_db)
