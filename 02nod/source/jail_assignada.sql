use nodrizas;

drop table if exists jail_assignada;
drop table if exists jail_assignada_1;
drop table if exists jail_assignada_2;
create table jail_assignada_1 like assignada_tot;
insert ignore into jail_assignada_1
select
    id_cip_sec
    ,id_cip
    ,codi_sector
    ,usua_uab_ep ep
    ,usua_uab_up up
    ,usua_uab_codi_uab uba
    ,usua_abs_codi_abs
    ,ass_codi_up upinf
    ,ass_codi_unitat ubainf
    ,usua_uab_servei_centre centre_codi
    ,usua_uab_servei_centre_classe centre_classe
    ,usua_up_rca
    ,usua_data_naixement data_naix
    ,(year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')) edat
    ,usua_sexe sexe
    ,usua_nacionalitat nacionalitat
    ,cart_tipus nivell_cobertura
    ,usua_relacio relacio
    ,usua_data_peticio_lms
    ,usua_data_connexio_lms
    ,'' uas
    ,'' upOrigen
    ,'' upABS
    ,'' seccio_censal
    ,'' last_visit
    ,1 ates
    ,0 n_year
    ,'' last_odn
    ,0 ates_inf
    ,'' last_visit_novc
    ,0 ates_novc
    ,0 n_year_novc
    ,'' last_odn_novc
    ,0 ates_inf_novc
    ,'' up_residencia
    ,0 institucionalitzat
    ,0 institucionalitzat_ps
    ,0 atdom
    ,0 trasplantat
    ,0 maca
    ,0 pcc
    ,'' piramide
from
    import_jail.assignada a
    ,nodrizas.dextraccio
where
    usua_data_naixement<=data_ext
    and usua_situacio='A'
    and exists (select '1' from nodrizas.jail_centres b where a.usua_uab_up=b.scs_codi)
    and concat(usua_uab_up, usua_uab_codi_uab) <> '07674M15'
;

update jail_assignada_1 a inner join jail_atesa b on a.id_cip_sec = b.id_cip_sec and a.up = b.up
set ates = 1, last_visit = ingres, ates_novc = 1, last_visit_novc = ingres
;

create table jail_assignada_2
select
	ja.*,
	ates_inf_2
from
	jail_assignada_1 ja
left join (
	select
		distinct id_cip_sec,
		1 ates_inf_2
	from
		import_jail.visites,
		dextraccio
	where
		visi_col_prov_resp like '3%'
		and s_espe_codi_especialitat not in ('EXTRA', '10102')
		and visi_tipus_visita not in ('9E', 'EXTRA', 'EXT')
		and visi_situacio_visita = 'R'
		and visi_servei_codi_servei = 'INF'
		and visi_modul_codi_modul not like 'VCA%'
		and visi_modul_codi_modul not like 'VCM%'
		and visi_modul_codi_modul not like 'VCP%'
		and visi_modul_codi_modul not like 'VCJ%'
		and visi_modul_codi_modul not in ('VCAP', 'VCAP2', 'VCAP3')
		and visi_data_visita between adddate(data_ext, INTERVAL -1 YEAR) and data_ext) jv
on ja.id_cip_sec = jv.id_cip_sec;

update
	nodrizas.jail_assignada_2
set
	ates_inf = 1
where
	ates_inf_2 = 1;

alter table nodrizas.jail_assignada_2 drop ates_inf_2;

create table jail_assignada like assignada_tot;
insert ignore into jail_assignada
select
	*
from
	nodrizas.jail_assignada_2;

drop table if exists jail_assignada_1;
drop table if exists jail_assignada_2;

drop table if exists assignada_tot_with_jail;
create table assignada_tot_with_jail like assignada_tot;
alter table assignada_tot_with_jail ENGINE=MERGE;
alter table assignada_tot_with_jail UNION=(assignada_tot,jail_assignada);