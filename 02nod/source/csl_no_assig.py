import collections as c
from sisapUtils import createTable, getOne, multiprocess, getAll, listToTable


orig = 'import'
table = 'csl_no_assig'
db = 'nodrizas'

def KhxCobertura(cobertura):
    if cobertura == 'AC':
        return 'USUACT'
    elif cobertura == 'PN':
        return 'USUPEN'
    elif cobertura == 'FG':
        return 'USUPEN'
    else:
        return 'USUALT'


def do_it(params):
    taula, poblacio, edatKHLX = params
    data = c.Counter()
    sql = "select id_cip, visi_up, visi_data_visita \
           from {}, nodrizas.dextraccio \
           where visi_data_visita between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext and visi_situacio_visita = 'R' and visi_rel_proveidor in ('3', '4')".format(taula)
    for id, up, dat in getAll(sql, orig):
        in_aga = 1
        if up in up_to_aga:
            if id in poblacio:
                if up != poblacio[id][1]:
                    aga, up_a, sexe, edat, nacio, cob = poblacio[id]
                    if up_to_aga[up] != aga:
                        in_aga = 0
                    if int(edat) in edatKHLX:
                        edat = edatKHLX[int(edat)]
                        data[(id, up, sexe, edat, nacio, KhxCobertura(cob), dat.month, in_aga)] += 1
    resul = [k + (v,) for k, v in data.items()]
    return(resul)


if __name__ == '__main__':
    up_to_aga = {up: aga for (up, aga) in getAll("select scs_codi, if(aga='',scs_codi, aga) from cat_centres", "nodrizas")}
    edatKHLX = {edat: khalix for (khalix, edat) in getAll('select khalix, edat from khx_edats5a', 'nodrizas')}
    nacionalitat = {int(n): k for (k,n) in getAll('select codi_k, codi_nac from cat_nacionalitat', 'nodrizas')}
    poblacio = {}
    for id, up, sexe, edat, nac, nivell_cobertura in getAll("select id_cip, usua_uab_up, usua_sexe, round(datediff(data_ext, usua_data_naixement)/365) as edat, if(usua_nacionalitat = '', 724, usua_nacionalitat), cart_tipus from import.assignada, nodrizas.dextraccio", 'import'):
        nacio = int(nac)
        try:
            nacio = nacionalitat[nacio]
        except KeyError:
            nacio = 'NAC19'
        if up in up_to_aga:
            poblacio[id] = [up_to_aga[up], up, sexe, edat, nacio, nivell_cobertura]
        else:
            poblacio[id] = ['', up, sexe, edat, nacio, nivell_cobertura]
            continue
    tables = getOne('show create table visites1', 'import')[1].split('UNION=(')[1][:-1].split(',')
    jobs = [(t, poblacio, edatKHLX) for t in tables]
    resul = multiprocess(do_it, jobs)
    createTable(table, '(id_cip int, up varchar(5), sexe varchar(1), edat varchar(10), nacionalitat varchar(10), cobertura varchar(7), visites int, mesos int, igual_aga int)', db, rm=True)
    visites = c.Counter()
    mesos = c.defaultdict(set)
    for chunk in resul:
        for id, up, sexe, edat, nacio, cobertura, mes, aga, n in chunk:
            visites[(id, up, sexe, edat, nacio, cobertura, aga)] += n
            mesos[(id, up)].add(mes)
    upload = [(id, up, sexe, edat, nacio, cobertura, n, len(mesos[(id, up)]), aga) for (id, up, sexe, edat, nacio, cobertura, aga), n in visites.items()]
    listToTable(upload, table, db)

    sql = "select id_cip, hash_d from u11 where codi_sector = '6102'"
    u11 = {row[0]: row[1] for row in getAll(sql, "import")}
    sql = "select id_cip, b.sector, up, visites, mesos \
           from {} a \
           inner join cat_centres b on a.up = b.scs_codi \
           where mesos > 2 and amb_codi = '09'".format(table)
    upload = [(u11[row[0]],) + row[1:] for row in getAll(sql, "nodrizas")]
    cols = "(hash varchar2(40), sector varchar2(4), up varchar(5), \
             visites int, mesos int)"
    createTable("atesa_pirineu", cols, "pdp", rm=True)
    listToTable(upload, "atesa_pirineu", "pdp")
