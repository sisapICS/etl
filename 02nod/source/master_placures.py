# coding: UTF-8

# Importaci� de les biblioteques necessaries
from datetime import datetime, date
import collections as c
import hashlib as h
import psutil as p
import sisapUtils as u

# Definici� de constants
PRODUCCIO = 1
tb = 'MASTER_PLACURES'
tb_jail = tb + '_JAIL'
tb_intervencions_planificades = 'MASTER_PLACURES_INTERVENCIONS_PLANIFICADES'
tb_intervencions_planificades_jail = tb_intervencions_planificades + '_JAIL'
tb_intervencions = 'MASTER_PLACURES_INTERVENCIONS'
tb_intervencions_jail = tb_intervencions + '_JAIL'
tb_diagnostics = 'MASTER_PLACURES_DIAGNOSTICS'
tb_diagnostics_jail = tb_diagnostics + '_JAIL'
db = 'nodrizas'
db_exadata = 'exadata'

# codis dels Plans de Cures de ferides cr�niques i nafres
CODIS_PC_NAFRES_I_FERIDES_CRONIQUES = ("PC0035", "PC0073", "PC0074", "PC0086", "PC0087", "PC0088", "PC0111", "PC0128", "PC0129") #TODO: el 0035 forma parte de PC de ferides croniques pero no se est� pidiendo, pese a todo lo he incluido en el indicador general

# Columnes i formats de columne per les taules m�ster, que viuran a les bbdd local (nodrizas) i exadata
COLS_FINAL_FORMAT_DBLOCAL = '(id_cip_sec int(11), up_assignada varchar(5), codi_sector varchar(4), pc_id varchar(14), pc_codi varchar(12), data_inici date, data_fi date, pc_ultima_intervencio_data date, flag_aguda int, flag_pc_obert int, flag_pc_tancat_ultim_any int, flag_pc_tanc_ultany_naf_fer_cr int, flag_pc_alta_anual int, flag_pc_alta_anual_deixarfumar int, flag_pc_seguiment_i_cronicitat int, flag_pc_alta_mensual int, flag_pc_alta_3_mesos int, flag_pc_alta_setmanal int, flag_pc_actiu int)'
COLS_FINAL_FORMAT_EXADATA = '(hash_d varchar2(40), up_assignada varchar2(5), codi_sector varchar2(4), pc_id varchar2(14), pc_codi varchar2(12), data_inici date, data_fi date, pc_ultima_intervencio_data date, flag_aguda int, flag_pc_obert int, flag_pc_tancat_ultim_any int, flag_pc_tanc_ultany_naf_fer_cr int, flag_pc_alta_anual int, flag_pc_alta_anual_deixarfumar int, flag_pc_seguiment_i_cronicitat int, flag_pc_alta_mensual int, flag_pc_alta_3_mesos int, flag_pc_alta_setmanal int, flag_pc_actiu int)'

COLS_FINAL_INTERVENCIONS_PLANIFICADES_FORMAT_DBLOCAL = '(id_cip_sec int(11), codi_sector varchar(4), pc_id varchar(14), pc_codi varchar(12), pc_data_inici date, pc_data_fi date, intervencions_data_inici date, intervencions_data_fi date, intervencio_codi varchar(20))'
COLS_FINAL_INTERVENCIONS_PLANIFICADES_FORMAT_EXADATA = '(hash_d varchar2(40), codi_sector varchar2(4), pc_id varchar2(14), pc_codi varchar2(12), pc_data_inici date, pc_data_fi date, intervencions_data_inici date, intervencions_data_fi date, intervencio_codi varchar2(20))'

COLS_FINAL_INTERVENCIONS_FORMAT_DBLOCAL = '(id_cip_sec int(11), codi_sector varchar(4), pc_id varchar(14), pc_codi varchar(12), pc_data_inici date, pc_data_fi date, intervencio_data date, intervencio_codi varchar(20))'
COLS_FINAL_INTERVENCIONS_FORMAT_EXADATA = '(hash_d varchar2(40), codi_sector varchar2(4), pc_id varchar2(14), pc_codi varchar2(12), pc_data_inici date, pc_data_fi date, intervencio_data date, intervencio_codi varchar2(20))'

COLS_FINAL_DIAGNOSTICS_FORMAT_DBLOCAL = '(id_cip_sec int(11), codi_sector varchar(4), pc_id varchar(14), pc_codi varchar(12), pc_data_inici date, pc_data_fi date, diagnostics_data_inici date, diagnostics_data_fi date, diagnostics_codi varchar(20))'
COLS_FINAL_DIAGNOSTICS_FORMAT_EXADATA = '(hash_d varchar2(40), codi_sector varchar2(4), pc_id varchar2(14), pc_codi varchar2(12), pc_data_inici date, pc_data_fi date, diagnostics_data_inici date, diagnostics_data_fi date, diagnostics_codi varchar2(20))'

# Dates

def get_dates():
    """."""

    global dext, dext_menys1any, dext_menys1mes, dext_menys2mesos, dext_menys3mesos, dext_menys1setmana

    sql = """
            SELECT
                data_ext,
                date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
                date_add(date_add(data_ext, INTERVAL -1 MONTH), INTERVAL + 1 DAY),
                date_add(date_add(data_ext, INTERVAL -2 MONTH), INTERVAL + 1 DAY),
                date_add(date_add(data_ext, INTERVAL -3 MONTH), INTERVAL + 1 DAY),
                date_add(date_add(data_ext, INTERVAL -1 WEEK), INTERVAL + 1 DAY)
            FROM
                dextraccio
            """
    dext, dext_menys1any, dext_menys1mes, dext_menys2mesos, dext_menys3mesos, dext_menys1setmana = u.getOne(sql, "nodrizas")


def get_poblacio():
    """Obt� la poblaci� assignada a cada Centre d'Atenci� Prim�ria."""

    global poblacio

    sql = """
            SELECT
                id_cip_sec,
                up
            FROM
                assignada_tot_with_jail
            """
    poblacio = {id_cip_sec: up for id_cip_sec, up in u.getAll(sql, "nodrizas")}


def crear_taules():
    """Crea les taules a la base de dades Exadata i local."""

    u.createTable(tb, COLS_FINAL_FORMAT_EXADATA, db_exadata, rm=True)
    u.createTable(tb_jail, COLS_FINAL_FORMAT_EXADATA, db_exadata, rm=True)
    u.createTable(tb_intervencions_planificades, COLS_FINAL_INTERVENCIONS_PLANIFICADES_FORMAT_EXADATA, db_exadata, rm=True)
    u.createTable(tb_intervencions_planificades_jail, COLS_FINAL_INTERVENCIONS_PLANIFICADES_FORMAT_EXADATA, db_exadata, rm=True)
    u.createTable(tb_intervencions, COLS_FINAL_INTERVENCIONS_FORMAT_EXADATA, db_exadata, rm=True)
    u.createTable(tb_intervencions_jail, COLS_FINAL_INTERVENCIONS_FORMAT_EXADATA, db_exadata, rm=True)
    u.createTable(tb_diagnostics, COLS_FINAL_DIAGNOSTICS_FORMAT_EXADATA, db_exadata, rm=True)
    u.createTable(tb_diagnostics_jail, COLS_FINAL_DIAGNOSTICS_FORMAT_EXADATA, db_exadata, rm=True)    
    u.grantSelect(tb, 'DWSISAP_ROL', db_exadata)
    u.grantSelect(tb_jail, 'DWSISAP_ROL', db_exadata)
    u.grantSelect(tb_intervencions, 'DWSISAP_ROL', db_exadata)
    u.grantSelect(tb_intervencions_jail, 'DWSISAP_ROL', db_exadata)
    u.grantSelect(tb_diagnostics, 'DWSISAP_ROL', db_exadata)
    u.grantSelect(tb_diagnostics_jail, 'DWSISAP_ROL', db_exadata)
    u.grantSelect(tb_intervencions_planificades, 'DWSISAP_ROL', db_exadata)
    u.grantSelect(tb_intervencions_planificades_jail, 'DWSISAP_ROL', db_exadata)
    u.createTable(tb, COLS_FINAL_FORMAT_DBLOCAL, db, rm=True)
    u.createTable(tb_jail, COLS_FINAL_FORMAT_DBLOCAL, db, rm=True)
    u.createTable(tb_intervencions_planificades, COLS_FINAL_INTERVENCIONS_PLANIFICADES_FORMAT_DBLOCAL, db, rm=True)
    u.createTable(tb_intervencions_planificades_jail, COLS_FINAL_INTERVENCIONS_PLANIFICADES_FORMAT_DBLOCAL, db, rm=True)
    u.createTable(tb_intervencions, COLS_FINAL_INTERVENCIONS_FORMAT_DBLOCAL, db, rm=True)
    u.createTable(tb_intervencions_jail, COLS_FINAL_INTERVENCIONS_FORMAT_DBLOCAL, db, rm=True)
    u.createTable(tb_diagnostics, COLS_FINAL_DIAGNOSTICS_FORMAT_DBLOCAL, db, rm=True)
    u.createTable(tb_diagnostics_jail, COLS_FINAL_DIAGNOSTICS_FORMAT_DBLOCAL, db, rm=True)


def get_pc_agudes_i_nom():
    """Obt� la informaci� sobre els codis de Plans de Cures d'aguda i els seus noms."""

    global pc_agudes, pc_noms
    pc_agudes = c.defaultdict(set)
    pc_noms = dict()
    
    sql = """
            SELECT DISTINCT 
                pc_codi,
                pc_nom,
                pc_aguda
            FROM
                cat_prstb335
        """
    for pc_codi, pc_nom, pc_aguda in u.getAll(sql, "import"):
        # Afegeix el codi de Pla de Cura d'aguda al diccionari pc_agudes
        pc_agudes[pc_aguda].add(pc_codi)
        # Afegeix el nom del Pla de Cura d'aguda al diccionari pc_noms
        pc_noms[pc_codi] = pc_nom


def get_resvech_ultims2mesos():
    """."""
    
    global resvech_ultims2mesos
    resvech_ultims2mesos = set()

    sql = """
            SELECT
                id_cip_sec
            FROM
                xml_detall
            WHERE
                xml_origen = 'GABINETS'
                AND xml_tipus_orig  = 'XML0000075'
                AND camp_codi = 'Resvech_Total'
                AND xml_data_alta BETWEEN '{}' AND '{}'
            """.format(dext_menys2mesos, dext)
    for id_cip_sec, in u.getAll(sql, "import"):
        resvech_ultims2mesos.add(id_cip_sec)


def get_placures_sector(sector):
    """Obt� les dades dels plans de cures per a un sector espec�fic."""

    # Seleccionar si importem d'import o import_jail, en funci� del sector
    db_querys = "import" if sector != "6951" else "import_jail"

    # Inicialitzar les estructures de dades
    idcipsec_2_hash = dict()
    
    placures = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(set)))
    placures_alta_antics = set()
    placures_alta_deixarfumar_antics = set()
    placures_seguimenticronicitat_antics = set()
    placures_actius_antics = set()
    placures_alta_mensual = set()
    placures_alta_mensual_antics = set()
    placures_alta_ultims_3_mesos_antics = set()
    placures_alta_setmanal_antics = set()

    resultat_local = list()
    resultat_exadata = list()
    resultat_local_intervencions_planificades = list()
    resultat_exadata_intervencions_planificades = list()
    resultat_local_intervencions = list()
    resultat_exadata_intervencions = list()
    resultat_local_diagnostics = list()
    resultat_exadata_diagnostics = list()

    # Consulta per obtenir un conversor d'id_cip_sec a hash
    sql_u11 = """
                SELECT
                    id_cip_sec,
                    hash_d
                FROM
                    eqa_u11_with_jail
                WHERE
                    codi_sector = {}
              """.format(sector)
    for id_cip_sec, hash_d in u.getAll(sql_u11, "nodrizas"):
        if id_cip_sec in poblacio:
            idcipsec_2_hash[id_cip_sec] = hash_d              

    # Consulta per obtenir les dades de PRSTB337 / ARES
    tb_prstb337 = "ares_s{}".format(sector) if sector != "6951" else "ares"
    sql_prstb337 = """
                    SELECT
                        id_cip_sec,
                        pi_codi_seq AS pc_id,
                        pi_codi_pc,
                        CASE
                            WHEN date_add(data_ext, INTERVAL - 1 YEAR) < pi_data_inici THEN 1
                            ELSE 0
                        END AS iniciat_ultim_any,
                        CASE
                            WHEN date_add(data_ext, INTERVAL - 1 MONTH) < pi_data_inici THEN 1
                            ELSE 0
                        END AS iniciat_ultim_mes,
                        CASE
                            WHEN date_add(data_ext, INTERVAL - 3 MONTH) < pi_data_inici THEN 1
                            ELSE 0
                        END AS iniciat_ultim_3_mesos,
                        CASE
                            WHEN date_add(data_ext, INTERVAL - 1 WEEK) < pi_data_inici THEN 1
                            ELSE 0
                        END AS iniciat_ultima_setmana,
                        CASE
                            WHEN pi_data_fi > date_add(date_add(data_ext,interval - 1 month),interval +1 day) THEN 1
                            ELSE 0
                        END AS no_tancat_fa_un_mes,
                        CASE
                            WHEN (pi_data_fi <= data_ext AND pi_data_fi <> '0000-00-00') THEN 1
                            ELSE 0
                        END AS finalitzat,
                        pi_data_inici,
                        CASE WHEN pi_data_fi = '0000-00-00' THEN data_ext ELSE pi_data_fi END AS data_fi,
                        CASE WHEN pi_data_fi > date_add(date_add(data_ext,interval - 1 year),interval +1 day)
                             OR pi_data_fi = '0000-00-00' THEN 1 ELSE 0 END AS calcul_flags
                    FROM
                        {},
                        nodrizas.dextraccio
                    WHERE
                        (pi_data_fi > date_add(date_add(data_ext,interval - 2 year),interval +1 day)
                        OR pi_data_fi = '0000-00-00')
                        AND pi_data_inici <= data_ext
                """.format(tb_prstb337)
    for id_cip_sec, pc_id, pc_codi, iniciat_ultim_any, iniciat_ultim_mes, iniciat_ultim_3_mesos, iniciat_ultima_setmana, no_tancat_fa_un_mes, finalitzat, pc_data_inici, pc_data_fi, calcul_flags in u.getAll(sql_prstb337, db_querys):
        if id_cip_sec in poblacio:
            
            # Emplenar el diccionari placures amb les dades de la consulta
            placures[id_cip_sec][pc_id]["pc_codi"] = pc_codi
            placures[id_cip_sec][pc_id]["data_inici"] = pc_data_inici
            placures[id_cip_sec][pc_id]["data_fi"] = pc_data_fi

            placures[id_cip_sec][pc_id]["flag_pc_obert"] = 0
            placures[id_cip_sec][pc_id]["flag_pc_tancat_ultim_any"] = 0
            placures[id_cip_sec][pc_id]["flag_pc_tancat_nafres_i_ferides_croniques"] = 0
            placures[id_cip_sec][pc_id]["flag_pc_alta_anual"] = 0
            placures[id_cip_sec][pc_id]["flag_pc_alta_anual_deixarfumar"] = 0
            placures[id_cip_sec][pc_id]["flag_pc_seguiment_i_cronicitat"] = 0
            placures[id_cip_sec][pc_id]["flag_pc_alta_mensual"] = 0
            placures[id_cip_sec][pc_id]["flag_pc_alta_setmanal"] = 0
            placures[id_cip_sec][pc_id]["flag_pc_alta_3mesos"] = 0
            placures[id_cip_sec][pc_id]["flag_pc_actiu"] = 0

            placures[id_cip_sec][pc_id]["intervencions_codis"] = c.defaultdict(lambda: c.defaultdict(set))
            placures[id_cip_sec][pc_id]["ultima_intervencio_data"] = date(1900,1,1)            

             # Establecer variables amb el prefix "flags" en funci� de les dades obtingudes
            if calcul_flags:

                if not finalitzat:
                    # P.C. Oberts
                    placures[id_cip_sec][pc_id]["flag_pc_obert"] = 1

                elif finalitzat:
                    # P.C. Tancats
                    placures[id_cip_sec][pc_id]["flag_pc_tancat_ultim_any"] = 1
                    if pc_codi in CODIS_PC_NAFRES_I_FERIDES_CRONIQUES:
                        placures[id_cip_sec][pc_id]["flag_pc_tancat_nafres_i_ferides_croniques"] = 1

                if iniciat_ultim_any:
                    # P.C. Alta
                    placures[id_cip_sec][pc_id]["flag_pc_alta_anual"] = 1
                    if pc_codi == "PC0016":
                        placures[id_cip_sec][pc_id]["flag_pc_alta_anual_deixarfumar"] = 1
                    # P.C. seguiment i Cronicitat
                    if pc_codi in pc_agudes["N"]:
                        placures[id_cip_sec][pc_id]["flag_pc_seguiment_i_cronicitat"] = 1
                    # P.C. Actius
                    if not finalitzat:
                        placures[id_cip_sec][pc_id]["flag_pc_actiu"] = 1

                elif not iniciat_ultim_any:
                    # P.C. Alta
                    placures_alta_antics.add(pc_id)
                    if pc_codi == "PC0016":
                        placures_alta_deixarfumar_antics.add(pc_id)
                    # P.C. seguiment i Cronicitat
                    if pc_codi in pc_agudes["N"]:
                        placures_seguimenticronicitat_antics.add(pc_id)
                    # P.C. Actius
                    if not finalitzat:
                        placures_actius_antics.add(pc_id)

                # P.C. Alta Mensual
                if iniciat_ultim_mes:
                    placures[id_cip_sec][pc_id]["flag_pc_alta_mensual"] = 1

                elif not iniciat_ultim_mes:
                    placures_alta_mensual_antics.add(pc_id)

                # P.C. Alta 3 mesos
                if iniciat_ultim_3_mesos:
                    placures[id_cip_sec][pc_id]["flag_pc_alta_3mesos"] = 1

                elif not iniciat_ultim_3_mesos:
                    placures_alta_ultims_3_mesos_antics.add(pc_id)

                # P.C. Alta Setmanal
                if iniciat_ultima_setmana:
                    placures[id_cip_sec][pc_id]["flag_pc_alta_setmanal"] = 1

                elif not iniciat_ultima_setmana:
                    placures_alta_setmanal_antics.add(pc_id)

                # P.C Actius amb nafres i ferides cr�niques
                if pc_codi in CODIS_PC_NAFRES_I_FERIDES_CRONIQUES and id_cip_sec in resvech_ultims2mesos and not finalitzat:
                    placures[id_cip_sec][pc_id]["flag_pc_actiu"] = 1


    print("      - Ares: sector {}".format(sector))

    tb_prstb338 = "atic_s{}".format(sector) if sector != "6951" else "atic"
    sql_prstb338 = """
                    SELECT
                        id_cip_sec,
                        eli_codi_seq AS pc_id,
                        eli_codi_element,
                        eli_dia_inici,
                        eli_dia_fi,
                        eli_tipus_element
                    FROM
                        {},
                        nodrizas.dextraccio
                    WHERE
                        eli_dia_inici <= data_ext
                """.format(tb_prstb338)
    for id_cip_sec, pc_id, intervencio_codi, eli_dia_ini, eli_dia_fi, eli_tipus_element in u.getAll(sql_prstb338, db_querys):
        if pc_id in placures[id_cip_sec]:
            placures[id_cip_sec][pc_id]["intervencions_codis"][intervencio_codi]["data_inici"] = eli_dia_ini
            if eli_dia_fi:
                if eli_dia_fi < placures[id_cip_sec][pc_id]["data_fi"]:
                    placures[id_cip_sec][pc_id]["intervencions_codis"][intervencio_codi]["data_fi"] = eli_dia_fi
                else:
                    placures[id_cip_sec][pc_id]["intervencions_codis"][intervencio_codi]["data_fi"] = placures[id_cip_sec][pc_id]["data_fi"]
            else:
                placures[id_cip_sec][pc_id]["intervencions_codis"][intervencio_codi]["data_fi"] = placures[id_cip_sec][pc_id]["data_fi"]
            hash_d = idcipsec_2_hash[id_cip_sec]
            pc_codi = placures[id_cip_sec][pc_id]["pc_codi"]
            pc_data_inici = placures[id_cip_sec][pc_id]["data_inici"]
            pc_data_fi = placures[id_cip_sec][pc_id]["data_fi"]
            eli_dia_fi = pc_data_fi if pc_data_fi < dext and (eli_dia_fi is None or eli_dia_fi > pc_data_fi) else eli_dia_fi
            if eli_tipus_element not in ('D', 'AE'):
                resultat_local_intervencions_planificades.append((id_cip_sec, sector, pc_id, pc_codi, pc_data_inici, pc_data_fi, eli_dia_ini, eli_dia_fi, intervencio_codi))
                resultat_exadata_intervencions_planificades.append((hash_d, sector, pc_id, pc_codi, pc_data_inici, pc_data_fi, eli_dia_ini, eli_dia_fi, intervencio_codi))
            if eli_tipus_element == 'D':
                resultat_local_diagnostics.append((id_cip_sec, sector, pc_id, pc_codi, pc_data_inici, pc_data_fi, eli_dia_ini, eli_dia_fi, intervencio_codi))
                resultat_exadata_diagnostics.append((hash_d, sector, pc_id, pc_codi, pc_data_inici, pc_data_fi, eli_dia_ini, eli_dia_fi, intervencio_codi))


    print("      - Atic: sector {}".format(sector))

    if sector != '6951':
        sql_prstb016_017 = {"variables":
                                ("""
                                    SELECT
                                        DISTINCT vs_cod,
                                        vs_cod_ofic
                                    FROM
                                        import.cat_prstb004
                                """, """
                                    SELECT
                                        id_cip_sec,
                                        vu_cod_vs,
                                        vu_dat_act,
                                        CASE
                                            WHEN date_add(data_ext, INTERVAL - 1 MONTH) < vu_dat_act THEN 1
                                            ELSE 0
                                        END AS intervencio_ultim_mes                                        
                                    FROM
                                        variables2,
                                        nodrizas.dextraccio
                                    WHERE
                                        codi_sector = {}
                                """.format(sector)),
                            "activitats":
                                ("""
                                    SELECT
                                        DISTINCT ac_cod,
                                        ac_cod_ofic
                                    FROM
                                        import.cat_prstb002
                                ""","""
                                    SELECT
                                        id_cip_sec,
                                        au_cod_ac,
                                        au_dat_act,
                                        CASE
                                            WHEN date_add(data_ext, INTERVAL - 1 MONTH) < au_dat_act THEN 1
                                            ELSE 0
                                        END AS intervencio_ultim_mes
                                    FROM
                                        activitats2,
                                        nodrizas.dextraccio
                                    WHERE
                                        codi_sector = {} 
                                """.format(sector))}
    else:
        sql_prstb016_017 = {"variables":
                                ("""
                                    SELECT
                                        DISTINCT vs_cod,
                                        vs_cod_ofic
                                    FROM
                                        import.cat_prstb004
                                """, """
                                    SELECT
                                        id_cip_sec,
                                        vu_cod_vs,
                                        vu_dat_act,
                                        CASE
                                            WHEN date_add(data_ext, INTERVAL - 1 MONTH) < vu_dat_act THEN 1
                                            ELSE 0
                                        END AS intervencio_ultim_mes
                                    FROM
                                        import_jail.variables,
                                        nodrizas.dextraccio
                                    WHERE
                                        vu_dat_act BETWEEN date_add(date_add(data_ext, INTERVAL - 1 YEAR), INTERVAL + 1 DAY) AND data_ext
                                """),
                            "activitats":
                                ("""
                                    SELECT
                                        DISTINCT ac_cod,
                                        ac_cod_ofic
                                    FROM
                                        import.cat_prstb002
                                ""","""
                                    SELECT
                                        id_cip_sec,
                                        au_cod_ac,
                                        au_dat_act,
                                        CASE
                                            WHEN date_add(data_ext, INTERVAL - 1 MONTH) < au_dat_act THEN 1
                                            ELSE 0
                                        END AS intervencio_ultim_mes
                                    FROM
                                        import_jail.activitats,
                                        nodrizas.dextraccio
                                    WHERE
                                        au_dat_act BETWEEN date_add(date_add(data_ext, INTERVAL - 1 YEAR), INTERVAL + 1 DAY) AND data_ext 
                                """)}
    for taula, (sql_codis, sql_intervencions) in sql_prstb016_017.items():
        codis = {cod: cod_ofic for cod, cod_ofic in u.getAll(sql_codis, db_querys)}
        for id_cip_sec, intervencio_codi, data_intervencio, intervencio_ultim_mes in u.getAll(sql_intervencions, db_querys):
            if id_cip_sec in poblacio:
                if intervencio_codi in codis:
                    intervencio_codi = codis[intervencio_codi]
                    for pc_id in placures[id_cip_sec]:
                        if intervencio_codi in placures[id_cip_sec][pc_id]["intervencions_codis"]:

                            pc_data_inici = placures[id_cip_sec][pc_id]["data_inici"]
                            pc_data_fi = placures[id_cip_sec][pc_id]["data_fi"]

                            if (pc_data_fi is None and pc_data_inici <= data_intervencio) or (pc_data_inici <= data_intervencio <= pc_data_fi):

                                pc_codi = placures[id_cip_sec][pc_id]["pc_codi"]
                                hash_d = idcipsec_2_hash[id_cip_sec]

                                resultat_local_intervencions.append((id_cip_sec, sector, pc_id, pc_codi, pc_data_inici, pc_data_fi, data_intervencio, intervencio_codi))
                                resultat_exadata_intervencions.append((hash_d, sector, pc_id, pc_codi, pc_data_inici, pc_data_fi, data_intervencio, intervencio_codi))

                            # P.C. Alta
                            if pc_data_inici <= data_intervencio:
                                if pc_data_fi:
                                    if not data_intervencio <= placures[id_cip_sec][pc_id]["intervencions_codis"][intervencio_codi]["data_fi"]:
                                        continue

                                placures[id_cip_sec][pc_id]["intervencions_codis"][intervencio_codi]["intervencions"].add(data_intervencio)
                                if data_intervencio > placures[id_cip_sec][pc_id]["ultima_intervencio_data"]:
                                    placures[id_cip_sec][pc_id]["ultima_intervencio_data"] = data_intervencio
                                    if data_intervencio >= dext_menys1any:
                                        if pc_id in placures_alta_antics:
                                            placures[id_cip_sec][pc_id]["flag_pc_alta_anual"] = 1
                                        if pc_id in placures_alta_deixarfumar_antics:
                                             placures[id_cip_sec][pc_id]["flag_pc_alta_anual_deixarfumar"] = 1
                                        if pc_id in placures_seguimenticronicitat_antics:
                                            placures[id_cip_sec][pc_id]["flag_pc_seguiment_i_cronicitat"] = 1
                                        if pc_id in placures_actius_antics:
                                            placures[id_cip_sec][pc_id]["flag_pc_actiu"] = 1
                                    if data_intervencio >= dext_menys1mes:
                                        if pc_id in placures_alta_mensual_antics:
                                            placures[id_cip_sec][pc_id]["flag_pc_alta_mensual"] = 1
                                    if data_intervencio >= dext_menys3mesos:
                                        if pc_id in placures_alta_ultims_3_mesos_antics:
                                            placures[id_cip_sec][pc_id]["flag_pc_alta_3mesos"] = 1
                                    if data_intervencio >= dext_menys1setmana:
                                        if pc_id in placures_alta_setmanal_antics:
                                            placures[id_cip_sec][pc_id]["flag_pc_alta_setmanal"] = 1

    print("      - Variables: sector {}".format(sector))


    for id_cip_sec in placures:
        for pc_id in placures[id_cip_sec]:
            
            hash_d = idcipsec_2_hash[id_cip_sec]
            up_assignada = poblacio[id_cip_sec]

            pc_codi = placures[id_cip_sec][pc_id]["pc_codi"]
            pc_data_inici = placures[id_cip_sec][pc_id]["data_inici"]
            pc_data_fi = placures[id_cip_sec][pc_id]["data_fi"]

            pc_ultima_intervencio_data = placures[id_cip_sec][pc_id]["ultima_intervencio_data"] if placures[id_cip_sec][pc_id]["ultima_intervencio_data"] != date(1900,1,1) else None
            flag_aguda = 0 if pc_codi in pc_agudes["N"] else 1
            flag_pc_obert = placures[id_cip_sec][pc_id]["flag_pc_obert"]
            flag_pc_tancat_ultim_any = placures[id_cip_sec][pc_id]["flag_pc_tancat_ultim_any"]
            flag_pc_tancat_nafres_i_ferides_croniques = placures[id_cip_sec][pc_id]["flag_pc_tancat_nafres_i_ferides_croniques"]
            flag_pc_alta_anual = placures[id_cip_sec][pc_id]["flag_pc_alta_anual"]
            flag_pc_alta_anual_deixarfumar = placures[id_cip_sec][pc_id]["flag_pc_alta_anual_deixarfumar"]
            flag_pc_seguiment_i_cronicitat = placures[id_cip_sec][pc_id]["flag_pc_seguiment_i_cronicitat"]
            flag_pc_alta_mensual = placures[id_cip_sec][pc_id]["flag_pc_alta_mensual"]
            flag_pc_alta_3_mesos = placures[id_cip_sec][pc_id]["flag_pc_alta_3mesos"]
            flag_pc_alta_setmanal = placures[id_cip_sec][pc_id]["flag_pc_alta_setmanal"]
            flag_pc_actiu = placures[id_cip_sec][pc_id]["flag_pc_actiu"]

            resultat_local.append([id_cip_sec, up_assignada, sector, pc_id, pc_codi, pc_data_inici, pc_data_fi, 
                                   pc_ultima_intervencio_data, flag_aguda, flag_pc_obert, flag_pc_tancat_ultim_any, 
                                   flag_pc_tancat_nafres_i_ferides_croniques, flag_pc_alta_anual, flag_pc_alta_anual_deixarfumar, 
                                   flag_pc_seguiment_i_cronicitat, flag_pc_alta_mensual, flag_pc_alta_3_mesos, flag_pc_alta_setmanal, 
                                   flag_pc_actiu])

            resultat_exadata.append([hash_d, up_assignada, sector, pc_id, pc_codi, pc_data_inici, pc_data_fi, 
                                    pc_ultima_intervencio_data, flag_aguda, flag_pc_obert, flag_pc_tancat_ultim_any, 
                                    flag_pc_tancat_nafres_i_ferides_croniques, flag_pc_alta_anual, flag_pc_alta_anual_deixarfumar, 
                                    flag_pc_seguiment_i_cronicitat, flag_pc_alta_mensual, flag_pc_alta_3_mesos, flag_pc_alta_setmanal,
                                    flag_pc_actiu])                                 

    if sector != '6951':
        u.listToTable(resultat_local, tb, db)
        u.listToTable(resultat_exadata, tb, db_exadata)
        u.listToTable(resultat_local_intervencions_planificades, tb_intervencions_planificades, db)
        u.listToTable(resultat_exadata_intervencions_planificades, tb_intervencions_planificades, db_exadata)
        u.listToTable(resultat_local_diagnostics, tb_diagnostics, db)
        u.listToTable(resultat_exadata_diagnostics, tb_diagnostics, db_exadata)
        u.listToTable(resultat_local_intervencions, tb_intervencions, db)
        u.listToTable(resultat_exadata_intervencions, tb_intervencions, db_exadata)
    else:
        u.listToTable(resultat_local, tb_jail, db) 
        u.listToTable(resultat_exadata, tb_jail, db_exadata)
        u.listToTable(resultat_local_intervencions_planificades, tb_intervencions_planificades_jail, db) 
        u.listToTable(resultat_exadata_intervencions_planificades, tb_intervencions_planificades_jail, db_exadata)
        u.listToTable(resultat_local_diagnostics, tb_diagnostics_jail, db) 
        u.listToTable(resultat_exadata_diagnostics, tb_diagnostics_jail, db_exadata)
        u.listToTable(resultat_local_intervencions, tb_intervencions_jail, db) 
        u.listToTable(resultat_exadata_intervencions, tb_intervencions_jail, db_exadata)
    
    print("   - Acabat: sector {}".format(sector))


def get_placures_dades():
    """Obt� les dades dels plans de cures, mitjan�ant un multiprocess de la funci� "get_placures_sector(input)"."""

    vars_funcio = [sector for sector in u.sectors + ['6951']]       
    workers = 4

    u.multiprocess(get_placures_sector, vars_funcio, workers)

if __name__ == '__main__':
    
    get_dates();                       print("get_dates()")
    get_poblacio();                    print("get_poblacio()")
    get_pc_agudes_i_nom();             print("get_pc_agudes_i_nom()")
    get_resvech_ultims2mesos();        print("get_resvech_ultims2mesos()")
    crear_taules();                    print("crear_taules()")
    get_placures_dades();              print("get_placures_dades()")