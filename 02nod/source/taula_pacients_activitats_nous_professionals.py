#  coding: latin1

import sisapUtils as u
import collections as c

tb_pacients = "pacients_activitats_nous_professionals"
tb_grupals = "activitats_grupals_nous_professionals"
db_exportar = "nodrizas"

def get_centres():
    """."""

    global cat_centres

    sql = """
            SELECT
                scs_codi,
                ics_codi
            FROM
                cat_centres
          UNION
            SELECT
                scs_codi,
                ics_codi
            FROM
                jail_centres
            """
    cat_centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}

def get_poblacio():
    """."""

    global poblacio;            poblacio = c.defaultdict(dict)

    sql = """
            SELECT
                id_cip,
                id_cip_sec,
                up,
                edat,
                sexe
            FROM
                assignada_tot_with_jail
            """
    for id_cip, id_cip_sec, up, edat, sexe in u.getAll(sql, "nodrizas"):
        br = cat_centres[up]
        edat = u.ageConverter(edat,"g")
        sexe = u.sexConverter(sexe)
        poblacio[id_cip_sec] = (id_cip, br, edat, sexe)

def get_professionals_referents():
    """."""

    global conversor_ids_professionals_rols, conversor_dnis_professionals_rols, conversor_numcol_professionals_rols

    sql_1 = """
                SELECT
                    rol_usu,
                    'BEN' as rol
                FROM
                    cat_pritb799
                WHERE
                    ROL_ROL LIKE "%BEN%"
            UNION
                SELECT
                    ide_usuari,
                    'NUT' as rol
                FROM
                    cat_pritb992
                WHERE
                    ide_categ_prof_c = "11199"
            UNION
                SELECT
                    ide_usuari,
                    'FIS' as rol
                FROM
                    cat_pritb992
                WHERE
                    ide_categ_prof_c = "03005"
            UNION
                SELECT
                    ide_usuari,
                    'HIG' as rol
                FROM
                    cat_pritb992
                WHERE
                    ide_categ_prof_c = "10778"
            """
    conversor_ids_professionals_rols = {usuari: rol for usuari, rol in u.getAll(sql_1, "import")}

    sql_2 = """
                SELECT
                    LEFT(ide_dni, 8),
                    ide_usuari
                FROM
                    cat_pritb992
            """
    conversor_dnis_professionals_rols = {usu_dni: conversor_ids_professionals_rols[usu_codi] \
        for usu_dni, usu_codi in u.getAll(sql_2, "import") if usu_codi in conversor_ids_professionals_rols}


    sql_3 = """
                SELECT
                    RIGHT(ide_numcol, 8),
                    ide_usuari
                FROM
                    cat_pritb992
            """
    conversor_numcol_professionals_rols = {usu_numcol: conversor_ids_professionals_rols[usu_codi] \
        for usu_numcol, usu_codi in u.getAll(sql_3, "import") if usu_codi and usu_codi in conversor_ids_professionals_rols}

def get_visites_de_consulta():
    """."""

    counter = 0
    jobs = [table for table in u.getSubTables("visites")]
    jobs.append("visites") # Afegim la taula visites, que es la que consultem a import_jail
    
    u.multiprocess(sub_get_visites_de_consulta, jobs, 8)

def get_activitats_grupals_amb_participacio_referent():

    global activitats_grupals_amb_participacio_referent
    activitats_grupals_amb_participacio_referent = c.defaultdict(set) 

    sqls = (("""
                SELECT
                    codi_sector,
                    afgr_num_grup,
                    LEFT(afgr_dni, 8) AS dni
                FROM
                    cat_prstb293
                """, "import"),
            ("""
                SELECT
                    codi_sector,
                    pfgr_num_grup,
                    LEFT(pfgr_dni_proveidor, 8) AS dni
                FROM
                    grupal6
                """, "import"),
            ("""
                SELECT
                    codi_sector,
                    pfgr_num_grup,
                    RIGHT(pfgr_dni_proveidor, 8) AS numcol
                FROM
                    grupal6
                """, "import_jail"))
    for sql, db in sqls:
        for codi_sector, grup_id_sector, usu_id in u.getAll(sql, db):
            grup_id = (codi_sector, grup_id_sector)
            if usu_id in conversor_dnis_professionals_rols:
                tipus_prof = conversor_dnis_professionals_rols[usu_id]
                activitats_grupals_amb_participacio_referent[grup_id].add(tipus_prof)
            elif usu_id in conversor_numcol_professionals_rols:
                tipus_prof = conversor_numcol_professionals_rols[usu_id]
                activitats_grupals_amb_participacio_referent[grup_id].add(tipus_prof)

def get_grupals():
    """."""

    counter = 0
    jobs = u.sectors + ['6951']
    
    u.multiprocess(sub_get_grupals, jobs, 8)

def create_taules():
    """."""

    # Crear taula tb_pacients
    columnes_tb_pacients = """(id_cip int(11), id_cip_sec int(11), codi_sector varchar(4), codi_grup int, up_intervencio varchar(5), codi_intervencio bigint(20), data_intervencio date,
                               data_fi_grupal date, tipus_intervencio varchar(4), tipus_activitat varchar(2), tema_activitat varchar(2),
                               duracio_sessio decimal(2), nombre_sessions int(11), nombre_assistencies int(11), professional varchar(15),
                               pagr_situacio varchar(1), cod_ps varchar(50), procedencia varchar(10), rol_participacio varchar(30), rol_creador varchar(3))"""
    u.execute("DROP TABLE IF EXISTS {_tb_pacients}".format(_tb_pacients=tb_pacients), db_exportar)
    particions_db = ", ".join("PARTITION {_tb_pacients}_{_sector} VALUES IN ('{_sector}')".format(_tb_pacients=tb_pacients, _sector=sector) for sector in u.sectors)
    sql = """
            CREATE TABLE {_tb_pacients} 
            {_columnes_tb_pacients}
            PARTITION BY LIST COLUMNS (CODI_SECTOR) ({_particions_db})
            """
    u.execute(sql.format(_tb_pacients=tb_pacients, _particions_db=particions_db, _columnes_tb_pacients=columnes_tb_pacients), db_exportar)

    # Ojo! Taula tb_pacients
    # u.execute("ALTER TABLE {} MODIFY codi_sector TEXT CHARACTER SET latin1 COLLATE latin1_general_ci".format(tb_pacients), db_exportar)

    # Crear taula tb_grupals

    columnes_tb_grupals = """(codi_sector varchar(4), codi_grup int, up_intervencio varchar(5), data_intervencio date, n_assistents int, n_sessions int, hores int, data_ultima_sessio date,
                              tipus_intervencio varchar(4), tipus_activitat varchar(2), tema_activitat varchar(2), cod_ps varchar(50), rol_participacio varchar(30), rol_creador varchar(3))"""
    u.execute("DROP TABLE IF EXISTS {_tb_grupals}".format(_tb_grupals=tb_grupals), db_exportar)
    particions_db = ", ".join("PARTITION {_tb_grupals}_{_sector} VALUES IN ('{_sector}')".format(_tb_grupals=tb_grupals, _sector=sector) for sector in u.sectors)
    sql = """
            CREATE TABLE {_tb_grupals} 
            {_columnes_tb_grupals}
            PARTITION BY LIST COLUMNS (CODI_SECTOR) ({_particions_db})
            """
    u.execute(sql.format(_tb_grupals=tb_grupals, _particions_db=particions_db, _columnes_tb_grupals=columnes_tb_grupals), db_exportar)

def export_pob_rebec():

    sql = """
            SELECT
                DISTINCT id_cip_sec
            FROM
                {}
            WHERE
                tipus_intervencio IN ('9R', '9C', '9T', '9E', '9D', 'G', 'GC')
                AND (rol_participacio LIKE '%BEN%' OR rol_creador = 'BEN')
            """.format(tb_pacients)
    poblacio_counter = c.Counter()
    poblacio_rebec_exportar = list()

    u.createTable("pob_rebec", "(br varchar(5), edat varchar(7), sexe varchar(4), n int)", db_exportar, rm=True)

    for id_cip_sec, in u.getAll(sql, db_exportar):
        dades_pacient = poblacio.get(id_cip_sec)
        if dades_pacient:
            _, br, edat, sexe = dades_pacient
            poblacio_counter[(br,edat,sexe)] += 1
    for (br, edat, sexe), n in poblacio_counter.items():
        poblacio_rebec_exportar.append([br, edat, sexe, n])
    u.listToTable(poblacio_rebec_exportar, "pob_rebec", db_exportar)
    

    file_pobrebec = "POBREBEC"
    sql = "select 'POBREBEC', 'Aperiodo', br, 'NOCLI', edat, 'NOIMP', sexe, 'N', n FROM {}.{}".format(db_exportar, "pob_rebec")
    # u.exportKhalix(sql, file_pobrebec)

def sub_get_visites_de_consulta(table):

    sub_files_pacients = set()

    db = "import" if table != "visites" else "import_jail"

    sql = """
            SELECT
                id_cip,
                id_cip_sec,
                codi_sector,
                visi_up,
                visi_id AS codi_intervencio,
                visi_data_visita AS data_intervencio,
                visi_tipus_visita AS tipus_intervencio,
                NULL AS tipus_activitat,
                NULL AS tema_activitat,
                NULL AS duracio_sessio,
                NULL AS nombre_sessions,
                visi_assign_visita AS professional,
                CASE WHEN visi_modul_codi_modul LIKE 'REBE%' THEN 'BEN'
                     WHEN visi_modul_codi_modul LIKE 'RBE%' THEN 'BEN'
                     WHEN visi_servei_codi_servei LIKE 'REBE%' THEN 'BEN'
                     WHEN visi_servei_codi_servei LIKE 'RBE%' THEN 'BEN'
                     WHEN visi_servei_codi_servei LIKE 'NUT%' THEN 'NUT'
                     WHEN visi_servei_codi_servei LIKE 'FIS%' THEN 'FIS'
                     ELSE '' END rol_creador
            FROM
                {}
            WHERE
                visi_situacio_visita = 'R'
                AND (visi_servei_codi_servei LIKE 'NUT%'
                     OR visi_servei_codi_servei LIKE 'FIS%'
                     OR visi_servei_codi_servei LIKE 'REBE%' OR visi_servei_codi_servei LIKE 'RBE%'
                     OR visi_modul_codi_modul LIKE 'REBE%' OR visi_modul_codi_modul LIKE 'RBE%')
            """.format(table)
    for id_cip, id_cip_sec, codi_sector, visi_up, codi_intervencio, data_intervencio, tipus_intervencio, tipus_activitat, \
        tema_activitat, duracio_sessio, nombre_sessions, professional, rol_creador in u.getAll(sql, db):
        rol_participacio = rol_creador
        sub_files_pacients.add((id_cip, id_cip_sec, codi_sector, None, visi_up, codi_intervencio, data_intervencio, None, tipus_intervencio, 
                            tipus_activitat, tema_activitat, duracio_sessio, nombre_sessions, 0, professional, None, None, 'visites', rol_participacio, rol_creador))
    
    # Exportar a la taula
    u.listToTable(list(sub_files_pacients), tb_pacients, db_exportar)
    print("      sub_get_visites_de_consulta({})".format(table))
 

def sub_get_grupals(sector):
    """."""

    db = "import" if sector != "6951" else "import_jail"

    # GRUPAL 1 i 2: Assistencies a activitats
    
    assistencies_activitats, conv_num_pacient = c.Counter(), {}

    sql = """
                SELECT
                    id_cip_sec,
                    codi_sector,
                    cinp_num_int
                FROM
                    {}
          """.format("grupal1_s{}".format(sector) if sector != "6951" else "grupal1")
    for id_cip_sec, codi_sector, num_pacient in u.getAll(sql, db):
        conv_num_pacient[(codi_sector, num_pacient)] = id_cip_sec

    sql = """
            SELECT
                codi_sector,
                inp_num_grup,
                inp_num_int
            FROM
                {}
          """.format("grupal2_s{}".format(sector) if sector != "6951" else "grupal2")
    for codi_sector, grup_id_sector, num_pacient in u.getAll(sql, db):
        id_cip_sec = conv_num_pacient.get((codi_sector, num_pacient), None)
        if id_cip_sec:
            assistencies_activitats[(id_cip_sec, (codi_sector, grup_id_sector))] += 1

    # GRUPAL3: Pacients a activitats grupals

    pacients_activitats_grupals = c.defaultdict(set)

    sql = """
            SELECT
                id_cip_sec,
                codi_sector,
                pagr_num_grup,
                pagr_situacio
            FROM
                {}
            """.format("grupal3_s{}".format(sector) if sector != "6951" else "grupal3")
    for id_cip_sec, codi_sector, codi_intervencio, pagr_situacio in u.getAll(sql, db):
        pacients_activitats_grupals[(codi_sector, codi_intervencio)].add((id_cip_sec, pagr_situacio))

    # GRUPAL5: Activitats grupals amb seguiment

    ids_activitats_grupals_amb_seguiment = set()

    sql = """
            SELECT
                DISTINCT codi_sector,
                sgru_num_grup
            FROM
                {}
            """.format("grupal5_s{}".format(sector) if sector != "6951" else "grupal5")
    for codi_sector, codi_intervencio in u.getAll(sql, db):
        grup_id = (codi_sector, codi_intervencio)
        ids_activitats_grupals_amb_seguiment.add(grup_id)

    # GRUPAL 4: Activitats grupals

    sub_files_pacients, sub_files_grupals = set(), set()

    sql = """
            SELECT
                codi_sector,
                grup_num,
                grup_codi_up,
                grup_assistents,
                grup_hora_ini,
                grup_hora_fi,
                grup_num AS codi_intervencio,
                grup_data_ini AS data_intervencio,
                grup_data_fi AS data_fi_grupal,
                grup_tipus AS tipus_intervencio,
                grup_tipus_activitat AS tipus_activitat,
                grup_activitat AS tema_activitat,
                (grup_hora_fi - grup_hora_ini) / 3600 AS duracio_sessio,
                grup_num_sessions AS nombre_sessions,
                grup_usu_alta AS professional_creador,
                grup_usu_modif AS professional_modif,
                grup_cod_ps
            FROM
                {}
            WHERE
                grup_validada = 'S'
                AND (grup_tipus_activitat <> '' AND grup_activitat <> '')
            """.format("grupal4_s{}".format(sector) if sector != "6951" else "grupal4")
    for codi_sector, codi_grup, grup_up, n_assistents, grup_hora_ini, grup_hora_fi, codi_intervencio, data_intervencio, data_fi_grupal, tipus_intervencio, \
        tipus_activitat, tema_activitat, duracio_sessio, nombre_sessions, professional_creador, professional_modif, grup_cod_ps \
        in u.getAll(sql, db):
        grup_id = (codi_sector, codi_intervencio)

        if grup_up in cat_centres and (grup_id in ids_activitats_grupals_amb_seguiment or tipus_intervencio == 'C') and (grup_id in activitats_grupals_amb_participacio_referent or (professional_creador in conversor_ids_professionals_rols or professional_modif in conversor_ids_professionals_rols)):

            rol_creador, rol_participacio = None, None
            rol_creador = conversor_ids_professionals_rols.get(professional_creador, None)
            llista_participants = set(activitats_grupals_amb_participacio_referent[grup_id])
            if professional_modif in conversor_ids_professionals_rols:
                rol_modif = conversor_ids_professionals_rols[professional_modif]
                llista_participants.add(rol_modif)
            rol_participacio = "{}{}{}{}".format("BEN" if "BEN" in llista_participants and rol_creador != "BEN" else "", "NUT" if "NUT" in llista_participants and rol_creador != "NUT" else "", "FIS" if "FIS" in llista_participants and rol_creador != "FIS" else "", "HIG" if "HIG" in llista_participants and rol_creador != "HIG" else "")

            # Calculem el nombre de sessions com el m�xim entre nombre_sessions de grupal4 i el major nombre d'assistencies dels pacients a grupal2

            max_nombre_assistencies = 0
            if tipus_intervencio != 'C' and grup_id in pacients_activitats_grupals:
                for (id_cip_sec, pagr_situacio) in pacients_activitats_grupals[grup_id]:
                    if id_cip_sec in poblacio and pagr_situacio == 'S':
                        nombre_assistencies = assistencies_activitats.get((id_cip_sec, grup_id), 0)
                        max_nombre_assistencies = max(max_nombre_assistencies, nombre_assistencies)
            try:
                nombre_sessions = max(max_nombre_assistencies, nombre_sessions)
            except:
                nombre_sessions = 1 if not nombre_sessions else nombre_sessions

            # Inclu�m dades a la taula de pacients (quan l'activitat no �s comunit�ria)
            
            if tipus_intervencio != 'C' and grup_id in pacients_activitats_grupals:

                for id_cip_sec, pagr_situacio in pacients_activitats_grupals[grup_id]:
                    dades_pacient = poblacio.get(id_cip_sec)
                    if dades_pacient:
                        id_cip, _, _, _ = dades_pacient
                        nombre_assistencies = assistencies_activitats.get((id_cip_sec, grup_id), 0)
                        sub_files_pacients.add((id_cip, id_cip_sec, codi_sector, codi_grup, grup_up, codi_intervencio, data_intervencio, data_fi_grupal, tipus_intervencio,
                                                    tipus_activitat, tema_activitat, duracio_sessio, nombre_sessions, nombre_assistencies, professional_creador, pagr_situacio, grup_cod_ps, 'grupal4', rol_participacio, rol_creador))

            # Inclu�m dades a la taula de grupals (G, GC o C)

            hores = nombre_sessions * ((grup_hora_fi - grup_hora_ini) / 3600.0) if grup_hora_ini and grup_hora_fi else 0
            sub_files_grupals.add((codi_sector, codi_grup, grup_up, data_intervencio, n_assistents, nombre_sessions, hores, data_fi_grupal, tipus_intervencio, tipus_activitat, tema_activitat, grup_cod_ps, rol_participacio, rol_creador))

    # Exportar a les taules

    u.listToTable(list(sub_files_pacients), tb_pacients, db_exportar)
    u.listToTable(list(sub_files_grupals), tb_grupals, db_exportar)
    

if __name__ == '__main__':

    get_centres();                                         print("get_centres()")
    get_poblacio();                                        print("get_poblacio()")
    get_professionals_referents();                         print("get_professionals_referents()")
    create_taules();                                       print("create_taules()")   
    get_visites_de_consulta();                             print("get_visites_de_consulta()")
    get_activitats_grupals_amb_participacio_referent();    print("get_activitats_grupals_amb_participacio_referent()")
    get_grupals();                                         print("get_grupals()")
    export_pob_rebec();                                    print("export_pob_rebec()")