import sisapUtils as u
import collections as c


def tires():
    codis_tires = set()
    sql = """select PD_CODI from import.cat_cpftb025 where PD_TIPUS = 'T'"""
    for cod, in u.getAll(sql, 'import'):
        codis_tires.add(cod)
    
    jail_2 = c.defaultdict(set)
    sql = """select DTR_COD_PAC, dtr_pd_cod, dtr_cont_rec, dtr_per_rec
            from import.tires2withjail
            where dtr_per_rec is not null"""
    for cod_pac, cod_tira, n_tires, dies in u.getAll(sql, 'import'):
         if cod_tira in codis_tires:
            jail_2[cod_pac].add((int(n_tires), cod_tira, int(dies)))
            if cod_pac in (6625271161,6625325079):
                print(cod_pac, jail_2[cod_pac])

    tires_pac = {}
    sql = """select
		j1.id_cip_sec, fit_cod
	from
		import.tireswithjail j1, nodrizas.dextraccio
	where
		fit_data_alta <= data_ext
		and (fit_dat_baixa = 0
			or fit_dat_baixa > data_ext)"""
    for id_cip, fit_cod in u.getAll(sql, 'import'):
        if fit_cod in jail_2:
            for n_tires, cod_tira, per_rec in jail_2[fit_cod]:
                if per_rec == 0: tires = 0
                else:
                    tires = round((7 * n_tires)/float(per_rec), 1)
            if id_cip in tires_pac:
                n_tires_ant = tires_pac[id_cip]
                if n_tires_ant < tires:
                    tires_pac[id_cip] = tires
            else:
                tires_pac[id_cip] = tires
            if fit_cod in (6625271161,6625325079):
                print(fit_cod, tires, n_tires, cod_tira, per_rec)

    upload = [(id_cip, n_tires) for id_cip, n_tires in tires_pac.items()]
    sql = """drop tables if exists eqa_tires"""
    u.execute(sql, 'nodrizas')
    sql = """create table eqa_tires (
                id_cip_sec int
                ,tires double
                ,unique(id_cip_sec)
                )"""
    u.execute(sql, 'nodrizas')
    u.listToTable(upload, 'eqa_tires', 'nodrizas')


if __name__ == "__main__":
    tires()
