from sisapUtils import getAll, getOne, createTable, listToTable
from calendar import monthrange
from datetime import date


imp = 'import'
nod = 'nodrizas'
tab = 'ag_internet'


def get_moduls():
    centres = set([up for up in getAll('select scs_codi from cat_centres', nod)])
    serveis = {cod: hom for (cod, hom) in getAll('select distinct servei, codi_khalix from cat_ct_der_serveio', imp)}
    sql = "select concat(codi_sector, modu_id_modul), modu_codi_up, modu_codi_uab, modu_servei_codi_servei, modu_internet from cat_vistb027 \
           where modu_codi_uab <> '' and modu_descripcio not like 'RG MUTUAM%' and modu_model_agenda = 2 and modu_data_baixa = '47120101'"
    moduls = {id: (up, uba, serveis.get(servei, 'OALTEAP'), internet[1:]) for (id, up, uba, servei, internet) in getAll(sql, imp)}
    return moduls


def get_trams():
    trams = {id: tipus for (id, tipus) in getAll("select concat(codi_sector, bh_id_tram), bh_tipus_visita from cat_vistb203 where bh_ddb = 1 and bh_tipus_visita in ('9C', '9R')", imp)}
    return trams


def get_dates():
    year, month = getOne("select year(data_ext), month(data_ext) from dextraccio", nod)
    last_day = monthrange(year, month)[1]
    first_dat, last_dat = date(year, month, 1), date(year, month, last_day)
    return first_dat, last_dat


def get_data():
    moduls, trams, (first_dat, last_dat) = get_moduls(), get_trams(), get_dates()
    data = []
    sql = "select concat(codi_sector, ag_id_modul), concat(codi_sector, ag_id_tram) from forats where ag_data between '{}' and '{}' and ag_for in ('N', '')".format(first_dat, last_dat)
    for modul, tram in getAll(sql, imp):
        if modul in moduls and tram in trams:
            up, uba, servei, internet = moduls[modul]
            tipus = trams[tram]
            ofertada = 1 if internet == tipus else 0
            data.append([up, uba, servei, ofertada])
    upload_data(data)


def upload_data(data):
    createTable(tab, '(up varchar(5), uba varchar(5), servei varchar(10), ofertada int)', nod, rm=True)
    listToTable(data, tab, nod)


get_data()


'''
select ics_codi br, ics_desc eap, capes, internet, if(resultat is null, 0, resultat) resultat from (
select modu_codi_up up, sum(if(modu_model_agenda = 2, 1, 0)) / count(1) capes, sum(if(modu_internet = 'P9C', 1, 0)) / count(1) internet
from import.cat_vistb027 where modu_data_baixa = '47120101' and modu_codi_uab <> '' and modu_descripcio not like 'RG MUTUAM%'
group by modu_codi_up) a
left join (select up, avg(ofertada) resultat from nodrizas.ag_internet group by up) b on a.up = b.up
inner join nodrizas.cat_centres c on a.up = c.scs_codi
order by br
'''
