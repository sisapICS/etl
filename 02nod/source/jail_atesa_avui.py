# coding: iso-8859-1

import sisapUtils as u
import dateutil.relativedelta

"""
Codi per crear la taula 'jail_atesa_avui' actualitzada fins al dia d'avui.

Taules creades:
- 'jail_atesa_avui': Cont� moviments fins a la data d'extracci� actual.
- 'jail_atesa_avui_60': Subconjunt de moviments de 'jail_atesa_avui' amb una durada >= 60 dies.
"""

# Noms de les taules i configuraci� inicial
table_atesa = 'jail_atesa_avui'
table_atesa_60 = table_atesa + '_60'
db = 'nodrizas'
dies = 0

# Obtenim la data d'extracci� i calculem una data un any enrere
sql = "select data_ext, date_format(data_ext, '%Y%m%d') from dextraccio"
dext, dext_s = u.getOne(sql, db)
dext_menys1any = dext - dateutil.relativedelta.relativedelta(years=1) + dateutil.relativedelta.relativedelta(days=1)

# Variables per a emmagatzemar moviments processats
moviments = []
moviments_60 = []
actual = [None, None, None, None, None, None]

# Variables temporals per al processament
id_cip_sec_actual = None
id_cip_sec_descartat = None
id_cip_sec_a_afegir = None

# Consulta per obtenir els moviments ordenats per id_cip_sec i data
sql = """
        SELECT
            id_cip_sec,
            huab_data_ass,                                                                      -- Data d'assignaci� o inici del moviment
            IF(huab_data_final = 0, NULL, IF(huab_data_final > '{}', NULL, huab_data_final)),   -- Data final del moviment
            huab_up_codi,                                                                       -- Codi de la unitat productiva
            huab_uab_codi                                                                       -- Codi de la unitat b�sica assistencial
        FROM
            moviments
        WHERE
            huab_data_ass <= '{}'
        ORDER BY
            1 ASC,      -- Ordenem per id_cip_sec
            2 DESC,     -- Data d'inici descendent
            3 ASC       -- Data final ascendent
    """.format(dext_s, dext_s)

# Processament dels resultats de la consulta
for id_cip_sec, ini, fi, up, uba in u.getAll(sql, 'import_jail'):
    if id_cip_sec != id_cip_sec_descartat:  # Ignorem IDs descartats
        if not id_cip_sec_actual:  # Primer moviment d'aquest id_cip_sec

            # Inicialitzem els valors per al nou id_cip_sec
            sortit = 1 if fi else 0  # Determinem si hi ha data de sortida
            fi_computat = fi if fi else dext  # Si no hi ha sortida, usem la data d'extracci�
            # Guardem els valors actuals
            id_cip_sec_actual = id_cip_sec
            up_actual = up
            fi_computat_actual = fi_computat
            fi_original_actual = fi
            uba_actual = uba
            ini_actual = ini
            id_cip_sec_a_afegir = id_cip_sec
            sortit_actual = sortit

            # Si el moviment �s anterior a fa un any, el descartem
            if fi_computat_actual < dext_menys1any:
                id_cip_sec_descartat = id_cip_sec

        elif id_cip_sec == id_cip_sec_actual and up == up_actual and ini_actual == fi:  # Actualitzaci� del moviment
            ini_actual = ini

        elif id_cip_sec != id_cip_sec_actual:  # Nou id_cip_sec
            if id_cip_sec_a_afegir != id_cip_sec_descartat:
                # Calculem la durada del moviment
                durada = (fi_computat_actual - ini_actual).days
                this = [id_cip_sec_actual, up_actual, ini_actual, fi_original_actual, uba_actual, durada, sortit_actual]
                moviments.append(this)  # Afegim als moviments
                if durada >= 60:
                    moviments_60.append(this)  # Afegim a moviments_60 si durada >= 60
            id_cip_sec_a_afegir = None

            # Inicialitzem els valors per al nou id_cip_sec
            sortit = 1 if fi else 0
            fi_computat = fi if fi else dext
            # Guardem els valors actuals
            id_cip_sec_actual = id_cip_sec
            up_actual = up
            fi_computat_actual = fi_computat
            fi_original_actual = fi
            uba_actual = uba
            ini_actual = ini
            id_cip_sec_a_afegir = id_cip_sec
            sortit_actual = sortit
            if fi_computat_actual < dext_menys1any:
                id_cip_sec_descartat = id_cip_sec

        else:
            next

# Processament del darrer moviment
if id_cip_sec_a_afegir == id_cip_sec_actual != id_cip_sec_descartat:
    durada = (fi_computat_actual - ini_actual).days
    this = [id_cip_sec_actual, up_actual, ini_actual, fi_computat_actual, uba_actual, actual[5], sortit_actual]
    moviments.append(this)
    if durada >= 60:
        moviments_60.append(this)

# Creaci� de les taules i inserci� dels moviments
for table in (table_atesa, table_atesa_60):
    u.createTable(
        table,
        '(id_cip_sec int, up varchar(5), ingres date, sortida date, uba varchar(5), durada int, sortit int, primary key (id_cip_sec))',  # noqa
        db,
        rm=True
    )
    # Inserim els moviments processats a la taula corresponent
    u.listToTable(moviments if table == table_atesa else moviments_60, table, db)