from sisapUtils import *
db= 'import'
taules= ['variables','activitats','laboratori','visites']
anys= [1,2]
dat= {'variables':'vu_dat_act','activitats':'au_dat_act','laboratori':'cr_data_reg','visites':'visi_data_visita'}
dextraccio= "nodrizas.dextraccio"

for taula in taules:
    for any in anys:
        execute("drop table if exists %s%s" % (taula,any),db)
        execute("create table %s%s like %s" % (taula,any,taula),db)
        execute("alter table %s%s UNION=()" % (taula,any),db)
        particions= ""
        sql= "select table_name from information_schema.tables where table_schema='%s' and table_name like '%s\_%%'" % (db,taula)
        for particio in getAll(sql,db):
            query="select if(%s between cast(date_format(date_add(date_add(data_ext,interval - %d year),interval +1 day),'%%Y%%m01') as date) and last_day(data_ext),1,0) from %s,%s%s limit 1" % (dat[taula],any,particio[0],dextraccio," where cr_codi_lab<> 'RIMAP'" if taula== 'laboratori' else '')
            try:
                enter,= getOne(query,db)
            except:
                enter= 0
            if enter == 1:
                particions += "," + particio[0]
        query="alter table %s%s union=(%s)" % (taula,any,particions[1:])
        execute(query,db)
