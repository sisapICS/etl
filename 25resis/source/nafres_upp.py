import hashlib as h
import datetime as d
import collections as c

import sisaptools as u  # t
import sisapUtils as su  # u
import shared as s

TEST = False
RECOVER = False
SUFIX = "_resi"

file_name = "NAFRESUPP{}".format(SUFIX)
nod = "nodrizas"
imp = "import"
# d.date(2021, 1, 31), d.date(2021, 2, 28), d.date(2021, 3, 30)
DATABASE = "resis"
DEXTD = d.date(2021, 8, 31) if RECOVER else u.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]  # noqa

#  nafres = 833, 834
num_agrs = {"upp": ('91', '92'),
            "barthel": ('91', '92'),  # era "atdom"
            "nafres": ('833', '834'),
            "uisq": ('885', '886'),  # juntar con 'peu'
            "peu": ('887', '888'),
            "urinaria": ('454', '454'),  # en eqa_criteris solo esta las prevalentes: proximo calculo ('454', '892')  # noqa
            # "demencia": ('86', '86'),
            # "maltracte": ('545', '545'),
            }
# agr_atdom = 45


ind_opt = {
    "upp": {"prev": "RES029A", "inc": "RES030A"},
    "barthel": {"prev": "RES029A1", "inc": "RES030A1"},  # UPP en barthel
    "nafres": {"prev": "RES031A", "inc": "RES032A"},
    "uisq": {"prev": "RES033A", "inc": "RES034A"},  # juntar con 'peu'
    "peu": {"prev": "RES033A1", "inc": "RES034A1"},
    "urinaria": {"inc": "RES038A"},
    # "urinaria": {"prev": "RES022A", "inc": "RES038A"},
    # "demencia": {"prev": "RES020A"},
    # "maltracte": {"prev": "RES054A"},
    }


class Qcresis(object):
    """."""

    def __init__(self):
        """."""
        ts = init = d.datetime.now()
        print("comienzo: {}".format(init))
        self.get_conversor()
        su.printTime("get_conversor(): {}".format(len(self.conversor)))
        self.get_pob()  # get_pob_antic()
        su.printTime("""get_pob(): {}, {}
                    """.format(len(self.pob_act), len(self.pob_tot)))
        self.get_atdom()
        su.printTime("get_atdom(): {}".format(len(self.atdom_ass)))
        # self.get_hcovid_map()  # DEPRECATED
        self.get_idcipsec_map()
        su.printTime("""get_idcipsec_map(): {} hashes, {} u11, {} ids,
                    {} hash_sec, {} resi_sec
                    """.format(len(self.hashes), len(self.u11), len(self.ids),
                               len(self.hash_sec), len(self.resi_sec)))
        self.get_id_resi()
        su.printTime("""get_id_resi(): {}, {}
                    """.format(len(self.id_resi), len(self.id_resi_tot)))
        #
        # self.get_pob()
        # self.get_hcovid_map()
        # self.get_idcipsec_map()
        # self.get_id_resi()
        # self.get_pob_barthel()
        #
        self.get_variables()
        ts = d.datetime.now()
        print("acabo: {}".format(ts))
        print("total: {}".format(ts - init))

    def get_conversor(self):
        """."""
        self.conversor = {}
        sql = "select usua_cip, usua_cip_cod from pdptb101 \
               where usua_cip_cod like '{}%'"
        for cip_a, hash_a in s.get_data(sql, model="redics", redics="pdp"):
            self.conversor[cip_a] = hash_a

    def get_atdom(self):
        """ atdoms """
        self.atdom = set([id for id, in su.getAll("""
                                      SELECT
                                        id_cip_sec
                                      FROM
                                        eqa_problemes
                                      WHERE
                                        ps = 45 AND
                                        dde <= DATE '{DEXTD}'
                                      """.format(DEXTD=DEXTD),
                                      'nodrizas')
                          ])
        atdom_ubas = set()
        self.atdom_ass = c.defaultdict(dict)
        for id, _resi, up, uba, upinf, ubainf, _pcc, _maca in su.getAll(
                """
                select
                    id_cip_sec, up_residencia, up, uba,
                    upinf, ubainf, pcc, maca
                from assignada_tot
                """,
                "nodrizas"):
            if id in self.atdom:
                if uba:
                    atdom_ubas.add((up, uba, 'M'))
                if ubainf:
                    atdom_ubas.add((upinf, ubainf, 'I'))
                for row in [(up, uba, 'M'), (upinf, ubainf, 'I')]:
                    if row in atdom_ubas:
                        self.atdom_ass[id]['M'] = (up, uba)
                        self.atdom_ass[id]['I'] = (upinf, ubainf)
                        self.atdom_ass[id]['UP'] = up
                        self.atdom_ass[id]['dep'] = ''

    def get_pob(self):
        db = "exadata"
        # sch = "data"
        self.pob_act = {}
        self.pob_tot = {}
        resis = set()
        sql = """
            SELECT resi_cod
            FROM cat_sisap_map_residencies
            WHERE sisap_class = 1
        """
        for resi, in su.getAll(sql, 'import'):
            resis.add(resi)
        sql_act = """
            WITH
                max_data AS (
                    SELECT DATE '{DEXTD}' AS max_data FROM dual
                    -- SELECT max(data) AS max_data
                    -- FROM residencies_cens
                    -- WHERE perfil = 'Resident'
                    ),
                cip_resis AS (
                    SELECT
                        r.cip,
                        r.residencia,
                        max(r.data) AS max_resi_data
                    FROM
                        residencies_cens r
                        -- INNER JOIN residencies_cataleg rc
                        --    ON r.residencia = rc.codi
                        ,max_data m
                    WHERE
                        r.perfil = 'Resident'
                        -- AND rc.tipus = 1
                        AND r.data = m.max_data
                    GROUP BY
                        r.cip,
                        r.residencia
                    ),
                cip_last_data AS (
                    SELECT cip, max(max_resi_data) AS last_data
                    FROM cip_resis
                    GROUP BY cip
                    )
            SELECT
                cr.cip, max(cr.residencia) AS residencia
            FROM
                cip_resis cr
                INNER JOIN cip_last_data cld
                    ON cr.cip = cld.cip
                    AND cr.max_resi_data = cld.last_data
            GROUP BY
                cr.cip
        """.format(DEXTD=DEXTD)
        sql_tot = """
            WITH
                max_data AS (
                    SELECT DATE '{DEXTD}' AS max_data FROM dual
                    -- SELECT max(data) AS max_data
                    -- FROM residencies_cens
                    -- WHERE perfil = 'Resident'
                    ),
                cip_resis AS (
                    SELECT
                        r.cip,
                        r.residencia,
                        max(r.DATA) AS max_resi_data
                    FROM
                        residencies_cens r
                        -- INNER JOIN residencies_cataleg rc
                        --    ON r.residencia = rc.CODI
                        ,max_data m
                    WHERE
                        r.perfil = 'Resident'
                        -- AND rc.tipus = 1
                        AND r.data BETWEEN add_months(m.max_data, -12) + 1
                                       AND m.max_data
                    GROUP BY
                        r.cip,
                        r.residencia
                    HAVING
                        count(*) >= 30
                    ),
                cip_last_data AS (
                    SELECT cip, max(max_resi_data) AS last_data
                    FROM cip_resis
                    GROUP BY cip
                    )
            SELECT
                cr.cip, max(cr.residencia) AS residencia
            FROM
                cip_resis cr
                INNER JOIN cip_last_data cld
                    ON cr.cip = cld.cip
                    AND cr.max_resi_data = cld.last_data
            GROUP BY
                cr.cip
        """.format(DEXTD=DEXTD)
        # with u.Database(db, sch) as conn:
        #     for cip, resi in conn.get_all(sql_act):
        #         self.pob_act[cip] = resi
        # with u.Database(db, sch) as conn:
        #     for cip, resi in conn.get_all(sql_tot):
        #         self.pob_tot[cip] = resi
        for cip, resi in su.getAll(sql_act, db):
            if resi in resis:
                self.pob_act[cip] = resi
        for cip, resi in su.getAll(sql_tot, db):
            if resi in resis:
                self.pob_tot[cip] = resi

    # def get_pob_antic(self):
    #     """
    #     Gets residents from res_master.
    #     - {pob_act}  # hcovid: (attributes)  # actual residents
    #     - {pob_tot}  # hcovid: (attributes)  # residents since 2020-03-01
    #     """
    #     db = "redics"
    #     sch = "data"
    #     tb = "sisap_covid_res_master"
    #     cols = ["hash", "actual", "residencia", "entrada", "motiu","sortida",
    #             "barthel_dat", "barthel_val", "pfeifer_dat", "pfeiffer_val",
    #             "situacio", "exitus_data", "vis_ultima",
    #             ]
    #     cols = ", ".join(cols)
    #     sql = "select {} from {}".format(cols, tb)
    #     self.pob_tot = {}
    #     self.pob_act = {}
    #     with u.Database(db, sch) as conn:
    #         for row in conn.get_all(sql):
    #             hcovid, actual = row[0], row[1]
    #             self.pob_tot[hcovid] = row[2:]
    #             if actual == 1:
    #                 self.pob_act[hcovid] = row[2:]

    def get_idcipsec_map(self):
        """
        Gets id_cip_sec and hash_d from hash_a.
        - {u11}  # hash_a: {id_cip_sec, hash_d}
        """
        self.resi_sec = {}
        with u.Database('p2262', 'nodrizas') as conn:
            for resi, sector in conn.get_all("""
                    WITH
                        resi_up as (SELECT
                                        distinct resi_cod as resi, eap_up as up
                                    FROM
                                        import.cat_sisap_map_residencies
                                    WHERE
                                        sisap_class =1),
                        up_sec as  (SELECT
                                        scs_codi as up, sector
                                    FROM
                                    nodrizas.cat_centres)
                    SELECT
                        r.resi, u.sector
                    FROM
                        resi_up r INNER JOIN
                        up_sec u ON r.up = u.up
                    """):
                self.resi_sec[resi] = sector
        db = "p2262"
        sch = "import"
        tb = "u11_all"
        cols = ["id_cip_sec", "hash_a", "hash_d", "codi_sector"]
        cols = ", ".join(cols)
        sql = "select {} from {}".format(cols, tb)
        self.hash_sec = {}
        self.hashes = set()
        self.u11 = {}
        self.ids = {}
        for cip, resi in self.pob_tot.items():
            sector = self.resi_sec.get(resi)
            hash_a = self.conversor.get(cip)
            if hash_a is not None:
                self.hash_sec[hash_a] = sector
                self.hashes.add(hash_a)
        for cip, resi in self.pob_act.items():
            sector = self.resi_sec.get(resi)
            hash_a = self.conversor.get(cip)
            if hash_a is not None:
                self.hash_sec[hash_a] = sector
                self.hashes.add(hash_a)
        with u.Database(db, sch) as conn:
            for id_cip_sec, hash_a, hash_d, sector in conn.get_all(sql):
                # if hash_a in self.hashes or id_cip_sec in self.atdom_ass:
                #     self.ids[id_cip_sec] = (hash_d, sector)
                #     self.u11[hash_a] = {'id_cip_sec': id_cip_sec,
                #                         'hash_d': hash_d}
                sec = self.hash_sec.get(hash_a)
                if sec == sector:
                    self.ids[id_cip_sec] = (hash_d, sector)
                    self.u11[hash_a] = {'id_cip_sec': id_cip_sec,
                                        'hash_d': hash_d,
                                        'sector': sector,
                                        }

    # def get_idcipsec_map_antic(self):
    #     """
    #     Gets id_cip_sec and hash_d from hash_a.
    #     - {u11}  # hash_a: {id_cip_sec, hash_d}
    #     """
    #     db = "p2262"
    #     sch = "import"
    #     tb = "u11_all"  # cambio u11 por u11_all
    #     cols = ["id_cip_sec", "hash_a", "hash_d"]  # cambio hash_d por hash_a
    #     cols = ", ".join(cols)
    #     sql = "select {} from {}".format(cols, tb)
    #     self.u11 = {}
    #     self.ids = {}
    #     with u.Database(db, sch) as conn:
    #         for id_cip_sec, hash_a, hash_d in conn.get_all(sql):
    #             if hash_a in self.hashes:
    #                 self.ids[id_cip_sec] = True
    #                 self.u11[hash_a] = {'id_cip_sec': id_cip_sec,
    #                                     'hash_d': hash_d}

    def get_hash(self, cip):
        """Returns hcovid from cip."""
        return h.sha1(cip).hexdigest().upper()

    # def get_hcovid_map(self):
    #     """
    #     Gets hash_d from hcovid.
    #     - {hcovid_hash_map} # hcovid: hash_a
    #     - set(hashes)  # set([hash_a, ...])
    #     !!! hay unos 67/62k que son hash_a que no ligan con u11.hash_d
    #     """

    #     LIMIT = 1000000
    #     limit = " where rownum <{}".format(LIMIT)
    #     db = 'redics'
    #     sch = 'pdp'
    #     tb = "pdptb101{}".format(limit if TEST else '')
    #     # conn = u.Database(db, sch)
    #     sql = 'select usua_cip, usua_cip_cod from {} '.format(tb)
    #     self.hcovid_hash_map = {}
    #     self.hashes = set()
    #     with u.Database(db, sch) as conn:
    #         for cip, hash_a in conn.get_all(sql):
    #             hcovid = self.get_hash(cip)
    #             if hcovid in self.pob_tot:
    #                 self.hashes.add(hash_a)
    #                 self.hcovid_hash_map[hcovid] = hash_a

    def get_id_resi(self):
        # DEPENDENCIA II_III
        sql = """
            SELECT
                cip,
                max(grau) AS grau
            FROM
                sisap_covid_tmp_dep
            WHERE
                cip IS NOT NULL AND
                grau IN ('II','III')
            GROUP BY
                cip
            """
        dependencia = {cip: grau for (cip, grau) in su.getAll(sql, 'redics')}
        # for cip, grau in cip_dep.items():
        #     hash_a = self.conversor.get(cip)
        #     try:
        #         id = self.u11[hash_a]['id_cip_sec']
        #         dependencia[id] = grau
        #     except KeyError:
        #         continue
        self.id_resi = c.defaultdict(dict)
        self.id_resi_tot = c.defaultdict(dict)
        for cip, resi in self.pob_act.items():
            hash_a = self.conversor.get(cip)
            dep = dependencia.get(cip)
            try:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi[id]['UP'] = resi
                self.id_resi[id]['dep'] = dep
            except KeyError:
                continue
        self.id_resi.default_factory = None
        for cip, resi in self.pob_tot.items():
            hash_a = self.conversor.get(cip)
            dep = dependencia.get(cip)
            try:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi_tot[id]['UP'] = resi
                self.id_resi_tot[id]['dep'] = dep
            except KeyError:
                continue
        self.id_resi_tot.default_factory = None

    # def get_id_resi_old(self):
    #     self.id_resi = {}
    #     for hcovid, v in self.pob_act.items():
    #         resi = v[0]
    #         hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
    #         try:
    #             id = self.u11[hash_a]['id_cip_sec']
    #             self.id_resi[id] = resi
    #         except KeyError:
    #             continue
    #     self.id_resi_tot = c.defaultdict(set)
    #     for hcovid, v in self.pob_tot.items():
    #         resi = v[0]
    #         hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
    #         if hash_a in self.u11:
    #             id = self.u11[hash_a]['id_cip_sec']
    #             self.id_resi_tot[id].add(resi)
    #     self.id_resi_tot.default_factory = None

    def get_variables(self):
        dades = c.defaultdict(lambda: c.defaultdict(dict))
        sql = """
            select
                codi,
                lower(substr(agrupador, 3, length(agrupador)))
            from DBSCAT where
            agrupador in ('V_BARTHEL', 'V_PFEIFER', 'V_BRADEN', 'V_PESO')
            """
        codis = {cod: var for (cod, var)
                 in u.Database("redics", "data").get_all(sql)}
        codis['N'] = 'caigudes'  # 'Y00002' en 6 meses (bajo reg), 'TUGT' riesgo  # noqa
        sqls = []
        sqls.append(
            """
            SELECT
                id_cip_sec, vu_cod_vs, vu_dat_act, vu_val
            FROM
                variables2
            WHERE
                vu_cod_vs in {keys} AND
                vu_dat_act BETWEEN adddate(DATE '{DEXTD}', INTERVAL -1 year)
                               AND DATE '{DEXTD}'
            """.format(keys=tuple(codis.keys()), DEXTD=DEXTD)
            )
        for sql in sqls:
            for cip, var, dat, val in su.getAll(sql, 'import'):
                var = codis[var] if var in codis else var
                if val > 0:
                    dades[var][cip][dat] = val
        self.last_vars = c.defaultdict(lambda: c.defaultdict(dict))
        for var, cips in dades.items():
            for cip, dats in cips.items():
                dat = max(dats)
                val = dats[dat]
                self.last_vars[cip][var] = val
        self.varss_last = c.defaultdict(lambda: c.defaultdict(dict))
        for cip, varss in self.last_vars.items():
            for var, val in varss.items():
                if var == 'barthel':
                    self.varss_last[var][cip] = val
        self.grups = c.defaultdict(set)
        for id, var_val in self.last_vars.items():
            for var, val in var_val.items():  # "barthel"
                self.grups[var].add(id)
                if var == "barthel":
                    if val <= 40:
                        self.grups['barthel_lt40'].add(id)

    def get_pob_barthel(self):
        self.id_resi_barthel = {}
        for hcovid, v in self.pob_act.items():
            resi = v[0]
            barthel = v[5]
            if barthel >= 40:
                hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
                try:
                    id = self.u11[hash_a]['id_cip_sec']
                    self.id_resi_barthel[id] = resi
                except KeyError:
                    pass
        self.id_resi_barthel_tot = c.defaultdict(set)
        for hcovid, v in self.pob_tot.items():
            resi = v[0]
            barthel = v[5]
            if barthel >= 40:
                hash_a = self.hcovid_hash_map[hcovid] if hcovid in self.hcovid_hash_map else None  # noqa
                if hash_a in self.u11:
                    id = self.u11[hash_a]['id_cip_sec']
                    self.id_resi_barthel_tot[id].add(resi)
        self.id_resi_barthel_tot.default_factory = None


def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql = "select data_ext from {}.dextraccio;".format(nod)
    return su.getOne(sql, nod)[0]


def get_pob_agr(agr):
    """
    Selecciona los pacientes con un determinador agrupador (agr).
    Devuelve un set de ids.
    """
    sql = "select id_cip_sec from {}.eqa_problemes where ps = {}".format(nod, agr)  # noqa
    return {id for (id, ) in su.getAll(sql, nod)}


def get_id_agr_prev_inc(agrs, current_date):
    """
    Selecciona los pacientes que tengan alguno de los agrupadores en agrs.
    Se guardan dos valores, la prevalencia, que son los pacientes que presentan
    el agrupador.
    y la incidencia, que son aquellos pacientes a los que se les ha
    diagnosticado el agr en los ultimos 12 meses.
    Se devuelve un diccionario con el formato:
        {agr: {"prev": set(ids), "inc": set(ids)}
    """
    id_agrs = c.defaultdict(lambda: c.defaultdict(set))
    sql = """
        SELECT id_cip_sec, ps, dde
        FROM {}.eqa_problemes where ps in ({})
        """.format(nod, ", ".join(agrs))
    print(sql)

    for id, agr, dde in su.getAll(sql, nod):
        agr = str(agr)
        id_agrs[agr]["prev"].add(id)
        if su.monthsBetween(dde, current_date) < 12:
            id_agrs[agr]["inc"].add(id)
    return id_agrs


def get_indicador(codi, num, dim6set, pob_ids=None):
    """
    Construye el indicador a nivel de up.
    Si pob_ids es dada (un diccionario de ids: up),
    el denominador se construye con los ids que estan dentro de esta poblacion.
    En caso contrario, es el total de personas en la up.
    El numerador se obtiene a partir de num, otro set de ids.
       Devuelve una lista de filas (tuples).
    """
    indicador = c.defaultdict(c.Counter)
    if pob_ids is None:
        sql = "select id_cip_sec, up from {}.assignada_tot".format(nod)
        pob_ids = {id: up for id, up in su.getAll(sql, nod)}
    pacients = []
    for id, up in pob_ids.items():
        compleix = 0
        indicador[up]["DEN"] += 1
        if id in num:
            compleix = 1
            indicador[up]["NUM"] += 1
        if dim6set == 'POBACTUAL':
            pacients.append((id, codi, compleix))

    upload = []
    for up in indicador:
        for part, val in indicador[up].items():
            upload.append((codi, up, part, dim6set, val))
    su.listToTable(pacients, 'pacients_resis', 'resis')
    return [(up, codi, indicador[up]["NUM"], indicador[up]["DEN"]) for up in indicador], upload  # noqa


def export_table(table, columns, db, rows):
    su.createTable(table, columns, db, rm=True)
    su.listToTable(rows, table, db)


def push_to_khalix(db, taula, taula_centres, file_name, khalix=False):
    """."""
    cataleg = ".".join(("import", "cat_sisap_map_residencies b"))

    cat_cond = "WHERE a.up = b.resi_cod and b.sisap_class = 1"
    cat_cond = " ".join((cataleg, cat_cond))
    cat_cond = """exists (select 1 from {cat_cond})
    """.format(cat_cond=cat_cond)

    query_template_string = """
        SELECT
            indicador, concat('A', 'periodo'),
            concat('R', replace(up,'-','_')) as resi, {strg},
            'NOCAT', 'NOIMP', 'POBACTUAL', 'N', {var}
        FROM
            {db}.{taula} a
        WHERE {cat_cond}
            -- INNER JOIN
            -- nodrizas.{taula_centres} b
            --    on a.up = scs_codi
            and a.{var} != 0
    """
    query_string1 = query_template_string.format(strg="'NUM'",
                                                 var="numerador",
                                                 db=db,
                                                 taula=taula,
                                                 cat_cond=cat_cond,
                                                 taula_centres=taula_centres)

    query_string2 = query_template_string.format(strg="'DEN'",
                                                 var="denominador",
                                                 db=db,
                                                 taula=taula,
                                                 cat_cond=cat_cond,
                                                 taula_centres=taula_centres)

    query_string = query_string1 + " union " + query_string2

    print(query_string)
    # if khalix:
    #     su.exportKhalix(query_string, file_name)

def filter_export(file_name, DADES, dim="4DIM", DATABASE=DATABASE, remove=False):  # noqa
    tb = "exp_khalix_{}".format(file_name)

    KHALIX_6DIM = ('ind varchar(15)',
                   'period varchar(5)',
                   'entity varchar(13)',
                   'analysys varchar(10)',
                   'detail varchar(10)',
                   'dim6set varchar(10)',
                   'valor int',
                   )
    KHALIX_5DIM = tuple(item for item in KHALIX_6DIM if 'period' not in item)
    KHALIX_4DIM = tuple(item for item in KHALIX_5DIM if 'detail' not in item)

    K_OPTS = {'6DIM': {'kdim': KHALIX_6DIM,
                       'period': 'period',
                       'detail': 'detail',
                       },
              '5DIM': {'kdim': KHALIX_5DIM,
                       'period': "'Aperiodo'",
                       'detail': 'detail',
                       },
              '4DIM': {'kdim': KHALIX_4DIM,
                       'period': "'Aperiodo'",
                       'detail': "'NOIMP'",
                       },
              }

    KDIM = K_OPTS[dim]['kdim']
    PERIOD, DETAIL = K_OPTS[dim]['period'], K_OPTS[dim]['detail']

    with u.Database('p2262', DATABASE) as conn:
        conn.create_table(tb, KDIM, remove=remove)
        conn.list_to_table(DADES, tb)

    # if file_name.endswith("_atdom"):
    #     cataleg = ".".join(("nodrizas", "cat_centres b"))
    #     cond = "WHERE a.entity = b.scs_codi"

    #     cat_cond = " ".join((cataleg, cond))
    #     # cat_cond = "exists (select 1 from {cat_cond})".format(cat_cond=cat_cond)  # noqa

    #     query_string = """
    #         SELECT
    #             ind, {period}, b.ics_codi as resi,
    #             analysys, 'NOCAT', {detail},
    #             dim6set, 'N', valor
    #         FROM
    #             {db}.{taula} a,
    #             {cat_cond} and a.valor <> 0
    #         """.format(db=DATABASE, taula=tb, cat_cond=cat_cond,
    #                    detail=DETAIL, period=PERIOD)
    # else:
    #     cataleg = ".".join(("import", "cat_sisap_map_residencies b"))
    #     cond = "WHERE a.entity = b.resi_cod and b.sisap_class = 1"

    #     cat_cond = " ".join((cataleg, cond))
    #     cat_cond = "exists (select 1 from {cat_cond})".format(cat_cond=cat_cond)  # noqa

    #     query_string = """
    #         SELECT
    #             ind, {period}, concat('R', replace(entity,'-','_')) as resi,
    #             analysys, 'NOCAT', {detail},
    #             dim6set, 'N', valor
    #         FROM
    #             {db}.{taula} a
    #         WHERE
    #             {cat_cond} and a.valor <> 0
    #         """.format(db=DATABASE, taula=tb, cat_cond=cat_cond,
    #                    detail=DETAIL, period=PERIOD)

    # su.exportKhalix(query_string, file_name)


if __name__ == '__main__':
    Resi = Qcresis()
    su.printTime('inici')
    current_date = DEXTD if RECOVER else get_date_dextraccio()
    su.printTime(current_date)
    # Se crea la tabla
    table_name = "exp_khalix_nafres_upp{}".format(SUFIX)
    table_columns = "(up varchar(13), indicador varchar(15), numerador int, denominador int)"  # noqa
    su.createTable(table_name, table_columns, DATABASE, rm=True)

    # Se genera la lista de agrs para el numerador
    # y se pasa a la funcion que nos dara los ids para cada uno de ellos
    # (prev e inc)
    agrs_list = list(el for tuplee in set(num_agrs.values()) for el in tuplee)
    ids_agr = get_id_agr_prev_inc(agrs_list, current_date)
    su.printTime("ids_agr")

    # Se obtiene la poblacion atdom
    # pob_atdom = get_pob_agr(agr_atdom)
    # su.printTime("atdom_pob")

    pobs = {
        'nafres_resi': Resi.id_resi,
        'nafres_resi_total': Resi.id_resi_tot,
        }

    for file_name, pob_d in pobs.items():
        dim6set = 'POBTOTAL' if file_name == 'nafres_resi_total' else 'POBACTUAL'  # noqa
        pob = {k: pob_d[k]['UP'] for k in pob_d}
        # pob = c.defaultdict()
        # for k, v in Resi.id_resi:
        #     pob[k] = Resi.id_resi[k]['UP']

        pob_barthel_lt40 = {
            k: pob_d[k]['UP'] for k in pob_d
            if k in Resi.grups['barthel_lt40']
            }
        # pob_barthel_lt40 = c.defaultdict()
        # for k in Resi.id_resi:
        #     if k in Resi.grups['barthel_lt40']:
        #         pob_barthel_lt40[k] = Resi.id_resi[k]['UP']

        upload = []
        for key in ind_opt:
            print(key)
            # Para cada key (upp, atdom, nafres):

            # Diccionario de opciones para determinar si el denominador es:
            #   pob_atdom o el total de gente de la up (opcion None)
            trad_dict = {"upp": pob,  # Resi.id_resi
                         # "barthel": Resi.id_resi_barthel,
                         "barthel": pob_barthel_lt40,  # Resi.id_resi_barthel
                         "nafres": pob,  # Resi.id_resi
                         "uisq": pob,    # Resi.id_resi # juntar con 'peu'
                         "peu": pob,  # Resi.id_resi
                         "urinaria": pob,  # Resi.id_resi
                         "demencia": pob,  # Resi.id_resi
                         "maltracte": pob,  # Resi.id_resi
                         }
            current_pob = trad_dict[key]

            # El agrupador que va a determinar el numerador,
            # para cada key, segun el tipo
            num_by_type = {"inc": num_agrs[key], "prev": (num_agrs[key][0],)}

            for type_num, codi in ind_opt[key].iteritems():
                # Una vez obtenido el tipo, selecciona los agrupadores (tuple)
                # que van a dar el numerador
                agrs_for_num = num_by_type[type_num]

                # Obtiene los ids del numerador
                # haciendo una union con los sets de ids
                # correspondientes al agr y al tipo
                num = set.union(*[ids_agr[num_agr][type_num]
                                for num_agr in agrs_for_num])

                # Se obtienen las rows por up para cada tipo de indicador
                # (prev o inc) dentro de una key (upp, upp_barthel, nafres)
                # dim6set = 'POBACTUAL'
                rows, rows2 = get_indicador(codi, num, dim6set, current_pob)

                su.listToTable(rows, table_name, DATABASE)
                su.printTime("Insert into the table {}".format(codi))

                upload.extend(rows2)

        # Export via tabla (probando exportar desde Lista)
        # push_to_khalix(db=DATABASE, taula=table_name,
        #                taula_centres="cat_centres",
        #                file_name=file_name, khalix=True)
        # file_name = 'nafres_resi'
        filter_export(file_name,
                      upload,
                      dim="4DIM",
                      DATABASE=DATABASE,
                      remove=True)
