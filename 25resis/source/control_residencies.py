# coding: utf8

"""
Afegim indicadors a la taula de control. En aquest cas els indicadors de la carpeta resis
"""

import urllib as w
import os
import collections as c
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u
import sisaptools as t

db = ('shiny', 'ladybug')
tb = 'mst_control_eqa'
tb_cat = 'cat_control_indicadors'
db_origen = "resis"
csv = 'cataleg_residencies.csv'
"""Taules origen"""
# POBACTUAL A DIM6SET
# exp_khalix_resis - indicador, up, analisi, val (agafar POBACTUAL)
# exp_khalix_cont - ind, entity, analysys, valor (agafar POBACTUAL)
# exp_khalix_bo - ind, entity, analysys, valor (agafar POBACTUAL)
# nous_indicadors_resis - indi, up_resi, analisi, val
# exp_khalix_nafres_resi - ind, entity, analysys, valor (agafar POBACTUAL)
# exp_khalix_gida_resi - ind, ent, analisi, valor
# exp_khalix_longit - ind, period, entity, analysys, valor (agafar POBACTUAL)

# cols = "(periode int, sector varchar(4), ambit varchar(50), indicador varchar(20), resolts int, n int)"
# u.createTable('prova_ladybug', cols, 'resis', rm=True)

tables = {
    'exp_khalix_resis': 'indicador, up, analisi, val',
    'exp_khalix_cont': 'ind, entity, analysys, valor',
    'exp_khalix_bo': 'ind, entity, analysys, val',
    'nous_indicadors_resis': 'indi, up_resi, analisi, val',
    'exp_khalix_nafres_resi': 'ind, entity, analysys, valor',
    'exp_khalix_gida_resi': 'ind, ent, analisi, valor',
    'exp_khalix_longit': 'ind, entity, analysys, valor'
}

class LadyBug_resis(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """."""
        self.indicadors, self.resultats, self.cataleg = {}, Counter(), {}
        self.periode, = u.getOne("select date_format(data_ext,'%Y%m') from dextraccio", 'nodrizas')
        self.get_territoris()
        self.get_ind()
        self.upload_cat()
        self.export_data()
        
    def upload_cat(self):
        cataleg = {}
        path = './dades_noesb/'
        print(path+csv)
        for (indicador, desc) in u.readCSV(path + csv, sep=';'):
            cataleg[indicador] = desc
        sql = "delete from {tb} where indicador = '{ind}'"
        for ind in cataleg:
            t.Database(*db).execute(sql.format(tb=tb_cat, ind=ind))
        upload = []
        pare = 'QC Residencies'
        for ind, desc in cataleg.items():
            upload.append((ind, desc, pare))
        t.Database(*db).list_to_table(upload, tb_cat)

    def get_ind(self):
        """."""
          
        sql = "select {camps} from {table} {cond}"
        for tb in tables:
            if tb in ('exp_khalix_gida_resi'):
                cond = ''
            else:
                cond = "WHERE dim6set = 'POBACTUAL'"
            print(sql.format(camps=tables[tb], table=tb, cond=cond))
            try:
                for indicador, up, analisi, n in u.getAll(sql.format(camps=tables[tb], table=tb, cond=cond), db_origen):
                    if up in self.centresResis:
                        up = self.centresResis[up]['up']
                    if indicador[:7] == 'GESTINF':
                        indicador = indicador + 'RES'
                    self.indicadors[indicador] = True
                    ambit = self.centresSCS[up]['ambit']
                    sector = self.centresSCS[up]['sector']
                    if analisi == 'DEN':
                        self.resultats[self.periode, sector, ambit, indicador, 'den'] += n
                    if analisi == 'NUM':
                        self.resultats[self.periode, sector, ambit, indicador, 'num'] += n
            except Exception as e:
                continue
    
    def get_territoris(self):
        """Obtenim ambits i sector"""
        self.centres, self.centresSCS, self.centresResis = {}, {}, {}
        # sql = 'select ics_codi, scs_codi, amb_desc, sector from cat_centres'
        # for br, up, amb, sector in u.getAll(sql, 'nodrizas'):
        #     self.centres[br] = {'sector': sector, 'ambit': amb}
        #     self.centresSCS[up] = {'sector': sector, 'ambit': amb}
        sql = """
            SELECT concat('R', replace(resi_cod, '-', '_')) as entity, resi_cod, eap_up
            FROM cat_sisap_map_residencies
            WHERE sisap_class = 1
        """
        resis = c.defaultdict(set)
        for br, resi, up in u.getAll(sql, 'import'):
            resis[up].add((resi, br))
        sql = """
            SELECT scs_codi, amb_desc, sector
            FROM cat_centres
        """
        for up, amb, sector in u.getAll(sql, 'nodrizas'):
            if up in resis:
                for (resi, br) in resis[up]:
                    self.centres[br] = {'sector': sector, 'ambit': amb}
                    self.centresSCS[up] = {'sector': sector, 'ambit': amb}
                    self.centresResis[resi] = {'sector': sector, 'ambit': amb, 'up': up}
    
    def export_data(self):
        """."""
        upload = []
        for (period, sector, ambit, ind, tipus), n in self.resultats.items():
            if tipus == 'den':
                resolts = self.resultats[period, sector, ambit, ind, 'num']
                upload.append([int(period), sector, ambit, ind, resolts, n])
                # print(upload[-1])
        
        indi = []
        for codi, n in self.indicadors.items():
            indi.append(codi)
            
        indicadors = tuple(indi)
        sql = "delete a.* from {0} a where periode = {1} and indicador in {2}".format(tb, self.periode, indicadors)
        # u.execute(sql, 'resis')
        # u.listToTable(upload, tb, 'resis')
        t.Database(*db).execute(sql)
        t.Database(*db).list_to_table(upload, tb)
        
  
if __name__ == '__main__':
    LadyBug_resis()
    