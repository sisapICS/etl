import sisapUtils as u


sql = """
    SELECT DISTINCT indicador, 'Aperiodo', concat('R', replace(a.up,'-','_')) as resi,
    analisi, 'NOCAT', 'NOIMP', dim6set, 'N', val
    FROM resis.exp_khalix_resis a
    WHERE exists (
        SELECT 1
        FROM import.cat_sisap_map_residencies b 
        WHERE a.up = b.resi_cod
        AND b.sisap_class = 1)
    AND a.val <> 0

    UNION
    SELECT DISTINCT ind, 'Aperiodo', concat('R', replace(entity,'-','_')) as resi,
    analysys, 'NOCAT', 'NOIMP', dim6set, 'N', val
    FROM resis.exp_khalix_bo a
    WHERE exists (
        SELECT 1
        FROM import.cat_sisap_map_residencies b 
        WHERE a.entity = b.resi_cod
        AND b.sisap_class = 1)
    AND a.val <> 0

    UNION
    SELECT DISTINCT i.indi, 'Aperiodo', concat('R', replace(i.up_resi,'-','_')),
    i.analisi, 'NOCAT', 'NOIMP', dim6set, 'N', i.val
    FROM resis.nous_indicadors_resis i, import.cat_sisap_map_residencies c
    WHERE i.up_resi = c.resi_cod
    AND c.sisap_class = 1

    UNION
    SELECT DISTINCT ind, 'Aperiodo', concat('R', replace(entity,'-','_')) as resi,
    analysys, 'NOCAT', 'NOIMP', dim6set, 'N', valor
    FROM resis.exp_khalix_nafres_resi a
    WHERE exists (
        SELECT 1
        FROM import.cat_sisap_map_residencies b 
        WHERE a.entity = b.resi_cod
        AND b.sisap_class = 1)
    AND a.valor <> 0

    UNION
    SELECT DISTINCT ind, 'Aperiodo', concat('R', replace(entity,'-','_')) as resi,
    analysys, 'NOCAT', 'NOIMP', dim6set, 'N', valor
    FROM resis.exp_khalix_nafres_resi_total a
    WHERE exists (
        SELECT 1
        FROM import.cat_sisap_map_residencies b 
        WHERE a.entity = b.resi_cod
        AND b.sisap_class = 1)
    AND a.valor <> 0

    UNION
    SELECT
    ind, 'Aperiodo', concat('R', replace(ent,'-','_')) as resi,
    analisi, 'NOCAT', 'NOIMP', 'POBACTUAL', 'N', sum(valor) as val
    FROM resis.exp_khalix_gida_resi a
    WHERE exists (
        SELECT 1
        FROM import.cat_sisap_map_residencies b
        WHERE a.ent = b.resi_cod
        AND b.sisap_class = 1)
    AND analisi = 'DEN'
    AND (motiu = 'PALADU'
    OR ind in ('GESTINF02','GESTINF01','GESTINF07'))
    GROUP BY ind, concat('R', replace(ent,'-','_')), analisi
    HAVING sum(valor) > 0

    UNION
    SELECT
    ind, 'Aperiodo', concat('R', replace(ent,'-','_')) as resi,
    analisi, 'NOCAT', 'NOIMP', 'POBACTUAL', 'N', sum(valor) as val
    FROM resis.exp_khalix_gida_resi a
    WHERE exists (
        SELECT 1
        FROM import.cat_sisap_map_residencies b
        WHERE a.ent = b.resi_cod
        AND b.sisap_class = 1)
    AND analisi = 'NUM'
    GROUP BY ind, concat('R', replace(ent,'-','_')), analisi
    HAVING sum(valor) > 0

    UNION
    SELECT
    ind, 'Aperiodo', concat('R', replace(ent,'-','_')) as resi,
    analisi, 'NOCAT', 'NOIMP', 'POBTOTAL', 'N', sum(valor) as val
    FROM resis.exp_khalix_gida_resi_total a
    WHERE exists (
        SELECT 1
        FROM import.cat_sisap_map_residencies b
        WHERE a.ent = b.resi_cod
        AND b.sisap_class = 1)
    AND analisi = 'DEN'
    AND (motiu = 'PALADU'
    OR ind in ('GESTINF02','GESTINF01','GESTINF07'))
    GROUP BY ind, concat('R', replace(ent,'-','_')), analisi
    HAVING sum(valor) > 0

    UNION
    SELECT
    ind, 'Aperiodo', concat('R', replace(ent,'-','_')) as resi,
    analisi, 'NOCAT', 'NOIMP', 'POBTOTAL', 'N', sum(valor) as val
    FROM resis.exp_khalix_gida_resi_total a
    WHERE exists (
        SELECT 1
        FROM import.cat_sisap_map_residencies b
        WHERE a.ent = b.resi_cod
        AND b.sisap_class = 1)
    AND analisi = 'NUM'
    GROUP BY ind, concat('R', replace(ent,'-','_')), analisi
    HAVING sum(valor) > 0
"""

u.exportKhalix(sql, 'QCRES')

sql = """
    SELECT DISTINCT ind, period, concat('R', replace(entity,'-','_')) as resi,
    analysys, 'NOCAT', detail, dim6set, 'N', valor
    FROM resis.exp_khalix_longit a
    WHERE exists (
        SELECT 1
        FROM import.cat_sisap_map_residencies b 
        WHERE a.entity = b.resi_cod
        AND b.sisap_class = 1)
    AND a.valor <> 0"""

u.exportKhalix(sql, 'QCRES_LONG')

if u.IS_MENSUAL:
    sql = """
        SELECT DISTINCT ind, 'Aperiodo', concat('R', replace(entity,'-','_')) as resi,
        analysys, 'NOCAT', detail, dim6set, 'N', valor
        FROM resis.exp_khalix_cont a
        WHERE exists (
            SELECT 1
            FROM import.cat_sisap_map_residencies b 
            WHERE a.entity = b.resi_cod
            AND b.sisap_class = 1)
        AND a.valor <> 0
    """
    u.exportKhalix(sql, 'QCRES_CONT')
