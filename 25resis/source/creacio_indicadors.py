# -*- coding: utf-8 -*-

import hashlib as h
import datetime as d
import collections as c
from dateutil.relativedelta import relativedelta

import sisaptools as t
import sisapUtils as u
import shared as s

DEXTD = t.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]  # noqa
DEXT = str(DEXTD)
now_1m = DEXTD - relativedelta(months=1)
now_1y = DEXTD - relativedelta(years=1)

u.createTable(
    'exp_ecap_uba_hist',
    "(up varchar(5), \
      uba varchar(10), \
      tipus varchar(1), \
      indicador varchar(8), \
      numerador int, \
      denominador int, \
      resultat double, \
      noresolts int)",
    'resis',
    rm=True
)

u.createTable(
    'pacients_resis',
    "(id_cip_sec int, \
    indicador varchar(15), \
    compleix int)",
    'resis',
    rm=True
)

def sub_get_master(sql):
    # partition, sql = params
    dades = c.defaultdict(dict)
    for id, antipsico, benzo, iace_memantina, antidepressius, ansio_hipnotics, estatines, aines, pcc, maca, piic, urinaria, maltracte, osteoporosi, demencia, depre, ci, avc, claud_int, irc, insuf_hepat, antiosteop, DMO, gds_fast, npi_q, gds7b, edat, registre_caig, braden, mna, barthel, imc, pes, max_pes, pfeifer, caigudes, polif, psicof, MEDyear, MEDmonth, INFyear, INFmonth, P_MEDyear, P_INFyear, T_MEDyear, T_INFyear, insomni, pcc_maca, servei_mg, servei_inf, parla, fragilitat, antidepressius_2, ansio_hipnotics_2, barthel_frag, pfeifer_frag, mna_frag, caigudes_frag, emocional, social in u.getAll(sql, 'resis'):
        if antipsico:
            dades['antipsico'][id] = antipsico
        if benzo:
            dades['benzo'][id] = benzo
        if iace_memantina:
            dades['iace_memantina'][id] = iace_memantina
        if antidepressius:
            dades['antidepressius'][id] = antidepressius
        if ansio_hipnotics:
            dades['ansio_hipnotics'][id] = ansio_hipnotics
        if estatines:
            dades['estatines'][id] = estatines
        if aines:
            dades['aines'][id] = aines
        if edat:
            dades['edat'][id] = edat
        if barthel != -1:
            dades['barthel'][id] = barthel
        if imc:
            dades['imc'][id] = imc
        if pes:
            dades['pes'][id] = pes
        if max_pes:
            dades['max_pes'][id] = max_pes
        if pfeifer != -1:
            dades['pfeifer'][id] = pfeifer
        if caigudes != -1:
            dades['caigudes'][id] = caigudes
        if polif:
            dades['polif'][id] = polif
        if psicof:
            dades['psicof'][id] = psicof
        if MEDyear:
            dades['MEDyear'][id] = MEDyear
        if MEDmonth:
            dades['MEDmonth'][id] = MEDmonth
        if INFyear:
            dades['INFyear'][id] = INFyear
        if INFmonth:
            dades['INFmonth'][id] = INFmonth
        if T_MEDyear:
            dades['T_MEDyear'][id] = T_MEDyear
        if T_INFyear:
            dades['T_INFyear'][id] = T_INFyear
        if P_MEDyear:
            dades['P_MEDyear'][id] = P_MEDyear
        if P_INFyear:
            dades['P_INFyear'][id] = P_INFyear
        if servei_mg != 0:
            dades['MG'][id] = servei_mg
        if servei_inf != 0:
            dades['INF'][id] = servei_inf
        if pcc == 1:
            dades['pcc'][id] = pcc
        if maca == 1:
            dades['maca'][id] = maca
        if piic == 1:
            dades['piic'][id] = piic
        # if piir == 1:
        #     dades['piir'][id] = piir
        # if pirda == 1:
        #     dades['pirda'][id] = pirda
        # if piirV == 1:
        #     dades['piirV'][id] = piirV
        # if piirT == 1:
        #     dades['piirT'][id] = piirT
        if urinaria == 1:
            dades['urinaria'][id] = urinaria
        if maltracte == 1:
            dades['maltracte'][id] = maltracte
        if osteoporosi == 1:
            dades['osteoporosi'][id] = osteoporosi
        if demencia == 1:
            dades['demencia'][id] = demencia
        if depre == 1:
            dades['depre'][id] = depre
        if ci == 1:
            dades['ci'][id] = ci
        if avc == 1:
            dades['avc'][id] = avc
        if claud_int == 1:
            dades['claud_int'][id] = claud_int
        if irc == 1:
            dades['irc'][id] = irc
        if insuf_hepat == 1:
            dades['insuf_hepat'][id] = insuf_hepat
        if antiosteop == 1:
            dades['antiosteop'][id] = antiosteop
        if DMO == 1:
            dades['DMO'][id] = DMO
        if gds_fast == 1:
            dades['gds_fast'][id] = gds_fast
        if npi_q == 1:
            dades['npi_q'][id] = npi_q
        if gds7b == 1:
            dades['gds7b'][id] = gds7b
        if registre_caig == 1:
            dades['registre_caig'][id] = registre_caig
        if braden == 1:
            dades['braden'][id] = braden
        if mna == 1:
            dades['mna'][id] = mna
        if insomni == 1:
            dades['insomni'][id] = insomni
        if pcc_maca == 1:
            dades['pcc_maca'][id] = pcc_maca
        if parla == 0: #exclusio de RES027A
            dades['parla'][id] = parla
        if barthel_frag != -1 and (pfeifer_frag or demencia or parla) and mna_frag and caigudes_frag and (emocional or demencia) and edat >= 15:
            dades['valoracio'][id] = 1
        if fragilitat and edat >= 15:
            dades['valoracio'][id] = 1
        if social and barthel_frag and edat < 15:
            dades['valoracio'][id] = 1
        if antidepressius_2 > 1:
            dades['antidepressius_2'][id] = antidepressius_2
        if ansio_hipnotics_2 > 1:
            dades['ansio_hipnotics_2'][id] = ansio_hipnotics_2
    # u.printTime(str(partition))
    # for d in dades:
    #     print(str(len(dades[d])))
    return dades


class Master():
    def __init__(self):
        u.printTime('inici')
        self.dades = c.defaultdict(dict)
        self.get_conversor()
        u.printTime('get_conversor')
        self.get_pob()
        u.printTime('get_pob')
        self.get_idcipsec_map()
        u.printTime('get_idcipsec_map')
        self.get_id_resi()
        u.printTime('get_id_resi')
        self.get_master()
        u.printTime('get_master')
        self.treat_data()
        u.printTime('treat_data')
        self.get_indicadors()
        u.printTime('get_indicadors')
        self.get_bo()
        u.printTime('get_bo')
        self.get_longit_dades()
        u.printTime('get_longit_dades')
        if u.IS_MENSUAL:
            self.get_cont()
            u.printTime('get_cont')
            self.upload_polimedicats()
            u.printTime('upload_polimedicats')
        u.printTime('fi')
    
    def upload_polimedicats(self):
        # cols = "(mes int, hash varchar2(40), up varchar2(10), uba varchar2(10), polif int, situacio varchar2(1), resi int, atdom int)"
        # u.createTable('polimedicacio_resi_atdom', cols, 'exadata', rm=True)
        sql = """
            SELECT id_cip_sec
            FROM eqa_problemes
            WHERE ps = 45
            AND dde <= DATE '{DEXTD}'
        """.format(DEXTD=DEXTD)
        atdom = set([id for id, in u.getAll(sql, 'nodrizas')])
        residents = set([id for id, in u.getAll("""
            SELECT id_cip_sec FROM assignada_tot WHERE up_residencia <> ''
            """,
            "nodrizas")])
        self.atdom = atdom - residents
        sql = """
            SELECT id_cip_sec, up, uba, ubainf, edat, pcc, maca
            FROM assignada_tot
        """
        poblacio = {}
        for id, up, uba, ubainf, edat, pcc, maca in u.getAll(sql, 'nodrizas'):
            poblacio[id] = (up, uba, ubainf, edat, pcc, maca)
        
        sql = """
            SELECT hash_redics, hash_covid
            FROM dwsisap.pdptb101_relacio
        """
        hashos = {}
        for redics, covid in u.getAll(sql, 'exadata'):
            hashos[covid] = redics
        
        sql = """
            SELECT hash, situacio
            FROM dwsisap.dbc_poblacio
        """
        estat = {}
        for covid, situacio in u.getAll(sql, 'exadata'):
            try:
                hash = hashos[covid]
                estat[hash] = situacio if situacio == 'D' else 'A'
            except KeyError:
                continue
        upload = []
        pobtotal = set(self.id_resi_tot.keys()) | self.atdom
        for id in pobtotal:
            try:
                polif = self.varss['polif'][id] if id in self.varss['polif'] else 0
                if id in poblacio:
                    (up, uba, ubainf, edat, pcc, maca) = poblacio[id]
                    (hash_d, sector) = self.ids[id]
                    situacio = estat[hash_d]
                    resi = 1 if id in self.id_resi_tot else 0
                    atdom = 1 if id in self.atdom else 0
                    upload.append((DEXTD.year, DEXTD.month, sector, hash_d, polif, up, uba, ubainf, edat, situacio, resi, atdom, pcc, maca))
            except KeyError:
                continue
        u.listToTable(upload, 'polimedicacio_resi_atdom', 'redics')
        


    def get_cont(self):
        DETALLES = {'MG': 'TIPPROF1',
                    'PED': 'TIPPROF2',
                    'ODN': 'TIPPROF3',
                    'INF': 'TIPPROF4'}
        cont = c.Counter()
        opts = {
            'actual': self.id_resi,
            'total': self.id_resi_tot
        }
        pacients = []
        for file_name, pob_d in opts.items():
            dim6set = 'POBTOTAL' if file_name == 'total' else 'POBACTUAL'
            for servei in ('MG', 'INF'):
                for id in self.varss[servei]:
                    if self.varss[servei][id] > 0:
                        if id in pob_d:
                            if dim6set == 'POBACTUAL':
                                pacients.append((id, 'CONT0002', 1))
                            up = pob_d[id]['UP']
                            # servei = self.varss['servei'][id]
                            r2 = self.varss[servei][id]
                            cont[('CONT0002', up, 'NUM', DETALLES[servei], dim6set)] += r2
                            cont[('CONT0002', up, 'DEN', DETALLES[servei], dim6set)] += 1
                            if id in self.varss['pcc_maca']:
                                if dim6set == 'POBACTUAL':
                                    pacients.append((id, 'CONT0002P', 1))
                                cont[('CONT0002P', up, 'NUM', DETALLES[servei], dim6set)] += r2
                                cont[('CONT0002P', up, 'DEN', DETALLES[servei], dim6set)] += 1
        upload = []
        for (ind, up, a, det, dim6set), v in cont.items():
            upload.append((ind, up, a, det, dim6set, v))
        cols = "(ind varchar(15), entity varchar(13), analysys varchar(3), detail varchar(10), dim6set varchar(10), valor int)"
        u.createTable('exp_khalix_cont', cols, 'resis', rm=True)
        u.listToTable(upload, 'exp_khalix_cont', 'resis')
        u.listToTable(pacients, 'pacients_resis', 'resis')


    def get_partition(self, db, tb, by):
        """."""
        PERIODE = "DATE '{DEXTD}'".format(DEXTD=DEXTD)
        self.partitions = {}
        dext, = u.getOne("select {data_ext} from dextraccio".format(data_ext=PERIODE), "nodrizas")
        dexta, = u.getOne("""
                          select date_add({data_ext}, interval - 1 month)
                          from dextraccio
                          """.format(data_ext=PERIODE), "nodrizas")
        sql = "select {} from {} partition({}) limit 1"
        for partition in u.getTablePartitions(tb, db):
            try:
                dvis, = u.getOne(sql.format(by, tb, partition), db)
            except TypeError:
                continue
            if (dvis.year == dext.year and dvis.month == dext.month) or \
                    (dvis.year == dexta.year and dvis.month == dexta.month):
                self.partitions[partition] = True

    def get_bo(self):
        """ source: BO """
        file_name = "bo_resi"

        map = {
            "GER5": "RES009",
            # "GER20": "RES025A",  # valoracio integral, pasado a get_tests()
            # "GER19": "RES028A",  # BRADEN, pasado a get_tests()
            # "GER21": "RES037A",  # % caigudes, pasado a get_tests()
            # "GER16": "RES036A",  # RES039(mna) o RES036A (pes), pasado a get_tests()  # noqa
            "GER23": "RES058A",  # taxa fractures <---------------------------- (SIN RESULTADOS)  # noqa
            "GER25": "RES040A",
            "GER26": "RES041A",
            # "GER24": "RES043A1",  # Polifarmacia 10+, pasado a get_tests()
            # "GER22": "RES057A",  # caigudes (MTJ), passado a gest_tests()
            "GER33": "RES060A",  # urgencias AP
            # "GER37": "RES062A",  # Ingresos Hx
            "GER40": "RES066A",  # den: GER2
            "GER44": "RES067A",
            "GER45": "RES068A",  # 27 resis sin denominador: GER2?
            "GER46": "RES074A",
            "GER47": "RES071A",
            "GER48": "RES073A",
            "GER49": "RES070A",  # "GER49": "RES069A", # num/dx_incontinencia?
            "GER50": "RES072A",
            "GER51": "RES065A"
            }

        sql = """
            SELECT
                'GER40' AS indicador,
                UP_RESI,
                SUM(CASE WHEN indicador = 'GER40'
                    THEN NVL2(NUM, NUM, VALOR) ELSE 0 END) AS NUM,
                SUM(CASE WHEN indicador = 'GER2'
                    THEN NVL2(DEN, DEN, VALOR) ELSE 0 END) AS DEN
            FROM
                DWAP.INDICADORS_RESIDENCIES ir
            WHERE
                indicador IN ('GER40', 'GER2')
            GROUP BY
                UP_RESI, periode_ind
            HAVING
                periode_ind = max(periode_ind)
            UNION
            SELECT indicador, UP_RESI,
                sum(case indicador
                        -- when 'GER22' then num/100
                        when 'GER33' then num/100
                        --  when 'GER37' then num/100
                        when 'GER23' then num/100
                        else num
                        end) AS NUM,
                sum(den) AS DEN
            FROM
                DWAP.INDICADORS_RESIDENCIES ir2
            WHERE
                indicador IN (
                    'GER5',
                    -- 'GER20',
                    -- 'GER19',
                    -- 'GER21',
                    -- 'GER16',
                    'GER25',
                    'GER26',
                    -- 'GER24',
                    -- 'GER22',
                    'GER33',
                    -- 'GER37',
                    'GER23',
                    'GER51',
                    -- 'GER40', 'GER2',  -- ger40/ger2:  [JUST_VAL]
                    'GER44',
                    'GER45', -- /GER2 [JUST_NUM: 27, NUM_DEN: 697]
                    'GER49', -- (num)/dx incontinencia
                    'GER47',
                    'GER50',
                    'GER48',
                    'GER46')
            GROUP BY
                up_resi, indicador, periode_ind
            HAVING
                sum(den) > 0
                and periode_ind = max(periode_ind)
            """
        upload = []
        for bo_ind, resi, num, den in u.getAll(sql, 'exadata'):
            ind = map[bo_ind]
            upload.append((ind, resi, 'NUM', 'POBACTUAL', num))
            upload.append((ind, resi, 'DEN', 'POBACTUAL', den))
        cols = "(ind varchar(15), entity varchar(13), analysys varchar(3), dim6set varchar(10), val int)"
        u.createTable('exp_khalix_bo', cols, 'resis', rm=True)
        u.listToTable(upload, 'exp_khalix_bo', 'resis')

    def get_longit_dades(self):

        # file_name = "LONG_resi"

        pobs = {
            'long_actual': self.id_resi,
            'long_total': self.id_resi_tot,
            # 'LONG_resi_atdom': self.atdom_ass,
            }

        self.get_partition(db='nodrizas', tb='ag_longitudinalitat_new', by='vis_data')
        self.longit_dades = c.Counter()
        pacients = []
        for file_name, pob_d in pobs.items():
            dim6set = 'POBTOTAL' if file_name == 'long_total' else 'POBACTUAL'
            for parti in self.partitions:
                sql = """
                    SELECT
                        pac_id, pac_up, pac_uba, pac_ubainf,
                        pac_pcc, pac_maca, pac_atdom,
                        modul_serv_homol, modul_eap,
                        prof_responsable, prof_delegat, modul_relacio,
                        prof_rol_gestor, modul_serv_gestor,
                        right(year(vis_data),2),date_format(vis_data,'%m')
                    FROM
                        {} partition({})
                    WHERE
                        prof_numcol <> '' AND
                        vis_laborable = 1
                """.format('ag_longitudinalitat_new', parti)  # Error al filtrar por pac_instit = 0
                for (id, _up, _uba, _ubainf, pcc, maca, _atdom, serv,
                        eap, resp, deleg, _rel, gc_rol, gc_serv,
                        anys, mes) in u.getAll(sql, 'nodrizas'):
                    if id in pob_d:
                        # pob = "POBACTUAL"
                        resi = pob_d[id]['UP']
                        gc = (gc_rol == 1 or gc_serv == 1)
                        periodo = "A" + str(anys) + str(mes)
                        if (eap and serv in ("MG", "PED", "INF", "INFP")) or gc:
                            # br = self.centres[up]
                            tip = ["2"]
                            if pcc or maca:
                                tip.append("1")
                            # tip = "1" if (pcc or maca) else "2"  # quito atdom
                            tipprof = 1 if serv == "MG" else 2 if serv == "PED" else 4  # noqa
                            detalle = "TIPPROF{}".format(tipprof)
                            for t in tip:
                                compleix = 0
                                cuenta = "LONG000{}".format(t)
                                self.longit_dades[(cuenta, periodo, resi,
                                                detalle, dim6set,
                                                "DEN")] += 1
                                if resp == 1 or gc == 1 or deleg == 1:
                                    compleix = 1
                                    self.longit_dades[("LONG000{}".format(t),
                                                    periodo, resi,
                                                    detalle, dim6set,
                                                    "NUM")] += 1
                                if dim6set == 'POBACTUAL':
                                    pacients.append((id, cuenta, compleix))

        longit_dades_ls = []
        for (ind, period, entity, detail, pob, analysys), v in self.longit_dades.items():  # noqa
            longit_dades_ls.append((ind, period, entity, analysys, detail, pob, v))

        cols = "(ind varchar(15), period varchar(5), entity varchar(13), analysys varchar(3), detail varchar(10), dim6set varchar(10), valor int)"
        u.createTable('exp_khalix_longit', cols, 'resis', rm=True)
        u.listToTable(longit_dades_ls, 'exp_khalix_longit', 'resis')
        u.listToTable(pacients, 'pacients_resis', 'resis')
            
    def get_id_resi(self):
        filia = c.defaultdict(lambda: c.defaultdict())
        sql = """
            SELECT
                id_cip_sec,
                up, uba,
                upinf, ubainf, up_residencia
            from assignada_tot
            where institucionalitzat = 1
        """
        resis = c.defaultdict()
        self.pob_ass = {}
        for id, up, uba, upinf, ubainf, resi in u.getAll(sql, 'eqa_ind'):
            resis[id] = resi
            if up and uba:
                filia[id]['M'] = (up, uba)
            if upinf and ubainf:
                filia[id]['I'] = (upinf, ubainf)
            self.pob_ass[id] = (up, uba, ubainf)
        self.id_resi = c.defaultdict(dict)
        self.id_resi_tot = c.defaultdict(dict)
        for cip, resi in self.pob_act.items():
            hash_a = self.conversor.get(cip)
            # dep = dependencia.get(cip)
            try:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi[id]['UP'] = resi
                # self.id_resi[id]['dep'] = dep
                ubas = []
                if filia.get(id):
                    if filia.get(id).get('M'):
                        ubas.append(filia.get(id).get('M') + ('M',))
                    if filia.get(id).get('I'):
                        ubas.append(filia.get(id).get('I') + ('I',))
                self.id_resi[id]['ubas'] = ubas
                # cada element es una tupla amb (up, uba, M o I)
                #self.id_resi[id]['resis'] = resis
            except KeyError:
                continue
        self.id_resi.default_factory = None
        for cip, resi in self.pob_tot.items():
            hash_a = self.conversor.get(cip)
            # dep = dependencia.get(cip)
            try:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi_tot[id]['UP'] = resi
                # self.id_resi_tot[id]['dep'] = dep
            except KeyError:
                continue
        self.id_resi_tot.default_factory = None
        # upload = []
        # for id in self.id_resi:
        #     dim6set = 'POBACTUAL'
        #     up = self.id_resi[id]['UP']
        #     upload.append((id, dim6set, up))
        # for id in self.id_resi_tot:
        #     dim6set = 'POBTOTAL'
        #     up = self.id_resi_tot[id]['UP']
        #     upload.append((id, dim6set, up))
        # cols = "(id_cip_sec int, up varchar(7), dim6set varchar(10))"
        # u.createTable('val_master', cols, 'resis', rm=True)
        # u.listToTable(upload, 'val_master', 'resis')


    def get_idcipsec_map(self):
        """
        Gets id_cip_sec and hash_d from hash_a.
        - {u11}  # hash_a: {id_cip_sec, hash_d}
        """
        self.resi_sec = {}
        with t.Database('p2262', 'nodrizas') as conn:
            for resi, sector in conn.get_all("""
                    WITH
                        resi_up as (SELECT
                                        distinct resi_cod as resi, eap_up as up
                                    FROM
                                        import.cat_sisap_map_residencies
                                    WHERE
                                        sisap_class =1),
                        up_sec as  (SELECT
                                        scs_codi as up, sector
                                    FROM
                                    nodrizas.cat_centres)
                    SELECT
                        r.resi, u.sector
                    FROM
                        resi_up r INNER JOIN
                        up_sec u ON r.up = u.up
                    """):
                self.resi_sec[resi] = sector
        sql = "select id_cip_sec, hash_a, hash_d, codi_sector from u11_all"
        self.hash_sec = {}
        self.hashes = set()
        self.u11 = {}
        self.ids = {}
        for cip, resi in self.pob_tot.items():
            sector = self.resi_sec.get(resi)
            hash_a = self.conversor.get(cip)
            if hash_a is not None:
                self.hash_sec[hash_a] = sector
                self.hashes.add(hash_a)
        for cip, resi in self.pob_act.items():
            sector = self.resi_sec.get(resi)
            hash_a = self.conversor.get(cip)
            if hash_a is not None:
                self.hash_sec[hash_a] = sector
                self.hashes.add(hash_a)

        for id_cip_sec, hash_a, hash_d, sector in u.getAll(sql, 'import'):
            sec = self.hash_sec.get(hash_a)
            if sec == sector:
                self.ids[id_cip_sec] = (hash_d, sector)
                self.u11[hash_a] = {'id_cip_sec': id_cip_sec,
                                    'hash_d': hash_d,
                                    'sector': sector,
                                    }
        # print(len(self.ids))
        # print(len(self.u11))

    def treat_data(self):
        # perdua pes: imc <= 18.5 or peso <= 0.90 * max_pes
        for id in self.varss['imc']:
            imc = self.varss['imc'][id]
            if imc <= 18.5:
                self.varss['perdua_pes'][id] = 1
        for id in self.varss['pes']:
            peso = self.varss['pes'][id]
            if id in self.varss['max_pes']:
                if peso <= 0.9 * self.varss['max_pes'][id]:
                    self.varss['perdua_pes'][id] = 1
        
        # polif10: polif > 10
        for id in self.varss['polif']:
            polif = self.varss['polif'][id]
            if polif > 10:
                self.varss['polif10'][id] = 1
            if polif > 8:
                self.varss['polif8'][id] = 1
        
        # barthel: dividir en < 20, <= 40, entre 20 i 60, 60-100, < 60
        for id in self.varss['barthel']:
            barthel = self.varss['barthel'][id]
            if barthel < 20:
                self.varss['barthel_lt20'][id] = 1
            if barthel <= 40:
                self.varss['barthel_lt40'][id] = 1
            if 20 <= barthel < 60:
                self.varss['barthel_20-59'][id] = 1
            if 60 <= barthel < 100:
                self.varss['barthel_60-99'][id] = 1
            if barthel == 100:
                self.varss['barthel_100'][id] = 1
            if barthel < 60:
                self.varss['barthel_lt60'][id] = 1
        #     if id in (self.varss['pfeifer'] | self.varss['lobo'] | self.varss['demencia']) and id in self.varss['mna'] and id in (self.varss['caigudes'] | self.varss['registre_caig']) and id in self.varss['edat'] and self.varss['edat'] >= 15:
        #         self.varss['valoracio'][id] = 1
        #     elif id in self.varss['socialrisk'] and id in self.varss['edat'] and self.varss['edat'][id] < 15:
        #         self.varss['valoracio'][id] = 1
        # for id in self.varss['fragilitat']:
        #     if id in self.varss['edat'] and self.varss['edat'] >= 15:
        #         self.varss['valoracio'][id] = 1
        
        # pfeifer
        for id in self.varss['pfeifer']:
            pfeifer = self.varss['pfeifer'][id]
            if pfeifer <= 2:
                self.varss['pfeifer_le2'][id] = 1
            if 3<= pfeifer <= 4:
                self.varss['pfeifer_3-4'][id] = 1
            if 5<= pfeifer <= 7:
                self.varss['pfeifer_5-7'][id] = 1
            if 8 <= pfeifer <= 10:
                self.varss['pfeifer_ge8'][id] = 1
            if pfeifer >= 5:
                self.varss['pfeifer_ge5'][id] = 1
        # antipsico >= 90 dies
        for id in self.varss['antipsico']:
            data = self.varss['antipsico'][id]
            if u.daysBetween(data, DEXTD) >= 90:
                self.varss['antipsico_90'][id] = 1
        # ((self.grups['barthel'] & (self.grups['pfeifer'] | self.grups['lobo'] | self.grups['demencia']) & self.grups['mna'] & (self.grups['caigudes'] | self.grups['registre_caig'])) | self.grups['fragilitat']) & self.grups['parla'],

        for id in self.varss['edat']:
            if self.varss['edat'][id] > 65:
                self.varss['>65 anys'][id] = 1

    def get_llistat(self, codi, pob_d, num, pac, den=None):
        indicador_uba = c.defaultdict(c.Counter)
        pre_llistat = c.defaultdict(lambda: c.defaultdict(set))
        for id in pob_d:
            for uba in pob_d[id]['ubas']:
                # uba te estructura (up, uba, 'M' o 'I')
                up = uba[0]
                tipus = uba[2]
                uba = uba[1]
                denominador = id in den if den else True
                if denominador:
                    number_den = 1 if pac else 1 if id in den else 0
                    indicador_uba[(up,uba,tipus)]["DEN"] += number_den
                    pre_llistat[(up,uba,tipus)]["DEN"].add(den[id] if den else id)   # noqa
                    if id in num:
                        indicador_uba[(up,uba,tipus)]["NUM"] += 1  # noqa
                        pre_llistat[(up,uba,tipus)]["NUM"].add(id)
        try:
            invers = [row for row, in u.getAll("""select invers from resiscataleg
                                                where indicador = '{}'
                                                """.format(codi), 'pdp')][0]
        except IndexError:
            invers = 0
        llistats = []
        for cosa in pre_llistat.keys():
            up, uba, tipus = cosa
            if invers == 1:
                no_resolts = pre_llistat[(up, uba, tipus)]['NUM']
                resolts = pre_llistat[(up, uba, tipus)]['DEN'] - no_resolts
            else:
                resolts = pre_llistat[(up, uba, tipus)]['NUM']
                no_resolts = pre_llistat[(up, uba, tipus)]['DEN'] - resolts
            indicador_uba[(up, uba, tipus)]['noresolts'] = len(no_resolts)
            # for e9 in resolts:
            #     if e9 in self.ids:
            #         llistats.append((up, uba, tipus, codi, self.ids[e9][0], self.ids[e9][1], 9, None))  # noqa
            for e0 in no_resolts:
                if e0 in self.ids:
                    llistats.append((up, uba, tipus, codi, self.ids[e0][0], self.ids[e0][1], 0, None))  # noqa
        # print("llistat rows: {}".format(llistats[:3]))

        indicador_out = [(DEXTD.year, DEXTD.month,
                            up, uba, None, tipus, codi,
                            indicador_uba[(up, uba, tipus)]["NUM"],
                            indicador_uba[(up, uba, tipus)]["DEN"],
                            indicador_uba[(up, uba, tipus)]["NUM"] * 1.0
                            / indicador_uba[(up, uba, tipus)]["DEN"],
                            indicador_uba[(up, uba, tipus)]['noresolts'])
                            for (up, uba, tipus) in indicador_uba]
        print(len(indicador_out))
        print(len(llistats))
        return (indicador_out, list(set(llistats)))

    def get_indicadors(self):
        self.grups = c.defaultdict(set)
        for var in self.varss:
            for id in self.varss[var]:
                if self.varss[var] > 0:
                    self.grups[var].add(id)
        u.printTime('despres grups')
        ind_opts = {
            "RES001A": {'num': self.grups['INFyear'],
                        'type': 'MTJ',
                        'val': self.varss['INFyear']
                        },
            "RES001AP": {'num': self.grups['P_INFyear'],
                        'type': 'MTJ',
                        'val': self.varss['P_INFyear']
                        },
            "RES001AT": {'num': self.grups['T_INFyear'],
                        'type': 'MTJ',
                        'val': self.varss['T_INFyear']
                        },
            "RES002A": {'num': self.grups['INFmonth'],
                        'type': 'MTJ',
                        'val': self.varss['INFmonth']
                        },
            'RES003A': {'num': self.grups['INFyear'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES003AP': {'num': self.grups['P_INFyear'],
                            'type': 'PCT',
                        'val': None
                            },
            'RES003AT': {'num': self.grups['T_INFyear'],
                            'type': 'PCT',
                        'val': None
                            },
            'RES004A': {'num': self.grups['INFmonth'],
                        'type': 'PCT',
                        'val': None
                        },
            "RES005A": {'num': self.grups['MEDyear'],
                        'type': 'MTJ',
                        'val': self.varss['MEDyear']
                        },
            "RES005AP": {'num': self.grups['P_MEDyear'],
                        'type': 'MTJ',
                        'val': self.varss['P_MEDyear']
                        },
            "RES005AT": {'num': self.grups['T_MEDyear'],
                        'type': 'MTJ',
                        'val': self.varss['T_MEDyear']
                        },
            "RES006A": {'num': self.grups['MEDmonth'],
                        'type': 'MTJ',
                        'val': self.varss['MEDmonth']
                        },
            'RES007A': {'num': self.grups['MEDyear'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES007AP': {'num': self.grups['P_MEDyear'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES007AT': {'num': self.grups['T_MEDyear'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES008A': {'num': self.grups['MEDmonth'],
                        'type': 'PCT',
                        'val': None
                        },
            "RES011A": {'num': self.grups['edat'],
                        'type': 'MTJ',
                        'val': self.varss['edat']
                        },
            # "RES012A": {'num': self.grups['gma'],
            #             'type': 'MTJ',
            #             },
            "RES013A": {'num': self.grups['barthel'],
                        'den': self.grups['barthel'],
                        'type': 'MTJ',
                        'val': self.varss['barthel']
                        },
            "RES014A": {'num': self.grups['pfeifer'],
                        'den': self.grups['pfeifer'],
                        'type': 'MTJ',
                        'val': self.varss['pfeifer']
                        },
            'RES015A': {'num': self.grups['barthel_lt60'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES016A': {'num': self.grups['pfeifer_ge5'],
                        'type': 'PCT',
                        'val': None
                        },
            # "RES017A": {'num': self.grups['dep'],
            #             'type': 'PCT',
            #             },
            'RES018A': {'num': self.grups['pcc'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES019A': {'num': self.grups['maca'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES020A': {'num': self.grups['demencia'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES021A': {'num': self.grups['npi_q'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES022A': {'num': self.grups['urinaria'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES023A': {'num': self.grups['piic'] & self.grups['pcc'],
                        'den': self.grups['pcc'],
                        'type': 'PCT',
                        'val': None
                        },  # pcc
            # 'RES023A1': {'num':
            #                 self.grups['piir'] &
            #                 self.grups['pcc'],
            #                 'den':
            #                 self.grups['pcc'],
            #                 'type': 'PCT',
            #             'val': None
            #                 },  # pcc_piic
            # 'RES023A2': {'num': self.grups['pirda'] &
            #                 self.grups['pcc'] & self.grups['piic'],
            #                 'den':
            #                 self.grups['pcc'],
            #                 'type': 'PCT',
            #             'val': None
            #                 },  # pcc_piic
            'RES024A': {'num': self.grups['piic'] & self.grups['maca'],
                        'den': self.grups['maca'],
                        'type': 'PCT',
                        'val': None
                        },  # maca
            # 'RES024A1': {'num': self.grups['piir'] &
            #                 self.grups['maca'],
            #                 'den':
            #                 self.grups['maca'],
            #                 'type': 'PCT',
            #             'val': None
            #                 },  # maca_piic
            # 'RES024A2': {'num': self.grups['pirda'] &
            #                 self.grups['maca'] & self.grups['piic'],
            #                 'den':
            #                 self.grups['maca'],
            #                 'type': 'PCT',
            #             'val': None
            #                 },  # maca_piic
            'RES025A': {'num': self.grups['valoracio'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES026A': {'num': self.grups['barthel'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES026A1': {'num': self.grups['barthel_lt20'],
                            'type': 'PCT',
                        'val': None
                            },
            'RES026A2': {'num': self.grups['barthel_20-59'],
                            'type': 'PCT',
                        'val': None
                            },
            'RES026A3': {'num': self.grups['barthel_60-99'],
                            'type': 'PCT',
                        'val': None
                            },
            'RES026A4': {'num': self.grups['barthel_100'],
                            'type': 'PCT',
                        'val': None
                            },
            'RES027A': {'num': self.grups['pfeifer'],
                        'den': self.grups['parla'] - self.grups['demencia'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES027A1': {'num': self.grups['pfeifer_ge8'],
                        'den': self.grups['parla'] - self.grups['demencia'],
                            'type': 'PCT',
                        'val': None
                        },
            'RES027A2': {'num': self.grups['pfeifer_5-7'],
                        'den': self.grups['parla'] - self.grups['demencia'],
                            'type': 'PCT',
                        'val': None
                            },
            'RES027A3': {'num': self.grups['pfeifer_3-4'],
                        'den': self.grups['parla'] - self.grups['demencia'],
                            'type': 'PCT',
                        'val': None
                            },
            'RES027A4': {'num': self.grups['pfeifer_le2'],
                        'den': self.grups['parla'] - self.grups['demencia'],
                            'type': 'PCT',
                        'val': None
                            },
            'RES028A': {'num': self.grups['braden'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES035A': {'num':
                        self.grups['gds_fast'] &
                        self.grups['demencia'],
                        'den': self.grups['demencia'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES036A': {'num': self.grups['perdua_pes'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES037A': {'num': self.grups['caigudes'] | self.grups['registre_caig'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES039A': {'num': self.grups['mna'],
                        'type': 'PCT',
                        'val': None
                        },
            'RES042A': {
                'num': self.grups['psicof'],  # psicofarm
                'type': 'MTJ',
                        'val': self.varss['psicof']
                },
            'RES043A1': {
                'num': self.grups['polif10'],
                'type': 'PCT',
                        'val': None
                },
            'RES043A2': {
                'num': self.grups['polif8'],
                'type': 'PCT',
                        'val': None
                },
            'RES044A': {  # demencia
                'num': self.grups['demencia'] & self.grups['iace_memantina'],  # iace_memantina
                'den': self.grups['demencia'],
                'type': 'PCT',
                        'val': None
                },
            'RES045A': {  # gds7b
                'num': self.grups['gds7b'] & self.grups['iace_memantina'],  # iace_memantina
                'type': 'PCT',
                        'val': None
                },
            "RES046A": {  # demencia
                "num": self.grups['demencia'] & self.grups['antipsico_90'],  # antipsico
                "den": self.grups['demencia'] & self.grups['iace_memantina'],  # iace_memantina
                'type': 'PCT', # 90 days
                        'val': None
                },
            'RES047A': {
                'num': self.grups['antidepressius_2'],  # antidepressius
                'type': 'PCT', # 2 times
                        'val': None
                },
            'RES048A': {
                'num': self.grups['ansio_hipnotics_2'],  # ansio_hipnotics
                'type': 'PCT', # 2 times
                        'val': None
                },
            'RES049A': {
                'num': self.grups['estatines'],  # estatines
                'type': 'PCT',
                        'val': None
                },
            "RES050A": {  # maca
                "num": self.grups['maca'] & self.grups['estatines'],  # estatines
                "den": self.grups['maca'],
                'type': 'PCT',
                        'val': None
                },
            "RES051A": {  # depre
                "num": self.grups['antidepressius'] & self.grups['depre'],  # antidepressius
                "den": self.grups['depre'],
                'type': 'PCT',
                        'val': None
                },
            "RES052A": {  # insomni
                "num": self.grups['insomni'] & self.grups['benzo'],  # benzo
                "den": self.grups['insomni'] & self.grups['>65 anys'],
                'type': 'PCT',
                        'val': None
                },
            "RES053A": {  # ci, avc, claud_int, irc, insuf_hepat
                "num": self.grups['aines'] & (self.grups['ci'] | self.grups['avc'] | self.grups['claud_int'] | self.grups['irc'] | self.grups['insuf_hepat']),  # aines
                "den": self.grups['ci'] | self.grups['avc'] | self.grups['claud_int'] | self.grups['irc'] | self.grups['insuf_hepat'],
                'type': 'PCT',
                        'val': None
                },
            'RES054A': {'num': self.grups['maltracte'],
                        'type': 'PCT',
                        'val': None
                        },
            "RES056A": {
                'num':
                    self.grups['DMO'] &
                    self.grups['osteoporosi']
                    - self.grups['antiosteop'],
                'den':
                    self.grups['osteoporosi']
                    - self.grups['antiosteop'],
                'type': 'PCT',
                        'val': None
                },
            'RES057A': {'num': self.grups['caigudes'],
                        'type': 'MTJ',
                        'val': self.varss['caigudes']
                        },
            'RES081A': {'num': self.grups['piic'] & (self.grups['pcc'] | self.grups['maca']),
                        'type': 'PCT',
                        'val': None
                        }
            }

        # 'RES057A': {'num': self.grups['caigudes'],
        #                 'type': 'MTJ',
        #                 'val': self.varss['caigudes']
        #                 },
        pacients = []
        landing = []
        upload = c.Counter()
        pobs = {'actual': self.id_resi, 'total': self.id_resi_tot}
        for file_name, pob_d in pobs.items():
            dim6set = 'POBTOTAL' if file_name == 'total' else 'POBACTUAL'  # noqa
            for ind in ind_opts:
                num = ind_opts[ind]['num']
                den = ind_opts[ind]['den'] if 'den' in ind_opts[ind] else pob_d
                typ = ind_opts[ind]['type']
                val = ind_opts[ind]['val']
                if file_name != 'total' and typ == 'PCT':
                    val_ind, val_llist = self.get_llistat(ind,
                        pob_d,
                        num,
                        1 if 'den' not in ind_opts[ind] else None,  # noqa
                        {k: k
                        for k in pob_d.keys()
                        if k in den}
                    )
                    try:
                        sql = "delete from RESISINDICADORS where dataany = {any} and datames = {mes} and indicador = '{indicador}'".format(any=DEXTD.year, mes=DEXTD.month, indicador=ind)
                        u.execute(sql, 'pdp')
                        u.listToTable(val_ind, "RESISINDICADORS", "pdp")
                    except:
                        raise
                    try:
                        sql = "delete from RESISLLISTATS where indicador = '{indicador}'".format(indicador=ind)
                        u.execute(sql, 'pdp')
                        u.listToTable(val_llist, "RESISLLISTATS", "pdp")
                    except:
                        raise
                    try:
                        u.listToTable([(row[2], row[3], row[5], row[6], row[7], row[8], row[9], row[10]) for row in val_ind], 'exp_ecap_uba_hist', 'resis')
                    except:
                        raise
                for id in den:
                    if id in pob_d:
                        compleix = 0
                        resi = pob_d[id]['UP']
                        upload[(ind, resi, 'DEN', dim6set)] += 1
                        if id in num:
                            compleix = 1
                            if typ == 'PCT':
                                upload[(ind, resi, 'NUM', dim6set)] += 1
                            if typ == 'MTJ':
                                upload[(ind, resi, 'NUM', dim6set)] += val[id]
                        if dim6set == 'POBACTUAL':
                            pacients.append((id, ind, compleix))
                            if id in self.pob_ass:
                                (up, uba, ubainf) = self.pob_ass[id]
                                landing.append((id, ind, up, uba, ubainf, compleix))


        upload = [tuple(list(k) + [v]) for k, v in upload.items()]
        cols = "(indicador varchar(10), up varchar(10), analisi varchar(3), dim6set varchar(10), val int)"
        u.createTable('exp_khalix_resis', cols, 'resis', rm=True)
        u.listToTable(upload, 'exp_khalix_resis', 'resis')
        u.listToTable(pacients, 'pacients_resis', 'resis')
        cols = "(id_cip_sec int, ind varchar(10), up varchar(10), uba varchar(20), ubainf varchar(20), num int)"
        u.createTable('landing_resis', cols, 'resis', rm=True)
        u.listToTable(landing, 'landing_resis', 'resis')

    def get_master(self):
        sql = """
            SELECT id_cip_sec, antipsico, benzo, iace_memantina,
            antidepressius, ansio_hipnotics, estatines, aines,
            pcc, maca, piic, urinaria,
            maltracte, osteoporosi, demencia, depre, ci, avc,
            claud_int, irc, insuf_hepat, antiosteop, DMO, gds_fast, npi_q,
            gds7b, edat, registre_caig, braden, mna, barthel,
            imc, pes, max_pes, pfeifer, caigudes, polif, psicof,
            MEDyear, MEDmonth, INFyear, INFmonth, P_MEDyear,
            P_INFyear, T_MEDyear, T_INFyear, insomni, pcc_maca,
            servei_mg, servei_inf, parla, fragilitat,
            antidepressius_2, ansio_hipnotics_2,
            barthel_frag, pfeifer_frag, mna_frag, caigudes_frag,
            emocional, social
            FROM master_residencies
            -- partition()
        """
        
        # partitions = ['resis_1', 'resis_2', 'resis_3', 'resis_4', 'resis_5']
        # jobs = [(partition, sql) for partition in partitions]
        # self.varss = c.defaultdict(dict)
        self.varss = sub_get_master(sql)
        # for dades in u.multiprocess(sub_get_master, jobs, 5, close=True):
        # self.varss.update(dades)
        # vars_last �s [var][cip] = val
        # grups �s [var].add(id)
        # u.printTime('multiprocess')
        # for id, antipsico, benzo, iace_memantina, antidepressius, ansio_hipnotics, estatines, aines, pcc, maca, piic, piir, pirda, piirV, piirT, urinaria, maltracte, osteoporosi, demencia, depre, ci, avc, claud_int, irc, insuf_hepat, antiosteop, DMO, gds_fast, gds7b, edat, registre_caig, braden, mna, barthel, imc, pes, max_pes, pfeifer, caigudes, polif, psicof, MEDyear, MEDmonth, INFyear, INFmonth, P_MEDyear, P_INFyear, T_MEDyear, T_INFyear, insomni, pcc_maca, servei, r2 in u.getAll(sql, 'resis'):
        #     self.varss
        # falta afegir antidepressius_2 i ansio_hipnotics_2

    # def get_id_resi(self):
    #     filia = c.defaultdict(lambda: c.defaultdict())
    #     sql = """
    #         SELECT
    #             id_cip_sec,
    #             up, uba,
    #             upinf, ubainf
    #         from assignada_tot
    #     """
    #     for id, up, uba, upinf, ubainf in u.getAll(sql, 'eqa_ind'):
    #         if up and uba:
    #             filia[id]['M'] = (up, uba)
    #         if upinf and ubainf:
    #             filia[id]['I'] = (upinf, ubainf)
    #     self.id_resi = c.defaultdict(dict)
    #     self.id_resi_tot = c.defaultdict(dict)
    #     for id, resi in self.pob_act.items():
    #         try:
    #             self.id_resi[id]['UP'] = resi
    #             ubas = []
    #             if filia.get(id):
    #                 if filia.get(id).get('M'):
    #                     ubas.append(filia.get(id).get('M') + ('M',))
    #                 if filia.get(id).get('I'):
    #                     ubas.append(filia.get(id).get('I') + ('I',))
    #             self.id_resi[id]['ubas'] = ubas
    #             # cada element es una tupla amb (up, uba, M o I)
    #             #self.id_resi[id]['resis'] = resis
    #         except KeyError:
    #             continue
    #     self.id_resi.default_factory = None
    #     for id, resi in self.pob_tot.items():
    #         try:
    #             self.id_resi_tot[id]['UP'] = resi
    #         except KeyError:
    #             continue
    #     self.id_resi_tot.default_factory = None

    # def get_conversor(self):
    #     self.conversor = c.defaultdict()
    #     sql = """
    #         SELECT id_cip_sec, hash_d
    #         FROM u11
    #     """
    #     for id, hash in u.getAll(sql, 'import'):
    #         self.conversor[hash] = id

    def get_conversor(self):
        """."""
        self.conversor = {}
        sql = "select usua_cip, usua_cip_cod from pdptb101 \
               where usua_cip_cod like '{}%'"
        for cip_a, hash_a in s.get_data(sql, model="redics", redics="pdp"):
            self.conversor[cip_a] = hash_a
        # print(str(len(self.conversor)))

    def get_pob(self):
        db = "exadata"
        sch = "data"
        self.pob_act = {}
        self.pob_tot = {}
        resis = set()
        sql = """
            SELECT resi_cod
            FROM cat_sisap_map_residencies
            WHERE sisap_class = 1
        """
        for resi, in u.getAll(sql, 'import'):
            resis.add(resi)
        sql_act = """
            WITH
                max_data AS (
                    SELECT DATE '{DEXTD}' AS max_data FROM dual
                    ),
                cip_resis AS (
                    SELECT
                        r.cip,
                        r.residencia,
                        max(r.data) AS max_resi_data
                    FROM
                        residencies_cens r
                        -- INNER JOIN residencies_cataleg rc
                        --    ON r.residencia = rc.codi
                        ,max_data m
                    WHERE
                        r.perfil = 'Resident'
                        -- AND rc.tipus = 1
                        AND r.data = m.max_data
                    GROUP BY
                        r.cip,
                        r.residencia
                    ),
                cip_last_data AS (
                    SELECT cip, max(max_resi_data) AS last_data
                    FROM cip_resis
                    GROUP BY cip
                    )
            SELECT
                cr.cip, max(cr.residencia) AS residencia
            FROM
                cip_resis cr
                INNER JOIN cip_last_data cld
                    ON cr.cip = cld.cip
                    AND cr.max_resi_data = cld.last_data
            GROUP BY
                cr.cip
        """.format(DEXTD=DEXTD)
        sql_tot = """
            WITH
                max_data AS (
                    SELECT DATE '{DEXTD}' AS max_data FROM dual
                    ),
                cip_resis AS (
                    SELECT
                        r.cip,
                        r.residencia,
                        max(r.DATA) AS max_resi_data
                    FROM
                        residencies_cens r
                        -- INNER JOIN residencies_cataleg rc
                        --    ON r.residencia = rc.CODI
                        ,max_data m
                    WHERE
                        r.perfil = 'Resident'
                        -- AND rc.tipus = 1
                        AND r.data BETWEEN add_months(m.max_data, -12)+1 AND m.max_data
                    GROUP BY
                        r.cip,
                        r.residencia
                    HAVING
                        count(*) >= 30
                    ),
                cip_last_data AS (
                    SELECT cip, max(max_resi_data) AS last_data
                    FROM cip_resis
                    GROUP BY cip
                    )
            SELECT
                cr.cip, max(cr.residencia) AS residencia
            FROM
                cip_resis cr
                INNER JOIN cip_last_data cld
                    ON cr.cip = cld.cip
                    AND cr.max_resi_data = cld.last_data
            GROUP BY
                cr.cip
        """.format(DEXTD=DEXTD)
        with t.Database(db, sch) as conn:
            for cip, resi in conn.get_all(sql_act):
                if resi in resis:
                    self.pob_act[cip] = resi
        with t.Database(db, sch) as conn:
            for cip, resi in conn.get_all(sql_tot):
                if resi in resis:
                    self.pob_tot[cip] = resi

    

if __name__=='__main__':
    try:
        Master()
        mail = t.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "indicadors residencies b� :)"
        text = ''
        mail.text = text
        mail.send()
    except Exception as e:
        text = str(e)
        mail = t.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "indicadors residencies malament :("
        mail.text = text
        mail.send()
        raise