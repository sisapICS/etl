# -*- coding: utf-8 -*-

"""
.
"""

import datetime as d
import hashlib as h
import itertools as i
import multiprocessing as m
# import requests as r
import time

# import portalics.replica as replica
import sisaptools as u


DEBUG = False
FORCE_PROD = False

START = d.date(2020, 3, 1)
resta = d.datetime.now().hour < 13
TODAY = d.datetime.now().date() - d.timedelta(days=1 * resta)
DATES = (START.strftime("%Y-%m-%d"), TODAY.strftime("%Y-%m-%d"))

PROD = d.datetime.now().hour in (21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7) or FORCE_PROD  # noqa
CONN = "prod" if PROD else "data"
SECTORS = u.constants.SECTORS_ECAP

PREFIX = "sisap_covid_"
SUFIX = "" if PROD else "_exp"

# USERS = ("PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB",
#          "PREDUEHE")

# TELEGRAM_KEY = "6UKElvDv7db4zH7yvEaSkXeLgk/poSQLGNwDvTaLE+1SYp++pQBqAy8oxkCTMqaRJ858V999cW+SGyUbq06qsCQ4TLBkZlKzSfgCczojTYc="  # noqa
# TELEGRAM_USERS = {"dadescovid": -1001280281721, "mmedina": 1461656201,
#                   "scordomi": 176458908, "ylejardi": 730976381,
#                   "ffina": 629410867, "educacio": -1001265004496,
#                   "mallopis": 975985198, "amas": 874969697,
#                   "tarcat": -1001450851901, "huvh": -477462256,
#                   "residencies": -454760142, "vacunes_prof": -440240074,
#                   "vacunes": -530044598, "universitats": -460456072,
#                   "premium": -505649049}


def get_data(sql, model="sectors", redics="data", force=False):
    """."""
    if model == "sectors":
        jobs = [(sql.format(sector), (sector, "prod" if force else CONN))
                for sector in SECTORS]
        n_workers = len(jobs)
    elif model in ("redics", "exadata"):
        jobs = []
        digits = [format(digit, "x").upper() for digit in range(16)]
        jobs = [(sql.format("".join(combo)), (model, redics))
                for combo in i.product(digits, digits)]
        n_workers = 32 if model == "redics" else 8
    if DEBUG:
        dades = [worker(jobs[0])]
    else:
        pool = m.Pool(n_workers)
        dades = pool.map(worker, jobs, chunksize=1)
        pool.close()
    for chunk in dades:
        for row in chunk:
            yield row


def worker(params):
    """."""
    sql, connexio = params
    dades = list(u.Database(*connexio, retry=1).get_all(sql))
    return dades


# def populate_table(table, cols, dades, force=False, chunk=None, intent=1, externs=None):  # noqa
#     """."""
#     temp = "sisap_{}".format(d.datetime.now().strftime("%Y%m%d%H%M%S%f"))
#     conflictius = ("res_casos", "res_poblacio", "agr_casos", "res_master",
#                    "pac_4cw", "agr_ili", "res_web_resum")
#     n = chunk if chunk else 100 if table in conflictius else 10 ** 5
#     with u.Database("redics", "data") as redics:
#         redics.create_table(temp, cols, remove=True)
#         if dades:
#             try:
#                 redics.list_to_table(dades, temp, chunk=n)
#             except Exception as e:
#                 if "03113" in str(e) and intent < 3:
#                     populate_table(table, cols, dades, force,
#                                    chunk=100, intent=intent+1)
#                     return(None)
#                 else:
#                     raise
#         create_table(table, temp, force, rename=True, externs=externs)
#         redics.drop_table(temp)


# def create_table(table, info, force=False, rename=False, externs=None):
#     """."""
#     table_name = PREFIX + table + ("" if force else SUFIX)
#     with u.Database("redics", "data") as redics:
#         redics.drop_table(table_name)
#         if rename:
#             redics.execute("rename {} to {}".format(info, table_name))
#         else:
#             redics.execute("create table {} as {}".format(table_name, info))
#         redics.set_grants("select", table_name, USERS)
#         if externs:
#             redics.set_grants("select", table_name, replica.USERS.keys())
#             sql = "create or replace synonym {}.{} for preduffa.{}"
#             for user in replica.USERS.keys():
#                 redics.execute(sql.format(user, externs, table_name))
#         if table == "web_timings":
#             redics.set_grants("all", table_name, USERS)
#         create_indexes(redics, table, table_name, force)
#         redics.set_statistics(table_name)


# def create_indexes(redics, table, table_name, force=False):
#     """."""
#     for word in ("UP", "HASH", "CIP", "PACIENT", "UP_COD"):
#         if word in redics.get_table_columns(table_name):
#             if "web_master" not in table:
#                 redics.execute("create index {0}_idx_{1}{2} \
#                                 on {3} ({1})".format(table, word, "" if force else SUFIX, table_name))  # noqa


def get_edat(inici, fi, grups=3):
    """."""
    edat = u.years_between(inici, fi)
    if grups == 3:
        grup = 0 if edat < 15 else 2 if edat > 64 else 1
    elif grups == 4:
        grup = 0 if edat < 15 else 3 if edat > 74 else 2 if edat > 64 else 1
    elif grups == 10:
        grup = min(int(10 * int(edat / 10)), 90)
    return (edat, grup)


def get_nivell(naix):
    """."""
    first = d.datetime.now().year - (d.datetime.now().month < 9) - 15
    this = naix.year
    if first <= this <= (first + 3):
        nivell = "ESO"
    elif first + 4 <= this <= first + 9:
        nivell = "PRI"
    elif first + 10 <= this <= first + 12:
        nivell = "INF2C"
    elif first + 13 <= this <= first + 15:
        nivell = "INF1C"
    else:
        nivell = None
    return nivell


def get_hash(cip):
    """."""
    if u.constants.IS_PYTHON_3:
        cip = cip.encode("latin1")
    return h.sha1(cip).hexdigest().upper()


# def send_telegram(user, text=None, document=None):
#     """."""
#     token = u.aes.AESCipher().decrypt(TELEGRAM_KEY)
#     if document:
#         url = "https://api.telegram.org/bot{}/sendDocument".format(token)
#         params = {"chat_id": TELEGRAM_USERS[user]}
#         files = {"document": open(document, "rb")}
#         response = r.get(url, params=params, files=files)
#     else:
#         url = "https://api.telegram.org/bot{}/sendMessage".format(token)
#         params = {"chat_id": TELEGRAM_USERS[user], "text": text,
#                   "parse_mode": "Markdown"}
#         response = r.get(url, params=params)  # noqa


def populate_exadata(table, cols, dades, model="upload", idx=None):
    """."""
    users = ("DWSISAP_ROL", "DWHC3", "DWBO", "DWRCDS", "DWAQUAS",
             "DWCATSALUT_ROL", "DWETL", "DWCOVID19_ROL")
    temp = "sisap_{}".format(d.datetime.now().strftime("%Y%m%d%H%M%S%f"))
    with u.Database("exadata", "data") as exadata:
        if model == "upload":
            exadata.create_table(temp, cols, remove=True)
            if dades:
                exadata.list_to_table(dades, temp, chunk=10**5)
        elif model == "create":
            exadata.drop_table(temp)
            exadata.execute("create table {} as {}".format(temp, dades))
        exadata.drop_table(table)
        exadata.execute("rename {} to {}".format(temp, table))
        exadata.set_grants("select", table, users, inheritance=False)
        if idx:
            sql = "create index {0}_{1} on {2} ({1})"
            exadata.execute(sql.format(table[:25], idx, table))
        exadata.set_statistics(table)


def populate_vision(table, cols, dades):
    """."""
    with u.Database("sidics", "covid") as covid:
        covid.create_table(table, cols, remove=True)
        covid.list_to_table(dades, table)


def proc_wait(pas, taula="web_timings"):
    """."""
    proceed = False
    while not proceed:
        sql = "select {}_status from {}{}".format(pas, PREFIX, taula)
        status, = u.Database("redics", "data").get_one(sql)
        if status == 0:
            proceed = True
        elif status is None:
            time.sleep(30)
        else:
            error = "Aborting, el procés {} ha fallat.".format(pas.upper())
            raise Exception(error)


def proc_init(pas, taula="web_timings"):
    """."""
    sql = "update {0}{1} \
           set {2}_inici = current_timestamp, \
               {2}_fi = null, \
               {2}_status = null".format(PREFIX, taula, pas)
    u.Database("redics", "data").execute(sql)


def proc_end(pas, status, taula="web_timings"):
    """."""
    sql = "update {0}{1} \
           set {2}_inici = nvl({2}_inici, current_timestamp), \
               {2}_fi = current_timestamp, \
               {2}_status = {3}".format(PREFIX, taula, pas, status)
    u.Database("redics", "data").execute(sql)


def drop_temp_tables():
    """."""
    for db in ("redics", "exadata"):
        tables = [table for table in u.Database(db, "data").get_tables()
                  if "SISAP_202" in table]
        with u.Database(db, "data") as conn:
            for table in tables:
                try:
                    conn.drop_table(table)
                except Exception as e:
                    print(db, table, str(e))


def get_pretty(num):
    """."""
    return str(round(num, 2)).replace(".", ",")
