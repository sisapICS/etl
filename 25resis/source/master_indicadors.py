# -*- coding: utf-8 -*-

import hashlib as h
import datetime as d
import collections as c
from dateutil.relativedelta import relativedelta

import sisaptools as t
import sisapUtils as u
import shared as s

DEXTD = t.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]  # noqa
DEXT = str(DEXTD)
now_1m = DEXTD - relativedelta(months=1)
now_1y = DEXTD - relativedelta(years=1)
# d_resp = c.defaultdict(c.Counter)
d_tractaments1 = c.Counter()
d_tractaments2 = c.Counter()

class Master():
    def __init__(self):
        u.printTime('inici')
        self.dades = c.defaultdict(dict)
        self.get_data()
        u.printTime('get_data')
        self.get_farmacs()
        u.printTime('get_farmacs')
        self.get_cont()
        u.printTime('get_cont')
        self.get_master()
        u.printTime('fi')

    def get_data(self):
        # pcc = ER0001
        # maca = ER0002
        GRUPS = {
            'ER0001': 'pcc',
            'ER0002': 'maca'
        }
        sql = """
            select id_cip_sec, es_cod
            from estats
            where es_cod in ('ER0001', 'ER0002')
            and es_dde <= DATE '{DEXTD}'
        """.format(DEXTD=DEXTD)
        for id, codi in u.getAll(sql, 'import'):
            self.dades[id][GRUPS[codi]] = 1
        u.printTime('estats')

        tables = u.getSubTables('xml_detall', 'import')
        sql = """
            SELECT id_cip_sec, xml_data_alta, xml_tipus_orig, camp_codi, substr(camp_valor, 1, 15)
            FROM {tb}
            WHERE xml_tipus_orig in ('VAL_SPS_AD', 'VAL_SPS_NE', 'VAL_SPS_TN')
            AND xml_data_alta between DATE '{now_1y}' and DATE '{DEXTD}'
        """
        jobs = [(table, sql) for table in tables]
        xmls = c.defaultdict(set)
        detalls = {}
        for d1, d2 in u.multiprocess(sub_get_xmls, jobs, 8, close=True):
            for id in d1:
                xmls[id] |= d1[id]
            for (id, data) in d2:
                detalls[(id, data)] = 1
        for id in xmls:
            data = max(xmls[id])
            if (id, data) in detalls:
                self.dades[id]['val_soc_alterada'] = 1
            else:
                self.dades[id]['val_soc_alterada'] = 0
        u.printTime('val_soc_alterada')
        # T4101002 fora per piic
        # piic = 'T4101001', 'T4101002', 'T4101017' no value
        # piir = 'T4101001' no value
        # piirV = 'T4101014' value
        # piirT = 'T4101015' value
        # pirda = 'T4101002', 'T4101017' no value

        # tambe es podria fer multiprocess per piic
        # de moment no cal perque tarda 1 minut nomes
        piirV = set()
        piirT = set()
        piirF = c.defaultdict(set)
        maca = set()
        c_2025 = c.defaultdict(dict)
        sql = """
            select id_cip_sec, left(mi_cod_var, 8), right(mi_cod_var, 2)
            from piic
            where left(mi_cod_var, 8) in ('T4101001', 'T4101002', 'T4101017', 'T4101014', 'T4101015')
            and mi_data_reg between adddate(DATE '{DEXTD}', interval -1 year) and DATE '{DEXTD}'
        """.format(DEXTD=DEXTD)
        for id, codi, val in u.getAll(sql, 'import'):
            if codi == 'T4101001':
                self.dades[id]['piic'] = 1
            elif codi == 'T4101002':
                maca.add(id)
            elif codi == 'T4101014':
                piirV.add((id, val))
                if val == '05':
                    c_2025['c2'][int(id)] = 1
                elif val == '07':
                    c_2025['c3'][int(id)] = 1
            elif codi == 'T4101015':
                piirT.add((id, val))
            else:
                piirF[id].add(val)
            # if codi in ('T4101001', 'T4101002', 'T4101017'):
            #     self.dades[id]['piic'] = 1
            #     if codi == 'T4101001':
            #         self.dades[id]['piir'] = 1
            #     else:
            #         self.dades[id]['pirda'] = 1
            # elif codi == 'T4101014':
            #     self.dades[id]['piirV'] = val
            # else: # T4101015
            #     self.dades[id]['piirT'] = val
        for (id, val) in piirV:
            if (id, val) in piirT:
                self.dades[id]['piic'] = 1
        for id in piirF:
            if len(piirF[id]) == 4:
                self.dades[id]['piic'] = 1
            if len(piirF[id]) >= 3:
                if '05' in piirF[id] and '06' in piirF[id] and '07' in piirF[id]:
                    c_2025['c1'][id] = 1
        for id in c_2025['c1']:
            if id in c_2025['c2'] and id in c_2025['c3']:
                self.dades[id]['piic'] = 1
        for id in self.dades:
            if 'piic' in self.dades[id] and 'maca' in self.dades[id] and id not in maca:
                self.dades[id].pop('piic')
        u.printTime('piic')

        GRUPS = {
            454: 'urinaria',
            545: 'maltracte',
            916: 'maltracte',
            77: 'osteoporosi',
            86: 'demencia',
            792: 'depre',
            812: 'depre',
            1: 'ci',
            211: 'ci',
            7:'avc',
            212: 'avc',
            11: 'claud_int',
            213: 'claud_int',
            53: 'irc',
            160: 'insuf_hepat',
            783: 'parla'
        }
        sql = """
            SELECT id_cip_sec, ps
            FROM eqa_problemes
            WHERE ps in {codis}
            and dde <= DATE '{DEXTD}'
        """.format(codis=tuple(list(GRUPS.keys())), DEXTD=DEXTD)
        for id, codi in u.getAll(sql, 'nodrizas'):
            self.dades[id][GRUPS[codi]] = 1
        u.printTime('eqa_problemes')

        # antiosteop = 80
        sql = """
            SELECT id_cip_sec
            FROM eqa_tractaments
            WHERE farmac in (80)
            and DATE '{DEXTD}' between pres_orig and data_fi
        """.format(DEXTD=DEXTD)
        for id, in u.getAll(sql, 'nodrizas'):
            self.dades[id]['antiosteop'] = 1
        u.printTime('eqa_tractaments')
        dmo = {
            476: 'DMO',
            497: 'DMO'
        }
        GRUPS = {
            89: 'socialrisk',
            918: 'fragilitat',
            88: 'pfeifer_frag',
            999: 'mna_frag',
            751: 'caigudes_frag',
            940: 'caigudes_frag',
            1079: 'barthel_frag'
        }
        sql = """
            select id_cip_sec, agrupador
            from eqa_variables
            where (agrupador in {codis}
            and data_var between DATE '{now_1y}' and DATE '{DEXTD}')
            or (agrupador in {dmo} and data_var <= DATE '{DEXTD}')
        """.format(codis=tuple(GRUPS.keys()), dmo=tuple(dmo.keys()),now_1y=now_1y, DEXTD=DEXTD)
        for id, codi in u.getAll(sql, 'nodrizas'):
            if codi in GRUPS:
                self.dades[id][GRUPS[codi]] = 1
            elif codi in dmo:
                self.dades[id][dmo[codi]] = 1
        u.printTime('eqa_variables')

        GRUPS = {
            '7192': 'gds-fast',
            '6039': 'npi-q',
        }
        # no cal afegir filtre dels ultims 2 anys quan variables2 nomes conte ultims 2 anys
        # and vu_dat_act between adddate(DATE '{DEXTD}', interval -2 year) and DATE '{DEXTD}'
        sql = """
            SELECT id_cip_sec, vu_cod_vs, vu_val
            FROM variables2
            WHERE vu_cod_vs in ('7192', '6039')
        """
        for id, codi, val in u.getAll(sql, 'import'):
            self.dades[id][GRUPS[codi]] = 1
            if codi == '7192' and val >= 12:
                self.dades[id]['gds7b'] = 1
        u.printTime('variables2')

        # edat
        # tambe es pot fer multiprocess de assignada
        # de moment no es fa multiprocess perque nomes tarda 1:30 mins
        sql = """
            SELECT
                id_cip_sec,
                TIMESTAMPDIFF(YEAR, usua_data_naixement, DATE '{DEXTD}') AS val
            FROM assignada
        """.format(DEXTD=DEXTD)
        for id, edat in u.getAll(sql, 'import'):
            self.dades[id]['edat'] = edat
        u.printTime('assignada')

        # caigudes, registre_caig, imc, peso, barthel, pfeifer, braden
        codis = {
            'ABVDB': 'barthel',
            'EDU20': 'barthel',
            'BRADE': 'braden',
            'ICSVR': 'braden',
            'BRADE2': 'braden',
            'TT103': 'imc',
            'TT102': 'peso',
            'PES': 'peso',
            'PES2': 'peso',
            'PES3': 'peso',
            'PES4': 'peso',
            'VMTPF': 'pfeifer',
            'EDU21': 'pfeifer',
            'VMDLC': 'lobo',
            'VMDLO': 'lobo',
            'N': 'caigudes',
            'Y00002': 'caigudes',
            'N02': 'caigudes',
            'N09': 'caigudes',
            'N14': 'caigudes',
            'N32': 'caigudes',
            'NCAIG': 'caigudes',
            'N4': 'caigudes',
            'N7': 'caigudes',
            'N04': 'registre_caig',
            'TUGT': 'registre_caig',
            'XL1003': 'registre_caig',
            'VT4001': 'mna',
            'VT4002': 'mna',
            'VMEDG': 'emocional',
            'EP5105': 'emocional',
            'TIRS': 'social',
            'VZ3005': 'social',
            'VA030I':'fragilitat'
        }
        # llegir de eqa_criteris

        # no cal afegir filtre del ultim any quan variables1 nomes conte ultims any
        #  AND vu_dat_act between adddate(DATE '{DEXTD}', interval -1 year) and DATE '{DEXTD}'
        sql = """
            SELECT
                id_cip_sec, vu_cod_vs, vu_dat_act, vu_val
            FROM
                variables2
            WHERE
                vu_cod_vs in {keys} AND
                    vu_dat_act BETWEEN
                        adddate(DATE '{DEXTD}', interval -1 year) AND
                        DATE '{DEXTD}'
            """.format(keys=tuple(codis.keys()), DEXTD=DEXTD)
        d_im = c.defaultdict(lambda: c.defaultdict(dict))
        # construim diccionari intermig perque hem d'agafar maxim valor o maxima data
        for id, var, data, val in u.getAll(sql, 'import'):
            var = codis[var]
            if var in ('registre_caig', 'braden', 'mna'):
                self.dades[id][var] = 1
            elif val >= 0:
                d_im[var][id][data] = val
        u.printTime('variables2')

        # REGISTRE DE CAIGUDES
        sql = """
            select id_cip_sec, au_dat_act
            from activitats1 
            where au_cod_ac in ('00584', '00585', '02010',
                '6490', 'AC', 'AC-00', 'AC-01', 'AC-14',
                'AC.02', 'AC.03')
        """
        for id, dat in u.getAll(sql, 'import'):
            self.dades[id]['registre_caig'] = 1
        u.printTime('activitats1')

        # es pot fer multiprocess de ares (no cal, tarda 10 segons)
        sql = """
            select id_cip_sec, pi_data_inici
            from ares
            where pi_data_fi > sysdate()
            and pi_codi_pc = 'PC0103'
        """
        for id, dat in u.getAll(sql, 'import'):
            self.dades[id]['registre_caig'] = 1
        u.printTime('ares')

        # processament tarda 2:30 mins
        for var, cips in d_im.items():
            for cip, dats in cips.items():
                dat = max(dats)
                val = dats[dat]
                self.dades[cip][var] = val
                if var == 'peso':
                    self.dades[cip]['max_pes'] = max(dats.values())
        u.printTime('max_pes')

        tables = u.getSubTables('tractaments', 'import')
        sql_1 = """
            SELECT id_cip_sec
            FROM {tb} t
            WHERE ppfmc_data_fi > DATE '{DEXTD}'
            AND (ppfmc_durada > 360 OR ppfmc_seg_evol = 'S')
            AND length(ppfmc_atccodi) = 7
            AND NOT EXISTS
                (SELECT 1
                FROM cat_cpftb006 c
                WHERE
                c.pf_codi = t.ppfmc_pf_codi
                AND (c.pf_ff_codi in ('GE','PO','CL')
                OR c.pf_via_adm = 'B31'
                OR c.pf_gt_codi like '23C%'))
        """
        sql_2 = """
            SELECT DISTINCT id_cip_sec, ppfmc_atccodi
            FROM {tb}
            WHERE DATE '{DEXTD}' between ppfmc_pmc_data_ini
            and ppfmc_data_fi
            and ppfmc_atccodi in {atcs}
        """
        psicofarm = set(
            ['N03AG01', 'N05CM02', 'N05AX08', 'N04BA02',
                'N06BA09', 'N06AA09', 'N05BA06', 'N05AH06',
                'N05AH05', 'N05AH04', 'N05AH03', 'N05AH02',
                'N05BA12', 'N07BB04', 'N03AX09', 'N07BB01',
                'N05BA10', 'N05AG02', 'N05BA05', 'N05AN01',
                'N06AX05', 'N05BA01', 'N03AE01', 'N07BC02',
                'N02AB03', 'N02AA01', 'N04AA02', 'N06AB04',
                'N05AX12', 'N05AX13', 'N05AD01', 'N06AB05',
                'N06BA04', 'N05BA09', 'N05BA08', 'N05AL03',
                'N05AF05', 'N05AL01', 'N05BA02', 'N05AE04',
                'N06AB06', 'N07BC51', 'N03AF01', 'N05CD06',
                'N05CD01', 'N06AB03', 'N05AB03', 'N05AB02',
                'N03AX12', 'N06AA04', 'N03AX11', 'N03AX16',
                'N05AC01', 'N03AX14', 'N06AX11', 'N06AX16',
                'N05AA02', 'N03AA02', 'N05AA01']
        )
        
        # 6 minuts executant-se
        jobs = [(table, sql_1, sql_2, psicofarm) for table in tables]
        for d_tractaments1, d_tractaments2 in u.multiprocess(sub_get_tractaments, jobs, 8, close=True):
            for id, val in d_tractaments1.items():
                if val > 8:
                    self.dades[id]['polif'] = val
            for id, val in d_tractaments2.items():
                self.dades[id]['psicof'] = val
        u.printTime('tractaments polif/psicof')
        # tarda 2 mins
        sql = """
            SELECT id_cip_sec,
            case left(visi_col_prov_resp, 1)
            WHEN  1 THEN 'MED'
            WHEN 3 THEN 'INF'
            WHEN 4 THEN 'TS' end as var,
            visi_data_visita,
            visi_tipus_visita,
            visi_lloc_visita,
            s_espe_codi_especialitat
            FROM import.{tb}
            WHERE visi_situacio_visita = 'R'
            -- AND s_espe_codi_especialitat not in ('EXTRA','10102')
            AND left(visi_col_prov_resp, 1) in ('1', '3', '4')
        """
        tables = u.getSubTables('visites1', 'import')
        jobs = [(table, sql) for table in tables]
        for chunk in u.multiprocess(sub_get_responsables, jobs, 8, close=True):
            for cip, vars in chunk.items():
                for var, val in vars.items():
                    if val >= 0:
                        if var in self.dades[cip]:
                            self.dades[cip][var] += val
                        else:
                            self.dades[cip][var] = val
        u.printTime('proc1 visites1')

        # tarda 2:30 mins
        tables = u.getSubTables('problemes','import')
        sql = """
            SELECT id_cip_sec
            FROM {tb}
            WHERE pr_cod_ps in ('F51.0', 'G47.0',
                'C01-G47.0', 'C01-F51.09',
                'C01-G47.00', 'C01-F51.0')
            AND pr_cod_o_ps = 'C'
            AND pr_hist = 1
            AND pr_data_baixa = 0
        """
        jobs = [(table, sql) for table in tables]
        d = u.multiprocess(sub_get_insomni, jobs, 8, close=True)
        for chunk in d:
            for id in chunk:
                self.dades[id]['insomni'] = 1
        u.printTime('problemes')

        sql = """
            SELECT id_cip_sec
            FROM mst_indicadors_pacient_placures
            WHERE indicador = 'PLACU012'
            AND compleix = 1
        """
        for id, in u.getAll(sql, 'altres'):
            self.dades[id]['placures'] = 1
        u.printTime('placures')

    def get_farmacs(self):
        antipsico = set(['N05AF05', 'N05AB03', 'N05AB02', 'N05AN01', 'N05AH03',
                         'N05AG02', 'N05AX08', 'N05AC01', 'N05AX12', 'N05AX13',
                         'N05AL01', 'N05AD01', 'N05AL03', 'N05AH06', 'N05AH05',
                         'N05AH04', 'N05AA02', 'N05AH02', 'N05AE04', 'N05AA01']
                        )

        benzo = set(['N05CD06', 'N05CD01', 'N05CD08', 'N05BA06', 'N05BA12',
                     'N05BA09', 'N05BA08', 'N03AE01', 'N05BA05', 'N05BA10',
                     'N05BA02', 'N05BA01'])

        iace = set(['N06DA03', 'N06DA02', 'N06DA04'])
        memantina = set(['N06DX01'])
        iace_memantina = iace.union(memantina)

        antidepressius = set(['N06AA02', 'N06AA04', 'N06AA06', 'N06AA07',
                             'N06AA09', 'N06AA10', 'N06AA12', 'N06AA16',
                             'N06AA17', 'N06AA19', 'N06AA21', 'N06AB03',
                             'N06AB04', 'N06AB05', 'N06AB06', 'N06AB08',
                             'N06AB10', 'N06AF04', 'N06AG02', 'N06AX01',
                             'N06AX03', 'N06AX05', 'N06AX06', 'N06AX07',
                             'N06AX11', 'N06AX12', 'N06AX14', 'N06AX16',
                             'N06AX18', 'N06AX21', 'N06AX22', 'N06AX23',
                             'N06AX26'])
        ansio_hipnotics = set(['N05BA01', 'N05BA05', 'N05BA06', 'N05BA12',
                               'N05CD06'])
        estatines = set(['C10AA01', 'C10AA02', 'C10AA03', 'C10AA04', 'C10AA05',
                         'C10AA06', 'C10AA07', 'C10AA08', 'C10AX09', 'C10BA02',
                         'C10BA03', 'C10BX03', 'C10BX06'])
        aines = set(['M01AA01', 'M01AB01', 'M01AB05', 'M01AB16', 'M01AB55',
                     'M01AC01', 'M01AC02', 'M01AC05', 'M01AC06', 'M01AE01',
                     'M01AE02', 'M01AE03', 'M01AE09', 'M01AE14', 'M01AE17',
                     'M01AE52', 'M01AG01', 'M01AH01', 'M01AH05', 'N02BA01',
                     'N02BG91'])
        
        total = antipsico.union(benzo, iace_memantina, antidepressius, ansio_hipnotics, estatines, aines)
        sql = """
            select id_cip_sec, ppfmc_atccodi, ppfmc_pmc_data_ini
            from {tb}
            where ppfmc_atccodi in {codis}
            and DATE '{DEXTD}' between ppfmc_pmc_data_ini
            and ppfmc_data_fi
        """
        tables = u.getSubTables('tractaments', 'import')
        jobs = [(table, sql, total, antipsico, benzo, iace_memantina, antidepressius, ansio_hipnotics, estatines, aines) for table in tables]
        d = u.multiprocess(sub_get_farmacs, jobs, 8, close=True)
        for chunk in d:
            for id, codi, data in chunk:
                self.dades[id][codi] = data
                if codi in ('antidepressius', 'ansio_hipnotics'):
                    if codi+'_2' in self.dades[id]:
                        self.dades[id][codi+'_2'] += 1
                    else:
                        self.dades[id][codi+'_2'] = 1

        u.printTime('tractaments')

    def get_cont(self):
        sql = """
            SELECT id_cip_sec
            FROM assignada_tot
            WHERE edat > 14
            AND (maca = 1 or pcc = 1)
        """
        for id, in u.getAll(sql, 'nodrizas'):
            self.dades[id]['pcc_maca'] = 1
        u.printTime('pcc_maca')
        sql = """
            SELECT id_cip_sec, servei, r2
            FROM mst_long_cont_pacient
            WHERE servei in ('MG', 'INF')
        """
        for id, servei, r2 in u.getAll(sql, 'altres'):
            if servei in self.dades[id]:
                self.dades[id][servei] += r2
            else:
                self.dades[id][servei] = r2
            # self.dades[id][servei] = servei
            # self.dades[id]['r2'] = r2
        u.printTime('servei + r2')

    def get_master(self):
        cols = """(
            id_cip_sec int,
            antipsico date,
            benzo date,
            iace_memantina date,
            antidepressius date,
            antidepressius_2 int,
            ansio_hipnotics date,
            ansio_hipnotics_2 int,
            estatines date,
            aines date,
            pcc int,
            maca int,
            piic int,
            urinaria int,
            maltracte int,
            osteoporosi int,
            demencia int,
            depre int,
            ci int,
            avc int,
            claud_int int,
            irc int,
            insuf_hepat int,
            antiosteop int,
            DMO int,
            gds_fast int,
            npi_q int,
            gds7b int,
            edat int,
            registre_caig int,
            braden int,
            mna int,
            barthel double,
            imc double,
            pes double,
            max_pes double,
            pfeifer double,
            caigudes int,
            polif double,
            psicof double,
            MEDyear int,
            MEDmonth int,
            INFyear int,
            INFmonth int,
            TSyear int,
            TSmonth int,
            T_MEDyear int,
            T_INFyear int,
            P_MEDyear int,
            P_INFyear int,
            D_MEDyear int,
            D_MEDmonth int,
            D_INFyear int,
            D_INFmonth int,
            D_TSyear int,
            D_TSmonth int,
            insomni int,
            pcc_maca int,
            servei_mg double,
            servei_inf double,
            parla int,
            val_soc_alterada int,
            placures int,
            socialrisk int,
            fragilitat int,
            barthel_frag int,
            pfeifer_frag int,
            mna_frag int,
            caigudes_frag int,
            emocional int,
            social int
        )"""
        u.createTable('master_residencies', cols, 'resis', rm=True)
        upload = []
        for id in self.dades:
            antipsico = self.dades[id]['antipsico'] if 'antipsico' in self.dades[id] else None
            benzo = self.dades[id]['benzo'] if 'benzo' in self.dades[id] else None
            iace_memantina = self.dades[id]['iace_memantina'] if 'iace_memantina' in self.dades[id] else None
            antidepressius = self.dades[id]['antidepressius'] if 'antidepressius' in self.dades[id] else None
            antidepressius_2 = self.dades[id]['antidepressius_2'] if 'antidepressius_2' in self.dades[id] else None
            ansio_hipnotics = self.dades[id]['ansio_hipnotics'] if 'ansio_hipnotics' in self.dades[id] else None
            ansio_hipnotics_2 = self.dades[id]['ansio_hipnotics_2'] if 'ansio_hipnotics_2' in self.dades[id] else None
            estatines = self.dades[id]['estatines'] if 'estatines' in self.dades[id] else None
            aines = self.dades[id]['aines'] if 'aines' in self.dades[id] else None
            pcc = 1 if 'pcc' in self.dades[id] else 0
            maca = 1 if 'maca' in self.dades[id] else 0
            piic = 1 if 'piic' in self.dades[id] else 0
            # piir = 1 if 'piir' in self.dades[id] else 0
            # pirda = 1 if 'pirda' in self.dades[id] else 0
            # piirV = 1 if 'piirV' in self.dades[id] else 0
            # piirT = 1 if 'piirT' in self.dades[id] else 0
            urinaria = 1 if 'urinaria' in self.dades[id] else 0
            maltracte = 1 if 'maltracte' in self.dades[id] else 0
            osteoporosi = 1 if 'osteoporosi' in self.dades[id] else 0
            demencia = 1 if 'demencia' in self.dades[id] else 0
            depre = 1 if 'depre' in self.dades[id] else 0
            ci = 1 if 'ci' in self.dades[id] else 0
            avc = 1 if 'avc' in self.dades[id] else 0
            claud_int = 1 if 'claud_int' in self.dades[id] else 0
            irc = 1 if 'irc' in self.dades[id] else 0
            insuf_hepat = 1 if 'insuf_hepat' in self.dades[id] else 0
            antiosteop = 1 if 'antiosteop' in self.dades[id] else 0
            DMO = 1 if 'DMO' in self.dades[id] else 0
            gds_fast = 1 if 'gds-fast' in self.dades[id] else 0
            npi_q = 1 if 'npi-q' in self.dades[id] else 0
            gds7b = 1 if 'gds7b' in self.dades[id] else 0
            edat = self.dades[id]['edat'] if 'edat' in self.dades[id] else None
            registre_caig = 1 if 'registre_caig' in self.dades[id] else 0
            braden = 1 if 'braden' in self.dades[id] else 0
            mna = 1 if 'mna' in self.dades[id] else 0
            barthel = self.dades[id]['barthel'] if 'barthel' in self.dades[id] else -1
            imc = self.dades[id]['imc'] if 'imc' in self.dades[id] else None
            pes = self.dades[id]['peso'] if 'peso' in self.dades[id] else None
            max_pes = self.dades[id]['max_pes'] if 'max_pes' in self.dades[id] else None
            pfeifer = self.dades[id]['pfeifer'] if 'pfeifer' in self.dades[id] else -1
            caigudes = self.dades[id]['caigudes'] if 'caigudes' in self.dades[id] else -1
            polif = self.dades[id]['polif'] if 'polif' in self.dades[id] else None
            psicof = self.dades[id]['psicof'] if 'psicof' in self.dades[id] else None
            MEDyear = self.dades[id]['MEDyear'] if 'MEDyear' in self.dades[id] else None
            MEDmonth = self.dades[id]['MEDmonth'] if 'MEDmonth' in self.dades[id] else None
            INFyear = self.dades[id]['INFyear'] if 'INFyear' in self.dades[id] else None
            INFmonth = self.dades[id]['INFmonth'] if 'INFmonth' in self.dades[id] else None
            TSyear = self.dades[id]['TSyear'] if 'TSyear' in self.dades[id] else None
            TSmonth = self.dades[id]['TSmonth'] if 'TSmonth' in self.dades[id] else None
            T_MEDyear = self.dades[id]['T_MEDyear'] if 'T_MEDyear' in self.dades[id] else None
            T_INFyear = self.dades[id]['T_INFyear'] if 'T_INFyear' in self.dades[id] else None
            P_MEDyear = self.dades[id]['P_MEDyear'] if 'P_MEDyear' in self.dades[id] else None
            P_INFyear = self.dades[id]['P_INFyear'] if 'P_INFyear' in self.dades[id] else None
            D_MEDyear = self.dades[id]['D_MEDyear'] if 'D_MEDyear' in self.dades[id] else None
            D_MEDmonth = self.dades[id]['D_MEDmonth'] if 'D_MEDmonth' in self.dades[id] else None
            D_INFyear = self.dades[id]['D_INFyear'] if 'D_INFyear' in self.dades[id] else None
            D_INFmonth = self.dades[id]['D_INFmonth'] if 'D_INFmonth' in self.dades[id] else None
            D_TSyear = self.dades[id]['D_TSyear'] if 'D_TSyear' in self.dades[id] else None
            D_TSmonth = self.dades[id]['D_TSmonth'] if 'D_TSmonth' in self.dades[id] else None
            insomni = 1 if 'insomni' in self.dades[id] else 0
            pcc_maca = 1 if 'pcc_maca' in self.dades[id] else 0
            servei_mg = self.dades[id]['MG'] if 'MG' in self.dades[id] else None
            servei_inf = self.dades[id]['INF'] if 'INF' in self.dades[id] else None
            parla = self.dades[id]['parla'] if 'parla' in self.dades[id] else None
            socialrisk = self.dades[id]['socialrisk'] if 'socialrisk' in self.dades[id] else None
            fragilitat = self.dades[id]['fragilitat'] if 'fragilitat' in self.dades[id] else None
            val_soc_alterada = self.dades[id]['val_soc_alterada'] if 'val_soc_alterada' in self.dades[id] else None
            placures = self.dades[id]['placures'] if 'placures' in self.dades[id] else None
            barthel_frag = self.dades[id]['barthel_frag'] if 'barthel_frag' in self.dades[id] else None
            pfeifer_frag = self.dades[id]['pfeifer_frag'] if 'pfeifer_frag' in self.dades[id] else None
            mna_frag = self.dades[id]['mna_frag'] if 'mna_frag' in self.dades[id] else None
            caigudes_frag = self.dades[id]['caigudes_frag'] if 'caigudes_frag' in self.dades[id] else None
            emocional = self.dades[id]['emocional'] if 'emocional' in self.dades[id] else None
            social = self.dades[id]['social'] if 'social' in self.dades[id] else None
            upload.append((id, antipsico, benzo, iace_memantina, antidepressius, antidepressius_2, ansio_hipnotics, ansio_hipnotics_2, estatines, aines, pcc, maca, piic, urinaria, maltracte, osteoporosi, demencia, depre, ci, avc, claud_int, irc, insuf_hepat, antiosteop, DMO, gds_fast, npi_q, gds7b, edat, registre_caig, braden, mna, barthel, imc, pes, max_pes, pfeifer, caigudes, polif, psicof, MEDyear, MEDmonth, INFyear, INFmonth, TSyear, TSmonth, T_MEDyear, T_INFyear, P_MEDyear, P_INFyear, D_MEDyear, D_MEDmonth, D_INFyear, D_INFmonth, D_TSyear, D_TSmonth, insomni, pcc_maca, servei_mg, servei_inf, parla, val_soc_alterada, placures, socialrisk, fragilitat, barthel_frag, pfeifer_frag, mna_frag, caigudes_frag, emocional, social))
        u.listToTable(upload, 'master_residencies', 'resis')
        # REVISAR ANTIDEPRESSIUS

def sub_get_xmls(params):
    tb, sql = params
    dades = c.defaultdict(set)
    detalls = c.defaultdict()
    for id, data, tipus, camp, valor in u.getAll(sql.format(tb=tb, now_1y=now_1y, DEXTD=DEXTD), 'import'):
        dades[id].add(data)
        if camp in ('Xarxa_Result', 'Habitatge_Result', 'Economia_Result', 'Capacitat_Result') and valor == 'Valorat amb alt':
            detalls[(id, data)] = 1
    return (dades, detalls)

def sub_get_farmacs(params):
    d = set()
    table, sql, total, antipsico, benzo, iace_memantina, antidepressius, ansio_hipnotics, estatines, aines = params
    for id, codi, data in u.getAll(sql.format(tb=table, DEXTD=DEXTD, codis=tuple(list(total))), 'import'):
        if codi in antipsico:
            if u.daysBetween(data, DEXTD) >= 90:
                d.add((id, 'antipsico', data))
        if codi in benzo:
            d.add((id, 'benzo', data))
        if codi in iace_memantina:
            d.add((id, 'iace_memantina', data))
        if codi in antidepressius:
            d.add((id, 'antidepressius', data))
        if codi in ansio_hipnotics:
            d.add((id, 'ansio_hipnotics', data))
        if codi in estatines:
            d.add((id, 'estatines', data))
        if codi in aines:
            d.add((id, 'aines', data))
    u.printTime(str(table))
    return d

def sub_get_tractaments(params):
    table, sql1, sql2, atcs = params
    d_tractaments1 = c.Counter()
    d_tractaments2 = c.Counter()
    for id, in u.getAll(sql1.format(tb=table, DEXTD=DEXTD), 'import'):
        d_tractaments1[id] += 1
    for id, codi in u.getAll(sql2.format(tb=table, atcs=tuple(atcs), DEXTD=DEXTD), 'import'):
        d_tractaments2[id] += 1
    u.printTime(str(table))
    return (d_tractaments1, d_tractaments2)

def sub_get_insomni(params):
    table, sql = params
    d = set()
    for id, in u.getAll(sql.format(tb=table), 'import'):
        d.add(id)
    u.printTime(str(table))
    return d

def sub_get_responsables(params):
    table, sql = params
    d_resp = c.defaultdict(c.Counter)
    for cip, var, data, tipus, lloc, esp in u.getAll(sql.format(tb=table), 'import'):
        if data >= now_1y and data <= DEXTD:
            if tipus not in ('9E', 'EXTRA', 'EXT') and esp not in ('EXTRA', '10102'):
                d_resp[cip][var+'year'] += 1
                if data >= now_1m:
                    d_resp[cip][var+'month'] += 1
            if tipus == '9T' and esp not in ('EXTRA', '10102'):
                d_resp[cip]['T_'+var+'year'] += 1
            if (tipus in ('9D', '9R', '9C') or (lloc == 'C' and tipus not in ('9T', '9E'))) and esp not in ('EXTRA', '10102'):
                d_resp[cip]['P_'+var+'year'] += 1
            if tipus == '9D' or lloc == 'D':
                d_resp[cip]['D_'+var+'year'] += 1
                if data >= now_1m:
                    d_resp[cip]['D_'+var+'month'] += 1
    u.printTime(str(table))
    return d_resp

if __name__=='__main__':
    try:
        Master()
        mail = t.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "master residencies b� :)"
        text = ''
        mail.text = text
        mail.send()
    except Exception as e:
        text = str(e)
        mail = t.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "master residencies malament :("
        mail.text = text
        mail.send()
        raise
    