# -*- coding: utf-8 -*-

# Indicadors resid�ncies i hospitalitzacions

import collections as c
import sisapUtils as u
import sisaptools as t
import datetime as d

from dateutil.relativedelta import relativedelta

DEXTD = t.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]
# data de fa exactament 1 any
# DEXTD = d.date(2023,5,31) 
LAST12 = DEXTD + relativedelta(years=-1)

class Indicadors():
    def __init__(self):
        self.get_converters()
        print('converters')
        self.get_institucionalitzats()
        print('get_institucionalitzats')
        self.get_hospitalitzats_exitus()
        print('get_hospitalitzats_exitus')
        self.get_ingres_intermedia()
        print('get_ingres_intermedia')
        self.get_visites_urgencies()
        print('get_visites_urgencies')
        self.get_indicadors()
        print('get_indicadors')
        self.uploading()
        print('done')

    def get_ingres_intermedia(self):
        self.dates_intermedia = c.defaultdict(set)
        sql = """
            SELECT nia, dies_est, d_ingres, d_alta
            FROM dwcatsalut.cmbd_ss
            WHERE d_ingres >= DATE '{LAST12}'
        """.format(LAST12=LAST12)
        self.intermedia = c.Counter()
        self.ingres_intermedia = c.Counter()
        for nia, dies, data, fi in u.getAll(sql, 'exadata'):
            if nia is not None and int(nia) in self.nia_2_cip:
                cip = self.nia_2_cip[int(nia)]
                if cip in self.residents_tot or cip in self.residents_act:
                    self.intermedia[cip] += dies
                    self.ingres_intermedia[cip] += 1
                    self.dates_intermedia[cip].add((data, fi))

    def get_converters(self):
        self.nia_2_cip = {}
        self.hash_c_2_cip = {}
        self.exitus_generals = set()
        id_cip_hash = {}
        self.id_cip = {}
        sql = """
            SELECT id_cip_sec, hash_d
            FROM u11
        """
        for id, hash_r in u.getAll(sql, 'import'):
            id_cip_hash[hash_r] = id
        
        sql = """
            SELECT hash_redics, hash_covid
            FROM dwsisap.pdptb101_relacio
        """
        hashos = {}
        for redics, covid in u.getAll(sql, 'exadata'):
            try:
                hashos[covid] = id_cip_hash[redics]
            except:
                pass

        sql = """SELECT CIP, HASH, NIA, SITUACIO FROM dwsisap.RCA_CIP_NIA 
                WHERE situacio = 'A' OR 
                (situacio = 'D' AND DATA_DEFUNCIO > date '{LAST12}')""".format(LAST12=LAST12)
        for cip, hash, nia, situacio in u.getAll(sql, 'exadata'):
            cip = cip[:13]
            self.nia_2_cip[nia] = cip
            self.hash_c_2_cip[hash] = cip
            if hash in hashos:
                self.id_cip[cip] = hashos[hash]
            if situacio == 'D':
                self.exitus_generals.add(cip)
        
    
    def get_institucionalitzats(self):
        # prenc la poblaci� de resis de la mateixa
        # manera que es fa al qc de resis
        resis = set()
        sql = """
            SELECT resi_cod
            FROM cat_sisap_map_residencies
            WHERE sisap_class = 1
        """
        for resi, in u.getAll(sql, 'import'):
            resis.add(resi)

        sql = """WITH
                max_data AS (
                    SELECT DATE '{DEXTD}' AS max_data FROM dual
                    -- SELECT max(data) AS max_data
                    -- FROM residencies_cens
                    -- WHERE perfil = 'Resident'
                    ),
                cip_resis AS (
                    SELECT
                        r.cip,
                        r.residencia,
                        max(r.DATA) AS max_resi_data
                    FROM
                        residencies_cens r
                        -- INNER JOIN residencies_cataleg rc
                        --    ON r.residencia = rc.CODI
                        ,max_data m
                    WHERE
                        r.perfil = 'Resident'
                        -- AND rc.tipus = 1
                        AND r.data BETWEEN add_months(m.max_data, -12)+1 AND m.max_data
                    GROUP BY
                        r.cip,
                        r.residencia
                    HAVING
                        count(*) >= 30
                    ),
                cip_last_data AS (
                    SELECT cip, max(max_resi_data) AS last_data
                    FROM cip_resis
                    GROUP BY cip
                    )
            SELECT
                cr.cip, max(cr.residencia) AS residencia
            FROM
                cip_resis cr
                INNER JOIN cip_last_data cld
                    ON cr.cip = cld.cip
                    AND cr.max_resi_data = cld.last_data
            GROUP BY
                cr.cip""".format(DEXTD=DEXTD)
        self.residents_tot = {}
        self.residents_act = {}
        for cip, residencia in u.getAll(sql, 'exadata'):
            if residencia in resis:
                cip = cip[:13]
                self.residents_tot[cip] = residencia
        
        sql = """
            WITH
                max_data AS (
                    SELECT DATE '{DEXTD}' AS max_data FROM dual
                    ),
                cip_resis AS (
                    SELECT
                        r.cip,
                        r.residencia,
                        max(r.data) AS max_resi_data
                    FROM
                        residencies_cens r
                        -- INNER JOIN residencies_cataleg rc
                        --    ON r.residencia = rc.codi
                        ,max_data m
                    WHERE
                        r.perfil = 'Resident'
                        -- AND rc.tipus = 1
                        AND r.data = m.max_data
                    GROUP BY
                        r.cip,
                        r.residencia
                    ),
                cip_last_data AS (
                    SELECT cip, max(max_resi_data) AS last_data
                    FROM cip_resis
                    GROUP BY cip
                    )
            SELECT
                cr.cip, max(cr.residencia) AS residencia
            FROM
                cip_resis cr
                INNER JOIN cip_last_data cld
                    ON cr.cip = cld.cip
                    AND cr.max_resi_data = cld.last_data
            GROUP BY
                cr.cip
        """.format(DEXTD=DEXTD)
        for cip, residencia in u.getAll(sql, 'exadata'):
            if residencia in resis:
                cip = cip[:13]
                self.residents_act[cip] = residencia


    def get_hospitalitzats_exitus(self):
        self.visitats_urgencies_hospital = set()
        # agafo els pacients que han estat hospitalitzats
        # els dies que han estat ingressat i si han sigut �xitus o no
        sql = """SELECT nia,
                data_alta - data_ingres AS dies_hosp,
                CASE WHEN C_ALTA = '6' THEN 1 ELSE 0 END exitus,
                CASE WHEN t_act = '134' AND c_alta = '1' THEN 1 ELSE 0 END urg
                FROM dwcatsalut.tf_cmbdha
                WHERE nia IS NOT NULL
                AND data_alta > DATE '{LAST12}'
                AND ser_alta <> '40101'
                AND pr_ingres <> '7'""".format(LAST12=LAST12)
        self.dies_hospitalitzats = c.Counter()
        self.exitus_hosp = set()
        self.ingressos_hosp = c.Counter()
        for nia, dies_hosp, exitus, urg in u.getAll(sql, 'exadata'):
            if nia in self.nia_2_cip:
                cip = self.nia_2_cip[nia]
                if urg:
                    if cip in self.residents_tot or cip in self.residents_act:
                        self.visitats_urgencies_hospital.add(cip)
                else:
                    if cip in self.residents_tot or cip in self.residents_act:
                        self.dies_hospitalitzats[cip] += dies_hosp
                        self.ingressos_hosp[cip] += 1
                        if exitus == 1:
                            self.exitus_hosp.add(cip)
                            self.exitus_generals.add(cip)

    def get_visites_urgencies(self):
        #self.visitats_urgencies = set()
        self.urg_intermedia = c.defaultdict(set)
        self.visitats_urgencies_altres = set()
        # prenc els que s'han visitat a un CUAP en els �ltims 12 mesos
        sql = """SELECT pacient, data FROM dwsisap.sisap_master_visites
                WHERE data > DATE '{LAST12}'
                AND up IN (SELECT up_cod FROM dwsisap.dbc_rup 
                WHERE subtip_cod = '24')""".format(LAST12=LAST12)
        print(sql)
        for hash_c, data in u.getAll(sql, 'exadata'):
            if hash_c in self.hash_c_2_cip:
                cip = self.hash_c_2_cip[hash_c]
                if cip in self.residents_tot or cip in self.residents_act:
                    self.visitats_urgencies_altres.add(cip)
                    self.urg_intermedia[cip].add(data)
        
        # prenc els que s'han visitat a urg�ncies d'un hospital en els �ltims 12 mesos
        sql = """SELECT c_nia, c_data_entr, tip_cod, subtip_cod
                FROM dwsisap.cmbd_urg a
                left join dwsisap.dbc_rup b
                on a.c_up = b.up_cod
                WHERE c_nia != 99999999
                AND c_data_entr > DATE '{LAST12}'""".format(LAST12=LAST12)
        print(sql)
        for nia, data, tip, subtip in u.getAll(sql, 'exadata'):
            if nia in self.nia_2_cip:
                cip = self.nia_2_cip[nia]
                if cip in self.residents_tot or cip in self.residents_act:
                    self.visitats_urgencies_hospital.add(cip)
                    if not (tip == '30' and subtip in ('48', '31')):
                        self.urg_intermedia[cip].add(data)
    
    def get_indicadors(self):
        self.upload = c.Counter()
        self.pacients = c.defaultdict()
        # indicador 1
        # Mitjana de dies d�ingres en un centre hospitalari o sociosanitari 
        # de les persones que viuen en una resid�ncia de gent gran
        # indicador 2
        # Percentatge d'�xitus durant l�ingr�s hospitalari o sociosanitari 
        # de la poblaci� institucionalitzada en resid�ncia gent gran
        # indicador 3
        # Percentatge de mortalitat de les persones que viuen en una 
        # resid�ncia de gent gran (grups usuaris, institucionalitzats).
        # indicador 4
        # Taxa d�urg�ncies hospital�ries o CUAP o PAC de les persones que 
        # viuen en una resid�ncia de gent gran (grups usuaris, institucionalitzats).
        for cip, up in self.residents_tot.items():
            # self.upload['RES075A', up, 'POBTOTAL', 'DEN'] += 1
            # self.upload['RES076A', up, 'POBTOTAL','DEN'] += 1
            self.upload['RES077A', up, 'POBTOTAL','DEN'] += 1
            self.upload['RES078A', up, 'POBTOTAL','DEN'] += 1
            self.upload['RES078AA', up, 'POBTOTAL','DEN'] += 1
            self.upload['RES078AB', up, 'POBTOTAL','DEN'] += 1
            self.upload['RES079A', up, 'POBTOTAL','DEN'] += 1
            self.upload['RES062A', up, 'POBTOTAL','DEN'] += 1
            self.upload['RES080A', up, 'POBTOTAL', 'DEN'] += 1
            self.upload['RES082A', up, 'POBTOTAL', 'DEN'] += 1
            self.upload['RES083A', up, 'POBTOTAL', 'DEN'] += 1
        
        for cip, up in self.residents_act.items():
            # self.upload['RES075A', up, 'POBACTUAL', 'DEN'] += 1
            # self.upload['RES076A', up, 'POBACTUAL','DEN'] += 1
            self.upload['RES077A', up, 'POBACTUAL','DEN'] += 1
            self.upload['RES078A', up, 'POBACTUAL','DEN'] += 1
            self.upload['RES078AA', up, 'POBACTUAL','DEN'] += 1
            self.upload['RES078AB', up, 'POBACTUAL','DEN'] += 1
            self.upload['RES079A', up, 'POBACTUAL','DEN'] += 1
            self.upload['RES062A', up, 'POBACTUAL','DEN'] += 1
            self.upload['RES080A', up, 'POBACTUAL', 'DEN'] += 1
            self.upload['RES082A', up, 'POBACTUAL', 'DEN'] += 1
            self.upload['RES083A', up, 'POBACTUAL', 'DEN'] += 1
            if cip in self.id_cip:
                self.pacients[self.id_cip[cip], 'RES077A'] = 0
                self.pacients[self.id_cip[cip], 'RES078A'] = 0
                self.pacients[self.id_cip[cip], 'RES078AA'] = 0
                self.pacients[self.id_cip[cip], 'RES078AB'] = 0
                self.pacients[self.id_cip[cip], 'RES079A'] = 0
                self.pacients[self.id_cip[cip], 'RES080A'] = 0
                self.pacients[self.id_cip[cip], 'RES062A'] = 0
                self.pacients[self.id_cip[cip], 'RES082A'] = 0
                self.pacients[self.id_cip[cip], 'RES083A'] = 0
            # self.pacients['RES062A', cip, up, 'POBACTUAL', 1] = 0
            # self.pacients['intermedia', cip, up, 'POBACTUAL', 1] = 0

        for cip, dies in self.dies_hospitalitzats.items():
            if cip in self.residents_tot:
                up = self.residents_tot[cip]
                self.upload['RES075A', up, 'POBTOTAL', 'NUM'] += dies
                self.upload['RES075A', up, 'POBTOTAL', 'DEN'] += 1
                self.upload['RES076A', up, 'POBTOTAL', 'DEN'] += 1
            if cip in self.residents_act:
                up = self.residents_act[cip]
                self.upload['RES075A', up, 'POBACTUAL', 'NUM'] += dies
                self.upload['RES075A', up, 'POBACTUAL', 'DEN'] += 1
                self.upload['RES076A', up, 'POBACTUAL', 'DEN'] += 1
                if cip in self.id_cip:
                    self.pacients[self.id_cip[cip], 'RES075A'] = 1
                    self.pacients[self.id_cip[cip], 'RES076A'] = 0
        for cip in self.residents_tot:
            up = self.residents_tot[cip]
            dies = 0
            if cip in self.dies_hospitalitzats:
                dies += self.dies_hospitalitzats[cip]
            if cip in self.intermedia:
                dies += self.intermedia[cip]
            self.upload['RES080A', up, 'POBTOTAL', 'NUM'] += 365 - dies
            # self.pacients['intermedia', cip, up, 'POBTOTAL', 1] += 365 - dies
        
        for cip in self.residents_act:
            up = self.residents_act[cip]
            dies = 0
            if cip in self.dies_hospitalitzats:
                dies += self.dies_hospitalitzats[cip]
            if cip in self.intermedia:
                dies += self.intermedia[cip]
            self.upload['RES080A', up, 'POBACTUAL', 'NUM'] += 365 - dies
            if cip in self.id_cip:
                self.pacients[self.id_cip[cip], 'RES080A'] = 1
            # self.pacients['intermedia', cip, up, 'POBACTUAL', 1] += 365 - dies
        for cip in self.exitus_hosp:
            if cip in self.residents_tot and cip in self.dies_hospitalitzats:
                up = self.residents_tot[cip]
                self.upload['RES076A', up, 'POBTOTAL', 'NUM'] += 1
            if cip in self.residents_act and cip in self.dies_hospitalitzats:
                up = self.residents_act[cip]
                self.upload['RES076A', up, 'POBACTUAL', 'NUM'] += 1
                if cip in self.id_cip:
                    self.pacients[self.id_cip[cip], 'RES076A'] = 1
        
        for cip in self.exitus_generals:
            if cip in self.residents_tot:
                up = self.residents_tot[cip]
                self.upload['RES077A', up, 'POBTOTAL', 'NUM'] += 1
            if cip in self.residents_act:
                up = self.residents_act[cip]
                self.upload['RES077A', up, 'POBACTUAL', 'NUM'] += 1
                if cip in self.id_cip:
                    self.pacients[self.id_cip[cip], 'RES077A'] = 1
        
        for cip in self.visitats_urgencies_hospital:
            if cip in self.residents_tot:
                up = self.residents_tot[cip]
                self.upload['RES078AA', up, 'POBTOTAL', 'NUM'] += 1
            if cip in self.residents_act:
                up = self.residents_act[cip]
                self.upload['RES078AA', up, 'POBACTUAL', 'NUM'] += 1
                if cip in self.id_cip:
                    self.pacients[self.id_cip[cip], 'RES078AA'] = 1

        for cip in self.visitats_urgencies_altres:
            if cip in self.residents_tot:
                up = self.residents_tot[cip]
                self.upload['RES078AB', up, 'POBTOTAL', 'NUM'] += 1
            if cip in self.residents_act:
                up = self.residents_act[cip]
                self.upload['RES078AB', up, 'POBACTUAL', 'NUM'] += 1
                if cip in self.id_cip:
                    self.pacients[self.id_cip[cip], 'RES078AB'] = 1

        self.visitats_urgencies = self.visitats_urgencies_altres.union(self.visitats_urgencies_hospital)

        for cip in self.visitats_urgencies:
            if cip in self.residents_tot:
                up = self.residents_tot[cip]
                self.upload['RES078A', up, 'POBTOTAL', 'NUM'] += 1
            if cip in self.residents_act:
                up = self.residents_act[cip]
                self.upload['RES078A', up, 'POBACTUAL', 'NUM'] += 1
                if cip in self.id_cip:
                    self.pacients[self.id_cip[cip], 'RES078A'] = 1
        
        for cip, dies in self.dies_hospitalitzats.items():
            if cip in self.residents_tot:
                up = self.residents_tot[cip]
                self.upload['RES079A', up, 'POBTOTAL', 'NUM'] += 1
            if cip in self.residents_act:
                up = self.residents_act[cip]
                self.upload['RES079A', up, 'POBACTUAL', 'NUM'] += 1
                if cip in self.id_cip:
                    self.pacients[self.id_cip[cip], 'RES079A'] = 1

        for cip, n in self.ingressos_hosp.items():
            if cip in self.residents_tot:
                up = self.residents_tot[cip]
                self.upload['RES062A', up, 'POBTOTAL', 'NUM'] += n
                # self.pacients['RES062A', cip, up, 'POBTOTAL', 1] += n
            if cip in self.residents_act:
                up = self.residents_act[cip]
                self.upload['RES062A', up, 'POBACTUAL', 'NUM'] += n
                if cip in self.id_cip:
                    self.pacients[self.id_cip[cip], 'RES062A'] = 1
        
        for cip, n in self.ingres_intermedia.items():
            if cip in self.residents_tot:
                up = self.residents_tot[cip]
                self.upload['RES082A', up, 'POBTOTAL', 'NUM'] += 1
                self.upload['RES083A', up, 'POBTOTAL', 'NUM'] += n
                self.upload['RES084A', up, 'POBTOTAL', 'DEN'] += 1
            if cip in self.residents_act:
                up = self.residents_act[cip]
                self.upload['RES082A', up, 'POBACTUAL', 'NUM'] += 1
                self.upload['RES083A', up, 'POBACTUAL', 'NUM'] += n
                self.upload['RES084A', up, 'POBACTUAL', 'DEN'] += 1
        
        for cip in self.dates_intermedia:
            entra = 0
            if cip in self.urg_intermedia:
                matching = {dingres for (dingres, dfi) in self.dates_intermedia[cip] for durg in self.urg_intermedia[cip] if abs((dingres - durg).days) <= 2}
                matching2 = {dingres for (dingres, dfi) in self.dates_intermedia[cip] for (dingres2, dfi2) in self.dates_intermedia[cip] if abs((dingres - dingres2).days) <= 2 and dingres != dingres2}
                matching3 = {dingres for (dingres, dfi) in self.dates_intermedia[cip] for (dingres2, dfi2) in self.dates_intermedia[cip] if dfi2 and abs((dingres - dfi2).days) <= 2 and dingres != dfi2}
                matching4 = {dingres for (dingres, dfi) in self.dates_intermedia[cip] for (dingres2, dfi2) in self.dates_intermedia[cip] if dfi2 and dingres2 <= dingres <= dfi2 and dingres != dingres2}
                if len(matching) == 0 and len(matching2) == 0 and len(matching3) == 0 and len(matching4) == 0:
                    entra = 1
            else:
                entra = 1
            if entra:
                if cip in self.residents_tot:
                    up = self.residents_tot[cip]
                    self.upload['RES084A', up, 'POBTOTAL', 'NUM'] += 1
                if cip in self.residents_act:
                    up = self.residents_act[cip]
                    self.upload['RES084A', up, 'POBACTUAL', 'NUM'] += 1


    def uploading(self):
        up = [(key[0], key[1], key[2], key[3], val) for (key, val) in self.upload.items()]
        cols = '(indi varchar(10), up_resi varchar(10), dim6set varchar(10), analisi varchar(3), val int)'
        u.createTable('nous_indicadors_resis', cols, 'resis', rm = True)
        u.listToTable(up, 'nous_indicadors_resis', 'resis')
        
        u.execute("""ALTER TABLE resis.nous_indicadors_resis MODIFY up_resi VARCHAR(10)
                        CHARACTER SET latin1 COLLATE latin1_general_ci""", 'resis')
        
        pacients = []
        for (id, ind) in self.pacients:
            pacients.append((id, ind, self.pacients[id, ind]))
        u.listToTable(pacients, 'pacients_resis', 'resis')
        # sql = """SELECT DISTINCT i.indi, 'Aperiodo', concat('R', replace(i.up_resi,'-','_')), i.analisi, 
        #             'NOCAT', 'NOIMP', 'POBTOTAL', 'N', i.val
        #             FROM resis.nous_indicadors_resis i, import.cat_sisap_map_residencies c
        #             where i.up_resi = c.resi_cod and c.sisap_class = 1
        #         UNION
        #         SELECT DISTINCT i.indi, 'Aperiodo', concat('R', replace(i.up_resi,'-','_')), i.analisi, 
        #             'NOCAT', 'NOIMP', 'POBACTUAL', 'N', i.val
        #             FROM resis.nous_indicadors_resis i, import.cat_sisap_map_residencies c
        #             where i.up_resi = c.resi_cod and c.sisap_class = 1"""
        # u.exportKhalix(sql, 'RESHOSP')


if __name__ == "__main__":
    Indicadors()

