# -*- coding: latin1 -*-

"""
copiado de 08alt/source/gida.py
    # 5 y 6 *ubainf (taxa poblacional), 3 y 4 *Colegiado (% visitas)
"""
# import hashlib as h
import datetime as d
import collections as c

import sisaptools as u  # t
import sisapUtils as su  # u
import shared as s

# import dateutil.relativedelta as r

TEST = False
RECOVER = False
SUFIX = "_resi"

# MASTER_TB = "mst_gida{}".format(SUFIX)
# KHALIX_UBA = "GIDA_UBA{}".format(SUFIX)
# CORTES_TB = "exp_khalix_gida_mensual{}".format(SUFIX)
# CORTES_UP = "GIDA_MENSUAL{}".format(SUFIX)
# QC_TB = "exp_qc_gida{}".format(SUFIX)

KHALIX_TB = "exp_khalix_gida{}".format(SUFIX)
KHALIX_UP = "GIDA{}".format(SUFIX)
# d.date(2021, 1, 31), d.date(2021, 2, 28), d.date(2021, 3, 30)
DATABASE = "resis"
DEXTD = d.date(2021, 8, 31) if RECOVER else u.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]  # noqa

TAXES = {
    "PABCESA": 5.2577444780838,
    "PAFTAA": 0.64353537675087,
    "PANTIC": 0.314891408767176,
    "PANSIE": 3.71921925701736,
    "PAPZB": 0.425027223520727,
    "PCEFALA": 5.16472350344691,
    "PCERVI": 2.52356334348391,
    "PCOLIC": 0.0411098732994345,
    "PCONTD": 1.21704997599514,
    "PCONTU": 5.78656077591262,
    "PCOSEN": 0.0350197187929564,
    "PCOSEO": 0.0877768743695683,
    "PCREM": 2.31795604290189,
    "PCREPE": 0.329983737596058,
    "PDIAGN": 1.03730557538692,
    "PDIARR": 11.4234445032924,
    "PDML": 8.57241327519725,
    "PEPIS": 0.410744946003007,
    "PEPISN": 0.0460311485676763,
    "PFEBAN": 4.27140333603846,
    "PFEBSF": 5.13751961771815,
    "PFERID": 19.1768056475241,
    "PFERNE": 4.16971002682668,
    "PHERPLA": 0.166687886578287,
    "PINSOMA": 0.0939083809556885,
    "PLDERM": 0.98219608971855,
    "PLESC": 0.633488070053698,
    "PMAREI": 10.8921688274182,
    "PMOLOR": 13.5785540195654,
    "PMUCO": 1.3594578348242,
    "PMUGN": 0.044018906711633,
    "PMURI": 33.7683944007229,
    "PMVAGA": 1.31619769359147,
    "PODINO": 25.8706516172878,
    "PODON": 1.35240896560876,
    "PPICIN": 0.672004294395738,
    "PPICPE": 2.754989077508,
    "PPLOIN": 0.0394714334870186,
    "PPOLLS": 0.0467511548140302,
    "PPRESA": 4.36583663891906,
    "PREGUR": 0.060582542618725,
    "PRESTN": 0.159984697721928,
    "PRESTREA": 0.603810144826813,
    "PRESVA": 49.8269922191717,
    "PREVACA": 0.162048520067087,
    "PREVACAP": 0.0453135247117959,
    "PSCOVA": 1.39566644032561,
    "PSCOVAP": 0.0302052523143464,
    "PSOSVA": 0.0473657986723003,
    "PTOS": 2.66359958714152,
    "PTURME": 1.17070731198391,
    "PULL": 5.7918438680642,
    "PUNGA": 0.788700028620055,
    "PURTIA": 2.24985018751402,
    "PVOLTA": 0.744654244982946,
    "PVOMIT": 11.1681124722259,
    "PVOMNE": 1.39029301590878,
    "PRAOADU": 0.257873147546163,
    "PRAOPED": 0.0527365935237381,
    "PACEI": 0.0401676783290664,
    "PVOLTAP": 0.128464121310962,
    "PUNGAP": 0.114838209198685,
    "PHERPAP": 0.0483585401145677,
    "PAFTAAP": 0.181361219737247,
    "PODINAP": 1.88137686510272,
    "PODONTAP": 0.0544801665116972,
    "PMELICAP": 0.0393639903497557,
    "PDOLMA": 0,
    "PGRIPA": 0
    }


CONVERSIO = {"ACE_A": "ANTIC", "CERVI_A": "CERVI", "CONT_A": "CONTU",
             "CREM_A": "CREM", "C_ANSI_A": "ANSIE", "DIARR_A": "DIARR",
             "LUMB_A": "DML", "EPA_A": "PRESA", "ENTOR_A": "TURME",
             "EPIST_A": "EPIS", "FEBRE_A": "FEBSF", "FERIDA_A": "FERID",
             "PLEC_A": "LDERM", "MAREIG_A": "MAREI", "M_OID_A": "MOLOR",
             "M_ULL_A": "ULL", "M_URI_A": "MURI", "ODIN_A": "ODINO",
             "ODONT_A": "ODON", "PICAD_A": "PICPE", "SRVA_A": "RESVA",
             "VOMIT_A": "VOMIT", "ALT_A": "A", "CEFAL": "CEFAL_A",
             "HERP": "HERPL_A", "ABCES": "ABCES_A", "UNG": "UNG_A",
             "VOLT": "VOLT_A", "RESTR": "RESTRE_A", "MVAG": "MVAG_A",
             "REVAC": "REVAC_A", "AFTA": "AFTA_A", "INSOM": "INSOM_A",
             "URTI": "URTI_A", "LESFC_A": "LESC",
             "COLIC_AP": "COLIC", "CON_D_AP": "CONTD", "CEORE_AP": "COSEO",
             "CENAS_AP": "COSEN", "CREMA_AP": "CREPE", "DER_B_AP": "APZB",
             "DIARR_AP": "DIAGN", "EPIST_AP": "EPISN", "FEBRE_AP": "FEBAN",
             "FERID_AP": "FERNE", "MOCS_AP": "MUCO", "PICAD_AP": "PICIN",
             "PLORS_AP": "PLOIN", "POLLS_AP": "POLLS", "REGUR_AP": "REGUR",
             "RESTR_AP": "RESTN", "MUGUE_AP": "MUGN", "VARIC_AP": "SOSVA",
             "TOS_AP": "TOS", "VOMIT_AP": "VOMNE"}


class GIDA(object):
    """."""
    def __init__(self):
        """."""
        # POBLACIONES
        self.get_conversor()
        self.get_pob()
        self.get_atdom()
        self.get_idcipsec_map()
        self.get_id_resi()
        # AUXILIARES independientes de población
        self.get_edats()  # categories: adu, ped
        self.get_centres()
        self.get_usu_to_col()
        self.get_motius()
        # self.get_ubas()
        # self.get_col_to_uba()

        # INDICADORES
        # ts = d.datetime.now()
        # self.recomptes = c.defaultdict(lambda: c.defaultdict(c.Counter))
        # # self.mensual = c.Counter()  # get_aguda: 154 y export_mensual: 296
        # self.get_poblacio()  # {id: (up, ubainf)}, fitro por id_resi  # <-PB
        # self.get_visites()  # <-PB
        # self.get_aguda()  # ojo! dext escapa RECOVER <-PB
        # self.get_master()
        # self.get_derivats()
        # print(d.datetime.now() - ts)

        # # SALIDAS
        # # self.export_master()
        # self.get_khalix()
        # self.get_taxes()
        # self.export_khalix()
        # # self.export_mensual()
        # # self.export_qc()

    def get_conversor(self):
        """."""
        self.conversor = {}
        sql = "select usua_cip, usua_cip_cod from pdptb101 \
               where usua_cip_cod like '{}%'"
        for cip_a, hash_a in s.get_data(sql, model="redics", redics="pdp"):
            self.conversor[cip_a] = hash_a

    def get_atdom(self):
        """ atdoms """
        self.atdom = set([id for id, in su.getAll("""
                                      SELECT
                                        id_cip_sec
                                      FROM
                                        eqa_problemes
                                      WHERE
                                        ps = 45 AND
                                        dde <= DATE '{DEXTD}'
                                      """.format(DEXTD=DEXTD),
                                      'nodrizas')
                          ])
        atdom_ubas = set()
        self.atdom_ass = c.defaultdict(dict)
        for id, _resi, up, uba, upinf, ubainf, _pcc, _maca in su.getAll(
                """
                select
                    id_cip_sec, up_residencia, up, uba,
                    upinf, ubainf, pcc, maca
                from assignada_tot
                """,
                "nodrizas"):
            if id in self.atdom:
                if uba:
                    atdom_ubas.add((up, uba, 'M'))
                if ubainf:
                    atdom_ubas.add((upinf, ubainf, 'I'))
                for row in [(up, uba, 'M'), (upinf, ubainf, 'I')]:
                    if row in atdom_ubas:
                        self.atdom_ass[id]['M'] = (up, uba)
                        self.atdom_ass[id]['I'] = (upinf, ubainf)
                        self.atdom_ass[id]['UP'] = up
                        self.atdom_ass[id]['dep'] = ''

    def get_pob(self):
        db = "exadata"
        # sch = "data"
        self.pob_act = {}
        self.pob_tot = {}
        resis = set()
        sql = """
            SELECT resi_cod
            FROM cat_sisap_map_residencies
            WHERE sisap_class = 1
        """
        for resi, in su.getAll(sql, 'import'):
            resis.add(resi)

        sql_act = """
            WITH
                max_data AS (
                    SELECT DATE '{DEXTD}' AS max_data FROM dual
                    -- SELECT max(data) AS max_data
                    -- FROM residencies_cens
                    -- WHERE perfil = 'Resident'
                    ),
                cip_resis AS (
                    SELECT
                        r.cip,
                        r.residencia,
                        max(r.data) AS max_resi_data
                    FROM
                        residencies_cens r
                        -- INNER JOIN residencies_cataleg rc
                        --    ON r.residencia = rc.codi
                        ,max_data m
                    WHERE
                        r.perfil = 'Resident'
                        -- AND rc.tipus = 1
                        AND r.data = m.max_data
                    GROUP BY
                        r.cip,
                        r.residencia
                    ),
                cip_last_data AS (
                    SELECT cip, max(max_resi_data) AS last_data
                    FROM cip_resis
                    GROUP BY cip
                    )
            SELECT
                cr.cip, max(cr.residencia) AS residencia
            FROM
                cip_resis cr
                INNER JOIN cip_last_data cld
                    ON cr.cip = cld.cip
                    AND cr.max_resi_data = cld.last_data
            GROUP BY
                cr.cip
        """.format(DEXTD=DEXTD)
        sql_tot = """
            WITH
                max_data AS (
                    SELECT DATE '{DEXTD}' AS max_data FROM dual
                    -- SELECT max(data) AS max_data
                    -- FROM residencies_cens
                    -- WHERE perfil = 'Resident'
                    ),
                cip_resis AS (
                    SELECT
                        r.cip,
                        r.residencia,
                        max(r.DATA) AS max_resi_data
                    FROM
                        residencies_cens r
                        -- INNER JOIN residencies_cataleg rc
                        --    ON r.residencia = rc.CODI
                        ,max_data m
                    WHERE
                        r.perfil = 'Resident'
                        -- AND rc.tipus = 1
                        AND r.data BETWEEN add_months(m.max_data, -12)+1 AND
                                           m.max_data
                    GROUP BY
                        r.cip,
                        r.residencia
                    HAVING
                        count(*) >= 30
                    ),
                cip_last_data AS (
                    SELECT cip, max(max_resi_data) AS last_data
                    FROM cip_resis
                    GROUP BY cip
                    )
            SELECT
                cr.cip, max(cr.residencia) AS residencia
            FROM
                cip_resis cr
                INNER JOIN cip_last_data cld
                    ON cr.cip = cld.cip
                    AND cr.max_resi_data = cld.last_data
            GROUP BY
                cr.cip
        """.format(DEXTD=DEXTD)
        # with u.Database(db, sch) as conn:
        #     for cip, resi in conn.get_all(sql_act):
        #         self.pob_act[cip] = resi
        # with u.Database(db, sch) as conn:
        #     for cip, resi in conn.get_all(sql_tot):
        #         self.pob_tot[cip] = resi
        for cip, resi in su.getAll(sql_act, db):
            if resi in resis:
                self.pob_act[cip] = resi
        for cip, resi in su.getAll(sql_tot, db):
            if resi in resis:
                self.pob_tot[cip] = resi

    def get_idcipsec_map(self):
        """
        Gets id_cip_sec and hash_d from hash_a.
        - {u11}  # hash_a: {id_cip_sec, hash_d}
        """
        self.resi_sec = {}
        with u.Database('p2262', 'nodrizas') as conn:
            for resi, sector in conn.get_all("""
                    WITH
                        resi_up as (SELECT
                                        distinct resi_cod as resi, eap_up as up
                                    FROM
                                        import.cat_sisap_map_residencies
                                    WHERE
                                        sisap_class =1),
                        up_sec as  (SELECT
                                        scs_codi as up, sector
                                    FROM
                                    nodrizas.cat_centres)
                    SELECT
                        r.resi, u.sector
                    FROM
                        resi_up r INNER JOIN
                        up_sec u ON r.up = u.up
                    """):
                self.resi_sec[resi] = sector
        db = "p2262"
        sch = "import"
        tb = "u11_all"
        cols = ["id_cip_sec", "hash_a", "hash_d", "codi_sector"]
        cols = ", ".join(cols)
        sql = "select {} from {}".format(cols, tb)
        self.hash_sec = {}
        self.hashes = set()
        self.u11 = {}
        self.ids = {}
        for cip, resi in self.pob_tot.items():
            sector = self.resi_sec.get(resi)
            hash_a = self.conversor.get(cip)
            if hash_a is not None:
                self.hash_sec[hash_a] = sector
                self.hashes.add(hash_a)
        for cip, resi in self.pob_act.items():
            sector = self.resi_sec.get(resi)
            hash_a = self.conversor.get(cip)
            if hash_a is not None:
                self.hash_sec[hash_a] = sector
                self.hashes.add(hash_a)
        with u.Database(db, sch) as conn:
            for id_cip_sec, hash_a, hash_d, sector in conn.get_all(sql):
                # if hash_a in self.hashes or id_cip_sec in self.atdom_ass:
                #     self.ids[id_cip_sec] = (hash_d, sector)
                #     self.u11[hash_a] = {'id_cip_sec': id_cip_sec,
                #                         'hash_d': hash_d}
                sec = self.hash_sec.get(hash_a)
                if sec == sector:
                    self.ids[id_cip_sec] = (hash_d, sector)
                    self.u11[hash_a] = {'id_cip_sec': id_cip_sec,
                                        'hash_d': hash_d,
                                        'sector': sector,
                                        }

    # def get_hash(self, cip):  # QcResis
    #     """Returns hcovid from cip."""
    #     return h.sha1(cip).hexdigest().upper()

    def get_id_resi(self):
        # DEPENDENCIA II_III
        sql = """
            SELECT
                cip,
                max(grau) AS grau
            FROM
                sisap_covid_tmp_dep
            WHERE
                cip IS NOT NULL AND
                grau IN ('II','III')
            GROUP BY
                cip
            """
        dependencia = {cip: grau for (cip, grau) in su.getAll(sql, 'redics')}
        # for cip, grau in cip_dep.items():
        #     hash_a = self.conversor.get(cip)
        #     try:
        #         id = self.u11[hash_a]['id_cip_sec']
        #         dependencia[id] = grau
        #     except KeyError:
        #         continue
        self.id_resi = c.defaultdict(dict)
        self.id_resi_tot = c.defaultdict(dict)
        for cip, resi in self.pob_act.items():
            hash_a = self.conversor.get(cip)
            dep = dependencia.get(cip)
            try:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi[id]['UP'] = resi
                self.id_resi[id]['dep'] = dep
            except KeyError:
                continue
        self.id_resi.default_factory = None
        for cip, resi in self.pob_tot.items():
            hash_a = self.conversor.get(cip)
            dep = dependencia.get(cip)
            try:
                id = self.u11[hash_a]['id_cip_sec']
                self.id_resi_tot[id]['UP'] = resi
                self.id_resi_tot[id]['dep'] = dep
            except KeyError:
                continue
        self.id_resi_tot.default_factory = None

    def get_edats(self):
        """."""
        sql = """
            SELECT
                id_cip_sec,
                TIMESTAMPDIFF(YEAR, usua_data_naixement, DATE '{DEXTD}') AS val
            FROM assignada
            """.format(DEXTD=DEXTD)
        self.edats = {id: "ADU" if edat > 14 else "PED"
                      for (id, edat) in su.getAll(sql, "import")
                      # if id in set(self.id_resi) | set(self.id_resi_tot)
                      }

    def get_centres(self):
        """."""
        sql = "SELECT scs_codi, tip_eap, ics_codi FROM cat_centres"
        self.centres = {row[0]: row[1:] for row in su.getAll(sql, "nodrizas")}

    def get_usu_to_col(self):
        """."""
        sql = "SELECT codi_sector, ide_usuari, ide_numcol \
               FROM cat_pritb992 \
               WHERE ide_numcol <> ''"
        self.usu_to_col = {row[:2]: row[2] for row in su.getAll(sql, "import")}

    def get_motius(self):
        """."""
        sql = """
            SELECT sym_name
            FROM icsap.klx_master_symbol
            WHERE
                dim_index = 4 AND (
                sym_index IN (
                    SELECT sym_index
                    FROM icsap.klx_parent_child
                    WHERE
                        dim_index = 4 AND
                        parent_index IN (
                            SELECT sym_index
                            FROM icsap.klx_master_symbol
                            WHERE
                                dim_index = 4 AND
                                sym_name IN ('PMOTIUPRPE', 'PMOTIUTEL')
                            )
                    ) OR
                sym_name = 'PALPED')
            """
        self.motius = {motiu: "PED" for motiu, in su.getAll(sql, "khalix")}

    def _get_motiu(self, motiu, grup):
        """."""
        motiu = CONVERSIO.get(motiu, motiu)
        if motiu == "A":
            motiu = "PAL{}".format(grup)
        else:
            motiu = "P{}".format(motiu.replace("_", ""))
        if motiu not in self.motius:
            self.motius[motiu] = "ADU"
        return motiu

    # def get_ubas(self):
    #     """Nomes pujarem UBAs amb EQA."""
    #     self.ubas = c.defaultdict(set)
    #     for db, grup in (("eqa_ind", "ADU"), ("pedia", "PED")):
    #         sql = "SELECT up, uba FROM mst_ubas WHERE tipus = 'I'"
    #         for key in su.getAll(sql, db):
    #             self.ubas[grup].add(key)
    #             self.ubas["MIX"].add(key)

    # def get_col_to_uba(self):
    #     """Per passar dades individuals a khalix dels indicadors de NUMCOL."""  # noqa
    #     self.col_to_uba = c.defaultdict(set)
    #     sql = "SELECT ide_numcol, up, uab \
    #            FROM cat_professionals \
    #            WHERE tipus = 'I'"
    #     for col, up, uba in su.getAll(sql, "import"):
    #         self.col_to_uba[(up, col)].add(uba)

    # def export_master(self):
    #     """."""
    #     su.createTable(MASTER_TB,
    #                    "(resi varchar(13), \
    #                     ind varchar(10), up varchar(5), \
    #                     prof varchar(10), tip varchar(3), \
    #                     grup varchar(3), motiu varchar(10), \
    #                     num int, den int)",
    #                    DATABASE, rm=True)
    #     upload = [k + (v["NUM"], v["DEN"]) for (k, v) in self.master.items()]
    #     su.listToTable(upload, MASTER_TB, DATABASE)

    # def export_mensual(self):
    #     """."""
    #     cols = """(
    #             ind varchar(10),
    #             periode varchar(10),
    #             resi varchar(13),
    #             motiu varchar(10),
    #             valor int
    #             )
    #         """
    #     su.createTable(CORTES_TB, cols, DATABASE, rm=True)
    #     upload = [k + (v,) for (k, v) in self.mensual.items()]
    #     su.listToTable(upload, CORTES_TB, DATABASE)

    #     sql = """
    #             SELECT
    #                 ind, periode, resi, 'NUM', motiu,
    #                 'NOIMP', 'DIM6SET', 'N', valor
    #             FROM
    #                 {}.{} -- INNER JOIN
    #                 -- nodrizas.cat_centres
    #                 --    ON up = scs_codi
    #           """.format(DATABASE, CORTES_TB)
    #     su.exportKhalix(sql, CORTES_UP)

    # def export_qc(self):
    #     """."""
    #     indicadors = ("GESTINF05", "GESTINF06")
    #     dext, = su.getOne("SELECT data_ext FROM dextraccio", "nodrizas")
    #     fa1m = dext - r.relativedelta(months=1)
    #     pactual = "A{}".format(dext.strftime("%y%m"))
    #     pprevi = "A{}".format(fa1m.strftime("%y%m"))
    #     sqls = []
    #     #  en la sql, cuando 'ent' es una residencia sera mas larga que 5
    #     #  el codigo original lo usaba para distinguir las UBAS de khalix
    #     for x, y in ((pactual, 'ANUAL'), (pactual, 'ACTUAL'), (pprevi, 'PREVI')):  # noqa
    #         sqls.append("""
    #                     SELECT
    #                         ind, '{}',
    #                         ent, 'DEN', '{}',
    #                         'TIPPROF4', sum(valor)
    #                     FROM {}
    #                     WHERE
    #                         ind in {} AND
    #                         -- length(ent) = 5 AND
    #                         analisi = 'DEN' AND
    #                         motiu in ('PALADU', 'PALPED')
    #                     GROUP BY
    #                         ind, ent
    #                     """.format(x, y, KHALIX_TB, indicadors))
    #     sqls.append("""
    #                 SELECT
    #                     ind, '{}',
    #                     ent, 'NUM', 'ANUAL',
    #                     'TIPPROF4', sum(valor)
    #                 FROM {}
    #                 WHERE
    #                     ind in {} AND
    #                     -- length(ent) = 5 AND
    #                     analisi = 'NUM'
    #                 GROUP BY
    #                     ind, ent
    #                 """.format(pactual, KHALIX_TB, indicadors))
    #     # aqui cambio ics_codi (br) por resi
    #     # !! habra que transformar a codi kalix de la residencia "-": "_"
    #     sqls.append("""
    #                 SELECT
    #                     ind, replace(periode, 'B', 'A'),
    #                     resi, 'NUM', if(replace(periode, 'B', 'A') = '{}',
    #                                         'ACTUAL', 'PREVI'),
    #                     'TIPPROF4', sum(valor)
    #                 FROM
    #                     {} a -- INNER JOIN
    #                     -- nodrizas.cat_centres b
    #                     --    on a.up = b.scs_codi
    #                 WHERE
    #                     ind in {}
    #                 GROUP BY
    #                     ind, periode, resi
    #                 """.format(pactual, CORTES_TB, indicadors))
    #     dades = []
    #     for sql in sqls:
    #         dades.extend([("Q" + row[0],) + row[1:]
    #                       for row in su.getAll(sql, DATABASE)])
    #     cols = ["k{} varchar(10)".format(i) for i in range(6)] + ["v int"]
    #     su.createTable(QC_TB, "({})".format(", ".join(cols)), DATABASE, rm=True)  # noqa
    #     su.listToTable(dades, QC_TB, DATABASE)

    def get_poblacio(self, pob_ids):
        """."""
        self.poblacio = {}
        sql = "SELECT id_cip_sec, up, ubainf FROM assignada_tot WHERE ates = 1"
        for id, up, uba in su.getAll(sql, "nodrizas"):
            if id in pob_ids:
                resi = pob_ids[id]['UP']
                grup = self.edats[id]
                for ind in ("GESTINF05", "GESTINF06"):
                    # self.recomptes[ind][(up, uba, "UBA", grup)]["DEN"] += 1
                    self.recomptes[ind][(resi, up, uba, "UBA", grup)]["DEN"] += 1  # noqa
                self.poblacio[id] = (up, uba)

    def get_visites(self, pob_ids):
        """."""
        sql = """
            SELECT id_cip_sec, visi_up, visi_col_prov_resp
            FROM visites2
            WHERE
                visi_col_prov_resp like '3%' AND
                s_espe_codi_especialitat not in ('EXTRA', '10102') AND
                visi_tipus_visita not in ('9E', 'EXTRA', 'EXT') AND
                visi_situacio_visita = 'R' AND
                visi_data_visita BETWEEN
                    ADDDATE(DATE '{DEXTD}', interval -1 year) AND
                    DATE '{DEXTD}'
            """.format(DEXTD=DEXTD)
        cnt_visites = cnt_edats = cnt_id_resi = 0
        for id, up, col in su.getAll(sql, "import"):
            cnt_visites += 1
            # tipus = self.centres[up] if up in self.centres else 'M'
            # QUITO FILTRO DE CENTROS EAP QUE PERDIAN 100k visitas (PADES, etc...)  # noqa
            # if up in self.centres:
            #     tipus = self.centres[up][0]
            if id in self.edats:
                cnt_edats += 1
                grup = self.edats[id]
                # go = (tipus == "M", (grup == "ADU" and tipus == "A"),
                #       # (grup == "PED" and tipus == "N"),
                #       )
                # if any(go):  # QcResi
                if id in pob_ids:
                    cnt_id_resi += 1
                    resi = pob_ids[id]['UP']  # QcResi
                    for ind in ("GESTINF03", "GESTINF04"):
                        self.recomptes[ind][(resi, up, col, "COL", grup)]["DEN"] += 1  # noqa

    def get_aguda(self, pob_ids):
        """."""
        dext, = su.getOne("SELECT data_ext FROM dextraccio", "nodrizas")
        # first = (dext - r.relativedelta(months=1)).replace(day=1)
        sql = """
            SELECT
                id_cip_sec, codi_sector, cesp_up, cesp_usu_alta,
                cesp_cod_mce,
                cesp_estat,
                cesp_data_alta
            FROM
                aguda
            WHERE
                cesp_data_alta between
                    adddate(DATE '{DEXTD}', interval -1 year)
                    and DATE '{DEXTD}'
            UNION
            SELECT
                id_cip_sec, codi_sector, pi_up, pi_usu_alta,
                upper(replace(pi_anagrama, ' ', '')),
                if(pi_ser_derivat = '', 'I', 'M'),
                pi_data_alta
            FROM
                ares
            WHERE
                pi_anagrama not in ('', 'TAP_A') and
                pi_data_alta between
                    adddate(DATE '{DEXTD}', interval -1 year)
                    and DATE '{DEXTD}'
            """.format(DEXTD=DEXTD)
        for id, sec, up, usu, motiu, estat, dat in su.getAll(sql, "import"):
            if id in self.edats:
                grup = self.edats[id]
                motiu = self._get_motiu(motiu, grup)
                motiu_tip = self.motius[motiu]
                if grup == motiu_tip:
                    if id in pob_ids:
                        resi = pob_ids[id]['UP']
                        if id in self.poblacio:
                            up_pac, uba = self.poblacio[id]
                            if up == up_pac:
                                key = (resi, up, uba, "UBA", grup)
                                self.recomptes["GESTINF05"][key][motiu] += 1
                                if estat == "I":
                                    self.recomptes["GESTINF06"][key][motiu] += 1  # noqa
                                # if dat >= first:
                                #     key = ("B" + dat.strftime("%y%m"), resi, motiu)  # noqa
                                #     self.mensual[("GESTINF05",) + key] += 1
                                #     if estat == "I":
                                #         self.mensual[("GESTINF06",) + key] += 1  # noqa
                        col = self.usu_to_col.get((sec, usu))
                        key = (resi, up, col, "COL", grup)
                        if key in self.recomptes["GESTINF03"]:
                            self.recomptes["GESTINF03"][key][motiu] += 1
                            if estat == "I":
                                self.recomptes["GESTINF04"][key][motiu] += 1
                            # if dat >= first:
                            #     key = ("B" + dat.strftime("%y%m"), resi, motiu)  # noqa
                            #     self.mensual[("GESTINF03",) + key] += 1
                            #     if estat == "I":
                            #         self.mensual[("GESTINF04",) + key] += 1
                    # if id in self.id_resi_tot:
                    #     for resi in list(self.id_resi_tot[id]):

    def get_master(self):
        """."""
        self.master = c.defaultdict(c.Counter)
        for ind, dades in self.recomptes.items():
            for (resi, up, prof, tip, grup), minidades in dades.items():
                den = minidades["DEN"]
                for motiu, motiu_tip in self.motius.items():
                    if motiu_tip == grup:
                        key = (resi, ind, up, prof, tip, grup, motiu)
                        self.master[key]["DEN"] = den
                        self.master[key]["NUM"] = minidades[motiu]

    def get_derivats(self):
        """."""
        for (resi, ind, up, prof, tip, grup, motiu), dades in self.master.items():  # noqa
            if ind == "GESTINF03":
                key = (resi, "GESTINF02", up, prof, tip, "MIX", "TIPMOTIU")
                self.master[key]["DEN"] += dades["NUM"]
                if motiu not in ("PALADU", "PALPED"):
                    self.master[key]["NUM"] += dades["NUM"]
            if ind in ("GESTINF03", "GESTINF04"):
                key = (resi, "GESTINF07", up, prof, tip, "grup", "motiu")
                analysis = "DEN" if ind == "GESTINF03" else "NUM"
                self.master[key][analysis] += dades["NUM"]

    def get_khalix(self):
        """."""
        self.khalix = c.Counter()
        for (resi, ind, _up, _prof, _tip, _grup, motiu), dades in self.master.items():  # noqa
            # br = self.centres[up][1]
            for analisi in ("NUM", "DEN"):
                self.khalix[(resi, ind, motiu, analisi)] += dades[analisi]
            # if tip == "COL":
            #     ubas = self.col_to_uba[(up, prof)]
            # else:
            #     ubas = (prof,)
            # for uba in ubas:
            #     if (up, uba) in self.ubas[grup]:
            #         ent = "{}I{}".format(br, uba)
            #         for analisi in ("NUM", "DEN"):
            #             self.khalix[(ent, ind, motiu, analisi)] += dades[analisi]  # noqa

    def get_taxes(self):
        """Hace GESTINF001 a partir de GESTINF005"""
        for (ent, ind, motiu, analisi), n in self.khalix.items():
            if ind == "GESTINF05" and analisi == "DEN" and motiu in TAXES:
                taxa = 1000 * self.khalix[(ent, ind, motiu, "NUM")] / float(n)
                meta = TAXES[motiu]
                good = taxa >= meta
                self.khalix[(ent, "GESTINF01", motiu, "DEN")] = 1
                self.khalix[(ent, "GESTINF01", motiu, "NUM")] = 1 * good

    def export_khalix(self, file_name, dim6set):
        """."""
        tb = "exp_khalix_{}".format(file_name)
        cols = """(
                ent varchar(13),
                ind varchar(10),
                motiu varchar(10),
                analisi varchar(3),
                valor int
                )
            """
        su.createTable(tb, cols, DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in self.khalix.items()]
        su.listToTable(upload,     # (ent, "GESTINF01", motiu, "DEN", v)
                       tb,  # "exp_khalix_gida_resi"
                       DATABASE)
        # file = KHALIX_UP          # GIDA_resi
        # for param, file in (("=", KHALIX_UP), (">", KHALIX_UBA)):
        # cataleg = ".".join(("import", "cat_sisap_map_residencies b"))

        # cat_cond = "WHERE a.ent = b.resi_cod and b.sisap_class = 1"
        # cat_cond = " ".join((cataleg, cat_cond))
        # cat_cond = """exists (select 1 from {cat_cond})""".format(cat_cond=cat_cond)  # noqa
        # cat_cond_den = cat_cond + " and analisi = 'DEN' and (motiu = 'PALADU' or ind in ('GESTINF02','GESTINF01','GESTINF07'))"  # noqa
        # cat_cond_num = cat_cond + "and analisi = 'NUM'"

        # sql = """
        #     SELECT
        #         ind, 'Aperiodo', concat('R', replace(ent,'-','_')) as resi,
        #         analisi, 'NOCAT', 'NOIMP', '{dim6set}', 'N', sum(valor) as val
        #     FROM
        #         {db}.{tb} a
        #     WHERE {cat_cond_den}
        #     GROUP BY
        #         ind, concat('R', replace(ent,'-','_')), analisi
        #     HAVING
        #         sum(valor) > 0
        #     union
        #     SELECT
        #         ind, 'Aperiodo', concat('R', replace(ent,'-','_')) as resi,
        #         analisi, 'NOCAT', 'NOIMP', '{dim6set}', 'N', sum(valor) as val
        #     FROM
        #         {db}.{tb} a
        #     WHERE {cat_cond_num}
        #     GROUP BY
        #         ind, concat('R', replace(ent,'-','_')), analisi
        #     HAVING
        #         sum(valor) > 0
        # """.format(db=DATABASE,
        #            tb=tb, dim6set=dim6set,
        #            cat_cond_den=cat_cond_den,
        #            cat_cond_num=cat_cond_num)
        # su.exportKhalix(sql, file_name)


if __name__ == "__main__":
    ts = init = d.datetime.now()
    print("comienzo: {}".format(init))

    Resi = GIDA()

    pobs = {
        'gida_resi': Resi.id_resi,
        'gida_resi_total': Resi.id_resi_tot,
        }

    for file_name, pob_d in pobs.items():
        Resi.recomptes = c.defaultdict(lambda: c.defaultdict(c.Counter))
        dim6set = 'POBTOTAL' if file_name == 'gida_resi_total' else 'POBACTUAL'  # noqa
        # Resi.get_GIDA(Resi, dim6set=dim6set, file_name=file_name, pob_ids=pob_d)  # noqa
        Resi.get_poblacio(pob_d)
        Resi.get_visites(pob_d)  # <-PB
        Resi.get_aguda(pob_d)  # ojo! dext escapa RECOVER <-PB
        Resi.get_master()
        Resi.get_derivats()
        # SALIDAS
        Resi.get_khalix()
        Resi.get_taxes()
        Resi.export_khalix(file_name=file_name, dim6set=dim6set)
        ts = d.datetime.now()

    print("acabo: {}".format(ts))
    print("total: {}".format(ts - init))
