def filter_export(file_name, dim="4DIM", DATABASE=DATABASE, DADES=upload):
    tb = "exp_khalix_{}".format(file_name)

    KHALIX_6DIM = ('ind varchar(15)',
                   'period varchar(5)',
                   'entity varchar(13)',
                   'analysys varchar(10)',
                   'detail varchar(10)',
                   'dim6set varchar(10)',
                   'valor int',
                   )
    KHALIX_5DIM = tuple(item for item in KHALIX_6DIM if 'period' not in item)
    KHALIX_4DIM = tuple(item for item in KHALIX_5DIM if 'detail' not in item)

    K_OPTS = {'6DIM': {'kdim': KHALIX_6DIM,
                       'period': 'period',
                       'detail': 'detail',
                       },
              '5DIM': {'kdim': KHALIX_5DIM,
                       'period': "'Aperiode'",
                       'detail': 'detail',
                       },
              '4DIM': {'kdim': KHALIX_4DIM,
                       'period': "'Aperiode'",
                       'detail': "'NOIMP'",
                       },
              }

    KDIM = K_OPTS[dim]['kdim']
    PERIOD, DETAIL = K_OPTS[dim]['period'], K_OPTS[dim]['detail']

    with t.Database('p2262', DATABASE) as conn:
        conn.create_table(tb, KDIM, remove=True)
        conn.list_to_table(DADES, tb)

    cataleg = ".".join(("import", "cat_sisap_map_residencies b"))
    cond = "WHERE a.entity = b.resi_cod and b.sisap_class = 1"

    cat_cond = " ".join((cataleg, cond))
    cat_cond = "exists (select 1 from {cat_cond})".format(cat_cond=cat_cond)

    query_string = """
        SELECT
            ind, {period}, concat('R', replace(entity,'-','_')) as resi,
            analysys, 'NOCAT', {detail},
            dim6set, 'N', valor
        FROM
            {db}.{taula} a
        WHERE
            {cat_cond} and a.valor <> 0
        """.format(db=DATABASE, taula=tb, cat_cond=cat_cond,
                   detail=DETAIL, period=PERIOD)

    u.exportKhalix(query_string, file_name)
