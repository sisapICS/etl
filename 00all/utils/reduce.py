import sisapUtils as u


tables = ("seguiment", "visites", "laboratori", "activitats", "variables")


for table in tables:
    try:
        u.delSubTables(table, sure="True")
    except Exception as e:
        print(str(e))
