# coding: latin1

import sys

import sisaptools as t
import datetime as d
import pytz
import sisapUtils as u
import sys
import os
import telegram

TABLE = "LV_PRE_CTL"
CONNEXIO = ("exadata", "data")
ACCIO = sys.argv[1]

port = os.environ.get('DATABASE_PORT')

if port == '3309':
    with t.Database(*CONNEXIO, retry=1) as exadata:
        if ACCIO == "inici":
            cols = ("referencia date", "inici date", "final date", "mensual int")
            exadata.create_table(TABLE, cols, remove=True)
            exadata.set_grants("select", TABLE, "DWSISAP_ROL", inheritance=False)
            sql = "select data_ext, current_timestamp, null, mensual \
                from dextraccio"  
            dades = [[row for row in t.Database("p2262", "nodrizas").get_one(sql)]]
            exadata.list_to_table(dades, TABLE)
        elif ACCIO == "final":
            sql = "update {} set final = current_timestamp".format(TABLE)
            exadata.execute(sql)
            telegram.Bot("1661303293:AAFx9UBxR0GvNlUqg47sbD_zh4WzlqZ2zXQ").send_message(-4027737276, "Setmanal acabat!")
            avui = d.datetime.today()
            card = u.WekanCard("SISAP", "Carril principal", "Pre", 'Revisar ladybug final ' + avui.strftime("%d/%m/%Y"))  # noqa
            dext = u.getOne("select data_ext from dextraccio", "nodrizas")[0].strftime("%d/%m/%Y")  # noqa
            card.add_description("Data de calcul: " + dext)
            card.add_members("Souhel")
            card.add_labels("Definició")
            dia = avui + d.timedelta(days=2)
            while not u.isWorkingDay(dia):
                dia = dia + d.timedelta(days=1)
            dia_hora = d.datetime(dia.year, dia.month, dia.day, 8, 0, 0, 0)
            amb_tz = pytz.timezone('Europe/Madrid').localize(dia_hora)
            card.add_date(amb_tz)
            checklist = "Tasques"
            card.add_to_checklist(checklist, "Revisar ladybug: http://eines.portalics/ladybug/")
            card.add_to_checklist(checklist, "Comunicar incidencies")
            card.add_to_checklist(checklist, "Tancar targeta")
            card.add_peticionari("ICS")
            card.add_comment("@Souhel targeta nova")
