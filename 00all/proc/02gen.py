from sisapUtils import createDatabase, createTable, execute, readCSV
from sys import argv


db = argv[1]
table = 'ctl_' + db
tema = argv[2]


createDatabase(db, rm=False)
createTable(table, "(file varchar(50) not null default '', extension varchar(50) not null default '', ts_start timestamp default 0, ts_finish timestamp default 0, \
                    status double null, error varchar(1000) not null default '', param varchar(250) not null default '', primary key (file, extension, param))", db, rm=False)
execute("create or replace view {0}_web as select concat(file, '.', extension, if(param <> '', concat(' (', param, ')'), '')) arxiu, \
        if(status = 1, 5, \
        if(status = 4, 4, \
        if(ts_finish > 0, 0, \
        if(ts_start > 0, 1, \
        if(ts_start=0, 3, 9))))) status \
        ,if(ts_finish > 0, unix_timestamp(ts_finish) - unix_timestamp(ts_start), if(ts_start > 0, unix_timestamp(current_timestamp) - unix_timestamp(ts_start), 0)) temps, error \
        from {0}".format(table), db)
execute("create or replace view {0}_web_tot as select '{1}' tema, if(max(if(status = 1, 1, 0)) = 1, max(if(status = 1, status, -1)), max(status)) status,sum(temps) temps \
        from {0}_web".format(table, tema), db)

for file, extension, exe, param, delayable in readCSV('./proc/__source.txt', sep=';'):
    execute("insert ignore into {} select '{}', '{}', 0, 0, 9, '', '{}' from dual".format(table, file, extension, param), db)
