# -*- coding: utf-8 -*-

import sisapUtils as u
import collections as c

map_activitat_tipus = {"SE": "PJ", "OTR": "AL", "SB": "PG", "PR": "PG", "CO": "MC", "CA": "PG"}
map_activitat = {"PS": "AL", "VA": "AL", "OTR": "AL", "II": "AL", "CR": "AL", "CO": "AL"}
keys_act_grupal_i_o_comunitaria = {"C": "B", "G": "A", "GC": "C"}

indicadors_prhig_jail = "('PRHIG003', 'PRHIG008', 'PRHIG009')"
indicadors_achig_jail = "('ACHIG01')"

# Fluoracions

class higienistes():

    def __init__(self):

        self.get_dextraccio();                      print("self.get_dextraccio()")
        self.create_table();                        print("self.create_table()")
        self.get_centres();                         print("self.get_centres()")
        self.get_professionals();                   print("self.get_professionals()")
        self.get_poblacio();                        print("self.get_poblacio()")
        self.get_embarassades();                    print("self.get_embarassades()")
        self.get_risc_caries();                     print("self.get_risc_caries()")
        self.get_antecedents_caries();              print("self.get_antecedents_caries()")
        self.get_segellats();                       print("self.get_segellats()")
        self.get_carralls();                        print("self.get_carralls()")
        self.get_neteges();                         print("self.get_neteges()")
        self.get_grups();                           print("self.get_grups()")
        self.fluoracions_tartrectomies();           print("self.fluoracions_tartrectomies()")
        self.segellats();                           print("self.segellats()")
        self.get_prhig011();                        print("self.get_prhig011()")
        self.get_prhig010_012();                    print("self.get_prhig010_012()")
        self.get_prhig013_14_15_16_17();            print("self.get_prhig013_14_15_16_17()")
        self.get_achig();                           print("self.get_achig()")
        self.to_khalix();                           print("self.to_khalix()")
    
    def get_dextraccio(self):
        """."""

        sql = """
                SELECT
                    data_ext,
                    date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY)
                FROM
                    dextraccio
              """
        self.data_ext, self.data_ext_menys1any = u.getOne(sql, "nodrizas")

    def create_table(self):
        """."""

        self.db = "odn"
        self.taula_pr = "higienistes_pr"
        self.taula_ac = "higienistes_ac"

        # PRHIG
        cols_pr = "(indicador varchar(13), up varchar(5), analisis varchar(5), valor int)"
        u.createTable(self.taula_pr, cols_pr, self.db, rm=True)

        # ACHIG
        cols_ac = "(indicador varchar(13), br varchar(5), analisis varchar(3), valor double)"
        u.createTable(self.taula_ac, cols_ac, self.db, rm=True)

    def get_centres(self):
        """ . """

        sql = """
                SELECT
                    scs_codi,
                    ics_codi
                FROM
                    cat_centres
              UNION
                SELECT
                    scs_codi,
                    ics_codi
                FROM
                    jail_centres
              """
        self.centres = {up: br for up, br in u.getAll(sql, "nodrizas")}

    def get_professionals(self):
        """ S'obtenen els identificadors dels professionals amb la categoria professional de 
            higienista dental (ide_categ_prof_c=10778). """

        sql = """
                SELECT
                    ide_usuari
                FROM
                    cat_pritb992
                WHERE
                    ide_categ_prof_c = '10778'
              """
        self.professionals = {ide_usuari for ide_usuari, in u.getAll(sql, "import")}

    def get_poblacio(self):
        """ Obt� informaci� de poblaci� (edat, sexe) dels pacients a partir de la taula de la base de dades. """

        self.poblacio = {}
        self.poblacio_up = c.Counter()

        sql = """
                SELECT
                    id_cip_sec,
                    up,
                    upOrigen,
                    edat
                FROM
                    assignada_tot
                WHERE
                    ates = 1
                    AND institucionalitzat = 0
                    AND atdom = 0
              """
        for id_cip_sec, up, upOrigen, edat in u.getAll(sql, "nodrizas"):
            if up in self.centres:
                self.poblacio[id_cip_sec] = {"up": up, "upOrigen": upOrigen, "edat": edat}
                self.poblacio_up[up] += 1

        sql = """
                SELECT
                    id_cip_sec,
                    up
                FROM
                    jail_atesa_avui
                WHERE 
                    durada >= 7
                    AND (sortida = '0000-00-00' OR sortida BETWEEN '{}' AND '{}')
            """.format(self.data_ext_menys1any, self.data_ext)
        poblacio_ultim_any = {id_cip_sec: {"up": up} for id_cip_sec, up in u.getAll(sql, "nodrizas") if up in self.centres}

        sql = """
                SELECT
                    id_cip_sec,
                    usua_data_naixement
                FROM
                    assignada
            """
        for id_cip_sec, data_naix in u.getAll(sql, "import_jail"):
            if id_cip_sec in poblacio_ultim_any:
                up = poblacio_ultim_any[id_cip_sec]["up"]
                self.poblacio[id_cip_sec] = {"up": up, 
                                             "upOrigen": up, 
                                             "edat": u.yearsBetween(data_naix, self.data_ext)}
                self.poblacio_up[up] += 1

    def get_embarassades(self):
        """."""

        self.embaras, self.embaras_jail = c.defaultdict(set), c.defaultdict(set)

        sql = """
                SELECT
                    id_cip_sec,
                    emb_d_fi
                FROM
                    embaras
                WHERE
                    (emb_d_tanca IS NULL OR emb_d_fi IS NULL OR (emb_d_fi - emb_dur) > 90)
                    AND emb_c_tanca NOT IN ('A', 'Al', 'IV', 'MF', 'MH', 'EE')
                    AND emb_d_fi >= DATE'{_data_ext_menys1any}'
                    AND emb_d_ini <= DATE'{_data_ext}'
            """.format(_data_ext_menys1any=self.data_ext_menys1any, _data_ext=self.data_ext)
        for db in ("import", "import_jail"):
            for id_cip_sec, emb_d_fi in u.getAll(sql, db):
                if id_cip_sec in self.poblacio:
                    self.embaras[id_cip_sec].add(emb_d_fi)

    def get_risc_caries(self):
        """ S'obtenen els pacients que tenen risc de c�ries dental o que ja presenten c�ries o obturacions en les seves dents 
            temporals o definitives. En concret, busca els registres de la taula "odn_variables" on l'agrupador �s "Risc c�ries" 
            o "COD" o "CAOD" amb valors concrets. Despr�s, crea un conjunt amb els identificadors dels pacients que tenen entre 
            6 i 14 anys, i que es troben en la poblaci� assignada.

             - 286: Risc c�ries
             - 307: COD. Una dent temporal cariada i/o obturada comporta un �ndex COD amb valor superior a 1
             - 308: CAOD. Una dent definitiva cariada, absent per c�ries i/o obturada comporta un �ndex CAOD superior a 0
        """

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    odn_variables
                WHERE
                    (agrupador = 286 AND valor = 1501)
                    OR (agrupador = 307 AND valor >= 1)
                    OR (agrupador = 308 AND valor >= 1)
              """
        self.risc_caries_6a14anys = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in self.poblacio if 6 <= self.poblacio[id_cip_sec]["edat"] <= 14}
        self.risc_caries_jail = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in self.poblacio and id_cip_sec < 0}
        self.risc_caries = self.risc_caries_6a14anys | self.risc_caries_jail

    def get_antecedents_caries(self):
        """."""

        self.antecedents_caries = set()

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    odn505
                WHERE
                    cfd_ind_cod > 0
                    AND cfd_data_c <= DATE'{_data_ext}'
              """.format(_data_ext=self.data_ext)
        for db in ("import", "import_jail"):
            self.antecedents_caries.update({id_cip_sec for id_cip_sec, in u.getAll(sql, db) 
                                            if id_cip_sec in self.poblacio})

    def get_segellats(self):
        """."""

        self.segellats_ultimany = c.defaultdict(c.Counter)
        self.segellats_ultimany_e = c.defaultdict(lambda: c.defaultdict(set))

        sql = """
                SELECT
                    id_cip_sec,
                    dtd_usu,
                    dtd_up,
                    dtd_cod_p,
                    dtd_et,
                    date_format(dtd_data, '%Y%m%d')
                FROM
                    odn508
                WHERE
                    dtd_tra = 'SEG'
                    AND dtd_data BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
              """.format(_data_ext_menys1any=self.data_ext_menys1any, _data_ext=self.data_ext)
        for db in ("import", "import_jail"):
            for id_cip_sec, ide_usuari, up, peca, et, data in u.getAll(sql, db):
                if id_cip_sec in self.poblacio:
                    self.segellats_ultimany[id_cip_sec][(ide_usuari, up, peca, et)] += 1
                    if et == "E":
                        tipus_dent = 'DDEF' if peca <= 50 else 'DTEMP'
                        self.segellats_ultimany_e[tipus_dent][id_cip_sec].add((peca, data, ide_usuari))

    def get_carralls(self):
        """."""

        self.odn509 = {}
        self.carralls = c.defaultdict(c.Counter)

        sql = """
                SELECT
                    id_cip_sec,
                    codi_sector,
                    cb_cod,
                    cb_data,
                    cb_val
                FROM
                    odn509
                WHERE
                    cb_val = '1303'
                    AND cb_data_fi IS NULL
                    AND cb_data <= DATE'{_data_ext}'
            """.format(_data_ext=self.data_ext)
        for db in ("import", "import_jail"):
            for id_cip_sec, sector, id_codi, cb_data, cb_val in u.getAll(sql, db):
                if id_cip_sec in self.poblacio:
                    self.carralls[id_cip_sec][((sector, id_codi), cb_data)] += 1

    def get_neteges(self):
        """ FC: Fluoraci� amb cubetes / FSC: Fluoraci� sense cubetes / TT: Tartrectomies """

        self.neteges = c.defaultdict(c.Counter)

        sql = """
                SELECT
                    tb_usu,
                    id_cip_sec,
                    tb_up,
                    tb_trac,
                    codi_sector,
                    tb_cod,
                    tb_data
                FROM
                    odn511
                WHERE
                    tb_trac IN ('FC', 'FSC', 'TT')
                    AND tb_data BETWEEN '{_data_ext_menys1any}' AND DATE'{_data_ext}'
              """.format(_data_ext_menys1any=self.data_ext_menys1any, _data_ext=self.data_ext)
        for db in ("import", "import_jail"):
            for ide_usuari, id_cip_sec, up, trac, sector, id_codi, tb_data in u.getAll(sql, db):
                if id_cip_sec in self.poblacio:
                    self.neteges[id_cip_sec][(sector, id_codi, ide_usuari, up, trac, tb_data)] += 1

    def get_grups(self):
        """."""

        self.grups = {}

        sql = """
                SELECT
                    codi_sector,
                    codi_grup
                FROM
                    pacients_activitats_nous_professionals
                WHERE
                    data_intervencio BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
                    AND tipus_intervencio in ('G', 'GC')
                    AND procedencia = 'grupal4'
                """.format(_data_ext_menys1any=self.data_ext_menys1any, _data_ext=self.data_ext)
        
        # Recorre els resultats de la consulta
        activitats_amb_pacients = {(codi_sector, grup_id_sector) for codi_sector, grup_id_sector in u.getAll(sql, "nodrizas")}
                                        
        sql = """
                SELECT
                    codi_sector,
                    codi_grup,
                    up_intervencio,
                    n_assistents,
                    hores,
                    tipus_intervencio,
                    tipus_activitat,
                    tema_activitat
                FROM
                    activitats_grupals_nous_professionals
                WHERE
                    data_intervencio BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
                    AND (rol_creador = 'HIG' OR rol_participacio LIKE '%HIG%')
              """.format(_data_ext_menys1any=self.data_ext_menys1any, _data_ext=self.data_ext)
        for codi_sector, grup_id_sector, up, n_assistents, hores, act_grupal_i_o_comunitaria, activitat_tipus, activitat_tematica in u.getAll(sql, "nodrizas"):
            if up in self.centres:
                grup_id = (codi_sector, grup_id_sector)
                activitat_tipus = map_activitat_tipus.get(activitat_tipus, activitat_tipus)
                activitat_tematica = map_activitat.get(activitat_tematica, activitat_tematica)
                activitat_amb_pacients = 1 if grup_id in activitats_amb_pacients else 0
                self.grups[grup_id] = (up, n_assistents, hores, act_grupal_i_o_comunitaria, activitat_tipus, activitat_tematica, activitat_amb_pacients)

    def fluoracions_tartrectomies(self):
        """
        PRHIG001 - Nombre de fluoracions realitzades per higienista durant els �ltims 12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4403/ver/

        PRHIG002 - Nombre d' usuaris amb fluoracions realitzades per higienista durant els �ltims 12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4432/ver/

        PRHIG003 - Percentatge de fluoracions realitzades per higienista respecte al total de l' equip �ltims 12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4433/ver/

        PRHIG007 - Nombre de tartrectomies realitzades per higienista durant els �ltims 12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4437/ver/

        PRHIG008 - Nombre d' usuaris amb tartrectomies realitzades per higienista durant els �ltims 12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4438/ver/

        PRHIG009 - Percentatge de tartrectomies realitzades per higienista respecte total de l' equip �ltims 12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4439/ver/

        PRHIG010 - Percentatge d'activitats preventives en poblaci� de 0 a 5 anys amb hist�ria de c�ries per higienista
        http://10.80.217.201/sisap-umi/indicador/indicador/4440/ver/
        """

        PRHIG001 = c.Counter()
        PRHIG002 = c.defaultdict(set)
        PRHIG003_den = c.Counter()
        PRHIG003_num = c.Counter()
        PRHIG007 = c.Counter()
        PRHIG008 = c.defaultdict(set)
        PRHIG009_den = c.Counter()
        PRHIG009_num = c.Counter()     

        for id_cip_sec in self.neteges:
            upOrigen = self.poblacio[id_cip_sec]["upOrigen"]
            for (_, _, ide_usuari, _, trac, _), n in self.neteges[id_cip_sec].items():
                if trac in ("FC", "FSC"):
                    if ide_usuari in self.professionals:
                        PRHIG001[upOrigen] += n
                        if id_cip_sec in self.risc_caries_6a14anys:
                            PRHIG002[upOrigen].add(id_cip_sec)
                    if id_cip_sec in self.risc_caries:
                        PRHIG003_den[upOrigen] += n
                        if ide_usuari in self.professionals:
                            PRHIG003_num[upOrigen] += n
                elif trac == "TT":
                    PRHIG009_den[upOrigen] += n
                    if ide_usuari in self.professionals:
                        PRHIG007[upOrigen] += n
                        PRHIG008[upOrigen].add(id_cip_sec)
                        PRHIG009_num[upOrigen] += n

        upload_pr = []
        for upOrigen in PRHIG001:
            upload_pr.append(("PRHIG001", upOrigen, "NUM", PRHIG001[upOrigen]))
        for upOrigen in PRHIG002:
            upload_pr.append(("PRHIG002", upOrigen, "NUM", len(PRHIG002[upOrigen])))            
        for upOrigen in PRHIG003_den:
            upload_pr.append(("PRHIG003", upOrigen, "DEN", PRHIG003_den[upOrigen]))
            if upOrigen in PRHIG003_num:
                upload_pr.append(("PRHIG003", upOrigen, "NUM", PRHIG003_num[upOrigen]))
        for upOrigen in PRHIG007:
            upload_pr.append(("PRHIG007", upOrigen, "NUM", PRHIG007[upOrigen]))
        for upOrigen in PRHIG008:
            upload_pr.append(("PRHIG008", upOrigen, "NUM", len(PRHIG008[upOrigen])))          
        for upOrigen in PRHIG009_den:
            upload_pr.append(("PRHIG009", upOrigen, "DEN", PRHIG009_den[upOrigen]))
            if upOrigen in PRHIG009_num:
                upload_pr.append(("PRHIG009", upOrigen, "NUM", PRHIG009_num[upOrigen]))                     

        u.listToTable(upload_pr, self.taula_pr, self.db)
    
    def segellats(self):
        """
        PRHIG004 - Nombre de segellats realitzats per higienista durant els �ltims 12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4435/ver/

        PRHIG005 - Nombre d' usuaris amb segellats realitzats per higienista durant els �ltims 12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4436/ver/

        PRHIG006 - Percentatge de segellats realitzats per higienista respecte al total de l' equip darrers 12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4434/ver/
        """

        PRHIG004 = c.Counter()
        PRHIG005 = c.defaultdict(set)
        PRHIG006_den = c.Counter()
        PRHIG006_num = c.Counter()

        for id_cip_sec in self.segellats_ultimany:
            upOrigen = self.poblacio[id_cip_sec]["upOrigen"]
            for (ide_usuari, _, cod_p, et), n in self.segellats_ultimany[id_cip_sec].items():
                if ide_usuari in self.professionals:
                    PRHIG004[upOrigen] += n
                    if id_cip_sec in self.risc_caries_6a14anys:
                        PRHIG005[upOrigen].add(id_cip_sec)

        # Per PRHIG006: dtd_et = 'E' perqu� quadri amb el PRODOA corresponent
        for tipus_dent in self.segellats_ultimany_e:
            for id_cip_sec, detall_tractament in self.segellats_ultimany_e[tipus_dent].items():
                upOrigen = self.poblacio[id_cip_sec]["upOrigen"]
                for peca, data, ide_usuari in detall_tractament:
                    PRHIG006_den[upOrigen] += 1
                    if ide_usuari in self.professionals:
                        PRHIG006_num[upOrigen] += 1

        upload_pr = []
        for upOrigen in PRHIG004:
            upload_pr.append(("PRHIG004", upOrigen, "NUM", PRHIG004[upOrigen]))
        for upOrigen in PRHIG005:
            upload_pr.append(("PRHIG005", upOrigen, "NUM", len(PRHIG005[upOrigen])))            
        for upOrigen in PRHIG006_den:
            upload_pr.append(("PRHIG006", upOrigen, "DEN", PRHIG006_den[upOrigen]))
            if upOrigen in PRHIG006_num:
                upload_pr.append(("PRHIG006", upOrigen, "NUM", PRHIG006_num[upOrigen]))                  
        u.listToTable(upload_pr, self.taula_pr, self.db)

    def get_prhig011(self):
        """
        PRHIG011 - Percentatge de nens amb ARC (alt risc de c�ries) d'entre 6 i 14 anys a qui 
                   s'ha realitzat un segellat de fissures en una molar definitiva
        http://10.80.217.201/sisap-umi/indicador/indicador/4660/ver/
        """

        PRHIG011_den = c.Counter()
        PRHIG011_num = c.Counter()

        sql = """
                SELECT
                    id_cip_sec,
                    den,
                    num
                FROM
                    odn_arc_indicador
              """
        for id_cip_sec, den, num in u.getAll(sql, "odn"):
            dades_pacient = self.poblacio.get(id_cip_sec)
            if dades_pacient:
                upOrigen = dades_pacient["upOrigen"]
                PRHIG011_den[upOrigen] += den
                PRHIG011_num[upOrigen] += num

        upload_pr = []
        for upOrigen in PRHIG011_den:
            upload_pr.append(("PRHIG011", upOrigen, "DEN", PRHIG011_den[upOrigen]))
            if upOrigen in PRHIG011_num:
                upload_pr.append(("PRHIG011", upOrigen, "NUM", PRHIG011_num[upOrigen]))
        u.listToTable(upload_pr, self.taula_pr, self.db)

    def get_prhig010_012(self):
        """
        PRHIG012 - Percentatge d'infants entre 0 i 5 anys amb antecedents de c�ries en qu� s'hagi fet 
                   registre d'aplicaci� de vern�s de fluor (sense cubetes) m�s instrucci� en higiene 
                   oral i/o consell diet�tic
        http://10.80.217.201/sisap-umi/indicador/indicador/4661/ver/
        """        
        
        dades_ind = c.Counter()
        nens_den = set()
        nens_prob = c.defaultdict(lambda: c.defaultdict(dict))

        # Creem DEN PRHIG012

        for id_cip_sec in self.antecedents_caries:
            upOrigen, edat = (self.poblacio[id_cip_sec][key] for key in ("upOrigen", "edat"))
            if edat <= 5:
                dades_ind[("PRHIG012", "DEN", upOrigen)] += 1
                nens_den.add(id_cip_sec) 

        # Creem NUM: Nens que hagin tingut FSC i (CD i/o EHO)
        
        sql = """
                SELECT
                    id_cip_sec,
                    tb_trac,
                    tb_usu
                FROM
                    odn511
                WHERE
                    tb_trac IN ('FSC', 'CD', 'EHO')
              """
        for id_cip_sec, trac, ide_usuari in u.getAll(sql, "import"):
            if id_cip_sec in nens_den:
                nens_prob["tot"].setdefault(id_cip_sec, {"FSC": 0, "CD": 0, "EHO": 0})
                nens_prob["higienista"].setdefault(id_cip_sec, {"FSC": 0, "CD": 0, "EHO": 0})
                nens_prob["tot"][id_cip_sec][trac] = 1
                if ide_usuari in self.professionals:
                    nens_prob["higienista"][id_cip_sec][trac] = 1

        for id_cip_sec in nens_prob["tot"]:
            if nens_prob["tot"][id_cip_sec]["FSC"] and (nens_prob["tot"][id_cip_sec]["CD"] + nens_prob["tot"][id_cip_sec]["EHO"]) >= 1:
                if id_cip_sec in self.antecedents_caries:
                    upOrigen = self.poblacio[id_cip_sec]["upOrigen"]
                    dades_ind[("PRHIG012", "NUM", upOrigen)] += 1
                    dades_ind[("PRHIG010", "DEN", upOrigen)] += 1
                    if id_cip_sec in nens_prob["higienista"]:
                        if nens_prob["higienista"][id_cip_sec]["FSC"] and (nens_prob["higienista"][id_cip_sec]["CD"] + nens_prob["higienista"][id_cip_sec]["EHO"]) >= 1:
                            dades_ind[("PRHIG010", "NUM", upOrigen)] += 1

        upload_pr = []
        for ind in ("PRHIG010", "PRHIG012"):
            for (ind, tipus, upOrigen), pacients in dades_ind.items():
                if tipus == "DEN":
                    upload_pr.append((ind, upOrigen, "DEN", pacients))
                    if dades_ind[(ind, "NUM", upOrigen)]:
                        upload_pr.append((ind, upOrigen, "NUM", dades_ind[(ind, "NUM", upOrigen)]))

        u.listToTable(upload_pr, self.taula_pr, self.db)

    def get_prhig013_14_15_16_17(self):
        """
        PRHIG013 - Percentatge embarassades amb carrall amb tartrectomia feta per l' equip durant els darrers 12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4663/ver/

        PRHIG014 - Percentatge de tartrectomies en embarassades amb carrall fetes per higienista respecte al total de 
                   l' equip durant els darrers 12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4665/ver/

        PRHIG015 - Percentatge poblaci� pedi�trica amb carrall amb tartrectomia feta per l' equip durant els darrers 
                   12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4666/ver/

        PRHIG016 - Percentatge de tartrectomies en poblaci� pedi�trica amb carrall fetes per higienista durant els 
                   darrers 12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4667/ver/

        PRHIG017 - Rati d' usuaris amb segellats de fosses i fisures / usuaris amb obturacions durant els darrers 
                   12 mesos
        http://10.80.217.201/sisap-umi/indicador/indicador/4668/ver/
        """        

        set_den013, set_den015 = set(), set()
        dades_ind = c.defaultdict(set)

        for id_cip_sec in self.carralls:
            upOrigen, edat = (self.poblacio[id_cip_sec][key] for key in ("upOrigen", "edat"))
            for ((sector, id_codi), cb_data), n in self.carralls[id_cip_sec].items():
                for emb_d_tanca in self.embaras[id_cip_sec]:
                    if cb_data <= emb_d_tanca:
                        dades_ind[("PRHIG013", "DEN", upOrigen)].add(id_cip_sec)
                        set_den013.add(id_cip_sec)
                if edat <= 14:
                    dades_ind[("PRHIG015", "DEN", upOrigen)].add(id_cip_sec)
                    set_den015.add(id_cip_sec)

        for id_cip_sec in self.neteges:
            upOrigen = self.poblacio[id_cip_sec]["upOrigen"]
            for (sector, id_codi, ide_usuari, _, trac, tb_data), n in self.neteges[id_cip_sec].items():
                if trac == "TT":
                    if id_cip_sec in set_den013:
                        for emb_d_tanca in self.embaras[id_cip_sec]:
                            if tb_data and emb_d_tanca and tb_data <= emb_d_tanca:
                                dades_ind[("PRHIG013", "NUM", upOrigen)].add(id_cip_sec)
                                dades_ind[("PRHIG014", "DEN", upOrigen)].add((sector, id_codi))
                                if ide_usuari in self.professionals:
                                    dades_ind[("PRHIG014", "NUM", upOrigen)].add((sector, id_codi))
                                    
                    if id_cip_sec in set_den015:
                        if self.data_ext_menys1any <= tb_data:
                            dades_ind[("PRHIG015", "NUM", upOrigen)].add(id_cip_sec)
                            dades_ind[("PRHIG016", "DEN", upOrigen)].add((sector, id_codi))
                            if ide_usuari in self.professionals:
                                dades_ind[("PRHIG016", "NUM", upOrigen)].add((sector, id_codi))

        sql = """
                SELECT
                    id_cip_sec,
                    codi_sector,
                    dtd_cod_p
                FROM
                    odn508
                WHERE
                    dtd_tra IN ('OBT', 'CON')
                    AND dtd_et = 'E'
                    AND dtd_data BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
              """.format(_data_ext_menys1any=self.data_ext_menys1any, _data_ext=self.data_ext)
        for id_cip_sec, sector, id_codi in u.getAll(sql, "import"):
            dades_pacient = self.poblacio.get(id_cip_sec)
            if dades_pacient:
                upOrigen = dades_pacient["upOrigen"] 
                dades_ind[("PRHIG017", "DEN", upOrigen)].add(id_cip_sec)
        for tipus_dent in self.segellats_ultimany_e:
            for id_cip_sec in self.segellats_ultimany_e[tipus_dent]:
                dades_pacient = self.poblacio.get(id_cip_sec)
                if dades_pacient:
                    upOrigen = dades_pacient["upOrigen"]   
                    dades_ind[("PRHIG017", "NUM", upOrigen)].add(id_cip_sec)                     
          
        upload_pr = []
        for ind in ("PRHIG013", "PRHIG014", "PRHIG015", "PRHIG016", "PRHIG017"):
            for (ind, tipus, upOrigen), pacients in dades_ind.items():
                if tipus == "DEN":
                    upload_pr.append((ind, upOrigen, "DEN", len(pacients)))
                    if dades_ind[(ind, "NUM", upOrigen)]:
                        upload_pr.append((ind, upOrigen, "NUM", len(dades_ind[(ind, "NUM", upOrigen)])))

        u.listToTable(upload_pr, self.taula_pr, self.db)                

    def get_achig(self):

        upload_ac = c.Counter()
        upload_ac_cero = c.Counter()

        indicadors_poblacio_atesa_assignada = ["ACHIG01", "ACHIG01A", "ACHIG01B", "ACHIG01C", "ACHIG03"]

        # DEN

        for up in self.poblacio_up:
            br = self.centres[up]
            for indicador in indicadors_poblacio_atesa_assignada:
                upload_ac[(br, indicador, "DEN")] = self.poblacio_up[up]

        # NUM

        for grup_id, (up, n_assistents, hores, act_grupal_i_o_comunitaria, activitat_tipus, activitat_tematica, activitat_amb_pacients) in self.grups.items():
            
            if (act_grupal_i_o_comunitaria in ("G", "GC") and activitat_amb_pacients) or act_grupal_i_o_comunitaria == "C":
                br = self.centres[up]
                upload_ac[(br, "ACHIG01", "NUM")] += 1
                upload_ac[(br, "ACHIG03", "NUM")] += hores

                if act_grupal_i_o_comunitaria in keys_act_grupal_i_o_comunitaria:
                    upload_ac[(br, "ACHIG01{}".format(keys_act_grupal_i_o_comunitaria[act_grupal_i_o_comunitaria]), "NUM")] += 1

        # Casos amb NUM = 0
        upload_ac.update({(br, ind, "NUM"): 0 for br, ind, tip in upload_ac if tip == "DEN" and (br, ind, "NUM") not in upload_ac})

        upload_ac = [(ind, br, analisis, n) for (br, ind, analisis), n in upload_ac.items()]

        u.listToTable(upload_ac, self.taula_ac, self.db)


    def to_khalix(self):

        # PRHIG
        sql = """
                SELECT DISTINCT
                    indicador,
                    concat('A', 'periodo'),
                    ICS_CODI,
                    ANALISIS,
                    'NOCAT',
                    'NOIMP',
                    'DIM6SET',
                    'N',
                    VALOR
                FROM
                    {_db}.{_taula_pr} h,
                    nodrizas.cat_centres CC
                WHERE
                    CC.scs_codi = H.up
              """.format(_db=self.db, _taula_pr=self.taula_pr)
        file = "PRHIG"
        u.exportKhalix(sql, file)

        sql = """
                SELECT DISTINCT
                    CONCAT(indicador, 'PRS'),
                    concat('A', 'periodo'),
                    ICS_CODI,
                    ANALISIS,
                    'NOCAT',
                    'NOIMP',
                    'DIM6SET',
                    'N',
                    VALOR
                FROM
                    {_db}.{_taula_pr} h,
                    nodrizas.jail_centres CC
                WHERE
                    CC.scs_codi = H.up
                    AND indicador in {_indicadors_prhig_jail}
              """.format(_db=self.db, _taula_pr=self.taula_pr, _indicadors_prhig_jail=indicadors_prhig_jail)
        file = "PRHIG_JAIL"
        u.exportKhalix(sql, file)

        # ACHIG
        sql = """
                SELECT DISTINCT
                    indicador,
                    "Aperiodo",
                    br,
                    analisis,
                    "NOCAT",
                    "NOIMP",
                    "DIM6SET",
                    "N",
                    VALOR
                FROM
                    {_db}.{_taula_ac} h,
                    nodrizas.cat_centres CC
                WHERE
                    CC.ics_codi = H.br
              """.format(_db=self.db, _taula_ac=self.taula_ac)
        file = "ACHIG"
        u.exportKhalix(sql, file)

        sql = """
                SELECT DISTINCT
                    CONCAT(indicador, 'PRS'),
                    "Aperiodo",
                    br,
                    analisis,
                    "NOCAT",
                    "NOIMP",
                    "DIM6SET",
                    "N",
                    VALOR
                FROM
                    {_db}.{_taula_ac} h,
                    nodrizas.jail_centres CC
                WHERE
                    CC.ics_codi = H.br
                    AND indicador in {_indicadors_achig_jail}
              """.format(_db=self.db, _taula_ac=self.taula_ac, _indicadors_achig_jail=indicadors_achig_jail)
        file = "ACHIG_JAIL"
        u.exportKhalix(sql, file)  
        
if __name__ == "__main__":
    higienistes()