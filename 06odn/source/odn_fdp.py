# coding: UTF-8

from datetime import datetime, date
import collections as c
import sisapUtils as u

tb = "odn_fdp"
db = "odn"

def get_dates():

    global data_ext, data_ext_menys1any, periode
    
    sql = """
            SELECT
                data_ext,
                date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY)
            FROM
                dextraccio
        """
    data_ext, data_ext_menys1any = u.getOne(sql, 'nodrizas')
    periode = 'A{}{}'.format(str(data_ext.year)[2:], str(data_ext.month).zfill(2))

def get_centres():

    global cat_centres;             cat_centres = dict()

    sql = """
            SELECT
                scs_codi,
                ics_codi,
                amb_desc
            FROM
                cat_centres
        """
    for up, br, amb_desc in u.getAll(sql, 'nodrizas'):
        cat_centres[up] = {'br': br, 'ambit': amb_desc}

def get_assignada_tot():

    global assignada_tot;           assignada_tot = dict()

    sql = """
            SELECT
                id_cip_sec,
                uporigen,
                edat,
                sexe,
                ates,
                institucionalitzat
            FROM
                assignada_tot
          """
    for id_cip_sec, up, edat, sexe, ates, institucionalitzat in u.getAll(sql, 'nodrizas'):
        if institucionalitzat:
            if ates:
                poblatips = ("INSAT", "INSASS")
            else:
                poblatips = ("INSASS",)
        if not institucionalitzat:
            if ates:
                poblatips = ("NOINSAT", "NOINSASS")
            else:
                poblatips = ("NOINSASS",)
        assignada_tot[id_cip_sec] = {'up': up, 'edat': edat, 'sexe': sexe, 'ates': ates, 'institucionalitzat': institucionalitzat, 'poblatips': poblatips}

def get_problemes():
    """ Obt� les dades dels problemes de salut especificats a l'objecte ps_dict que han estat 
        detectats als pacients assignats i atesos durant el per�ode compr�s pel nombre de mesos 
        anteriors al final del per�ode d'an�lisi especificat a la clau "periode" de ps_dict. """

    global problemes;               problemes = c.defaultdict(set)

    codis_ps = ('C01-K04.6', 'C01-K04.7', 'C01-K08.89', 'K04.6', 'K04.7', 'K08.8', 'K08.89')

    sql = """
            SELECT
                id_cip_sec,
                pr_cod_ps,
                pr_dde
            FROM
                {}
            WHERE 
                pr_cod_ps IN {} 
                AND YEAR(pr_dde) = {}
          """
    jobs = [sql.format(table, codis_ps, data_ext.year) for table in u.getSubTables("problemes") if table[-6:] != '_s6951']
    count = 0
    for sub_problemes in u.multiprocess(sub_get_problemes, jobs, 4):
        for id_cip_sec, up, _, _ in sub_problemes:
            problemes[up].add(id_cip_sec)
        count+=1
        print("    probs: {}/{}. {}".format(count, len(jobs), datetime.now()))
        
    return problemes

def sub_get_problemes(sql):

    global sub_problemes;           sub_problemes = set()

    for id_cip_sec, codi_ps, data_ps in u.getAll(sql, "import"):
        if id_cip_sec in assignada_tot:
            up = assignada_tot[id_cip_sec]["up"]
            sub_problemes.add((id_cip_sec, up, codi_ps, data_ps))
    
    return sub_problemes

def get_placures_diagnostics():

    global placures_diagnostics;    placures_diagnostics = c.defaultdict(set)

    codis_intervencions = ("IRE05830", "IRE01544")

    sql = """
            SELECT
                id_cip_sec 
            FROM
                master_placures_diagnostics
            WHERE
                diagnostics_codi IN {}
                AND YEAR(diagnostics_data_inici) = {}
          """.format(codis_intervencions, data_ext.year)
    for id_cip_sec, in u.getAll(sql, "nodrizas"):
        if id_cip_sec in assignada_tot:
            up = assignada_tot[id_cip_sec]["up"]
            placures_diagnostics[up].add(id_cip_sec)

def get_exodoncies():

    global exodoncies_1;            exodoncies_1 = c.defaultdict(lambda: c.defaultdict(set))
    global exodoncies_2;            exodoncies_2 = c.defaultdict(lambda: c.defaultdict(set))
    pat_previa_1 = c.defaultdict(lambda: c.defaultdict(set))
    pat_previa_2 = c.defaultdict(lambda: c.defaultdict(set))

    sql = """
            SELECT
                id_cip_sec,
                dpd_cod_p
            FROM
                odn507
            WHERE
                dpd_pat IN ('CA', 'PAO', 'RR')
                AND dpd_cod_p IN (51, 52, 54, 55, 61, 62, 64, 65, 74, 75, 84, 85)
          """
    for id_cip_sec, dent in u.getAll(sql, "import"):
        if id_cip_sec in assignada_tot:
            up = assignada_tot[id_cip_sec]["up"]
            if dent in (51, 52, 61, 62):
                pat_previa_1[up][id_cip_sec].add(dent)
            if dent in (54, 55, 64, 65, 74, 75, 84, 85):
                pat_previa_2[up][id_cip_sec].add(dent)

    sql = """
            SELECT
                id_cip_sec,
                dtd_cod_p,
                dtd_pat
            FROM
                odn508
            WHERE
                dtd_tra IN ('EX', 'EXO')
                AND dtd_cod_p IN (51, 52, 54, 55, 61, 62, 64, 65, 74, 75, 84, 85)
                AND dtd_et = 'E'
                AND YEAR(dtd_data) = {}
          """.format(data_ext.year)
    for id_cip_sec, dent, pat in u.getAll(sql, "import"):
        if id_cip_sec in assignada_tot:
            up = assignada_tot[id_cip_sec]["up"]
            if pat in ('CA', 'PAO', 'RR'):
                if dent in (51, 52, 61, 62):
                    exodoncies_1[up][id_cip_sec].add(dent)
                if dent in (54, 55, 64, 65, 74, 75, 84, 85):
                    exodoncies_2[up][id_cip_sec].add(dent)
            else:
                if dent in (51, 52, 61, 62) and dent in pat_previa_1[up][id_cip_sec]:
                    exodoncies_1[up][id_cip_sec].add(dent)
                if dent in (54, 55, 64, 65, 74, 75, 84, 85) and dent in pat_previa_2[up][id_cip_sec]:
                    exodoncies_2[up][id_cip_sec].add(dent)

def create_indicadors():

    global resultats;               resultats = c.Counter()

    # FDP01

    for id_cip_sec in assignada_tot:
        edat, sexe, up, poblatips = tuple([assignada_tot[id_cip_sec][key] for key in ("edat", "sexe", "up", "poblatips")])
        edat_lv, sexe_lv = u.ageConverter(edat), u.sexConverter(sexe)
        br, ambit = tuple([cat_centres[up][key] for key in ("br", "ambit")])

        if edat < 9:
            if id_cip_sec in problemes[up] or id_cip_sec in placures_diagnostics[up]:
                for poblatip in poblatips:
                    resultats[(br, ambit, edat_lv, sexe_lv, poblatip, "FDP01", "NUM")] += 1

        if edat < 5:
            if id_cip_sec in exodoncies_1[up]:
                for poblatip in poblatips:
                    resultats[(br, ambit, edat_lv, sexe_lv, poblatip, "FDP02", "NUM")] += len(exodoncies_1[up][id_cip_sec])

        if edat < 10:
            if id_cip_sec in exodoncies_2[up]:
                for poblatip in poblatips:
                    resultats[(br, ambit, edat_lv, sexe_lv, poblatip, "FDP03", "NUM")] += len(exodoncies_2[up][id_cip_sec])

def export_taules():
    """."""

    columnes = "(ind varchar(10), br varchar(5), amb_desc varchar(40), edat varchar(10), sexe varchar(4), poblatip varchar(10), analisis varchar(3), n int)"

    upload = [(ind, br, ambit, edat, sexe, poblatip, analisis, n) for (br, ambit, edat, sexe, poblatip, ind, analisis), n in resultats.items()]
    u.createTable(tb , columnes, db, rm=True)
    u.listToTable(upload, tb, db)

def export_khalix():
    """."""

    file = 'ODN_FDP'

    sql = """
            SELECT
                ind,
                '{}',
                br,
                analisis,
                edat,
                poblatip,
                sexe,
                'N',
                n
            FROM
                {}.{} a
            """.format(periode, db, tb)
    u.exportKhalix(sql, file)

if u.IS_MENSUAL:

    get_dates();                                    print("self.get_dates()")
    get_centres();                                  print("self.get_centres()")
    get_assignada_tot();                            print("self.get_assignada_tot()")
    get_problemes();                                print("self.get_problemes()")
    get_placures_diagnostics();                     print("self.get_placures_diagnostics()")
    get_exodoncies();                               print("self.get_exodoncies()")
    create_indicadors();                            print("self.create_indicadors()")
    export_taules();                                print("self.export_taules()")
    export_khalix();                                print("self.export_khalix()")