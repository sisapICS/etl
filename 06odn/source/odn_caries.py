from sisapUtils import *
from collections import defaultdict

ind_codi = "EQA9119"

def denominador():
    upload_pacient = dict()
    pacients_den = defaultdict(dict)
    # pacients_den de 0-5 anys que tenen index COD positiu
    sql = """
            SELECT DISTINCT
                a.id_cip_sec,
                ates,
                institucionalitzat,
                maca                
            FROM
                nodrizas.assignada_tot a,
                import.odn505 b
            WHERE
                a.id_cip_sec = b.id_cip_sec
                AND edat <= 5
                AND b.cfd_ind_cod > 0
          """
    for id_cip_sec, ates, institucionalitzat, maca in getAll(sql, "nodrizas"):
        pacients_den[id_cip_sec] = {"ates": ates, "institucionalitzat": institucionalitzat, "maca": maca}
        upload_pacient[(id_cip_sec, ates, institucionalitzat, maca)] = 0

    return pacients_den, upload_pacient


def numerador(upload_pacient, pacients_den):
    # pacients_den que hagin tingut ('FSC', 'CD', 'EHO') [els 3 camps, AND]
    print('calculant numerador')
    ids_num_candidats = dict()
    sql = """
            SELECT
                id_cip_sec,
                tb_trac
            FROM
                import.odn511
            WHERE
                TB_TRAC IN ('FSC', 'CD', 'EHO')
          """
    for id_cip_sec, trac in getAll(sql, "import"):
        if id_cip_sec in pacients_den:
            if id_cip_sec not in ids_num_candidats.keys():
                ids_num_candidats[id_cip_sec] = [0,0,0]
            if trac == 'FSC':
                ids_num_candidats[id_cip_sec][0] = 1
            elif trac == 'CD':
                ids_num_candidats[id_cip_sec][1] = 1
            elif trac == 'EHO':
                ids_num_candidats[id_cip_sec][2] = 1
    
    # pacients_den que hagin tingut FSC i (CD i/o EHO)
    for id_cip_sec in ids_num_candidats.keys():
        if ids_num_candidats[id_cip_sec][0] == 1 and ids_num_candidats[id_cip_sec][1] + ids_num_candidats[id_cip_sec][2] >= 1:
            try:
                ates, institucionalitzat, maca = tuple([pacients_den[id_cip_sec][key] for key in ("ates", "institucionalitzat", "maca")])
                upload_pacient[(id_cip_sec, ates, institucionalitzat, maca)] = 1
            except:
                pass
    
    return upload_pacient


if __name__ == '__main__':
    pacients_den, upload_pacient = denominador() 
    print(len(pacients_den))
    upload_pacient = numerador(upload_pacient, pacients_den)
    print('creem taula') 

    upload_pacient_taula = list()
    for (id_cip_sec, ates, institucionalitzat, maca), num in upload_pacient.items():
        upload_pacient_taula.append((id_cip_sec, ind_codi, ates, institucionalitzat, maca, 1, num, 0))

    # Creating table

    cols = "(id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int,den int,num int,excl int)"
    
    table = "odn_caries_{}_indicador".format(ind_codi)
    createTable(table, cols, 'odn', rm=True)
    listToTable(upload_pacient_taula, table, 'odn')