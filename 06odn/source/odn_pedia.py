# -*- coding: latin1 -*-

import sisapUtils as u

# Diccionari de conversi� d'indicadors de pediatria a indicadors d'ODN
conv_indicadors = {
    "EQA0802": "EQA9103",
    "EQA0803": "EQA9104",
    "EQA0806": "EQA9113",
}

def get_dades_pedia():
    """
    Obt� les dades de pediatria des de la base de dades 'pedia' i les desa en una llista global.

    Aquesta funci� consulta la taula 'mst_indicadors_pacient' per obtenir les dades dels indicadors
    pedi�trics definits al diccionari 'conv_indicadors'. Despr�s, converteix els indicadors
    a la nomenclatura d'ODN i els desa en 'tb_dades'.

    Variables globals:
    ------------------
    tb_dades : list
        Llista global on s'emmagatzemen les dades recuperades.
    """
    
    global tb_dades
    tb_dades = []

    # Consulta SQL per obtenir els indicadors pedi�trics
    sql = """
        SELECT
            id_cip_sec,
            indicador,
            ates,
            institucionalitzat,
            maca,
            den,
            num,
            excl
        FROM
            mst_indicadors_pacient
        WHERE
            indicador IN {_indicadors_pedia}
    """.format(_indicadors_pedia=tuple(conv_indicadors.keys()))

    # Executa la consulta i desa els resultats a la llista global
    for id_cip_sec, indicador_pedia, ates, institucionalitzat, maca, den, num, excl in u.getAll(sql, "pedia"):
        indicador_odn = conv_indicadors.get(indicador_pedia)
        tb_dades.append((id_cip_sec, indicador_odn, ates, institucionalitzat, maca, den, num, excl))


def export_table():
    """ 
    Exporta les dades a una taula de la base de dades ODN.

    Aquesta funci� crea una taula anomenada 'odn_pedia' a la base de dades 'odn' 
    i insereix les dades de la llista 'tb_dades'.
    """
    
    tb_name = "odn_pedia"
    tb_cols = "(id_cip_sec int, indicador varchar(8), ates int, institucionalitzat int, maca int, den int, num int, excl int)"
    tb_db = "odn"
    
    # Crear la taula (elimina la taula si ja existeix amb rm=True)
    u.createTable(tb_name, tb_cols, tb_db, rm=True)
    
    # Inserir les dades a la taula
    u.listToTable(tb_dades, tb_name, tb_db)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dades_pedia();                  print("get_revisions_odn() completed")
    export_table();                     print("export_table() completed")