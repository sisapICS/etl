from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
#Validation
validation=False
up_codi=("00380",)

alt = "odn"
nod = "nodrizas"
assig=nod+".assignada_tot"
odn=nod+".odn_variables"


limit=""
codis= {
	7:"EQA9111",
	12:"EQA9112"
}

#Agrupador indicador de ARC
agr_maloc=823


#Funciones	

def get_birth_year(anys):
    sql="select data_ext from {0}.dextraccio;".format(nod)
    current_date= getOne(sql,nod)[0]
    return current_date.year-anys
    
def get_nens_maloclusions(agr_ARC):
	""" Selecciona los id_cip_seq de ninos que presentan un maloclusions segun el agrupador en la tabla odn_variables
		Devuelve un set de id_cip_seq
	"""
	sql = "select id_cip_sec from {0} where agrupador={1} and baixa=0".format(odn,agr_ARC)
	sql += " union select id_cip_sec from import.odn510 where ro_sa = 'S'"
	maloc_nens = set([id for id, in getAll(sql, nod)])
	return maloc_nens

def get_rows_by_year(year, numerador):
	"""Adaptado de: sisap\08alt\source\prof_transaminases.py
	   De una orden sql, selecciona aquellos id cuyo any de nacimiento sea igual a year y construye una row (un tuple) con columnas: id,
	   up, data_naix y result. Si este id esta en el numerador, el result tendra valor 1 y si no, tendra valor 0.
	   Devuelve una lista de row(tuples)
	"""
	sql = "select id_cip_sec, upOrigen, data_naix from {} where year(data_naix)= {} and ates=1 {}".format(assig,year,limit)
	rows = []      
	for id, up, data_naix in getAll(sql, nod):
		num=0
		if id in numerador:
			num=1
		rows.append((id, up,data_naix, num))
	return rows

def get_uba_pacient_tables (year, numerador, years_old):
	"""Adaptado de: sisap\08alt\source\prof_od_inr.py
	"""
	uba_rows=[]
		
	sql = "select id_cip_sec, ates, institucionalitzat, maca from {} where year(data_naix)= {} {}".format(assig,year,limit)
	   
	for id, ates, insti, maca in getAll(sql, nod):
		num = 0
		if id in numerador:
			num = 1
		uba_rows.append((id, codis[years_old], ates, insti, maca, 1, num, 0))
	
	return uba_rows
	
def validation_by_up_pacient(codi,up_tuple,table_name,table_columns):
	sql="select * from altres.exp_ecap_{0}_{1} where up in ({2})".format(codi,table_name,",".join(up_tuple))
	up_rows= [(row) for row in getAll(sql,alt)]
	export_table("exp_ecap_{0}_{1}".format(codi,table_name),
				table_columns,
				alt,
				up_rows)
	
	
def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)
	
	
if __name__ == '__main__':
# comencem!
	printTime('inici')
	
	maloc_nens=get_nens_maloclusions(agr_maloc)
	printTime('Nens amb maloclusions')

	for years_old in (7,12):
		birth_year=get_birth_year(years_old)
		printTime(birth_year)

		#for row in get_rows_by_year(birth_year,maloc_nens)[0:20]:
		#	print(row)
	
		#conseguir las listas de filas para las dos tablas que se van a crear, la agrupadas por up y la de pacientes no incluidos
		uba_rows =get_uba_pacient_tables(birth_year,maloc_nens, years_old)
		printTime('Retrieving rows new table OK')
	
		#exportar las tablas a la base de datos altres
		uba_columns="(id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int,den int,num int,excl int)"
		export_table("odn_maloclusions_{}_indicador".format(years_old),
					uba_columns,
					alt,
					uba_rows)
				
	
		if validation:
			validation_by_up_pacient(codis[years_old],up_codi,"uba",uba_columns)
			validation_by_up_pacient(codis[years_old], up_codi,"pacient",pacient_columns)
	