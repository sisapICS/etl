# -*- coding: latin1 -*-

"""
Aquest script processa dades relacionades amb pacients, edats i indicadors EQA ODN 
a partir de diverses fonts, incloent fitxers CSV i taules de bases de dades.

### Funcionalitats principals:
1. Llegeix un fitxer d'indicadors ('odn_edats.txt') que defineix rangs d'edat i condicions associades.
2. Obt� informaci� de la base de dades sobre pacients, incloent edats i altres atributs.
3. Calcula edats dels pacients segons els criteris especificats:
   - **Tipus "edat_actual"**: L'edat actual del pacient en el moment del c�lcul.
   - **Tipus "edat_compleix"**: L'edat que el pacient compleix durant l'any en curs.
4. Valida si els pacients compleixen els criteris dels indicadors basats en edats m�nima i m�xima.
5. Genera una llista de pacients amb les dades processades que compleixen els criteris definits.
6. Insereix els resultats processats en una taula de destinaci� a la base de dades.

### Arxiu 'odn_edats.txt':
Cont� els camps seg�ents separats per '@':
- **'indicador'**: Identificador de l'indicador EQA ODN (ex. 'EQA9101').
- **'tipus_edat'**: Pot ser:
  - '"edat_actual"': Edat actual del pacient en el moment del c�lcul.
  - '"edat_compleix"': Edat que el pacient compleix durant l'any en curs.
- **'edat_minima'**: L'edat m�nima que pot tenir o complir el pacient.
- **'edat_maxima'**: L'edat m�xima que pot tenir o complir el pacient.

### Flux de treball:
1. Es carreguen els indicadors i els criteris d'edat des del fitxer 'odn_edats.txt'.
2. Es calcula l'any d'extracci� a partir de la taula 'dextraccio'.
3. Es llegeixen les dades dels pacients de la taula 'assignada_tot_with_jail'.
4. S'obt� l'edat del pacient segons el tipus d'edat especificat per cada indicador.
5. Es valida si l'edat calculada es troba dins del rang d'edat m�nim i m�xim.
6. Els pacients que compleixen els criteris es guarden en una nova taula a la base de dades.

### Taula de destinaci�:
La taula de destinaci� tindr� l'esquema seg�ent:
- **'id_cip_sec'**: Identificador �nic del pacient.
- **'indicador'**: Nom de l'indicador EQA ODN associat.
- **'data_naix'**: Data de naixement del pacient.
- **'edat'**: Edat calculada o registrada del pacient.
- **'ates'**: lag que indica si el pacient ha sigut at�s l'�ltim any (1 o 0).
- **'institucionalitzat'**: Flag que indica si el pacient est� institucionalitzat (1 o 0).
- **'maca'**: Flag que indica si el pacient presenta fragilitat, amb cronicitat avan�ada (1 o 0).

Aquest codi assegura que nom�s es consideren els pacients que compleixen els criteris d'edat als indicadors EQA ODN definits.
"""

import sisapUtils as u
import csv

# Inicialitzaci� de llistes i variables.
indicadors = []  # Llista per emmagatzemar els indicadors carregats del fitxer.
resultats = []  # Llista per emmagatzemar els resultats que compleixen els criteris.
ruta_fitxer_indicadors = "./dades_noesb/odn_edats.txt"  # Ruta al fitxer d'indicadors.
db_dest = "odn"  # Nom de la base de dades de destinaci�.
tb_dest = "denominadors"  # Nom de la taula de destinaci�.
tb_dest_cols = "(id_cip_sec int, indicador varchar(8), data_naix date, edat int, ates int, institucionalitzat int, maca int)"  # Esquema de la taula.

# Llegeix el fitxer d'indicadors i carrega les dades a la llista 'indicadors'.
with open(ruta_fitxer_indicadors, 'rb') as fitxer:
    lector_csv = csv.reader(fitxer, delimiter='@', quotechar='|')  # Llegeix el fitxer amb el delimitador "@".
    for fila in lector_csv:
        indicador, tipus_edat, edat_minima, edat_maxima = fila[0], fila[1], int(fila[2]), int(fila[3])
        indicadors.append((indicador, tipus_edat, edat_minima, edat_maxima))  # Afegeix els indicadors a la llista.

# Obt� l'any d'extracci� des de la taula "dextraccio".
sql = """
        SELECT
            YEAR(data_ext)
        FROM
            dextraccio
      """
any_extraccio, = u.getOne(sql, "nodrizas")  # Consulta SQL per obtenir l'any d'extracci�.

# Llegeix les dades de la taula "assignada_tot_with_jail".
sql = """
        SELECT
            id_cip_sec,
            YEAR(data_naix),
            edat,
            ates,
            institucionalitzat,
            maca,
            data_naix
        FROM
            assignada_tot_with_jail
      """
# Processa cada fila retornada per la consulta.
for id_cip, any_naixement, edat, ates, institucionalitzat, maca, data_naixement in u.getAll(sql, "nodrizas"):
    any_naixement = int(any_naixement)  # Converteix l'any de naixement en sencer.
    edat = int(edat)  # Converteix l'edat en sencer.

    # Verifica cada indicador i aplica la l�gica.
    for indicador, tipus_edat, edat_minima, edat_maxima in indicadors:
        edat_calculada = edat if tipus_edat == "edat_actual" else any_extraccio - any_naixement  # Calcula l'edat segons el tipus.
        if edat_minima <= edat_calculada <= edat_maxima:  # Valida si l'edat es troba dins del rang especificat.
            resultats.append([int(id_cip), indicador, data_naixement, edat, int(ates), int(institucionalitzat), int(maca)])  # Guarda el resultat.

# Crea una taula temporal per emmagatzemar els resultats processats.
u.createTable(tb_dest, tb_dest_cols, db_dest, rm=1)

# Insereix els resultats processats a la taula de destinaci�.
u.listToTable(resultats, tb_dest, db_dest)  # Carrega els resultats a la taula.