# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime

# Obtenci� de la data d'extracci� de les dades

sql = """
        SELECT
            data_ext
        FROM
            dextraccio
    """
data_ext, = u.getOne(sql, 'nodrizas')

# Definici� de l'any actual i anterior a partir de la data d'extracci�

data_year_actual = data_ext.year
data_year_anterior = data_year_actual - 1
data_month = data_ext.month

# Definici� de les dates d'inici i final del per�ode d'estudi

data_inici = datetime.date(data_year_anterior, 9, 1)
data_fi = datetime.date(data_year_actual, 6, 30)

# Seguirem executant el codi nom�s si estem calculan el mensual de maig

if u.IS_MENSUAL and data_month == 6:

    # Obtenci� de les dades d'assignaci� amb les identificacions i dates de naixement

    sql = """
            SELECT
                id_cip_sec,
                data_naix
            FROM
                assignada_tot
          """
    assignada_tot = {id_cip_sec: data_naix for id_cip_sec, data_naix in u.getAll(sql, "nodrizas")}

    # Obtenci� de les dades dels centres amb les seves regions, sectors i descriptors d'abs�ncia

    sql = """
            SELECT
                up_cod,
                regio_des,
                sector_des,
                abs_des
            FROM
                cat_sisap_covid_dbc_rup
          """
    centres = {up: (regio, sector, abs) for up, regio, sector, abs in u.getAll(sql, "import")}

    # Obtenci� de les dades de revisions escolars odontol�giques

    dades_taula = list()

    sql = """
            select
                id_cip_sec,
                ro_data,
                ro_up,
                ro_escola,
                ro_ind_caod,
                ro_ind_cod,
                CASE
                    WHEN ro_ind_caod = 0 AND ro_ind_cod = 0 THEN 1
                    ELSE 0
                END AS LLIURES_CAR
            FROM
                odn510
            WHERE
                ro_data BETWEEN '{_data_inici}' AND '{_data_fi}'
                AND ro_estat = 'F'
                AND ro_escola IS NOT null
          """.format(_data_inici=data_inici, _data_fi=data_fi)
    for id_cip_sec, data, up, escola, ind_caod, ind_cod, lliures_car in u.getAll(sql, "import"):
        if id_cip_sec in assignada_tot and up in centres:
            data_naix = assignada_tot[id_cip_sec]
            edat = u.yearsBetween(data_naix, data)
            if edat in (4,5,6,7,11,12):
                if edat in (4,5):
                    edat = '4 a 5'
                elif edat in (6,7):
                    edat = '6 a 7'
                elif edat in (11,12):
                    edat = '11 a 12'

                regio, sector, abs = centres[up]
                nen_sa = 1 if ind_caod == 0 and ind_cod == 0 else 0

                dades_taula.append((edat, up, escola, regio, sector, abs, ind_caod, ind_cod, lliures_car, nen_sa))
    
    # Creaci� d'una taula a la base de dades i inserci� de les dades
    
    col_names = "(edat varchar(10), up varchar(5), escola varchar(100), regio_des varchar(100), sector_des varchar(100), abs_des varchar(100), ind_caod int, ind_cod int, lliures_car int, nen_sa int)"
    tb_name = "odn_escoles_anual"
    db_name = "odn"

    u.createTable(tb_name, col_names, db_name, rm=1)
    u.listToTable(dades_taula, tb_name, db_name)

    # Definici� dels noms de les columnes per als resultats
    col_names = ('regio_des', 'sector_des', 'abs_des', 'escola', 'edat', 'total', 'med_caod', 'med_co', 'lliures_car', 'perc_nens_sans')

    # Resultats definitius a enviar

    sql = """
            SELECT
                regio_des,
                sector_des,
                abs_des,
                escola,
                edat,
                count(1) AS total,
                sum(ind_caod)/ count(1) AS med_caod,
                sum(ind_cod)/ count(1) AS med_co,
                sum(lliures_car) AS lliures_car,
                ROUND((sum(nen_sa)*100)/count(1), 2) AS perc_nens_sans
            FROM
                {}
            WHERE
                escola <> ''
            GROUP BY
                regio_des,
                sector_des,
                abs_des,
                escola,
                edat
          """.format(tb_name)
    resultats = [(regio_des, sector_des, abs_des, escola, edat, total, str(med_caod).replace(".", ","), str(med_co).replace(".", ","), lliures_car, perc_nens_sans) for 
                (regio_des, sector_des, abs_des, escola, edat, total, med_caod, med_co, lliures_car, perc_nens_sans) in u.getAll(sql, db_name)]

    # Guardar els resultats en un arxiu CSV
    arxiu_resultats = u.tempFolder + 'revisions_escolars_odontologiques_curs_{_data_year_anterior}_{_data_year_actual}.csv'.format(_data_year_anterior=data_year_anterior, _data_year_actual=data_year_actual)
    u.writeCSV(arxiu_resultats, [col_names] + resultats, sep=";")

    # Configuraci� i enviament del correu electr�nic amb els resultats adjunts
    me = "SISAP <sisap@gencat.cat>"
    to = ["elisabetcaula@gencat.cat", "mfigueras.girona.ics@gencat.cat"]
    cc = ["mquintana@ambitcp.catsalut.net", "alejandrorivera@gencat.cat"]
    subject = "Dades del curs {}-{} sobre revisions escolars odontol�giques".format(data_year_anterior, data_year_actual)
    text = "Bon dia,\n\nUs fem arribar les dades de les revisions escolars odontol�giques per escola d'odontologia. Podeu consultar-les a l'arxiu adjunt a aquest correu.\n\nSalutacions cordials,\n\nSISAP"
    u.sendGeneral(me, to, cc, subject, text, arxiu_resultats)