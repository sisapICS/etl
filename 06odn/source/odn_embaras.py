# -*- coding: latin1 -*-

from sisapUtils import *
import csv, os
from time import strftime
import dateutil.relativedelta

# Imprimim l'hora d'inici per registrar el temps d'execuci�
print(strftime("%Y-%m-%d %H:%M:%S"))

# Configuraci� inicial i connexi� a la base de dades
db_name = "odn"  # Nom de la base de dades
conn = connect((db_name, 'aux'))  # Connexi� a la base de dades
c = conn.cursor()

# Ruta del fitxer de dades
path = "./dades_noesb/odn_relacio.txt"

# Noms de les taules utilitzades
tb_ass_embaras = "nodrizas.ass_embaras"
tb_criteris = "nodrizas.eqa_criteris"
tb_denominadors = "denominadors"
tb_ind_embaras = "embaras_indicador"
tb_odn_criteris = "nodrizas.odn_variables"
tb_odn_embaras = "odn_embaras"

# Obtenim la data d'extracci� i calculem la data fa un any
sql = """
         SELECT
            data_ext
         FROM
            dextraccio
      """
data_ext, = getOne(sql, "nodrizas")
data_ext_menys1any = data_ext - dateutil.relativedelta.relativedelta(months=12)

# Diccions d'avortaments i criteris relacionats
avortaments = dict()

# Obtenim els codis diagn�stics d'avortament
sql = """
         SELECT
            criteri_codi
         FROM
            eqa_criteris
         WHERE
            agrupador = 1081
      """
codis_avortaments = tuple([codi_ps for codi_ps, in getAll(sql, "nodrizas")])

def sub_get_problemes(sql):
    """
    Funci� auxiliar per obtenir registres relacionats amb avortaments de les sub-taules de problemes.

    Retorna:
        - Un conjunt de tuples amb (id_cip_sec, data_avortament).
    """
    sub_problemes = set()

    for id_cip_sec, data_avortament in getAll(sql, "import"):
        sub_problemes.add((id_cip_sec, data_avortament))
    
    return sub_problemes

# Recorrem les subtaules de problemes i recollim els avortaments enregistrats
sql = """
         SELECT
            id_cip_sec,
            pr_dde
         FROM
            {}
         WHERE
            pr_cod_ps IN {}
      """
jobs = [sql.format(table, codis_avortaments) for table in getSubTables("problemes") if table[-6:] != '_s6951']
count = 0
for sub_problemes in multiprocess(sub_get_problemes, jobs, 4):
    for id_cip_sec, data_avortament in sub_problemes:
         avortaments.setdefault(id_cip_sec, data_avortament)
         if data_avortament > avortaments[id_cip_sec]:
               avortaments[id_cip_sec] = data_avortament
    count += 1
    print("    probs: {}/{}. {}".format(count, len(jobs), datetime.now()))

# Diccionari d'embarassos actius l'�ltim any
embarasos = dict()

# Obtenim els embarassos actius l'�ltim any mitjan�ant el m�dul d'embaras, sense avortament enregistrat despr�s de l'inici
sql = """
         SELECT
            id_cip_sec,
            inici,
            fi,
            temps
         FROM
            {_tb_ass_embaras} a,
            dextraccio
         WHERE
            fi > DATE'{_data_ext_menys1any}'
      """.format(_tb_ass_embaras=tb_ass_embaras, _data_ext_menys1any=data_ext_menys1any)
for id_cip_sec, inici_embaras, fi_embaras, temps in getAll(sql, "nodrizas"):
   data_avortament = avortaments.get(id_cip_sec)
   if not (data_avortament and data_avortament >= inici_embaras):
      embarasos[id_cip_sec] = (inici_embaras, fi_embaras, temps)

# Afegim embarassos des de la taula de problemes d'incid�ncia, si la pacient no s'ha registrat mitjan�ant el m�dul d'embaras, sense avortament enregistrat despr�s de l'inici
sql = """
         SELECT
            id_cip_sec,
            dde inici_embaras,
            CASE
               WHEN dba = '0000-00-00' THEN
                        CASE
                        WHEN DATE_ADD(dde, INTERVAL 294 DAY) > DATE'{_data_ext}' THEN DATE'{_data_ext}'
                        ELSE DATE_ADD(dde, INTERVAL 294 DAY)
                     END
               ELSE
                        CASE
                        WHEN dba <= DATE_ADD(dde, INTERVAL 294 DAY) THEN dba
                        WHEN DATE_ADD(dde, INTERVAL 294 DAY) < dba AND DATE_ADD(dde, INTERVAL 294 DAY) <= DATE'{_data_ext}' THEN DATE_ADD(dde, INTERVAL 294 DAY)
                        ELSE DATE'{_data_ext}'
                     END
            END AS fi_embaras
         FROM
            eqa_problemes_incid a
         WHERE
            ps IN (39, 597, 1062, 1063)
            AND DATE_ADD(dde, INTERVAL 90 DAY) <= DATE'{_data_ext}'
            AND dba < dde
            AND ((dba = '0000-00-00' AND dde BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}')
               OR (dba != '0000-00-00' AND ((dba <= DATE_ADD(dde, INTERVAL 294 DAY) AND dba BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}')
                                       OR (dba > DATE_ADD(dde, INTERVAL 294 DAY) AND DATE_ADD(dde, INTERVAL 294 DAY) BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'))
                     )
                  )
        """.format(_tb_odn_embaras=tb_odn_embaras, _data_ext=data_ext, _data_ext_menys1any=data_ext_menys1any)
for id_cip_sec, inici_embaras, fi_embaras in getAll(sql, "nodrizas"):
   temps = daysBetween(inici_embaras, fi_embaras)
   data_avortament = avortaments.get(id_cip_sec)
   if id_cip_sec not in embarasos and not (data_avortament and data_avortament >= inici_embaras) and temps >= 90:
      embarasos[id_cip_sec] = (inici_embaras, fi_embaras, temps)

# Creem la taula per guardar els embarassos seleccionats
createTable(tb_odn_embaras, "(id_cip_sec int, inici date, fi date, temps int)", db_name, rm=True)
listToTable([(id_cip_sec,) + dada_embaras for id_cip_sec, dada_embaras in embarasos.items()], tb_odn_embaras, db_name)

# Afegim un �ndex a la taula
c.execute("alter table %s add index(id_cip_sec,inici,fi)" % tb_odn_embaras)

# Processament del fitxer de relaci� d'indicadors
with open(path, 'rb') as file:
   p = csv.reader(file, delimiter='@', quotechar='|')
   for i in p:
      ind, tipus, agr, f, tmin, tmax, vmin, vmax = i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7]
      if tipus == "e":
         c.execute("alter table %s add column %s int" % (tb_odn_embaras, ind))
         c.execute("update %s set %s=0" % (tb_odn_embaras, ind))
      elif tipus == "num_e":
         c.execute("select min(taula) from %s where agrupador='%s' " % (tb_criteris, agr))
         pres = c.fetchall()
         for a in pres:
            taula = a[0]
            if taula.startswith("odn"):
               c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set %s=1 where agrupador='%s' and dat between inici and fi" % (tb_odn_embaras, tb_odn_criteris, ind, agr))

# Creem la taula de resultats finals
c.execute("drop table if exists %s" % tb_ind_embaras)
c.execute("create table %s (id_cip_sec int, indicador varchar(8), ates int, institucionalitzat int, maca int, den int, num int, excl int)" % tb_ind_embaras)
c.execute("insert into %s select a.id_cip_sec,indicador,ates,institucionalitzat,maca,1 den,%s num, 0 excl from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where indicador='%s'" % (tb_ind_embaras, ind, tb_odn_embaras, tb_denominadors, ind))

# Tanquem la connexi�
conn.close()

# Imprimim l'hora de finalitzaci�
print(strftime("%Y-%m-%d %H:%M:%S"))