from sisapUtils import *
import csv, os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="odn"

conn = connect((db,'aux'))
c=conn.cursor()

patho="./dades_noesb/odn_peces.txt"

peca="<50"
peces_especials="in ('11','12','13','21','21','23','31','32','33','41','42','43')"
tto_especial="like ('%EX%')"

tto="peca_tractaments"
pato="peca_pato"
ind="peca_indicador1"
ind2="peca_indicador2"
ind_peca="peca_indicador"
peces="nodrizas.odn_peces"
den="denominadors"
i='EQA9107'


c.execute("drop table if exists %s" % tto)
c.execute("create table %s (tto varchar(5) not null default'')" % tto)
c.execute("drop table if exists %s" % pato)
c.execute("create table %s (pato varchar(5) not null default'')" % pato)


with open(patho, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for t in p:
      tipus,tt= t[0],t[1]
      if tipus=="t":
         c.execute("insert into %s values ('%s')" % (tto,tt))
         c.execute("alter table %s add index(tto)" % tto)
      if tipus=="p":
         c.execute("insert into %s values ('%s')" % (pato,tt))
         c.execute("alter table %s add index(pato)" % pato)

c.execute("drop table if exists %s" % ind)
c.execute("create table %s (id_cip_sec int,peca int,tto varchar(2000),num double)" % ind)
c.execute("insert into %s select id_cip_sec,peca,tto,0 num from %s a,%s b where peca %s and a.pato like concat('%%',b.pato,'%%')" % (ind,peces,pato,peca))
c.execute("update %s a, %s b set num=1 where a.tto like concat('%%',b.tto,'%%')" %(ind,tto))
c.execute("update %s set num=1 where peca %s and tto %s" %(ind,peces_especials,tto_especial))
c.execute("drop table if exists %s" % ind2)
c.execute("create table %s (id_cip_sec int,num double)" % ind2)
c.execute("insert into %s select id_cip_sec,max(num) from %s group by id_cip_sec" % (ind2,ind))
c.execute("alter table %s add index(id_cip_sec,num)" % ind2)
c.execute("drop table if exists %s" % ind_peca)
c.execute("create table %s (id_cip_sec int,indicador varchar(8),ates int,institucionalitzat int,maca int,den int,num int,excl int)" % ind_peca)
c.execute("insert into %s select a.id_cip_sec,indicador,ates,institucionalitzat,maca,1 den,num, 0 excl from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where indicador='%s'" % (ind_peca,ind2,den,i))

conn.close()

print strftime("%Y-%m-%d %H:%M:%S")
