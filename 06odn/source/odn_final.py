from sisapUtils import *
import csv, os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="odn"
hide = ("EQA9101", "EQA9105", "EQA9106")
indicadors_jail = ("EQA9102", "EQA9107", "EQA9108", "EQA9117")

pathodn="./dades_noesb/odn_relacio.txt"
pathc="./dades_noesb/odn_ind.txt"
pathcprs="./dades_noesb/prsodn_ind.txt"
pathesp="./dades_noesb/eqaodo_prevalences_anual.txt"
pathespeorig="./dades_noesb/eqaodo_poresper_anual.txt"
pathmetes="./dades_noesb/eqaodo_metesres_subind_anual.txt"
pathpond="./dades_noesb/eqaodo_ponderacio_anual.txt"
pathpond_jail = "./dades_noesb/eqaprsodo_ponderacio_anual.txt"

indicadors="odn_indicadors"
den="odn_denominadors"
num="odn_numeradors"

criteris="nodrizas.eqa_criteris"
assignada="nodrizas.assignada_tot";                assignada_jail="nodrizas.jail_assignada"
jail_atesa_avui_60= "nodrizas.jail_atesa_avui_60"
edats_k = "nodrizas.khx_edats5a"
dext="nodrizas.dextraccio"
u11 = "mst_u11"
u11nod = "nodrizas.eqa_u11"
u11nod_jail = "nodrizas.eqa_u11_with_jail"
ind = "eqa_ind";                                   ind_jail = ind + "_jail"
peca="peca_indicador"
embaras="embaras_indicador"
inr = "odn_inr_indicador"
pedia = "odn_pedia"
arc = "odn_arc_indicador"
maloclusions7 = "odn_maloclusions_7_indicador"
maloclusions12 = "odn_maloclusions_12_indicador"
odn_caries = "odn_caries_eqa9119_indicador"
arc_fc = "odn_arc_fc_indicadors"
odn_dm2 = "odn_dm2_indicadors"
odn_alerg = "odn_indicador_alergias"


pac = "mst_indicadors_pacient"
ecap_pac_pre = "odn_ecap_pacient_pre"
ecap_pac = "exp_ecap_pacient";                     ecap_pac_jail = ecap_pac + "_jail"
ecap_marca_exclosos = "if(institucionalitzat=1,2,if(excl=1,4,if(ates=0,5,if(maca=1,1,0))))"
instit="if(edat between 0 and 14,institucionalitzat_ps,b.institucionalitzat)"
ecap_cataleg_exclosos = [[0,'Pacients que formen part de l&#39;indicador',0],[1,'Pacients MACA',2],[2,'Pacients institucionalitzats',3],[3,'Pacients exclosos per edat',4],[4,'Pacients exclosos per motius cl&iacute;nics',5],[5,'Pacients no atesos el darrer any',1]]
condicio_ecap_llistat = "llistat=1 and excl=0 and num=0"

up = "eqa_khalix_up_pre"
pobk = "eqa_poblacio_khx"
pobk_jail = pobk + "_jail"
khalix_pre = "eqa_khalix_up_ind"
khalix = "exp_khalix_up_ind"
khalix_jail = khalix+"_jail"
sexe = "if(sexe='H','HOME','DONA')"
comb = "concat(if(institucionalitzat=1,'INS','NOINS'),if(ates=1,'AT','ASS'))"
condicio_khalix = "excl=0 and (institucionalitzat=1 or (institucionalitzat=0 and maca=0))"
condicio_khalix_pob = "institucionalitzat=1 or (institucionalitzat=0 and maca=0)"
khalix_pob = "exp_khalix_up_pob"
khalix_pob_jail = khalix_pob + "_jail"

conceptes=[['NUM','num'],['DEN','den']]

pob = "eqa_poblacio";                              pob_jail = "eqa_poblacio" + "_jail"
condicio_poblacio = "ates=1 and institucionalitzat=0 and maca=0"
minPob = 1
uba_in = "mst_ubas";                               uba_in_jail = "mst_ubas" + "_jail"
uba1 = "eqa_uba_1";                                uba1_jail = uba1 + "_jail"
uba2 = "eqa_uba_2";                                uba2_jail = uba2 + "_jail"
uba3 = "eqa_uba_3";                                uba3_jail = uba3 + "_jail"
uba4 = "eqa_uba_4";                                uba4_jail = uba4 + "_jail"
uba5 = "eqa_uba_5";                                uba5_jail = uba5 + "_jail"
ecap_ind = "exp_ecap_uba";                         ecap_ind_jail = ecap_ind + "_jail" 
ecap_indodn = "exp_ecap_uba_odn";                  ecap_indodn_jail = ecap_indodn + "_jail"
condicio_ecap_ind = "ates=1 and institucionalitzat=0 and maca=0 and excl=0"
medea = "nodrizas.cat_centres"  #si es '' (o no existis a centres) posem 2U (compte que khalix no ho fa, i per tant no tindran agregat de medea en el detall)
esperades = "eqa_esperades"
esperadesFactor = 1
esperadesCond = "NOINSAT"
metesOrig = "odn_metes_subind_anual"
metes = "eqa_metes"
ponderacio = "odn_ponderacio";                     ponderacio_jail = ponderacio+"_jail"
odontolegs = "mst_professionals"

catalegKhx = "exp_khalix_cataleg"

dimensions = [['NUM','resolts'],['DEN','detectats'],['DENESPER','esperats'],['AGNORESOL','llistat'],['AGDETECT','deteccio'],['AGRESOLT','resolucio'],['AGRESULT','resultat'],['AGASSOLP','resultatPerPunts'],['AGASSOL','punts']]

cataleg = "exp_ecap_cataleg";                      cataleg_jail = cataleg + "_jail"
catalegPare = "exp_ecap_catalegPare";              catalegPare_jail = catalegPare + "_jail"
catalegPunts = "exp_ecap_catalegPunts";            catalegPunts_jail = catalegPunts + "_jail"
catalegExclosos = "exp_ecap_catalegExclosos";      catalegExclosos_jail = catalegExclosos + "_jail"
catalegKhx = "exp_khalix_cataleg";                 catalegKhx_jail = catalegKhx + "_jail"

conn = connect((db,'aux'))
c=conn.cursor()


c.execute("drop table if exists %s" % indicadors)
c.execute("create table %s (id_cip_sec int,indicador varchar(13),ates int,institucionalitzat int,maca int,den int,num int,excl int)" % indicadors)
with open(pathodn, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tipus,agr,f,vmin,vmax,tmin,tmax = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7]
      if tipus== "d" and agr=="z":
         c.execute("insert into %s select id_cip_sec,indicador,ates,institucionalitzat,maca,1 as den, 0 as num, 0 as excl from %s where indicador='%s'" % (indicadors,den,ind))
c.execute("alter table %s add index (id_cip_sec,indicador,ates,institucionalitzat,maca,den,num,excl)" % indicadors)

             
c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec and a.indicador=b.indicador set num=1" % (indicadors,num))  
c.execute("insert into %s select * from %s" % (indicadors,peca))
c.execute("insert into %s select * from %s" % (indicadors,embaras))
c.execute("insert into %s select * from %s" % (indicadors,inr))
c.execute("insert into %s select * from %s" % (indicadors,arc))
c.execute("insert into %s select * from %s" % (indicadors,pedia))
c.execute("insert into %s select * from %s" % (indicadors,maloclusions7))
c.execute("insert into %s select * from %s" % (indicadors,maloclusions12))
c.execute("insert into %s select * from %s" % (indicadors,odn_caries))
c.execute("insert into %s select * from %s" % (indicadors,arc_fc))
c.execute("insert into %s select * from %s" % (indicadors,odn_dm2))
c.execute("insert into %s select * from %s" % (indicadors,odn_alerg))


with open(pathodn, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for ind in p:
      ind,tipus,agr,f,vmin,vmax,tmin,tmax = ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6],ind[7]
      if tipus=="excl":
         c.execute("select distinct taula from %s where agrupador='%s' " % (criteris,agr))
         pres=c.fetchall()
         for i in pres:
            te= i[0]
            texcl="nodrizas.eqa_%s" % te
            if te=="problemes":
               c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set excl=1 where ps=%s and indicador='%s'" % (indicadors,texcl,agr,ind))
            else:
               c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set excl=1 where agrupador=%s and indicador='%s'" % (indicadors,texcl,agr,ind))

c.execute("drop table if exists %s" % pac)
c.execute("create table %s (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,num double,den double,excl double,llistat double)" % pac)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      c.execute("insert into %s select a.id_cip_sec,indicador,if(uporigen != '' and a.id_cip_sec >= 0, uporigen, up) as up,edat,sexe,a.ates,%s as institucionalitzat,a.maca,num,den,excl,'%s' as llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where indicador='%s' and a.id_cip_sec > 0" % (pac,instit,llistat,indicadors,assignada,i))
      if i in indicadors_jail:
         c.execute("insert into %s select a.id_cip_sec,indicador,if(uporigen != '' and a.id_cip_sec >= 0, uporigen, up) as up,edat,sexe,a.ates,%s as institucionalitzat,a.maca,num,den,excl,'%s' as llistat from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where indicador='%s' and a.id_cip_sec in (select id_cip_sec from %s)" % (pac,instit,llistat,indicadors,assignada_jail,i, jail_atesa_avui_60))

c.execute("drop table if exists %s" % ecap_pac_pre)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default '',indicador varchar(10) not null default '',maca int,institucionalitzat int,excl int,exclos int,toShow int)" % ecap_pac_pre)
c.execute("insert into %s select id_cip_sec,up,indicador,maca,institucionalitzat,excl,%s,if(indicador in %s, 0, 1) from %s where %s" % (ecap_pac_pre,ecap_marca_exclosos,hide,pac,condicio_ecap_llistat))
c.execute("alter table %s add index(id_cip_sec)" % ecap_pac_pre)

c.execute("drop table if exists %s" % ecap_pac)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default '',indicador varchar(10) not null default '',exclos int,hash_d varchar(40) not null default '',sector varchar(4) not null default '')" % ecap_pac)
c.execute("drop table if exists %s" % u11)
c.execute("create table %s like %s" % (u11,u11nod))
c.execute("insert into %s select * from %s a where exists (select 1 from %s b where toShow=1 and a.id_cip_sec=b.id_cip_sec and (a.id_cip_sec > 0 or a.id_cip_sec in (select id_cip_sec from %s)))" % (u11,u11nod_jail,ecap_pac_pre,jail_atesa_avui_60))
c.execute("alter table %s add unique(id_cip_sec,hash_d,codi_sector)" % u11)
c.execute("insert into %s select a.id_cip_sec,up,indicador as grup_codi,exclos,hash_d,codi_sector sector from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where toShow=1 and a.id_cip_sec >= 0" % (ecap_pac,ecap_pac_pre,u11))

c.execute("drop table if exists %s" % ecap_pac_jail)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default '',indicador varchar(10) not null default '',exclos int,hash_d varchar(40) not null default '',sector varchar(4) not null default '')" % ecap_pac_jail)
c.execute("insert into %s select a.id_cip_sec,up,CONCAT(indicador, 'PRS') as grup_codi,exclos,hash_d,codi_sector sector from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where toShow=1 and a.id_cip_sec < 0 and a.id_cip_sec in (select id_cip_sec from %s)" % (ecap_pac_jail,ecap_pac_pre,u11,jail_atesa_avui_60))

c.execute("drop table if exists %s" % up)
c.execute("create table %s (id_cip_sec double,up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double)" % up)
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe,%s comb,indicador,num,den from %s a inner join %s b on a.edat=b.edat where %s" % (up,sexe,comb,pac,edats_k,condicio_khalix))

c.execute("drop table if exists %s" % khalix_pre)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double)" % khalix_pre)
c.execute("insert into %s select up,edat,sexe,comb,indicador,sum(num) num,sum(den) den from %s group by 1,2,3,4,5" % (khalix_pre,up))
c.execute("drop table if exists %s" % khalix)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',conc varchar(6) not null default '',n double)" % khalix)
for r in conceptes:
    conc,var=r[0],r[1]
    c.execute("insert into %s select up,edat,sexe,comb,indicador,'%s',%s n from %s a inner join nodrizas.cat_centres c on a.up = c.scs_codi where up <> '' and comb like '%%AT%%'" % (khalix,conc,var,khalix_pre))
    c.execute("insert into %s select up,edat,sexe,replace(comb,'AT','ASS'),indicador,'%s',sum(%s) n from %s a inner join nodrizas.cat_centres c on a.up = c.scs_codi where up <> ''  group by 1,2,3,4,5" % (khalix,conc,var,khalix_pre))
c.execute("drop table if exists %s" % khalix_jail)
c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',conc varchar(6) not null default '',n double)" % khalix_jail)
for r in conceptes:
    conc,var=r[0],r[1]
    c.execute("insert into %s select up,edat,sexe,comb,CONCAT(indicador,'PRS'),'%s',%s n from %s a inner join nodrizas.jail_centres c on a.up = c.scs_codi where up <> '' and comb like '%%AT%%' and indicador in %s" % (khalix_jail,conc,var,khalix_pre, indicadors_jail))
    c.execute("insert into %s select up,edat,sexe,replace(comb,'AT','ASS'),CONCAT(indicador,'PRS'),'%s',sum(%s) n from %s a inner join nodrizas.jail_centres c on a.up = c.scs_codi where up <> '' and indicador in %s  group by 1,2,3,4,5" % (khalix_jail,conc,var,khalix_pre, indicadors_jail))

c.execute("drop table if exists %s" % pobk)
c.execute("create table %s (id_cip_sec double null,up varchar(5) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '')" % pobk)
c.execute("insert into %s select id_cip_sec,uporigen as up,khalix edat,%s sexe,%s comb from %s a inner join %s b on a.edat=b.edat where %s" % (pobk,sexe,comb,assignada,edats_k,condicio_khalix_pob))
c.execute("drop table if exists %s" % khalix_pob)
c.execute("create table %s (up varchar(5) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',n int)" % khalix_pob)
c.execute("insert into %s select up,edat,sexe,comb,count(1) n from %s where comb like '%%AT%%' group by 1,2,3,4" % (khalix_pob,pobk))
c.execute("insert into %s select up,edat,sexe,replace(comb,'AT','ASS'),count(1) n from %s group by 1,2,3,4" % (khalix_pob,pobk))

c.execute("drop table if exists %s" % pobk_jail)
c.execute("create table %s (id_cip_sec double null,up varchar(5) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '')" % pobk_jail)
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe,%s comb from %s a inner join %s b on a.edat=b.edat where %s and a.id_cip_sec in (select id_cip_sec from %s)" % (pobk_jail,sexe,comb,assignada_jail,edats_k,condicio_khalix_pob,jail_atesa_avui_60))
c.execute("drop table if exists %s" % khalix_pob_jail)
c.execute("create table %s (up varchar(5) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',n int)" % khalix_pob_jail)
c.execute("insert into %s select up,edat,sexe,comb,count(1) n from %s where comb like '%%AT%%' group by 1,2,3,4" % (khalix_pob_jail,pobk_jail))
c.execute("insert into %s select up,edat,sexe,replace(comb,'AT','ASS'),count(1) n from %s group by 1,2,3,4" % (khalix_pob_jail,pobk_jail))         

c.execute("drop table if exists %s" % pob)
c.execute("create table %s (id_cip_sec double null,up varchar(5) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '')" % pob)
c.execute("insert into %s select id_cip_sec,uporigen as up,khalix edat,%s sexe from %s a inner join %s b on a.edat=b.edat where %s" % (pob,sexe,assignada,edats_k,condicio_poblacio))
c.execute("drop table if exists %s" % uba_in)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(20) not null default '',tipus varchar(1) not null default '', tipus_up varchar(1) not null default '',primary key(up,uba,tipus,tipus_up))" % uba_in)
c.execute("insert into %s select a.up,0 uba,'O' tipus,tip_eap tipus_up from %s a inner join nodrizas.cat_centres c on a.up = c.scs_codi where up <> ''  group by up having count(1)>=%d" % (uba_in,pob,minPob))
c.execute("insert into %s select distinct 'UP' up, dni uba,'O' tipus,tip_eap tipus_up from %s a inner join nodrizas.cat_centres c on a.up = c.scs_codi" % (uba_in,odontolegs))
c.execute("drop table if exists %s" % uba1)
c.execute("create table %s (up varchar(5) not null default '',medea varchar(8) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',n int)" % uba1)

c.execute("drop table if exists %s" % pob_jail)
c.execute("create table %s (id_cip_sec double null,up varchar(5) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '')" % pob_jail)
c.execute("insert into %s select id_cip_sec,up,khalix edat,%s sexe from %s a inner join %s b on a.edat=b.edat where %s" % (pob_jail,sexe,assignada_jail,edats_k,condicio_poblacio))
c.execute("drop table if exists %s" % uba_in_jail)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(20) not null default '',tipus varchar(1) not null default 'O', tipus_up varchar(1) not null default 'O',primary key(up,uba,tipus,tipus_up))" % uba_in_jail)
c.execute("insert into %s select a.up,0 uba,'O' tipus,tip_eap tipus_up from %s a inner join nodrizas.jail_centres c on a.up = c.scs_codi where up <> ''  group by up having count(1)>=%d" % (uba_in_jail,pob_jail,minPob))
c.execute("insert into %s select distinct 'UP' up, dni uba,'O' tipus,tip_eap tipus_up from %s a inner join nodrizas.jail_centres c on a.up = c.scs_codi" % (uba_in_jail,odontolegs))
c.execute("drop table if exists %s" % uba1_jail)
c.execute("create table %s (up varchar(5) not null default '',medea varchar(8) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',n int)" % uba1_jail)

c.execute("insert into %s select up,'TIPUS2U2' medea,edat,sexe,count(1) n from %s group by up,edat,sexe" % (uba1,pob))
c.execute("update %s a inner join %s b on a.up=b.scs_codi set a.medea=concat('TIPUS',b.medea,'2') where b.medea<>''" % (uba1,medea))

c.execute("insert into %s select up,'TIPUS2U2' medea,edat,sexe,count(1) n from %s group by up,edat,sexe" % (uba1_jail,pob_jail))
c.execute("update %s a inner join nodrizas.jail_centres b on a.up=b.scs_codi set a.medea=concat('TIPUS',b.medea,'2') where b.medea<>''" % (uba1_jail))

c.execute("drop table if exists %s" % uba2)
c.execute("create table %s (up varchar(5) not null default '',indicador varchar(13) not null default '',medea varchar(8) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',poblacio int)" % uba2)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      c.execute("insert into %s select up,'%s' indicador,medea,edat,sexe,n poblacio from %s" % (uba2,i,uba1))

c.execute("drop table if exists %s" % uba2_jail)
c.execute("create table %s (up varchar(5) not null default '',indicador varchar(13) not null default '',medea varchar(8) not null default '',edat varchar(8) not null default '',sexe varchar(4) not null default '',poblacio int)" % uba2_jail)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      if i in indicadors_jail:
         c.execute("insert into %s select up,'%s' indicador,medea,edat,sexe,n poblacio from %s" % (uba2_jail,i,uba1_jail))

c.execute("drop table if exists %s" % uba3)
c.execute("create table %s like %s" % (uba3,uba2))
c.execute("alter table %s add column esperats double null" % uba3)
c.execute("insert into {} select a.*, 0 esperats from {} a".format(uba3, uba2))
c.execute("alter table {} add index esperades (indicador, medea, edat, sexe)".format(uba3))

c.execute("drop table if exists %s" % uba3_jail)
c.execute("create table %s like %s" % (uba3_jail,uba2_jail))
c.execute("alter table %s add column esperats double null" % uba3_jail)
c.execute("insert into {} select a.*, 0 esperats from {} a".format(uba3_jail, uba2_jail))
c.execute("alter table {} add index esperades (indicador, medea, edat, sexe)".format(uba3_jail))

with open(pathesp, 'rb') as file:
   e=csv.reader(file, delimiter='{', quotechar='|')
   for esp in e:
      i,periode,medea,ct,edat,pob,sexe,prev = esp[0],esp[1],esp[2],esp[3],esp[4],esp[5],esp[6],esp[7]
      if pob == esperadesCond:
          c.execute("update %s set esperats=poblacio*%s where indicador='%s' and medea='%s' and edat='%s' and sexe='%s'" % (uba3,prev,i,medea,edat,sexe))
          c.execute("update %s set esperats=poblacio*%s where indicador='%s' and medea='%s' and edat='%s' and sexe='%s'" % (uba3_jail,prev,i,medea,edat,sexe))
          
c.execute("drop table if exists %s" % uba4)
c.execute("create table %s (up varchar(5) not null default '',indicador varchar(13) not null default '',poblacio int,esperats double)" % uba4)
c.execute("insert into %s select up,indicador,sum(poblacio) poblacio,sum(esperats) esperats from %s group by 1,2" % (uba4,uba3))

c.execute("drop table if exists %s" % uba4_jail)
c.execute("create table %s (up varchar(5) not null default '',indicador varchar(13) not null default '',poblacio int,esperats double)" % uba4_jail)
c.execute("insert into %s select up,indicador,sum(poblacio) poblacio,sum(esperats) esperats from %s group by 1,2" % (uba4_jail,uba3_jail))

c.execute("drop table if exists %s" % uba5)
c.execute("create table %s (up varchar(5) not null default '',indicador varchar(13) not null default '',detectats int,resolts int)" % uba5)
c.execute("insert into %s select up,indicador,sum(den) detectats,sum(num) resolts from %s where %s group by 1,2" % (uba5,pac,condicio_ecap_ind))
c.execute("alter table %s add unique (up,indicador)" % uba4)
c.execute("alter table %s add unique (up,indicador)" % uba5)

c.execute("drop table if exists %s" % uba5_jail)
c.execute("create table %s (up varchar(5) not null default '',indicador varchar(13) not null default '',detectats int,resolts int)" % uba5_jail)
c.execute("insert into %s select up,indicador,sum(den) detectats,sum(num) resolts from %s where %s and id_cip_sec < 0 group by 1,2" % (uba5_jail,pac,condicio_ecap_ind))
c.execute("alter table %s add unique (up,indicador)" % uba4_jail)
c.execute("alter table %s add unique (up,indicador)" % uba5_jail)

c.execute("drop table if exists %s" % ecap_ind)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(20) not null default '',tipus varchar(1) not null default '', indicador varchar(13) not null default '',esperats double,detectats int,resolts int,deteccio double,deteccioPerResultat double,resolucio double,resultat double,resultatPerPunts double,punts double,llistat int,resolucioPerPunts double, invers int, mmin_p double, mmax_p double, tipus_up varchar(1))" % ecap_ind)
c.execute("insert into %s select a.up,0 uba, 'O' tipus, a.indicador,esperats,if(detectats is null,0,detectats),if(resolts is null,0,resolts),0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat,0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p, tip_eap tipus_up from %s a left join %s b on a.up=b.up and a.indicador=b.indicador inner join nodrizas.cat_centres c on a.up = c.scs_codi" % (ecap_ind,uba4,uba5))

c.execute("drop table if exists %s" % ecap_ind_jail)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(20) not null default '',tipus varchar(1) not null default '', indicador varchar(13) not null default '',esperats double,detectats int,resolts int,deteccio double,deteccioPerResultat double,resolucio double,resultat double,resultatPerPunts double,punts double,llistat int,resolucioPerPunts double, invers int, mmin_p double, mmax_p double, tipus_up varchar(1))" % ecap_ind_jail)
c.execute("insert into %s select a.up,0 uba, 'O' tipus, CONCAT(a.indicador,'PRS'),esperats,if(detectats is null,0,detectats),if(resolts is null,0,resolts),0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat,0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p, 'O' tipus_up from %s a left join %s b on a.up=b.up and a.indicador=b.indicador inner join nodrizas.jail_centres c on a.up = c.scs_codi" % (ecap_ind_jail,uba4_jail,uba5_jail))

c.execute("drop table if exists %s" % ecap_indodn)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(20) not null default '',tipus varchar(1) not null default '', indicador varchar(13) not null default '',esperats double,detectats int,resolts int,deteccio double,deteccioPerResultat double,resolucio double,resultat double,resultatPerPunts double,punts double,llistat int,resolucioPerPunts double, invers int, mmin_p double, mmax_p double, tipus_up varchar(1))" % ecap_indodn)
c.execute("insert into %s select 'UP' up, dni uba, 'O' tipus, a.indicador,sum(esperats),sum(detectats),sum(resolts),0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat,0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p, tip_eap tipus_up from %s a inner join %s b on a.up=b.up inner join nodrizas.cat_centres c on a.up = c.scs_codi group by up, dni, indicador" % (ecap_indodn,ecap_ind,odontolegs))
c.execute("insert into %s select * from %s" % (ecap_ind, ecap_indodn))

c.execute("drop table if exists %s" % ecap_indodn_jail)
c.execute("create table %s (up varchar(5) not null default '',uba varchar(20) not null default '',tipus varchar(1) not null default '', indicador varchar(13) not null default '',esperats double,detectats int,resolts int,deteccio double,deteccioPerResultat double,resolucio double,resultat double,resultatPerPunts double,punts double,llistat int,resolucioPerPunts double, invers int, mmin_p double, mmax_p double, tipus_up varchar(1))" % ecap_indodn_jail)
c.execute("insert into %s select 'UP' up, dni uba, 'O' tipus, a.indicador,sum(esperats),sum(detectats),sum(resolts),0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat,0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p, 'O' tipus_up from %s a inner join %s b on a.up=b.up inner join nodrizas.jail_centres c on a.up = c.scs_codi group by up, dni, indicador" % (ecap_indodn_jail,ecap_ind_jail,odontolegs))
c.execute("insert into %s select * from %s" % (ecap_ind_jail, ecap_indodn_jail))

c.execute("update %s set deteccio=if(esperats=0,0,detectats/esperats),resolucio=if(detectats=0,0,resolts/detectats)" % ecap_ind)
c.execute("update %s set deteccio=if(esperats=0,0,detectats/esperats),resolucio=if(detectats=0,0,resolts/detectats)" % ecap_ind_jail)

c.execute("drop table if exists %s" % esperades)
c.execute("create table %s (indicador varchar(13) not null default '',prof double,unique (indicador,prof))" % esperades)
with open(pathespeorig, 'rb') as file:
   eo=csv.reader(file, delimiter='{', quotechar='|')
   for espo in eo:
      indicador,z1,z2,z3,z4,grup,z5,eap = espo[0],espo[1],espo[2],espo[3],espo[4],espo[5],espo[6],espo[7]
      if grup==esperadesCond:
         c.execute("insert into %s values('%s',%s*%s/100)" % (esperades,indicador,esperadesFactor,eap))
c.execute("alter table %s add unique (indicador)" % esperades)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      if baixa=="1":
         ok=1
      else:
         c.execute("insert ignore into %s values('%s', 0)" %(esperades,i))
c.execute("update %s a inner join %s b on a.indicador=b.indicador set deteccioPerResultat=if(deteccio>=prof or prof=0,1,deteccio/prof)" % (ecap_ind,esperades))
c.execute("update %s a inner join %s b on a.indicador=b.indicador set deteccioPerResultat=if(deteccio>=prof or prof=0,1,deteccio/prof)" % (ecap_ind_jail,esperades))
c.execute("update %s set resultat=resolucio" % ecap_ind)
c.execute("update %s set resultat=resolucio" % ecap_ind_jail)


c.execute("drop table if exists %s" % metes)
c.execute("create table %s (indicador varchar(13) not null default '',mmin double,mint double,mmax double,unique (indicador,mmin,mmax))" % metes)
c.execute("drop table if exists %s" % metesOrig)
c.execute("create table %s(indicador varchar(10) not null default '',z1 varchar(10) not null default '',z2 varchar(10) not null default '',meta varchar(10) not null default '',z3 varchar(10) not null default '',z4 varchar(10) not null default '',z5 varchar(10) not null default '',z6 varchar(10) not null default '',valor double)" % metesOrig)
with open(pathmetes, 'rb') as file:
   m=csv.reader(file, delimiter='{', quotechar='|')
   for met in m:
      indicador,z1,z2,meta,z3,z4,z5,valor = met[0],met[1],met[2],met[3],met[4],met[5],met[6],met[7]
      if z4 == esperadesCond:
          c.execute("insert into %s Values('%s','%s','%s','%s','%s','%s','%s','',%s)" % (metesOrig,indicador,z1,z2,meta,z3,z4,z5,valor))
c.execute("insert into %s select a.indicador,a.valor/100,b.valor/100,c.valor/100 from (select indicador,valor from %s where meta='AGMMINRES') a inner join (select indicador,valor from %s where meta='AGMINTRES') b on a.indicador=b.indicador inner join (select indicador,valor from %s where meta='AGMMAXRES') c on a.indicador=c.indicador" % (metes,metesOrig,metesOrig,metesOrig))
c.execute("update %s a inner join %s b on a.indicador=b.indicador set resolucioPerPunts=if(resolucio>=mmax,1,if(resolucio<mmin,0,(resolucio-mmin)/(mmax-mmin))), mmin_p=mmin, mmax_p=mmax" % (ecap_ind,metes))
# c.execute("update %s a inner join %s b on a.indicador=b.indicador set resolucioPerPunts=if(resolucio>=mmax,1,if(resolucio<mmin,0,(resolucio-mmin)/(mmax-mmin))), mmin_p=mmin, mmax_p=mmax" % (ecap_ind_jail,metes))
c.execute("update %s a set llistat=detectats-resolts" % (ecap_ind))
c.execute("update %s a set llistat=detectats-resolts" % (ecap_ind_jail))
c.execute("update %s a set resultatPerPunts=resolucioPerPunts*deteccioPerResultat" % (ecap_ind))
# c.execute("update %s a set resultatPerPunts=resolucioPerPunts*deteccioPerResultat" % (ecap_ind_jail))

c.execute("drop table if exists %s" % ponderacio)
c.execute("create table %s (indicador varchar(13) not null default '',tipus varchar(1) not null default '',valor double, tipus_up varchar(1) not null default '',unique (indicador,tipus, valor, tipus_up))" % ponderacio)
tipus_up_conv = {"AMBTS": "M", "NENS": "N", "ADULTS": "A"}
with open(pathpond, 'rb') as file:
   pon=csv.reader(file, delimiter='{')
   for indicador, _r, _r, _r, tipus_up, _r, _r, punts in pon:
      this = (indicador, 'O', 0 if int(float(punts)) == -1 else float(punts), tipus_up_conv[tipus_up])
      c.execute("insert into {} values('{}', '{}', {}, '{}')".format(ponderacio, *this))
c.execute("drop table if exists %s" % ponderacio_jail)
c.execute("create table %s (indicador varchar(13) not null default '',tipus varchar(1) not null default '',valor double, tipus_up varchar(1) not null default '',unique (indicador,tipus, valor, tipus_up))" % ponderacio_jail)
with open(pathpond_jail, 'rb') as file:
   pon=csv.reader(file, delimiter='{')
   for indicador, _r, _r, _r, tipus_up, _r, _r, punts in pon:
      this = (indicador+"PRS", 'O', 0 if int(float(punts)) == -1 else float(punts), "O")
      c.execute("insert into {} values('{}', '{}', {}, '{}')".format(ponderacio_jail, *this))
c.execute("update %s a inner join %s b on a.indicador=b.indicador and a.tipus_up = b.tipus_up set punts=resolucioPerPunts*deteccioPerResultat*valor" % (ecap_ind,ponderacio))
c.execute("alter table %s add unique (up,uba, indicador)" % ecap_ind)
# c.execute("update %s a inner join %s b on a.indicador=b.indicador and a.tipus_up = b.tipus_up set punts=resolucioPerPunts*deteccioPerResultat*valor" % (ecap_ind_jail,ponderacio))
# c.execute("alter table %s add unique (up,uba, indicador)" % ecap_ind_jail)

with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      c.execute("insert ignore into %s select up,uba,tipus,'%s' indicador,0 esperats,0 detectats,0 resolts,0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat, 0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p, tipus_up from (select up,uba,tipus,tipus_up from %s) a" % (ecap_ind,i,uba_in))

c.execute("drop table if exists %s,%s,%s,%s,%s" % (cataleg,catalegPare,catalegPunts,catalegExclosos,catalegKhx))
c.execute("create table %s (indicador varchar(13),literal varchar(80),ordre int,pare varchar(8),llistat int,invers int,mdet double,mmin double,mint double,mmax double,toShow int,wiki varchar(250),curt varchar(80),pantalla varchar(50))" % cataleg)
c.execute("drop table if exists %s,%s,%s,%s" % (cataleg_jail,catalegPare_jail,catalegPunts_jail,catalegExclosos_jail))
c.execute("create table %s (indicador varchar(13),literal varchar(80),ordre int,pare varchar(8),llistat int,invers int,mdet double,mmin double,mint double,mmax double,toShow int,wiki varchar(250),curt varchar(80),pantalla varchar(50))" % cataleg_jail)
c.execute("drop table if exists %s" %ind)
c.execute("create table %s (id int, indicador varchar(13), literal varchar(80),grup varchar(8),grup_desc varchar(80),baixa_ int, dbaixa varchar(80),llistats int,ordre int,grup_ordre int)" % ind)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      if baixa=="1":
         ok=1
      else:
         c.execute("insert into %s values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (ind,id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre))
c.execute("drop table if exists %s" %ind_jail)
c.execute("create table %s (id int, indicador varchar(13), literal varchar(80),grup varchar(8),grup_desc varchar(80),baixa_ int, dbaixa varchar(80),llistats int,ordre int,grup_ordre int)" % ind_jail)
with open(pathcprs, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      if baixa=="1":
         ok=1
      else:
         c.execute("insert into %s values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (ind_jail,id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre))
c.execute("insert into %s select distinct a.indicador indicador,literal,ordre,grup pare,llistats llistat,0 invers,prof mdet,if(mmin is null,0,mmin),if(mint is null,0,mint),if(mmax is null,0,mmax),if(a.indicador in %s, 0, 1) toShow,concat('http://10.80.217.201/sisap-umi/indicador/codi/',a.indicador) wiki,'' curt,'GENERAL' pantalla from %s a inner join %s b on a.indicador=b.indicador left join %s c on a.indicador=c.indicador where baixa_=0" % (cataleg,hide,ind,esperades,metes))
c.execute("insert into %s select distinct a.indicador indicador,literal,ordre,grup pare,llistats llistat,0 invers,prof mdet,0 mmin,0 mint,0 mmax,if(a.indicador in %s, 0, 1) toShow,concat('http://10.80.217.201/sisap-umi/indicador/codi/',a.indicador) wiki,'' curt,'GENERAL' pantalla from %s a inner join %s b on a.indicador=CONCAT(b.indicador, 'PRS') where baixa_=0" % (cataleg_jail,hide,ind_jail,esperades))
c.execute("create table %s (pare varchar(10) not null default '',literal varchar(80) not null default '',ordre int,curt varchar(80),proces varchar(50))" % catalegPare)
c.execute("insert into %s select distinct grup pare, grup_desc literal,grup_ordre ordre,''curt,'' proces from %s where baixa_=0" % (catalegPare,ind))
c.execute("create table %s (pare varchar(10) not null default '',literal varchar(80) not null default '',ordre int,curt varchar(80),proces varchar(50))" % catalegPare_jail)
c.execute("insert into %s select distinct grup pare, grup_desc literal,grup_ordre ordre,''curt,'' proces from %s where baixa_=0" % (catalegPare_jail,ind_jail))
c.execute("create table %s like %s" % (catalegPunts,ponderacio))
c.execute("insert into %s select * from %s" % (catalegPunts,ponderacio))
c.execute("create table %s like %s" % (catalegPunts_jail,ponderacio_jail))
c.execute("insert into %s select indicador, tipus, valor, 'O' from %s" % (catalegPunts_jail,ponderacio_jail))
# c.execute("insert into %s select INDICADOR, 'O', VALOR, TIPUS_UP from %s where TIPUS_UP = 'M'" % (catalegPunts,catalegPunts))

c.execute("create table %s (indicador varchar(10) not null default'',desc_ind varchar(300) not null default'',grup varchar(10) not null default'', grup_desc varchar(300) not null default'')" % catalegKhx)
with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      if baixa=="1":
         ok=1
      else:
         c.execute("insert into %s Values('%s','%s','%s','%s')" % (catalegKhx,i,desc,grup,grupdesc))     
         
c.execute("create table %s (codi int,descripcio varchar(150),ordre int)" % catalegExclosos)
for r in ecap_cataleg_exclosos:
    codi,desc,ordre=r[0],r[1],r[2]
    c.execute("insert into %s VALUES (%d,'%s',%d)" % (catalegExclosos,codi,desc,ordre))  

c.execute("create table %s (codi int,descripcio varchar(150),ordre int)" % catalegExclosos_jail)
for r in ecap_cataleg_exclosos:
    codi,desc,ordre=r[0],r[1],r[2]
    c.execute("insert into %s VALUES (%d,'%s',%d)" % (catalegExclosos_jail,codi,desc,ordre))

taula="eqa_ind.mst_eqa_literals_curts"
sql="update {0} a inner join {1} b on a.indicador=b.indicador set a.curt=b.curt".format(cataleg,taula)
execute(sql,db)
sql="update {0} a inner join {1} b on a.pare=b.indicador set a.curt=b.curt".format(catalegPare,taula)
execute(sql,db)
sql="update {0} a inner join {1} b on a.indicador=b.indicador set a.curt=b.curt".format(cataleg_jail,taula)
execute(sql,db)
sql="update {0} a inner join {1} b on a.pare=b.indicador set a.curt=b.curt".format(catalegPare_jail,taula)
execute(sql,db)
    
#c.execute("update exp_ecap_catalegpunts set valor=0")
#c.execute("update exp_ecap_cataleg set mdet=0")

conn.close()

print strftime("%Y-%m-%d %H:%M:%S")