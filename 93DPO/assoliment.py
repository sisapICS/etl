# coding: latin1

"""
El 2017 aquest proc�s creuava fitxes amb assoliment per pujar nosaltres
tant EQA com AG. Pel 2018 simplement crea un Excel amb EQA per enviar
als territoris. Si es vol recuperar l'anterior caldr� seguir la hist�ria
del control de versions.
"""

import collections as c
import datetime as d

import sisapUtils as u


YEAR = d.datetime.now().year - 1
FILE = u.tempFolder + "EQA_DPO_{}.CSV".format(YEAR)


class DPO(object):
    """."""

    def __init__(self):
        """."""
        self.year = YEAR
        self.get_centres()
        self.get_uba_to_dni()
        self.get_up_to_dni()
        self.get_assoliment()
        self.get_export()
        self.export_data()

    def get_centres(self):
        """."""
        self.up_to_br = {}
        sql = "select {}, {}, amb_desc from {} where amb_codi <> '00'"
        for params in (("scs_codi", "ics_codi", "cat_centres"),
                       ("up", "br", "ass_centres")):
            for up, br, amb in u.getAll(sql.format(*params), "nodrizas"):
                if up not in self.up_to_br:
                    self.up_to_br[up] = (br, amb)

    def get_uba_to_dni(self):
        """."""
        sql = "select up, uab, tipus, ide_dni \
               from cat_professionals \
               where tipus in ('M', 'I')"
        self.uba_to_dni = {row[:3]: row[3]
                           for row in u.getAll(sql, "import")}

    def get_up_to_dni(self):
        """."""
        self.up_to_dni = c.defaultdict(lambda: c.defaultdict(set))
        sql = "select up, dni from mst_professionals"
        for db in ("odn", "social"):
            for up, dni in u.getAll(sql, db):
                self.up_to_dni[db[:3]][up].add(dni)

    def get_assoliment(self):
        """."""
        self.assoliment = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(list)))  # noqa
        sql = "select eqa, up, uab, tipus, punts \
               from {}sintetic \
               where dataany = {} and datames = 12 and up <> 'UP'"
        for tb in ("eqa", "ped", "odn", "soc", "ass"):
            for eqa, up, uba, tip, punts in u.getAll(sql.format(tb, self.year), "pdp"):  # noqa
                if up in self.up_to_br:
                    br, amb = self.up_to_br[up]
                    if tb in ("eqa", "ped"):
                        dnis = set([self.uba_to_dni.get((up, uba, tip))])
                    elif tb in ("odn", "soc"):
                        dnis = self.up_to_dni[tb][up]
                    elif tb == "ass":
                        dnis = set([uba])
                    if dnis:
                        for dni in dnis:
                            id = (u.getNif(dni), br, amb)
                            self.assoliment[tb][id][eqa].append(punts)

    def get_export(self):
        """."""
        self.export = []
        for tipus, dades in self.assoliment.items():
            for (dni, br, amb), assol in dades.items():
                if len(assol["G"]) == 1:
                    status = "Correcte"
                    indiv = "Individualizat" if "I" in assol else "General"
                    punts = str(round(assol["I"][0] if "I" in assol else assol["G"][0], 2)).replace(".", ",")  # noqa
                else:
                    status = "Consta m�s d'un resultat per aquest DNI en aquesta UP"  # noqa
                    indiv = None
                    punts = None
                this = (amb, tipus, br, dni, status, indiv, punts)
                self.export.append(this)

    def export_data(self):
        """."""
        header = [("gerencia", "eqa", "br", "dni", "status", "tipus", "punts")]
        u.writeCSV(FILE, header + sorted(self.export), sep=";")


if __name__ == "__main__":
    DPO()
