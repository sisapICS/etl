# coding: latin1

"""
.
"""

import sisapUtils as u


TABLE = "sidiap_padris_projectes"
DATABASE = "redics"
USERS = ("PREDUMMP", "PREDUMBO", "PREDULMB", "PREDUEHE", "PREDUMAP")


class Padris(object):
    """."""

    def __init__(self):
        """."""
        self.filename = raw_input("nom del fitxer (amb ext) a tempFolder (1 columna sense header): ")  # noqa
        self.projecte = raw_input("# GIR del projecte: ")
        self.create_table()
        self.prepare_upload()
        self.upload_data()
        self.set_grants()

    def create_table(self):
        """."""
        cols = "(id varchar(40), projecte varchar(25))"
        u.createTable(TABLE, cols, DATABASE, rm=False)

    def prepare_upload(self):
        """."""
        file = u.tempFolder + self.filename
        self.upload = [(hash, self.projecte) for hash, in u.readCSV(file)]

    def upload_data(self):
        exists = u.getOne("select 1 from {table} where projecte = '{prj}'".format(table = TABLE, prj = self.projecte), DATABASE)
        delete_sql = """delete from {table} where projecte = '{prj}'""".format(table = TABLE, prj = self.projecte)
        do = len(self.upload)
        if exists and do:
            u.execute(delete_sql, DATABASE)
            u.listToTable(self.upload, TABLE, DATABASE)
        else:
            u.listToTable(self.upload, TABLE, DATABASE)


    def set_grants(self):
        """da los grants"""
        u.grantSelect(TABLE, USERS, DATABASE)


if __name__ == "__main__":
    Padris()
