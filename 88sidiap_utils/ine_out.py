# -*- coding: utf8 -*-

"""
Fitxer d'entrada: columna única amb hash:sector
Fitxers de sortida:
  ___ine: fitxer per entregar
  ___err: registres erronis
  ___rel: relació hash - id
  ___nf: hash:sector no trobats
La gestió i custòdia d'aquests fitxers és responsabilitat de l'usuari.
"""

import collections as c

import sisapUtils as u


class INE(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.resultat = {"ine": [], "rel": [], "nf": [], "err": []}
        txt = "Nom del fitxer (amb ext) a tempFolder: "
        self.filename = u.tempFolder + raw_input(txt)
        self.get_input_data()
        self.get_sector_data()
        self.write_output_files()
        self.print_result()

    def get_input_data(self):
        """Dades del fitxer."""
        self.input_data = c.defaultdict(set)
        for i, (hash, sector) in enumerate(u.readCSV(self.filename, sep=":")):
            if sector in u.sectors and len(hash) == 40:
                self.input_data[sector].add((str(i).zfill(8), hash.upper()))
            else:
                self.resultat["err"].append((":".join((hash, sector)),))

    def get_sector_data(self):
        """
        Extracció seqüencial (21 workers) de les dades dels sectors.
        """
        # self.get_sector_data_worker(('6211', self.input_data['6211']))
        mega = u.multiprocess(self.get_sector_data_worker,
                              self.input_data.items())
        for ine, relacio, no_trobats in mega:
            self.resultat["ine"].extend(ine)
            self.resultat["rel"].extend(relacio)
            self.resultat["nf"].extend(no_trobats)

    def get_sector_data_worker(self, dades):
        """Worker de get_sector_data."""
        sector, pacients = dades
        db = "{}a".format(sector)
        flt = "flt_car"
        u.execute("delete from {}".format(flt), db)
        u.listToTable([(hash,) for (i, hash) in pacients], flt, db)
        hash_to_id = {hash: i for (i, hash) in pacients}
        sql = "select d.filtre, \
                      substr(rpad(a.usua_nom, 20, ' '), 0, 20), \
                      substr(rpad(a.usua_cognom1, 25, ' '), 0, 25), \
                      substr(rpad(decode(a.usua_cognom2, '', ' ', a.usua_cognom2), 25, ' '), 0, 25), \
                      a.usua_sexe, \
                      to_char(to_date(a.usua_data_naixement, 'J'), 'DD'), \
                      to_char(to_date(a.usua_data_naixement, 'J'), 'MM'), \
                      to_char(to_date(a.usua_data_naixement, 'J'), 'YYYY'), \
                      '  ', \
                      '   ', \
                      lpad(decode(a.usua_provincia, '', '  ', a.usua_provincia), 2, '0'), \
                      substr(decode(a.usua_localitat, '', '     ', a.usua_localitat), 2, 3), \
                      substr(rpad(decode(a.usua_dni, '', ' ', a.usua_dni), 8, ' '), 0, 8), \
                      '  ', \
                      '  ', \
                      '    ', \
                      ' ', \
                      '  ', \
                      '   ', \
                      '  ', \
                      '  ', \
                      '    ', \
                      '    ' \
               from usutb040 a, \
                    pdptb101 b, \
                    usutb011 c, \
                    flt_car d \
               where d.filtre = c.cip_cip_anterior \
                     and c.cip_usuari_cip = b.usua_cip_cod \
                     and b.usua_cip = a.usua_cip"
        ine = []
        relacio = []
        trobats = set()
        for row in u.getAll(sql, db):
            ine.append(("".join((hash_to_id[row[0]],) + row[1:]),))
            relacio.append((":".join((row[0], sector)), hash_to_id[row[0]]))
            trobats.add(row[0])
        no_trobats = [(":".join((pacient, sector)),)
                      for pacient in hash_to_id
                      if pacient not in trobats]
        return (ine, relacio, no_trobats)

    def write_output_files(self):
        """Fitxers de sortida."""
        filename = ".".join(self.filename.split(".")[:-1])
        for concept, dades in self.resultat.items():
            file = "{}___{}.txt".format(filename, concept)
            u.writeCSV(file, dades, sep="@")

    def print_result(self):
        """Imprimeix per pantalla un resum del procés."""
        tot = sum([len(dades) for dades in self.input_data.values()])
        err = len(self.resultat["err"])
        ine = len(self.resultat["ine"])
        nf = len(self.resultat["nf"])
        print "origen: {} registres".format(tot + err)
        print "erronis: {} registres".format(err)
        print "convertits: {} registres".format(ine)
        print "no trobats: {} registres".format(nf)


if __name__ == "__main__":
    INE()
