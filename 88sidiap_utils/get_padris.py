#!/usr/bin/python
# -*- coding: utf-8 -*-

# Librerias
import sisapUtils as u
from secret import aquas_cloud
# from secret import sidiap_cloud
import owncloud as o
from datetime import datetime as dt
# import easywebdav
import os
import shutil
import zipfile
from pyunpack import Archive


class Main(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_project_variables()
        self.prepare_environment()
        self.get_nextcloud()
        self.unpack_project()
        self.clean_workingDirectory()
        self.get_parameters()
        # self.get_hash(NOM_DEL_FITXER = self.nom_del_fitxer, SEPARADOR_DE_COLUMNES = self.separador_de_columna, ID_ENTRADA = self.id_entrada, POSICIO_ID = self.posicio_id )  # noqa
        # self.garbage_out()
        # self.put_zip()
        # self.put_nexica()

    def get_project_variables(self):
        """
        Preguntes inicials a l'usuari.

            padrisFolder: ej ->  "P_569 Arteriopatia  periferica (ICS 343)"
                (no puede llevar acentos, cambialo en: nextcloud.aquas.cat)
                esto es porque con acentos se puede descargar del servidor,
                pero ...
                la libreria pyunpack solo soporta ASCII (sin acentos)

            padrisId: ej -> '569'
                El codigo del proyecto en padris

            icsId: ej -> '343'
                El codigo del proyecto en sidiap
        """
        padrisFolder = raw_input('padrisFolder (si tiene acentos, cambialo en: nextcloud.aquas.cat): ')  # noqa
        padrisId = raw_input('padrisId (# int): ')
        icsId = raw_input('icsId (# int): ')
        self.aquasUsr = raw_input('ermen, leo: ')

        isoDate = dt.date(dt.now()).isoformat()
        zExtension = '.zip'

        fullPrjFolder = "{}P{}_ICS{}_{}".format(u.tempFolder, padrisId, icsId, isoDate)  # noqa
        fullPrjZip = "{}{}".format(fullPrjFolder, zExtension)

        self.prj_vars = {
                'padrisFolder': padrisFolder,
                'padrisId': padrisId,
                'icsId': icsId,
                'fullPrjFolder': fullPrjFolder,
                'fullPrjZip': fullPrjZip
                }
        print(self.prj_vars)

    def prepare_environment(self):
        """
        Trunca y [fullPrjFolder] sera la carpeta de extracción de [fullPrjZip]
            - los ficheros zip no hace falta pre-eliminarlos, se automachacan
            - self.fullPrjZip = "{}{}".format(self.fullPrjFolder, zExtension)
        """
        try:
            shutil.rmtree(self.prj_vars['fullPrjFolder'])
        except Exception as e:
            print(e, 'No hace falta eliminar carpeta de proyecto')

    def get_nextcloud(self):
        """ Descarga de nextcloud en fichero_zip destino """
        srv = aquas_cloud[self.aquasUsr]['url']
        usr = aquas_cloud[self.aquasUsr]['usr']
        token = aquas_cloud[self.aquasUsr]['token']
        oc = o.Client(srv)
        oc.login(usr, token)
        oc.get_directory_as_zip(self.prj_vars['padrisFolder'],
                                self.prj_vars['fullPrjZip'])
        oc.logout()

    def unpack_project(self):
        '''
        Descomprime en carpeta con el nombre del zip:
            - Descomprimo al directorio raiz los archivos hoja zip,
            - y muevo todas los ficheros hoja (comprimidos o no) a la raiz
        '''
        zf = zipfile.ZipFile(self.prj_vars['fullPrjZip'])
        zipfile.ZipFile.extractall(zf, path=self.prj_vars['fullPrjFolder'])

        # consigue recursivamente la listOfFiles y listOfDirs
        # de un arbol de ficheros partiendo de la carpeta del proyecto
        listOfFiles = list()
        self.listOfDirs = set()
        for (dirpath, dummy, filenames) in os.walk(self.prj_vars['fullPrjFolder']):  # noqa
            listOfFiles += [os.path.join(dirpath, file) for file in filenames]
            self.listOfDirs.add(dirpath)

        # Quito el directorio raiz de la lista
        # (la usare para borrar directorios intermedios)
        self.listOfDirs.remove(self.prj_vars['fullPrjFolder'])

        # Descomprimo al directorio raiz los archivos hoja zip,
        # y muevo todas las hojas (comprimidos o no) a la raiz
        zipExtensions = ['.zip', '.7z']
        for fil in listOfFiles:
            for ext in zipExtensions:
                if ext in fil:
                    Archive(fil).extractall(self.prj_vars['fullPrjFolder'])
            else:
                # este else no esta funcionado,
                # mueve tanto zip como no zip a la raiz,
                # esto sucede porque primero procesa .zip y luego .7z
                # cuando hace la segunda pasada...
                # ... los ficheros ya no estan en su origen.
                try:
                    shutil.move(fil, self.prj_vars['fullPrjFolder'])
                except shutil.Error:
                    print("""
                            Ojo!!! no pude mover algunos ficheros a la raiz,
                            y he borrado su origen: comprueba en NextCloud
                            """)
                except IOError:
                    print("""
                            IOError: parece que no existe el fichero
                            que quiero mover a la raiz, es el .7z""")

    def clean_workingDirectory(self):
        """
        - Elimina las carpetas intermedias:
            Ahora todo esta en la raiz, con el fichero zip como garbage.
        - Tambien elimino los ficheros zip de la raiz:
            Ahora solo quedan ficheros de trabajo.
        """

        for path in self.listOfDirs:
            shutil.rmtree(path)

        self.listOfFiles = list()
        for (dirpath, dummy, filenames) in os.walk(self.prj_vars['fullPrjFolder']):  # noqa
            self.listOfFiles += [os.path.join(dirpath, file)
                                 for file in filenames]
        self.listOfFiles = set(self.listOfFiles)

        # elimino los ficheros zip de la raiz:
        #   ahora solo quedan ficheros de trabajo
        self.listOfFiles2 = self.listOfFiles.copy()
        zipExtensions = ['.7z', '.zip']
        for fil in self.listOfFiles:
            for ext in zipExtensions:
                if ext in fil:
                    os.remove(fil)
                    self.listOfFiles2.remove(fil)

        self.listOfFiles = self.listOfFiles2

    def get_parameters(self):
        '''
        Consigue parametros para id_change
            - asigna a NOM_DEL_FITXER el [episodisFile]
                antes buscaba 'epi' en el nombre
                pero es imposible que parametricen
            - TE_HEADER (default = 's')
            - determina ID_ENTRADA, SEPARADOR_DE_COLUMNAS Y POSICIO_ID
                    -   quedarme con el texto encontrado:
                            ID_ENTRADA (default = 'nia')
                    -   quedarme con el caracter siguiente como separador:
                            SEPARADOR_DE_COLUMNAS (default = '|')
                    -   quedarme con la posición del ID_ENTRADA:
                            POSICIO_ID (default = 1)
        '''

        # imposible que paramtericen nombre de ficheros con ids a convertir
        # episodis_lemas = ['epi', '_cap.', 'cohort', 'Cohort']

        ids = ['cip', 'nia', 'NIA', 'dni', 'cip_encript']
        for fname in self.listOfFiles:
            # for lema in episodis_lemas if lema in fname:
            # if '.zip' not in fname or '.7z' not in fname:
            self.nom_del_fitxer = fname.replace(u.tempFolder, '', 1)  # noqa
            print('\n')
            print('nom_del_fitxer: {}'.format(self.nom_del_fitxer))  # noqa
            n = 0
            fobject = u.readCSV(fname)
            for row in fobject:
                while n == 0:
                    headString = row[0]
                    for i in ids:
                        if i in headString:
                            self.id_entrada = i
                            pos_ini = headString.index(self.id_entrada)
                            self.separador_de_columnas = (headString[pos_ini + len(self.id_entrada)])  # noqa
                            headList = headString.split(self.separador_de_columnas)  # noqa
                            self.posicio_id = headList.index(self.id_entrada)  # noqa
                            print('separador de columnas: {}'.format(self.separador_de_columnas))  # noqa
                            print('te header: s')
                            print('id entrada: {}'.format(self.id_entrada).lower())
                            print('id amb sha1: n')
                            print('posicio id: {}'.format(self.posicio_id))
                    n += 1

    def get_hash(self, NOM_DEL_FITXER, SEPARADOR_DE_COLUMNES='|', TE_HEADER='s', ID_ENTRADA='nia', ID_AMB_SHA1='n', POSICIO_ID=1):  # noqa
        """
        TODO: implementar este metodo
        llamar a id_change2.py pasandoloe los parametros identificados por:
            get_parameters()
        """

    def garbage_out(self):
        """
        TODO: implementar este metodo
            - eliminar fichero original (like '%epi%')
            - si existe, eliminar ficheros *___nf y *___dup
                (ej:   rm /temp/P482_ICS269_2019-10-22/*___nf.*
                          /temp/P482_ICS269_2019-10-22/*___dup.*
                )
        """

    def put_zip(self):
        """ TODO: implementar este metodo """

    def put_nexica(self):
        """ TODO: implementar este metodo """


""" a partir de aqui SCRATCH ZONE """

# webdav_nx = easywebdav.connect('nexicafiles.com',
#                                 username=nexicaUsr,
#                                 password=nexicaToken,
#                                 protocol='https',
#                                 port=443,
#                                 verify_ssl=False)

# print webdav_nx.ls("/owncloud/remote.php/dav/files/sidiap")

# ocPath = "index.php/f/26871"

# webdav = easywebdav.connect('nextcloud.aquas.cat',
#                              username=usr,
#                              password=pwd,
#                              protocol='https',
#                              port=443,
#                              verify_ssl=False)
# print webdav.ls("/remote.php/webdav/"+ocPath)
# print webdav.ls("/remote.php/webdav/82158/")


if __name__ == '__main__':
    Main()
