from sisapUtils import *
from collections import defaultdict,Counter
import sys

ped = "zpedia"
nod = "znodrizas"

sql='select concat(year(data_ext),month(data_ext)) from dextraccio'
for d in getOne(sql, nod):
    dcalcul=d

OutFile= tempFolder + '01toxicsAdolescents' + dcalcul + '.txt'


ics='where ep=0208'

def tabacConverter(tab):

    if tab == 1:
        return 'Fumador'
    elif tab == 2:
        return 'Exfumador'
    elif tab == 0:
        return 'No fumador'
    else:
        return 'Sense info'
        
centres={}
sql="select scs_codi from cat_centres {}".format(ics)
for up, in getAll(sql,nod):
    centres[up] = True

printTime('inici adolescents')

toxicsAdolescents, Fumen = {},{}

sql = "select id_cip_sec,tab from eqa_tabac where last=1"
for id, tab in getAll(sql,nod):
    Fumen[id] = {'tab':tab}
    
sql =  "select id_cip_sec,up,sexe,ates,num,den from mst_indicadors_pacient where indicador='EQA0713' and excl=0"
for id,up,sexe,ates,num,den in getAll(sql,ped):
    try:
        centres[up]
    except KeyError:
        continue
    try:
        tab = Fumen[id]['tab']
    except KeyError:
        tab = 9999
    tabac = tabacConverter(tab)
    if (up,sexe,ates,tabac) in toxicsAdolescents:   
        toxicsAdolescents[(up,sexe,ates,tabac)]['num'] += num
        toxicsAdolescents[(up,sexe,ates,tabac)]['den'] += den
        toxicsAdolescents[(up,sexe,ates,tabac)]['n'] += 1
    else:
        try:
            toxicsAdolescents[(up,sexe,ates,tabac)] = {'num':num,'den':den,'n':1}
        except KeyError:
            ok=1

with openCSV(OutFile) as c:
    for (up,sexe,ates,tabac),d in toxicsAdolescents.items():
        c.writerow([up,sexe,ates,tabac,d['num'],d['den'],d['n']])

        
printTime('Fi adolescents')