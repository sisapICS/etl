# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict,Counter
import csv,os,sys
from time import strftime

ped = "pedia"
nod = "nodrizas"
imp = "import"

Pobatesa = True

path0 = os.path.realpath("../")
path30 = path0.replace("\\","/") + "/09catsalut/dades_noesb/csl_IMC30infants.txt"


sql='select concat(year(data_ext),month(data_ext)),year(data_ext),data_ext from dextraccio'
for d,y,e in getAll(sql, nod):
    dcalcul=d
    dataAny = y
    dataExt = e

OutFile= tempFolder + '02EQAObesitat_Premsa_' + dcalcul + '.txt'
OutFile2= tempFolder + '02PSObesitat_Premsa_' + dcalcul + '.txt'

all_pob = {}
sql = 'select id_cip_sec, usua_sexe, usua_data_naixement, year(usua_data_naixement) from assignada'
for id,  sexe, naix, anynaix in getAll(sql, imp):
    all_pob[id] = {'sexe':sexe, 'naix': naix, 'anynaix':anynaix}

def ageGroupConverter(edat):
    if 11 <= edat <= 14:
        return 'ADOLESCENTS'
    elif 6 <= edat <= 10:
        return 'NENS'
    elif 2 <= edat <= 5:
        return 'de dos a 5'
    elif 0 <= edat <= 1:
        return 'nadons'
    else:
        return 'ERROR'
    
i30ped={}
with open(path30, 'rb') as file:
    p=csv.reader(file, delimiter=';', quotechar='|')
    for ind in p:
        s,mesos,val=ind[0],ind[1],ind[2]
        mesos=int(mesos)
        i30ped[(s,mesos)]={'valimc': val}
        
ics='where ep=0208'
       
centres={}
sql="select scs_codi from cat_centres {}".format(ics)
for up, in getAll(sql,nod):
    centres[up] = True

printTime('inici EQA0708')

cribObes = {}    
sql =  "select id_cip_sec,up,edat,sexe,ates,num,den from mst_indicadors_pacient where indicador='EQA0708' and excl=0"
for id,up,edat,sexe,ates,num,den in getAll(sql,ped):
    try:
        centres[up]
    except KeyError:
        continue
    if (edat,sexe,ates) in cribObes:   
        cribObes[(edat,sexe,ates)]['num'] += num
        cribObes[(edat,sexe,ates)]['den'] += den
    else:
        try:
            cribObes[(edat,sexe,ates)] = {'num':num,'den':den}
        except KeyError:
            ok=1
try:
    remove(OutFile)
except:
    pass            
with openCSV(OutFile) as c:
    for (edat,sexe,ates),d in cribObes.items():
        c.writerow([edat,sexe,ates,d['num'],d['den']])

printTime('Inici Obesitat')
anys = list(range(2009, 2050))
evolucioObesitat = {}
for dany in anys:
    if dany > dataAny:
        continue
    else:
        pobInfantil = {}
        if dany == dataAny:
            sql = "select id_cip_sec,up,sexe,{0} - year(data_naix),edat,data_naix from assignada_tot {1}".format(dany, ' where ates=1' if Pobatesa else '')
            for id,up,sexe,edat,edat2,naix in getAll(sql,nod): 
                if dany == dataAny:
                    edat = edat2
                    dara = dataExt
                else:
                    edat = edat
                    dara = datetime(year=dany, month=int(12), day=int(31))
                try:
                    if centres[up]:
                        if 0 <= edat <= 14:
                            mesos = monthsBetween(naix,dara)
                            pobInfantil[id] = {'sexe':sexe,'edat':edat,'mesos':mesos}
                            if (dany,edat,sexe,ageGroupConverter(edat)) in evolucioObesitat:
                                evolucioObesitat[(dany,edat,sexe,ageGroupConverter(edat))]['pob'] += 1
                            else:
                                evolucioObesitat[(dany,edat,sexe,ageGroupConverter(edat))] = {'ps':0,'var':0,'tot':0,'pob':1}
                except KeyError:
                    continue
        else:
            sql = "select id_cip_sec, up from assignadahistorica where dataany={0} {1}".format(dany, ' and ates=1' if Pobatesa else '')
            for id, up in getAll(sql, imp):
                if up in centres:
                    try:
                        sexe = all_pob[id]['sexe']
                        naix = all_pob[id]['naix']
                        anynaix = all_pob[id]['anynaix']
                        edat = dany - anynaix
                    except KeyError:
                        continue
                    dara = datetime(year=dany, month=int(12), day=int(31))
                    if 0 <= edat <= 14:
                        mesos = monthsBetween(naix,dara)
                        pobInfantil[id] = {'sexe':sexe,'edat':edat,'mesos':mesos}
                        if (dany,edat,sexe,ageGroupConverter(edat)) in evolucioObesitat:
                            evolucioObesitat[(dany,edat,sexe,ageGroupConverter(edat))]['pob'] += 1
                        else:
                            evolucioObesitat[(dany,edat,sexe,ageGroupConverter(edat))] = {'ps':0,'var':0,'tot':0,'pob':1}
        problemes = {}        
        sql = "select id_cip_sec from eqa_problemes where ps='239' and year(dde)<={}".format(dany)
        for id, in getAll(sql,nod):
            try:
                sexe = pobInfantil[id]['sexe']
                edat = pobInfantil[id]['edat']
                mesos = pobInfantil[id]['mesos']
            except KeyError:
                continue
            if (dany,edat,sexe,ageGroupConverter(edat)) in evolucioObesitat:
                evolucioObesitat[(dany,edat,sexe,ageGroupConverter(edat))]['ps'] += 1
                evolucioObesitat[(dany,edat,sexe,ageGroupConverter(edat))]['tot'] += 1
                problemes[id] = True
        maxDatIMC={}
        sql="select id_cip_sec,vu_val,vu_dat_act from variables where vu_cod_vs='TT103' and year(vu_dat_act) <={0}".format(dany)
        for id_cip_sec,val,dat in getAll(sql,imp):
            try:
                if id_cip_sec in pobInfantil:
                    if id_cip_sec in maxDatIMC:
                        if dat>maxDatIMC[id_cip_sec]['data']:
                            maxDatIMC[id_cip_sec]={'data':dat,'valor':val}
                    else:
                        maxDatIMC[id_cip_sec]={'data':dat,'valor':val}
            except KeyError:
                continue
        for id,d in maxDatIMC.items():
            val=d['valor']
            try:
                sexe = pobInfantil[id]['sexe']
                edat = pobInfantil[id]['edat']
                mesos = pobInfantil[id]['mesos']
                vali30ped=i30ped[(sexe,mesos)]['valimc']
            except KeyError:
                continue
            if float(vali30ped)<float(val):
                if (dany,edat,sexe,ageGroupConverter(edat)) in evolucioObesitat:
                    evolucioObesitat[(dany,edat,sexe,ageGroupConverter(edat))]['var'] += 1
                    try:
                        if problemes[id]:
                            ok =1
                    except KeyError:
                        evolucioObesitat[(dany,edat,sexe,ageGroupConverter(edat))]['tot'] += 1
try:
    remove(OutFile2)
except:
    pass                 
with openCSV(OutFile2) as c:
    for (dany,edat,sexe,grup),d in evolucioObesitat.items():
        ps = d['ps']
        pob = d['pob']
        tot = d['tot']
        var = d['var']
        c.writerow([dany,edat,sexe,grup,ps,var,tot,pob])
printTime('Fi procés')
