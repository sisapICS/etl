# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict,Counter
import csv,os,sys
from time import strftime

imp = "import"
nod="nodrizas"

sql='select concat(year(data_ext),month(data_ext)),year(data_ext) from dextraccio'
for d,y in getAll(sql, nod):
    dcalcul=d
    dataAny = y

OutFile = tempFolder + '04POCS_' + dcalcul + '.txt'
OutFileUP = tempFolder + '04POCS_UP_' + dcalcul + '.txt'

OutFile2 = tempFolder + '04POCS_perTerritori_' + dcalcul + '.txt'
printTime('Inici procés')

visites80 = {}
assig = {}
sql = "select id_cip_sec from assignada_tot where edat>=80 and year(last_visit)='2015' and month(last_visit)=6"
for id, in getAll(sql,nod):
    assig[id] = True 
    visites80[id] = {'num':0,'den':1}
    
sql = "select id_cip_sec from crg where left(crg,1)>6"
for id, in getAll(sql,imp):
    if id in assig:
        continue
    else:
        assig[id] = True
        visites80[id] = {'num':0,'den':1}
'''
visites80 = {}

sql= "select table_name from information_schema.tables where table_schema='%s' and table_name like '%s\_%%'" % (imp,'visites')
particions = {}
for particio in getAll(sql,imp):
    sql = "select if(date_format(visi_data_visita,'%Y%m') between date_format(date_add(data_ext,interval - 1 month),'%Y%m') and date_format(data_ext,'%Y%m'),1,0) from {},nodrizas.dextraccio  limit 1".format(particio[0])
    try:
        enter, = getOne(sql,imp)
    except:
        enter = 0
    if enter == 1:
        particions[particio[0]] = True

for particio,t in particions.items():   
    sql = "select id_cip_sec from {0} where visi_data_baixa=0".format(particio)
    for id, in getAll(sql,imp):
       try:
            if id in assig:
                if id in visites80:
                    continue
                else:
                    visites80[id] = {'num':0,'den':1}
       except KeyError:
           continue 
'''

territoris = {}
sql = "select up.up_codi_up_scs ,amb.amb_desc_amb  from    cat_gcctb007 br \
        inner join cat_gcctb008 up on br.up_codi_up_ics=up.up_codi_up_ics \
        inner join cat_gcctb006 sap on br.dap_codi_dap=sap.dap_codi_dap \
        inner join cat_gcctb005 amb on sap.amb_codi_amb=amb.amb_codi_amb "
for up, territori in getAll(sql,imp):
    territoris[up] = {'ambit':territori}



POCS = Counter()
POCSUP = Counter()
no80 = 0
POCSterrit = Counter()
sql = "select id_cip_sec,au_val, year(au_dat_act),month(au_dat_act), au_dat_act,au_up from activitats where au_cod_ac='POCS' and year(au_dat_act) in ('2015') and month(au_dat_act) in (6,7,8,9)".format(dataAny)
for id,valor,anys,mes,dia,up in getAll(sql,imp):
    POCS[dia] += 1
    POCSUP[up,mes] += 1
    try:
        territori = territoris[up]['ambit']
    except:
        territori = 'SENSE TERRITORI'
    POCSterrit[anys,dia,territori] += 1
    if anys == 2015 and mes == 6:
        if id in visites80:
            visites80[id]['num'] = 1
        else:
            no80 += 1
                
a=0
b=0
for (id),d in visites80.items():
    num = d['num']
    den = d['den']
    if num == 1:
        a += 1
    if den == 1:
        b += 1

print a, b, no80
    
'''
try:
    remove(OutFile)
except:
    pass     
    
with openCSV(OutFile) as c:
    for (dia),d in POCS.items():
        c.writerow([dia,d])

try:
    remove(OutFile2)
except:
    pass     
    
with openCSV(OutFile2) as c:
    for (anys,dia,territori),d in POCSterrit.items():
        c.writerow([anys,dia,territori,d])
'''
try:
    remove(OutFileUP)
except:
    pass     
    
with openCSV(OutFileUP) as c:
    for (up,mes),d in POCSUP.items():
        c.writerow([up,mes,d])

printTime('Fi procés')
        