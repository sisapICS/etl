
import sisapUtils as u
import collections as c

def nom_indicadors(modul, visites):
    if modul == 'URCEN' and visites == '':
        return 'TOTAL_URCEN'
    elif modul == 'URDOM' and visites == '':
        return 'TOTAL_URDOM'
    elif modul == 'URCEN' and visites == '9C':
        return 'URCEN_9C'
    elif modul == 'URDOM' and visites == '9C':
        return 'URDOM_9C'
    elif modul == 'URCEN' and visites == '9D':
        return 'URCEN_9D'
    elif modul == 'URDOM' and visites == '9D':
        return 'URDOM_9D'
    elif modul == 'URCEN' and visites == '9T':
        return 'URCEN_9T'
    elif modul == 'URDOM' and visites == '9T':
        return 'URDOM_9T'


class VisitesUrgents():
    def __init__(self):
        self.get_data()
        self.visites_totals()
        self.visites_tipus_v()
        self.uploading()
    
    def get_data(self):
        self.visites_modul = c.defaultdict(c.Counter)
        self.visites_tipus =  c.defaultdict(c.Counter)
        self.totals = c.Counter()
        sql = """select visi_centre_codi_centre, visi_centre_classe_centre, 
                    visi_servei_codi_servei, visi_modul_codi_modul, visi_tipus_visita
                    from acut.pac_visites, nodrizas.dextraccio
                    where visi_servei_codi_servei = 'URGEN' 
                    and visi_modul_codi_modul in ('URCEN', 'URDOM')
                    and visi_data_visita between date_add(data_ext,interval - 1 month) and data_ext"""
        for centre, classe, servei, modul, tipus_v in u.getAll(sql, 'acut'):
            self.visites_modul[(centre, classe, servei)][modul] += 1
            self.visites_tipus[(centre, classe, servei, modul)][tipus_v] += 1
            self.totals[(centre, classe, servei)] += 1
    
    def visites_totals(self):
        self.upload = []
        for (centre, classe, servei), modul_d in self.visites_modul.items():
            for modul, val in modul_d.items():
                indi = nom_indicadors(modul, '')
                self.upload.append((indi, centre, classe, servei, modul, '', val, 1, 'N', val/1))
                den = self.totals[(centre, classe, servei)]
                self.upload.append((indi, centre, classe, servei, modul, '', val, den, 'PCT', val/float(den)))
        for (centre, classe, servei), val in self.totals.items():
            self.upload.append(('TOTALS', centre, classe, servei, '', '', val, 1, 'N', val/1))
    
    def visites_tipus_v(self):
        homol = {"URCEN": ("9C", "9T", "9D"), "URDOM": ("9D", "9T")}
        for (centre, classe, servei, modul), tipus_v_d in self.visites_tipus.items():
            if modul in homol:
                den = sum([v for k, v in tipus_v_d.items() if k in homol[modul]])
                for tipus_v in homol[modul]:
                    val = tipus_v_d.get(tipus_v, 0)
                    indi = nom_indicadors(modul, tipus_v)
                    self.upload.append((indi, centre, classe, servei, modul, tipus_v, val, 1, 'N', val/1))
                    self.upload.append((indi, centre, classe, servei, modul, tipus_v, val, den, 'PCT', val/float(den) if den else 0))

    def uploading(self):
        cols = """(indicador varchar(20), centre varchar(17), classe varchar(2), servei varchar(7), 
                    modul varchar(7), tipus_v varchar(7), num int, 
                    den int, tipus varchar(3), resultat float)"""
        u.createTable('indicadors_urgents', cols, 'acut', rm=True)
        u.listToTable(self.upload, 'indicadors_urgents', 'acut')


if __name__ == "__main__":
    VisitesUrgents()