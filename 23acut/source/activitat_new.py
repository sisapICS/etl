
import sisapUtils as u
import collections as c
import sisaptools as t
import datetime as d

DEXT = t.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]
# DEXT = d.date(2023, 9, 30)
year = DEXT.year
DEXTD=str(DEXT)
# PERIOD = "A{}".format(DEXT.strftime("%y%m"))

def nom_indicadors(modul, tipus, etiqueta):
    if modul == 'URCEN':
        if tipus == '9C': return 'VISURCEN9C'
        elif tipus == '9D': return 'VISURCEN9D'
        elif tipus == '9T': return 'VISURCEN9T'
        else: return 'VISURCENALTRES'
    elif modul == 'URDOM':
        if tipus == '9D': return 'VISURDOM9D'
        elif tipus == '9T': return 'VISURDOM9T'
        else: return 'VISURDOMALTRES'
    elif modul == 'PRDOM':
        if tipus == '9D': 
            if etiqueta == 'CURA': return 'VISPRODOM9DCURA'
            elif etiqueta == 'TTO': return 'VISPRODOM9DTTO'
            else: return 'VISPRODOM9DALTRES'
    elif modul == 'PRCEN':
        if tipus == '9C': 
            if etiqueta == 'CURA': return 'VISPROCEN9CCURA'
            elif etiqueta == 'TTO': return 'VISPROCEN9CTTO'
            else: return 'VISPROCEN9CALTRES'
    else:
        return 'VISNOHOMOLOG'


class VisitesUrgents():
    def __init__(self):
        u.printTime('inici')
        self.get_pob()
        u.printTime('pob')
        self.get_data()
        u.printTime('data')
        self.visites_totals()
        u.printTime('visites')
        self.uploading()
        u.printTime('upload')
        self.to_khalix()
        u.printTime('fi')
    
    def get_pob(self):
        self.pob = {}
        sql = """select id_cip_sec, TIMESTAMPDIFF(YEAR, usua_data_naixement, CURDATE()) from import.assignada"""
        for id, edat in u.getAll(sql, 'import'):
            age = u.ageConverter(int(edat), 5)
            self.pob[id] = age
    
    def get_data(self):
        self.visites_modul = c.defaultdict(c.Counter)
        self.visites_tipus =  c.defaultdict(c.Counter)
        self.totals = c.Counter()
        sql = """select id_cip_sec, FLOOR(visi_hora_visita/3600) hora, visi_data_visita,
                    visi_centre_codi_centre, visi_centre_classe_centre, 
                    visi_modul_codi_modul, visi_tipus_visita,
                    visi_etiqueta, visi_servei_codi_servei
                    from acut.pac_visites_no_estricte
                    where visi_data_visita between DATE '{year}-01-01' and DATE '{DEXTD}'""".format(year=year, DEXTD=DEXTD)
        for id_cip_sec, hora, dia, centre, classe, modul, tipus_v, etiqueta, servei in u.getAll(sql, 'acut'):
            if id_cip_sec in self.pob:
                edat = self.pob[id_cip_sec]
                festiu = u.isWorkingDay(dia)
                period = "A{}".format(dia.strftime("%y%m"))
                if servei == 'URGEN':
                    if modul == 'URCEN':
                        if tipus_v in ('9C', '9D', '9T'):
                            self.visites_tipus[(hora, period, festiu, centre, classe, modul, tipus_v, edat)][''] += 1
                        else:
                            self.visites_tipus[(hora, period, festiu, centre, classe, modul, '', edat)][''] += 1
                    elif modul == 'URDOM':
                        if tipus_v in ('9D', '9T'):
                            self.visites_tipus[(hora, period, festiu, centre, classe, modul, tipus_v, edat)][''] += 1
                        else:
                            self.visites_tipus[(hora, period, festiu, centre, classe, modul, '', edat)][''] += 1
                elif servei == 'PROGR':
                    if etiqueta in ('CURA', 'TTO'):
                        self.visites_tipus[(hora, period, festiu, centre, classe, modul, tipus_v, edat)][etiqueta] += 1
                    else:
                        self.visites_tipus[(hora, period, festiu, centre, classe, modul, tipus_v, edat)]['ALTRES'] += 1
                else:
                    # VISNOHOMOLOG
                    self.visites_tipus[(hora, period, festiu, centre, classe, modul, tipus_v, edat)][''] += 1

    
    def visites_totals(self):
        self.upload = []
        for (hora, period, festiu, centre, classe, modul, tipus_v, edat), etiquetes_N in self.visites_tipus.items():
            if modul in ('URCEN', 'URDOM'):
                indi = nom_indicadors(modul, tipus_v, '')
                if indi is not None:
                    N = self.visites_tipus[(hora, period, festiu, centre, classe, modul, tipus_v, edat)]['']
                    self.upload.append((indi, period, centre, classe, edat, festiu, hora, 'NOCLI', N))
            else:    
                for etiqueta, N in etiquetes_N.items():
                    indi = nom_indicadors(modul, tipus_v, etiqueta)
                    if indi is not None:
                        self.upload.append((indi, period, centre, classe, edat, festiu, hora, 'NOCLI', N))

    def uploading(self):
        cols = """(indicador varchar(20), period varchar(5), centre varchar(17), classe varchar(2), edat varchar(7), 
                    laborable varchar(7), hora varchar(7), tipus varchar(10), num int)"""
        u.createTable('indicadors_visites', cols, 'acut', rm=True)
        u.listToTable(self.upload, 'indicadors_visites', 'acut')
    
    def to_khalix(self):
        sql = """select indicador, period, concat(concat(centre, '_'), classe), tipus, edat, 
                case when laborable = 'True' then 'DIALAB' else 'DIAFES' end lab_fest,
                case when length(hora) = 2 then CONCAT('HORA', HORA) else CONCAT(CONCAT('HORA', '0'),HORA) end HORA,
                'N', sum(num)
                from acut.indicadors_visites v inner join acut.centres_acut c on v.centre=c.codi_centre and v.classe=c.classe_centre
                where indicador != ''
                group by indicador, period, concat(concat(centre, '_'), classe), tipus, edat, lab_fest, HORA"""
        u.exportKhalix(sql, 'VISACUTTOT', force=True)


if __name__ == "__main__":
    VisitesUrgents()