# coding: latin1

"""
.
"""

import sisapUtils as u


TABLE = "exp_khalix"
DATABASE = "acut"
FILE = "ACUT_NEW"

CENTRES = {(codi, centre):(codi + '_' + centre) for (codi, centre)
           in u.getAll("select codi_centre, classe_centre from acut.centres_acut", "acut")}  # noqa
VISITES = "pac_visites"


def worker(params):
    """."""
    return([row for row in u.getAll(*params)])
