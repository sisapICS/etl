# coding: latin1

"""
.
"""

import shared as s
import sisapUtils as u


DEFAULT = [("URGEN", "URCEN"), ("URGEN", "URDOM"), ("PROGR", "PRCEN"), ("PROGR", "PRDOM")]


class Visites(object):
    """."""

    def __init__(self):
        """."""
        self.create_table()
        self.get_cataleg()
        self.get_visites()

    def create_table(self):
        """."""
        cols = "(visi_centre_codi_centre varchar(17), \
                visi_centre_classe_centre varchar(17), visi_servei_codi_servei varchar(17), \
                visi_modul_codi_modul varchar(17),\
                visi_id varchar(17), visi_up varchar(5), \
                visi_data_visita date, visi_hora_visita int, \
                visi_tipus_visita varchar(4), visi_etiqueta varchar(4), id_cip_sec int)"
        u.createTable(s.VISITES, cols, s.DATABASE, rm=True)
        cols = "(visi_centre_codi_centre varchar(17), \
                visi_centre_classe_centre varchar(17), visi_servei_codi_servei varchar(17), \
                visi_modul_codi_modul varchar(17),\
                visi_id varchar(17), visi_up varchar(5), \
                visi_data_visita date, visi_hora_visita int, \
                visi_tipus_visita varchar(4), visi_etiqueta varchar(4), id_cip_sec int)"
        u.createTable(s.VISITES + '_no_estricte', cols, s.DATABASE, rm=True)

    def get_cataleg(self):
        """."""
        sql = "select cuag_sector, cuag_codi_centre, cuag_classe_centre, \
                      cuag_codi_servei, cuag_codi_modul \
               from cat_cmbdtb002"
        self.cataleg = set([row for row in u.getAll(sql, "import")
                            if row[3:5] not in DEFAULT])

    def get_visites(self):
        """."""
        sql = "show create table visites1"
        tables = u.getOne(sql, "import")[1].split("UNION=(")[1][:-1].split(",")
        centres = []
        for centre in s.CENTRES.keys():
            centres.append(centre[0])
        centres = tuple(centres)
        sql = "select codi_sector, visi_centre_codi_centre, \
                      visi_centre_classe_centre, visi_servei_codi_servei, \
                      visi_modul_codi_modul, visi_id, visi_up, \
                      visi_data_visita, visi_hora_visita, visi_tipus_visita, \
                      visi_etiqueta, id_cip_sec \
               from {{}}, nodrizas.dextraccio \
               where visi_centre_codi_centre in {} and \
                     visi_data_visita between \
                                adddate(data_ext, interval -1 year) and \
                                data_ext and \
                     visi_situacio_visita = 'R'".format(centres)
        jobs = [(sql.format(table), "import") for table in tables]
        for visites in u.multiprocess(s.worker, jobs):
            upload = [row[1:] for row in visites
                      if row[3:5] in DEFAULT or row[:5] in self.cataleg]
            upload1 = [row[1:] for row in visites]
            u.listToTable(upload, s.VISITES, s.DATABASE)
            u.listToTable(upload1, s.VISITES + '_no_estricte', s.DATABASE)


if __name__ == "__main__":
    Visites()
