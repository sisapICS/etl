# coding: latin1

"""
.
"""

import collections as c

import shared as s
import sisapUtils as u


ACCOUNT = "ACUT001"


class Us(object):
    """."""

    def __init__(self):
        """."""
        self.get_informes()
        self.get_visites()
        self.export_data()

    def get_informes(self):
        """."""
        sql = "select iau_id_visita from urg_informes, nodrizas.dextraccio \
               where iau_data_imp between adddate(data_ext, interval -1 year) and data_ext"  # noqa
        self.informes = set([id for id, in u.getAll(sql, "import")])

    def get_visites(self):
        """."""
        self.resultat = c.defaultdict(c.Counter)
        sql = "select visi_id, visi_centre_codi_centre, visi_centre_classe_centre from {}"
        for id, centre, classe in u.getAll(sql.format(s.VISITES), s.DATABASE):
            if (centre, classe) in s.CENTRES:
                self.resultat[(centre, classe)]["DEN"] += 1
                if id in self.informes:
                    self.resultat[(centre, classe)]["NUM"] += 1

    def export_data(self):
        """."""
        export = []
        period = "A{}{}".format(*u.getKhalixDates()[1:])
        for entity, dades in self.resultat.items():
            for analysis, value in dades.items():
                this = (ACCOUNT, period, s.CENTRES[entity], analysis,
                        "NOCAT", "NOIMP", "DIM6SET", "N", value)
                export.append(this)
        u.listToTable(export, s.TABLE, s.DATABASE)


if __name__ == "__main__":
    Us()
