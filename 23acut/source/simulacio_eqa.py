import sisapUtils as u
import collections as c
from datetime import datetime, timedelta

CODIS = {
    'C01-N30': 'ITU',
    'C01-N30.0': 'ITU',
    'C01-N30.00': 'ITU',
    'C01-N30.01': 'ITU',
    'C01-N30.90': 'ITU',
    'C01-N39.0': 'ITU',
    'C01-N39.9': 'ITU',
    'IRE06777': 'ITU',
    'N30': 'ITU',
    'N30.0': 'ITU',
    'N39.0': 'ITU',
    'C01-N30.91': 'ITU',
    'C01-N30.8': 'ITU',
    'C01-N30.81': 'ITU',
    'C01-N30.2': 'ITU',
    'C01-N30.21': 'ITU',
    'C01-N20.0': 'COLIC_NEFRITIC',
    'C01-N20.1': 'COLIC_NEFRITIC',
    'C01-N20.2': 'COLIC_NEFRITIC',
    'C01-N20.9': 'COLIC_NEFRITIC',
    'C01-N21.0': 'COLIC_NEFRITIC',
    'C01-N21.1': 'COLIC_NEFRITIC',
    'C01-N21.8': 'COLIC_NEFRITIC',
    'C01-N21.9': 'COLIC_NEFRITIC',
    'C01-N23': 'COLIC_NEFRITIC',
    'N20': 'COLIC_NEFRITIC',
    'N20.0': 'COLIC_NEFRITIC',
    'N20.1': 'COLIC_NEFRITIC',
    'N20.2': 'COLIC_NEFRITIC',
    'N20.9': 'COLIC_NEFRITIC',
    'N21': 'COLIC_NEFRITIC',
    'N21.0': 'COLIC_NEFRITIC',
    'N21.1': 'COLIC_NEFRITIC',
    'N21.8': 'COLIC_NEFRITIC',
    'N21.9': 'COLIC_NEFRITIC',
    'N23': 'COLIC_NEFRITIC'
}


TRACTAMENTS = {
        # 'A03BA01': ('COLIC', 'OK'),
        # 'A03BB01': ('COLIC', 'OK'),
        # 'A03CB': ('COLIC', 'OK'),
        # 'A03DB04': ('COLIC', 'OK'),
        # 'A03DB91': ('COLIC', 'OK'),
        # 'A03DB92': ('COLIC', 'OK'),
        # 'A03ED': ('COLIC', 'OK'),
        # 'A03ED91': ('COLIC', 'OK'),
        # 'A03ED93': ('COLIC', 'OK'),
        # 'A03FA01': ('COLIC', 'OK'),
        # 'A03FA02': ('COLIC', 'OK'),
        # 'A03FA03': ('COLIC', 'OK'),
        # 'A03FA06': ('COLIC', 'OK'),
        # 'A03FA08': ('COLIC', 'OK'),
        # 'A03FA51': ('COLIC', 'OK'),
        # 'A03FA56': ('COLIC', 'OK'),
        # 'A03FA91': ('COLIC', 'OK'),
        'A03BB01': ('COLIC', 'KO'),
        'A03DB04': ('COLIC', 'KO'),
        'J01XX01': ('ITU', 'OK'),
        'J01XE01': ('ITU', 'OK')
}


class EQA():
    def __init__(self):
        cols = '(indicador varchar(10), up varchar(15), analisi varchar(13), n int)'
        u.createTable('simulacio_eqas', cols, 'acut', rm=True)
        self.get_atesa()
        print('ok')
        self.get_problemes()
        self.get_tractaments()
        print('anem x indicadors')
        self.get_indicadors()
    
    def get_atesa(self):
        hash_2_cip = {}
        sql = """SELECT hash, cip FROM dwsisap.dbc_poblacio
                WHERE edat BETWEEN 14 AND 90"""
        for hash, cip in u.getAll(sql, 'exadata'):
            hash_2_cip[hash] = cip
        self.atesa = set()
        sql = """SELECT pacient FROM dwsisap.sisap_master_visites
                    WHERE DATA BETWEEN DATE '2024-10-01'- 365 AND DATE '2024-10-01'
                    AND up IN (SELECT up_cod FROM dwsisap.DBC_RUP WHERE subtip_cod = '24')"""
        for pacient, in u.getAll(sql, 'exadata'):
            if pacient in hash_2_cip:
                self.atesa.add(hash_2_cip[pacient])
    
    def get_problemes(self):
        self.problemes = c.defaultdict(set)
        sql = """SELECT id, codi, DATA, up FROM dwtw.PROBLEMES p 
                    WHERE DATA BETWEEN DATE '2024-10-01'- 30 AND DATE '2024-10-01'
                    AND up IN (SELECT up_cod FROM dwsisap.DBC_RUP WHERE subtip_cod = '24')"""
        for cip, codi, data, up in u.getAll(sql, 'exadata'):
            if codi in CODIS:
                ps = CODIS[codi]
                self.problemes[ps].add((cip, data, up))
    
    def get_tractaments(self):
        self.tractaments = c.defaultdict(lambda: c.defaultdict(set))
        sql = """SELECT PMC_USUARI_CIP, ATCCODI, DATA_INICI, pmc_amb_cod_up 
                FROM dwtw.PRESCRIPCIONS
                WHERE DATA BETWEEN DATE '2024-10-01'- 30 AND DATE '2024-10-01'
                AND data_inici BETWEEN DATE '2024-10-01'- 30 AND DATE '2024-10-01'
                AND pmc_amb_cod_up IN (SELECT up_cod FROM dwsisap.DBC_RUP WHERE subtip_cod = '24')"""
        for cip, codi, data, up in u.getAll(sql, 'exadata'):
            if codi in TRACTAMENTS:
                trac = TRACTAMENTS[codi]
                self.tractaments[trac][cip].add((data, up))
    
    def get_indicadors(self):
        self.upload = c.defaultdict(c.Counter)
        for ps in self.problemes:
            if ps == 'ITU':
                for (cip, data, up) in self.problemes[ps]:
                    if cip in self.atesa:
                        self.upload[('EQA0230', up)]['DEN'] += 1
                        if cip in self.tractaments[('ITU', 'OK')]:
                            for data_t, up_t in self.tractaments[('ITU', 'OK')][cip]:
                                time_difference = abs(data_t - data)
                                if time_difference <= timedelta(days=1) and up_t == up:
                                    self.upload[('EQA0230', up)]['NUM'] += 1
            elif ps == 'COLIC_NEFRITIC':
                for (cip, data, up) in self.problemes[ps]:
                    if cip in self.atesa:
                        self.upload[('EQA0223', up)]['DEN'] += 1
                        num = 1
                        if cip in self.tractaments[('COLIC', 'KO')]:
                            for data_t_ko, up_t_ko in self.tractaments[('COLIC', 'KO')][cip]:
                                time_difference = abs(data_t_ko - data)
                                if time_difference <= timedelta(days=1) and up_t_ko == up:
                                    num = 0
                        self.upload[('EQA0223', up)]['NUM'] += num
    
        upload = []
        for indicador, up in self.upload:
            for analisi, n in self.upload[(indicador, up)].items():
                upload.append((indicador, up, analisi, n))
        u.listToTable(upload, 'simulacio_eqas', 'acut')
    

if __name__ == "__main__":
    EQA()



