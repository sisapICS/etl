# coding: latin1

import sisapUtils as u
import collections as c

cols = """(scs_cod varchar(10), up_des varchar(100), up_cod varchar(100), 
        dap varchar(100), dap_desc varchar(100), ambit varchar(100), ambit_desc varchar(100), sap varchar(100),
        es_ics varchar(100), tip_cod varchar(100), tip_des varchar(100), 
        abs_cod varchar(100), abs_des varchar(100), sector_cod varchar(100), sector_des varchar(100), aga_cod varchar(100), 
        aga_des varchar(100), regio_cod varchar(100),
        regio_des varchar(100), ep_cod varchar(100), ep_des varchar(100), tit_cod varchar(100), tit_des varchar(100),
        codi_centre varchar(100), classe_centre varchar(100), nom_centre varchar(100), sector_ecap varchar(10))"""
u.createTable('centres_acut', cols, 'permanent', rm=True)
u.createTable('centres_acut', cols, 'acut', rm=True)

sector_ref = {}
sql = """SELECT CENTRE_CODI, CENTRE_Classe, sector, count(1) 
        FROM dwsisap.SISAP_MASTER_VISITES 
        WHERE DATA > date '2023-01-01'
        GROUP BY CENTRE_CODI, CENTRE_Classe, sector"""
for centre, classe, sector, n in u.getAll(sql, 'exadata'):
    if (centre, classe) in sector_ref:
        if sector_ref[(centre, classe)][1] < n:
            sector_ref[(centre, classe)] = [sector, n]
    else:
        sector_ref[(centre, classe)] = [sector, n]

centre_classe = c.defaultdict(set)
sql = """select codi_sector, cent_codi_centre, cent_classe_centre, cent_codi_up, cent_nom_centre from import.cat_pritb010
        where cent_data_baixa > sysdate()"""
for sector, codi_centre, classe_centre, up, nom in u.getAll(sql, 'import'):
    centre_classe[up].add((codi_centre, classe_centre, nom, sector))

info_catsalut = c.defaultdict(set)
sql = """SELECT up_cod, up_des, es_ics, tip_cod, tip_des, 
        abs_cod, abs_des, sector_cod ,sector_des, aga_cod, aga_des, regio_cod,
        regio_des, ep_cod, ep_des, tit_cod, tit_des FROM dwsisap.DBC_RUP WHERE subtip_cod = '24'"""
for row in u.getAll(sql, 'exadata'):
    if row[0] in centre_classe:
        up = row[0]
        for row1 in centre_classe[row[0]]:
            print(row1[0], row1[1])
            if (row1[0], row1[1]) in sector_ref:
                print('sectors')
                print(row1[0], row1[1])
                print(row1[3])
                print(sector_ref[(row1[0], row1[1])][0])
                if sector_ref[(row1[0], row1[1])][0] == row1[3]:
                    print('yassss')
                    row2 = row[1:] + row1
                    info_catsalut[up].add(row2)

agregacions = {}
sql = """SELECT a.up_codi_up_ics, b.up_codi_up_scs,
                c.dap_codi_dap, c.dap_desc_dap, c.amb_codi_amb, d.amb_desc_amb,
                d.sap_codi_sap
                FROM gcctb007 a
                INNER JOIN gcctb008 b ON a.UP_CODI_UP_ICS = b.up_codi_up_ics
                INNER JOIN gcctb006 c ON a.DAP_CODI_DAP = c.dap_codi_dap
                INNER JOIN gcctb005 d ON c.AMB_CODI_AMB = d.AMB_CODI_AMB 
                WHERE a.UP_DATA_BAIXA IS NULL AND c.DAP_DATA_BAIXA IS NULL"""
for up_ics, up_scs, dap, dap_desc, amb, amb_desc, sap_codi in u.getAll(sql, 'redics'):
    agregacions[up_scs] = [up_scs, up_ics, dap, dap_desc, amb, amb_desc, sap_codi]

upload = []
for up in info_catsalut:
    if up in agregacions:
        for info in info_catsalut[up]:
            upl = list([agregacions[up][0]]) + list([info[0]]) + list(agregacions[up][1:]) + list(info[1:])
            upload.append(upl)

u.listToTable(upload, 'centres_acut', 'permanent')
u.listToTable(upload, 'centres_acut', 'acut')

# Exportem jerarquia a Khalix
if u.IS_MENSUAL:
    sql = """select dap, dap_desc, ambit, ambit_desc, up_cod, up_des, concat(concat(codi_centre, '_'), classe_centre), nom_centre from acut.centres_acut"""
    u.exportKhalix(sql, 'CAT_ACUT')