# coding: latin1

import datetime as d
import json
import socket
import sys

import sisapUtils as u


def send_petition(d):
    d = [d, "fi_de_la_transmissio"]
    HOST = "10.80.217.84"
    PORT = 53007
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    data_json = json.dumps(d)
    s.sendall(data_json)

    received = ""
    while True:
        packet = s.recv(4096)
        if not packet:
            break
        received += packet
        if '"fi_de_la_transmissio"]' in received:
            break

    received_json = json.loads(received)
    s.close()
    return received_json


resul = send_petition({'fx': 'create'})[0]
if resul == "fet":
    dat, = u.getOne("select date_format(data_ext, '%d %b') from dextraccio", "nodrizas")
    u.sendSISAP(['mfabregase@gencat.cat', 'eballo.girona.ics@gencat.cat',
                 'mbenitez.bcn.ics@gencat.cat', 'cguiriguet.bnm.ics@gencat.cat',
                 'mbustos.bnm.ics@gencat.cat', 'sflayeh.bnm.ics@gencat.cat',
                 'mquintana@ambitcp.catsalut.net','framospe@gencat.cat',
                 'arene.girona.ics@gencat.cat'],
                'Actualitzaciˇ SISAP-REV',
                'all',
                "S\'ha actualitzat SISAP-REV amb dades corresponents a {0}.".format(dat))
else:
    sys.exit(resul)

"""
print send_petition({'fx': 'patient', 'key': '700E6ECCF399128D364DC5FA41AE560552080237:6519'})
print send_petition({'fx': 'patient', 'key': '700E6ECCF399128D364DC5FA41AE560552080237:6519/'})
print send_petition({'fx': 'agrupador', 'key': '16'})
print send_petition({'fx': 'agrupador', 'key': '16/'})
print send_petition({'fx': 'agrupador', 'key': 'EQA0203A'})
print send_petition({'fx': 'agrupador', 'key': 'EQA0203A/'})
print send_petition({'fx': 'agrupador', 'key': 'EQA0202A'})
print send_petition({'fx': 'agrupador', 'key': 'EQA0202A/'})
"""
