#  coding: latin1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *

nod = "nodrizas"
imp = "import"
db = 'depart'

dext, = getOne('select data_ext from dextraccio', nod)

codis_vacunes = {'T': 49,
                 'VNC': 698,
                 'VNC23': 48,
                 'GRIP': 99,
                 'VHZ': 920,
                 'VNC13': 977
                }
                  
criteris_indicadors = { 
    'VAC071': {'desc': 'VNC23 1 dosi en >= 65 anys', 'vacuna': 'VNC23', 'dosis': 1},
    'VAC072': {'desc': 'VNC13 1 dosi en >= 65 anys', 'vacuna': 'VNC', 'dosis': 1},
    'VAC073': {'desc': 'T�tanus 1 dosi en >= 65 anys', 'vacuna': 'T', 'dosis': 1},
    'VAC076': {'desc': 'Grip en >= 65 anys', 'vacuna': 'GRIP', 'dosis': 1},
    'VAC161': {'desc': 'Vacunaci� antigripal en ≥ 70 anys', 'vacuna': 'GRIP', 'dosis': 1},
}

criteris_indicadors_Xedat = {
    (60,64): {
        'VAC173': {'desc': 'Grip 60-64 anys', 'vacuna': 'GRIP', 'dosis': 1}
        },  
    (65,70): { 
        'VAC121': {'desc': 'VNC23 1 dosi en 65 - 70 anys', 'vacuna': 'VNC23', 'dosis': 1},
        'VAC122': {'desc': 'VNC13 1 dosi en 65 - 70 anys', 'vacuna': 'VNC', 'dosis': 1},
        'VAC123': {'desc': 'T�tanus 1 dosi en 65 - 70 anys', 'vacuna': 'T', 'dosis': 1},
        },
    (65,74): {
        'VAC174': {'desc': 'Grip 65-74 anys', 'vacuna': 'GRIP', 'dosis': 1}
        },  
    (65,200): {
        'VAC183': {'desc': 'VHZ una dosi en majors de 64 anys', 'vacuna': 'VHZ', 'dosis': 1},
        'VAC184': {'desc': 'VHZ dues dosis en majors de 64 anys', 'vacuna': 'VHZ', 'dosis': 2}
        },  
    (75,200): {
        'VAC175': {'desc': 'Grip 75 anys o m�s', 'vacuna': 'GRIP', 'dosis': 1}
        },  
    65: { 
        'VAC151': {'desc': 'VHZ una dosi als 65 anys', 'vacuna': 'VHZ', 'dosis': 1},
        'VAC152': {'desc': 'VHZ dues dosis als 65 anys', 'vacuna': 'VHZ', 'dosis': 2},
        'VAC177': {'desc': 'VNC23 1 dosi despr�s dels 60 als 65 anys', 'vacuna': 'VNC23', 'dosis': 1, 'edatd': 60, 'tip': 'despres'},
        'VAC179': {'desc': 'VNC13 1 dosi despr�s dels 60 als 65 anys', 'vacuna': 'VNC13', 'dosis': 1, 'edatd': 60, 'tip': 'despres'},
        'VAC181': {'desc': 'Td 1 dosi a partir dels 60 als 65 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 60, 'tip': 'despres'},
        },
    66: { 
        'VAC153': {'desc': 'VHZ dues dosis als 66 anys', 'vacuna': 'VHZ', 'dosis': 2},
        'VAC178': {'desc': 'VNC23 1 dosi despr�s dels 60 als 66 anys', 'vacuna': 'VNC23', 'dosis': 1, 'edatd': 60, 'tip': 'despres'},
        'VAC180': {'desc': 'VNC13 1 dosi despr�s dels 60 als 66 anys', 'vacuna': 'VNC13', 'dosis': 1, 'edatd': 60, 'tip': 'despres'},
        'VAC182': {'desc': 'Td 1 dosi a partir dels 60 als 66 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 60, 'tip': 'despres'},
        },
    80: { 
        'VAC154': {'desc': 'VHZ una dosi als 80 anys', 'vacuna': 'VHZ', 'dosis': 1},
        'VAC155': {'desc': 'VHZ dues dosis als 80 anys', 'vacuna': 'VHZ', 'dosis': 2}
        },
    81: { 
        'VAC156': {'desc': 'VHZ dues dosis als 81 anys', 'vacuna': 'VHZ', 'dosis': 2}
        },
}

criteris_indicadors_institucionalitzada = {
    'VAC162': {'desc': 'Vacunaci� antigripal en poblaci� institucionalitzada', 'vacuna': 'GRIP', 'dosis': 1},
    'VAC163': {'desc': 'T�tanus 1 dosi a partir dels 40 anys en poblaci� institucionalitzada', 'vacuna': 'T', 'dosis': 1},
}

# Obtenir els codis de antigens asociats als agrupadors detallats a antigens_agrupadors
antigens, vacunes_relacionades = defaultdict(list), {}
for antigen_desc in codis_vacunes:
    agrupador = codis_vacunes[antigen_desc]
    sql = """
            SELECT DISTINCT 
                criteri_codi
            FROM
                eqa_criteris
            WHERE
                taula IN ('vacunesped', 'vacunes')
                AND agrupador = {}
          """.format(agrupador)
    for codi_antigen, in getAll(sql, nod):
        antigens[antigen_desc].append(codi_antigen)
        antigens[antigen_desc].append('cutre')

# Obtenir els codis de vacunaci� asociats als codis d'antigen ontinguts al pas anterior
    in_crit = tuple(antigens[antigen_desc]) 
    sql = """
            SELECT
                vacuna
            FROM
                cat_prstb040_new
            WHERE
                antigen IN {0}
          """.format(in_crit)
    for codi_vacuna, in getAll(sql, imp):
        vacunes_relacionades[codi_vacuna] = antigen_desc

# Obtenir dades i dates de vacunaci� d'import.vacunes
dates_vacunes =  defaultdict(list)

for particio in getSubTables("vacunes"):
    sql = """
            SELECT
                id_cip_sec,
                va_u_cod,
                va_u_data_vac
            FROM
                {0}
            WHERE
                va_u_data_baixa = 0
          """.format(particio)
    for id_cip_sec, codi_vacuna, data_vacuna in getAll(sql, imp):
        if codi_vacuna in vacunes_relacionades:
            antigen_agrupador = vacunes_relacionades[codi_vacuna]
            dates_vacunes[(id_cip_sec, antigen_agrupador)].append(data_vacuna)

# C�lcul indicadors

def get_recompte(criteris_indicadors_Xedat, edats_indicador, edat, recompte, up, sexe, id_cip_sec, dates_vacunes, dnaix, ates):
    if isinstance(edats_indicador, int):
        edat_indicador_menor = edats_indicador
        edat_indicador_major = edats_indicador
    elif isinstance(edats_indicador, tuple):
        edat_indicador_menor = edats_indicador[0]
        edat_indicador_major = edats_indicador[1]
    if edat_indicador_menor <= edat <= edat_indicador_major:
        for indicador in criteris_indicadors_Xedat[edats_indicador]:
            antigen_agrupador = criteris_indicadors_Xedat[edats_indicador][indicador]['vacuna']
            dosi = criteris_indicadors_Xedat[edats_indicador][indicador]['dosis']
            desc = criteris_indicadors_Xedat[edats_indicador][indicador]['desc']
            if not ('tip' in criteris_indicadors_Xedat[edats_indicador][indicador] and 'edatd' in criteris_indicadors_Xedat[edats_indicador][indicador]):
                if antigen_agrupador == 'GRIP':
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
                    if ates == 1:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT' , indicador, 'DEN'] += 1
                    if (id_cip_sec, antigen_agrupador) in dates_vacunes:
                        ok = 0
                        for data in dates_vacunes[(id_cip_sec, antigen_agrupador)]:
                            edat_y = yearsBetween(dnaix, data)
                            if edat_y > 64:
                                if dext.month in (9,10,11,12):
                                    if data.year == dext.year and data.month in (9,10,11,12):
                                        ok += 1
                                elif dext.month in (1,2,3,4,5,6,7,8):
                                    if (data.year == dext.year-1 and data.month in (9,10,11,12)) or (data.year == dext.year and data.month in (1,2)):
                                        ok += 1
                        if ok >= dosi:
                            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS' , indicador, 'NUM'] += 1
                            num = 1
                            if ates == 1:
                                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT' , indicador, 'NUM'] += 1
                else:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
                    if ates == 1:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT' , indicador, 'DEN'] += 1
                    if (id_cip_sec, antigen_agrupador) in dates_vacunes:
                        ok = 0
                        for data in dates_vacunes[(id_cip_sec, antigen_agrupador)]:
                            edat_y = yearsBetween(dnaix, data)
                            if edat_y > 64:
                                ok += 1
                        if ok >= dosi:
                            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS' , indicador, 'NUM'] += 1
                            num = 1
                            if ates == 1:
                                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT' , indicador, 'NUM'] += 1
            else:
                edatDosi = criteris_indicadors_Xedat[edats_indicador][indicador]['edatd']
                tip = criteris_indicadors_Xedat[edats_indicador][indicador]['tip']
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
                if ates == 1:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT', indicador, 'DEN'] += 1
                if (id_cip_sec, antigen_agrupador) in dates_vacunes:
                    ok = 0
                    for data in dates_vacunes[(id_cip_sec, antigen_agrupador)]:
                        edat_m = monthsBetween(dnaix, data)
                        if tip == 'abans':
                            if edat_m <= edatDosi:
                                ok += 1
                        else:
                            if edat_m >= edatDosi:
                                ok += 1
                    if ok >= dosi:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'NUM'] += 1
                        num = 1
                        if ates == 1:
                            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT', indicador, 'NUM'] += 1

recompte = Counter()
       
sql = """
        SELECT
            id_cip_sec,
            up,
            ates,
            edat,
            sexe,
            data_naix
        FROM
            assignada_tot_with_jail
        WHERE
            edat > 64
      """
for id_cip_sec, up, ates, edat, sexe, dnaix in getAll(sql, nod):
    for indicador in criteris_indicadors:
        edat = int(edat)
        den, num = 1, 0
        antigen_agrupador = criteris_indicadors[indicador]['vacuna']
        dosi = criteris_indicadors[indicador]['dosis']
        desc = criteris_indicadors[indicador]['desc']
        if antigen_agrupador == 'GRIP':
            if indicador not in ('VAC161', 'VAC162', 'VAC163'):
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
                if ates == 1:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT', indicador, 'DEN'] += 1
                if (id_cip_sec, antigen_agrupador) in dates_vacunes:
                    ok = 0
                    for data in dates_vacunes[(id_cip_sec, antigen_agrupador)]:
                        if dext.month in (9,10,11,12):
                            if data.year == dext.year and data.month in (9,10,11,12):
                                ok += 1
                        elif dext.month in (1,2,3,4,5,6,7,8):
                            if (data.year == dext.year-1 and data.month in (9,10,11,12)) or (data.year == dext.year and data.month in (1,2)):
                                ok += 1
                    if ok >= dosi:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'NUM'] += 1
                        num = 1
                        if ates == 1:
                            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT', indicador, 'NUM'] += 1
            
            elif indicador == 'VAC161' and edat >= 70:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
                if ates == 1:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT', indicador, 'DEN'] += 1
                if (id_cip_sec, antigen_agrupador) in dates_vacunes:
                    ok = 0
                    for data in dates_vacunes[(id_cip_sec, antigen_agrupador)]:
                        b = monthsBetween(data, dext)
                        if 0 <= b <= 6:
                            ok += 1
                    if ok >= dosi:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'NUM'] += 1
                        num = 1
                        if ates == 1:
                            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT', indicador, 'NUM'] += 1
        # Tot el que no �s grip
        else:
            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
            if ates == 1:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT', indicador, 'DEN'] += 1
            if (id_cip_sec, antigen_agrupador) in dates_vacunes:
                ok = 0
                for data in dates_vacunes[(id_cip_sec, antigen_agrupador)]:
                    edat_y = yearsBetween(dnaix, data)
                    if edat_y > 64:
                        ok += 1
                if ok >= dosi:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'NUM'] += 1
                    num = 1
                    if ates == 1:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT', indicador, 'NUM'] += 1

    for edats_indicador in criteris_indicadors_Xedat:
        get_recompte(criteris_indicadors_Xedat, edats_indicador, edat, recompte, up, sexe, id_cip_sec, dates_vacunes, dnaix, ates)
        

sql = """
        SELECT
            id_cip_sec,
            up,
            ates,
            edat,
            sexe,
            data_naix
        FROM
            assignada_tot
        WHERE
            institucionalitzat = 1
      """
for id_cip_sec, up, ates, edat, sexe, dnaix in getAll(sql, nod):
    for indicador in criteris_indicadors_institucionalitzada:
        edat = int(edat)
        den, num = 1, 0
        antigen_agrupador = criteris_indicadors_institucionalitzada[indicador]['vacuna']
        dosi = criteris_indicadors_institucionalitzada[indicador]['dosis']
        desc = criteris_indicadors_institucionalitzada[indicador]['desc']
        if antigen_agrupador == 'GRIP' and indicador == 'VAC162':
            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'INSASS', indicador, 'DEN'] += 1
            if ates == 1:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'INSAT' , indicador, 'DEN'] += 1
            if (id_cip_sec, antigen_agrupador) in dates_vacunes:
                ok = 0
                for data in dates_vacunes[(id_cip_sec, antigen_agrupador)]:
                    if dext.month in (9,10,11,12):
                        if data.year == dext.year and data.month in (9,10,11,12):
                            ok += 1
                    elif dext.month in (1,2,3,4,5,6,7,8):
                        if (data.year == dext.year-1 and data.month in (9,10,11,12)) or (data.year == dext.year and data.month in (1,2)):
                            ok += 1
                if ok >= dosi:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'INSASS' , indicador, 'NUM'] += 1
                    num = 1
                    if ates == 1:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'INSAT' , indicador, 'NUM'] += 1      
        # Tot el que no �s grip
        else:
            if edat >= 40:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'INSASS', indicador, 'DEN'] += 1
                if ates == 1:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'INSAT' , indicador, 'DEN'] += 1
                if (id_cip_sec, antigen_agrupador) in dates_vacunes:
                    ok = 0
                    for data in dates_vacunes[(id_cip_sec, antigen_agrupador)]:
                        edat_y = yearsBetween(dnaix, data)
                        if edat_y > 64:
                            ok += 1
                    if ok >= dosi:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'INSASS' , indicador, 'NUM'] += 1
                        num = 1
                        if ates == 1:
                            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'INSAT' , indicador, 'NUM'] += 1
                                                                
# Exportar dades en format taula
table = 'exp_dep_vacunes_adults'
create = "(up varchar(5) not null default'', edat varchar(10), sexe varchar(10), comb varchar(10), indicador varchar(10), tipus varchar(10), recompte int)"
createTable(table,create,db, rm=True)

upload = []
for (up, edat, sexe, comb, indicador, tip), rec in recompte.items():
    indicador = 'D' + indicador
    upload.append([up, edat, sexe, comb, indicador, tip , rec])
listToTable(upload, table, db) 