# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

db = 'depart'

imp = 'import'
nod = 'nodrizas'

indicador = 'PS0002'

def get_fumadorsFa1any():
    fumadors = {}
    sql = 'select id_cip_sec from eqa_tabac,dextraccio where tab=1 and (date_add(data_ext,interval - 12 month) between dalta and dbaixa)'
    for id, in getAll(sql, nod):
        fumadors[id] = True                    

    return fumadors   

def get_exfumadors():
    exfumadors = {}
    sql = 'select id_cip_sec, tab from eqa_tabac, dextraccio where data_ext between dalta and dbaixa'
    for id, tabac in getAll(sql, nod):
        if tabac == 2:
            exfumadors[id] = True
    return exfumadors
    
def do_things(fumadors, exfumadors):
    upload = []
    recompte = Counter()
    sql = "select id_cip_sec, edat, sexe, up, if(institucionalitzat=1,'INS','NOINS'), ates from assignada_tot where edat >14"
    for id, edat, sexe, up, insti, ates in getAll(sql, nod):
        if 15<= int(edat) <= 79:
            den,  num, excl = 0, 0, 0
            if id in fumadors:
                den = 1
                if id in exfumadors:
                    num = 1
                comb = insti + 'ASS'
                recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, 'DEN'] += den
                recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, 'NUM'] += num
                if int(ates) == 1:
                    comb = insti + 'AT'
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, 'DEN'] += den
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, 'NUM'] += num
    for (up, edat, sexe, comb, tip), rec in recompte.items():
         upload.append([up, edat, sexe, comb, indicador, tip , rec])
    listToTable(upload, TableT, db)

fumadors = get_fumadorsFa1any()
exfumadors = get_exfumadors()

TableT = 'exp_dep_cessacions'
create = "(up varchar(5) not null default'', edat varchar(10), sexe varchar(10), comb varchar(10), indicador varchar(10), tipus varchar(10), recompte int)"
createTable(TableT,create,db, rm=True)

do_things(fumadors, exfumadors)
