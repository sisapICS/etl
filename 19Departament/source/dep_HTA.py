# coding: iso-8859-1
import sisapUtils as u
from collections import Counter

db = 'depart'
nod = 'nodrizas'

indicador = 'PS0001'

table = 'exp_dep_TA'
create = """(
    up varchar(5) not null default'',
    edat varchar(10),
    sexe varchar(10),
    comb varchar(10),
    indicador varchar(10),
    tipus varchar(10),
    recompte int
    )
    """
u.createTable(table, create, db, rm=True)


def do_process():
    upload = []
    recompte = Counter()
    sql = """
        select
            id_cip_sec,
            edat,
            sexe,
            up,
            if(institucionalitzat = 1, 'INS', 'NOINS') as insti,
            ates,
            num
        from
            {}_ind2
        """.format(indicador)
    for id, edat, sexe, up, insti, ates, num in u.getAll(sql, 'eqa_ind'):
        edat = int(edat)
        comb = insti + 'ASS'
        recompte[up, u.ageConverter(edat, 5),
                 u.sexConverter(sexe), comb, 'DEN'] += 1
        recompte[up, u.ageConverter(edat, 5),
                 u.sexConverter(sexe), comb, 'NUM'] += num
        if int(ates) == 1:
            comb = insti + 'AT'
            recompte[up, u.ageConverter(edat, 5),
                     u.sexConverter(sexe), comb, 'DEN'] += 1
            recompte[up, u.ageConverter(edat, 5),
                     u.sexConverter(sexe), comb, 'NUM'] += num
    for (up, edat, sexe, comb, tip), rec in recompte.items():
        upload.append([up, edat, sexe, comb, indicador, tip, rec])
    u.listToTable(upload, table, db)


do_process()
