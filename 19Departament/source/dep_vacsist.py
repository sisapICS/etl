#  coding: latin1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *

nod = "nodrizas"
imp = "import"
db = 'depart'

calcular_grip = {
                  1: {'calcular': True, 'mesos': [11,12,1]},
                  2: {'calcular': True, 'mesos': [11,12,1,2]},
                  3: {'calcular': True, 'mesos': [11,12,1,2,3]},
                  4: {'calcular': True, 'mesos': [11,12,1,2,3,4]},
                  5: {'calcular': True, 'mesos': [11,12,1,2,3,4]},
                  6: {'calcular': False, 'mesos': []},
                  7: {'calcular': False, 'mesos': []},
                  8: {'calcular': False, 'mesos': []},
                  9: {'calcular': False, 'mesos': []},
                  10: {'calcular': False, 'mesos': []},
                  11: {'calcular': True, 'mesos': [11]},
                  12: {'calcular': True, 'mesos': [11, 12]},
                }
                
mes, dext = getOne('select month(data_ext), data_ext from dextraccio', nod)
calcular = calcular_grip[mes]['calcular']
mesos = calcular_grip[mes]['mesos']

antigens_agrupadors = {'HB': 15,
                       'HA': 34,
                       'XRP': 38,
                       'VNC23': 48,
                       'T': 49,
                       'Grip': 99,
                       'P': 311,
                       'Hib': 312,
                       'MCC': 313,
                       'VVZ': 314,
                       'MeB': 548,
                       'TF': 631,
                       'VNC': 698,
                       'VPH': 782,
                       'R': 790,
                       'MACYW': 883,
                      }   

criteris_indicadors = { 
        0:{
            'VAC005': {'desc': 'DTPa Primovacunaciķ 0 anys', 'vacuna': 'T', 'dosis': 2},
            'VAC009': {'desc': 'VPI Primovacunaciķ 0 anys', 'vacuna': 'P', 'dosis': 2}, 
            'VAC014': {'desc': 'Hib Primovacunaciķ 0 anys', 'vacuna': 'Hib', 'dosis': 2},
            'VAC018': {'desc': 'HB Primovacunaciķ 0 anys', 'vacuna': 'HB', 'dosis': 2},
            'VAC022': {'desc': 'MCC Primovacunaciķ 0 anys', 'vacuna': 'MCC', 'dosis': 1},
            'VAC024': {'desc': 'VNC Primovacunaciķ 0 anys', 'vacuna': 'VNC', 'dosis': 2},
            'VAC056': {'desc': 'Rotavirus 2 dosis 0 anys', 'vacuna': 'R', 'dosis': 2},
            'VAC096': {'desc': 'Meningitis B 2 dosis < 2 anys en 0 i 1 anys', 'vacuna': 'MeB', 'dosis': 2},
          },
        1: {
            'VAC001': {'desc': 'XRP Primera dosi 1 any', 'vacuna': 'XRP', 'dosis': 1},
            'VAC006': {'desc': 'DTPa Primovacunaciķ 1 any', 'vacuna': 'T', 'dosis': 2},
            'VAC007': {'desc': 'DTPa Record 1 any', 'vacuna': 'T', 'dosis': 3},
            'VAC010': {'desc': 'VPI Primovacunaciķ 1 any', 'vacuna': 'P', 'dosis': 2},
            'VAC011': {'desc': 'VPI Record 1 any', 'vacuna': 'P', 'dosis': 3},
            'VAC015': {'desc': 'Hib Primovacunaciķ 1 any', 'vacuna': 'Hib', 'dosis': 2},
            'VAC016': {'desc': 'Hib Record 1 any', 'vacuna': 'Hib', 'dosis': 3},
            'VAC019': {'desc': 'HB Primovacunaciķ 1 anys', 'vacuna': 'HB', 'dosis': 2},
            'VAC020': {'desc': 'HB Record 1 any', 'vacuna': 'HB', 'dosis': 3},
            'VAC023': {'desc': 'MCC Primovacunaciķ 1 any', 'vacuna': 'MCC', 'dosis': 1},
            'VAC025': {'desc': 'VNC Primovacunaciķ 1 any', 'vacuna': 'VNC', 'dosis': 2},
            'VAC026': {'desc': 'VNC Record 1 any', 'vacuna': 'VNC', 'dosis': 3},
            'VAC028': {'desc': 'VVZ Primera dosi 1 any', 'vacuna': 'VVZ', 'dosis': 1},
            'VAC049': {'desc': 'HA 1 dosi 1 any', 'vacuna': 'HA', 'dosis': 1},
            'VAC057': {'desc': 'Rotavirus 2 dosis 1 any', 'vacuna': 'R', 'dosis': 2},
            'VAC096': {'desc': 'Meningitis B 2 dosis < 2 anys en 0 i 1 anys', 'vacuna': 'MeB', 'dosis': 2},
            'VAC176': {'desc': 'Cobertura vacunal de grip en poblaciķ de 1 a 4 anys', 'vacuna': 'Grip', 'dosis': 1},
            },
         2: {   
            'VAC002': {'desc': 'XRP Primera dosi 2 anys', 'vacuna': 'XRP', 'dosis': 1},
            'VAC008': {'desc': 'DTPa Record 2 anys', 'vacuna': 'T', 'dosis': 3},
            'VAC012': {'desc': 'VPI Record 2 anys', 'vacuna': 'P', 'dosis': 3},
            'VAC017': {'desc': 'Hib Record 2 anys', 'vacuna': 'Hib', 'dosis': 3},
            'VAC021': {'desc': 'HB Record 2 anys', 'vacuna': 'HB', 'dosis': 3},
            'VAC027': {'desc': 'VNC Record 2 anys', 'vacuna': 'VNC', 'dosis': 3},
            'VAC029': {'desc': 'VVZ Primera dosi 2 anys', 'vacuna': 'VVZ', 'dosis': 1},
            'VAC050': {'desc': 'HA 1 dosi 2 anys', 'vacuna': 'HA', 'dosis': 1},
            'VAC077': {'desc': 'DTPa Primovacunaciķ 2 anys', 'vacuna': 'T', 'dosis': 2},
            'VAC078': {'desc': 'VPI Primovacunaciķ 2 anys', 'vacuna': 'P', 'dosis': 2}, 
            'VAC079': {'desc': 'Hib Primovacunaciķ 2 anys', 'vacuna': 'Hib', 'dosis': 2},
            'VAC080': {'desc': 'HB Primovacunaciķ 2 anys', 'vacuna': 'HB', 'dosis': 2},
            'VAC081': {'desc': 'MCC Primovacunaciķ 2 anys', 'vacuna': 'MCC', 'dosis': 1},
            'VAC082': {'desc': 'Rotavirus 2 dosis 2 anys', 'vacuna': 'R', 'dosis': 2},
            'VAC083': {'desc': 'VNC Primovacunaciķ 2 anys', 'vacuna': 'VNC', 'dosis': 2},
            'VAC176': {'desc': 'Cobertura vacunal de grip en poblaciķ de 1 a 4 anys', 'vacuna': 'Grip', 'dosis': 1},  
            },
        3: {
            'VAC084': {'desc': 'HA 1 dosi 3 anys', 'vacuna': 'HA', 'dosis': 1},
            'VAC176': {'desc': 'Cobertura vacunal de grip en poblaciķ de 1 a 4 anys', 'vacuna': 'Grip', 'dosis': 1},
            },
        4: {
            'VAC003': {'desc': 'XRP Segona dosi 4 anys', 'vacuna': 'XRP', 'dosis': 2},
            'VAC030': {'desc': 'VVZ Segona dosi 4 anys', 'vacuna': 'VVZ', 'dosis': 2},
            'VAC176': {'desc': 'Cobertura vacunal de grip en poblaciķ de 1 a 4 anys', 'vacuna': 'Grip', 'dosis': 1},
            },
        5:{
            'VAC004': {'desc': 'XRP Segona dosi 5 anys', 'vacuna': 'XRP', 'dosis': 2},
            'VAC013': {'desc': 'VPI tres dosis', 'vacuna': 'P', 'dosis': 3},
            'VAC031': {'desc': 'VVZ Segona dosi 5 anys', 'vacuna': 'VVZ', 'dosis': 2},
            },
        6:{
            'VAC051': {'desc': 'HA 2 dosis 6 anys', 'vacuna': 'HA', 'dosis': 2},
            'VAC159': {'desc': 'P en 6 anys que han rebut quatre dosis de VPI', 'vacuna': 'P', 'dosis': 4},
            },
        7:{
            'VAC052': {'desc': 'HA 2 dosis 7 anys', 'vacuna': 'HA', 'dosis': 2},
            'VAC103': {'desc': 'DT 4 dosis als 7 anys', 'vacuna': 'T', 'dosis': 4},
            'VAC104': {'desc': 'Pertussis 4 dosis als 7 anys', 'vacuna': 'TF', 'dosis': 4},
            'VAC105': {'desc': 'Polio 4 dosis als 7 anys', 'vacuna': 'P', 'dosis': 4},
            'VAC106': {'desc': 'Hib 4 dosis als 7 anys', 'vacuna': 'Hib', 'dosis': 4},
            'VAC107': {'desc': 'HB 3 dosis als 7 anys', 'vacuna': 'HB', 'dosis': 3},
            'VAC108': {'desc': 'VNC 3 dosis als 7 anys', 'vacuna': 'VNC', 'dosis': 3},
            'VAC109': {'desc': 'XRP 2 dosis als 7 anys', 'vacuna': 'XRP', 'dosis': 2},
            'VAC110': {'desc': 'VVZ 2 dosis als 7 anys', 'vacuna': 'VVZ', 'dosis': 2},
            'VAC111': {'desc': 'MCC 3 dosis als 7 anys', 'vacuna': 'MCC', 'dosis': 3},
            'VAC160': {'desc': 'P en 7 anys que han rebut quatre dosis de VPI', 'vacuna': 'P', 'dosis': 4},
            },
        8:{
            'VAC112': {'desc': 'DT 4 dosis als 8 anys', 'vacuna': 'T', 'dosis': 4},
            'VAC113': {'desc': 'Pertussis 4 dosis als 8 anys', 'vacuna': 'TF', 'dosis': 4},
            'VAC114': {'desc': 'Polio 4 dosis als 8 anys', 'vacuna': 'P', 'dosis': 4},
            'VAC115': {'desc': 'Hib 4 dosis als 8 anys', 'vacuna': 'Hib', 'dosis': 4},
            'VAC116': {'desc': 'HB 3 dosis als 8 anys', 'vacuna': 'HB', 'dosis': 3},
            'VAC117': {'desc': 'VNC 2 dosis als 8 anys', 'vacuna': 'VNC', 'dosis': 3},
            'VAC118': {'desc': 'XRP 2 dosis als 8 anys', 'vacuna': 'XRP', 'dosis': 2},
            'VAC119': {'desc': 'VVZ 2 dosis als 8 anys', 'vacuna': 'VVZ', 'dosis': 2},
            'VAC120': {'desc': 'MCC 3 dosis als 8 anys', 'vacuna': 'MCC', 'dosis': 3},
            },
        12:{
            'VAC032': {'desc': 'VPH una dosi als 12 anys nenes', 'vacuna': 'VPH', 'dosis': 1, 'sexe':'D'},
            'VAC139': {'desc': 'VPH una dosi als 12 anys nens', 'vacuna': 'VPH', 'dosis': 1, 'sexe':'H'},
            'VAC140': {'desc': 'VPH una dosi als 12 anys nens i nenes', 'vacuna': 'VPH', 'dosis': 1},
            'VAC034': {'desc': 'VPH dues dosis als 12 anys nenes', 'vacuna': 'VPH', 'dosis': 2, 'sexe':'D'},
            'VAC143': {'desc': 'VPH dues dosis als 12 anys nens', 'vacuna': 'VPH', 'dosis': 2, 'sexe':'H'},
            'VAC144': {'desc': 'VPH dues dosis als 12 anys nens i nenes', 'vacuna': 'VPH', 'dosis': 2},
            'VAC044': {'desc': 'VVZ agrupat dues dosis als 12 anys', 'vacuna': 'VVZ', 'dosis': 2},
            'VAC126': {'desc': 'HA agrupat dues dosis als 12 anys', 'vacuna': 'HA', 'dosis': 2},
            'VAC129': {'desc': 'HA una dosis als 12 anys', 'vacuna': 'HA', 'dosis': 1},
            },   
        13:{
            'VAC033': {'desc': 'VPH una dosi als 13 anys nenes', 'vacuna': 'VPH', 'dosis': 1, 'sexe':'D'},
            'VAC141': {'desc': 'VPH una dosi als 13 anys nens', 'vacuna': 'VPH', 'dosis': 1, 'sexe':'H'},
            'VAC142': {'desc': 'VPH una dosi als 13 anys nens i nenes', 'vacuna': 'VPH', 'dosis': 1},
            'VAC035': {'desc': 'VPH dues dosis als 13 anys nenes', 'vacuna': 'VPH', 'dosis': 2, 'sexe':'D'},
            'VAC145': {'desc': 'VPH dues dosis als 13 anys nens', 'vacuna': 'VPH', 'dosis': 2, 'sexe':'H'},
            'VAC146': {'desc': 'VPH dues dosis als 13 anys nens i nenes', 'vacuna': 'VPH', 'dosis': 2},
            'VAC045': {'desc': 'VVZ agrupat dues dosis als 13 anys', 'vacuna': 'VVZ', 'dosis': 2},
            'VAC053': {'desc': 'HA agrupat dues dosis als 13 anys', 'vacuna': 'HA', 'dosis': 2},
            }, 
        15:{
            'VAC127': {'desc': 'VPH una dosi als 15 anys', 'vacuna': 'VPH', 'dosis': 1, 'sexe':'D'},
            'VAC147': {'desc': 'VPH una dosi als 15 anys nens', 'vacuna': 'VPH', 'dosis': 1, 'sexe':'H'},
            'VAC148': {'desc': 'VPH una dosi als 15 anys nens i nenes', 'vacuna': 'VPH', 'dosis': 1},
            'VAC128': {'desc': 'VPH dues dosis als 15 anys nenes', 'vacuna': 'VPH', 'dosis': 2, 'sexe':'D'},
            'VAC149': {'desc': 'VPH dues dosis als 15 anys nens', 'vacuna': 'VPH', 'dosis': 2, 'sexe':'H'},
            'VAC150': {'desc': 'VPH dues dosis als 15 anys nens i nenes', 'vacuna': 'VPH', 'dosis': 2},
            }, 
        16:{
            'VAC058': {'desc': 'DT 5 dosis als 16 anys', 'vacuna': 'T', 'dosis': 5},
            'VAC059': {'desc': 'Pertussis 4 dosis als 16 anys', 'vacuna': 'TF', 'dosis': 4},
            'VAC060': {'desc': 'Polio 4 dosis als 16 anys', 'vacuna': 'P', 'dosis': 4},
            'VAC061': {'desc': 'Hib 3 dosis als 16 anys', 'vacuna': 'Hib', 'dosis': 3},
            'VAC062': {'desc': 'HB 3 dosis als 16 anys', 'vacuna': 'HB', 'dosis': 3},
            'VAC063': {'desc': 'VNC 3 dosis als 16 anys', 'vacuna': 'VNC', 'dosis': 3},
            'VAC064': {'desc': 'XRP 2 dosis als 16 anys', 'vacuna': 'XRP', 'dosis': 2},
            'VAC065': {'desc': 'VVZ 2 dosis als 16 anys', 'vacuna': 'VVZ', 'dosis': 2},
            'VAC066': {'desc': 'VPH 2 dosis als 16 anys', 'vacuna': 'VPH', 'dosis': 2, 'sexe':'D'},
            },  
        17:{
            'VAC086': {'desc': 'DT 5 dosis als 17 anys', 'vacuna': 'T', 'dosis': 5},
            'VAC087': {'desc': 'Pertussis 4 dosis als 17 anys', 'vacuna': 'TF', 'dosis': 4},
            'VAC088': {'desc': 'Polio 4 dosis als 17 anys', 'vacuna': 'P', 'dosis': 4},
            'VAC089': {'desc': 'Hib 3 dosis als 17 anys', 'vacuna': 'Hib', 'dosis': 3},
            'VAC090': {'desc': 'HB 3 dosis als 17 anys', 'vacuna': 'HB', 'dosis': 3},
            'VAC091': {'desc': 'VNC 3 dosis als 17 anys', 'vacuna': 'VNC', 'dosis': 3},
            'VAC092': {'desc': 'XRP 2 dosis als 17 anys', 'vacuna': 'XRP', 'dosis': 2},
            'VAC093': {'desc': 'VVZ 2 dosis als 17 anys', 'vacuna': 'VVZ', 'dosis': 2},
            'VAC094': {'desc': 'VPH 2 dosis als 17 anys', 'vacuna': 'VPH', 'dosis': 2, 'sexe':'D'},
            }, 
}

criteris_indicadors_Xedat = { 
        0:{
            'VAC036': {'desc': 'HB 1 dosi abans 1 mes  0 anys', 'vacuna': 'HB', 'dosis': 1, 'edatd': 0, 'tip': 'abans'},
            'VAC135': {'desc': 'MeB 2 dosis abans primer any de vida  1 any', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 12, 'tip': 'abans'},
          },
        1:{
            'VAC037': {'desc': 'HB 1 dosi abans 1 mes  1 any', 'vacuna': 'HB', 'dosis': 1, 'edatd': 0, 'tip': 'abans'},
            'VAC038': {'desc': 'MCC 1 dosi despres primer any de vida  1 any', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
            'VAC097': {'desc': 'Meningitis B 2 dosis < 2 anys en 1 i 2 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'abans'},
            'VAC136': {'desc': 'MeB 2 dosis despres primer any de vida  2 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'abans'},
            'VAC137': {'desc': 'MeB 3 dosis despres primer any de vida  2 anys', 'vacuna': 'MeB', 'dosis': 3, 'edatd': 24, 'tip': 'abans'},
            'VAC168': {'desc': 'MACYW 1 dosi despres 10 anys de vida 1 any', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
          },
        2:{
            'VAC039': {'desc': 'MCC 1 dosi despres primer any de vida  2 anys', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
            # 'VAC137': {'desc': 'MeB 3 dosis durant abans 3 anys de vida', 'vacuna': 'MeB', 'dosis': 3, 'edatd': 36, 'tip': 'abans'},
            'VAC097': {'desc': 'Meningitis B 2 dosis < 2 anys en 1 i 2 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'abans'},
            'VAC098': {'desc': 'Meningitis B 2 dosis < 2 anys en 2 i 3 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'abans'},
            'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
            'VAC169': {'desc': 'MACYW 1 dosi després de l any de vida als 2 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
          },
        3:{
            'VAC098': {'desc': 'Meningitis B 2 dosis < 2 anys en 2 i 3 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'abans'},
            'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
            'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
           }, 
        4:{
           'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
           'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
           }, 
        5:{
           'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
           'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
           }, 
        6:{
            'VAC046': {'desc': 'T 1 dosi despres 6 anys  6 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
            'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
            'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
            'VAC164': {'desc': 'VPI 1 dosi a partir dels 6 anys en infants de sis anys', 'vacuna': 'P', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
          },
        7:{
            'VAC047': {'desc': 'T 1 dosi despres 6 anys  7 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
            'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
            'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
            'VAC165': {'desc': 'VPI 1 dosi a partir dels 6 anys en infants de set anys', 'vacuna': 'P', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
          }, 
        8:{
            'VAC085': {'desc': 'T 1 dosi despres 6 anys  8 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
            'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
            'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
          },
        9:{
           'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
           'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
           'VAC125': {'desc': 'T 1 dosi despres 6 anys  9 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
           }, 
        10:{
           'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
           'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
           }, 
        11:{
            'VAC100': {'desc': 'Meningitis B 2 dosis > 2 anys en 3 - 11 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
            'VAC101': {'desc': 'Meningitis B 2 dosis > 11 anys en 11 - 14 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'despres'},
            },
        12:{
            'VAC040': {'desc': 'MCC 1 dosi despres 10 anys de vida  12 anys', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
            'VAC130': {'desc': 'MACYW 1 dosi despres 10 anys de vida  12 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
            'VAC042': {'desc': 'VVZ 2 dosis despres 10 anys de vida  12 anys', 'vacuna': 'VVZ', 'dosis': 2, 'edatd': 120, 'tip': 'despres'},
            'VAC101': {'desc': 'Meningitis B 2 dosis > 11 anys en 11 - 14 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'despres'},
            'VAC102': {'desc': 'Meningitis B 2 dosis > 11 anys en 12 - 15 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'despres'},
            'VAC171': {'desc': 'HA 1 dosi a partir dels 10 anys als 12 anys', 'vacuna': 'HA', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
          },
        13:{
            'VAC041': {'desc': 'MCC 1 dosi despres 10 anys de vida  13 anys', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
            'VAC131': {'desc': 'MACYW 1 dosi despres 10 anys de vida  13 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
            'VAC043': {'desc': 'VVZ 2 dosis despres 10 anys de vida  13 anys', 'vacuna': 'VVZ', 'dosis': 2, 'edatd': 120, 'tip': 'despres'},
            'VAC054': {'desc': 'HA 2 dosis despres 10 anys de vida  13 anys', 'vacuna': 'HA', 'dosis': 2, 'edatd': 120, 'tip': 'despres'},
            'VAC101': {'desc': 'Meningitis B 2 dosis > 11 anys en 11 - 14 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'despres'},
            'VAC102': {'desc': 'Meningitis B 2 dosis > 11 anys en 12 - 15 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'despres'},
            'VAC132': {'desc': 'MACYW 1 dosi despres dels 12 anys en 13 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 144, 'tip': 'despres'},
            'VAC172': {'desc': 'HA 1 dosi a partir dels 10 anys als 13 anys', 'vacuna': 'HA', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
          },
        14:{
            'VAC048': {'desc': 'T 1 dosi despres 12 anys  14 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 144, 'tip': 'despres'},
            'VAC055': {'desc': 'HA 2 dosis despres 10 anys de vida  14 anys', 'vacuna': 'HA', 'dosis': 2, 'edatd': 120, 'tip': 'despres'},
            'VAC101': {'desc': 'Meningitis B 2 dosis > 11 anys en 11 - 14 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'despres'},
            'VAC102': {'desc': 'Meningitis B 2 dosis > 11 anys en 12 - 15 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'despres'},
            'VAC133': {'desc': 'MACYW 1 dosi despres dels 12 anys en 14 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 144, 'tip': 'despres'},
            'VAC166': {'desc': 'DTPa/ dTpa/ Td 1 dosi despres 6 anys als 14 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
          },
        15:{
            'VAC068': {'desc': 'T 1 dosi despres 12 anys  15 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 144, 'tip': 'despres'},
            'VAC102': {'desc': 'Meningitis B 2 dosis > 11 anys en 12 - 15 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'despres'},
            'VAC134': {'desc': 'MACYW 1 dosi despres dels 14 anys en 15 - 18 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 168, 'tip': 'despres'},
          },   
        16:{
            'VAC067': {'desc': 'MCC 1 dosi despres 10 anys  16 anys', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
            'VAC134': {'desc': 'MACYW 1 dosi despres dels 14 anys en 15 - 18 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 168, 'tip': 'despres'},
          }, 
        17:{
            'VAC124': {'desc': 'MCC 1 dosi despres 10 anys  17 anys', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
            'VAC134': {'desc': 'MACYW 1 dosi despres dels 14 anys en 15 - 18 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 168, 'tip': 'despres'},
          },
        18:{
            'VAC134': {'desc': 'MACYW 1 dosi despres dels 14 anys en 15 - 18 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 168, 'tip': 'despres'},
            'VAC167': {'desc': 'Tetanus 1 dosi despres 12 anys als 18 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 144, 'tip': 'despres'}, 
            'VAC170': {'desc': 'MACYW 1 dosi després dels 10 anys de vida als 18 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
          },
}

# Obtenir els codis de antigens asociats als agrupadors detallats a antigens_agrupadors
antigens, vacunes_relacionades = defaultdict(list), defaultdict(list)
for antigen_desc in antigens_agrupadors:
    agrupador = antigens_agrupadors[antigen_desc]
    sql = """
            SELECT DISTINCT
                criteri_codi
            FROM
                eqa_criteris
            WHERE
                taula IN ('vacunesped', 'vacunes')
                AND agrupador = {}
          """.format(agrupador)
    for codi_antigen, in getAll(sql, nod):
        antigens[antigen_desc].append(codi_antigen)
        antigens[antigen_desc].append('cutre')

# Obtenir els codis de vacunaciķ asociats als codis d'antigen ontinguts al pas anterior
    in_crit = tuple(antigens[antigen_desc]) 
    sql = """
            SELECT
                vacuna
            FROM
                cat_prstb040_new
            WHERE
                antigen IN {0}
          """.format(in_crit)
    for codi_vacuna, in getAll(sql, imp):
        vacunes_relacionades[codi_vacuna].append(antigen_desc)

# Obtenir dades de vacunaciķ de ped_vacunes
vacunats = {}
sql = """
        SELECT
            id_cip_sec,
            agrupador,
            dosis
        FROM
            ped_vacunes
      """
for id_cip_sec, agrupador, dosi in getAll(sql, nod):
    vacunats[(id_cip_sec, agrupador)] = dosi

# Obtenir dades de vacunaciķ de eqa_vacunes
sql = """
        SELECT
            id_cip_sec,
            agrupador,
            dosis
        FROM
            eqa_vacunes
      """
for id_cip_sec, agrupador, dosi in getAll(sql, nod):
    vacunats[(id_cip_sec, agrupador)] = dosi

# Obtenir dades i dates de vacunaciķ d'import.vacunes
dates_vacunes =  defaultdict(list)

for particio in getSubTables("vacunes"):
    sql = """
            SELECT
                id_cip_sec,
                va_u_cod,
                va_u_data_vac
            FROM
                {0}
            WHERE
                va_u_data_baixa = 0
          """.format(particio)
    for id_cip_sec, codi_vacuna, data_vacuna in getAll(sql, imp):  
        if codi_vacuna in vacunes_relacionades:
            for antigen_agrupador in vacunes_relacionades[codi_vacuna]:
                dates_vacunes[(id_cip_sec, antigen_agrupador)].append(data_vacuna)

sql = """
            SELECT
                id_cip_sec,
                va_u_cod,
                va_u_data_vac
            FROM
                vacunes
            WHERE
                va_u_data_baixa = 0
          """
for id_cip_sec, codi_vacuna, data_vacuna in getAll(sql, 'import_jail'):
        if codi_vacuna in vacunes_relacionades:
            for antigen_agrupador in vacunes_relacionades[codi_vacuna]:
                dates_vacunes[(id_cip_sec, antigen_agrupador)].append(data_vacuna)
    
# Cālcul indicadors
resultatsIndividual = dict()
recompte = Counter()

sql = """
        SELECT
            id_cip_sec,
            up,
            ates,
            edat,
            sexe,
            data_naix
        FROM
            assignada_tot_with_jail
        WHERE
            edat < 19
      """
for id_cip_sec, up, ates, edat, sexe, dnaix in getAll(sql, nod):
    if edat in criteris_indicadors:
        for indicador in criteris_indicadors[edat]:
            vac = criteris_indicadors[edat][indicador]['vacuna']
            if vac != 'Grip' or (vac == 'Grip' and calcular):
                try:
                    sex = criteris_indicadors[edat][indicador]['sexe']
                except KeyError:
                    sex = sexe
                if sex == sexe:
                    den, num = 1, 0
                    dosi = criteris_indicadors[edat][indicador]['dosis']
                    desc = criteris_indicadors[edat][indicador]['desc']
                    agr_vac = antigens_agrupadors[vac]
                    edat = int(edat)
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
                    if ates == 1:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'DEN'] += 1
                    if (id_cip_sec, agr_vac) in vacunats:
                        dosiposada = vacunats[(id_cip_sec, agr_vac)]
                        if dosiposada >= dosi:
                            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS',indicador, 'NUM'] += 1
                            num = 1
                            if ates == 1:
                                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'NUM'] += 1
                    resultatsIndividual[(id_cip_sec, indicador)] = {'ates': ates, 'desc': desc, 'up': up, 'den': den, 'num': num}
    if edat in criteris_indicadors_Xedat:
        for indicador in criteris_indicadors_Xedat[edat]:
            edat = int(edat)
            den, num = 1, 0
            vac = criteris_indicadors_Xedat[edat][indicador]['vacuna']
            dosi = criteris_indicadors_Xedat[edat][indicador]['dosis']
            desc = criteris_indicadors_Xedat[edat][indicador]['desc']
            edatDosi = criteris_indicadors_Xedat[edat][indicador]['edatd']
            tip = criteris_indicadors_Xedat[edat][indicador]['tip']
            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', indicador, 'DEN'] += 1
            if ates == 1:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'DEN'] += 1
            if (id_cip_sec, vac) in dates_vacunes:
                ok = 0
                for data in dates_vacunes[(id_cip_sec, vac)]:
                    edat_m = monthsBetween(dnaix, data)
                    if tip == 'abans':
                        if edat_m <= edatDosi:
                            ok += 1
                    else:
                        if edat_m >= edatDosi:
                            ok += 1
                if ok >= dosi:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS',indicador, 'NUM'] += 1
                    num = 1
                    if ates == 1:
                        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT',indicador, 'NUM'] += 1
            resultatsIndividual[(id_cip_sec, indicador)] = {'ates': ates, 'desc': desc, 'up': up, 'den': den, 'num': num}

# Cālcul estrany pel meningo B (VAC069 i VAC070)
vac069 = 'Meningitis B en 2-10 anys amb 3 dosis i que tenen 1 dosi abans dels 2 anys'
vac070 = 'Meningitis B en 2-10 anys amb 2 dosis i sense cap dosi abans dels 2 anys' 

sql = """
        SELECT
            id_cip_sec,
            datamin,
            dosis
        FROM
            ped_vacunes
        WHERE
            agrupador = 548
      """
vacunats_meningoB = {id_cip_sec: {'datamin': datamin, 'dosi': dosi} for id_cip_sec, datamin, dosi in getAll(sql, nod)}

sql = """
        SELECT
            id_cip_sec,
            up,
            ates,
            edat_a,
            sexe,
            data_naix
        FROM
            ped_assignada
        WHERE
            edat_a BETWEEN 2 AND 10
      """
for id_cip_sec, up, ates, edat, sexe, naix in getAll(sql, nod):
    edat = int(edat)
    if id_cip_sec in vacunats_meningoB:
        datamin, dosis = vacunats_meningoB[id_cip_sec]['datamin'], vacunats_meningoB[id_cip_sec]['dosi']
        edat_primera_vacunacio_meningoB = monthsBetween(naix, datamin)
        if 0 <= int(edat_primera_vacunacio_meningoB) <= 24:
            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', 'VAC069', 'DEN'] += 1
            if ates == 1:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT','VAC069', 'DEN'] += 1
            if dosis >= 3: 
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', 'VAC069', 'NUM'] += 1
                if ates == 1:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT','VAC069', 'NUM'] += 1
        else:
            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', 'VAC070', 'DEN'] += 1
            if ates == 1:
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT','VAC070', 'DEN'] += 1
            if dosis >= 2: 
                recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', 'VAC070', 'NUM'] += 1
                if ates == 1:
                    recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT','VAC070', 'NUM'] += 1
    else:
        recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSASS', 'VAC070', 'DEN'] += 1
        if ates == 1:
            recompte[up, ageConverter(edat,5), sexConverter(sexe), 'NOINSAT','VAC070', 'DEN'] += 1

# Exportar dades en format taula
table = 'exp_dep_vacunes'
create = "(up varchar(5) not null default'', edat varchar(10), sexe varchar(10), comb varchar(10), indicador varchar(10), tipus varchar(10), recompte int)"
createTable(table,create,db, rm=True)

upload = []
for (up, edat, sexe, comb, indicador, tip), rec in recompte.items():
    indicador = 'D' + indicador
    upload.append([up, edat, sexe, comb, indicador, tip, rec])
listToTable(upload, table, db)