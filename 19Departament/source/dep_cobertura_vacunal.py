import sisapUtils as u
import sisaptools as t
import collections as c
import datetime as d
from dateutil.relativedelta import relativedelta

class Cobertura():
    def __init__(self):
        u.printTime('inici')
        self.get_pob()
        u.printTime('get_pob')
        self.get_vacunes()
        u.printTime('get_vacunes')
        self.get_indicadors()
        u.printTime('get_indicadors')
    
    def get_pob(self):
        sql = """
            SELECT id_cip_sec, up, data_naix, edat, ates, sexe
            FROM assignada_tot
        """
        self.pob = c.defaultdict()
        for id, up, data, edat, ates, sexe in u.getAll(sql, 'nodrizas'):
            self.pob[id] = {
                'up': up,
                'naixement': data,
                'edat': edat,
                'ates': ates,
                'sexe': sexe
            }
    
    def get_vacunes(self):
        self.vacunes = c.defaultdict(list)
        grups = {
            977: 'pn13-15',
            978: 'pn20',
            698: 'pn13-15-20',
            48: 'pn23',
            49: 'td',
            576: 'td',
            920: 'hz'
        }
        sql = """
            SELECT id_cip_sec, agrupador, maxedat, dosis
            FROM eqa_vacunes
            WHERE agrupador in (977, 978, 698, 48, 49, 576, 920)
            AND dosis > 0
        """
        for id, codi, maxedat, dosis in u.getAll(sql, 'nodrizas'):
            self.vacunes[id].append((codi, maxedat, dosis))
    def get_indicadors(self):
        indicadors = {
            'DVAC185': {
                'edat': 65,
                'interval': False,
                'num': [977,698,978,48],
                'dosis': None,
                'minedat': 60
            },
            'DVAC186': {
                'edat': 66,
                'interval': False,
                'num': [977,698,978,48],
                'dosis': None,
                'minedat': 60
            },
            'DVAC187': {
                'edat': 75,
                'interval': False,
                'num': [977,698,978,48],
                'dosis': None,
                'minedat': 60
            },
            'DVAC188': {
                'edat': 76,
                'interval': False,
                'num': [977,698,978,48],
                'dosis': None,
                'minedat': 60
            },
            'DVAC189': {
                'edat': 75,
                'interval': False,
                'num': [977,698,978],
                'dosis': None,
                'minedat': 60
            },
            'DVAC190': {
                'edat': 76,
                'interval': False,
                'num': [977,698,978],
                'dosis': None,
                'minedat': 60
            },
            'DVAC191': {
                'edat': 75,
                'interval': False,
                'num': [48],
                'dosis': None,
                'minedat': 60
            },
            'DVAC192': {
                'edat': 76,
                'interval': False,
                'num': [48],
                'dosis': None,
                'minedat': 60
            },
            'DVAC193': {
                'edat': 75,
                'interval': False,
                'num': [49,576],
                'dosis': None,
                'minedat': 60
            },
            'DVAC194': {
                'edat': 76,
                'interval': False,
                'num': [49,576],
                'dosis': None,
                'minedat': 60
            },
            'DVAC195': {
                'edat': 65,
                'interval': False,
                'num': [977,698,978,48],
                'dosis': None,
                'minedat': 65
            },
            'DVAC196': {
                'edat': 90,
                'interval': True,
                'num': [920],
                'dosis': 1,
                'minedat': 0
            },
            'DVAC197': {
                'edat': 90,
                'interval': True,
                'num': [920],
                'dosis': 2,
                'minedat': 0
            },
        }
        dades = c.defaultdict(set)
        # procediment:
        # - comprovar denominador
        # - comprovar que te vacunes
        # - comprovar edat de les vacunes
        # - comprovar dosis
        for ind, data in indicadors.items():
            edat_ind = data['edat']
            interval = data['interval']
            num = data['num']
            dosis = data['dosis']
            minedat = data['minedat']
            for id in self.pob:
                if interval:
                    cond = self.pob[id]['edat'] >= edat_ind
                else:
                    cond = self.pob[id]['edat'] == edat_ind
                if cond:
                    up = self.pob[id]['up']
                    ates = self.pob[id]['ates']
                    sexe = self.pob[id]['sexe']
                    edat = self.pob[id]['edat']
                    dades[(ind, up, u.ageConverter(edat,5), u.sexConverter(sexe), 'NOINSASS', 'DEN')].add(id)
                    if ates == 1:
                        dades[(ind, up, u.ageConverter(edat,5), u.sexConverter(sexe), 'NOINSAT', 'DEN')].add(id)
                    if id in self.vacunes:
                        for codi, maxedat, dosis_v in self.vacunes[id]:
                            if codi in num and maxedat/12 >= minedat:
                                if dosis and dosis_v == dosis:
                                    dades[(ind, up, u.ageConverter(edat,5), u.sexConverter(sexe), 'NOINSASS', 'NUM')].add(id)
                                    if ates == 1:
                                        dades[(ind, up, u.ageConverter(edat,5), u.sexConverter(sexe), 'NOINSAT', 'NUM')].add(id)
                                elif not dosis:
                                    dades[(ind, up, u.ageConverter(edat,5), u.sexConverter(sexe), 'NOINSASS', 'NUM')].add(id)
                                    if ates == 1:
                                        dades[(ind, up, u.ageConverter(edat,5), u.sexConverter(sexe), 'NOINSAT', 'NUM')].add(id)
        upload = []
        for ind, up, edat, sexe, situacio, analisi in dades:
            upload.append(( up, edat, sexe, situacio, ind, analisi, len(dades[(ind, up, edat, sexe, situacio, analisi)])))
        cols = "(up varchar(5), edat varchar(10), sexe varchar(10), comb varchar(10), indicador varchar(7), tipus varchar(3), recompte int)"
        u.createTable('exp_dep_vacunes2', cols, 'depart', rm=True)
        u.listToTable(upload, 'exp_dep_vacunes2', 'depart')

    def upload_master(self):
        upload = []
        for id in self.vacunes:
            if id in self.pob:
                up = self.pob[id]['up']
                edat = self.pob[id]['edat']
                data = self.pob[id]['naixement']
                for codi, maxedat, dosis in self.vacunes[id]:
                    upload.append((id, up, data, edat, codi, maxedat, dosis))
        cols = "(id_cip_sec int, up varchar(5), data_naixement date, edat int, codi int, maxedat int, dosis int)"
        u.createTable('cobertura_vacunal', cols, 'altres', rm=True)
        u.listToTable(upload, 'cobertura_vacunal', 'altres')

Cobertura()