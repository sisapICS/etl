# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter


components = ['DNOENOL', 'DENOLBRISC', 'DENOLRISC', 'DENOLDEP']


def get_alcohol():
    alcohols = {}
    # Prenem els problemes de salut codificants
    codis = ('C01-F10.10', 'C01-F10.129', 'C01-F10.19', 'C01-F10.988', 'C01-F10.120',
                    'C01-F10.14', 'C01-F10.188', 'C01-F10.980', 'C01-F10.920', 'C01-F10.96',
                    'C01-F10.180', 'C01-F10.239', 'C01-F10.280', 'C01-F10.29', 'C01-F10.24', 'C01-F10.231',
                    'C01-F10.19', 'C01-F10.229', 'C01-F10.230', 'C01-F10.232', 'C01-F10.921',
                    'C01-F10.259', 'C01-F10.121', 'C01-F10.220')
    sql = 'select pr_cod_ps, id_cip_sec from import.problemes where pr_v_dat is not null and pr_cod_ps in {}'.format(codis)
    for codi, id in getAll(sql, 'import'):
        if codi in ('C01-F10.10', 'C01-F10.129', 'C01-F10.19', 'C01-F10.988', 'C01-F10.120',
                    'C01-F10.14', 'C01-F10.188', 'C01-F10.980', 'C01-F10.920', 'C01-F10.96',
                    'C01-F10.180'):
            alcoholTxt = 'DENOLRISC'
        if codi in ('C01-F10.239', 'C01-F10.280', 'C01-F10.29', 'C01-F10.24', 'C01-F10.231',
                    'C01-F10.19', 'C01-F10.229', 'C01-F10.230', 'C01-F10.232', 'C01-F10.921',
                    'C01-F10.259', 'C01-F10.121', 'C01-F10.220'):   
            alcoholTxt = 'DENOLDEP'              
        alcohols[(id)] = {'alcohol': alcoholTxt}

    # Obtenim diccionari hash cip per poder guardar info de redics
    hash_cip = {}
    sql = 'select id_cip_sec, hash_d from import.u11'
    for id, hash in getAll(sql, 'import'):
        hash_cip[hash] = id

    # Afegim actuacions codificants
    codis = ('CP201','ALRIS','VP2001','EP2004','EP2005','AUDIT','VP201')
    sql = 'select au_cod_u, au_cod_ac, au_val from prstb016 \
            where au_cod_ac in {} and au_val is not null'.format(codis)
    for id, codi, valor in getAll(sql, 'redics'):
        try:
            valor = int(valor)
            if ((codi == 'CP201' and valor == 1) or (codi == 'ALRIS' and valor == 0) or
                (codi == 'VP2001' and valor == 0) or (codi == 'EP2004' and valor == 0) or
                (codi == 'EP2005' and valor == 0) or (codi == 'AUDIT' and valor == 0)):
                alcoholTxt = 'DNOENOL'
            elif ((codi == 'CP201' and valor == 2) or (codi == 'ALRIS' and valor == 1) or
                (codi == 'VP2001' and valor in (1,2,3,4)) or (codi == 'EP2004' and valor in (1,2,3,4)) or
                (codi == 'EP2005' and valor in (1,2,3,4,5,6)) or (codi == 'AUDIT' and valor in (1,2,3,4,5,6))):
                alcoholTxt = 'DENOLBRISC'
            elif ((codi == 'CP201' and valor in (3,4)) or (codi == 'VP201' and valor in (1,2)) or
                (codi == 'VP2001' and valor >= 5) or (codi == 'EP2004' and valor >= 5) or
                (codi == 'EP2005' and valor in (7,8,9,10,11)) or (codi == 'AUDIT' and valor in (7,8,9,10,11))):
                alcoholTxt = 'DENOLRISC'
            elif ((codi == 'CP201' and valor == 5) or (codi == 'VP201' and valor in (3,4)) or
                (codi == 'EP2005' and valor >= 12) or (codi == 'AUDIT' and valor >= 12)):
                alcoholTxt = 'DENOLDEP'
        except:
            text = 'nothing'
        try:
            id_sec = (hash_cip[id])
            alcohols[id_sec] = {'alcohol': alcoholTxt}
        except KeyError:
            pass
        
    return alcohols


def do_things(alcohols):
    uploadAlcohol = []
    recompte = Counter()
    sql = "select id_cip_sec, edat, sexe, up, if(institucionalitzat=1,'INS','NOINS'), ates from assignada_tot where edat >14"
    for id, edat, sexe, up, insti, ates in getAll(sql, 'nodrizas'):
        # Obtenim denominador, el total de la població major de 14
        for component in components:
            comb = insti + 'ASS'
            recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, component,'DEN'] += 1
            if int(ates) == 1:
                comb = insti + 'AT'
                recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, component,'DEN'] += 1
        # Comprovació que existeixi 
        try:
            alcoholTxt = alcohols[(id)]['alcohol']
        except KeyError:
            alcoholTxt = 'DNOINFOALCOHOL'
        # Obtenim numerador
        comb = insti + 'ASS'
        recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, alcoholTxt, 'NUM'] += 1
        if int(ates) == 1:
            comb = insti + 'AT'
            recompte[up, ageConverter(edat,5), sexConverter(sexe), comb, alcoholTxt, 'NUM'] += 1
    # Creació de la taula
    for (up, edat, sexe, comb, alcohol, tip), rec in recompte.items():
         uploadAlcohol.append([up, edat, sexe, comb, alcohol, tip , rec])
    listToTable(uploadAlcohol, TableAlcohol, 'depart')
    
              
alcohol = get_alcohol()

TableAlcohol = 'exp_dep_alcohol'
create = "(up varchar(5) not null default'', edat varchar(10), sexe varchar(10), comb varchar(10), indicador varchar(10), tipus varchar(10), recompte int)"
createTable(TableAlcohol,create,'depart', rm=True)

do_things(alcohol)

