# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime
from dateutil.relativedelta import relativedelta

# Definici� de l'indicador
INDICADOR = "IASSIR28"
TB_NAME = "cpr_indicador_{}".format(INDICADOR)
DB_NAME = 'assir'
COL_NAMES = "(indicador varchar(10), assir varchar(10), analisi varchar(10), n int)"
COL_NAMES_MST = "(indicador varchar(10), id_cip_sec int, assir varchar(10), num int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants.

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any

    sql = """
            SELECT
                data_ext,
                date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY)
            FROM
                dextraccio
          """
    data_ext, data_ext_menys1any = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� assignada al servei ASSIR.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les persones assignades.
    """
    global poblacio_ass;            poblacio_ass = c.defaultdict(lambda: c.defaultdict(set))

    for tipus_up in ("up_assir", "up"):
        sql = """
                SELECT
                    id_cip_sec,
                    visi_{}
                FROM
                    ass_imputacio_{}
            """.format(tipus_up, tipus_up)
        for id_cip_sec, up in u.getAll(sql, 'nodrizas'):
            poblacio_ass[id_cip_sec][tipus_up].add(up)

def get_denominador():
    """
    Calcula el denominador de l'indicador.

    Defineix:
        - 'denominador': Conjunt d'identificadors de les dones embarassades assignades al servei ASSIR durant el per�ode d'avaluaci�.
    """
    global denominador;             denominador = c.defaultdict(set)

    sql = """
            SELECT
                id_cip_sec,
                dde
            FROM
                eqa_problemes_incid
            WHERE
                ps = 454         -- Agrupador 454 indica diagn�stic d'incontin�ncia urin�ria
                AND dde BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
          """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    for id_cip_sec, data_dx in u.getAll(sql, "nodrizas"):
        if id_cip_sec in poblacio_ass:
            denominador[id_cip_sec].add(data_dx)

def get_numerador():
    """
    Calcula el numerador de l'indicador, que inclou:
    1. Dones embarassades que han deixat de fumar o que no fumen durant l'embar�s.

    Defineix:
        - 'numerador': Conjunt d'identificadors de les dones embarassades que han deixat de fumar o no fumen durant l'embar�s.
    """
    global numerador
    numerador = set()

    # Identifica les embarassades que han deixat de fumar o no fumen
    sql = """
            SELECT
                id_cip_sec,
                dat
            FROM
                ass_variables
            WHERE
                agrupador = 1091  -- Agrupador 1091 indica el test de Wexner
                AND dat BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
          """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    for id_cip_sec, data_ecografia in u.getAll(sql, "nodrizas"):
        if id_cip_sec in denominador and any(data_dx - datetime.timedelta(days=180) <= data_ecografia <= data_dx + datetime.timedelta(days=90) for data_dx in denominador[id_cip_sec]):
            numerador.add(id_cip_sec)

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """

    global upload, upload_mst
    upload, upload_mst = c.defaultdict(list), c.defaultdict(list)
    resultat = c.defaultdict(lambda: c.defaultdict(set))

    for id_cip_sec in denominador:
        for tipus_up, ups in poblacio_ass[id_cip_sec].items():
            num = id_cip_sec in numerador
            for up in ups:
                resultat["DEN"][(tipus_up, INDICADOR, up)].add(id_cip_sec)
                if num:
                    resultat["NUM"][(tipus_up, INDICADOR, up)].add(id_cip_sec)

    # Dades per UP
    for analisi in resultat:
        for (tipus_up, indicador, up), pacients in resultat[analisi].items():
            upload[tipus_up].append((indicador, up, analisi, len(pacients)))

    # Dades per pacient (mst)
    for (tipus_up, indicador, up), pacients_den in resultat["DEN"].items():
        pacients_num = resultat["NUM"].get((tipus_up, indicador, up), set())
        for id_cip_sec in pacients_den:
            num = 1 if id_cip_sec in pacients_num else 0
            upload_mst[tipus_up].append((indicador, id_cip_sec, up, num))

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """

    # Taules per UP
    u.createTable('{}_up'.format(TB_NAME), COL_NAMES, 'assir', rm=True)
    u.listToTable(upload["up"], '{}_up'.format(TB_NAME), 'assir')
    u.createTable('{}_up_assir'.format(TB_NAME), COL_NAMES, 'assir', rm=True)
    u.listToTable(upload["up_assir"], '{}_up_assir'.format(TB_NAME), 'assir')

    # Taules per pacient (mst)
    u.createTable('mst_{}_up'.format(TB_NAME), COL_NAMES_MST, 'assir', rm=True)
    u.listToTable(upload_mst["up"], 'mst_{}_up'.format(TB_NAME), 'assir')
    u.createTable('mst_{}_up_assir'.format(TB_NAME), COL_NAMES_MST, 'assir', rm=True)
    u.listToTable(upload_mst["up_assir"], 'mst_{}_up_assir'.format(TB_NAME), 'assir')

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))