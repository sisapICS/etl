# coding: latin1

"""
.
"""

import collections as c

import shared as s
import sisapUtils as u


class Poblacio(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_data()
        self.get_assignacio()
        self.get_visites()
        self.get_cataleg()
        self.get_poblacio()
        self.set_visites()

    def get_centres(self):
        """."""
        sql = "select scs_codi from cat_centres"
        self.centres = set([up for up, in u.getAll(sql, "nodrizas")])

    def get_data(self):
        """."""
        self.demos = {}
        self.dades = c.defaultdict(lambda: c.defaultdict(set))
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        sql = "select id_cip, usua_uab_up, usua_up_rca, \
                      usua_sexe, usua_data_naixement \
               from assignada \
               where usua_situacio = 'A'"
        for id, up, rca, sexe, naix in u.getAll(sql, "import"):
            self.demos[id] = (sexe, u.yearsBetween(naix, dext))
            if up:
                self.dades[id]["up"].add(up)
            if rca:
                self.dades[id]["rca"].add(rca)

    def get_assignacio(self):
        """."""
        self.assignacio = {}
        for id, dades in self.dades.items():
            if len(dades["up"]) == 1:
                self.assignacio[id] = dades["up"].pop()
            elif len(dades["up"]) == 0 and len(dades["rca"]) == 1:
                this = dades["rca"].pop()
                if this not in self.centres:
                    self.assignacio[id] = this

    def get_visites(self):
        """."""
        self.visites = c.defaultdict(c.Counter)
        self.visites_for_later = []
        sql = "select * from {}".format(s.VISITES_TMP)
        for row in u.getAll(sql, s.DATABASE):
            id, up = row[:2]
            self.visites[id][up] += 1
            self.visites_for_later.append(row)

    def get_cataleg(self):
        """."""
        dades = list()
        sql = "select a.up_cod, a.es_ecap, nvl(b.assir_cod, 'ND'), \
                      nvl(c.up_codi_up_ics, nvl(b.assir_cod, 'ND')),\
                      nvl(d.up_des, 'No disponible'), \
                      decode(b.assir_cod, null, 0, 1) \
               from sisap_covid_dbc_centres a \
               left join sisap_covid_dbc_assir b on a.up_cod = b.up_cod \
               left join (select distinct up_codi_up_scs, up_codi_up_ics \
                          from gcctb008 \
                          where up_data_baixa is null) c \
                    on b.assir_cod = c.up_codi_up_scs \
               left join sisap_covid_dbc_rup d on b.assir_cod = d.up_cod"
        for row in u.getAll(sql, "redics"):
            if row[3] == '00AAA':
                row = ("05238", 1, "05238", "05238", "ASSIR Aran", 1)
            if row[3] == '00BBB':
                row = ("00024", 1, "03334", "03334", "ASSIR Cerdanya", 1)
            dades.append(row)
        # dades = [row for row in u.getAll(sql, "redics")]
        cols = "(eap varchar(5), ecap int, up varchar(5), br varchar(5), \
                 nom varchar(255), valid int)"
        u.createTable(s.ASSIGNACIO, cols, s.DATABASE, rm=True)
        u.listToTable(dades, s.ASSIGNACIO, s.DATABASE)
        sql = "create or replace table {} as \
               select distinct up, br, nom \
               from {} \
               where valid = 1".format(s.CATALEG, s.ASSIGNACIO)
        u.execute(sql, s.DATABASE)

    def get_poblacio(self):
        """."""
        sql = "select eap, up from {} where valid = 1".format(s.ASSIGNACIO)
        cataleg = {up: assir for (up, assir) in u.getAll(sql, s.DATABASE)}
        self.assirs = set(cataleg.values())
        self.poblacio = {id: (up, cataleg[up]) + self.demos[id] + (1 * any([assir in self.assirs for assir in self.visites[id]]),)  # noqa
                         for (id, up) in self.assignacio.items()
                         if up in cataleg}
        cols = "(id int, eap varchar(5), assir varchar(5), \
                 sexe varchar(1), edat int, ates int)"
        u.createTable(s.POBLACIO, cols, s.DATABASE, rm=True)
        upload = [(k,) + v for (k, v) in self.poblacio.items()]
        u.listToTable(upload, s.POBLACIO, s.DATABASE)

    def set_visites(self):
        """."""
        upload = [(row[0],) + self.poblacio[row[0]] + row[1:]
                  for row in self.visites_for_later
                  if row[0] in self.poblacio and row[1] in self.assirs]
        cols = [u.getColumnInformation(column, s.POBLACIO, s.DATABASE)["create"]  # noqa
                for column in u.getTableColumns(s.POBLACIO, s.DATABASE)]
        cols += [u.getColumnInformation(column, s.VISITES_TMP, s.DATABASE)["create"]  # noqa
                 for column in u.getTableColumns(s.VISITES_TMP, s.DATABASE)
                 if column != "id"]
        cols_s = "({})".format(", ".join(cols))
        u.createTable(s.VISITES, cols_s, s.DATABASE, rm=True)
        u.listToTable(upload, s.VISITES, s.DATABASE)


if __name__ == "__main__":
    Poblacio()