# coding: latin1

"""
.
"""

import collections as c
import sisapUtils as u
import sisaptools as t
import datetime

TB_NAME = "cpr_assir"
DB_NAME = "assir"
COL_NAMES = "(up_assir varchar(5), indicador varchar(10), analisi varchar(5), n int)"

LV_FILE = "ASSIRIND"
FILE = "SIAC_ECAP_ASSR_{}{}.txt"

# registred indicator: (khalix key, catsalut key)
ind_noms = {'AGACCEMB': ('IAGACCEMB', 'AGACCEMB'),
            'AGACCIVE': ('IAGACCIVE', 'AGACCIVE'),
            'AGASSDXVM': ('IAGASSDXVM', 'AGASSDXVM'),
            'AS_02': ('AS_02', 'AS-02'),
            'AS_10': ('AS_10', 'AS-10'),
            'AS_14': ('AS_14', 'AS-14'),
            'AS_16': ('AS_16', 'AS-16'),
            'AS_21': ('AS_21', 'AS-21'),
            'CAT0303B': ('CAT0303B', 'CAT0303B'),
            'EQA9202': ('IASSIR19', 'EQA9202'),
            'EQA9204': ('IASSIR20', 'EQA9204'),
            'EQA9208': ('IASSIR21', 'EQA9208'),
            'EQA9210': ('IASSIR14', 'EQA9210'),
            'EQA9214': ('IASSIR15', 'EQA9214'),
            'EQA9215': ('IASSIR22', 'EQA9215'),
            'EQA9218': ('IASSIR23', 'EQA9218'),
            'EQA9220': ('IASSIR24', 'EQA9220'),
            'EQA9221': ('IASSIR16', 'EQA9221'),
            'EQA9225': ('IASSIR17', 'EQA9225'),
            'EQA9227B': ('IASSIR18', 'EQA9227B'),
            'EQA9227C': ('IASSIR18B', 'EQA9227C'),
            'EQA9230': ('IASSIR25', 'EQA9230'),
            'IASSIR01': ('IASSIR01', 'ASSIR01'),
            'IASSIR02': ('IASSIR02', 'ASSIR02'),
            'IASSIR05': ('IASSIR05', 'IASSIR05'),
            'IASSIR05B': ('IASSIR05B', 'IASSIR05B'),
            'IASSIR20B': ('IASSIR20B', 'ASSIR20B'),
            'IASSIR26': ('IASSIR26', 'ASSIR26'),
            'IASSIR26B': ('IASSIR26B', 'ASSIR26B'),
            'IASSIR27': ('IASSIR27', 'ASSIR27'),
            'IASSIR28': ('IASSIR28', 'ASSIR28'),
            }

def get_dextraccio():
    """."""

    global data_ext, data_ext_any, data_ext_mes, estat_dades

    sql = """
            SELECT
                data_ext
            FROM
                dextraccio
          """
    data_ext, = u.getOne(sql, "nodrizas")
    data_ext_any = data_ext.year
    data_ext_mes = data_ext.strftime("%m")
    estat_dades =  "D" if data_ext.month == 12 else "P"

def get_eqa():
    """."""

    indicadors_eqa = ('EQA9202', 'EQA9204', 'EQA9208', 'EQA9210', 'EQA9214', 'EQA9215', 'EQA9218', 'EQA9220', 'EQA9221', 'EQA9225', 'EQA9227B', 'EQA9227C', 'EQA9230')

    sql = """
            SELECT
                UP,
                INDICADOR,
                upper(CONC),
                SUM(N)
            FROM
                exp_khalix_up_ind
            WHERE
                indicador IN {}  
                AND comb = 'NOINSAT'
            GROUP BY
                UP,
                INDICADOR,
                CONC
            """.format(indicadors_eqa)
    for up_assir, indicador, analisi, n in u.getAll(sql, 'ass'):
        resultats[(up_assir, indicador)][analisi] += n

def get_iassir():
    """."""

    indicadors_iassir = ('IASSIR01', 'IASSIR02')

    sql = """
            SELECT
                UP,
                INDICADOR,
                SUM(NUM),
                SUM(DEN)
            FROM
                csl_khalix_upass_ind
            WHERE
                indicador IN {}
            GROUP BY
                UP,
                INDICADOR
            """.format(indicadors_iassir)
    for up_assir, indicador, num, den in u.getAll(sql, 'catsalut'):
        resultats[(up_assir, indicador)]['NUM'] += num
        resultats[(up_assir, indicador)]['DEN'] += den

def get_agas():
    """."""

    indicadors_agas_1 = ('AGACCIVE', 'AGACCEMB')

    sql = """
            SELECT
                INDICADOR,
                UP,
                UPPER(TIPUS),
                RECOMPTE
            FROM
                exp_khalix_ag_assir_acc
            WHERE
                indicador IN {}
            """.format(indicadors_agas_1)
    for indicador, up_assir, analisi, n in u.getAll(sql, 'altres'):
        resultats[(up_assir, indicador)][analisi] += n

    sql = """
            SELECT
                up,
                UPPER(analisi),
                val
            FROM
                AGASSDXVM
            """
    for up_assir, analisi, n in u.getAll(sql, 'altres'):
        resultats[(up_assir, 'AGASSDXVM')][analisi] += n

def get_cpr():
    """."""

    taules = {"cpr_indicador_as_02_up_assir",
              "cpr_indicador_as_10_up_assir",
              "cpr_indicador_as_16_up_assir",
              "cpr_indicador_cat0303b_up_assir",
              "cpr_indicador_iassir05_up_assir",
              "cpr_indicador_iassir05b_up_assir",
              "cpr_indicador_iassir20b_up_assir",
              "cpr_indicador_iassir26_up_assir",
              "cpr_indicador_iassir26b_up_assir",
              "cpr_indicador_iassir27_up_assir",
              "cpr_indicador_iassir28_up_assir",}
    
    for taula in taules:
        sql = """
                SELECT
                    indicador,
                    assir,
                    analisi,
                    n
                FROM
                    {_taula}
              """.format(_taula=taula)
        for indicador, up_assir, analisi, n in u.getAll(sql, 'assir'):
            resultats[(up_assir, indicador)][analisi] += n

def upload_table():
    """."""

    upload = [(up_assir, ind_noms[indicador][0], analisi, n) for (up_assir, indicador), dades in resultats.items() for analisi, n in dades.items()]

    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(upload, TB_NAME, DB_NAME)
    
def export_khalix():
    """."""
    
    sql = """
            SELECT DISTINCT
                INDICADOR,
                concat('A', 'periodo'),
                br_assir,
                upper(analisi),
                'NOCAT' ,
                'NOIMP',
                'DIM6SET',
                'N',
                N
            FROM
                {}.{} a,
                nodrizas.ass_centres b
            WHERE
                a.up_assir = b.up_assir
            GROUP BY
                indicador,
                br_assir,
                upper(analisi)
            """.format(DB_NAME, TB_NAME)
    u.exportKhalix(sql, LV_FILE)

def export_catsalut():
    """."""

    sql = """
        SELECT
            up_assir
        FROM
            ass_centres
          """
    ups = {up_assir for up_assir, in u.getAll(sql, "nodrizas")}
    
    upload = []
    
    for (up, indicador), dades in resultats.items():
        # inputem nom catsalut
        indicador = ind_noms[indicador][1]
        if up in ups:

            den, num = (dades.get(key, 0) for key in ['DEN', 'NUM'])
            
            res_t = float(num) / float(den) if den != 0 else num
            res = str(round(res_t, 4)).replace(".", ",")

            data_ext_format = data_ext.strftime("%d/%m/%Y")
            data_exportacio = datetime.date.today().strftime("%d/%m/%Y")

            upload.append(("{}{}".format(data_ext_any, data_ext_mes), indicador, up, up, res, int(num), int(den), data_ext_format, "", estat_dades, data_exportacio, 'ASSR'))

    cols = ("periode varchar(20)", "indicador varchar(20)", "up_ics varchar(5)",
            "up_rca varchar(5)", "resultat varchar(20)", "num number", "den number",
            "dades_calcul varchar(20)", "camp varchar(20)", "estat varchar(20)",
            "data_carrega varchar(20)", "tipus varchar(20)")
    with t.Database("exadata", "data") as exadata:
        print(len(upload))
        print(upload[0])
        exadata.create_table('contracte_catsalut', cols, remove=False)
        exadata.set_grants("select", 'contracte_catsalut', "DWSISAP_ROL", inheritance=False)
        exadata.execute("delete FROM DWSISAP.contracte_catsalut WHERE DADES_CALCUL = '{}' AND TIPUS = 'ASSR'".format(str(data_ext.strftime("%d/%m/%Y"))))
        exadata.list_to_table(upload, "contracte_catsalut")

    
    # file = FILE.format(data_ext.year, data_ext_mes)
    # u.writeCSV(u.tempFolder + file, upload, sep=";")

    # text = """Benvolguts,\r\n\r\nAdjuntem arxiu SIAC_ECAP SISAP ASSIR corresponent a {}/{}.\r\n\r\nSalutacions cordials,\r\n\r\nSISAP""".format(data_ext_mes, data_ext.year)
    
    # u.sendGeneral(
    #     'SISAP <sisap@gencat.cat>',
    #     'ecros@catsalut.cat',
    #     'sisap@gencat.cat',
    #     'Dades SIAC_ECAP SISAP ASSIR',
    #     text,
    #     u.tempFolder + file)
    # u.sshPutFile(file, "export", "catsalut", subfolder="ICS/")
    # u.sshChmod(file, "export", "catsalut", 0666, subfolder="ICS/")

    # # u.writeCSV(u.tempFolder + file,
    # #            [('Periode', 'Indicador', 'UP_ICS', 'UP', 'Resultat',
    # #              'Numerador', 'Denominador', 'Data Extraccio', 'Nulo',
    # #              'Estat', 'Data Execucio', 'sublinia')] + upload,
    # #            sep=";")
    
    # # u.sendGeneral(
    # #     'SISAP <sisap@gencat.cat>',
    # #     'roser.cantenys@catsalut.cat',
    # #     'roser.cantenys@catsalut.cat',
    # #     'Dades SIAC_ECAP SISAP ASSIR',
    # #     text,
    # #     u.tempFolder + file)

    text = """Benvolguts,\r\n\r\nJa podeu trobar disponibles a Exadata les dades de SIAC_ECAP SISAP ASSIR corresponent a {}/{} \r\n\r\nSalutacions cordials,
    \r\n\r\nSISAP""".format(data_ext_mes, data_ext_any)
    u.sendGeneral(
        'SISAP <sisap@gencat.cat>',
        'ecros@catsalut.cat',
        'sisap@gencat.cat',
        'Dades SIAC_ECAP SISAP ASSIR',
        text)


if __name__ == '__main__' and u.IS_MENSUAL:
    
    get_dextraccio();                                       print("get_dextraccio(): {}".format(datetime.datetime.now()))
    
    resultats = c.defaultdict(c.Counter)

    get_eqa();                                              print("get_eqa(): {}".format(datetime.datetime.now()))
    get_iassir();                                           print("get_iassir(): {}".format(datetime.datetime.now()))
    get_agas();                                             print("get_agas(): {}".format(datetime.datetime.now()))
    get_cpr();                                              print("get_cpr(): {}".format(datetime.datetime.now()))
    
    upload_table();                                         print("upload_table(): {}".format(datetime.datetime.now()))
    export_khalix();                                        print("export_khalix(): {}".format(datetime.datetime.now()))
    export_catsalut();                                      print("export_catsalut(): {}".format(datetime.datetime.now()))