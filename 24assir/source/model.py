# coding: latin1

"""
.
"""

import collections as c

import shared as s
import sisapUtils as u


PROCS = 8
INDICADORS = {"MODEL_PITS": {"umi": (3492, 4501, 4503, 4651, 4652),
                             "jobs": "laboratori1",
                             "codi": "cr_codi_prova_ics",
                             "where": "cr_data_reg between data_ext - interval 1 year and data_ext"},  # noqa
              "MODEL_ITS": {"umi": (3329, 999999999),
                            "jobs": "problemes",
                            "codi": "pr_cod_ps",
                            "where": "pr_dde between data_ext - interval 1 year and data_ext"},  # noqa
              "MODEL_P": {"umi": (4653, 999999999),
                          "jobs": "problemes",
                          "codi": "pr_cod_ps",
                          "where": "pr_dde < data_ext and (pr_dba = 0 or pr_dba > data_ext - interval 1 year)"},  # noqa
              "MODEL_IVEF": {"codis": ('FW3001', 'model'),
                             "jobs": "variables1",
                             "codi": "vu_cod_vs",
                             "where": "vu_dat_act between data_ext - interval 1 year and data_ext and vu_val = 0"}}  # noqa


class Model(object):
    """."""

    def __init__(self):
        """."""
        self.get_codis()
        self.get_queries()
        self.get_jobs()
        self.get_poblacio()
        self.get_data()
        self.upload_data()
        self.get_embaras()
        self.get_comunitaria()

    def get_codis(self):
        """."""
        self.codis = c.defaultdict(set)
        sql = """select c.descripcio, b.codi
                 from indicador_concepte_codis a
                 inner join indicador_codi b on a.codi_id = b.id
                 inner join indicador_origen c on b.origen_id = c.id
                 where a.concepte_id in {}"""
        db = ("umi", "x0001")
        for ind, info in INDICADORS.items():
            if "umi" in info:
                for taula, codi in u.getAll(sql.format(info["umi"]), db):
                    self.codis[ind].add(codi)
            else:
                self.codis[ind] = info["codis"]

    def get_queries(self):
        """."""
        self.queries = {}
        for ind, info in INDICADORS.items():
            sql = "select id_cip, '{}' \
                   from {{}}, nodrizas.dextraccio \
                   where {} in {} and {}".format(ind,
                                                 info["codi"],
                                                 tuple(self.codis[ind]),
                                                 info["where"])
            self.queries[ind] = sql

    def get_jobs(self):
        """."""
        jobs = []
        for ind, info in INDICADORS.items():
            sql = "show create table {}".format(info["jobs"])
            for table in u.getOne(sql, "import")[1].split("UNION=(")[1][:-1].split(","):  # noqa
                n = u.getTableCount(table[1:-1], "import")
                jobs.append((self.queries[ind].format(table), n))
        self.jobs = [k for (k, v)
                     in sorted(jobs, key=lambda x: x[1], reverse=True)]

    def get_poblacio(self):
        """."""
        self.poblacio = c.defaultdict(set)
        sql = "select id, assir, eap, 9, sexe, edat, espe \
               from {}".format(s.VISITES)
        for row in u.getAll(sql, s.DATABASE):
            self.poblacio[row[0]].add(row[1:-1] + ("TOT",))
            self.poblacio[row[0]].add(row[1:-1] + (s.ESPECIALITATS.get(row[-1], "ALT"),))  # noqa

    def get_data(self):
        """."""
        self.dades = c.defaultdict(set)
        for worker in u.multiprocess(self.download_it, self.jobs, PROCS):
            for k, pacs in worker:
                self.dades[k] |= pacs

    def download_it(self, sql):
        """."""
        dades = c.defaultdict(set)
        for id, ind in u.getAll(sql, "import"):
            for pob in self.poblacio[id]:
                k = pob + (ind, "PERSONES")
                dades[k].add(id)
        return dades.items()

    def upload_data(self):
        """."""
        delete = "delete {} where indicador in {}".format(s.QC_TABLE, tuple(INDICADORS.keys()))  # noqa
        u.execute(delete, s.QC_DATABASE)
        upload = [k + (len(pacs),) for k, pacs in self.dades.items()]
        u.listToTable(upload, s.QC_TABLE, s.QC_DATABASE)

    def get_embaras(self):
        """."""
        homol = {'': "CE", "Baix risc": "EN", "Normal": "EN",
                 "Risc alt": "ERA", "Risc mitj�": "EN", "Risc molt alt": "ERA",
                 "Sense risc": "EN"}
        sql = "select id_cip, emb_risc \
               from embaras, nodrizas.dextraccio \
               where emb_dur between data_ext - interval 1 year and data_ext"
        riscos = c.defaultdict(set)
        for id, risc in u.getAll(sql, "import"):
            if id in self.poblacio:
                riscos[id].add(homol[risc])
        embaras = c.Counter()
        for id, riscs in riscos.items():
            keys = []
            for pob in self.poblacio[id]:
                keys.append(pob + ("MODEL_CE", "PERSONES"))
                if "ERA" in riscs:
                    keys.append(pob + ("MODEL_ERA", "PERSONES"))
                elif "EN" in riscs:
                    keys.append(pob + ("MODEL_EN", "PERSONES"))
            for key in keys:
                embaras[key] += 1
        delete = "delete {} where indicador in ('MODEL_CE', 'MODEL_ERA', 'MODEL_EN')".format(s.QC_TABLE)  # noqa
        u.execute(delete, s.QC_DATABASE)
        upload = [k + (v,) for k, v in embaras.items()]
        u.listToTable(upload, s.QC_TABLE, s.QC_DATABASE)

    def get_comunitaria(self):
        """."""
        sql = """select codi_sector, grup_num,
                    if(grup_titol like '%NAIXEMENT%', 'MODEL_PN',
                       if(grup_titol like '%POST%PART%', 'MODEL_PG',
                          'MODEL_COMG'))
                 from grupal4, nodrizas.dextraccio
                 where grup_activitat in ('SS','MI') and
                       grup_validada = 'S' and
                       grup_data_ini between data_ext - interval 1 year and
                                             data_ext"""
        cataleg = {row[:2]: row[2] for row in u.getAll(sql, "import")}
        comunitaria = c.defaultdict(set)
        sql = "select id_cip, codi_sector, pagr_num_grup from grupal3"
        for id, sec, num in u.getAll(sql, "import"):
            grup = (sec, num)
            if grup in cataleg:
                ind = cataleg[grup]
                for pob in self.poblacio[id]:
                    key = pob + (ind, "PERSONES")
                    comunitaria[key].add(id)
        delete = "delete {} where indicador in ('MODEL_PN', 'MODEL_PG', 'MODEL_COMG')".format(s.QC_TABLE)  # noqa
        u.execute(delete, s.QC_DATABASE)
        upload = [k + (len(pacs),) for k, pacs in comunitaria.items()]
        u.listToTable(upload, s.QC_TABLE, s.QC_DATABASE)


if __name__ == "__main__":
    Model()
