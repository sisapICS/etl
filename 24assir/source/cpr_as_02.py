# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime

# Definici� de l'indicador
INDICADOR = "AS_02"
TB_NAME = "cpr_indicador_{}".format(INDICADOR)
DB_NAME = 'assir'
COL_NAMES = "(indicador varchar(10), assir varchar(10), analisi varchar(10), n int)"
COL_NAMES_MST = "(indicador varchar(10), id_cip_sec int, assir varchar(10), num int)"

def get_dextraccio():
    """
    Obtenir la data d'extracci� i calcular la data fa un any.

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any

    sql = """
        SELECT
            data_ext,
            DATE_ADD(data_ext, INTERVAL -1 YEAR)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obtenir la poblaci� assignada al servei ASSIR.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les persones assignades.
    """
    global poblacio_ass
    poblacio_ass = c.defaultdict(lambda: c.defaultdict(set))

    for tipus_up in ("up_assir", "up"):
        sql = """
            SELECT
                id_cip_sec,
                visi_{}
            FROM
                ass_imputacio_{}
        """.format(tipus_up, tipus_up)
        for id_cip_sec, up in u.getAll(sql, 'nodrizas'):
            poblacio_ass[id_cip_sec][tipus_up].add(up)

def get_visites_ass():
    """
    Obtenir les visites amb les dates de petici� i realitzaci�.

    Defineix:
        - 'visites_ass': Diccionari amb el temps d'espera per visita.
    """
    global visites_ass
    visites_ass = {}

    sql = """
        SELECT
            id_cip_sec,
            visi_dia_peticio,
            visi_data_visita
        FROM
            ass_visites
        WHERE
            visi_dia_peticio IS NOT NULL
            AND visi_data_visita IS NOT NULL
    """
    for id_cip_sec, data_peti, data_visi in u.getAll(sql, 'nodrizas'):
        dies_espera = (data_visi - data_peti).days
        visites_ass[(id_cip_sec, data_visi)] = dies_espera

def get_denominador():
    """
    Calcular el denominador: Nombre total de casos de IVE farmacol�gica realitzats.

    Defineix:
        - 'denominador': Conjunt d'identificadors (id_cip_sec, data_IVE) de casos v�lids.
    """
    global denominador
    denominador = set()

    sql = """
        SELECT
            id_cip_sec,
            dat
        FROM
            ass_variables
        WHERE
            agrupador = 1082 AND val_num = 1        -- IVE farmacol�gica realitzat
            AND dat BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
    """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    for id_cip_sec, data_IVE in u.getAll(sql, "nodrizas"):
        denominador.add((id_cip_sec, data_IVE))

def get_numerador():
    """
    Calcular el numerador: Nombre total de casos de IVE farmacol�gica realitzats amb temps d'espera ? 7 dies.

    Defineix:
        - 'numerador': Conjunt d'identificadors que compleixen el criteri d'accessibilitat.
    """
    global numerador
    numerador = set()

    for id_cip_sec, data_IVE in denominador:
        if visites_ass.get((id_cip_sec, data_IVE), 9999) <= 7:
            numerador.add((id_cip_sec, data_IVE))

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """

    global upload, upload_mst
    upload, upload_mst = c.defaultdict(list), c.defaultdict(list)
    resultat = c.defaultdict(lambda: c.defaultdict(set))

    for id_cip_sec, data_IVE in denominador:
        for tipus_up, ups in poblacio_ass[id_cip_sec].items():
            num = 1 if (id_cip_sec, data_IVE) in numerador else 0
            for up in ups:
                resultat["DEN"][(tipus_up, INDICADOR, up)].add((id_cip_sec, data_IVE))
                if num:
                    resultat["NUM"][(tipus_up, INDICADOR, up)].add((id_cip_sec, data_IVE))

    # Dades per UP
    for analisi in resultat:
        for (tipus_up, indicador, up), pacients in resultat[analisi].items():
            upload[tipus_up].append((indicador, up, analisi, len(pacients)))

    # Dades per pacient (mst)
    for (tipus_up, indicador, up), pacients_den in resultat["DEN"].items():
        pacients_num = resultat["NUM"].get((tipus_up, indicador, up), set())
        for (id_cip_sec, data_IVE) in pacients_den:
            num = 1 if (id_cip_sec, data_IVE) in pacients_num else 0
            upload_mst[tipus_up].append((indicador, id_cip_sec, up, num))

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """

    # Taules per UP
    u.createTable('{}_up'.format(TB_NAME), COL_NAMES, 'assir', rm=True)
    u.listToTable(upload["up"], '{}_up'.format(TB_NAME), 'assir')
    u.createTable('{}_up_assir'.format(TB_NAME), COL_NAMES, 'assir', rm=True)
    u.listToTable(upload["up_assir"], '{}_up_assir'.format(TB_NAME), 'assir')

    # Taules per pacient (mst)
    u.createTable('mst_{}_up'.format(TB_NAME), COL_NAMES_MST, 'assir', rm=True)
    u.listToTable(upload_mst["up"], 'mst_{}_up'.format(TB_NAME), 'assir')
    u.createTable('mst_{}_up_assir'.format(TB_NAME), COL_NAMES_MST, 'assir', rm=True)
    u.listToTable(upload_mst["up_assir"], 'mst_{}_up_assir'.format(TB_NAME), 'assir')

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_visites_ass();          print("get_visites_ass(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))