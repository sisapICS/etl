# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime

"""
Indicador basat en EQA9214
"""

# Definici� de l'indicador
INDICADOR = "AS_10"
TB_NAME = "cpr_indicador_{}".format(INDICADOR)
DB_NAME = 'assir'
COL_NAMES = "(indicador varchar(10), assir varchar(10), analisi varchar(10), n int)"
COL_NAMES_MST = "(indicador varchar(10), id_cip_sec int, assir varchar(10), num int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants.

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys22mesos': Fa 22 mesos des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys22mesos

    sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
            date_add(date_add(data_ext, INTERVAL -22 MONTH), INTERVAL + 1 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys22mesos = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� (dones i homes) assignada al servei ASSIR.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les persones (dones i homes) assignades.
    """

    global poblacio_ass;            poblacio_ass = c.defaultdict(lambda: c.defaultdict(set))

    for tipus_up in ("up_assir", "up"):
        sql = """
                SELECT
                    id_cip_sec,
                    visi_{}
                FROM
                    ass_imputacio_{}_mixta
            """.format(tipus_up, tipus_up)
        for id_cip_sec, up in u.getAll(sql, 'nodrizas'):
            poblacio_ass[id_cip_sec][tipus_up].add(up)

def get_dx_ITS():
    """
    Obt� els diagn�stics d'infeccions de transmissi� sexual (ITS) realitzats en l'�ltim any.

    Defineix:
        - 'dx_ITS': Diccionari amb identificadors i dates de diagn�stic ITS.
    """
    global dx_ITS
    dx_ITS = c.defaultdict(set)

    sql = """
            SELECT
                id_cip_sec,
                dde
            FROM
                eqa_problemes_incid
            WHERE
                ps IN (406, 680) -- Agrupadors ITS
                AND dde BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
          """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    for id_cip_sec, data_dx in u.getAll(sql, "nodrizas"):
        if id_cip_sec in poblacio_ass:
            dx_ITS[id_cip_sec].add(data_dx)

def get_denominador():
    """
    Calcula el denominador de l'indicador, que inclou:
    - Dones amb un nou diagn�stic d'ITS en l'�ltim any que han estat visitades a l'ASSIR despr�s del diagn�stic.

    Defineix:
        - 'denominador': Conjunt d'identificadors de dones amb diagn�stic ITS i visita a l'ASSIR posterior.
    """
    global denominador
    denominador = set()

    sql = """
            SELECT
                id_cip_sec,
                visi_data_visita
            FROM
                ass_visites
            WHERE
                visi_data_visita BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
          """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    for id_cip_sec, data_visita in u.getAll(sql, "nodrizas"):
        if id_cip_sec in dx_ITS and any(data_dx <= data_visita for data_dx in dx_ITS[id_cip_sec]):
            denominador.add(id_cip_sec)

def get_exclusions():
    """
    Calcula les exclusions per a l'indicador:
    - Dones amb episodis d'embar�s actiu o puerperi en els �ltims 90 dies.

    Defineix:
        - 'exclusions': Conjunt d'identificadors de dones excloses.
    """
    global exclusions
    exclusions = set()

    sql = """
        SELECT
            id_cip_sec,
            inici,
            fi
        FROM
            ass_embaras
        WHERE
            fi BETWEEN DATE'{_data_ext_menys22mesos}' AND DATE'{_data_ext}'
    """.format(_data_ext_menys22mesos=data_ext_menys22mesos, _data_ext=data_ext)
    for id_cip_sec, inici, fi in u.getAll(sql, "nodrizas"):
        if id_cip_sec in denominador and any(inici <= data_dx <= fi for data_dx in dx_ITS[id_cip_sec]):
            exclusions.add(id_cip_sec)

def get_numerador():
    """
    Calcula el numerador de l'indicador, que inclou:
    - Dones amb serologia VIH realitzada dins dels 6 mesos posteriors i/o un mes anterior al diagn�stic d'ITS.

    Defineix:
        - 'numerador': Conjunt d'identificadors amb serologia VIH realitzada.
    """
    global numerador
    numerador = set()

    sql = """
        SELECT
            id_cip_sec,
            dat
        FROM
            ass_variables
        WHERE
            ((agrupador = 388) OR (agrupador = 397) OR (agrupador = 398 AND val_num = 1))   -- Agrupadors per a serologia VIH
            AND dat BETWEEN DATE'{_data_ext_menys22mesos}' AND DATE'{_data_ext}'
        UNION
        SELECT
            id_cip_sec,
            dde
        FROM
            eqa_problemes
        WHERE
            ps = 101    -- Agrupador per a diagn�stic de VIH
            AND dde BETWEEN DATE'{_data_ext_menys22mesos}' AND DATE'{_data_ext}'
    """.format(_data_ext_menys22mesos=data_ext_menys22mesos, _data_ext=data_ext)
    for id_cip_sec, data_vih in u.getAll(sql, "nodrizas"):
        if id_cip_sec in denominador and any(data_dx - datetime.timedelta(days=30) <= data_vih <= data_dx + datetime.timedelta(days=180) for data_dx in dx_ITS[id_cip_sec]):
            numerador.add(id_cip_sec)

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """

    global upload, upload_mst
    upload, upload_mst = c.defaultdict(list), c.defaultdict(list)
    resultat = c.defaultdict(lambda: c.defaultdict(set))

    for id_cip_sec in denominador:
        for tipus_up, ups in poblacio_ass[id_cip_sec].items():
            num = id_cip_sec in numerador
            for up in ups:
                resultat["DEN"][(tipus_up, INDICADOR, up)].add(id_cip_sec)
                if num:
                    resultat["NUM"][(tipus_up, INDICADOR, up)].add(id_cip_sec)

    # Dades per UP
    for analisi in resultat:
        for (tipus_up, indicador, up), pacients in resultat[analisi].items():
            upload[tipus_up].append((indicador, up, analisi, len(pacients)))

    # Dades per pacient (mst)
    for (tipus_up, indicador, up), pacients_den in resultat["DEN"].items():
        pacients_num = resultat["NUM"].get((tipus_up, indicador, up), set())
        for id_cip_sec in pacients_den:
            num = 1 if id_cip_sec in pacients_num else 0
            upload_mst[tipus_up].append((indicador, id_cip_sec, up, num))

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """

    # Taules per UP
    u.createTable('{}_up'.format(TB_NAME), COL_NAMES, 'assir', rm=True)
    u.listToTable(upload["up"], '{}_up'.format(TB_NAME), 'assir')
    u.createTable('{}_up_assir'.format(TB_NAME), COL_NAMES, 'assir', rm=True)
    u.listToTable(upload["up_assir"], '{}_up_assir'.format(TB_NAME), 'assir')

    # Taules per pacient (mst)
    u.createTable('mst_{}_up'.format(TB_NAME), COL_NAMES_MST, 'assir', rm=True)
    u.listToTable(upload_mst["up"], 'mst_{}_up'.format(TB_NAME), 'assir')
    u.createTable('mst_{}_up_assir'.format(TB_NAME), COL_NAMES_MST, 'assir', rm=True)
    u.listToTable(upload_mst["up_assir"], 'mst_{}_up_assir'.format(TB_NAME), 'assir')

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_dx_ITS();               print("get_dx_ITS(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_exclusions();           print("get_exclusions(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))