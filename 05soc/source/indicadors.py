# -*- coding: latin1 -*-

"""
PER RETROACTIUS:
  - Llen�ar 02nod/source/ts_visites.sql
"""

import collections as c
import dateutil
import dateutil.relativedelta

import sisapUtils as u

TABLE = "mst_indicadors_pacient"
DATABASE = "social"
ECAP_PAC = "exp_ecap_pacient"

agrupadors_diagnostics_discapacitat = (7, 61, 301, 302, 303, 304, 305, 306)
agrupador_diagnostic_demencia = (86,)
agrupador_diagnostic_viu_sol = (107,)
agrupadors_diagnostics_ATDOM = (45,)
agrupadors_diagnostics_infants_adolescents_amb_risc = (868,)
agrupadors_diagnostics_dependencia = (98,)
agrupador_escala_suport_social = 825

codis_escala_suport_social = str(tuple(codi for codi, in u.getAll("SELECT criteri_codi FROM nodrizas.eqa_criteris WHERE agrupador = 825", "nodrizas")))
codis_actuacions_treball_social = "('AZ1001','AZ1002','AZ1003','VZ1003')"
codis_activitats_treball_social = 'VZ101'
codis_escala_gijon = "('VSEVS', 'VZ3005')"
codis_pacients_cronics_complex = 'ER0001'

# Obtenim la data d'extracci� de les dades

sql = """
        SELECT
            data_ext
        FROM
            dextraccio
        """
data_ext, = u.getOne(sql, "nodrizas")
data_ext_menys3anys = data_ext - dateutil.relativedelta.relativedelta(months=36)
data_ext_menys2anys = data_ext - dateutil.relativedelta.relativedelta(months=24)
data_ext_menys1any = data_ext - dateutil.relativedelta.relativedelta(months=12)


class EQATreballSocial():
    """."""

    def __init__(self):
        """."""

        self.get_centres();                                                     print("get_centres(): executat")
        self.get_poblacio();                                                    print("get_poblacio(): executat")
        self.get_diagnostics();                                                 print("get_diagnostics(): executat")
        self.get_xml();                                                         print("get_xml(): executat")
        self.get_variables();                                                   print("get_variables(): executat")
        self.get_activitats();                                                  print("get_activitats(): executat")
        self.get_pcc();                                                         print("get_pcc(): executat")
        self.get_gma4();                                                        print("get_gma4(): executat")
        self.get_visites_domiciliaries_realitzades_treballadors_social();       print("get_visites_domiciliaries_realitzades_treballadors_social(): executat")
        self.get_dbs();                                                         print("get_dbs(): executat")
        self.get_indicadors();                                                  print("get_indicadors(): executat")

        self.upload_master();                                                   print("upload_master(): executat")

    def get_centres(self):
        """."""

        sql = """
                SELECT
                    scs_codi,
                    ics_codi
                FROM
                    cat_centres
              """
        self.centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}

    def get_poblacio(self):
        """
        entitats: (br, br + 'M' + uba, br + 'I' + ubainf
        """

        self.conversor_hash2idcipsec, self.conversor_idcipsec2hash = {}, {}

        sql = """
                SELECT
                    id_cip_sec,
                    codi_sector,
                    up,
                    uba,
                    edat,
                    sexe,
                    ates,
                    institucionalitzat,
                    piramide
                FROM
                    assignada_tot
              """
        self.poblacio = {id_cip_sec: (codi_sector, up, self.centres.get(up), uba, piramide, edat, sexe, ates, institucionalitzat) 
                        for id_cip_sec, codi_sector, up, uba, edat, sexe, ates, institucionalitzat, piramide in u.getAll(sql, "nodrizas") 
                        if up in self.centres}

        sql = """
                SELECT
                    id_cip_sec,
                    hash_d
                FROM
                    u11
              """
        for id_cip_sec, hash_d in u.getAll(sql, "import"):
            if id_cip_sec in self.poblacio:
                self.conversor_hash2idcipsec[hash_d] = id_cip_sec
                self.conversor_idcipsec2hash[id_cip_sec] = hash_d

    def get_diagnostics(self):
        """ Ens quedem amb la primera de les dates d'un dx lligat amb discapacitats """

        self.dates_diagnostics_discapacitat, self.dates_diagnostics_demencia, self.dates_diagnostics_ATDOM = c.defaultdict(set), c.defaultdict(set), c.defaultdict(set)
        self.viu_sol, self.infants_adolescents_amb_risc, self.dependencia = set(), set(), set()

        agrupadors_diagnostics = agrupadors_diagnostics_discapacitat
        agrupadors_diagnostics += agrupador_diagnostic_demencia
        agrupadors_diagnostics += agrupador_diagnostic_viu_sol
        agrupadors_diagnostics += agrupadors_diagnostics_ATDOM
        agrupadors_diagnostics += agrupadors_diagnostics_infants_adolescents_amb_risc
        agrupadors_diagnostics += agrupadors_diagnostics_dependencia

        sql = """
                SELECT
                    id_cip_sec,
                    ps,
                    dde
                FROM
                    eqa_problemes
                WHERE
                    ps IN {}
                    AND dde <= '{}'
              """.format(agrupadors_diagnostics, data_ext)
        for id_cip_sec, agrupador, data_diagnostic in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                if agrupador in agrupador_diagnostic_demencia:
                    self.dates_diagnostics_demencia[id_cip_sec].add(data_diagnostic)
                elif agrupador in agrupadors_diagnostics_discapacitat:
                    self.dates_diagnostics_discapacitat[id_cip_sec].add(data_diagnostic)
                elif agrupador in agrupadors_diagnostics_ATDOM:
                    self.dates_diagnostics_ATDOM[id_cip_sec].add(data_diagnostic)
                elif agrupador in agrupador_diagnostic_viu_sol:
                    self.viu_sol.add(id_cip_sec)
                elif agrupador in agrupadors_diagnostics_infants_adolescents_amb_risc:
                    self.infants_adolescents_amb_risc.add(id_cip_sec)
                elif agrupador in agrupadors_diagnostics_dependencia:
                    self.dependencia.add(id_cip_sec)

    def get_xml(self):
        """Obt� les dades dels formularis XML."""

        registres_viu_sol, registres_valoracions_socials = c.defaultdict(set), c.defaultdict(set)

        for sub_registres_viu_sol, sub_registres_valoracions_socials in u.multiprocess(sub_get_xml, u.getSubTables("xml_detall"), 4):
            for (id_cip_sec, data_alta, valor) in sub_registres_viu_sol:
                if id_cip_sec in self.poblacio:
                    registres_viu_sol[id_cip_sec].add((data_alta, valor))
            for (id_cip_sec, data_valoracio_social) in sub_registres_valoracions_socials:
                if id_cip_sec in self.poblacio:
                    registres_valoracions_socials[id_cip_sec].add(data_valoracio_social)
        
        self.viu_sol.update({id_cip_sec for id_cip_sec, registres in registres_viu_sol.items() if max(registres)[1] == '0'})
        self.dates_valoracions_socials = {id_cip_sec: max(dates) for id_cip_sec, dates in registres_valoracions_socials.items()}

    def get_variables(self):
        """ . """

        self.dates_escala_gijon, self.dates_actuacions_treball_social, self.dates_escala_suport_social = c.defaultdict(set), c.defaultdict(set), c.defaultdict(set)

        for particio in u.multiprocess(get_variables_single, u.getSubTables("variables"), 8):
            for id_cip_sec, vu_cod_vs, data_vs in particio:
                if id_cip_sec in self.poblacio:
                    if vu_cod_vs in codis_actuacions_treball_social:
                        self.dates_actuacions_treball_social[id_cip_sec].add(data_vs)
                    elif vu_cod_vs in codis_escala_gijon:
                        self.dates_escala_gijon[id_cip_sec].add(data_vs)
                    elif vu_cod_vs in codis_escala_suport_social:
                        self.dates_escala_suport_social[id_cip_sec].add(data_vs)


    def get_activitats(self):
        for particio in u.multiprocess(get_activitats_single, u.getSubTables("activitats"), 8):
            for id_cip_sec, vu_cod_vs, data_vs in particio:
                if id_cip_sec in self.poblacio and vu_cod_vs in codis_activitats_treball_social:
                    self.dates_actuacions_treball_social[id_cip_sec].add(data_vs)

    def get_pcc(self):
        """ . """
        
        sql = """
                SELECT
                    id_cip_sec 
                FROM
                    estats
                WHERE
                    es_cod = '{}'
                    AND es_dde <= '{}'
              """.format(codis_pacients_cronics_complex, data_ext)
        self.pcc = {id_cip_sec for id_cip_sec, in u.getAll(sql, "import") if id_cip_sec in self.poblacio}


    def get_gma4(self):
        """ . """
        
        sql = """
                SELECT
                    id_cip_sec 
                FROM
                    gma
                WHERE
                    gma_pniv = 4
              """
        self.gma4 = {id_cip_sec for id_cip_sec, in u.getAll(sql, "import") if id_cip_sec in self.poblacio}

    def get_visites_domiciliaries_realitzades_treballadors_social(self):
        """ . """
        
        sql = """
                SELECT
                    id_cip_sec
                FROM
                    ts_visites
                WHERE
                    domi = 1
              """
        self.visitats_domiciliariament_per_treballadors_social = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") 
                                                                  if id_cip_sec in self.poblacio}

    def get_dbs(self):
        """."""

        self.dbs = c.defaultdict(dict)

        sql = """
                SELECT
                    c_cip,
                    CASE
                        WHEN ps_demencia_data IS NOT NULL THEN 1
                        ELSE 0
                    END demencia,
                    CASE
                        WHEN c_gma_codi LIKE '4%' THEN 1
                        ELSE 0
                    END gma4,
                    CASE
                        WHEN v_barthel_valor <= 20 THEN '<20'
                        WHEN v_barthel_valor BETWEEN 21 AND 60 THEN '21 a 60'
                        ELSE '>60'
                    END barthel
                FROM
                    dbs
              """
        self.dbs = {self.conversor_hash2idcipsec.get(hash): {"demencia": demencia, "gma4": gma4, "barthel": barthel}
                    for hash, demencia, gma4, barthel in u.getAll(sql, "redics")}


    def get_indicadors(self):
        """ . """

        self.indicadors = []
        self.pacients = {}

        # 001

        sql = """
                SELECT
                    id_cip_sec,
                    den,
                    num
                FROM
                    mst_indicadors_pacient
                WHERE
                    indicador = 'EQA1202'
                    AND excl = 0
              """
        for id_cip_sec, den, compleix in u.getAll(sql, "pedia"):
            codi_sector, up, br, uba, piramide, edat, sexe, ates, institucionalitzat = self.poblacio[id_cip_sec]
            hash_d = self.conversor_idcipsec2hash[id_cip_sec]
            exclos = 1 if not ates else 0
            self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9301', compleix, 0))
            if not institucionalitzat:
                self.pacients[(id_cip_sec, up, "EQA9301", exclos, hash_d, codi_sector)] = compleix

        # 002

        pcc_i_gma4 = self.pcc & self.gma4

        for id_cip_sec in pcc_i_gma4:
            codi_sector, up, br, uba, piramide, edat, sexe, ates, institucionalitzat = self.poblacio[id_cip_sec]
            if edat > 14:
                hash_d = self.conversor_idcipsec2hash[id_cip_sec]
                exclos = 1 if not ates else 0
                compleix = 0
                if id_cip_sec in self.dates_valoracions_socials:
                    data_ultima_valoracio_social = self.dates_valoracions_socials[id_cip_sec]
                    if data_ext_menys2anys < data_ultima_valoracio_social:
                        compleix = 1
                self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9302', compleix, 0))
                if not institucionalitzat:
                    self.pacients[(id_cip_sec, up, "EQA9302", exclos, hash_d, codi_sector)] = compleix                

        # 003

        for id_cip_sec in self.dates_diagnostics_demencia:
            codi_sector, up, br, uba, piramide, edat, sexe, ates, institucionalitzat = self.poblacio[id_cip_sec]
            data_ultima_diagnostic_demencia = max(self.dates_diagnostics_demencia[id_cip_sec])
            if data_ext_menys1any <= data_ultima_diagnostic_demencia:
                hash_d = self.conversor_idcipsec2hash[id_cip_sec]
                exclos = 1 if not ates else 0
                compleix = 0
                if id_cip_sec in self.dates_valoracions_socials and id_cip_sec in self.dates_actuacions_treball_social and id_cip_sec in self.dates_escala_suport_social:
                    data_ultima_valoracio_social = self.dates_valoracions_socials[id_cip_sec]
                    data_ultima_actuacio_treball_social = max(self.dates_actuacions_treball_social[id_cip_sec])
                    data_ultima_escala_suport_social = max(self.dates_escala_suport_social[id_cip_sec])      
                    if data_ext_menys1any <= min(data_ultima_valoracio_social, data_ultima_actuacio_treball_social, data_ultima_escala_suport_social):
                        compleix = 1
                self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9303', compleix, 0))
                if not institucionalitzat:
                    self.pacients[(id_cip_sec, up, "EQA9303", exclos, hash_d, codi_sector)] = compleix

        # 004

        sql = """
                SELECT
                    id_cip_sec,
                    den,
                    num
                FROM
                    mst_indicadors_pacient
                WHERE
                    ind_codi = 'EQA0408A'
                    AND excl = 0
              """
        for id_cip_sec, den, compleix in u.getAll(sql, "eqa_ind"):
            codi_sector, up, br, uba, piramide, edat, sexe, ates, institucionalitzat = self.poblacio[id_cip_sec]
            hash_d = self.conversor_idcipsec2hash[id_cip_sec]
            exclos = 1 if not ates else 0
            self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9304', compleix, 0))
            if not institucionalitzat:
                self.pacients[(id_cip_sec, up, "EQA9304", exclos, hash_d, codi_sector)] = compleix
            if id_cip_sec in self.dbs:
                if (self.dbs[id_cip_sec]["barthel"] == "<20") or (self.dbs[id_cip_sec]["barthel"] == '21 a 60' and self.dbs[id_cip_sec]["demencia"]) or (self.dbs[id_cip_sec]["barthel"] == '21 a 60' and not self.dbs[id_cip_sec]["demencia"] and self.dbs[id_cip_sec]["gma4"]):
                    self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9304A', compleix, 0))
                    if not institucionalitzat:
                        self.pacients[(id_cip_sec, up, "EQA9304A", exclos, hash_d, codi_sector)] = compleix
                if (self.dbs[id_cip_sec]["barthel"] == "21 a 60" and not self.dbs[id_cip_sec]["demencia"] and not self.dbs[id_cip_sec]["gma4"]) or (self.dbs[id_cip_sec]["barthel"] == ">60" and self.dbs[id_cip_sec]["demencia"]) or (self.dbs[id_cip_sec]["barthel"] == ">60" and not self.dbs[id_cip_sec]["demencia"] and self.dbs[id_cip_sec]["gma4"]):
                    self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9304B', compleix, 0))
                    if not institucionalitzat:
                        self.pacients[(id_cip_sec, up, "EQA9304B", exclos, hash_d, codi_sector)] = compleix
                if (self.dbs[id_cip_sec]["barthel"] == ">60" and not self.dbs[id_cip_sec]["demencia"] and not self.dbs[id_cip_sec]["gma4"]):
                    self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9304C', compleix, 0))
                    if not institucionalitzat:
                        self.pacients[(id_cip_sec, up, "EQA9304C", exclos, hash_d, codi_sector)] = compleix

        # 005
        
        for id_cip_sec in self.dates_diagnostics_ATDOM:
            codi_sector, up, br, uba, piramide, edat, sexe, ates, institucionalitzat = self.poblacio[id_cip_sec]
            data_ultim_diagnostic_ATDOM = max(self.dates_diagnostics_ATDOM[id_cip_sec])            
            if edat >= 65 and data_ext_menys1any <= data_ultim_diagnostic_ATDOM:
                hash_d = self.conversor_idcipsec2hash[id_cip_sec]
                exclos = 1 if not ates else 0
                compleix = 1 if id_cip_sec in self.dates_valoracions_socials and id_cip_sec in self.dates_escala_gijon and id_cip_sec in self.visitats_domiciliariament_per_treballadors_social else 0
                self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9305', compleix, 0))
                if not institucionalitzat:
                    self.pacients[(id_cip_sec, up, "EQA9305", exclos, hash_d, codi_sector)] = compleix

        # 006

        for id_cip_sec in self.infants_adolescents_amb_risc:
            codi_sector, up, br, uba, piramide, edat, sexe, ates, institucionalitzat = self.poblacio[id_cip_sec]
            if edat <= 14:
                hash_d = self.conversor_idcipsec2hash[id_cip_sec]
                exclos = 1 if not ates else 0
                compleix = 1 if id_cip_sec in self.dates_valoracions_socials or id_cip_sec in self.dates_actuacions_treball_social else 0
                self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9306', compleix, 0))
                if not institucionalitzat:
                    self.pacients[(id_cip_sec, up, "EQA9306", exclos, hash_d, codi_sector)] = compleix

        # 007
        
        for id_cip_sec in self.infants_adolescents_amb_risc:
            codi_sector, up, br, uba, piramide, edat, sexe, ates, institucionalitzat = self.poblacio[id_cip_sec]
            if edat in range(15,18): 
                hash_d = self.conversor_idcipsec2hash[id_cip_sec]
                exclos = 1 if not ates else 0
                compleix = 1 if id_cip_sec in self.dates_valoracions_socials or id_cip_sec in self.dates_actuacions_treball_social else 0
                self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9307', compleix, 0))
                if not institucionalitzat:
                    self.pacients[(id_cip_sec, up, "EQA9307", exclos, hash_d, codi_sector)] = compleix

        # 008

        sql = """
                SELECT
                    id_cip_sec,
                    den,
                    num
                FROM
                    mst_indicadors_pacient
                WHERE
                    ind_codi = 'EQA0409A'
                    AND excl = 0
              """
        for id_cip_sec, den, compleix in u.getAll(sql, "eqa_ind"):
            codi_sector, up, br, uba, piramide, edat, sexe, ates, institucionalitzat = self.poblacio[id_cip_sec]
            hash_d = self.conversor_idcipsec2hash[id_cip_sec]
            exclos = 1 if not ates else 0
            self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9308', compleix, 0))
            if not institucionalitzat:
                self.pacients[(id_cip_sec, up, "EQA9308", exclos, hash_d, codi_sector)] = compleix
            if id_cip_sec in self.dbs:
                if (self.dbs[id_cip_sec]["barthel"] == "<20") or (self.dbs[id_cip_sec]["barthel"] == '21 a 60' and self.dbs[id_cip_sec]["demencia"]) or (self.dbs[id_cip_sec]["barthel"] == '21 a 60' and not self.dbs[id_cip_sec]["demencia"] and self.dbs[id_cip_sec]["gma4"]):
                    self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9308A', compleix, 0))
                    if not institucionalitzat:
                        self.pacients[(id_cip_sec, up, "EQA9308A", exclos, hash_d, codi_sector)] = compleix
                if (self.dbs[id_cip_sec]["barthel"] == "21 a 60" and not self.dbs[id_cip_sec]["demencia"] and not self.dbs[id_cip_sec]["gma4"]) or (self.dbs[id_cip_sec]["barthel"] == ">60" and self.dbs[id_cip_sec]["demencia"]) or (self.dbs[id_cip_sec]["barthel"] == ">60" and not self.dbs[id_cip_sec]["demencia"] and self.dbs[id_cip_sec]["gma4"]):
                    self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9308B', compleix, 0))
                    if not institucionalitzat:
                        self.pacients[(id_cip_sec, up, "EQA9308B", exclos, hash_d, codi_sector)] = compleix
                if (self.dbs[id_cip_sec]["barthel"] == ">60" and not self.dbs[id_cip_sec]["demencia"] and not self.dbs[id_cip_sec]["gma4"]):
                    self.indicadors.append((id_cip_sec, up, uba, piramide, edat, sexe, ates, institucionalitzat, 'EQA9308C', compleix, 0))
                    if not institucionalitzat:
                        self.pacients[(id_cip_sec, up, "EQA9308C", exclos, hash_d, codi_sector)] = compleix


    def upload_master(self):
        """."""
        # mst_indicadors_pacient
        cols = "(id_cip_sec int, up varchar(5), uba varchar(10), modul_piramide varchar(10), edat int, sexe varchar(1), \
                 ates int, instit int, indicador varchar(10), compleix int, \
                 exclos int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)        
        u.listToTable(self.indicadors, TABLE, DATABASE)

        # ecap_pac

        u.execute("drop table if exists {}".format(ECAP_PAC), DATABASE)
        u.createTable(ECAP_PAC , "(id_cip_sec int, up varchar(5), indicador varchar(10), \
                                   exclos int, hash_d varchar(40), sector varchar(4))", DATABASE, rm = True)
        u.listToTable([k for (k, compleix) in self.pacients.items() if compleix == 0], ECAP_PAC , DATABASE)        


def get_variables_single(taula):
    """ . """
    
    sql = """
            SELECT
                id_cip_sec,
                vu_cod_vs,
                vu_dat_act
            FROM
                {_taula}
            WHERE
                (vu_cod_vs IN {_codis_actuacions_treball_social}
                OR vu_cod_vs IN {_codis_escala_gijon}
                OR vu_cod_vs IN {_codis_escala_suport_social})
                AND vu_dat_act <= DATE'{_data_ext}'
            """.format(_taula=taula, _data_ext=data_ext, _codis_actuacions_treball_social=codis_actuacions_treball_social, _codis_escala_gijon=codis_escala_gijon, _codis_escala_suport_social=codis_escala_suport_social)
    return list(u.getAll(sql, "import"))

def get_activitats_single(taula):
    """ . """
    
    sql = """
            SELECT
                id_cip_sec,
                au_cod_ac,
                au_dat_act
            FROM
                {_taula}
            WHERE
                au_cod_ac = '{_codis_activitats_treball_social}'
                AND au_dat_act <= DATE'{_data_ext}'
            """.format(_taula=taula, _codis_activitats_treball_social=codis_activitats_treball_social, _data_ext=data_ext)
    return {registre for registre in u.getAll(sql, "import")}

def sub_get_xml(table):
    """ . """

    sub_registres_viu_sol, sub_registres_valoracions_socials = c.defaultdict(set), c.defaultdict(set)
    
    sql = """
        SELECT
            id_cip_sec,
            xml_data_alta,
            camp_valor 
        FROM
            {_table}
        WHERE
            xml_origen = 'GABINETS'
            AND xml_tipus_orig IN ('VAL_SPS_NE', 'VAL_SPS_TN', 'VAL_SPS_AD')
            AND camp_codi = 'IiC_Convivencia'
            AND xml_data_alta <= DATE'{_data_ext}'
        """.format(_table=table, _data_ext=data_ext)
    sub_registres_viu_sol = {registre for registre in u.getAll(sql, "import")}
   
    sql = """
            SELECT
                id_cip_sec,
                xml_data_alta
            FROM
                {_table}
            WHERE
                camp_codi IN ('Identitat_Result', 'Xarxa_Result', 'Habitatge_Result',
                                'Economia_Result', 'Laboral_Result', 'Formacio_Result',
                                'Nivell_Result', 'Juridica_Result', 'Capacitat_Result',
                                'Rif_Result', 'Recursos_Result', 'Prestacions_Result')
                AND camp_valor <> 'Sense Valorar'
                AND xml_data_alta <= DATE'{_data_ext}'
          """.format(_table=table, _data_ext=data_ext)
    sub_registres_valoracions_socials = {registre for registre in u.getAll(sql, "import")}

    return sub_registres_viu_sol, sub_registres_valoracions_socials


if __name__ == "__main__":
    EQATreballSocial()