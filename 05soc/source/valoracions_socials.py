# coding: UTF-8

import sisapUtils as u
import collections as c

class VALORACIONS_SOCIALS():

    def __init__(self):
        """ Inicialitza la classe `VALORACIONS_SOCIALS` i crida les funcions necess�ries per obtenir
            les dades i exportar-les"""

        self.get_poblacio();                            print("self.get_poblacio()")
        self.get_pacients_valoracions_socials();        print("self.get_pacients_valoracions_socials()")
        self.get_dates_valoracions_socials();           print("self.get_dates_valoracions_socials()")
        self.create_files_exportar();                   print("self.create_files_exportar()")
        self.exportar_taula();                          print("self.exportar_taula()")

    def get_poblacio(self):
        """ Obt� la poblaci� de la base de dades a trav�s d'una consulta SQL i emmagatzema els 
            `id_cip_sec` en un conjunt anomenat `self.poblacio`"""

        self.poblacio = set()

        sql= """
            SELECT
                id_cip_sec
            FROM
                assignada_tot
            """
        for id_cip_sec, in u.getAll(sql, "nodrizas"):
            self.poblacio.add(id_cip_sec)
    
    def get_pacients_valoracions_socials(self):
        """ Obt� els pacients amb valoracions socials de la base de dades i emmagatzema els 
            `id_cip_sec` en un conjunt anomenat `self.pacients_valoracions_socials`"""
            
        self.pacients_valoracions_socials = set()

        sql = """
                SELECT
                    id_cip_sec,
                    xml_tipus_orig
                FROM
                    xml_detall
                WHERE
                    xml_origen = 'GABINETS'
                    AND XML_TIPUS_ORIG IN ('VAL_SPS_NE', 'VAL_SPS_TN', 'VAL_SPS_AD')   
            """
        for id_cip_sec, xml_tipus_orig in u.getAll(sql, "import"):
            if id_cip_sec in self.poblacio:
                self.pacients_valoracions_socials.add(id_cip_sec)

    def get_dates_valoracions_socials(self):
        """ Obt� les dates de les valoracions socials dels pacients i les emmagatzema en un 
            diccionari anomenat `self.dates_valoracions_socials`, on les claus s�n els 
            `id_cip_sec` i els valors s�n conjunts de dates de valoracions socials"""  

        self.dates_valoracions_socials = c.defaultdict(set)

        sql = """
                SELECT
                    id_cip_sec,
                    xml_data_alta
                FROM
                    xml_detall
                WHERE
                    camp_codi IN ('Identitat_Result', 'Xarxa_Result', 'Habitatge_Result',
                                'Economia_Result', 'Laboral_Result', 'Formacio_Result',
                                'Nivell_Result', 'Juridica_Result', 'Capacitat_Result',
                                'Rif_Result', 'Recursos_Result', 'Prestacions_Result')
                    AND camp_valor <> 'Sense Valorar'   
            """     
        for id_cip_sec, xml_data_alta in u.getAll(sql, "import"):
            if id_cip_sec in self.pacients_valoracions_socials:
                self.dates_valoracions_socials[id_cip_sec].add(xml_data_alta)

    def create_files_exportar(self):
        """ Crea una llista de tuples amb els `id_cip_sec` i les dates de valoracions socials, i 
            l'ordena."""

        self.files = list()

        for id_cip_sec in self.dates_valoracions_socials:
            for xml_data_alta in self.dates_valoracions_socials[id_cip_sec]:
                self.files.append((id_cip_sec, xml_data_alta))

        self.files.sort()

    def exportar_taula(self):
        """ Crea una nova taula a la base de dades amb les columnes `id_cip_sec` i `data_alta`, i
            omple la taula amb les dades de la llista creada a la funci� `create_files_exportar`"""
       
        columnes = "(id_cip_sec varchar(11), data_alta date)"
        db = "social"
        tb = "valoracions_socials"
        u.createTable(tb, columnes, db, rm=True)
        u.listToTable(self.files, tb, db)


if __name__ == "__main__":
    VALORACIONS_SOCIALS()