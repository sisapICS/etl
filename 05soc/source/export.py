# coding: latin1

"""
.
"""

import collections as c
import simplejson as j

import sisapUtils as u


BASE = "mst_indicadors_pacient"
KHALIX_IND = "exp_khalix_up_ind"
KHALIX_POB = "exp_khalix_up_pob"
PROFESSIONALS = "mst_professionals"
UBAS = "mst_ubas"
ECAP_IND = "exp_ecap_uba"
ECAP_PAC  = "exp_ecap_pacient"
CAT_IND = "exp_ecap_cataleg"
CAT_PARE = "exp_ecap_catalegPare"
CAT_PUNTS = "exp_ecap_catalegPunts"
CAT_EXC = "exp_ecap_catalegExclosos"
CAT_KLX = "exp_khalix_cataleg"

DATABASE = "social"

tipus_up_conv1 = {"AMBTS": "M", "NENS": "N", "ADULTS": "A"}
tipus_up_conv2 = {v: k for k, v in tipus_up_conv1.items()}

tipus = "T"

class Export(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_professionals()
        self.get_metes()
        self.get_ponderacio()
        self.set_ecap_indicadors()
        self.get_id_to_hash()
        self.set_ecap_pacients()
        self.get_cataleg()

    def get_centres(self):
        """."""
        sql = "SELECT scs_codi, tip_eap FROM nodrizas.cat_centres"
        self.centres = {up: tipus_up_conv2[tipus_up] for up, tipus_up in u.getAll(sql, "nodrizas")}

    def get_professionals(self):
        """."""
        self.professionals = dict()
        upload = []
        sql = "select distinct a.up, left(a.ide_dni, 8) \
              from cat_profvisual a \
              where a.visualitzacio = 1 \
              and exists (select 1 from cat_professionals b \
                          where a.ide_usuari = b.ide_usuari and b.tipus = 'T')"
        for up, dni in u.getAll(sql, "import"):
            self.professionals[dni] = up
            upload.append((up, dni))
        u.createTable(PROFESSIONALS, '(up varchar(5), dni varchar(20))',
                      DATABASE, rm=True)
        u.listToTable(upload, PROFESSIONALS, DATABASE)

    def get_metes(self):
        """."""
        file = "dades_noesb/eqats_metesres_subind_anual.txt"
        self.metes = {(ind, meta): float(valor) / 100.0
                      for (ind, _r, _r, meta, _r, pob, _r, valor)
                      in u.readCSV(file, sep="{")
                      if pob == "NOINSAT"}

    def get_ponderacio(self):
        """."""
        self.ponderacio_tb = list()
        self.ponderacio = c.defaultdict(dict)
        file = "dades_noesb/eqats_ponderacio_anual.txt"
        
        for (indicador, _, _, _, tipus_up, _, _, valor) in u.readCSV(file, sep="{"):
            if int(float(valor)) == -1:
                valor = "0"
            self.ponderacio_tb.append((indicador, tipus, float(valor), tipus_up))
            self.ponderacio[indicador][tipus_up] = float(valor)
        cols = "(indicador varchar(10), tipus varchar(1), valor double, tipus_up varchar(10))"
        u.createTable(CAT_PUNTS, cols, DATABASE, rm=True)
        u.listToTable(self.ponderacio_tb, CAT_PUNTS, DATABASE)


    def set_ecap_indicadors(self):
        """."""
        dades = []
        ubas = set()
        distinct_ind = set()
        distinct_entitat = set()
        sql = "select up, '0', indicador, sum(compleix), count(1), \
                      sum(compleix) / count(1) \
               from {} \
               where instit = 0 and \
                     ates = 1 and \
                     exclos = 0 \
               group by up, indicador".format(BASE)
        sql += " union select a.up, dni, indicador, sum(compleix), count(1), \
                      sum(compleix) / count(1) \
               from {} a \
               inner join mst_professionals b on a.up = b.up \
               where instit = 0 and \
                     ates = 1 and \
                     exclos = 0 \
               group by dni, indicador".format(BASE)
        for up, uba, ind, num, den, res in u.getAll(sql, DATABASE):
            mmin = self.metes.get((ind, "AGMMINRES"), 0)
            mmax = self.metes.get((ind, "AGMMAXRES"), 0)       
            tipus_up = self.centres.get(up, None)
            pond = self.ponderacio[ind].get(tipus_up, 0)
            punts = pond if res >= mmax or den == 0 else 0 if res < mmin else pond * ((float(res) - mmin) / (mmax - mmin))  # noqa
            resolucioPerPunts = 1 if res >= mmax or den == 0 else 0 if res < mmin else (float(res) - mmin) / (mmax - mmin)  # noqa
            up = up if uba == '0' else 'UP'
            
            dades.append((up, uba, tipus, ind, 0, den, num, 1, 1, res, res, res, punts, den - num, resolucioPerPunts, 0, mmin, mmax, tipus_up))
            ubas.add((up, tipus, uba, tipus_up))
            distinct_ind.add(ind)
            distinct_entitat.add((up, uba, ind))
        
        esperades = c.defaultdict(dict)
        sql = """select up, '0', indicador, SUM(N) from social.exp_khalix_up_det
                    where comb = 'NOINSAT' and CONC = 'NUM'
                    group by up, '0', 'T', indicador
                    UNION
                    select 'UP', dni, indicador, SUM(N) 
                    from social.exp_khalix_up_det d, social.mst_professionals mp 
                    where mp.up = d.up
                    and comb = 'NOINSAT' and CONC = 'NUM'
                    group by 'UP', dni, '0', indicador"""
        for up, uba, ind, num in u.getAll(sql, DATABASE):
            esperades[(up, uba, ind)]['NUM'] = num
            distinct_ind.add(ind)
            distinct_entitat.add((up, uba, ind))

        sql = """select up, '0', indicador, SUM(N) from social.exp_khalix_up_det
                    where comb = 'NOINSAT' and CONC = 'DEN'
                    group by up, '0', 'T', indicador
                    UNION
                    select 'UP', dni, indicador, SUM(N) 
                    from social.exp_khalix_up_det d, social.mst_professionals mp 
                    where mp.up = d.up
                    and comb = 'NOINSAT' and CONC = 'DEN'
                    group by 'UP', dni, '0', indicador"""
        for up, uba, ind, den in u.getAll(sql, DATABASE):
            esperades[(up, uba, ind)]['DEN'] = den
            distinct_ind.add(ind)
            distinct_entitat.add((up, uba, ind))

        for (up, uba, ind), analisis in esperades.items():
            num, den = 0, 0
            for analisi, val in analisis.items():
                if analisi == 'NUM': num = val
                elif analisi == 'DEN': den = val
            res = (float(num)/float(den)) if den != 0 else 1
            mmin = self.metes.get((ind, "AGMMINRES"), 0)
            mmax = self.metes.get((ind, "AGMMAXRES"), 0)       
            tipus_up = self.centres.get(up, None)
            pond = self.ponderacio[ind].get(tipus_up, 0)
            punts = pond if res >= mmax or den == 0 else 0 if res < mmin else pond * ((float(res) - mmin) / (mmax - mmin))  # noqa
            resolucioPerPunts = 1 if res >= mmax or den == 0 else 0 if res < mmin else (float(res) - mmin) / (mmax - mmin)  # noqa
            up = up if uba == '0' else 'UP'
            res = 1 if res > 1 else res
            noresolts = den - num if den >= num else 0

            dades.append((up, uba, tipus, ind, 0, den, num, 0, 0, res, res, 0, punts, noresolts, resolucioPerPunts, 0, mmin, mmax, tipus_up))
            ubas.add((up, "T", uba, tipus_up))

        # Afegim les esperades que manquen
        
        for up in self.centres:
            tipus_up = self.centres.get(up, None)
            for ind in distinct_ind:
                if (up, '0', ind) not in distinct_entitat:
                    pond = self.ponderacio[ind].get(tipus_up, 0)
                    punts = pond
                    resolucioPerPunts = 1
                    res = 1
                    dades.append((up, '0', tipus, ind, 0, 0, 0, 1, 1, res, res, 0, punts, 0, resolucioPerPunts, 0, mmin, mmax, tipus_up))

        for uba, up in self.professionals.items():
            tipus_up = self.centres.get(up, None)
            for ind in distinct_ind:
                if ("UP", uba, ind) not in distinct_entitat:
                    pond = self.ponderacio[ind].get(tipus_up, 0)
                    punts = pond
                    resolucioPerPunts = 1
                    res = 1
                    dades.append(("UP", uba, tipus, ind, 0, 0, 0, 1, 1, res, res, 0, punts, 0, resolucioPerPunts, 0, mmin, mmax, tipus_up))
                
        cols = "(up varchar(5), uba varchar(10), tipus varchar(1), \
                 indicador varchar(8), esperats double, detectats int, \
                 resolts int, deteccio double, deteccioPerResultat double, \
                 resolucio double, resultat double, resultatPerPunts double, \
                 punts double, llistat int, resolucioPerPunts double, \
                 invers int, mmin_p double, mmax_p double, tipus_up varchar(10))"
        u.createTable(ECAP_IND, cols, DATABASE, rm=True)
        u.listToTable(dades, ECAP_IND, DATABASE)
        cols = "(up varchar(5), tipus varchar(1), uba varchar(10), tipus_up varchar(1))"
        u.createTable(UBAS, cols, DATABASE, rm=True)
        u.listToTable(list(ubas), UBAS, DATABASE)

    def get_id_to_hash(self):
        """."""
        ids = set([int(id) for id,
                   in u.getAll("select id_cip_sec from {}".format(BASE),
                               DATABASE)])
        sql = "select id_cip_sec, hash_d, codi_sector from u11 \
               where id_cip_sec in {}".format(tuple(ids))
        self.id_to_hash = {row[0]: row[1:] for row in u.getAll(sql, "import")}

    def set_ecap_pacients(self):
        """."""
        # sql = "select id_cip_sec, up, indicador, \
        #               if(instit = 1, 2, \
        #                  if(ates = 0, 5, \
        #                     if(exclos = 1, 4, 0))) \
        #        from {} \
        #        where compleix = 0".format(BASE)
        # dades = [row + self.id_to_hash[row[0]] for row
        #          in u.getAll(sql, DATABASE)]
        # cols = "(id_cip_sec int, up varchar(5), indicador varchar(10), \
        #          exclos int, hash_d varchar(40), sector varchar(4))"
        # u.createTable(ECAP_PAC, cols, DATABASE, rm=True)
        # u.listToTable(dades, ECAP_PAC, DATABASE)
        cataleg = [(0, "Pacients que formen part de l&#39;indicador", 0),
                   (1, "Pacients no atesos el darrer any", 1)]
        cols = "(codi int, descripcio varchar(255), ordre int)"
        u.createTable(CAT_EXC, cols, DATABASE, rm=True)
        u.listToTable(cataleg, CAT_EXC, DATABASE)

    def get_cataleg(self):
        """."""
        upload = []
        khalix = []
        file = 'dades_noesb/indicadors.json'
        cataleg = j.load(open(file), encoding="latin1")
        pare = "EQAT01"
        pare_lit = "Indicadors treball social"
        umi = "http://10.80.217.201/sisap-umi/indicador/codi/"
        for codi, dades in cataleg.items():
            this = (codi, dades["literal"].encode("latin1"), int(codi[-2:]),
                    pare, 1, 0, 0, self.metes.get((codi, "AGMMINRES"), 0),
                    self.metes.get((codi, "AGMINTRES"), 0),
                    self.metes.get((codi, "AGMMAXRES"), 0),
                    1, umi + codi, "", "GENERAL")
            upload.append(this)
            khalix.append((codi, dades["literal"].encode("latin1"),
                           pare, pare_lit))
        cols = "(indicador varchar(8), literal varchar(255), ordre int, \
                 pare varchar(8), llistat int, invers int, mdet double, \
                 mmin double, mint double, mmax double, toShow int, \
                 wiki varchar(255), curt varchar(80), pantalla varchar(50))"
        u.createTable(CAT_IND, cols, DATABASE, rm=True)
        LITERALS = (("EQA9301", "Valoraci� social a infants amb discapacitat", 1),
                    ("EQA9302", "Valoraci� social a PCC amb GMA 4", 1),
                    ("EQA9303", "Valoraci� social a persones amb recent diagn�stic de dem�ncia", 1),
                    ("EQA9304", "Valoraci� social persones fr�gils que viuen soles majors de 80 anys", 0),
                    ("EQA9304A", "Valoraci� social persones fr�gils que viuen soles majors de 80 anys prioritat alta", 1),
                    ("EQA9304B", "Valoraci� social persones fr�gils que viuen soles majors de 80 anys prioritat mitja", 1),
                    ("EQA9304C", "Valoraci� social persones fr�gils que viuen soles majors de 80 anys prioritat baixa", 1),
                    ("EQA9305", "Valoraci� social domicili�ria dels nou atdom", 1),
                    ("EQA9306", "Intervenci� amb professionals d'altres institucions per infants en risc social", 1),
                    ("EQA9307", "Intervenci� amb professionals d'altres institucions per adolescents en risc social", 1),
                    ("EQA9308", "Atenci� a les persones amb depend�ncia", 0),
                    ("EQA9308A", "Atenci� a les persones amb depend�ncia prioritat alta", 1),
                    ("EQA9308B", "Atenci� a les persones amb depend�ncia  prioritat mitja", 1),
                    ("EQA9308C", "Atenci� a les persones amb depend�ncia prioritat baixa", 1))
        umi = "http://10.80.217.201/sisap-umi/indicador/codi/{}"
        upload1 = [(cod, des, i + 1, "EQATS" if cod[-1].isdigit() else cod[:-1] , llis, 0, 0, 0, 0, 0, 1, umi.format(cod), "", "GENERAL") for i, (cod, des, llis) in enumerate(LITERALS)]
        LITERALS = (("EQD9301", "Detecci� d'infants amb discapacitat", 0),
                    ("EQD9302", "Detecci� de PCC amb GMA4", 0),
                    ("EQD9303", "Detecci� de persones amb recent diagn�stic de dem�ncia", 0),
                    ("EQD9304", "Detecci� de persones fr�gils que viuen soles majors de 80 anys", 0),
                    ("EQD9305", "Detecci� de nous atdom", 0),
                    ("EQD9306", "Detecci� d'infants en risc social", 0),
                    ("EQD9307", "Detecci� d'adolescents en risc social", 0),
                    ("EQD9308", "Detecci� de persones amb deped�ncia", 0))
        upload2 = [(cod, des, i + 1, "EQDTS" if cod[-1].isdigit() else cod[:-1] , llis, 0, 0, 0, 0, 0, 1, umi.format(cod), "", "GENERAL") for i, (cod, des, llis) in enumerate(LITERALS)]
        upload = upload1 + upload2
        u.listToTable(upload, CAT_IND, DATABASE)        
        cols = "(pare varchar(10), literal varchar(255), ordre int, \
                 curt varchar(80), proces varchar(50))"
        u.createTable(CAT_PARE, cols, DATABASE, rm = True)
        pare = [("EQATS", "EQA Treball Social", 9, DATABASE, ""), ("EQDTS", "EQD Treball Social", 10, DATABASE, "")]
        u.listToTable(pare, CAT_PARE, DATABASE)
        cols = "(indicador varchar(10), desc_ind varchar(255), \
                 grup varchar(10), grup_desc varchar(255))"
        u.createTable(CAT_KLX, cols, DATABASE, rm=True)
        upload = [(cod, des, "EQATS", "Indicadors treball social") for i, (cod, des, _) in enumerate(LITERALS)]
        u.listToTable(khalix, CAT_KLX, DATABASE)


if __name__ == "__main__":
    Export()