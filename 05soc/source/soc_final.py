# coding: UTF-8


import sisapUtils as u
import collections as c
import csv
import math
import multiprocessing as m
import os
import sisaptools as t

CONNEXIO = ("p2262", "social")

DETECCIO = {"EQD9301": ("DISCAPACITAT", ("EQA9301",)),  # noqa
            "EQD9302": ("PCC", ("EQA9302",)),
            "EQD9303": ("DEMENCIA", ("EQA9303",)),
            "EQD9304": ("FRAGILS", ("EQA9304",)),
            "EQD9305": ("ATDOM", ("EQA9305",)),
            "EQD9306": ("IN_RISC", ("EQA9306",)),
            "EQD9307": ("AD_RISC", ("EQA9307",)),
            "EQD9308": ("DEPENDENCIA", ("EQA9308",))}
LV_POB = "exp_khalix_up_pob"
LV_UP = "exp_khalix_up_ind"
LV_DET = "exp_khalix_up_det"
ES_BASAL = False

class EQA():
    def __init__(self):
        # detecci�
        self.deteccio_inv = c.defaultdict(list)
        self.deteccio_lit = []
        for nou, (literal, antics) in DETECCIO.items():
            for antic in antics:
                self.deteccio_inv[antic].append(nou)
            self.deteccio_lit.append((nou, literal))
        this = os.path.dirname(os.path.realpath(__file__))
        that = "/prevalences.txt"
        self.deteccio_file = this.replace("source", "dades_noesb") + that

    def get_prev_anual(self):
        """."""
        dades = c.defaultdict(c.Counter)
        ind = tuple(self.deteccio_inv.keys())
        sql = """select a.indicador, nvl(b.medea, '2U'), a.edat, a.comb,
                        a.sexe, a.n, c.n
                 from {} a
                 inner join nodrizas.cat_centres b on a.up = b.scs_codi
                 inner join {}
						c on a.up = c.up and a.edat = c.edat and
                        a.comb = c.comb and a.sexe = c.sexe
                 where indicador in {} and
                       conc = 'DEN'""".format(LV_UP, LV_POB, ind)
        for row in t.Database(*CONNEXIO).get_all(sql):
            for nou in self.deteccio_inv[row[0]]:
                dades[(nou,) + row[1:-2]]["num"] += row[-2]
                dades[(nou,) + row[1:-2]]["den"] += row[-1]
        upload = [key + (vals["num"] / float(vals["den"]),)
                  for key, vals in dades.items()]
        t.TextFile(self.deteccio_file).write_iterable(upload, delimiter="{",
                                                      endline="\r\n")
    

if __name__ == "__main__":
    eqa = EQA()
    if ES_BASAL:
        eqa.get_prev_anual()
