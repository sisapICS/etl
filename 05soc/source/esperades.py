# coding: UTF-8


import sisapUtils as u
import collections as c
import csv
import math
import multiprocessing as m
import os
import sisaptools as t

CONNEXIO = ("p2262", "social")

DETECCIO = {"EQD9301": ("DISCAPACITAT", ("EQA9301",)),  # noqa
            "EQD9302": ("PCC", ("EQA9302",)),
            "EQD9303": ("DEMENCIA", ("EQA9303",)),
            "EQD9304": ("FRAGILS", ("EQA9304",)),
            "EQD9305": ("ATDOM", ("EQA9305",)),
            "EQD9306": ("IN_RISC", ("EQA9306",)),
            "EQD9307": ("AD_RISC", ("EQA9307",)),
            "EQD9308": ("DEPENDENCIA", ("EQA9308",))}

LV_POB = "exp_khalix_up_pob"
LV_UP = "exp_khalix_up_ind"
LV_DET = "exp_khalix_up_det"

BASE = "mst_indicadors_pacient"
KHALIX_IND = "exp_khalix_up_ind"
KHALIX_IND_PIRAM = "exp_khalix_piram_ind"
KHALIX_POB = "exp_khalix_up_pob"
PROFESSIONALS = "mst_professionals"
UBAS = "mst_ubas"
ECAP_IND = "exp_ecap_uba"
ECAP_PAC  = "exp_ecap_pacient"
CAT_IND = "exp_ecap_cataleg"
CAT_PARE = "exp_ecap_catalegPare"
CAT_PUNTS = "exp_ecap_catalegPunts"
CAT_EXC = "exp_ecap_catalegExclosos"
CAT_KLX = "exp_khalix_cataleg"

DATABASE = "social"

tipus_conv = {"AMBTS": "M", "NENS": "N", "ADULTS": "A"}

up2br = {up: br for up, br in u.getAll("SELECT scs_codi, ics_codi FROM cat_centres", "nodrizas")}

class EQA():
    def __init__(self):
        # detecci�
        self.deteccio_inv = c.defaultdict(list)
        self.deteccio_lit = []
        for nou, (literal, antics) in DETECCIO.items():
            for antic in antics:
                self.deteccio_inv[antic].append(nou)
            self.deteccio_lit.append((nou, literal))
        this = os.path.dirname(os.path.realpath(__file__))
        that = "/prevalences.txt"
        self.deteccio_file = this.replace("source", "dades_noesb") + that
    
    def set_khalix_ind(self):
        """."""
        dades_up, dades_piram = c.Counter(), c.Counter()
        sql = "select id_cip_sec, up, modul_piramide, indicador, edat, sexe, ates, \
                      instit, compleix \
               from {} \
               where exclos = 0".format(BASE)
        for id, up, modul_piramide, ind, edat, sexe, ates, ins, num in u.getAll(sql, DATABASE):
            br = up2br[up]
            pobs = ["{}INSASS".format("" if ins else "NO")]
            if ates:
                pobs.append("{}INSAT".format("" if ins else "NO"))
            edat_k = u.ageConverter(int(edat))
            sexe_k = u.sexConverter(sexe)
            for pob in pobs:
                dades_up[(up, edat_k, sexe_k, pob, ind, "DEN")] += 1
                if modul_piramide:
                    dades_piram[(br+"T"+modul_piramide, edat_k, sexe_k, pob, ind, "DEN")] += 1
                if num:
                    dades_up[(up, edat_k, sexe_k, pob, ind, "NUM")] += 1
                    if modul_piramide:
                        dades_piram[(br+"T"+modul_piramide, edat_k, sexe_k, pob, ind, "NUM")] += 1

        upload_up = [k + (v,) for (k, v) in dades_up.items()]
        cols_up = "(up varchar(10), edat varchar(10), sexe varchar(10), \
                 comb varchar(10), indicador varchar(10), conc varchar(10), \
                 n int)"
        u.createTable(KHALIX_IND, cols_up, DATABASE, rm=True)
        u.listToTable(upload_up, KHALIX_IND, DATABASE)

        upload_piram = [k + (v,) for (k, v) in dades_piram.items()]
        cols_piram = "(piram varchar(20), edat varchar(10), sexe varchar(10), \
                 comb varchar(10), indicador varchar(10), conc varchar(10), \
                 n int)"
        u.createTable(KHALIX_IND_PIRAM, cols_piram, DATABASE, rm=True)
        u.listToTable(upload_piram, KHALIX_IND_PIRAM, DATABASE)
    
    def set_khalix_pob(self):
        """."""
        dades = c.Counter()
        sql = "select id_cip_sec, up, edat, sexe, \
                      ates, institucionalitzat \
               from assignada_tot"
        for id, up, edat, sexe, ates, instit in u.getAll(sql, "nodrizas"):
            pobs = ["{}INSASS".format("" if instit else "NO")]
            if ates:
                pobs.append("{}INSAT".format("" if instit else "NO"))
            for pob in pobs:
                key = (up, u.ageConverter(edat), u.sexConverter(sexe), pob)
                dades[key] += 1
        upload = [k + (v,) for (k, v) in dades.items()]
        cols = "(up varchar(10), edat varchar(10), sexe varchar(10), \
                 comb varchar(10), n int)"
        u.createTable(KHALIX_POB, cols, DATABASE, rm=True)
        u.listToTable(upload, KHALIX_POB, DATABASE)

    def get_lv_deteccio(self):
        """."""
        # detectats
        dades = c.Counter()
        ind = tuple(self.deteccio_inv.keys())
        sql = """select a.up, a.indicador, nvl(b.medea, '2U'), a.edat, a.comb,
                        a.sexe, a.n
                    from {} a
                    inner join nodrizas.cat_centres b
                               on a.up = b.scs_codi
                    where indicador in {} and
                          conc = 'DEN'""".format(LV_UP, ind)
        for row in t.Database(*CONNEXIO).get_all(sql):
            for nou in self.deteccio_inv[row[1]]:
                key = (row[0], row[3], row[5], row[4], nou, "NUM")
                dades[key] += row[-1]
        upload = [k + (v,) for (k, v) in dades.items()]
        # esperats
        poblacio = c.defaultdict(dict)
        sql = """select a.up, nvl(b.medea, '2U'), a.edat, a.comb, a.sexe, a.n
                 from {} a
                 inner join nodrizas.cat_centres b
                            on a.up = b.scs_codi""".format(LV_POB)  # noqa
        for up, medea, edat, pob, sex, n in t.Database(*CONNEXIO).get_all(sql):
            poblacio[(medea, edat, pob, sex)][up] = n
        for ind, medea, edat, pob, sex, prev in csv.reader(open(self.deteccio_file), delimiter="{"):  # noqa
            ups = poblacio[(medea, edat, pob, sex)]
            for up, n in ups.items():
                upload.append((up, edat, sex, pob, ind, "DEN", n * float(prev)))  # noqa
        # upload
        cols = ("up varchar(5)", "edat varchar(10)", "sexe varchar(4)",
                "comb varchar(10)", "indicador varchar(10)",
                "conc varchar(10)", "n double")
        with t.Database(*CONNEXIO) as conn:
            conn.create_table(LV_DET, cols, remove=True)
            conn.list_to_table(upload, LV_DET)


if __name__ == "__main__":
    eqa = EQA()
    eqa.set_khalix_ind()
    eqa.set_khalix_pob()
    eqa.get_lv_deteccio()
    