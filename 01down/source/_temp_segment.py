import sisapUtils as u

crear = "create table sisap_problemes as select pr_cod_u, codi_sector, pr_cod_o_ps, pr_cod_ps, pr_th, pr_dde, pr_dba, pr_data_baixa, pr_v_dat, pr_dum, pr_hist, pr_gra, pr_up, pr_usu, pr_proc_diag from REDICS.PRSTB015_T where 1 = 0"  # noqa
u.execute(crear, "redics")

sql = "insert into sisap_problemes select pr_cod_u, codi_sector, pr_cod_o_ps, pr_cod_ps, pr_th, pr_dde, pr_dba, pr_data_baixa, pr_v_dat, pr_dum, pr_hist, pr_gra, pr_up, pr_usu, pr_proc_diag from REDICS.PRSTB015_T where codi_sector = '{}'"  # noqa
for sector in u.sectors:
    this = sql.format(sector)
    print(sector)
    u.execute(this, "redics")

u.calcStatistics("sisap_problemes", "redics")
