# coding: latin1

"""
.
"""

import shared as s
import sisaptools as u


ORIGEN_DB = ("sidics", "catalegs")


class Main(object):
    """."""

    def __init__(self):
        """."""
        tables = [table for table in u.Database(*ORIGEN_DB).get_tables()]
        for i, table in enumerate(tables):
            with u.Database(*ORIGEN_DB, retry=1) as origen:
                columns = [origen.get_column_information(column, table)
                           for column in origen.get_table_columns(table)]
                create = [column["create"] for column in columns]
                columns = ", ".join([column["query"]
                                     for column in columns])
                sql = "select {} from {}".format(columns, table)
                data = list(origen.get_all(sql))
            with u.Database(*s.DESTI_DB, retry=1) as desti:
                desti.create_table(table, create, remove=True)
                desti.list_to_table(data, table)
