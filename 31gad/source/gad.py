# coding: iso-8859-1

import sisapUtils as u
import datetime as dt
# import pandas as pd
from unicodedata import normalize
import collections as c

INCLUSIONS = set([row[0] for row in u.readCSV('./dades_noesb/inclusions.txt')])  # noqa
EXCLUSIONS = set([row[0] for row in u.readCSV('./dades_noesb/exclusions.txt')])  # noqa

DATABASE = "Gis"

ORIGEN = 'UAC'
SERVEI_CLASS = ('MF', 'PED', 'INF', 'URG')
AGENDA_COD = ('1.1', '2.1', '2.4', '2.8')


def get_te_motiu(motius=None):
    te_motiu = 0
    motius = motius if motius else ""
    no_motius = ("CAP", "Planifi.cat")
    no_expresa = ("No expressa motiu", "El pacient no vol expressar el motiu de la demanda")
    motiu_list = set()
    for motiu in motius.split(';'):
        motiu = "" if motiu in no_motius else motiu
        motiu_list.add(motiu)
    for no_exp in no_expresa:
        if no_exp in motiu_list:
            motiu_list = set([""]) 
    motiu_list = motiu_list - set([""])
    if len(motiu_list) == 0:
        te_motiu = 0
    else:
        te_motiu = 1
    return te_motiu


def get_te_motiu_inf(motius=None):
    te_motiu_inf = 0
    motius = motius if motius else ""
    no_motius = ("CAP", "Planifi.cat")
    no_expresa = ("No expressa motiu", "El pacient no vol expressar el motiu de la demanda")
    motiu_list = set()
    for motiu in motius.split(';'):
        motiu = "" if motiu in no_motius else motiu
        motiu_list.add(motiu)
    for no_exp in no_expresa:
        if no_exp in motiu_list:
            motiu_list = set([""]) 
    motiu_list = motiu_list - set([""])
    if len(motiu_list) == 0:
        te_motiu_inf = 0
    elif len(INCLUSIONS.intersection(motiu_list)) > 0 and len(EXCLUSIONS.intersection(motiu_list)) == 0:  # noqa
        te_motiu_inf = 1
    return te_motiu_inf

def normr(it):
    normalize('NFD', it.decode('latin1')).encode('ascii', 'ignore')
    return it


class Gad():
    def __init__(self, TALL):
        """."""
        self.tall = TALL
        self.get_centres()
        self.get_visites()
        self.export_khalix()

    def _sql_composer(self):
        FIELD_RECODES = (
            # ("flag_motiu2",
            #  "flag_motiu_prior * CASE substr(MOTIU_PRIOR,1,3) WHEN 'No ' THEN 0 ELSE flag_motiu_prior END",  # noqa
            #  ),
            ("tp_codi2",
            """
            CASE SUBSTR(tipus,1,2)
                    WHEN '9C' THEN 'C9C'
                    WHEN '9R' THEN 'C9R'
                    WHEN '9D' THEN 'D9D'
                    WHEN '9T' THEN '9T'
                    WHEN '9E' THEN CASE etiqueta
                        WHEN 'ECTA' THEN '9E'||etiqueta
                        WHEN 'INFC' THEN '9E'||etiqueta
                        WHEN 'PLME' THEN '9E'||etiqueta
                        WHEN 'REIT' THEN '9E'||etiqueta
                        WHEN 'TRSA' THEN '9E'||etiqueta
                        WHEN 'VALP' THEN '9E'||etiqueta
                        WHEN 'VITA' THEN '9E'||etiqueta
                        ELSE lloc||'ALTRE' END
                    ELSE lloc||'ALTRE' END
            """,
            ),
        )

        def get_dextraccio():
            db = 'nodrizas'
            if self.tall:
                data_ext = self.tall
            else:
                data_ext = [row.strftime('%Y-%m-%d')
                            for row, in u.getAll(
                                "select data_ext from dextraccio",
                                db)
                            ][0]
            return data_ext

        def sql_recodes(_):
            ret = []
            for field, code in _:
                ret.append(code + " AS {field}".format(field=field))
            return ", ".join(ret)

        sql = """
            WITH vis AS (
                SELECT
                    a.*, {field_recodes}
                FROM dwsisap.SISAP_MASTER_VISITES a
                WHERE
                    MONTHS_BETWEEN(last_day(DATE '{tall}'), peticio) BETWEEN 0 AND 12 AND
                    sisap_servei_class IN {servei_class} AND
                    ORIGEN = '{origen}' AND
                    atributs_agenda_cod IN {agenda_cod}
            )
            SELECT
                UP,
                sisap_servei_class,
                atributs_agenda_cod,
                motiu_prior,
                tp_codi2,
                motiu_etiqueta,
                peticio,
                pacient
                -- flag_motiu2
                -- count(*)
            FROM vis
            WHERE
                tp_codi2 IN ('9T','C9C','C9R','CALTRE', 'D9D') OR
                tipus = '9E'
            -- GROUP BY
            --    UP,
            --    sisap_servei_class,
            --    atributs_agenda_cod,
            --    tp_codi2,
            --    flag_motiu2,
        """.format(field_recodes=sql_recodes(FIELD_RECODES),
                servei_class=SERVEI_CLASS,
                origen=ORIGEN,
                agenda_cod=AGENDA_COD,
                tall=self.tall if self.tall else get_dextraccio()
                )
        return sql

    def get_centres(self):
        """."""
        print("centres TALL: {}".format(self.tall))
        self.centres = {}
        sql = "select scs_codi, ics_codi, ics_desc, sap_desc, amb_desc \
               from cat_centres"
        for up, br, des, sap, amb in u.getAll(sql, "nodrizas"):
            des, sap, amb = normr(des), normr(sap), normr(amb)
            self.centres[up] = {'br': br, 'desc': des, 'sap': sap, 'amb': amb}
        print("centres: {}".format(len(self.centres)))

    def get_visites(self):
        data_ext = [row.strftime('%Y-%m-%d')
                        for row, in u.getAll(
                            "select data_ext from dextraccio",
                            'nodrizas')
                        ][0]
        print("visites_start")
        inici_indi_hes = c.defaultdict(c.Counter)
        sql = """SELECT up, EXTRACT(MONTH FROM peticio), extract(YEAR FROM peticio) FROM dwsisap.sisap_master_visites
                WHERE DATA > DATE '2023-05-01' AND 
                motiu_etiqueta IN ('PXM', 'ORIPXM') AND
                tipus != '9R' AND
                MONTHS_BETWEEN(last_day(DATE '{tall}'), peticio) BETWEEN 0 AND 12 AND
                sisap_servei_class IN {servei_class} AND
                ORIGEN = '{origen}' AND
                atributs_agenda_cod IN {agenda_cod}""".format(
                                    servei_class=SERVEI_CLASS,
                                    origen=ORIGEN,
                                    agenda_cod=AGENDA_COD,
                                    tall=self.tall if self.tall else data_ext
                                    )
        for up, month, year in u.getAll(sql, 'exadata'):
            month = str(month) if len(str(month)) == 2 else '0'+str(month)
            periode = str(year) + month + '01'
            if up in self.centres:
                br = self.centres[up]['br']
                inici_indi_hes[br][int(periode)] += 1
        first_x_up = {}
        for br, periodes in inici_indi_hes.items():
            sorted_periods = sorted(periodes.keys())
            for period in sorted_periods:
                if periodes[int(period)] >= 50:
                    if br not in first_x_up:
                        first_x_up[br] = period
        print('---first_x_up----')
        print(first_x_up)
        self.visites = c.defaultdict(int)
        self.visites2 = c.defaultdict(int)
        self.gad001 = c.defaultdict(int)
        self.gad002 = c.defaultdict(int)
        sql = self._sql_composer()
        print(sql)
        for up, serv, _atr, motius, _tip, motiu_etiqueta, peticio, hash_covid in u.getAll(sql, 'exadata'):
            te_motiu = get_te_motiu(motius)
            te_motiu_inf = get_te_motiu_inf(motius)
            if up in self.centres:
                br = self.centres[up]['br']
                des = self.centres[up]['desc']
                sap = self.centres[up]['sap']
                amb = self.centres[up]['amb']
                this = (br,
                        te_motiu,
                        des, sap, amb,
                        _tip, motiu_etiqueta,
                        peticio, hash_covid
                        )
                self.visites[this] += 1
                if te_motiu_inf == 1:
                    num = 1 if serv == 'INF' else 0
                    that = (br, num, des, sap, amb,)
                    self.visites2[that] += 1
        print("visites: {}".format(len(self.visites)))
        for (br, te_motiu, des, sap, amb, tip, motiu_etiqueta, peticio, hash_covid), v in self.visites.items():
            self.gad001[('GAD001ECAP', br, 'DEN')] += v
            if motiu_etiqueta not in ('PXM', 'ORIPXM') and te_motiu == 1:
                self.gad001[('GAD001ECAP', br, 'NUM')] += v
            if tip != 'C9R' and br in first_x_up:
                periode_min = first_x_up[br]
                if peticio >= dt.datetime.strptime(str(periode_min), '%Y%m%d'):
                    self.gad001[('GAD001HES', br, 'DEN')] += v
                    if motiu_etiqueta in ('PXM', 'ORIPXM') and te_motiu == 1:
                        self.gad001[('GAD001HES', br, 'NUM')] += v
        for k, v in self.visites2.items():
            self.gad002[('GAD002', k[0], 'DEN')] += v
            if k[1] == 1:
                self.gad002[('GAD002', k[0], 'NUM')] += v

        self.upload = [k + (v,) for k, v in self.gad001.items()]
        self.upload = self.upload + [k + (v,) for k, v in self.gad002.items()]

        # self.visites = [k + (v,) for k, v in self.visites.items()]
        # self.visites2 = [k + (v,) for k, v in self.visites2.items()]
        print("visites_end")

    def export_khalix(self):
        print("sortida khalix")
        TB = "exp_khalix_up_gad"
        u.createTable(
            TB,
            "(indicador varchar(12), br varchar(5), analisis varchar(10), n int)",  # noqa
            DATABASE,
            rm=True)
        u.listToTable(self.upload, TB, DATABASE)
        """Exportar tots indicadors GAD a Khalix"""
        sql = """
            select
                indicador, '{periode}', br, analisis,
                'NOCAT', 'NOIMP', 'DIM6SET', 'N',
                n
            from {db}.{tb}
        """.format(
            db=DATABASE,
            tb=TB,
            periode='A'+self.tall[2:4]+self.tall[5:7] if self.tall else 'Aperiodo')

        file = 'GAD_UP{tall}'.format(tall='_'+self.tall[5:7]+'_'+self.tall[0:4] if self.tall else '')
        u.exportKhalix(sql, file, force=True if self.tall else False)
        # u.createTable('exp_khalix_uba_gad', \
        #              '(indicador varchar(12), entity varchar(25), \
        #                comb varchar(10), analisis varchar(10), n int)',
        #                DATABASE, rm=True)


if __name__ == "__main__":
    TALL = False  # ex: '2022-01-01' o False: si anem al dia.
    print(TALL, u.IS_MENSUAL)
    Gad(TALL)
