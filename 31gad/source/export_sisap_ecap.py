# -*- coding: utf-8 -*-

import sisapUtils as u
import collections as c
import hashlib as h

class ExportECAP():
    def __init__(self):
        self.get_dextraccio()
        self.taules_mb()
        self.centres()
        self.get_dni()
        self.sisap_ecap_adm()
        self.export_ecap_cataleg()
        self.export_ecap_cataleg_pare()
        self.cataleg_administratius()
    
    def get_dextraccio(self):
        sql = """select year(data_ext), month(data_ext) from nodrizas.dextraccio"""
        for y, m in u.getAll(sql, 'nodrizas'):
            self.any = y
            self.mes = m
    
    def taules_mb(self):
        self.tb = 'export_ecap_adm'
        self.db = 'gad'
        cols = """(up varchar(5), adm varchar(10), uas varchar(5), 
                    uba varchar(5), indicador varchar(15), edat varchar(10), num int, den int, 
                    res float, nivell int)"""
        u.createTable(self.tb, cols, self.db, rm = True)
    
    def centres(self):
        self.up_br = {}
        sql = """select ics_codi, scs_codi from nodrizas.cat_centres"""
        for br, up in u.getAll(sql, 'nodrizas'):
            self.up_br[up] = br
    
    def get_dni(self):
        self.entity_dni = {}
        sql = """SELECT up_ics, nif
                FROM DWSISAP.CAT_ADMINISTRATIUS
                where up_ics is not null"""
        for up_ics, dni in u.getAll(sql, 'exadata'):
            if up_ics in self.up_br:
                br = self.up_br[up_ics]
                hash = h.sha1(dni).hexdigest().upper()[0:6]
                entity = br + 'A' + hash
                self.entity_dni[entity] = dni
    
    def sisap_ecap_adm(self):
        # A nivell up i administratiu
        indicadors = c.defaultdict(dict)
        sql = """select indicador, up, entity, analisi, sum(value)
                from gad.indicadors_adm
                where (indicador like 'ADM001%'
                or indicador like 'ADM002%'
                or indicador like 'ADM005%'
                or indicador like 'ADM006%'
                or indicador like 'ADM007%'
                or indicador = 'ADM008')
                and f_v in ('F','V','N')
                group by indicador, up, entity, analisi"""
        for indicador, up, entity, analisi, value in u.getAll(sql, 'gad'):
            indicadors[(indicador, up, entity)][analisi] = value
        upload = []
        for key in indicadors:
            indicador, up, entity = key
            if entity in self.entity_dni:
                dni = self.entity_dni[entity]
                num, den = 0,0
                for analisi in indicadors[key]:
                    if analisi == 'DEN': den = indicadors[key][analisi]
                    elif analisi == 'NUM': num = indicadors[key][analisi]
                if indicador in ('ADM005A', 'ADM005B', 'ADM006A', 'ADM006B', 'ADM006C', 'ADM006D', 'ADM008', 'ADM007A', 'ADM007B', 'ADM007C', 'ADM007D', 'ADM007E', 'ADM007F'):
                    res = num
                else:
                    res = num/den*100
                upload.append((up, dni, '', '', indicador, '', num, den, res, 1))
        u.listToTable(upload, self.tb, self.db)
        
        # A nivell admi, uas, uba
        indicadors_up_uas_uba = c.defaultdict(c.Counter)
        indicadors_up_uas = c.defaultdict(c.Counter)
        indicadors_up_uas_edat = c.defaultdict(c.Counter)
        indicadors_up = c.defaultdict(c.Counter)
        upload = []
        sql = """select indicador, up, entity, analisi, uas, uba, sum(value)
                from gad.indicadors_adm
                where indicador = 'ADM004'
                and F_V in ('F','V','N')
                group by indicador, up, entity, analisi, uas, uba"""
        for indicador, up, entity, analisi, uas, uba, v in u.getAll(sql, 'gad'):
            if uas != '' and uba != '':
                indicadors_up_uas_uba[(indicador, up, entity, uas, uba)][analisi] += v
            if uas != '':
                indicadors_up_uas[(indicador, up, entity, uas )][analisi] += v
                indicadors_up_uas_edat[(indicador, up, entity, uas)][analisi] += v
            indicadors_up[(indicador, up, entity)][analisi] += v
        for key in indicadors_up_uas_uba:
            indicador, up, entity, uas, uba = key
            if entity in self.entity_dni:
                dni = self.entity_dni[entity]
                den = 0
                num = 0
                for analisi in indicadors_up_uas_uba[key]:
                    if analisi == 'DEN': den = indicadors_up_uas_uba[key][analisi]
                    elif analisi == 'NUM': num = indicadors_up_uas_uba[key][analisi]
                if den == 0: res = 0
                else:
                    res = num/den*100
                upload.append((up, dni, uas, uba, indicador, '', num, den, res, 3))
        for key in indicadors_up_uas:
            indicador, up, entity, uas  = key
            if entity in self.entity_dni:
                dni = self.entity_dni[entity]
                for analisi in indicadors_up_uas[key]:
                    if analisi == 'DEN': den = indicadors_up_uas[key][analisi]
                    elif analisi == 'NUM': num = indicadors_up_uas[key][analisi]
                res = num/den*100
                upload.append((up, dni, uas, '', indicador, '', num, den, res, 2))
        for key in indicadors_up:
            indicador, up, entity = key
            if entity in self.entity_dni:
                dni = self.entity_dni[entity]
                for analisi in indicadors_up[key]:
                    if analisi == 'DEN': den = indicadors_up[key][analisi]
                    elif analisi == 'NUM': num = indicadors_up[key][analisi]
                res = num/den*100
                upload.append((up, dni, '', '', indicador, '', num, den, res, 1))

        u.listToTable(upload, self.tb, self.db)

        # A nivell adm, uas, uba, age
        indicadors_up_uas_uba = c.defaultdict(c.Counter)
        indicadors_up_uas = c.defaultdict(c.Counter)
        indicadors_up_uas_edat = c.defaultdict(c.Counter)
        indicadors_up = c.defaultdict(c.Counter)
        upload = []
        sql = """select indicador, up, entity, analisi, uas, uba, age, sum(value)
                from gad.indicadors_adm
                where indicador = 'ADM003B'
                and F_V in ('F','V','N')
                group by indicador, up, entity, analisi, uas, uba, age
                having SUM(VALUE) > 0"""
        for indicador, up, entity, analisi, uas, uba, age, v in u.getAll(sql, 'gad'):
            if uas != '' and uba != '':
                indicadors_up_uas_uba[(indicador, up, entity, uas, uba, age)][analisi] += v
            if uas != '':
                indicadors_up_uas[(indicador, up, entity, uas)][analisi] += v
                indicadors_up_uas_edat[(indicador, up, entity, uas, age)][analisi] += v
            indicadors_up[(indicador, up, entity)][analisi] += v
        for key in indicadors_up_uas_uba:
            indicador, up, entity, uas, uba, age = key
            if entity in self.entity_dni:
                dni = self.entity_dni[entity]
                for analisi in indicadors_up_uas_uba[key]:
                    num = indicadors_up_uas_uba[key][analisi]
                upload.append((up, dni, uas, uba, indicador, age, num, 1, num, 4))
        for key in indicadors_up_uas:
            indicador, up, entity, uas = key
            if entity in self.entity_dni:
                dni = self.entity_dni[entity]
                for analisi in indicadors_up_uas[key]:
                    num = indicadors_up_uas[key][analisi]
                upload.append((up, dni, uas, '', indicador, '', num, 1, num, 2))
        for key in indicadors_up_uas_edat:
            indicador, up, entity, uas, age = key
            if entity in self.entity_dni:
                dni = self.entity_dni[entity]
                for analisi in indicadors_up_uas_edat[key]:
                    num = indicadors_up_uas_edat[key][analisi]
                upload.append((up, dni, uas, '', indicador, age, num, 1, num, 3))
        for key in indicadors_up:
            indicador, up, entity = key
            if entity in self.entity_dni:
                dni = self.entity_dni[entity]
                for analisi in indicadors_up[key]:
                     num = indicadors_up[key][analisi]
                upload.append((up, dni, '', '', indicador, '', num, 1, num, 1))
        u.listToTable(upload, self.tb, self.db)

    def export_ecap_cataleg(self):
        """."""
        cataleg = [
            ('ADM001AECAP', '% visites amb motius protocollitzats de PxM (ECAP anual)', 1, 'PROXMOT', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/5536/ver/', 'PCT'),
            ('ADM001AHES', '% visites amb motius protocollitzats de PxM (HES anual)', 2, 'PROXMOT', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/5632/ver/', 'PCT'),
            ('ADM001MECAP', '% visites amb motius protocollitzats de PxM (ECAP mensual)', 3, 'PROXMOT', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/5535/ver/', 'PCT'),
            ('ADM001MHES', '% visites amb motius protocollitzats de PxM (HES mensual)', 4, 'PROXMOT', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/5537/ver/', 'PCT'),
            ('ADM002A', '% visites amb motius protocollitzats de PxM programats a infermeria (anual)', 5, 'PROXMOT', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4347/ver/', 'PCT'),
            ('ADM002M', '% visites amb motius protocollitzats de PxM programats a infermeria (mensual)', 6, 'PROXMOT', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4348/ver/', 'PCT'),
            # ('ADM003A', '% de persones assignades a una Unitat Administrativa Sanitaria', 5, 'ADMIND', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4358/ver/', 'PCT'),
            ('ADM003B', 'Nombre de persones assignades a una Unitat Administrativa Sanitaria', 1, 'ADMPOB', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4359/ver/', 'VA'),
            ('ADM004', '% Programacio realitzada per administratiu de referencia', 7, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4349/ver/', 'PCT'),
            ('ADM005A', 'Visites programades per administratius en el propi EAP', 8, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4352/ver/', 'VA'),
            ('ADM005B', 'Visites programades per administratius en altres EAP', 9, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4353/ver/', 'VA'),
            ('ADM006A', 'Visites programades per administratius a l ASSIR', 10, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4354/ver/', 'VA'),
            ('ADM006B', 'Visites programades per administratius a salut mental', 11, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4355/ver/', 'VA'),
            ('ADM006C', 'Visites programades per administratius a altres especialitats', 12, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4356/ver/', 'VA'),
            ('ADM006D', 'Visites programades per administratius a altres serveis', 13, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4357/ver/', 'VA'),
            ('ADM007', 'Visites realitzades per administratiu per tipus de visita', 14, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4515/ver/', 'VA'),
            ('ADM007A', 'Visites 9C realitzades per administratiu', 15, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4357/ver/', 'VA'),
            ('ADM007B', 'Visites 9E realitzades per administratiu', 16, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4329/ver/', 'VA'),
            ('ADM007C', 'Visites 9R realitzades per administratiu', 17, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4530/ver/', 'VA'),
            ('ADM007D', 'Visites 9T realitzades per administratiu', 18, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4531/ver/', 'VA'),
            ('ADM007E', 'Visites 9EC realitzades per administratiu', 19, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4532/ver/', 'VA'),
            ('ADM007F', 'Visites 9D realitzades per administratiu', 20, 'PROVIS', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4533/ver/', 'VA'),
            ('PLA001A', 'Nombre de pacients programats planificat per administratiu', 21, 'PLAIND', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4671/ver/', 'VA'),
            ('PLA002A', 'Nombre de visites conjuntes programades per administratiu', 22, 'PLAIND', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4674/ver/', 'VA'),
            ('PLA003A', 'Nombre de visites de seguiment programades per administratiu', 23, 'PLAIND', 0,0,0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/4675/ver/', 'VA'),
        ]

        tb_ecap_cat = 'ecap_cataleg'
        db = 'gad'
        cols = """(indicador varchar(15), literal varchar(300), ordre int, pare varchar(10), llistat int, invers int, 
                    mmin int, mint int, mmax int, toshow int, wiki varchar(250), tipusvalor varchar(8))"""
        u.createTable(tb_ecap_cat, cols, db, rm=True)
        u.listToTable(cataleg, tb_ecap_cat, db)
        u.exportPDP("select * from gad.ecap_cataleg",
                "admcataleg",
                datAny=True)
        
        cataleg_edats = [
            ('A1544', 'Edat 15-44 anys'),
            ('A4564', 'Edat 45-64 anys'),
            ('P02', 'Edat 0-2 anys'),
            ('P37', 'Edat 3-7 anys'),
            ('A75MORE', 'Edat >74 anys'),
            ('A6574', 'Edat 65-74 anys'),
            ('P814', 'Edat 8-14 anys'),
        ]
        tb_ecap_cat = 'ecap_cataleg_edats'
        db = 'gad'
        cols = """(edat varchar(10), desc_edat varchar(300))"""
        u.createTable('admcatalegedats', cols, 'pdp', rm=True)
        u.listToTable(cataleg_edats, 'admcatalegedats', 'pdp')

    def export_ecap_cataleg_pare(self):
        """."""
        tb_ecap_cat_pare = 'ecap_cataleg_pare'
        db = 'gad'
        cols = """(pare varchar(10), literal varchar(300), ordre int, pantalla varchar(20))"""
        u.createTable(tb_ecap_cat_pare, cols, db, rm=True)
        pares = [("ADMPOB", "Poblacio administratius", 1, "POBLACIO"),
                 ("PROXMOT", "Programaci� per motius", 1, "INDICADORS"),
                 ("PROVIS", "Programaci� de visites", 1, "INDICADORS"),
                 ("PLAIND", "Programaci� Planificat", 2, "INDICADORS")]
        u.listToTable(pares, tb_ecap_cat_pare, db)
        u.exportPDP("select * from gad.ecap_cataleg_pare",
                "admcatalegpare",
                datAny=True)
    
    def cataleg_administratius(self):
        upload = []
        sql = """SELECT nif, up_ics FROM dwsisap.CAT_ADMINISTRATIUS
                WHERE fix_variable IN ('F', 'V', 'N')
                AND UP_ICS IS NOT NULL"""
        for nif, up in u.getAll(sql, 'exadata'):
            upload.append((nif, up))
        cols = '(nif varchar(10), up varchar(5))'
        u.createTable('admup', cols, 'pdp', rm=True)
        u.listToTable(upload, 'admup', 'pdp')


if __name__ == "__main__":
    ExportECAP()