# -*- coding: utf-8 -*-

"""
Indicadors d'administratius per UAS
"""

import sisapUtils as u
import collections as c
import hashlib as h
from unicodedata import normalize

KHALIX_CATALEG = 'ADM_CATALEG'
KHALIX_UBA = 'ADM_UBA'
KHALIX_UP = 'ADM_UP'
DB = 'GAD'

class export_khalix():
    def __init__(self):
        self.cataleg()
        self.ambitos()
        self.pambitos()
    
    def cataleg(self):
        # primer ho hem de copiar a nodrizas
        ups = {}
        sql = """select scs_codi, ics_codi from nodrizas.cat_centres"""
        for ics, br in u.getAll(sql, 'nodrizas'):
            ups[ics] = br
        upload = []
        sql = """select DISTINCT UP_ICS, nif, 
                    concat(CONCAT(nom, ' '), concat(CONCAT(cognom_1, ' '), cognom_2)) AS UAS_DESC
                    FROM DWSISAP.CAT_ADMINISTRATIUS
                    WHERE FIX_VARIABLE in ('F', 'N')
                    AND UP_ICS IS NOT NULL"""""
        for up, nif, desc in u.getAll(sql, 'exadata'):
            hash = h.sha1(nif).hexdigest().upper()[0:6]
            if up in ups:
                entity = ups[up] + 'A' + hash
                upload.append((entity, desc))
        cols = """(entity varchar(15), desc varchar(100))"""
        for db in (DB, "exadata"):
            u.createTable('ADM_CATALEG', '(entity varchar(15), desc_adm varchar(100))', db, rm = True)
            u.listToTable(upload, KHALIX_CATALEG, db)
        # han d'haver-hi 4 camps a l'hora d'enviar a kahlix x la funció
        sql = """select *, concat('A','periodo'), '' from gad.ADM_CATALEG"""
        u.exportKhalix(sql,KHALIX_CATALEG)

    def ambitos(self):
        sql = """select indicador, concat('A','periodo'), BR, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N', SUM(value) 
                from gad.indicadors_adm 
                where indicador in ('ADM001AECAP', 'ADM001AHES', 'ADM001MECAP', 'ADM001MHES', 'ADM002A', 'ADM002M', 'ADM004')
                and ((ASS_BR = 1 and F_V = 'F') or F_V = 'N')
                group by indicador, concat('A','periodo'), BR, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N'
                UNION
                select indicador, concat('A','periodo'), BR, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N', SUM(value) 
                from gad.indicadors_adm 
                where indicador = 'ADM003A'
                and ASS_BR = 0 
                group by indicador, concat('A','periodo'), BR, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N'
                UNION
                select indicador, concat('A','periodo'), BR, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N', SUM(value) 
                from gad.indicadors_adm 
                where indicador = 'ADM005A'
                and ((ASS_BR = 1 and F_V IN ('F', 'V')) or F_V = 'N')
                group by indicador, concat('A','periodo'), BR, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N'
                UNION
                select indicador, concat('A','periodo'), BR, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N', SUM(value) 
                from gad.indicadors_adm 
                where (indicador IN ('ADM005B', 'ADM008') or INDICADOR LIKE 'ADM006%' )
                and ASS_BR = 0 and F_V in ('F','V','N')
                group by indicador, concat('A','periodo'), BR, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N'"""
        u.exportKhalix(sql,KHALIX_UP,force=True)
    
    def pambitos(self):
        sql = """select indicador, concat('A','periodo'), entity, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N', SUM(value) 
                from gad.indicadors_adm 
                where indicador in ('ADM001AECAP', 'ADM001AHES', 'ADM001MECAP', 'ADM001MHES', 'ADM002A', 'ADM002M', 'ADM004')
                and ((ASS_BR = 1 and F_V = 'F') or F_V = 'N')
                and entity in (select entity from gad.ADM_CATALEG)
                group by indicador, concat('A','periodo'), entity, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N'
                UNION
                select indicador, concat('A','periodo'), entity, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N', SUM(value) 
                from gad.indicadors_adm 
                where indicador = 'ADM005A'
                and ((ASS_BR = 1 and F_V = 'F') or F_V = 'N')
                and entity in (select entity from gad.ADM_CATALEG)
                group by indicador, concat('A','periodo'), entity, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N'
                UNION
                select indicador, concat('A','periodo'), entity, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N', SUM(value) 
                from gad.indicadors_adm 
                where (indicador in ('ADM005B', 'ADM008') or INDICADOR LIKE 'ADM006%' or INDICADOR LIKE 'ADM007%')
                and ASS_BR = 0 and F_V in ('F', 'N')
                and entity in (select entity from gad.ADM_CATALEG)
                group by indicador, concat('A','periodo'), entity, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N' 
                UNION
                select indicador, concat('A','periodo'), entity, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N', SUM(value) 
                from gad.indicadors_adm 
                where indicador = 'ADM003B'
                and ((ASS_BR = 1 and F_V = 'F') or F_V = 'N')
                and entity in (select entity from gad.ADM_CATALEG)
                group by indicador, concat('A','periodo'), entity, analisi, 
                age_class, 'NOIMP', 'DIM6SET', 'N'
                having sum(value) > 0 """
        u.exportKhalix(sql,KHALIX_UBA,force=True)
        

#### EXPORTEM INDICADORS###

if __name__ == '__main__':
    export_khalix()