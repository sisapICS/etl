# -*- coding: utf-8 -*-

"""
Indicadors d'administratius per UAS
"""

import sisapUtils as u
import collections as c
import hashlib as h
from unicodedata import normalize
import datetime as dt


TB = 'indicadors_adm'
DB = 'gad'
INCLUSIONS = set([row[0] for row in u.readCSV('./dades_noesb/inclusions.txt')])  # noqa
EXCLUSIONS = set([row[0] for row in u.readCSV('./dades_noesb/exclusions.txt')])  # noqa

TALL = False  # ex: '2022-01-01' o False: si anem al dia.
ORIGEN = 'UAC'
SERVEI_CLASS = ('MF', 'PED', 'INF', 'URG')
AGENDA_COD = ('1.1', '2.1', '2.4', '2.8')

def get_dextraccio():
        db = 'nodrizas'
        if TALL:
            data_ext = TALL
        else:
            data_ext = [row.strftime('%Y-%m-%d')
                            for row, in u.getAll(
                                "select data_ext from dextraccio",
                                db)
                        ][0]
        return data_ext

def get_te_motiu(motius=None):
    te_motiu = 0
    motius = motius if motius else ""
    no_motius = ("CAP", "Planifi.cat")
    no_expresa = ("No expressa motiu", "El pacient no vol expressar el motiu de la demanda")
    motiu_list = set()
    for motiu in motius.split(';'):
        motiu = "" if motiu in no_motius else motiu
        motiu_list.add(motiu)
    for no_exp in no_expresa:
        if no_exp in motiu_list:
            motiu_list = set([""]) 
    motiu_list = motiu_list - set([""])
    if len(motiu_list) == 0:
        te_motiu = 0
    else:
        te_motiu = 1
    return te_motiu

def get_te_motiu_inf(motius=None):
    te_motiu_inf = 0
    motius = motius if motius else ""
    no_motius = ("CAP", "Planifi.cat")
    no_expresa = ("No expressa motiu", "El pacient no vol expressar el motiu de la demanda")
    motiu_list = set()
    for motiu in motius.split(';'):
        motiu = "" if motiu in no_motius else motiu
        motiu_list.add(motiu)
    for no_exp in no_expresa:
        if no_exp in motiu_list:
            motiu_list = set([""]) 
    motiu_list = motiu_list - set([""])
    if len(motiu_list) == 0:
        te_motiu_inf = 0
    elif len(INCLUSIONS.intersection(motiu_list)) > 0 and len(EXCLUSIONS.intersection(motiu_list)) == 0:  # noqa
        te_motiu_inf = 1
    return te_motiu_inf

def age_convert_class(age):
    if age[1:] in ('4564', '1544', '6574'):
        return 'ED' + age[1:]
    elif age == 'A75MORE':
        return 'EDM75'
    elif age[0] == 'P':
        return 'ED0014'

def sql_composer():
    ORIGEN = 'UAC'
    SERVEI_CLASS = ('MF', 'PED', 'INF', 'URG')
    AGENDA_COD = ('1.1', '2.1', '2.4', '2.8')
    FIELD_RECODES = (
        ("tp_codi2",
         """
         CASE SUBSTR(tipus,1,2)
                 WHEN '9C' THEN 'C9C'
                 WHEN '9R' THEN 'C9R'
                 WHEN '9D' THEN 'D9D'
                 WHEN '9T' THEN '9T'
                 WHEN '9E' THEN CASE etiqueta
                     WHEN 'ECTA' THEN '9E'||etiqueta
                     WHEN 'INFC' THEN '9E'||etiqueta
                     WHEN 'PLME' THEN '9E'||etiqueta
                     WHEN 'REIT' THEN '9E'||etiqueta
                     WHEN 'TRSA' THEN '9E'||etiqueta
                     WHEN 'VALP' THEN '9E'||etiqueta
                     WHEN 'VITA' THEN '9E'||etiqueta
                     ELSE lloc||'ALTRE' END
                 ELSE lloc||'ALTRE' END
         """,
         ),
    )

    def get_dextraccio():
        db = 'nodrizas'
        if TALL:
            data_ext = TALL
        else:
            data_ext = [row.strftime('%Y-%m-%d')
                            for row, in u.getAll(
                                "select data_ext from dextraccio",
                                db)
                        ][0]
        return data_ext


    def sql_recodes(_):
        ret = []
        for field, code in _:
            ret.append(code + " AS {field}".format(field=field))
        return ", ".join(ret)

    sql = """
        WITH vis AS (
            SELECT
                a.*, {field_recodes}, 
                MONTHS_BETWEEN(DATE '{tall}', peticio) as months
            FROM dwsisap.SISAP_MASTER_VISITES a
            WHERE
                MONTHS_BETWEEN(DATE '{tall}', peticio) BETWEEN 0 AND 12 AND
                sisap_servei_class IN {servei_class} AND
                ORIGEN = '{origen}' AND
                atributs_agenda_cod IN {agenda_cod}
        )
        SELECT
            UP,
            sisap_servei_class,
            atributs_agenda_cod,
            motiu_prior,
            tp_codi2,
            months,
            USUARI,
            motiu_etiqueta,
            peticio,
            pacient
            -- flag_motiu2
            -- count(*)
        FROM vis
        WHERE
            tp_codi2 IN ('9T','C9C','C9R','CALTRE', 'D9D') OR
            tipus = '9E'
        -- GROUP BY
        --    UP,
        --    sisap_servei_class,
        --    atributs_agenda_cod,
        --    tp_codi2,
        --    flag_motiu2,
    """.format(field_recodes=sql_recodes(FIELD_RECODES),
               servei_class=SERVEI_CLASS,
               origen=ORIGEN,
               agenda_cod=AGENDA_COD,
               tall=TALL if TALL else get_dextraccio()
               )
    print(sql)
    return sql


def normr(it):
    normalize('NFD', it.decode('latin1')).encode('ascii', 'ignore')
    return it


class Adm():
    def __init__(self):
        """."""
        self.create_table()
        self.get_centres()
        self.info_capitol1()
        self.get_visites()
        self.ADM0102()
        self.ADM003()
        self.ADM00405()
        self.ADM00708()
    
    def create_table(self):
        cols = """(indicador varchar(12), up varchar(5), 
                br varchar(5), entity varchar(15), 
                uas varchar(10), uba varchar(10),
                analisi varchar(5), age varchar(10), age_class varchar(10),
                f_v varchar(1), ass_br int, value int)"""
        u.createTable(TB, cols, DB, rm = True)

    def get_centres(self):
        """."""
        self.centres = {}
        sql = "select scs_codi, ics_codi, ics_desc, sap_desc, amb_desc \
               from cat_centres"
        for up, br, des, sap, amb in u.getAll(sql, "nodrizas"):
            des, sap, amb = normr(des), normr(sap), normr(amb)
            self.centres[up] = {'br': br, 'desc': des, 'sap': sap, 'amb': amb}
        print("centres: {}".format(len(self.centres)))

    def info_capitol1(self):
        print('cataleg')
        self.cataleg = {}
        self.fixs = set()
        self.variables = set()
        self.no_ics = set()
        self.cap1 = {}
        self.login_nif = {}
        sql = """SELECT nif, login, fix_variable, 
                    up_ics, UP_CAPITOL_1
                    FROM DWSISAP.CAT_ADMINISTRATIUS
                    WHERE up_ics IS NOT null"""
        for nif, login, f_v, up_ics, up_cap1 in u.getAll(sql, 'exadata'):
            self.cataleg[login] = [up_cap1, nif, f_v]
            self.login_nif[login] = nif
            if f_v == 'F': self.fixs.add(nif)
            elif f_v == 'V': self.variables.add(nif)
            elif f_v == 'N': self.no_ics.add(nif)
            self.cap1[nif] = (up_ics, f_v)
        print('login_nif', len(self.login_nif))
        print('no pot ser 0, len cap1', len(self.cap1))

    def get_visites(self):
        print("visites_start")
        data_ext = [row.strftime('%Y-%m-%d')
                        for row, in u.getAll(
                            "select data_ext from dextraccio",
                            'nodrizas')
                        ][0]
        print("visites_start")
        inici_indi_hes = c.defaultdict(c.Counter)
        sql = """SELECT up, EXTRACT(MONTH FROM peticio), extract(YEAR FROM peticio) FROM dwsisap.sisap_master_visites
                WHERE DATA > DATE '2023-05-01' AND 
                motiu_etiqueta IN ('PXM', 'ORIPXM') AND
                tipus != '9R' AND
                MONTHS_BETWEEN(last_day(DATE '{tall}'), peticio) BETWEEN 0 AND 12 AND
                sisap_servei_class IN {servei_class} AND
                ORIGEN = '{origen}' AND
                atributs_agenda_cod IN {agenda_cod}""".format(
                                    servei_class=SERVEI_CLASS,
                                    origen=ORIGEN,
                                    agenda_cod=AGENDA_COD,
                                    tall=data_ext
                                    )
        for up, month, year in u.getAll(sql, 'exadata'):
            month = str(month) if len(str(month)) == 2 else '0'+str(month)
            periode = str(year) + month + '01'
            if up in self.centres:
                br = self.centres[up]['br']
                inici_indi_hes[br][int(periode)] += 1
        self.first_x_up = {}
        for br, periodes in inici_indi_hes.items():
            sorted_periods = sorted(periodes.keys())
            for period in sorted_periods:
                if periodes[int(period)] >= 50:
                    if br not in self.first_x_up:
                        self.first_x_up[br] = period
        print('---first_x_up----')
        print(self.first_x_up)
        self.visites = c.defaultdict(int)
        self.visites2 = c.defaultdict(int)
        self.adm001a = c.defaultdict(int)
        self.adm002a = c.defaultdict(int)
        self.adm001m = c.defaultdict(int)
        self.adm002m = c.defaultdict(int)
        sql = sql_composer()
        print(sql)
        print('get visites')
        count = 0
        for up, serv, _atr, motius, _tip, months, login, motiu_etiqueta, peticio, hash_covid in u.getAll(sql, 'exadata'):
            te_motiu = get_te_motiu(motius)
            te_motiu_inf = get_te_motiu_inf(motius)
            if login in self.cataleg:
                up_cat, nif, fv = self.cataleg[login]
                if fv == 'N': count += 1
                if up in self.centres:
                    br = self.centres[up]['br']
                    ass_br = 1 if up_cat == br else 0
                    des = self.centres[up]['desc']
                    sap = self.centres[up]['sap']
                    amb = self.centres[up]['amb']
                    this = (br, te_motiu,
                            des, sap, amb,
                            months, ass_br, 
                            fv, nif, up, motiu_etiqueta, _tip, peticio 
                            )
                    self.visites[this] += 1
                    if te_motiu_inf == 1:
                        num = 1 if serv == 'INF' else 0
                        that = (br, num, des, sap, amb, months, ass_br, fv, nif, up)
                        self.visites2[that] += 1
        print("visites: {}".format(len(self.visites)))
        print('no ics: ', count)

    def ADM0102(self):
        for k, v in self.visites.items():
            br, num, des, sap, amb, months, ass_br, fv, nif, up, motiu_etiqueta, tip, peticio = k
            hash = h.sha1(nif).hexdigest().upper()
            self.adm001a[('ADM001AECAP', up, br, br+'A'+hash[0:6], '', '', 'DEN', '', 'EDSC', fv, ass_br)] += v
            if num == 1 and motiu_etiqueta not in ('PXM', 'ORIPXM'):
                self.adm001a[('ADM001AECAP', up, br, br+'A'+hash[0:6], '', '', 'NUM', '', 'EDSC', fv, ass_br)] += v
            if months <= 1:
                self.adm001m[('ADM001MECAP', up, br, br+'A'+hash[0:6], '', '', 'DEN', '', 'EDSC', fv, ass_br)] += v
                if num == 1 and motiu_etiqueta not in ('PXM', 'ORIPXM'):
                    self.adm001a[('ADM001MECAP', up, br, br+'A'+hash[0:6], '', '', 'NUM', '', 'EDSC', fv, ass_br)] += v
            if tip != 'C9R' and br in self.first_x_up:
                periode_min = self.first_x_up[br]
                if peticio >= dt.datetime.strptime(str(periode_min), '%Y%m%d'):
                    self.adm001a[('ADM001AHES', up, br, br+'A'+hash[0:6], '', '', 'DEN', '', 'EDSC', fv, ass_br)] += v
                    if num == 1 and motiu_etiqueta in ('PXM', 'ORIPXM'):
                        self.adm001a[('ADM001AHES', up, br, br+'A'+hash[0:6], '', '', 'NUM', '', 'EDSC', fv, ass_br)] += v
                    if months <= 1:
                        self.adm001m[('ADM001MHES', up, br, br+'A'+hash[0:6], '', '', 'DEN', '', 'EDSC', fv, ass_br)] += v
                        if num == 1 and motiu_etiqueta in ('PXM', 'ORIPXM'):
                            self.adm001m[('ADM001MHES', up, br, br+'A'+hash[0:6], '', '', 'NUM', '', 'EDSC', fv, ass_br)] += v
        
        for k, v in self.visites2.items():
            br, num, des, sap, amb, months, ass_br, fv, nif, up = k
            hash = h.sha1(nif).hexdigest().upper()
            self.adm002a[('ADM002A', up, br, br+'A'+hash[0:6], '', '', 'DEN', '', 'EDSC', fv, ass_br)] += v
            if num == 1:
                self.adm002a[('ADM002A', up, br, br+'A'+hash[0:6], '', '', 'NUM', '', 'EDSC', fv, ass_br)] += v
            if months <= 1:
                self.adm002m[('ADM002M', up, br, br+'A'+hash[0:6], '', '', 'DEN', '', 'EDSC', fv, ass_br)] += v
                if num == 1:
                    self.adm002m[('ADM002M', up, br, br+'A'+hash[0:6], '', '', 'NUM', '', 'EDSC', fv, ass_br)] += v

        upload = [k + (v,) for k, v in self.adm001a.items()]
        upload = upload + [k + (v,) for k, v in self.adm002a.items()]
        upload = upload + [k + (v,) for k, v in self.adm001m.items()]
        upload = upload + [k + (v,) for k, v in self.adm002m.items()]
        u.listToTable(upload, TB, DB)

    def ADM003(self):
        print('-------ADM003--------')
        # ADM003: Població assignada a una UAS
        """
            Definició: nombre de persones assignades a 
                una Unitat Administrativa Sanitària (UAS)
            - EAP/ Administratius (PAMBITOS?=professional UP+hash)
            - Edat (edatS5)
            - Unitat Administrativa Sanitària (UAS) 
                    (1 adm pot tenir més d'una UAS), UAB??, períodos
        """

        hash_cat = c.defaultdict(c.defaultdict)
        pob_ass_uas = c.Counter()
        ups = set()
        sql = """SELECT up, uas, uba, hash, dni, P02, 
                    P37, P814, A1544, A4564, A6574, A75MORE 
                    FROM dwsisap.CAT_ADMINISTRATIUS_uba"""
        for up, uas, uba, hash, dni, P02, P37, P814, A1544, A4564, A6574, A75MORE in u.getAll(sql, 'exadata'):
            ups.add(up)
            v = P02 + P37 + P814 + A1544 + A4564 + A6574 + A75MORE
            pob_ass_uas[up] +=  v
            hash_cat[(up, uas, uba, hash, dni)]['P02'] = P02
            hash_cat[(up, uas, uba, hash, dni)]['P37'] = P37
            hash_cat[(up, uas, uba, hash, dni)]['P814'] = P814
            hash_cat[(up, uas, uba, hash, dni)]['A1544'] = A1544
            hash_cat[(up, uas, uba, hash, dni)]['A4564'] = A4564
            hash_cat[(up, uas, uba, hash, dni)]['A6574'] = A6574
            hash_cat[(up, uas, uba, hash, dni)]['A75MORE'] = A75MORE

        upload = []
        for key in hash_cat:
            up, uas, uba, hash, dni = key
            if up in self.centres:
                br = self.centres[up]['br']
                if dni in self.fixs: f_v = 'F'
                elif dni in self.variables: f_v = 'V'
                elif dni in self.no_ics: f_v = 'N'
                else: f_v = ''
                c1 = 1 if dni in self.cap1 and self.cap1[dni][0] == up else 0
                for age in hash_cat[key]:
                    upload.append(('ADM003B', up, br, br+'A'+hash, uas, uba, 'NUM', age, age_convert_class(age), f_v, c1, hash_cat[key][age]))
                    # upload.append(('ADM003B', up, br, br+'A'+hash, uas, '', 'DEN', '', 'EDSC', f_v, c1, 1))

        print('assignada_tot')
        pob_ass_eap = {}
        sql = """
            select up, count(1), sum(case when uas != '' then 1 else 0 end) 
                from nodrizas.assignada_tot
                group by up
        """
        for up, den, num  in u.getAll(sql, 'nodrizas'):
            pob_ass_eap[up] = [num, den]
        for up, v in pob_ass_eap.items():
            if up in self.centres:
                br = self.centres[up]['br']
                upload.append(('ADM003A', up, br, br, '', '', 'DEN', '', 'EDSC', '', '', v[1]))
                upload.append(('ADM003A', up, br, br, '', '', 'NUM', '', 'EDSC', '', '', v[0]))
        u.listToTable(upload, TB, DB)

    def ADM00405(self):
        """
        Numerador: Nombre de visites programades per administratius (servei UAC) 
        en la població assignada a una Unitat Administrativa Sanitària (UAS) 
        que ha realitzat el professional administratiu de referència del pacient.
        Denominador: Nombre de visites programades per administratius (servei UAC) 
        en la població assignada a una Unitat Administrativa Sanitària (UAS)
        """
        print('-------ADM004--------')

        print('visites')
        self.pacient_uas_programacio = c.defaultdict(c.Counter)
        self.visites_nif = c.defaultdict(c.Counter)
        num6 = c.Counter()
        sql = """SELECT USUARI, PACIENT, sisap_servei_class, atributs_agenda_cod, 
                    up, sisap_servei_codi
                FROM DWSISAP.SISAP_MASTER_VISITES 
                WHERE 
                    FLAG_4CW  = 0 AND
                    MONTHS_BETWEEN(DATE '{tall}', peticio) BETWEEN 0 AND 1 AND
                    ORIGEN = '{origen}'""".format(
                        tall=TALL if TALL else get_dextraccio(),
                        origen=ORIGEN
                        )
        for login, pacient, servei, agenda, up, codi_servei in u.getAll(sql, 'exadata'):
            if login in self.login_nif:
                nif = self.login_nif[login]
                if nif in self.cap1:
                    if servei in SERVEI_CLASS and agenda in AGENDA_COD:
                        self.pacient_uas_programacio[pacient][nif] += 1
                        if up == self.cap1[nif][0]:
                            self.visites_nif[nif]['NO_ALIE'] += 1
                        else:
                            self.visites_nif[nif]['ALIE'] += 1
                    if codi_servei == 'ASSIR':
                        num6['ADM006A', nif] += 1
                    if codi_servei == 'XSM':
                        num6['ADM006B', nif] += 1
                    if codi_servei == 'ALTESP':
                        num6['ADM006C', nif] += 1
                    if codi_servei in ('FARM', 'TEX', 'REHA'):
                        num6['ADM006D', nif] += 1
                
        print('self.pacient_uas_programacio', len(self.pacient_uas_programacio))

        print('cat_uas')
        uas_nif = {}
        sql = """SELECT NIF, UAS, UP_UAS
                FROM DWSISAP.CAT_ADMINISTRATIUS_UAS"""
        for nif, uas, up in u.getAll(sql, 'exadata'):
            uas_nif[(uas, up)] = nif
        print('uas_nif', len(uas_nif))
        
        print('040')
        ass_pacients = {}
        sql = """SELECT USUA_CIP, USUA_UAB_CODI_UAB, USUA_UAS_CODI_UAS, usua_uab_up,
                    floor(months_between(SYSDATE, to_date(USUA_DATA_NAIXEMENT,'J'))/12)
                    FROM usutb040
                    WHERE usua_situacio = 'A'"""
        for s in u.sectors:
            print(s)
            for usua, uba, uas, up, edat in u.getAll(sql, s):
                if (uas, up) in uas_nif:
                    hash = h.sha1(usua).hexdigest().upper()
                    if edat < 3: age = 'P02'
                    elif edat < 8: age = 'P37'
                    elif edat < 15: age = 'P814'
                    elif edat < 45: age = 'A1544'
                    elif edat < 65: age = 'A4564'
                    elif edat < 75: age = 'A6574'
                    else: age = 'A75MORE'
                    ass_pacients[hash] = (uas_nif[(uas, up)], uas, up, age, uba)
    
        print('ass_pacients', len(ass_pacients))
        # creem indicador ADM004
        # denominador = totes les creades per un administratiu, 
        # num = les q l'administratiu ha programat q té assignades
        print('cuinetes')
        indi004 = c.Counter()
        for pacient in self.pacient_uas_programacio:
            if pacient in ass_pacients:
                nif, uas, up, age, uba = ass_pacients[pacient]
                for admi in self.pacient_uas_programacio[pacient]:
                    indi004[('ADM004', nif, uas, up, uba, age, 'DEN')] += self.pacient_uas_programacio[pacient][admi]
                    if admi == nif:
                        indi004[('ADM004', nif, uas, up, uba, age, 'NUM')] += self.pacient_uas_programacio[pacient][admi]
        print('indi04', len(indi004))

        upload = []
        for k, v in indi004.items():
            if k[3] in self.centres:
                br = self.centres[k[3]]['br']
                hash = h.sha1(k[1]).hexdigest().upper()
                if k[1] in self.fixs: f_v = 'F'
                elif k[1] in self.variables: f_v = 'V'
                elif k[1] in self.no_ics: f_v = 'N'
                else: f_v = ''
                c1 = 1 if k[1] in self.cap1 and self.cap1[k[1]][0] == k[3] else 0
                upload.append((k[0], k[3], br, br+'A'+hash[:6], k[2], k[4], k[6], k[5], age_convert_class(k[5]), f_v, c1, v))
        print(len(upload))
        u.listToTable(upload, TB, DB)

        print('-------ADM005--------')
        upload = []
        # NUM adm005
        for nif in self.visites_nif:
            hash = h.sha1(nif).hexdigest().upper()
            up_ics, f_v = self.cap1[nif]
            if up_ics in self.centres:
                br = self.centres[up_ics]['br']
                for alie in self.visites_nif[nif]:
                    if alie == 'ALIE':
                        upload.append(('ADM005B', up_ics, br, br+'A'+hash[0:6], '', '', 'NUM', '', 'EDSC', f_v, 0, self.visites_nif[nif][alie]))
                    else:
                        upload.append(('ADM005A', up_ics, br, br+'A'+hash[0:6], '', '', 'NUM', '', 'EDSC', f_v, 1, self.visites_nif[nif][alie]))
        # DEN adm005 i adm06
        for nif in self.cap1:
            up, fv = self.cap1[nif]
            if up in self.centres:
                br = self.centres[up]['br']
                hash = h.sha1(nif).hexdigest().upper()
                upload.append(('ADM005A', up, br, br+'A'+hash[:6], '', '', 'DEN', '', 'EDSC', fv, 1, 1))
                upload.append(('ADM005B', up, br, br+'A'+hash[:6], '', '', 'DEN', '', 'EDSC', fv, 0, 1))
                upload.append(('ADM006A', up, br, br+'A'+hash[:6], '', '', 'DEN', '', 'EDSC', fv, 0, 1))
                upload.append(('ADM006B', up, br, br+'A'+hash[:6], '', '', 'DEN', '', 'EDSC', fv, 0, 1))
                upload.append(('ADM006C', up, br, br+'A'+hash[:6], '', '', 'DEN', '', 'EDSC', fv, 0, 1))
                upload.append(('ADM006D', up, br, br+'A'+hash[:6], '', '', 'DEN', '', 'EDSC', fv, 0, 1))
        u.listToTable(upload, TB, DB)

        print('-------ADM006--------')
        upload = []
        for k, v in num6.items():
            indi, nif = k
            if nif in self.cap1:
                up, fv = self.cap1[nif]
                if up in self.centres:
                    br = self.centres[up]['br'] 
                    hash = h.sha1(nif).hexdigest().upper()
                    upload.append((indi, up, br, br+'A'+hash[:6], '', '', 'NUM', '', 'EDSC', fv, 0, v))
        u.listToTable(upload, TB, DB)

    def ADM00708(self):
        print('ADM007')
        print('visites')
        v = ['9C', '9E', '9R', '9T', '9EC', '9D']
        ADM007 = c.Counter()
        ADM008 = c.Counter()
        sql = """SELECT 
                        up,modul,tipus
                    FROM 
                        dwsisap.SISAP_MASTER_VISITES a
                    WHERE
                        MONTHS_BETWEEN(DATE '{tall}', data) BETWEEN 0 AND 1
                        AND SISAP_SERVEI_CODI='UAC' 
                        AND modul<>'4CW' AND SITUACIO='R'
                        AND tipus IN ('9C', '9E', '9R', '9T', '9Ec', '9D')""".format(tall=TALL if TALL else get_dextraccio())
        for up, modul, tipus in u.getAll(sql, 'exadata'):
            ADM007[(up, modul, tipus)] += 1
            ADM008[(up, modul)] += 1
        # EXTRACT (MONTH FROM data)='10' AND extract(YEAR FROM data)='2022'
        
        print('admins')
        admins = {}
        sql = """SELECT nif, up_uas, uas FROM dwsisap.CAT_ADMINISTRATIUS_UAS"""
        for nif, up, uas in u.getAll(sql, 'exadata'):
            admins[(up, uas)] = nif

        print('cuinetes')
        upload = []
        dens = set()
        for k, v in ADM007.items():
            up, modul, tipus = k
            admi = (up, modul)
            if admi in admins:
                nif = admins[admi]
                if nif in self.cap1:
                    up, fv = self.cap1[nif]
                    if up in self.centres:
                        br = self.centres[up]['br'] 
                        hash = h.sha1(nif).hexdigest().upper()
                        if tipus == '9C':
                            dens.add(('ADM007A', up, br, br+'A'+hash[:6], '', '', 'DEN', '', 'EDSC', fv, 0, 1))
                            upload.append(('ADM007A', up, br, br+'A'+hash[:6], '', '', 'NUM', '', 'EDSC', fv, 0, v))
                        elif tipus == '9E':
                            dens.add(('ADM007B', up, br, br+'A'+hash[:6], '', '', 'DEN', '', 'EDSC', fv, 0, 1))
                            upload.append(('ADM007B', up, br, br+'A'+hash[:6], '', '', 'NUM', '', 'EDSC', fv, 0, v))
                        elif tipus == '9R':
                            dens.add(('ADM007C', up, br, br+'A'+hash[:6], '', '', 'DEN', '', 'EDSC', fv, 0, 1))
                            upload.append(('ADM007C', up, br, br+'A'+hash[:6], '', '', 'NUM', '', 'EDSC', fv, 0, v))
                        elif tipus == '9T':
                            dens.add(('ADM007D', up, br, br+'A'+hash[:6], '', '', 'DEN', '', 'EDSC', fv, 0, 1))
                            upload.append(('ADM007D', up, br, br+'A'+hash[:6], '', '', 'NUM', '', 'EDSC', fv, 0, v))
                        elif tipus == '9Ec':
                            dens.add(('ADM007E', up, br, br+'A'+hash[:6], '', '', 'DEN', '', 'EDSC', fv, 0, 1))
                            upload.append(('ADM007E', up, br, br+'A'+hash[:6], '', '', 'NUM', '', 'EDSC', fv, 0, v))
                        elif tipus == '9D':
                            dens.add(('ADM007F', up, br, br+'A'+hash[:6], '', '', 'DEN', '', 'EDSC', fv, 0, 1))
                            upload.append(('ADM007F', up, br, br+'A'+hash[:6], '', '', 'NUM', '', 'EDSC', fv, 0, v))
        
        for k, v in ADM008.items():
            up, modul = k
            admi = (up, modul)
            if admi in admins:
                nif = admins[admi]
                if nif in self.cap1:
                    up, fv = self.cap1[nif]
                    if up in self.centres:
                        br = self.centres[up]['br'] 
                        hash = h.sha1(nif).hexdigest().upper()
                        dens.add(('ADM008', up, br, br+'A'+hash[:6], '', '', 'DEN', '', 'EDSC', fv, 0, 1))
                        upload.append(('ADM008', up, br, br+'A'+hash[:6], '', '', 'NUM', '', 'EDSC', fv, 0, v))

        for d in dens:
            upload.append(d)

        u.listToTable(upload, TB, DB)


if __name__ == "__main__":
    Adm()
    