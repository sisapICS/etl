
import sisapUtils as u
import collections as c

sql = """select per_nom, count(DISTINCT per_usuari)
			from pritb987
			where PER_SISAP = 'S'
			GROUP BY per_nom
		HAVING count(DISTINCT per_usuari)>1"""
persones_n = c.Counter()
for s in u.sectors:
    print(s)
    for nom, nombre in u.getAll(sql, s):
        persones_n[nombre] += 1
print(persones_n)

sql = """SELECT per_usuari, per_up, per_nom FROM pritb987
			where PER_SISAP = 'S'"""
info = c.Counter()
for s in u.sectors:
    print(s)
    for login, up, nom in u.getAll(sql, s):
        info[login] = [up, nom, s]

upload = []    
for login, v in info.items():
	up, nom, s = v
	upload.append((login, up, nom, s))

cols = '(login varchar(20), up varchar(5), nom varchar(90), sector varchar(5))'
u.createTable('admis_sisap', cols, 'gad', rm=True)
u.listToTable(upload, 'admis_sisap', 'gad')
u.createTable('admis_sisap', cols, 'exadata', rm=True)
u.listToTable(upload, 'admis_sisap', 'exadata')
u.grantSelect('admis_sisap', 'DWSISAP_ROL', 'exadata')