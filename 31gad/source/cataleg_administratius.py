# encoding: utf8

import sisapUtils as u
import sisaptools as t
import collections as c
import hashlib as h
import urllib
import ssl
import os

ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
ctx.set_ciphers('HIGH:!DH:!aNULL')

USERS_EXT = {"PREDUJVG": ("03", None), "PREDUMPP": ("02", None),
            "PREDUJCD": ("11", None), "PREDUMER": ("11", None),
            "PREDUCAM": ("11", None), "PREDUNNC": ("00", "C9"),
            "DDIAZA": ("00", "E3"), "PREDUJMS": ("00", "E3"),
            "PREDUDDA": ("00", "E3"), "PREDUMAG": ("00", "D5"),
            "PREDUXMG": ("07", None), "PREDUXGM": ("00", "E5"),
            "PREDUSLA": ("00", "E5"), "PREDUADT": ("00", "A5"),
            "PREDUXCP": ("00", "A1"), "PREDUABO": ("00", "A2"),
            "PREDUMAA": ("05", None), "PREDUGLA": ("06", None),
            "PREDUCMC": ("06", None), "PREDUFFS": ("00", "A6"),
            "PREDUEVC": ("07", None), "PREDUESF": ("00", "C8"),
            "PREDUPGR": ("01", None), "PREDUMBM": ("00", "F0"),
            "PREDUAMA": ("00", "F0"), "PREDUNTN": ("00", "A3"),
            "PREDUJA1": ("05", None), "PREDUJMT": ("02", None)}

def cataleg():
    print('logins actius')
    logins_actius = set()
    sql = """SELECT usuari FROM dwsisap.SISAP_MASTER_VISITES smv 
            WHERE peticio > add_months(SYSDATE,-2)
            AND origen = 'UAC'"""
    for login, in u.getAll(sql, 'exadata'):
        logins_actius.add(login)

    # up, categoria profesisonal, login de redics
    print('redics')
    mare = {}
    sql = """SELECT substr(ide_dni,0,8), ide_codi_up, ide_usuari, ide_categ_prof FROM pritb992
                WHERE IDE_DATA_BAIXA IS NULL 
                AND ide_categ_prof IN ('ADMINISTRATIU','ZELADOR', 'AUXILIAR ADMINISTRATIU')
                AND ide_dni IS NOT null"""
    for dni, up, login, categoria in u.getAll(sql, 'redics'):
        mare[login] = [up, dni, categoria]

    # noms cognoms i dni de sectors
    print('dnis sectors')
    noms = {}
    sql = """SELECT substr(prov_dni_proveidor,1,8), prov_Nom, prov_Cognom1, prov_cognom2  FROM import.cat_pritb031"""
    for dni, nom, cog1, cog2 in u.getAll(sql, 'import'):
        noms[dni] = [nom, cog1, cog2]

    ARXIU_PROFESSIONALS = os.path.join(
        u.tempFolder,
        'professionals.csv'
        )

    URL = "http://p400.ecap.intranet.gencat.cat/sisap/csv/capitol1personaltransposat/df54Ed8easdf/lGFH56dkjhlk"
    urllib.urlretrieve(url=URL, filename=ARXIU_PROFESSIONALS, context=ctx)

    capitol1 = {}
    for (nif, cog1, cog2, nom, br_original, tipus_concepte, categoria, codi_categoria, particions,
                    br1, br1_ponderacio,
                    br2, br2_ponderacio,
                    br3, br3_ponderacio,
                    br4, br4_ponderacio,
                    br5, br5_ponderacio,
                    br6, br6_ponderacio,
                    br7, br7_ponderacio,
                    br8, br8_ponderacio,
                    br9, br9_ponderacio,
                    br10, br10_ponderacio,
                    br11, br11_ponderacio,
                    br12, br12_ponderacio,
                    br13, br13_ponderacio,
                    br14, br14_ponderacio,
                    br15, br15_ponderacio,
                    br16, br16_ponderacio,
                    br17, br17_ponderacio,
                    br18, br18_ponderacio,
                    br19, br19_ponderacio,
                    br20, br20_ponderacio,
                    ) in u.readCSV(
                        ARXIU_PROFESSIONALS,
                        sep=";",
                        header=True):
                    dni_nif = str(nif[1:-1]).replace(" ", "")
                    capitol1[dni_nif] = [br1, tipus_concepte]

    print('ups ics i no ics')
    sql = """select ics_codi, scs_codi, ep from nodrizas.cat_centres"""
    ups= {}
    ups_no_ics = set()
    for br, up, ep in u.getAll(sql, 'nodrizas'):
        ups[br] = up
        if ep != '0208':
            ups_no_ics.add(up)
    
    no_ics = {}
    sql = """SELECT per_nom, max(per_data_alta) FROM import.cat_pritb987
			where PER_SISAP = 'S'
			GROUP BY per_nom"""
    sql2 = """SELECT per_nom, per_up, per_data_alta FROM import.cat_pritb987
			where PER_SISAP = 'S'"""
    sql3 = """SELECT per_nom, per_usuari FROM import.cat_pritb987
			where PER_SISAP = 'S'"""
    usuari_data = {}
    usuari_up = {}
    for nom, data in u.getAll(sql, 'import'):
        usuari_data[nom] = data
    for nom, up, data in u.getAll(sql2, 'import'):
        if data == usuari_data[nom]:
            if up in ups_no_ics:
                usuari_up[nom] = up
    for nom, login in u.getAll(sql3, 'import'):
        if nom in usuari_up:
            no_ics[login] = usuari_up[nom]

    print('upload')
    upload = []
    for login in mare:
        up, nif, categoria = mare[login]
        if nif in noms:
            nom, cog1, cog2 = noms[nif]
        else: nom, cog1, cog2 = '', '', ''
        nif_cap1 = nif if nif[0] in ('0','1','2','3','4','5','6','7','8','9') else nif[1:]
        if nif_cap1 in capitol1:
            up_capitol_1, fix_variable = capitol1[nif_cap1]
        else: 
            if login in no_ics: up_capitol_1, fix_variable = '','N'
            else: up_capitol_1, fix_variable = '',''
        if up_capitol_1 in ups:
            up_ics = ups[up_capitol_1]
        elif login in no_ics: 
            up_ics = no_ics[login]
        else: up_ics = ''
        if login in logins_actius: actiu = 1
        else: actiu = 0
        upload.append((up_capitol_1, up_ics, up, nif, login, nom, cog1, cog2, fix_variable, categoria, actiu))

    # creem i omplim taula
    cols = """(up_capitol_1 varchar(50), up_ics varchar(5), up_login varchar(5), nif varchar(10), login varchar(20),
                nom varchar(100), cognom_1 varchar(100), cognom_2 varchar(100), fix_variable varchar(50),
                categoria varchar(40), actiu int)"""
    u.createTable('cat_administratius', cols, 'exadata', rm=True)
    u.createTable('cat_administratius', cols, 'redics', rm=True)
    u.grantSelect('cat_administratius','DWSISAP_ROL','exadata')
    u.listToTable(upload, 'cat_administratius', 'exadata')
    u.listToTable(upload, 'cat_administratius', 'redics')

    # taula 2: uas
    # de sectors
    print('uas')
    info_uas = c.defaultdict(dict)
    dni_uas = {}
    sql = """SELECT substr(UAS_PROVEIDOR_DNI_PROVEIDOR,0,8), uas_codi_up, uas_codi_uas, UAS_DESCRIPCIO,
                uas_tancar_assig, uas_modul_codi_centre, uas_modul_classe_centre,
                uas_modul_codi_servei, uas_modul_codi_modul, uas_data_carr_ass FROM import.cat_vistb099"""
    for dni, up, uas, uas_desc, uas_tancada_assign, uas_m_codi_centre, uas_m_classe_centre, uas_m_codi_servei, uas_m_codi_modul, uas_data_carr_ass in u.getAll(sql, 'import'):
        info_uas[dni][(up, uas)] = [uas_desc, uas_tancada_assign, uas_m_codi_centre, uas_m_classe_centre, uas_m_codi_servei, uas_m_codi_modul, uas_data_carr_ass] 
        dni_uas[(up,uas)] = dni
    
    print('upload')
    upload = set()
    for login in mare:
        up, nif, categoria = mare[login]
        if nif in info_uas:
            for key in info_uas[nif]:
                up_uas, uas = key[0], key[1]
                uas_desc, uas_tancada_assign, uas_m_codi_centre, uas_m_classe_centre, uas_m_codi_servei, uas_m_codi_modul, uas_data_carr_ass = info_uas[nif][(up_uas, uas)] 
                upload.add((nif, up_uas, uas, uas_desc, uas_tancada_assign, uas_m_codi_centre, uas_m_classe_centre, uas_m_codi_servei, uas_m_codi_modul, uas_data_carr_ass))
    upload1 = []
    for set_e in upload:
        upload1.append(tuple(i for i in set_e))
    # creem i omplim taula
    cols = """(nif varchar(9), up_uas varchar(5),
                uas varchar(5), uas_descripcio varchar(55), uas_tancada_assig varchar(1),
                uas_modul_codi_centre varchar(9), uas_modul_classe_centre varchar(2), uas_modul_codi_servei varchar(5),
                uas_modul_codi_modul varchar(5), uas_data_carr_ass date)"""
    u.createTable('cat_administratius_uas', cols, 'exadata', rm=True)
    u.createTable('cat_administratius_uas', cols, 'redics', rm=True)
    u.grantSelect('cat_administratius_uas','DWSISAP_ROL','exadata')
    u.listToTable(upload1, 'cat_administratius_uas', 'exadata')
    u.listToTable(upload1, 'cat_administratius_uas', 'redics')

    # TAULA UAB
    # info per cada uab assignada a la uas dalta alta i baixa
    print('taula uab')
    print('040')
    uas_uba = c.defaultdict(c.Counter)
    sql = """select uba, uas, up, edat, codi_sector from nodrizas.assignada_tot"""
    for uba, uas, up, age, s in u.getAll(sql, 'nodrizas'):
        if age < 3:
            uas_uba[(up, uas, uba, s)]['P02'] += 1
        elif age < 8:
            uas_uba[(up, uas, uba, s)]['P37'] += 1
        elif age < 15:
            uas_uba[(up, uas, uba, s)]['P814'] += 1
        elif age < 45:
            uas_uba[(up, uas, uba, s)]['A1544'] += 1
        elif age < 65:
            uas_uba[(up, uas, uba, s)]['A4564'] += 1
        elif age < 75:
            uas_uba[(up, uas, uba, s)]['A6574'] += 1 
        else:
            uas_uba[(up, uas, uba, s)]['A75MORE'] += 1 

    print('prof')
    professionals = {}
    sql = """SELECT up, uab, uab_descripcio, nom, cognom1, cognom2
            FROM PROFESSIONALS
            WHERE tipus = 'M'"""
    for up, uba, desc, nom, cog1, cog2 in u.getAll(sql, 'pdp'):
        professionals[(up, uba)] = (desc, nom, cog1, cog2)

    upload = []
    for key in uas_uba:
        if (key[0], key[1]) in dni_uas:
            dni = dni_uas[(key[0], key[1])]
            if dni:
                hash = h.sha1(dni).hexdigest().upper()
                if (key[0], key[2]) in professionals:
                    desc_prof, nom, cog1, cog2 = professionals[(key[0], key[2])]
                else: desc_prof, nom, cog1, cog2 = '', '', '', ''
                if uas_uba[key]['P02']:
                    P02 = uas_uba[key]['P02']
                else: P02 = 0
                if uas_uba[key]['P37']:
                    P37 = uas_uba[key]['P37']
                else: P37 = 0
                if uas_uba[key]['P814']:
                    P814 = uas_uba[key]['P814']
                else: P814 = 0
                if uas_uba[key]['A1544']:
                    A1544 = uas_uba[key]['A1544']
                else: A1544 = 0
                if uas_uba[key]['A4564']:
                    A4564 = uas_uba[key]['A4564']
                else: A4564 = 0
                if uas_uba[key]['A6574']:
                    A6574 = uas_uba[key]['A6574']
                else: A6574 = 0
                if uas_uba[key]['A75MORE']:
                    A75MORE = uas_uba[key]['A75MORE']
                else: A75MORE = 0
                upload.append((key[3], key[0], dni, hash[:6], key[1], key[2], desc_prof, nom, cog1, cog2, P02, P37, P814, A1544, A4564, A6574, A75MORE))
    # creem i omplim taula
    cols = """(sector varchar(5), up varchar(5), dni varchar(9), hash varchar(6),
                uas varchar(5), uba varchar(5), uba_descripcio varchar(55),
                nom varchar(15), cognom1 varchar(20), cognom2 varchar(20), 
                P02 int, P37 int, P814 int, A1544 int, 
                A4564 int, A6574 int, A75MORE int)"""
    u.createTable('cat_administratius_uba', cols, 'exadata', rm=True)
    u.createTable('cat_administratius_uba', cols, 'redics', rm=True)
    u.grantSelect('cat_administratius_uba','DWSISAP_ROL','exadata')
    u.listToTable(upload, 'cat_administratius_uba', 'exadata')
    u.listToTable(upload, 'cat_administratius_uba', 'redics')

    for user, (amb, sap) in USERS_EXT.items():
            zona = amb if not sap else "".join((amb, sap))
            view = "".join(('cat_administratius_uba', zona))
            me = "PREDUFFA"
            with t.Database("redics", "data") as redics:
                sql = """create or replace view {0} as 
                            select 
                                a.up_cod, a.up_des eap, a.sap_cod, 
                                a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                            from 
                                sisap_covid_cat_up a, {3} cat
                            where 
                                a.up_cod = cat.up
                                and a.amb_cod = '{1}'"""
                if sap:
                    sql += " and sap_cod = '{2}'"
                if user in ("PREDUMPP", "PREDUJMT"):
                    sql += """ union all 
                            select 
                                a.up_cod, a.up_des eap, a.sap_cod, 
                                a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                            from 
                                sisap_covid_cat_up a, {3} cat
                            where 
                                a.up_cod = cat.up
                                and a.amb_cod = '00'
                                and sap_cod in ('B6', 'D8', 'E6', 'F4') """
                elif user in ("PREDUMBM", "PREDUAMA"):
                    sql += """ union all 
                            select 
                                a.up_cod, a.up_des eap, a.sap_cod, 
                                a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                            from 
                                sisap_covid_cat_up a, {3} cat
                            where 
                                a.up_cod = cat.up
                                and a.amb_cod = '00'
                                and sap_cod = 'F2'"""
                redics.execute(sql.format(view, amb, sap, 'cat_administratius_uba'))
                redics.set_grants("select", view, user)
                redics.set_grants("select", view, me)
    
    for user, (amb, sap) in USERS_EXT.items():
        zona = amb if not sap else "".join((amb, sap))
        view = "".join(('cat_administratius_uas', zona))
        me = "PREDUFFA"
        with t.Database("redics", "data") as redics:
            sql = """create or replace view {0} as 
                        select 
                            a.up_cod, a.up_des eap, a.sap_cod, 
                            a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                        from 
                            sisap_covid_cat_up a, {3} cat
                        where 
                            a.up_cod = cat.up_uas
                            and a.amb_cod = '{1}'"""
            if sap:
                sql += " and sap_cod = '{2}'"
            if user in ("PREDUMPP", "PREDUJMT"):
                sql += """ union all 
                        select 
                            a.up_cod, a.up_des eap, a.sap_cod, 
                            a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                        from 
                            sisap_covid_cat_up a, {3} cat
                        where 
                            a.up_cod = cat.up_uas
                            and a.amb_cod = '00'
                            and sap_cod in ('B6', 'D8', 'E6', 'F4') """
            elif user in ("PREDUMBM", "PREDUAMA"):
                sql += """ union all 
                        select 
                            a.up_cod, a.up_des eap, a.sap_cod, 
                            a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                        from 
                            sisap_covid_cat_up a, {3} cat
                        where 
                            a.up_cod = cat.up_uas
                            and a.amb_cod = '00'
                            and sap_cod = 'F2'"""
            redics.execute(sql.format(view, amb, sap, 'cat_administratius_uas'))
            redics.set_grants("select", view, user)
            redics.set_grants("select", view, me)
    for user, (amb, sap) in USERS_EXT.items():
        zona = amb if not sap else "".join((amb, sap))
        view = "".join(('cat_administratius', zona))
        me = "PREDUFFA"
        with t.Database("redics", "data") as redics:
            sql = """create or replace view {0} as 
                        select 
                            a.up_cod, a.up_des eap, a.sap_cod, 
                            a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                        from 
                            sisap_covid_cat_up a, {3} cat
                        where 
                            a.up_cod = cat.up_ics
                            and a.amb_cod = '{1}'"""
            if sap:
                sql += " and sap_cod = '{2}'"
            if user in ("PREDUMPP", "PREDUJMT"):
                sql += """ union all 
                        select 
                            a.up_cod, a.up_des eap, a.sap_cod, 
                            a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                        from 
                            sisap_covid_cat_up a, {3} cat
                        where 
                            a.up_cod = cat.up_ics
                            and a.amb_cod = '00'
                            and sap_cod in ('B6', 'D8', 'E6', 'F4') """
            elif user in ("PREDUMBM", "PREDUAMA"):
                sql += """ union all 
                        select 
                            a.up_cod, a.up_des eap, a.sap_cod, 
                            a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                        from 
                            sisap_covid_cat_up a, {3} cat
                        where 
                            a.up_cod = cat.up_ics
                            and a.amb_cod = '00'
                            and sap_cod = 'F2'"""
            redics.execute(sql.format(view, amb, sap, 'cat_administratius'))
            redics.set_grants("select", view, user)
            redics.set_grants("select", view, me)

def historic():
    sql = """select datediff(current_date, data_ext), day(data_ext), month(data_ext), year(data_ext) from nodrizas.dextraccio"""
    for diff, d, m, y in u.getAll(sql, 'nodrizas'):
        difference = diff
        month = m
        year = y
        day = d
    print(difference, month, year)
    # només actualitzem si la data d'extracció és, com a molt, 20 dies anterior a la d'avui
    if difference <= 20:
        sql = """SELECT EXTRACT(MONTH FROM max(data)), EXTRACT(year FROM max(data)) FROM dwsisap.cat_administratius_h"""
        for m, y in u.getAll(sql, 'exadata'):
            last_m = m
            last_y = y
        # En cas que carreguem de nou un mes eliminarem els possibles registres d'aquest abans.
        if last_m == month and last_y == year:
            bd = "exadata"
            conn = u.connect(bd)
            c = conn.cursor()
            for taula in ("cat_administratius_h", "cat_administratius_uas_h", "cat_administratius_uba_h"):
                sql = """DELETE FROM {} 
                         WHERE EXTRACT(MONTH FROM data) = {} 
                         AND EXTRACT(year FROM data) = {}""".format(taula, month, year)
                print(sql)
                c.execute(sql)
                conn.commit()
            conn.close()
            bd = "redics"
            conn = u.connect(bd)
            c = conn.cursor()
            for taula in ("cat_administratius_h", "cat_administratius_uas_h", "cat_administratius_uba_h"):
                sql = """DELETE FROM {} 
                         WHERE EXTRACT(MONTH FROM data) = {} 
                         AND EXTRACT(year FROM data) = {}""".format(taula, month, year)
                print(sql)
                c.execute(sql)
                conn.commit()
            conn.close()

        upload = []
        sql = """select a.*, TRUNC(to_date('{}-{}-{}', 'DD-MM-YYYY')) from dwsisap.cat_administratius a""".format(day, month, year)
        for up_c, up, up_log, nif, log, nom, cog1, cog2, fv, cat, a, d in u.getAll(sql, 'exadata'):
            upload.append((up_c, up, up_log, nif, log, nom, cog1, cog2, fv, cat, a, d))
        u.listToTable(upload, 'cat_administratius_h', 'exadata')
        u.listToTable(upload, 'cat_administratius_h', 'redics')

        upload1 = []
        sql = """select a.*, TRUNC(to_date('{}-{}-{}', 'DD-MM-YYYY')) from dwsisap.cat_administratius_uas a""".format(day, month, year)
        for nif, up, uas, desc, ta, codi, classe, servei, modul, dat1, data in u.getAll(sql, 'exadata'):
            upload1.append((nif, up, uas, desc, ta, codi, classe, servei, modul, dat1, data))
        u.listToTable(upload1, 'cat_administratius_uas_h', 'exadata')
        u.listToTable(upload1, 'cat_administratius_uas_h', 'redics')

        upload2 = []
        sql = """select a.*, TRUNC(to_date('{}-{}-{}', 'DD-MM-YYYY')) from dwsisap.cat_administratius_uba a""".format(day, month, year)
        for sector, up, dni, hash, uas, uba, uba_desc, nom, cog1, cog2, p02, p37, p814, a1544, a4564, a6574, a75more, data in u.getAll(sql, 'exadata'):
            upload2.append((sector, up, dni, hash, uas, uba, uba_desc, nom, cog1, cog2, p02, p37, p814, a1544, a4564, a6574, a75more, data))
        u.listToTable(upload2, 'cat_administratius_uba_h', 'exadata')
        u.listToTable(upload2, 'cat_administratius_uba_h', 'redics')

        for user, (amb, sap) in USERS_EXT.items():
            zona = amb if not sap else "".join((amb, sap))
            view = "".join(('cat_administratius_uba_h', zona))
            me = "PREDUFFA"
            with t.Database("redics", "data") as redics:
                sql = """create or replace view {0} as 
                            select 
                                a.up_cod, a.up_des eap, a.sap_cod, 
                                a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                            from 
                                sisap_covid_cat_up a, {3} cat
                            where 
                                a.up_cod = cat.up
                                and a.amb_cod = '{1}'"""
                if sap:
                    sql += " and sap_cod = '{2}'"
                if user in ("PREDUMPP", "PREDUJMT"):
                    sql += """ union all 
                            select 
                                a.up_cod, a.up_des eap, a.sap_cod, 
                                a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                            from 
                                sisap_covid_cat_up a, {3} cat
                            where 
                                a.up_cod = cat.up
                                and a.amb_cod = '00'
                                and sap_cod in ('B6', 'D8', 'E6', 'F4') """
                elif user in ("PREDUMBM", "PREDUAMA"):
                    sql += """ union all 
                            select 
                                a.up_cod, a.up_des eap, a.sap_cod, 
                                a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                            from 
                                sisap_covid_cat_up a, {3} cat
                            where 
                                a.up_cod = cat.up
                                and a.amb_cod = '00'
                                and sap_cod = 'F2'"""
                redics.execute(sql.format(view, amb, sap, 'cat_administratius_uba_h'))
                redics.set_grants("select", view, user)
                redics.set_grants("select", view, me)
    
    for user, (amb, sap) in USERS_EXT.items():
        zona = amb if not sap else "".join((amb, sap))
        view = "".join(('cat_administratius_uas_h', zona))
        me = "PREDUFFA"
        with t.Database("redics", "data") as redics:
            sql = """create or replace view {0} as 
                        select 
                            a.up_cod, a.up_des eap, a.sap_cod, 
                            a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                        from 
                            sisap_covid_cat_up a, {3} cat
                        where 
                            a.up_cod = cat.up_uas
                            and a.amb_cod = '{1}'"""
            if sap:
                sql += " and sap_cod = '{2}'"
            if user in ("PREDUMPP", "PREDUJMT"):
                sql += """ union all 
                        select 
                            a.up_cod, a.up_des eap, a.sap_cod, 
                            a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                        from 
                            sisap_covid_cat_up a, {3} cat
                        where 
                            a.up_cod = cat.up_uas
                            and a.amb_cod = '00'
                            and sap_cod in ('B6', 'D8', 'E6', 'F4') """
            elif user in ("PREDUMBM", "PREDUAMA"):
                sql += """ union all 
                        select 
                            a.up_cod, a.up_des eap, a.sap_cod, 
                            a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                        from 
                            sisap_covid_cat_up a, {3} cat
                        where 
                            a.up_cod = cat.up_uas
                            and a.amb_cod = '00'
                            and sap_cod = 'F2'"""
            redics.execute(sql.format(view, amb, sap, 'cat_administratius_uas_h'))
            redics.set_grants("select", view, user)
            redics.set_grants("select", view, me)
    for user, (amb, sap) in USERS_EXT.items():
        zona = amb if not sap else "".join((amb, sap))
        view = "".join(('cat_administratius_h', zona))
        me = "PREDUFFA"
        with t.Database("redics", "data") as redics:
            sql = """create or replace view {0} as 
                        select 
                            a.up_cod, a.up_des eap, a.sap_cod, 
                            a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                        from 
                            sisap_covid_cat_up a, {3} cat
                        where 
                            a.up_cod = cat.up_ics
                            and a.amb_cod = '{1}'"""
            if sap:
                sql += " and sap_cod = '{2}'"
            if user in ("PREDUMPP", "PREDUJMT"):
                sql += """ union all 
                        select 
                            a.up_cod, a.up_des eap, a.sap_cod, 
                            a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                        from 
                            sisap_covid_cat_up a, {3} cat
                        where 
                            a.up_cod = cat.up_ics
                            and a.amb_cod = '00'
                            and sap_cod in ('B6', 'D8', 'E6', 'F4') """
            elif user in ("PREDUMBM", "PREDUAMA"):
                sql += """ union all 
                        select 
                            a.up_cod, a.up_des eap, a.sap_cod, 
                            a.sap_des sap, a.amb_cod, a.amb_des ambit, cat.*
                        from 
                            sisap_covid_cat_up a, {3} cat
                        where 
                            a.up_cod = cat.up_ics
                            and a.amb_cod = '00'
                            and sap_cod = 'F2'"""
            redics.execute(sql.format(view, amb, sap, 'cat_administratius_h'))
            redics.set_grants("select", view, user)
            redics.set_grants("select", view, me)
    

if __name__ == '__main__':
    cataleg()
    if u.IS_MENSUAL:
        historic()
