# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c

# CONTINGUT DE LES VISITES


class Contingut_visites():
    def __init__(self):
        # self.get_visits_its()
        self.get_peticions_analitiques()
        # self.get_prescripcions()
        # self.get_dx()
        # self.get_proves()
        # self.get_derivacions()
    
    def get_visits_its(self):
        # ITs
        """
        Afegirem 3 tipus de caus�stiques per les ITs
        1. visites on data visita = data de baixa
        2. visites on data baixa = data petici� visita
        3. visites on data alta = data visita

        output:
        taula amb id visita i tipus de relaci� IT 
        (n�mero de 1 a 3 d'acord amb classificaci� anterior)
        """
        cols = """(visi_id varchar2(17), relacio number)"""
        u.createTable('tcues_it', cols, 'exadata')
        u.grantSelect('tcues_it', 'DWSISAP_ROL', 'exadata')
        baixes = c.defaultdict(set)
        its = """SELECT hash, data_alta, data_baixa FROM dwsisap.MASTER_IT a 
                INNER JOIN dwsisap.DBC_POBLACIO dp 
                ON a.cip = dp.cip
                WHERE EXTRACT(YEAR FROM data_alta) = 2023 OR EXTRACT(YEAR FROM data_baixa) = 2023"""
        for hash, data_alta, data_baixa in u.getAll(its, 'exadata'):
            baixes[hash].add((data_alta, data_baixa))
        upload = []
        visites = """SELECT pacient, DATA, VISI_ID, PETICIO  FROM dwsisap.VISITES_TCUES """
        for pacient, data, visi_id, peticio in u.getAll(visites, 'exadata'):
            if pacient in baixes:
                rel = 0
                for (data_alta, data_baixa) in baixes[pacient]:
                    if data == data_baixa: rel = 1
                    elif data_baixa == peticio: rel = 2
                    elif data_alta == data: rel = 3
                if rel != 0:
                    upload.append((visi_id, rel))
        u.listToTable(upload, 'tcues_it', 'exadata')
    
    def get_peticions_analitiques(self):
        """peticions d'anal�tiques, hist�riques disponibles. 
            No hi ha manera de lligar directament amb id_visita, 
            podem lligar per data i hora i/o data servei 
            (en cas que un pacient tingui m�s d'una visita en un dia, 
            a quina contribuir la petici�).
            podem lligar labtb200 amb visites_tcues per num_peticio = pet_etiqueta and data = pet_data_ext and rel_peticio = pet_relacio_deriv
        """
        cols = """(visi_id varchar2(17), relacio number)"""
        u.createTable('tcues_peticions_analitiques', cols, 'exadata', rm=True)
        u.grantSelect('tcues_peticions_analitiques', 'DWSISAP_ROL', 'exadata')
        visites = {}
        sql = """SELECT visi_id, num_peticio, DATA, REL_PETICIO FROM dwsisap.visites_tcues
                WHERE num_peticio IS NOT NULL and DATA IS NOT NULL and REL_PETICIO IS NOT null"""
        for id, peticio, data, rel_peticio in u.getAll(sql, 'exadata'):
            visites[(str(peticio), data, str(rel_peticio))] = id
        sql1 = """SELECT pet_numpet, pet_etiqueta, pet_data_ext, pet_relacio_deriv FROM labtb200                        
                    WHERE pet_data_ext BETWEEN DATE '2023-01-01' AND DATE '2024-01-01'
                    AND pet_etiqueta IS NOT NULL AND pet_data_ext IS NOT NULL AND pet_relacio_deriv IS NOT null  """
        for sector in u.sectors:
            upload = []
            print(sector)
            sql = """SELECT cpt_codi_protocol FROM labtb125"""
            protocols = set([protocol for protocol, in u.getAll(sql, sector)])
            print(1)
            sql = """SELECT pro_numpet, pro_codi_protocol FROM labtb202"""
            pets = set()
            i = 0
            for numpet, protocol in u.getAll(sql, sector):
                if protocol in protocols:
                    pets.add(numpet)
            print(2)
            for peticio, etiqueta, data, relacio in u.getAll(sql1, sector):
                i += 1
                if i % 10**5 == 0:
                    print(i)
                if peticio in pets:
                    if (etiqueta, data, relacio) in visites:
                        upload.append((visites[(etiqueta, data, relacio)], 1))
            print(len(upload))
            u.listToTable(upload, 'tcues_peticions_analitiques', 'exadata')

    def get_prescripcions(self):
        u.printTime('presc')
        """
        cal mirar com lligar-ho amb visites, hi ha la data de la prescripci� 
        per� no sabem veure com va el tema de les prescripcions cr�niques, 
        quan renoves una prescripci� creiem que es modifica el registre enlloc 
        de generar-ne un de nou. 
        Pel tema de les cr�niques podr�em obtenir auditoria de canvis a partir 
        de la ppftb816
        """
        sql = """
            SELECT visi_id, a.up, p.ide_numcol, a.data
            FROM dwsisap.visites_tcues a
            INNER JOIN dwsisap.professionals p
            on a.professional = p.ide_dni
            INNER JOIN dwsisap.pdptb101_relacio_nia b
            ON a.pacient = b.hash_covid
            INNER JOIN dwtw.prescripcions c
            ON b.cip = c.pmc_usuari_cip
            AND a.up = c.pmc_amb_cod_up
            AND p.ide_numcol = c.pmc_amb_num_col
            AND a.data = c.data_inici
            WHERE extract(year from c.data_inici) = 2023
        """
        upload = [row for row in u.getAll(sql, 'exadata')]
        cols = """(visi_id varchar2(17), up varchar2(10), num_col varchar2(20), data date)"""
        u.createTable('tcues_prescripcions', cols, 'exadata', rm=True)
        u.grantSelect('tcues_prescripcions', 'DWSISAP_ROL', 'exadata')
        u.listToTable(upload, 'tcues_prescripcions', 'exadata')
        u.printTime('fi')
    
    def get_dx(self):
        u.printTime('dx')
        """lligar de forma directa amb visites (visi_numero_visita, centre, classe, servei, m�dul, data,...)"""
        """
            taula prstb015, columnes x lligar: pr_v_cen, pr_v_cla, pr_v_ser, pr_v_mod, pr_v_dat
        """
        sql = """
            SELECT a.visi_id, a.centre_codi, a.centre_classe, a.servei, a.modul, a.data
            FROM dwsisap.visites_tcues a
            INNER JOIN dwsisap.pdptb101_relacio_nia b
            ON a.pacient = b.hash_covid
            INNER JOIN dwtw.problemes c
            on b.cip = c.id
            and a.centre_classe = c.centre_classe
            and a.centre_codi = c.centre_codi
            and a.servei = c.servei
            and a.modul = c.modul
            and a.data = to_date(c.data_visita, 'J')
            WHERE extract(year from to_date(c.data_visita, 'J')) = 2023
        """
        upload = [row for row in u.getAll(sql, 'exadata')]
        cols = """(visi_id varchar2(17), centre_codi varchar2(9), centre_classe varchar2(2), servei varchar2(5), modul varchar2(5), data date)"""
        u.createTable('tcues_dx', cols, 'exadata', rm=True)
        u.grantSelect('tcues_dx', 'DWSISAP_ROL', 'exadata')
        u.listToTable(upload, 'tcues_dx', 'exadata')
    
    def get_proves(self):
        """proves demanades: m�ster de proves, tenim data, hora i professional que ho demana"""
        cols = """(visi_id varchar2(17), pacient varchar2(40), data date, professional varchar2(9))"""
        u.createTable('tcues_proves', cols, 'exadata', rm=True)
        u.grantSelect('tcues_proves', 'DWSISAP_ROL', 'exadata')
        sql = """
            SELECT a.visi_id, pacient, data, professional
            FROM dwsisap.visites_tcues a
            INNER JOIN dwsisap.master_proves b
            ON a.pacient = b.hash
            AND a.data = b.oc_data
            AND a.professional = b.oc_dni_professional
            WHERE EXTRACT(YEAR FROM oc_data) = 2023
        """
        upload = [row for row in u.getAll(sql, 'exadata')]
        u.listToTable(upload, 'tcues_proves', 'exadata')

    
    def get_derivacions(self):
        """m�ster de derivacions, tenim data, hora i professional que ho demana"""
        cols = """(visi_id varchar2(17), pacient varchar2(40), data date, professional varchar2(9))"""
        u.createTable('tcues_derivacions', cols, 'exadata', rm=True)
        u.grantSelect('tcues_derivacions', 'DWSISAP_ROL', 'exadata')
        sql = """
            SELECT  a.visi_id, pacient, data, professional
            FROM dwsisap.visites_tcues a
            INNER JOIN dwsisap.master_derivacions b
            ON a.pacient = b.hash
            AND a.data = b.oc_data
            AND a.professional = b.oc_dni_professional
            WHERE EXTRACT(YEAR FROM oc_data) = 2023
        """
        upload = [row for row in u.getAll(sql, 'exadata')]
        u.listToTable(upload, 'tcues_derivacions', 'exadata')


# Temps de les visites:  pritb700   
# hem vist que aqu� es guarda la informaci� 
# dels accessos a panatlles. En Francesc no acaba de recordar 
# del tot en quins processos es va usar


if __name__ == "__main__":
    Contingut_visites()