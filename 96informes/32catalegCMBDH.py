from sisapUtils import readCSV, createTable, listToTable, calcStatistics, grantSelect


file = 'CIM9MC_2014_2015.csv'  # convertir i eliminar primera fila excel original mmp 18-12-15 13:52h.
tables = {'': '(cod_cat varchar2(1), cod_cod varchar2(6), cod_des varchar2(2000), cod_grup varchar(2), cod_agr varchar2(5))',
          'grup': '(grup_cat varchar2(1), grup_cod varchar2(2), grup_des varchar2(2000))',
          'agr': '(agr_cat varchar2(1), agr_cod varchar2(5), agr_des varchar2(2000), agr_tip varchar2(2))',
          'agr_tip': '(agr_tip_cat varchar2(1), agr_tip_cod varchar2(2), agr_tip_des varchar2(2000))'}
upload = {key: {} for key in tables}
db = 'redics'


for _accio, _versio, _vigencia, tipus, cod_cod, cod_des, _curta, _0a, _0b, _0c, _0d, _0e, _0f, _0g, _0h, _0i, _0j, _0k, _0l, _0m, _0n, _0o, _0p, _0q, gran_cod, gran_des, agr_cod, agr_des, tip_cod, tip_des in readCSV(file, sep=';'):
    upload[''][(tipus, cod_cod)] = (cod_des, gran_cod, agr_cod,)
    if gran_cod:
        upload['grup'][(tipus, gran_cod)] = (gran_des,)
    if agr_cod:
        upload['agr'][(tipus, agr_cod)] = (agr_des, tip_cod,)
    if tip_cod:
        upload['agr_tip'][(tipus, tip_cod)] = (tip_des,)

for key, script in tables.items():
    table = 'SISAP_CT_CMBDH{}'.format('_{}'.format(key) if key else '')
    createTable(table, script, db, rm=True)
    data = [k + v for k, v in upload[key].items()]
    listToTable(data, table, db)
    calcStatistics(table, db)
    grantSelect(table, ('PREDUMMP','PREDUEDT'), db)
