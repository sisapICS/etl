from sisapUtils import readCSV, tempFolder, writeCSV, ftp_upload
from collections import defaultdict
from os import listdir
from os.path import isfile, join


cuentas = defaultdict(list)
for cuenta, num, den in readCSV('d:/sisap/80eqdm2/dades_noesb/conversio_cuentas.csv', sep=';'):
    cuentas[num].append([cuenta, 'NUM'])
    cuentas[den].append([cuenta, 'DEN'])

for file in listdir(tempFolder):
    if isfile(join(tempFolder, file)) and file[:5] == 'EQDM2' and 'NOU' not in file:
        output = []
        file_period = 'A{}{}'.format(file[11:13], file[6:8])
        new_name = file.replace('EQDM2_', 'EQDM2_NOU_')
        for cuenta, period, br, conc, edat, comb, sexe, _n, n in readCSV(tempFolder + file, sep='{'):
            if cuenta.upper() in cuentas:
                for cuenta_n, conc_n in cuentas[cuenta.upper()]:
                    output.append([cuenta_n, file_period, br, conc_n, edat, comb, sexe, _n, n])
        writeCSV(tempFolder + new_name, output, sep='{')
        ftp_upload('10.80.217.112', 'ftpecap', 'UsR3C1ap', tempFolder, new_name, rm=True)
