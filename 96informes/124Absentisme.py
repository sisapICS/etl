# coding: iso-8859-1
#es basa en arxiu 109VisitesRealitzades.py

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

debug = False

#visites realitzades 2016 i 2017

odoits = {}

sql = "select distinct s_codi_servei,s_descripcio from cat_pritb103 where s_espe_codi_especialitat='05999'"
for servei, desc in getAll(sql, 'import'):
    odoits[servei] = True
sql = "select distinct s_codi_servei,s_descripcio from cat_pritb103 where s_descripcio like ('%ODON%')"
for servei, desc in getAll(sql, 'import'):
    odoits[servei] = True

centres = {scs_c: (ics_c, amb_d, sap_d, ics_d) for (scs_c, ics_c, amb_d, sap_d, ics_d) in getAll("select scs_codi, ics_codi, amb_desc, sap_desc, ics_desc from cat_centres where ep='0208'", 'nodrizas')}
moduls = {}
for sec, cen, cla, ser, cod in getAll("select codi_sector, modu_centre_codi_centre, modu_centre_classe_centre, modu_servei_codi_servei, modu_codi_modul from cat_vistb027 where modu_codi_uab <> '' ", 'import'):
    moduls[(sec, cen, cla, ser, cod)] = True
 
for sec, cen, cla, ser, cod in getAll("select codi_sector, modu_centre_codi_centre, modu_centre_classe_centre, modu_servei_codi_servei, modu_codi_modul from cat_vistb027", 'import'):
    if ser in odoits:
        moduls[(sec, cen, cla, ser, cod)] = True
 
def get_servei(servei):

    if servei == 'MG':
        serdesc = 'Medicina Familia'
    elif servei == 'PED':
        serdesc = 'Pediatria'
    elif servei in ('INFP', 'INFPD'):
        serdesc = 'Infermeria pediatria'
    elif servei in ('INF','INFMG','ENF','INFG','ATS','GCAS'):
        serdesc = 'Infermeria'
    elif servei in ('ODO','ODN','ODN-P','ODON','RNODN','ODO2','EXO','HODO','00492','ODN2'):
        serdesc = 'Odontologia'
    elif servei in ('DTS','ASS','TSOC','TS','AS','ASO','ASI','ASIS','SS','RNAS','AASS','05999'):
        serdesc = 'Treball social'
    else:
        serdesc = 'ALTRES'
        
    return serdesc

programades = Counter()
realitzades = Counter()
realitzades_qc = Counter()
h48 = Counter()
hmes48 = Counter()
h48r = Counter()
hmes48r = Counter()
dies_setmanaprog = Counter()
dies_setmanafet = Counter()

for table in getSubTables('visites'):
    dat, = getOne('select year(visi_data_visita) from {} limit 1'.format(table), 'import')
    if dat == 2016 or dat == 2017:
        sql = "select  codi_sector, visi_centre_codi_centre, visi_centre_classe_centre,visi_servei_codi_servei,visi_modul_codi_modul, visi_data_visita, extract(year from visi_data_visita), extract(year_month from visi_data_visita), visi_dia_peticio,visi_up, visi_situacio_visita \
               from {}, nodrizas.dextraccio where visi_data_visita <= data_ext and visi_data_baixa = 0 {}".format(table, ' limit 10' if debug else '')
        for sec, cen, cla, ser, cod,data, anys, periode,peti, up,sit in getAll(sql, 'import'):
            if up in centres:
                amb = centres[up][1]
                if (sec, cen, cla, ser, cod) in moduls:
                    servdesc = get_servei(ser)
                    dia = data.weekday()
                    programades[amb, periode, servdesc] += 1
                    dies_setmanaprog[anys, dia, servdesc] += 1
                    if sit == 'R':
                        realitzades[amb, periode, servdesc] += 1
                        realitzades_qc[amb, periode, servdesc] += 1
                        dies_setmanafet[anys, dia, servdesc] += 1
                    elif sit == 'P':
                        realitzades_qc[amb, periode, servdesc] += 1
                    b = daysBetween(peti,data)
                    if 0 <= b <= 2:
                        h48[amb, periode, servdesc] += 1
                        if sit == 'R':
                            h48r[amb, periode, servdesc] += 1
                    else:
                        hmes48[amb, periode, servdesc] += 1
                        if sit == 'R':
                            hmes48r[amb, periode, servdesc] += 1
                    
                    
                    
            
            
data_file = []
for (amb, periode, servdesc), count in programades.items():
    fetes = realitzades[amb, periode, servdesc]
    fetes_qc = realitzades_qc[amb, periode, servdesc]
    menys48H = h48[amb, periode, servdesc]
    mes48H = hmes48[amb, periode, servdesc]
    menys48Hr = h48r[amb, periode, servdesc]
    mes48Hr = hmes48r[amb, periode, servdesc]
    data_file.append([amb, periode, servdesc, fetes, fetes_qc,count,menys48H, mes48H,menys48Hr, mes48Hr])

if data_file:
    file = tempFolder + 'Visites_realitzades_2017_2016.txt'
    writeCSV(file, data_file, sep=';')
    
data_file = []
for (anys, dia, servdesc), count in dies_setmanaprog.items():
    fetes = dies_setmanafet[anys, dia, servdesc]
    data_file.append([anys, dia, servdesc, fetes, count])

if data_file:
    file = tempFolder + 'Visites_realitzades_perDia_2017_2016.txt'
    writeCSV(file, data_file, sep=';')