#rosa morral (ion 2957), agost14, francesc

from sisapUtils import *
import time,csv
from time import strftime

conn= {'my':connect('import'),'ora':connect('redics')}
cursor= {'my':conn['my'].cursor(),'ora':conn['ora'].cursor()}

debug= False
debugging= ['43527','6837']
contador= {}
debugDescuadre= False

##########

print 'pob: ' + strftime("%Y-%m-%d %H:%M:%S")
amb,sap= {},{}
cursor['my'].execute("select distinct scs_codi,amb_codi,sap_codi from nodrizas.cat_centres where ep='0208'")
for u,a,s in cursor['my']:
    amb[u]= a
    sap[u]= s
pob= {}
contador['poblacio']= 0
cursor['my'].execute("select id_cip_sec,up,edat_a,sexe from nodrizas.ped_assignada")
for id,up,edat,sexe in cursor['my']:
    try:
        amb[up]
    except KeyError:
        continue
    pob[int(id)]= {'fitxa':0,'edat':int(edat),'sexe':sexe,'ambit':amb[up],'sap':sap[up],'eap':up}
    contador['poblacio']+= 1
    
##########
    
print 'fitxa: ' + strftime("%Y-%m-%d %H:%M:%S")
pre,needed= {},{}
contador['fitxesTotal']= 0
cursor['ora'].execute("select cfd_cod||'$'||codi_sector,cfd_cip||'$'||codi_sector,cfd_ind_caod,cfd_ind_cod from prstb505{0}".format(" where cfd_cod='{0}' and codi_sector='{1}'".format(*debugging) if debug else ""))
for id,hash,cao,co in cursor['ora']:
    pre[id]= {'hash':hash,'cao':cao,'co':co}
    needed[hash]= True
    contador['fitxesTotal']+= 1

##########

file= tempFolder + 'odn2del.txt'
try:
    with open(file,'rb') as f:
        pass
except IOError:
    print 'file: ' + strftime("%Y-%m-%d %H:%M:%S")
    cursor['ora'].execute("select hash_a||'$'||codi_sector,id_cip_sec from sisap_u11")
    with open(file,'wb') as f:
        w= csv.writer(f, delimiter='@')
        for hash,id in cursor['ora']:
            try:
                needed[hash]
                pob[id]
            except KeyError:
                continue
            w.writerow([hash,id])

print 'id: ' + strftime("%Y-%m-%d %H:%M:%S")
cip= {}
with open(file,'rb') as f:
    r= csv.reader(f, delimiter='@')
    for hash,id in r:
        cip[hash]= int(id)

odn= {}
contador['fitxesInfantils']= 0
for key in pre.keys():
    try:
        id= cip[pre[key]['hash']]
    except KeyError:
        continue
    pre[key]['id']= id
    pob[id]['fitxa']+= 1
    odn[key]= pre[key]
    contador['fitxesInfantils']+= 1
            
cip.clear()
pre.clear()    
        
##########

print 'dx: ' + strftime("%Y-%m-%d %H:%M:%S")
dx= {}
cursor['ora'].execute("select dpd_cod||'$'||codi_sector,dpd_cod_p,dpd_pat from prstb507{0}".format(" where dpd_cod='{0}' and codi_sector='{1}'".format(*debugging) if debug else ""))
for id,peca,pato in cursor['ora']:
    try:
        odn[id]
    except KeyError:
        continue
    try:
        dx[id]
    except KeyError:
        dx[id]= {}
    try:
        dx[id][peca]
    except KeyError:
        dx[id][peca]= {'caries': False,'altres':False}
    if pato== 'CA':
        dx[id][peca]['caries']= True
        dx[id][peca]['altres']= False
    elif not dx[id][peca]['caries']:
        dx[id][peca]['altres']= True

########## 

print 'tx: ' + strftime("%Y-%m-%d %H:%M:%S")
tx= {}
cursor['ora'].execute("select dtd_cod||'$'||codi_sector,dtd_cod_p,dtd_tra from prstb508 where dtd_tra in ('OBT','CON','SEG') and dtd_et in ('E','T'){0}".format(" and dtd_cod='{0}' and codi_sector='{1}'".format(*debugging) if debug else ""))
for id,peca,tto in cursor['ora']:
    try:
        odn[id]
    except KeyError:
        continue
    try:
        tx[id]
    except KeyError:
        tx[id]= {}
    try:
        tx[id][peca]
    except KeyError:
        tx[id][peca]= {}
    tx[id][peca][tto]= True

########## 
    
print 'dents: ' + strftime("%Y-%m-%d %H:%M:%S")
cursor['ora'].execute("select dfd_cod||'$'||codi_sector,dfd_cod_p,dfd_estat,dfd_p_a,dfd_mot_a from prstb506 where ((dfd_estat in ('C','O','S') and dfd_p_a='P') or dfd_p_a='A'){0}".format(" and dfd_cod='{0}' and codi_sector='{1}'".format(*debugging) if debug else ""))
for id,peca,estat,present,motiu in cursor['ora']:
    try:
        odn[id]
    except KeyError:
        continue
    definitiva= 'def' if peca< 50 else 'temp'
    try:
        car_confirmada= dx[id][peca]['caries']
        fora= dx[id][peca]['altres']
    except KeyError:
        car_confirmada= False
        fora= False
    try:
        obt_confirmada= tx[id][peca]['OBT']
    except KeyError:
        obt_confirmada= False
    try:
        obt_probable= tx[id][peca]['CON']
    except KeyError:
        obt_probable= False
    try:
        seg_confirmada= tx[id][peca]['SEG']
    except KeyError:
        seg_confirmada= False
    cariada= estat== 'C' and car_confirmada and present== 'P'
    obturada= estat== 'O' and obt_confirmada+obt_probable== 1 and present== 'P'# (sembla que CAO conta si tenen OBT o si tenen CON, pero no si tenen les dues!)
    obturada_f1= obturada and fora# en teoria hauriem de treure sempre fora (com fem a absents), pero en desquadrem mes dels que quadrem
    obturada_f2= estat== 'O' and obt_probable and not obt_confirmada and present== 'P'# en alguns casos no conta si nomes CON
    segellada= estat== 'S' and seg_confirmada and present== 'P'
    absent= present== 'A' and motiu not in ('N','A') and not fora
    if cariada or absent or obturada or segellada:
        if absent:
            claus= [definitiva+present]
        else:
            claus= [definitiva+estat]
            if obturada_f1 or obturada_f2:
                claus.append(definitiva+'F')
        for clau in claus:
            try:
                odn[id][clau]+= 1
            except KeyError:
                odn[id][clau]= 1
    if debug:
        print peca,cariada,absent,obturada,obturada_f1,obturada_f2,fora,car_confirmada,obt_confirmada,obt_probable

dx.clear()
tx.clear()

#chapucillas: hi ha molt pocs desquadres positius (mes cao que desglose), i la gran majoria dels negatius desapareixen fent aixo (tot relacionat amb obturacions)
if debug:
    print odn['{0}${1}'.format(*debugging)]   

for key in odn.keys():
    if odn[key]['cao']< odn[key].get('defC',0)+odn[key].get('defA',0)+odn[key].get('defO',0) and odn[key]['cao']== odn[key].get('defC',0)+odn[key].get('defA',0)+odn[key].get('defO',0)-odn[key].get('defF',0):
        odn[key]['defO']= odn[key].get('defO',0)-odn[key]['defF']
    if odn[key]['co']< odn[key].get('tempC',0)+odn[key].get('tempO',0) and odn[key]['co']== odn[key].get('tempC',0)+odn[key].get('tempO',0)-odn[key].get('tempF',0):
        odn[key]['tempO']= odn[key].get('tempO',0)-odn[key]['tempF']
        
if debug:
    print odn['{0}${1}'.format(*debugging)]            

##########

print 'fluor: ' + strftime("%Y-%m-%d %H:%M:%S")
fluor= {}
cursor['ora'].execute("select tb_cod||'$'||codi_sector,to_char(tb_data,'YYYYMMDD') from prstb511 where tb_trac='FC'{0}".format(" and tb_cod='{0}' and codi_sector='{1}'".format(*debugging) if debug else ""))
for id,data in cursor['ora']:
    try:
        odn[id]
    except KeyError:
        continue
    try:
        fluor[id]
    except KeyError:
        fluor[id]= []
        odn[id]['fluor']= 0
    if data not in fluor[id]:
        fluor[id].append(data)
        odn[id]['fluor']+= 1

fluor.clear()

##########

print 'union: ' + strftime("%Y-%m-%d %H:%M:%S")
cols= {'pob':['edat','sexe','ambit','sap','eap','codi'],'odn':['cao','defC','defA','defO','defS','co','tempC','tempO','tempS','fluor']}
contador['outNul']=contador['outDoble']=contador['fitxesCorrectes']=contador['outDescuadre']= 0
for cod in odn.keys():
    id= odn[cod]['id']
    if odn[cod]['cao'] is None or odn[cod]['co'] is None:
        contador['outNul']+= 1
    elif pob[id]['fitxa']> 1:
        contador['outDoble']+= 1
    else:
        for key in cols['odn']:
            try:
                odn[cod][key]
            except KeyError:
                odn[cod][key]= 0
        if debug or debugDescuadre or (odn[cod]['cao']== odn[cod]['defC']+odn[cod]['defA']+odn[cod]['defO'] and odn[cod]['co']== odn[cod]['tempC']+odn[cod]['tempO']):
            pob[id]['codi']= cod
            for key in cols['odn']:
                pob[id][key]= odn[cod][key]
            contador['fitxesCorrectes']+= 1
        else:
            contador['outDescuadre']+= 1
        
##########
        
print 'up: ' + strftime("%Y-%m-%d %H:%M:%S")
if debug:
    table= 'sisap_odontologia_debug'
elif debugDescuadre:
    table= 'sisap_odontologia_all'
else:
    table= 'sisap_odontologia'

try:
    cursor['ora'].execute("drop table {0}".format(table))
except:
    pass
finally:
    cursor['ora'].execute("create table {0} (id varchar2(50),edat number,sexe varchar2(1),ambit varchar2(2),sap varchar2(2),eap varchar2(5),fitxa_ecap varchar2(25),def_cao number,def_car number,def_abs number,def_obt number,def_seg number,temp_co number,temp_car number,temp_obt number,temp_seg number,fluor number)".format(table))

rows= []
cols= cols['pob'] + cols['odn']
for key in pob.keys():
    if debug:
        try:
            pob[key]['cao']
        except KeyError:
            continue
    row= [key]
    for col in cols:
        try:
            val= pob[key][col]
        except KeyError:
            val= None
        row.append(val)
    rows.append(row)

if len(rows)> 0:
    long= len(rows[0])
    values= []
    for k in xrange(long):
        values.append(":" + `k+1`)
    values=",".join(values)
    cursor['ora'].prepare("insert into {0} VALUES ({1})".format(table,values))
    cursor['ora'].executemany(None,rows)
    conn['ora'].commit()

##########

for key in cursor.keys():
    cursor[key].close()
for key in conn.keys():
    conn[key].close()
    
print 'fi: ' + strftime("%Y-%m-%d %H:%M:%S")

print contador
