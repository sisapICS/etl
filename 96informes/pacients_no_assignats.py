import sisapUtils as u
import collections as c



def multi_worker(params):
    sector, sql1, sql2 = params
    print(sector)
    upload = []
    visitats = c.Counter()
    for cip, in u.getAll(sql2, sector):
        visitats[cip] += 1
    for cip, usua_origen, cotitzacio, garanties, abes in u.getAll(sql1, sector):
        n = visitats[cip] if cip in visitats else 0
        upload.append((cip, usua_origen, cotitzacio, garanties, abes, n))
    u.listToTable(upload, 'no_assignats_5_7_24', 'exadata')

def no_assignats():
    sql1 = """SELECT
                    CIP_USUARI_CIP,
                    USUA_SERVEI_ORIGEN,
                    CART_ENT_COTITZACIO,
                    CART_GRUP_GARANTIES,
                    USUA_ABS_CODI_ABS
                FROM
                    (
                    SELECT
                        usua_cip,
                        usua_nass1,
                        USUA_NASS2,
                        USUA_NASS3,
                        USUA_SERVEI_ORIGEN,
                        USUA_ABS_CODI_ABS
                    FROM
                        usutb040
                    WHERE
                        USUA_SITUACIO = 'A'
                        AND USUA_UAB_UP IS NULL) a
                INNER JOIN 
                            (
                    SELECT
                        CART_NASS1,
                        CART_NASS2,
                        CART_NASS3,
                        CART_ENT_COTITZACIO,
                        CART_GRUP_GARANTIES
                    FROM
                        USUTB009) b 
                            ON
                    CART_NASS1 = usua_nass1
                    AND CART_NASS2 = USUA_NASS2
                    AND CART_NASS3 = USUA_NASS3
                INNER JOIN 
                            USUTB011 ON
                    CIP_CIP_ANTERIOR = A.USUA_CIP"""
    
    sql2 = """SELECT CIP_USUARI_CIP FROM vistb043 INNER JOIN 
                USUTB011 ON CIP_CIP_ANTERIOR = VISI_USUARI_CIP
                WHERE to_date(visi_data_visita, 'j') >= DATE '2023-07-01' 
                AND VISI_SITUACIO_VISITA  = 'R'"""

    cols = """(cip varchar2(40), usua_origen number, cotitzacio varchar2(2), garanties varchar2(4), abs number, n_visites number)"""
    u.createTable('no_assignats_5_7_24', cols, 'exadata', rm=True)
    u.grantSelect('no_assignats_5_7_24', "DWSISAP_ROL", 'exadata')
    jobs = [(sector, sql1, sql2) for sector in u.sectors]
    u.multiprocess(multi_worker, jobs, 4, close=True)



if __name__ == "__main__":
    no_assignats()


"""
SELECT AGA_COD, AGA_DES, abs_cod, ABS_DES, cotitzacio, garanties, count(1) FROM  dwsisap.no_assignats_5_7_24 INNER JOIN dwsisap.DBC_CENTRES
ON abs_cod = abs
WHERE abs IS NOT null and n_visites >= 2
AND cip NOT IN (SELECT cip FROM dwsisap.dbc_poblacio WHERE ecap_up IS NOT null)
GROUP BY AGA_COD, AGA_DES, abs_cod, ABS_DES, cotitzacio, garanties
"""