import sisapUtils as u
import collections as c


def multi_worker(params):
    periode, sql = params
    sql = sql.format(periode=periode)
    print(periode)
    upload = []
    for N in u.getOne(sql, 'exadata'):
        upload.append((110, str(periode), '', '', '', '', N))
    u.listToTable(upload, 'krfyhu5gs_tmpach_110', 'exadata')


def get_110():
    """
            --HINT DISTRIBUTE_ON_KEY(stratum_1)
        CREATE TABLE DWOMOP_CALIDAD.krfyhu5gs_tmpach_110
        AS
        SELECT
            110 AS analysis_id,  
            CAST(t1.obs_month AS VARCHAR(255)) AS stratum_1,
            CAST(NULL AS varchar(255)) AS stratum_2,
            CAST(NULL AS varchar(255)) AS stratum_3,
            CAST(NULL AS varchar(255)) AS stratum_4,
            CAST(NULL AS varchar(255)) AS stratum_5,
            COUNT(DISTINCT op1.PERSON_ID) AS count_value
        FROM
            DWOMOPCS.observation_period op1
        JOIN 
        (
            SELECT
                DISTINCT 
            EXTRACT(YEAR FROM observation_period_start_date)* 100 + EXTRACT(MONTH FROM observation_period_start_date) AS obs_month,
                TO_DATE(TO_CHAR(EXTRACT(YEAR FROM observation_period_start_date), '0000')|| '-' || TO_CHAR(EXTRACT(MONTH FROM observation_period_start_date), '00')|| '-' || TO_CHAR(1, '00'), 'YYYY-MM-DD')
            AS obs_month_start,
                TO_DATE(to_char(last_day(observation_period_start_date), 'YYYY-MM-DD')|| ' 23:59:59', 'YYYY-MM-DD HH24:MI:SS') AS obs_month_end
            FROM
                DWOMOPCS.observation_period
        ) t1 ON
            op1.observation_period_start_date <= t1.obs_month_start
            AND op1.observation_period_end_date >= t1.obs_month_end
        GROUP BY
            t1.obs_month
    """
    sql = """SELECT
                min(EXTRACT(YEAR FROM observation_period_start_date)* 100 + EXTRACT(MONTH FROM observation_period_start_date)),
                max(EXTRACT(YEAR FROM OBSERVATION_PERIOD_END_DATE)* 100 + EXTRACT(MONTH FROM OBSERVATION_PERIOD_END_DATE))
            FROM
                dwomopcs.OBSERVATION_PERIOD"""
    data_min, data_max = u.getOne(sql, 'exadata')

    any_min = data_min/100
    any_max = data_max/100

    periodes = set()
    for any in range(any_min, any_max+1):
        for mes in range(1, 13):
            periode = any * 100 + mes
            if periode >= data_min and  periode <= data_max:
                periodes.add(periode)

    cols = """(id number, stratum_1 varchar2(255),
                stratum_2 varchar2(255), stratum_3 varchar2(255),
                stratum_4 varchar2(255), stratum_5 varchar2(255),
                count_value number)"""
    u.createTable('krfyhu5gs_tmpach_110', cols, 'exadata', rm=True)
    u.grantSelect('krfyhu5gs_tmpach_110', "DWSISAP_ROL", 'exadata')
    u.grantSelect('krfyhu5gs_tmpach_110', "DWOMOP_CALIDAD", 'exadata')

    sql = """SELECT count(1) FROM dwomopcs.OBSERVATION_PERIOD
                WHERE EXTRACT(YEAR FROM observation_period_start_date)* 100 + EXTRACT(MONTH FROM observation_period_start_date) <= '{periode}'
                AND EXTRACT(YEAR FROM OBSERVATION_PERIOD_END_DATE)* 100 + EXTRACT(MONTH FROM OBSERVATION_PERIOD_END_DATE) >= '{periode}'"""
    jobs = [(periode, sql) for periode in periodes]
    u.multiprocess(multi_worker, jobs, 4, close=True)


def get_117():
    """
        -HINT DISTRIBUTE_ON_KEY(stratum_1)
        CREATE TABLE DWOMOP_CALIDAD.m92dksfgs_tmpach_117
        AS
        SELECT
        117 as analysis_id,  
        CAST(t1.obs_month AS VARCHAR(255)) as stratum_1,
        Cast(null as varchar(255)) as stratum_2, cast(null as varchar(255)) as stratum_3, cast(null as varchar(255)) as stratum_4, cast(null as varchar(255)) as stratum_5,
        COUNT(distinct op1.PERSON_ID) as count_value
        FROM
        DWOMOPCS.observation_period op1
        join 
        (SELECT distinct 
            EXTRACT(YEAR FROM observation_period_start_date)*100 + EXTRACT(MONTH FROM observation_period_start_date)  as obs_month
        FROM DWOMOPCS.observation_period
        ) t1 on EXTRACT(YEAR FROM op1.observation_period_start_date)*100 + EXTRACT(MONTH FROM op1.observation_period_start_date) <= t1.obs_month
        and EXTRACT(YEAR FROM op1.observation_period_end_date)*100 + EXTRACT(MONTH FROM op1.observation_period_end_date) >= t1.obs_month
        group by t1.obs_month
    """

    sql = """SELECT
                min(EXTRACT(YEAR FROM observation_period_start_date)* 100 + EXTRACT(MONTH FROM observation_period_start_date)),
                max(EXTRACT(YEAR FROM OBSERVATION_PERIOD_END_DATE)* 100 + EXTRACT(MONTH FROM OBSERVATION_PERIOD_END_DATE))
            FROM
                dwomopcs.OBSERVATION_PERIOD"""
    data_min, data_max = u.getOne(sql, 'exadata')

    any_min = data_min/100
    any_max = data_max/100

    periodes = set()
    for any in range(any_min, any_max+1):
        for mes in range(1, 13):
            periode = any * 100 + mes
            if periode >= data_min and  periode <= data_max:
                periodes.add(periode)

    cols = """(id number, stratum_1 varchar2(255),
                stratum_2 varchar2(255), stratum_3 varchar2(255),
                stratum_4 varchar2(255), stratum_5 varchar2(255),
                count_value number)"""
    u.createTable('krfyhu5gs_tmpach_110', cols, 'exadata')
    u.grantSelect('krfyhu5gs_tmpach_110', "DWSISAP_ROL", 'exadata')
    u.grantSelect('krfyhu5gs_tmpach_110', "DWOMOP_CALIDAD", 'exadata')

    sql = """SELECT count(1) FROM dwomopcs.OBSERVATION_PERIOD
                WHERE EXTRACT(YEAR FROM observation_period_start_date)* 100 + EXTRACT(MONTH FROM observation_period_start_date) <= '{periode}'
                AND EXTRACT(YEAR FROM OBSERVATION_PERIOD_END_DATE)* 100 + EXTRACT(MONTH FROM OBSERVATION_PERIOD_END_DATE) >= '{periode}'"""
    jobs = [(periode, sql) for periode in periodes]
    u.multiprocess(multi_worker, jobs, 4, close=True)

if __name__ == "__main__":
    get_110()
    # get_117()

"""
-- A EXECUTAR AMB USUARI DWOMOP_CALIDAD 
CREATE TABLE DWOMOP_CALIDAD.krfyhu5gs_tmpach_110 AS SELECT * FROM dwsisap.krfyhu5gs_tmpach_110;
SELECT * FROM DWOMOP_CALIDAD.krfyhu5gs_tmpach_110;
CREATE TABLE DWOMOP_CALIDAD.m92dksfgs_tmpach_117 AS SELECT '117' AS id, stratum_1, stratum_2, stratum_3, stratum_4, stratum_5, count_value FROM dwsisap.krfyhu5gs_tmpach_110;
SELECT * FROM DWOMOP_CALIDAD.m92dksfgs_tmpach_117;
"""


