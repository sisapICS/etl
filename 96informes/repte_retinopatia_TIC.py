# -*- coding: utf8 -*-

import sisapUtils as u
# import collections

# # Inclusion creteria
# # People born between 01/01/1942 and 01/01/1992
# # Diabetics before 01/01/2022 alive in 01/01/2012
# # Interest: 24, 28, 563 (1st dx)
# # Main: 44 retinopatia 44
# # DX
print('dx')
upload = []
users = set()
sql = """
    select id_cip_sec, ps, dde
    from eqa_problemes
    where ps in (24, 18, 563, 44, 47, 55)
        and dde < date '2022-01-01'
    """
for id, ps, date in u.getAll(sql, 'nodrizas'):
    upload.append((id, ps, date))
    if ps not in (55, 47):
        users.add(id)
cols = "(cip varchar2(13), ps int, data_dx date)"
u.createTable('retinopatia_dx', cols, 'redics', rm=True)
u.listToTable(upload, 'retinopatia_dx', 'redics')

# USERS
print('users')
upload = []
sql = """select id_cip_sec, usua_data_naixement, usua_sexe, usua_nacionalitat
        from import.assignada
        where usua_data_naixement > DATE("1942-01-01")
        and usua_data_naixement < DATE("1992-01-01")
        and (usua_situacio != 'D'
        or (usua_situacio = 'D' and usua_data_situacio > DATE("2012-01-01")))
        """
for id, birth, sex, nacionality in u.getAll(sql, 'import'):
    if id in users:
        upload.append((id, birth, sex, nacionality))
cols = "(cip varchar2(13), data_naixement date, sexe varchar(5), nacionalitat int)"  # noqa
u.createTable('retinopatia_users', cols, 'redics', rm=True)
u.listToTable(upload, 'retinopatia_users', 'redics')

# # 2. Pharmacy: n rows x user
# #       (ppfmc_pmc_data_ini, ppfmc_data_fi, ppfmc_durada, ppfmc_freq, ppfmc_posologia, ppfmc_num_recs, ppfmc_periodicitat, ppfmc_atccodi, ppfmc_pf_codi)  # noqa
#         # Antidiabètics orals o insulina (Codi ATC A10)
#         # Medicació antihipertensiva (Codi ATC C02)
#         # Medicació hiperlipidèmia (codi ATC C10)
print('farmàcia')
upload = []
sql = """select id_cip_sec, ppfmc_pmc_data_ini, ppfmc_data_fi,
        ppfmc_durada, ppfmc_freq, ppfmc_posologia,
        ppfmc_num_recs, ppfmc_periodicitat,
        ppfmc_atccodi, ppfmc_pf_codi
        from import.tractaments
        WHERE ppfmc_atccodi LIKE 'A10%'
        OR ppfmc_atccodi LIKE 'C02%'
        OR ppfmc_atccodi LIKE 'C10%'"""
for id, data_ini, data_fi, durada, freq, posologia, num_recs, periodicitat, atccodi, pf_codi in u.getAll(sql, 'import'):  # noqa
    if id in users:
        this = (id, data_ini, data_fi, durada, freq, posologia,
                num_recs, periodicitat, atccodi, pf_codi)
        upload.append(this)
cols = """(cip varchar2(13), data_inici date, data_fi date, durada int,
        frequencia int, posologia number(25,3), num_recs int, periodicitat int,
        atccodi varchar(7), pf_codi int)"""
u.createTable('retinopatia_farmacia', cols, 'redics', rm=True)
u.listToTable(upload, 'retinopatia_farmacia', 'redics')

print('tabac')
upload = []
sql = "select id_cip_sec, dalta, dbaixa, dlast, last from nodrizas.eqa_tabac"
for id, dalta, dbaixa, dlast, last in u.getAll(sql, 'nodrizas'):
    if id in users:
        upload.append((id, dalta, dbaixa, dlast, last))
cols = """(cip varchar2(13), data_inici date, data_fi date, data_last date,
        last int)"""
u.createTable('retinopatia_tabac', cols, 'redics', rm=True)
u.listToTable(upload, 'retinopatia_tabac', 'redics')

print('variables EQA')
upload = []
sql = """select id_cip_sec, agrupador, data_var, valor
        from nodrizas.eqa_variables
        where AGRUPADOR in
        (20, 875, 30, 513, 458, 459, 460, 461, 197,
        422, 423, 424, 416, 16, 17, 19, 268)
"""
for id, agrupador, data, valor in u.getAll(sql, 'nodrizas'):
    if id in users:
        upload.append((id, agrupador, data, valor))
cols = """(cip varchar2(13), agrupador int, data date, valor number(25,3))"""
u.createTable('retinopatia_eqa_variables', cols, 'redics', rm=True)
u.listToTable(upload, 'retinopatia_eqa_variables', 'redics')

print('proves')
upload = []
sql = """select id_cip_sec, agrupador, data
        from nodrizas.eqa_proves ep
        where agrupador = 699"""
for id, agrupador, data in u.getAll(sql, 'nodrizas'):
    if id in users:
        upload.append((id, agrupador, data))
cols = """(cip varchar2(13), agrupador int, data date)"""
u.createTable('retinopatia_proves', cols, 'redics', rm=True)
u.listToTable(upload, 'retinopatia_proves', 'redics')


print('variables')
upload = []
sql = """select id_cip_sec, vu_cod_vs, vu_val, vu_dat_act
        from import.variables
        where vu_cod_vs in ('TT101', 'FT4000','VF3001','VF3002',
                'RALTD','RALTE','RDMD','RDME', 'RHTAD', 'RHTAE', 'TF3000')"""
for id, codi, val, data in u.getAll(sql, 'import'):
    if id in users:
        upload.append((id, codi, val, data))
cols = "(cip varchar2(13), agrupador varchar2(6), valor number(25,3), data date)"  # noqa
u.createTable('retinopatia_variables', cols, 'redics', rm=True)
u.listToTable(upload, 'retinopatia_variables', 'redics')

print('ACTIVITATS')
upload = []
sql = """select id_cip_sec, au_cod_ac, au_val, au_dat_act
        from IMPORT.ACTIVITATS
        where AU_COD_AC in ('VF301', 'VF302', 'CA321')
"""
for id, codi, val, data in u.getAll(sql, 'import'):
    if id in users:
        upload.append((id, codi, val, data))
cols = "(cip varchar2(13), agrupador varchar(5), valor number(25,3), data date)"  # noqa
u.createTable('retinopatia_activitats', cols, 'redics', rm=True)
u.listToTable(upload, 'retinopatia_activitats', 'redics')


# taules
# retinopatia_eqa_variables, retinopatia_tabac, retinopatia_farmacia,
# retinopatia_users, retinopatia_dx, retinopatia_proves, retinopatia_variables,
# retinopatia_activitats
# PREDUMBO donar-li accés i paqui: PREDUPRP
taules = [
    'retinopatia_eqa_variables',
    'retinopatia_tabac',
    'retinopatia_farmacia',
    'retinopatia_users',
    'retinopatia_dx',
    'retinopatia_proves',
    'retinopatia_variables',
    'retinopatia_activitats',
    ]
for e in taules:
    u.grantSelect(e, 'PREDUPRP', 'redics')
