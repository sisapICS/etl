# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta
from time import time,sleep

import sisapUtils as u



class upc_dia(object):
    """."""

    def __init__(self):
        """."""
        self.get_dades()
        self.export_mail()
        
    def get_dades(self):
        """."""
        self.upload = [] 
        sql = """ select data, residencia, grup, sum(casos_confirmat) casos, sum(casos_confirmat_only_tar),
            sum(ingressats_total) ingressats_total, sum(ingressats_critic), sum(ingressos_total),
            sum(ingressos_critic), sum(pcr), sum(tar), sum(pcr_no_cas_pos),sum(pcr_no_cas), sum(tar_no_cas_pos), sum(tar_no_cas), sum(poblacio),
            sum(casos_confirmat_only_tar_simpt), sum(tar_simpt_no_cas_pos), sum(tar_simpt_no_cas), sum(vacuna_dosi_1),sum(vacuna_dosi_2)
            from preduffa.sisap_covid_web_master_t
            group by data, residencia, grup"""
        for dat, resi, grup, casos, casos_tar, ing_t, uci, ingt2, uci2,pcr, tar, pcr_no_cas_pos, pcr_no_cas, tar_no_cas_pos, tar_no_cas, pob, tarsimpt, tar_simpt_no_cas_pos, tar_simpt_no_cas, vac1, vac2 in u.getAll(sql, 'redics'):
            if int(grup) == 0:
                if vac1 > 0:
                    print dat, vac1
                    vac1 = 0
                    print vac1
                if vac2 > 0:
                    print dat, vac2
                    vac2 = 0
                    print vac2
            self.upload.append([dat, resi, grup, casos, casos_tar, ing_t, uci, ingt2, uci2,pcr, tar, pcr_no_cas_pos, pcr_no_cas, tar_no_cas_pos, tar_no_cas, pob, tarsimpt,
            tar_simpt_no_cas_pos, tar_simpt_no_cas, vac1, vac2])

    
    def export_mail(self):
        """Enviem fitxer"""
        
        file = u.tempFolder + "dades_covid_10.csv"
        u.writeCSV(file, self.upload, sep=';')
        text= "Us fem arribar el fitxer actualitzat."
        u.sendPolite(['clara.prats@upc.edu','marti.catala@upc.edu','m.inmaculada.villanueva@estudiantat.upc.edu'],'Dades covid',text,file)
        
if __name__ == '__main__':
    u.printTime("Inici")
      
    upc_dia()
    
    u.printTime("Final")