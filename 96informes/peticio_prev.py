# coding: iso-8859-1
from sisapUtils import *
from time import strftime
from collections import defaultdict,Counter
from dateutil.relativedelta import relativedelta
import datetime

codi="POCS2018"
imp="import"
nod="nodrizas"
alt="altres"


anys=(2008,2009,2010,2011)
ciaps=('K86','K87','T89','T90','T93')

def get_up_ambit():
    """Asigna a cada up la descripcion del ambito al que pertenece.
       Devuelve un dict {up: amb_desc}
    """
    sql="select scs_codi,amb_desc from {}.cat_centres where ep = '0208'".format(nod)
    return {up:amb_desc for up,amb_desc in getAll(sql,nod)}

def get_id_edat():
    """ Asocia a cada id de paciente su fecha de nacimiento.
        Devuelve un diccionario {id: data_naix}
    """
    sql = "select id_cip_sec,usua_data_naixement from {}.assignada".format(imp)
    return {id: dat_naix for id, dat_naix in getAll(sql, imp)  }

def get_tables_year():
    """ Selecciona las subtablas de assignadahistorica que guardan los registros de un any de los que se encuentran en anys.
        Devuelve un diccionario {year: subtabla}
    """
    tables = dict()
    for table in getSubTables("assignadahistorica"):
        #dat = [dat for dat, in getAll("select dataany from {} limit 1".format(table), imp)][0]
        year_tuple = getOne("select dataany from {} limit 1".format(table), 'import')
        if not year_tuple:
            continue
        if year_tuple[0] in anys:
            tables[year_tuple[0]]=table
    printTime("Tables")
    return tables

def get_cim_ciap(ciaps):
    """ Asocia a cada codigo cim10 el codigo ciap que lo engloba.
        Devuelve un diccionario {cim:ciap}
    """
    sql="SELECT codi_cim10,codi_ciap FROM MD_CT_CIM10_CIAP WHERE codi_ciap in {}".format(ciaps)
    return {cim: ciap for cim,ciap in getAll(sql,"redics")}

def get_problemes(params):
    """Selecciona de import.problemas aquellos ids que tengan algun problema de salud guardado en las keys de cim_ciap y que esten activos en
       la fecha que se pasa como tercer paramentro. Es decir, que esta fecha este en el intervalo que hay entre la fecha de diagnostico (dde) y la de fin (dba).
       Si esta es nula, se asigna el valor de fecha. Agrupa los pacientes por el codigo ciap al que esta asociado el cim10 de la tabla.
       Devuelve un diccionario de sets {ciap: set(ids)}
    """
    taula,cim_ciap,fecha=params
    sql="select id_cip_sec,pr_dde,pr_dba,pr_cod_ps from {} \
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_cod_ps in {}".format(taula,tuple(cim_ciap.keys()))
    pacients=defaultdict(set)
    for id, dde,dba,cod in getAll(sql,imp):
        dba= fecha if not dba else dba
        if dde <= fecha <=dba:
            pacients[cim_ciap[cod]].add(id)
    return pacients

def get_dades_up(table,fecha,id_naix,up_ambit,pacients):
    """Agrupa a nivel de edad y problema de salud los ids en pacients. Itera por las ups en la tabla de assignadahistorica del any deseado y 
       filtra por la ups en el up_ambit y por los ids con fecha de nacimiento. Se agrupan por intervalos de edad de 5 anys dados por la funcion ageConverter.
       Tambien crea un Counter con el numero de pacientes asignados en el any en cuestion.
       Devuelve:
       -> dict {edat: {cod:n}}
       -> Counter {edat:n}
    """
    dades=defaultdict(Counter)
    assig_total=Counter()
    sql="select id_cip_sec,up from {}".format(table)
    for id, up in getAll(sql,imp):
        if up in up_ambit:
            if id in id_naix:
                edat=ageConverter(yearsBetween(id_naix[id],fecha))
                assig_total[edat]+=1
                for cod in pacients:
                    if id in pacients[cod]:
                        dades[edat][cod]+=1

    return dades,assig_total

"""
def get_dades_no_assig(pacients,fecha,id_naix):
    dades=defaultdict(Counter)
    for cod in pacients:
        for id in pacients[cod]:
            if id in id_naix:
                edat=ageConverter(yearsBetween(id_naix[id],fecha)+1)
                dades[edat][cod]+=1
    return dades
"""

def export_txt_file(name,header,rows):
    with open(name,"wb") as out_file:
        print(rows[0])
        row_format ="{:>35}" * len(rows[0])+"\n"
        out_file.write(row_format.format(*header))
        for row in rows:
            out_file.write(row_format.format(*row))

if __name__ == '__main__':
    
    printTime("inici")

    tables=get_tables_year()

    up_ambit=get_up_ambit()
    printTime("up_ambit")

    id_naix=get_id_edat()
    printTime("id_naix")

    cim_ciap=get_cim_ciap(ciaps)
    printTime(cim_ciap)

    rows=[]
    #Para cada year in anys, obtiene los pacientes con problemas de salud en cim_ciap activos en el 31 de diciembre de ese any.
    for year in anys:
        pacients_total=defaultdict(set)
        fecha=datetime.date(day=31,month=12,year=year)
        for pacients in multiprocess(get_problemes,[ (table,cim_ciap,fecha) for table in getSubTables("problemes") ]):
            for cod in pacients:
                pacients_total[cod] |= pacients[cod]
        printTime("pacients_total")

        print(year)
        if year == 2008:
            #Coge los ids que esten asignados en 2009, pero la poblacion asignada la deja en 0
            dades,assig_total=get_dades_up(tables[2009],fecha,id_naix,up_ambit,pacients_total)
            rows.extend([(year,edat,cod,dades[edat][cod],0) for edat in dades for cod in dades[edat]])
            
        else:
            dades,assig_total=get_dades_up(tables[year],fecha,id_naix,up_ambit,pacients_total)
            rows.extend([(year,edat,cod,dades[edat][cod],assig_total[edat]) for edat in assig_total for cod in dades[edat]])
            

    export_txt_file("prevalencias.txt",["PERIODE","GROUP EDAT", "PROBLEMA SALUT", "NUM", "DEN"],rows)


