import sisapUtils as u


def inadequacio():
    cols = """(hash varchar2(100), up varchar2(5))"""
    u.createTable('inadequacio_ibp', cols, 'exadata')
    u.grantSelect('inadequacio_ibp', "DWSISAP_ROL", 'exadata')
    print('done')

    no_cumplidors = set()
    sql = """SELECT idpac FROM EQALLISTATS e WHERE e.INDICADOR = 'EQA0222' AND TIPUS = 'M'"""
    for hash, in u.getAll(sql, 'pdp'):
        no_cumplidors.add(hash)

    receptador = {}
    sql = """SELECT PPFMC_PMC_USUARI_CIP, PPFMC_PMC_AMB_COD_UP  FROM PPFTB016
        WHERE PF_COD_ATC like 'A02BC%'
        AND (ppfmc_data_fi >= DATE '2024-06-12'
            OR ppfmc_data_fi IS NULL)"""
    for hash, up in u.getAll(sql, 'redics'):
        if hash in no_cumplidors:
            receptador[hash] = up

    upload = []
    for hash, up in receptador.items():
        upload.append((hash, up))
    u.listToTable(upload, 'inadequacio_ibp', 'exadata')


def multi_worker(params):
    sector, sql, tipus, cip_2_hash, combo_malindicats = params
    print(sector)
    upload = []
    for cip, up, ini, fi in u.getAll(sql, sector):
        subtip = tipus[up] if up in tipus else '0'
        hash = cip_2_hash[cip] if cip in cip_2_hash else ''
        mal = 1 if hash in combo_malindicats else 0
        upload.append((hash, up, ini, fi, subtip, mal))
    u.listToTable(upload, 'inadequacio_ibp', ('backup', 'analisi'))


def inadequacio_resi():
    sql = """SELECT up_cod, SUBTIP_COD FROM dwsisap.DBC_RUP dr 
                WHERE tip_cod in ('10', '20')"""
    tipus = {}
    for up, sub in u.getAll(sql, 'exadata'):
        tipus[up] = sub
    cols = """(cip varchar(40), up varchar(5), data_ini date, data_fi date, subtipus varchar(5), combo_malindicats int)"""
    u.createTable('inadequacio_ibp', cols, ('backup', 'analisi'), rm=True)
    sql = """SELECT usua_cip, usua_cip_cod FROM pdptb101"""
    cip_2_hash = {}
    for cip, hash in u.getAll(sql, 'pdp'):
        cip_2_hash[cip] = hash
    sql = """SELECT hash  FROM preduffa.sisap_polifarmacia_pacient"""
    combo_malindicats = set()
    for hash, in u.getAll(sql, 'redics'):
        combo_malindicats.add(hash)
    sql = """SELECT PPFMC_PMC_USUARI_CIP,
                PPFMC_PMC_AMB_COD_UP, PPFMC_PMC_DATA_INI,
                PPFMC_DATA_FI
                FROM PPFTB016
                WHERE PPFMC_ATCCODI like 'A02BC%'
                AND PPFMC_PMC_DATA_INI  >= DATE '2023-01-01'
                AND PPFMC_PMC_DATA_INI  < DATE '2024-01-01'"""
    jobs = [(sector, sql, tipus, cip_2_hash, combo_malindicats) for sector in u.sectors]
    u.multiprocess(multi_worker, jobs, 4, close=True)


if __name__ == "__main__":
    inadequacio_resi()