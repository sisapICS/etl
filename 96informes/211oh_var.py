# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


TABLE = "oh_variables"
DATABASE = "test"


class OH(object):
    """."""

    def __init__(self):
        """."""
        self.get_jobs()
        self.get_data()
        self.get_pacients()
        self.upload_data()

    def get_jobs(self):
        """."""
        self.jobs = []
        params = [("variables2",
                   "import",
                   "select id_cip_sec, vu_cod_vs \
                    from {}, nodrizas.dextraccio \
                    where vu_cod_vs in {} and \
                          vu_dat_act between adddate(adddate(data_ext, interval -2 year), interval +1 day) and data_ext",  # noqa
                   "variables",
                   84),
                  ("activitats2",
                   "import",
                   "select id_cip_sec, au_cod_ac \
                    from {}, nodrizas.dextraccio \
                    where au_cod_ac in {} and \
                          au_dat_act between adddate(adddate(data_ext, interval -2 year), interval +1 day) and data_ext",  # noqa
                   "actuacions",
                   84),
                  ("problemes",
                   "import",
                   "select id_cip_sec, 'PS' \
                    from {}, nodrizas.dextraccio \
                    where pr_cod_ps in {} and \
                          pr_dba = 0 and \
                          pr_hist = 1 and \
                          pr_dde <= data_ext",
                   "problemes",
                   85)]
        for mare, db, master, taula, agr in params:
            q = "select criteri_codi from eqa_criteris \
                 where taula = '{}' and agrupador = {}".format(taula, agr)
            codis = tuple([cod for cod, in u.getAll(q, "nodrizas")])
            q = "show create table {}".format(mare)
            tables = u.getOne(q, db)[1].split("UNION=(")[1][:-1].split(",")
            for table in tables:
                sql = master.format(table, codis)
                self.jobs.append((sql, db))

    def get_data(self):
        """."""
        self.data = c.defaultdict(set)
        codis = {"AUDIT": "AUDIT", "VP2001": "AUDIT-C", "EP2004": "AUDIT-C",
                 "ALRIS": "CALC", "CP201": "GRAU", "PS": "PS"}
        for worker in u.multiprocess(self._worker, self.jobs, 12):
            for id, cod in worker:
                agr = codis.get(cod, "ALTRES")
                self.data[id].add(agr)

    def get_pacients(self):
        """."""
        self.upload = []
        sql_c = "select scs_codi from cat_centres where ep = '0208'"
        centres = set([up for up, in u.getAll(sql_c, "nodrizas")])
        sql = "select id_cip_sec, up, edat, sexe, num \
               from mst_indicadors_pacient \
               where ind_codi = 'EQA0301A' and \
                     ates = 1 and \
                     institucionalitzat = 0"
        for id, up, edat, sexe, compleix in u.getAll(sql, "eqa_ind"):
            if up in centres:
                this = [id, edat, sexe, compleix]
                this.extend([1 * (agr in self.data[id])
                             for agr in ("AUDIT", "AUDIT-C", "CALC",
                                         "GRAU", "ALTRES", "PS")])
                if compleix == max(this[4:]):
                    self.upload.append(this)
                else:
                    print id

    def upload_data(self):
        """."""
        cols = "(id int, edat int, sexe varchar(1), compleix int, \
                 audit int, auditc int, calculadora int, grau int, \
                 altres int, ps int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.upload, TABLE, DATABASE)

    def _worker(self, params):
        return [row for row in u.getAll(*params)]


if __name__ == "__main__":
    OH()
