# -*- coding: utf8 -*-

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *

visitats = {}
for table in getSubTables('visites'):
    dat, = getOne('select year(visi_data_visita) from {} limit 1'.format(table), 'import')
    if dat in [2017, 2016, 2015, 2014, 2013]:
        sql = "select  id_cip_sec from {} where visi_data_baixa = 0 and visi_situacio_visita = 'R'".format(table)
        for id, in getAll(sql, 'import'):
            visitats[(id)] = True

centres = {}            
sql = "select scs_codi from cat_centres where ep='0208'"            
for up, in getAll(sql, 'nodrizas'):
    centres[up] = True
   
recomptes = Counter()   
sql = 'select id_cip_sec, edat, up from assignada_tot'
for id, edat, up in getAll(sql, 'nodrizas'):
    if up in centres:
        recomptes[(edat, 'den')] += 1
        if (id) in visitats:
            recomptes[(edat, 'num')] += 1    
upload = []
for (edat, tip), r in recomptes.items():
    if tip == 'den':
        num = recomptes[(edat, 'num')]
        upload.append([edat,num, r])
        
file = tempFolder + 'Visitats_en_5 anys.txt'
writeCSV(file, upload, sep=';') 