# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *


for sector in sectors:
    standby = sector + 'sb'
    print standby

  
debug = False

nod = 'nodrizas'
imp = 'import'

anys = [2016, 2017]

ansietat = ("('F41.0', 'F41.1', 'F41', 'F41.2', 'F41.3','F41.8','F41.9','F41.1','F43','F43.0','F43.1','F43.8','F43.9','Z73.3')")

def get_patologia(cim10, th):

    if cim10 == 'F41.0':
        patologia = 'Crisis angoixa'
    elif cim10 == 'F41.1' and th in ('5409','5410'):
        patologia = 'Crisis angoixa'
    elif cim10 in ('F41','F41.2','F41.3','F41.8', 'F41.9'):
        patologia = 'Trastorns ansietat'
    elif cim10 in ('F41.1') and th not in ('5409','5410'):
        patologia = 'Ansietat generalitzada'
    elif cim10 in ('F43','F43.0','F43.1','F43.8','F43.9','Z73.3'):
        patologia = 'Estrés post-traumàtic'
    else:
        patologia = 'NO'
        
    return patologia
        



centres = {}
sql = "select scs_codi from cat_centres where ep='0208'"
for up, in getAll(sql, nod):
    centres[up] = True
    

recomptes = Counter()
sql = "select  id_cip_sec, extract(year from pr_dde), pr_dde, pr_up, pr_th, pr_cod_ps from problemes where pr_cod_ps in {0} \
        and pr_cod_o_ps='C' and pr_hist=1 and pr_data_baixa is null {1}".format(ansietat, ' limit 10' if debug else '')
for id, Ydde, dde, up, th, cim10 in getAll(sql, imp):
    if Ydde == 2016 or Ydde == 2017:
        wk = dde.isocalendar()[1]
        if up in centres:
            patologia = get_patologia(cim10, th)
            if patologia == 'NO':
                continue
            recomptes[patologia, Ydde, wk] += 1

upload = []                
for (patologia, Ydde, wk), res in recomptes.items():
    upload.append([patologia, Ydde, wk, res])
    
   
file = tempFolder + 'proces_anisetat.txt'
writeCSV(file, upload, sep=';')

