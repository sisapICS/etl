# coding: latin1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
import pandas as pd 
import unicodedata
import numpy as np




redics = 'redics'
imp = 'import'
nod = 'nodrizas'
table = 'SiEcataleg'

#hay que pasar los excels a csv, 
excel_escolas_info= 'escolas_info.csv'
excel_escolas_nens= 'escolas_nens.csv'

class SiECataleg(object):
    def __init__(self):
        self.excel_escolas_info=excel_escolas_info
        self.excel_escolas_nens=excel_escolas_nens
        self.up_desc=self.get_up_desc()
        self.table_name='escoles_sie'
        self.columns_list=['Denominacio completa', 'Codi naturalesa',
       'Nom naturalesa', 'Codi titularitat', 'Nom titularitat', 'Adreca', 'Codi comarca', 'Nom comarca','Codi municipi', 'Nom municipi', 
       'Nivells educatius','up','br','desc','amb',
       '1 ESO','2 ESO','3 ESO','4 ESO','1 BATX','2 BATX','Programes Formacio','Primaria']
        self.columns_types = ["codi_escola varchar(80) ", "nom_escola varchar(100)",  "codi_naturalesa varchar(40)",  "nom_naturalesa varchar(80)","codi_tituralitat varchar(40)",  "nom_tituralitat varchar(80)", "adreca varchar(80)", 
            "codi_comarca varchar(40)", "nom_comarca varchar(80)", "codi_municipi varchar(40)", "nom_municipi varchar(80)",
            "nivells_educatius varchar(80)", "up varchar(80)",  "br_codi varchar(80)","up_desc varchar(80)","amb_desc varchar(80)"] + ['n_{} number'.format(curs.lower().replace(' ','_')) for curs in
            ['1 ESO','2 ESO','3 ESO','4 ESO','1 BATX','2 BATX','Programes Formacio','primaria'] ]
   
    def read_data(self):
        self.df_info= pd.read_csv(self.excel_escolas_info,sep=';',dtype={u'Codi centre': str, 'Codi titularitat':str,'Codi naturalesa':str, 'Codi municipi':str, 'Codi comarca':str})
        self.df_info = self.df_info[pd.notnull(self.df_info[u'Codi centre'])]
        self.df_info = self.df_info.fillna('')

       
        for col_name in self.df_info:
            if 'UP' in col_name:
                self.up_column= col_name
            elif 'Denominac' in col_name:
                self.columns_list[0]=col_name
            elif 'Adre' in col_name:
                self.columns_list[5]=col_name

        
        self.df_nens= pd.read_csv(self.excel_escolas_nens,sep=';',dtype={u'Centre codi': str, 'CFGM 1':float,'CFGM 2':float,'CFGM NIVELL DESCONEGUT':float,'CFGM P':float,'CFGS 1':float,'CFGS 2':float,'CFGS NIVELL DESCONEGUT':float,'CFGS P':float})
        self.df_nens = self.df_nens.fillna(0)
        self.df_nens[u'Programes Formacio']=self.df_nens[ [col_name for col_name in self.df_nens.columns if 'CFG' in col_name] ].sum(axis = 1)
        
        
        for df in [self.df_nens,self.df_info]:
            cols_float=[col_name for col_name in df.columns if df[col_name].dtype == np.float64]
            df[cols_float] = df[cols_float].astype(int)

        
    
    def create_dicts(self):
        self.dict_escola_info=self.df_info.set_index(u'Codi centre').to_dict(orient='index')
        
        self.dict_escola_nens=self.df_nens.set_index(u'Centre codi').to_dict(orient='index')
       
        

    
    def get_up_desc(self):
        sql='select scs_codi,ics_codi,ics_desc,amb_desc from nodrizas.cat_centres;'
        return {u'{}'.format(up): {"br":ics_codi,"desc":ics_desc, "amb":amb_desc} for up,ics_codi,ics_desc,amb_desc in getAll(sql,nod)}
        
    def process_dicts(self):
        rows_dict=defaultdict(dict)
        
        for codi_centre in self.dict_escola_info:
            for i,key in enumerate(self.columns_list):
                rows_dict[codi_centre].setdefault(key,None)
                if key in self.dict_escola_info[codi_centre]:
                    rows_dict[codi_centre][key]=self.dict_escola_info[codi_centre][key]
                elif key == 'up':
                    if self.dict_escola_info[codi_centre][self.up_column]: 
                        up=self.dict_escola_info[codi_centre][self.up_column]
                        rows_dict[codi_centre][key]=up
                        if up in self.up_desc:
                            for up_features in self.up_desc[up]:
                                rows_dict[codi_centre][up_features]=self.up_desc[up][up_features]    
                try: 
                    if key in self.dict_escola_nens[codi_centre]:
                        rows_dict[codi_centre][key]=self.dict_escola_nens[codi_centre][key]
                except:
                    continue
           
        self.rows_dict=rows_dict
        
      


    def get_rows(self):
        return [[ codi_centre ]+ [self.rows_dict[codi_centre][centre_info] for centre_info in self.columns_list] for codi_centre in self.rows_dict] 
    
    def export_table(self):
        db_out='redics'
        self.rows=self.get_rows()

    
        createTable(self.table_name, "({})".format(", ".join(self.columns_types)), db_out, rm=True)
        listToTable(self.rows, self.table_name, db_out)

        if db_out =='redics':
            users= ['PREDUMMP']
            for user in users:
                execute("grant select on {} to {}".format(self.table_name,user),db_out)
    
        
        

     

    

if __name__ == "__main__":
    cat=SiECataleg()
    cat.read_data()
    cat.create_dicts()
    cat.process_dicts()
    cat.export_table()
   