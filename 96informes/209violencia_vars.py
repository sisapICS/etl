# coding: latin1

"""
Petici� anual de "Variables de viol�ncia de g�nere"
"""

import sisapUtils as u
import datetime as d

TODAY = d.datetime.now().date()
YEAR = 2019
DATES = ('2019-01-01','2019-12-31')
#DATES = ('2020-01-01','2020-06-31')

class Violencia(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_vardesc()
        self.get_edatc()
        self.get_up()
        self.get_variables()
        self.taula_final()
        self.export_taula()

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_desc from cat_centres"
        self.centres = {up: desc for (up, desc) in u.getAll(sql, 'nodrizas')}

    def get_vardesc(self):
        """."""
        sql = "select vs_cod, vs_des \
               from cat_prstb004 \
               where vs_cod in ('VP3001','EP3001') \
               group by vs_cod, vs_des"
        self.vardesc = {cod: desc for (cod, desc) in u.getAll(sql, 'import')}

    def get_edatc(self):
        """ . """
        self.edatc = {}
        sql = "select id_cip_sec, edat \
               from assignada_tot \
               where sexe='D'"
        for id_cip_sec, edat in u.getAll(sql, "nodrizas"):
            edatc = u.ageConverter(edat)
            self.edatc[edatc]=True

    def get_up(self):
        """."""
        self.up = {}
        self.recompte_pob_up = {}
        sql = "select id_cip_sec, up, edat from assignada_tot where sexe='D'"
        for id, up, edat in u.getAll(sql, "nodrizas"):
            edatc = u.ageConverter(edat)
            self.up[id] = {'up': str(up), 'edatc': edatc}
            key = (up, edatc)
            if key not in self.recompte_pob_up:
                self.recompte_pob_up[key] = 1
            else:
                self.recompte_pob_up[key] += 1

    def get_variables(self):
        """."""
        self.variables = {}
        self.recompte_var = {}
        
        sql = "select id_cip_sec, vu_cod_vs from variables2 \
               where vu_cod_vs in ('EP3001','VP3001') \
               and vu_dat_act between CAST('{}' AS DATE) \
                              and CAST('{}' AS DATE) \
               and vu_val > 0".format(*DATES)
        for cod, cod_desc in self.vardesc.items():
            for up, up_desc in self.centres.items():
                for edatc in self.edatc:
                    key = (cod,up,edatc)
                    self.recompte_var[key]=0
        for id, var in u.getAll(sql, "import"):
            if id in self.up:
                self.variables[id] = (var, self.up[id]['up'], self.up[id]['edatc'])
            else:
                self.variables[id] = (var, '00000', '000000')
            key = self.variables[id]
            if key in self.recompte_var:
                self.recompte_var[key] += 1

    def taula_final(self):
        """."""      
        self.taula_final = {}
        for id in self.recompte_var:
            if id[1] == '00000':
                up_desc = 'No UP'
                cod_desc = self.vardesc[id[0]]
                edatc = '000000'
                self.taula_final[id] = (id[0], cod_desc, id[1], up_desc, \
                                        edatc, self.recompte_var[id], \
                                        '00000')
            else:
                up_desc = self.centres[id[1]]
                cod_desc = self.vardesc[id[0]]
                if (id[1], id[2]) in self.recompte_pob_up:
                    self.taula_final[id] = (id[0], cod_desc, id[1], up_desc, \
                                            id[2], self.recompte_var[id], \
                                            self.recompte_pob_up[id[1],id[2]])

    def export_taula(self):
        """."""
        upload = self.taula_final.values()
        u.writeCSV(u.tempFolder + "violencia_vars_{}_{}.csv".format(YEAR,TODAY.strftime("%Y%m%d")),
                   [('var_cod','var_desc', \
                     'up','up_desc', \
                     'edat', \
                     'n', 'den')]
                   + sorted(upload), sep=";")

if __name__ == "__main__":
    Violencia()
