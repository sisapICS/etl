# -*- coding: utf8 -*-

import sisapUtils as u
import pandas as pd


class Satisfaccio():
    def __init__(self):
        self.cataleg()
        self.info()
        self.khalix()

    def cataleg(self):
        info = [
            ("P1", "Valoració dels diversos canals per a rebre atenció", "SATUSU01"),
            ("P2", "Facilitat pel dia de visita?", "SATUSU02"),
            ("P3", "Puntualitat per entrar a consulta?", "SATUSU03"),
            ("P4", "El soroll habitual?", "SATUSU04"),
            ("P5", "Neteja del CAP?", "SATUSU05"),
            ("P6", "Temps que li dedica el metge?", "SATUSU06"),
            ("P7", "El metge l'escolta i es fa càrrec?", "SATUSU07"),
            ("P8", "Pot donar la seva opinió?", "SATUSU08"),
            ("P9", "El metge li dóna la  informació que necessita?", "SATUSU09"),
            ("P10", "S'entenen les explicacions?", "SATUSU10"),
            ("P11", "Està en bones mans (metge)?", "SATUSU11"),
            ("P12", "Tracte personal metge?", "SATUSU12"),
            ("P13", "La infermera l'escolta i es fa càrrec?", "SATUSU13"),
            ("P14", "Tracte personal infermera?", "SATUSU14"),
            ("P15", "Està en bones mans (infermera)?", "SATUSU15"),
            ("P16", "Informació coherent?", "SATUSU16"),
            ("P17", "El metge de capçalera té informació del que li han fet?", "SATUSU17"),
            ("P18", "Atenció rebuda al CAP fora de l'horari habitual?", "SATUSU18"),
            ("P19", "Tracte personal del taulell?", "SATUSU19"),
            ("P20", "Mantenir o controlar el seu estat de salut", "SATUSU20"),
            ("P21", "Valoració atenció presencial", "SATUSU21"),
            ("P22", "Valoració atenció telefònica", "SATUSU22"),
            ("P23", "Valoració atenció a través d'internet (econsulta)", "SATUSU23"),
            ("P101", "Grau de satisfacció global", "SATUSU101"),
            ("P102", "Continuaria venint?", "SATUSU102")
        ]
        cols = "(indicador varchar(5), descripcio varchar(70), codi_longview varchar(10))"
        u.createTable('satisfaccio_cat', cols, "exadata", rm=True)
        u.listToTable(info, 'satisfaccio_cat', "exadata")
        u.grantSelect('satisfaccio_cat','DWSISAP_ROL','exadata')

    def info(self):
        sql = """select abs, scs_codi  from nodrizas.cat_centres"""
        abs_up = {}
        for abs, up in u.getAll(sql, 'nodrizas'):
            abs_up[abs] = up
        # Per cada any
        cols = "(up varchar(5), indicador varchar(5), num NUMBER(7,4), den NUMBER(7,4), periode int)"
        # u.createTable('satisfaccio', cols, "exadata", rm=True)
        anys = [2022, 2023]
        for a in anys:
            df = pd.read_excel('Dades_AP_num_i_den_2023.xlsx',  sheet_name="Dades {}".format(a))
            print(df)
            print('read')
            upload = []
            for i in df.index:
                try:
                    upload.append((abs_up[int(df["ABS"][i])], df["Pregunta"][i], round(df["num"][i], 4), round(df["den"][i], 4,), a))
                except:
                    pass
        
            u.listToTable(upload, 'satisfaccio', "exadata")
            print(upload)
        u.grantSelect('satisfaccio','DWSISAP_ROL','exadata')
    
    def khalix(self):
        print('br ics')
        up_br = {}
        sql = """select scs_codi, ics_codi from nodrizas.cat_centres"""
        for up, br in u.getAll(sql, 'nodrizas'):
            up_br[up] = br
        print('indicadors de P a khalix')
        indicadors_khalix = {}
        sql = """SELECT indicador, codi_longview FROM dwsisap.satisfaccio_cat"""
        for i, l in u.getAll(sql, 'exadata'):
            indicadors_khalix[i] = l
        print('crear taula format khalix altres')
        sql = """select up, indicador, num, den, periode from dwsisap.satisfaccio"""
        upload = []
        for up, indicador, num, den, periode in u.getAll(sql, 'exadata'):
            if up in up_br:
                up = up_br[up]
                indicador = indicadors_khalix[indicador]
                if periode == 2022:
                    upload.append((indicador, 'A2301', up, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', num))
                    upload.append((indicador, 'A2301', up, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', den))
                elif periode == 2023:
                    upload.append((indicador, 'A2401', up, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', num))
                    upload.append((indicador, 'A2401', up, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', den))
        file = "SATUSU"
        cols = "(indicador varchar(10), periode varchar(5), up varchar(5), \
            analisi varchar(5), nocat varchar(5), noinsat varchar(5), \
            dim6set varchar(7), val double)"
        u.createTable('satisfaccio', cols, "altres", rm=True)
        u.listToTable(upload, 'satisfaccio', "altres")
        print('export khalix')
        sql = "select * from altres.satisfaccio"
        u.exportKhalix(sql, file)
        
if __name__ == "__main__":
    Satisfaccio()
