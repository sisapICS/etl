# coding: latin1

"""
Embarassades amb incontinčncia.
"""

import sisapUtils as u


class Violencia(object):
    """."""

    def __init__(self):
        """."""
        self.get_thesaurus()
        self.get_edat()
        self.get_up()
        self.get_poblacio_up()
        self.taula_final()
        self.export_taula()
 
    def get_thesaurus(self):
        """."""
        self.thesaurus = {}
        self.data_prob = {}
        sql = "select id_cip_sec, pr_th, pr_dde from import.problemes \
               where pr_cod_ps = 'T74.9' and pr_th in (5481,5482) \
               and year(pr_dde)=2018"
        for id, cod_thesaurus, data in u.getAll(sql, "import"):
            self.thesaurus[id] = cod_thesaurus
            self.data_prob[id] = data
 
    def get_edat(self):
        """."""
        self.edat = {}
        sql = "select id_cip_sec, edat from assignada_tot"
        for id, edat in u.getAll(sql, "nodrizas"):
            if id in self.thesaurus:
                self.edat[id] = u.ageConverter(edat)
        
    def get_up(self):
        """."""
        self.up = {}
        sql = "select id_cip_sec, up from assignada_tot"
        for id, up in u.getAll(sql, "nodrizas"):
            if id in self.edat:
                self.up[id] = up
        
    def get_poblacio_up(self):
        """."""
        self.poblacio_up = {}
        self.recompte_pob_up = {}
        sql = "select id_cip_sec, up, edat from assignada_tot"
        for id, up, edat in u.getAll(sql, "nodrizas"):
            self.poblacio_up[id] = (u.ageConverter(edat), up)
            key = self.poblacio_up[id]
            if key not in self.recompte_pob_up:
                self.recompte_pob_up[key] = 1
            else:
                self.recompte_pob_up[key] += 1
    
    def taula_final(self):
        """."""
        self.taula_final = {}
        for id in self.edat:
            self.taula_final[id] = (self.thesaurus[id], self.edat[id], \
                                    self.up[id])
        self.recompte = {}
        for id in self.edat:
            key = self.taula_final[id]
            if key not in self.recompte:
                self.recompte[key] = 1 
            else:
                self.recompte[key] += 1
        
        self.recompte_up = {}
        for key in self.recompte:
            key1 = key[1]
            key2 = key[2]
            self.recompte_up[key] = (self.recompte[key], \
                                     self.recompte_pob_up[key1, key2])
                                     
        self.taula_final = {}
        for id in self.recompte_up:
            self.taula_final[id] = (id[0], id[1], id[2], \
                                    self.recompte_up[id][0],
                                    self.recompte_up[id][1])

    def export_taula(self):
        """."""
        upload = self.taula_final.values()
        u.writeCSV(u.tempFolder + "violencia.csv", upload, sep=";")

if __name__ == "__main__":
    Violencia()
