# -*- coding: utf8 -*-

import hashlib as h

"""
peti manolo
"""

import collections as c
import datetime as d
import sys
from datetime import timedelta

import sisapUtils as u


sql = " SELECT min(CAS_DATA), max(CAS_DATA) FROM dwsisap.DBC_VACUNA dv where cas_data >= DATE '2021-11-01'"
for ini, fi in u.getAll(sql, 'exadata'):
    ini=ini
    fi = fi  + timedelta(days=1)

upload = []  
for dat in u.dateRange(ini, fi):
    dat1 = dat.strftime("%Y-%m-%d")
    print dat1
    sql = """SELECT 
        CASE WHEN edat >=70 THEN '70 o mes'
        WHEN edat BETWEEN 60 AND 69 THEN '60 a 69'
        WHEN edat BETWEEN 40 AND 59 THEN '40 a 59'
        WHEN edat BETWEEN 15 AND 39 THEN '15 a 39'
        WHEN edat BETWEEN 0 AND 14 THEN '0 a 14' END edad,
        CASE WHEN immunitzat=0 OR vacuna_1_data > DATE '{0}' THEN 'no_vacunat'
        WHEN  DATE '{0}' - BOOSTER_ADMINISTRACIO_DATA>24 THEN 'booster'
        WHEN DATE '{0}' - IMMUNITZAT_DATA>24 THEN 'completa'
        ELSE 'parcial' END vacuna,
        sum (CASE WHEN cas_data BETWEEN DATE '{0}' - 10 AND  DATE '{0}' - 3 THEN 1 ELSE 0 end)casos,count(1)poblacio,
        round(sum (CASE WHEN cas_data BETWEEN DATE '{0}'-10 AND  DATE '{0}' - 3 THEN 1 ELSE 0 end)*100000/count(1),1)IA7
        FROM dwsisap.DBC_VACUNA
        where es_actiu=1
        GROUP BY 
        CASE WHEN immunitzat=0  OR vacuna_1_data > DATE '{0}'  THEN 'no_vacunat'
        WHEN DATE '{0}' - BOOSTER_ADMINISTRACIO_DATA>24 THEN 'booster'
        WHEN DATE '{0}' - IMMUNITZAT_DATA>24 THEN 'completa'
        ELSE 'parcial' END,
        CASE WHEN edat >=70 THEN '70 o mes'
        WHEN edat BETWEEN 60 AND 69 THEN '60 a 69'
        WHEN edat BETWEEN 40 AND 59 THEN '40 a 59'
        WHEN edat BETWEEN 15 AND 39 THEN '15 a 39'
        WHEN edat BETWEEN 0 AND 14 THEN '0 a 14' END
        ORDER BY edad,vacuna desc""".format(dat1)
    for edat, vacuna, casos, pob, IA7 in u.getAll(sql, 'exadata'):
        upload.append([dat1, edat, vacuna, casos, pob, IA7])

file = u.tempFolder + "evolucio.txt"
u.writeCSV(file, upload, sep='@')