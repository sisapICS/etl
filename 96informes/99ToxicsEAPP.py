# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter


imp = 'import_jail'
nod = 'nodrizas'

anys = '2017'


toxics = [
        ('Alcohol', ['F10.0', 'F10.1', 'F10.2', 'F10.3', 'F10.4', 'F10.5']),
        ('Opiacis', ['F11.0', 'F11.1', 'F11.2', 'F11.3']),
        ('Cannabis', ['F12.0', 'F12.1', 'F12.2', 'F12.3']),
        ('Sedants', ['F13.0', 'F13.1', 'F13.2', 'F13.3']),
        ('Cocaina', ['F14.0', 'F14.1', 'F14.2', 'F14.3']),
        ('Estimulants', ['F15.0', 'F15.1', 'F15.2', 'F15.3']),
        ('Allucinogens', ['F16.0', 'F16.1', 'F16.2', 'F16.3']),
        ('Tabac', ['F17', 'F17.0', 'F17.1', 'F17.2', 'F17.3', 'F17.4', 'F17.5', 'F17.6', 'F17.7', 'F17.8', 'F17.9']),
        ('Volatils', ['F18.0', 'F18.1', 'F18.2', 'F18.3']),
        ('Multiples_drogues', ['F19.0', 'F19.1', 'F19.2', 'F19.3'])
        ]


problemes_toxics = {}

for toxic, codis in toxics:
    t_cod = tuple(codis)
    sql = "select id_cip_sec,  if(pr_dba > 0 and pr_dba <= data_ext, 2, 1) \
        from problemes, nodrizas.dextraccio \
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa is null or pr_data_baixa > data_ext) and pr_cod_ps in {}". format(t_cod)
    for id, tancat in getAll(sql, imp):
        problemes_toxics[(id, toxic)] = tancat
    
pob = {}
sql = "select id_cip_sec, usua_sexe, (year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')) from assignada, nodrizas.dextraccio"
for id, sexe, edat in getAll(sql, imp):
    pob[id] = {'sexe': sexe, 'edat': edat}
    
drogues = {}
sql = "select id_cip_sec, vu_val, vu_dat_act, year(vu_dat_act) from variables where vu_cod_vs='YC0117'"
for id, valor, data, Yvar in getAll(sql, imp):
    if str(Yvar) == anys:
        if (id) in drogues:
            data2 = drogues[(id)]['data']
            if data2 < data:
                drogues[(id)]['data'] = data
                drogues[(id)]['valor'] = valor
        else:
            drogues[(id)] = {'data': data, 'valor': valor}

resultats = Counter()
sql = 'select id_cip_sec, up, year(ingres) from jail_atesa'
for id, up, Yingr in getAll(sql, nod):
    if str(Yingr) == anys:
        edat = pob[id]['edat']
        sexe = pob[id]['sexe']
        valordr = 999
        try:
            valordr = drogues[(id)]['valor']
        except KeyError:
            valordr = 999
        for toxic, codis in toxics:
            exec(toxic + " = 0" )
            if (id, toxic) in problemes_toxics:
                tancat = problemes_toxics[(id, toxic)]
                exec(toxic + " = tancat" )
        resultats[(up, edat, sexe, int(valordr), int(Alcohol), int(Opiacis), int(Cannabis), int(Sedants), int(Cocaina), int(Estimulants), int(Allucinogens), int(Tabac), int(Volatils), int(Multiples_drogues))] += 1
    
upload = []
for (up, edat, sexe, valordr, Alcohol, Opiacis, Cannabis, Sedants, Cocaina, Estimulants, Allucinogens, Tabac, Volatils, Multiples_drogues), d in resultats.items():
    upload.append([up, edat, sexe, valordr, Alcohol, Opiacis, Cannabis, Sedants, Cocaina, Estimulants, Allucinogens, Tabac, Volatils, Multiples_drogues, d])

file = tempFolder + 'toxics_eapp.txt'
writeCSV(file, upload, sep=';')
