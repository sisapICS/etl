
import sisapUtils as u

cols = """(sector varchar(5), up varchar(5), data date, hora varchar(10), centre_codi varchar(10), 
        centre_classe varchar(10), descriptiu varchar(100), servei varchar(10), sisap_codi varchar(10), 
        modul varchar(10), sisap_class varchar(10), tipus varchar(4), tipus_class varchar(10), 
        tipus_class_desc varchar(50))"""
u.createTable('qc_acut', cols, 'test', rm=True)

info_centres = {}
sql = """select CENT_NOM_CENTRE, codi_sector, cent_codi_centre, cent_classe_centre from import.cat_pritb010"""
for nom, sec, cod_cen, cod_class in u.getAll(sql, 'import'):
    info_centres[(sec, cod_cen, cod_class)] = nom

sql = """SELECT SECTOR, up, data, hora, CENTRE_CODI, CENTRE_CLASSE,
        SERVEI, SISAP_SERVEI_CODI, modul,
        SISAP_SERVEI_CLASS, tipus, TIPUS_CLASS, TIPUS_CLASS_DESC 
        FROM dwsisap.sisap_master_visites
        WHERE 
        DATA > to_date('2022-01-01', 'YYYY-MM-DD') AND
        ((centre_codi = 'E43008183' AND CENTRE_CLASSE = '02') OR
        (centre_codi = 'E43026320' AND CENTRE_CLASSE = '02') OR
        (centre_codi = 'E43021590' AND CENTRE_CLASSE = '02') OR
        (centre_codi = 'E43035934' AND CENTRE_CLASSE = '02') OR
        (centre_codi = 'E43019996' AND CENTRE_CLASSE = '02') OR
        (centre_codi = 'E43028535' AND CENTRE_CLASSE = '02') OR
        (centre_codi = 'E43007567' AND CENTRE_CLASSE = '02') OR
        (centre_codi = 'E43028729' AND CENTRE_CLASSE = '02'))
        AND SITUACIO = 'R'"""

upload = []
for sector, up, data, hora, centre_codi, centre_classe, servei, sisap_codi, modul, sisap_classe, tipus, tipus_class, tipus_class_desc in u.getAll(sql, 'exadata'):
    if (sector, centre_codi, centre_classe) in info_centres:
        descriptiu = info_centres[(sector, centre_codi, centre_classe)]
        upload.append((sector, up, data, hora, centre_codi, centre_classe, descriptiu, servei, sisap_codi, modul, sisap_classe, tipus, tipus_class, tipus_class_desc))

u.listToTable(upload, 'qc_acut', 'test')