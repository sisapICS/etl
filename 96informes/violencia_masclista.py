# encoding: utf-8

import sisapUtils as u
import collections as c
import datetime
import dateutil.relativedelta
import time


ANY = 2023

DX_VG = """('C01-T74.01', 'C01-T74.01XA', 'C01-T74.01XD', 'C01-T74.01XS', 'C01-T74.11', 'C01-T74.11XA', 
            'C01-T74.11XD', 'C01-T74.11XS', 'C01-T74.21', 'C01-T74.21XA', 'C01-T74.21XD', 'C01-T74.21XS', 
            'C01-T74.31', 'C01-T74.31XA', 'C01-T74.31XD', 'C01-T74.31XS', 'C01-T74.51XA', 'C01-T74.51XD', 
            'C01-T74.51XS', 'C01-T74.61XA', 'C01-T74.61XD', 'C01-T74.61XS', 'C01-T74.91', 'C01-T74.91XA', 
            'C01-T74.91XD', 'C01-T74.91XS', 'T74.01', 'T74.01XA', 'T74.01XD', 
            'T74.01XS', 'T74.11', 'T74.11XA', 'T74.11XD', 'T74.11XS', 'T74.21', 'T74.21XA', 'T74.21XD', 
            'T74.21XS', 'T74.31', 'T74.31XA', 'T74.31XD', 'T74.31XS', 'T74.51XA', 'T74.51XD', 'T74.51XS', 
            'T74.61XA', 'T74.61XD', 'T74.61XS', 'T74.91', 'T74.91XA', 'T74.91XD', 'T74.91XS', 
            'T74', 'C01-T74', 'T74.0', 'C01-T74.0', 'T74.02', 'C01-T74.02', 'T74.02XA', 'C01-T74.02XA', 'T74.02XD', 
            'C01-T74.02XD', 'C01-T74.02XS', 'T74.02XS', 'T74.1', 'C01-T74.1', 'T74.12', 'C01-T74.12', 'T74.12XA', 
            'C01-T74.12XA', 'T74.12XD', 'C01-T74.12XD', 'T74.12XS', 'C01-T74.12XS', 'T74.2', 'C01-T74.2', 'T74.22', 
            'C01-T74.22', 'T74.22XA', 'C01-T74.22XA', 'T74.22XD', 'C01-T74.22XD', 'T74.22XS', 'C01-T74.22XS', 
            'T74.3', 'C01-T74.3', 'T74.32', 'C01-T74.32', 'T74.32XA', 'C01-T74.32XA', 'T74.32XD', 'C01-T74.32XD', 
            'T74.32XS', 'C01-T74.32XS', 'T74.5', 'C01-T74.5', 'T74.51', 'C01-T74.51', 'T74.52', 'C01-T74.52', 
            'T74.52XA', 'C01-T74.52XA', 'T74.52XD', 'C01-T74.52XD', 'T74.52XS', 'C01-T74.52XS', 'T74.6', 'C01-T74.6', 
            'T74.61', 'C01-T74.61', 'T74.62', 'C01-T74.62', 'T74.62XA', 'C01-T74.62XA', 'T74.62XD', 'C01-T74.62XD', 
            'T74.62XS', 'C01-T74.62XS', 'T74.9', 'C01-T74.9', 'T74.92', 'C01-T74.92', 'T74.92XA', 'C01-T74.92XA', 
            'T74.92XD', 'C01-T74.92XD', 'T74.92XS', 'C01-T74.92XS', 'N90.81', 'N90.810', 
            'N90.811', 'N90.812', 'N90.813', 'N90.818', 'S38.2', 'S38.212A', 'S38.212D', 'S38.211S', 'C10-N90.81', 
            'C10-N90.810', 'C10-N90.811', 'C10-N90.812', 'C10-N90.813', 'C10-N90.818', 'C10-S38.2', 'C10-S38.212A', 
            'C10-S38.212D', 'C10-S38.211S')"""


class VIOLENCIA_MASCLISTA():

    def __init__(self):
        """ . """
        a = time.time()
        self.get_cat_nacionalitat();                    print("get_cat_nacionalitat", time.time()-a)
        self.get_poblacio();                            print("get_poblacio", time.time()-a)
        self.get_id_to_hash();                          print("get_id_to_hash", time.time()-a)
        self.get_comunicats_lessions();                 print("get_comunicats_lessions", time.time()-a)
        self.get_emb_part_puer();                       print("get_emb_part_puer", time.time()-a)
        #self.hash_to_id
        self.dx_vg = c.defaultdict(set)
        for sector in u.sectors:
            self.get_dx_vg(sector);                     print("get_dx_vg({})".format(sector), time.time()-a)
        self.dades = c.Counter()
        self.dades_nia = c.Counter()
        self.get_dades_01();                            print("get_dades_01", time.time()-a)
        self.get_dades_02();                            print("get_dades_02", time.time()-a)
        self.get_dades_03();                            print("get_dades_03", time.time()-a)
        self.get_dades_04();                            print("get_dades_04", time.time()-a)
        self.get_dades_05();                            print("get_dades_05", time.time()-a)
        self.exportar_dades();                          print("exportar_dades", time.time()-a)
        print("ya")


    def get_cat_nacionalitat(self):
        self.nacionalitat = dict()
        sql = "select codi_nac, desc_nac from cat_nacionalitat"
        self.nacionalitat['NO REGISTRADA'] = 'NO REGISTRADA'
        for codi_nac, desc_nac in u.getAll(sql, 'nodrizas'):
            self.nacionalitat[str(int(codi_nac))] = desc_nac


    def get_poblacio(self):
        # Hi ha moltes persones en import.assignada que tenen diferents paissos assignats a usua_nacionalitat. Tracto de
        # trobar un registre per nacionalitat que no sigui buit i, si hi han diversos diferents no-buits, aquell que la
        # nacionalitat no sigui espanyola (per trobar la procedència original)

        self.poblacio = dict()
        sql = """
                SELECT
                    id_cip,
                    usua_data_naixement,
                    usua_nacionalitat
                FROM
                    assignada
              """        
        for id_cip, data_naix, nacionalitat in u.getAll(sql, "import"):
            nacionalitat = nacionalitat if nacionalitat else "NO REGISTRADA"
            if not id_cip in self.poblacio:
                self.poblacio[id_cip] = {"data_naix": data_naix,
                                         "nacionalitat": nacionalitat}
            else:
                if nacionalitat != "NO REGISTRADA":
                    if self.poblacio[id_cip]["nacionalitat"] in ("NO REGISTRADA", "724"):
                        self.poblacio[id_cip]["nacionalitat"] = nacionalitat


    def get_id_to_hash(self):
        """."""

        self.hash_to_id = dict()
        self.id_to_hash = dict()
        sql = """
                SELECT
                    id_cip,
                    hash_d
                FROM
                    u11
                WHERE
                    id_cip IN {}
              """.format(tuple(str(id_cip) for id_cip in self.poblacio.keys()))
        for (id_cip, hash) in u.getAll(sql, "import"):
            self.hash_to_id[hash] = id_cip
            self.id_to_hash[id_cip] = hash

        self.cip_to_hash = dict()
        self.hash_to_cip = dict()

        sql = """
                SELECT
                    usua_cip,
                    usua_cip_cod
                FROM
                    pdptb101
              """
        for (cip, hash) in u.getAll(sql, "pdp"):
            self.hash_to_cip[hash] = cip
            self.cip_to_hash[cip] = hash

        self.id_to_nia = dict()

        sql = """
                SELECT
                    cip,
                    nia
                FROM
                    dwsisap.RCA_CIP_NIA
              """
        for (cip, nia) in u.getAll(sql, "exadata"):
            hash = self.cip_to_hash.get(cip[:13], "")
            id_cip = self.hash_to_id.get(hash, "")
            if id_cip:
                self.id_to_nia[id_cip] = nia

    def get_comunicats_lessions(self):

        self.comunicats = c.defaultdict(set)

        for sector in u.sectors:


            sql = """
                    SELECT
                        com_cip,
                        com_data
                    FROM
                        vistb018
                    WHERE
                        com_incident = 'AGR'
                        AND com_data BETWEEN to_date('27/12/{}', 'dd/mm/yyyy') 
                        AND to_date('05/01/{}', 'dd/mm/yyyy')
                """.format(ANY - 1, ANY + 1)        
            for cip, data in u.getAll(sql, sector):
                if cip in self.cip_to_hash:
                    hash = self.cip_to_hash[cip]
                    if hash in self.hash_to_id:
                        id_cip = self.hash_to_id[hash]
                        if id_cip in self.poblacio:
                            edat = dateutil.relativedelta.relativedelta(data, self.poblacio[id_cip]['data_naix']).years
                            self.comunicats[id_cip].add((data.date(), edat))


    def get_emb_part_puer(self):

        self.emb_part_puer = c.defaultdict(set)

        sql = """
                SELECT
                    pr_cod_u,
                    pr_dde,
                    pr_dba
                FROM
                    prstb015
                WHERE
                    pr_cod_ps >= 'C01-O00'
                    AND pr_cod_ps <= 'C01-O94'
              """
        for hash, data_ini, data_fi in u.getAll(sql, "redics"):
            if hash in self.hash_to_id:
                id_cip = self.hash_to_id[hash]
                if id_cip in self.poblacio:
                    data_ini = data_ini.date()
                    data_fi = data_fi.date() if data_fi else data_ini + dateutil.relativedelta.relativedelta(months=12)
                    self.emb_part_puer[id_cip].add((data_ini, data_fi))


    def get_dx_vg(self, sector):

        dx_vg_sector = []

        sql = """
                SELECT
                    id_cip,
                    pr_dde
                FROM
                    import.problemes_s{}
                WHERE
                    pr_cod_ps IN {}
                    AND pr_dde BETWEEN '{}-12-27' AND '{}-01-05'
              """.format(sector, DX_VG, ANY - 1, ANY + 1)
        for id_cip, data_dx in u.getAll(sql, 'import'):
            if id_cip in self.poblacio:
                edat = dateutil.relativedelta.relativedelta(data_dx, self.poblacio[id_cip]['data_naix']).years
                if edat >= 14:
                    dx_vg_sector.append([id_cip, data_dx, edat])
        for id_cip, data_dx, edat in dx_vg_sector:
            self.dx_vg[id_cip].add((data_dx, edat))


    def get_dades_01(self):
        """ Dones de 14 anys i més que tenen enregistrat nou diagnòstic de violència de gènere entre entre l’1 de gener
        i 31 de desembre ANY (data alta del diagnòstic en aquest període) """

        for id_cip in self.dx_vg:
            edat_menor = 999
            nacionalitat_reg = self.poblacio[id_cip]['nacionalitat']
            for data_dx, edat in self.dx_vg[id_cip]:
                if data_dx >= datetime.date(ANY, 1, 1) and data_dx <= datetime.date(ANY, 12, 31):
                    edat_menor = edat if edat < edat_menor else edat_menor
            if edat_menor != 999:
                edat_reg = u.ageConverter(edat_menor)
                nia = self.id_to_nia.get(id_cip, "")
                self.dades[('IND01', nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat_menor, edat_reg, "NUM")] += 1
                if nia:
                    self.dades_nia[('IND01', nia, nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat_menor, edat_reg, "NUM")] += 1

    def get_dades_02(self):
        """ Dones de 14 anys i més que tenen enregistrat nou diagnòstic de violència de gènere entre entre l’1 de gener
        i 31 de desembre ANY (data alta del diagnòstic en aquest període), que tenen emissió de comunicat de lesions,
        coincidint amb aquest diagnòstic.
            NUM: Total de dones amb dx de violència de gènere en el temps definit i emissió de comunicat de lesions
            DEN: Total de dones amb dx de violència de gènere en el temps definit
        """

        for id_cip in self.dx_vg:
            try:
                edat_menor_den = 999
                edat_menor_num = 999
                edat = None
                den_02 = False
                num_02 = False
                nacionalitat_reg = self.poblacio[id_cip]['nacionalitat']
                for data_dx, edat in self.dx_vg[id_cip]:
                    if data_dx >= datetime.date(ANY, 1, 1) and data_dx <= datetime.date(ANY,12,31):
                        den_02 = True
                        edat_menor_den = edat if edat < edat_menor_den else edat_menor_den
                        if id_cip in self.comunicats:
                            for data_comunicat, _ in self.comunicats[id_cip]:
                                if abs(u.daysBetween(data_dx, data_comunicat)) <= 5:
                                    num_02 = True
                                    edat_menor_num = edat if edat < edat_menor_num else edat_menor_num
                nia = self.id_to_nia.get(id_cip, "")
                if num_02:
                    edat_reg = u.ageConverter(edat_menor_num)
                    edat = edat_menor_num
                    self.dades[('IND02', nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat, edat_reg, "NUM")] += 1
                    if nia:
                        self.dades_nia[('IND02', nia, nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat, edat_reg, "NUM")] += 1
                elif den_02:
                    edat_reg = u.ageConverter(edat_menor_den)
                    edat = edat_menor_den
                if den_02:
                    self.dades[('IND02', nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat, edat_reg, "DEN")] += 1
                    if nia:
                        self.dades_nia[('IND02', nia, nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat, edat_reg, "DEN")] += 1
            except:
                continue


    def get_dades_03(self):
        """ Nombre total de dones de 14 anys i més amb un o més diagnòstic del grup Embaràs, part i puerperi, que tenen
            enregistrat algun diagnòstic de violència de gènere (data alta diagnòstic entre l’1 de gener i 31 de desembre ANY)
            coincidint amb el període d’embaràs, part o puerperi. ""
            """

        for id_cip in self.dx_vg:
            try:
                if id_cip in self.emb_part_puer:
                    edat_menor = 999
                    nacionalitat_reg = self.poblacio[id_cip]['nacionalitat']
                    for data_ini, data_fi in self.emb_part_puer[id_cip]:
                        for data_dx, edat in self.dx_vg[id_cip]:
                            if data_dx >= datetime.date(ANY, 1, 1) and data_dx <= datetime.date(ANY, 12, 31):
                                if data_ini <= data_dx and data_dx <= data_fi:
                                    edat_menor = edat if edat < edat_menor else edat_menor
                    if edat_menor != 999:
                        edat_reg = u.ageConverter(edat_menor)
                        nia = self.id_to_nia.get(id_cip, "")
                        self.dades[('IND03', nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat_menor, edat_reg, "NUM")] += 1
                        if nia:
                            self.dades_nia[('IND03', nia, nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat_menor, edat_reg, "NUM")] += 1
            except:
                continue


    def get_dades_04(self):
        """ Nombre total de dones de 14 anys i més amb un o més diagnòstic del grup grup Embaràs, part i puerperi, que tenen
            enregistrat algun diagnòstic de violència de gènere (data alta diagnòstic entre l’1 de gener  i 31 de desembre ANY)
            coincidint amb el període d’embaràs, part o puerperi), que tenen emissió de comunicat de lesions, coincidint amb
            aquest diagnòstic
            """

        for id_cip in self.dx_vg:
            try:
                if id_cip in self.emb_part_puer:
                    if id_cip in self.comunicats:
                        edat_menor = 999
                        nacionalitat_reg = self.poblacio[id_cip]['nacionalitat']
                        for data_ini, data_fi in self.emb_part_puer[id_cip]:
                            for data_dx, edat in self.dx_vg[id_cip]:
                                if data_dx >= datetime.date(ANY, 1, 1) and data_dx <= datetime.date(ANY, 12, 31):
                                    if data_ini <= data_dx and data_dx <= data_fi:
                                        for data_comunicat, _ in self.comunicats[id_cip]:
                                            if abs(u.daysBetween(data_dx, data_comunicat)) <= 5:
                                                edat_menor = edat if edat < edat_menor else edat_menor
                        if edat_menor != 999:
                            edat_reg = u.ageConverter(edat_menor)
                            nia = self.id_to_nia.get(id_cip, "")
                            self.dades[('IND04', nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat_menor, edat_reg, "NUM")] += 1
                            if nia:
                                self.dades_nia[('IND04', nia, nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat_menor, edat_reg, "NUM")] += 1
            except:
                continue


    def get_dades_05(self):
        """
        Dones amb emissió d’algun comunicat de lesions que tenen a més a més diagnòstic de violència de gènere
            NUM: Dones del denominador que tenen un dx de violència de gènere donat d’alta  entre 01/01/ANY a 31/12/ANY
            DEN: Total de dones que tenen algun comunicat de lesions
        """

        for id_cip in self.comunicats:
            edat_menor_den = 999
            edat_menor_num = 999
            edat = None
            num_05 = False
            nacionalitat_reg = self.poblacio[id_cip]['nacionalitat']
            for data_comunicat, edat in self.dx_vg[id_cip]:
                if data_comunicat >= datetime.date(ANY, 1, 1) and data_comunicat <= datetime.date(ANY, 12, 31):
                    edat_menor_den = edat if edat < edat_menor_den else edat_menor_den
                    if id_cip in self.dx_vg:
                        for data_dx, _ in self.comunicats[id_cip]:
                            if abs(u.daysBetween(data_dx, data_comunicat)) <= 5:
                                num_05 = True
                                edat_menor_num = edat if edat < edat_menor_num else edat_menor_num
            nia = self.id_to_nia.get(id_cip, "")
            if num_05:
                edat_reg = u.ageConverter(edat_menor_num)
                edat = edat_menor_num
                self.dades[('IND05', nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat, edat_reg, "NUM")] += 1
                if nia:
                    self.dades_nia[('IND05', nia, nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat, edat_reg, "NUM")] += 1
            else:
                edat_reg = u.ageConverter(edat_menor_den)
                edat = edat_menor_den
            if edat_menor_den < 999:
                self.dades[('IND05', nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat, edat_reg, "DEN")] += 1
                if nia:
                    self.dades_nia[('IND05', nia, nacionalitat_reg, self.nacionalitat[nacionalitat_reg.lstrip('+-0')], edat, edat_reg, "DEN")] += 1


    def exportar_dades(self):

        # A csv

        file = u.tempFolder + "violencia_masclista_{}.csv".format(ANY)
        self.dades_llista = [["indicador", "codi_nac", "nacionalitat", "edat", "edat_grup", "tipus", "n"]]
        self.dades_llista.extend([[ind, codi_nac, nac, edat, edat_grup, tip, n] for (ind, codi_nac, nac, edat, edat_grup, tip), n in self.dades.items()])
        u.writeCSV(file, self.dades_llista, sep=';')

        # A taula a db test

        tb = "violencia_masclista_{}".format(ANY)
        db = "test"

        u.createTable(tb,
                      "(indicador varchar(12), codi_nac double, nacionalitat varchar(150), edat int, edat_grup varchar(6), tipus varchar(6), n int)",
                      db, rm=True)
        u.listToTable(self.dades_llista[1:], tb, db)


        ####################################################################################################################################################

        # file = u.tempFolder + "violencia_masclista_{}.csv".format(ANY)
        # self.dades_llista = [["indicador", "codi_nac", "nacionalitat", "edat", "edat_grup", "tipus", "n"]]
        # self.dades_llista.extend([[ind, codi_nac, nac, edat, edat_grup, tip, n] for (ind, codi_nac, nac, edat, edat_grup, tip), n in self.dades.items()])
        # u.writeCSV(file, self.dades_llista, sep=';')

        # A taula a db test

        tb_nia = "violencia_masclista_nia_{}".format(ANY)
        db = "test"
        self.dades_llista_nia = [[ind, nia, codi_nac, nac, edat, edat_grup, tip, n] for (ind, nia, codi_nac, nac, edat, edat_grup, tip), n in self.dades_nia.items()]
        u.createTable(tb_nia,
                      "(indicador varchar(12), nia int, codi_nac double, nacionalitat varchar(150), edat int, edat_grup varchar(6), tipus varchar(6), n int)",
                      db, rm=True)
        u.listToTable(self.dades_llista_nia, tb_nia, db)

if True:
    VIOLENCIA_MASCLISTA()