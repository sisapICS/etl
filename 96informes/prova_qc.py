# -*- coding: utf8 -*-

"""
Adaptat del procés de la Paula (resident de Valle)
Adaptació d'un sql penjat  a trello a principis de setembre del 2018
Afegits dos indicadors que no estaven calculats.

Com a info:
- CONT0001: MMCI
- CONT0002: UPC
- CONT0003: SECON
- CONT0004: COC
"""

import collections as c

import sisapUtils as u

pob = {}

sql = """ select id_cip_sec, upinf from assignada_tot where institucionalitzat=1 and (pcc=1 or maca=1)"""
for id, up in u.getAll(sql, 'nodrizas'):
    pob[id] = up
    
centres = {}

sql = """SELECT scs_codi, tip_eap, ics_codi FROM cat_centres where tip_eap in ('M', 'A')"""
for up, tip, br in u.getAll(sql, 'nodrizas'):
    centres[up] = tip

recomptes = c.Counter()
altres = c.Counter()

sql = """
            select
                id_cip_sec,
                visi_up,
                if(left(visi_col_prov_resp, 1) = 1, "MED", "INF") as colegiat,
                if(visi_data_visita > adddate(data_ext, interval -1 month),
                    1, 0) as last_month
            from
                import.visites1 v,
                nodrizas.dextraccio
            where
                visi_situacio_visita = 'R'
                and s_espe_codi_especialitat not in ('EXTRA', '10102')
                and visi_tipus_visita not in ('9E', 'EXTRA', 'EXT')
                and visi_data_visita between
                    adddate(data_ext, interval -1 year)
                    and data_ext
                and left(visi_col_prov_resp, 1) in ('1', '3')
            """
for id, up, col, last_month in u.getAll(sql, 'import'):
    if id in pob:
        if up in centres:
            recomptes[col] += 1
        else:
            altres[up] += 1
            
 
        
for col, n in recomptes.items():
    print col, n
    
rec2 = c.Counter()  
sql = """select id_cip_sec, r2, servei from mst_long_cont_pacient"""
for id, r, servei in u.getAll(sql, "altres"):
    if id in pob:
        rec2[(servei, 'den')] += 1
        rec2[(servei, 'num')] += r
        rec2[('tot', 'den')] += 1
        rec2[('tot', 'num')] += r
        
for (tip, tip2), n in rec2.items():
    print tip, tip2, n

for up, n in altres.items():
    print up, n
    
 
    
        
        