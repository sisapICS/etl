# coding: iso-8859-1

import sisapUtils as u
import pprint as pp
import os
import sys

"""
des de 1 DNI:
    Exporta fitxers de:
        - problemes
        - derivacions
        - proves
        - visites

get_persona():
    mostra candidats de DNI, parint del Nom i els Cognoms
"""

TEST = False
EXT = 'txt'
LIMIT = "limit 2" if TEST else ''


def get_persona(nom, cognom1, cognom2):
    """
    aconsegueix el DNI quan no ens lo donen
    """
    nom = nom[:15] if nom != '' else ''
    n1 = " AND usua_nom = '{nom}'".format(nom=nom.upper()) if nom != '' else ''
    c1 = cognom1.upper()
    c2 = " AND usua_cognom2 = '{c2}'".format(c2=cognom2.upper()) if cognom2 != '' else ''  # noqa
    persona_sec = []
    print("\n Buscando DNI...")
    i = 0
    for sector in u.sectors:
        i += 1
        print("{}.) {}".format(i, sector))
        sql = """
            SELECT
                usua_dni, usua_nom, usua_cognom1, usua_cognom2,
                usua_uab_up, usua_situacio, usua_cip
            FROM
                usutb040
            WHERE
                usua_cognom1 = '{c1}' {c2} {n1}
        """.format(c1=c1, c2=c2, n1=n1)
        for id, nom, cog1, cog2, up, sit, cip in u.getAll(sql, sector):
            persona_sec.append((id, cip, nom, cog1, cog2))
    for persona in persona_sec:
        print(persona)


class Buscar(object):
    """ . """

    def __init__(self):
        """Execució seqüencial."""
        u.printTime(" - Inici \n")

        self.get_user_input()

        # Ids
        self.get_hashos()
        self.get_cips()

        # Catalegs
        u.printTime(" - Bajando catalogos \n")
        self.get_centres()
        self.get_especialitats()
        self.get_cataleg_ps()
        self.get_cataleg_derivacions()

        # Exports
        self.get_problemes()
        self.get_derivacions()
        self.get_proves()
        self.get_visites()

        u.printTime("\n Fi")

    def get_user_input(self):
        """Preguntes inicials a l'usuari."""
        cip, dni = None, None
        fitxa = raw_input('codi de la fitxa: ')
        if fitxa == '':
            sys.exit('fitxa es Obligatori')
        cip_in = raw_input('tenim cip (s/n)?')
        cip = raw_input('cip: ') if cip_in == 's' else None 
        if cip_in != 's':
            dni_in = raw_input('tenim dni (s/n): ')
            dni = raw_input('dni: ') if dni_in == 's' else None  
            if dni_in != 's':
                cognom1 = raw_input("cognom 1: ")
                if cognom1 == '':
                    sys.exit('es Obligatori: DNI o Cognom 1')
                cognom2 = raw_input("cognom 2: ")
                nom = raw_input("nom: ")
                if cognom2 == '' and nom == '':
                    sys.exit('es Oligatori: Cognom 2 o Nom')
                get_persona(nom, cognom1, cognom2)
                sys.exit('\n Ahi tienes, ahora puedes lanzar con DNI')
        self.user_input = {
                'fitxa': fitxa,
                'dni': dni if dni else None,
                'cip': cip if cip else None,
                }

    def get_hashos(self):
        self.hashos = {}
        dni = self.user_input.get('dni')
        cip = self.user_input.get('cip')
        for sector in u.sectors:
            if not cip:
                sql = """
                    SELECT
                        usua_dni, usua_nom, usua_cognom1, usua_cognom2,
                        usua_uab_up, usua_situacio, usua_cip
                    FROM
                        usutb040
                    WHERE
                        usua_dni = '{}'
                """.format(dni)
                for id, nom, cog1, cog2, up, sit, cip in u.getAll(sql, sector):
                    sql = """
                        SELECT
                            cip_cip_anterior, cip_usuari_cip
                        FROM
                            usutb011
                        WHERE
                            cip_usuari_cip = '{}'
                    """.format(cip)
                    for cipa, cipu in u.getAll(sql, sector):
                        sql = """
                            SELECT
                                usua_cip, usua_cip_cod
                            FROM
                                pdptb101
                            WHERE
                                usua_cip = '{}'
                        """.format(cipu)
                        for c, h in u.getAll(sql, 'pdp'):
                            self.hashos[(sector, h)] = True
            else:
                sql = """
                    SELECT
                        cip_cip_anterior, cip_usuari_cip
                    FROM
                        usutb011
                    WHERE
                        cip_usuari_cip = '{}'
                """.format(cip)
                for cipa, cipu in u.getAll(sql, sector):
                    sql = """
                        SELECT
                            usua_cip, usua_cip_cod
                        FROM
                            pdptb101
                        WHERE
                            usua_cip = '{}'
                    """.format(cipu)
                    for c, h in u.getAll(sql, 'pdp'):
                        self.hashos[(sector, h)] = True
                pp.pprint(self.hashos)

    def get_cips(self):
        self.cips = {}
        for (sector, hash), d in self.hashos.items():
            sql = """
                SELECT
                    id_cip, id_cip_sec
                FROM
                    u11
                WHERE
                    codi_sector='{}' AND
                    hash_d = '{}'
            """.format(sector, hash)
            for id_cip, id_cip_sec in u.getAll(sql, 'import'):
                self.cips[id_cip] = True
        pp.pprint(self.cips)

    def get_centres(self):
        self.centres = {}
        sql = """
            SELECT
                a.amb_codi_amb,
                a.amb_desc_amb,
                s.dap_codi_dap,
                s.dap_desc_dap,
                c.up_codi_up_scs,
                u.up_codi_up_ics,
                u.up_desc_up_ics
            FROM
                cat_gcctb008 c
                inner join cat_gcctb007 u
                    on c.up_codi_up_ics = u.up_codi_up_ics
                inner join cat_gcctb006 s
                    on u.dap_codi_dap = s.dap_codi_dap
                inner join cat_gcctb005 a
                    on s.amb_codi_amb = a.amb_codi_amb
            WHERE
                c.up_data_baixa = 0 AND
                u.up_data_baixa = 0 AND
                s.dap_data_baixa = 0 AND
                a.amb_data_baixa = 0
            """
        for amb, adesc, dap, ddesc, up, br, desc in u.getAll(sql, 'import'):
            self.centres[up] = {
                'ambit': amb,
                'ambdesc': adesc,
                'dap': dap,
                'dapdesc': ddesc,
                'br': br,
                'desc': desc,
                }

    def get_especialitats(self):
        sql = """
            SELECT
                espe_codi_especialitat, espe_especialitat
            FROM
                cat_pritb023
        """
        self.especialitats = {
            cod: des for (cod, des) in u.getAll(sql, 'import')
            }

    def get_cataleg_ps(self):
        self.cataleg_ps = {}
        sql = "select ps_cod, ps_des from cat_prstb001"
        for ps, psdes in u.getAll(sql, 'import'):
            self.cataleg_ps[ps] = psdes

    def get_cataleg_derivacions(self):
        self.servei_d = {}
        sql = 'select servei, desc_servei from cat_ct_der_serveid'
        for servei, desc in u.getAll(sql, 'import'):
            self.servei_d[servei] = desc

    def get_problemes(self):
        """ Exporta Problemes """
        tema = "Problemes"
        fitxa = self.user_input.get('fitxa')
        fname = "{}_{}.{}".format(tema, fitxa, EXT)
        filename = os.path.join(u.tempFolder, fname)
        upload = []
        for cip, d in self.cips.items():
            u.printTime("{}: Problemes".format(cip))
            sql = """
                SELECT
                    pr_cod_ps, pr_dde, pr_dba
                FROM
                    problemes,
                    nodrizas.dextraccio
                WHERE
                    id_cip = '{}' AND
                    pr_cod_o_ps = 'C' AND
                    pr_hist = 1 AND
                    pr_dde <= data_ext AND
                    (pr_data_baixa = 0 OR pr_data_baixa > data_ext)
                {}
            """.format(cip, LIMIT)
            for ps, dde, dba in u.getAll(sql, 'import'):
                if ps in self.cataleg_ps:
                    desc = self.cataleg_ps[ps]
                    upload.append([dde, ps, desc, dba])
        u.writeCSV(filename, upload, sep=';')

    def get_derivacions(self):
        """ Exporta Derivacions """
        tema = "Derivacions"
        fitxa = self.user_input.get('fitxa')
        fname = "{}_{}.{}".format(tema, fitxa, EXT)
        filename = os.path.join(u.tempFolder, fname)
        upload = []
        for cip, d in self.cips.items():
            u.printTime("{}: Derivacions".format(cip))
            sql = """
                SELECT
                    oc_data,
                    inf_servei_d_codi,
                    motiu
                FROM
                    nod_derivacions
                WHERE
                    id_cip = '{}'
                {}
            """.format(cip, LIMIT)
            for dat, serv, motiu in u.getAll(sql, 'nodrizas'):
                descS = self.servei_d.get(serv, 'SERVEI NO TROBAT')
                try:
                    descps = self.cataleg_ps[motiu]
                except KeyError:
                    descps = None
                upload.append([dat, descS, descps])
        u.writeCSV(filename, upload, sep=';')

    def get_proves(self):
        """ Exporta Proves"""
        tema = "Proves"
        fitxa = self.user_input.get('fitxa')
        fname = "{}_{}.{}".format(tema, fitxa, EXT)
        filename = os.path.join(u.tempFolder, fname)
        upload = []
        for cip, d in self.cips.items():
            u.printTime("{}: Proves".format(cip))
            sql = """
                SELECT
                    codi_sector, oc_data, inf_codi_prova
                FROM
                    nod_proves
                WHERE
                    id_cip = '{}'
                {}
            """.format(cip, LIMIT)
            for sector, dat, motiu in u.getAll(sql, 'nodrizas'):
                sql = """
                    SELECT
                        pro_desc_prova
                    FROM
                        gcctb003
                    WHERE
                        pro_codi_prova = '{}'
                """.format(motiu)
                desc_prova, = u.getOne(sql, str(sector))
                upload.append([dat, motiu, desc_prova])
            u.writeCSV(filename, upload, sep=';')

    def get_visites(self):
        """ Exporta visites """
        tema = "Visites"
        fitxa = self.user_input.get('fitxa')
        fname = "{}_{}.{}".format(tema, fitxa, EXT)
        filename = os.path.join(u.tempFolder, fname)
        upload = []
        for cip, d in self.cips.items():
            u.printTime("{}: Visites".format(cip))
            sql = """
                SELECT
                    visi_data_visita,
                    visi_up,
                    s_espe_codi_especialitat
                FROM
                    visites
                WHERE
                    id_cip = '{}' AND
                    visi_situacio_visita = 'R'
                {}
            """.format(cip, LIMIT)
            for data, up, espe in u.getAll(sql, 'import'):
                if up in self.centres:
                    desc = self.centres[up]['desc']
                    descespe = self.especialitats[espe]
                    # ep = centres[up]['ep']
                    upload.append([data, up, desc, descespe])
        u.writeCSV(filename, upload, sep=';')


if __name__ == '__main__':
    Buscar()
