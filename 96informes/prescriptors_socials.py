import sisapUtils as u
import collections as c


def extract_perfil_prescriptor_social():
    u.createTable('perfil_prescriptor_social', '(periode varchar(100), rol varchar(100), n int)', 'altres', rm=True)
    print(1)
    usuaris = {}
    sql = """select IDE_USUARI, IDE_CATEG_PROF
            from import.cat_pritb992"""
    for usuari, cat_prof in u.getAll(sql, 'import'):
        usuaris[usuari] = cat_prof
    print(2)
    rols = {}
    sql = """select rol_rol, ROL_USU
                from import.cat_pritb799"""
    for rol, usu in u.getAll(sql, 'import'):
        rols[usu] = rol
    print(3)
    upload = c.Counter()
    sql = """SELECT
                CASE
                    WHEN a.so_data BETWEEN '2019-10-01' AND '2020-09-30' THEN 'OCTUBRE 2019-SETEMBRE 2020'
                    WHEN a.so_data BETWEEN '2020-10-01' AND '2021-09-30' THEN 'OCTUBRE 2020-SETEMBRE 2021'
                    WHEN a.so_data BETWEEN '2021-10-01' AND '2022-09-30' THEN 'OCTUBRE 2021-SETEMBRE 2022'
                    WHEN a.so_data BETWEEN '2022-10-01' AND '2023-09-30' THEN 'OCTUBRE 2022-SETEMBRE 2023'
                END AS PERIODE, a.so_usu_alta
            FROM
                import.presc_social a
            WHERE
                a.so_data BETWEEN '2019-10-01' AND '2023-09-30'"""
    for periode, usu_alt in u.getAll(sql, 'import'):
        rol = ''
        if usu_alt in rols:
            if rols[usu_alt] == 'ECAP_BENES_EMOCIONAL':
                rol = 'REBEC'
                print(rol)
            else:
                if usu_alt in usuaris:
                    rol = usuaris[usu_alt]
        else:
            if usu_alt in usuaris:
                rol = usuaris[usu_alt]
        upload[periode, rol] += 1
    
    u.listToTable([(periode, rol, n) for (periode, rol), n in upload.items()], 'perfil_prescriptor_social', 'altres')


def prescripcio_social_hiperfrequentadors():
    u.createTable('prescripcio_social_hiperfrequentadors', '(periode varchar2(100), hash varchar2(100), up varchar2(10), sexe varchar2(10), edat int, prescripcio int)', 'exadata', rm=True)
    u.grantSelect('prescripcio_social_hiperfrequentadors', 'DWSISAP_ROL', 'exadata')
    print(1)
    frequentadors = c.defaultdict(set)
    sql = """select
                case
                    when a.DATA between DATE '2019-10-01' and DATE '2020-09-30' then 'OCTUBRE 2019-SETEMBRE 2020'
                    when a.DATA between DATE '2020-10-01' and DATE '2021-09-30' then 'OCTUBRE 2020-SETEMBRE 2021'
                    when a.DATA between DATE '2021-10-01' and DATE '2022-09-30' then 'OCTUBRE 2021-SETEMBRE 2022'
                    when a.DATA between DATE '2022-10-01' and DATE '2023-09-30' then 'OCTUBRE 2022-SETEMBRE 2023'
                end as PERIODE,
                PACIENT,
                count(1)
            from
                DWSISAP.SISAP_MASTER_VISITES   a
            where
                a.DATA between DATE '2019-10-01' and DATE '2023-09-30'
                and SITUACIO = 'R'
            group by
                case
                    when a.DATA between DATE '2019-10-01' and DATE '2020-09-30' then 'OCTUBRE 2019-SETEMBRE 2020'
                    when a.DATA between DATE '2020-10-01' and DATE '2021-09-30' then 'OCTUBRE 2020-SETEMBRE 2021'
                    when a.DATA between DATE '2021-10-01' and DATE '2022-09-30' then 'OCTUBRE 2021-SETEMBRE 2022'
                    when a.DATA between DATE '2022-10-01' and DATE '2023-09-30' then 'OCTUBRE 2022-SETEMBRE 2023'
                end,
                PACIENT
            HAVING COUNT(1) > 12"""
    for periode, pacient, N in u.getAll(sql, 'exadata'):
        frequentadors[periode].add(pacient)
    print(2)
    pacients = {}
    sql = """SELECT hash, ecap_up, sexe, cip,
            (DATE '2019-10-01' - DATA_NAIXEMENT) / 365 AS edat_2019,
            (DATE '2020-10-01' - DATA_NAIXEMENT) / 365 AS edat_2020,
            (DATE '2021-10-01' - DATA_NAIXEMENT) / 365 AS edat_2021,
            (DATE '2022-10-01' - DATA_NAIXEMENT) / 365 AS edat_2022
            FROM dwsisap.dbc_poblacio
            WHERE ecap_up IS NOT null"""
    for pacient, up, sexe, cip, edat19, edat20, edat21, edat22 in u.getAll(sql, 'exadata'):
        pacients[pacient] = [up, sexe, cip[:13], edat19, edat20, edat21, edat22]
    print(3)
    prescripcions = c.defaultdict(set)
    sql = """select 
            so_cip,
            CASE
                WHEN a.so_data BETWEEN DATE '2019-10-01' AND DATE '2020-09-30' THEN 'OCTUBRE 2019-SETEMBRE 2020'
                WHEN a.so_data BETWEEN DATE '2020-10-01' AND DATE '2021-09-30' THEN 'OCTUBRE 2020-SETEMBRE 2021'
                WHEN a.so_data BETWEEN DATE '2021-10-01' AND DATE '2022-09-30' THEN 'OCTUBRE 2021-SETEMBRE 2022'
                WHEN a.so_data BETWEEN DATE '2022-10-01' AND DATE '2023-09-30' THEN 'OCTUBRE 2022-SETEMBRE 2023'
            END AS PERIODE
            from ppftb080 a
            where a.so_data BETWEEN DATE '2019-10-01' AND DATE '2023-09-30'"""
    for sector in u.sectors:
        print(sector)
        for cip, periode in u.getAll(sql, sector):
            prescripcions[periode].add(cip)
    print('tractament')
    upload = set()
    for periode, pacients_freq in frequentadors.items():
        for pacient in pacients_freq:
            if pacient in pacients:
                up, sexe, cip, edat19, edat20, edat21, edat22 = pacients[pacient]
                if periode == 'OCTUBRE 2019-SETEMBRE 2020': edat = int(edat19)
                elif periode == 'OCTUBRE 2020-SETEMBRE 2021': edat = int(edat20)
                elif periode == 'OCTUBRE 2021-SETEMBRE 2022': edat = int(edat21)
                elif periode == 'OCTUBRE 2022-SETEMBRE 2023': edat = int(edat22)
                if edat >= 15:
                    prescripcio = 0
                    if cip in prescripcions[periode]:
                        prescripcio = 1    
                    upload.add((periode, pacient, up, sexe, edat, prescripcio))

    u.listToTable(list(upload), 'prescripcio_social_hiperfrequentadors', 'exadata')

if __name__ == "__main__":
    # extract_perfil_prescriptor_social()
    prescripcio_social_hiperfrequentadors()