import sisapUtils as u
import collections as c

file = './EXTRACCIONDATOS DPO PRIMARIA VALLES ORIENTAL I OCCIDENTAL.csv'

dades = []
count = 0
cols = []
for row in u.readCSV(file, sep=';'):
    if count == 0:
        cols = [tuple(row)]
        count += 1
    else:
        dades.append(row)

professionals = []
for row in dades:
    if 'Incapacitat' in row[20] or 'incapacitat' in row[20] or 'baixes' in row[20] or 'QCIT' in row[22] or 'baixes' in row[22] or 'baixa' in row[22] or 'Incapacitat' in row[22]:
        print(row[20])
        professionals.append(tuple(row))

u.writeCSV('dpo_it_valles.csv', cols+professionals, sep=';')