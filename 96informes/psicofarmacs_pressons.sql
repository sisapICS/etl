#####
## Projecte: psicofarmacs en centres penitenciaris
#####

# 1. Població d_estudi
## cip (en prision)
## first_ingress (esta es la fecha index del estudio: la primera entrada en prisión between date '2016-01-01' and date '2018-05-31', de encarcelamientos >365 dias acumulados)
## up_ppal (el centro penitenciario principal definido como el de mayor duración)
## up_ppal_days (dias de estancia acumulada en la up_ppal)
## cummulative_days (dias de estancia acumulada totales)
## distinct_ups (numero de carceles por las que ha estado)
## n_moviments (numero de reasignaciones ,de modulos carcelarios, por las que ha pasado)

# TODO población: falta aplicar explicitamente los criterios de exclusión: "primera estancia en prision: sin ingresos previos a la fecha index"
# TODO variables_secundarias: falta añadir la fecha de ultima visita (antes de la fecha index) en primaria, para ello creo una tabla de relación 1:1 id_cip_sec_prision:id_cip_primaria en el siguiente paso 

select
	ingressos.*, TIMESTAMPDIFF(YEAR, a.usua_data_naixement, first_ingress) AS age_ingress_anys
from 
	(select 
		cip
		,min(ingres) as first_ingress -- el primer ingres
		,left(group_concat(huab_up_codi order by dif desc),5) as up_ppal-- up principal
		,max(dif) as up_ppal_days -- els dies que va estar en la UP que mas dies va estar (per agafar la UP principal)
		,sum(dif) as cummulative_days -- el total de dies del pacient
		,count(*) as distinct_ups
		,sum(n_moviments) as n_moviments
	from 
		(select
			id_cip_sec as cip
			,huab_up_codi
			,min(huab_data_ass) as ingres
			,sum(datediff(if(huab_data_final = 0, d.data_ext, huab_data_final), huab_data_ass)) as dif	-- sum(dias amb cada uba)
			,count(*) as n_moviments
		from
			import_jail.moviments
			,dextraccio d
		group by
			id_cip_sec
			,huab_up_codi
		) moviments_vw
	group by
	 	cip
	having
		min(ingres) between date '2016-01-01' and date '2018-05-31'
		and sum(dif) > 365
	) ingressos
left join
	import_jail.assignada a
		on a.id_cip_sec=ingressos.cip
;

# 2. tabla de relación de ids prision:primaria
# necesaria para buscar las visitas en primaria de los prisioneros.
# id_cip_sec_pressons : id_cip_primaria (relación 1:1 para facilitar el id_cip_sec_primaria)

# ojo solo 1 registro por prisionero que exista en primaria.
# ojo el id en primaria es el id_cip (sin sector), aun no tengo claro si es mejor pasar a id_cip_sector de primaria (dependera de la tabla de visitas como rule mejor)

select
	left(group_concat(distinct id_cip_sec order by id_cip_sec),locate(',',group_concat(distinct id_cip_sec order by id_cip_sec))-1) as id_cip_sec_pressons,
	left(group_concat(distinct id_cip order by id_cip desc),locate(',',group_concat(distinct id_cip order by id_cip desc))-1) as id_cip_primaria
	-- 	group_concat(distinct id_cip_sec order by id_cip_sec) as id_cip_sec_pressons
	-- 	,group_concat(distinct id_cip order by id_cip desc) as id_cip_primaria
from import.u11withjail
group by hash_d
having
	left(group_concat(distinct id_cip_sec order by id_cip_sec),1)='-'
	and left(group_concat(distinct id_cip order by id_cip desc),1)<>'-'
;


# 3. catalogo de psicofarmacos
drop temporary table  if exists cat_atc_psicofarmacs;

create temporary table cat_atc_psicofarmacs (atc3 char(3) not null default '', atc7 char(7) not null default '', atc7_desc varchar(50) not null default '') ENGINE = MEMORY;

INSERT INTO cat_atc_psicofarmacs
    (atc3, atc7, atc7_desc)
VALUES
  ('N02','N02AA01','Morfina'),
	('N02','N02AA03','HIDROMORFONA'),
	('N02','N02AA05','OXICODONA'),
	('N02','N02AB03','Fentanilo'),
	('N02','N02AX02','Tramadol'),
	('N03','N03AA02','Fenobarbital'),
	('N03','N03AA03','PRIMIDONA'),
	('N03','N03AE01','Clonazepam'),
	('N03','N03AF01','Carbamazepina'),
	('N03','N03AF02','Oxcarbazepina'),
	('N03','N03AF03','Rufinamida'),
	('N03','N03AF04','Eslicarbazepina'),
	('N03','N03AG01','Ac.Valproic'),
	('N03','N03AG04','Vigatrabina'),
	('N03','N03AG06','Tiagabina'),
	('N03','N03AX09','Lamotrigina'),
	('N03','N03AX11','Topiramat'),
	('N03','N03AX12','Gabapapentina'),
	('N03','N03AX14','Levetiracetam'),
	('N03','N03AX15','Zonisamida'),
	('N03','N03AX16','Pregabalina'),
	('N03','N03AX18','Lacosamida'),
	('N04','N04AA01','Trihexifenidilo'),
	('N04','N04AA02','Biperidèn'),
	('N04','N04BA02','Levodopa amb inhibidor de la descarboxilasa'),
	('N05','N05AA01','Clorpromazina'),
	('N05','N05AA02','Levomepromazina'),
	('N05','N05AB02','Flufenazina'),
	('N05','N05AB03','Perfenazina'),
	('N05','N05AC01','Periciazina'),
	('N05','N05AD01','Haloperidol'),
	('N05','N05AD08','Droperidol'),
	('N05','N05AE04','Ziprasidona'),
	('N05','N05AF05','Zuclopentixol'),
	('N05','N05AG02','Pimozida'),
	('N05','N05AH01','Loxapina'),
	('N05','N05AH02','Clozapina'),
	('N05','N05AH03','Olanzapina'),
	('N05','N05AH04','Quetiapina'),
	('N05','N05AH05','Asenapina'),
	('N05','N05AH06','Clotiapina'),
	('N05','N05AL01','Sulpiride'),
	('N05','N05AL03','Tiaprida'),
	('N05','N05AL05','Amisulprida'),
	('N05','N05AL07','Levosulpirida'),
	('N05','N05AN01','Litio'),
	('N05','N05AX08','Risperidona'),
	('N05','N05AX12','Aripiprazol'),
	('N05','N05AX13','Paliperidona'),
	('N05','N05BA01','Diazepam'),
	('N05','N05BA02','Clordiazepóxido'),
	('N05','N05BA05','Clorazepat dipotàssic'),
	('N05','N05BA06','Lorazepam'),
	('N05','N05BA08','Bromazepam'),
	('N05','N05BA09','Clobazam'),
	('N05','N05BA10','Ketazolam'),
	('N05','N05BA12','Alprazolam'),
	('N05','N05BA14','Pinazepam'),
	('N05','N05BB01','Hidroxizina'),
	('N05','N05CD01','Flurazepam'),
	('N05','N05CD05','Triazolam'),
	('N05','N05CD06','Lormetazepam'),
	('N05','N05CD08','Midazolam'),
	('N05','N05CD09','Brotizolam'),
	('N05','N05CD10','Quazepam'),
	('N05','N05CD11','Loprazolam'),
	('N05','N05CF01','Zoplicona'),
	('N05','N05CF02','Zolpidem'),
	('N05','N05CH01','Melatonina'),
	('N05','N05CM02','Clometiazole'),
	('N06','N06AA02','Imipramina'),
	('N06','N06AA04','Clomipramina'),
	('N06','N06AA06','Trimipramina'),
	('N06','N06AA09','Amitriptilina'),
	('N06','N06AA10','Nortriptilina'),
	('N06','N06AA12','Doxepina'),
	('N06','N06AA14','Melitraceno'),
	('N06','N06AA21','Maprotilina'),
	('N06','N06AB10','Escitalopram'),
	('N06','N06AB03','Fluoxetina'),
	('N06','N06AB04','Citalopram'),
	('N06','N06AB05','Paroxetina'),
	('N06','N06AB06','Sertralina'),
	('N06','N06AB08','Fluvoxamina'),
	('N06','N06AC01','Amitriptilina/ perfenazina'),
	('N06','N06AX03','Mianserina'),
	('N06','N06AX05','Trazodona'),
	('N06','N06AX11','Mirtazapina'),
	('N06','N06AX12','Bupropion'),
	('N06','N06AX14','Tianeptina'),
	('N06','N06AX16','Venlafaxina'),
	('N06','N06AX18','Reboxetina'),
	('N06','N06AX21','Duloxetina'),
	('N06','N06AX22','Agomelatina'),
	('N06','N06AX23','Desvenlafaxina'),
	('N06','N06BA04','Metilfenidat'),
	('N06','N06BA07','Modafinilo'),
	('N06','N06BA09','Atomoxetina'),
	('N06','N06BA12','Lisdexanfetamina'),
	('N06','N06BX03','Piracetam'),
	('N06','N06BX06','Citicolina'),
	('N06','N06CA02','Flupentixol'),
	('N07','N07BB01','Disulfiram'),
	('N07','N07BB04','Naltrexona'),
	('N07','N07BC02','Metadona'),
	('N07','N07BC51','Buprenorfina combinacions')
;

select * from cat_atc_psicofarmacs;


# 4. tabla de psicofarmacos por paciente
# pendiente de construir
# exploratoria:
## 113 ATC diferentes entre N02 ·· N07 (hay 106 en catalogo del paso 3., 100 de ellos en catalogo cpftb006)
## la mitad de los registros (unos 400k) son psicofarmacos N02 ·· N07
## hay 44016 registros sin codigo ATC, de los que 43914 tampoco tienen pf_codi, y 35 codigos pf con conteos, de ellos el pf: 759399 es el principal y tiene solo 22 registros
## registros sin pf por año de inicio de prescripción:
### 2014	375
### 2015	18067
### 2016	12207
### 2017	5407
### 2018	4767
### 2019	3091 
 

select ppfmc_durada, count(*) from import_jail.tractaments where ppfmc_pf_codi=0 group by 1;


# 5. calculo de consumos en numero de DDD prescritas (por producto farmaceutico). y por grupo de ATC3


SELECT pf_cod_atc, count(*) FROM import.cat_cpftb006 pf
	WHERE 	pf_tipus_pf = 1 and pf_situacio <> 'B' and substr(pf_cod_atc,1,3) BETWEEN 'N02' AND 'N07' OR pf_cod_atc = ''
GROUP BY pf_cod_atc ORDER BY LENGTH(pf_cod_atc) desc;  -- pf_codi, pf_cod_atc, pf_via_adm, pf_cod_um_ddd, pf_val_ddd, pf_codi_fi_ddd, pf_desc

SELECT * FROM import.cat_cpftb001 ce WHERE rownum <50; -- ce_pf_codi ce_ind_pa_pri, ce_quantitat, ce_uni_mesu  #not_in_import
SELECT * FROM import.cat_cpftb014 um WHERE rownum <50; -- um_codi, um_desc
SELECT * FROM import.cat_cpftb010 atc where left(atc_codi,3) between 'N02' and 'N07'; -- atc_codi, atc_desc, atc_gpare
SELECT * FROM import.cat_cpftb015 vias;  #not_in_import
SELECT * FROM import.cat_cpftb005 principis;  #not_in_import


SELECT count(*) FROM ppftb116 hppf WHERE rownum <50;
SELECT ppfmc_atccodi, count(*), count(DISTINCT ppfmc_pascodi) FROM ppftb016 ppf WHERE substr(ppfmc_atccodi ,1,3) BETWEEN 'N02' AND 'N07' OR ppfmc_atccodi IS NULL GROUP BY ppfmc_atccodi ORDER BY LENGTH(ppfmc_atccodi) desc; -- ppfmc_pascodi, ppfmc_atccodi


select
  n_vies,
  g_vies,
  sum(n_pf),
  group_concat(
    distinct pf_cod_atc
    order by
      pf_cod_atc
  )
from
  (
    select
      pf_cod_atc,
      count(*) as n_pf,
      count(distinct PF_VIA_ADM) as n_vies,
      group_concat(
        distinct PF_VIA_ADM
        order by
          PF_VIA_ADM
      ) as g_vies
    from
      import.cat_cpftb006 pf
    where
      pf_tipus_pf = 1
      and pf_situacio <> 'B' -- and substr(pf_cod_atc,1,3) between 'N02' and 'N07'
      and pf_cod_atc in (
        'N02AA01',
        'N02AA03',
        'N02AA05',
        'N02AB03',
        'N02AX02',
        'N03AA02',
        'N03AA03',
        'N03AE01',
        'N03AF01',
        'N03AF02',
        'N03AF03',
        'N03AF04',
        'N03AG01',
        'N03AG04',
        'N03AG06',
        'N03AX09',
        'N03AX11',
        'N03AX12',
        'N03AX14',
        'N03AX15',
        'N03AX16',
        'N03AX18',
        'N04AA01',
        'N04AA02',
        'N04BA02',
        'N05AA01',
        'N05AA02',
        'N05AB02',
        'N05AB03',
        'N05AC01',
        'N05AD01',
        'N05AD08',
        'N05AE04',
        'N05AF05',
        'N05AG02',
        'N05AH01',
        'N05AH02',
        'N05AH03',
        'N05AH04',
        'N05AH05',
        'N05AH06',
        'N05AL01',
        'N05AL03',
        'N05AL05',
        'N05AL07',
        'N05AN01',
        'N05AX08',
        'N05AX12',
        'N05AX13',
        'N05BA01',
        'N05BA02',
        'N05BA05',
        'N05BA06',
        'N05BA08',
        'N05BA09',
        'N05BA10',
        'N05BA12',
        'N05BA14',
        'N05BB01',
        'N05CD01',
        'N05CD05',
        'N05CD06',
        'N05CD08',
        'N05CD09',
        'N05CD10',
        'N05CD11',
        'N05CF01',
        'N05CF02',
        'N05CH01',
        'N05CM02',
        'N06AA02',
        'N06AA04',
        'N06AA06',
        'N06AA09',
        'N06AA10',
        'N06AA12',
        'N06AA14',
        'N06AA21',
        'N06AB010',
        'N06AB03',
        'N06AB04',
        'N06AB05',
        'N06AB06',
        'N06AB08',
        'N06AC01',
        'N06AX03',
        'N06AX05',
        'N06AX11',
        'N06AX12',
        'N06AX14',
        'N06AX16',
        'N06AX18',
        'N06AX21',
        'N06AX22',
        'N06AX23',
        'N06BA04',
        'N06BA07',
        'N06BA09',
        'N06BA12',
        'N06BX03',
        'N06BX06',
        'N06CA02',
        'N07BB01',
        'N07BB04',
        'N07BC02',
        'N07BC51'
      )
      and exists (
        select
          1
        from
          import.cat_cpftb010 atc
        where
          pf.pf_cod_atc = atc.atc_codi
      )
    group by
      pf_cod_atc
    ORDER BY
      LENGTH(pf_cod_atc) desc
  ) x
group by
  n_vies,
  g_vies;


/*
PF_VIA_ADM (Route per ATC/DDD WHO)
-----------------------------
B10	PARENTERAL (P)
B11	INTRAMUSCULAR (P)
B12	INTRAVENOSA (P)
B13	SUBCUTANEA (P)
B21	ORAL (O)
B22	SUBLINGUAL (SL)
B24	BUCAL (SL)
B32	PERCUTANEA (TD)
B34	NASAL (N)
B41	RECTAL (R)

case pf_via_adm
when 'B10' then 'P'
when 'B11' then 'P'
when 'B12' then 'P'
when 'B13' then 'P'
when 'B21' then 'O'
when 'B22' then 'SL'
when 'B24' then 'SL'
when 'B32' then 'TD'
when 'B34' then 'N'
when 'B41' then 'R'
else 'desconocido'
end

Route of administration (Adm.R)
-------------------------------
Implant	=	Implant
Inhal	= 	Inhalation
Instill	= 	Instillation
N	=	nasal
O	=	oral
P	=	parenteral
R	=	rectal
SL	= 	sublingual/buccal/oromucosal
TD	=	transdermal
V	=	vaginal

*/

# catalogo:

SELECT
	ce.ce_pascodi,
	pa.pasdescri,
	ce.ce_quantitat,
	ce.ce_uni_mesu,
	um.um_desc,
	ce.ce_ind_pa_pri,
	ce.ce_pf_codi,
	pf.pf_desc, pf.pf_unitats, pf.pf_cod_atc, pf.pf_ff_codi, ff.ff_descri, pf_via_adm, va.va_desc, pf_val_ddd, pf_cod_um_ddd
	-- , ce_quantitat * pf_unitats AS Dosis_envase
--	pf.*
FROM
	cpftb001 ce,
	cpftb014 um,
	cpftb005 pa,
	cpftb015 va,
	cpftb002 ff,
	(SELECT
	PF_CODI,
	pf_desc,
	pf_situacio, pf_any_dat_sit, pf_mes_dat_sit,
	pf_tipus_pf,
	pf_unitats,
	pf_cod_atc,
	-- pf_ind_fact,
	pf_ff_codi,
	pf_via_adm,
	pf_val_ddd,
	pf_cod_um_ddd
FROM
	cpftb006 pf
WHERE
	-- pf_tipus_pf <> 1
	pf_cod_atc IN (
        'N02AA01',
        'N02AA03',
        'N02AA05',
        'N02AB03',
        'N02AX02',
        'N03AA02',
        'N03AA03',
        'N03AE01',
        'N03AF01',
        'N03AF02',
        'N03AF03',
        'N03AF04',
        'N03AG01',
        'N03AG04',
        'N03AG06',
        'N03AX09',
        'N03AX11',
        'N03AX12',
        'N03AX14',
        'N03AX15',
        'N03AX16',
        'N03AX18',
        'N04AA01',
        'N04AA02',
        'N04BA02',
        'N05AA01',
        'N05AA02',
        'N05AB02',
        'N05AB03',
        'N05AC01',
        'N05AD01',
        'N05AD08',
        'N05AE04',
        'N05AF05',
        'N05AG02',
        'N05AH01',
        'N05AH02',
        'N05AH03',
        'N05AH04',
        'N05AH05',
        'N05AH06',
        'N05AL01',
        'N05AL03',
        'N05AL05',
        'N05AL07',
        'N05AN01',
        'N05AX08',
        'N05AX12',
        'N05AX13',
        'N05BA01',
        'N05BA02',
        'N05BA05',
        'N05BA06',
        'N05BA08',
        'N05BA09',
        'N05BA10',
        'N05BA12',
        'N05BA14',
        'N05BB01',
        'N05CD01',
        'N05CD05',
        'N05CD06',
        'N05CD08',
        'N05CD09',
        'N05CD10',
        'N05CD11',
        'N05CF01',
        'N05CF02',
        'N05CH01',
        'N05CM02',
        'N06AA02',
        'N06AA04',
        'N06AA06',
        'N06AA09',
        'N06AA10',
        'N06AA12',
        'N06AA14',
        'N06AA21',
        'N06AB10',
        'N06AB03',
        'N06AB04',
        'N06AB05',
        'N06AB06',
        'N06AB08',
        'N06AC01',
        'N06AX03',
        'N06AX05',
        'N06AX11',
        'N06AX12',
        'N06AX14',
        'N06AX16',
        'N06AX18',
        'N06AX21',
        'N06AX22',
        'N06AX23',
        'N06BA04',
        'N06BA07',
        'N06BA09',
        'N06BA12',
        'N06BX03',
        'N06BX06',
        'N06CA02',
        'N07BB01',
        'N07BB04',
        'N07BC02',
        'N07BC51'
      )) pf
WHERE
	pf.pf_codi = ce.ce_pf_codi
	AND pf.pf_ff_codi = ff.ff_codi
	AND pf.pf_via_adm = va.va_codi
	AND ce.ce_uni_mesu = um.um_codi
	AND ce.ce_pascodi = pa.pascodi
	AND ce.ce_ind_pa_pri = 'S'
--	AND ce.ce_pf_codi IN (654805, 793604)
;


#
N05AH03
N05AX13
N05AX13
N05AX08
N05AX12
N05AD01
N05AB02
N05AB03
N04BA02

-- n: 269951
-- n de inner join con cpftb001 ce : 263564
SELECT pf_codi, pf_desc, ATC, principi, VIA, min(quantitat) AS quantitat_mg, min(ddd) AS ddd_mg, min(quantitat)/min(DDD) AS ratio_quant_ddd, count(*) FROM (
SELECT
	ppfmc_pf_codi AS pf_codi,
	pf_desc,
	ppfmc_posologia AS posologia,
	ce_quantitat AS quantitat,
	ppfmc_freq AS freq,
	ppfmc_durada AS durada,
	ppfmc_pmc_data_ini AS data_inici,
	ppfmc_data_fi AS data_fi,
	ppfmc_atccodi AS ATC,
	pasdescri AS principi,
	pf_via_adm AS via,
	ce_quantitat * ppfmc_posologia * (24/ppfmc_freq) AS DDP,
	CASE WHEN pf_val_ddd = 0 THEN NULL ELSE pf_val_ddd END AS DDD,
	CASE WHEN pf_val_ddd = 0 THEN null ELSE (ce_quantitat * ppfmc_posologia * (24/ppfmc_freq))/ pf_val_ddd END AS DDDPr
FROM (
SELECT
		PPFMC_PF_CODI
	,PPFMC_PROD_FORM
	,PPFMC_DURADA
	,PPFMC_FREQ
	,PPFMC_POSOLOGIA
	,PPFMC_NUM_RECS
	,PPFMC_PERIODICITAT
	,PPFMC_NUM_RECS_SIRE
	,PPFMC_PERIODICITAT_SIRE
	,PPFMC_PASCODI
	,PPFMC_ATCCODI
	,PPFMC_VIA_ADM,
	-- pf_ff_codi,
	case pf_via_adm when 'B10' then 'P'
					when 'B11' then 'P'
					when 'B12' then 'P'
					when 'B13' then 'P'
					when 'B21' then 'O'
					when 'B22' then 'SL'
					when 'B24' then 'SL'
					when 'B32' then 'TD'
					when 'B34' then 'N'
					when 'B41' then 'R'
					WHEN NULL THEN 'pfff'
					else 'pfff'
					END AS pf_via_adm,
	pf_desc,
	pf_val_ddd,
	pf_cod_um_ddd,
	ppfmc_pmc_data_ini,
	ppfmc_data_fi,
		ce.ce_pascodi,
	ce.ce_quantitat,
	ce.ce_uni_mesu,
	ce.ce_ind_pa_pri,
	pa.pasdescri
FROM
	ppftb016 ppf
LEFT JOIN cpftb006 pf ON
	ppfmc_pf_codi = pf.pf_codi
left JOIN cpftb001 ce ON
	ppf.ppfmc_pf_codi = ce.ce_pf_codi
	AND ce.ce_ind_pa_pri = 'S'
LEFT JOIN cpftb005 pa ON
	ce.ce_pascodi = pa.pascodi
where
	pf_cod_atc
	-- ppfmc_atccodi
	IN (
'N02AA01','N02AA03','N02AA05','N02AB03','N02AX02','N03AA02','N03AA03','N03AB02',
'N03AE01','N03AF01','N03AF02','N03AF03','N03AF04','N03AG01','N03AG04','N03AG06',
'N03AX09','N03AX11','N03AX12','N03AX14','N03AX15','N03AX16','N03AX18','N04AA01',
'N04AA02','N04BA02','N05AA01','N05AA02','N05AB02','N05AB03','N05AC01','N05AD01',
'N05AD08','N05AE04','N05AF05','N05AG02','N05AH01','N05AH02','N05AH03','N05AH04',
'N05AH05','N05AH06','N05AL01','N05AL03','N05AL05','N05AL07','N05AN01','N05AX08',
'N05AX12','N05AX13','N05BA01','N05BA02','N05BA05','N05BA06','N05BA08','N05BA09',
'N05BA10','N05BA12','N05BA14','N05BB01','N05CD01','N05CD05','N05CD06','N05CD08',
'N05CD09','N05CD10','N05CD11','N05CF01','N05CF02','N05CH01','N05CM02','N06AA02',
'N06AA04','N06AA06','N06AA09','N06AA10','N06AA12','N06AA14','N06AA21','N06AB10',
'N06AB03','N06AB04','N06AB05','N06AB06','N06AB08','N06AC01','N06AX03','N06AX05',
'N06AX11','N06AX12','N06AX14','N06AX16','N06AX18','N06AX21','N06AX22','N06AX23',
'N06BA04','N06BA07','N06BA09','N06BA12','N06BX03','N06BX06','N06CA02','N07BB01',
'N07BB04','N07BC02','N07BC51')
)
-- WHERE CASE WHEN pf_val_ddd = 0 THEN null ELSE (ce_quantitat * ppfmc_posologia * (24/ppfmc_freq))/ pf_val_ddd END < 0.1
)
GROUP BY 
 pf_codi, pf_desc, ATC, principi, VIA, quantitat / DDD
ORDER BY nvl(quantitat / DDD, 0)
; 

/*
entregables:
Taula 1.
hash  edat  up  tall	atc3	atc7	pf	posol	freq	quantitat	dosis	DDD	DDDpr 

taula 2.
hash	edat up	tall	atc3	n_atc	n_pf	DDDpr-ATC3 
*/


-- version para P2262
SELECT pf_codi, pf_desc, ATC, principi, VIA, min(quantitat) AS quantitat_mg, min(ddd) AS ddd_mg, min(quantitat)/min(DDD) AS ratio_quant_ddd, count(*) FROM (
SELECT
	ppfmc_pf_codi AS pf_codi,
	pf_desc,
	ppfmc_posologia AS posologia,
	ce_quantitat AS quantitat,
	ppfmc_freq AS freq,
	ppfmc_durada AS durada,
	ppfmc_pmc_data_ini AS data_inici,
	ppfmc_data_fi AS data_fi,
 	ppfmc_atccodi AS ATC,
	pasdescri AS principi,
 	pf_via_adm AS via,
	ce_quantitat * ppfmc_posologia * (24/ppfmc_freq) AS DDP,
	CASE WHEN pf_val_ddd = 0 THEN NULL ELSE pf_val_ddd END AS DDD,
	CASE WHEN pf_val_ddd = 0 THEN null ELSE (ce_quantitat * ppfmc_posologia * (24/ppfmc_freq))/ pf_val_ddd END AS DDDPr
FROM (
SELECT
		PPFMC_PF_CODI
-- 	,PPFMC_PROD_FORM
	,PPFMC_DURADA
	,PPFMC_FREQ
	,PPFMC_POSOLOGIA
-- 	,PPFMC_NUM_RECS
-- 	,PPFMC_PERIODICITAT
-- 	,PPFMC_NUM_RECS_SIRE
-- 	,PPFMC_PERIODICITAT_SIRE
-- 	,PPFMC_PASCODI
	,PPFMC_ATCCODI
-- 	,PPFMC_VIA_ADM
	-- ,pf_ff_codi
	,case pf_via_adm when 'B10' then 'P'
					when 'B11' then 'P'
					when 'B12' then 'P'
					when 'B13' then 'P'
					when 'B21' then 'O'
					when 'B22' then 'SL'
					when 'B24' then 'SL'
					when 'B32' then 'TD'
					when 'B34' then 'N'
					when 'B41' then 'R'
					WHEN NULL THEN 'pfff'
					else 'pfff'
					END AS pf_via_adm
	,pf_desc
	,pf_val_ddd
	,pf_cod_um_ddd
	,ppfmc_pmc_data_ini
	,ppfmc_data_fi
		,ce.ce_pascodi
	,ce.ce_quantitat
	,ce.ce_uni_mesu
	,ce.ce_ind_pa_pri
	,pa.pasdescri
FROM
	-- ppftb016 ppf
	import_jail.tractaments ppf
LEFT JOIN import.cat_cpftb006 pf ON
	ppfmc_pf_codi = pf.pf_codi
left JOIN import.cat_cpftb001 ce ON
	ppf.ppfmc_pf_codi = ce.ce_pf_codi
	AND ce.ce_ind_pa_pri = 'S'
LEFT JOIN import.cat_cpftb005 pa ON
	ce.ce_pascodi = pa.pascodi
where	
	pf_cod_atc
	-- ppfmc_atccodi
	IN
 	('N02AA01','N02AA03','N02AA05','N02AB03','N02AX02','N03AA02','N03AA03','N03AB02',
 	'N03AE01','N03AF01','N03AF02','N03AF03','N03AF04','N03AG01','N03AG04','N03AG06',
 	'N03AX09','N03AX11','N03AX12','N03AX14','N03AX15','N03AX16','N03AX18','N04AA01',
 	'N04AA02','N04BA02','N05AA01','N05AA02','N05AB02','N05AB03','N05AC01','N05AD01',
 	'N05AD08','N05AE04','N05AF05','N05AG02','N05AH01','N05AH02','N05AH03','N05AH04',
 	'N05AH05','N05AH06','N05AL01','N05AL03','N05AL05','N05AL07','N05AN01','N05AX08',
 	'N05AX12','N05AX13','N05BA01','N05BA02','N05BA05','N05BA06','N05BA08','N05BA09',
 	'N05BA10','N05BA12','N05BA14','N05BB01','N05CD01','N05CD05','N05CD06','N05CD08',
 	'N05CD09','N05CD10','N05CD11','N05CF01','N05CF02','N05CH01','N05CM02','N06AA02',
 	'N06AA04','N06AA06','N06AA09','N06AA10','N06AA12','N06AA14','N06AA21','N06AB10',
 	'N06AB03','N06AB04','N06AB05','N06AB06','N06AB08','N06AC01','N06AX03','N06AX05',
 	'N06AX11','N06AX12','N06AX14','N06AX16','N06AX18','N06AX21','N06AX22','N06AX23',
 	'N06BA04','N06BA07','N06BA09','N06BA12','N06BX03','N06BX06','N06CA02','N07BB01',
 	'N07BB04','N07BC02','N07BC51')
) y
-- WHERE CASE WHEN pf_val_ddd = 0 THEN null ELSE (ce_quantitat * ppfmc_posologia * (24/ppfmc_freq))/ pf_val_ddd END < 0.1
) x
GROUP BY 
 pf_codi, pf_desc, ATC, principi, VIA, quantitat / DDD
-- ORDER BY nvl(quantitat / DDD, 0)
;