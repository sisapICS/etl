import sisapUtils as u
import collections

# taking people assigned to uba3
print('assigned')
assignats = {}
sql = """select id_cip_sec, uas, up from nodrizas.assignada_tot"""
for id, uba3, up in u.getAll(sql, 'nodrizas'):
    assignats[id] = (uba3, up)

info_up = {}
# taking ups descriptions, sap, ambits...
sql = """select scs_codi, ics_desc, amb_desc, sap_desc from nodrizas.cat_centres"""
for up, desc_up, ambit, sap in u.getAll(sql, 'nodrizas'):
    info_up[up] = [desc_up, ambit, sap]


# taking the login corresponding to uba3
print('uba3')
login_uba3 = {}
logins_to_ubas = collections.defaultdict(list)
for e, sector in enumerate(u.sectors):
    print(e, sector)
    sql = """select ide_usuari login, uas_codi_up, uas_codi_uas, ide_nom_usuari
                from pritb992 p
                join (
                SELECT uas_codi_up, uas_codi_uas, UAS_PROVEIDOR_DNI_PROVEIDOR, UAS_DESCRIPCIO 
                FROM vistb099) v
                ON p.ide_dni = v.UAS_PROVEIDOR_DNI_PROVEIDOR"""
    for login, up, uba3, nom in u.getAll(sql, sector):
        login_uba3[login] = (nom, up)
        logins_to_ubas[login].append(uba3)

# studying programmed visits
print('getting visits')
uba_assigned = collections.Counter()
uba_not_assigned = collections.Counter()
uba_programmed = collections.Counter()
uba_not_programmed = collections.Counter()
sql = """select visi_assign_visita, id_cip_sec, visi_servei_codi_servei, visi_modul_codi_modul 
            from import.visites1
            where visi_dia_peticio >= date("2022-05-01")"""
for login, patient, servei, modul in u.getAll(sql, 'import'):
    try:
        uba3_a, up_a = assignats[patient]
        if uba3_a in logins_to_ubas[login]:
            uba_assigned[login] += 1
        else:
            uba_not_assigned[login] += 1
    except:
        uba_not_assigned[login] += 1
    if servei in ('UAAU', 'UAC'):
        try:
            uba3_a, up_a = assignats[patient]
            if modul in logins_to_ubas[login]:
                if modul == uba3_a:
                    uba_programmed[(login, modul)] += 1
                else:
                    uba_not_programmed[(login, modul)] += 1
        except:
            uba_not_programmed[(login, modul)] += 1

upload = []
# table programmed visits everywhere
print('export 1')
for login in uba_assigned:
    N_assignats = uba_assigned[login]
    try:
        N_no_assignats = uba_not_assigned[login]
    except: N_no_assignats = 0
    try:
        up = login_uba3[login][1]
        if up in info_up:
            desc_up = info_up[up][0]
            ambit= info_up[up][1]
            sap = info_up[up][2]
        else:
            up = ''
            desc_up = ''
            ambit= ''
            sap = ''
    except:
        up = ''
        desc_up = ''
        ambit= ''
        sap = ''
    try:
        nom = login_uba3[login][0]
    except:
        nom = ''
    upload.append((login, nom, up, desc_up, ambit, sap, N_assignats, N_no_assignats))

cols = """(login varchar(20), nom varchar(40), up varchar(5),
            desc_up varchar(40), ambit varchar(40), sap varchar(40),
            N_assignats int, N_no_assignats int)"""
u.createTable('uba3_programats', cols, 'test', rm = True)
u.listToTable(upload, 'uba3_programats', 'test')

# Programmed for uba3 "visit"
print('export 2')
upload = []
for e in uba_programmed:
    login = e[0]
    uba3 = e[1]
    try:
        up = login_uba3[login][1]
        if up in info_up:
            desc_up = info_up[up][0]
            ambit= info_up[up][1]
            sap = info_up[up][2]
    except:
        up = ''
        desc_up = ''
        ambit= ''
        sap = ''
    try:
        nom = login_uba3[login][0]
    except:
        nom = ''
    try:
        programats_no_assignats = uba_not_programmed[e]
    except:
        programats_no_assignats = 0
    upload.append((login, nom, up, desc_up, ambit, sap, uba_programmed[e], programats_no_assignats))

cols = '(login varchar(20), nom varchar(40), up varchar(5), desc_up varchar(40), ambit varchar(40), sap varchar(40), N_assignats int, N_NO_assignats int)'
u.createTable('uba3_programats_uba', cols, 'test', rm = True)
u.listToTable(upload, 'uba3_programats_uba', 'test')
