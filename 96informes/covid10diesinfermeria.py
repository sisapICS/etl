import sisapUtils as u
from datetime import timedelta
import pandas as pd
from sisapUtils import getAll, getOne, createTable, listToTable
# import csv
# import os
# import sys
# from time import strftime
# from collections import defaultdict, Counter

TEST = False


def visitats_covid10():
    # Dicionari hash-cip
    # print('ddicionari hash cip')
    # hash_to_cip = {}
    # sql = 'select usua_cip, usua_cip_cod from pdp.pdptb101'
    # for id, hash in u.getAll(sql, 'pdp'):
    #     hash_to_cip[hash] = id

    # Diccionari id-data visita aguda (prstb261) --> import.aguda
    print('creaci diccionari id-data visita aguda')
    ids = {}
    sql = """
        select id_cip_sec, cesp_data_alta
        from import.aguda
        where year(cesp_data_alta) = '2020'
        {}
    """.format('AND rownum < 50' if TEST else '')
    for id, data in u.getAll(sql, 'import'):
        ids[id] = data

    # Diccionari id-data visita aguda (prstb337) --
    print('creacio diccionari id-data visita aguda 2')
    sql = """select id_cip_sec, pi_data_inici
        from import.ares a, import.cat_prstb335 b
        where b.pc_aguda = 'S' and a.pi_codi_pc = b.pc_codi
        and year(pi_data_inici) = '2020'
        {}""".format('AND rownum < 50' if TEST else '')
    for id, data in u.getAll(sql, 'import'):
        ids[id] = data



    # Visites per covid
    print('visites per covid')
    sql = """
        SELECT id_cip_sec, pr_dde
        FROM import.problemes
        WHERE
            PR_COD_PS IN (
                'C01-B34.2', 'C01-B97.21',
                'C01-B97.29', 'C01-J12.81', 'C01-J12.89',
                'C01-U07.1', 'C01-Z20.828') and YEAR(pr_dde) = 2020
            {}
    """.format('AND rownum < 50' if TEST else '')

    # Total de gent que ha fet visita aguda
    #   i als seguents 10 dies ha agafat covid
    total = 0
    # Creem diccionari de la gent que em de reportar
    ids_final = {}
    for id, data in u.getAll(sql, 'import'):
        try:
            if ids[id] < data and ids[id] > (data - timedelta(days=10)):
                total += 1
                ids_final[id] = ids[id]

        except Exception as e:
            # print(str(e))
            pass

    print('total: ', total)
    csv = pd.DataFrame(ids_final.items(), columns=['ID_CIP_SEC', 'DATE'])
    csv.to_csv("IDs_dates.csv") 
    
    
def import_information():
    id_date = pd.read_csv("IDs_dates.csv")
    info = []
    for i in range(len(id_date)):
        id = id_date["ID_CIP_SEC"][i]
        print(id)
        date = id_date["DATE"][i]
        # Data naixement, any naixement, edat en anys
        sql_edat = """select data_naix, YEAR(data_naix) 
                        from nodrizas.assignada_tot
                        where id_cip_sec = {}""".format(id)

        for dob, year in u.getAll(sql_edat, 'nodrizas'):
            d_o_b = dob
            year_of_b = year 

        sql_motiu = """select distinct(cesp_cod_mce) from import.aguda
                    where id_cip_sec = {} and 
                    cesp_data_alta = {}""".format(id, date)
        for cesp_cod in u.getAll(sql_motiu, 'nodrizas'):
            cesp = cesp_cod

        sql_info_ares = """select distinct id_cip_sec, pi_anagrama, pi_ser_derivat, pi_data_inici, pi_data_fi, pi_data_alta\
                , pi_codi_pc, pi_alies, pi_ser_derivat_altres, pi_v_ser, pi_altres_ca, pi_pla_terapeutic\
                , pi_mot_altres, pi_mot_tancament from import.ares where 
                pi_data_inici = {} and id_cip_sec = {}""".format(date, id)
        for (id, pi_anagrama, pi_ser_derivat, pi_data_inici, pi_data_fi, pi_data_alta,
            pi_codi_pc, pi_alies, pi_ser_derivat_altres, pi_v_ser, pi_altres_ca, pi_pla_terapeutic,
            pi_mot_altres, pi_mot_tancament) in u.getAll(sql_info_ares, 'nodrizas'):
                an = pi_anagrama
                der = pi_ser_derivat
                ini = pi_data_inici
                fi = pi_data_fi
                alta = pi_data_alta
                cod_pc = pi_codi_pc
                alies = pi_alies
                pi_der_altres = pi_ser_derivat_altres
                pi_v = pi_v_ser
                altres_ca = pi_altres_ca
                pla_ter = pi_pla_terapeutic
                mot_altr = pi_mot_altres
                mot_tanc = pi_mot_tancament

        info.append((d_o_b, year_of_b, cesp, an, de,
                    ini, fi, alta, cod_pc, alies, pi_der_altres,
                    pi_v, altres_ca, pla_ter, mot_altr, mot_tanc))
    

    columns = ('date_of_birth date, year_of_birth date, cesp_cod_mce varchar(15), pi_anagrama varchar(15),\
            pi_ser_derivat varchar(8), pi_data_inici date, pi_data_fi date, pi_data_alta date,\
            pi_codi_pc varchar(8), pi_alies varchar(30), pi_ser_derivat_altres varchar(30), pi_v_ser varchar(30),\
            pi_altres_ca varchar(30), pi_pla_terapeutic varchar(50), pi_mot_altres varchar(30), pi_mot_tancament varchar(10)')
    
    createTable('covid_10_dies_aguda', columns, 'altres', rm=True)
    listToTable(info, 'covid_10_dies_aguda', 'altres')

            
def info_ares():
    data = pd.read_csv("IDs_dates.csv")
    dates = {}
    data['DATE'] = pd.to_datetime(data['DATE'])
    data['DATE'] = data['DATE'].apply(lambda x: x.to_pydatetime())
    data['DATE'] = data['DATE'].apply(lambda x: x.date())
    for i in range(len(data)):
        dates[data['ID_CIP_SEC'][i]] = data['DATE'][i]
    upload = []
    i = 0
    ids = dates.keys()
    sql_info_ares = """select distinct id_cip_sec, pi_anagrama, pi_ser_derivat, pi_data_inici, pi_data_fi, pi_data_alta\
                , pi_codi_pc, pi_alies, pi_ser_derivat_altres, pi_v_ser, pi_altres_ca, pi_pla_terapeutic\
                , pi_mot_altres, pi_mot_tancament from import.ares"""
    print("comencem query")
    for (id, pi_anagrama, pi_ser_derivat, pi_data_inici, pi_data_fi, pi_data_alta,
        pi_codi_pc, pi_alies, pi_ser_derivat_altres, pi_v_ser, pi_altres_ca, pi_pla_terapeutic,
        pi_mot_altres, pi_mot_tancament) in u.getAll(sql_info_ares, 'nodrizas'):
        if id in ids:
            if dates[id] == pi_data_inici:
                upload.append([id, pi_anagrama, pi_ser_derivat, pi_data_inici, pi_data_fi, pi_data_alta,
                                pi_codi_pc, pi_alies, pi_ser_derivat_altres, pi_v_ser, pi_altres_ca, pi_pla_terapeutic,
                                pi_mot_altres, pi_mot_tancament])
                print(i)
                i += 1

    csv = pd.DataFrame(upload, columns =['id', 'pi_anagrama', 'pi_ser_derivat', 'pi_data_inici', 'pi_data_fi', 'pi_data_alta',
                                'pi_codi_pc', 'pi_alies', 'pi_ser_derivat_altres', 'pi_v_ser', 'pi_altres_ca', 'pi_pla_terapeutic',
                                'pi_mot_altres', 'pi_mot_tancament'])
    csv.to_csv('ares.csv')


def info_aguda():
    data = pd.read_csv("IDs_dates.csv")
    dates = {}
    data['DATE'] = pd.to_datetime(data['DATE'])
    data['DATE'] = data['DATE'].apply(lambda x: x.to_pydatetime())
    data['DATE'] = data['DATE'].apply(lambda x: x.date())
    for i in range(len(data)):
        dates[data['ID_CIP_SEC'][i]] = data['DATE'][i]
    upload = []
    i = 0
    ids = dates.keys()
    # sql_hash_cip = """SELECT ID_CIP_SEC, HASH_A, sector, FROM sisap_u11"""
    # hashos = {}
    # for id, hash in u.getAll(sql_info_ares, 'redics'):
    #     if id in ids:
    #         hashos[hash] = id

    sql_info_ares = """SELECT id_cip_sec, a.CESP_DATA_ALTA, a.cesp_cod_mce, 
                        a.cesp_motiu_altres, a.CESP_SER_DERIVAT FROM prstb261 a, 
                        SISAP_U11 b WHERE a.CESP_COD_U = hash_a AND 
                        a.codi_sector = b.sector 
                        AND EXTRACT(YEAR FROM a.CESP_DATA_ALTA) = 2020"""
    print("comencem query")
    for hash, data, motiu, motiu_altres, derivat in u.getAll(sql_info_ares, 'redics'):
        if id in ids:
            if dates[id] == data:
                upload.append([id, data, motiu, motiu_altres, derivat])
                print(i)
                i += 1

    csv = pd.DataFrame(upload, columns =['id', 'data', 'motiu', 'motiu_altres', 'derivacio'])
    csv.to_csv('aguda.csv')





# - edat en anys


# A aguda mirar si puc importar cesp_motiu_altres
# Calcular edat en anys

if __name__ == "__main__":
    #visitats_covid10()
    #import_information()
    #info_ares()
    info_aguda()
