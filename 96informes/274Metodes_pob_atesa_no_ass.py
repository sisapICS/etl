import collections as c
from sisapUtils import createTable, getOne, multiprocess, getAll, listToTable


orig = 'import'
table = 'no_assig_comparacio'
db = 'nodrizas'


def do_it(params):
    taula, poblacio = params
    data = c.Counter()
    sql = "select id_cip, visi_up, visi_data_visita \
           from {}, nodrizas.dextraccio \
           where visi_data_visita between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext and visi_situacio_visita = 'R' and visi_rel_proveidor in ('3', '4')".format(taula)
    for id, up, dat in getAll(sql, orig):
        if up in up_to_aga:
            aga = up_to_aga[up]
            if id in poblacio:
                aga_a, up_a = poblacio[id]
                if up_a != up:
                    data[(up, 'M1')] += 1
                if up_a != up and aga_a != aga:
                    data[(up, 'M2')] += 1
    resul = [k + (v,) for k, v in data.items()]
    return(resul)


if __name__ == '__main__':
    up_to_aga = {up: aga for (up, aga) in getAll("select scs_codi, if(aga='',scs_codi, aga) from cat_centres", "nodrizas")}
    poblacio = {}
    for id, up in getAll("select id_cip, up from assignada_tot", 'nodrizas'):
        poblacio[id] = [up_to_aga[up], up]
    tables = getOne('show create table visites1', 'import')[1].split('UNION=(')[1][:-1].split(',')
    jobs = [(t, poblacio) for t in tables]
    resul = multiprocess(do_it, jobs)
    createTable(table, '(up varchar(5), metode varchar(5), visites int)', db, rm=True)
    visites = c.Counter()
    for chunk in resul:
        for up, metode, n in chunk:
            visites[(up, metode)] += n
    upload = [(up, metode, n) for (up, metode), n in visites.items()]
    listToTable(upload, table, db)

