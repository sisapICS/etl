# -*- coding: utf8 -*-

"""
12493.
"""

import collections as c

import sisapUtils as u


class HEPAT(object):
    """."""

    def __init__(self):
        """."""
        self.dades = []
        self.municipis = {}
        self.nacionalitats = {}
        self.get_centres()
        for sector in u.sectors:
            self.get_municipis(sector)
            self.get_nacionalitat(sector)
            self.get_dades(sector)
        self.export_data()

    def get_centres(self):
        """Trec descripció centre de cat_Centres"""
        self.centres = {}
        sql = 'select ics_desc, scs_codi from cat_centres'
        for desc, up in u.getAll(sql, 'nodrizas'):
            self.centres[up] = desc
        
    def get_municipis(self, sector):
        """Trec els municipis del catàleg ECAP"""
        
        sql = "select muni_codi, muni_descripcio from rittb028 where muni_data_baixa=1"
        for codi, desc in u.getAll(sql, sector):
            self.municipis[codi] = desc
    
    def get_nacionalitat(self, sector):
        """Trec la descripció de la nacionalitat del catàleg"""
        
        sql = "select resi_codi,resi_descripcio from rittb034 where resi_data_baixa=1"
        for codi, desc in u.getAll(sql, sector):
            self.nacionalitats[codi] = desc
    
    
    def get_dades(self, sector):
        """."""

        sql2 = "select usua_uab_up as up, usua_cip as cip, usua_cognom1 as cognom1, usua_cognom2 as cognom2, usua_nom as nom, \
                usua_telefon_principal, usua_telefon_secundari, \
                usua_sexe as sexe, \
                2017-extract(YEAR FROM  to_date(usua_data_naixement,'J')) as edat_17, \
                usua_nacionalitat as nacionalitat,\
                usua_abs_codi_abs, \
                substr(usua_localitat,1,5) as municipi,\
                usua_uab_up \
                from usutb040 where usua_uab_up in ( \
                    '00012', '00021', '00022', '00044', '00050', '00062', '00090', \
                    '00091', '00108', '00119', '00129', '00130', '00154', '00156', \
                    '00159', '00163', '00169', '00171', '00174', '00202', '00291', \
                    '00296', '00346', '00351', '00368', '00369', '06187', '00395', \
                    '00450', '00452', '00470', '00475', '00489', '00491', '00496', \
                    '04054', '05945') \
                    AND usua_situacio = 'A' AND usua_usuari= 'S' \
                    AND extract(YEAR FROM  to_date(usua_data_naixement,'J'))  BETWEEN 1937 AND 2015 \
                    and usua_telefon_principal is not null"
        for row in u.getAll(sql2, sector):
            desc_abs = self.centres[row[12]]
            municipi = self.municipis[row[11]] if row[11] in self.municipis else row[11]
            nacionalitat = self.nacionalitats[row[9]] if row[9] in self.nacionalitats else row[9]
            self.dades.append([row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7], row[8], municipi, nacionalitat, row[10], row[12], desc_abs])

        

    def export_data(self):
        """."""
        header_full = [('up', 'cip', 'cognom1', 'cognom2', 'nom', 'tlf1', 'tlf2', 'sexe', 'edat_17', 'municipi', 'nacionalitat', 'abs', 'up', 'desc_abs')]
        u.writeCSV(u.tempFolder + 'seroprev_full.txt', header_full + sorted(self.dades))
        header = [('tlf','tlf2','sexe','edat_17',  'municipi', 'nacionalitat', 'abs', 'up', 'desc_abs')]
        u.writeCSV(u.tempFolder + 'seroprev.txt', header + sorted([row[5:14] for row in self.dades]))


if __name__ == '__main__':
    HEPAT()
