# coding: utf-8

"""
 - TIME EXECUTION m

 - PETICIÓ:
    - Trello: https://trello.com/c/MUrKZ6BU
    - Drive:

"""

import sisapUtils as u
from datetime import datetime


class mpocMinisterio(object):
    """ . """

    def __init__(self):
        """ . """
        self.get_poblacio()
        self.get_variables()
        self.get_denominador()
        self.get_numerador()

    def get_poblacio(self):
        """ . """
        print("------------------------------------------------- get_poblacio")
        self.poblacio = {}
        sql = """select
                    id_cip
                    , sexe
                 from
                    assignada_tot
                 where
                    ep = '0208'"""
        for id_cip, _sexe in u.getAll(sql, 'nodrizas'):
            self.poblacio[id_cip] = True
        print("N registres:", len(self.poblacio))

    def get_variables(self):
        """ . """
        print("------------------------------------------------ get_variables")
        self.variables = {}
        self.espiro = set()
        self.reg_tec = set()
        self.reg_tec_correcte = set()
        sql = """select
                    id_cip
                    , vu_cod_vs
                    , vu_val
                    , vu_dat_act
                 from
                    variables
                 where
                    vu_cod_vs in ('EF/F',
                                  'TR3017','PESPE')"""
        for id_cip, cod, val, dat in u.getAll(sql, 'import'):
            if id_cip in self.poblacio:
                self.variables[id_cip] = (cod, val, dat)
                if cod in ('EF/F'):
                    self.espiro.add((id_cip, dat))
                if cod in ('TR3017', 'PESPE'):
                    self.reg_tec.add((id_cip, dat))
                    if val in (1, 2, 4):
                        self.reg_tec_correcte.add((id_cip, dat))
        print("N pacients ESPIRO:", len(self.espiro))
        print("N pacients REGISTRE TECNICA:", len(self.reg_tec))
        print("N pacients REGISTRE TECNICA CORRECTE:", len(self.reg_tec_correcte))  # noqa

    def get_denominador(self):
        """ . """
        print("---------------------------------------------- get_denominador")
        self.den1 = set()
        self.den2 = set()
        for (id_cip, dat) in self.espiro:
            self.den1.add((id_cip, dat))
            if (id_cip, dat) in self.reg_tec:
                self.den2.add((id_cip, dat))
        print("N registres DENOMINADOR -1 :", len(self.den1))
        print("N registres DENOMINADOR -1 :", len(self.den2))

    def get_numerador(self):
        """ . """
        print("------------------------------------------------ get_numerador")
        self.num1 = set()
        self.num2 = set()
        for (id_cip, dat) in self.den1:
            if (id_cip, dat) in self.reg_tec:
                self.num1.add((id_cip, dat))
        print("N registres NUMERADOR - 1:", len(self.num1))
        for (id_cip, dat) in self.den2:
            if (id_cip, dat) in self.reg_tec_correcte:
                self.num2.add((id_cip, dat))
        print("N registres NUMERADOR - 2:", len(self.num2))


if __name__ == '__main__':
    print(datetime.now())
    ts = datetime.now()
    mpocMinisterio()
    print('Time execution {}'.format(datetime.now() - ts))
