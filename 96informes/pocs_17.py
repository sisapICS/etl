# coding: iso-8859-1
from sisapUtils import *
import random
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

nod="nodrizas"
imp="import"

periodos=(2016,2017,2018)


def get_up_ambit():
    """Asocia a cada up el ambito al que pertenece. Filtra por las up donde ep= '0208'.
       Devuelve un diccionario {up:amb_desc}
    """
    sql="select scs_codi,amb_desc from {}.cat_centres where ep = '0208'".format(nod)
    return {up:amb_desc for up,amb_desc in getAll(sql,nod)}

def get_tables_year(year):
    """ Selecciona las tablas de activitats cuya fecha tiene un any igual a year y el mes es 6,7 u 8.
        Devuelve una lista con las tablas que cumplen este criterio.
    """
    tables = []
    for table in getSubTables("activitats"):
        dat = [dat for dat, in getAll("select au_dat_act from {} limit 1".format(table), imp)][0]
        #dat, = getOne("select visi_data_visita from {} limit 1".format(table), 'import')
        if dat.year == year and dat.month in(6,7,8):
            tables.append(table)
    printTime("Tables")
    return tables
   

def get_pocs_rows(params):
    """Obtiene las filas de la tabla iterando a traves de los registros de una particion de la tabla de activitats en los cuales el codigo de la actividad
       es POCS. Se coge la up, la fecha y el id del paciente. Filtrando por las up del ics, se obtiene para el id en cuestion, si en el mes (junio,julio o agosto) y 
       any (que viene dado por el parametro year, que se le pasa a la funcion) en el que se hizo la actividad, es adulto o ped, es atdom, pcc o maca 
       (esto ultimo utilizando una func, get_estat). Se genera un contaje uitilizando un diccionario en el que la key es la combinacion de
       mes, ambito, edad, estado atdom, estado pcc y estado maca.
       Devuelve un diccionario {(dat.month,amb,edat,atdom_est,pcc_est,maca_est): N}
    """
    taula,rest_params=params
    year,up_ambit,dat_naix,atdom,estats=rest_params
    printTime(taula)
    sql="select au_up,id_cip_sec,au_dat_act from {} where au_cod_ac='POCS'".format(taula)
    pocs=Counter()
    for up,id,dat in getAll(sql,imp):
        if up in up_ambit:
            amb=up_ambit[up]
            if dat.year==year and dat.month in (6,7,8):
                if id in dat_naix:
                    edat= "adl" if yearsBetween(dat_naix[id],dat) > 13 else "ped"
                    atdom_est,pcc_est,maca_est=[0,0,0]
                    #print(id,len(atdom))
                    if id in atdom:
                        atdom_est=get_estat(atdom[id],dat)
                    if id in estats['ER0001']:
                        pcc_est=get_estat(estats['ER0001'][id],dat)
                    if id in estats['ER0002']:
                        maca_est=get_estat(estats['ER0002'][id],dat)
                    pocs[(dat.month,amb,edat,atdom_est,pcc_est,maca_est)]+=1
                    
    printTime("Pocs {}".format(len(pocs)))
    return pocs


def get_estat(id_fechas,dat):
    """ Obtiene el estado (0: no vigente, 1:vigente ) de un id para una patologia en una fecha, dat concreta.
        Iterando por los tuples (fecha inicio, fin) del set id_fechas, si la dat esta entre las fechas de alguno de estos
        tuples se cambia la variable stat a 1.
        Devuelve un int (0/1)
    """
    stat=0
    for dde,dba in id_fechas:
        if dde <= dat <= dba:
            stat=1
            break
    return stat

def get_multiprocess_total(tables,all_params):
    n_poc_total=Counter()
    for pocs in multiprocess(get_pocs_rows, [(table, all_params) for table in tables]):
        for key in pocs:
            n_poc_total[key] += pocs[key]
    printTime("n poc total done {}".format(len(n_poc_total)))

    return n_poc_total

def get_ids_ATDOM():
    """Selecciona de import.problemes los ids con un codigo que empiece por Z74% y data de baja logica nula. Si dba (fecha de fin) es nula, se pone el 
       dia de hoy como fecha de fin. Se guarda en un diccionario para cada id un tuple con la fecha de dde (diagnostico) y la dba.
       Devuelve un dict {id: (dde,dba)}
    """
    id_atdom_dates=defaultdict(set)
    sql="select id_cip_sec,pr_dde,pr_dba from {}.problemes where pr_cod_ps like 'Z74%' and pr_data_baixa =0".format(imp)
    for id,dde,dba in getAll(sql,imp):
        if not dba:
            dba=datetime.date.today()
        id_atdom_dates[id].add((dde,dba))
    return id_atdom_dates

def get_ids_estat():
    """ Obtiene los pcc y maca y su fecha de diagnostico. De import.estats selecciona los ids y los guarda en un diccionario con sus fechas de dde y
        como fecha de fin la fecha de hoy.
        Devuelve un diccionario {estat: {id: (dde,dba)}}
    """
    id_estat_dates=defaultdict(lambda: defaultdict(set))
    sql="select id_cip_sec,es_dde,es_cod from {}.estats where es_cod in ('ER0001','ER0002')".format(imp)
    for id,dde,es_cod in getAll(sql,imp):
        id_estat_dates[es_cod][id].add((dde,datetime.date.today()))
    return id_estat_dates

def get_id_edat():
        """ Asocia a cada id su fecha de nacimiento.
            Devuelve un dict {id:dat_naix}
        """
        sql = "select id_cip_sec,  usua_data_naixement from {}.assignada".format(imp)
        return {id: dat_naix for id, dat_naix in getAll(sql, imp)  }

def export_txt_file(name,header,rows):
    with open(name,"wb") as out_file:
        print(rows[0])
        row_format ="{:>35}" * len(rows[0])+"\n"
        out_file.write(row_format.format(*header))
        for row in rows:
            out_file.write(row_format.format(*row))



   
if __name__ == '__main__':

    printTime('inici')
    atdom=get_ids_ATDOM()
    print(len(atdom))
    printTime('atdom')
    estats=get_ids_estat()
    printTime('estats')
    dat_naix=get_id_edat()
    printTime('dat_naix')
    up_ambit=get_up_ambit()
    print(len(up_ambit))

    rows=[]
    for year in periodos:
        #se obtienen las tablas de activitats en las quqe hay registros del any en cuestion
        tables=get_tables_year(year)
        rest_params=year,up_ambit,dat_naix,atdom,estats
        n_poc_total=defaultdict(lambda: defaultdict(lambda: defaultdict(Counter)))
        for table in tables:
            poc=get_pocs_rows((table,rest_params))
            printTime("{},{}".format(table,len(poc)))
            for month,amb,edat,atdom_est,pcc_est,maca_est in poc:
                n_poc_total[month][amb][edat][(atdom_est,pcc_est,maca_est)]+=poc[(month,amb,edat,atdom_est,pcc_est,maca_est)]
            #n_poc_total=get_multiprocess_total(tables,rest_params)
        
        rows.extend([('0{}-{}'.format(month,year),amb,edat,atdom_est,pcc_est,maca_est,n) for month in n_poc_total for amb in n_poc_total[month] for edat in n_poc_total[month][amb] for ((atdom_est,pcc_est,maca_est),n) in n_poc_total[month][amb][edat].iteritems()])
        printTime("poc total")
    
    header=["PERIODE","AMB","EDAT","ATDOM","PCC", "MACA", "N"]
    export_txt_file("poc.txt",header,rows)
    