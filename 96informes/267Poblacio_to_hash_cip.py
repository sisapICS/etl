
import sisapUtils as u 

cip_2_hash = {}
hash_2_cip = {}
sql = """SELECT usua_cip, usua_cip_cod FROM pdptb101"""
for cip, hash in u.getAll(sql, 'pdp'):
    cip_2_hash[cip] = hash
    hash_2_cip[hash] = cip

with open('dones_muntanya_sense_hash.txt') as f:
    contents = f.readlines()

upload = []
contents_to_hash = []
for cip in contents:
    try:
        cip = cip[1:14]
        hash = cip_2_hash[cip]
        upload.append((cip, hash))
    except:
        print(cip)

with open("dones_muntanya_amb_hash.txt", 'w') as output:
    for row in upload:
        output.write(str(row) + '\n')
    
with open('dones_muntanya_sense_CIP.txt') as f:
    contents = f.readlines()

upload = []
contents_to_cip = []
for hash in contents:
    hash = hash[1:41]
    cip = hash_2_cip[hash]
    upload.append((hash, cip))

with open("dones_muntanya_amb_CIP.txt", 'w') as output:
    for row in upload:
        output.write(str(row) + '\n')
