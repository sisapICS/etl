# coding: iso-8859-1
#Departament: ansietat, Ermengol, Octubre 2016
from sisapUtils import *
from collections import defaultdict, Counter
import csv
import os
import sys
from time import strftime


nod = 'nodrizas'
imp = 'import'

codisCim10 =  "('F41','F41.1','F41.3','F41.0','F41.8','F41.9','R45.0')"

sql = 'select year(data_ext) from dextraccio'
dext,=getOne(sql, nod)

centres = {}
sql ='select scs_codi,ics_codi,ics_desc, amb_desc from cat_centres'
for up, br, desc, ambit in getAll(sql, nod):
    centres[up]={'br':br, 'desc':desc, 'ambit': ambit}
    
pob = {}
sql = 'select id_cip_sec, up from assignada_tot'
for id, up in getAll(sql, nod):
    pob[id] = up

upload = []
anys = list(range(2009, 2016))
for danys in anys:
    recompte = {}
    if danys > dext:
        continue
    dataext = str(danys) +'-12-31'
    tracta = {}
    sql ="select id_cip_sec from import.tractaments where (pf_cod_atc like ('N05B%') or pf_cod_atc like ('N05C%') or pf_cod_atc like ('N06A%')) and ((ppfmc_pmc_data_ini<='{0}' and ppfmc_data_fi>'{0}') or (ppfmc_pmc_data_ini between date_add('{0}',interval - 12 month) and '{0}'))".format(dataext)
    for id, in getAll(sql, nod):
        tracta[id] = True
    sql = "select distinct id_cip_sec from problemes where pr_cod_ps in {0} \
        and pr_cod_o_ps='C' and pr_hist=1 and pr_dde<='{1}' and (pr_data_baixa is null or pr_data_baixa>'{1}') and (pr_dba is null or pr_dba>'{1}')".format(codisCim10, dataext)
    for id,   in getAll(sql, imp):
        if id in pob:
            den = 1
            num = 0
            if id in tracta:
                num = 1
            up = pob[id]
            br = centres[up]['br']
            desc = centres[up]['desc']
            ambit = centres[up]['ambit']
            if (up, br, desc, ambit, danys) in recompte:
                recompte[(up, br, desc, ambit, danys)]['den']+= den
                recompte[(up, br, desc, ambit, danys)]['num']+= num
            else:
                recompte[(up, br, desc, ambit, danys)] = {'den': den, 'num': num}
    for (up, br, desc, ambit, danys), valors in recompte.items():
        upload.append([up,br,desc,ambit,danys,valors['den'],valors['num']])
file = tempFolder + 'peticio_ansietat.txt'
writeCSV(file, upload, sep=';') 
        
            
        
        
        

        