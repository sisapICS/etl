import sisapUtils as u
import collections as c


ups = set()
sql = """select scs_codi, ics_codi from nodrizas.cat_centres cc  """
for up, x in u.getAll(sql, 'nodrizas'):
    ups.add(up)


cols = """(year int, servei varchar(10), n int)"""
u.createTable('cancelacions_saip', cols, 'altres', rm=True)


sql = """SELECT EXTRACT(YEAR FROM to_date(visi_data_visita, 'j')),
        CASE WHEN VISI_SERVEI_CODI_SERVEI LIKE '%MF%' OR VISI_SERVEI_CODI_SERVEI LIKE '%MG%' THEN 'MG'
        WHEN VISI_SERVEI_CODI_SERVEI LIKE '%INF%' OR VISI_SERVEI_CODI_SERVEI LIKE '%INFPED%' THEN 'INF'
        WHEN VISI_SERVEI_CODI_SERVEI LIKE '%PED%' THEN 'PED'
        ELSE 'ALTRES' END SERVEI,
        VISI_UP
        FROM vistb043 
        WHERE VISI_MOTIU_VISITA = '1'
        AND to_date(visi_data_visita, 'j') BETWEEN DATE '2023-01-01' AND DATE '2024-07-01'"""

counting = c.Counter()
for sector in u.sectors:
    print(sector)
    for year, servei, up in u.getAll(sql, sector):
        if up in ups:
            counting[(year, servei)] += 1

upload = [(year, servei, n) for (year, servei), n in counting.items()]
u.listToTable(upload, 'cancelacions_saip', 'altres')
