# coding: iso-8859-1

# Modificació del codi 96lactancia per ampliar lactants fins a 2018
# - 20201014 Actualització fins 20191231
# - 20210812 Actualització fins 20201231
# - 20220824 Actualització 2021

import sisapUtils as u
from collections import Counter
import datetime as d
from os import path

current_year, = u.getOne("select YEAR(data_ext) from dextraccio", "nodrizas")

# PARAMETROS QUE PIDEN ACTUALIZAR, SON LAS COHORTES DE AÑOS DE NACIMIENTO
YEAR_NAIX_INI = current_year - 1
YEAR_NAIX_FI = current_year - 1

# Llista de codis de lactància (variables agrupador 285 de lactància)
codis_nen11 = set('PEPAL01')
codis_nen12 = set(['PENDE051', 'PENDE052'])

sql = """
    SELECT
        scs_codi, ics_desc, sap_desc, amb_desc
    from
        cat_centres
    where
        ep = '0208'
    """
centros = {}
for up, ics_desc, sap_desc, amb_desc in u.getAll(sql, 'nodrizas'):
    centros[up] = {'ics': ics_desc, 'sap': sap_desc, 'ambit': amb_desc}

sql = """
    SELECT
        id_cip_sec,
        val_val,
        val_var
    FROM
        import.nen11
    """
valormax = {}
i = 0
for id, valor, var in u.getAll(sql, 'nodrizas'):
    if var in codis_nen11:
        i += 1
        if(i % 100000 == 0):
            print(i)
        valor = int(float(valor.replace(',', '.')))
        if valor > 24:
            valor = 24
        if id in valormax:
            valor2 = valormax[id]
            if valor > valor2:
                valormax[id] = valor
        else:
            valormax[id] = valor

i = 0
sql = """
    SELECT
        id_cip_sec,
        val_val,
        val_var
    FROM
        import.nen12
    """
for id, valor, var in u.getAll(sql, 'nodrizas'):
    if var in codis_nen12:
        i += 1
        if(i % 100000 == 0):
            print(i)
        valor = int(float(valor.replace(',', '.')))
        if valor > 24:
            valor = 24
        if id in valormax:
            valor2 = valormax[id]
            if valor > valor2:
                valormax[id] = valor
        else:
            valormax[id] = valor

dnaix_ini = d.date(YEAR_NAIX_INI, 1, 1)
dnaix_fin = d.date(YEAR_NAIX_FI, 12, 31)

mesosLM = Counter()
sql = """
    SELECT
        id_cip_sec,
        year(data_naix) as any_naix,
        up
    FROM
        nodrizas.ped_assignada
    WHERE
        data_naix BETWEEN DATE '{}' and DATE '{}'
    """.format(dnaix_ini, dnaix_fin)
for id, anys, up in u.getAll(sql, 'nodrizas'):
    if up in centros:
        try:
            valor = valormax[id]
        except KeyError:
            valor = None
        mesosLM[(anys, valor)] += 1

upload = []
for (anys, valor), n in mesosLM.items():
    upload.append([anys, valor, n])

filename = path.join(u.tempFolder, 'lm_mes_{}.csv'.format(dnaix_fin))
u.writeCSV(filename, [('any', 'lm_mes', 'n')] + sorted(upload), sep=';')
