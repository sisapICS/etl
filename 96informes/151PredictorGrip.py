# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
import datetime

debug = False

nod = 'nodrizas'
imp = 'import'

party = 0
for partition in getTablePartitions('visites', imp): 
    party += 1


file = 'se.csv'
setmanes_epi = {}
for (data1,anys,mes,dia_natural,dia_setmana,setmana_natural,primer_dissabte,setmana_epi,any_epi1,any_epi) in readCSV(file, sep=';'):
    setmanes_epi[str(data1)] = {'anys': any_epi1, 'set': setmana_epi}

l_cuap = {
            '01482':'RS78',
            '07111':'RS78',
            '01480':'RS78',
            '01345':'RS78',
            '01342': 'RS78',
            '04880': 'RS78',
            '01483': 'RS78',
            '01341': 'RS78',
            '01479':'RS78',
            '07208':'RS78',
            '07207': 'RS78',
            '07365':'RS78',
            '07367':'RS78',
            '03303': 'RS67',
            '04846': 'RS78',
            '04913':'RS78',
            '04912':'RS78',
            '04911':'RS78',
            '04910':'RS78',
            '01481':'RS78',
            '04842':'RS78',
         }   
         
def get_servei(servei):
    if servei == 'MG':
        serdesc = "MG"
    elif servei == 'PED':
        serdesc = "PED"
    elif servei in ('INFP', 'INFPD'):
        serdesc = "INFP"
    elif servei in ('INF','INFMG','ENF','INFG','ATS','GCAS'):
        serdesc = "INF"
    else:
        serdesc = "NO"
    return serdesc

rsdicc = {}
sql = 'select distinct rs_cod, rs_des from cat_sisap_agas'
for cod, desc in getAll(sql, imp):
    cod = 'RS' + str(cod)
    rsdicc[cod] = desc
    
centres, saps, cuaps = {}, {}, {}
sql = 'select scs_codi, rs, sap_desc from cat_centres'
for up, rscod, sap in getAll(sql, nod):
    rsdesc = rsdicc[rscod]
    centres[up] = rsdesc
    saps[sap] =  rscod
    
sql = "select up_codi_up_scs,a.up_codi_up_ics,up_desc_up_ics, dap_desc_dap  from cat_gcctb007 a inner join cat_gcctb008 b on a.up_codi_up_ics=b.up_codi_up_ics inner join cat_gcctb006 c on a.dap_codi_dap = c.dap_codi_dap \
    where (a.up_desc_up_ics like 'ACUT%' or a.up_desc_up_ics like 'CUAP%' or a.up_desc_up_ics like 'PAC %' or a.up_desc_up_ics like 'CAC %' or a.up_desc_up_ics like 'DISPOSITIU%') \
    and a.up_data_baixa = 0 and b.up_data_baixa = 0"
for up, be, desc, sap in getAll(sql, imp):   
    if sap in saps:
        rsc = saps[sap]
        rs = rsdicc[rsc]
    else:
        rsc = l_cuap[up]
        rs = rsdicc[rsc]
    cuaps[up] = rs

vCuap,vMF, vINF, vPED, vINFP, = Counter(), Counter(),Counter(),Counter(),Counter()
 
print party
for partition in getTablePartitions('visites', imp): 
    party = party - 1
    print party
    sql = "select visi_data_visita, visi_lloc_visita, visi_up, visi_servei_codi_servei from {0} where visi_situacio_visita='R' and visi_data_baixa=0 {1}".format(partition, ' limit 10' if debug else '')
    for datav, lloc, up, servei in getAll(sql, imp):
        if str(datav) in setmanes_epi:
            se = setmanes_epi[str(datav)]['set']
            anyse =setmanes_epi[str(datav)]['anys']
            if up in cuaps:
                rs = cuaps[up]
                vCuap[(anyse, se, rs, lloc)] += 1
            elif up in centres:
                rs = centres[up]
                try:
                    serv = get_servei(servei)
                except KeyError:
                    continue
                if serv =='MG':
                    vMF[(anyse, se, rs, lloc)] += 1
                elif servei == 'INF':
                    vINF[(anyse, se, rs, lloc)] += 1
                elif servei == 'PED':
                    vPED[(anyse, se, rs, lloc)] += 1    
                elif servei == 'INFP':
                    vINFP[(anyse, se, rs, lloc)] += 1    

upload = []  
for (anyse, se, rs, lloc), n in vMF.items():
    cuapC, cuapD,mfC, mfD, infC, infD, pedC, pedD, infpC, infpD = 0,0,0,0,0,0,0,0,0,0
    if lloc == 'C':
        mfC = n
        mfD = vMF[(anyse, se, rs, 'D')]
        cuapC = vCuap[(anyse, se, rs, 'C')]
        cuapD = vCuap[(anyse, se, rs, 'D')]
        infC = vINF[(anyse, se, rs, 'C')]
        infD = vINF[(anyse, se, rs, 'D')]
        pedC = vPED[(anyse, se, rs, 'C')]
        pedD = vPED[(anyse, se, rs, 'D')]
        infpC = vINFP[(anyse, se, rs, 'C')]
        infpD = vINFP[(anyse, se, rs, 'D')]
        upload.append([anyse, se,rs,cuapC, cuapD,mfC,mfD,infC,infD,pedC,pedD,infpC,infpD])

            
file = tempFolder + 'Visites_per_SE.txt'
writeCSV(file, upload, sep=';')
