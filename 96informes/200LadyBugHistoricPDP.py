# coding: iso-8859-1

from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

printTime("Inici")

taules = [('eqaindicadors', 'M', 'EQA adults'),('pedindicadors', 'M', 'EQA pediatria'),('odnindicadors', 'O', 'EQA Odontologia'),('socindicadors', 'T', 'EQA Treball social'),('assindicadors', 'G', 'EQA assir')]

catalegs = ['eqacataleg', 'pedcataleg', 'odncataleg', 'soccataleg', 'asscataleg']

anyC, = getOne('select year(data_ext) from dextraccio', 'nodrizas')
anyC = 2018

class Control_EQA(object):
    """."""

    def __init__(self):
        """."""
        self.get_territoris()
        self.get_indicadors()
        self.get_dades()
        self.export_data()

    def get_territoris(self):
        """Obtenim àmbits i sector"""
        
        self.centres = {}
        sql = 'select scs_codi, amb_desc, sector from cat_centres'
        for up, amb, sector in getAll(sql, 'nodrizas'):
            self.centres[up] = {'sector': sector, 'ambit': amb}
        sql = "select amb_desc, up from ass_centres"
        for amb, up in getAll(sql, 'nodrizas'):
            if up not in self.centres:
                self.centres[up] = {'sector': None, 'ambit': amb}
            
    def get_indicadors(self):
        """agafem indicadors del catàleg"""
        
        self.cataleg = {}
        for cat in catalegs:
            sql = "select indicador, literal from {} where pantalla = 'GENERAL' and dataany={}".format(cat, anyC)
            for ind, literal in getAll(sql, 'pdp'):
                self.cataleg[ind] = literal

    def get_dades(self):
        """Obtenim resultats dels indicadors per territori"""
        
        self.resultats = Counter()
        for (taula, tip, eqa) in taules:
            if tip in ('O', 'T'):
                sql = "select dataany, datames, up, indicador, detectats, resolts from {} where tipus = '{}' and uab = '0' and datames <>0".format(taula, tip)
            else:    
                sql = "select dataany, datames, up, indicador, detectats, resolts from {} where tipus = '{}' and datames <>0".format(taula, tip) 
            for dataany, mes1, up, ind, det, res in getAll(sql, 'pdp'):
                mes = '0' + str(mes1) if 1 <= mes1 <= 9 else str(mes1)
                periode = str(dataany) + str(mes)
                try:
                    ambit = self.centres[up]['ambit']
                    sector = self.centres[up]['sector']
                except KeyError:
                    continue
                try:
                    desc_ind =self.cataleg[ind]
                except KeyError:
                    continue
                self.resultats[periode, sector, ambit, ind, desc_ind, eqa, 'detectats'] += det
                self.resultats[periode, sector, ambit, ind, desc_ind, eqa, 'resolts'] += res

    def export_data(self):
        """."""
        upload = []
        for (periode, sector, ambit, ind, desc_ind, eqa, tipus), n in self.resultats.items():
            if tipus == 'detectats':
                resolts = self.resultats[periode, sector, ambit, ind, desc_ind, eqa, 'resolts']
                upload.append([periode, sector, ambit, ind, desc_ind, eqa, resolts, n])
        
        table = 'mst_control_EQA'
        columns = "(periode int, sector varchar(5), ambit varchar(150), indicador varchar(10), literal varchar(150), pare varchar(150), resolts int, detectats int)"
        createTable(table, columns, 'permanent', rm=True)
        listToTable(upload, table, 'permanent')
      
if __name__ == "__main__":
    Control_EQA()  

printTime("Fi")