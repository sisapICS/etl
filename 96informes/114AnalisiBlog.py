# coding: iso-8859-1


import tweepy
from tweepy import OAuthHandler
import collections 
from datetime import *
import time
from sisapUtils import *
from wordpress_xmlrpc import Client, WordPressPost
from wordpress_xmlrpc.methods.posts import GetPosts, NewPost, GetPost
from wordpress_xmlrpc.methods.users import GetUserInfo
from wordpress_xmlrpc.methods.comments import GetComments, GetCommentCount, GetComment, GetCommentStatusList
import csv
import requests
from collections import defaultdict, Counter

#blog


wp = Client('https://si9sapics.wordpress.com/xmlrpc.php', 'sisap@gencat.cat', 'S19s4pics2')
posts = wp.call(GetPosts())


entrades_permes = Counter()
offset = 0
increment = 10
while True:
    filter = { 'offset' : offset }
    posts = wp.call(GetPosts(filter))
    if len(posts) == 0:
        break # no more posts returned
    for post in posts:
        data = post.date
        mes = data.month
        anys = data.year
        entrades_permes[(anys, mes)] += 1
    offset = offset + increment

upload = []
for (anys, mes), d in entrades_permes.items():
    upload.append([anys, mes, d])

file = tempFolder + 'Entrades_si9sap_per_mes.txt'
writeCSV(file, upload, sep=';')      
 
csv_url = 'https://stats.wordpress.com/csv.php?api_key=3a483a4cfbcf&blog_uri=https://si9sapics.wordpress.com/&days=20000'
 
visites_permes = Counter()
with requests.Session() as s:
    download = s.get(csv_url)
    
    decoded_content = download.content.decode('utf-8')

    cr = csv.reader(decoded_content.splitlines(), delimiter=',')
    my_list = list(cr)
    for data, visites in my_list:
        try:
            mes = data[5:-3]
            anys = data[:-6]
            visites_permes[(anys,mes)] += int(visites)
        except:
            pass
upload = []
for (anys, mes), d in visites_permes.items():
    upload.append([anys, mes, d])

file = tempFolder + 'Visites_si9sap_per_mes.txt'
writeCSV(file, upload, sep=';')      

comentaris_permes = Counter()
offset = 0
increment = 10
while True:
    filter = { 'offset' : offset }
    comments = wp.call(GetComments(filter))
    if len(comments) == 0:
        break
    for comment in comments:
        data = comment.date_created
        mes = data.month
        anys = data.year
        comentaris_permes[(anys,mes)] += 1
    offset = offset + increment

upload = []
for (anys, mes), d in comentaris_permes.items():
    upload.append([anys, mes, d])

file = tempFolder + 'Comentaris_si9sap_per_mes.txt'
writeCSV(file, upload, sep=';')    
