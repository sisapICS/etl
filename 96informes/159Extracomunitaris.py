# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter


descnac = {}

sql = 'select resi_codi, resi_descripcio from cat_rittb034'
for cod, desc in getAll(sql, 'import'):
    descnac[cod] = desc

recomptes = Counter()
for sector in sectors:
    nacio = {}
    sql = 'select usua_cip,usua_nacionalitat from usutb040'
    for id, nac in getAll(sql, sector):
        if nac in descnac:
            desc =descnac[cod]
        else:
            desc = None
        nacio[id] = desc
    sql = "select visi_usuari_cip from vistb043 where visi_data_baixa = 1 and to_char(to_date(visi_data_visita,'J'),'YYYY')='2017' and visi_situacio_visita='R'"
    for cip, in getAll(sql, sector):
        try:
            if cip[-1:].isdigit():
                continue
            else:
                if cip in nacio:
                    desc = nacio[cip]
                    recomptes[desc] += 1
        except TypeError:
            continue
            
upload = []
for (desc), d in recomptes.items():
    upload.append([desc, d])


file = tempFolder + 'Visites_Extracomunitaris.txt'
writeCSV(file, upload, sep='|')
    
