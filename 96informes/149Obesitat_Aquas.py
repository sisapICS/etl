# -*- coding: utf8 -*-

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *

nod = "nodrizas"
imp = "import"

tallsPED = {6 : 
                {'H': {'SP': 16.8, 'O': 18.5},
                 'D': {'SP': 17, 'O': 19.2},
                },
            7 : 
                {'H': {'SP': 17, 'O': 19},
                 'D': {'SP': 17.3, 'O': 19.8},
                },
            8 : 
                {'H': {'SP': 17.4, 'O': 19.7},
                 'D': {'SP': 17.7, 'O': 20.6},
                },
            9 : 
                {'H': {'SP': 17.9, 'O': 20.5},
                 'D': {'SP': 18.3, 'O': 21.5},
                },
            10 : 
                {'H': {'SP': 18.5, 'O': 21.4},
                 'D': {'SP': 19, 'O': 22.6},
                },    
            11 : 
                {'H': {'SP': 19.2, 'O': 22.5},
                 'D': {'SP': 19.9, 'O': 23.7},
                }, 
            12 : 
                {'H': {'SP': 19.9, 'O': 23.6},
                 'D': {'SP': 20.8, 'O': 25},
                },  
        }



dext, = getOne('select data_ext from dextraccio', nod)

centres = {}

sql = "select scs_codi, ics_codi, ics_desc from cat_centres"
for up, br, desc in getAll(sql, nod):
    centres[up] = {'br': br, 'desc': desc}
    
obesitat_ps = {}
sql = 'select id_cip_sec from eqa_problemes where ps=239'
for id, in getAll(sql, nod):
    obesitat_ps[id] = True
    
sql = 'select id_cip_sec from ped_problemes where ps=239'
for id, in getAll(sql, nod):
    obesitat_ps[id] = True    
    
sql = 'select id_cip_sec, valor from eqa_variables where agrupador=19 and usar=1'
for id, valor in getAll(sql, nod):
    if valor > 30:
        obesitat_ps[id] = True

'''
#comenten no fer-ho per percentils     
sql = "select id_cip_sec from ped_variables where agrupador=756 and val in ('P97', '>P97')"
for id, in getAll(sql, nod):
    obesitat_ps[id] = True   
'''

imc_nens = {}
sql = "select id_cip_sec, val, dat, edat_a from ped_variables where agrupador=19"
for id,val, dat, edat_a in getAll(sql, nod):
    if (id) in imc_nens:
        dant = imc_nens[(id)]['dat']
        if dant < dat:
            imc_nens[(id)]['dat'] = dat
            imc_nens[(id)]['val'] = val
            imc_nens[(id)]['ed'] = edat_a
    else:
        imc_nens[(id)] = {'val':val, 'dat':dat, 'ed': edat_a}

sobrepes = {} 
sql = 'select id_cip_sec, valor from eqa_variables where agrupador=19 and usar=1'
for id, valor in getAll(sql, nod):
    if 25 < valor <=30:
        sobrepes[id] = True
        
sql = 'select id_cip_sec from eqa_problemes where ps=267'
for id, in getAll(sql, nod):
    sobrepes[id] = True
        
recomptes = Counter()
den = Counter()
sql = 'select id_cip_sec, ates, sexe, edat, up from assignada_tot'
for id, ates, sexe, edat, up in getAll(sql, nod):
    if up in centres:
        desc = centres[up]['desc']
        br = centres[up]['br']
        den[(up, br, desc, ates, edat, sexe)] += 1
        if 18 <= int(edat) <= 74:
            edat_txt = 'Entre 18 i 74 anys'
            obes, sobpes=0, 0
            if id in obesitat_ps :
                obes = 1
            else:
                if id in sobrepes:
                    sobpes = 1
            recomptes[(up, br, desc, ates, edat, sexe, obes, sobpes)] += 1
        elif 6 <= int(edat) <= 12:
            edat_txt = 'Entre 6 i 12 anys'
            obes,sobpes = 0, 0
            if id in obesitat_ps :
                obes = 1
            if id in imc_nens:
                val = imc_nens[(id)]['val']
                edat_a = imc_nens[(id)]['ed']
                dati = imc_nens[(id)]['dat']
                try:
                    tall = tallsPED[edat_a][sexe]['O']
                    tallsp = tallsPED[edat_a][sexe]['SP']
                    if float(val) > float(tall):
                        obes = 1
                    else:
                        if float(val) > float(tallsp):
                            sobpes = 1
                except KeyError:
                    pass
            recomptes[(up, br, desc, ates, edat, sexe, obes, sobpes)] += 1
        else:
            continue
    
        
        
upload = []
for (up, br,desc, ates, edat, sexe, obes, sobrepes), d in recomptes.items():
    upload.append([up, br, desc, ates, edat, sexe, obes, sobrepes, d])

file = tempFolder + 'exces_de_pes.txt'
writeCSV(file, upload, sep=';')

den_upload = []
for (up, br,desc, ates, edat, sexe), n in den.items():
    den_upload.append([up, br, desc, ates, edat, sexe, n])

file = tempFolder + 'exces_de_pes_denominadors.txt'
writeCSV(file, den_upload, sep=';')
