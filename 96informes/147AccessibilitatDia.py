# -*- coding: utf8 -*-

#Peti per a Rosa Morral

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *



imp = 'import'
nod = 'nodrizas'

centres = {}
sql = "select scs_codi, amb_desc from cat_centres where ep='0208'"
for up, ambit in getAll(sql, nod):
    centres[up] = ambit

accessibilitat = Counter()
sql = "select * from sisap_accessibilitat where  peticions is not null and to_char(dia, 'YYYYMMDD') >='20171218'"
for row in getAll(sql, 'redics'):  
    up, servei, peticions =  row[1], row[3], row[5]
    forats48 = row[6] + row[7] + row[8]
    forats5d = row[6] + row[7] + row[8] + row[9] + row[10] + row[11]
    forats10d = row[6] + row[7] + row[8] + row[9] + row[10] + row[11] + row[12] + row[13] + row[14] + row[15] + row[16]
    mes = row[4].strftime('%m')
    anys = row[4].year
    dia = row[4]
    periode = str(anys) + str(mes)
    if forats48 > peticions:
        forats48 = peticions
    if forats5d > peticions:
        forats5d = peticions
    if forats10d > peticions:
        forats10d = peticions
    try:
        ambit = centres[up]
    except KeyError:
        continue
    accessibilitat[(ambit,servei, dia, 'peti')] += peticions
    accessibilitat[(ambit,servei, dia, 'forats48')] += forats48
    accessibilitat[(ambit,servei, dia, 'forats5d')] += forats5d
    accessibilitat[(ambit,servei, dia, 'forats10d')] += forats10d
    
upload = []
for (ambit,servei, dia, tipus),r in accessibilitat.items():
    if tipus == 'peti':
        forats48 = accessibilitat[(ambit,servei, dia, 'forats48')]
        forats5d = accessibilitat[(ambit,servei, dia, 'forats5d')]
        forats10d = accessibilitat[(ambit,servei, dia, 'forats10d')]
        upload.append([ambit,servei, dia, forats48, forats5d, forats10d, r])

file = tempFolder + 'Accessibilitat.txt'
writeCSV(file, upload, sep=';')  
