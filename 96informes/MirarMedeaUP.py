# coding: iso-8859-1

import sisapUtils as u
# import csv
import os
# import sys
# from time import strftime
from collections import Counter

codi_up = raw_input('Codi up (scs_codi): ')

FOLDER = os.path.dirname(os.path.abspath(__file__))
FILE = os.path.join(FOLDER, 'municipis.txt')


def get_medea(medea):
    if medea < 0.1:
        m = 1
    elif 0.1 <= medea <= 0.46:
        m = 2
    elif 0.46 < medea <= 0.87:
        m = 3
    else:
        m = 4
    return m


class MirarMedea(object):
    """."""

    def __init__(self):
        """."""
        self.get_abs()
        self.get_ruralitat()
        self.get_calculs()

    def get_abs(self):
        """Mirem quins abs té un EAP. Ho mirem en el catàleg del sector 6734"""

        self.codis_abs = []
        sql = """
            SELECT
                abs_codi_abs,
                abs_sector_siap
            FROM
                rittb001
            WHERE
                abs_codi_up = '{}'
        """.format(codi_up)
        for abs, self.sector_up in u.getAll(sql, '6734'):
            self.codis_abs.append(abs)

    def get_ruralitat(self):
        """
        Ruralitat a partir de la dels municipis als que dona servei un EAP.

        És urbà si:
            - Població de tots els municipis > 10.000
            - I densitat de població de tots els municipis > 150 hab./km2.

        Ens quedem també les localitats que formen un municipi
            pels càlculs posteriors
        """

        self.ets_rural = {}
        self.localitats = {}
        self.localitats_abs = {}
        self.calcula1 = False
        for (municipi,
             desc_municipi,
             abs,
             localitat,
             desc_localitat,
             pob,
             km,
             densitat) in u.readCSV(
                 FILE,
                 sep='@'):
            self.localitats[(municipi, localitat)] = True
            if int(abs) in self.codis_abs or abs in self.codis_abs:
                self.calcula1 = True
                self.localitats_abs[(municipi, localitat)] = True
                if municipi in self.ets_rural:
                    pass
                else:
                    clas = 0
                    if int(pob) > 10000 and float(densitat) > 150:
                        clas = 1
                    self.ets_rural[(municipi)] = {'clas': clas,
                                                  'pob': pob,
                                                  'km': km
                                                  }

    def get_calculs(self):
        """
        Acabem de calcular:
        - Si tots els municipis d'un eap són URBANS, eap és URBÀ
        - Si tots els municipis d'un eap són RURALS, eap és RURAL
        - Si n_municipis URBANS i RURALS, l'eap és URBÀ si:
            - la població de les localitats* de l'eap és > 10k*0,85*n_municipis
              (*part proporcional del municipi que representen les localitats)
            - i la densitat de població de les localitats de l'EAP és >0,85*150

        Subclasifiquem:
        - URBANS: els classifiquem segons medea
        - RURALS: en funció de
            la població del centre més gran de l'EAP
            i la densitat de població
        """
        if self.calcula1:
            urba = 0
            n_municipis = 0
            for (municipi), n in self.ets_rural.items():
                n_municipis += 1
                urba += n['clas']

            n_loc, n_loc_abs, pobs, kms, n_mun = Counter(), Counter(), 0, 0, 0
            for (municipi, localitat), n in self.localitats.items():
                n_loc[municipi] += 1
            for (municipi, localitat), n in self.localitats_abs.items():
                n_loc_abs[municipi] += 1
            for municipi, locs in n_loc_abs.items():
                n_mun += 1
                pob = self.ets_rural[municipi]['pob']
                km = self.ets_rural[municipi]['km']
                n_locs = n_loc[municipi]
                pob_ = (float(pob)/float(n_locs))*float(locs)
                pobs += pob_
                kms += float(km)

            classificacio_eap = 0
            if urba == n_municipis:
                classificacio_eap = 'U'
            elif urba == 0:
                classificacio_eap = 'R'
            else:
                if pobs > (n_mun * 10000 * 0.85) and pobs/kms > (0.85 * 150):
                    classificacio_eap = 'U'
                else:
                    classificacio_eap = 'R'

            if classificacio_eap == 'U':
                sql = "select valor from dwsisap.medea_eap where up = '{}'"
                valor = u.getOne(sql.format(codi_up), 'exadata')
                if valor:
                    medea = valor[0]
                    m = get_medea(medea)
                    resultat = str(m) + classificacio_eap
                    print 'classificacio eap: {}\nvalor:{}'.format(resultat, medea)
                else:
                    print "UP es urbana pero no la tenim a medea_eap"
            elif classificacio_eap == 'R':
                taula = "s" + self.sector_up
                pob_centre = 0
                sql = """
                    SELECT
                        usua_uab_servei_centre,
                        count(*)
                    FROM
                        usutb040 partition({0})
                    WHERE
                        usua_uab_up in ('{1}')
                        AND usua_situacio = 'A'
                    GROUP BY
                        usua_uab_servei_centre
                """.format(taula, codi_up)
                for centre, pobl in u.getAll(sql, ("sidics", "x0002")):
                    if pobl > pob_centre:
                        pob_centre = pobl
                tip_rur = '1'
                if pob_centre >= 7500 and pobs/kms >= 100:
                    tip_rur = '2'
                elif pob_centre < 7500 and pobs/kms < 100:
                    tip_rur = '0'
                resultat = tip_rur + classificacio_eap
                print 'classificacio eap: ', resultat
        else:
            sql = "select valor from dwsisap.medea_eap where up = '{}'"
            valor = u.getOne(sql.format(codi_up), 'exadata')
            if valor:
                medea = valor[0]
                m = get_medea(medea)
                resultat = str(m) + "U"
                print "ABS no el tenim lligat a cap municipi, per� si fos urb� seria {}\nvalor {}".format(resultat, medea)
            else:
                print "ABS no el tenim lligat a cap municipi ni apareix a medea_eap"


if __name__ == "__main__":
    u.printTime('Inici')
    MirarMedea()
    u.printTime('Fi')
