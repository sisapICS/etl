
import sisapUtils as u
import pandas as pd
import collections as c

print('llegeixo 1')
cips_vacunes = pd.read_csv("vacunes_sms_pedia.csv", header = 0, encoding='latin1')

print(cips_vacunes.head(n=5))

telf_cips = c.defaultdict(dict)
telf_idiomes = {}
cips_vacunes = cips_vacunes.reset_index()
for index, row in cips_vacunes.iterrows():
    telf = str(row['telf1']) if str(row['telf1'])[0] in ('6','7') else ''
    if telf == '':
        telf = str(row['telf2']) if str(row['telf2'])[0] in ('6','7') else ''
    if telf != '':
        nom = row['nom'].encode('utf-8')
        telf_cips[telf][nom] = row['cip']
        telf_idiomes[telf] = row['idioma'][0:5]

print('llegeixo 2')
exlucions_vari = pd.read_excel("vacunacio_pedia_disculpes.xls", names = ['telf', 'missatge'])
exlucions_vari = exlucions_vari.reset_index()
excloure = set()
for index, row in exlucions_vari.iterrows():
    telf = str(row['telf'])
    if str(telf) in telf_cips:
        if telf_idiomes[telf] == 'CATAL':
            m = row['missatge']
            for nom in telf_cips[telf]:
                long_nom = 70 + len(nom)
                print(nom, m[70:long_nom])
                if nom == m[70:long_nom].encode('latin1'):
                    excloure.add((telf, nom))
        else:
            m = row['missatge']
            for nom in telf_cips[telf]:
                long_nom = 67 + len(nom)
                if nom == m[67:long_nom]:
                    excloure.add((telf, nom))

print('els q hem d eliminar', len(excloure))


# info cips de 040
print('info usutb040')
info_cips = {}
for s in u.sectors:
    print(s)
    sql = """SELECT usua_cip, USUA_DATA_NAIXEMENT, usua_uab_up, usua_sexe  
                FROM usutb040
                WHERE usua_situacio = 'A' """
    for cip, data_n, up, sexe in u.getAll(sql, s):
        info_cips[cip] = [data_n, up, sexe]

print('cuinetes')
exclos = 0
upload = []
for telf in telf_cips:
    for nom in telf_cips[telf]:
        if (telf, nom) not in excloure:
            cip = telf_cips[telf][nom]
            if cip in info_cips:
                data_n, up, sexe = info_cips[cip]
                upload.append((cip, nom, data_n, up, sexe))
        else:
            exclos += 1

print('exclosos', exclos)

print('creacio taules')
cols = """(cip varchar2(14), nom varchar2(100), data_naix int, up varchar2(5), sexe varchar2(1))"""
u.createTable('estudi_sms', cols, 'exadata', rm=True)
u.listToTable(upload, 'estudi_sms', 'exadata')
u.grantSelect('estudi_sms', ('DWSISAP_ROL'), 'exadata')


# # no ho puc fer aixi perque hi ha telfs duplicats!!!!
# telfs_vari = exlucions_vari.set_index('telf').transpose().to_dict(orient='dict')
# print(type(telfs_vari))
# print(exlucions_vari.head(n=5))
# print(telfs_vari[606202794])