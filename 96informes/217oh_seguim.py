# coding: latin1

"""
Seguim des informe 211.
"""

import collections as c

import sisapUtils as u


PERIODE = (2013, 2018)
FILE = u.tempFolder + "variables_oh_per_periode.csv"


class OH(object):
    """."""

    def __init__(self):
        """."""
        self.get_jobs()
        self.get_data()
        self.get_result()
        self.get_poblacio()
        self.export_data()

    def get_jobs(self):
        """."""
        self.jobs = []
        params = [("variables",
                   "import",
                   "select id_cip_sec, year(vu_dat_act), vu_cod_vs \
                    from {}, nodrizas.dextraccio \
                    where vu_cod_vs in {}",
                   "variables",
                   84,
                   "select year(vu_dat_act) from {} limit 1"),
                  ("activitats",
                   "import",
                   "select id_cip_sec, year(au_dat_act), au_cod_ac \
                    from {}, nodrizas.dextraccio \
                    where au_cod_ac in {}",
                   "actuacions",
                   84,
                   "select year(au_dat_act) from {} limit 1")]
        for mare, db, master, taula, agr, check in params:
            q = "select criteri_codi from eqa_criteris \
                 where taula = '{}' and agrupador = {}".format(taula, agr)
            codis = tuple([cod for cod, in u.getAll(q, "nodrizas")])
            tables = u.getSubTables(mare)
            for table in tables:
                if PERIODE[0] <= u.getOne(check.format(table), db)[0] <= PERIODE[1]:  # noqa
                    sql = master.format(table, codis, *PERIODE)
                    self.jobs.append((sql, db))

    def get_data(self):
        """."""
        self.data = c.defaultdict(set)
        codis = {"AUDIT": "AUDIT", "VP2001": "AUDIT-C", "EP2004": "AUDIT-C",
                 "ALRIS": "CALC", "CP201": "GRAU"}
        for worker in u.multiprocess(self._worker, self.jobs, 12):
            for id, dat, cod in worker:
                agr = codis.get(cod, "ALTRES")
                self.data[(dat, agr)].add(id)

    def get_result(self):
        """."""
        self.result = [(dat, agr, len(pacs)) for (dat, agr), pacs
                       in self.data.items()]

    def get_poblacio(self):
        """."""
        sql = "select dataany, count(1), sum(ates) \
               from assignadahistorica \
               where dataany between {} and {} \
               group by dataany".format(*PERIODE)
        for dat, ass, at in self._worker((sql, "import")):
            self.result.append((int(dat), "pobass", ass))
            self.result.append((int(dat), "pobat", at))

    def export_data(self):
        """."""
        u.writeCSV(FILE, sorted(self.result), sep=";")

    def _worker(self, params):
        return [row for row in u.getAll(*params)]


if __name__ == "__main__":
    OH()
