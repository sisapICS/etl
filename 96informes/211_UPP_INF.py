# coding: utf8

"""
Visites en pacients amb UPP i nafres
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

DEBUG = False

db = 'test'
tb = 'mst_visites_inf_nafres'

nafres = "(91, 92, 833, 834)"

ORIG_TB = "ag_longitudinalitat_new"
ORIG_DB = "nodrizas"

class UPP_inf(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_assignada()
        self.get_visites()
        self.get_nafres()
        self.export_data()
        
    def get_centres(self):
        """EAP ICS"""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres where ep='0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_assignada(self):
        """pacients assignats a ICS"""
        self.pob = {}
        sql = "select id_cip_sec, up from assignada_tot"
        for id, up in u.getAll(sql, ORIG_DB):
            if up in self.centres:
                self.pob[id] = up 
    
    def get_visites(self):
        """."""
        self.visites = Counter()
        if DEBUG:
            visites = [self._get_visites_w("p0")]
        else:
            partitions = u.getTablePartitions(ORIG_TB, ORIG_DB)
            visites = u.multiprocess(self._get_visites_w, partitions)
        for worker in visites:
            for pac, dades in worker:
                self.visites[pac] += dades

    def _get_visites_w(self, particio):
        """."""
        visites = Counter()
        sql = "select pac_id, pac_up \
               from {} partition({}) \
               where modul_espe<>'EXTRA' and prof_categ='30999'".format(ORIG_TB, particio)
        for id, up in u.getAll(sql, ORIG_DB):
            visites[(id)] += 1
        return visites.items()
    
       
    def get_nafres(self):
        """Obtenim pacients amb nafres"""
        self.nafres = {}
        sql = "select id_cip_sec,date_format(pr_dde,'%Y%m%d'), date_format(pr_dba, '%Y%m%d')\
        from problemes, nodrizas.dextraccio \
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and pr_data_baixa = 0 and pr_dba=0 and pr_cod_ps in ('L89', 'I83.0')"
        for id, dde, dba in u.getAll(sql, 'import'):
            if id in self.pob:
                self.nafres[(id)] = True
        
        self.upload = []
        for (id), r in self.nafres.items():
            vis = self.visites[id] if id in self.visites else 0
            self.upload.append([id, vis])
      
    def export_data(self):
        """."""
        columns = ["id int", "visites int"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
   
if __name__ == '__main__':
    UPP_inf()
    