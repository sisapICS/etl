# coding: latin1

"""
Variables de viol�ncia de g�nere embarassades any anterior a novembre 2019
"""

import sisapUtils as u

import datetime as d

from datetime import timedelta

def get_data_extraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {}.dextraccio;".format('nodrizas')
    return u.getOne(sql,'nodrizas')[0]

class Violencia_Emb_19(object):
    """."""

    def __init__(self):
        """."""
        self.get_embarassos()
        self.get_variables()
        #self.taula_final()
        #self.export_taula()
        
    def get_embarassos(self):
        """ Obtenim embarassos del darrer any
        """
        self.embarassos = {}
        d_fi_periode = get_data_extraccio()
        str_fi_periode = d.datetime.strftime(d_fi_periode,'%Y%m%d')
        d_ini_periode = d_fi_periode + timedelta(days=-365)
        str_ini_periode = d.datetime.strftime(d_ini_periode,'%Y%m%d')
        sql = "select id_cip_sec, emb_d_ini, emb_d_fi from embaras where \
               emb_d_fi >= {} and emb_d_ini <= {} and emb_c_tanca in ('P','C','Pr') \
               and emb_durada > 0".format(str_ini_periode, str_fi_periode)
        for id, data_ini, data_fi in u.getAll(sql, "import"):
            self.embarassos[id] = (data_ini, data_fi)
        print(len(self.embarassos))
                
    def get_variables(self):
        """."""
        self.test_PVS = set()
        self.test_PVS_pos = set()
        self.reg_RVD = set()
        
        sql = "select id_cip_sec, vu_dat_act, vu_val from variables1 \
               where vu_cod_vs in ('EP3001')"
        for id, data, valor in u.getAll(sql, "import"):
            if id in self.embarassos:
                if data >= self.embarassos[id][0] and data <= self.embarassos[id][1]:
                    self.test_PVS.add(id)
                    if valor==1:
                        self.test_PVS_pos.add(id)
        print(len(self.test_PVS))
        print(len(self.test_PVS_pos))
        print("Percentatge de dones embarassades amb el test fet")
        print(len(self.test_PVS) / float(len(self.embarassos)) * 100)
        print("Percentatge de dones embarassades amb el test fet i resultat positiu")
        print(len(self.test_PVS_pos) / float(len(self.embarassos)) * 100)
        
        sql = "select id_cip_sec from variables1 \
               where vu_cod_vs in ('VP3001')"
        for id, in u.getAll(sql, "import"):
            if id in self.test_PVS_pos:
                self.reg_RVD.add(id)
        print(len(self.reg_RVD))
        print("Percentatge de dones amb el test fet que tenen registrat RVD")
        print(len(self.reg_RVD) / float(len(self.test_PVS_pos)) * 100)
        
if __name__ == "__main__":
    Violencia_Emb_19()
