from sisapUtils import getAll, multiprocess, sectors, createTable, \
                       listToTable, grantSelect
from collections import Counter


year = 2016
table = 'sisap_problemes_pediatria'
db = 'redics'


def age_conv(edat):
    if edat < 2:
        conv = '0 a 1'
    elif edat < 5:
        conv = '2 a 4'
    elif edat < 10:
        conv = '5 a 9'
    elif edat < 15:
        conv = '10 a 14'
    return conv


def get_poblacio():
    sql = "select scs_codi from cat_centres where ep = '0208'"
    centres = set([up for up, in getAll(sql, 'nodrizas')])
    poblacio = {sector: {} for sector in sectors}
    sql = 'select id_cip_sec, codi_sector, up, edat, sexe \
           from assignada_tot where edat < 15'
    for id, sector, up, edat, sexe in getAll(sql, 'nodrizas'):
        if up in centres:
            poblacio[sector][id] = (age_conv(edat), sexe)
    return poblacio


def get_problemes(args):
    sector, poblacio = args
    resultat = {}
    sql = 'select id_cip_sec, pr_cod_ps, pr_dde, pr_dba \
           from import.problemes_s{}'
    for id, ps, dde, dba in getAll(sql.format(sector), 'import'):
        if id in poblacio:
            key = (ps,) + poblacio[id]
            if key not in resultat:
                resultat[key] = {'p': set(), 'i': set()}
            if not dba:
                resultat[key]['p'].add(id)
            if dde.year == year:
                resultat[key]['i'].add(id)
    return [(ps, edat, sexe, len(n['p']), len(n['i']))
            for (ps, edat, sexe), n in resultat.items()]


if __name__ == '__main__':
    poblacio = get_poblacio()

    jobs = ((sector, poblacio[sector]) for sector in sectors)
    mega_resul = multiprocess(get_problemes, jobs, 8)
    resultat = {}
    for resul in mega_resul:
        for ps, edat, sexe, prevalents, incidents in resul:
            key = (ps, edat, sexe)
            if key not in resultat:
                resultat[key] = [0, 0]
            resultat[key][0] += prevalents
            resultat[key][1] += incidents

    sql = 'select ps_cod, ps_cat, ps_des from cat_prstb001'
    descripcio = {cod: (cat, des) for (cod, cat, des) in getAll(sql, 'import')}
    sql = "select codi_cim10, codi_ciap_m, desc_ciap_m \
           from cat_md_ct_cim10_ciap"
    ciap = {cim: (cod, des) for (cim, cod, des) in getAll(sql, 'import')}

    createTable(table, '(ps_cod varchar2(10), \
                         ps_cat varchar2(1), \
                         ps_des varchar2(200), \
                         ciap2_cod varchar2(10), \
                         ciap2_des varchar2(200), \
                         edat varchar2(10), \
                         sexe varchar2(1), \
                         prevalents number(6, 0), \
                         incidents number(6, 0))', db, rm=True)
    upload = [(ps,) +
              (descripcio[ps] if ps in descripcio else (None, None)) +
              (ciap[ps] if ps in ciap else (None, None)) +
              (edat, sexe, prevalents, incidents)
              for (ps, edat, sexe), (prevalents, incidents)
              in resultat.items()]
    listToTable(upload, table, db)
    grantSelect(table, 'PREDUPRP', db)
