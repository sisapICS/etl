# coding: utf8
from sisapUtils import *
from collections import defaultdict
import pandas as pd

TEST = False

# hash to id_cip_sec
print('id_cip a hash')
sql = """SELECT ID_CIP_SEC, HASH_D FROM IMPORT.U11"""
id_hash = {}
for id, hash in getAll(sql, 'import'):
    id_hash[hash] = id

# control i demencia
sql = """SELECT CASE WHEN V_HBA1C_DATA > sysdate - 365 AND V_HBA1C_VALOR <= 7.5 THEN 1 ELSE 0 END control,
        CASE WHEN PS_DEMENCIA_DATA IS NOT NULL THEN 1 ELSE 0 END demencia, a.c_cip
        FROM DBS a WHERE C_EDAT_ANYS < 65
        AND (PS_DIABETIS2_DATA IS NOT NULL )
        AND F_DIAB_INSULINA IS NOT NULL
        AND F_DIAB_INSULINA LIKE '%¬%' {}""".format('and rownum < 50' if TEST else '')

print(sql)


dic = defaultdict()
print('control i demencia')
for control, demencia, hash in getAll(sql.decode("utf8").encode("latin1"), 'redics'):
    try: 
        dic[id_hash[hash]] = {}
        dic[id_hash[hash]]['control'] = control
        dic[id_hash[hash]]['demencia'] = demencia
        dic[id_hash[hash]]['retino'] = 0
        dic[id_hash[hash]]['ceguesa'] = 0
        dic[id_hash[hash]]['tetraplegia'] = 0
    except:
        'passem'

print('comencem retino')
# retinopatia
sql = """SELECT id_cip_sec FROM nodrizas.eqa_variables
       where agrupador = 46 and valor in (4,5,6) and usar = 1"""
keys = dic.keys()
for id_cip, in getAll(sql, 'import'):
    if id_cip in keys:
        dic[id_cip]['retino'] = 1

print('comencem ceguesa')
# ceguesa
sql = """SELECT id_cip_sec, pr_cod_ps from import.problemes
        WHERE pr_cod_ps IN ('C01-H54.0', 'C01-H54.10', 'C01-H54.40', 'C01-G82.50', 'G82', 'G82.3', 'G82.4', 'G82.5')"""
for id_cip_sec, codi in getAll(sql, 'import'):
    if codi in ('C01-G82.50', 'G82', 'G82.3', 'G82.4', 'G82.5'):
        if id_cip_sec in keys:
            dic[id_cip_sec]['tetraplegia'] = 1

    else:
        if id_cip_sec in keys:
            dic[id_cip_sec]['ceguesa'] = 1

# print('comencem tetra')
# # tetraplegia
# sql = """SELECT pr_cod_u from prstb015 
#         WHERE pr_cod_ps IN ('C01-G82.50', 'G82', 'G82.3', 'G82.4', 'G82.5')"""
# for hash, in getAll(sql, 'redics'):
#     try:
#         dic[hash]['tetraplegia'] = 1
#     except:
#         txt = 'no passa res'

df = pd.DataFrame.from_dict(dic, orient='index')
df.to_csv("proves_diabetis_tipus2.csv")
