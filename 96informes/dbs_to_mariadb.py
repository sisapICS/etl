import sisapUtils as u 
import collections as c


def dbs():
    columnes = """(PACIENT VARCHAR(255),
        C_UP VARCHAR(5),
        C_UP_ABS VARCHAR(5),
        C_DATA_NAIX DATE,
        C_SEXE VARCHAR(1),
        C_GMA_COMPLEXITAT DOUBLE,
        C_NACIONALITAT VARCHAR(100),
        I_PERTUSSIS_DATA DATE,
        I_PERTUSSIS_VALOR DOUBLE)"""

    db = ('backup', 'analisi')
    tb_name = "DBS_1_5_24"

    u.createTable(tb_name, columnes, db, rm=1)

    for e in ('A', 'B', 'C', 'D', 'E', 'F', 1, 2, 3, 4, 5, 6, 7, 8, 9, 0):
        dades = list()
        print(e)
        sql = """
                SELECT
                    c_cip,
                    c_up,
                    C_UP_ABS,
                    C_DATA_NAIX,
                    C_SEXE,
                    C_GMA_COMPLEXITAT,
                    C_NACIONALITAT,
                    I_PERTUSSIS_DATA,
                    I_PERTUSSIS_VALOR
                FROM
                    dbs
                WHERE 
                    substr(c_cip,1,1) = '{}'
                """.format(e)
        count=0
        for row in u.getAll(sql, 'redics'):
            dades.append(row)
            count+=1

        upload = list()
        count=0
        for row in dades:
            upload.append(row)
            count+=1
            if count%100000 == 0:
                print(count)
                u.listToTable(upload, tb_name, db)
                upload = list()
        u.listToTable(upload, tb_name, db)
        print('----------------------')


def problemes():
    columnes = """(PACIENT VARCHAR(255),
        CODI_DX VARCHAR(25),
        DATA_ALTA DATE,
        DATA_BAIXA DATE)"""

    db = ('backup', 'analisi')
    tb_name = "problemes_1_5_24"

    id_2_hash = {}
    sql = """select id_cip, hash_d from import.u11"""
    for id, hash in u.getAll(sql, 'import'):
        id_2_hash[id] = hash

    u.createTable(tb_name, columnes, db, rm=1)
    sql = """select ID_CIP, pr_cod_ps, pr_dde, pr_dba  
                from import.problemes where pr_cod_ps like 'A37%'
                or pr_cod_ps like 'C01-A37%'"""
    upload = []
    for id, dx, dda, ddb in u.getAll(sql, 'import'):
        if id in id_2_hash:
            hash = id_2_hash[id]
            upload.append((hash, dx, dda, ddb))
    u.listToTable(upload, tb_name, db)


if __name__ == "__main__":
    problemes()

