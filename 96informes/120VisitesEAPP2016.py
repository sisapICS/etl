# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

nod = 'nodrizas'
imp = 'import'
jail = '6951'

centres = {}

sql = 'select scs_codi, ics_codi, ics_desc from jail_centres'
for up, br, desc in getAll(sql, nod):
    centres[up] = {'br': br, 'desc': desc}

tipus_visita = {}
sql ='select tv_tipus, tv_cita, tv_des from vistb206'
for tipus, cita, descr in getAll(sql, jail):
    tipus_visita[tipus, cita] = descr

serveis = {}
sql = 'select s_codi_servei, s_descripcio from pritb103'
for s, d in getAll(sql, jail):
    serveis[s] = d
    
u11 = {}    
sql = 'select cip_cip_anterior, cip_usuari_cip from usutb011'
for cip_ant, cip in getAll(sql, jail):
    u11[cip_ant] = cip
    
rel_centres = {}
sql = 'select cent_codi_centre,cent_classe_centre,cent_codi_up from pritb010'
for centre, classe, up in getAll(sql, jail):
    rel_centres[centre, classe]= up

dones = {}
sql = 'select usua_cip,usua_sexe from usutb040'
for cip, sexe  in getAll(sql, jail):
    if sexe == 'D':
        dones[cip] = True
        
visites = Counter()
sql = "select visi_usuari_cip, visi_centre_codi_centre, visi_centre_classe_centre, to_char(to_date(visi_data_visita,'J'),'YYYY'),visi_situacio_visita,visi_servei_codi_servei,visi_tipus_visita, visi_tipus_citacio from vistb043"
for cipant, centre, classe, anys, situacio, servei, tipus, citacio in getAll(sql, jail):
    if anys == '2016':
        up = rel_centres[centre,classe]
        if up in centres:
            br = centres[up]['br']
            desceap = centres[up]['desc']
            cip = u11[cipant]
            if cip in dones:
                if situacio == 'R':
                    tipusdesc = tipus_visita[tipus, citacio]
                    desc_servei = serveis[servei]
                    visites[up, br, desceap, servei, desc_servei,tipus, tipusdesc] += 1
            
upload = []                
for (up, br, desceap, servei, desc_servei,tipus, tipusdesc), res in visites.items():
     upload.append([up, br, desceap, servei, desc_servei,tipus, tipusdesc, res])
    
file = tempFolder + 'Visites_EAPP_2016.txt'
writeCSV(file, upload, sep=';') 
   