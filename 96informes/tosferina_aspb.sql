## Petici�
# cobertura de primo-vacunacions a los 1,2 a�os y de vacunaci�n completa a los 16,17 en Barcelona
# destinatario:  agencia de salut publica de bcn
# granularidad: UPs (con descripci�n)


## RECURSOS
# poblaci�n:
 # import.assignadahistorica
 # import.assignada, para a�o-de-nacimiento
 # nodrizas.cat_centres, barcelona: aga=(46,47,70,71)

# vacunas:
 # import.vacunes
 # import.cat_prstb040_new, tetanos-como-proxy: antigen='A00002'

use test;

## 1. Poblaci�n
# poblaci�n historica del periodo de interes (2010-2015)
drop table if exists test.pob_pertusis;
create table test.pob_pertusis as select
	id_cip_sec
	, dataany
	, up
	, 0 as aga
	, -100 as edat
from
	import.assignadahistorica
where
	dataany between 2010 and 2015;
;

#  poblaci�n hasta el limite superior de edad de interes (nacidos >= 1993)
drop table if exists test.pob_current;
create table test.pob_current as
 select id_cip_sec, usua_data_naixement
 from import.assignada
 where extract(year from usua_data_naixement) >= 1993 limit 100
;

# heredo el a�o de nacimiento de la poblaci�n actual
alter table test.pob_current add index(id_cip_sec);
alter table test.pob_pertusis add index(id_cip_sec);
update test.pob_pertusis p join test.pob_current c
 on p.id_cip_sec = c.id_cip_sec
 set p.edat = if(p.dataany - extract(year from usua_data_naixement) < 1, -100
				, p.dataany - extract(year from usua_data_naixement))
;				
drop table if exists test.pob_current;

# filtro las 4 cohortes de edad de interes
drop table if exists test.pob_pertusis_age;
create table test.pob_pertusis_age as select * from test.pob_pertusis where edat in (1,2,16,17);
drop table test.pob_pertusis;

# filtro poblaci�n de UPs de Barcelona
# descarto hacerlo por abs de la aga (cat_rittb001, cat_sisap_agas) pues no contempla lineas pediatricas
alter table test.pob_pertusis_age add index(up);
update test.pob_pertusis_age p join nodrizas.cat_centres c
 on  p.up = c.scs_codi
 set
	 p.aga = right(c.aga,2)
;
drop table if exists test.pob_pertusis_age_aga;
create table test.pob_pertusis_age_aga as
 select
	p.*
	,0 as vac_ok
 from
	test.pob_pertusis_age p
 where
  aga in (46,47,70,71)
;
drop table if exists test.pob_pertusis_age;

## 2. Vacunas
# codigos de vacuna del antigeno A00002 (tetanos)
drop table if exists test.cat_prstb040_new_tetanus;
create table test.cat_prstb040_new_tetanus as select * from import.cat_prstb040_new where antigen='A00002';

# vacunas del antigeno de interes
alter table test.cat_prstb040_new_tetanus add index(vacuna);
drop table if exists test.vacunes;
create table test.vacunes as select
	id_cip_sec
	, extract(year from va_u_data_vac) as vac_year
from
	import.vacunes p inner join
	test.cat_prstb040_new_tetanus c
		on p.va_u_cod = c.vacuna; 

# vacunas del A00002 de la poblacion de interes
alter table test.vacunes add index (id_cip_sec);
alter table test.pob_pertusis_age_aga add index(id_cip_sec);
drop table if exists test.vacunes_pob;
create table test.vacunes_pob as
 select	v.*
 from
	test.vacunes v inner join
	test.pob_pertusis_age_aga p
	 on v.id_cip_sec = p.id_cip_sec
;
drop table if exists test.vacunes;

# filtro por limites maximos del periodo a riesgo de vacuna (minimo birth_year - maximo a�o de interes)
# agregando la dosis por paciente y a�o de vacuna
alter table test.vacunes_pob add index (id_cip_sec);
drop table if exists test.vacunes_pob_agr;
create table test.vacunes_pob_agr as 
 select
	id_cip_sec
	,vac_year
	,count(*) as dosis
 from
	test.vacunes_pob
 where
	vac_year between 1993 and 2015
group by
	id_cip_sec
	,vac_year
;
drop table if exists test.vacunes_pob;

# acumulo las dosis totales del paciente hasta el a�o de la vacunaci�n
# HACK: tabla(x) anidada de si misma(t) con la condici�n de sumar solo los (x).periodos previos
alter table test.vacunes_pob_agr add index(id_cip_sec, vac_year);
drop table if exists  test.vacunes_pob_cum_dose;
create table test.vacunes_pob_cum_dose as
 select
	t.id_cip_sec
	,t.vac_year
    ,t.dosis
	,(select
		sum(x.dosis)
      from
		test.vacunes_pob_agr x
      where
        t.id_cip_sec = x.id_cip_sec
        and x.vac_year <= t.vac_year) as cumulative_sum
 from
	test.vacunes_pob_agr t
 order by
	t.id_cip_sec
	,t.vac_year
;
drop table if exists test.vacunes_pob_agr;

# solo las vacunas (dosis acumuladas) de momentos entre el nacimiento y el a�o de interes
alter table test.vacunes_pob_cum_dose add index(id_cip_sec);
drop table if exists test.pob_pertusis_age_aga_dosis;
create table test.pob_pertusis_age_aga_dosis as 
select
	p.*,
	(select
		max(cumulative_sum)
	 from
	 	test.vacunes_pob_cum_dose v
	where
		v.id_cip_sec = p.id_cip_sec
		and v.vac_year between p.dataany - p.edat -- naix
							and p.dataany) as cumulative_dose
from
	test.pob_pertusis_age_aga p
;
drop table if exists test.pob_pertusis_age_aga;
drop table if exists test.vacunes_pob_cum_dose;


## 3. condiciones de vacunaci�n correcta (vac_ok) en funci�n de la cohorte de edad
# primovacunaci�n >=3 dosis en 1 y 2 a�os, completa >=5 dosis en 16 y 16 a�os
update test.pob_pertusis_age_aga_dosis p set p.vac_ok = 1 where edat in (1,2) and cumulative_dose >=3;
update test.pob_pertusis_age_aga_dosis p set p.vac_ok = 1 where edat in (16,17) and cumulative_dose >=5;


## 4. export
select dataany, up, c.ics_desc, edat, sum(vac_ok), count(*), avg (vac_ok)  from test.pob_pertusis_age_aga_dosis p
left join (select scs_codi, ics_desc
from nodrizas.cat_centres where right(aga,2) in (46,47,70,71)) c on p.up=c.scs_codi
group by  dataany, up, c.ics_desc, edat
;
