from sisapUtils import *
from multiprocessing import Pool
from time import sleep

def doIt(param):
    sector,pdp,table,desti = param
    resul = []
    sql = "select alv_cip,alv_tipus,alv_up,alv_uab_up,alv_uab_codi_uab,alv_uab_servei,alv_inf_codi_uni,alv_inf_servei,alv_data_al,alv_estat from prstb550"
    for row in getAll(sql,sector):
        try:
            id = pdp[row[0]]
        except KeyError:
            continue
        else:
            resul.append((id, sector, ) + row[1:])
    listToTable(resul,table,desti)

if __name__ == '__main__':
    
    table = 'sisap_avisos'
    desti = 'redics'
    createTable(table,'(alv_cip varchar2(40),codi_sector varchar2(4),alv_tipus varchar2(6),alv_up varchar2(5),alv_uab_up varchar2(5),alv_uab_codi_uab varchar2(5),\
                alv_uab_servei varchar2(5),alv_inf_codi_uni varchar2(5),alv_inf_servei varchar2(5),alv_data_al date,alv_estat varchar2(1))',desti,rm=True)
    grantSelect(table,('PREDUMMP','PREDUPRP','PREDUECR',),desti)
    
    cataleg = []
    cataleg_table = 'sisap_avisos_ct'
    sql = 'select talv_tipus,talv_desc from prstb551'
    for row in getAll(sql,'6734'):
        cataleg.append(row)
    createTable(cataleg_table,'(cod varchar2(6),des varchar2(60))',desti,rm=True)
    grantSelect(cataleg_table,('PREDUMMP','PREDUPRP','PREDUECR',),desti)
    listToTable(cataleg,cataleg_table,desti)
    calcStatistics(cataleg_table,desti)
        
    printTime('pdp')
    pdp = {}
    sql = 'select usua_cip,usua_cip_cod from pdptb101'
    for cip, hash in getAll(sql, 'pdp'):
        pdp[cip] = hash

    printTime('sectors')
    p = Pool(8)
    p.map(doIt, [(sector,pdp,table,desti) for sector in sectors])
    p.close()
    printTime('fin')
    sleep(60)
    calcStatistics(table,desti)
