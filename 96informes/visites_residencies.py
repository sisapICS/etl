import sisapUtils as u 


print('convertint')
converters = {}
sql = """SELECT hash_redics, hash_covid FROM pdptb101_relacio"""
for hash_r, hash_c in u.getAll(sql, 'pdp'):
    converters[hash_r] = hash_c

print('assignada')
pacients = set()
sql = """select c_cip from dbs.dbs_2021
        where C_INSTITUCIONALITZAT != ''"""

for hash_r, in u.getAll(sql, ("dbs", "x0002")):
    if hash_r in converters:
        pacients.add(converters[hash_r])

print('visites')
counting = 0
sql = """SELECT pacient FROM dwsisap.sisap_master_visites
        WHERE DATA >= DATE '2021-06-01' AND DATA < DATE '2022-06-01'
        AND situacio = 'R'
        AND SISAP_SERVEI_CLASS = 'INF'
        AND tipus IN ('9C', '9R')"""
for pacient, in  u.getAll(sql, 'exadata'):
    if pacient in pacients:
        counting += 1

print('EN INF EN TOTAL ', counting, ' visites')

