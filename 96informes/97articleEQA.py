# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

debug = True

nod = 'nodrizas'
imp = 'import'
farmacs = '(82, 658, 216, 659, 713, 714, 715, 716, 717, 718, 719, 720)'
farmacsText = ['Estatines','MSRE','Denosumab', 'Bifosfonats', 'Altres aOP']

problems ='(1,211,7,212,79, 710, 711, 712)'

rcv = '(10)'

def farmconverter(farmac):
    
    if farmac in (82, 658, 216, 659):
        far = 'Estatines'
    elif farmac in (713, 714):
        far = 'MSRE'
    elif farmac in (715, 716):
        far = 'Denosumab'
    elif farmac in (717, 718):
        far = 'Bifosfonats'
    elif farmac in (719, 720):
        far = 'Altres aOP'
    else:
        far = 'error'
    return far

def psconverter(ps):
    
    if ps in (1,211):
        prob = 'CI'
    elif ps in (7,212):
        prob = 'AVC'
    elif ps in (79, 710, 711, 712):
        prob = 'Fractura fragilitat'
    else:
        prob = 'error'
    return prob

anys1 = [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016]
anys = [2016]
#mesos = ['01','02','03','04','05','06','07','08','09','10','11','12']
mesos = ['01']

centres = {}

sql = "select sha1(concat(scs_codi,'EQA')), medea from cat_centres where ep='0208'"
for up, br in getAll(sql, nod):
    centres[up] = medea
    
#dates naixement tota la pob

all_pob = {}
sql = 'select id_cip_sec, usua_sexe, year(usua_data_naixement) from assignada {0}'. format(' limit 10' if debug else '')
for id, sexe, naix in getAll(sql, imp):
    all_pob[id] = {'sexe':sexe, 'naix': naix}

#farmacs

minfarmac = {}
prevfarmac = {}
sql = 'select id_cip_sec, farmac, pres_orig, data_fi, extract(year_month from pres_orig), extract(year_month from data_fi) from eqa_tractaments where farmac in {0} {1}'.format(farmacs, ' limit 10' if debug else '')
for id, farmac, inici, fi, iniciYM, fiYM in getAll(sql, nod):
    far = farmconverter(farmac)
    if (id, far) in minfarmac:
        inici2 = minfarmac[(id, far)]
        if float(iniciYM) < float(inici2):
            minfarmac[(id, far)] = str(iniciYM)
    else:
        minfarmac[(id, far)] = str(iniciYM)
    for dat in range(iniciYM, fiYM + 1):
        dat = str(dat)
        if '00' < dat[4:] < '13':
            prevfarmac[(id, far, dat)] = True

#problemes salut:

incidents = defaultdict(list)
sql = 'select id_cip_sec, ps, dde, extract(year_month from dde) from eqa_problemes_incid where ps in {0} {1}'.format(problems, ' limit 10' if debug else '')
for id, ps, dde, Ydde in getAll(sql, nod):
    prob = psconverter(ps)
    Ydde = str(Ydde)
    incidents[id, prob].append(Ydde)
    
prevalents = {}
sql = 'select id_cip_sec, ps, dde, extract(year_month from dde) from eqa_problemes where ps in {0} {1}'.format(problems, ' limit 10' if debug else '')
for id, ps, dde, Ydde in getAll(sql, nod):
    prob = psconverter(ps)
    Ydde = str(Ydde)
    prevalents[(id, prob)] = {Ydde}

#rcv

risccv =  defaultdict(list)
sql = 'select id_cip_sec, usar, extract(year_month from data_var), valor from eqa_variables where agrupador in {0} {1}'.format(rcv, ' limit 20' if debug else '')
for id, usar, Ydde, valor  in getAll(sql, nod):
    Ydde = str(Ydde)
    valor = float(valor)
    risccv[id].append([Ydde, usar, valor])


'''
visites = {}
for ane in anys:
    for table in getSubTables('visites'):
        if ane < 2014:
            dat, = getOne('select year(visi_data_visita) from {} limit 1'.format(table), 'import')
            if dat == ane:
                for id, up in getAll("select id_cip_sec,  visi_up  from {} where visi_situacio_visita = 'R'".format(table), 'import'):
                    visites[(id, ane)] = True
'''

resultat1, resultat2, resultat3, resultat4, resultat5, resultat6, resultat7, resultat8, resultat9, resultat10, resultat11, resultat12 = Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter()
resultat13, resultat14, resultat15, resultat16, resultat17, resultat18, resultat19, resultat20, resultat21, resultat22, resultat23, resultat24,resultat25,resultat26,  = Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter()

for ane in anys:
    assignada = {}
    sql = "select id_cip_sec, sha1(concat(up,'EQA')), ates from assignadahistorica where dataany={0}{1}".format(ane, ' limit 10' if debug else '')
    for id, up, ates in getAll(sql, imp):
        if up in centres:
            medea = centres[up]
            assig = 1
            if ane < 2014:
                if (id, ane) in visites:
                    ates = 1
                else:
                    ates = 0
            sexe = 'NS'
            edat, edat5 = 'NS', 'NS'
            try:
                sexe = all_pob[id]['sexe']
                naix = all_pob[id]['naix']
                edat = ane - naix
            except KeyError:
                pass
            if edat < 15:
                continue
            if edat <>'NS':
                edat5 = ageConverter(edat)
            for mes in mesos:
                periode = str(ane) + mes
                estatinesP, MSREP, DenosumabP, bifosfonatsP, AltresP = 0,0,0,0,0
                if (id, 'Estatines', periode) in prevfarmac:
                    estatinesP = 1
                if (id, 'MSRE', periode) in prevfarmac:
                    MSREP = 1  
                if (id, 'Denosumab', periode) in prevfarmac:
                    DenosumabP = 1    
                if (id, 'Bifosfonats', periode) in prevfarmac:
                    bifosfonatsP = 1        
                if (id, 'Altres aOP', periode) in prevfarmac:
                    AltresP = 1
                estatinesI, MSREI, DenosumabI, bifosfonatsI, AltresI = 0,0,0,0,0
                if (id, 'Estatines') in minfarmac:
                    iniciest = minfarmac[(id, 'Estatines')]
                    if iniciest == periode:
                        estatinesI = 1    
                if (id, 'MSRE') in minfarmac:
                    inicimsre = minfarmac[(id, 'MSRE')]
                    if inicimsre == periode:
                        MSREI = 1 
                if (id, 'Denosumab') in minfarmac:
                    inicid = minfarmac[(id, 'Denosumab')]
                    if inicid == periode:
                        DenosumabI = 1   
                if (id, 'Bifosfonats') in minfarmac:
                    inicib = minfarmac[(id, 'Bifosfonats')]
                    if inicib == periode:
                        bifosfonatsI = 1 
                if (id, 'Altres aOP') in minfarmac:
                    inicial = minfarmac[(id, 'Altres aOP')]
                    if inicial == periode:
                        AltresI = 1  
                ciI, avcI, fractI = 0,0,0
                if (id, 'CI') in incidents:
                    for inicialM in incidents[id, 'CI']:
                        if inicialM == periode:
                            ciI = 1
                if (id, 'AVC') in incidents:
                    for inicialM in incidents[id, 'AVC']:
                        if inicialM == periode:
                            avcI = 1
                if (id, 'Fractura fragilitat') in incidents:
                    for inicialF in incidents[id, 'Fractura fragilitat']:
                        if inicialF == periode:
                            fractI = 1    
                mcvprev = 0
                if (id, 'MCV') in prevalents:
                    dprev = prevalents[(id, 'MCV')]
                    if dprev <= periode:
                        mcvprev = 1
                rcv999est, rcv510est, rcv10est, riscv5est = 0,0,0,0
                rcv999NOe, rcv510NOe, rcv10NOe, riscv5NOe = 0,0,0,0
                rcv999eve, rcv510eve, rcv10eve, riscv5eve = 0,0,0,0
                if id in risccv:
                    ultimrcv = {}
                    rows = risccv[id]
                    for drcv, usar, valorrcv in rows:
                        if drcv <= periode:
                            if id in ultimrcv:
                                lastrcv = ultimrcv[id]['dat']
                                if lastrcv < drcv:
                                    ultimrcv[id]['dat'] = drcv
                                    ultimrcv[id]['valor'] = valorrcv
                            else:
                                ultimrcv[id] = {'dat': drcv, 'valor':valorrcv}
                    if (id) in ultimrcv:
                        vrcv = ultimrcv[id]['valor']
                        if vrcv <5:
                            if estatinesP == 1:
                                riscv5est = 1
                            else:
                                riscv5NOe = 1
                            if mcvprev == 1:
                                riscv5eve = 1
                        elif 5 <= vrcv <10:
                            if estatinesP == 1:
                                rcv510est = 1
                            else:
                                rcv510NOe = 1
                            if mcvprev == 1:
                                rcv510eve = 1
                        elif vrcv >= 10:
                            if estatinesP == 1:
                                rcv10est = 1
                            else:
                                rcv10NOe = 1
                            if mcvprev == 1:
                                rcv10eve = 1
                    else:
                        if estatinesP == 1:
                            rcv999est = 1
                        else:
                            rcv999NOe = 1
                        if mcvprev == 1:
                            rcv999eve = 1
                else:
                    if estatinesP == 1:
                        rcv999est = 1
                    else:
                        rcv999NOe = 1
                    if mcvprev == 1:
                        rcv999eve = 1
                resultat1[(periode, up, medea, edat5, sexe)] += estatinesP
                resultat2[(periode, up, medea, edat5, sexe)] += estatinesI
                resultat3[(periode, up, medea, edat5, sexe)] += bifosfonatsI
                resultat4[(periode, up, medea, edat5, sexe)] += MSREI
                resultat5[(periode, up, medea, edat5, sexe)] += DenosumabI
                resultat6[(periode, up, medea, edat5, sexe)] += bifosfonatsP
                resultat7[(periode, up, medea, edat5, sexe)] += MSREP
                resultat8[(periode, up, medea, edat5, sexe)] += DenosumabP
                resultat9[(periode, up, medea, edat5, sexe)] += ciI
                resultat27[(periode, up, medea, edat5, sexe)] += avcI
                resultat10[(periode, up, medea, edat5, sexe)] += fractI
                resultat11[(periode, up, medea, edat5, sexe)] += assig
                resultat12[(periode, up, medea, edat5, sexe)] += ates
                resultat13[(periode, up, medea, edat5, sexe)] += rcv999est
                resultat14[(periode, up, medea, edat5, sexe)] += rcv510est
                resultat15[(periode, up, medea, edat5, sexe)] += rcv10est
                resultat16[(periode, up, medea, edat5, sexe)] += riscv5est
                resultat17[(periode, up, medea, edat5, sexe)] += rcv999NOe
                resultat18[(periode, up, medea, edat5, sexe)] += rcv510NOe
                resultat19[(periode, up, medea,  edat5, sexe)] += rcv10NOe
                resultat20[(periode, up, medea, edat5, sexe)] += riscv5NOe
                resultat21[(periode, up, medea, edat5, sexe)] += rcv999eve
                resultat22[(periode, up, medea, edat5, sexe)] += rcv510eve
                resultat23[(periode, up, medea, edat5, sexe)] += rcv10eve
                resultat24[(periode, up, medea, edat5, sexe)] += riscv5eve
                resultat25[(periode, up, medea, edat5, sexe)] += AltresP
                resultat26[(periode, up, medea, edat5, sexe)] += AltresI
            
upload = []
for (periode, up, edat5, sexe), res11 in resultat11.items():
    res1 = resultat1[(periode, up, medea, edat5, sexe)]
    res2 = resultat2[(periode, up, medea, edat5, sexe)]
    res3 = resultat3[(periode, up, medea, edat5, sexe)]
    res4 = resultat4[(periode, up, medea, edat5, sexe)]
    res5 = resultat5[(periode, up, medea, edat5, sexe)]
    res6 = resultat6[(periode, up, medea, edat5, sexe)]
    res7 = resultat7[(periode, up, medea, edat5, sexe)]
    res8 = resultat8[(periode, up, medea, edat5, sexe)]
    res9 = resultat9[(periode, up, medea, edat5, sexe)]
    res10 = resultat10[(periode, up, medea, edat5, sexe)]
    res12 = resultat12[(periode, up, medea, edat5, sexe)]
    res13 = resultat13[(periode, up, medea, edat5, sexe)]
    res14 = resultat14[(periode, up, medea, edat5, sexe)]
    res15 = resultat15[(periode, up, medea, edat5, sexe)]
    res16 = resultat16[(periode, up, medea, edat5, sexe)]
    res17 = resultat17[(periode, up, medea, edat5, sexe)]
    res18 = resultat18[(periode, up, medea, edat5, sexe)]
    res19 = resultat19[(periode, up, medea, edat5, sexe)]
    res20 = resultat20[(periode, up, medea, edat5, sexe)]
    res21 = resultat21[(periode, up, medea, edat5, sexe)]
    res22 = resultat22[(periode, up, medea, edat5, sexe)]
    res23 = resultat23[(periode, up, medea, edat5, sexe)]
    res24 = resultat24[(periode, up, medea, edat5, sexe)]
    res25 = resultat25[(periode, up, medea, edat5, sexe)]
    res26  = resultat26[(periode, up, medea, edat5, sexe)]
    res27  = resultat27[(periode, up, medea, edat5, sexe)]
    upload.append([periode, up, medea, edat5, sexe, res1, res2,res3,res4,res5,res25,res6,res7,res8,res26, res9,res27,res10,res11,res12,res13,res14,res15,res16,res17,res18,res19,res20,res21,res22,res23,res24])
file = tempFolder + 'projecteEQA.txt'
writeCSV(file, upload, sep=';')
