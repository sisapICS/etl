# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter


sql = ("select id_cip_sec, usua_data_naixement\
                from assignada","import")
pob = {(id): naix for (id, naix) 
                    in getAll(*sql)}

centres = {}
sql = "select up_codi_up_scs,up_desc_up_ics from import.cat_gcctb008 a inner join import.cat_gcctb007 b on a.up_codi_up_ics=b.up_codi_up_ics and dap_codi_dap in (select dap_codi_dap from import.cat_gcctb006 where amb_codi_amb='05')"
for up, desc in getAll(sql, 'nodrizas'):
    centres[up] = desc
  
visites_psp = Counter()  
for table in getSubTables('visites'):
    dat, = getOne('select year(visi_data_visita) from {} limit 1'.format(table), 'import')
    if dat == 2017 or dat == 2018:
        sql = "select  id_cip_sec,extract(year_month from visi_data_visita), visi_data_visita, s_espe_codi_especialitat, visi_up \
                   from {}, nodrizas.dextraccio where visi_data_visita <= data_ext and visi_data_baixa = 0 and visi_situacio_visita='R' and s_espe_codi_especialitat in ('02004','10147')".format(table)
        for id, periode, data, especialitat, up in getAll(sql, 'import'):
            if up in centres:
                desc = centres[up]
                if id in pob:
                    naix = pob[(id)]
                    edat = yearsBetween(naix, data)
                    if edat > 14:
                        tipus = 'pob adulta'
                    else:
                        tipus = 'pob infantil'
                    visites_psp[(periode,up,desc,especialitat,tipus, id)] += 1

pac = Counter()
visits = Counter()
for (periode,up,desc,especialitat,tipus, id), n in visites_psp.items():
    pac[(periode,up,desc,especialitat,tipus)] += 1
    visits[(periode,up,desc,especialitat,tipus)] += n
    
upload = []
for (periode,up,desc,especialitat,tipus), d in pac.items():
    v = visits[(periode,up,desc,especialitat,tipus)]
    upload.append([periode,up,desc,especialitat,tipus, d, v])


file = tempFolder + 'visites_ps.txt'
writeCSV(file, upload, sep=';')

