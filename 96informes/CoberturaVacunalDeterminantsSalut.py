# coding: utf8

"""
Vacunes segons any de naixement
"""

import urllib as w
import os
import datetime as d
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import sisapUtils as u
import collections as c

TODAY = d.datetime.now().date()

db = 'permanent'

codis_vacunes = { 'Hepatitis B': 15,
                  'Hepatitis A': 34,
                  'Triple vírica (XRP)': 38,
                  'Malaltia neumocòccica invasora 23 valent(VNC23)': 48,
                  'Tètanus': 49,
                  'Poliomieltitis (VPI)': 311,
                  'Haemophilus influenzae (Hib)': 312,                  
                  'Meningocòccica invasora serogrup C (MCC)': 313,
                  'Varicel·la (VVZ)': 314,
                  'Malaltia meningocòccica invasora serogrup B': 548,
                  'Tos ferina': 631,
                  'Malaltia neumocòccica invasora 13 valent(VNC13)': 698,                  
                  'Virus del papil·loma humà (VPH)': 782,                  
                  'Rotavirus': 790,
                  'Malaltia meningocòccica tetravalent (MACYW)': 883,
                  }   

class CoberturaVacunalDeterminantsSalut(object):
    """ . """

    def __init__(self):
        """ . """        
        self.get_municipis()
        self.get_nrenda()
        self.get_u11()
        self.get_rs()
        self.get_centres()
        self.get_nacionalitat()
        self.get_cobertura()
        self.get_problemes()
        self.get_vacunes()
        self.get_poblacio()
        self.export_data()
        # self.export_cataleg()
        # self.table_dates()
        # self.table_dates()

    def get_nrenda(self):
        """."""
        print("--------------------------------------------------- get_nrenda")
        self.nrenda = {}
        sql = "select hash, sector, nrenda from sisap_nivellrenda"  # noqa
        for hash_d, sector, nrenda in u.getAll(sql, 'redics'):
            self.nrenda[(hash_d, sector)] = nrenda

    def get_u11(self):
        """ . """
        print("------------------------------------------------------ get_u11")
        self.u11 = {}
        sql = """select id_cip_sec, hash_d, codi_sector
                 from u11"""
        for id_cip_sec, hash_d, sector in u.getAll(sql, "import"):
            self.u11[id_cip_sec] = {'hash_d': hash_d, 'sector': sector}

    def get_rs(self):
        """."""
        print("------------------------------------------------------- get_rs")
        self.rs_cat = {}
        sql = 'select rs_cod, rs_des from cat_sisap_agas'
        for rs, des in u.getAll(sql, 'import'):
            self.rs_cat[int(rs)] = des
    
    def get_centres(self):
        """ . """
        print("-------------------------------------------------- get_centres")
        self.centres = {}
        upload = []
        sql = """select 
                    scs_codi, ics_codi, ics_desc, 
                    right(rs, 2), amb_desc, ep,
                    medea
                 from cat_centres"""
        for up, br, desc, rs, ambit, ep, medea in u.getAll(sql, 'nodrizas'):
            # self.centres[up] = br
            self.centres[up] = {'br': br, 'ambit': ambit, 'medea': medea}
            upload.append([up, br, desc, self.rs_cat[int(rs)], ambit, ep])

    def get_municipis(self):
        """ . """
        u.printTime(": Municipi")
        self.municipis = {}
        sql = """select up, localitat
                 from sisap_covid_cat_territori"""
        for up, municipi in u.getAll(sql, 'redics'):
            self.municipis[up] = municipi
    
    def get_nacionalitat(self):
        """ . """
        print("--------------------------------------------- get_nacionalitat")
        self.nacionalitat = {}
        sql = """select codi_nac, regio_desc from cat_nacionalitat"""
        for cod, regio_desc in u.getAll(sql, 'nodrizas'):
            self.nacionalitat[str(int(cod))] = regio_desc
        # print(self.nacionalitat)

    def get_cobertura(self):
        """ . """
        u.printTime(": Cobertura (Atesa/Assignada)")
        self.cobertura = {}
        sql = """select
                    up
                    , count(1) as pob
                    , sum(ates) as ates
                    , count(1)/sum(ates) as ratio
                    , sum(ates)/count(1) as atesp
                 from
                    assignada_tot
                 group by up"""
        for up, _pob, _ates, _ratio, atesp in u.getAll(sql, 'nodrizas'):
            if atesp < 0.70:
                atespc = '<70 (%)'
            elif atesp >= 0.70 and atesp < 0.75:
                atespc = '70 - 74 (%)'
            elif atesp >= 0.75 and atesp < 0.80:
                atespc = '75 - 79 (%)'
            elif atesp >= 0.80 and atesp < 0.85:
                atespc = '80 - 84 (%)'
            elif atesp >= 0.85:
                atespc = '>=85 (%)'
            self.cobertura[up] = atespc

    def get_problemes(self):
        """ . """
        self.Z04 = set()
        self.Z01 = set()
        sql = """select id_cip_sec, pr_cod_ps, pr_dde
                 from problemes 
                 where pr_cod_ps like '%Z04%'
                    or pr_cod_ps like '%Z01%'"""
        for id_cip_sec, cod, _dat in u.getAll(sql, 'import'):
            if 'Z04' in cod:
                self.Z04.add(id_cip_sec)
            elif 'Z01' in cod:
                self.Z01.add(id_cip_sec)                

    def get_vacunes(self):
        """ . """
        print("-------------------------------------------------- get_vacunes")
        self.vacunats = {}
        sql = """select id_cip_sec, agrupador, dosis from ped_vacunes"""
        for id, agr, dosi in u.getAll(sql, 'nodrizas'):
            self.vacunats[(id, agr)] = dosi

        sql = """select id_cip_sec, agrupador, dosis from eqa_vacunes"""
        for id, agr, dosi in u.getAll(sql, 'nodrizas'):
            self.vacunats[(id, agr)] = dosi
                
    def get_poblacio(self):
        """ . """
        self.resultats = Counter()
        sql = """select
                    id_cip_sec, sexe, year(data_naix), up, nacionalitat
                from
                    assignada_tot
                where ates=1
                and year(data_naix)>=2004
                and sexe<>'M'"""
        for id_cip_sec, sexe, any_naix, up, nac in u.getAll(sql, 'nodrizas'):            
            hash_d = self.u11[id_cip_sec]['hash_d'] if id_cip_sec in self.u11 else None  # noqa
            sector = self.u11[id_cip_sec]['sector'] if id_cip_sec in self.u11 else None  # noqa
            nrenda = self.nrenda[(hash_d, sector)] if (hash_d, sector) in self.nrenda else None  # noqa
            regio_desc = self.nacionalitat[nac] if nac in self.nacionalitat else None  # noqa
            z04 = 'Z04' if id_cip_sec in self.Z04 else ''
            z01 = 'Z01' if id_cip_sec in self.Z01 else ''
            if up in self.centres:
                municipi = self.municipis[up]
                ambit = self.centres[up]['ambit']
                atesa_perc = self.cobertura[up]
                medea = self.centres[up]['medea']
                for c in codis_vacunes:
                    agr = codis_vacunes[c]
                    if agr == 15:
                        vacuna = 'HEPB'
                    elif agr == 34:
                        vacuna = 'HEPA'
                    elif agr == 38:
                        vacuna = 'XRP'
                    elif agr == 48:
                        vacuna = 'VNC23'
                    elif agr == 49:
                        vacuna = 'DTPa/dTPa'
                    elif agr == 311:
                        vacuna = 'VPI'
                    elif agr == 312:
                        vacuna = 'HiB'                        
                    elif agr == 313:
                        vacuna = 'MCC'
                    elif agr == 314:
                        vacuna = 'VVZ'
                    elif agr == 548:
                        vacuna = 'MCB'
                    elif agr == 631:
                        vacuna = 'TOS FERINA'
                    elif agr == 698:
                        vacuna = 'VNC13'
                    elif agr == 782:
                        vacuna = 'VPH'
                    elif agr == 790:
                        vacuna = 'ROTAV'
                    elif agr == 883:
                        vacuna = 'MACYW'
                    self.resultats[(any_naix, ambit, up, municipi, atesa_perc, medea, sexe, nrenda, regio_desc, vacuna, z04, z01, 'den')] += 1  # noqa
                    if (id_cip_sec, agr) in self.vacunats:
                        dosi = self.vacunats[(id_cip_sec, agr)]
                        self.resultats[(any_naix, ambit, up, municipi, atesa_perc, medea, sexe, nrenda, regio_desc, vacuna, z04, z01, 'num1')] += 1 if dosi > 0 else 0  # noqa
                        self.resultats[(any_naix, ambit, up, municipi, atesa_perc, medea, sexe, nrenda, regio_desc, vacuna, z04, z01, 'num2')] += 1 if dosi > 1 else 0  # noqa
                        self.resultats[(any_naix, ambit, up, municipi, atesa_perc, medea, sexe, nrenda, regio_desc, vacuna, z04, z01, 'num3')] += 1 if dosi > 2 else 0  # noqa
                        self.resultats[(any_naix, ambit, up, municipi, atesa_perc, medea, sexe, nrenda, regio_desc, vacuna, z04, z01, 'num4')] += 1 if dosi > 3 else 0  # noqa
                        self.resultats[(any_naix, ambit, up, municipi, atesa_perc, medea, sexe, nrenda, regio_desc, vacuna, z04, z01, 'num5')] += 1 if dosi > 4 else 0  # noqa
                            
    def export_data(self):
        """."""
        print("-------------------------------------------------- export_data")
        self.upload = []
        for (any_naix, ambit, up, municipi, atesa_perc, medea, sexe, nrenda, regio_desc, vacuna, z04, z01, tip), n in self.resultats.items():  # noqa
            if tip == 'den':
                num1 = self.resultats[(any_naix, ambit, up, municipi, atesa_perc, medea, sexe, nrenda, regio_desc, vacuna, z04, z01, 'num1')]  # noqa
                num2 = self.resultats[(any_naix, ambit, up, municipi, atesa_perc, medea, sexe, nrenda, regio_desc, vacuna, z04, z01, 'num2')]  # noqa
                num3 = self.resultats[(any_naix, ambit, up, municipi, atesa_perc, medea, sexe, nrenda, regio_desc, vacuna, z04, z01, 'num3')]  # noqa
                num4 = self.resultats[(any_naix, ambit, up, municipi, atesa_perc, medea, sexe, nrenda, regio_desc, vacuna, z04, z01, 'num4')]  # noqa
                num5 = self.resultats[(any_naix, ambit, up, municipi, atesa_perc, medea, sexe, nrenda, regio_desc, vacuna, z04, z01, 'num5')]  # noqa
                self.upload.append([any_naix, ambit, up, municipi, atesa_perc, medea, sexe, nrenda, regio_desc, vacuna, z04, z01, num1, num2, num3, num4, num5, n])  # noqa

        # u.listToTable(upload, tb, db) 
        u.writeCSV(u.tempFolder + 'CoberturaVacunalDeterminantsSalut_{}.txt'.format(TODAY.strftime('%Y%m%d')),  # noqa
                   [('any_naix',
                     'ambit', 'up', 'municipi',
                     'atesa_perc',  'medea', 'sexe', 'nrenda',
                     'nacionalitat', 'vacuna',
                     'z04', 'z01',
                     '1dosi', '2dosi', '3dosi', '4dosi', '5dosi',
                     'poblacio')] + self.upload)
        
    # def export_cataleg(self):
    #     """."""
    #     upload = []
    #     for c in codis_vacunes:
    #         agr = codis_vacunes[c]
    #         upload.append([agr, c])
            
    #     u.listToTable(upload, cataleg, db) 

    # def table_dates(self):
    #     """."""
    #     upload = []
    #     sql = "select data_ext from dextraccio"
    #     for data_ext, in u.getAll(sql, 'nodrizas'):
    #         upload.append([data_ext])
           
    #     u.listToTable(upload, tb_date, db)  

   
if __name__ == '__main__':
    u.printTime(": Inici")
    
    # tb = "mst_cohorts_vacunals"
    # columns =  ["Cohort int", "up varchar(5)", "sexe varchar(10)", "codi_vacuna int", "1dosi int", "2dosis int", "3dosis int", "4dosis int", "5dosis int", "poblacio int"]     
    # u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
    
    # cataleg = "mst_cohorts_cataleg"
    # columns = ["codi_vacuna int", "literal varchar(300)"]
    # u.createTable(cataleg, "({})".format(", ".join(columns)), db, rm=True)
    
    # tb_date = "mst_cohorts_data"
    # columns = ["data_calcul date"]
    # u.createTable(tb_date, "({})".format(", ".join(columns)), db, rm=True)
    
    # tb_eap = "mst_cohorts_eaps"
    # columns = ["up varchar(5)", "br varchar(5)", "descripcio varchar(500)", "RS varchar(100)", "ambit varchar(100)", "ep varchar(5)"]
    # u.createTable(tb_eap, "({})".format(", ".join(columns)), db, rm=True)
    
    CoberturaVacunalDeterminantsSalut()
    
    u.printTime(": Fi")