# -*- coding: utf-8 -*-

"""
- 2020/11/19 : Mail: Des del Ministeri, ens sol·liciten un avançament de les
  dades de cobertures de vacunació de la GRIP de la població de 65 anys o més.
  Les dades del Segrip són per majors de 59 anys.

"""

import sisapUtils as u
import collections as c
import datetime as d
from os import remove

TODAY = d.datetime.now().date()

vacunes_grip = ('GRIP-N',
                'GRIP',
                'GRIP-A',
                'G(A)-4',
                'G(A)-2',
                'G(A)-3',
                'G(A)-1',
                'P-G-AR',
                'P-G-NE',
                'P-G-AD',
                'P-G-60')


class VacunacioGrip65a(object):
    """ . """

    def __init__(self):
        """ . """

        self.get_vacunes()
        self.get_centres()
        self.get_poblacio()
        self.get_u11()
        self.get_upload()
        self.export_upload()

    def get_vacunes(self):
        """
        cols = u.getTableColumns('prstb051','6844')
        print(cols)
        """
        print("-------------------------------------------------- get_vacunes")
        ts = d.datetime.now()
        resultat = u.multiprocess(self.get_vacunes_worker, u.sectors)
        self.vacunes = {}
        for dbsector in resultat:
            for sector, hash_d, cod, dat, motiu in dbsector:
                self.vacunes[(hash_d, sector)] = {'cod': cod, 'dat': dat,
                                                  'motiu': motiu}
        print('Time execution {} / nrow {}'.format(d.datetime.now() - ts,
                                                   len(self.vacunes)))

    def get_vacunes_worker(self, sector):
        """ Worker de prstb051 """
        sql = "select va_u_usua_cip, va_u_cod, va_u_data_vac, va_u_mot_vac \
               from prstb051 \
               where va_u_cod in {} \
               and va_u_data_vac >= to_date('01/09/2020','dd/mm/yyyy') \
               and va_u_dosi > 0".format(vacunes_grip)

        db = sector + 'af'
        converter = []
        for hash_d, cod, dat, motiu in u.getAll(sql, db):
            converter.append((sector, hash_d, cod, dat, motiu))
        print(db)
        return converter

    def get_centres(self):
        """ . """
        print("-------------------------------------------------- get_centres")
        sql = "select scs_codi, ics_desc, amb_desc  from cat_centres"
        db = "nodrizas"
        self.cat_centres = {}
        for up, up_desc, amb_desc in u.getAll(sql, db):
            self.cat_centres[up] = {'up_desc': up_desc, 'amb_desc': amb_desc}

    def get_poblacio(self):
        """ . """
        print("------------------------------------------------- get_poblacio")

        sql = "select id_cip_sec, up, edat from assignada_tot where edat >= 65"
        db = "nodrizas"
        self.poblacio = {}
        for id_cip_sec, up, edat in u.getAll(sql, db):
            self.poblacio[id_cip_sec] = {'up': up, 'edat': edat}

    def get_u11(self):
        """ . """
        print("------------------------------------------------------ get_u11")

        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        db = "import"
        self.u11 = {}
        for id_cip_sec, sector, hash_d in u.getAll(sql, db):
            self.u11[id_cip_sec] = {'sector': sector, 'hash_d': hash_d}

    def get_upload(self):
        """ . """
        self.upload_agr = c.Counter()
        for id_cip_sec in self.poblacio:
            self.upload_agr[('DEN')] += 1
            sector = self.u11[id_cip_sec]['sector']
            hash_d = self.u11[id_cip_sec]['hash_d']
            if (hash_d, sector) in self.vacunes:
                self.upload_agr[('NUM')] += 1
        print(self.upload_agr)

    def export_upload(self):
        """ . """
        print("------------------------------------------------ export_upload")
        self.uploadtxt = []
        for (tip), n in self.upload_agr.items():
            self.uploadtxt.append((tip, n))
        file = u.tempFolder + 'VacunacioGrip65a_{}.csv'
        file = file.format(TODAY.strftime('%Y%m%d'))
        u.writeCSV(file,
                   [('tip', 'n')]
                   + self.uploadtxt,
                   sep=';')
        print("Send Mail")
        subject = 'Vacunació Grip >=65a'
        text = 'Arxiu amb les dades de vacunació grip >= 65a'
        u.sendGeneral('SISAP <sisap@gencat.cat>',
                      'cguiriguet.bnm.ics@gencat.cat',
                      'ehermosilla@idiapjgol.info',
                      subject,
                      text,
                      file)
        remove(file)


if __name__ == "__main__":
    VacunacioGrip65a()
