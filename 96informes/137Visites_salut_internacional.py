# coding: utf8

debug = False

from sisapUtils import *
from collections import defaultdict,Counter

imp = 'import'
nod = 'nodrizas'

centres = ['03576','05882','00519','04355','03954']

centres_desc,brs = {},{}
sql = 'select up_codi_up_ics, up_codi_up_scs from cat_gcctb008'
for br, up in getAll(sql, imp):
    if str(up) in centres:
        brs[br] = up

sql = 'select up_codi_up_ics, up_desc_up_ics from cat_gcctb007'
for br, desc in getAll(sql, imp):
    if br in brs:
        up = brs[br]
        centres_desc[up] = desc
    
serveis = {}
sql = 'select codi_sector, s_codi_servei, s_descripcio from cat_pritb103'
for sec, servei, desc in getAll(sql, 'import'):
    serveis[(sec, servei)] = desc
    
moduls = {}
for sec, cen, cla, ser, cod, desc,  magenda in getAll("select codi_sector, modu_centre_codi_centre, modu_centre_classe_centre, modu_servei_codi_servei, modu_codi_modul, modu_descripcio, modu_model_agenda from cat_vistb027", 'import'):
    moduls[(sec, cen, cla, ser, cod)] = {'desc': desc, 'capes': magenda}

tipus_visita = {}
sql ='select codi_sector, tv_tipus, tv_cita, tv_des from cat_vistb206'
for sector, tipus, cita, descr in getAll(sql, imp):
    tipus_visita[(sector, tipus, cita)] = descr

recomptes = Counter()  
agendes = {}  
for table in getSubTables('visites'):
    dat, = getOne('select year(visi_data_visita) from {} limit 1'.format(table), imp)
    if dat == 2016:
        sql = "select  codi_sector, visi_up, visi_centre_codi_centre, visi_centre_classe_centre, visi_servei_codi_servei,visi_modul_codi_modul, visi_data_visita, extract(year_month from visi_data_visita),visi_tipus_visita, visi_tipus_citacio\
               from {} where visi_data_baixa = 0 and  visi_situacio_visita ='R' {}".format(table, ' limit 10' if debug else '')
        for sector, up, centre, classe, servei, modul, data, periode, tipus, citacio in getAll(sql, imp):
            if str(up) in centres:
                desc = centres_desc[up]
                try:
                    serveidesc = serveis[(sector, servei)]
                except KeyError:
                    serveidesc = None
                try:
                    modulsdesc = moduls[(sector, centre, classe, servei, modul)]['desc']
                except KeyError:
                    modulsdesc = None
                try:
                    capes = moduls[(sector, centre, classe, servei, modul)]['capes']
                except KeyError:
                    capes = None
                try:
                    tipusdesc = tipus_visita[(sector, tipus, citacio)]
                except KeyError:
                    tipusdesc = None
                recomptes[(up, desc, sector, servei, serveidesc, modul, modulsdesc, tipus, tipusdesc, periode)] += 1
                agendes[(sector, centre, classe, servei, modul)] = capes
upload = []
for (up, desc, sector, servei, serveidesc, modul, moduldesc, tipus, tipusdesc, periode), r in recomptes.items():
    upload.append([up, desc, sector, servei, serveidesc, modul, moduldesc, tipus, tipusdesc, periode, r])
    
file = tempFolder + 'Visites_salut_internacional_2016.txt'
writeCSV(file, upload, sep=';')         

upload = []
for (sector, centre, classe, servei, modul),capes in agendes.items():
    upload.append([sector, centre, classe, servei, modul, capes])

file = tempFolder + 'Agendes_salut_internacional_2016.txt'
writeCSV(file, upload, sep=';')  