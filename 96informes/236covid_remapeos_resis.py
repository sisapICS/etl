import sisapUtils as u

LEO = "DWLMENDEZ"
DB_DESTI = 'exadata'
TB_DESTI = 'sisap_remapeo_resis'


cols = ['old_resi varchar2(13)',
        'new_resi varchar2(13)',
        ]

u.createTable(TB_DESTI, "({})".format(", ".join(cols)), DB_DESTI, rm=True)
upload = [
    ['6838-7950', '04764'],
    ['6102-5071', '13239'],
    ['6102-5131', '13105'],
    ['6102-5471', '13213'],
    ['6102-5491', '13236'],
    ['6102-5531', '13469'],
    ['6102-5591', '13154'],
    ['6102-5651', '13225'],
    ['6209-2021', '13303'],
    ['6209-2081', '13080'],
    ['6211-5284', '13259'],
    ['6211-5924', '13180'],
    ['6416-812', '13053'],
    ['6416-815', '13426'],
    ['6416-829', '07518'],
    ['6519-3767', '13050'],
    ['6519-3768', '13328'],
    ['6519-3847', '13287'],
    ['6519-3848', '13287'],
    ['6519-3867', '13110'],
    ['6519-3868', '13276'],
    ['6519-3869', '13086'],
    ['6519-3870', '13228'],
    ['6519-545', '13287'],
    ['6521-3745', '04450'],
    ['6522-1104', '13059'],
    ['6523-2569', '08446'],
    ['6523-2609', '13156'],
    ['6625-1543', '04433'],
    ['6625-1583', '13317'],
    ['6625-1584', '13319'],
    ['6625-1684', '13238'],
    ['6625-1685', '13215'],
    ['6625-1723', '13309'],
    ['6626-8909', '20804'],
    ['6731-5733', '13191'],
    ['6731-6593', '13314'],
    ['6731-6594', '13165'],
    ['6735-7385', '13113'],
    ['6837-682', '13092'],
    ['6838-5930', '13286'],
    ['6838-6830', '13282'],
    ['6838-808', '13290'],
    ['6839-2181', '13212'],
    ['6839-865', '13092'],
    ['6844-2666', '13262'],
    ['6844-2746', '13111'],
    ['6844-3366', '13271'],
    ['6844-3367', '13220'],
    ['6844-3368', '13121'],
    ['6844-3386', '13277'],
    ['6844-3466', '13323'],
    ['6844-3506', '13253'],
    ['6844-3846', '13252'],
    ['6844-3847', '13251'],
    ['6844-4148', '13142'],
    ['6844-4486', '13153'],
    ['6102-5631', '13519'],
    ['6211-5744', '13551'],
    ['6310-1883', '13492'],
    ['6416-822', '13541'],
    ['6519-3807', '13507'],
    ['6625-1123', '13522'],
    ['6626-4819', '07944'],
    ['6731-6497', '13560'],
    ['6731-6498', '13171'],
    ['6838-7210', '13514'],
    ['6839-1680', '13573'],
    ['6839-2201', '13496'],
    ['6844-3266', '13601'],
    ['6844-3286', '13537'],
    ['6844-3746', '13627'],
    ['6844-4347', '13562'],
    ['6102-5371', '13874'],
    ['6416-823', '13822'],
    ['6416-8483', '14155'],
    ['6523-2589', '13949'],
    ['6523-2649', '13017'],
    ['6626-8849', '13737'],
    ['6844-3226', '13987'],
    ['6844-3467', '13986'],
    ['6844-4326', '13932'],
    ['6838-7130', '08186'],
    ]
u.listToTable(upload, TB_DESTI, DB_DESTI)
u.grantSelect(TB_DESTI, LEO, DB_DESTI)

# BACKUP
sql = """
    DROP TABLE residencies_cens_bkp_leo
    """
u.execute(sql, DB_DESTI)
sql = """
    CREATE TABLE residencies_cens_bkp_leo AS (SELECT * FROM residencies_cens)
    """
u.execute(sql, DB_DESTI)
u.grantSelect('residencies_cens_bkp_leo', LEO, DB_DESTI)

# UPDATES
sqls = []
tb = 'residencies_cens'
for old, new in upload:
    sqls.append("update {tb} set residencia = '{new}' where residencia = '{old}'".format(tb=tb, old=old, new=new))

for sql in sqls:
    u.execute(sql, 'exadata')

# LIMPIEZA
sql = "DROP TABLE {}".format(TB_DESTI)
u.execute(sql, DB_DESTI)

sql = "DROP TABLE {}".format("residencies_cens_bkp_leo")
u.execute(sql, DB_DESTI)
