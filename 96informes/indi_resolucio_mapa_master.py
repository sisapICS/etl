# -*- coding: utf-8 -*-

import sisapUtils as u
import collections as c


ESPECIALISTES_INTERES = {
    '10104': 
        {'ps': 18,
        'def': 'ENDOCRI'}, # hipotiroidisme 
    '10098': 
        {'ps': 951,
        'def': 'TRAUMA'}, # total, no codis
    '10096': 
        {'ps': 950,
        'def': 'UROLOGIA'}, # total, no codis + aquest 950
    # derma total
    '10103':
        {'def': 'DERMA'},
    # oftal total
    '10110':
        {'def': 'OFTALMOLOGIA'},
    # otorrino total
    '10111':
        {'def': 'OTORRINO'},
    # cardio total
    '10131':
        {'def': 'CARDIO'}
}

INDICADORS = {
        ('10104', 'pri', '')  :  'P_POB_ENDO_P',
        ('10104', 'esp', '')  :  'P_POB_ENDO_A',
        ('10098', 'pri', '')  :  'P_POB_TRAUMA_P',
        ('10098', 'esp', '')  :  'P_POB_TRAUMA_A',
        ('10096', 'pri', '')  :  'P_POB_URO_P',
        ('10096', 'esp', '')  :  'P_POB_URO_A',
        ('10103', 'pri', '')  :  'P_POB_DERMA_P',
        ('10103', 'esp', '')  :  'P_POB_DERMA_A',
        ('10110', 'pri', '')  :  'P_POB_OFTAL_P',
        ('10110', 'esp', '')  :  'P_POB_OFTAL_A',
        ('10111', 'pri', '')  :  'P_POB_OTORRINO_P',
        ('10111', 'esp', '')  :  'P_POB_OTORRINO_A',
        ('10131', 'pri', '')  :  'P_POB_CARDIO_P',
        ('10131', 'esp', '')  :  'P_POB_CARDIO_A',
        ('10104', 'pri', 'dm2')  :  'P_MAL_DM2_P',
        ('10104', 'esp', 'dm2')  :  'P_MAL_DM2_A',
        ('10104', 'pri', 'hipo')  :  'P_MAL_HIPO_P',
        ('10104', 'esp', 'hipo')  :  'P_MAL_HIPO_A',
}

ESPECIALISTES = ('10104', '10098', '10096', '10103', '10110', '10111', '10131')
QUI_DERIVA = ('pri', 'esp')


class MST_EXTERNES():
    def __init__(self):
        self.create_tables()
        self.get_dextraccio()
        self.get_externes()
        self.get_assignada()
        self.get_hash_cip()
        self.get_taula()

    
    def create_tables(self):
        print('create_tables')
        self.table = 'mst_resolucio_mapa'
        self.db = 'altres'
        cols = """(id_cip_sec varchar(100), up varchar(10), uba varchar(10), 
                    upinf varchar(10), ubainf varchar(10), 
                    edat varchar(10), sexe varchar(10), 
                    ates int, institu int,
                    diabetis int, hipotiroidisme int, 
                    endocri int, qui_endo varchar(2),
                    trauma int, qui_trauma varchar(2),
                    uro int, qui_uro varchar(2),
                    derma int, qui_derma varchar(2),
                    oftal int, qui_oftal varchar(2),
                    otorrino int, qui_otorrino varchar(2),
                    cardio int, qui_cardio varchar(2))"""
        u.createTable(self.table, cols, self.db, rm=True)

    def get_dextraccio(self):
        print('dextraccio')
        dext, = u.getOne('select data_ext from nodrizas.dextraccio', 'nodrizas')
        day = str(dext.day) if len(str(dext.day)) == 2 else '0'+str(dext.day)
        month = str(dext.month) if len(str(dext.month)) == 2 else '0'+str(dext.month)
        year = str(dext.year)
        self.dat_str = year + '-' + month + '-' + day
    
    def get_externes(self):
        print('externes')
        especialitats = tuple(ESPECIALISTES_INTERES.keys())
        self.visitats = c.defaultdict(set)
        sql = """SELECT 
                    a.hash_redics, 
                    c.AEA_VVI_ESASS,
                    c.AEA_VVI_DVISI,
                    c.AEA_VRC_PRASS
                FROM 
                    DWCATSALUT.CMBD_AEA c INNER JOIN dwsisap.RCA_CIP_NIA b
                    ON c.nia = b.nia 
                    INNER JOIN DWSISAP.PDPTB101_RELACIO a
                    ON a.hash_covid = b.hash 
                WHERE 
                    c.AEA_VVI_ESASS in {}
                    AND MONTHS_BETWEEN(to_date('{}', 'YYYY-MM-DD'),c.AEA_VVI_DVISI) <= 12""".format(especialitats, self.dat_str)
                # si volguessim nom�s prim�ria AND c.AEA_VRC_PRASS='1'    
        for hash_r, especialista, data_visita, qui_deriva in u.getAll(sql, 'exadata'):
            if qui_deriva == '1': 
                qui_deriva = 'pri'
                self.visitats[(especialista, qui_deriva)].add(hash_r)
                try:
                    if hash_r in self.visitats[(especialista, 'esp')]:
                        self.visitats[(especialista, 'esp')].pop(hash_r)
                except:
                    pass
            else: 
                qui_deriva = 'esp'
                try:
                    if hash_r not in self.visitats[(especialista, 'pri')]:
                        self.visitats[(especialista, qui_deriva)].add(hash_r)
                except:
                    pass

        for especialista_d in self.visitats:
            print(especialista_d, len(self.visitats[especialista_d]))
    
    def get_assignada(self):
        print('assignada')
        sql = """SELECT
                    c_cip,
                    c_up,
                    c_metge,
                    c_infermera,
                    c_edat_anys,
                    C_SEXE,
                    CASE
                        WHEN MONTHS_BETWEEN(CURRENT_DATE , C_ULTIMA_VISITA_EAP) <= 12 THEN 1
                        ELSE 0
                    END ates,
                    CASE WHEN PS_DIABETIS2_DATA IS NOT null THEN 1 ELSE 0 END diabetis,
	                CASE WHEN PS_HIPOTIROIDISME_DATA IS NOT null THEN 1 ELSE 0 END hipotiroidisme,
                    CASE WHEN C_INSTITUCIONALITZAT IS NULL or C_INSTITUCIONALITZAT = '0' THEN 0 ELSE 1 END insti
                FROM
                    dbs"""
        self.poblacio = {}
        self.diabetics = set()
        self.hipotoroidisme = set()
        for id, up, uba, ubainf, edat, sexe, ates, dm2, hipo, insti in u.getAll(sql, 'redics'):
            self.poblacio[id] = [up, uba, up, ubainf, edat, sexe, ates, insti, dm2, hipo]

    def get_hash_cip(self):
        self.hash_2_cip = {}
        sql = """select id_cip_sec, hash_d from import.u11"""
        for cip, hash in u.getAll(sql, 'import'):
            self.hash_2_cip[hash] = cip

    def get_taula(self):
        for id, (up, uba, upinf, ubainf, edat, sexe, ates, insti, dm2, hipo) in poblacio.items():
            if id in self.hash_2_cip:
                id_cip = self.hash_2_cip[id]
                # ENDO
                if id in self.visitats[('10104', 'pri')]: 
                    endo = 1
                    qui_endo = 'P'
                elif id in self.visitats[('10104', 'esp')]:
                    endo = 1
                    qui_endo = 'A'
                else: 
                    endo = 0 
                    qui_endo = ''
                # TRAUMA
                if id in self.visitats[('10098', 'pri')]: 
                    trauma = 1
                    qui_trauma = 'P'
                elif id in self.visitats[('10098', 'esp')]:
                    trauma = 1
                    qui_trauma = 'A'
                else: 
                    trauma = 0
                    qui_trauma = ''
                # URO
                if id in self.visitats[('10096', 'pri')]: 
                    uro = 1
                    qui_uro = 'P'
                elif id in self.visitats[('10096', 'esp')]:
                    uro = 1
                    qui_uro = 'A'
                else: 
                    uro = 0
                    qui_uro = ''
                # DERMA
                if id in self.visitats[('10103', 'pri')]: 
                    derma = 1
                    qui_derma = 'P'
                elif id in self.visitats[('10103', 'esp')]:
                    derma = 1
                    qui_derma = 'A'
                else: 
                    derma = 0
                    qui_derma = ''
                # OFTAL
                if id in self.visitats[('10110', 'pri')]: 
                    oftal = 1
                    qui_oftal = 'P'
                elif id in self.visitats[('10110', 'esp')]:
                    oftal = 1
                    qui_oftal = 'A'
                else: 
                    oftal = 0
                    qui_oftal = ''
                # OTORRINO
                if id in self.visitats[('10111', 'pri')]: 
                    otorrino = 1
                    qui_otorrino = 'P'
                elif id in self.visitats[('10111', 'esp')]:
                    otorrino = 1
                    qui_otorrino = 'A'
                else: 
                    otorrino = 0
                    qui_otorrino = ''
                # CARDIO
                if id in self.visitats[('10131', 'pri')]: 
                    cardio = 1
                    qui_cardio = 'P'
                elif id in self.visitats[('10131', 'esp')]:
                    cardio = 1
                    qui_cardio = 'A'
                else: 
                    cardio = 0
                    qui_cardio = ''
            
            upload.append((id_cip, up, uba, upinf, ubainf, edat, sexe, ates, insti, dm2, hipo, endo, 
                    qui_endo, trauma, qui_trauma, uro, qui_uro, derma, qui_derma, oftal, qui_oftal, 
                    otorrino, qui_otorrino, cardio, qui_cardio))
        
        u.listToTable(upload, self.table, self.db)    

if __name__ == "__main__":
    MST_EXTERNES()
    
