# coding: iso-8859-1


from sisapUtils import *
from collections import defaultdict, Counter

sql = "select scs_codi, ics_desc, sap_desc, amb_desc from cat_centres where ep='0208'"
centros = {}
for up, ics_desc, sap_desc, amb_desc in getAll(sql, 'nodrizas'):
    centros[up] = {'ics': ics_desc, 'sap': sap_desc, 'ambit':amb_desc}


sql= 'select id_cip_sec, val from nodrizas.ped_variables where agrupador=285'
valormax = {}
for id, valor in getAll(sql, 'nodrizas'):
    valor = int(float(valor.replace(',', '.')))
    if valor > 6:
        valor = 6
    if id in valormax:
        valor2 = valormax[id]
        if valor > valor2:
            valormax[id] = valor
    else:
        valormax[id] = valor
        
mesosLM = Counter()
sql = "select id_cip_sec, year(data_naix) as any_naix, up from nodrizas.ped_assignada where data_naix between '2011/01/01' and '2015/12/31'"
for id, anys, up in getAll(sql, 'nodrizas'):
    if up in centros:
        try:
            valor = valormax[id]
        except KeyError:
            valor = None
        mesosLM[(anys, valor)] += 1
        

upload = []
for (anys, valor), d in mesosLM.items():
    upload.append([anys, valor, d])

file = tempFolder + 'lm_mes.txt'
writeCSV(file, upload, sep=';')
