# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict, Counter
import csv
import os
import sys
from time import strftime

printTime('Inici')

db = 'altres'
redics = 'redics'
table = 'sisap_tiresGlucomen'

imp = 'import'
nod = 'nodrizas'
tractaments = 'eqa_tractaments'
tires = 'tires'
tires2 = 'tires2'
problemes = 'eqa_problemes'
agrupadorDM = "ps in ('18', '24', '487')"
agrFar = "farmac in (412,413,22)"

OutFile = tempFolder + 'tiresglucomen.txt'
tableMy = 'tiresglucomen'

hashos = {}
sql = 'select id_cip_sec, codi_sector, hash_d from u11'
for id, sector, hash in getAll(sql, imp):
    hashos[id] = {'sector': sector, 'hash': hash}

assig = {}
sql = 'select id_cip_sec,up,uba,upinf,ubainf from assignada_tot'
for id, up, uba, upinf, ubainf in getAll(sql, nod):
    assig[id] = {'up': up, 'uba': uba, 'upinf': upinf, 'ubainf': ubainf}

pacTires, tipusDM, farmacs = {}, {}, {}

sql = "select id_cip_sec, fit_cod from {}".format(tires)
for id, cod in getAll(sql, imp):
    pacTires[cod] = id

sql = "select id_cip_sec, ps from {0} where {1} order by ps desc".format(problemes, agrupadorDM)
for id, agr in getAll(sql, nod):
    if agr == 18:
        tip = 'DM2'
    if agr == 24:
        tip = 'DM1'
    if agr == 487:
        tip = 'DMgestacional'
    tipusDM[id] = tip

sql = "select id_cip_sec, farmac from {0} where {1} order by farmac asc".format(tractaments, agrFar)
for id, farm in getAll(sql, nod):
    if farm == 22:
        fs = 'ADO'
    elif farm == 412:
        fs = 'Insulina ràpida'
    elif farm == 413:
        fs = 'Insulina lenta i intermitja'
    farmacs[id] = fs

tiresGlucomen = Counter()
upload = []

sql = "select distinct dtr_cod_pac,dtr_up from {0} where  dtr_data_baixa is null and DTR_PD_COD in ('1076','1078') and DTR_DARRER_LL>'2015/07/01'".format(tires2)
for codi, up in getAll(sql, imp):
    try:
        idcip = pacTires[codi]
    except KeyError:
        continue
    try:
        dm = tipusDM[idcip]
    except KeyError:
        dm = 'NO PS'
    try:
        tto = farmacs[idcip]
    except KeyError:
        tto = 'Sense tractament'
    tiresGlucomen[up, dm, tto] += 1
    try:
        upass = assig[idcip]['up']
        uba = assig[idcip]['uba']
        upinf = assig[idcip]['upinf']
        ubainf = assig[idcip]['ubainf']
    except KeyError:
        upass = ''
        uba = ''
        upinf = ''
        ubainf = ''
    sector = hashos[idcip]['sector']
    hash = hashos[idcip]['hash']
    upload.append([sector, hash, up, upass, uba, upinf, ubainf, dm, tto])

try:
    remove(OutFile)
except:
    pass

with openCSV(OutFile) as c:
    for (up, dm, tto), d in tiresGlucomen.items():
        c.writerow([up, dm, tto, d])

'''
execute("drop table if exists {}".format(tableMy), db)
execute("create table {} (up varchar(5), ps varchar(20) not null default'', tractament varchar(20) not null default'', recompte double)".format(tableMy), db)
loadData(OutFile, tableMy, db)
'''


try:
    execute('drop table {}'.format(table), redics)
except:
    pass

sql = "create table {}(codi_sector varchar2(4), hash varchar2(40),up varchar2(5), upassig varchar2(5), uba varchar2(5), upinf varchar2(5),ubainf varchar2(5),\
         diagnostic varchar2(200), tractament varchar2(200))".format(table)
execute(sql, redics)
uploadOra(upload, table, redics)

users = ['PREDUECR', 'PREDUMMP', 'PREDUPRP']
for user in users:
    execute("grant select on {} to {}".format(table, user), redics)

printTime('Fi')
