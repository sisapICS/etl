# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter


###DBSCATDESC


#Canviar descripcio cataleg DBSCATDESC
'''
agrupador = 'V_VHB_CARREGA'
descripcio = 'VHB Carrega'

sql = "update dbscatdesc set literal_curt='{0}' where agrupador='{1}'".format(descripcio, agrupador)
execute(sql, 'redics')

sql = "select * from dbscatdesc where agrupador='{}'".format(agrupador)
for row in getAll(sql, 'redics'):
    print row
'''
    
#Fer update: un camp en una variable (per ex. afegir a laboratori variable ja existent)
"""
agrupador = 'PS_TDAH'
columna = 'ADULTS'

sql = "update dbscatdesc set {0} = 1 where agrupador = '{1}'".format(columna, agrupador)
execute(sql, 'redics')

sql = "select * from dbscatdesc where agrupador='{}'".format(agrupador)
for row in getAll(sql, 'redics'):
    print row
"""

#Canviar literal titol (os igui tooltip)

'''
agrupador = 'V_UDVP'
tooltip ='(0 No Consumidor;1 Consumidor VP; 2 Ex-consumidor VP i consumidor VNP; 3 Ex-consumidor VP i ex-consumidor VNP; 4 Ex-consumidor VP; 5 Consumidor VNP 6 Ex-Consumidor VNP)'

sql = "update dbscatdesc set literal_titol = '{0}' where agrupador = '{1}'".format(tooltip, agrupador)
execute(sql, 'redics')

sql = "select * from dbscatdesc where agrupador='{}'".format(agrupador)
for row in getAll(sql, 'redics'):
    print row

'''
#Insertar un nou grup a catdesc
"""
agrupador = 'F_MHDA_VHC'
presons, adults, nens, odn, lab = 1, 1, 0, 0, 0
literal = ''
literal_curt = 'MHDA - F�rmacs pel VHC'
tipus = ''
ordre = 0
literal_titol = ''

sql = "insert into dbscatdesc values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')".format(agrupador, presons, adults, nens, odn, lab, literal, literal_curt, tipus, ordre, literal_titol)
execute(sql, 'redics')
sql = "select * from dbscatdesc where agrupador in ('{0}')".format(agrupador)
for row in getAll(sql, 'redics'):
    print row
"""

###DBSCAT

#Substituir codi existent per un altre
'''
codi_antic = 'A10BX09'
codi = 'A10BK01'

sql = "update dbscat set codi='{0}' where codi='{1}'".format(codi,codi_antic)
execute(sql, 'redics')

sql = "select * from dbscat where codi in ('{0}','{1}')".format(codi, codi_antic)
for row in getAll(sql, 'redics'):
    print row

'''
"""
#Canviar codi de grup

codis= ['J05AR01','J05AR02','J05AR03','J05AR06','J05AR08','J05AR09','J05AR10','J05AR13','J05AR14','J05AR15','J05AR17','J05AR18','J05AX08','J05AX09','J05AX12']
nou_grup = 'F_MHDA_VIH'

for codi in codis:
    sql = "update dbscat set agrupador='{0}' where codi='{1}'".format(nou_grup,codi)
    execute(sql, 'redics')

    sql = "select * from dbscat where codi='{0}'".format(codi)
    for row in getAll(sql, 'redics'):
        print row
"""

#afegir registres


taula = 'tractaments'
codis = ['R03AK11']
agrupador = 'F_MPOC_ASMA'
pstancat = 0

for codi in codis:
    sql = "insert into dbscat values ('{0}','{1}','{2}','{3}')".format(taula, codi, agrupador, pstancat)
    execute(sql, 'redics')
sql = "select * from dbscat where agrupador in ('{0}')".format(agrupador)
for row in getAll(sql, 'redics'):
    print row

###FARMACS NO RECOMANATS

#inserir atc
'''
atcs = ['N06AX26']

for atc in atcs:
    sql = "insert into dbs_cataleg_no_recomanat values ('{0}')".format(atc)
    execute(sql, 'redics')
    sql = "select * from dbs_cataleg_no_recomanat where atc_7 in ('{0}')".format(atc)
    for row in getAll(sql, 'redics'):
        print row

'''

#COMMIT

sql = 'commit'
execute(sql, 'redics')

    
    