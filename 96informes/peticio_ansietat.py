
import sisapUtils as u
import collections as c

"""
365 --> benzos
en tota hs actius --> despres ddx benzo > 50 dies
"""

u.createTable('peticio_ansietat', '(id int, ansietat date, tractament date)', 'test', rm=True)

# poblacio assignada atesa major o igual a 18 anys
print('poblacions')
users = set()
sql = """select id_cip_sec, edat
            from nodrizas.assignada_tot"""
for id, edat in u.getAll(sql, 'nodrizas'):
    if edat >= 18:
        users.add(id)

# Ansietat
num = set()
print('tractaments')
cips_fets = set()
tractaments = c.defaultdict()
sql = """select id_cip_sec,
        case when datediff(current_date(),ppfmc_pmc_data_ini) <= 365 then 1 else 0 end,
        ppfmc_pmc_data_ini 
        from import.tractaments
            where PPFMC_atccodi like 'N05BA%'
            and (datediff(ppfmc_data_fi,ppfmc_pmc_data_ini) > 50
            or ppfmc_data_fi is null)"""
for id, actual, inici in u.getAll(sql, 'import'):
    if id in tractaments:
        if tractaments[id] < inici:
            tractaments[id] = inici
    else: tractaments[id] = inici


# ansietat
print('ansietat')
ansietat = c.defaultdict()
sql = """select id_cip_sec, 
        case when datediff(current_date(),pr_dde) <= 365 then 1 else 0 end,
        pr_dde
        from import.problemes
        where pr_cod_ps in
        ('C01-F40.00','C01-F40.10','C01-F40.298',
        'C01-F40.8','C01-F40.9','C01-F41','C01-F41.0',
        'C01-F41.1','C01-F41.3','C01-F41.8','C01-F41.9',
        'F40','F40.0','F40.1','F40.2','F40.8','F40.9',
        'F41','F41.0','F41.1' ,'F41.2' ,'F41.3' ,'F41.8' ,
        'F41.9','F40.3', 'F40.4', 'F40.5', 'F40.6', 'F40.7', 
        'F41.4', 'F41.5', 'F41.6', 'F41.7') and 
        (pr_data_baixa = '' or pr_data_baixa > current_date())
        and (pr_dba = '' or pr_dba > current_date())
        """
for cip, actiu, data in u.getAll(sql, 'nodrizas'):
    if cip in users:
        if cip in ansietat:
            if ansietat[cip] < data:
                ansietat[cip] = data
        else: ansietat[cip] = data

print('ansietat', len(ansietat))

tractats = 0
upload = []
for id in ansietat:
    if id in tractaments:
        upload.append((id, ansietat[id], tractaments[id]))
    else:
        upload.append((id, ansietat[id], None))

u.listToTable(upload, 'peticio_ansietat', 'test')

print('amb ansietat tractats', tractats)
