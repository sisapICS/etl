from collections import Counter
import collections as c
import sisapUtils as u
import sisaptools as t
import datetime as d
from dateutil.relativedelta import relativedelta

## EXTRACCIO DE DADES

class Extraccio():
    def __init__(self):
        self.get_cims()
        self.assignades()
        self.get_incidencies()
        self.get_assignada()
        self.upload()
        self.medea()
        self.centres_ics()

    def get_cims(self):
        print('cims')
        self.cims = {}
        sql = """select criteri_codi, agrupador from nodrizas.eqa_criteris
                where agrupador in (1, 21, 18, 55, 62, 58, 239)"""
        for cim, agrupador in u.getAll(sql, 'nodrizas'):
            self.cims[cim] = str(agrupador)
        
        sql = "select col3, col11 from sisap_cat_ccs \
                where col12 = '02' and col10 < '00042'"
        for codi, ps in t.Database("redics", "data").get_all(sql):
            self.cims[codi] = ps

    def assignades(self):
        print('ass_historica')
        self.assignada = c.defaultdict(dict)
        sql = """select id_cip, dataany, up from import.assignadahistorica"""
        for id, any, up in u.getAll(sql, 'import'):
            self.assignada[str(any)][id] = up
        
        print('ass_actual')
        self.abs = dict()
        sql = """select id_cip, ass_codi_up, usua_abs_codi_abs from import.assignada"""
        for id, up, abs in u.getAll(sql, 'import'):
            self.assignada['2022'][id] = up
            self.abs[id] = abs


    def get_incidencies(self):
        print('incidencies')
        self.ps = dict()
        sql = """select id_cip, pr_cod_ps, pr_dde, year(pr_dde), pr_up from import.problemes 
                where year(pr_dde) >= 2014 AND pr_cod_o_ps = 'C' AND
                pr_data_baixa = 0 AND pr_hist = '1'"""
        for id, cim, data, any, up in u.getAll(sql,'import'):
            if cim in self.cims:
                any = int(any)
                if up == '':
                    while any >= 2014 and up == '':
                        if id in self.assignada[str(any)]:
                            up = self.assignada[str(any)][id]
                        any-= 1
                
                if up == '':
                    if id in self.abs:
                        up = 'A' + str(self.abs[id])

                if (id, self.cims[cim]) in self.ps:
                    if self.ps[(id, self.cims[cim])][0] > data:
                        self.ps[(id, self.cims[cim])] = (data, up)
                else:
                    self.ps[(id, self.cims[cim])] = (data, up)


    def get_assignada(self):
        print('assignada')
        self.poblacio = {}
        sql = """select id_cip, usua_data_naixement, usua_sexe from import.assignada"""
        for id, naix, sexe in u.getAll(sql, 'import'):
            self.poblacio[id] = (naix, sexe)

    def upload(self):
        print('upload')
        counting = Counter()
        for (id, ps), val in self.ps.items():
            date_dx, up = val
            if id in self.poblacio: 
                date_birth, sex = self.poblacio[id][0], self.poblacio[id][1]
                age = int(relativedelta(date_dx, date_birth).years)
                if age > 14:
                    counting[int(date_dx.month), int(date_dx.year), ps, age, sex, up] += 1
        
        upload = []
        for e in counting:
            upload.append((e[0], e[1], e[2], e[3], e[4], e[5], counting[e]))
        
        cols = '(month_dx int, year_dx int, ps int, age int, sex varchar(1), up varchar(5), n int)'
        u.createTable('dx_TFM', cols, 'test', rm=True)
        u.listToTable(upload, 'dx_TFM', 'test')

    def medea(self):
        up_ics = dict()
        up_medea = dict()
        abs_medea = dict()
        sql = """select scs_codi, ics_codi, medea, abs from NODRIZAS.CAT_CENTRES"""
        for scs, ics, medea, abs in u.getAll(sql, 'nodrizas'):
            up_ics[scs] = ics
            if medea[-1] != 'R':
                up_medea[scs] = medea
                abs = 'A' + str(abs)
                abs_medea[abs] = medea
            else: up_medea[scs] = 'R'
            
        upload = []
        sql = """select MONTH_DX, year_dx, ps, age, sex, up, n 
                from test.dx_TFM"""
        for month, year, ps, age, sex, up, n in u.getAll(sql, 'test'):
            if up in up_medea:
                medea = up_medea[up]
            elif up in abs_medea:
                medea = abs_medea[up]
            else: medea = None
            if up in up_ics:
                up = up_ics[up]
            else: up = None
            data = d.date(year, month, 1)
            upload.append((data, month, year, ps, age, sex, up, medea, n))
        cols = '(data date, month_dx int, year_dx int, ps int, age int, sex varchar(1), up varchar(5), medea varchar(5), n int)'
        u.createTable('dx_TFM_medea', cols, 'test', rm=True)
        u.listToTable(upload, 'dx_TFM_medea', 'test')
    
    def centres_ics(self):
        cols = '(data date, month_dx int, year_dx int, ps int, age int, sex varchar(1), up varchar(5), medea varchar(5), n int, ics int)'
        u.createTable('dx_TFM_medea_ics', cols, 'test', rm=True)
        ics = set()
        sql = """select ics_codi from nodrizas.cat_centres where ep = '0208'"""
        for ics_codi, in u.getAll(sql, 'nodrizas'):
            ics.add(ics_codi)

        upload = []
        sql = """select data, month_dx, year_dx, ps, age, sex, up, medea, n 
                from test.dx_tfm_medea dtm """
        for data, month_dx, year_dx, ps, age, sex, up, medea, n in u.getAll(sql, 'nodrizas'):
            if up in ics:
                i = 1
            else: i= 0
            upload.append((data, month_dx, year_dx, ps, age, sex, up, medea, n, i))
        u.listToTable(upload, 'dx_TFM_medea_ics', 'test')


if __name__ == "__main__":
    Extraccio()