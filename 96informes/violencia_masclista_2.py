# -*- coding: latin1 -*-

import sisapUtils as u
import time
import collections as c
from datetime import *

# get_centres_ass

centres_ass = dict()

sql = """
        SELECT
            eap AS up,
            up AS up_assir
        FROM
            assir.cat_assignacio ca
      """
for up, up_assir in u.getAll(sql, "nodrizas"):
    centres_ass[up] = up_assir

sql = """
        SELECT
            up,
            up_assir
        FROM
            nodrizas.ass_centres
      """
for up, up_assir in u.getAll(sql, "nodrizas"):
    centres_ass[up] = up_assir
print("get_centres_ass")


# get_centres_ambits

centres_ambits = dict()

sql = """
        SELECT
            scs_codi,
            amb_desc
        FROM
            nodrizas.cat_centres	
      """
for up, ambit in u.getAll(sql, "nodrizas"):
    centres_ambits[up] = ambit

print("get_centres_ambits")


# get_centres_desc

centres_desc = dict()
sql = """
        select scs_codi, ics_desc from cat_centres
        union
        select scs_codi, ics_desc from urg_centres
        union
        select up_assir, assir from ass_centres
      """ 
for up, up_desc in u.getAll(sql, 'nodrizas'):
    centres_desc[up] = up_desc

centres_assir_desc = dict()
sql = """select up, nom from cat_assignacio"""
for up, up_desc in u.getAll(sql, 'assir'):
    centres_assir_desc[up] = up_desc


# get_poblacio_dates_naixement

poblacio_dates_naixement = dict()

sql = """
		SELECT
			id_cip_sec,
			usua_data_naixement
		FROM
			import.assignada
        WHERE
	        usua_sexe = 'D'
      """
for id_cip_sec, data_naixement in u.getAll(sql, "nodrizas"):
    poblacio_dates_naixement[id_cip_sec] = data_naixement

print("get_poblacio_dates_naixement")


# get_nod_poblacio_ups

nod_poblacio_ups = dict()

sql = """
		SELECT
			id_cip_sec,
			up
		FROM
			nodrizas.assignada_tot
        WHERE
	        sexe = 'D'
      """
for id_cip_sec, up in u.getAll(sql, "nodrizas"):
    nod_poblacio_ups[id_cip_sec] = up

print("get_nod_poblacio_ups")


# get_embarassos_puerperis

embarassades_puerperes = dict()

sql = """
		SELECT
			id_cip_sec,
            puerperi,
			inici AS inici_embaras,
			fi AS fi_embaras
		FROM
			ass_embaras
        HAVING
            fi_embaras >= '2022-09-03'
      """
for id_cip_sec, puerperi, inici_embaras, fi_embaras in u.getAll(sql, "nodrizas"):
    fi = fi_embaras + timedelta(days=120) if puerperi else fi_embaras
    embarassades_puerperes[id_cip_sec] = {"inici": inici_embaras, "fi": fi}

print("get_embarassos_puerperis")


# get_questionaris_violencia_masclista

questionaris_violencia_masclista_counter = c.Counter()
questionaris_violencia_masclista_list = list()
quest_viogen_pacient = list()
quest_rvd_pacient = list()

sql = """
		SELECT
			id_cip_sec,
			vu_dat_act, 
			vu_up,
			vu_val
		FROM
			import.variables2
		-- WHERE vu_cod_vs IN ('EP3001', 'VP3001')
		WHERE vu_cod_vs = 'EP3001'
		AND vu_dat_act BETWEEN '2023-01-01' AND '2023-12-31'
      """
for id_cip_sec, data_questionari, up_questionari, resultat_questionari in u.getAll(sql, "nodrizas"):
    if id_cip_sec in nod_poblacio_ups:
        up_pacient = nod_poblacio_ups[id_cip_sec]
        up_desc_pacient = centres_desc[up_pacient] if up_pacient in centres_desc else None
        up_assir_pacient = centres_ass[up_pacient] if up_pacient in centres_ass else None
        up_assir_desc_pacient = centres_assir_desc[up_assir_pacient] if up_assir_pacient in centres_assir_desc else None            
    else:
        up_pacient = None
        up_desc_pacient = None
        up_assir_pacient = None
        up_assir_desc_pacient = None
    if up_pacient is None:
        ambit_pacient = 'ND'
    else:
        ambit_pacient = centres_ambits[up_pacient] if up_pacient in centres_ambits else 'ND'
    up_desc_questionari = centres_desc[up_questionari] if up_questionari in centres_desc else None
    up_assir_questionari = centres_ass[up_questionari] if up_questionari in centres_ass else None
    up_assir_desc_questionari = centres_assir_desc[up_assir_questionari] if up_assir_questionari in centres_assir_desc else None
    ambit_questionari = centres_ambits[up_questionari] if up_questionari in centres_ambits else 'ND'
    if id_cip_sec in poblacio_dates_naixement:
        if u.yearsBetween(poblacio_dates_naixement[id_cip_sec], data_questionari) >= 14:
            quest_viogen_pacient.append((id_cip_sec, data_questionari, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_questionari, up_desc_questionari, up_assir_questionari, up_assir_desc_questionari, ambit_questionari, resultat_questionari))
            if id_cip_sec in embarassades_puerperes:
                if embarassades_puerperes[id_cip_sec]["inici"] <= data_questionari <= embarassades_puerperes[id_cip_sec]["fi"]:
                    questionaris_violencia_masclista_counter[1, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_questionari, up_desc_questionari, up_assir_questionari, up_assir_desc_questionari, ambit_questionari, resultat_questionari] += 1
                else:
                    questionaris_violencia_masclista_counter[0, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_questionari, up_desc_questionari, up_assir_questionari, up_assir_desc_questionari, ambit_questionari, resultat_questionari] += 1
            else:
                questionaris_violencia_masclista_counter[0, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_questionari, up_desc_questionari, up_assir_questionari, up_assir_desc_questionari, ambit_questionari, resultat_questionari] += 1

for (emb_pue, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_questionari, up_desc_questionari, up_assir_questionari, up_assir_desc_questionari, ambit_questionari, resultat_questionari), n in questionaris_violencia_masclista_counter.items():
    questionaris_violencia_masclista_list.append((emb_pue, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_questionari, up_desc_questionari, up_assir_questionari, up_assir_desc_questionari, ambit_questionari, resultat_questionari, n))

print("get_questionaris_violencia_masclista")

# get_questionaris_rvd

questionaris_rvd_counter = c.Counter()
questionaris_rvd_list = list()
quest_rvd_pacient = list()

sql = """
		SELECT
			id_cip_sec,
			vu_dat_act, 
			vu_up,
			vu_val
		FROM
			import.variables2
		-- WHERE vu_cod_vs IN ('EP3001', 'VP3001')
		WHERE vu_cod_vs = 'VP3001'
		AND vu_dat_act BETWEEN '2023-01-01' AND '2023-12-31'
      """
for id_cip_sec, data_questionari, up_questionari, resultat_questionari in u.getAll(sql, "nodrizas"):
    if id_cip_sec in nod_poblacio_ups:
        up_pacient = nod_poblacio_ups[id_cip_sec]
        up_desc_pacient = centres_desc[up_pacient] if up_pacient in centres_desc else None
        up_assir_pacient = centres_ass[up_pacient] if up_pacient in centres_ass else None
        up_assir_desc_pacient = centres_assir_desc[up_assir_pacient] if up_assir_pacient in centres_assir_desc else None            
    else:
        up_pacient = None
        up_desc_pacient = None
        up_assir_pacient = None
        up_assir_desc_pacient = None
    if up_pacient is None:
        ambit_pacient = 'ND'
    else:
        ambit_pacient = centres_ambits[up_pacient] if up_pacient in centres_ambits else 'ND'
    up_desc_questionari = centres_desc[up_questionari] if up_questionari in centres_desc else None
    up_assir_questionari = centres_ass[up_questionari] if up_questionari in centres_ass else None
    up_assir_desc_questionari = centres_assir_desc[up_assir_questionari] if up_assir_questionari in centres_assir_desc else None
    ambit_questionari = centres_ambits[up_questionari] if up_questionari in centres_ambits else 'ND'
    if id_cip_sec in poblacio_dates_naixement:
        if u.yearsBetween(poblacio_dates_naixement[id_cip_sec], data_questionari) >= 14:
            quest_rvd_pacient.append((id_cip_sec, data_questionari, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_questionari, up_desc_questionari, up_assir_questionari, up_assir_desc_questionari, ambit_questionari, resultat_questionari))
            if id_cip_sec in embarassades_puerperes:
                if embarassades_puerperes[id_cip_sec]["inici"] <= data_questionari <= embarassades_puerperes[id_cip_sec]["fi"]:
                    questionaris_rvd_counter[1, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_questionari, up_desc_questionari, up_assir_questionari, up_assir_desc_questionari, ambit_questionari, resultat_questionari] += 1
                else:
                    questionaris_rvd_counter[0, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_questionari, up_desc_questionari, up_assir_questionari, up_assir_desc_questionari, ambit_questionari, resultat_questionari] += 1
            else:
                questionaris_rvd_counter[0, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_questionari, up_desc_questionari, up_assir_questionari, up_assir_desc_questionari, ambit_questionari, resultat_questionari] += 1

for (emb_pue, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_questionari, up_desc_questionari, up_assir_questionari, up_assir_desc_questionari, ambit_questionari, resultat_questionari), n in questionaris_rvd_counter.items():
    questionaris_rvd_list.append((emb_pue, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_questionari, up_desc_questionari, up_assir_questionari, up_assir_desc_questionari, ambit_questionari, resultat_questionari, n))

print("get_questionaris_rvd")

# get_diagnostics_vg

diagnostics_vg = ['T76.01', 'T76.11', 'T76.21', 'T76.31', 'T76.51', 'T76.61', 'T76.91', 'T74.01', 'T74.11', 'T74.21', 'T74.31', 
                  'T74.51', 'T74.61', 'T74.91', 'C01-T76.01', 'C01-T76.11', 'C01-T76.21', 'C01-T76.31', 'C01-T76.51', 'C01-T76.61', 
                  'C01-T76.91', 'C01-T74.01', 'C01-T74.11', 'C01-T74.21', 'C01-T74.31', 'C01-T74.51', 'C01-T74.61', 'C01-T74.91']

def get_diagnostics_vg_sector(sector):

    dx_vg_sector_counter = c.Counter()
    dx_vg_sector_list = list()
    dx_viogen_sector_pacient = list()

    query_diagnostics_vg = " "
    for diagnostic_vg in diagnostics_vg:
        query_diagnostics_vg += "OR pr_cod_ps LIKE '{}%'".format(diagnostic_vg)

    sql = """
            SELECT
                id_cip_sec,
                pr_dde,
                pr_up,
                pr_cod_ps
            FROM
                import.problemes_s{}
            WHERE
                ({})
                AND pr_dde BETWEEN '2023-01-01' AND '2023-12-31'
            """.format(sector, query_diagnostics_vg[3:])

    for id_cip_sec, data_dx, up_dx, codi_dx in u.getAll(sql, 'import'):
        if id_cip_sec in nod_poblacio_ups:
            up_pacient = nod_poblacio_ups[id_cip_sec]
            up_desc_pacient = centres_desc[up_pacient] if up_pacient in centres_desc else None
            up_assir_pacient = centres_ass[up_pacient] if up_pacient in centres_ass else None
            up_assir_desc_pacient = centres_assir_desc[up_assir_pacient] if up_assir_pacient in centres_assir_desc else None            
        else:
            up_pacient = None
            up_desc_pacient = None
            up_assir_pacient = None
            up_assir_desc_pacient = None
        if up_pacient is None:
            ambit_pacient = 'ND'
        else:
            ambit_pacient = centres_ambits[up_pacient] if up_pacient in centres_ambits else 'ND'
        if up_dx == '':
            if id_cip_sec in nod_poblacio_ups:
                up_dx = nod_poblacio_ups[id_cip_sec]
            else:
                up_dx = None
        if up_dx is not None:
            up_desc_dx = centres_desc[up_dx] if up_dx in centres_desc else None
            up_assir_dx = centres_ass[up_dx] if up_dx in centres_ass else None
            up_assir_desc_dx = centres_assir_desc[up_assir_dx] if up_assir_dx in centres_assir_desc else None
        else:
            up_desc_dx = None
            up_assir_dx = None
            up_assir_desc_dx = None
        if up_dx is None:
            ambit_dx = 'ND'
        else:
            ambit_dx = centres_ambits[up_dx] if up_dx in centres_ambits else 'ND'
        if id_cip_sec in poblacio_dates_naixement:
            if u.yearsBetween(poblacio_dates_naixement[id_cip_sec], data_dx) >= 14:
                dx_viogen_pacient.append((id_cip_sec, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_dx, up_desc_dx, up_assir_dx, up_assir_desc_dx, ambit_dx, codi_dx))
                dx_vg_sector_counter[(up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_dx, up_desc_dx, up_assir_dx, up_assir_desc_dx, ambit_dx, codi_dx)] += 1
    for (up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_dx, up_desc_dx, up_assir_dx, up_assir_desc_dx, ambit_dx, codi_dx), n in dx_vg_sector_counter.items():
        dx_vg_sector_list.append((up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, ambit_pacient, up_dx, up_desc_dx, up_assir_dx, up_assir_desc_dx, ambit_dx, codi_dx, n))

    return dx_vg_sector_list, dx_viogen_sector_pacient

dx_vg_total_list = []
dx_viogen_pacient = []
for sector in u.sectors:
    dx_vg_sector_list, dx_viogen_sector_pacient = get_diagnostics_vg_sector(sector)
    dx_vg_total_list.extend(dx_vg_sector_list)
    dx_viogen_pacient.extend(dx_viogen_sector_pacient)

print("get_diagnostics_vg")


# create_tables

tb_1 = "questionaris_violencia_masclista"
columns_1 = "(embarassada_puerpera int, up_pacient varchar(5), des_up_pacient varchar(100), up_assir_pacient varchar(5), des_up_assir_pacient varchar(100), ambit_desc_pacient varchar(40), up_questionari varchar(5), des_up_questionari varchar(100), up_assir_questionari varchar(5), des_up_assir_questionari varchar(100), ambit_desc_questionari varchar(40), resultat_questionari int, n int)"
tb_2 = "diagnostics_violencia_masclista"
columns_2 = "(up_pacient varchar(5), des_up_pacient varchar(100), up_assir_pacient varchar(5), des_up_assir_pacient varchar(100), ambit_desc_pacient varchar(40), up_dx varchar(5), des_up_dx varchar(100), up_assir_dx varchar(5), des_up_assir_dx varchar(100), ambit_desc_dx varchar(40), codi_dx varchar(50), n int)"
db = "test"
tb_quest_viogen_pacient = "quest_viogen_pacient"
cols_quest_viogen_pacient = "(id_cip_sec int, data date, up_pacient varchar(5), des_up_pacient varchar(100), up_assir_pacient varchar(5), des_up_assir_pacient varchar(100), ambit_desc_pacient varchar(40), up_questionari varchar(5), des_up_questionari varchar(100), up_assir_questionari varchar(5), des_up_assir_questionari varchar(100), ambit_desc_questionari varchar(40), resultat_questionari int)"
tb_quest_rvd_pacient = "quest_rvd_pacient"
cols_quest_rvd_pacient = "(id_cip_sec int, data date, up_pacient varchar(5), des_up_pacient varchar(100), up_assir_pacient varchar(5), des_up_assir_pacient varchar(100), ambit_desc_pacient varchar(40), up_questionari varchar(5), des_up_questionari varchar(100), up_assir_questionari varchar(5), des_up_assir_questionari varchar(100), ambit_desc_questionari varchar(40), resultat_questionari int)"
tb_dx_viogen_pacient = "dx_viogen_pacient"
cols_dx_viogen_pacient = "(id_cip int(11), up_pacient varchar(5), des_up_pacient varchar(100), up_assir_pacient varchar(5), des_up_assir_pacient varchar(100), ambit_desc_pacient varchar(40), up_dx varchar(5), des_up_dx varchar(100), up_assir_dx varchar(5), des_up_assir_dx varchar(100), ambit_desc_dx varchar(40), codi_dx varchar(50))"

u.createTable(tb_1, columns_1, db, rm=True)
u.createTable(tb_2, columns_2, db, rm=True)
u.createTable(tb_quest_viogen_pacient, cols_quest_viogen_pacient, db, rm=True)
u.createTable(tb_quest_rvd_pacient, cols_quest_rvd_pacient, db, rm=True)
u.createTable(tb_dx_viogen_pacient, cols_dx_viogen_pacient, db, rm=True)

u.listToTable(questionaris_violencia_masclista_list, tb_1, db)
u.listToTable(dx_vg_total_list, tb_2, db)
u.listToTable(quest_viogen_pacient, tb_quest_viogen_pacient, db)
u.listToTable(quest_rvd_pacient, tb_quest_rvd_pacient, db)
u.listToTable(dx_viogen_pacient, tb_dx_viogen_pacient, db)


# u.createTable(tb_2, columns_2, db, rm=True)

# u.listToTable(dx_vg_total_list, tb_2, db)

# UNA ALTRA FORMA DE FER LA PETICI� �NICAMENT AMB LA TAULA DE diagnostics_violencia_masclista(2)



# get_diagnostics_vg

diagnostics_vg = ['T74', 'T74.0', 'T74.1', 'T74.2', 'T74.3', 'T74.8', 'T74.9', 'T74.51', 'T74.61', 'T74.91', 'C01-T76.01', 
                  'C01-T76.11', 'C01-T76.21', 'C01-T76.31', 'C01-T76.51', 'C01-T76.61', 'C01-T76.91', 'C01-T74.01', 
                  'C01-T74.11', 'C01-T74.21', 'C01-T74.31', 'C01-T74.51', 'C01-T74.61', 'C01-T74.91']

def get_diagnostics_vg_sector(sector):

    dx_vg_sector_counter = list()
    dx_vg_sector_list = list()
    
    query_diagnostics_vg = " "
    for diagnostic_vg in diagnostics_vg:
        query_diagnostics_vg += "OR pr_cod_ps LIKE '{}%'".format(diagnostic_vg)

    sql = """
            SELECT
                id_cip,
                pr_dde,
                pr_dba,
                pr_up,
                pr_cod_ps
            FROM
                import.problemes_s{}
            WHERE
                ({})
            """.format(sector, query_diagnostics_vg[3:])

    for id_cip, data_dx, data_fi_dx, up_dx, codi_dx in u.getAll(sql, 'import'):
        if id_cip in nod_poblacio_ups:
            up_pacient = nod_poblacio_ups[id_cip]
            up_desc_pacient = centres_desc[up_pacient] if up_pacient in centres_desc else None
            up_assir_pacient = centres_ass[up_pacient] if up_pacient in centres_ass else None
            up_assir_desc_pacient = centres_assir_desc[up_assir_pacient] if up_assir_pacient in centres_assir_desc else None            
        else:
            up_pacient = None
            up_desc_pacient = None
            up_assir_pacient = None
            up_assir_desc_pacient = None
        if up_dx == '':
            if id_cip in nod_poblacio_ups:
                up_dx = nod_poblacio_ups[id_cip]
            else:
                up_dx = None
        if up_dx is not None:
            up_desc_dx = centres_desc[up_dx] if up_dx in centres_desc else None
            up_assir_dx = centres_ass[up_dx] if up_dx in centres_ass else None
            up_assir_desc_dx = centres_assir_desc[up_assir_dx] if up_assir_dx in centres_assir_desc else None
        else:
            up_desc_dx = None
            up_assir_dx = None
            up_assir_desc_dx = None
        if id_cip in poblacio_dates_naixement:
            if u.yearsBetween(poblacio_dates_naixement[id_cip], data_dx) >= 14:
                dx_vg_sector_counter.append((id_cip, data_dx, data_fi_dx, codi_dx, up_pacient, up_desc_pacient, up_assir_pacient, up_assir_desc_pacient, up_dx, up_desc_dx, up_assir_dx, up_assir_desc_dx))

    return dx_vg_sector_counter
    
# dx_vg_total_list = []
# for sector in u.sectors:
#     dx_vg_sector_list = get_diagnostics_vg_sector(sector)
#     dx_vg_total_list.extend(dx_vg_sector_list)

print("get_diagnostics_vg")


# create_tables

tb_2 = "diagnostics_violencia_masclista_2"
columns_2 = "(id_cip int(11), data_dx date, data_fi_dx date, codi_dx varchar(50), up_pacient varchar(5), des_up_pacient varchar(100), up_assir_pacient varchar(5), des_up_assir_pacient varchar(100), up_dx varchar(5), des_up_dx varchar(100), up_assir_dx varchar(5), des_up_assir_dx varchar(100))"
db = "test"

# u.createTable(tb_2, columns_2, db, rm=True)

# u.listToTable(dx_vg_total_list, tb_2, db)