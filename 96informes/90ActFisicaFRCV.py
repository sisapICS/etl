# coding: iso-8859-1
#Angelina,  pafes en frcv, marc17, Ermengol

from sisapUtils import *
import csv,os,sys
from time import strftime
import datetime
from collections import defaultdict,Counter


nod = 'nodrizas'

sql = 'select year(data_ext), data_ext from dextraccio'
for dexta, dextt in getAll(sql, nod):
    dext=dextt
    anya=dexta

centres = {}
sql = "select scs_codi from cat_centres where ep='0208'"
for up, in getAll(sql, nod):
    centres[up] = True

pob = {}
sql = 'select id_cip_sec, edat, up from assignada_tot'
for id, edat, up in getAll(sql, nod):
    if up in centres:
        edat_t = 'missing'
        if edat < 15:
            edat_t = 'Menors de 15'
        elif 15<= edat < 45:
            edat_t = 'Entre 15 i 44 anys'
        elif 45 <= edat < 65:
            edat_t= 'Entre 45 i 64 anys'
        elif edat >64:
            edat_t = 'Majors de 65 anys'
        pob[id] = {'edat': edat_t, 'up': up}
 
vp1101 = {}
EP4012 = {}
CA331 = {}
VA3030 = {}

vp1101_id = defaultdict(list)
EP4012_id = defaultdict(list)
CA331_id = defaultdict(list)
VA3030_id = defaultdict(list)

sql = 'select id_cip_sec, agrupador,  data_var, valor from eqa_variables where agrupador in (240, 243, 242, 241)'
for id, agr,  data, val in getAll(sql, nod):
    if agr == 240:
        vp1101_id[id].append(data)
        vp1101[id, data]= val
    elif agr == 243:
        EP4012_id[id].append(data)
        EP4012[id, data] = val
    elif agr == 242:
        CA331_id[id].append(data)
        CA331[id, data] = val
    elif agr == 241:
        VA3030_id[id].append(data)
        VA3030[id, data] = val
    else:
        print agr
        
anys = list(range(2008, 2017))
      
pafes_ps = {}
sql = "select id_cip_sec from nodrizas.eqa_problemes where ps in ('18','24','55','239','47','267')"
for id, in getAll(sql, nod):
    pafes_ps[id] = True

frcv = Counter()
for id, a in pafes_ps.items():
    if id in pob:
        edat = pob[id]['edat']
        for danys in anys:
            vp = 'missing'
            ep = 'missing'
            ca = 'missing'
            va = 'missing'
            vpdate = datetime.date(1900, 12, 31)
            epdate = datetime.date(1900, 12, 31)
            cadate = datetime.date(1900, 12, 31)
            vadate = datetime.date(1900, 12, 31)
            if id in vp1101_id:
                for dat in vp1101_id[id]:
                    if dat.year == danys:
                        if dat >= vpdate:
                            val = vp1101[id, dat]
                            vp = val
                            vpdate = data
            if id in EP4012_id:
                for dat in EP4012_id[id]:
                    if dat.year == danys:
                        if dat >= epdate:
                            val = EP4012[id, dat]
                            ep = val
                            epdate = data
            if id in CA331_id:
                for dat in CA331_id[id]:
                    if dat.year == danys:
                        if dat >= cadate:
                            val = CA331[id, dat]
                            ca = val
                            cadate = data
            if id in VA3030_id:
                for dat in VA3030_id[id]:
                    if dat.year == danys:
                        if dat >= vadate:
                            val = VA3030[id, dat]
                            va = val
                            vadate = data
            frcv[(danys, edat, vp, ep, ca, va)] += 1
                       

upload = []
for (danys, edat, vp, ep, ca, va), v in frcv.items():
    upload.append([danys, edat, vp, ep, ca, va, v])
file = tempFolder + 'act_fisica_frcv.txt'
writeCSV(file, upload, sep=';')

