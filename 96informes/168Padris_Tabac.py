# coding: iso-8859-1

from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter


tb1 = "sisap_tabac_padris"
db = "redics"
usr = ("PREDUMMP", "PREDUECR", "PREDUPRP", "PREDUMBO")

file = tempFolder + 'padris.tabac.txt'


class Padris(object):
    """."""

    def __init__(self):
        """."""
        self.export_txt()

    def export_txt(self):
        """."""
        export = []
        for (hash, sector, dat, dbaixa, val) in readCSV(file, sep='|'):
            if hash == 'hash':
                continue
            export.append([hash, sector, dat, dbaixa, val]) 
        columns = "(hash varchar2(40), sector varchar2(4), dat varchar2(10), dbaixa varchar2(10), val int)"
        createTable(tb1, columns, db, rm=True)
        listToTable(export, tb1, db)
        grantSelect(tb1, usr, db)

            
if __name__ == "__main__":
    Padris()
    


