# -*- coding: utf8 -*-

"""
Procés per guardar a REDICS-PDP les taules PDP
dels sectors abans que les eliminin.
"""

import sisaptools as u


bk_tb = 'sisap_backup_pdp_{}'
bk_db = ('exadata', 'data')


class Main(object):
    """."""

    def __init__(self):
        """."""
        self.db = u.Database('6734', 'pdp')
        self.desti = u.Database(*bk_db)
        self.get_tables()
        for table in self.tables:
            self.create_table(table)
            print 'taula {} creada'.format(table)
        self.get_pool()

    def get_tables(self):
        """."""
        exclude = ('PDPTB000', 'PDPTB003', 'PDPTB101')
        sql = "select table_name from all_tables where table_name like 'PDP%'"
        self.tables = [table for table, in self.db.get_all(sql)
                       if table not in exclude]

    def create_table(self, table):
        """."""
        columns = [self.db.get_column_information(column, table, desti='ora')['create']
                   for column in self.db.get_table_columns(table)]
        self.desti.create_table(bk_tb.format(table),
                                ['codi_sector_origen varchar2(4)'] + columns,
                                remove=True)

    def get_pool(self):
        """."""
        sectors = ('6102', '6209', '6211', '6310', '6416', '6519', '6520',
                   '6521', '6522', '6523', '6625', '6626', '6627', '6728',
                   '6731', '6734', '6735', '6837', '6838', '6839', '6844')
        pool = u.NoDaemonPool(len(sectors))
        for sector in sectors:
            pool.apply_async(Backup, args=(sector, self.tables))
        pool.close()
        pool.join()


class Backup(object):
    """."""

    def __init__(self, sector, tables):
        """."""
        self.sector = sector
        self.db = u.Database(self.sector, 'pdp')
        self.desti = u.Database(*bk_db)
        for table in tables:
            self.backup_table(table)
        print 'sector {} acabat'.format(sector)

    def backup_table(self, table):
        sql = "select '{}', a.* from {} a".format(self.sector, table)
        data = [row for row in self.db.get_all(sql)]
        self.desti.list_to_table(data, bk_tb.format(table))


if __name__ == '__main__':
    Main()
