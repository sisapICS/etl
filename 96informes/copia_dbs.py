# import sisaptools as u
# import sisapUtils as u2


# table = "dbs_2021"
# dext, = u.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")  # noqa
# anual = "dbs_2021_BCN"


# def worker(letter):
#     sql = """select * from dbs.dbs_2021 where c_sap in ('51','52','53','54') and c_cip like '{}%'""".format(letter)
#     dades = list(u.Database("sidics", "dbs").get_all(sql))
#     with u.Database("redics", "data") as sidics:
#         # sidics.execute("alter session set wait_timeout = 28800")
#         sidics.list_to_table(dades, anual)


# if __name__ == "__main__":
#     with u.Database("sidics", "dbs") as redics:
#         columns = [redics.get_column_information(column, table, desti="ora")["create"]
#                     for column in redics.get_table_columns(table)]
#     u.Database("redics", "data").create_table(anual, list(columns), remove=True)
#     jobs = []
#     digits = [format(digit, 'x').upper() for digit in range(16)]
#     for digit1 in digits:
#         for digit2 in digits:
#             jobs.append(digit1 + digit2)
#     u2.multiprocess(worker, jobs, 32)


import sisaptools as u
import sisapUtils as u2


table = "dbs_2023"
anual = "dbs_2023"


def worker(letter):
    sql = """SELECT HASH_REDICS, HASH_COVID FROM dwsisap.PDPTB101_RELACIO pr where hash_redics like '{}%'""".format(letter)
    conversors = {hash_r: hash_c for (hash_r, hash_c) in u.Database("exadata", "data").get_all(sql)}
    sql = """select * from dbs.dbs_2023 where c_cip like '{}%'""".format(letter)
    dades = []
    for row in u.Database("sidics", "dbs").get_all(sql):
        if row[0] in conversors:
            hash_e = [conversors[row[0]]]
            dades.append((tuple(hash_e) + row[1:]))
    with u.Database("exadata", "data") as exadata:
        exadata.list_to_table(dades, anual)


if __name__ == "__main__":
    with u.Database("sidics", "dbs") as redics:
        columns = [redics.get_column_information(column, table, desti="ora")["create"]
                    for column in redics.get_table_columns(table)]
    u.Database("exadata", "data").create_table(anual, list(columns), remove=True)
    u2.grantSelect(anual, 'DWSISAP_ROL', 'exadata')

    jobs = []
    digits = [format(digit, 'x').upper() for digit in range(16)]
    for digit1 in digits:
        for digit2 in digits:
            jobs.append(digit1 + digit2)
    u2.multiprocess(worker, jobs, 4)