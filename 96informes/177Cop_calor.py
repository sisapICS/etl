"""
Peticio cop de calor. extreure dades de produccio/explotacio
"""
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import datetime

inici = '20180801'
fi = '20180808'

sb = datetime.now().hour < 7

codis = "('T67.0', 'T67.1')"


class CopCalor(object):
    """."""
    
    def __init__(self):
        """."""
        self.dades = Counter()
        self.get_catalegs()
        self.get_centres()
        self.get_cop()
        self.export_data()
        
        
    def get_catalegs(self):
        """."""
        sql = ("select ps_cod, ps_des from cat_prstb001 where ps_cod in {}".format(codis), "import")
        self.cim10 = {(ps_cod): (ps_des) for (ps_cod, ps_des)
                        in getAll(*sql)}
        
        sql = ("select pst_th, pst_des from cat_prstb305", "import")
        self.thes = {(ps_cod): (ps_des) for (ps_cod, ps_des)
                        in getAll(*sql)} 
                                 
    def get_centres(self):
        """."""
        self.territoris = {} 
        sql = "select up.up_codi_up_scs ,br.up_desc_up_ics, amb.amb_desc_amb, sap.dap_desc_dap   from    cat_gcctb007 br \
                inner join cat_gcctb008 up on br.up_codi_up_ics=up.up_codi_up_ics \
                inner join cat_gcctb006 sap on br.dap_codi_dap=sap.dap_codi_dap \
                inner join cat_gcctb005 amb on sap.amb_codi_amb=amb.amb_codi_amb \
                where e_p_cod_ep='0208'"
        for up, desc, territori, sap in getAll(sql, "import"):
            self.territoris[up] = {'ambit':territori, 'sap': sap, 'desc': desc }
                   
    def get_cop(self):
        """."""

        for sector in sectors:
            if sb:
                sector += "sb"
            sql= "select pr_cod_u, pr_cod_ps, pr_dde, pr_th, pr_up, pr_dba from prstb015 \
                where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_data_baixa is null \
                and pr_cod_ps in {} and to_char(pr_dde, 'YYYYMMDD') between '{}' and '{}'".format(codis, inici, fi)
            for cip, ps, dde, th, up, dba in getAll(sql, sector):
                if up in self.territoris:
                    desc = self.territoris[up]['desc']
                    ambit = self.territoris[up]['ambit']
                    descps = self.cim10[ps]
                    descth = self.thes[th] if th in self.thes else None
                    self.dades[(dde, ps, descps, th, descth,ambit)] += 1

        
    def export_data(self):
        """."""
        upload = [] 
        for (dde, ps, descps, th, descth,ambit),d in self.dades.items():
            upload.append([dde, ps, descps, th, descth,ambit, d])
        writeCSV(tempFolder + 'Cop_calor.txt', upload)

        
if __name__ == '__main__':
    CopCalor()