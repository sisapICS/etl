from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *

printTime('inici')
# dep_vacsist
# 'VAC046': {'desc': 'T 1 dosi despres 6 anys  6 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
# 'VAC047': {'desc': 'T 1 dosi despres 6 anys  7 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
# 'VAC164': {'desc': 'VPI 1 dosi a partir dels 6 anys en infants de sis anys', 'vacuna': 'P', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
# 'VAC165': {'desc': 'VPI 1 dosi a partir dels 6 anys en infants de set anys', 'vacuna': 'P', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
# 'VAC048': {'desc': 'T 1 dosi despres 12 anys  14 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 144, 'tip': 'despres'},
# 'VAC068': {'desc': 'T 1 dosi despres 12 anys  15 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 144, 'tip': 'despres'},
# 'VAC042': {'desc': 'VVZ 2 dosis despres 10 anys de vida  12 anys', 'vacuna': 'VVZ', 'dosis': 2, 'edatd': 120, 'tip': 'despres'},
# 'VAC043': {'desc': 'VVZ 2 dosis despres 10 anys de vida  13 anys', 'vacuna': 'VVZ', 'dosis': 2, 'edatd': 120, 'tip': 'despres'},
# 'VAC038': {'desc': 'MCC 1 dosi despres primer any de vida  1 any', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
# 'VAC039': {'desc': 'MCC 1 dosi despres primer any de vida  2 anys', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
# 'VAC168': {'desc': 'MACYW 1 dosi despres 10 anys de vida 1 any', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
# 'VAC169': {'desc': 'MACYW 1 dosi despres de l any de vida als 2 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
# 'VAC132': {'desc': 'MACYW 1 dosi despres dels 12 anys en 13 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 144, 'tip': 'despres'},
# 'VAC133': {'desc': 'MACYW 1 dosi despres dels 12 anys en 14 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 144, 'tip': 'despres'},
# 'VAC134': {'desc': 'MACYW 1 dosi despres dels 14 anys en 15 - 18 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 168, 'tip': 'despres'},
# 'VAC171': {'desc': 'HA 1 dosi a partir dels 10 anys als 12 anys', 'vacuna': 'HA', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
# 'VAC172': {'desc': 'HA 1 dosi a partir dels 10 anys als 13 anys', 'vacuna': 'HA', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
# 'VAC101': {'desc': 'Meningitis B 2 dosis > 11 anys en 11 - 14 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'despres'},
# 'VAC058': {'desc': 'DT 5 dosis als 16 anys', 'vacuna': 'T', 'dosis': 5},
# 'VAC059': {'desc': 'Pertussis 4 dosis als 16 anys', 'vacuna': 'TF', 'dosis': 4},
# 'VAC060': {'desc': 'Polio 4 dosis als 16 anys', 'vacuna': 'P', 'dosis': 4},
# 'VAC061': {'desc': 'Hib 3 dosis als 16 anys', 'vacuna': 'Hib', 'dosis': 3},
# 'VAC062': {'desc': 'HB 3 dosis als 16 anys', 'vacuna': 'HB', 'dosis': 3},
# 'VAC063': {'desc': 'VNC 3 dosis als 16 anys', 'vacuna': 'VNC', 'dosis': 3},
# 'VAC064': {'desc': 'XRP 2 dosis als 16 anys', 'vacuna': 'XRP', 'dosis': 2},
# 'VAC065': {'desc': 'VVZ 2 dosis als 16 anys', 'vacuna': 'VVZ', 'dosis': 2},
# 'VAC066': {'desc': 'VPH 2 dosis als 16 anys', 'vacuna': 'VPH', 'dosis': 2, 'sexe':'D'},


nod = "nodrizas"
imp = "import"
db = 'depart'
                
mes, dext = getOne('select month(data_ext), data_ext from dextraccio', nod)

antigens_agrupadors = {'HB': 15,
                       'HA': 34,
                       'XRP': 38,
                       'VNC23': 48,
                       'T': 49,
                       'Grip': 99,
                       'P': 311,
                       'Hib': 312,
                       'MCC': 313,
                       'VVZ': 314,
                       'MeB': 548,
                       'TF': 631,
                       'VNC': 698,
                       'VPH': 782,
                       'R': 790,
                       'MACYW': 883,
                      }   


criteris_indicadors = {
    2007:{
        'VAC058': {'desc': 'DT 5 dosis als 16 anys', 'vacuna': 'T', 'dosis': 5},
        'VAC059': {'desc': 'Pertussis 4 dosis als 16 anys', 'vacuna': 'TF', 'dosis': 4},
        'VAC060': {'desc': 'Polio 4 dosis als 16 anys', 'vacuna': 'P', 'dosis': 4},
        'VAC061': {'desc': 'Hib 3 dosis als 16 anys', 'vacuna': 'Hib', 'dosis': 3},
        'VAC062': {'desc': 'HB 3 dosis als 16 anys', 'vacuna': 'HB', 'dosis': 3},
        'VAC063': {'desc': 'VNC 3 dosis als 16 anys', 'vacuna': 'VNC', 'dosis': 3},
        'VAC064': {'desc': 'XRP 2 dosis als 16 anys', 'vacuna': 'XRP', 'dosis': 2},
        'VAC065': {'desc': 'VVZ 2 dosis als 16 anys', 'vacuna': 'VVZ', 'dosis': 2},
        'VAC066': {'desc': 'VPH 2 dosis als 16 anys', 'vacuna': 'VPH', 'dosis': 2, 'sexe':'D'},
    }
}
criteris_indicadors_Xedat = {
    2022:{
        'VAC038': {'desc': 'MCC 1 dosi despres primer any de vida  1 any', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
        'VAC168': {'desc': 'MACYW 1 dosi despres 10 anys de vida 1 any', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
    },
    2021:{
        'VAC039': {'desc': 'MCC 1 dosi despres primer any de vida  2 anys', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
        'VAC169': {'desc': 'MACYW 1 dosi despres de l any de vida als 2 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 12, 'tip': 'despres'},
        'VAC097': {'desc': 'Meningitis B 2 dosis < 2 anys en 1 i 2 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'abans'},
        'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
    },
    2020:{
        'VAC098': {'desc': 'Meningitis B 2 dosis < 2 anys en 2 i 3 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'abans'},
        'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
    },
    2019:{
        'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
    },
    2018:{
        'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
    },
    2017:{
        'VAC046': {'desc': 'T 1 dosi despres 6 anys  6 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
        'VAC164': {'desc': 'VPI 1 dosi a partir dels 6 anys en infants de sis anys', 'vacuna': 'P', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
        'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
    },
    2016:{
        'VAC047': {'desc': 'T 1 dosi despres 6 anys  7 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
        'VAC165': {'desc': 'VPI 1 dosi a partir dels 6 anys en infants de set anys', 'vacuna': 'P', 'dosis': 1, 'edatd': 72, 'tip': 'despres'},
        'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
    },
    2015:{
        'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
    },
    2014:{
        'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
    },
    2013:{
        'VAC099': {'desc': 'Meningitis B 2 dosis > 2 anys en 2 - 10 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 24, 'tip': 'despres'},
    },
    2012:{
        'VAC101': {'desc': 'Meningitis B 2 dosis > 11 anys en 11 - 14 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'despres'},
    },
    2011:{
        'VAC042': {'desc': 'VVZ 2 dosis despres 10 anys de vida  12 anys', 'vacuna': 'VVZ', 'dosis': 2, 'edatd': 120, 'tip': 'despres'},
        'VAC171': {'desc': 'HA 1 dosi a partir dels 10 anys als 12 anys', 'vacuna': 'HA', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
        'VAC101': {'desc': 'Meningitis B 2 dosis > 11 anys en 11 - 14 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'despres'},
        'VAC132': {'desc': 'MACYW 1 dosi despres dels 12 anys en 13 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
    },
    2010:{
        'VAC043': {'desc': 'VVZ 2 dosis despres 10 anys de vida  13 anys', 'vacuna': 'VVZ', 'dosis': 2, 'edatd': 120, 'tip': 'despres'},
        'VAC133': {'desc': 'MACYW 1 dosi despres dels 12 anys en 14 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
        'VAC172': {'desc': 'HA 1 dosi a partir dels 10 anys als 13 anys', 'vacuna': 'HA', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
        'VAC101': {'desc': 'Meningitis B 2 dosis > 11 anys en 11 - 14 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'despres'},
    },
    2009:{
        'VAC048': {'desc': 'T 1 dosi despres 12 anys  14 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 144, 'tip': 'despres'},
        'VAC101': {'desc': 'Meningitis B 2 dosis > 11 anys en 11 - 14 anys', 'vacuna': 'MeB', 'dosis': 2, 'edatd': 132, 'tip': 'despres'},
    },
    2008:{
        'VAC068': {'desc': 'T 1 dosi despres 12 anys  15 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 144, 'tip': 'despres'},
    },
    2005:{
        'VAC134': {'desc': 'MACYW 1 dosi despres dels 14 anys en 15 - 18 anys', 'vacuna': 'MACYW', 'dosis': 1, 'edatd': 120, 'tip': 'despres'},
    },
    2007: {
        'VAC067': {'desc': 'MCC 1 dosi despres 10 anys  16 anys', 'vacuna': 'MCC', 'dosis': 1, 'edatd': 120, 'tip': 'despres'}
    }
}

# Obtenir els codis de antigens asociats als agrupadors detallats a antigens_agrupadors
antigens, vacunes_relacionades = defaultdict(list), defaultdict(list)
for antigen_desc in antigens_agrupadors:
    agrupador = antigens_agrupadors[antigen_desc]
    sql = """
            SELECT DISTINCT
                criteri_codi
            FROM
                eqa_criteris
            WHERE
                taula IN ('vacunesped', 'vacunes')
                AND agrupador = {}
          """.format(agrupador)
    for codi_antigen, in getAll(sql, nod):
        antigens[antigen_desc].append(codi_antigen)
        antigens[antigen_desc].append('cutre')
    # Obtenir els codis de vacunacio asociats als codis d'antigen ontinguts al pas anterior
    in_crit = tuple(antigens[antigen_desc]) 
    sql = """
            SELECT
                vacuna
            FROM
                cat_prstb040_new
            WHERE
                antigen IN {0}
            """.format(in_crit)
    for codi_vacuna, in getAll(sql, imp):
        vacunes_relacionades[codi_vacuna].append(antigen_desc)
printTime('cataleg')
# Obtenir dades de vacunacio de ped_vacunes
vacunats = {}
sql = """
        SELECT
            id_cip_sec,
            agrupador,
            dosis
        FROM
            ped_vacunes
      """
for id_cip_sec, agrupador, dosi in getAll(sql, nod):
    vacunats[(id_cip_sec, agrupador)] = dosi
printTime('pedia')
# Obtenir dades de vacunacio de eqa_vacunes
sql = """
        SELECT
            id_cip_sec,
            agrupador,
            dosis
        FROM
            eqa_vacunes
      """
for id_cip_sec, agrupador, dosi in getAll(sql, nod):
    vacunats[(id_cip_sec, agrupador)] = dosi
printTime('eqa')
# Obtenir dades i dates de vacunacio d'import.vacunes
dates_vacunes =  defaultdict(list)

for particio in getSubTables("vacunes"):
    sql = """
            SELECT
                id_cip_sec,
                va_u_cod,
                va_u_data_vac
            FROM
                {0}
            WHERE
                va_u_data_baixa = 0
          """.format(particio)
    for id_cip_sec, codi_vacuna, data_vacuna in getAll(sql, imp):  
        if codi_vacuna in vacunes_relacionades:
            for antigen_agrupador in vacunes_relacionades[codi_vacuna]:
                dates_vacunes[(id_cip_sec, antigen_agrupador)].append(data_vacuna)
    printTime(particio)
printTime('vacunes')
# Calcul indicadors
resultatsIndividual = dict()
recompte = Counter()

sql = """
        SELECT
            id_cip_sec,
            up,
            ates,
            year(data_naix),
            sexe,
            data_naix
        FROM
            assignada_tot
        WHERE
            edat < 19
      """
for id_cip_sec, up, ates, edat, sexe, dnaix in getAll(sql, nod):
    if edat in criteris_indicadors:
        for indicador in criteris_indicadors[edat]:
            vac = criteris_indicadors[edat][indicador]['vacuna']
            try:
                sex = criteris_indicadors[edat][indicador]['sexe']
            except KeyError:
                sex = sexe
            if sex == sexe:
                den, num = 1, 0
                dosi = criteris_indicadors[edat][indicador]['dosis']
                desc = criteris_indicadors[edat][indicador]['desc']
                agr_vac = antigens_agrupadors[vac]
                edat = int(edat)
                if ates == 1:
                    recompte[up, edat, sexConverter(sexe), 'NOINSAT',indicador, 'DEN'] += 1
                if (id_cip_sec, agr_vac) in vacunats:
                    dosiposada = vacunats[(id_cip_sec, agr_vac)]
                    if dosiposada >= dosi:
                        num = 1
                        if ates == 1:
                            recompte[up, edat, sexConverter(sexe), 'NOINSAT',indicador, 'NUM'] += 1
                resultatsIndividual[(id_cip_sec, indicador)] = {'ates': ates, 'desc': desc, 'up': up, 'den': den, 'num': num}
    if edat in criteris_indicadors_Xedat:
        for indicador in criteris_indicadors_Xedat[edat]:
            edat = int(edat)
            den, num = 1, 0
            vac = criteris_indicadors_Xedat[edat][indicador]['vacuna']
            dosi = criteris_indicadors_Xedat[edat][indicador]['dosis']
            desc = criteris_indicadors_Xedat[edat][indicador]['desc']
            edatDosi = criteris_indicadors_Xedat[edat][indicador]['edatd']
            tip = criteris_indicadors_Xedat[edat][indicador]['tip']
            if ates == 1:
                recompte[up, edat, sexConverter(sexe), 'NOINSAT',indicador, 'DEN'] += 1
            if (id_cip_sec, vac) in dates_vacunes:
                ok = 0
                for data in dates_vacunes[(id_cip_sec, vac)]:
                    edat_m = monthsBetween(dnaix, data)
                    if edat_m >= edatDosi:
                        ok += 1
                if ok >= dosi:
                    num = 1
                    if ates == 1:
                        recompte[up, edat, sexConverter(sexe), 'NOINSAT',indicador, 'NUM'] += 1
            resultatsIndividual[(id_cip_sec, indicador)] = {'ates': ates, 'desc': desc, 'up': up, 'den': den, 'num': num}
printTime('cuinetes')
# Exportar dades en format taula
table = 'peticio_dvac'
create = "(up varchar(5) not null default'', edat int, sexe varchar(10), comb varchar(10), indicador varchar(10), tipus varchar(10), recompte int)"
createTable(table,create,db, rm=True)

upload = []
for (up, edat, sexe, comb, indicador, tip), rec in recompte.items():
    indicador = 'D' + indicador
    upload.append([up, edat, sexe, comb, indicador, tip, rec])
listToTable(upload, table, db)
printTime('fi')

# # dep_vacadults
# printTime('inici')

# nod = "nodrizas"
# imp = "import"
# db = 'depart'

# dext, = getOne('select data_ext from dextraccio', nod)

# codis_vacunes = {'T': 49,
#                  'VNC': 698,
#                  'VNC23': 48,
#                  'GRIP': 99,
#                  'VHZ': 920,
#                  'VNC13': 977
#                 }   
                  
# criteris_indicadors_Xedat = {
#     1958: {
#         'VAC177': {'desc': 'VNC23 1 dosi despres dels 60 als 65 anys', 'vacuna': 'VNC23', 'dosis': 1, 'edatd': 60, 'tip': 'despres'},
#         'VAC179': {'desc': 'VNC13 1 dosi despres dels 60 als 65 anys', 'vacuna': 'VNC13', 'dosis': 1, 'edatd': 60, 'tip': 'despres'},
#         'VAC181': {'desc': 'Td 1 dosi a partir dels 60 als 65 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 60, 'tip': 'despres'},
#         'VAC152': {'desc': 'VHZ dues dosis als 65 anys', 'vacuna': 'VHZ', 'dosis': 2},
#     },
#     1957: {
#         'VAC178': {'desc': 'VNC23 1 dosi despres dels 60 als 66 anys', 'vacuna': 'VNC23', 'dosis': 1, 'edatd': 60, 'tip': 'despres'},
#         'VAC180': {'desc': 'VNC13 1 dosi despres dels 60 als 66 anys', 'vacuna': 'VNC13', 'dosis': 1, 'edatd': 60, 'tip': 'despres'},
#         'VAC182': {'desc': 'Td 1 dosi a partir dels 60 als 66 anys', 'vacuna': 'T', 'dosis': 1, 'edatd': 60, 'tip': 'despres'},
#     },
#     1943: {
#         'VAC155': {'desc': 'VHZ dues dosis als 80 anys', 'vacuna': 'VHZ', 'dosis': 2}
#     },
#     (1910,1958): {
#         'VAC184': {'desc': 'VHZ dues dosis en majors de 64 anys', 'vacuna': 'VHZ', 'dosis': 2}
#     }
# }

# # Obtenir els codis de antigens asociats als agrupadors detallats a antigens_agrupadors
# antigens, vacunes_relacionades = defaultdict(list), {}
# for antigen_desc in codis_vacunes:
#     agrupador = codis_vacunes[antigen_desc]
#     sql = """
#             SELECT DISTINCT 
#                 criteri_codi
#             FROM
#                 eqa_criteris
#             WHERE
#                 taula IN ('vacunesped', 'vacunes')
#                 AND agrupador = {}
#           """.format(agrupador)
#     for codi_antigen, in getAll(sql, nod):
#         antigens[antigen_desc].append(codi_antigen)
#         antigens[antigen_desc].append('cutre')
#     # Obtenir els codis de vacunacio asociats als codis d'antigen ontinguts al pas anterior
#     in_crit = tuple(antigens[antigen_desc]) 
#     sql = """
#             SELECT
#                 vacuna
#             FROM
#                 cat_prstb040_new
#             WHERE
#                 antigen IN {0}
#             """.format(in_crit)
#     for codi_vacuna, in getAll(sql, imp):
#         vacunes_relacionades[codi_vacuna] = antigen_desc
# printTime('cataleg')
# # Obtenir dades i dates de vacunacio d'import.vacunes
# dates_vacunes =  defaultdict(list)

# for particio in getSubTables("vacunes"):
#     sql = """
#             SELECT
#                 id_cip_sec,
#                 va_u_cod,
#                 va_u_data_vac
#             FROM
#                 {0}
#             WHERE
#                 va_u_data_baixa = 0
#           """.format(particio)
#     for id_cip_sec, codi_vacuna, data_vacuna in getAll(sql, imp):
#         if codi_vacuna in vacunes_relacionades:
#             antigen_agrupador = vacunes_relacionades[codi_vacuna]
#             dates_vacunes[(id_cip_sec, antigen_agrupador)].append(data_vacuna)
#     printTime(particio)
# printTime('vacunes')
# # Calcul indicadors

# def get_recompte(criteris_indicadors_Xedat, edats_indicador, edat, recompte, up, sexe, id_cip_sec, dates_vacunes, dnaix, ates):
#     if isinstance(edats_indicador, int):
#         edat_indicador_menor = edats_indicador
#         edat_indicador_major = edats_indicador
#     elif isinstance(edats_indicador, tuple):
#         edat_indicador_menor = edats_indicador[0]
#         edat_indicador_major = edats_indicador[1]
#     if edat_indicador_menor <= edat <= edat_indicador_major:
#         for indicador in criteris_indicadors_Xedat[edats_indicador]:
#             antigen_agrupador = criteris_indicadors_Xedat[edats_indicador][indicador]['vacuna']
#             dosi = criteris_indicadors_Xedat[edats_indicador][indicador]['dosis']
#             desc = criteris_indicadors_Xedat[edats_indicador][indicador]['desc']
#             if not ('tip' in criteris_indicadors_Xedat[edats_indicador][indicador] and 'edatd' in criteris_indicadors_Xedat[edats_indicador][indicador]):
#                 if antigen_agrupador == 'GRIP':
#                     if ates == 1:
#                         recompte[up, edat, sexConverter(sexe), 'NOINSAT' , indicador, 'DEN'] += 1
#                     if (id_cip_sec, antigen_agrupador) in dates_vacunes:
#                         ok = 0
#                         for data in dates_vacunes[(id_cip_sec, antigen_agrupador)]:
#                             edat_y = yearsBetween(dnaix, data)
#                             if edat_y > 64:
#                                 if dext.month in (9,10,11,12):
#                                     if data.year == dext.year and data.month in (9,10,11,12):
#                                         ok += 1
#                                 elif dext.month in (1,2,3,4,5,6,7,8):
#                                     if (data.year == dext.year-1 and data.month in (9,10,11,12)) or (data.year == dext.year and data.month in (1,2)):
#                                         ok += 1
#                         if ok >= dosi:
#                             num = 1
#                             if ates == 1:
#                                 recompte[up, edat, sexConverter(sexe), 'NOINSAT' , indicador, 'NUM'] += 1
#                 else:
#                     if ates == 1:
#                         recompte[up, edat, sexConverter(sexe), 'NOINSAT' , indicador, 'DEN'] += 1
#                     if (id_cip_sec, antigen_agrupador) in dates_vacunes:
#                         ok = 0
#                         for data in dates_vacunes[(id_cip_sec, antigen_agrupador)]:
#                             edat_y = yearsBetween(dnaix, data)
#                             if edat_y > 64:
#                                 ok += 1
#                         if ok >= dosi:
#                             num = 1
#                             if ates == 1:
#                                 recompte[up, edat, sexConverter(sexe), 'NOINSAT' , indicador, 'NUM'] += 1
#             else:
#                 edatDosi = criteris_indicadors_Xedat[edats_indicador][indicador]['edatd']
#                 tip = criteris_indicadors_Xedat[edats_indicador][indicador]['tip']
#                 if ates == 1:
#                     recompte[up, edat, sexConverter(sexe), 'NOINSAT', indicador, 'DEN'] += 1
#                 if (id_cip_sec, antigen_agrupador) in dates_vacunes:
#                     ok = 0
#                     for data in dates_vacunes[(id_cip_sec, antigen_agrupador)]:
#                         edat_m = monthsBetween(dnaix, data)
#                         if tip == 'abans':
#                             if edat_m <= edatDosi:
#                                 ok += 1
#                         else:
#                             if edat_m >= edatDosi:
#                                 ok += 1
#                     if ok >= dosi:
#                         num = 1
#                         if ates == 1:
#                             recompte[up, edat, sexConverter(sexe), 'NOINSAT', indicador, 'NUM'] += 1

# recompte = Counter()
# printTime('recompte')
# sql = """
#         SELECT
#             id_cip_sec,
#             up,
#             ates,
#             year(data_naix),
#             sexe,
#             data_naix
#         FROM
#             assignada_tot_with_jail
#         WHERE
#             edat > 64
#       """
# for id_cip_sec, up, ates, edat, sexe, dnaix in getAll(sql, nod):
#     for edats_indicador in criteris_indicadors_Xedat:
#         get_recompte(criteris_indicadors_Xedat, edats_indicador, edat, recompte, up, sexe, id_cip_sec, dates_vacunes, dnaix, ates)
        
# printTime('cuinetes')
# # Exportar dades en format taula
# table = 'peticio_dvac'

# upload = []
# for (up, edat, sexe, comb, indicador, tip), rec in recompte.items():
#     indicador = 'D' + indicador
#     upload.append([up, edat, sexe, comb, indicador, tip , rec])
# listToTable(upload, table, db)
# printTime('fi')



# # dep_cobertura_vacunal
# printTime('inici')
# indicadors = {
#     'DVAC191': {
#     'edat': 1948,
#     'interval': False,
#     'num': [48],
#     'dosis': None,
#     'minedat': 60
#     },
#     'DVAC192': {
#         'edat': 1947,
#         'interval': False,
#         'num': [48],
#         'dosis': None,
#         'minedat': 60
#     },
#     'DVAC189': {
#         'edat': 1948,
#         'interval': False,
#         'num': [977,698,978],
#         'dosis': None,
#         'minedat': 60
#     },
#     'DVAC190': {
#         'edat': 1947,
#         'interval': False,
#         'num': [977,698,978],
#         'dosis': None,
#         'minedat': 60
#     },
#     'DVAC185': {
#         'edat': 1958,
#         'interval': False,
#         'num': [977,698,978,48],
#         'dosis': None,
#         'minedat': 60
#     },
#     'DVAC186': {
#         'edat': 1957,
#         'interval': False,
#         'num': [977,698,978,48],
#         'dosis': None,
#         'minedat': 60
#     },
#     'DVAC187': {
#         'edat': 1948,
#         'interval': False,
#         'num': [977,698,978,48],
#         'dosis': None,
#         'minedat': 60
#     },
#     'DVAC188': {
#         'edat': 1947,
#         'interval': False,
#         'num': [977,698,978,48],
#         'dosis': None,
#         'minedat': 60
#     },
#     'DVAC193': {
#         'edat': 1948,
#         'interval': False,
#         'num': [49,576],
#         'dosis': None,
#         'minedat': 60
#     },
#     'DVAC194': {
#         'edat': 1947,
#         'interval': False,
#         'num': [49,576],
#         'dosis': None,
#         'minedat': 60
#     },
# }

# sql = """
#     SELECT id_cip_sec, up, data_naix, year(data_naix), ates, sexe
#     FROM assignada_tot
# """
# pob = defaultdict()
# for id, up, data, edat, ates, sexe in getAll(sql, 'nodrizas'):
#     pob[id] = {
#         'up': up,
#         'naixement': data,
#         'edat': edat,
#         'ates': ates,
#         'sexe': sexe
#     }
# printTime('assignada')
# vacunes = defaultdict(list)
# grups = {
#     977: 'pn13-15',
#     978: 'pn20',
#     698: 'pn13-15-20',
#     48: 'pn23',
#     49: 'td',
#     576: 'td',
#     920: 'hz'
# }
# sql = """
#     SELECT id_cip_sec, agrupador, maxedat, dosis
#     FROM eqa_vacunes
#     WHERE agrupador in (977, 978, 698, 48, 49, 576, 920)
#     AND dosis > 0
# """
# for id, codi, maxedat, dosis in getAll(sql, 'nodrizas'):
#     vacunes[id].append((codi, maxedat, dosis))
# printTime('eqa')
# dades = defaultdict(set)
# # procediment:
# # - comprovar denominador
# # - comprovar que te vacunes
# # - comprovar edat de les vacunes
# # - comprovar dosis
# for ind, data in indicadors.items():
#     edat_ind = data['edat']
#     interval = data['interval']
#     num = data['num']
#     dosis = data['dosis']
#     minedat = data['minedat']
#     for id in pob:
#         if interval:
#             cond = pob[id]['edat'] >= edat_ind
#         else:
#             cond = pob[id]['edat'] == edat_ind
#         if cond:
#             up = pob[id]['up']
#             ates = pob[id]['ates']
#             sexe = pob[id]['sexe']
#             edat = pob[id]['edat']
#             if ates == 1:
#                 dades[(ind, up, edat, sexConverter(sexe), 'NOINSAT', 'DEN')].add(id)
#             if id in vacunes:
#                 for codi, maxedat, dosis_v in vacunes[id]:
#                     if codi in num and maxedat/12 >= minedat:
#                         if dosis and dosis_v == dosis:
#                             if ates == 1:
#                                 dades[(ind, up, edat, sexConverter(sexe), 'NOINSAT', 'NUM')].add(id)
#                         elif not dosis:
#                             if ates == 1:
#                                 dades[(ind, up, edat, sexConverter(sexe), 'NOINSAT', 'NUM')].add(id)
# printTime('cuinetes')
# upload = []
# for ind, up, edat, sexe, situacio, analisi in dades:
#     upload.append(( up, edat, sexe, situacio, ind, analisi, len(dades[(ind, up, edat, sexe, situacio, analisi)])))
# listToTable(upload, 'peticio_dvac', 'depart')
# printTime('fi')