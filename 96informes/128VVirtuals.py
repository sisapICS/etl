from sisapUtils import *
from os import remove
from zipfile import ZipFile
from os.path import basename
from collections import defaultdict,Counter


imp = 'import'
nod = 'nodrizas'
inici, final = '20170101', '20171231' 

centres = {}

sql = "select scs_codi, right(medea,1) from cat_centres where ep='0208'"
for up, rural in getAll(sql, nod):
    centres[up] = rural

pob = {}    
sql = 'select id_cip_sec, up, sexe, edat from assignada_tot'
for id, up, sexe, edat in getAll(sql, nod):
    if up in centres:
        rural = centres[up]
        pob[id] = {'sexe': sexe, 'edat': edat, 'rural':rural}

sex = Counter()  
ed = Counter()    
ruralitat = Counter()  
sql = "select id_cip_sec, cp_up from cmbdap where cp_t_act = 44 and cp_ecap_dvis between {} and {}".format(inici, final)
for id, up in getAll(sql, imp):
    if id in pob:
        sexe = pob[id]['sexe']
        edat = pob[id]['edat']
        rural = pob[id]['rural']
        sex[sexe]+= 1
        b = ageConverter(edat,5)
        ed[b] += 1
        ruralitat[rural] += 1
print "sexe"
print sex

print "ruralitat"
print ruralitat

for a,b in ed.items():
    print a, b
