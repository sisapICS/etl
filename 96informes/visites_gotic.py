import sisapUtils as u
import collections as c
import itertools as i
import multiprocessing as m

class Visites():
    def __init__(self):
        self.assignades()
        self.visitats()
    
    def assignades(self):
        print('assignades')
        self.a = set()
        sql = """SELECT hash FROM dwsisap.RCA_CIP_NIA rcn  WHERE eap = '00441'"""
        for cip, in u.getAll(sql, 'exadata'):
            self.a.add(cip)
    
    def visitats(self):
        print('visites')
        visites_assignats = c.Counter()
        visites_no_assignats = c.Counter()
        sql = """SELECT pacient, EXTRACT(year FROM DATA), extract(MONTH FROM data) FROM DWSISAP.SISAP_MASTER_VISITES 
                WHERE up = '00441' AND DATA < TO_DATE('2022-05-01', 'yyyy-mm-dd') AND SITUACIO = 'R' AND servei IN ('MG', 'INF')"""
        for pacient, any, mes in u.getAll(sql, 'exadata'):
            if pacient in self.a: visites_assignats[(any, mes)] += 1
            else: visites_no_assignats[(any, mes)] += 1
            
        print('pacients')
        pacients_assignats = c.Counter()
        pacients_no_assignats = c.Counter()
        sql = """SELECT distinct pacient, EXTRACT(year FROM DATA), extract(MONTH FROM data) FROM DWSISAP.SISAP_MASTER_VISITES 
                WHERE up = '00441' AND DATA < TO_DATE('2022-05-01', 'yyyy-mm-dd') AND SITUACIO = 'R' AND servei IN ('MG', 'INF')"""
        for pacient, any, mes in u.getAll(sql, 'exadata'):
            if pacient in self.a: pacients_assignats[(any, mes)] += 1
            else: pacients_no_assignats[(any, mes)] += 1

        print('tractament i penjar')
        upload = []
        print(len(pacients_assignats))
        for e in pacients_assignats:
            upload.append((e[0], e[1], pacients_assignats[e], pacients_no_assignats[e], visites_assignats[e], visites_no_assignats[e]))
        
        cols = "(any int, mes int, pacients_assi int, pacients_no_assi int, visites_assi int, visites_no_assi int)"
        u.createTable('visites_gotic', cols, 'test')
        u.listToTable(upload, 'visites_gotic', 'test')

def get_data(sql, model="sectors", redics="data", force=False, workers=None):
    """."""
    if model in ("redics", "exadata"):
        jobs = []
        digits = [format(digit, "x").upper() for digit in range(16)]
        jobs = [(sql.format("".join(combo)), (model, redics))
                for combo in i.product(digits, digits)]
        n_workers = workers if workers else 32 if model == "redics" else 8
    else:
        pass
    pool = m.Pool(n_workers)
    dades = pool.map(worker, jobs, chunksize=1)
    pool.close()
    for chunk in dades:
        for row in chunk:
            yield row
class Visites1():
    def __init__(self):
        self.assignades()
        self.visitats()
    
    def assignades(self):
        print('usutb101')
        hash_cip = {}
        sql = """SELECT usua_cip, usua_cip_cod FROM pdp.pdptb101"""
        for cip, hash in u.getAll(sql, 'pdp'):
            hash_cip[hash] = cip 
        print('assignades')
        self.a2019 = set()
        self.a2020 = set()
        self.a2021 = set()
        self.a2022 = set()
        sql = """SELECT DATAANY, IDPAC FROM SISAP_ASSIGNADAHISTORICA sa 
                WHERE DATAANY IN (2019, 2020, 2021, 2022)
                AND up = '00441'"""
        for year, hash in u.getAll(sql, 'import'):
            cip = hash_cip[hash]
            hash_c = h.sha1(cip).hexdigest().upper()
            if year == 2019: self.a2019.add(hash_c)
            if year == 2020: self.a2020.add(hash_c)
            if year == 2021: self.a2021.add(hash_c)
        sql = """select id_cip_sec from nodrizas.assignada_tot
                where up = '00441'"""
        for cip, in u.getAll(sql, 'nodrizas'):
            self.a2022.add(cip)
        print(len(self.a2019), len(self.a2020),len(self.a2021),len(self.a2022))
    
    def visitats(self):
        print('visites')
        visites_assignats = c.Counter()
        visites_no_assignats = c.Counter()
        sql = """SELECT pacient, EXTRACT(year FROM DATA), extract(MONTH FROM data) FROM DWSISAP.SISAP_MASTER_VISITES 
                WHERE up = '00441' AND DATA < TO_DATE('2022-05-01', 'yyyy-mm-dd') AND SITUACIO = 'R' AND servei IN ('MG', 'INF')"""
        for pacient, any, mes in u.getAll(sql, 'exadata'):
            if any == 2019:
                if pacient in self.a2019: visites_assignats[(any, mes)] += 1
                else: visites_no_assignats[(any, mes)] += 1
            elif any == 2020:
                if pacient in self.a2020: visites_assignats[(any, mes)] += 1
                else: visites_no_assignats[(any, mes)] += 1
            elif any == 2021:
                if pacient in self.a2021: visites_assignats[(any, mes)] += 1
                else: visites_no_assignats[(any, mes)] += 1
            else:
                if pacient in self.a2022: visites_assignats[(any, mes)] += 1
                else: visites_no_assignats[(any, mes)] += 1
            
        print('pacients')
        pacients_assignats = c.Counter()
        pacients_no_assignats = c.Counter()
        sql = """SELECT distinct pacient, EXTRACT(year FROM DATA), extract(MONTH FROM data) FROM DWSISAP.SISAP_MASTER_VISITES 
                WHERE up = '00441' AND DATA < TO_DATE('2022-05-01', 'yyyy-mm-dd') AND SITUACIO = 'R' AND servei IN ('MG', 'INF')"""
        for pacient, any, mes in u.getAll(sql, 'exadata'):
            if any == 2019:
                if pacient in self.a2019: pacients_assignats[(any, mes)] += 1
                else: pacients_no_assignats[(any, mes)] += 1
            elif any == 2020:
                if pacient in self.a2020: pacients_assignats[(any, mes)] += 1
                else: pacients_no_assignats[(any, mes)] += 1
            elif any == 2021:
                if pacient in self.a2021: pacients_assignats[(any, mes)] += 1
                else: pacients_no_assignats[(any, mes)] += 1
            else:
                if pacient in self.a2022: pacients_assignats[(any, mes)] += 1
                else: pacients_no_assignats[(any, mes)] += 1

        print('tractament i penjar')
        upload = []
        print(len(pacients_assignats))
        for e in pacients_assignats:
            upload.append((e[0], e[1], pacients_assignats[e], pacients_no_assignats[e], visites_assignats[e], visites_no_assignats[e]))
        
        cols = "(any int, mes int, pacients_assi int, pacients_no_assi int, visites_assi int, visites_no_assi int)"
        u.createTable('visites_gotic', cols, 'test')
        u.listToTable(upload, 'visites_gotic', 'test')

if __name__ == "__main__":
    # Visites()
    Visites1()

        