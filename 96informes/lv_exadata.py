import sisapUtils as u
import collections as c
import sisaptools as t
import datetime as d

file = u.baseFolder + ["96informes", "Estudi_PxM.txt"]
file = "/".join(file)
brs = ['BR292',	'BR097',	'B3527',
'BR379',	'BR054',	'BR160',
'BR288',	'BR072',	'B0462',
'BR115',	'BR320',	'BR363',
'BR380',	'BR108',	'B0464',
'BR243',	'BR344',	'BR076',
'B6156',	'B4276',	'BR065',
'BR139',	'BR104',	'BR256',
'B4863',	'BR073',	'BR233',
'B0355',	'BR182',	'BR014',
'BR122',	'B0294',	'B1485',
'BR192',	'BR018',	'BR002',
'BR125',	'BR338',	'B0117',
'BR365',	'BR053',	'BR086',
'BR398',	'BR348',	'BR234',
'BR334',	'BR203',	'BR337',
'BR336',	'BR214',	'BR004',
'B0702',	'BR227',	'B1327',
'BR085',	'BR325',	'BR255',
'BR079',	'B0695',	'B0125',
'B0299',	'BR064',	'BR400',
'BR311',	'BR341',	'BR376',
'B0460',	'B1121',	'BR127',
'B1273',	'BR039',	'BR207',
'B7084',	'BR095',	'BR121',
'BR261',	'BR366',	'BR130',
'B0466',	'BR021',	'BR128',
'BR241',	'BR350',	'BR262',
'B0479',	'BR056',	'BR179',
'B0480',	'BR022',	'B0005'
]
sql = "select scs_codi, ics_codi from cat_centres"
centres = {br: up for up, br in u.getAll(sql, 'nodrizas')}
dades = c.defaultdict(dict)
for (ind, periode, br, analisi, _p, tipprof, _dim, val) in u.readCSV(file, sep='{'):
    periode = '20' + periode[1:]
    if ind in ('QACC2DF', 'QACC10DF', 'QACC5DF', 'LONG0002A', 'CONT0002A', 'VISURG', 'VISUBA'):
        ind = ind + '-' + tipprof
    elif ind in ('POBASS', 'POBASSAT'):
        ind = ind + '-' + _p
        analisi = 'AGRESULT'
        dades[(ind, periode, br, tipprof)]['NUM'] = val
    if ind[:6] == 'SATUSU':
        periode = periode[-4:] + '12'
    dades[(ind, periode, br, tipprof)][analisi] = val

upload = []
for (ind, periode, br, tipprof), valors in dades.items():
    if br in brs and br in centres:
        up = centres[br]
        num = dades[(ind, periode, br, tipprof)]['NUM'] if 'NUM' in dades[(ind, periode, br, tipprof)] else None
        den = dades[(ind, periode, br, tipprof)]['DEN'] if 'DEN' in dades[(ind, periode, br, tipprof)] else None
        res = dades[(ind, periode, br, tipprof)]['AGRESULT'] if 'AGRESULT' in dades[(ind, periode, br, tipprof)] else None
        upload.append((ind, up, periode, num, den, res))

cols = "(codi varchar2(20), up varchar2(10), anymes varchar2(6), n int, d int, r float)"
u.createTable('estudi_pxm', cols, 'exadata', rm=True)
u.listToTable(upload, 'estudi_pxm', 'exadata')
u.grantSelect('estudi_pxm', 'DWSISAP_ROL', 'exadata')
