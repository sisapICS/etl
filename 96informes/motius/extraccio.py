# OJO, ejecuto con python3 desde X0002 (repo: masters)
# a x0002 (repo masters) no es pot importar sisapUtils d'on penja tempfolder

import sisaptools as u
from sisapUtils import tempFolder
import pandas as pd
from datetime import datetime as dt
import os

now = dt.now()
today, time = now.date(), now.time()
OUTFILE = os.path.join(
    tempFolder,
    'motius_{}_{}h{}m.txt'.format(today, time.hour, time.minute)
    )
con = u.Database('exadata', 'data')
sql = """
    SELECT servei,
        DATA,
        USUARI_TEXT
    FROM DWSISAP.SISAP_CORONAVIRUS_ACTIVITAT_F act
        JOIN dwsisap.DBC_CENTRES B ON act.up = b.UP_COD
        LEFT JOIN DWSISAP.LOGINS_ECAP logins ON act.SECTOR = logins.CODI_SECTOR
        AND act.USUARI = logins.IDE_USUARI
        LEFT JOIN DWSISAP.SISAP_MAP_CATPROF map_catprof2 ON logins.IDE_CATEG_PROF_C = map_catprof2.cod
    WHERE FLAG_TEXT = 1
        AND CATPROF_CLASS = 'UAC'
        AND B.SAP_COD = '57'
        AND -- Delta del llobregat
        DATA between DATE '2022-03-07' AND DATE '2022-03-13'
"""

# df = pd.read_sql(sql, con, index_col=None, coerce_float=True, params=None, parse_dates=None, columns=None, chunksize=None

columns = ('servei', 'date', 'text')
data = list(con.get_all(sql))

df = pd.DataFrame(data, columns=columns)
df.to_json(OUTFILE, orient='table')


# corpus de centres amb xuleta dels enginyers de procesos.
OUTFILE = os.path.join(
    tempFolder,  # a x0002 (repo masters) no es pot importar sisapUtils d'on penja tempfolder
    'xuleta_{}_{}h{}m.txt'.format(today, time.hour, time.minute)
    )

sql = """
    SELECT servei,
        DATA,
        regexp_replace(substr(USUARI_TEXT, 0, 3), '[^0-9]', '') AS xuleta,
        usuari_text
    FROM DWSISAP.SISAP_CORONAVIRUS_ACTIVITAT_F a
        JOIN dwsisap.DBC_CENTRES B ON
            a.up = b.UP_COD AND
            b.SAP_DES LIKE 'BADALONA SERVEIS%'
    WHERE DATA >= DATE '2022-03-07'
        AND REGEXP_LIKE(usuari_text, '^[0-9]')
        AND regexp_replace(substr(USUARI_TEXT, 0, 3), '[^0-9]', '') IS NOT NULL
        AND (
            usuari_text LIKE '% %'
            OR usuari_text LIKE '%-%'
            OR usuari_text LIKE '%.%'
        )
        AND length(usuari_text) >= 6
"""

columns = ('servei', 'date', 'xuleta', 'text')
data = list(con.get_all(sql))

df = pd.DataFrame(data, columns=columns)
df.to_json(OUTFILE, orient='table')