# coding: utf8
import urllib as w
import os
from datetime import *
import sisapUtils as u
from collections import defaultdict,Counter

imp = 'import'

def get_temps(t):

    temps = 0
    if t == 0:
        temps = '0 dies'
    elif 1 <= t <= 2:
        temps = '1 i 2 dies'
    elif 3 <= t <= 7:
        temps = 'de 3 a 7 dies'
    elif 8 <= t <= 15:
        temps = 'De 8 a 15 dies'
    elif 16 <= t <= 30:
        temps = 'De 16 a 30 dies'
    elif t > 30:
        temps = 'mes de 30 dies'
    elif t == -1:
        temps = 'err'
    return temps

    

tipus_visita_MG = ['9C', '9R', 'CP', 'ECOC', 'PM','ECOD']
                    
class Absentisme(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_homol()
        self.get_poblacio()
        self.get_gma()
        self.get_medea()
        self.dades_up()
        self.get_institucionalitzats()
        self.get_moduls()
        self.get_hash()
        self.get_visit_anualsMG()
        self.get_visites()
        self.get_dades()
        
    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi, amb_desc,  right(medea, 1), medea \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: (br, amb, sap,  amb_desc, rural, medea) for (up, br, amb, sap, amb_desc, rural, medea)
                        in u.getAll(*sql)}

    def get_homol(self):
        """Homologa tipus visita."""
        sql = ("select concat(codi_sector, tv_cita, tv_tipus), if(tv_model_nou = 'S', tv_tipus, tv_homol) \
                from cat_vistb206", "import")
                
        self.hm = {tipus: homol for tipus, homol in u.getAll(*sql)}

    def get_poblacio(self):
        """Obtenim edat i sexe pacients."""
        sql = ("select id_cip_sec, usua_data_naixement, usua_sexe, usua_nacionalitat\
                from assignada","import")
        self.pob = {(id): (naix, sexe, nac) for (id, naix, sexe, nac) 
                    in u.getAll(*sql)}
    
    def get_gma(self):
        """Obté els tres valors de GMA."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        self.gma = {(id): (cod, cmplx, num) for (id, cod, cmplx, num) 
                    in  u.getAll(*sql)}
    
    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        sql = ("select sector, valor \
                from sisap_medea", "redics")
        valors = {sector: str(valor) for (sector, valor)
                  in u.getAll(*sql)}
        self.Imedea = {}                  
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if str(sector) in valors:
                self.Imedea[(id)] = valors[sector] 
    
    def dades_up(self):
        """
        Exporta les dades de nivell UP a partir d'un Excel de l'Albert
        de Trello (https://goo.gl/U7AJQN) que formatejo manualment i
        torno a penjar a Trello (https://goo.gl/GWx3b5).
        També afegeix l'índex ISC de l'ACQUAS a partir d'un Excel del
        Manolo que formatejo a mà i penjo a Trello (https://goo.gl/frQ3TN).
        """
        file = 'frequentacio_up.txt'
        self.dispersio = {up:(dispersio) for (up, dispersio, docencia, medea, assignada, atesa)
                            in  u.readCSV(file)}
   
    def get_institucionalitzats(self):
        """
        Agafem els institucionalitzats de grups de tractaments.
        """
        sql = ("select id_cip_sec,gu_up_residencia,1 \
                from institucionalitzats a inner join cat_ppftb011_def b on a.ug_codi_grup=b.gu_codi_grup and a.codi_sector=b.codi_sector", "import")
        self.instis = {(id): institucionalitzat for (id, resi, institucionalitzat) 
                    in u.getAll(*sql)}
   
    def get_moduls(self):
        """
        Agafem moduls lligats a uba
        """
        sql = ("select codi_sector, modu_centre_codi_centre, modu_centre_classe_centre, modu_servei_codi_servei, modu_codi_modul \
                        from cat_vistb027 \
                        where modu_codi_uab <> '' ", "import")
        self.moduls = {(sec, cen, cla, ser, cod): 1 for sec, cen, cla, ser, cod 
                        in u.getAll(*sql)}
    
    def get_hash(self):
        """Conversió ID-HASH."""
        sql = ("select id_cip_sec, hash_d from eqa_u11", "nodrizas")
        self.hash = {(id):(hash) for id, hash 
                    in u.getAll(*sql)}
    
    def get_visit_anualsMG(self):
        """
        Agafem visites de qualsevol EAP de l'ICS i fetes en una
        agenda del servei MG.
        """
        self.visitsMG = Counter()
        sql = ("select id_cip_sec, visi_up, visi_tipus_visita \
               from visites1 \
               where visi_situacio_visita = 'R'", "import")
        for id, up, tipus in u.getAll(*sql):
            if up in self.centres:
                if tipus in tipus_visita_MG:
                    self.visitsMG[id] += 1
    
    def get_visites(self):
        """Obtenim les visites programades."""
        self.dades = []
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = ("select codi_sector, visi_centre_codi_centre, visi_centre_classe_centre,visi_modul_codi_modul,id_cip_sec,visi_data_visita,visi_dia_peticio, date_format(visi_data_visita, '%Y-%m-%d'),date_format(visi_dia_peticio, '%Y-%m-%d') ,visi_up, visi_situacio_visita, visi_servei_codi_servei,  concat(codi_sector, visi_tipus_citacio, visi_tipus_visita), extract(year_month from visi_data_visita), visi_tipus_visita \
           from visites1 \
           where visi_data_baixa = 0 and visi_lloc_visita = 'C' and visi_data_visita <'2017-11-30'", "import")
        for sec,cen,cla,cod,id, datav1, datap1, datav, datap, visi_up, situacio, servei, hmv, periode, tipusv  in u.getAll(*sql):
            if visi_up in self.centres:
                id = int(id)
                dia = datav1.weekday()
                if (id) in self.pob:
                    nac = self.pob[(id)][2]
                else:
                    nac = 0
                if nac in renta:
                    renta_baixa = 1
                else:
                    renta_baixa = 0
                try:
                    b = u.daysBetween(datap1,datav1)
                except:
                    b = -1
                temps = get_temps(b)
                try:
                    naix = self.pob[(id)][0]
                    edat = u.yearsBetween(naix, datav1)
                except:
                    edat = ''
                if situacio == 'R':
                    visit = 1
                else:
                    visit = 0
                try:
                    insti = self.instis[(id)]
                except KeyError:
                    insti = 0
                try:
                    uba = self.moduls[(sec, cen, cla, servei, cod)]
                except KeyError:
                    uba = 0
                if tipusv in tipus_visita_MG:
                    if id in self.visitsMG:
                        frequentacio_pacient = self.visitsMG[id]
                        self.dades.append([id, self.hash[(id)] if (id) in self.hash else '', sec,  edat,  uba,
                              self.pob[(id)][1] if (id) in self.pob else '', self.gma[(id)][0] if (id) in self.gma else '', self.gma[(id)][1] if (id) in self.gma else '',
                              self.gma[(id)][2] if (id) in self.gma else '', self.Imedea[(id)] if (id) in self.Imedea else '', self.centres[visi_up][4], self.centres[visi_up][5], self.dispersio[visi_up], insti,
                              renta_baixa,  datav, datap, dia, periode,  servei, self.hm[hmv] if hmv in self.hm else tipusv, 
                              frequentacio_pacient, 0,0,0, temps, visit, situacio,
                               visi_up, self.centres[visi_up][3]])

   
    def get_dades(self):
        """Metode per crear i omplir taules."""
        table = 'SISAP_ABSENTISME'
        columns = ('id int', 'hash varchar(40)', 'codi_sector varchar(4)', 'edat int', 'lligatUBA int',
                            'sexe varchar(1)', 'gma_codi varchar(3)', 'gma_complexitat double',
                           'gma_num_croniques int','medea_seccio double','ruralitat varchar(2)', 'medea_sisap varchar(2)', 'dispersio varchar(2)', 'institucionalitzat int',
                            'immigracio_baixa_renta int', 'data_visita varchar(20)', 'data_peticio varchar(20)','dia int', 'periode int', 'servei varchar(10)', 'tipus_visita varchar(10)',
                            'frequentacio_pacient int', 'acc_48h double', 'acc_5dies double', 'acc_10dies double', 'temps_peti_visita varchar(20)', 'visita_feta int', 'visit_sit varchar(10)',
                            'codi_up varchar(5)', 'ambit varchar(100)')
        columns_str = "({})".format(', '.join(columns))
        db = "test"
        u.createTable(table, columns_str, db, rm=True)
        u.listToTable(self.dades, table, db)
   
if __name__ == '__main__':
    Absentisme()
