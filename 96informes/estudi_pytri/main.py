import sisapUtils as u
import pandas as pd
import os
from datetime import date

FILENAME = "pytri2_{}.{}".format(date.today(), 'csv')
# ruta = ['J:\\', 'MENCIA', 'ESTUDIS DADES SISAP', 'PYTRI', 'resultats_leo']
# ruta.append(FILENAME)
# fname = os.path.join(*ruta)

fname = os.path.join(*[u.tempFolder, "pytri2_{}.{}".format(date.today(), 'csv')])

sql = """
    WITH escoge as (
    SELECT
        id_cip_sec,
        xml_tipus_orig,
        max(xml_data_alta) as xml_data_alta
    FROM
        xml_detall
    WHERE
        xml_tipus_orig in (
            'XML0000050', -- ficha de visita 1
            'XML0000051', -- ficha de visita 2
            'XML0000099'  -- ficha de visita 3
        )
    GROUP BY
        id_cip_sec,
        xml_tipus_orig
    )
    SELECT
        id_cip_sec as cip,
        concat('xml',substr(xml_tipus_orig,9,2),'_DATA_FITXA') as var,
        xml_data_alta as val
    FROM
        escoge
    UNION
    SELECT
        xd.id_cip_sec as cip,
        concat('xml',substr(xd.xml_tipus_orig,9,2),'_',xd.camp_codi) as var,
        max(xd.camp_valor) as val
    FROM
        xml_detall xd INNER JOIN
        escoge e ON
            xd.id_cip_sec = e.id_cip_sec
            AND xd.xml_tipus_orig = e.xml_tipus_orig
            AND xd.xml_data_alta = e.xml_data_alta
    GROUP BY
        xd.id_cip_sec,
        concat('xml',substr(xd.xml_tipus_orig,9,2),'_',xd.camp_codi)
"""

df_orig = pd.DataFrame([row for row in u.getAll(sql, 'import')],
                       columns=['cip', 'camp', 'val'])

df_orig.shape
# df['fitxa'] = df.camp.str[:5]
# df = df_orig2.set_index(['cip', 'camp'])

df = df_orig.set_index(['cip', 'camp'])
df = df.unstack()
# aplano el multiindex de columnas (val, camp) quedando solo camp.
df.columns = [col[1] for col in df.columns.values]
df.shape

# Add datos poblacionales
sql = """
    WITH escoge as (
    SELECT
        distinct id_cip_sec
    FROM xml_detall
    WHERE
        xml_tipus_orig IN (
        'XML0000050', -- ficha de visita 1
        'XML0000051', -- ficha de visita 2
        'XML0000099'  -- ficha de visita 3
        )
    )
    SELECT
        e.id_cip_sec,
        if(usua_uab_up = '', c.scs_codi, usua_uab_up) as up,
        usua_uab_codi_uab,
        usua_nacionalitat
    FROM
        assignada a INNER JOIN escoge e
            ON a.id_cip_sec = e.id_cip_sec
        LEFT JOIN nodrizas.cat_centres c
            ON a.usua_abs_codi_abs = c.abs
            AND c.abs <> 0
"""

# datos poblacionales
uba = pd.DataFrame(
    [row for row in u.getAll(sql, 'import')],
    columns=['cip', 'up', 'uba', 'nacionalitat']
    )


df = df.join(uba.set_index('cip'), on='cip')
# sujetos sin UP(ass)/abs(domicili)
len(df[df['up'].isna()].index)  # 6
# df.loc[[1869909]].stack()

# add datos ecologicos
sql = "SELECT scs_codi, medea FROM cat_centres"
medea = pd.DataFrame([row for row in u.getAll(sql, 'nodrizas')],
                     columns=['up', 'medea'])
df = df.join(medea.set_index('up'), on='up')

# add datos centres seleccionats
select_centres = (
    'BR185', 'BR238', 'BR237', 'BR373', 'BR176',
    'BR092', 'BR187', 'BR267', 'BR106', 'BR181',
    'BR335', 'BR353', 'BR112', 'BR107', 'BR178',
    'BR328', 'BR021', 'BR040', 'BR060', 'BR396',
    'BR042', 'BR297', 'BR316', 'BR359', 'BR374',
    'BR261', 'BR126', 'BR125', 'BR009', 'BR095',
    'BR282', 'BR349', 'BR151', 'BR304', 'BR243',
    'BR312', 'BR070', 'BR296', 'BR184', 'BR158',
    'BR275', 'BR300', 'BR193', 'BR303', 'BR339',
    )

sql = """
    SELECT
        scs_codi,
        'Si' as centre_seleccionat,
        ics_codi,
        ics_desc,
        sap_codi,
        sap_desc,
        amb_codi,
        amb_desc
    FROM nodrizas.cat_centres
    WHERE
        ics_codi in {}
    """.format(select_centres)

centres = pd.DataFrame(
    [row for row in u.getAll(sql, 'nodrizas')],
    columns=[
        'up',
        'centre_seleccionat',
        'ics_codi', 'ics_desc',
        'sap_codi', 'sap_desc',
        'amb_codi', 'amb_desc']
    )
df = df.join(centres.set_index('up'), on='up')


# add datos tto: IBPs
sql = """
    WITH
        escoge as (
            SELECT
                id_cip_sec,
                max(xml_data_alta) as alta
            FROM import.xml_detall
            WHERE xml_tipus_orig = 'XML0000050' and camp_codi='DATA_Test'
            GROUP by id_cip_sec
        ),
        data_ref as (
        SELECT
            e.id_cip_sec,
            min(str_to_date(camp_valor, '%d/%m/%Y')) as data_test
        FROM
            import.xml_detall x INNER JOIN
            escoge e
                on x.id_cip_sec = e.id_cip_sec
                and e.alta = x.xml_data_alta
                and xml_tipus_orig = 'XML0000050'
                and camp_codi= 'DATA_Test'
        GROUP BY id_cip_sec
        ),
        fuerzas_IBPs as (
        SELECT
            pf_codi,
            if(substring_INDEX(rtrim(left(replace(pf_desc, 'FARMACEUTICA', 'FARMACEUTICA '),position('MG' in upper(replace(pf_desc, 'FARMACEUTICA', 'FARMACEUTICA ')))-1)),' ', -1)*1 <10,
               substring_INDEX(rtrim(left(replace(pf_desc, 'FARMACEUTICA', 'FARMACEUTICA '),position('MG' in upper(replace(pf_desc, 'FARMACEUTICA', 'FARMACEUTICA ')))-1)),' ', -1)*1 *10,
               substring_INDEX(rtrim(left(replace(pf_desc, 'FARMACEUTICA', 'FARMACEUTICA '),position('MG' in upper(replace(pf_desc, 'FARMACEUTICA', 'FARMACEUTICA ')))-1)),' ', -1)*1) as fuerza
        FROM
            import.cat_cpftb006
        WHERE
            pf_cod_atc in ('A02BC01', 'A02BC02', 'A02BC05', 'A02BC03')
        )
    SELECT distinct
        r.id_cip_sec cip,
        -- ppfmc_pf_codi pfc,
        ppfmc_atccodi ibp_atc,
        ppfmc_pmc_data_ini ibp_ini,
        ppfmc_durada ibp_durada,
        -- ppfmc_posologia pos,
        -- ppfmc_freq freq,
        (24*ppfmc_posologia)/ppfmc_freq as ibp_freq_dia,
        ibp.fuerza as ibp_mg
        -- ,r.data_test
    FROM
        import.tractaments t inner join
        data_ref r
            on t.id_cip_sec = r.id_cip_sec
        left join fuerzas_IBPs ibp
            on t.ppfmc_pf_codi = ibp.pf_codi
    WHERE
        t.ppfmc_pmc_data_ini between r.data_test and date_add(r.data_test, interval 15 day)
        and t.ppfmc_atccodi in (
        -- 'J01CA04',  --  Amoxicilina
        -- 'J01FA09',  --  Claritromicina
        'A02BC01', 'A02BC02', 'A02BC05', 'A02BC03'  -- IBPs (incluye esomeprazol:C05 y lanzoprazol:C03)
        )
        and t.ppfmc_durada > 0
"""
ibp = pd.DataFrame(
    [row for row in u.getAll(sql, 'import')],
    columns=[
        'cip',
        'ibp_atc',
        'ibp_ini',
        'ibp_durada',
        'ibp_freq_dia',
        'ibp_mg'
        ]
    )

# unicos por ibp (el primero de cada cip, desempates por menor durada)
ibp = ibp.sort_values(
    ['cip', 'ibp_ini', 'ibp_durada']
    ).groupby(['cip']).head(1).reset_index(drop=True)

# aqui dry-run, cal assignar a objecte: df = ...
df = df.join(ibp.set_index('cip'), on='cip')

# tres casos amb mes d'un IBP per cip
"""
ibp.loc[ibp['cip'] == 6457056]  # mateix dia
ibp.loc[ibp['cip'] == 6593631]
ibp.loc[ibp['cip'] == 6855144]
ibp.loc[ibp['cip'] == 18311719]

for cip in (
    6457056,
    6593631,
    7739182,
    5014545,
    2605421,
    2103054,
    18932378,
    11183144,
    18311719
        ):
    ibp.loc[ibp['cip'] == cip]
"""

# add datos tto: Amoxi, Claritro
sql = """
    WITH
        escoge as (
            SELECT
                id_cip_sec,
                max(xml_data_alta) as alta
            FROM import.xml_detall
            WHERE xml_tipus_orig = 'XML0000050' and camp_codi='DATA_Test'
            GROUP by id_cip_sec
        ),
        data_ref as (
        SELECT
            e.id_cip_sec,
            min(str_to_date(camp_valor, '%d/%m/%Y')) as data_test
        FROM
            import.xml_detall x INNER JOIN
            escoge e
                on x.id_cip_sec = e.id_cip_sec
                and e.alta = x.xml_data_alta
                and xml_tipus_orig = 'XML0000050'
                and camp_codi= 'DATA_Test'
        GROUP BY id_cip_sec
        )
    SELECT distinct
        r.id_cip_sec cip
        -- ,ppfmc_pf_codi pfc
        ,ppfmc_atccodi ibp_atc
        -- ,ppfmc_pmc_data_ini ibp_ini,
        -- ppfmc_durada ibp_durada,
        -- ppfmc_posologia pos,
        -- ppfmc_freq freq,
        -- (24*ppfmc_posologia)/ppfmc_freq as ibp_freq_dia,
        -- ,r.data_test
    FROM
        import.tractaments t inner join
        data_ref r
            on t.id_cip_sec = r.id_cip_sec
    WHERE
        t.ppfmc_pmc_data_ini between r.data_test and date_add(r.data_test, interval 15 day)
        and t.ppfmc_atccodi in (
         'J01CA04'  -- Amoxicilina
        ,'J01FA09'  -- Claritromicina
        ,'P01AB01'  -- metronidazol
        ,'A02BD08'  -- Pylera
        ,'A02BC05'  -- Esomeprazol
        ,'A02BC03'  -- Lanzoprazol
        )
        and t.ppfmc_durada > 0
"""

atb = pd.DataFrame(
    [row for row in u.getAll(sql, 'import')],
    columns=[
        'cip',
        'atc'
        ]
    )


atb = pd.pivot_table(atb, index='cip', columns='atc', aggfunc=lambda x: True)
atb.columns = [col for col in atb.columns.values]

df = df.join(atb, on='cip')

# Quitamos los 10 que no tienen ni la primera ficha.
df = df[~df['xml50_DATA_FITXA'].isna()]

# # Quitamos los missing de Inclusio o Interpretacio? los dejare
# df = df[~df['xml50_Inclusio'].isna() & ~df['xml50_Interpretacio'].isna()]

sql = """
    SELECT
        id_cip_sec,
        hash_d,
        codi_sector
    FROM
        u11
    """

u11 = pd.DataFrame(
    [row for row in u.getAll(sql, 'import')],
    columns=[
        'cip',
        'hash',
        'sector'
        ]
    )

df = df.join(u11.set_index('cip'), on='cip')

# Correcion de tipos
YMD = "%Y/%m/%d"
DMY = "%d/%m/%Y"

df['xml50_DATA_FITXA'] = pd.to_datetime(df['xml50_DATA_FITXA'], format=YMD)
df['xml51_DATA_FITXA'] = pd.to_datetime(df['xml51_DATA_FITXA'], format=YMD)
df['xml99_DATA_FITXA'] = pd.to_datetime(df['xml99_DATA_FITXA'], format=YMD)

df['xml50_Edat'] = pd.to_numeric(df['xml50_Edat'])
df['xml50_EdatPacient'] = pd.to_numeric(df['xml50_EdatPacient'])
df['xml51_EdatPacient'] = pd.to_numeric(df['xml51_EdatPacient'])
df['xml99_EdatPacient'] = pd.to_numeric(df['xml99_EdatPacient'])

df['xml50_DATA_Test'] = pd.to_datetime(df['xml50_DATA_Test'], format=DMY)
df['xml50_DataNaixPacient'] = pd.to_datetime(df['xml50_DataNaixPacient'], format=DMY)
df['xml51_DATA_Test'] = pd.to_datetime(df['xml51_DATA_Test'], format=DMY)
df['xml51_DataNaixPacient'] = pd.to_datetime(df['xml51_DataNaixPacient'], format=DMY)
df['xml51_dataencaspositiu'] = pd.to_datetime(df['xml51_dataencaspositiu'], format=DMY)
df['xml99_DATA_Test'] = pd.to_datetime(df['xml99_DATA_Test'], format=DMY)
df['xml99_DataNaixPacient'] = pd.to_datetime(df['xml99_DataNaixPacient'], format=DMY)
df['xml99_Dataprovadecontrol'] = pd.to_datetime(df['xml99_Dataprovadecontrol'], format=DMY)

# Exporta a csv
df.to_csv(fname.replace('.xlsx', '.csv'))

# rencoding para sacar a excel XLSX
df['xml50_Centre'] = df['xml50_Centre'].str.decode('iso_8859-1').str.encode('utf-8')
df['xml50_DadesIdentifProfessional'] = df['xml50_DadesIdentifProfessional'].str.decode('iso_8859-1').str.encode('utf-8')
df['xml50_Interpretacio'] = df['xml50_Interpretacio'].str.decode('iso_8859-1').str.encode('utf-8')
df['xml51_Centre'] = df['xml51_Centre'].str.decode('iso_8859-1').str.encode('utf-8')
df['xml51_DadesIdentifProfessional'] = df['xml51_DadesIdentifProfessional'].str.decode('iso_8859-1').str.encode('utf-8')
df['xml99_Centre'] = df['xml99_Centre'].str.decode('iso_8859-1').str.encode('utf-8')
df['xml99_DadesIdentifProfessional'] = df['xml99_DadesIdentifProfessional'].str.decode('iso_8859-1').str.encode('utf-8')
df['ics_codi'] = df['ics_codi'].str.decode('iso_8859-1').str.encode('utf-8')
df['ics_desc'] = df['ics_desc'].str.decode('iso_8859-1').str.encode('utf-8')
df['sap_desc'] = df['sap_desc'].str.decode('iso_8859-1').str.encode('utf-8')
df['amb_desc'] = df['amb_desc'].str.decode('iso_8859-1').str.encode('utf-8')

# Exporta a xlsx
df.to_excel(fname, index=False)

# # # SCRATCH / EXPLORACIONES # # #

# CONTEOS por inclusion/exclusion

# Inclusio(1/0/Na) i Interpretacio('I'/'E'/Na) son la mateixa info
"""
df.groupby('xml50_Interpretacio')['xml50_Interpretacio'].count()  # 239(I), 62(E), 12:Na
df.groupby('xml50_Inclusio')['xml50_Inclusio'].count()  # 240(1), 62(0), 11:Na
"""
#   (excepte: 1 cas inclos que es missing a Interpretacio)
"""
pd.crosstab(df['xml50_Inclusio'].fillna('-99'), df['xml50_Interpretacio'].fillna('-99'), dropna=False, margins=True)
pd.crosstab(df['xml50_Inclusio'].fillna('-99'), df['ibp_atc'].fillna('-99'), dropna=False, margins=True)
pd.crosstab(df['xml50_DATA_FITXA'].fillna('-99'), df['xml50_Inclusio'].fillna('-99'), dropna=False, margins=True)
pd.crosstab(df['xml50_DATA_FITXA'].fillna('-99'), df['xml50_DATA_Test'].fillna('-99'), dropna=False, margins=True)
"""
# quedan 12 != 'Inclos'/'Exclos', aqui intento indentificarlos
"""
for cip in df[df['xml50_Interpretacio'].isna()].index:
    print(df.loc[[cip]]['xml50_Inclusio'])  # 1946770
    print(cip)
"""

# Identifica columnas por patron de nombre (para typear)
"""
for col in df.columns:
    if 'edat' in col.lower():
        print col
        df[col].dtype
        df[col].describe()
"""

# test de rencodings para sacar a excel
"""
test = pd.DataFrame(
    ['a', 'b', 'c', 'algo'],
    [1, 2, 3, 4],
    columns=['col']
    ).reset_index()
test.to_excel(fname, index=False)
"""
