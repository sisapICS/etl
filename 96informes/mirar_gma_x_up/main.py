import pandas as pd
import sisapUtils as u
import os
import sys
sys.path.insert(1, 'C:\\Users\\lmendez\\projects\\sisap\\25resis\\source')
import shared as s

SECTOR_LLEIDA = '6102'
arxiu = os.path.join(u.tempFolder, "gma4newUP", "Cervera_Leo.xlsx")


def get_hash():
    sql = "select usua_cip, usua_cip_cod from pdptb101 where usua_cip_cod like '{}%'"
    hshs = {k: v for k, v in s.get_data(sql, model="redics", redics="pdp") if k in cips}
    return hshs


def get_idcips():
    db = "import"
    sql = """
        select
            hash_d, id_cip_sec
        from
            u11
        where
            codi_sector = '{}'
    """
    sql = sql.format(SECTOR_LLEIDA)
    id_cips = {k: v for k, v in u.getAll(sql, db) if k in hshs_set}
    return id_cips


def get_gma():
    sql = "select id_cip_sec, gma_ind_cmplx from gma"
    # sql = "select gma_any, count(*) from gma group by gma_any"
    gmas = {k: v for k, v in u.getAll(sql, "import") if k in id_cips_set}
    print(len(gmas))
    return gmas


df = pd.read_excel(arxiu)
cips = set([cip.encode('utf-8') for cip in list(df['CIP'])])
hshs = get_hash()
hshs_set = set(hshs.values())
id_cips = get_idcips()
id_cips_set = set(id_cips.values())
gmas = get_gma()


print("""X GMA: {}, pacients lligats: {}, pacients inicials: {}({})\
      """.format(round(sum(gmas.values())/len(gmas), 3),
                 len(gmas),
                 len(cips),
                 len(gmas)-len(cips)
                 )
      )
