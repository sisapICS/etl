
import sisapUtils as u
import collections as c

print('dbs')
sql = """SELECT c_sap, grup
            FROM dbs
            WHERE c_edat_anys > 14"""

data = c.Counter()
assignada_sap = c.Counter()

for sap, grup in u.getAll(sql, 'redics'):
    assignada_sap[sap] += 1
    data[(sap, grup)] += 1

print('centres')
centres = {}
sql = """select sap_codi, sap_desc, amb_codi, amb_desc from nodrizas.cat_centres"""
for sap_c, sap_d, amb_c, amb_d in u.getAll(sql, 'nodrizas'):
    centres[sap_c] =  (sap_d, amb_c, amb_d)

print('grups')
descripcio_grups = {}
sql = """SELECT grup, descripcio FROM visita_cronics_cat"""
for grup, desc in u.getAll(sql, 'pdp'):
    descripcio_grups[grup] = desc

upload = []
print('cuinetes')
for (sap, grup), N in data.items():
    if sap in centres:
        desc_sap, ambit, desc_ambit = centres[sap]
    else: desc_sap, ambit, desc_ambit = '','',''
    if grup == 0: planificat = 'NO PLANIFICAT'
    else: planificat = 'PLANIFICAT'
    assignada = assignada_sap[sap]
    if grup in descripcio_grups: desc_grup = descripcio_grups[grup]
    else: desc_grup = ''
    upload.append((sap, desc_sap, ambit, desc_ambit, planificat, grup, desc_grup, N, assignada))

with open("dades_planificat.txt", 'w') as output:
    for row in upload:
        output.write(str(row) + '\n')

file_eap = u.tempFolder + 'Dades_planificat.csv'
u.writeCSV(file_eap, [('SAP', 'DESC_SAP', 'AMBIT', 'DESC_AMBIT', 'PLANIFICAT', 'GRUP', 'DESC_GRUP', 'N', 'ASSIGNATS')] + upload, sep=";")

# email
subject = 'Dades planificat'
text = "Bon dia.\n\nAdjuntem arxiu .csv\n"
text += "\n\nSalutacions."

u.sendGeneral('SISAP <sisap@gencat.cat>',
                      ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                       ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat'),
                      subject,
                      text,
                      file_eap)