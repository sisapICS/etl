import re
import sisapUtils as u
import sisaptools as t
import collections as c
# import pandas as pd


DB_DESTI = ("p2262", "permanent")
TABLE = "names2sex"
noms = c.Counter()
sql = "select usua_nom, usua_sexe, count(*) as n from usutb040 group by usua_nom, usua_sexe"

for sector in u.sectors:
    for nom, sexe, n in u.getAll(sql, sector):
        if sexe in ('D', 'H'):
            nom = re.sub('\s+', ' ', nom.lstrip().rstrip())
            noms[(nom, sexe)] += n

upload = []
for k, v in noms.items():
    nom, sexe = k[0], k[1]
    upload.append((nom, sexe, v))

with t.Database(*DB_DESTI) as db:
    cols = (
        "nom varchar(255)",
        "sexe varchar(1)",
        "n int")
    db.create_table(TABLE, cols, remove=True)
    db.list_to_table(upload, TABLE)

# noms_sexe = c.defaultdict(dict)
# for k, v in noms.items():
#     nom = k[0]
#     sexe = k[1]
#     noms_sexe[nom][sexe] =  v

# noms_df = pd.DataFrame.from_dict(noms_sexe, orient='index')
# noms_df = noms_df.reset_index()
# noms_df = noms_df.fillna(0)
# noms_df['D_H'] = noms_df['D']/noms_df['H']
# noms_df['TOTAL'] = noms_df['D']+noms_df['H']

# noms_df['TOTAL'].nsmallest(n=10)
