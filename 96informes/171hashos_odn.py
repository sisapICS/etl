# coding: iso-8859-1

from sisapUtils import *
from datetime import *
import csv,os,sys
from time import strftime

file = tempFolder + 'odn_juvany.txt'

cips = {}
sql = "select usua_cip, usua_cip_cod from pdptb101"
for cip, hash in getAll(sql, 'pdp'):
    cips[hash] = cip

upload = []    
for (h, sector, indicador, up, valor) in readCSV(file, sep='@'):
    cip = cips[h]
    upload.append([cip, indicador, up, valor])
    
file = tempFolder + 'cips_juvany.txt'
writeCSV(file, upload, sep=';')