# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
import sys

import sisapUtils as u



class EConsulta(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_centres()
        self.get_converses()
        self.get_poblacio()
        self.export_data()
        
    
    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres where ep='0208'"
        self.centres = {up: br for (up, br) in u.getAll(sql, 'nodrizas')}
        
    def get_converses(self):
        """."""
        sql = "select id_cip_sec, codi_sector, conv_id, conv_autor, \
                      conv_desti, conv_estat, conv_dini \
               from econsulta_conv, nodrizas.dextraccio \
               where conv_dini  > '2017-12-31'"
        self.converses = {}
        for id, sector, idc, autor, desti, estat, ini in u.getAll(sql, 'import'):
            iniciaprof =  1 if autor else 0
            iniciapac = 1 if desti else 0
            if (id) in self.converses:
                ini2 = self.converses[id]['ini']
                if ini < ini2:
                    self.converses[id]['prof'] = iniciaprof
                    self.converses[id]['pac'] = iniciapac
            else:
                self.converses[(id)] = {'prof': iniciaprof, 'pac': iniciapac, 'ini': ini}
    
    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, up, uba, ubainf from assignada_tot"
        self.poblacio_pac = {}
        for id, up, uba, ubainf in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                br = self.centres[up]
                if id in self.converses:
                    iniciaprof = self.converses[id]['prof']
                    iniciapac = self.converses[id]['pac']
                    if (up, br, uba, 'M') in self.poblacio_pac:
                        self.poblacio_pac[(up, br, uba, 'M')]['p'] += iniciaprof
                        self.poblacio_pac[(up, br, uba, 'M')]['pac'] += iniciapac
                    else:
                        self.poblacio_pac[(up, br, uba, 'M')] = {'p': iniciaprof, 'pac': iniciapac }
                    if (up, br, ubainf, 'I') in self.poblacio_pac:
                        self.poblacio_pac[(up, br, ubainf, 'I')]['p'] += iniciaprof
                        self.poblacio_pac[(up, br, ubainf, 'I')]['pac'] += iniciapac
                    else:
                        self.poblacio_pac[(up, br, ubainf, 'I')] = {'p': iniciaprof, 'pac': iniciapac }

    def export_data(self):
        """."""
        dades = []
        for (up, br, inf, tup), d in self.poblacio_pac.items():
            dades.append([up, br, inf, tup, d['p'], d['pac']])
        u.writeCSV(u.tempFolder + 'econsulta_pac.txt', dades)
        
if __name__ == '__main__':
    EConsulta()