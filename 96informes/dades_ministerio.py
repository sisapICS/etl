# -*- coding: utf-8 -*-

"""
---
"""

from sisapUtils import *
import pandas as pd
from collections import defaultdict


### PACIENTS ###
def pacients():
    # obtenir ups d'interes
    # sql = """SELECT up_cod FROM dwsisap.DBC_CENTRES 
    #             WHERE abs_des LIKE '%AMPOSTA%'
    #             OR abs_des LIKE '%BARCELONA 1-B%'
    #             OR abs_des LIKE '%BARCELONA 7-G%'
    #             OR abs_des LIKE '%BARCELONA 8-G%'
    #             OR abs_des LIKE '%BARCELONA 9-A%'
    #             OR abs_des LIKE '%BARCELONA 9-E%'
    #             OR abs_des LIKE 'CALAF'
    #             OR abs_des LIKE 'CAPELLADES'
    #             OR abs_des LIKE '%CARDONA%'
    #             OR abs_des LIKE '%BARCELONA 1-B%'
    #             OR abs_des LIKE '%CERDANYOLA-RIPOLLET%'
    #             OR abs_des LIKE '%CUBELLES%'
    #             OR abs_des LIKE '%DELTEBRE%'
    #             OR abs_des LIKE '%EN BAS%'
    #             OR abs_des LIKE '%LLEIDA 3%'
    #             OR abs_des LIKE '%CUBELLES%'
    #             OR abs_des LIKE '%MOLLET DEL VALLÈS-EST%'
    #             OR abs_des LIKE 'PIERA'
    #             OR abs_des LIKE 'PREMIÀ DE MAR'
    #             OR abs_des LIKE 'RODA DE TER'
    #             OR abs_des LIKE 'SABADELL 1A'
    #             OR abs_des LIKE 'SABADELL 3B'
    #             OR abs_des LIKE 'SANT ANDREU DE LA BARCA'
    #             OR abs_des LIKE 'SANT VICENÇ DELS HORTS-1%'
    #             OR abs_des LIKE 'SANTA COLOMA DE QUERALT%'
    #             OR abs_des LIKE 'SANTA COLOMA DE GRAMENET 4'
    #             OR abs_des LIKE 'SANTA PERPÈTUA DE LA MOGODA%'
    #             OR abs_des LIKE 'TARRAGONA 1'
    #             OR abs_des LIKE 'VILAFANT'
    #             OR abs_des LIKE 'VILANOVA I LA GELTRÚ 1'"""
    # up = list()
    # for u, in getAll(sql, 'exadata'):
    #     up.append(u)

    # up = ('00010', '00171', '00174', '00179',
    #      '00185', '00186', '00369', '00388', '00440',
    #      '00449', '00453', '00491', '00498', '00196',
    #      '00205', '00286', '00341', '00358', '00049',
    #      '00050', '00089', '00090', '00133', '01122',
    #      '03449', '04713', '05945', '01883')

    # # obtenir nacionalitats
    # print('nacionalitats')
    # sql = """SELECT codi, nom FROM DWSISAP.DIM_NACIONALITAT"""
    # nacionalitats = dict()
    # for codi, pais in getAll(sql, 'exadata'):
    #     nacionalitats[str(pais)] = codi


    # obtenir defuncions
    print('poblacio')
    sql = """SELECT cip, DATA_NAIXEMENT, sexe, DATA_DEFUNCIO, NACIONALITAT, UP_RESIDENCIA 
            FROM DWSISAP.RCA_CIP_NIA 
            WHERE (DATA_DEFUNCIO IS NULL OR 
            extract(YEAR FROM DATA_DEFUNCIO) = 2020) AND  
            extract(YEAR FROM DATA_naixement) <= 2020
            AND situacio != 'T' 
            AND cip NOT IN (SELECT cip FROM DWSISAP.RCA_CIP_NIA 
            WHERE DATA_DEFUNCIO IS NULL AND situacio = 'D')"""

    # obtenir poblacio
    # print('poblacio')
    # sql = """SELECT b.CIP, a.C_DATA_NAIX, a.C_SEXE, b.DATA_DEFUNCIO, a.c_nacionalitat, a.c_up, a.c_metge, a.c_infermera 
    #         FROM dwsisap.dbs a, DWSISAP.RCA_CIP_NIA b
    #         WHERE a.C_CIP = b.hash"""

    persones = []

    for cip, naix, sexe, defuncio, nacionalitat, up in getAll(sql, 'exadata'):
        # transformacio sexe
        if sexe == '0': sex = 1
        elif sexe == '1': sex = 2
        else: sex = 3 
        # transformacio nacionalitat
        # try:
        #     nac = nacionalitats[str(nacionalitat)]
        # except:
        #     nac = 999
        persones.append((str(cip), naix, sex, defuncio, nacionalitat, str(up)))

    print(len(persones))

    print('creating database')
    cols = "(cip varchar2(14), fecha_nacimiento date, sexo varchar2(1), \
            fecha_defuncion date, pais_nacimiento varchar2(4), up varchar2(5))"

    createTable('bdcap_pacientes', cols, 'exadata', rm=True)
    listToTable(persones, 'bdcap_pacientes', "exadata")
    grantSelect('bdcap_pacientes','DWSISAP_ROL','exadata')


### HASH A CIP ###
def get_cips():
    print('cips')
    cips = dict()
    sql = """SELECT usua_cip, usua_cip_cod 
            FROM pdp.pdptb101"""
    for cip, hash in getAll(sql, "pdp"):
        cips[hash] = cip
    return cips

### CREAR CATÀLEG ###
def definicions(x, proc_dic, param_dic):
    p = proc_dic.keys()
    p = [str(e)for e in p]
    v = param_dic.keys()
    v = [str(e) for e in v]
    if str(x) in p:
        return proc_dic[x] 
    elif str(x) in v:
        return param_dic[x]
    else: 
        return ''

def cataleg():
    equiv = pd.read_csv('equiv.csv')
    equiv = equiv[['TIPUS','CODI_ECAP', 'CONCEPTID', 'ORIGEN_TIPUS_DADES']]
    param = pd.read_csv('param.csv')
    params = param['Código '].to_list() 
    params = [str(p) for p in params]
    param_dic = param.set_index("Código ")["Nombre parámetro BDCAP"].to_dict()
    param_dic_2 = {str(key): str(value) for key, value in param_dic.items()}
    proc = pd.read_csv('proc.csv')
    procs = proc['Código'].to_list()
    procs = [str(p) for p in procs]
    proc_dic = proc.set_index("Código")["Término abreviado"].to_dict()
    equiv['Procedimiento'] = equiv['CONCEPTID'].apply(lambda x: 1 if x in procs else 0)
    equiv['Variables'] = equiv['CONCEPTID'].apply(lambda x: 1 if x in params else 0)
    print(len(proc_dic.keys()))
    print(len(param_dic_2.keys()))
    # inter = pd.read_csv('interconsulta.csv')
    # inter_dic = proc.set_index("Código")["Descripción"].to_dict()
    equiv['Termino_abrev'] = equiv['CONCEPTID'].apply(lambda x: definicions(x, proc_dic, param_dic_2))
    upload = []
    for i in range(len(equiv)):
        u = [str(equiv['TIPUS'][i]),
            str(equiv['CODI_ECAP'][i]),
            str(equiv['CONCEPTID'][i]),
            str(equiv['ORIGEN_TIPUS_DADES'][i]),
            str(equiv['Termino_abrev'][i]),
            equiv['Procedimiento'][i],
            equiv['Variables'][i]]
        if u[5] == 1 and u[0] not in ('PRO', 'VARIPRO'): u[5] = 0
        if u[6] == 1 and u[0] not in ('VAR', 'VARIPRO'): u[6] = 0
        u = list(u)
        # if u[7] ==1 and u[0] not in ('INT','INTS'): u[7] = 0
        upload.append(u)
    cols = "(tipus varchar(6), codi_ecap varchar2(14), conceptid varchar2(200), origen_dades varchar2(15), \
        des_minis varchar(400), procedimiento int, variables int)"

    createTable('bdcap_cataleg', cols, 'exadata', rm=True)
    listToTable(upload, 'bdcap_cataleg', "exadata")
    grantSelect('bdcap_cataleg','DWSISAP_ROL','exadata')

def do_it(inf):
    p, cips, origen_codi = inf[0], inf[1], inf[2]
    print('multiprocesss iujuuuu')
    procediments = set()
    i = 0
    codis = list(origen_codi.keys())
    sql = """select cr_cip, cr_codi_prova_ics, CR_DATA_REG
                        from sidics.labtb101
                        partition({})""".format(p)
    for id, prova, data in getAll(sql, ("sidics", "x0002")):
        try:
            if prova in codis:
                procediments.add((cips[id], origen_codi[prova], data, 'FICTICIO', 'LABTB101'))
            i += 1
        except:
            pass
        # if i % 10000 == 0:
        #     listToTable(list(procediments), 'bdcap_procedimientos', "exadata")
        #     procediments = set()
    print(len(procediments))
    listToTable(list(procediments), 'bdcap_procedimientos', "exadata")
    print('escrit i acabat')
    return procediments

def get_vacunes(sector):
    origen_codi = defaultdict(dict)
    sql = """SELECT codi_ecap, CONCEPTID
                FROM dwsisap.bdcap_cataleg
                WHERE procedimiento = 1
                AND origen_dades= 'PRSTB051'"""
    for ecap, ministerio in getAll(sql, 'exadata'):
        origen_codi[ecap] = ministerio
    codis = list(origen_codi.keys())
    print('vamos a workear')
    upload = []
    sql = """select VA_U_USUA_CIP, va_u_cod_an, VA_U_DATA_MODI
            from PRSTB051
            WHERE extract(YEAR FROM VA_U_DATA_MODI) = 2020"""
    for id, codi, data in getAll(sql, sector):
        if codi in codis:
            upload.append((id, origen_codi[codi], data, 'FICTICIO', 'PRSTB051'))
    listToTable(upload, 'bdcap_procedimientos', "exadata")
    print('un menysss:))))')

### CREAR PROCEDIMIENTO ###
def procediment(cips):
    print('procediments')
    # cols = "(id varchar2(13), prova varchar2(200), data date, \
    #         episodio varchar2(100), taula varchar2(15))"
    # createTable('bdcap_procedimientos', cols, 'exadata', rm=True)
    # grantSelect('bdcap_procedimientos','DWSISAP_ROL','exadata')

    origen_codi = defaultdict(dict)
    sql = """SELECT codi_ecap, CONCEPTID, origen_dades 
                FROM dwsisap.bdcap_cataleg
                WHERE procedimiento = 1
                OR TIPUS = 'PROVAR'"""
    for ecap, ministerio, origen in getAll(sql, 'exadata'):
        origen_codi[origen][ecap] = ministerio
    
    execute("DELETE dwsisap.bdcap_procedimientos WHERE taula = 'LABTB101'", "exadata")
    
    
    for taula, dict_codis in origen_codi.items():
        print(taula)
        print(dict_codis.keys())

        codis = list(dict_codis.keys())
        if taula == 'LABTB101':
            particions = ['t20200101', 't20200116',
                            't20200201', 't20200216',
                            't20200301', 't20200316',
                            't20200401', 't20200416',
                            't20200501', 't20200516',
                            't20200601', 't20200616',
                            't20200701', 't20200716',
                            't20200801', 't20200816',
                            't20200901', 't20200916',
                            't20201001', 't20201016',
                            't20201101', 't20201116',
                        ]
            print('here')
            proc_results = multiprocess(do_it, [(p, cips, dict_codis) for p in particions], 4, close=True)
            print(len(proc_results))   
        
        # if taula == 'GPITB004':
        #     convert = dict()
        #     sql = """SELECT pro_codi_prova, PRO_CODI_REALITZABLE FROM gcctb003 WHERE PRO_CODI_REALITZABLE IS not NULL"""
        #     for vell, nou in getAll(sql, 'redics'):
        #         convert[vell] = nou
            
        #     codis1 = set([convert[c] if c in convert.keys() else 0 for c in codis])

        #     procediments = []
        #     coses = set()
        #     sql = """SELECT a.oc_data, a.OC_USUA_CIP, b.INF_CODI_PROVA, b.inf_cod_ps
        #                 FROM gpitb104 a, gpitb004 b 
        #                 WHERE b.INF_NUMID=a.OC_NUMID
        #                 AND extract(YEAR FROM a.oc_data) = 2020""" #and rownum <= 500
        #     print(sql)
        #     print(codis1)
        #     # codis vells i nous van al revés!!!!
        #     for data, id, prova, dx in getAll(sql, 'redics'):
        #         coses.add(prova)
        #         try: 
        #             if prova in codis1:
        #                 # print('prova:', prova, 'codi_nou', convert[prova])
        #                 procediments.append((cips[id], dict_codis[prova], data, dx, 'gpitb004'))
        #         except: pass
        #     print(len(procediments))
        #     print(coses)
        #     listToTable(procediments, 'bdcap_procedimientos', "exadata")
        
        # if taula == 'PRSTB051':
        #     multiprocess(get_vacunes, sectors, 12, close=True)
        
        # if taula == 'PRSTB016':
        #     procediments = []
        #     coses = set()
        #     sql = """select AU_COD_U, AU_COD_AC, AU_DAT_ACT
        #             from PRSTB016
        #             WHERE extract(YEAR FROM AU_DAT_ACT) = 2020"""
        #     print(sql)
        #     print(codis)
        #     for id, prova, data in getAll(sql, 'redics'):
        #         coses.add(prova)
        #         if prova in codis:
        #             try:
        #                 procediments.append((cips[id], dict_codis[prova], data, 'FICTICIO', 'PRSTB016'))
        #             except:
        #                 pass
        #     print(len(procediments))
        #     # print(coses)
        #     listToTable(procediments, 'bdcap_procedimientos', "exadata")

        # if taula == 'PRSTB017':
        #     procediments = []
        #     coses = set()
        #     sql = """select VU_COD_U, VU_COD_VS, VU_V_DAT
        #             from PRSTB017
        #             WHERE extract(YEAR FROM VU_V_DAT) = 2020"""
        #     print(sql)
        #     print(codis)
        #     for id, prova, data in getAll(sql, 'redics'):
        #         coses.add(prova)
        #         if prova in codis:
        #             try:
        #                 procediments.append((cips[id], dict_codis[prova], data, 'FICTICIO', 'PRSTB017'))
        #             except:
        #                 pass
        #     print(len(procediments))
        #     print(coses)
        #     listToTable(procediments, 'bdcap_procedimientos', "exadata")

    print(origen_codi.keys())


def do_it2(inf):
    p, cips, dict_codis = inf[0], inf[1], inf[2]
    codis = list(dict_codis.keys())
    print('multiprocesss iujuuuu')
    procediments = []
    i = 0
    sql = """select cr_cip, cr_codi_prova_ics, cr_res_lab, CR_DATA_REG
                        from sidics.labtb101
                        partition({})""".format(p)
    for id, prova, val, data in getAll(sql, ("sidics", "x0002")):
        try:
            if prova in codis:
                if len(val) > 2000: val = val[0:2000]
                procediments.append((cips[id], dict_codis[prova], val, data, 'FICTICIO', 'LABTB101'))
                i += 1
        except:
            pass
        if i % 10000 == 0:
            listToTable(procediments, 'bdcap_variables', "exadata")
            procediments = []
    print(len(procediments))
    listToTable(procediments, 'bdcap_variables', "exadata")
    print('escrit i acabat')
    return procediments

### CREAR VARIABLES ###
def variables(cips):
    print('variables')
    cols = "(id varchar2(40), prova varchar2(200), valor varchar2(2000), data date, \
            episodio varchar2(100), taula varchar2(15))"
    createTable('bdcap_variables', cols, 'exadata', rm=True)
    grantSelect('bdcap_variables','DWSISAP_ROL','exadata')

    origen_codi = defaultdict(dict)
    sql = """SELECT codi_ecap, CONCEPTID, origen_dades 
                FROM dwsisap.bdcap_cataleg
                WHERE variables = 1"""
    for ecap, ministerio, origen in getAll(sql, 'exadata'):
        origen_codi[origen][ecap] = ministerio
    
    
    for taula, dict_codis in origen_codi.items():
        print(taula)
        print(dict_codis.keys())
        
        codis = list(dict_codis.keys())
        if taula == 'LABTB101':
            particions = ['t20200101', 't20200116',
                            't20200201', 't20200216',
                            't20200301', 't20200316',
                            't20200401', 't20200416',
                            't20200501', 't20200516',
                            't20200601', 't20200616',
                            't20200701', 't20200716',
                            't20200801', 't20200816',
                            't20200901', 't20200916',
                            't20201001', 't20201016',
                            't20201101', 't20201116',
                        ]
            print('here')
            proc_results = multiprocess(do_it2, [(p, cips, dict_codis) for p in particions], 4, close=True)
            print(len(proc_results))
        
        if taula == 'PRSTB016':
            procediments = []
            coses = set()
            sql = """select AU_COD_U, AU_COD_AC, AU_VAL, AU_DAT_ACT
                    from PRSTB016
                    WHERE extract(YEAR FROM AU_DAT_ACT) = 2020"""
            print(sql)
            print(codis)
            for id, prova, val, data in getAll(sql, 'redics'):
                coses.add(prova)
                if prova in codis:
                    try:
                        procediments.append((cips[id], dict_codis[prova], val, data, 'FICTICIO', 'PRSTB016'))
                    except:
                        pass
            print(len(procediments))
            # print(coses)
            listToTable(procediments, 'bdcap_variables', "exadata")

        if taula == 'PRSTB017':
            procediments = []
            coses = set()
            sql = """select VU_COD_U, VU_COD_VS, VU_VAL, VU_V_DAT
                    from PRSTB017
                    WHERE extract(YEAR FROM VU_V_DAT) = 2020"""
            print(sql)
            print(codis)
            for id, prova, val, data in getAll(sql, 'redics'):
                coses.add(prova)
                if prova in codis:
                    try:
                        procediments.append((cips[id], dict_codis[prova], val, data, 'FICTICIO', 'PRSTB017'))
                    except:
                        pass
            print(len(procediments))
            # print(coses)
            listToTable(procediments, 'bdcap_variables', "exadata")
    
    print(origen_codi.keys())


#### CREAR EPISODIO ####
def episodio(cips):
    print('episodio')
    cols = "(id varchar2(40), problema_salud varchar2(15), clasificacion int, fecha_apertura date, \
            fecha_cierre date)"
    createTable('bdcap_episodio', cols, 'exadata', rm=True)
    grantSelect('bdcap_episodio','DWSISAP_ROL','exadata')
    i = 0
    upload = []
    sql = """SELECT pr_cod_u, pr_cod_ps, pr_dde, pr_dba 
            FROM prstb015 
            WHERE (extract(YEAR FROM pr_dde) <= 2020 AND pr_dba IS NULL) OR 
            extract(YEAR FROM pr_dba) = 2020"""
    # n'hi ha 21,855,159
    for id, ps, apertura, cierre in getAll(sql, 'redics'):
        try:
            if ps[0:4] == 'C01-':
                ps = ps[4:]
            upload.append((cips[id], ps, 4, apertura, cierre))
            i += 1
        except: pass
        if i % 10000 == 0:
            listToTable(upload, 'bdcap_episodio', "exadata")
            print(i)
            upload = []
    listToTable(upload, 'bdcap_episodio', "exadata")


def get_visites(sector):
    print('vamos a workear')
    id_visites = set()
    sql = """SELECT VISI_ID FROM vistb043 WHERE VISI_SITUACIO_VISITA = 'R'
            AND extract(YEAR FROM to_date(visi_data_visita, 'j')) =2020"""
    for id, in getAll(sql, sector):
        id_visites.add(id)
    upload = []
    sql = """SELECT MC_CIP, MC_DATA, MC_MOTIU, MC_PORTA_ID FROM vistb042 
            WHERE extract(YEAR FROM mc_data) = 2020
            AND mc_porta = 'VISITA'"""
    for id, data, codi, visi in getAll(sql, sector):
        if visi in id_visites:
            if codi[0:4] == 'C01-':
                codi = codi[4:]
            upload.append((id, codi, 4, data))
    listToTable(upload, 'bdcap_visitas', "exadata")
    print('un menysss:))))')


#### CREAR VISITAS ####
def visitas():
    cols = "(id varchar2(40), problema_salud varchar2(40), clasificacion int, fecha_visita date)"
    createTable('bdcap_visitas', cols, 'exadata', rm=True)
    grantSelect('bdcap_visitas','DWSISAP_ROL','exadata')
    multiprocess(get_visites, sectors, 12, close=True)


def interconsultes(cips):
    print('taula')
    cols = "(id varchar2(14), caracter varchar2(3), fecha date)"
    createTable('bdcap_interconsultas', cols, 'exadata', rm=True)
    grantSelect('bdcap_interconsultas','DWSISAP_ROL','exadata')
    print('int')
    ecap_ministerio = dict()
    sql = """SELECT codi_ecap, conceptid
            FROM dwsisap.bdcap_cataleg
            WHERE tipus = 'INT'"""
    for ecap, mini in getAll(sql, 'exadata'):
        ecap_ministerio[ecap] = mini
    print('pritb023')
    sire = set()
    sql = """SELECT ESPE_CODI_SIRE FROM import.cat_pritb023"""
    for codi, in getAll(sql, 'import'):
        sire.add(codi)
    print('info')
    upload = []
    sql = """SELECT a.oc_data, a.OC_USUA_CIP, b.inf_espec_sire
            FROM gpitb104 a, gpitb004 b 
            WHERE b.INF_NUMID=a.OC_NUMID
            AND extract(YEAR FROM a.oc_data) = 2020
            AND inf_espec_sire IS NOT NULL"""
    for data, usuari, espe in getAll(sql, 'redics'):
        if espe in sire and espe in ecap_ministerio.keys():
            try:
                upload.append((cips[usuari], ecap_ministerio[espe], data))
            except:
                pass
    print('afegir dades 1')
    listToTable(upload, 'bdcap_interconsultas', "exadata")

    print('ints')
    ecap_ministerio = dict()
    sql = """SELECT codi_ecap, conceptid
            FROM dwsisap.bdcap_cataleg
            WHERE tipus = 'INTS'"""
    for ecap, mini in getAll(sql, 'exadata'):
        ecap_ministerio[ecap] = mini
    print('pritb000')
    sire = set()
    sql = """SELECT rv_low_value
                FROM import.cat_pritb000 
                WHERE rv_TABLE ='GPITB004' 
                AND RV_COLUMN ='INF_SERVEI_D_CODI'"""
    for codi, in getAll(sql, 'import'):
        sire.add(codi)
    
    print('info')
    upload = []
    sql = """SELECT a.oc_data, a.OC_USUA_CIP, b.inf_servei_d_codi
            FROM gpitb104 a, gpitb004 b 
            WHERE b.INF_NUMID=a.OC_NUMID
            AND extract(YEAR FROM a.oc_data) = 2020
            AND inf_servei_d_codi IS NOT NULL"""
    for data, usuari, espe in getAll(sql, 'redics'):
        if espe in sire and espe in ecap_ministerio.keys():
            try:
                upload.append((cips[usuari], ecap_ministerio[espe], data))
            except:
                pass
    print('afegir dades 2')
    listToTable(upload, 'bdcap_interconsultas', "exadata")

# def delete_procediments_duplicats():
#     print('creem')
#     cols = "(id varchar2(13), prova varchar2(200), data date, \
#             episodio varchar2(100), taula varchar2(15))"
#     createTable('bdcap_procedimientos1', cols, 'exadata', rm=True)
#     grantSelect('bdcap_procedimientos1','DWSISAP_ROL','exadata')
#     upload = set()
#     print('busquem')
#     sql = """SELECT id, prova, to_DATE(DATA), episodio, taula
#                 FROM dwsisap.bdcap_procedimientos"""
#     for id, prova, data, epi, taula in getAll(sql, 'exadata'):
#         upload.add((id, prova, data, epi, taula))
#     print('penge,')
#     listToTable(list(upload), 'bdcap_procedimientos1', 'exadata')
    
    


if __name__ == "__main__":
    # pacients()
    # cataleg()
    cips = get_cips()
    # cips = []
    # procediment(cips)
    # variables(cips) 
    episodio(cips) # FET
    # visitas() # FET
    # interconsultes(cips) # FET
