from sisapUtils import sectors, getAll, createTable, listToTable, grantSelect, calcStatistics


table = 'sisap_professio'
db = 'redics'


centres = {up: br for (up, br) in getAll('select scs_codi, ics_codi from cat_centres', 'nodrizas')}

upload = []
sql = "select ilt_up_a, ilt_professio, count(1) from prttb105 where to_char(ilt_data_baixa, 'YYYY') = 2015 and ilt_professio is not null group by ilt_up_a, ilt_professio"
for sector in sectors:
    for up, prof, n in getAll(sql, sector):
        if up in centres:
            upload.append([centres[up], prof, n])

createTable(table, '(eap varchar2(5), professio varchar2(6), n int)', db, rm=True)
listToTable(upload, table, db)
grantSelect(table, ('PREDUMMP', 'PREDULMB'), db)
calcStatistics(table, db)
