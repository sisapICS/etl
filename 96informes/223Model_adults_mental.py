# coding: utf8

"""Petició de mirar malaltia mental pel model de adults... De moment ho faig amb dades actuals"""

import urllib as w
import os

import sisapUtils as u
from collections import defaultdict,Counter

pat_mental= "('P71','P72','P73','P74','P76','P76','P79','P80','P82','P85','P98')"

db = 'test'

class Frequentacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_patmental()
        self.get_pob()
        self.export_data()        
        
    def get_centres(self):
        """centres ICS"""
        self.centres = {}
        sql = "select scs_codi, ics_codi from cat_centres where ep='0208'"
        for up, br in u.getAll(sql, 'nodrizas'):
            self.centres[up] = br
    
    def get_patmental(self):
        """Pacients amb patologia mental a final del període."""
        pmentalciap = {}
        self.dades = {}
        sql = "select codi_cim10, codi_ciap from import.cat_md_ct_cim10_ciap where codi_ciap in {}".format(pat_mental)
        for cim10, ciap in u.getAll(sql, "import"):
            pmentalciap[cim10] = True
   
        in_crit = tuple(pmentalciap)
        
        sql = ("select id_cip_sec from problemes,  nodrizas.dextraccio \
                where  pr_cod_ps in {} \
                   and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext) and (pr_dba = 0 or pr_dba > data_ext)".format(in_crit),
               "import")
        for id, in u.getAll(*sql):
            self.dades[(id)] = 1
            
    def get_pob(self):
        """Agafem assignada actual"""
        self.mentals = Counter()
        sql = "select id_cip_sec, up from assignada_tot where edat>14"
        for id, up in u.getAll(sql, "nodrizas"):
            if up in self.centres:
                self.mentals[(up, 'DEN')] +=1
                num = 0
                if (id) in self.dades:
                    num = 1
                self.mentals[(up, 'NUM')] += num
            
    def export_data(self):
        """."""
        upload = []
        for (up, tip), n in self.mentals.items():
            if tip == 'DEN':
                num = self.mentals[(up, 'NUM')]
                upload.append([up, num, n])
                
        tb = "model_adults_mental"
        columns =  ["up varchar(10)", "num int", "den int"]     
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
    
        u.listToTable(upload, tb, db)             


if __name__ == '__main__':
    Frequentacio()