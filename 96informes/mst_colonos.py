import sisapUtils as u
import datetime as d
import collections as c
import sisaptools as t

u.printTime('inici')
def create_tables():
    # sql = """
    #     CREATE TABLE hosp_proves AS
    #     SELECT *
    #     FROM dwsisap.master_proves
    #     WHERE inf_codi_prova in ('PD00069', 'RA01172', 'RA00303','RA00304','RA00305','RA00306','RA00307','RA00308','RA00309','RA00310','RA00311','RA00312','RA00313','RA00314','RA00315','RA00316','RA00317','RA00318','RA00319','RA00320','RA00321','RA00322','RA00323','RA00324','RA00325','RA00326','RA00327','RA00328','RA00329','RA00330','RA00331','RA00332','RA00333','RA00334','RA00335','RA00336','RA00337','RA00338','RA00339','RA00340','RA00341','RA00342','RA00343','RA00344','RA00345','RA00346','RA00347','RA00348','RA00349','RA00350','RA00351','RA00352','RA00353','RA00354','RA00355','RA00356','RA00357','RA00358','RA00359','RA00360','RA00361','RA00362','RA00364','RA00365','RA00366','RA00367','RA00368','RA00369','RA00370','RA00371','RA00373','RA00374','RA00375','RA00377','RA00378','RA00379','RA00380','RA00381','RA00382','RA00383','RA00384','RA00385','RA00387','RA00388','RA00389','RA00390','RA00391','RA00392','RA00393','RA00394','RA00395','RA00396','RA00398','RA00399','RA00402','RA00403','RA00404','RA00405','RA00406','RA00407','RA00408','RA00409','RA00410','RA00411','RA00633','RA00635','RA00651','RA00652','RA00653','RA00654','RA00656','RA00657','RA00658','RA00659','RA00660','RA00661','RA00685','RA00686','RA00687','RA00688','RA00689','RA00690','RA00692','RA00693','RA00694','RA00695','RA00696','RA00697','RA00706','RA00707','RA00708','RA00709','RA00710','RA00711','RA00712','RA00832','RA01060','RA01105','RA01139','RA01157','RA01158','RA01159','RA01160','RA01161','RA01162','RA01163','RA01187','RA01212','RA01264','RA01270')
    # """
    # u.execute(sql, 'exadata')
    # u.printTime('penjat')
    # u.grantSelect('hosp_proves', 'DWSISAP_ROL', 'exadata')
    # u.printTime('fi')

    # sql = """
    #     CREATE TABLE hosp_der_prest AS
    #     SELECT *
    #     FROM dwfactics.derivacions_prestacions
    #     WHERE prestacio_final in ('PD00069', 'RA01172', 'RA00303','RA00304','RA00305','RA00306','RA00307','RA00308','RA00309','RA00310','RA00311','RA00312','RA00313','RA00314','RA00315','RA00316','RA00317','RA00318','RA00319','RA00320','RA00321','RA00322','RA00323','RA00324','RA00325','RA00326','RA00327','RA00328','RA00329','RA00330','RA00331','RA00332','RA00333','RA00334','RA00335','RA00336','RA00337','RA00338','RA00339','RA00340','RA00341','RA00342','RA00343','RA00344','RA00345','RA00346','RA00347','RA00348','RA00349','RA00350','RA00351','RA00352','RA00353','RA00354','RA00355','RA00356','RA00357','RA00358','RA00359','RA00360','RA00361','RA00362','RA00364','RA00365','RA00366','RA00367','RA00368','RA00369','RA00370','RA00371','RA00373','RA00374','RA00375','RA00377','RA00378','RA00379','RA00380','RA00381','RA00382','RA00383','RA00384','RA00385','RA00387','RA00388','RA00389','RA00390','RA00391','RA00392','RA00393','RA00394','RA00395','RA00396','RA00398','RA00399','RA00402','RA00403','RA00404','RA00405','RA00406','RA00407','RA00408','RA00409','RA00410','RA00411','RA00633','RA00635','RA00651','RA00652','RA00653','RA00654','RA00656','RA00657','RA00658','RA00659','RA00660','RA00661','RA00685','RA00686','RA00687','RA00688','RA00689','RA00690','RA00692','RA00693','RA00694','RA00695','RA00696','RA00697','RA00706','RA00707','RA00708','RA00709','RA00710','RA00711','RA00712','RA00832','RA01060','RA01105','RA01139','RA01157','RA01158','RA01159','RA01160','RA01161','RA01162','RA01163','RA01187','RA01212','RA01264','RA01270')
    # """
    # u.execute(sql, 'exadata')
    # u.printTime('penjat')
    # u.grantSelect('hosp_der_prest', 'DWSISAP_ROL', 'exadata')
    # u.printTime('fi')

    # sql = """
    #     CREATE TABLE master_hosp_tr AS
    #     SELECT a.CODI_SECTOR,
    #             a.HASH,
    #             a.EDAT,
    #             a.SEXE,
    #             a.DER_NUM_SOL,
    #             a.OC_NUMID,
    #             a.OC_UP_ORI,
    #             a.OC_UP_ORI_DESC,
    #             a.OC_SERVEI_ORI as OC_SERVEI_ORI_DER,
    #             a.OC_DATA,
    #             a.OC_COLLEGIAT,
    #             a.OC_DNI_PROFESSIONAL,
    #             a.INF_PRIORITAT as INF_PRIORITAT_DER,
    #             a.INF_DATA_ALTA,
    #             a.INF_DATA_BAIXA,
    #             a.INF_CODI_PROVA,
    #             a.INF_DES_PROVA,
    #             a.AGRUPADOR_PROVA as AGRUPADOR_PROVA_DER,
    #             a.AGRUPADOR_NIVELL_DOS,
    #             a.AGRUPADOR_TIPUS,
    #             a.AGRUPADOR_MEDI,
    #             a.AGRUPADOR_PACIENT,
    #             a.INF_SERVEI_D_CODI AS INF_SERVEI_D_CODI_DER,
    #             a.INF_SERVEI_D_CODI_DESC AS INF_SERVEI_D_CODI_DESC_DER,
    #             a.INF_ESPEC_SIRE,
    #             a.DER_ESPE_SIRE,
    #             a.CODI_ESPECIALITAT_FINAL,
    #             a.ESPECIALITAT_FINAL,
    #             a.DER_SERVEI_D,
    #             a.INF_RELACIO_DERIV,
    #             a.DER_COD_PR,
    #             a.OC_CODIS_PROBLEMES,
    #             a.DER_DATA_ALTA,
    #             a.DER_DATA_PROG,
    #             a.DER_DATA_REAL,
    #             a.DER_DATA_BAIX,
    #             a.GPI_MOTIU_BAIXA,
    #             a.GPI_MOTIU_BAIXA_DESC,
    #             a.AGRUPADOR_REBUIG,
    #             a.DER_ESTAT,
    #             a.DER_UP_SCS_DES,
    #             a.DER_UP_SCS_DES_DESC,
    #             a.DER_TIPUS_FLUXE,
    #             a.FIT_DESC,
    #             a.FIT_DIRECTORI,
    #             a.FIT_TIPUS,
    #             a.FIT_EXTENSIO,
    #             a.FLAG_COMPTA_GPI_REL_PETICIO,
    #             a.FLAG_PETICIO_ANULLADA,
    #             a.FLAG_PETICIO_SOLLICITADA,
    #             a.FLAG_PETICIO_TRAMITADA,
    #             a.FLAG_PETICIO_PROGRAMADA,
    #             a.FLAG_PETICIO_REALITZADA,
    #             a.FLAG_PETICIO_REBUTJADA,
    #             a.FLAG_PETICIO_PENDENT,
    #             a.FLAG_IS3_ADICIONAL,
    #             a.FLAG_IMATGE_ADJ,
    #             a.FLAG_DOCUMENT_ADJ,
    #             a.FLAG_ANATOMIA_PAT,
    #             a.FLAG_FORMULARI,
    #             a.DIES_DEMORA,
    #             b.CENTRE,
    #             b.NUM_PRESTACIO,
    #             b.UP_ORIGIN,
    #             b.EXT_PRGNR,
    #             b.ASYNCHRON,
    #             b.SYSTEM_ORIGIN,
    #             b.PRESTACIO_ORIGINAL,
    #             b.IS_CONT,
    #             b.TOKEN_FORM,
    #             b.COC,
    #             b.ERDAT,
    #             b.ERTIM,
    #             b.ESTAT_PRESTACIO,
    #             b.DATA_PRESTACIO,
    #             b.UT_PRESTACIO,
    #             b.SERVEI_PRESTACIO,
    #             b.PRESTACIO_FINAL,
    #             b.UP_ORIGIN_DESC,
    #             b.OC_SERVEI_ORI,
    #             b.INF_PRIORITAT,
    #             b.AGRUPADOR_PROVA,
    #             b.INF_SERVEI_D_CODI,
    #             b.INF_SERVEI_D_CODI_DESC
    #     FROM dwsisap.hosp_proves a
    #     FULL JOIN dwsisap.hosp_der_prest b
    #     ON a.der_num_sol = b.ext_prgnr
    # """

    # u.execute(sql, 'exadata')
    # u.printTime('penjat')
    # u.grantSelect('master_hosp_tr', 'DWSISAP_ROL', 'exadata')
    # u.printTime('fi')
    # sql = """
    #     CREATE TABLE hosp_prestacions AS
    #     SELECT c.hash_covid, a.*,
    #     b.PAT_NHC,
    #     b.PAT_CIP,
    #     b.PAT_CREDT,
    #     b.PAT_CREUS,
    #     b.PAT_UPDATE,
    #     b.PAT_UPUSER,
    #     b.PAT_BRDAY,
    #     b.PAT_BIRCITY,
    #     b.PAT_BIRCTRY,
    #     b.PAT_DEACAU,
    #     b.PAT_DEAIND,
    #     b.PAT_DEADATE,
    #     b.PAT_DEATIME,
    #     b.PAT_ORGAN,
    #     b.PAT_HOSP,
    #     b.PAT_RELIG,
    #     b.PAT_GENDER,
    #     b.PAT_NATION,
    #     b.PAT_PAIS,
    #     b.PAT_POSTAL,
    #     b.PAT_REGION,
    #     b.PAT_ABS,
    #     b.PAT_ABS2,
    #     b.PAT_MUNIC,
    #     b.PAT_LOCAL,
    #     b.PAT_COMAR,
    #     b.CIP_USUARI_CIP,
    #     b.Z76,
    #     b.ALTA_Z76,
    #     b.BAIXA_Z76,
    #     b.PCC,
    #     b.ALTA_PCC,
    #     b.BAIXA_PCC,
    #     b.MACA,
    #     b.ALTA_MACA,
    #     b.BAIXA_MACA,
    #     b.CRG_COD,
    #     b.CRG_ANY,
    #     b.Z59,
    #     b.ALTA_Z59,
    #     b.BAIXA_Z59,
    #     b.ATDOM,
    #     b.ALTA_ATDOM,
    #     b.BAIXA_ATDOM,
    #     b.GMA_PNIV,
    #     b.GMA_COD,
    #     b.GMA_IND_CMPLX,
    #     b.PAT_NIA,
    #     b.PERSON_SITUATION,
    #     b.COD_SNS
    #     FROM dwfactics.prestacions a
    #     INNER JOIN dwdimics.pacients b
    #     ON a.mce_nhc = b.pat_nhc
    #     INNER JOIN dwsisap.pdptb101_relacio_nia c
    #     ON substr(b.pat_cip, 1, 13) = c.cip
    #     WHERE mce_presta in ('PD00069', 'RA01172', 'RA00303','RA00304','RA00305','RA00306','RA00307','RA00308','RA00309','RA00310','RA00311','RA00312','RA00313','RA00314','RA00315','RA00316','RA00317','RA00318','RA00319','RA00320','RA00321','RA00322','RA00323','RA00324','RA00325','RA00326','RA00327','RA00328','RA00329','RA00330','RA00331','RA00332','RA00333','RA00334','RA00335','RA00336','RA00337','RA00338','RA00339','RA00340','RA00341','RA00342','RA00343','RA00344','RA00345','RA00346','RA00347','RA00348','RA00349','RA00350','RA00351','RA00352','RA00353','RA00354','RA00355','RA00356','RA00357','RA00358','RA00359','RA00360','RA00361','RA00362','RA00364','RA00365','RA00366','RA00367','RA00368','RA00369','RA00370','RA00371','RA00373','RA00374','RA00375','RA00377','RA00378','RA00379','RA00380','RA00381','RA00382','RA00383','RA00384','RA00385','RA00387','RA00388','RA00389','RA00390','RA00391','RA00392','RA00393','RA00394','RA00395','RA00396','RA00398','RA00399','RA00402','RA00403','RA00404','RA00405','RA00406','RA00407','RA00408','RA00409','RA00410','RA00411','RA00633','RA00635','RA00651','RA00652','RA00653','RA00654','RA00656','RA00657','RA00658','RA00659','RA00660','RA00661','RA00685','RA00686','RA00687','RA00688','RA00689','RA00690','RA00692','RA00693','RA00694','RA00695','RA00696','RA00697','RA00706','RA00707','RA00708','RA00709','RA00710','RA00711','RA00712','RA00832','RA01060','RA01105','RA01139','RA01157','RA01158','RA01159','RA01160','RA01161','RA01162','RA01163','RA01187','RA01212','RA01264','RA01270')
    # """
    # u.execute(sql, 'exadata')
    # u.printTime('penjat')
    # u.grantSelect('hosp_prestacions', 'DWSISAP_ROL', 'exadata')
    # u.printTime('fi')
    # s'ha de fer una taula intermitja amb prestacions que nomes continguin colonoscopies, perque sino agafa totes les prestacions de dwfactics.prestacions
    # sql = """
    # CREATE TABLE master_prestacions_hospitals_tr AS
    #     SELECT a.*, b.*,
    #         case when MCE_ST_PR in('RE') then
    #             MCE_DCURS - MCE_DCREPRES
    #         WHEN MCE_ST_PR IN ('CO') THEN
    #             MCE_DCONC - MCE_DCREPRES
    #         ELSE 0 END AS dies_espera_cal,
    #         CASE WHEN mce_st_pr in ('DI','CF','SN','EC') THEN
    #             Sysdate - mce_dcrepres
    #         ELSE 0 END AS dies_demora_cal
    #     FROM dwsisap.master_hosp_tr a
    #     FULL JOIN dwsisap.hosp_prestacions b
    #     ON a.num_prestacio = b.mce_npre
    #     AND a.centre = b.mce_centre
    # """

    # u.execute(sql, 'exadata')
    # u.printTime('penjat')
    # u.grantSelect('master_prestacions_hospitals_tr', 'DWSISAP_ROL', 'exadata')
    # u.printTime('fi')

    # sql = """
    #     CREATE TABLE master_colonoscopies_dates AS
    #     SELECT a.*,
    #         case when MCE_ST_PR in('RE') then
    #             MCE_DCURS - MCE_DCREPRES
    #         WHEN MCE_ST_PR IN ('CO') THEN
    #             MCE_DCONC - MCE_DCREPRES
    #         ELSE 0 END AS dies_espera_cal,
    #         CASE WHEN mce_st_pr in ('DI','CF','SN','EC') THEN
    #             Sysdate - mce_dcrepres
    #         ELSE 0 END AS dies_demora_cal
    #     FROM dwsisap.master_colonoscopies a
    # """
    # u.execute(sql, 'exadata')
    # u.printTime('penjat')
    # u.grantSelect('master_colonoscopies_dates', 'DWSISAP_ROL', 'exadata')
    # u.printTime('fi')

    # sql = """
    # CREATE TABLE reprogramacions_hosp as
    #     SELECT ltrim(srvseq, '0') as num_prestacio, repro_mot_desc as motiu, mot_repr_agr as motiu_agr
    #     FROM dwfactics.reprogramacions a
    #     INNER JOIN dwdimics.mot_reprogramacions b
    #     ON a.motrpr = b.id_repro
    #     WHERE a.presta in ('PD00069', 'RA01172', 'RA00303','RA00304','RA00305','RA00306','RA00307','RA00308','RA00309','RA00310','RA00311','RA00312','RA00313','RA00314','RA00315','RA00316','RA00317','RA00318','RA00319','RA00320','RA00321','RA00322','RA00323','RA00324','RA00325','RA00326','RA00327','RA00328','RA00329','RA00330','RA00331','RA00332','RA00333','RA00334','RA00335','RA00336','RA00337','RA00338','RA00339','RA00340','RA00341','RA00342','RA00343','RA00344','RA00345','RA00346','RA00347','RA00348','RA00349','RA00350','RA00351','RA00352','RA00353','RA00354','RA00355','RA00356','RA00357','RA00358','RA00359','RA00360','RA00361','RA00362','RA00364','RA00365','RA00366','RA00367','RA00368','RA00369','RA00370','RA00371','RA00373','RA00374','RA00375','RA00377','RA00378','RA00379','RA00380','RA00381','RA00382','RA00383','RA00384','RA00385','RA00387','RA00388','RA00389','RA00390','RA00391','RA00392','RA00393','RA00394','RA00395','RA00396','RA00398','RA00399','RA00402','RA00403','RA00404','RA00405','RA00406','RA00407','RA00408','RA00409','RA00410','RA00411','RA00633','RA00635','RA00651','RA00652','RA00653','RA00654','RA00656','RA00657','RA00658','RA00659','RA00660','RA00661','RA00685','RA00686','RA00687','RA00688','RA00689','RA00690','RA00692','RA00693','RA00694','RA00695','RA00696','RA00697','RA00706','RA00707','RA00708','RA00709','RA00710','RA00711','RA00712','RA00832','RA01060','RA01105','RA01139','RA01157','RA01158','RA01159','RA01160','RA01161','RA01162','RA01163','RA01187','RA01212','RA01264','RA01270')
    # """
    # u.execute(sql, 'exadata')
    # u.printTime('penjat')
    # u.grantSelect('reprogramacions_hosp', 'DWSISAP_ROL', 'exadata')
    # u.printTime('fi')

    sql = """
        CREATE TABLE master_prestacions_hospitals AS
        SELECT a.*, b.motiu, b.motiu_agr
        FROM dwsisap.master_prestacions_hospitals_tr a
        LEFT JOIN dwsisap.reprogramacions_hosp b
        ON a.mce_npre = b.num_prestacio
    """

    u.execute(sql, 'exadata')
    u.printTime('penjat')
    u.grantSelect('master_prestacions_hospitals', 'DWSISAP_ROL', 'exadata')
    u.printTime('fi')

create_tables()

sql = """
    SELECT hash, hash_covid, der_num_sol, centre, num_prestacio, ext_prgnr, mce_centre, mce_npre,
        oc_data, mce_dsol, mce_dcurs, mce_dconc, mce_data, der_data_prog, der_data_real, data_prestacio, mce_st_pr, der_estat
    FROM dwsisap.master_prestacions_hospitals
"""
dades = [row for row in u.getAll(sql, 'exadata')]
u.printTime('dades')

upload = []
for hash, hash_covid, der_num_sol, centre, num_prestacio, ext_prgnr, mce_centre, mce_npre, oc_data, mce_dsol, mce_dcurs, mce_dconc, mce_data, der_data_prog, der_data_real, data_prestacio, mce_st_pr, der_estat in dades:
    inici = None
    fi = None
    if hash:
        inici = oc_data
        if hash_covid:
            if mce_st_pr == 'RE':
                fi = mce_dcurs
            elif mce_st_pr == 'CO':
                fi = mce_dconc
            else: # no realitzades
                fi = mce_data
        else:
            if der_estat == 'REAL':
                fi = der_data_real
            elif der_data_prog:
                fi = der_data_prog
            elif data_prestacio:
                fi = data_prestacio
            else:
                fi = d.datetime.today()
    elif hash_covid:
        inici = mce_dsol
        if mce_st_pr == 'RE':
            fi = mce_dcurs
        elif mce_st_pr == 'CO':
            fi = mce_dconc
        else: # no realitzades
            fi = mce_data
    upload.append((hash, hash_covid, der_num_sol, centre, num_prestacio, ext_prgnr, mce_centre, mce_npre, inici, fi))
u.printTime('cuinetes')
cols = "(hash varchar2(40), hash_covid varchar2(40), der_num_sol varchar2(17), centre varchar2(3), num_prestacio varchar2(10), ext_prgnr varchar2(150), mce_centre varchar2(3), mce_npre varchar2(10), inici date, fi date)"
u.createTable('dates_prestacions', cols, 'exadata', rm=True)
u.listToTable(upload, 'dates_prestacions', 'exadata')
u.printTime('fi')
u.grantSelect('dates_prestacions', 'DWSISAP_ROL', 'exadata')
sql = """
    CREATE TABLE master_prestacions_hospitals_dates_2 AS
    SELECT CASE WHEN oc_up_ori is not null THEN oc_up_ori
            WHEN mce_upsol is not null THEN mce_upsol
            ELSE null END as UP_SOL,
            CASE WHEN oc_data is not null THEN oc_data
            WHEN mce_dsol is not null THEN mce_dsol
            ELSE null END as DATA_SOL,
    a.*, fi - inici as DIES_GLOBAL
    FROM dwsisap.master_prestacions_hospitals a
    INNER JOIN DWSISAP.dates_prestacions b
    ON a.hash = b.hash
    AND a.hash_covid = b.hash_covid
    AND a.der_num_sol = b.der_num_sol
    AND a.centre = b.centre
    AND a.num_prestacio = b.num_prestacio
    AND a.ext_prgnr = b.ext_prgnr
    AND a.mce_centre = b.mce_centre
    AND a.mce_npre = b.mce_npre
    WHERE a.hash is not null AND a.hash_covid is not null
    UNION
    SELECT CASE WHEN oc_up_ori is not null THEN oc_up_ori
            WHEN mce_upsol is not null THEN mce_upsol
            ELSE null END as UP_SOL,
            CASE WHEN oc_data is not null THEN oc_data
            WHEN mce_dsol is not null THEN mce_dsol
            ELSE null END as DATA_SOL,
            a.*, fi - inici
    FROM dwsisap.master_prestacions_hospitals a
    INNER JOIN DWSISAP.dates_prestacions b
    ON a.hash = b.hash
    AND a.der_num_sol = b.der_num_sol
    WHERE a.hash is not null AND a.hash_covid is null
    UNION
    SELECT CASE WHEN oc_up_ori is not null THEN oc_up_ori
            WHEN mce_upsol is not null THEN mce_upsol
            ELSE null END as UP_SOL,
            CASE WHEN oc_data is not null THEN oc_data
            WHEN mce_dsol is not null THEN mce_dsol
            ELSE null END as DATA_SOL,
            a.*, fi - inici
    FROM dwsisap.master_prestacions_hospitals a
    INNER JOIN DWSISAP.dates_prestacions b
    ON a.hash_covid = b.hash_covid
    AND a.mce_centre = b.mce_centre
    AND a.mce_npre = b.mce_npre
    WHERE a.hash is null AND a.hash_covid is not null
"""
u.execute(sql, 'exadata')
u.printTime('fi')
u.grantSelect('master_prestacions_hospitals_dates_2', 'DWSISAP_ROL', 'exadata')
sql = "drop table dwsisap.master_prestacions_hospitals"
u.execute(sql, 'exadata')
sql = "alter table master_prestacions_hospitals_dates_2 rename to master_prestacions_hospitals"
u.execute(sql, 'exadata')
# tarda molt (s'ha de veure per que)
# sql = """
#     CREATE TABLE neo_colonos_pre AS
#     SELECT id, DATA
#     FROM dwtw.problemes
#     WHERE codi like '%C18%'
#     OR codi like '%D01%'
# """
# u.execute(sql, 'exadata')
# u.printTime('fi')
# u.grantSelect('neo_colonos_pre', 'DWSISAP_ROL', 'exadata')

# sql = """
#     CREATE TABLE neo_colonos AS
#     SELECT HASH_COVID, DATA
#     FROM dwsisap.neo_colonos_pre a
#     INNER JOIN dwsisap.pdptb101_relacio_nia b
#     ON a.id = b.cip
# """
# u.execute(sql, 'exadata')
# u.printTime('fi')
# u.grantSelect('neo_colonos', 'DWSISAP_ROL', 'exadata')

# sql = """
#     CREATE TABLE master_prestacions_hospitals_neos AS
#     SELECT b.data as NEO_COLON, a.*
#     FROM dwsisap.master_prestacions_hospitals a
#     LEFT JOIN dwsisap.neo_colonos b
#     ON a.hash = b.hash_covid
#     WHERE a.hash is not null
#     UNION
#     SELECT b.data, a.*
#     FROM dwsisap.master_prestacions_hospitals a
#     LEFT JOIN dwsisap.neo_colonos b
#     ON a.hash_covid = b.hash_covid
#     WHERE a.hash_covid is not null
# """
# u.execute(sql, 'exadata')
# u.printTime('fi')
# u.grantSelect('master_prestacions_hospitals_neos', 'DWSISAP_ROL', 'exadata')