from sisapUtils import getAll, writeCSV, tempFolder, getSubTables, multiprocess, getOne
from collections import defaultdict


db = 'import'


def get_data(params):
    dat, var_tb, act_tb = params
    var = set([id for id, in getAll("select id_cip_sec from {} where vu_cod_vs = 'EP2700'".format(var_tb), db)])
    act = set([id for id, in getAll("select id_cip_sec from {} where au_cod_ac = 'ATA2' and au_val <> ''".format(act_tb), db)])
    resul = (dat, len(var), len(act), len(var.union(act)))
    return resul


if __name__ == '__main__':
    taules = defaultdict(dict)
    for concepte in ('variables', 'activitats'):
        for taula in getSubTables(concepte):
            dat, = getOne("select date_format({}u_dat_act, '%Y%m') from {} limit 1".format(concepte[0], taula), db)
            if dat > '201099':
                taules[dat][concepte] = taula
    jobs = [(dat, d['variables'], d['activitats']) for dat, d in taules.items()]
    resul = multiprocess(get_data, jobs, 8)
    writeCSV(tempFolder + 'tabac_variables.csv', [('DATA', 'EP2700', 'ATA2', 'COMBINAT')] + sorted(resul), sep=';')
