# coding: latin 1

import collections as c
import datetime as d

import sisapUtils as u

# METADONA
def metadona():
    PERIODE = 2023
    FARMACS = {"N07BC02": "metadona", "N07BC51": "buprenorfina/naloxona",
            "N07BC01": "bufrenorfina retardada"}

    participants = set()
    sql = "select b.cip_usuari_cip, trunc(a.huab_data_ass), \
                trunc(nvl(a.huab_data_final, sysdate)) \
        from vistb050 a \
        inner join usutb011 b on a.huab_usua_cip = b.cip_cip_anterior \
        where trunc(nvl(a.huab_data_final, sysdate)) >= to_date ('{}0101', 'YYYYMMDD')"  # noqa
    for cip, ini, fi in u.Database("6951", "data").get_all(sql.format(PERIODE)):
        if any([(ini + d.timedelta(days=i)).year == PERIODE
                for i in range((fi - ini).days)]):
            participants.add(cip)

    prenen = c.defaultdict(set)
    sql = "select b.cip_usuari_cip, a.ppfmc_atccodi, a.ppfmc_pmc_data_ini, \
                a.ppfmc_data_fi \
        from ppftb016 a \
        inner join usutb011 b on a.ppfmc_pmc_usuari_cip = b.cip_cip_anterior \
        where ppfmc_atccodi in {} and \
                ppfmc_data_fi >= to_date('{}0101', 'YYYYMMDD')".format(tuple(FARMACS), PERIODE)  # noqa
    for cip, atc, ini, fi in u.Database("6951", "data").get_all(sql):
        if cip in participants:
            if any([(ini + d.timedelta(days=i)).year == PERIODE
                    for i in range((fi - ini).days)]):
                prenen[FARMACS[atc]].add(cip)

    qualsevol = set()
    print("pacients: ", len(participants))
    for key, pacs in prenen.items():
        print(": ".join((key, str(len(pacs)))))
        qualsevol |= pacs
    print("qualsevol: ", len(qualsevol))



# BENZOS
"""
Interns que han ingressat a pres� procedent d�estar en llibertat (�S a dir si provenia d�una altra pres� no s�han d�incloure) en els darrers 12 mesos
Que no tinguin prescripci� activa de benzodiacepines en el moment de l�ingr�S (descartar que no la tinguessin activa en altres sectors fora del sector de presons)
Que se li hagi prescrit alguna benzodiacepina en el moment de l�ingr�S o en els dos dies posteriors
"""
def benzos():
    FARMACS = {"N03AE01": "Clonazepam",
            "N05BA01": "Diazepam ",
            "N05BA02": "Clordiazepoxido",
            "N05BA04": "Oxazepam",
            "N05BA05": "Clorazepato de potasio",
            "N05BA06": "Lorazepam",
            "N05BA08": "Bromazepam",
            "N05BA09": "Clobazam",
            "N05BA10": "Ketazolam",
            "N05BA11": "Desaparegut",
            "N05BA12": "Alprazolam",
            "N05BA13": "Halazepam",
            "N05BA14": "Pinazepam",
            "N05BA21": "Clotiazepam",
            "N05BA24": "Bentazepam",
            "N05BA51": "Diazepam, combinaciones con",
            "N05BA55": "Clorazepato dipotasico, combinaciones con",
            "N05BA91": "Bentazepam",
            "N05CD01": "Flurazepam",
            "N05CD02": "Nitrazepam",
            "N05CD03": "Flunitrazepam",
            "N05CD05": "Triazolam",
            "N05CD06": "Lormetazepam",
            "N05CD08": "Midazolam",
            "N05CD09": "Brotizolam",
            "N05CD10": "Quazepam",
            "N05CD11": "Loprazolam",
            "N05CF01": "Zopiclona",
            "N05CF02": "Zolpidem",
            "N05CF03": "Zaleplon"}

    ANSIETAT = {
            "C01-F40.00": "AGORAF�BIA, NO ESPECIFICADA",
            "C01-F40.10": "F�BIA SOCIAL, NO ESPECIFICADA",
            "C01-F40.298": "ALTRES F�BIES ESPECIFICADES",
            "C01-F40.8": "ALTRES TRASTORNS D'ANSIETAT F�BICA",
            "C01-F40.9": "TRASTORN D'ANSIETAT F�BICA NO ESPECIFICAT",
            "C01-F41": "ALTRES TRASTORNS D'ANSIETAT",
            "C01-F41.0": "TRASTORN DE P�NIC [ANSIETAT PAROXISMAL EPIS�DICA]",
            "C01-F41.1": "TRASTORN D'ANSIETAT GENERALITZADA",
            "C01-F41.3": "ALTRES TRASTORNS D'ANSIETAT MIXTOS",
            "C01-F41.8": "ALTRES TRASTORNS D'ANSIETAT ESPECIFICATS",
            "C01-F41.9": "TRASTORN D'ANSIETAT NO ESPECIFICAT",
            "F40": "TRASTORNS F�BICS D'ANSIETAT",
            "F40.0": "AGROF�BIA",
            "F40.1": "Desaparegut",
            "F40.2": "Desaparegut",
            "F40.8": "ALTRES TRASTORNS F�BICS D'ANSIETAT",
            "F40.9": "TRASTORN F�BIA D'ANSIETAT, NO ESPECIFICAT",
            "F41": "ALTRES TRASTORNS D'ANSIETAT",
            "F41.0": "TRASTORN DE P�NIC (ANSIETAT PAROXISMAL EPIS�DICA)",
            "F41.1": "TRASTORN D'ANSIETAT GENERALITZAT",
            "F41.2": "TRASTORNS MIXTOS D'ANSIETAT I DEPRESSI�",
            "F41.3": "ALTRES TRASTORNS D'ANSIETAT MIXTOS",
            "F41.8": "ALTRES TRASTORNS D'ANSIETAT ESPEC�FICS",
            "F41.9": "TRASTORN D'ANSIETAT, INESPEC�FIC"
            }

    TOXICOMANIA = {
            "C01-F10": "ALCOHOL",
            "C01-F11": "OPIOIDES",
            "C01-F12": "CÀNNABIS",
            "C01-F13": "SEDATIUS HIPNÒTICS",
            "C01-F14": "COCAÏNA",
            "C01-F15": "ALTRES ESTIMULANTS",
            "C01-F16": "AL·LUCINÒGENS",
            "C01-F17": "NICOTINA",
            "C01-F18": "INHALANTS",
            "C01-F19": "ALTRES"}
    
    cols = """(hash varchar2(40),
                data_ingres date,
                up varchar2(5), 
                eapp_desc varchar2(40), 
                benzo int, 
                benzo_desc varchar2(200),
                data_benzo date,
                ansietat int, 
                ansietat_desc varchar2(200), 
                ansietat_data date,
                toxicomania int, 
                toxicomania_desc varchar2(200),
                toxicomania_data date)"""
    u.createTable('benzos_noves_jail', cols, 'redics', rm=True)
    u.grantSelect('benzos_noves_jail', 'PREDUPRP', 'redics')
    print('created')

    sql = """SELECT usua_cip, usua_cip_cod FROM pdptb101"""
    cip_2_hash = {cip: hash for cip, hash in u.getAll(sql, 'pdp')}
    print('converters')
    
    # Interns que han ingressat a pres� procedent d�estar en llibertat 
    # (�S a dir si provenia d�una altra pres� no s�han d�incloure) 
    # en els darrers 12 mesos
    participants = c.defaultdict(set)
    anteriors = {}
    sql = """SELECT
                b.cip_usuari_cip,
                trunc(a.huab_data_ass),
                trunc(nvl(a.huab_data_final, sysdate)),
                huab_up_codi
            FROM
                vistb050 a
            INNER JOIN usutb011 b ON
                a.huab_usua_cip = b.cip_cip_anterior
            WHERE
                trunc(nvl(a.huab_data_final, sysdate)) >= to_date ('20230725',
                'YYYYMMDD')
            ORDER BY
                b.cip_usuari_cip,
                trunc(a.huab_data_ass),
                trunc(nvl(a.huab_data_final, sysdate))"""
    periode_inici = d.datetime.strptime("2023-07-31", "%Y-%m-%d").date()
    for cip, ini, fi, up in u.getAll(sql, '6951'):
        if isinstance(ini, d.datetime):
            ini = ini.date()
        if isinstance(fi, d.datetime):
            fi = fi.date()
        if ini >= periode_inici:
            if cip in cip_2_hash:
                hash = cip_2_hash[cip]
                if cip in anteriors:
                    previous_ini, previous_fi = anteriors[cip]
                    if previous_fi != ini:
                        participants[hash].add((ini, fi, up))
                else:
                    participants[hash].add((ini, fi, up))
        anteriors[cip] = (ini, fi)
    print('presos')

    sql = """select id_cip, hash_d from import.u11"""
    id_cip_2_hash = {id_cip: hash for id_cip, hash in u.getAll(sql, 'import')}
    sql = """select id_cip, hash_d from import_jail.u11"""
    for id_cip, hash in u.getAll(sql, 'import'):
        id_cip_2_hash[id_cip] = hash
    print('converters')

    
    dx = c.defaultdict(lambda : c.defaultdict(set))
    sql = """select
                id_cip,
                pr_cod_ps,
                pr_dde,
                pr_dba
            from
                import.problemes
            where
                (pr_cod_ps in {}
                or pr_cod_ps REGEXP '^C01-F10|^C01-F11|^C01-F12|^C01-F13|^C01-F14|^C01-F15|^C01-F16|^C01-F17|^C01-F18|^C01-F19' )
                and pr_data_baixa = ''
            """.format(tuple(ANSIETAT.keys()))
    for id, ps, dde, dba in u.getAll(sql, 'import'):
        if id in id_cip_2_hash:
            hash = id_cip_2_hash[id]
            if hash in participants:
                for ini, fi, up in participants[hash]:
                    if isinstance(dde, d.datetime):
                        dde = dde.date()
                    if isinstance(ini, d.datetime):
                        ini = ini.date()
                    if isinstance(dba, d.datetime):
                        dba = dba.date()
                    if dde <= ini and (dba is None or dba >= ini):
                        dx[hash][(ini, fi, up)].add((ps, dde))
    print(dx)
    print('problemes')
    tractaments = c.defaultdict(lambda : c.defaultdict(set))
    tractaments_anteriors = c.defaultdict(lambda : c.defaultdict(set))
    sql = """select
                id_cip,
                ppfmc_atccodi,
                ppfmc_pmc_data_ini,
                ppfmc_data_fi
            from
                import.tractaments
            where
                ppfmc_pmc_data_ini >= date '2023-01-01'
                and ppfmc_atccodi in {}""".format(tuple(FARMACS.keys()))
    for id, farmac, inici, final in u.getAll(sql, 'import'):
        if id in id_cip_2_hash:
            hash = id_cip_2_hash[id]
            if hash in participants:
                for ini, fi, up in participants[hash]:
                    if isinstance(ini, d.datetime):
                        ini = ini.date()
                    if isinstance(inici, d.datetime):
                        inici = inici.date()
                    if isinstance(final, d.datetime):
                        final = final.date()
                    if inici < ini and final >= ini:
                        tractaments_anteriors[hash][(ini, fi, up)].add((farmac, inici))
                    elif inici >= ini and inici <= ini + d.timedelta(days=2):
                        tractaments[hash][(ini, fi, up)].add((farmac, inici))
                    
    sql = """select
                id_cip,
                ppfmc_atccodi,
                ppfmc_pmc_data_ini,
                ppfmc_data_fi
            from
                import_jail.tractaments
            where
                ppfmc_pmc_data_ini >= date '2023-01-01'
                and ppfmc_atccodi in {}""".format(tuple(FARMACS.keys()))
    for id, farmac, inici, final in u.getAll(sql, 'import'):
        if id in id_cip_2_hash:
            hash = id_cip_2_hash[id]
            if hash in participants:
                for ini, fi, up in participants[hash]:
                    if isinstance(ini, d.datetime):
                        ini = ini.date()
                    if isinstance(inici, d.datetime):
                        inici = inici.date()
                    if isinstance(final, d.datetime):
                        final = final.date()
                    if inici < ini and final >= ini:
                        tractaments_anteriors[hash][(ini, fi, up)].add((farmac, inici))
                    elif inici >= ini and inici <= ini + d.timedelta(days=7):
                        tractaments[hash][(ini, fi, up)].add((farmac, inici))
    print(tractaments)
    print('tractaments')
    sql = """select scs_codi, ics_desc from nodrizas.jail_centres"""    
    jail_centres = {codi: desc for codi, desc in u.getAll(sql, 'nodrizas')}
    print('centres')
    upload = []
    for hash in participants:
        for ini, fi, up in participants[hash]:
            ansietat, benzo, toxicomania = 0, 0, 0
            ansietat_desc, benzo_desc, toxicomania_desc = '', '', ''
            ansietat_data, data_benzo, toxicomania_data = None, None, None
            eapp_desc = ''
            if hash in dx:
                if (ini, fi, up) in dx[hash]:
                    for ps, dde in dx[hash][(ini, fi, up)]:
                        if ps in ANSIETAT.keys():
                            ansietat = 1
                            ansietat_desc = ANSIETAT[ps]
                            ansietat_data = dde
                        elif ps[:7] in TOXICOMANIA.keys():
                            toxicomania = 1
                            toxicomania_desc = TOXICOMANIA[ps[:7]]
                            toxicomania_data = dde
            if hash in tractaments:
                if (ini, fi, up) in tractaments[hash]:
                    if hash not in tractaments_anteriors:
                        for farmac, inici in tractaments[hash][(ini, fi, up)]:
                            benzo = 1
                            benzo_desc = FARMACS[farmac]
                            data_benzo = inici
                    elif hash in tractaments_anteriors:
                        if (ini, fi, up) not in tractaments_anteriors[hash]:
                            for farmac, inici in tractaments[hash][(ini, fi, up)]:
                                benzo = 1
                                benzo_desc = FARMACS[farmac]
                                data_benzo = inici
                        elif (ini, fi, up) in tractaments_anteriors[hash]:
                            for farmac, inici in tractaments_anteriors[hash][(ini, fi, up)]:
                                benzo = 2
                                benzo_desc = FARMACS[farmac]
                                data_benzo = inici

            if up in jail_centres:
                eapp_desc = jail_centres[up]

            upload.append((hash, ini, up, eapp_desc, benzo, benzo_desc, data_benzo,
                    ansietat, ansietat_desc, ansietat_data,
                    toxicomania, toxicomania_desc, toxicomania_data))
    print('penjant')
    u.listToTable(upload, 'benzos_noves_jail', 'redics')


if __name__ == "__main__":
    benzos()