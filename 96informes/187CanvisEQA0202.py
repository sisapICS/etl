# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


class EQA0202(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_eqa()
        self.export_data()
               

    def get_centres(self):
        """Per treure descripcions del centre"""
        
        self.centres = {up: desc for up, desc in u.getAll("select scs_codi, ics_desc from cat_centres", "nodrizas")}
        
        
    def get_eqa(self):
        """Traiem dades indicador actual i nou calcul"""
        self.eqaactual = {}
        sql = "select up, sum(num), sum(den) from mst_indicadors_pacient where ates=1 and ind_codi like ('EQA0202%') and excl_edat=0 \
                and excl=0 and ci=0 and clin=0 and maca=0 and institucionalitzat=0 group by up"
        for up, num, den in u.getAll(sql, "eqa_ind"):
            self.eqaactual[up] = {'num': num, 'den':den}
        self.eqa_nou = {}    
        sql = "select up, sum(num),sum(den) from eqa_inr where ates=1 and den=1 and (edat between 15 and 89) and excl=0 and ci=0 and clin=0 and \
                maca = 0 and institucionalitzat=0 group by up"
        for up, num, den in u.getAll(sql, "eqa_ind"):
            self.eqa_nou[up] = {'num': num, 'den':den}
            
    def export_data(self):
        """treiem excel"""
        upload = []
        for up, result in self.eqaactual.items():
            if up in self.centres:
                desc = self.centres[up]
                num_nou = self.eqa_nou[up]['num'] if up in self.eqa_nou else 0
                den_nou =  self.eqa_nou[up]['den'] if up in self.eqa_nou else 0
                upload.append([up, desc, result['num'], result['den'], num_nou, den_nou])
        u.writeCSV(u.tempFolder + 'EQA0202.txt', upload)
        

if __name__ == '__main__':
    EQA0202()
            