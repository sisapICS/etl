# coding: utf8

"""
Vacuna grip en cohort 1954-1958
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter


import sisapUtils as u

dext, = u.getOne('select data_ext from dextraccio', 'nodrizas')

class vacunesTd(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_vacunes()
        self.get_pob()        

    def get_centres(self):
        """centres ICS"""
        self.centres = {}
        sql = "select scs_codi, ics_codi from cat_centres where ep='0208'"
        for up, br in u.getAll(sql, 'nodrizas'):
            self.centres[up] = br
    
    def get_vacunes(self):
        """aconseguim vacunes tètanus"""
        self.vacunes = {}
        sql = "select id_cip_sec,  datamax from eqa_vacunes where agrupador=99"
        for id,  dat in u.getAll(sql, 'nodrizas'):
            self.vacunes[id] = dat
        
    def get_pob(self):
        """agafem població segons criteris edat"""
        self.resultats = Counter()
        sql = "select id_cip_sec, year(data_naix), up, data_naix from assignada_tot where ates=1 and year(data_naix) between 1954 and 1958"
        for id, any_naix, up, dnaix in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                self.resultats['den'] += 1
                if (id) in self.vacunes:
                    dat_vac = self.vacunes[id]
                    b = u.monthsBetween(dat_vac, dext)
                    if 0 <= b <= 12:
                        self.resultats['num'] += 1
                    
        for (tip), n in self.resultats.items():
            print tip, n
            
   
if __name__ == '__main__':
    vacunesTd()