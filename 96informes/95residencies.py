# coding: iso-8859-1


from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

nod = 'nodrizas'
imp = 'import'
eqa = 'eqa_ind'


centres = {}
sql = "select scs_codi, ics_codi, ics_desc, sap_desc,amb_desc from cat_centres"
for up, br, desc, sap, ambit in getAll(sql, nod):
    centres[up] = {'br': br, 'desc': desc, 'sap': sap, 'ambit': ambit}
    
c_residencies = {}
sql = 'select residencia, residencia_desc, up from cat_residencies'
for resi, resi_desc, up in getAll(sql, imp):
    if up in centres:
        c_residencies[resi] = {'desc': resi_desc}
    

piir,piirV,piirT={},{},{}
sql="select id_cip_sec,if(mi_cod_var like ('T4101014%'),'var','text'),right(mi_cod_var,2) from piic where (mi_cod_var like ('T4101015%') or mi_cod_var like ('T4101014%')) and mi_ddb is null"
for id,camp,valor in getAll(sql,imp):
    if camp == 'var':
        piirV[(id,valor)] = True
    elif camp == 'text':
        piirT[(id,valor)] = True

for (id,valor),r in piirV.items():
    if (id,valor) in piirT:
        piir[id] = True
        
sql="select id_cip_sec,if(mi_cod_var='T4101001','rec',if(mi_cod_var='T4101002' or mi_cod_var like ('T4101017%'),'dant','0'))\
            from piic where mi_ddb is null"
for id,tipii in getAll(sql,imp):
    if tipii == 'rec':
        piir[int(id)]= True

        
dades_residents = Counter()
sql = 'select id_cip_Sec, up, up_residencia, pcc, maca from assignada_tot where institucionalitzat=1 and edat>14'
for id, up, up_res, pcc, maca in getAll(sql, nod):
    try:
        br = centres[up]['br']
        desc = centres[up]['desc']
        sap = centres[up]['sap']
        ambit = centres[up]['ambit']
    except KeyError:
        continue
    pii = 0
    if id in piir:
        pii = 1
    dades_residents[up, br, desc, sap, ambit, pcc, maca, pii] += 1
    
indicadors = ['EQA0213','EQA0235','EQA0208','EQA0209','EQA0210','EQA0212']

indicadors_resis = {}
for indicador in indicadors:
    sql = "select up, up_residencia,sum(num),count(*) from eqa_ind.mst_indicadors_pacient where ates=1 and edat>14 and excl=0 and ci=0 and clin=0 and institucionalitzat=1 and grup_codi like '{}%' group by up, up_residencia".format(indicador)
    for up, up_resi, num, den in getAll(sql, eqa):
        try:
            br = centres[up]['br']
            desc = centres[up]['desc']
            sap = centres[up]['sap']
            ambit = centres[up]['ambit']
        except KeyError:
            continue
        residen = ''
        if up_resi in c_residencies:
            residen = c_residencies[up_resi]['desc']
        indicadors_resis[(up, br, desc, up_resi, residen, sap, ambit, indicador, num, den)] = True
    
upload = []
for (up, br, desc, sap, ambit, pcc, maca, pii), v in dades_residents.items():
    upload.append([up, br, desc, sap, ambit, pcc, maca, pii, v])
file = tempFolder + 'pacients_en_residencies.txt'
writeCSV(file, upload, sep=';')
        
upload = []
for (up, br, desc, up_resi, residen, sap, ambit, indicador, num, den), v in indicadors_resis.items():
    upload.append([up, br, desc, up_resi, residen, sap, ambit, indicador, num, den])
file = tempFolder + 'indicadors_residencies.txt'
writeCSV(file, upload, sep=';')
    
    
    
    
    
    
    
    