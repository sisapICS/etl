# -*- coding: utf8 -*-

"""."""

import collections as c
import sys,datetime
from time import strftime

import sisapUtils as u

def grups_edats(edat):
    if edat < 28:
        ed = '< 28 anys'
    elif 28 <= edat <= 34:
        ed = 'Entre 28 i 34 anys'
    elif 35 <= edat <= 44:
        ed = 'Entre 35 i 44 anys'
    elif 45 <= edat <= 54:
        ed = 'Entr 45 i 54 anys'
    elif 55 <= edat <= 66:
        ed = 'Entre 55 i 66 anys'
    else:
        ed = '> 66 anys'
    
    return ed
    

class econsulta(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_dades()
        
    def get_dades(self):
        """."""
        upload = []
        sql =  "select edat, sexe from SISAP_ECONSULTA_PACIENTS where inici_pac>0"
        for edat, sexe in u.getAll(sql, 'redics'):
            upload.append([grups_edats(edat), sexe])
            
        u.writeCSV(u.tempFolder + 'econsulta.txt', upload)  
  
        
        
if __name__ == '__main__':
    econsulta()

            