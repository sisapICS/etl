# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

nod = 'nodrizas'
imp = 'import'

pob = {}
sql = "select id_cip_sec from assignada_tot where up='00352'"
for id, in getAll(sql, nod):
    pob[(id)] = True
    
centres = {}
sql = 'select up, up_desc, assir from ass_centres'
for up, desc, assir in getAll(sql, nod):
    centres[up] = {'desc':desc, 'assir':assir}
    
recomptes = Counter()
sql = "select id_cip_sec, visi_up from ass_visites where year(visi_data_visita)='2017'"
for id, up in getAll(sql, nod):
    if id in pob:
        desc = centres[up]['desc']
        assir = centres[up]['assir']
        recomptes[(up,desc,assir)] += 1
        
upload = []
for (up,desc,assir), d in recomptes.items():
    upload.append([up,desc,assir, d])


file = tempFolder + 'Assirs_en_pob_Montcada.txt'
writeCSV(file, upload, sep=';')