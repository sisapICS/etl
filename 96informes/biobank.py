# -*- coding: utf8 -*-

from datetime import date, datetime

import os
import shutil
from collections import Counter
import csv

import traceback
from multiprocessing import Pool

from ftplib import FTP
import warnings

import sisapUtils as u
import sisaptools as t

FLAG_SOURCE = 'SFTP' # 'JOC_PROVES' # 'SERVER' # SFTP

class BioBank():
    def __init__(self):
        self.get_arxius()
        self.part2()
        self.send()
    
    def get_arxius(self):
        self.cips_id = []
        """Download the files they send"""
        if FLAG_SOURCE == 'SFTP': 
            dates = []
            for file in t.SFTP("biobank").ls("biob/output"):
                if file[0:7] == 'hc_pet_':
                    dates.append(file[7:15])
            max_d = max(dates)
            self.FILE = 'hc_pet_' + max_d + '.csv'
            t.SFTP("biobank").get("biob/output/" + self.FILE, u.tempFolder + self.FILE)
            with open(u.tempFolder + self.FILE, 'rb') as f:
                c = csv.reader(f, delimiter=';')
                first = True
                for row in c:
                    print(row)
                    if not first:
                        cip = row[0]
                        dni = row[2]
                        self.cips_id.append((row[0], cip[0:13], dni))
                        # self.cips_id.append((row[0], cip[:-1], ''))
                        print(cip)
                        print(dni)
                        # en cas que hi hagi dni 
                        # cips_id[row[0], row[1][:-1], row[2]]
                    first = False

        if FLAG_SOURCE == 'SERVER':
            with open('hc_pet_20230309.csv', 'rb') as f:
                c = csv.reader(f, delimiter=';')
                first = True
                for row in c:
                    if not first:
                        cip = row[1]
                        self.cips_id.append((row[0], cip[0:13], row[2]))
                        # self.cips_id.append((row[0], cip[:-1], ''))
                        print(cip)
                        # en cas que hi hagi dni 
                        # cips_id[row[0], row[1][:-1], row[2]]
                    first = False
        if FLAG_SOURCE == 'JOC_PROVES':
            for i in range(1, 10):
                cip = 'AAAA' + str(i)*9
                dni = str(i)*8 + 'A'
                self.cips_id.append((i, cip, dni))

    def part2(self):
        # borro el q hi havia a biobank
        u.execute("truncate table CIPBIOBANK", 'pdp')
        u.listToTable(self.cips_id, 'CIPBIOBANK', 'pdp')
        print('pas1')
        # dels cips q m'envien, busco hash --> BIOBANK_HASH.txt
        self.hash_to_id = {}
        sql = """select id, usua_cip_cod from pdptb101,CIPBIOBANK
                    where usua_cip = CIP"""
        for id, hash in u.getAll(sql, 'pdp'):
            self.hash_to_id[hash] = id
        # els cips q no he pogut convertir --> \BIOBANK_HASH_DNI.txt
        sql = """select distinct tnoTrobats.id, usu.usua_cip 
                from usutb040 usu,
                    (select id, cip, dni 
                        from CIPBIOBANK 
                        where id not in (
                                    select id from pdptb101,CIPBIOBANK
                                    where usua_cip = substr(CIP,0,13)
                                    )
                    )tnoTrobats
                where usu.usua_dni = tnoTrobats.dni"""
        for id, hash in u.getAll(sql, 'pdp'):
            self.hash_to_id[hash] = id
        print('pas2')
        self.upload = []
        sql1= """SELECT 1 AS ordre,
                    'BIOB;9508;47419' AS cont
                FROM
                    nodrizas.dextraccio """
        for ordre, cont in u.getAll(sql1, 'nodrizas'):
            self.upload.append((ordre, cont))
        print('here')
        sql2 = """SELECT
                        2 AS ordre,
                        -- 'HASH@PS_ACV_MCV_DATA@PS_ALCOHOL_DATA@PS_APNEA_SON_DATA@PS_ASMA_DATA@PS_BRONQUITIS_CRONICA_DATA@PS_CARDIOPATIA_ISQUEMICA_DATA@PS_DEMENCIA_DATA@PS_DEPRESSIO_DATA@PS_DIABETIS1_DATA@PS_DIABETIS2_DATA@PS_DISLIPEMIA_DATA@PS_EMBARAS_DATA@PS_HEPATOPATIA_DATA@PS_HIPERTIROIDISME_DATA@PS_HIPOTIROIDISME_DATA@PS_HTA_DATA@PS_INSUF_CARDIACA_DATA@PS_MPOC_ENFISEMA_DATA@PS_M_VASCULAR_PERIF_DATA@PS_NEOPLASIA_M_DATA@PS_OBESITAT_DATA@PS_OPIACIS_DATA@PS_RENAL_CRO_DATA@PS_SEDANTS_HIPNOTICS_DATA@PS_SIDA_DATA@PS_TBC_DATA@PS_TBC_T_DATA@PS_TRAST_COAGULACIO_DATA@PS_VALVULOPATIA_DATA@PS_VIH_DATA@F_AINE@F_ANALGESICS@F_ANSIO_HIPN@F_ANTIAGREGANTS_ANTICOA@F_ANTIDEPRESSIUS@F_ANTIEPILEPTICS@F_ANTIPSICOTICS@F_ANTI_TBC@F_CARDIO_NOHTA@F_CORTIC_SISTEM@F_DIAB_ADO@F_DIAB_INSULINA@F_HIPOLIPEMIANTS@F_HTA_ALFABLOQ@F_HTA_ALTRES@F_HTA_BETABLOQ@F_HTA_CALCIOA@F_HTA_COMBINACIONS@F_HTA_DIURETICS@F_HTA_IECA_ARA2@F_MHDA@F_MPOC_ASMA@F_PSICOLEPTICS@VC_ACTV_FISICA_DATA@VC_ACTV_FISICA_VALOR@VC_ESTADIATGE_MRC_DATA@VC_ESTADIATGE_MRC_VALOR@VC_NYHA_DATA@VC_NYHA_VALOR@V_ALCOHOL_DATA@V_ALCOHOL_VALOR@V_FEV1_DATA@V_FEV1_VALOR@V_FEV1_FVC_DATA@V_FEV1_FVC_VALOR@V_IMC_DATA@V_IMC_VALOR@V_O2_DATA@V_O2_VALOR@V_PESO_DATA@V_PESO_VALOR@V_TABAC_DATA@V_TABAC_VALOR@V_TAD_DATA@V_TAD_VALOR@V_TALLA_DATA@V_TALLA_VALOR@V_TAS_DATA@V_TAS_VALOR@V_ALB_CREAT_DATA@V_ALB_CREAT_VALOR@V_COL_HDL_DATA@V_COL_HDL_VALOR@V_COL_LDL_DATA@V_COL_LDL_VALOR@V_COL_TOTAL_DATA@V_COL_TOTAL_VALOR@V_CREATININA_DATA@V_CREATININA_VALOR@V_FERRITINA_DATA@V_FERRITINA_VALOR@V_FILTRAT_GLOM_DATA@V_FILTRAT_GLOM_VALOR@V_GLUCOSA_DATA@V_GLUCOSA_VALOR@V_HBA1C_DATA@V_HBA1C_VALOR@V_HEMOGLOBINA_DATA@V_HEMOGLOBINA_VALOR@V_POTASI_DATA@V_POTASI_VALOR@V_TRIGLIC_DATA@V_TRIGLIC_VALOR@V_TSH_DATA@V_TSH_VALOR@V_VCM_DATA@V_VCM_VALOR@V_VIH_CD3_DATA@V_VIH_CD3_VALOR@V_VIH_CD4_DATA@V_VIH_CD4_VALOR@V_VIH_SEROLOGIA_DATA@V_VIH_SEROLOGIA_VALOR@C19_CAS_DATA'
                                    'IDOCC;157486;165506;165507;165508;165509;165510;165511;165512;165513;165514;165515;165516;165517;165518;165519;165520;165521;165522;165523;165524;165525;165526;165527;165528;165529;165530;165531;165532;165533;165534;165535;165536;165537;165538;165539;165540;165541;165542;165543;165544;165545;165546;165547;165548;165549;165550;165551;165552;165553;165554;165555;165556;165557;165558;165559;165560;165561;165562;165563;165564;165565;165566;165567;165568;165569;165570;165571;165572;165573;165574;165575;165576;165577;165579;165580;165581;165582;165583;165585;165586;165587;165588;165589;165590;165591;165592;165593;165594;165595;165596;165597;165598;165599;165600;165601;165602;165603;165604;165605;165606;165607;165608;165609;165610;165611;165612;165613;165614;165615;165616;165617;165618;165619;165620;167050'
                                    AS cont
                    FROM
                        nodrizas.dextraccio """
        for ordre, cont in u.getAll(sql2, 'nodrizas'):
            self.upload.append((ordre, cont))
        print('pas3 abans llarga')
        sql3 = """SELECT
                3 AS ordre,
                            c_cip, 'S',
                            to_char( PS_ACV_MCV_DATA , 'DD/MM/YYYY'),
                            to_char( PS_ALCOHOL_DATA , 'DD/MM/YYYY'),
                            to_char( PS_APNEA_SON_DATA , 'DD/MM/YYYY'),
                            to_char( PS_ASMA_DATA , 'DD/MM/YYYY'),
                            to_char( PS_BRONQUITIS_CRONICA_DATA , 'DD/MM/YYYY'),
                            to_char( PS_CARDIOPATIA_ISQUEMICA_DATA , 'DD/MM/YYYY'),
                            to_char( PS_DEMENCIA_DATA , 'DD/MM/YYYY'),
                            to_char( PS_DEPRESSIO_DATA , 'DD/MM/YYYY'),
                            to_char( PS_DIABETIS1_DATA , 'DD/MM/YYYY'),
                            to_char( PS_DIABETIS2_DATA , 'DD/MM/YYYY'),
                            to_char( PS_DISLIPEMIA_DATA , 'DD/MM/YYYY'),
                            to_char( PS_EMBARAS_DATA , 'DD/MM/YYYY'),
                            to_char( PS_HEPATOPATIA_DATA , 'DD/MM/YYYY'),
                            to_char( PS_HIPERTIROIDISME_DATA , 'DD/MM/YYYY'),
                            to_char( PS_HIPOTIROIDISME_DATA , 'DD/MM/YYYY'),
                            to_char( PS_HTA_DATA , 'DD/MM/YYYY'),
                            to_char( PS_INSUF_CARDIACA_DATA , 'DD/MM/YYYY'),
                            to_char( PS_MPOC_ENFISEMA_DATA , 'DD/MM/YYYY'),
                            to_char( PS_M_VASCULAR_PERIF_DATA , 'DD/MM/YYYY'),
                            to_char( PS_NEOPLASIA_M_DATA , 'DD/MM/YYYY'),
                            to_char( PS_OBESITAT_DATA , 'DD/MM/YYYY'),
                            to_char( PS_OPIACIS_DATA , 'DD/MM/YYYY'),
                            to_char( PS_RENAL_CRO_DATA , 'DD/MM/YYYY'),
                            to_char( PS_SEDANTS_HIPNOTICS_DATA , 'DD/MM/YYYY'),
                            to_char( PS_SIDA_DATA , 'DD/MM/YYYY'),
                            to_char( PS_TBC_DATA , 'DD/MM/YYYY'),
                            to_char( PS_TBC_T_DATA , 'DD/MM/YYYY'),
                            to_char( PS_TRAST_COAGULACIO_DATA , 'DD/MM/YYYY'),
                            to_char( PS_VALVULOPATIA_DATA , 'DD/MM/YYYY'),
                            to_char( PS_VIH_DATA , 'DD/MM/YYYY'),
                                F_AINE,
                                F_ANALGESICS,
                                F_ANSIO_HIPN,
                                F_ANTIAGREGANTS_ANTICOA,
                                F_ANTIDEPRESSIUS,
                                F_ANTIEPILEPTICS,
                                F_ANTIPSICOTICS,
                                F_ANTI_TBC,
                                F_CARDIO_NOHTA,
                                F_CORTIC_SISTEM,
                                F_DIAB_ADO,
                                F_DIAB_INSULINA,
                                F_HIPOLIPEMIANTS,
                                F_HTA_ALFABLOQ,
                                F_HTA_ALTRES,
                                F_HTA_BETABLOQ,
                                F_HTA_CALCIOA,
                                F_HTA_COMBINACIONS,
                                F_HTA_DIURETICS,
                                F_HTA_IECA_ARA2,
                                F_MHDA,
                                F_MPOC_ASMA,
                                F_PSICOLEPTICS,
                            to_char( VC_ACTV_FISICA_DATA , 'DD/MM/YYYY'),
                                VC_ACTV_FISICA_VALOR,
                            to_char( VC_ESTADIATGE_MRC_DATA , 'DD/MM/YYYY'),
                                VC_ESTADIATGE_MRC_VALOR,
                            to_char( VC_NYHA_DATA , 'DD/MM/YYYY'),
                                VC_NYHA_VALOR,
                            to_char( V_ALCOHOL_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_ALCOHOL_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_FEV1_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_FEV1_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_FEV1_FVC_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_FEV1_FVC_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_IMC_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_IMC_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_O2_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_O2_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_PESO_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_PESO_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_TABAC_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_TABAC_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_TAD_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_TAD_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_TALLA_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_TALLA_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_TAS_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_TAS_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_ALB_CREAT_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_ALB_CREAT_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_COL_HDL_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_COL_HDL_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_COL_LDL_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_COL_LDL_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_COL_TOTAL_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_COL_TOTAL_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_CREATININA_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_CREATININA_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_FERRITINA_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_FERRITINA_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_FILTRAT_GLOM_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_FILTRAT_GLOM_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_GLUCOSA_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_GLUCOSA_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_HBA1C_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_HBA1C_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_HEMOGLOBINA_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_HEMOGLOBINA_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_POTASI_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_POTASI_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_TRIGLIC_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_TRIGLIC_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_TSH_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_TSH_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_VCM_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_VCM_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_VIH_CD3_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_VIH_CD3_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_VIH_CD4_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_VIH_CD4_VALOR, '9999990D99'), '.', ',') ),
                            to_char( V_VIH_SEROLOGIA_DATA , 'DD/MM/YYYY'),
                            trim(REPLACE(TO_CHAR(V_VIH_SEROLOGIA_VALOR, '9999990D99'), '.', ',') ),
                            to_char( ps_covid_data , 'DD/MM/YYYY')
            FROM
                DBS
            WHERE rownum <= 1"""
        # for row in u.getAll(sql3, 'redics'):
        #     if row[1] in self.hash_to_id:
        #         id = self.hash_to_id[row[1]]
        #         up = str(id) + ';'
        #         for e in row[2:]:
        #             if e:
        #                 up = up + str(e) + ';'
        #             else:
        #                 up = up + ';'
        #         self.upload.append((row[0],up))
        for row in u.getAll(sql3, 'redics'):
            for (i, id, dni) in self.cips_id:
                up = str(i) + ';'
                for e in row[2:]:
                    if e:
                        up = up + str(e) + ';'
                    else:
                        up = up + ';'
                self.upload.append((row[0],up))
        print(self.upload)

    def send(self):
        print('sending')
        DIA = (datetime.now().date())
        # file = 'cbatch02_{}.csv'
        # file = file.format(DIA.strftime("%Y_%m_%d"))
        # print(file)
        # u.writeCSV(u.tempFolder + file, self.upload)
        # with t.SFTP("biobank") as sftp:
        #     sftp.put(u.tempFolder + file, "biob/input/sisap/{}".format(file))
        #     os.remove( u.tempFolder + file)
        # per DNIS falsos
        file = 'cbatch_' + self.FILE[7:]
        # file = file.format(DIA.strftime("%Y_%m_%d"))
        print(file)
        u.writeCSV(u.tempFolder + file, self.upload)
        with t.SFTP("biobank") as sftp:
            sftp.put(u.tempFolder + file, "biob/input/sisap/{}".format(file))
            os.remove(u.tempFolder + file)


if __name__ == "__main__":
    BioBank()