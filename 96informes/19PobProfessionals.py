# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict
import csv, os
import sisaptools as u

debug = False

nod = 'nodrizas'

conn= connect('pdp')
c= conn.cursor()

printTime('inici Pob professionals') 

assig = {}
sql = "select up,uba,'M',sum(if(edat<15,1,0)),sum(if(edat>14,1,0)) from assignada_tot group by up,uba {0} union select upinf,ubainf,'I',sum(if(edat<15,1,0)),sum(if(edat>14,1,0)) from assignada_tot group by upinf,ubainf {0}".format(' limit 2' if debug else '')
for up,uba,tipus,nens,adults in getAll(sql,nod):
    query = "update professionals set nens={0},adults={1} where up='{2}' and uab='{3}' and tipus='{4}'".format(nens,adults,up,uba,tipus)
    c.execute(query)

conn.commit()   
c.close    
printTime('Fi Pob professionals') 

table = "professionals"
with u.Database("redics", "data") as redics:
    col_list = redics.get_table_columns(table)
    cols = [redics.get_column_information(col, table)["create"] for col in col_list]
    dades = [row for row in redics.get_all("select * from {}".format(table))]
with u.Database("exadata", "data") as exadata:
    exadata.create_table(table, cols, remove=True)
    exadata.list_to_table(dades, table)
    exadata.set_grants("select", table, "DWSISAP_ROL", inheritance=False)
    exadata.set_grants("select", table, "DWRENV_SPUBLICA_SELECT", inheritance=False)
    exadata.set_statistics(table)
