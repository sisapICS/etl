# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
import datetime
from collections import defaultdict,Counter

imp = 'import'
nod = 'nodrizas'

consell = defaultdict(list)

sql = "select id_cip_sec, vu_dat_act from variables where vu_cod_vs in ('CW2003', 'CW2002')"
for id, dat in getAll(sql, imp):
    consell[id].append(dat)

recomptes = Counter()
sql = "select id_cip_sec, inici from ass_embaras where year(inici)='2017'"
for id, inici in getAll(sql, nod):
    num = 0
    if id in consell:
        for data in consell[id]:
            b = monthsBetween(data, inici)
            if 0<= b <= 12:
                num = 1
    recomptes['den'] += 1
    recomptes['num'] += num
 
print recomptes 