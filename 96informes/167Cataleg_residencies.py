# coding: iso-8859-1

from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter


res_ecap = {}
sql = 'select residencia, inclos from cat_residencies'
for re, inclos in getAll(sql, 'import'):
    res_ecap[re] = inclos


upload = []
sql = 'select gu_up_residencia, gu_desc from cat_ppftb011_def'
for residencia, desc in getAll(sql, 'import'):
    inclos = 999
    sisap = 0
    if residencia in res_ecap:
        sisap = 1
        inclos = res_ecap[residencia]
    upload.append([residencia, desc, int(sisap), int(inclos)])
        
file = tempFolder + 'Cataleg_residencies.txt'
writeCSV(file, upload, sep=';')
    