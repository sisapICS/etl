import sisapUtils as u
# from pcrFuncionaris_aux import ids # {dni:(centre, nom)} in 96informes/
import pandas as pd
import hashlib as h
import pprint as pp


reos_sql = "SELECT usua_cip, usua_uab_up FROM usutb040"
reo_to_up = {}
for sector in ["6951"]:
    print(sector)
    for cip, up in u.getAll(reos_sql, sector):
        reo_to_up[cip] = up

hash_to_cip = {}
for cip in reo_to_up:
    hash_to_cip[h.sha1(cip).hexdigest().upper()] = cip

proves_sql = """
    SELECT
        p.hash, count(*)
    FROM
        preduffa.sisap_covid_pac_prv_ext p
    WHERE
        prova = 'PCR'
        AND DATA BETWEEN DATE '2020-07-05' AND DATE '2020-07-19'
    group by p.hash
    """

# {hash: n}
proves = dict((row[0], row[1]) for row in u.getAll(proves_sql, 'redics') if row[0] in hash_to_cip)

n_proves = n_reos = 0
# {up = (n_reos, n_proves)}
data = {}
for reo_h in proves:
    if reo_to_up[hash_to_cip[reo_h]] not in data:
        data[reo_to_up[hash_to_cip[reo_h]]] = [1, proves[reo_h]]
    data[reo_to_up[hash_to_cip[reo_h]]][0] += 1
    data[reo_to_up[hash_to_cip[reo_h]]][1] += proves[reo_h]


pp.pprint(data)
