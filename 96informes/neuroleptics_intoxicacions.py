import sisapUtils as u
import collections as c


def prescripcio_total():
    u.createTable('prescripcio_total', '(regio varchar(20), regio_desc varchar(300), sexe varchar(2), edat varchar(20), n int)', 'altres', rm=True)
    poblacio = {}
    sql = """SELECT cip, sexe, edat FROM dwsisap.dbc_poblacio"""
    for cip, sexe, edat in u.getAll(sql, 'exadata'):
        poblacio[cip[:13]] = [sexe, edat]

    regions = {}
    sql = """SELECT up_cod, regio_cod, regio_des FROM sisap_covid_dbc_rup"""
    for up, regcod, reg in u.getAll(sql, 'redics'):
        regions[up] = [regcod, reg]

    tractaments = c.defaultdict(set)
    sql = """select ppfmc_pmc_usuari_cip, ppfmc_pmc_amb_cod_up 
                from ppftb016 
                where ppfmc_atccodi like 'N05%'
                and extract(YEAR FROM ppfmc_pmc_data_ini) < 2024
                and ppfmc_data_fi >= date '2023-12-31'"""
    for sector in u.sectors:
        for cip, up in u.getAll(sql, sector):
            if cip in poblacio and up in regions:
                regcod, reg = regions[up]
                sexe, edat = poblacio[cip]
                if edat < 15: edat_c = '<15'
                elif edat < 20: edat_c = '15-19'
                elif edat < 30: edat_c = '20-29'
                elif edat < 40: edat_c = '30-39'
                elif edat < 50: edat_c = '40-49'
                elif edat < 60: edat_c = '50-59'
                elif edat < 70: edat_c = '60-69'
                elif edat < 80: edat_c = '70-79'
                elif edat >= 80: edat_c = '>=80'
                tractaments[(regcod, reg, sexe, edat_c)].add(cip)

    upload = [(regcod, reg, sexe, edat_c, len(n)) for (regcod, reg, sexe, edat_c), n in tractaments.items()]
    u.listToTable(upload, 'prescripcio_total', 'altres')



def intox_agudes():
    u.createTable('intox_agudes', '(regio varchar(20), regio_desc varchar(300), sexe varchar(2), edat varchar(20), n int)', 'altres', rm=True)
    poblacio = {}
    sql = """SELECT cip, sexe, edat FROM dwsisap.dbc_poblacio"""
    for cip, sexe, edat in u.getAll(sql, 'exadata'):
        poblacio[cip[:13]] = [sexe, edat]

    regions = {}
    sql = """SELECT up_cod, regio_cod, regio_des FROM sisap_covid_dbc_rup"""
    for up, regcod, reg in u.getAll(sql, 'redics'):
        regions[up] = [regcod, reg]

    tractaments = c.defaultdict(set)
    sql = """select pr_cod_u, pr_up 
                from prstb015
                where pr_cod_ps in ('C01-T43.3X2S', 'C01-T43.3X3S', 'C01-T43.592A', 'C01-T43.593D', 'C01-T43.3X1D', 'C01-T43.504A', 'C01-T43.591A', 'C01-T43.502D', 'T43.3', 'T43.5', 'T43.4', 'C01-T43.3X4D', 'C01-T43.3X4S', 'C01-T43.3X2', 'C01-T43.3X2A', 'C01-T43.3X2D', 'C01-T43.3X3', 'C01-T43.3X3A', 'C01-T43.3X3D', 'C01-T43.3X4', 'C01-T43.3X4A', 'C01-T43.3', 'C01-T43.3X1', 'C01-T43.3X1A', 'C01-T43.3X1S', 'C01-T43.59', 'C01-T43.591', 'C01-T43.591D', 'C01-T43.50', 'C01-T43.501', 'C01-T43.501A', 'C01-T43.501D', 'C01-T43.593S', 'C01-T43.594', 'C01-T43.504S', 'C01-T43.594A', 'C01-T43.594D', 'C01-T43.594S', 'C01-T43.591S', 'C01-T43.592', 'C01-T43.592D', 'C01-T43.592S', 'C01-T43.593', 'C01-T43.593A', 'C01-T43.502S', 'C01-T43.503', 'C01-T43.503A', 'C01-T43.503D', 'C01-T43.503S', 'C01-T43.504', 'C01-T43.504D', 'C01-T43.5', 'C01-T43.3X', 'C01-T43.501S', 'C01-T43.502', 'C01-T43.502A')
                and extract(year from pr_dde) = 2023
                and pr_data_baixa = ''"""
    for sector in u.sectors:
        for cip, up in u.getAll(sql, sector):
            if cip in poblacio and up in regions:
                regcod, reg = regions[up]
                sexe, edat = poblacio[cip]
                if edat < 15: edat_c = '<15'
                elif edat < 20: edat_c = '15-19'
                elif edat < 30: edat_c = '20-29'
                elif edat < 40: edat_c = '30-39'
                elif edat < 50: edat_c = '40-49'
                elif edat < 60: edat_c = '50-59'
                elif edat < 70: edat_c = '60-69'
                elif edat < 80: edat_c = '70-79'
                elif edat >= 80: edat_c = '>=80'
                tractaments[(regcod, reg, sexe, edat_c)].add(cip)

    upload = [(regcod, reg, sexe, edat_c, len(n)) for (regcod, reg, sexe, edat_c), n in tractaments.items()]
    u.listToTable(upload, 'intox_agudes', 'altres')




if __name__ == "__main__":
    # prescripcio_total()
    intox_agudes()