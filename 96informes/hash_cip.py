import sisapUtils as u 

sql = """select cip from test.fetgegras1"""

a_fer = set()
for cip, in u.getAll(sql, 'test'):
    a_fer.add(cip)

upload = []
sql = """SELECT usua_cip, usua_cip_cod FROM pdptb101"""
for cip, hash in u.getAll(sql, 'pdp'):
    if cip in a_fer:
        upload.append((cip, hash))

cols = '(cip varchar(13), hash varchar(40))'
u.createTable('fetgegras2', cols, 'test', rm=False)
u.listToTable(upload, 'fetgegras2', 'test')
