# coding: latin1

"""
Longitudinalitat per gestió de casos (petició de MMedina)
Cal que existeixi la taula de longitudinalitat a nivell de pacient mst_long_cont_pacient
"""

import collections as c
import datetime as d

import sisapUtils as u


tb = "mst_long_cont_pacient"
db = "altres"

class LongGescasos(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_pob()
        self.get_long()
        self.get_gescasos()
        self.export_data()
        
    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, ics_desc \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: (br,desc) for (up, br, desc) in u.getAll(*sql)}
    
    def get_pob(self):
        """assignats"""
        self.pob = {}
        sql = "select id_cip_sec, up from assignada_tot where edat>14"
        for id, up in u.getAll(sql, "nodrizas"):
            self.pob[id] = up
            
    def get_long(self):
        """agafem longitudinalitat"""
        self.long = {}
        sql = "select id_cip_sec, up, r1, r2, r3, r4 from {}".format(tb)
        for id, up, r1, r2, r3, r4 in u.getAll(sql, db):
            self.long[id] = {'up':up, 'r1':r1, 'r2': r2, 'r3':r3, 'r4':r4}          
    
    def get_gescasos(self):
        """Agafem pacients en gestió de casos activa"""
        self.pac_gcas = c.Counter()
        sql = "select id_cip_sec, min(gesc_estat) \
               from gescasos, nodrizas.dextraccio \
               where gesc_estat in ('A', 'T') and \
               gesc_data_alta <= data_ext and \
               (gesc_data_sortida = 0 or gesc_data_sortida between \
                adddate(adddate(data_ext, interval -2 year), interval +1 day) \
                and data_ext) \
               group by id_cip_sec"
        for id, estat in u.getAll(sql, "import"):
            if id in self.pob:
                up = self.pob[id]
                self.pac_gcas[up, 'ass'] += 1
                if id in self.long:
                    self.pac_gcas[up,'long'] +=1
                    r1, r2, r3, r4 = self.long[id]['r1'], self.long[id]['r2'], self.long[id]['r3'], self.long[id]['r4']
                    self.pac_gcas[up,'CONT001'] += r1
                    self.pac_gcas[up,'CONT002'] += r2
                    self.pac_gcas[up,'CONT003'] += r3
                    self.pac_gcas[up,'CONT004'] += r4              
            
    def export_data(self):
        """."""
        upload = []
        for (up, indicador), recompte in self.pac_gcas.items():
            if indicador == 'ass':
                if up in self.centres:
                    br = self.centres[up][0]
                    desc = self.centres[up][1]
                    lg = self.pac_gcas[up,'long']
                    cont001 = self.pac_gcas[up,'CONT001'] / lg
                    cont002 = self.pac_gcas[up,'CONT002'] / lg
                    cont003 = self.pac_gcas[up,'CONT003'] / lg
                    cont004 = self.pac_gcas[up,'CONT004'] / lg
                    upload.append([up, br, desc, recompte, lg, cont001, cont002, cont003, cont004])
        
        u.writeCSV(u.tempFolder + 'Longitudinalitat_en_gescasos.txt', upload)
            
if __name__ == "__main__":
    LongGescasos()


