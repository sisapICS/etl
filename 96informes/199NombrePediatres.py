# coding: iso-8859-1

from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter


file = tempFolder + 'nombrePediatres.txt'

class pediatres(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_centres()
        self.get_nomines()
        self.get_prof_eqa()
        self.export_dades()
    
    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, amb_desc, sap_desc, ics_desc \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: (br, amb, sap, desc) for (up, br, amb, sap, desc)
                        in getAll(*sql)}
    
    
    def get_nomines(self):
        """."""
        self.nifs, self.llocs = {}, {}
        for (tipus, lloc, categoria, desc_cat, nif, nom, br) in readCSV(file, sep='@'): 
            self.nifs[nif] =  lloc
            self.llocs[lloc] = tipus
    
    def get_prof_eqa(self):
        """Mirem professionals que tenim a EQA segons catàleg del joan"""
        self.upload = []
        a, b = 0, 0
        sql = "select ide_dni, up, uab, servei, concat(nom, '_', cognom1, '_', cognom2), lloc_treball, nens, adults from cat_professionals where servei in ('PED', 'MG') and nens >150"
        for dni, up, uba, servei, nom, lloc, nens, adu in getAll(sql, "import"):
            if up in self.centres:
                br, ambit, sap, desc = self.centres[up][0],self.centres[up][1],self.centres[up][2],self.centres[up][3]
                dni = getNif(dni)
                if dni in self.nifs:
                    ok = 1
                else:
                    a += 1
                    llllloc = 0
                    if lloc in self.llocs:
                        llllloc = 1
                    else:
                        b += 1
                    self.upload.append([dni, ambit, sap, up, br, desc, uba, lloc, llllloc, servei, nom, nens, adu])
        print a, b
    
    def export_dades(self):
        """Treiem txt"""
        writeCSV(tempFolder + 'pediatres_no_rrhh.txt', self.upload)    
 
 
if __name__ == "__main__":
    printTime('Inici')
    
    pediatres()
    
    printTime('Fi')