# coding: iso-8859-1

import sisapUtils as u
import datetime
from dateutil.relativedelta import relativedelta
import pandas as pd
import collections as c

PERIODE = (2022, 2023)

class HepatitisC(object):
    def __init__(self):
        """."""
        self.get_centres();                     print("self.get_centres()")
        self.get_serologies_cataleg();          print("self.get_serologies_cataleg()")
        self.get_nacionalitats_cataleg();       print("self.get_nacionalitats_cataleg()")
        self.get_poblacio();                    print("self.get_poblacio()")
        self.get_u11();                         print("self.get_u11()")
        self.get_md_poblacio();                 print("self.get_md_poblacio()")
        self.get_serologies();                  print("self.get_serologies()")
        self.get_visit_subtables();             print("self.get_visit_subtables()");
        self.get_visites();                     print("self.get_visites()")
        self.get_visites_CUAP();                print("self.get_visites_CUAP()")
        self.get_derivacions();                 print("self.get_derivacions()")
        self.get_problemes();                   print("self.get_problemes()")
        self.get_table();                       print("self.get_table()")
   
    def get_centres(self):
        """ M�tode per obtenir i emmagatzemar informaci� sobre els centres a self.centres.
            La informaci� inclou el codi 'scs_codi' i la descripci� 'ics_desc'.
        """
        
        self.centres, self.centres_urg = dict(), dict()

        sql = """
                SELECT
                    scs_codi,
                    ics_desc
                FROM
                    cat_centres
              """
        for up, desc in u.getAll(sql, "nodrizas"):
            self.centres[up] = {"desc": desc}
    
        sql = """
                SELECT
                    scs_codi,
                    ics_codi 
                FROM
                    urg_centres
            """
        for up, desc in u.getAll(sql, "nodrizas"):
            self.centres_urg[up] = {"desc": desc}

        sql = """
                SELECT
                    up_cod,
                    regio_des
                FROM
                    dwsisap.dbc_rup
              """
        for up, regio_sanitaria in u.getAll(sql, "exadata"):
            if up in self.centres:
                self.centres[up]["regio_sanitaria"] = regio_sanitaria
            if up in self.centres_urg:
                self.centres_urg[up]["regio_sanitaria"] = regio_sanitaria

    def get_serologies_cataleg(self):
        """ M�tode per obtenir i emmagatzemar informaci� de serologies a self.serologies_cataleg.
            La informaci� inclou el codi i l'agrupador corresponent.
        """

        sql = """
                SELECT
                    codi,
                    agrupador
                FROM
                    cat_dbscat
                WHERE
                    taula = 'serologies'
                    AND agrupador IN ('V_VHC_SEROLOGIA', 'V_VHC_CARREGA', 'V_VHB_SEROLOGIA_AG_S', 'V_VHB_SEROLOGIA_AG_E', 
                                      'V_VHB_SEROLOGIA_AC_S', 'V_VHB_SEROLOGIA_AC_C_IGG', 'V_VHB_SEROLOGIA_AC_C_IGM',
                                      'V_VHB_SEROLOGIA_AC_E', 'V_VHB_CARREGA')        
              """       
        self.serologies_cataleg = {codi: agrupador for codi, agrupador in u.getAll(sql, "import")}

    def get_nacionalitats_cataleg(self):
        """ M�tode per obtenir i emmagatzemar informaci� de nacionalitats a self.nacionalitats_cataleg.
            La informaci� inclou el 'codi_nac' i la 'desc_nac' corresponent."""

        sql = """
                SELECT
                    codi_nac,
                    desc_nac
                FROM
                    cat_nacionalitat        
                """
        self.nacionalitats_cataleg = {str(int(cod)): desc for cod, desc in u.getAll(sql, "nodrizas")}

    def get_poblacio(self):
        """ . """
        
        self.poblacio = dict()
        self.conversor_id_cip = dict()
        self.poblacio_historica = c.defaultdict(dict)
        
        sql = """
                SELECT
                    id_cip,
                    id_cip_sec,
                    usua_nacionalitat,
                    usua_data_naixement,
                    usua_sexe
                FROM
                    assignada
                WHERE
                    usua_sexe != 'M'
              """
        for id_cip, id_cip_sec, nacionalitat, data_naixement, sexe in u.getAll(sql, "import"):
            nacionalitat = str(nacionalitat)
            self.conversor_id_cip[id_cip_sec] = id_cip
            if id_cip not in self.poblacio:
                self.poblacio[id_cip] = {'sexe': sexe, 'data_naixement': data_naixement, 'nacionalitat': self.nacionalitats_cataleg.get(nacionalitat, "")}
            else:
                if nacionalitat != '' and nacionalitat != '724':
                    self.poblacio[id_cip]['nacionalitat'] = self.nacionalitats_cataleg.get(nacionalitat, "")
                elif nacionalitat == '724' and self.poblacio[id_cip]['nacionalitat'] == "":
                    self.poblacio[id_cip]['nacionalitat'] = self.nacionalitats_cataleg.get(nacionalitat, "")
        print("     import.assignada")
        taules_historiques = [("assignadahistorica_s{}".format(year), year) for year in PERIODE]
        for poblacio_historica, year in u.multiprocess(sub_poblacio_historica, taules_historiques, 4):
            self.poblacio_historica[year] = poblacio_historica
            print("     import.assignadahistorica_s{}".format(year))

    def get_u11(self):
        """ . """

        sql = """
                SELECT
                    id_cip,
                    hash_d
                FROM
                    u11
              """
        for id_cip, hash_d in u.getAll(sql, "import"):
            if id_cip in self.poblacio:
                self.poblacio[id_cip]["hash_d"] = hash_d

    def get_md_poblacio(self):
        """ . """

        sql = """
                SELECT
                    usua_nia,
                    usua_cip
                FROM
                    md_poblacio
                WHERE
                    usua_nia IS NOT NULL
              """
        self.conversor_hash_nia = {hash_d: nia for nia, hash_d in u.getAll(sql, "redics")}

        sql = """
                SELECT
                    hash_redics,
                    hash_covid
                FROM
                    pdptb101_relacio
              """
        self.conversor_hash_exadata = {hash_d: hash_exadata for hash_d, hash_exadata in u.getAll(sql, "pdp")}

    def get_serologies(self):
        """."""

        self.serologies = c.defaultdict(lambda: c.defaultdict(dict))

        sql = """
                SELECT
                    id_cip_sec,
                    cod,
                    dat,
                    val
                FROM
                    nod_serologies
            """
        for id_cip_sec, codi, data_serologia, valor in u.getAll(sql, "nodrizas"):
            if codi in self.serologies_cataleg and id_cip_sec in self.conversor_id_cip:
                year = int(data_serologia[:4])
                month = int(data_serologia[4:6])
                day = int(data_serologia[6:])
                data_serologia = datetime.date(year, month, day)
                id_cip = self.conversor_id_cip[id_cip_sec]
                self.serologies[id_cip][self.serologies_cataleg[codi]][data_serologia] = valor

    def get_visit_subtables(self):
        """ M�tode per obtenir i emmagatzemar subtaules de visites les dates de les quals 
            estiguin en els anys de la tupla PERIODE a self.visit_subtables.
            S'exclouen la table corresponent al s6951'.
        """

        self.visit_subtables = list()     
        jobs = [table for table in u.getSubTables("visites") if table[-6:] != '_s6951']
        for visit_subtable in u.multiprocess(sub_get_visit_subtables, jobs, 4):
            if visit_subtable:
                self.visit_subtables.append(visit_subtable) 

    def get_visites(self):
        
        self.visites, self.atesEAP = c.defaultdict(set), c.defaultdict(set)
        for visites, atesEAP in u.multiprocess(sub_get_visites, self.visit_subtables, 4):
            for id_cip in visites:
                self.visites[id_cip].update(visites[id_cip])
            for year in atesEAP:
                self.atesEAP[year].update(atesEAP[year])

    def get_visites_CUAP(self):
        
        self.visitesCUAP, self.atesCUAP = c.defaultdict(set), c.defaultdict(set)
        sub_taules = [(PERIODE[0], sub_taula) for sub_taula in u.getSubTables("urg_visites", ordenat=True)]
        for visitesCUAP, atesCUAP in u.multiprocess(sub_get_visites_CUAP, sub_taules, 4):
            for id_cip in visitesCUAP:
                self.visitesCUAP[id_cip].update(visitesCUAP[id_cip])
            for year in atesCUAP:
                self.atesCUAP[year].update(atesCUAP[year])

    def get_derivacions(self):
        """."""

        self.derivacions = c.defaultdict(set)
        sql = """
                SELECT
                    id_cip_sec,
                    oc_data
                FROM
                    nod_derivacions
                WHERE
                    inf_servei_d_codi IN ('CL017', '10105', '10101', '10125')
              """
        for id_cip_sec, oc_data in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.conversor_id_cip:
                id_cip = self.conversor_id_cip[id_cip_sec]
                self.derivacions[id_cip].add(oc_data)

    def get_problemes(self):

        self.problemes = c.defaultdict(lambda: c.defaultdict(dict))

        sub_taules = u.getSubTables("problemes", ordenat=True)
        # for problemes in u.multiprocess(sub_get_problemes, sub_taules, 4):
        for sub_taula in sub_taules:
            problemes = sub_get_problemes(sub_taula)
            self.problemes.update(problemes)

    def get_table(self):
        
        cols_nod = "(hash varchar(200), \
                    nia int, \
                    regio_sanitaria varchar(200), \
                    codi_sector varchar(200), \
                    up varchar(200), \
                    edat varchar(200), \
                    sexe varchar(200), \
                    nacionalitat varchar(200), \
                    atesEAP int, \
                    ates int, \
                    atesCUAP int, \
                    vhc_serologia_data date, \
                    vhc_serologia_valor int, \
                    vhc_carrega_data date, \
                    vhc_carrega_valor int, \
                    vhc_dx_previ_data date, \
                    vhc_dx_previ_tancat_data date, \
                    vhc_dx_tipus varchar(200), \
                    vhc_der_data date, \
                    vhb_dx_previ_data date, \
                    vhb_dx_tipus varchar(200), \
                    vhb_ag_s_data date, \
                    vhb_ag_s_valor int, \
                    vhb_ag_e_data date, \
                    vhb_ag_e_valor int, \
                    vhb_serologia_ac_c_igg_data date, \
                    vhb_serologia_ac_c_igg_valor int, \
                    vhb_serologia_ac_c_igm_data date, \
                    vhb_serologia_ac_c_igm_valor int, \
                    vhb_serologia_ac_e_data date, \
                    vhb_serologia_ac_e_valor int, \
                    vhb_serologia_ac_s_data date, \
                    vhb_serologia_ac_s_valor int, \
                    vhb_carrega_data date, \
                    vhb_carrega_valor int, \
                    vhb_der_data date)"

        cols_exa = "(hash VARCHAR2(200), \
                    nia NUMBER, \
                    regio_sanitaria VARCHAR2(200), \
                    codi_sector VARCHAR2(200), \
                    up VARCHAR2(200), \
                    edat VARCHAR2(200), \
                    sexe VARCHAR2(200), \
                    nacionalitat VARCHAR2(200), \
                    atesEAP NUMBER, \
                    ates NUMBER, \
                    atesCUAP NUMBER, \
                    vhc_serologia_data DATE, \
                    vhc_serologia_valor NUMBER, \
                    vhc_carrega_data DATE, \
                    vhc_carrega_valor NUMBER, \
                    vhc_dx_previ_data DATE, \
                    vhc_dx_previ_tancat_data DATE, \
                    vhc_dx_tipus VARCHAR2(200), \
                    vhc_der_data DATE, \
                    vhb_dx_previ_data DATE, \
                    vhb_dx_tipus VARCHAR2(200), \
                    vhb_ag_s_data DATE, \
                    vhb_ag_s_valor NUMBER, \
                    vhb_ag_e_data DATE, \
                    vhb_ag_e_valor NUMBER, \
                    vhb_serologia_ac_c_igg_data DATE, \
                    vhb_serologia_ac_c_igg_valor NUMBER, \
                    vhb_serologia_ac_c_igm_data DATE, \
                    vhb_serologia_ac_c_igm_valor NUMBER, \
                    vhb_serologia_ac_e_data DATE, \
                    vhb_serologia_ac_e_valor NUMBER, \
                    vhb_serologia_ac_s_data DATE, \
                    vhb_serologia_ac_s_valor NUMBER, \
                    vhb_carrega_data DATE, \
                    vhb_carrega_valor NUMBER, \
                    vhb_der_data DATE)"

        for year in PERIODE:
            table = "Nou_HepBClink_{}".format(year)
            u.createTable(table, cols_nod, "test", rm=True)
            u.createTable(table, cols_exa, "exadata", rm=True)
            u.grantSelect(table, "DWSISAP_ROL", "exadata")
            table_files = list()
            for id_cip in self.poblacio_historica[year]:

                # Dades Generals
                if id_cip in self.poblacio:
                    hash_d = self.poblacio[id_cip]["hash_d"]
                    if hash_d in self.conversor_hash_nia and hash_d in self.conversor_hash_exadata:
                        hash_exadata = self.conversor_hash_exadata[hash_d]
                        nia = self.conversor_hash_nia[hash_d]
                        codi_sector = self.poblacio_historica[year][id_cip]["codi_sector"]
                        up = self.poblacio_historica[year][id_cip]["up"]
                        regio_sanitaria = self.centres[up]["regio_sanitaria"] if up in self.centres else (self.centres_urg[up]["regio_sanitaria"] if up in self.centres_urg else "")
                        edat = u.yearsBetween(self.poblacio[id_cip]["data_naixement"], datetime.date(year,12,31))
                        sexe = self.poblacio[id_cip]["sexe"]
                        nacionalitat = self.poblacio[id_cip]["nacionalitat"]
                        atesEAP = 1 if id_cip in self.atesEAP[year] else 0
                        ates = self.poblacio_historica[year][id_cip]["ates"]
                        atesCUAP = 1 if id_cip in self.atesCUAP[year] else 0
                        # Dades Hepatitis C
                        vhc_serologia_dates = (data_serologia for data_serologia in self.serologies[id_cip]["V_VHC_SEROLOGIA"] if data_serologia <= datetime.date(year, 12, 31)) if self.serologies[id_cip]["V_VHC_SEROLOGIA"] else None
                        try:
                            vhc_serologia_data = max(vhc_serologia_dates) if vhc_serologia_dates else None
                        except:
                            vhc_serologia_data = None
                        vhc_serologia_valor = self.serologies[id_cip]["V_VHC_SEROLOGIA"].get(vhc_serologia_data, None) if vhc_serologia_data else None
                        vhc_carrega_dates = (data_carrega for data_carrega in self.serologies[id_cip]["V_VHC_CARREGA"] if data_carrega <= datetime.date(year, 12, 31)) if self.serologies[id_cip]["V_VHC_CARREGA"] else None
                        try:
                            vhc_carrega_data = max(vhc_carrega_dates) if vhc_carrega_dates else None
                        except:
                            vhc_carrega_data = None
                        vhc_carrega_valor = self.serologies[id_cip]["V_VHC_CARREGA"].get(vhc_carrega_data, None) if vhc_carrega_data else None
                        vhc_dx_previ_dates = (data_dx for data_dx in self.problemes[id_cip]["Hepatitis C"] if data_dx <= datetime.date(year, 12, 31)) if self.problemes[id_cip]["Hepatitis C"] else None
                        try:
                            vhc_dx_previ_data = min(vhc_dx_previ_dates) if vhc_dx_previ_dates else None
                        except:
                            vhc_dx_previ_data = None
                        dx_dates = self.problemes[id_cip]["Hepatitis C"].keys() if id_cip in self.problemes else None
                        try:
                            vhc_dx_previ_tancat_data = self.problemes[id_cip]["Hepatitis C"][vhc_dx_previ_data]["data_fi"] if dx_dates else None 
                        except:
                            vhc_dx_previ_tancat_data = None
                        try:
                            vhc_dx_tipus = self.problemes[id_cip]["Hepatitis C"][vhc_dx_previ_data]["duracio"] if dx_dates else None 
                        except:
                            vhc_dx_tipus = None
                        vhc_der_dates = (der_data for der_data in self.derivacions[id_cip] if der_data <= datetime.date(year, 12, 31) and 0 <= u.monthsBetween(vhc_dx_previ_data, der_data) < 6) if self.derivacions[id_cip] and vhc_dx_previ_data else None
                        try:
                            vhc_der_data = min(vhc_der_dates) if vhc_der_dates else None
                        except:
                            vhc_der_data = None
                        # Dades Hepatitis B
                        vhb_dx_previ_dates = (data_dx for data_dx in self.problemes[id_cip]["Hepatitis B"] if data_dx <= datetime.date(year, 12, 31)) if self.problemes[id_cip]["Hepatitis B"] else None
                        try:
                            vhb_dx_previ_data = min(vhb_dx_previ_dates) if vhb_dx_previ_dates else None
                        except:
                            vhb_dx_previ_data = None
                        vhb_dx_tipus = self.problemes[id_cip]["Hepatitis B"][vhb_dx_previ_data]["duracio"] if vhb_dx_previ_data else None
                        vhb_ag_s_dates = (data_ag for data_ag in self.serologies[id_cip]["V_VHB_SEROLOGIA_AG_S"] if data_ag <= datetime.date(year, 12, 31)) if self.serologies[id_cip]["V_VHB_SEROLOGIA_AG_S"] else None
                        try:
                            vhb_ag_s_data = max(vhb_ag_s_dates) if vhb_ag_s_dates else None
                        except:
                            vhb_ag_s_data = None
                        vhb_ag_s_valor = self.serologies[id_cip]["V_VHB_SEROLOGIA_AG_S"].get(vhb_ag_s_data, None) if vhb_ag_s_data else None
                        vhb_ag_e_dates = (data_ag for data_ag in self.serologies[id_cip]["V_VHB_SEROLOGIA_AG_E"] if data_ag <= datetime.date(year, 12, 31)) if self.serologies[id_cip]["V_VHB_SEROLOGIA_AG_E"] else None
                        try:
                            vhb_ag_e_data = max(vhb_ag_e_dates) if vhb_ag_e_dates else None
                        except:
                            vhb_ag_e_data = None
                        vhb_ag_e_valor = self.serologies[id_cip]["V_VHB_SEROLOGIA_AG_E"].get(vhb_ag_e_data, None) if vhb_ag_e_data else None
                        vhb_serologia_ac_c_igg_dates = (data_serologia for data_serologia in self.serologies[id_cip]["V_VHB_SEROLOGIA_AC_C_IGG"] if data_serologia <= datetime.date(year, 12, 31)) if self.serologies[id_cip]["V_VHB_SEROLOGIA_AC_C_IGG"] else None
                        try:
                            vhb_serologia_ac_c_igg_data = max(vhb_serologia_ac_c_igg_dates) if vhb_serologia_ac_c_igg_dates else None
                        except:
                            vhb_serologia_ac_c_igg_data = None
                        vhb_serologia_ac_c_igg_valor = self.serologies[id_cip]["V_VHB_SEROLOGIA_AC_C_IGG"].get(vhb_serologia_ac_c_igg_data, None) if vhb_serologia_ac_c_igg_data else None
                        vhb_serologia_ac_c_igm_dates = (data_serologia for data_serologia in self.serologies[id_cip]["V_VHB_SEROLOGIA_AC_C_IGM"] if data_serologia <= datetime.date(year, 12, 31)) if self.serologies[id_cip]["V_VHB_SEROLOGIA_AC_C_IGM"] else None
                        try:
                            vhb_serologia_ac_c_igm_data = max(vhb_serologia_ac_c_igm_dates) if vhb_serologia_ac_c_igm_dates else None
                        except:
                            vhb_serologia_ac_c_igm_data = None
                        vhb_serologia_ac_c_igm_valor = self.serologies[id_cip]["V_VHB_SEROLOGIA_AC_C_IGM"].get(vhb_serologia_ac_c_igm_data, None) if vhb_serologia_ac_c_igm_data else None
                        vhb_serologia_ac_e_dates = (data_serologia for data_serologia in self.serologies[id_cip]["V_VHB_SEROLOGIA_AC_E"] if data_serologia <= datetime.date(year, 12, 31)) if self.serologies[id_cip]["V_VHB_SEROLOGIA_AC_E"] else None
                        try:
                            vhb_serologia_ac_e_data = max(vhb_serologia_ac_e_dates) if vhb_serologia_ac_e_dates else None
                        except:
                            vhb_serologia_ac_e_data = None
                        vhb_serologia_ac_e_valor = self.serologies[id_cip]["V_VHB_SEROLOGIA_AC_E"].get(vhb_serologia_ac_e_data, None) if vhb_serologia_ac_e_data else None
                        vhb_serologia_ac_s_dates = (data_serologia for data_serologia in self.serologies[id_cip]["V_VHB_SEROLOGIA_AC_S"] if data_serologia <= datetime.date(year, 12, 31)) if self.serologies[id_cip]["V_VHB_SEROLOGIA_AC_S"] else None
                        try:
                            vhb_serologia_ac_s_data = max(vhb_serologia_ac_s_dates) if vhb_serologia_ac_s_dates else None
                        except:
                            vhb_serologia_ac_s_data = None
                        vhb_serologia_ac_s_valor = self.serologies[id_cip]["V_VHB_SEROLOGIA_AC_S"].get(vhb_serologia_ac_s_data, None) if vhb_serologia_ac_s_data else None
                        vhb_carrega_dates = (data_carrega for data_carrega in self.serologies[id_cip]["V_VHB_CARREGA"] if data_carrega <= datetime.date(year, 12, 31)) if self.serologies[id_cip]["V_VHB_CARREGA"] else None
                        try:
                            vhb_carrega_data = max(vhb_carrega_dates) if vhb_carrega_dates else None
                        except:
                            vhb_carrega_data = None
                        vhb_carrega_valor = self.serologies[id_cip]["V_VHB_CARREGA"].get(vhb_carrega_data, None) if vhb_carrega_data else None
                        vhb_der_dates = (der_data for der_data in self.derivacions[id_cip] if der_data <= datetime.date(year, 12, 31) and 0 <= u.monthsBetween(vhb_dx_previ_data, der_data) < 6) if self.derivacions[id_cip] and vhb_dx_previ_data else None
                        try:
                            vhb_der_data = max(vhb_der_dates) if vhb_der_dates else None
                        except:
                            vhb_der_data = None

                        fila = (hash_exadata, nia, regio_sanitaria, codi_sector, up, edat, sexe, nacionalitat, atesEAP, ates, atesCUAP, vhc_serologia_data, vhc_serologia_valor,
                                vhc_carrega_data, vhc_carrega_valor, vhc_dx_previ_data, vhc_dx_previ_tancat_data, vhc_dx_tipus, vhc_der_data,
                                vhb_dx_previ_data, vhb_dx_tipus, vhb_ag_s_data, vhb_ag_s_valor, vhb_ag_e_data, vhb_ag_e_valor, vhb_serologia_ac_c_igg_data,
                                vhb_serologia_ac_c_igg_valor, vhb_serologia_ac_c_igm_data, vhb_serologia_ac_c_igm_valor, vhb_serologia_ac_e_data,
                                vhb_serologia_ac_e_valor, vhb_serologia_ac_s_data, vhb_serologia_ac_s_valor, vhb_carrega_data, vhb_carrega_valor, vhb_der_data)

                        table_files.append(fila)
            
            u.listToTable(table_files, table, "test")
            u.listToTable(table_files, table, "exadata")

def sub_get_visit_subtables(table):
    sql = "select year(visi_data_visita) from {} limit 1".format(table)
    year = u.getOne(sql, "import")[0]
    if year in PERIODE:
        return table

def sub_poblacio_historica(input):

    table, year = input
    poblacio_historica = dict()
    sql = """
            SELECT
                id_cip,
                codi_sector,
                up,
                ates
            FROM
                {}
          """.format(table)
    for id_cip, codi_sector, up, ates in u.getAll(sql, "import"):
        poblacio_historica[id_cip] = {"codi_sector": codi_sector, "up": up, "ates": ates}
    
    return poblacio_historica, year

def sub_get_visites(table):
    """."""

    visites = c.defaultdict(set)
    atesEAP = c.defaultdict(set)

    sql = """
            SELECT
                id_cip,
                codi_sector,
                visi_data_visita
            FROM
                {}
            WHERE 
                visi_situacio_visita = 'R'    
          """.format(table)
    for id_cip, codi_sector, data_visita in u.getAll(sql, "import"):
        visites[id_cip].add((data_visita, codi_sector))
        year = data_visita.year
        atesEAP[year].add(id_cip)

    return visites, atesEAP

def sub_get_visites_CUAP(input):
    """."""

    year, sub_taula = input

    visitesCUAP = c.defaultdict(set)
    atesCUAP = c.defaultdict(set)

    sql = """
            SELECT
                id_cip,
                codi_sector,
                vcu_dia_peticio
            FROM
                {}
            WHERE
                vcu_situacio_visita = 'R'
                AND YEAR(vcu_dia_peticio) = {}
          """.format(sub_taula, year)
    for id_cip, codi_sector, data_visita in u.getAll(sql, "import"):
        visitesCUAP[id_cip].add((data_visita, codi_sector))
        year = data_visita.year
        atesCUAP[year].add(id_cip)

    return visitesCUAP, atesCUAP

def sub_get_problemes(table):

    problemes = c.defaultdict(lambda: c.defaultdict(dict))

    problemes_codis = {'B18.2': {'diagnostic': 'Hepatitis C', 'duracio': 'Cr�nica'},
                       'B19.2': {'diagnostic': 'Hepatitis C', 'duracio': 'Cr�nica'},
                       'B19.20': {'diagnostic': 'Hepatitis C', 'duracio': 'Cr�nica'},
                       'B19.21': {'diagnostic': 'Hepatitis C', 'duracio': 'Cr�nica'},
                       'B17.1': {'diagnostic': 'Hepatitis C', 'duracio': 'Aguda'},
                       'B17.10': {'diagnostic': 'Hepatitis C', 'duracio': 'Aguda'},
                       'B17.11': {'diagnostic': 'Hepatitis C', 'duracio': 'Aguda'},
                       'B18.0': {'diagnostic': 'Hepatitis B', 'duracio': 'Cr�nica'},
                       'B18.1': {'diagnostic': 'Hepatitis B', 'duracio': 'Cr�nica'},
                       'B19.1': {'diagnostic': 'Hepatitis B', 'duracio': 'Cr�nica'},
                       'B19.10': {'diagnostic': 'Hepatitis B', 'duracio': 'Cr�nica'},
                       'B19.11': {'diagnostic': 'Hepatitis B', 'duracio': 'Cr�nica'},
                       'B16': {'diagnostic': 'Hepatitis B', 'duracio': 'Aguda'},
                       'B16.0': {'diagnostic': 'Hepatitis B', 'duracio': 'Aguda'},
                       'B16.1': {'diagnostic': 'Hepatitis B', 'duracio': 'Aguda'},
                       'B16.2': {'diagnostic': 'Hepatitis B', 'duracio': 'Aguda'},
                       'B16.9': {'diagnostic': 'Hepatitis B', 'duracio': 'Aguda'}
                       }

    # Creaci� d'una llista on apareixen tots els codis originals de problemes_codis i tamb� aquests codis amb el prefix "C01-" afegit.
    problemes_codis_complet = [item for codi_original in problemes_codis.keys() for item in (codi_original, "C01-{}".format(codi_original))]

    sql = """
            SELECT
                id_cip,
                pr_cod_ps,
                pr_dde,
                pr_dba 
            FROM
                import.{}
            WHERE pr_cod_ps IN {}
          """.format(table, tuple(problemes_codis_complet))
    for id_cip, pr_cod_ps, data_inici, data_fi in u.getAll(sql, "import"):
        pr_cod_ps = pr_cod_ps.replace("C01-", "")
        diagnostic = problemes_codis[pr_cod_ps]["diagnostic"]
        duracio = problemes_codis[pr_cod_ps]["duracio"]
        problemes[id_cip][diagnostic][data_inici] = {"data_fi": data_fi, "duracio": duracio}

    return problemes    

if __name__ == "__main__":
    HepatitisC()