# executar a x0002 o nymeria (ram)

import collections as c
import multiprocessing as m

import sisaptools as u


db_o = ('nymeria_d', 'sidiap_data')
tb_d = 'sisap_atc_up'
db_d = ('redics', 'data')
nod = ('p2262', 'nodrizas')


def get_data(p):
    data = c.defaultdict(set)
    sql = "select id_cip, rec_any_mes, rec_up_agr, pf_cod_atc \
           from facturacio partition (p{})".format(p)
    for id, dat, up, atc in u.Database(*db_o).get_all(sql):
        for i in (4, 6):
            for j in (3, 5, 7):
                data[':'.join((dat[:i], up, atc[:j]))].add(id)
    resul = [(key, len(pacs)) for (key, pacs) in data.items()]
    return resul


if __name__ == '__main__':
    pool = m.Pool(16)
    mega_resul = pool.map(get_data, range(32))
    pool.close()
    resultat = c.defaultdict(int)
    for resul in mega_resul:
        for (key, n) in resul:
            resultat[tuple(key.split(':'))] += n
    centres = {up: br for (up, br) in
               u.Database(*nod).get_all("select scs_codi, ics_codi \
                                         from cat_centres where ep = '0208'")}
    upload = [(dat, centres[up], atc, n) for (dat, up, atc), n
              in resultat.items() if up in centres]
    with u.Database(*db_d) as desti:
        desti.create_table(tb_d, ('dat varchar2(6)', 'eap varchar2(5)',
                                  'atc varchar2(7)', 'n number'), remove=True)
        desti.list_to_table(upload, tb_d)
        desti.set_grants('select', tb_d, 'PREDUMMP')
