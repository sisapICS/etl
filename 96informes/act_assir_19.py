import sisapUtils as u
from collections import defaultdict
import random

# S'utilitza nodrizas.ass_visites1, que cal tirar abans pq s'elimina
# (a ass_visites no hi ha homes). Compte que a la taula hi ha 18 mesos.
YEAR = 2020

nod = "nodrizas"
imp = "import"
write = True


def get_ass_visites1():
    tb = "ass_visites1"
    db = "nodrizas"
    columns = [
        "id_cip_sec",
        "visi_data_visita",
        "visi_up",
        "visi_dni_prov_resp",
        "visi_dia_peticio",
        "codi_sector",
        "visi_tipus_visita",
        "visi_tipus_citacio",
    ]

    sqls = (
        "drop table if exists {tb}".format(tb=tb),
        """
        create table {tb} as
        select {cols} from import.visites2 where 1 = 0
        """.format(tb=tb, cols=", ".join(columns)),
        )

    for sql in sqls:
        u.execute(sql, db)

    sql = """
        select distinct {cols}
        from
            import.visites2 a
            ,dextraccio
        where
            visi_data_visita between date_add(
                date_add(data_ext, interval -18 month),
                         interval +1 day) and data_ext AND
            visi_situacio_visita = 'R' and
            (s_espe_codi_especialitat in ('10099','10098','30084','30085') OR
            (s_espe_codi_especialitat = '30999' and
                visi_up in (
                    select up
                    from ass_centres
                    where
                    br like 'SD%' and
                    amb_desc like '%METRO%SUD%')
                    )
            ) and
            visi_modul_codi_modul not like 'ECO%' and
            visi_etiqueta not like 'ECO%' and
            visi_tipus_visita not in ('U', 'URG', 'GR')
        """.format(cols=", ".join(columns))
    upload = [row for row in u.getAll(sql, db)]
    u.listToTable(upload, tb, db)
    u.execute("alter table ass_visites1 add index (id_cip_sec)", db)


def get_embaras_actiu():
    """ Selecciona los id_cip_sec de:
            las mujeres
            en assir
            con embarazo activo en YEAR
                (que la fecha de inicio OR la de fin sea en YEAR) AND
                 que al menos hayan pasado mas de 3 meses de gestacion
        Devuelve un set de id_cip_sec
    """
    sql = "select id_cip_sec, inici, fi from {0}.ass_embaras;".format(nod)
    embaras_ids = {
        id: (inici, fi) for id, inici, fi in u.getAll(sql, nod)
        if u.monthsBetween(inici, fi) > 3 and
        (inici.year == YEAR or fi.year == YEAR)
        }
    return embaras_ids


def get_br_assir():
    sql = """
        select up, up_assir, br_assir, assir from {}.ass_centres
        """.format(nod)
    up_assir_dict = {}
    for up, up_assir, br, assir in u.getAll(sql, nod):
        up_assir_dict[up] = (up_assir, br, assir)
    return up_assir_dict


def get_pacient_sexe():
    sql = "select id_cip_sec, usua_sexe from {}.assignada".format(imp)
    return {id: sexe for id, sexe in u.getAll(sql, nod)}


def get_vis_up(embaras_id, id_sexe, up_br):
    sql = """
        select id_cip_sec,visi_up,visi_data_visita
        from {}.ass_visites1
        where year(visi_data_visita)={}
        """.format(nod, YEAR)

    vis = defaultdict(lambda: defaultdict(set))
    for id, up, data in u.getAll(sql, nod):
        if up in up_br and id in id_sexe:
            up_assir = up_br[up]
            vis[up_assir]["T"].add(id)
            sexe = id_sexe[id]

            if sexe == "D":
                vis[up_assir]["DT"].add(id)

            if id in embaras_id:
                vis[up_assir]["E"].add(id)
                if not embaras_id[id][0] < data < embaras_id[id][1]:
                    vis[up_assir]["D"].add(id)
            else:
                vis[up_assir][sexe].add(id)

    rows = [(up[0], up[1], up[2],
             len(vis[up]["E"]),
             len(vis[up]["D"]),
             len(vis[up]["DT"]),
             len(vis[up]["H"]),
             len(vis[up]["T"]))
            for up in vis]
    return rows


if __name__ == '__main__':
    u.printTime('inici')
    get_ass_visites1()
    u.printTime('ass_visites1')

    embaras_id = get_embaras_actiu()
    print(random.sample(embaras_id, 5))
    u.printTime("Emb activas")

    id_sexe = get_pacient_sexe()
    u.printTime("Id sexe")

    up_br = get_br_assir()
    u.printTime("Br assir")

    rows = get_vis_up(embaras_id, id_sexe, up_br)
    u.printTime('Get Rows')

    for row in rows[:3]:
        print(row)

    if write:
        header = [("UP ASSIR", "BR ASSIR", "ASSIR DESC", "EMBARASSADES",
                   "DONES NO EMBARASSADES", "DONES TOTAL", "HOMES",
                   "POBLACIO TOTAL")]
        u.writeCSV(u.tempFolder + "ACTIVITAT_ASSIR.CSV",
                   header + rows,
                   sep=";")
        """
        with open() as out_file:
            row_format ="{:>40}" * len(rows[0])+"\n"
            out_file.write(row_format.format(*header))
            for row in rows:
                out_file.write(row_format.format(*row))
        """
