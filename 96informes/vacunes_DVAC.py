# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    print('RESULTAT:')
    print(res[:1000])
    print('')
    return res
    

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

atesa = exemple(c, 'DVACUNES###;AYR20###;AMBITOS;NUM,DEN,AGRESULT;EDATS5;NOINSAT;SEXE')
assignada = exemple(c, 'DVACUNES###;AYR20###;AMBITOS;NUM,DEN,AGRESULT;EDATS5;NOINSASS;SEXE')




c.close()

print('export')

file = u.tempFolder + "DVAC_atesa_2020.csv"
with open(file, 'w') as f:
   f.write(atesa)
   
   file = u.tempFolder + "DVAC_assignada_2020.csv"
with open(file, 'w') as f:
   f.write(assignada)
