

import sisapUtils as u
import collections as c

sql = """select year(cr_data_reg), cr_codi_prova_ics, codi_up
        from import.laboratori2
        where cr_codi_prova_ics in ('011448', '010324', 'S18385', '007032', '011446', '011315')
        and year (cr_data_reg) in (2021, 2022)"""

proves = c.Counter()
for year, prova, up in u.getAll(sql, 'import'):
    proves[(year, prova, up)] += 1

upload = []
nom = {
    '011448': 'S087CLISE',
    '010324': 'S207CLISE',
    'S18385': 'S162EIASE',
    '007032': 'S276CLISE',
    '011446': 'S084CLISE',
    '011315': 'D025CLISE'
}

desc = {}
sql = """select scs_codi, ics_desc from nodrizas.cat_centres"""
for up, des in u.getAll(sql, 'nodrizas'):
    desc[up] = des

for (year, prova, up), v in proves.items():
    if up in desc:
        upload.append((year, up, desc[up], nom[prova], v))

cols = """(periode int, up varchar(5), desc_up varchar(100), prova varchar(100), unitats int)"""
u.createTable('cribmi', cols, 'test')
u.listToTable(upload, 'cribmi', 'test')