import sisapUtils as u
import sisaptools as su
import datetime as d
import collections as c


cols = "(id_cip_sec int, up varchar(7), esiap0602 int, res023a1_num int, res023a1_den int, res024a1_num int, res024a1_den int, val_elisabet int, val_souhel int)"
TaulaPIIC = 'piic'
taulaAss = 'assignada_tot'
DEXTD = su.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]  # noqa

poblacio = {}
dades = c.defaultdict(set)
sql = """
    SELECT
        id_cip_sec,
        up, uba, upinf, ubainf, edat
    FROM {}
    WHERE up = '04054'
    """.format(taulaAss)
for id, up, uba, upinf, ubainf, edat in u.getAll(sql, 'nodrizas'):
    edat = int(edat)
    poblacio[int(id)] = {'up': up,
                         'uba': uba,
                         'upinf': upinf,
                         'ubainf': ubainf,
                         'edat': edat}
u.printTime('pob')
piirV = {}
piirT = {}

sql = """
    SELECT
        id_cip_sec,
        if(mi_cod_var like ('T4101014%'),'var','text'),
        right(mi_cod_var,2)
    FROM {}
    WHERE
        (mi_cod_var like ('T4101015%') or mi_cod_var like ('T4101014%'))
        and mi_ddb = 0
    """.format(TaulaPIIC)
for id, camp, valor in u.getAll(sql, 'import'):
    id = int(id)
    if camp == 'var':
        piirV[(id, valor)] = True
    elif camp == 'text':
        piirT[(id, valor)] = True
u.printTime('piir')
piir = {}
for (id, valor), r in piirV.items():
    if (id, valor) in piirT:
        piir[id] = True
u.printTime('piir2')
sql = """
    SELECT
        id_cip_sec,
        if(mi_cod_var = 'T4101001', 'rec',
            if(mi_cod_var = 'T4101002' or mi_cod_var like 'T4101017%', 'dant',
            '0')
        )
    FROM {}
    WHERE mi_ddb = 0
    """.format(TaulaPIIC)
piic = {}

for id, tipii in u.getAll(sql, 'import'):
    piic[int(id)] = int(id)
    if tipii == 'rec':
        piir[int(id)] = True
u.printTime('piic')
ConvertPccMaca = """
    CASE es_cod
        WHEN 'ER0001' THEN 'PCC'
        WHEN 'ER0002' THEN 'MACA'
        ELSE 'error'
    END
"""
TaulaEstats = 'estats'

sql = """
    SELECT id_cip_sec,{0},es_up
    FROM {1}
    where es_dde <= DATE '{2}'
    """.format(ConvertPccMaca, TaulaEstats, DEXTD)

for id, estat, up_estat in u.getAll(sql, 'import'):
    if id in piir:
        dades['esiap0602'].add(id)
u.printTime('pcc_maca')
sql = """
    SELECT distinct id_cip_sec, mi_cod_var
    FROM piic
    WHERE left(mi_cod_var, 8) in ('T4101001', 'T4101002', 'T4101017', 'T4101015', 'T4101014')
    and DATE '{DEXTD}' between mi_data_reg and if(mi_ddb = 0, DATE '{DEXTD}', mi_ddb)
""".format(DEXTD=DEXTD)
piir = c.defaultdict()
piic = c.defaultdict()
piirv = c.defaultdict(list)
piirt = c.defaultdict(list)
for id, var in u.getAll(sql, 'import'):
    if var[:8] == 'T4101001':
        piir[id] = 1
    if var[:8] in ('T4101001', 'T4101002', 'T4101017'):
        piic[id] = 1
    elif var[:8] in ('T4101014'):
        piirv[id].append(var[8:])
    elif var[:8] in ('T4101015'):
        piirt[id].append(var[8:])
u.printTime('piir')
val_souhel = c.Counter()
for id in piirv.keys():
    if id in piirt:
        for codi in piirv[id]:
            if codi in piirt[id]:
                val_souhel[id] += 1

sql = "select id_cip_sec, es_cod from estats where es_cod in ('ER0001', 'ER0002') and es_dde <= DATE '{DEXTD}'".format(DEXTD=DEXTD)

pcc_maca = c.defaultdict(set)
for id, codi in u.getAll(sql, 'import'):
    if codi == 'ER0001':
        pcc_maca['pcc'].add(id)
    else:
        pcc_maca['maca'].add(id)
u.printTime('pcc_maca')
for var in pcc_maca:
    for id in pcc_maca[var]:
        if var == 'pcc' and id in piic:
            dades['RES023A1_num'].add(id)
        if var == 'maca' and id in piic:
            dades['RES024A1_num'].add(id)
        if var == 'pcc' and id in piic and (id in piir or (id in piirt and id in piirv)):
            dades['RES023A1_den'].add(id)
        if var == 'maca' and id in piic and (id in piir or (id in piirt and id in piirv)):
            dades['RES024A1_den'].add(id)
        if id in piir or (id in piirt and id in piirv):
            dades['val_elisabet'].add(id)
        if id in piir or (id in piirt and id in piirv and id in val_souhel and val_souhel[id] >= 4):
            dades['val_souhel'].add(id)
u.printTime('dades')
upload = []
for id in poblacio:
    up = poblacio[id]['up']
    esiap = 1 if id in dades['esiap0602'] else 0
    R23A1_num = 1 if id in dades['RES023A1_num'] else 0
    R23A1_den = 1 if id in dades['RES023A1_den'] else 0
    R24A1_num = 1 if id in dades['RES024A1_num'] else 0
    R24A1_den = 1 if id in dades['RES024A1_den'] else 0
    val_el = 1 if id in dades['val_elisabet'] else 0
    val_s = 1 if id in dades['val_souhel'] else 0
    upload.append((id, up, esiap, R23A1_num, R23A1_den, R24A1_num, R24A1_den, val_el, val_s))

u.createTable('piic_validacio', cols, 'nodrizas', rm=True)
u.listToTable(upload, 'piic_validacio', 'nodrizas')
u.printTime('fi')
# res023a1: pcc & piic & (piir || (piirT & piirV))
# res024a1: maca & piic & (piir || (piirT & piirV))
# esiap0602...
# val_elisabet: (pcc || maca) & (piir || (piirV & piirT))
# val_souhel: (pcc || maca) & (piir || (piirV & piirT)*4)