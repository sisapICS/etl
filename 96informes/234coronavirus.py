# coding: latin1

"""
.
"""

import collections as c
import csv
import cx_Oracle
import datetime as d
import hashlib as h
import multiprocessing as m
import os

import sisaptools as u


PROD = d.datetime.now().hour > 22 or d.datetime.now().hour < 6
CONN = "prod" if PROD else "data"
SUFFIX = "" if PROD else "_"

CODIS = {"C01-B97.29": (None, "M"),
         "C01-Z20.828": ((16136, 16137, 16138, 16139, 16143, 16144, 16145, 16146), "C"),  # noqa
         "C01-B34.2": (None, "M"),
         "C01-B97.21": (None, "M"),
         "C01-J12.81": (None, "M")}
LITERALS = {"C": "Contactes", "M": "Casos"}

TABLE = "sisap_coronavirus" + SUFFIX
AGRUPAT = "sisap_coronavirus_recompte" + SUFFIX
POBLACIO = "sisap_coronavirus_poblacio" + SUFFIX
LLISTATS = "sisap_coronavirus_llistat" + SUFFIX
RESIDENCIES = "sisap_coronavirus_residencies" + SUFFIX
DATA = "sisap_coronavirus_llistat_data"
CORBA = "sisap_coronavirus_corba" + SUFFIX
PCR = "sisap_coronavirus_pcr" + SUFFIX
VISITES = "sisap_coronavirus_visites" + SUFFIX
ACTIVITAT = "sisap_coronavirus_activitat" + SUFFIX
ACTIVITAT_FROM = (2020, 3, 1)
ACTIVITAT_AGR = "sisap_coronavirus_activitat_a" + SUFFIX
PROFESSIONALS = "sisap_coronavirus_activitat_p" + SUFFIX
XML = (("XML0000115", "sisap_coronavirus_xml" + SUFFIX),
       ("XML0000116", "sisap_coronavirus_xml_seg" + SUFFIX))
RADIOLOGIA = "sisap_coronavirus_radiologia" + SUFFIX
APP = "sisap_coronavirus_app" + SUFFIX

USERS = ("PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB",
         "PREDUEHE")
EXTERNS = ("PREDURAP", "PREDUJVG", "PREDUMPP", "PREDUJCD", "PREDUMER",
           "PREDUCAM", "PREDUNNC", "DDIAZA",   "PREDUJMS", "PREDUDDA",
           "PREDUMAG", "PREDUXMG", "PREDUXGM", "PREDUSLA", "PREDUADT",
           "PREDUXCP", "PREDUABO", "PREDUMAA",)
TODAY = d.datetime.now().date()


class Coronavirus(object):
    """."""

    def __init__(self):
        """."""
        self.get_catalegs()
        self.get_ficticis()
        self.get_dades()
        self.upload_dades()
        self.upload_agrupat()
        self.get_residencies()
        self.get_poblacio()
        self.upload_poblacio()
        self.upload_residencies()
        self.get_pcr()
        self.upload_corba()
        self.get_pneumonies()
        self.get_llistats()
        self.upload_llistats()
        self.get_radiologia()
        self.upload_residencies_casos()
        self.get_visites()
        self.upload_visites()
        self.get_activitat()
        self.agr_activitat()
        self.get_professionals()
        if int(cx_Oracle.version[0]) > 5:
            for form, table in XML:
                self.get_xml(form, table)
        self.get_app()

    def get_catalegs(self):
        """."""
        self.catalegs = {}
        sqls = {"up": ("select up_codi_up_scs, up_desc_up_ics \
                        from gcctb008 a, gcctb007 b \
                        where a.up_codi_up_ics = b.up_codi_up_ics and \
                              a.up_data_baixa is null and \
                              b.up_data_baixa is null", 1, ("6211", CONN)),
                "ps": ("select ps_cod, ps_def_norm from prstb001 \
                        where ps_cod in {}".format(tuple(CODIS.keys())), 1, ("6211", CONN)),  # noqa
                "th": ("select pst_th, pst_des_norm from prstb305 \
                        where pst_cod in {}".format(tuple(CODIS.keys())), 1, ("6211", CONN)),  # noqa
                "co": ("select cent_codi_sector, cent_codi_centre, \
                               cent_classe_centre, cent_nom_centre \
                        from pritb010", 3, ("redics", "data")),
                "et": ("select eti_codi, eti_descripcio \
                        from vistb079", 1, ("6416", CONN)),
                "am": ("select up_codi_up_scs, amb_desc_amb \
                        from gcctb008 a, gcctb007 b, gcctb006 c, gcctb005 d \
                        where a.up_codi_up_ics = b.up_codi_up_ics and \
                              a.up_data_baixa is null and \
                              b.up_data_baixa is null and \
                              b.dap_codi_dap = c.dap_codi_dap and \
                              c.amb_codi_amb = d.amb_codi_amb", 1, ("6211", CONN)),  # noqa
                "sa": ("select up_codi_up_scs, dap_desc_dap \
                        from gcctb008 a, gcctb007 b, gcctb006 c \
                        where a.up_codi_up_ics = b.up_codi_up_ics and \
                              a.up_data_baixa is null and \
                              b.up_data_baixa is null and \
                              b.dap_codi_dap = c.dap_codi_dap", 1, ("6211", CONN)),  # noqa
                "ce": ("select cent_codi_sector, cent_codi_centre, \
                               cent_classe_centre, cent_codi_up \
                        from pritb010", 3, ("redics", "data")),
                "r1": ("select residencia, descripcio_sisap from residencies \
                        where inclos = 1", 1, ("redics", "data")),
                "r2": ("select residencia, up from residencies \
                        where inclos = 1", 1, ("redics", "data"))}

        for domini, (sql, n, db) in sqls.items():
            dades = {row[:n]: row[n] for row in u.Database(*db).get_all(sql)}
            self.catalegs[domini] = dades
        file = os.path.dirname(os.path.abspath(__file__)) + "/234coronavirus.txt"  # noqa
        self.catalegs["ti"] = {up: tip.decode("utf8").encode("latin1")
                               for (up, tip)
                               in csv.reader(open(file), delimiter="@")}
        querys = {"pr": "select prov_dni_proveidor, espe_especialitat \
                         from pritb031, pritb023 \
                         where prov_categoria = espe_codi_especialitat",
                  "se": "select s_codi_servei, s_descripcio from pritb103",
                  "vi": "select tv_tipus||'||'||tv_cita, tv_des from vistb206"}
        for key, sql in querys.items():
            self.catalegs[key] = {}
            for sector in u.constants.SECTORS_ECAP[:-1]:
                with u.Database(sector, CONN) as db:
                    for cod, des in db.get_all(sql):
                        self.catalegs[key][(cod, sector)] = des
                if key == "vi":
                    self.catalegs[key][("9Ec||P", sector)] = "ECONSULTA"

    def get_ficticis(self):
        """."""
        self.ficticis = set()
        with u.Database("redics", "pdp") as pdp:
            sql = "select usua_cip from pdptb101 where usua_cip_cod = '{}'"
            for hash, in u.Database("redics", "data").get_all("select id from sisap_ficticis"):  # noqa
                cip = pdp.get_one(sql.format(hash))[0]
                self.ficticis.add(cip)

    def get_dades(self):
        """."""
        self.resultat = []
        self.agrupat = c.defaultdict(set)
        self.corba_estats = {}
        self.corba_dates = c.defaultdict(set)
        sql = "select usua_cip, usua_nom, usua_c1, usua_c2, usua_sexe, \
                      to_date(usua_data_naixement, 'J'), usua_uab_up, \
                      usua_uab_codi_uab, usua_codi_inf, pr_cod_ps, pr_th, \
                      pr_dde, pr_dba, pr_up, pr_usu, '{{}}' \
               from prstb015, usutb011, usutb040 \
               where pr_cod_u = cip_cip_anterior and \
                     cip_usuari_cip = usua_cip and \
                     pr_cod_o_ps = 'C' and \
                     pr_cod_ps in {} and \
                     pr_dde > date '2019-12-31' and \
                     pr_data_baixa is null".format(tuple(CODIS.keys()))
        jobs = [(sql.format(sector), sector)
                for sector in u.constants.SECTORS_ECAP[:-1]]
        pool = m.Pool(len(u.constants.SECTORS_ECAP))
        dades = pool.map(_worker, jobs, chunksize=1)
        pool.close()
        for sector in dades:
            for cip, nom, c1, c2, sex, naix, up, uba, ubainf, ps, th, dde, dba, dis, usu, sec in sector:  # noqa
                if not CODIS[ps][0] or th in CODIS[ps][0]:
                    if cip not in self.ficticis:
                        hash = h.sha1(cip).hexdigest().upper()
                        this = (sec, cip, hash, nom, c1, c2, sex, naix,
                                self.catalegs["am"].get((up,)),
                                up, self.catalegs["up"].get((up,)), uba, ubainf,  # noqa
                                dde, dba, self.catalegs["am"].get((dis,)),
                                dis, self.catalegs["up"].get((dis,)),
                                usu, ps, self.catalegs["ps"][(ps,)],
                                th, self.catalegs["th"].get((th,)),
                                CODIS[ps][1])
                        self.resultat.append(this)
                        self.agrupat[CODIS[ps][1]].add(cip)
                        if CODIS[ps][1] == "M":
                            self.corba_estats[cip] = "Possible"
                            self.corba_dates[cip].add((dde.date(), up, dis))

    def upload_dades(self):
        """."""
        cols = ("sector varchar2(4)", "pac_cip varchar2(13)",
                "pac_hash varchar2(40)", "pac_nom varchar2(255)",
                "pac_c1 varchar2(255)", "pac_c2 varchar2(255)",
                "pac_sexe varchar2(1)", "pac_naix date",
                "pac_ambit varchar2(255)", "pac_up_cod varchar2(5)",
                "pac_up_des varchar2(255)", "pac_uba varchar2(5)",
                "pac_inf varchar2(5)", "dx_data date", "dx_baixa date",
                "dx_ambit varchar2(255)", "dx_up_cod varchar2(5)",
                "dx_up_des varchar2(255)", "dx_usu varchar2(12)",
                "dx_ps_cod varchar2(15)", "dx_ps_des varchar2(255)",
                "dx_th_cod int", "dx_th_des varchar2(255)",
                "dx_grup varchar2(1)")
        with u.Database("redics", "data") as redics:
            redics.create_table(TABLE, cols, remove=True)
            redics.set_grants("select", TABLE, USERS)
            if self.resultat:
                redics.list_to_table(self.resultat, TABLE)
            redics.set_statistics(TABLE)

    def upload_agrupat(self):
        """."""
        cols = ("grup varchar2(255)", "recompte int")
        with u.Database("redics", "data") as redics:
            redics.create_table(AGRUPAT, cols, remove=True)
            redics.set_grants("select", AGRUPAT, USERS)
            if self.agrupat:
                upload = {}
                upload["Contacte"] = len(self.agrupat["C"] - self.agrupat["M"])
                upload["Malaltia"] = len(self.agrupat["M"] - self.agrupat["C"])
                upload["Contacte + Malaltia"] = len(self.agrupat["C"] & self.agrupat["M"])  # noqa
                redics.list_to_table(upload.items(), AGRUPAT)
            redics.set_statistics(AGRUPAT)

    def get_residencies(self):
        """."""
        self.residencies = {}
        sql = "select '{}', ug_usua_cip, gu_up_residencia \
               from ppftb012, ppftb011 \
               where ug_codi_grup = gu_codi_grup and \
                     gu_up_residencia is not null and \
                     gu_dba is null"
        jobs = [(sql.format(sector), sector)
                for sector in u.constants.SECTORS_ECAP[:-1]]
        pool = m.Pool(len(u.constants.SECTORS_ECAP))
        dades = pool.map(_worker, jobs, chunksize=1)
        pool.close()
        for chunk in dades:
            for sec, cip, resi in chunk:
                if (resi,) in self.catalegs["r1"]:
                    self.residencies[(sec, cip)] = resi

    def get_poblacio(self):
        """."""
        self.poblacio = c.defaultdict(set)
        self.ups = {}
        self.data_naix = {}
        self.denominadors = c.Counter()
        self.nia_resi = {}
        self.residencies_n = c.Counter()
        sql = "select '{}', usua_cip, usua_nia, usua_nom, usua_c1, usua_c2, \
                      usua_uab_up, usua_uab_codi_uab, usua_codi_inf, \
                      to_date(usua_data_naixement, 'J') \
               from usutb040"
        jobs = [(sql.format(sector), sector)
                for sector in u.constants.SECTORS_ECAP[:-1]]
        pool = m.Pool(len(u.constants.SECTORS_ECAP))
        dades = pool.map(_worker, jobs, chunksize=1)
        pool.close()
        for sector in dades:
            for sec, cip, nia, nom, c1, c2, up, uba, ubainf, naix in sector:
                if up and uba:
                    self.poblacio[cip].add((sec, nom, c1, c2, up, uba, ubainf))
                    self.ups[cip] = up
                    self.data_naix[cip] = naix
                    edat = u.years_between(naix, TODAY)
                    grup = 0 if edat < 15 else 2 if edat > 64 else 1
                    key = (self.catalegs["am"].get((up,)),
                           self.catalegs["sa"].get((up,)),
                           up, self.catalegs["up"].get((up,)), grup)
                    self.denominadors[key] += 1
                    if (sec, cip) in self.residencies:
                        resi = self.residencies[(sec, cip)]
                        if nia:
                            self.nia_resi[nia] = resi
                        self.residencies_n[resi] += 1

    def upload_poblacio(self):
        """."""
        cols = ("ambit varchar2(255)", "sap varchar2(255)", "up varchar2(5)",
                "eap varchar2(255)", "edat int", "poblacio int")
        with u.Database("redics", "data") as redics:
            redics.create_table(POBLACIO, cols, remove=True)
            redics.set_grants("select", POBLACIO, USERS)
            upload = [k + (v,) for (k, v) in self.denominadors.items()]
            redics.list_to_table(upload, POBLACIO)
            redics.set_statistics(POBLACIO)

    def upload_residencies(self):
        """."""
        upload = [(nia, resi, self.catalegs["r1"][(resi,)])
                  for (nia, resi) in self.nia_resi.items()]
        table = "sisap_residencies"
        cols = ("nia int", "up varchar(5)", "residencia varchar2(255)")
        with u.Database("exadata", "data") as exadata:
            exadata.create_table(table, cols, remove=True)
            exadata.list_to_table(upload, table)

    def get_pcr(self):
        """."""
        self.pcr = {}
        self.pcr_pos = set()
        literals = {"P": ("Positiu", "Confirmat"),
                    "N": ("Negatiu", "Descartat"),
                    "I": ("No valorable", "Possible")}
        sql = "select substr(cip, 0, 13), data_analitica, \
                      substr(resultat_pcr, 0, 1) \
               from dwaquas.alertes_pcr"
        for cip, data_, res in u.Database("exadata", "data").get_all(sql):
            data = data_.date()
            resultat, estat = literals[res]
            self.pcr[cip] = (estat, data, resultat)
            if res == "P":
                self.pcr_pos.add(cip)
                self.corba_estats[cip] = literals["P"][1]
                self.corba_dates[cip].add((data, self.ups.get(cip), None))
        upload = [(h.sha1(cip).hexdigest().upper(), data_, resultat_)
                  for cip, (estat_, data_, resultat_) in self.pcr.items()]
        cols = ("pacient varchar2(40)", "data date", "resultat varchar2(16)")
        with u.Database("redics", "data") as redics:
            redics.create_table(PCR, cols, remove=True)
            redics.set_grants("select", PCR, USERS)
            redics.list_to_table(upload, PCR)
            redics.set_statistics(PCR)

    def upload_corba(self):
        """."""
        dades = c.defaultdict(set)
        combinacions = c.defaultdict(set)
        for cip, estat in self.corba_estats.items():
            data, up, dis = min(self.corba_dates[cip])
            naix = self.data_naix.get(cip)
            edat_ = u.years_between(naix, data) if naix else None
            edat = 0 if edat_ < 15 else 2 if edat_ > 64 else 1
            dades[(estat, data, up, dis, edat)].add(cip)
            combinacions["key"].add((estat, up, dis, edat))
            combinacions["data"].add(data)
        for (estat, up, dis, edat) in combinacions["key"]:
            for data in combinacions["data"]:
                key = (estat, data, up, dis, edat)
                if key not in dades:
                    dades[key] = set()
        setmanals = c.Counter()
        for (estat, data, up, dis, edat), cips in dades.items():
            for i in range(7):
                dia = data - d.timedelta(days=i)
                casos = len(dades[(estat, dia, up, dis, edat)])
                setmanals[(estat, data, up, dis, edat)] += casos
        cols = ("pac_ambit varchar2(255)", "pac_sap varchar2(255)",
                "pac_up varchar2(255)", "dx_ambit varchar2(255)",
                "dx_sap varchar2(255)", "dx_up varchar2(255)", "data date",
                "estat varchar2(16)", "edat int", "recompte int",
                "setmanals int")
        with u.Database("redics", "data") as redics:
            redics.create_table(CORBA, cols, remove=True)
            redics.set_grants("select", CORBA, USERS + EXTERNS)
            upload = [(self.catalegs["am"].get((up,)),
                       self.catalegs["sa"].get((up,)),
                       self.catalegs["up"].get((up,)),
                       self.catalegs["am"].get((dis,)),
                       self.catalegs["sa"].get((dis,)),
                       self.catalegs["up"].get((dis,)),
                       data, estat, edat, len(cips),
                       setmanals[(estat, data, up, dis, edat)])
                      for (estat, data, up, dis, edat), cips in dades.items()]
            redics.list_to_table(upload, CORBA)
            indexes = [("idx_corba_1" + SUFFIX, "data, estat"),
                       ("idx_corba_2" + SUFFIX, "pac_ambit"),
                       ("idx_corba_3" + SUFFIX, "pac_sap"),
                       ("idx_corba_4" + SUFFIX, "pac_up")]
            for index, columns in indexes:
                sql = "create index {} on {} ({})".format(index, CORBA, columns)  # noqa
                redics.execute(sql)
            redics.set_statistics(CORBA)
        with u.Database("exadata", "data") as exadata:
            exadata.create_table(CORBA, cols, remove=True)
            exadata.list_to_table(upload, CORBA)

    def get_pneumonies(self):
        """."""
        codis = ('C01-J12.81', 'C01-J12.89', 'C01-J12.9')
        dat = d.date(*ACTIVITAT_FROM).strftime("%Y-%m-%d")
        sql = "select '{{}}', cip_usuari_cip, pr_dde \
               from prstb015, usutb011 \
               where pr_cod_u = cip_cip_anterior and \
                     pr_cod_o_ps = 'C' and \
                     pr_cod_ps in {} and \
                     pr_dde > date '{}' and \
                     pr_data_baixa is null".format(codis, dat)
        jobs = [(sql.format(sector), sector)
                for sector in u.constants.SECTORS_ECAP[:-1]]
        pool = m.Pool(len(u.constants.SECTORS_ECAP))
        dades = pool.map(_worker, jobs, chunksize=1)
        pool.close()
        pn = c.defaultdict(set)
        for sector in dades:
            for sec, cip, dde in sector:
                pn[(sec, cip)].add(dde)
        self.pneumonia = {key: max(dats) for key, dats in pn.items()}

    def get_llistats(self):
        """."""
        llistats = c.defaultdict(lambda: c.defaultdict(set))
        sql = "select sector, pac_cip, pac_nom, pac_c1, pac_c2, pac_up_cod, \
                      pac_uba, pac_inf, dx_grup, dx_data, dx_baixa, \
                      dx_ps_cod, dx_ps_des, dx_th_des \
               from {} \
               where pac_up_cod is not null".format(TABLE)
        with u.Database("redics", "data") as redics:
            for sec, cip, nom, c1, c2, up, uba, ubainf, grup, dde, dba, cod, ps, th in redics.get_all(sql):  # noqa
                this = (cod, dde, dba, th if th else ps, sec, nom, c1, c2, up, uba, ubainf)  # noqa
                llistats[grup][(sec, cip)].add(this)
        self.llistats = []
        for (sec, cip), dades in llistats["M"].items():
            cod, dde, dba, lit, _sec, nom, c1, c2, up, uba, ubainf = min(dades)
            sit = "Obert" if not dba else "Tancat"
            fi = dba.date() if dba else TODAY
            durada = (fi - dde.date()).days + 1
            resi = self.residencies.get((sec, cip))
            resi_d = self.catalegs["r1"].get((resi,))
            pneumonia = self.pneumonia.get((sec, cip))
            if cip in self.pcr:
                estat, pcr_dat, pcr_res = self.pcr[cip]
            else:
                estat, pcr_dat, pcr_res = "Possible", None, None
            that = (sec, cip, h.sha1(cip).hexdigest().upper(),
                    nom, c1, c2, up, uba, ubainf, resi, resi_d, estat,
                    sit, dde, dba, durada, cod + ": " + lit, pcr_dat, pcr_res,
                    min([dat for dat in (dde.date(), pcr_dat) if dat]), None,
                    pneumonia)
            self.llistats.append(that)
        for cip, (estat, pcr_dat, pcr_res) in self.pcr.items():
            if cip in self.pcr_pos:
                for sec, nom, c1, c2, up, uba, ubainf in self.poblacio[cip]:
                    if (sec, cip) not in llistats["M"]:
                        resi = self.residencies.get((sec, cip))
                        resi_d = self.catalegs["r1"].get((resi,))
                        pneumonia = self.pneumonia.get((sec, cip))
                        that = (sec, cip, h.sha1(cip).hexdigest().upper(),
                                nom, c1, c2, up, uba, ubainf, resi,
                                resi_d, estat, None, None, None, None, None,
                                pcr_dat, pcr_res, pcr_dat, None, pneumonia)
                        self.llistats.append(that)
        for (sec, cip), dades in llistats["C"].items():
            if (sec, cip) not in llistats["M"] and cip not in self.pcr_pos:
                cod, dde, dba, lit, _sec, nom, c1, c2, up, uba, ubainf = min(dades)  # noqa
                resi = self.residencies.get((sec, cip))
                resi_d = self.catalegs["r1"].get((resi,))
                if cip in self.pcr:
                    estat_, pcr_dat, pcr_res = self.pcr[cip]
                else:
                    pcr_dat, pcr_res = None, None
                that = (sec, cip, h.sha1(cip).hexdigest().upper(),
                        nom, c1, c2, up, uba, ubainf, resi, resi_d,
                        "Contacte", None, None, None, None, None, pcr_dat,
                        pcr_res, None, None, None)
                self.llistats.append(that)

    def upload_llistats(self):
        """."""
        cols = ("sector varchar2(4)", "cip varchar2(13)", "hash varchar2(40)",
                "nom varchar2(255)", "c1 varchar2(255)", "c2 varchar2(255)",
                "up varchar2(5)", "uba varchar2(5)", "inf varchar2(5)",
                "residencia_cod varchar2(5)", "residencia varchar2(255)",
                "estat varchar2(32)", "dx_situacio varchar2(10)",
                "dx_alta date", "dx_baixa date", "dx_durada int",
                "dx_literal varchar2(4000)", "pcr_data date",
                "pcr_resultat varchar2(32)", "cas_data date", "rx_data date",
                "pneumonia_data date")
        with u.Database("redics", "data") as redics:
            redics.create_table(LLISTATS, cols, remove=True)
            redics.set_grants("select", LLISTATS, USERS)
            redics.list_to_table(self.llistats, LLISTATS)
            redics.execute("create index llistat_1 on {} (cip, sector)".format(LLISTATS))  # noqa
            redics.execute("create index llistat_2 on {} (hash)".format(LLISTATS))  # noqa
            redics.set_statistics(LLISTATS)
            redics.create_table(DATA, ("data date",), remove=True)
            redics.set_grants("select", DATA, USERS)
            redics.list_to_table([(d.datetime.now(),)], DATA)
            redics.set_statistics(DATA)

    def get_radiologia(self):
        """."""
        sql = "select sector, cip, up, cas_data \
               from {} \
               where cas_data is not null".format(LLISTATS)
        pacients = {(sec, cip): (up, dat) for (sec, cip, up, dat)
                    in u.Database("redics", "data").get_all(sql)}
        sql = "select '{}', oc_usua_cip, oc_up_ori, oc_data \
               from gpitb104, gpitb004 \
               where oc_numid = inf_numid and \
                     oc_data >= date '{}' and \
                     inf_codi_prova in ('RA00001', 'RA00002', 'RA00590', 'RA00591')"  # noqa
        jobs = [(sql.format(sector, d.date(*ACTIVITAT_FROM).strftime("%Y-%m-%d")), sector)  # noqa
                for sector in u.constants.SECTORS_ECAP[:-1]]
        pool = m.Pool(len(u.constants.SECTORS_ECAP))
        dades = pool.map(_worker, jobs, chunksize=1)
        pool.close()
        upload = []
        update = c.defaultdict(set)
        for chunk in dades:
            for sec, cip, up_rx, dat in chunk:
                if (sec, cip) in pacients:
                    up_pac, dat_cas = pacients[(sec, cip)]
                    this = (sec, h.sha1(cip).hexdigest().upper(),
                            self.catalegs["am"].get((up_pac,)),
                            self.catalegs["sa"].get((up_pac,)),
                            self.catalegs["up"].get((up_pac,)),
                            self.catalegs["am"].get((up_rx,)),
                            self.catalegs["sa"].get((up_rx,)),
                            self.catalegs["up"].get((up_rx,)),
                            self.catalegs["ti"].get(up_rx),
                            dat_cas, dat)
                    upload.append(this)
                    update[(sec, cip)].add(dat)
        cols = ("sector varchar2(5)", "pacient varchar(40)",
                "pac_ambit varchar2(255)", "pac_sap varchar2(255)",
                "pac_up varchar2(255)", "oc_ambit varchar2(255)",
                "oc_sap varchar2(255)", "oc_up varchar2(255)",
                "oc_tip varchar2(255)", "cas_data date", "oc_data date")
        with u.Database("redics", "data") as redics:
            redics.create_table(RADIOLOGIA, cols, remove=True)
            redics.set_grants("select", RADIOLOGIA, USERS)
            redics.list_to_table(upload, RADIOLOGIA)
            redics.set_statistics(RADIOLOGIA)
            temp = "alskdcmkds"
            cols = ("sector varchar(4)", "cip varchar(13)", "dat date")
            redics.create_table(temp, cols, remove=True)  # noqa
            redics.list_to_table([(sec, cip, min(dats)) for (sec, cip), dats
                                  in update.items()], temp)
            sql = "create index idx_{0} on {0} (cip, sector)".format(temp)
            redics.execute(sql)
            sql = "update {} set rx_data = null".format(LLISTATS)
            redics.execute(sql)
            sql = "update {} a \
                   set rx_data = (select dat from {} b \
                                  where a.cip = b.cip and \
                                        a.sector = b.sector)".format(LLISTATS, temp)  # noqa
            redics.execute(sql)
            redics.drop_table(temp)

    def upload_residencies_casos(self):
        """."""
        casos = c.Counter()
        for row in self.llistats:
            casos[(row[9], row[11])] += 1
        upload = []
        for resi, n in self.residencies_n.items():
            eap = self.catalegs["r2"][(resi,)]
            this = (self.catalegs["am"].get((eap,)),
                    self.catalegs["sa"].get((eap,)),
                    self.catalegs["up"].get((eap,)),
                    resi, self.catalegs["r1"][(resi,)], n,
                    casos[(resi, "Confirmat")], casos[(resi, "Possible")])
            upload.append(this)
        cols = ("ambit varchar2(255)", "sap varchar2(255)",
                "eap varchar2(255)", "resi_cod varchar2(5)",
                "resi_des varchar2(255)", "pacients int", "confirmats int",
                "possibles int")
        with u.Database("redics", "data") as redics:
            redics.create_table(RESIDENCIES, cols, remove=True)
            redics.set_grants("select", RESIDENCIES, USERS + EXTERNS)
            redics.list_to_table(upload, RESIDENCIES)
            redics.set_statistics(RESIDENCIES)

    def get_visites(self):
        """."""
        self.visites = c.Counter()
        sql = "select '{}', visi_centre_codi_centre, \
                      visi_centre_classe_centre, visi_tipus_visita, \
                      visi_etiqueta, to_date(visi_data_visita, 'J'), \
                      visi_situacio_visita \
               from vistb043 \
               where visi_centre_codi_centre like '%' and \
                     visi_centre_classe_centre like '%' and \
                     visi_servei_codi_servei = 'MG' and \
                     visi_modul_codi_modul = 'COROV'"
        jobs = [(sql.format(sector), sector)
                for sector in u.constants.SECTORS_ECAP[:-1]]
        pool = m.Pool(len(u.constants.SECTORS_ECAP))
        dades = pool.map(_worker, jobs, chunksize=1)
        pool.close()
        for sector in dades:
            for sec, cod, cla, tip, eti, dat, sit in sector:
                if sit == "R":
                    centre = self.catalegs["co"].get((sec, cod, cla))
                    up_c = self.catalegs["ce"].get((sec, cod, cla))
                    up_d = self.catalegs["up"].get((up_c,))
                    amb = self.catalegs["am"].get((up_c,))
                    eti_d = self.catalegs["et"].get((eti,))
                    self.visites[(amb, up_c, up_d, cod, cla, centre,
                                  tip, eti, eti_d, dat)] += 1

    def upload_visites(self):
        """."""
        cols = ("ambit varchar2(255)", "up_cod varchar2(5)",
                "up_des varchar2(255)", "centre_cod varchar2(9)",
                "centre_cla varchar2(2)", "centre_des varchar2(255)",
                "tipus varchar2(4)", "etiqueta_cod varchar2(4)",
                "etiqueta_des varchar2(255)", "data date", "recompte int")
        with u.Database("redics", "data") as redics:
            redics.create_table(VISITES, cols, remove=True)
            redics.set_grants("select", VISITES, USERS)
            if self.visites:
                upload = [k + (v,) for (k, v) in self.visites.items()]
                redics.list_to_table(upload, VISITES)
            redics.set_statistics(VISITES)

    def get_activitat(self):
        """."""
        ini = u.gcal2jd(d.date(*ACTIVITAT_FROM))
        fi = u.gcal2jd(d.datetime.now())
        sql = "select visi_usuari_cip, to_date(visi_data_visita, 'J'), \
                      to_char(to_date(visi_hora_visita, 'sssss'), 'HH24:mi'), \
                      visi_up, visi_centre_codi_centre, \
                      visi_centre_classe_centre, visi_servei_codi_servei, \
                      visi_modul_codi_modul, \
                      decode(visi_etiqueta, 'ECTA', '9Ec', visi_tipus_visita),\
                      visi_tipus_citacio, visi_forcada_s_n, visi_etiqueta, \
                      modu_act_agenda, modu_subact_agenda, visi_dni_prov_resp,\
                      '{{}}', visi_situacio_visita \
               from vistb043, vistb027 \
               where visi_centre_codi_centre = modu_centre_codi_centre and \
                     visi_centre_classe_centre = modu_centre_classe_centre and\
                     visi_servei_codi_servei = modu_servei_codi_servei and \
                     visi_modul_codi_modul = modu_codi_modul and \
                     visi_data_visita between {} and {}".format(ini, fi)
        jobs = [(sql.format(sector), sector)
                for sector in u.constants.SECTORS_ECAP[:-1]]
        pool = m.Pool(len(u.constants.SECTORS_ECAP))
        dades = pool.map(_worker, jobs, chunksize=1)
        pool.close()
        cols = ("pacient varchar2(40)", "data date", "hora varchar2(5)",
                "up varchar2(5)", "centre_codi varchar2(9)",
                "centre_classe varchar2(2)", "servei varchar2(5)",
                "modul varchar2(5)", "tipus varchar2(4)",
                "citacio varchar2(1)", "forcada varchar2(1)",
                "etiqueta varchar2(4)", "atribut1 varchar2(5)",
                "atribut2 varchar2(5)", "professional varchar2(9)",
                "sector varchar(4)")
        with u.Database("redics", "data") as redics:
            redics.create_table(ACTIVITAT, cols, remove=True)
            redics.set_grants("select", ACTIVITAT, USERS + EXTERNS)
            for sector in dades:
                upload = [(h.sha1(row[0]).hexdigest().upper(),) + row[1:-1]
                          for row in sector if row[-1] == "R"]
                redics.list_to_table(upload, ACTIVITAT)
            redics.execute("create index activitat_1 on {} (pacient)".format(ACTIVITAT))  # noqa
            redics.set_statistics(ACTIVITAT)
        with u.Database("exadata", "data") as exadata:
            exadata.create_table(ACTIVITAT, cols, remove=True)
            exadata.list_to_table(upload, ACTIVITAT)

    def agr_activitat(self):
        """."""
        sql = "select sector, data, up, servei, tipus, citacio, \
                      count(1), sum(decode(modul, '4CW', 1, 0)), \
                      sum(decode(modul, 'COROV', 1, 0)) \
               from {} \
               group by sector, data, up, servei, tipus, citacio".format(ACTIVITAT)  # noqa
        cols = ("data varchar2(10)", "ambit varchar2(255)",
                "sap varchar2(255)", "up_cod varchar2(5)",
                "up_des varchar2(255)", "up_tip varchar2(255)",
                "servei varchar2(255)", "visita_tip varchar2(4)",
                "visita_cit varchar2(1)", "visita_des varchar2(255)",
                "recompte int", "recompte_4cw int", "recompte_corov int")
        upload = []
        no_p = []
        cw = []
        with u.Database("redics", "data") as redics:
            for sec, data, up, serv, tipus, cit, n, ncw, ncv in redics.get_all(sql):  # noqa
                amb = self.catalegs["am"].get((up,), "")
                sap = self.catalegs["sa"].get((up,), "")
                up_des = self.catalegs["up"].get((up,), "")
                up_tip = self.catalegs["ti"].get(up, "")
                servei_des = self.catalegs["se"].get((serv, sec), "")
                vis_des = self.catalegs["vi"].get(((tipus if tipus else "") + "||" + cit, sec), "")  # noqa            
                this = (data.strftime("%Y-%m-%d"), amb, sap, up, up_des,
                        up_tip, servei_des, tipus, cit, vis_des, n, ncw, ncv)
                upload.append(this)
                if tipus in ("9T", "9E", "9Ec"):
                    no_p.append(this)
                if ncw:
                    cw.append(list(this)[:-3] + [ncw])
            redics.create_table(ACTIVITAT_AGR, cols, remove=True)
            redics.set_grants("select", ACTIVITAT_AGR, USERS + EXTERNS)
            redics.list_to_table(upload, ACTIVITAT_AGR)
            redics.set_statistics(ACTIVITAT_AGR)
            columns = redics.get_table_columns(ACTIVITAT_AGR)
        with u.Database("exadata", "data") as exadata:
            exadata.create_table(ACTIVITAT_AGR, cols, remove=True)
            exadata.list_to_table(upload, ACTIVITAT_AGR)
        file = "covid19_no_presencial_{}.csv".format(TODAY.strftime("%Y%m%d"))
        dades = [columns] + sorted(no_p)
        _send_mail(["osolansf@gencat.cat"], [(file, dades)])
        file = "covid19_4cw_{}.csv".format(TODAY.strftime("%Y%m%d"))
        dades = [columns] + sorted(cw)
        _send_mail(["mgarcia.canela@gencat.cat"], [(file, dades)])

    def get_professionals(self):
        """."""
        sql = "select distinct data, up, professional, sector \
               from {}".format(ACTIVITAT)
        cols = ("data varchar2(10)", "ambit varchar2(255)",
                "sap varchar2(255)", "up_cod varchar2(5)",
                "up_des varchar2(255)", "up_tip varchar2(255)",
                "categoria varchar2(255)", "professionals int")
        professionals = c.defaultdict(set)
        with u.Database("redics", "data") as redics:
            for data, up, dni, sector in redics.get_all(sql):
                if dni:
                    amb = self.catalegs["am"].get((up,), "")
                    sap = self.catalegs["sa"].get((up,), "")
                    up_des = self.catalegs["up"].get((up,), "")
                    up_tip = self.catalegs["ti"].get(up, "")
                    categ = self.catalegs["pr"].get((dni, sector), "")
                    key = (data.strftime("%Y-%m-%d"), amb, sap, up, up_des,
                           up_tip, categ)
                    professionals[key].add((dni))
            upload = [k + (len(v),) for (k, v) in professionals.items()]
            redics.create_table(PROFESSIONALS, cols, remove=True)
            redics.set_grants("select", PROFESSIONALS, USERS + EXTERNS)
            redics.list_to_table(upload, PROFESSIONALS)
            redics.set_statistics(PROFESSIONALS)
            columns = redics.get_table_columns(PROFESSIONALS)
        with u.Database("exadata", "data") as exadata:
            exadata.create_table(PROFESSIONALS, cols, remove=True)
            exadata.list_to_table(upload, PROFESSIONALS)
        file = "covid19_professionals_{}.csv".format(TODAY.strftime("%Y%m%d"))
        dades = [columns] + sorted(upload)
        _send_mail(["jmbonets@gencat.cat"], [(file, dades)])

    def get_xml(self, form, table):
        """."""
        sql = "select '{}', xml_id, xml_cip, xml_data_alta, xml_up, \
                      p.xml_xml_1.getClobVal() \
               from prstb690 p \
               where xml_origen = 'GABINETS' and \
                     xml_tipus_orig = '{}'"
        jobs = [(sql.format(sector, form), sector)
                for sector in u.constants.SECTORS_ECAP[:-1]]
        pool = m.Pool(len(u.constants.SECTORS_ECAP))
        dades = pool.map(_worker, jobs, chunksize=1)
        pool.close()
        upload = []
        i = 0
        banned = ("Pacient", "Professional", "Centre")
        columns = ["pacient varchar(255)", "ambit varchar2(255)",
                   "sap varchar2(255)", "up_cod varchar2(5)",
                   "up_des varchar2(255)",  "data date",
                   "sector varchar2(4)", "id number(22)"]
        for chunk in dades:
            for sec, id, cip, dat, up, xml in chunk:
                hash = h.sha1(cip).hexdigest().upper()
                amb = self.catalegs["am"].get((up,))
                sap = self.catalegs["sa"].get((up,))
                up_des = self.catalegs["up"].get((up,))
                this = [hash, amb, sap, up, up_des, dat, sec, id]
                regs = xml.split("<Camp Id")
                if len(regs) > 10:
                    for reg in regs[1:]:
                        camps = reg.split("\"")
                        cod, val = camps[1], camps[5][:2000]
                        if not any([word in cod for word in banned]):
                            this.append(val)
                            if i == 0:
                                columns.append("xml_{} varchar2(2000)".format(cod[:26]))  # noqa
                    upload.append(this)
                    i += 1
        with u.Database("redics", "data") as redics:
            redics.create_table(table, columns, remove=True)
            redics.set_grants("select", table, USERS + EXTERNS)
            redics.list_to_table(upload, table)
            redics.set_statistics(table)
        with u.Database("exadata", "data") as exadata:
            exadata.create_table(table, columns, remove=True)
            exadata.list_to_table(upload, table)

    def get_app(self):
        """."""
        table = "covid19_appdatahistory"
        col_names = ("document_id", "document_id_type", "id", "created",
                     "modified", "classification", "contacted", "verified",
                     "sem_classification", "auditstatus")
        cols = c.defaultdict(list)
        with u.Database("exadata", "data") as exadata:
            for col in col_names:
                this = exadata.get_column_information(col, table, desti="ora")
                for key, val in this.items():
                    cols[key].append(val)
            cols["create"].extend(("ambit varchar2(255)", "sap varchar2(255)",
                                   "eap varchar2(255)", "ap_cas varchar2(16)",
                                   "ap_dx date"))
            with u.Database("redics", "data") as redics:
                sql = "select cip, estat, dx_alta from {}".format(LLISTATS)
                estats = {cip: (estat, dx) for (cip, estat, dx)
                          in redics.get_all(sql)}
                redics.create_table(APP, cols["create"], remove=True)
                redics.set_grants("select", APP, USERS)
                sql = "select {} from dwaquas.{}".format(", ".join(cols["query"]), table)  # noqa
                upload = set()
                for row in exadata.get_all(sql):
                    cip = row[0][:13]
                    hash = h.sha1(cip).hexdigest().upper()
                    up = self.ups.get(cip)
                    amb = self.catalegs["am"].get((up,))
                    sap = self.catalegs["sa"].get((up,))
                    eap = self.catalegs["up"].get((up,))
                    cas, dx = estats.get(cip, (None, None))
                    upload.add((hash,) + row[1:] + (amb, sap, eap, cas, dx))
                redics.list_to_table(list(upload), APP)
                redics.set_statistics(APP)


def _worker(params):
    """."""
    sql, sector = params
    dades = list(u.Database(sector, CONN).get_all(sql))
    return dades


def _send_mail(to, attachments):
    """."""
    mail = u.Mail()
    mail.to.extend(to)
    mail.cc.extend(("mmedinap@gencat.cat", "ffinaaviles@gencat.cat"))
    mail.subject = "Informe COVID-19"
    mail.text = "Adjuntem informe diari."
    mail.attachments.extend(attachments)
    # mail.send()


if __name__ == "__main__":
    try:
        Coronavirus()
    except Exception as e:
        subject = "COVID bad!!!"
        text = str(e)
    else:
        subject = "COVID good"
        text = ""
    finally:
        mail = u.Mail()
        mail.to.append("ffinaaviles@gencat.cat")
        mail.subject = subject
        mail.text = text
        mail.send()
