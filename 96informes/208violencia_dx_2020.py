# coding: latin1

"""
2020-10 (Edu):
- Reutilizaci�n script 208violencia_2018.py de Carmen
- Se modifica la definici�n de dx
- Se automatiza el a�o y el formato de salida del archivo csv
"""

import sisapUtils as u
import datetime as d

TODAY = d.datetime.now().date()
YEAR = 2020

class Violencia(object):
    """."""

    def __init__(self):
        """."""
        self.get_thesaurus()
        self.get_edat()
        self.get_up()
        self.get_poblacio_up()
        self.taula_final()
        self.export_taula()
 
    def get_thesaurus(self):
        """."""
        self.ps = {}
        self.thesaurus = {}
        self.data_prob = {}
        sql = "select id_cip_sec, pr_cod_ps, pr_th, pr_dde from import.problemes \
               where pr_cod_ps in ('C01-T74.11XA','C01-T74.91XA') \
               and year(pr_dde)={}".format(YEAR)
        for id, cod_ps, cod_thesaurus, data in u.getAll(sql, "import"):
            self.ps[id] = cod_ps
            self.thesaurus[id] = cod_thesaurus
            self.data_prob[id] = data
 
    def get_edat(self):
        """."""
        self.edat = {}
        sql = "select id_cip_sec, edat from assignada_tot"
        for id, edat in u.getAll(sql, "nodrizas"):
            if id in self.thesaurus:
                self.edat[id] = u.ageConverter(edat)
        
    def get_up(self):
        """."""
        self.up = {}
        sql = "select id_cip_sec, up from assignada_tot"
        for id, up in u.getAll(sql, "nodrizas"):
            if id in self.edat:
                self.up[id] = up
        
    def get_poblacio_up(self):
        """."""
        self.poblacio_up = {}
        self.recompte_pob_up = {}
        sql = "select id_cip_sec, up, edat from assignada_tot"
        for id, up, edat in u.getAll(sql, "nodrizas"):
            self.poblacio_up[id] = (u.ageConverter(edat), up)
            (edat, up) = self.poblacio_up[id]
            if (edat, up) not in self.recompte_pob_up:
                self.recompte_pob_up[(edat, up)] = 1
            else:
                self.recompte_pob_up[(edat, up)] += 1
    
    def taula_final(self):
        """."""
        self.taula_final = {}
        for id in self.edat:
            self.taula_final[id] = (self.ps[id], self.thesaurus[id], self.edat[id], \
                                    self.up[id])
        self.recompte = {}
        for id in self.edat:
            (ps, th, edat, up) = self.taula_final[id]
            if (ps, th, edat, up) not in self.recompte:
                self.recompte[(ps, th, edat, up)] = 1 
            else:
                self.recompte[(ps, th, edat, up)] += 1
        
        self.recompte_up = {}
        for (ps, th, edat, up) in self.recompte:
            self.recompte_up[(ps, th, edat, up)] = (self.recompte[(ps, th, edat, up)], \
                                                    self.recompte_pob_up[(edat,up)])
                                     
        self.taula_final = {}
        for (ps, th, edat, up) in self.recompte_up:
            self.taula_final[(ps, th, edat, up)] = (ps, th, edat, up, \
                                    self.recompte_up[(ps, th, edat, up)][0],
                                    self.recompte_up[(ps, th, edat, up)][1])

    def export_taula(self):
        """."""
        upload = self.taula_final.values()
        u.writeCSV(u.tempFolder + "violenciagenere_dx_{}_{}.csv".format(YEAR,TODAY.strftime('%Y%m%d')),
                   upload, sep=";")

if __name__ == "__main__":
    Violencia()
