# -*- coding: utf-8 -*-

"""
---
"""

from sisapUtils import *
import pandas as pd
from collections import defaultdict


### PACIENTS ###
def pacients():
    # obtenir ups d'interes
    # sql = """SELECT up_cod FROM dwsisap.DBC_CENTRES 
    #             WHERE abs_des LIKE '%AMPOSTA%'
    #             OR abs_des LIKE '%BARCELONA 1-B%'
    #             OR abs_des LIKE '%BARCELONA 7-G%'
    #             OR abs_des LIKE '%BARCELONA 8-G%'
    #             OR abs_des LIKE '%BARCELONA 9-A%'
    #             OR abs_des LIKE '%BARCELONA 9-E%'
    #             OR abs_des LIKE 'CALAF'
    #             OR abs_des LIKE 'CAPELLADES'
    #             OR abs_des LIKE '%CARDONA%'
    #             OR abs_des LIKE '%BARCELONA 1-B%'
    #             OR abs_des LIKE '%CERDANYOLA-RIPOLLET%'
    #             OR abs_des LIKE '%CUBELLES%'
    #             OR abs_des LIKE '%DELTEBRE%'
    #             OR abs_des LIKE '%EN BAS%'
    #             OR abs_des LIKE '%LLEIDA 3%'
    #             OR abs_des LIKE '%CUBELLES%'
    #             OR abs_des LIKE '%MOLLET DEL VALLÈS-EST%'
    #             OR abs_des LIKE 'PIERA'
    #             OR abs_des LIKE 'PREMIÀ DE MAR'
    #             OR abs_des LIKE 'RODA DE TER'
    #             OR abs_des LIKE 'SABADELL 1A'
    #             OR abs_des LIKE 'SABADELL 3B'
    #             OR abs_des LIKE 'SANT ANDREU DE LA BARCA'
    #             OR abs_des LIKE 'SANT VICENÇ DELS HORTS-1%'
    #             OR abs_des LIKE 'SANTA COLOMA DE QUERALT%'
    #             OR abs_des LIKE 'SANTA COLOMA DE GRAMENET 4'
    #             OR abs_des LIKE 'SANTA PERPÈTUA DE LA MOGODA%'
    #             OR abs_des LIKE 'TARRAGONA 1'
    #             OR abs_des LIKE 'VILAFANT'
    #             OR abs_des LIKE 'VILANOVA I LA GELTRÚ 1'"""
    # up = list()
    # for u, in getAll(sql, 'exadata'):
    #     up.append(u)

    # up = ('00010', '00171', '00174', '00179',
    #      '00185', '00186', '00369', '00388', '00440',
    #      '00449', '00453', '00491', '00498', '00196',
    #      '00205', '00286', '00341', '00358', '00049',
    #      '00050', '00089', '00090', '00133', '01122',
    #      '03449', '04713', '05945', '01883')

    # # obtenir nacionalitats
    # print('nacionalitats')
    # sql = """SELECT codi, nom FROM DWSISAP.DIM_NACIONALITAT"""
    # nacionalitats = dict()
    # for codi, pais in getAll(sql, 'exadata'):
    #     nacionalitats[str(pais)] = codi


    # obtenir defuncions
    print('poblacio')
    sql = """SELECT cip, DATA_NAIXEMENT, sexe, DATA_DEFUNCIO, NACIONALITAT, UP_RESIDENCIA 
            FROM DWSISAP.RCA_CIP_NIA 
            WHERE (DATA_DEFUNCIO IS NULL OR 
            extract(YEAR FROM DATA_DEFUNCIO) = 2012) AND  
            extract(YEAR FROM DATA_naixement) <= 2021
            AND situacio != 'T' 
            AND cip NOT IN (SELECT cip FROM DWSISAP.RCA_CIP_NIA 
            WHERE DATA_DEFUNCIO IS NULL AND situacio = 'D')"""

    # obtenir poblacio
    # print('poblacio')
    # sql = """SELECT b.CIP, a.C_DATA_NAIX, a.C_SEXE, b.DATA_DEFUNCIO, a.c_nacionalitat, a.c_up, a.c_metge, a.c_infermera 
    #         FROM dwsisap.dbs a, DWSISAP.RCA_CIP_NIA b
    #         WHERE a.C_CIP = b.hash"""

    persones = []

    for cip, naix, sexe, defuncio, nacionalitat, up in getAll(sql, 'exadata'):
        # transformacio sexe
        if sexe == '0': sex = 1
        elif sexe == '1': sex = 2
        else: sex = 3 
        # transformacio nacionalitat
        # try:
        #     nac = nacionalitats[str(nacionalitat)]
        # except:
        #     nac = 999
        persones.append((str(cip), naix, sex, defuncio, nacionalitat, str(up)))

    print(len(persones))

    print('creating database')
    cols = "(cip varchar2(14), fecha_nacimiento date, sexo varchar2(1), \
            fecha_defuncion date, pais_nacimiento varchar2(4), up varchar2(5))"

    createTable('bdcap_pacientes', cols, 'exadata', rm=True)
    listToTable(persones, 'bdcap_pacientes', "exadata")
    grantSelect('bdcap_pacientes','DWSISAP_ROL','exadata')


### HASH A CIP ###
def get_cips():
    print('cips')
    cips = dict()
    sql = """SELECT usua_cip, usua_cip_cod 
            FROM pdp.pdptb101"""
    for cip, hash in getAll(sql, "pdp"):
        cips[hash] = cip
    return cips

### CREAR CATÀLEG ###
def definicions(x, proc_dic, param_dic):
    p = proc_dic.keys()
    p = [str(e)for e in p]
    v = param_dic.keys()
    v = [str(e) for e in v]
    if str(x) in p:
        return proc_dic[x] 
    elif str(x) in v:
        return param_dic[x]
    else: 
        return ''

# def cataleg():
#     equiv = pd.read_csv('equiv.csv')
#     equiv = equiv[['TIPUS','CODI_ECAP', 'CONCEPTID', 'ORIGEN_TIPUS_DADES']]
#     param = pd.read_csv('param.csv')
#     params = param['Código '].to_list() 
#     params = [str(p) for p in params]
#     param_dic = param.set_index("Código ")["Nombre parámetro BDCAP"].to_dict()
#     param_dic_2 = {str(key): str(value) for key, value in param_dic.items()}
#     proc = pd.read_csv('proc.csv')
#     procs = proc['Código'].to_list()
#     procs = [str(p) for p in procs]
#     proc_dic = proc.set_index("Código")["Término abreviado"].to_dict()
#     equiv['Procedimiento'] = equiv['CONCEPTID'].apply(lambda x: 1 if x in procs else 0)
#     equiv['Variables'] = equiv['CONCEPTID'].apply(lambda x: 1 if x in params else 0)
#     print(len(proc_dic.keys()))
#     print(len(param_dic_2.keys()))
#     # inter = pd.read_csv('interconsulta.csv')
#     # inter_dic = proc.set_index("Código")["Descripción"].to_dict()
#     equiv['Termino_abrev'] = equiv['CONCEPTID'].apply(lambda x: definicions(x, proc_dic, param_dic_2))
#     upload = []
#     for i in range(len(equiv)):
#         u = [str(equiv['TIPUS'][i]),
#             str(equiv['CODI_ECAP'][i]),
#             str(equiv['CONCEPTID'][i]),
#             str(equiv['ORIGEN_TIPUS_DADES'][i]),
#             str(equiv['Termino_abrev'][i]),
#             equiv['Procedimiento'][i],
#             equiv['Variables'][i]]
#         if u[5] == 1 and u[0] not in ('PRO', 'VARIPRO'): u[5] = 0
#         if u[6] == 1 and u[0] not in ('VAR', 'VARIPRO'): u[6] = 0
#         u = list(u)
#         # if u[7] ==1 and u[0] not in ('INT','INTS'): u[7] = 0
#         upload.append(u)
#     cols = "(tipus varchar(6), codi_ecap varchar2(14), conceptid varchar2(200), origen_dades varchar2(15), \
#         des_minis varchar(400), procedimiento int, variables int)"

#     createTable('BDCAP_CATALEG_2021', cols, 'exadata', rm=True)
#     listToTable(upload, 'BDCAP_CATALEG_2021', "exadata")
#     grantSelect('BDCAP_CATALEG_2021','DWSISAP_ROL','exadata')

def do_it(inf):
    p, cips, origen_codi = inf[0], inf[1], inf[2]
    print('multiprocesss iujuuuu')
    procediments = set()
    i = 0
    codis = list(origen_codi.keys())
    sql = """select cr_cip, cr_codi_prova_ics, CR_DATA_REG
                        from sidics.labtb101
                        partition({})""".format(p)
    for id, prova, data in getAll(sql, ("sidics", "x0002")):
        try:
            if prova in codis:
                procediments.add((cips[id], origen_codi[prova], data, 'FICTICIO', 'LABTB101'))
            i += 1
        except:
            pass
        # if i % 10000 == 0:
        #     listToTable(list(procediments), 'bdcap_procedimientos', "exadata")
        #     procediments = set()
    print(len(procediments))
    listToTable(list(procediments), 'bdcap_procedimientos_2021', "exadata")
    print('escrit i acabat')
    return procediments

def get_vacunes(sector):
    origen_codi = defaultdict(dict)
    sql = """SELECT codi_ecap, CONCEPTID
                FROM dwsisap.BDCAP_CATALEG_2021
                WHERE procedimiento = 1
                AND origen_dades= 'PRSTB051'"""
    for ecap, ministerio in getAll(sql, 'exadata'):
        origen_codi[ecap] = ministerio
    codis = list(origen_codi.keys())
    print('vamos a workear')
    upload = []
    # sql = """select VA_U_USUA_CIP, va_u_cod_an, VA_U_DATA_MODI
    #         from PRSTB051
    #         WHERE extract(YEAR FROM VA_U_DATA_MODI) = 2021"""
    # for id, codi, data in getAll(sql, sector):
    #     if codi in codis:
    #         upload.append((id, origen_codi[codi], data, 'FICTICIO', 'PRSTB051'))
    ## aquest pegote és perquè els següents codis han estat definits com a va_u_cod no com a antígens
    sql = """select VA_U_USUA_CIP, va_U_COD, VA_U_DATA_MODI
            from PRSTB051
            WHERE extract(YEAR FROM VA_U_DATA_MODI) = 2021"""
    codis = ('DTP','P00250','P00245','FEBGRO','P-G-NE','P00243',
			'GRIP','P-G-60','COLERA','MA','P00246','TIFOID','DT','TDP','P00249','P00247','EJ',
			'P00248','Td','RABIA','P00244','TUBER','HA','ENCEF','P-G-AR','P-G-AD','XRP',
			'P00257','ECE','ROTAV')
    for id, codi, data in getAll(sql, sector):
        if codi in codis:
            upload.append((id, origen_codi[codi], data, 'FICTICIO', 'PRSTB051'))
    listToTable(upload, 'bdcap_procedimientos_2021', "exadata")
    print('un menysss:))))')

### CREAR PROCEDIMIENTO ###
def procediment(cips):
    print('procediments')
    cols = "(id varchar2(13), prova varchar2(200), data date, \
            episodio varchar2(100), taula varchar2(15))"
    # createTable('bdcap_procedimientos_2021', cols, 'exadata', rm=True)
    grantSelect('bdcap_procedimientos_2021','DWSISAP_ROL','exadata')

    origen_codi = defaultdict(dict)
    sql = """SELECT codi_ecap, CONCEPTID, origen_dades 
                FROM dwsisap.BDCAP_CATALEG_2021
                WHERE procedimiento = 1
                OR TIPUS = 'PROVAR'"""
    for ecap, ministerio, origen in getAll(sql, 'exadata'):
        origen_codi[origen][ecap] = ministerio
    
    ## execute("DELETE dwsisap.bdcap_procedimientos WHERE taula = 'LABTB101'", "exadata")
    
    
    for taula, dict_codis in origen_codi.items():
        print(taula)
        print(dict_codis.keys())

        codis = list(dict_codis.keys())
        # if taula == 'LABTB101':
        #     particions = [
        #                     't20210101', 't20210116',
        #                     't20210201', 't20210216',
        #                     't20210301', 't20210316',
        #                     't20210401', 't20210416',
        #                     't20210501', 't20210516',
        #                     't20210601', 't20210616',
        #                     't20210701', 't20210716',
        #                     't20210801', 't20210816',
        #                     't20210901', 't20210916',
        #                     't20211001', 't20211016',
        #                     't20211101', 't20211116',
        #                     't20211201', 't20211216',
        #                 ]
        #     print('here')
        #     proc_results = multiprocess(do_it, [(p, cips, dict_codis) for p in particions], 4, close=True)
        #     print(len(proc_results))   
        
        # if taula == 'GPITB004':
        #     convert = dict()
        #     sql = """SELECT pro_codi_prova, PRO_CODI_REALITZABLE FROM gcctb003 WHERE PRO_CODI_REALITZABLE IS not NULL"""
        #     for vell, nou in getAll(sql, 'redics'):
        #         convert[vell] = nou
            
        #     codis1 = set([convert[c] if c in convert.keys() else 0 for c in codis])

        #     procediments = []
        #     coses = set()
        #     sql = """SELECT a.oc_data, a.OC_USUA_CIP, b.INF_CODI_PROVA, b.inf_cod_ps
        #                 FROM gpitb104 a, gpitb004 b 
        #                 WHERE b.INF_NUMID=a.OC_NUMID
        #                 AND extract(YEAR FROM a.oc_data) = 2021""" #and rownum <= 500
        #     print(sql)
        #     print(codis1)
        #     # codis vells i nous van al revés!!!!
        #     for data, id, prova, dx in getAll(sql, 'redics'):
        #         coses.add(prova)
        #         try: 
        #             if prova in codis1:
        #                 # print('prova:', prova, 'codi_nou', convert[prova])
        #                 procediments.append((cips[id], dict_codis[prova], data, dx, 'gpitb004'))
        #         except: pass
        #     print(len(procediments))
        #     print(coses)
        #     listToTable(procediments, 'bdcap_procedimientos_2021', "exadata")
        
        if taula == 'PRSTB051':
            multiprocess(get_vacunes, sectors, 12, close=True)
        
        # if taula == 'PRSTB016':
        #     procediments = []
        #     coses = set()
        #     sql = """select AU_COD_U, AU_COD_AC, AU_DAT_ACT
        #             from PRSTB016
        #             WHERE extract(YEAR FROM AU_DAT_ACT) = 2021"""
        #     print(sql)
        #     print(codis)
        #     for id, prova, data in getAll(sql, 'redics'):
        #         coses.add(prova)
        #         if prova in codis:
        #             try:
        #                 procediments.append((cips[id], dict_codis[prova], data, 'FICTICIO', 'PRSTB016'))
        #             except:
        #                 pass
        #     print(len(procediments))
        #     # print(coses)
        #     listToTable(procediments, 'bdcap_procedimientos_2021', "exadata")

        # if taula == 'PRSTB017':
        #     procediments = []
        #     coses = set()
        #     sql = """select VU_COD_U, VU_COD_VS, VU_V_DAT
        #             from PRSTB017
        #             WHERE extract(YEAR FROM VU_V_DAT) = 2021"""
        #     print(sql)
        #     print(codis)
        #     for id, prova, data in getAll(sql, 'redics'):
        #         coses.add(prova)
        #         if prova in codis:
        #             try:
        #                 procediments.append((cips[id], dict_codis[prova], data, 'FICTICIO', 'PRSTB017'))
        #             except:
        #                 pass
        #     print(len(procediments))
        #     print(coses)
        #     listToTable(procediments, 'bdcap_procedimientos_2021', "exadata")

    print(origen_codi.keys())


def do_it2(inf):
    p, cips, dict_codis = inf[0], inf[1], inf[2]
    codis = list(dict_codis.keys())
    no_unitats = set()
    sql = """SELECT codi_ecap FROM dwsisap.bdcap_cataleg_2021 
            WHERE origen_dades = 'LABTB101'
            AND  unitats_bdcap IS null AND variables = 1"""
    for codi_ecap in getAll(sql, 'exadata'):
        no_unitats.add(codi_ecap)
    si_unitats = set()
    dic_unitats = dict()
    sql = """SELECT codi_ecap, UNITATS_BDCAP FROM dwsisap.bdcap_cataleg_2021 
            WHERE origen_dades = 'LABTB101'
            AND  unitats_bdcap IS not null AND variables = 1"""
    for codi_ecap, unitats in getAll(sql, 'exadata'):
        si_unitats.add(codi_ecap)
        dic_unitats[codi_ecap] = unitats
    print('multiprocesss iujuuuu')
    procediments = []
    i = 0
    sql = """select cr_cip, cr_codi_prova_ics, cr_res_lab, CR_DATA_REG, lower(replace(CR_UNITATS_LAB,' ', ''))
                        from sidics.labtb101
                        partition({})""".format(p)
    for id, prova, val, data, unitats in getAll(sql, ("sidics", "x0002")):
        try:
            if prova in no_unitats:
                if len(val) > 2000: val = val[0:2000]
                procediments.append((cips[id], dict_codis[prova], val, data, 'FICTICIO', 'LABTB101'))
                i += 1
            if prova in si_unitats:
                if dic_unitats[prova] == unitats:
                    if len(val) > 2000: val = val[0:2000]
                    procediments.append((cips[id], dict_codis[prova], val, data, 'FICTICIO', 'LABTB101'))
                    i += 1
            # cas espacial perque accepten dues unitats per aquesta prova
            if prova == 'Q48372' and unitats in ('pg/ml', 'ng/l'):
                if len(val) > 2000: val = val[0:2000]
                procediments.append((cips[id], dict_codis[prova], val, data, 'FICTICIO', 'LABTB101'))
                i += 1
        except:
            pass
        if i % 10000 == 0:
            listToTable(procediments, 'bdcap_variables_2021', "exadata")
            procediments = []
    print(len(procediments))
    listToTable(procediments, 'bdcap_variables_2021', "exadata")
    print('escrit i acabat')
    return procediments

### CREAR VARIABLES ###
def variables(cips):
    print('variables')
    cols = "(id varchar2(40), prova varchar2(200), valor varchar2(2000), data date, \
            episodio varchar2(100), taula varchar2(15))"
    # createTable('bdcap_variables_2021', cols, 'exadata', rm=True)
    grantSelect('bdcap_variables_2021','DWSISAP_ROL','exadata')

    origen_codi = defaultdict(dict)
    sql = """SELECT codi_ecap, CONCEPTID, origen_dades 
                FROM dwsisap.BDCAP_CATALEG_2021
                WHERE variables = 1"""
    for ecap, ministerio, origen in getAll(sql, 'exadata'):
        origen_codi[origen][ecap] = ministerio
    
    
    for taula, dict_codis in origen_codi.items():
        print(taula)
        print(dict_codis.keys())
        
        codis = list(dict_codis.keys())
        if taula == 'LABTB101':
            particions = [
                            't20210101', 't20210116',
                            't20210201', 't20210216',
                            't20210301', 't20210316',
                            't20210401', 't20210416',
                            't20210501', 't20210516',
                            't20210601', 't20210616',
                            't20210701', 't20210716',
                            't20210801', 't20210816',
                            't20210901', 't20210916',
                            't20211001', 't20211016',
                            't20211101', 't20211116',
                            't20211201', 't20211216',
                        ]
            print('here')
            proc_results = multiprocess(do_it2, [(p, cips, dict_codis) for p in particions], 4, close=True)
            print(len(proc_results))
        
    #     if taula == 'PRSTB016':
    #         procediments = []
    #         coses = set()
    #         sql = """select AU_COD_U, AU_COD_AC, AU_VAL, AU_DAT_ACT
    #                 from PRSTB016
    #                 WHERE extract(YEAR FROM AU_DAT_ACT) = 2021"""
    #         print(sql)
    #         print(codis)
    #         for id, prova, val, data in getAll(sql, 'redics'):
    #             coses.add(prova)
    #             if prova in codis:
    #                 try:
    #                     procediments.append((cips[id], dict_codis[prova], val, data, 'FICTICIO', 'PRSTB016'))
    #                 except:
    #                     pass
    #         print(len(procediments))
    #         # print(coses)
    #         listToTable(procediments, 'bdcap_variables_2021', "exadata")

    #     if taula == 'PRSTB017':
    #         procediments = []
    #         coses = set()
    #         sql = """select VU_COD_U, VU_COD_VS, VU_VAL, VU_V_DAT
    #                 from PRSTB017
    #                 WHERE extract(YEAR FROM VU_V_DAT) = 2021"""
    #         print(sql)
    #         print(codis)
    #         for id, prova, val, data in getAll(sql, 'redics'):
    #             coses.add(prova)
    #             if prova in codis:
    #                 try:
    #                     procediments.append((cips[id], dict_codis[prova], val, data, 'FICTICIO', 'PRSTB017'))
    #                 except:
    #                     pass
    #         print(len(procediments))
    #         # print(coses)
    #         listToTable(procediments, 'bdcap_variables_2021', "exadata")
    
    # print(origen_codi.keys())


#### CREAR EPISODIO ####
def episodio(cips):
    print('episodio')
    cols = "(id varchar2(40), problema_salud varchar2(15), clasificacion int, fecha_apertura date, \
            fecha_cierre date)"
    createTable('bdcap_episodio_2021', cols, 'exadata', rm=True)
    grantSelect('bdcap_episodio_2021','DWSISAP_ROL','exadata')
    i = 0
    upload = []
    sql = """SELECT pr_cod_u, pr_cod_ps, pr_dde, pr_dba 
            FROM prstb015 
            WHERE (extract(YEAR FROM pr_dde) <= 2021 AND pr_dba IS NULL) OR 
            extract(YEAR FROM pr_dba) = 2021"""
    # n'hi ha 21,855,159
    for id, ps, apertura, cierre in getAll(sql, 'redics'):
        try:
            if ps[0:4] == 'C01-':
                ps = ps[4:]
            upload.append((cips[id], ps, 4, apertura, cierre))
            i += 1
        except: pass
        if i % 10000 == 0:
            listToTable(upload, 'bdcap_episodio_2021', "exadata")
            print(i)
            upload = []
    listToTable(upload, 'bdcap_episodio_2021', "exadata")


def get_visites(sector):
    print('vamos a workear')
    categoria_prof = {}
    sql = """SELECT ide_dni, ide_categ_prof_c FROM DWSISAP.LOGINS_ECAP"""
    for dni, ctegoria in u.getAll(sql, 'exadata'):
        categoria_prof[dni] = cagtegoria
    id_visites = {}
    sql = """SELECT VISI_ID, visi_tipus_visita FROM vistb043 WHERE VISI_SITUACIO_VISITA = 'R'
            AND extract(YEAR FROM to_date(visi_data_visita, 'j')) =2022"""
    for id, tipus_visita in getAll(sql, sector):
        id_visites[id] = tipus_visita
    upload = []
    sql = """SELECT MC_CIP, MC_DATA, MC_MOTIU, MC_PORTA_ID, MC_DNI_PROF  FROM vistb042 
            WHERE extract(YEAR FROM mc_data) = 2022
            AND mc_porta = 'VISITA' AND MC_SUB_CAT = 'CIM10MC'"""
    for id, data, codi, visi, dni_prof in getAll(sql, sector):
        if visi in id_visites:
            if codi[0:4] == 'C01-':
                codi = codi[4:]
            upload.append((id, codi, 4, data))
    listToTable(upload, 'bdcap_visitas_2022', "exadata")
    cim10_mc = dict()
    sql = """SELECT ps_cod_o_ap, cim10mc 
                FROM import.cat_prstb001cmig cpc """
    for old, new in getAll(sql, "import"):
        cim10_mc[old] = new
    upload = []
    sql = """SELECT MC_CIP, MC_DATA, MC_MOTIU, MC_PORTA_ID, MC_DNI_PROF FROM vistb042 
            WHERE extract(YEAR FROM mc_data) = 2022
            AND mc_porta = 'VISITA' AND MC_SUB_CAT = 'CIM10'"""
    for id, data, codi, visi, dni_prof in getAll(sql, sector):
        if codi in cim10_mc: codi = cim10_mc[codi]
        if visi in id_visites:
            if codi[0:4] == 'C01-':
                codi = codi[4:]
            upload.append((id, codi, 4, data))
    listToTable(upload, 'bdcap_visitas_2022', "exadata")
    print('un menysss:))))')


#### CREAR VISITAS ####
def visitas():
    cols = "(id varchar2(40), problema_salud varchar2(40), clasificacion int, fecha_visita date)"
    createTable('bdcap_visitas_2022', cols, 'exadata', rm=True)
    grantSelect('bdcap_visitas_2022','DWSISAP_ROL','exadata')
    multiprocess(get_visites, sectors, 12, close=True)


def interconsultes(cips):
    print('taula')
    cols = "(id varchar2(14), caracter varchar2(3), fecha date)"
    createTable('bdcap_interconsultas_2022', cols, 'exadata', rm=True)
    grantSelect('bdcap_interconsultas_2022','DWSISAP_ROL','exadata')
    print('int')
    ecap_ministerio = dict()
    sql = """SELECT codi_ecap, conceptid
            FROM dwsisap.BDCAP_CATALEG_2022
            WHERE tipus = 'INT'"""
    for ecap, mini in getAll(sql, 'exadata'):
        ecap_ministerio[ecap] = mini
    print('pritb023')
    sire = set()
    sql = """SELECT ESPE_CODI_SIRE FROM import.cat_pritb023"""
    for codi, in getAll(sql, 'import'):
        sire.add(codi)
    print('info')
    upload = []
    sql = """SELECT a.oc_data, a.OC_USUA_CIP, b.inf_espec_sire
            FROM gpitb104 a, gpitb004 b 
            WHERE b.INF_NUMID=a.OC_NUMID
            AND extract(YEAR FROM a.oc_data) = 2022
            AND inf_espec_sire IS NOT NULL"""
    for data, usuari, espe in getAll(sql, 'redics'):
        if espe in sire and espe in ecap_ministerio.keys():
            try:
                upload.append((cips[usuari], ecap_ministerio[espe], data))
            except:
                pass
    print('afegir dades 1')
    listToTable(upload, 'bdcap_interconsultas_2022', "exadata")

    print('ints')
    ecap_ministerio = dict()
    sql = """SELECT codi_ecap, conceptid
            FROM dwsisap.BDCAP_CATALEG_2022
            WHERE tipus = 'INTS'"""
    for ecap, mini in getAll(sql, 'exadata'):
        ecap_ministerio[ecap] = mini
    print('pritb000')
    sire = set()
    sql = """SELECT rv_low_value
                FROM import.cat_pritb000 
                WHERE rv_TABLE ='GPITB004' 
                AND RV_COLUMN ='INF_SERVEI_D_CODI'"""
    for codi, in getAll(sql, 'import'):
        sire.add(codi)
    
    print('info')
    upload = []
    sql = """SELECT a.oc_data, a.OC_USUA_CIP, b.inf_servei_d_codi
            FROM gpitb104 a, gpitb004 b 
            WHERE b.INF_NUMID=a.OC_NUMID
            AND extract(YEAR FROM a.oc_data) = 2022
            AND inf_servei_d_codi IS NOT NULL"""
    for data, usuari, espe in getAll(sql, 'redics'):
        if espe in sire and espe in ecap_ministerio.keys():
            try:
                upload.append((cips[usuari], ecap_ministerio[espe], data))
            except:
                pass
    print('afegir dades 2')
    listToTable(upload, 'bdcap_interconsultas_2022', "exadata")
    
    


if __name__ == "__main__":
    # pacients() # FET
    # cataleg() # FET
    cips = get_cips()
    # cips = []
    # procediment(cips) # FET
    # variables(cips) 
    # episodio(cips) # FET
    visitas() # FET
    # interconsultes(cips) # FET
    # dic = {
    #     'Q55885':  'pend2',
    #     'Q46585':  '270992008',
    #     'S18685':  '41451006',
    #     'L69185':  '104381000',
    #     'A01185':  '165848009',
    #     'A01585':  '165848009',
    #     'A01685':  '165848009',
    #     'A02385':  '165848009',
    #     'A02485':  '165848009',
    #     'A02585':  '165848009',
    #     'A02685':  '165848009',
    #     'A02785':  '165848009',
    #     'A02985':  '165848009',
    #     'A03385':  '165848009',
    #     'A03485':  '165848009',
    #     'A03585':  '165848009',
    #     'A03885':  '165848009',
    #     'A03985':  '165848009',
    #     'A04085':  '165848009',
    #     'A04185':  '165848009',
    #     'A04285':  '165848009',
    #     'A04385':  '165848009',
    #     'A04685':  '165848009',
    #     'A04785':  '165848009',
    #     'A04885':  '165848009',
    #     'A05385':  '165848009',
    #     'A05685':  '165848009',
    #     'A05785':  '165848009',
    #     'A06585':  '165848009',
    #     'A06685':  '165848009',
    #     'A06785':  '165848009',
    #     'A06885':  '165848009',
    #     'A07185':  '165848009',
    #     'A07285':  '165848009',
    #     'A07485':  '165848009',
    #     'A07585':  '165848009',
    #     'A07785':  '165848009',
    #     'A08285':  '165848009',
    #     'A08385':  '165848009',
    #     'A08785':  '165848009',
    #     'A09185':  '165848009',
    #     'A09385':  '165848009',
    #     'A09485':  '165848009',
    #     'A09585':  '165848009',
    #     'A09685':  '165848009',
    #     'A09785':  '165848009',
    #     'A09885':  '165848009',
    #     'A09985':  '165848009',
    #     'A10185':  '165848009',
    #     'A10285':  '165848009',
    #     'A10385':  '165848009',
    #     'A10485':  '165848009',
    #     'A10985':  '165848009',
    #     'A11085':  '165848009',
    #     'A11285':  '165848009',
    #     'A11385':  '165848009',
    #     'A11585':  '165848009',
    #     'A11785':  '165848009',
    #     'A11885':  '165848009',
    #     'A11985':  '165848009',
    #     'A12085':  '165848009',
    #     'A12285':  '165848009',
    #     'A12385':  '165848009',
    #     'A12485':  '165848009',
    #     'A12785':  '165848009',
    #     'A12885':  '165848009',
    #     'A12985':  '165848009',
    #     'A13085':  '165848009',
    #     'A13285':  '165848009',
    #     'A13385':  '165848009',
    #     'A13485':  '165848009',
    #     'A13585':  '165848009',
    #     'A13785':  '165848009',
    #     'A13985':  '165848009',
    #     'A14085':  '165848009',
    #     'A14185':  '165848009',
    #     'A14285':  '165848009',
    #     'A14485':  '165848009',
    #     'A14585':  '165848009',
    #     'A14785':  '165848009',
    #     'A14885':  '165848009',
    #     'A14985':  '165848009',
    #     'A15085':  '165848009',
    #     '10102': '306111000',
    #     '20207': '306934005',
    #     '10105': '183523005',
    #     '21211': '306200004',
    #     '20201': '306191004',
    #     '20204': '384711009',
    #     '30215': '183547003',
    #     '20205': '306198005',
    #     '10104': '306118006',
    #     '10149': '183522000',
    #     '10107': '306148009',
    #     '70121': '307374004',
    #     '10101': 'pend5',
    #     '10109': '306125004',
    #     '10110': '306112007',
    #     '20208': '183546007',
    #     '10108': '183521007',
    #     '40101': '306133003',
    #     '10106': '306186000',
    #     '20209': '183543004',
    #     '10112': '306140002',
    #     '20210': '183544005',
    #     '30301': '306128002',
    #     '30302': '306128002',
    #     '30303': '306128002',
    #     '30304': '306128002',
    #     '30305': '306128002',
    #     '30306': '306128002',
    #     '30308': '306128002',
    #     '30310': '306128002',
    #     '30312': '306128002',
    #     '30313': '306128002',
    #     '30314': '306128002',
    #     '30315': '306128002',
    #     '30316': '306128002',
    #     '30317': '306128002',
    #     '30318': '306128002',
    #     '30319': '306128002',
    #     '30320': '306128002',
    #     '30321': '306128002',
    #     '50115': '183524004',
    #     '10155': '308461008',
    #     '10111': '306127007',
    #     '20212': '306201000',
    #     'S14885': '104291008',
    #     'S14985':  '104291008',
    #     'S15185':  '104291008',
    #     'PF002': '54550000',
    #     'PF003': '42803009',
    #     'PC007': '76746007',
    #     'PX020': '108243003',
    #     'PL004': '42987007',
    #     'PX027': '168172001',
    #     'ALSET': '228309001',
    #     'Q32036':  '40402000',
    #     'TBAR': '395070003',    
    #     'AP002': '417036008',
    #     'VMEAN': '273402009',
    #     'VMEDE': '273402009',
    #     'PV008': '170599006',
    #     'PFLOW': '18491006',
    #     'TF3000':  '410455004',
    #     'HA': '33879002',
    #     'MA': '33879002',
    #     'TIFOID':  '33879002',
    #     'TUBER': '33879002',
    #     'PIOD':  '164729009',
    #     'PIOE': '164729009',
    #     'Td':  '73152006',
    #     'TDP':  '399014008', 
    #     'GRIP':  '33879002',
    #     'HA+B':  '16584000',
    #     'B06019':  '61594008',
    #     'B06022':  '61594008',
    #     'B06723':  '61594008',
    #     'B08258':  '117010004',
    #     'B01958':  '117010004'}
    # conn= connect('exadata')
    # c= conn.cursor()
    # for e in dic:
    #     SQL = "update dwsisap.bdcap_cataleg_2021 SET CONCEPTID = '{}' WHERE CODI_ECAP = '{}'".format(dic[e], e)
    #     c.execute(SQL)
    # conn.commit() 
    # c.close
    
    # vacunes = {
    #     'P. Arb. COVID': 'P00243',
    #     'COVID1': 'P00244',
    #     'COVID8':  'P00257',
    #     'COVID3':  'P00246',
    #     'COVID6': 'P00249',
    #     'COVID7':  'P00250',
    #     'COVID4':  'P00247',
    #     'COVID5':  'P00248',
    #     'COVID2':  'P00245',
    # }
    # conn= connect('exadata')
    # c= conn.cursor()
    # for e in vacunes:
    #     SQL = "update dwsisap.bdcap_cataleg_2021 SET CODI_ECAP = '{}' WHERE CODI_ECAP = '{}'".format(vacunes[e], e)
    #     c.execute(SQL)
    # sql = """update dwsisap.bdcap_cataleg_2021 set procedimiento = 1 where tipus = 'PRO'"""
    # c.execute(sql)
    # grip = ('P-G-AR','P-G-NE','P-G-AD','P-G-60')
    # for e in grip:
    #     SQL = """insert into dwsisap.bdcap_cataleg_2021 SELECT tipus, '{}', conceptid, origen_dades, 'Gripe', 
    #                     procedimiento, variables, unitats_bdcap
    #                     FROM dwsisap.BDCAP_CATALEG_2021
    #                     WHERE tipus = 'PRO'
    #                     AND CODI_ECAP = 'GRIP'""".format(e)
    #     c.execute(SQL)
    # gripa = ['P00145']
    # for e in gripa:
    #     SQL = """insert into dwsisap.bdcap_cataleg_2021 SELECT tipus, '{}', conceptid, origen_dades, des_minis, 
    #                     procedimiento, variables, unitats_bdcap
    #                     FROM dwsisap.BDCAP_CATALEG_2021
    #                     WHERE tipus = 'PRO'
    #                     AND CODI_ECAP = 'A-GRIP'""".format(e)
    #     c.execute(SQL)
    # conn.commit() 
    # c.close

