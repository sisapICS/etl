# coding: iso-8859-1

from sisapUtils import tempFolder, getAll, readCSV, execute, listToTable, printTime
import sys, datetime
from time import strftime
from collections import defaultdict, Counter

"""
Procés per afegir les residències que s'hagin validat per Trello.
Cal afegir els codis al fitxer residencies_a_incloure de tempFolder separats per @
"""

tb1 = "residencies"
db = "redics"

nod = "nodrizas"


file = tempFolder + 'residencies_a_incloure.txt'

class Afegim(object):
    """."""

    def __init__(self):
        """."""
        self.get_resis_sisap()
        self.get_centres()
        self.get_places()
        self.get_UP()
        self.export_data()
        self.upload_data()

    def get_resis_sisap(self):
        """Mirem que no tinguem ja les residències al catàleg"""
        
        self.res_ecap = {}
        
        sql = 'select residencia, inclos from residencies'
        for re, inclos in getAll(sql, db):
            self.res_ecap[re] = inclos
    
    def get_centres(self):
        """Busquem descripcions UP"""
       
        self.centres = {}
       
        sql = "select scs_codi,ics_codi,ics_desc,amb_codi,amb_desc,sap_codi,sap_desc from cat_centres"
        for up,br,desc,amb,ambdesc,sap,sap_desc in getAll(sql, nod):
            self.centres[up] = {'br':br,'desc':desc,'amb':amb,'ambdesc':ambdesc,'sap':sap,'sapdesc':sap_desc}
        
    def get_places(self):
        """Mirem quanta gent tenim institucionalitzada a la residència"""
        
        self.pobres = Counter()
        
        sql = "select up,up_residencia from nodrizas.assignada_tot where institucionalitzat = 1"
        for up, resi in getAll(sql, nod):
            self.pobres[(up,resi)] += 1

    def get_UP(self):
        """Obtenim la UP principal segons població"""
        
        self.catResis = {}
        
        for (up,resi),count in self.pobres.items():
            if resi in self.catResis:
                if self.catResis[(resi)]['x'] < count:
                    self.catResis[(resi)]['up'] = up
                    self.catResis[(resi)]['x'] = count
                    self.catResis[(resi)]['tot'] += count
                else:
                    self.catResis[(resi)]['tot'] += count
            else:
                self.catResis[(resi)] = {'x':count,'up':up,'tot':count}

    def export_data(self):
        """Busquem totes les variables per omplir el catàleg del sisap"""
        
        self.upload = []

        for (residencia, desc, inclos) in readCSV(file, sep='@'):
            if residencia in self.res_ecap:
                # continue
                execute("delete from residencies where residencia = '{}'".format(residencia),'redics')
            up, descup, ambit, ambdesc, sap, sapdesc, br, recompte = None, None, None, None, None, None, None, None
            if residencia in self.catResis:
                up = self.catResis[residencia]['up']
                recompte = self.catResis[residencia]['tot']
            if up in self.centres:
                descup = self.centres[up]['desc']
                ambit = self.centres[up]['amb']
                ambdesc = self.centres[up]['ambdesc']
                sap = self.centres[up]['sap']
                sapdesc = self.centres[up]['sapdesc']
                br = self.centres[up]['br']
            reskhalix = 'R' + residencia
            self.upload.append([None,residencia,desc,desc,None,recompte,inclos,desc,up,descup,sap,sapdesc,ambit,ambdesc])
        print self.upload
    
    def upload_data(self):
        """Carreguem les dades al catàleg de residències"""
        
        listToTable(self.upload, tb1, db)

if __name__ == "__main__":
    printTime('Inici')
    
    Afegim()
    
    printTime('Fi')