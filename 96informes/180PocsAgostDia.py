from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import datetime


inici = '20180801'
fi = '20180831'

sb = datetime.now().hour < 7


class pocsAgost(object):
    """."""
    
    def __init__(self):
        """."""
        self.dades = Counter()
        self.get_centres()
        self.get_pocs()
        self.export_data()
        
                                 
    def get_centres(self):
        """."""
        self.territoris = {} 
        sql = "select up.up_codi_up_scs ,br.up_desc_up_ics, amb.amb_desc_amb, sap.dap_desc_dap   from    cat_gcctb007 br \
                inner join cat_gcctb008 up on br.up_codi_up_ics=up.up_codi_up_ics \
                inner join cat_gcctb006 sap on br.dap_codi_dap=sap.dap_codi_dap \
                inner join cat_gcctb005 amb on sap.amb_codi_amb=amb.amb_codi_amb \
                where e_p_cod_ep='0208'"
        for up, desc, territori, sap in getAll(sql, "import"):
            self.territoris[up] = {'ambit':territori, 'sap': sap, 'desc': desc }
                              
    def get_pocs(self):
        """."""
        rec = 0
        self.pocs={}
        for sector in sectors:
            if sb:
                sector += "sb"
            printTime(sector)  
            pob = {}
            sql = "select usua_cip, '2018' - to_char(to_date(usua_data_naixement,'J'), 'YYYY') from usutb040"
            for id, edat in getAll(sql, sector):
                pob[(id)] = edat
            sql = "select au_up, au_cod_u, au_dat_act from prstb016 \
            where au_cod_ac='POCS' and to_char(au_dat_act, 'YYYYMMDD') between '{}' and '{}'".format(inici, fi)
            for up, id, dat in getAll(sql, sector):
                if up in self.territoris:
                    ambit = self.territoris[up]['ambit']
                    edat1 = pob[(id)] if id in pob else None
                    if edat1:
                        edat= "adl" if edat1 > 14 else "ped"
                        self.dades[(dat, ambit, edat)] += 1
                        rec += 1
        print rec
            
    def export_data(self):
        """."""
        printTime('envia')
        upload = [] 
        for (dat, ambit, edat),d in self.dades.items():
            upload.append([dat, ambit, edat, d])
        header=[("Data","ambit","Edat","Recompte")]
        file = tempFolder + 'Pocs_agost.csv'
        writeCSV(file, header + upload)
        sendGeneral('SISAP <sisap@gencat.cat>', 'premsa.ics@gencat.cat', ['ecomaredon@gencat.cat', 'bpons@idiapjgol.info', 'lmendezboo@gencat.cat','cfuentes@idiapjgol.info'], 'Dades POCS agost', 'Dades dels POCS fins ahir',file)
        remove(file)
        
if __name__ == '__main__':
    pocsAgost()