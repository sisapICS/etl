import sisapUtils as u
import collections as c
import xlwt
from dateutil.relativedelta import relativedelta


CODIS = {
'491126':['MEPILEX BORDER FLEX 10X10CM 3U', 'FOAM'],
'491134':['MEPILEX BORDER FLEX 15X15CM 3U', 'FOAM'],
'488494':['ALLEVYN HEEL 2X(10X10CM) 3U', 'FOAM'],
'495747':['MEPILEX BORDER SACRUM 22X25 CM 3 U', 'FOAM'],
'486910':['MEPILEX XT 15X15 CM 3U', 'FOAM'],
'492959':['BIATAIN SILICONE 12,5X12,5CM 3U', 'FOAM'],
'473025':['ALLEVYN LIFE 15,4X15,4CM 3U', 'FOAM'],
'489468':['ALLEVYN SACRUM 17X17CM 3U', 'FOAM'],
'400118':['MEPILEX BORDER FLEX LITE 7,5X8,5 CM 3 U', 'FOAM'],
'477059':['ALLEVYN GENTLE BORDER LITE OVAL 8,6X7,7CM 3U', 'FOAM'],
'488114':['BIATAIN SILICONE SACRO 3U', 'FOAM'],
'492934':['BIATAIN SILICONE 15X15CM 3U', 'FOAM'],
'467142':['BIATAIN SILICONE LITE 10X10CM 3U', 'FOAM'],
'493619':['AQUACEL FOAM SACRO 3U', 'FOAM'],
'499087':['ASKINA HEEL 225CM2 3U', 'FOAM'],
'400120':['MEPILEX BORDER FLEX LITE 15X15 CM 3 U', 'FOAM'],
'486977':['URGOTUL FOAM BORDER SACRUM 20X20CM 3U', 'FOAM'],
'496695':['MEPILEX BORDER HEEL 3 U', 'FOAM'],
'471037':['CONVAMAX SUPERABSORBENTE NO ADH 15X15 CM 3 U', 'FOAM'],
'483313':['ALLEVYN ADHESIVE 12,5X12,5CM 3U', 'FOAM'],
'467159':['BIATAIN SILICONE MULTISHAPE 3U', 'FOAM'],
'496687':['MEPILEX LITE 10X10CM 3U', 'FOAM'],
'493064':['AQUACEL FOAM 12,5X12,5 CM 3U', 'FOAM'],
'488973':['BIATAIN ADHESIVE SACRO 17X17CM 3U', 'FOAM'],
'476887':['TEGADERM 1626W 10X12CM 10 U', 'FOAM'],
'482125':['AQUACEL AG FOAM ADH 12,5X12,5CM 3U', 'FOAM'],
'491845':['ALLEVYN LIFE 21X21CM 3U', 'FOAM'],
'486902':['MEPILEX XT 10X10 CM 3U', 'FOAM'],
'487728':['FOAM LITE CONVATEC 10X10CM 3U', 'FOAM'],
'494039':['MEPILEX XT 10X20CM 3U', 'FOAM'],
'493601':['AQUACEL FOAM TALON 3U', 'FOAM'],
'499715':['URGOTUL ABSORB BORDER 13X13CM 3U', 'FOAM'],
'492942':['BIATAIN SILICONE 17,5X17,5CM 3U', 'FOAM'],
'469767':['AQUACEL FOAM PRO SACRO 24X21,5 CM 3U', 'FOAM'],
'495739':['MEPILEX AG 10X10 CM 3 U', 'FOAM'],
'482133':['AQUACEL FOAM ADH 17,5X17,5 CM 3U', 'FOAM'],
'496661':['AQUACEL FOAM PRO 15X15 CM 3U', 'FOAM'],
'486282':['AQUACEL FOAM PRO TALON 19,8X14 CM 3U', 'FOAM'],
'491837':['ALLEVYN ADHESIVE 15X15CM 3U', 'FOAM'],
'400105':['MEPILEX BORDER FLEX OVAL 15X19 CM 3U', 'FOAM'],
'499681':['URGOTUL ABSORB 15X15CM 3U', 'FOAM'],
'499707':['URGOTUL ABSORB BORDER 15X15CM 3U', 'FOAM'],
'477067':['ALLEVYN GENTLE BORDER LITE 10X20CM 3U', 'FOAM'],
'400130':['BIATAIN SOFT-HOLD 15X15CM 3U', 'FOAM'],
'489187':['BIATAIN ADHESIVE TALON 19X20CM 3U', 'FOAM'],
'486480':['BIATAIN NON ADHESIVE 15X15 CM 3U', 'FOAM'],
'486852':['BIATAIN ADHESIVE 12,5X12,5 CM 3U', 'FOAM'],
'493072':['AQUACEL FOAM N/A 15X15CM 3U', 'FOAM'],
'487736':['AQUACEL AG FOAM N/A 15X15 CM 3U', 'FOAM'],
'495754':['MEPILEX LITE 15X15 CM 3 U', 'FOAM'],
'496679':['MEPILEX LITE 6X8,5CM 3U', 'FOAM'],
'400123':['ZETUVIT PLUS SILICONE 12,5X12,5 CM 3 U', 'FOAM'],
'482141':['AQUACEL AG FOAM SACRO 3U', 'FOAM'],
'483305':['ALLEVYN 15X15CM 3U', 'FOAM'],
'486969':['URGOTUL ABSORB TALON 12X19 CM 3U', 'FOAM'],
'400128':['BIATAIN SOFT-HOLD 10X10CM 3U', 'FOAM'],
'491852':['ALLEVYN GENTLE 15X15CM 3U', 'FOAM'],
'488940':['BIATAIN SILICONE NON-BORDER 15X15 CM 3U', 'FOAM'],
'496737':['BIATAIN AG NON-ADHESIVE 15X15CM 3U', 'FOAM'],
'496406':['HYDROTAC COMFORT 15X15CM 3U', 'FOAM'],
'486472':['BIATAIN NON ADHESIVE 10X10 CM 3U', 'FOAM'],
'499699':['URGOTUL ABSORB 10X12CM 3U', 'FOAM'],
'486860':['BIATAIN ADHESIVE 15X15 CM 3U', 'FOAM'],
'458117':['FOAM LITE CONVATEC 15X15 CM 3 U', 'FOAM'],
'478578':['ALLEVYN GENTLE 10X10CM 3U', 'FOAM'],
'400129':['BIATAIN SOFT-HOLD 10X20CM 3U', 'FOAM'],
'483297':['ALLEVYN 10X10CM 3U', 'FOAM'],
'400124':['HYDROTAC SACRAL 18X18CM 3U', 'FOAM'],
'400121':['BIATAIN AG ADHESIVE SACRO 3U', 'FOAM'],
'496372':['HYDROTAC 15X15CM 3U', 'FOAM'],
'496364':['HYDROTAC 10X10CM 3U', 'FOAM'],
'488932':['BIATAIN SILICONE NON-BORDER 12,5X12,5 CM 3U', 'FOAM'],
'488957':['BIATAIN SILICONE AG 12,5X12,5 CM 3 U', 'FOAM'],
'496729':['BIATAIN AG NON-ADHESIVE 10X10CM 3U', 'FOAM'],
'489245':['BIATAIN NON ADHESIVE 10X20 CM 3U', 'FOAM'],
'496380':['HYDROTAC 10X20CM 3U', 'FOAM'],
'497842':['BIATAIN SILICONE AG 15X15CM 3U', 'FOAM'],
'488965':['BIATAIN ADHESIVE LIGHT 12,5X12,5CM 3U', 'FOAM'],
'496703':['BIATAIN ADHESIVE CONTOUR 3U', 'FOAM'],
'400122':['BIATAIN AG ADHESIVE TALON 3U', 'FOAM'],
'400116':['SKINFOAM NO ADHESIVO 10X10CM 3U', 'FOAM'],
'486167':['ASKINA TRANSORBENT 15X15 CM 3U', 'FOAM'],
'400117':['SKINFOAM NO ADHESIVO 15X15CM 3U', 'FOAM'],
'495671':['ASKINA DRESSIL BORDER LITE 15X15 CM 3 U', 'FOAM'],
'491860':['ALLEVYN THIN 15X15CM 3U', 'FOAM'],
'495648':['ASKINA DRESSIL BORDER 15X15CM 3U', 'FOAM'],
'486159':['ASKINA DRESSIL 10X10 CM 3U', 'FOAM'],
'487181':['ASKINA TRANSORBENT BORDER 9X14 CM 3U', 'FOAM'],
'478545':['ALLEVYN GENTLE BORDER LITE 8X8CM 3U', 'FOAM'],
'486142':['ASKINA TRANSORBENT 5X7 CM 3U', 'FOAM'],
'478560':['ALLEVYN GENTLE BORDER LITE 5,5X12CM 3U', 'FOAM'],
'480921':['TIELLE 15X15CM 3U', 'FOAM'],
'400125':['PERMAFOAM CONCAVE 16,5X18CM 3U', 'FOAM'],
'488791':['ALGISITE M 10X10 CM 3U', 'FIBRES'],
'488817':['ALGISITE M 15X15 CM 3U', 'FIBRES'],
'488783':['ALGISITE M 5X5 CM 3U', 'FIBRES'],
'480236':['AQUACEL 2X45 CM 3U', 'FIBRES'],
'496075':['AQUACEL AG + 2,5CMX45CM 3U', 'FIBRES'],
'496042':['AQUACEL AG + EXTRA 5X6CM 3U', 'FIBRES'],
'496059':['AQUACEL AG + EXTRA 10X13CM 3U', 'FIBRES'],
'496067':['AQUACEL AG + EXTRA 15X14,5CM 3U', 'FIBRES'],
'480202':['AQUACEL EXTRA 5X5 CM 3U', 'FIBRES'],
'480210':['AQUACEL EXTRA 10X10 CM 3U', 'FIBRES'],
'480228':['AQUACEL EXTRA 15X15 CM 3U', 'FIBRES'],
'499095':['ASKINA CALGITROL AG 10X10CM', 'FIBRES'],
'499103':['ASKINA CALGITROL AG 15X15CM 3U', 'FIBRES'],
'488999':['ASKINA SORB 10X10CM 3U', 'FIBRES'],
'489005':['ASKINA SORB 15X15CM 3U', 'FIBRES'],
'489013':['ASKINA SORB 2,7X34CM 3U', 'FIBRES'],
'488981':['ASKINA SORB 6X6CM 3U', 'FIBRES'],
'486613':['ASKINA SORBSAN PLUS 10X15 CM 3U', 'FIBRES'],
'480681':['BIATAIN ALGINATE 10X10CM 3U', 'FIBRES'],
'480699':['BIATAIN ALGINATE AG 15X15 CM 3U', 'FIBRES'],
'482182':['BIATAIN FIBER 15X15 CM 3 U', 'FIBRES'],
'496760':['EXUFIBER 15X15CM 3U', 'FIBRES'],
'496752':['EXUFIBER AG+ 10X10 CM 3 U', 'FIBRES'],
'486761':['SORBALGON 10X10 CM 3U', 'FIBRES'],
'496000':['URGOCLEAN 10X10CM 3U', 'FIBRES'],
'480269':['URGOCLEAN AG 10 X 10 CM 3U', 'FIBRES'],
'480277':['URGOCLEAN AG 15 X 15 CM 3U', 'FIBRES'],
'496026':['URGOCLEAN MECHAS 2,5X40CM 3U', 'FIBRES']
}

try:
    data, = u.getOne('SELECT data_ext FROM nodrizas.dextraccio d', 'nodrizas')
    menys1m = data - relativedelta(month=1)
    info_up = c.Counter()
    info_uba = c.Counter()
    sql = """select ppfmc_pf_codi, ppfmc_pmc_amb_cod_up, ppfmc_pmc_amb_num_col 
            from import.tractaments t 
            where ppfmc_pf_codi in {codis}
            and ppfmc_data_fi >= (SELECT data_ext FROM nodrizas.dextraccio d)""".format(codis=tuple(CODIS.keys()))
    print(sql)
    for codi, up, col in u.getAll(sql, 'import'):
        info_up[(str(codi), up)] += 1
        info_uba[(str(codi), up, col)] += 1
    
    sql = """SELECT C_PRODUCTE_PRESc, c_up_disp, count(1)
    FROM OPS$DREPROD.B16_RECEPTES
    partition({part})
    WHERE C_PRODUCTE_PRESC IN ('496380', '486902', '499103', '400124', '488999', '496067',
    '493072', '496372', '482125', '486852', '480236', '496661', '496703', '489245', '486613',
    '488981', '492934', '491860', '483305', '495671', '480202', '476887', '400130', '483313',
    '491845', '494039', '496406', '488973', '400129', '400128', '491134', '482141', '400120',
    '400123', '400122', '400125', '496729', '496000', '480210', '496752', '491852', '488494',
    '400116', '400117', '488965', '400118', '480699', '496737', '486159', '495754', '491126',
    '467142', '477067', '478560', '480269', '486480', '489468', '499087', '496026', '488791',
    '488817', '486860', '400105', '497842', '467159', '488957', '489013', '495747', '486142',
    '499715', '488783', '488940', '471037', '491837', '478578', '496695', '486167', '499095',
    '489005', '496059', '493601', '495739', '478545', '499707', '487181', '488932', '486969',
    '489187', '492942', '486282', '499681', '496687', '482182', '480921', '495648', '486977',
    '483297', '496760', '480681', '473025', '477059', '493619', '496042', '487728', '486472',
    '469767', '492959', '480277', '486910', '499699', '486761', '496075', '482133', '496364',
    '488114', '400121', '487736', '458117', '493064', '480228', '496679')
    AND C_ESTAT_DISP in ('DISP')
    AND c_data_prev_disp between DATE '{menys1m}' AND DATE '{DEXTD}'
    GROUP BY C_PRODUCTE_PRESc, c_up_disp
    UNION
    SELECT C_PRODUCTE_PRESc, c_up_disp, count(1)
    FROM OPS$DREPROD.B16_RECEPTES
    partition({part2})
    WHERE C_PRODUCTE_PRESC IN ('496380', '486902', '499103', '400124', '488999', '496067',
    '493072', '496372', '482125', '486852', '480236', '496661', '496703', '489245', '486613',
    '488981', '492934', '491860', '483305', '495671', '480202', '476887', '400130', '483313',
    '491845', '494039', '496406', '488973', '400129', '400128', '491134', '482141', '400120',
    '400123', '400122', '400125', '496729', '496000', '480210', '496752', '491852', '488494',
    '400116', '400117', '488965', '400118', '480699', '496737', '486159', '495754', '491126',
    '467142', '477067', '478560', '480269', '486480', '489468', '499087', '496026', '488791',
    '488817', '486860', '400105', '497842', '467159', '488957', '489013', '495747', '486142',
    '499715', '488783', '488940', '471037', '491837', '478578', '496695', '486167', '499095',
    '489005', '496059', '493601', '495739', '478545', '499707', '487181', '488932', '486969',
    '489187', '492942', '486282', '499681', '496687', '482182', '480921', '495648', '486977',
    '483297', '496760', '480681', '473025', '477059', '493619', '496042', '487728', '486472',
    '469767', '492959', '480277', '486910', '499699', '486761', '496075', '482133', '496364',
    '488114', '400121', '487736', '458117', '493064', '480228', '496679')
    AND C_ESTAT_DISP in ('DISP')
    AND c_data_prev_disp between DATE '{menys1m}' AND DATE '{DEXTD}'
    GROUP BY C_PRODUCTE_PRESc, c_up_disp"""
    disp_up = {}
    part = 'PART_' + str(data.year) + str(data.month).zfill(2)
    part2 = 'PART_' + str(menys1m.year) + str(menys1m.month).zfill(2)
    for prod, up, val in u.getAll(sql.format(part=part, part2=part2, menys1m=menys1m, DEXTD=data), 'exadata_cs'):
        if prod == '476887':
            disp_up[(prod, up)] = val*10
        else:
            disp_up[(prod, up)] = val*3
    
    print('ok data')

    centres = {}
    sql = """select scs_codi, amb_codi, amb_desc, sap_codi, sap_desc, ics_desc  from nodrizas.cat_centres"""
    for up, ambit, ambit_desc, sap, desc_sap, up_desc in u.getAll(sql, 'nodrizas'):
        centres[up] = [ambit, ambit_desc, sap, desc_sap, up_desc]

    sql = """SELECT a.up_cod, b.ambit_cod, b.ambit_des, b.sap_cod, b.sap_des, a.up_des
            FROM dwsisap.dbc_rup a
            INNER JOIN dwsisap.dbc_centres b
            ON a.abs_cod = b.abs_cod"""
    for up, ambit, ambit_desc, sap, desc_sap, up_desc in u.getAll(sql, 'exadata'):
        centres[up] = [ambit, ambit_desc, sap, desc_sap, up_desc]
    

    enviament = []
    for (codi, up), n in info_up.items():
        descriptiu, tipus = CODIS[codi]
        if up in centres:
            ambit, ambit_desc, sap, desc_sap, up_desc = centres[up]
            enviament.append((ambit, ambit_desc, sap, desc_sap, up, up_desc, data, codi, descriptiu, tipus, n))

    data_text = str(data.day) + '/' + str(data.month) + '/' + str(data.year)
    wb1 = xlwt.Workbook()
    ws_1 = wb1.add_sheet(u'1')
    ws_1.write(0, 0, 'AMBIT'.decode('iso-8859-1'))
    ws_1.write(0, 1, 'AMBIT_DESC'.decode('iso-8859-1'))
    ws_1.write(0, 2, 'SAP'.decode('iso-8859-1'))
    ws_1.write(0, 3, 'SAP_DESC'.decode('iso-8859-1'))
    ws_1.write(0, 4, 'UP'.decode('iso-8859-1'))
    ws_1.write(0, 5, 'UP_DESC'.decode('iso-8859-1'))
    ws_1.write(0, 6, 'DATA'.decode('iso-8859-1'))
    ws_1.write(0, 7, 'CODI'.decode('iso-8859-1'))
    ws_1.write(0, 8, 'DESCRIPTIU'.decode('iso-8859-1'))
    ws_1.write(0, 9, 'TIPUS'.decode('iso-8859-1'))
    ws_1.write(0, 10, 'N'.decode('iso-8859-1'))
    i_1 = 1
    for (ambit, ambit_desc, sap, desc_sap, up, up_desc, data, codi, descriptiu, tipus, n) in enviament:
        ws_1.write(i_1, 0, ambit.decode('iso-8859-1'))
        ws_1.write(i_1, 1, ambit_desc.decode('iso-8859-1'))
        ws_1.write(i_1, 2, sap.decode('iso-8859-1'))
        ws_1.write(i_1, 3, desc_sap.decode('iso-8859-1'))
        ws_1.write(i_1, 4, up.decode('iso-8859-1'))
        ws_1.write(i_1, 5, up_desc.decode('iso-8859-1'))
        ws_1.write(i_1, 6, data_text.decode('iso-8859-1'))
        ws_1.write(i_1, 7, codi.decode('iso-8859-1'))
        ws_1.write(i_1, 8, descriptiu.decode('iso-8859-1'))
        ws_1.write(i_1, 9, tipus.decode('iso-8859-1'))
        ws_1.write(i_1, 10, n)
        i_1 += 1

    wb1.save(u.tempFolder + 'aprovisionament_aposits_{}.xls'.format(data.strftime("%Y_%m_%d")))  # noqa)

    subject = 'Seguiment prescripcio activa dels aposits'
    text = "Bon dia.\n\nAdjuntem arxiu Excel amb les dades de seguiment setmanal d aprovisonament dels aposits.\n"
    text += "\n\nSalutacions."

    u.sendGeneral('SISAP <sisap@gencat.cat>',
                        ('gemmacalvet@gencat.cat', 'ambonet.ics@gencat.cat', 'efornies@gencat.cat'),
                        ('roser.cantenys@catsalut.cat', 'cguiriguet.bnm.ics@gencat.cat'),
                        subject,
                        text,
                        u.tempFolder + 'aprovisionament_aposits_{}.xls'.format(data.strftime("%Y_%m_%d")))

    if u.IS_MENSUAL:
        enviament_mensual = []
        for (codi, up), n in disp_up.items():
            descriptiu, tipus = CODIS[codi]
            if up in centres:
                ambit, ambit_desc, sap, desc_sap, up_desc = centres[up]
                enviament_mensual.append((ambit, ambit_desc, sap, desc_sap, up, up_desc, data, codi, descriptiu, tipus, n))

        data_text = str(data.day) + '/' + str(data.month) + '/' + str(data.year)
        wb2 = xlwt.Workbook()
        ws_1 = wb2.add_sheet(u'1')
        ws_1.write(0, 0, 'AMBIT'.decode('iso-8859-1'))
        ws_1.write(0, 1, 'AMBIT_DESC'.decode('iso-8859-1'))
        ws_1.write(0, 2, 'SAP'.decode('iso-8859-1'))
        ws_1.write(0, 3, 'SAP_DESC'.decode('iso-8859-1'))
        ws_1.write(0, 4, 'UP'.decode('iso-8859-1'))
        ws_1.write(0, 5, 'UP_DESC'.decode('iso-8859-1'))
        ws_1.write(0, 6, 'DATA'.decode('iso-8859-1'))
        ws_1.write(0, 7, 'CODI'.decode('iso-8859-1'))
        ws_1.write(0, 8, 'DESCRIPTIU'.decode('iso-8859-1'))
        ws_1.write(0, 9, 'TIPUS'.decode('iso-8859-1'))
        ws_1.write(0, 10, 'N'.decode('iso-8859-1'))
        i_1 = 1
        for (ambit, ambit_desc, sap, desc_sap, up, up_desc, data, codi, descriptiu, tipus, n) in enviament_mensual:
            ws_1.write(i_1, 0, ambit.decode('iso-8859-1'))
            ws_1.write(i_1, 1, ambit_desc.decode('iso-8859-1'))
            ws_1.write(i_1, 2, sap.decode('iso-8859-1'))
            ws_1.write(i_1, 3, desc_sap.decode('iso-8859-1'))
            ws_1.write(i_1, 4, up.decode('iso-8859-1'))
            ws_1.write(i_1, 5, up_desc.decode('iso-8859-1'))
            ws_1.write(i_1, 6, data_text.decode('iso-8859-1'))
            ws_1.write(i_1, 7, codi.decode('iso-8859-1'))
            ws_1.write(i_1, 8, descriptiu.decode('iso-8859-1'))
            ws_1.write(i_1, 9, tipus.decode('iso-8859-1'))
            ws_1.write(i_1, 10, n)
            i_1 += 1

        wb2.save(u.tempFolder + 'aprovisionament_aposits_mensual_{}.xls'.format(data.strftime("%Y_%m_%d")))  # noqa)

        subject = 'Seguiment dispensacio dels aposits'
        text = "Bon dia.\n\nAdjuntem arxiu Excel amb les dades de seguiment mensual de dispensacio dels aposits.\n"
        text += "\n\nSalutacions."

        u.sendGeneral('SISAP <sisap@gencat.cat>',
                            ('gemmacalvet@gencat.cat', 'ambonet.ics@gencat.cat', 'efornies@gencat.cat'),
                            ('roser.cantenys@catsalut.cat', 'cguiriguet.bnm.ics@gencat.cat'),
                            subject,
                            text,
                            u.tempFolder + 'aprovisionament_aposits_mensual_{}.xls'.format(data.strftime("%Y_%m_%d")))
    
    upload_table = []
    for (codi, up, col), n in info_uba.items():
        descriptiu, tipus = CODIS[codi]
        if up in centres:
            ambit, ambit_desc, sap, desc_sap, up_desc = centres[up]
            upload_table.append((ambit, ambit_desc, sap, desc_sap, up, up_desc, col, data, codi, descriptiu, tipus, n, 'PRESC'))
    
    if u.IS_MENSUAL:
        for (codi, up), n in disp_up.items():
            descriptiu, tipus = CODIS[codi]
            if up in centres:
                ambit, ambit_desc, sap, desc_sap, up_desc = centres[up]
                upload_table.append((ambit, ambit_desc, sap, desc_sap, up, up_desc, '', data, codi, descriptiu, tipus, n, 'DISP'))

    cols = """(
        ambit varchar2(10),
        ambit_desc varchar2(100),
        sap varchar2(10),
        desc_sap varchar2(100),
        up varchar2(10),
        up_desc varchar2(100),
        col varchar2(100),
        data date,
        codi varchar2(10),
        descriptiu varchar2(100),
        tipus varchar2(10),
        n int,
        tipus_presc varchar2(10))"""
    # u.createTable('aprovisonament_aposits', cols, 'exadata')
    # u.grantSelect('aprovisonament_aposits', 'DWSISAP_ROL', 'exadata')
    # u.grantSelect('aprovisonament_aposits', 'DWRRODRIGUEZ', 'exadata')
    tipus = ('PRESC', 'DISP') if u.IS_MENSUAL else ('PRESC', '')
    sql = "delete from aprovisonament_aposits where data = date '{data}' and tipus in {tipus}"
    u.execute(sql.format(data=data, tipus=tuple(tipus)), 'exadata')
    u.listToTable(upload_table, 'aprovisonament_aposits', 'exadata')

except:
    subject = 'ERROR: Seguiment prescripcio activa dels aposits'
    text = "ERROR.\n"
    text += "\n\nSalutacions."

    u.sendGeneral('SISAP <sisap@gencat.cat>',
                        ('roser.cantenys@catsalut.cat', 'cguiriguet.bnm.ics@gencat.cat'),
                        ('roser.cantenys@catsalut.cat', 'cguiriguet.bnm.ics@gencat.cat'),
                        subject,
                        text)
