import sisapUtils as u
import collections as c

u.printTime('inici')
sql = """
    SELECT id_cip_sec
    FROM assignada_tot
    WHERE edat >= 0
    AND edat <= 14
"""

pob = set()
for id, in u.getAll(sql, 'nodrizas'):
    pob.add(id)
u.printTime('pob')
sql = """
    SELECT id_cip_sec, pr_cod_ps
    FROM {tb}
    WHERE pr_cod_ps in ('C01-D82.1', 'C01-Q99.9')
"""
upload = []
tables = u.getSubTables('problemes', 'import')
for tb in tables:
    for id, codi in u.getAll(sql.format(tb=tb), 'import'):
        if id in pob:
            upload.append((id, codi))
    u.printTime(tb)
u.printTime('problemes')
cols = "(id_cip_sec int, diagnostic varchar(10))"
u.createTable("peticio_tea", cols, "altres", rm=True)
u.listToTable(upload, "peticio_tea", "altres")
u.printTime('fi')