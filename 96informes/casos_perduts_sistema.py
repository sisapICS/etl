
# coding: utf8

import sisapUtils as u
import collections as c
import sisaptools as t


DATABASE = ("exadata", "thewhole")

POSITIUS_raw = set([
                'Positiu 1/320',
                'Positiu 1/1280',
                'S\'observa dèbil fluorescència amb una',
                'Positiu títol: 1/40',
                'POSITIU 1/40',
                'P640',
                'Positiu títol: 1/320',
                'Positiu títol: 1/80',
                'Positiu títol > 1/1280',
                'Positiu títol: 1/640',
                '563,4',
                'Positiu (títol 1:640) Específic enfront de M2',
                '* POSITIU DÈBIL',
                '>1/320',
                'Positiu, títol >=1/1280',
                'Positiu DÈBIL 1:40',
                'Positiu, 320 URF, Tinció difícil de classificar',
                'Positiu, 40 URF, compatible MA-M2',
                'Positiu: títol 1/1280',
                'Positiu 320 URF, Patró tipus "M2"',
                'Positiu 640 URF, AMA-M2',
                'Positiu 320 URF, possible compatible amb AMA-M2',
                'Positiu 40 URF, ATI Citoplasmàtic atípic, de signi',
                'Positiu 80 URF, No classificable podria ser però,',
                'Positiu 80 URF, Possible Patró AMA-M2',
                'veure comentari',
                'Positiu 80 URF, compatible amb Patró AMA-M2',
                '1/>640',
                'Positiu dubtos, 80 URF',
                'Positiu (títol 1:40)~Especi...',
                '320',
                'título 1/320',
                'Positiu, títol 1/320',
                'títol 1/80',
                'Positiu, títol 1:160',
                'Positiu, títol 1:80',
                'P80',
                'P>640',
                'Positiu',
                'Titol 1/80',
                'POSITIU 1/160',
                'POSITIU: 1/40',
                'Positiu, 80 URF, No classificable però compatible',
                'Positiu 640 URF, Patró tipus "M2"',
                'Positiu 160 URF, Patró no classificable',
                'Positiu, Ttol 1/320',
                'Positiu 640 URF, compatible AMA-M2',
                'Positiu, títol 1/1280',
                'Positiu 80 URF, compatible amb Patró AMA-M2',
                '1/80 tipus M2',
                'dubtós, possible positiu 40 URF, Patró compatible',
                'Positiu atípic 40 URF',
                'Positiu (títol 1:1280)~Espe...',
                'Positiu títol >1/640',
                'títol 1/640',
                'títol 1/160',
                'título 1/640',
                'Positiu 1/40',
                'Positiu, títol 1/640',
                'Positiu, títol 1:640',
                'Positiu 1/2560',
                'POSITIU  1:640',
                'POSITIU 1/80',
                'S\'observa una DÈBIL fluorescència compatible',
                'Positiu, 80 URF, No classificable podria ser',
                'P160',
                'Positiu, 1/40',
                'Positiu, títol >1/1280',
                '1280',
                'títol 1/1280',
                'Titol 1/1280',
                'Positiu, 80 URF',
                'títol 1/40, Patró motejat',
                'POSITIU: 1/160',
                'Positiu dubtós, 80 URF, Tinció atípica, que podria',
                'Positiu (títol 1:40) Específic enfront de M2',
                'Positiu, 80 URF, Compatible amb',
                'Positiu 40 URF, No classificable podria ser',
                'Positiu, Ttol 1/160',
                'Positiu 40 URF, Patró tipus "M2"',
                'Positiu 40 URF, compatible amb AMA-M2',
                'Positiu 160 URF (Patró diferent de AMA-M2)',
                'Positiu 80 URF, possible compatible amb AMA-M2',
                'Positiu 160 URF, possible compatible amb AMA-M2',
                'Positiu 640 URF, possible compatible amb AMA-M2',
                'Positiu 80 URF, compatible amb AMA-M2',
                '1/640 Positiu',
                'Positiu títol 1/640',
                'Positiu 1/80',
                'Positiu títol 1/160',
                'Positiu 1/640',
                'títol 1/320',
                'Positiu',
                'POSITIU 1:80',
                'POSITIU 1:40',
                '640',
                '1/320 (Positivo)',
                'POSITIU: 1/640',
                'Positiu, 40 URF, tipus M2',
                'Positiu (títol 1:40)',
                'Positiu, 80 URF Patró tipus "M2"',
                'Positiu: títol 1/160',
                'Positiu: títol 1/40',
                'Positiu: títol 1/80',
                'Positiu DÈBIL (títol 1:40)',
                'Positiu (títol 1:40)',
                'Positiu (títol 1:80) EspecÍFIC enfront de M2',
                'Positiu, 160 URF, No classificable podria ser',
                'Positiu, Ttol 1/1280',
                'Positiu, titol >1:1280',
                'Positiu, títol 1/320',
                'Positiu 160 URF, No classificable podria ser',
                'Dubtós 160 URF, No classificable',
                'Positiu 40 URF, No classificable podria ser M2COM',
                'Positiu',
                '160',
                '80',
                'título 1/640      < 1/20',
                'Positiu, > 640 URF',
                'Positiu títol: 1/1280',
                'Positiu, títol 1:320',
                'Positiu, 160 URF, Patró tipus "M2"',
                'Positiu, 40 URF, Patró tipus "M2"',
                'Positiu (títol 1:1280) EspecÍFIC enfront de M2',
                '1/640',
                'Positiu, 40 URF, No classificable podria ser',
                'POSITIU: 1/80',
                'Positiu 1/40 motejat',
                'Positiu, títol 1/80',
                'Positiu, 40 URF, No classificable',
                'POSITIU DÈBIL',
                'Positiu: títol > 1/1280',
                'Positiu, 80 URF, AMA-M2',
                'Positiu 160 URF, compatible amb AMA-M2, però també',
                'Positiu 80 URF, compatible amb Patró MA-M2',
                'Positiu, Ttol 1/40',
                'POSITIU 1/320',
                'Positiu 40 URF, possible compatible AMA-M2',
                'Positiu: títol > 1/640',
                '1/320 Positiu',
                'Positiu 160 URF, compatible amb AMA-M2',
                '1/160 Positiu',
                'Positiu (títol 1:80)~Especi...',
                'Positiu títol 1/80',
                '> 1/640',
                'POSITIU',
                '1/160',
                'Positiu, títol 1/40',
                'Positiu 1:320',
                'Positiu, títol 1/80',
                '1/320',
                'P40',
                'Positiu títol: 1/160',
                'Positiu 1/40 DÈBIL',
                'Positiu,40 URF, Patró No classificable que podria',
                'Positiu, 40 URF, No classificable que podria ser',
                'Possible Positiu molt DÈBIL, < 40 URF, que podria ser compatible amb el Patró AMA-M2',
                'Positiu 160 URF, Patró AMA-M2',
                'POSITIU: 1/320',
                'Veure comentari resultat',
                'Positiu, 80 URF, Patró atípic, podria ser',
                'dubtós, possible Mitocondrial diferent de AMA-M2',
                'Positiu 80 URF, Patró atípic, compatible amb',
                'Positiu DÈBIL',
                'Positiu ( títol 1:640 ) Esp...',
                'Positiu (títol 1:640)~Espec...',
                'Positiu',
                'Positiu 1:160',
                'Positiu, títol 1:1280',
                'Titol 1/640',
                'Titol 1/320',
                'Positiu 1/160',
                'Anti-mitocondrials títol 1/40',
                'Positiu títol: 1/20',
                'P320',
                'Positiu, 320 URF, Patró tipus "M2"',
                'Positiu (títol 1:320) Específic enfront de M2',
                'Positiu, títol >=1/160',
                'Positiu, 40 URF',
                '1/1280',
                'Positiu, >640 URF, Patró tipus "M2"',
                'Vegeu comentari',
                'Positiu títol > 1/640',
                'Positiu, 320 URF, Mitocondial atípic',
                'VEURE COMENTARI',
                'Positiu: títol 1/320',
                'Positiu, 40 URF, compatible amb',
                'Positiu, 80 URF, Patró compatible amb AMA-M2',
                'Positiu, Ttol 1/80',
                'Positiu 80 URF, No classificable, Podria ser',
                'Positiu 80 URF, no classificable',
                'Positiu 80 URF, parcialment',
                'Positiu 320 URF',
                'Positiu ( títol 1:40) Espec...',
                'Positiu ( títol 1:160). Esp...',
                'Positiu títol 1/40',
                'Positiu títol 1/320',
                'Titol 1/160',
                'título 1/160',
                'Positiu, títol 1/160',
                'Positiu a títols superiors a  1:640',
                'Positiu, 640 URF, Patró tipus "M2"',
                'Positiu, 80 URF, Patró tipus "M2"',
                'Positiu, 40 URF, Patró No classificable que podria',
                '"POSITIU	1:640"',
                'Positiu, 80 URF, Tinció No classificable que',
                'Positiu 1:40',
                'Positiu, 80 URF, No classificable, compatible',
                'Positiu 80 URF, Tinció no classificable que',
                'Positiu, 80 URF, Tinció atípica, parcialment',
                'Positiu, 80 URF, posible mitocondrial atípic,',
                'Positiu, 320 URF, Patró diferent de AMA-M2',
                'Positiu, títol 1:20',
                'Positiu: títol 1/640',
                'Positiu, 40 URF, Compatible M2',
                'POSITIU: 1/1280',
                'Positiu, 40 URF, No classificable, podria ser',
                'Positiu, 320 URF, Patró AMA-M2 (tipus AMA-M2b)',
                'Positiu 160 URF, Patró tipus "M2"',
                'Positiu 80 URF, Patró tipus "M2"',
                'Positiu 80 URF, No classificable podria ser',
                'Positiu, Ttol >1/1280',
                '1/160 tipus M2',
                'Positiu, Ttol 1/640',
                'Positiu 40 URF, possible compatible amb AMA-M2',
                'Positiu 80 URF, No classificable, podria ser',
                'Positiu 80 URF, Patró compatible amb AMA-M2',
                'Positiu 40 URF, M2 Compatible',
                'Positiu 40 URF, podria ser',
                'Positiu 80 URF, compatile amb AMA M1 i potser M2b'])


POSITIUS = set()

for e in POSITIUS_raw:
    e = e.lower()
    e = e.replace(" ", "")
    POSITIUS.add(e)


class Perduts():
    def __init__(self):
        self.get_population()
        self.get_problemes()
        self.get_info_pacient()
        self.uploading()
        
    def get_population(self):
        print('laboratori')
        self.population = {}
        self.fa = c.defaultdict(set)
        sql = """SELECT ID, UP, LAB, PROVA, DATA, RESULTAT, UNITATS 
                FROM dwtw.LABORATORI_DETALL
                WHERE data_modi > DATE '2015-01-01'
                AND DATA BETWEEN DATE '2015-01-01' 
                AND DATE '2022-12-31'
                AND PROVA IN ('Q30366','Q30385','A09385')"""
        with t.Database(*DATABASE) as the_whole:
            for cip, up, lab, prova, data, resultat, unitats in the_whole.get_all(sql):
                cip = cip[0:13]
                if prova == 'A09385':
                    r = resultat.lower()
                    r = r.replace(" ", "")
                    if r in POSITIUS:
                        if cip in self.population:
                            if data > self.population[cip][3]:
                                self.population[cip] = [up, lab, prova, data, resultat]
                        else:
                            self.population[cip] = [up, lab, prova, data, resultat]
                else:
                    self.fa[cip].add((data, resultat, unitats))

    def get_problemes(self):
        print('problemes')
        self.problemes = {}
        sql = """select id, DATA 
                    from dwtw.problemes
                    where codi IN ('C01-K83', 'C01-K74', 'C01-K74.3')"""
        with t.Database(*DATABASE) as the_whole:
            for cip, data in the_whole.get_all(sql):
                cip = cip[0:13]
                if cip in self.problemes:
                    if data < self.problemes[cip]:
                        self.problemes[cip] = data
                else:
                    self.problemes[cip] = data
    
    def get_info_pacient(self):
        print('info pacients')
        self.pacients = {}
        sql = """SELECT cip, edat, sexe 
            FROM dwsisap.dbc_poblacio
            WHERE situacio = 'A'"""
        for cip, edat, sexe in u.getAll(sql, 'exadata'):
            cip = cip[0:13]
            self.pacients[cip] = [edat, sexe]

        print('rup')
        self.altres = set()
        self.primaria = set()
        self.hospitalaria = set()
        sql = """SELECT tip_cod, up_cod FROM dwsisap.dbc_rup"""
        for tipus, up in u.getAll(sql, 'exadata'):
            if tipus == '10':
                self.primaria.add(up)
            if tipus == '20':
                self.hospitalaria.add(up)
            else:
                self.altres.add(up)


    def uploading(self):
        print('upload')
        cols = """(cip varchar2(13), ultima_ama date, resultat_ama varchar2(200), 
                    edat int, sexe varchar2(2), laboratori varchar2(5), 
                    origen_peti varchar2(3), dx date, fa_data date, fa_valor varchar2(200), 
                    fa_unitats varchar2(200))"""
        u.createTable('casos_ama_positius', cols, 'exadata', rm=True)
        u.grantSelect('casos_ama_positius','DWSISAP_ROL','exadata')
        self.upload = []
        for cip, (up, lab, prova, data, resultat) in self.population.items():
            if cip in self.pacients:
                print('cip in patients')
                edat, sexe = self.pacients[cip]
                try:
                    edat = int(edat)
                except:
                    edat = None
                origen_peti = ''
                if up in self.altres:
                    origen_peti = 'ALT'
                elif up in self.primaria:
                    origen_peti = 'PRI'
                elif up in self.hospitalaria:
                    origen_peti = 'HOS'
                dx = self.problemes[cip] if cip in self.problemes else None
                fa_data, fa_valor, fa_unitats = None, None, None
                if cip in self.fa:
                    for (data_f, resultat_f, unitats_f) in self.fa[cip]:
                        dif1 = data_f - data 
                        if dif1.days >= -180 and dif1.days <= 0:
                            fa_data, fa_valor, fa_unitats = data_f, resultat_f, unitats_f
                        dif2 = data_f - data 
                        if dif2.days <= 180 and dif2.days >= 0:
                            fa_data, fa_valor, fa_unitats = data_f, resultat_f, unitats_f
                self.upload.append((cip, data, resultat, edat, sexe, lab, origen_peti, dx, fa_data, fa_valor, fa_unitats))
        
        u.listToTable(self.upload, 'casos_ama_positius', 'exadata')

        
if __name__ == "__main__":
    try:
        Perduts()
    except Exception as e:
        mail = t.Mail()
        mail.to.append("roser.cantenys@catsalut.cat")
        mail.subject = "CASOS LAB MAL!!!"
        mail.text = str(e)
        mail.send()



                
                