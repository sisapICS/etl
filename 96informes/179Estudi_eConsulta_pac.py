# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = "SISAP_ECONSULTA_PACIENTS"
db = "redics"

class EConsultaPac(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_centres()
        self.get_gma()
        self.get_medea()
        self.get_id()
        self.get_visites()
        self.get_LMS()
        self.get_converses()
        self.get_missatges()
        self.get_dades_converses()
        self.cip_to_hash()
        self.get_col_to_servei()
        self.get_vvirtuals()
        self.get_poblacio()
        self.upload_data()


    def get_centres(self):
        """."""
        self.centres = {}
        sql = "select scs_codi, ics_codi, ics_desc, ep from cat_centres"
        for up, br, desc, ep in u.getAll(sql, 'nodrizas'):
            self.centres[up] = {'br': br, 'desc': desc, 'ep': ep}
    
    def get_gma(self):
        """."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        self.gma = {(id): (cod, cmplx, num) for (id, cod, cmplx, num) 
                    in  u.getAll(*sql)}
    
    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        sql = ("select sector, valor \
                from sisap_medea", "redics")
        valors = {sector: str(valor) for (sector, valor)
                  in u.getAll(*sql)}
        self.Imedea = {}                  
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if str(sector) in valors:
                self.Imedea[(id)] = valors[sector] 
    
    def get_id(self):
        """."""
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        self.id_to_hash = {(id): hash for (id, sec, hash) in u.getAll(sql, 'import')} 
        self.hash_to_id = {(hash, sec): id for (id, sec, hash) in u.getAll(sql, 'import')} 
    
    def get_visites(self):
        """."""
        self.visites = c.Counter()
        for table in u.getSubTables('visites'):
            dat, = u.getOne('select year(visi_data_visita) from {} limit 1'.format(table), 'import')
            if dat >= 2016:
                print table
                sql = "select  codi_sector, id_cip_sec,  visi_tipus_visita, year(visi_data_visita), visi_servei_codi_servei \
               from {}, nodrizas.dextraccio where visi_data_visita <= data_ext and visi_data_baixa = 0 and visi_situacio_visita ='R'".format(table)
                for sector, id, tipus, periode, servei in u.getAll(sql, 'import'):
                    if servei in ('MG', 'PED'):
                        tip = servei
                    elif servei in ('INF', 'ENF', 'GCAS', 'INFG'):
                        tip = 'INF'
                    else:
                        continue
                    self.visites[(id, str(periode), tip, 'totals')] += 1
                    if tipus not in ('9T', '9E'):
                        self.visites[(id, str(periode), tip, 'presencials')] += 1

   
    def get_LMS(self):
        """Mirem que el pacient estigui a la taula de redics de LMS que vam crear a partir de les dades que ens van passar els de HC3"""
        a = 0
        self.LMS_pacients = {}
        sql = "select codi_sector, hash, data_acreditacio, data_primer_autoregistre, data_ultim_autoregistre from SISAP_LMS_AUTOREGISTRATS"
        for sector, hash, data_acredit, data_first_auto, data_last_auto in u.getAll(sql, "redics"):
            try:
                data_a = datetime.strptime(data_acredit[:10], '%Y-%m-%d')
            except ValueError:
                data_a = datetime.strptime(data_acredit[:10], '%d/%m/%Y')
            try:
                data_pr = datetime.strptime(data_first_auto[:10], '%Y-%m-%d')
            except ValueError:
                data_pr = datetime.strptime(data_first_auto[:10], '%d/%m/%Y')
            try:
                data_ur = datetime.strptime(data_last_auto[:10], '%Y-%m-%d')
            except ValueError:
                data_ur = datetime.strptime(data_last_auto[:10], '%d/%m/%Y')
            id = self.hash_to_id[(hash, sector)] if (hash, sector) in self.hash_to_id else None
            if id:
                self.LMS_pacients[(id)] = {'acredit': data_a.strftime('%Y%m'), 'first': data_pr.strftime('%Y%m'), 'last': data_ur.strftime('%Y%m')}
        print a
       
    def get_converses(self):
        """."""
        self.converses = {}
        self.professionals_time = {}
        self.pacients_time = {}
        sql = "select id_cip_sec, codi_sector, conv_id, conv_autor, \
                      conv_desti, conv_estat, conv_dini \
               from econsulta_conv, nodrizas.dextraccio"
        for pac, sector, id, autor, desti, estat, dat in u.getAll(sql, 'import'):
            prof = (autor if autor else desti)
            self.converses[(sector, id)] = {'pac': pac,
                                            'prof': prof,
                                            'inicia': not desti,
                                            'estat': estat,
                                            'sector': sector,
                                            'msg': {}}
            if autor:
                if pac in self.professionals_time:
                    first, last = self.professionals_time[pac]['first'],self.professionals_time[pac]['last']
                    if dat < first:
                        self.professionals_time[pac]['first'] = dat
                    if dat > last:
                        self.professionals_time[pac]['last'] = dat
                else:
                    self.professionals_time[pac] = {'first': dat, 'last': dat}
            if desti:
                if pac in self.pacients_time:
                    first, last = self.pacients_time[pac]['first'],self.pacients_time[pac]['last']
                    if dat < first:
                        self.pacients_time[pac]['first'] = dat
                    if dat > last:
                        self.pacients_time[pac]['last'] = dat
                else:
                    self.pacients_time[pac] = {'first': dat, 'last': dat}
 
    def get_missatges(self):
        """."""
        sql = "select codi_sector, msg_conv_id, msg_id, msg_autor, \
                      msg_desti, msg_data, date_format(msg_data,'%Y%m') \
               from econsulta_msg"
        for sector, id_c, id, autor, desti, data, period in u.getAll(sql, 'import'):
            if (sector, id_c) in self.converses:
                this = {'prof': autor if autor else desti,
                        'inicia': not desti,
                        'data': data,
                        'periodemsg': period}
                self.converses[(sector, id_c)]['msg'][id] = this   
    
    def get_dades_converses(self):
        """."""
        a = 0
        self.inici_prof = c.Counter()
        self.inici_pac = c.Counter()
        self.missatges = c.Counter()
        for conversa in self.converses.values():
            if conversa['inicia']:
                if conversa['pac'] in self.LMS_pacients:
                    self.inici_prof[(conversa['pac'])] += 1
                else:
                    a+= 1
            if not conversa['inicia']:
                self.inici_pac[(conversa['pac'])] += 1
            for id in range(1, 1000):
                if id in conversa['msg']:
                    self.missatges[(conversa['pac'])] += 1  
        print a
    
    def cip_to_hash(self):
        """."""
        self.hashos1 = {}
        sql = "select usua_cip, usua_cip_cod from pdptb101"
        for c,h in u.getAll(sql, 'pdp'):
            self.hashos1[(c)] = h
    
    def get_col_to_servei(self):
        """."""
        sql = "select ide_numcol, servei from cat_professionals"
        self.col_to_servei = {col: servei for (col, servei) in u.getAll(sql, 'import')} 
    
    def get_vvirtuals(self):
        """."""
        self.virtuals = c.Counter()
        for sector in u.sectors:
            print sector
            sql = "select vvi_cip, to_char(VVI_DATA_ACTIV, 'YYYY'), vvi_tipus, vvi_num_col from vistb048 where vvi_situacio = 'R'"
            for cip, periode, tipus, col in u.getAll(sql, sector):
                hash = self.hashos1[cip] if cip in self.hashos1 else None
                id = self.hash_to_id[(hash, sector)] if (hash, sector) in self.hash_to_id else None
                if id:
                    servei = self.col_to_servei[col] if col in self.col_to_servei else None
                    self.virtuals[(id, servei, str(periode),'vv')] += 1
                    if tipus == 'ECONSULTA':
                        self.virtuals[(id, servei, str(periode), 'EC')] += 1
    
    def get_poblacio(self):
        """."""
        self.upload = []
        sql = "select id_cip_sec, codi_sector, up, uba, ubainf, edat, sexe, nacionalitat, nivell_cobertura from assignada_tot"
        for id, sector, up, uba, ubainf, edat, sexe, nac, cobertura in u.getAll(sql, 'nodrizas'):
            hash = self.id_to_hash[(id)]
            br, desc, ep = self.centres[(up)]['br'], self.centres[(up)]['desc'], self.centres[(up)]['ep']
            cod = self.gma[(id)][0] if (id) in self.gma else None
            complex = self.gma[(id)][1] if (id) in self.gma else None
            ncronic = self.gma[(id)][2] if (id) in self.gma else None
            medea =  self.Imedea[(id)] if (id) in self.Imedea else None
            vistot2016MF = self.visites[(id, '2016', 'MG', 'totals')] if (id, '2016', 'MG', 'totals') in self.visites else 0
            vispres2016MF = self.visites[(id, '2016', 'MG', 'presencials')] if (id, '2016', 'MG', 'presencials') in self.visites else 0
            vv2016MF = self.virtuals[(id, 'MG', '2016', 'vv')] if (id, 'MG', '2016', 'vv') in self.virtuals else 0
            vvEC2016MF = self.virtuals[(id, 'MG', '2016', 'EC')] if (id, 'MG', '2016', 'EC') in self.virtuals else 0
            vistot2017MF = self.visites[(id, '2017', 'MG', 'totals')] if (id, '2017', 'MG', 'totals') in self.visites else 0
            vispres2017MF = self.visites[(id, '2017', 'MG', 'presencials')] if (id, '2017', 'MG', 'presencials') in self.visites else 0
            vv2017MF = self.virtuals[(id, 'MG', '2017', 'vv')] if (id, 'MG', '2017', 'vv') in self.virtuals else 0
            vvEC2017MF = self.virtuals[(id, 'MG', '2017', 'EC')] if (id, 'MG', '2017', 'EC') in self.virtuals else 0
            vistot2018MF = self.visites[(id, '2018', 'MG', 'totals')] if (id, '2018', 'MG', 'totals') in self.visites else 0
            vispres2018MF = self.visites[(id, '2018', 'MG', 'presencials')] if (id, '2018', 'MG', 'presencials') in self.visites else 0
            vv2018MF = self.virtuals[(id, 'MG', '2018', 'vv')] if (id, 'MG', '2018', 'vv') in self.virtuals else 0
            vvEC2018MF = self.virtuals[(id, 'MG', '2018', 'EC')] if (id, 'MG', '2018', 'EC') in self.virtuals else 0
            vistot2016INF = self.visites[(id, '2016', 'INF', 'totals')] if (id, '2016', 'INF', 'totals') in self.visites else 0
            vispres2016INF = self.visites[(id, '2016', 'INF', 'presencials')] if (id, '2016', 'INF', 'presencials') in self.visites else 0
            vv2016INF = self.virtuals[(id, 'INF', '2016', 'vv')] if (id, 'INF', '2016', 'vv') in self.virtuals else 0
            vvEC2016INF = self.virtuals[(id, 'INF', '2016', 'EC')] if (id, 'INF', '2016', 'EC') in self.virtuals else 0
            vistot2017INF = self.visites[(id, '2017', 'INF', 'totals')] if (id, '2017', 'INF', 'totals') in self.visites else 0
            vispres2017INF = self.visites[(id, '2017', 'INF', 'presencials')] if (id, '2017', 'INF', 'presencials') in self.visites else 0
            vv2017INF = self.virtuals[(id, 'INF', '2017', 'vv')] if (id, 'INF', '2017', 'vv') in self.virtuals else 0
            vvEC2017INF = self.virtuals[(id, 'INF', '2017', 'EC')] if (id, 'INF', '2017', 'EC') in self.virtuals else 0
            vistot2018INF = self.visites[(id, '2018', 'INF', 'totals')] if (id, '2018', 'INF', 'totals') in self.visites else 0
            vispres2018INF = self.visites[(id, '2018', 'INF', 'presencials')] if (id, '2018', 'INF', 'presencials') in self.visites else 0
            vv2018INF = self.virtuals[(id, 'INF', '2018', 'vv')] if (id, 'INF', '2018', 'vv') in self.virtuals else 0
            vvEC2018INF = self.virtuals[(id, 'INF', '2018', 'EC')] if (id, 'INF', '2018', 'EC') in self.virtuals else 0
            iniciprof = self.inici_prof[(id)] if (id) in self.inici_prof else 0
            inicipac = self.inici_pac[(id)] if (id) in self.inici_pac else 0
            missatges = self.missatges[(id)] if (id) in self.missatges else 0
            pacfirst = self.pacients_time[id]['first'] if (id) in self.pacients_time else None
            paclast = self.pacients_time[id]['last'] if (id) in self.pacients_time else None
            proffirst = self.professionals_time[id]['first'] if (id) in self.professionals_time else None
            proflast = self.professionals_time[id]['last'] if (id) in self.professionals_time else None
            LMSacredit = self.LMS_pacients[(id)]['acredit'] if (id) in self.LMS_pacients else None
            LMSfirst = self.LMS_pacients[(id)]['first'] if (id) in self.LMS_pacients else None
            LMSlast = self.LMS_pacients[(id)]['last'] if (id) in self.LMS_pacients else None
            self.upload.append([sector, hash, up, br, desc, ep, uba, ubainf, edat, sexe, cod, complex, ncronic, nac, cobertura, medea,
                vistot2016MF, vispres2016MF, vv2016MF, vvEC2016MF,  vistot2017MF, vispres2017MF, vv2017MF, vvEC2017MF,  vistot2018MF, vispres2018MF, vv2018MF, vvEC2018MF,
                vistot2016INF, vispres2016INF, vv2016INF, vvEC2016INF,  vistot2017INF, vispres2017INF, vv2017INF, vvEC2017INF,  vistot2018INF, vispres2018INF, vv2018INF, vvEC2018INF,
                 iniciprof, inicipac, missatges, str(pacfirst), str(paclast), str(proffirst), str(proflast),LMSacredit, LMSfirst, LMSlast])
                 
    def upload_data(self):
        """."""
        columns = ["codi_sector varchar2(4)","hash varchar2(40)", "up varchar2(5)",   "br varchar2(5)",  "desc_up varchar2(300)", "ep varchar2(10)", 
            "uba varchar2(5)", "ubainf varchar2(5)", "edat number", "sexe varchar2(5)", "gma_cod varchar2(20)", "gma_complex varchar2(20)", "gma_ncronic number",
            "nacionalitat varchar2(5)", "cobertura varchar2(5)", "medea_seccio number", "visites_totals_2016_MF number","visites_presencialss_2016_MF number",
            "virtuals_2016_MF number", "virtuals_econsulta_2016_MF number", "visites_totals_2017_MF number","visites_presencialss_2017_MF number",
             "virtuals_2017_MF number", "virtuals_econsulta_2017_MF number", "visites_totals_2018_MF number","visites_presencialss_2018_MF number",
             "virtuals_2018_MF number", "virtuals_econsulta_2018_MF number","visites_totals_2016_INF number","visites_presencialss_2016_INF number",
            "virtuals_2016_INF number", "virtuals_econsulta_2016_INF number", "visites_totals_2017_INF number","visites_presencialss_2017_INF number",
             "virtuals_2017_INF number", "virtuals_econsulta_2017_INF number", "visites_totals_2018_INF number","visites_presencialss_2018_INF number",
             "virtuals_2018_INF number", "virtuals_econsulta_2018_INF number", "inici_prof number", "inici_pac number", "missatges number",
             "primera_conv_pac varchar2(40)", "ultima_conv_pac varchar2(40)","primera_conv_prof varchar2(40)", "ultima_conv_prof varchar2(40)",
             "LMS_acreditacio varchar(40)", "LMS_primer_acces varchar(40)", "LMS_ultim_acces varchar(40)"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        u.grantSelect(tb, ("PREDUMMP", "PREDUPRP", "PREDUECR"), db)
        
if __name__ == '__main__':
    EConsultaPac()
    