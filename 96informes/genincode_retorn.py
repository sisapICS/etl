import sisapUtils as u
import csv

data_dict = {}

# sql = """SELECT hash_redics, hash_covid FROM dwsisap.PDPTB101_RELACIO"""
# converters = {}
# for redics, covid in u.getAll(sql, 'exadata'):
#     converters[redics] = covid

# patients = []
# sql = """SELECT patient, up, UP_DESC, uba, tabaquisme FROM rcv_genicode"""  
# for e in u.getAll(sql, 'redics'):
#     if e[0] in converters:
#         hash_covid = converters[e[0]]
#         patients.append((hash_covid, e[1], e[2], e[3], e[4])) 

# u.execute('delete from rcv_genicode', 'exadata')
# u.listToTable(patients, 'rcv_genicode', 'exadata')

print('done')


with open("genincode_no_trobats.csv", mode="r") as file:
    reader = csv.reader(file, delimiter=";")
    for row in reader:
        key = row[1][:13]
        values = row
        data_dict[key] = values


print('here')
hash_2_cip = {}
sql = """SELECT hash_covid, cip FROM dwsisap.PDPTB101_RELACIO_nia"""
for hash, cip in u.getAll(sql, 'exadata'):
    if cip in data_dict:
        hash_2_cip[hash] = cip

print('here2')
sql = """SELECT
            c_cip,
            c_up,
            up_desc,
            c_metge,
            uab_descripcio,
            c_sexe,
            C_DATA_NAIX,
            CASE
                WHEN PS_DIABETIS1_DATA IS NOT NULL THEN 'DM1'
                WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 'DM2'
                ELSE 'no'
            END DM,
            v_TAS_VALOR,
            v_TAD_VALOR,
            v_peso_valor,
            v_talla_valor,
            v_col_total_valor,
            v_col_LDL_valor,
            v_col_HDL_valor,
            v_triglic_valor,
            tabaquisme
        FROM dwsisap.dbs_2024 dbs
        INNER JOIN
            dwsisap.rcv_genicode a ON
            dbs.c_cip = a.patient
        INNER JOIN (
            SELECT
                *
            FROM
                dwsisap.PROFESSIONALS
            WHERE
                tipus = 'M') p
                    ON
            dbs.c_metge = p.uab
            AND dbs.c_up = p.up"""
upload = []
for row in u.getAll(sql, 'exadata'):
    if row[0] in hash_2_cip:
        cip = hash_2_cip[row[0]] 
        element = list(data_dict[cip][0:2])
        element = element + list(row[1:])
        upload.append((element))


u.writeCSV('genincode_retorn_12_2_25_no_trobats.csv', [('CODI GENINCODE','CIP','UP','UP_DESC','UAB','UAB_DESC','SEXE','DATA_NAIX','DM','TAS','TAD','PES','TALLA','COLESTEROL_TOTAL','COLESTEROL_LDL','COLESTEROL_HDL','TRIGLICERIDS','TABAQUISME')] + upload, sep=";")




cips_inclosos = set()

with open("GENINCODE1.csv", mode="r") as file:
    reader = csv.reader(file, delimiter=";")
    headers = next(reader)
    
    for row in reader:
        cips_inclosos.add(row[1][:13])


cips_si = set()

with open("genincode_retorn_12_2_25_no_trobats.csv", mode="r") as file:
    reader = csv.reader(file, delimiter=";")
    headers = next(reader)
    
    for row in reader:
        cips_si.add(row[1][:13])

print(cips_inclosos-cips_si)







