# -*- coding: latin1 -*-

"""
.
"""

import datetime as d
import os

import sisapUtils as u


tb = "sisap_visites_diaries"
db = "redics"
us = ("PREDUMMP", "PREDUECR")

sectors = u.sectors
saps = tuple([sap for sap, in u.getAll("select distinct sap_codi \
                                        from cat_centres \
                                        where ep = '0208'", "nodrizas")])
serveis = ('MG', 'URGEN', 'URG')
ini = u.gcal2jd(2017, 12, 1)
fi = u.gcal2jd(2018, 1, 31)


def main():
    """."""
    u.createTable(tb, "(sap_cod varchar2(2), sap_des varchar2(255), \
                        up_cod varchar2(5), up_des varchar2(255), \
                        centre_cod varchar2(10), centre_cla varchar2(2), \
                        centre_des varchar2(255), modul_cod varchar2(5), \
                        modul_des varchar2(255), modul_uab varchar(5), \
                        tipus varchar2(5), data date, forcada varchar2(1))",
                  db, rm=True)
    u.grantSelect(tb, us, db)
    u.multiprocess(Visites, sectors)
    send_mail()


def send_mail():
    """."""
    mails = (("select data, sap_des, up_des, centre_des, modul_cod, count(1) \
               from {} \
               group by data, sap_des, up_des, centre_des, modul_cod".format(tb),
              u.tempFolder + "sisap_visites_diaries.csv",
              [("dia", "sap", "up", "centre", "agenda", "visites")],
              "Visites di�ries",
              "Adjuntem les visites actualitzades."),
             ("SELECT dia, SAP_DES,UP, CENTRE,COUNT(*) AS AGENDES, SUM(MES28),\
               replace(''||round(SUM(MES28)*100/COUNT(*),2),'.',',') \
               AS PORMES28 FROM \
               (SELECT SAP_DES,UP,CENTRE,MODUL,DIA, VISITES,CASE \
               WHEN VISITES >'28'  THEN '1' ELSE '0' END AS MES28 FROM \
               ( SELECT SAP_DES,UP_DES AS UP , CENTRE_DES AS CENTRE, \
               MODUL_COD AS MODUL, DATA AS DIA,COUNT (*) AS VISITES FROM \
               ( select * from  PREDUFFA.SISAP_VISITES_DIARIES \
               WHERE MODUL_UAb IS NOT NULL AND TIPUS IN ('9C','9R','9D')) \
               GROUP BY  SAP_DES,UP_DES, CENTRE_DES,MODUL_COD,DATA)) \
               GROUP BY dia,SAP_DES,UP,CENTRE",
              u.tempFolder + "sisap_agendes_diaries.csv",
              [("dia", "sap", "up", "centre", "agendes", "mes28", "%mes28")],
              "Agendes di�ries",
              "Adjuntem les agendes actualitzades."))
    for mail in mails:
        __send_mail(*mail)


def __send_mail(sql, file, header, subject, text):
    """."""
    dades = [row for row in u.getAll(sql, db)]
    u.writeCSV(file, header + sorted(dades), sep=";")
    u.sendGeneral("SISAP <sisap@gencat.cat>",
                  ("direcciogerencia@gencat.cat", "mjvazquez@gencat.cat",
                   "esadurni@gencat.cat", "eva.ps@gencat.cat"),
                  ("rmmorral@gencat.cat", "cpareja@gencat.cat",
                   "mmedinap@gencat.cat", "ecomaredon@gencat.cat",
                   "ffinaaviles@gencat.cat"),
                  subject, text, file)
    os.remove(file)


class Visites(object):
    """."""

    def __init__(self, sector):
        """."""
        self.db = (sector + "sb") if d.datetime.now().hour < 7 else sector
        self.get_saps()
        self.get_ups()
        self.get_centres()
        self.get_moduls()
        self.get_visites()
        self.upload_data()

    def get_saps(self):
        """."""
        sql = "select dap_codi_dap, dap_desc_dap from gcctb006"
        self.saps = {row[0]: row[1] for row in u.getAll(sql, self.db)}

    def get_ups(self):
        """."""
        sql = "select u_pr_cod_up, u_pr_descripcio from pritb008"
        self.ups = {row[0]: row[1] for row in u.getAll(sql, self.db)}

    def get_centres(self):
        """."""
        sql = "select cent_codi_centre, cent_classe_centre, cent_nom_centre \
               from pritb010"
        self.centres = {row[:2]: row[2] for row in u.getAll(sql, self.db)}

    def get_moduls(self):
        """."""
        sql = "select modu_centre_codi_centre, modu_centre_classe_centre, \
                      modu_servei_codi_servei, modu_codi_modul, \
                      modu_descripcio, modu_codi_uab \
               from vistb027"
        self.moduls = {row[:4]: row[4:] for row in u.getAll(sql, self.db)}

    def get_visites(self):
        """."""
        sql = "select visi_centre_codi_centre, visi_centre_classe_centre, \
                      visi_servei_codi_servei, visi_modul_codi_modul, \
                      visi_sap, visi_up, visi_tipus_visita, \
                      to_date(visi_data_visita, 'J'), visi_forcada_s_n \
               from vistb043 \
               where visi_data_visita between {} and {} and \
                     visi_situacio_visita = 'R' and \
                     visi_sap in {} and \
                     visi_servei_codi_servei in {}".format(ini, fi,
                                                           saps, serveis)
        self.dades = [(row[4], self.saps[row[4]],
                       row[5], self.ups[row[5]],
                       row[0], row[1], self.centres[row[:2]],
                       row[3]) + self.moduls[row[:4]] + row[6:]
                      for row in u.getAll(sql, self.db)]

    def upload_data(self):
        """."""
        u.listToTable(self.dades, tb, db)


if __name__ == "__main__":
    main()
