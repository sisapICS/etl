# coding: iso-8859-1
#Carolina Guiriguet, marc 2017, Ermengol


from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

nod = 'nodrizas'
imp = 'import'

f_hta = []


sql = "select codi from cat_dbscat where agrupador like 'F_HTA%'"
for atc, in getAll(sql, imp):
    f_hta.append(atc)

antihip = {}
sql = "select id_cip_sec from tractaments, nodrizas.dextraccio where ppfmc_pmc_data_ini<=data_ext and ppfmc_data_fi > data_ext and pf_cod_atc in {}".format(str(tuple(f_hta)))
for id, in getAll(sql, imp):
    antihip[id] = True
    
sql = "select id_cip_sec from tractaments, nodrizas.dextraccio where ppfmc_pmc_data_ini<=data_ext and ppfmc_data_fi > data_ext and pf_cod_atc in {}".format(str(tuple(f_hta)))
for id, in getAll(sql, 'import_jail'):
    antihip[id] = True
    
aag, aag_aco, betabloq, hipolip = {}, {}, {}, {}
sql = 'select id_cip_sec, farmac from eqa_tractaments where farmac in (4,5,3,6,57,82)'
for id, farmac in getAll(sql, nod):
    if farmac in (4, 5):
        aag[id] = True
        aag_aco[id] = True
    elif farmac in (3, 6):
        aag_aco[id] = True
    elif farmac == 57:
        betabloq[id] = True
    elif farmac == 82:
        hipolip[id] = True
    else:
        print farmac

acxfa, avc_ci, ci, hta, dislip = {}, {}, {}, {}, {}
sql = 'select id_cip_sec, ps from eqa_problemes where ps in (180, 7, 1, 55, 47)'
for id, ps in getAll(sql, nod):
    if ps == 180:
        acxfa[id] = True
    elif ps == 1:
        ci[id] = True
        avc_ci[id] = True
    elif ps == 7:
        avc_ci[id] = True
    elif ps == 55:
        hta[id] = True
    elif ps == 47:
        dislip[id] = True
  
total, tacxfa, tavc_ci, tci, thta, tdislip = {}, {}, {}, {}, {}, {}  
sql = 'select id_cip_sec, up, edat from assignada_tot_with_jail where ates=1'
for id, up, ed in getAll(sql, nod):
    edat = 'NO'
    if 18<= ed <= 34:
        edat = 'Entre 18 i 34 anys'
    elif 35<= ed <= 54:
        edat = 'Entre 35 i 54 anys'
    elif ed >54:
        edat = 'mes de 54 anys'
    if edat == 'NO':
        continue
    jail = 0
    if id <0:
        jail = 1
    raag, raag_aco, rbetabloq, rhipolip, rantihip = 0, 0, 0, 0, 0
    if id in aag:
        raag = 1
    if id in aag_aco:
        raag_aco=1
    if id in betabloq:
        rbetabloq = 1
    if id in hipolip:
        rhipolip = 1
    if id in antihip:
        rantihip = 1
    if (up, edat, jail) in total:
        total[(up, edat, jail)]['pob'] += 1
        total[(up, edat, jail)]['aag'] += raag
        total[(up, edat, jail)]['aag_aco'] += raag_aco
        total[(up, edat, jail)]['betabloq'] += rbetabloq
        total[(up, edat, jail)]['hipolip'] += rhipolip
        total[(up, edat, jail)]['antihip'] += rantihip
    else:
        total[(up, edat, jail)] = { 'pob': 1, 'aag': raag, 'aag_aco': raag_aco,'betabloq': rbetabloq, 'hipolip':rhipolip, 'antihip': rantihip }
    if id in acxfa:
        if (up, edat, jail) in tacxfa:
            tacxfa[(up, edat, jail)]['n'] += 1
            tacxfa[(up, edat, jail)]['aag_aco'] += raag_aco
        else:
            tacxfa[(up, edat, jail)] = {'n': 1, 'aag_aco': raag_aco}
    if id in avc_ci:
        if (up, edat, jail) in tavc_ci:
            tavc_ci[(up, edat, jail)]['n'] += 1
            tavc_ci[(up, edat, jail)]['aag'] += raag
        else:
            tavc_ci[(up, edat, jail)] = {'n': 1, 'aag': raag}
    if id in ci:
        if (up, edat, jail) in tci:
            tci[(up, edat, jail)]['n'] += 1
            tci[(up, edat, jail)]['betabloq'] += rbetabloq
        else:
            tci[(up, edat, jail)] = {'n': 1, 'betabloq': rbetabloq}
    if id in hta:
        if (up, edat, jail) in thta:
            thta[(up, edat, jail)]['n'] += 1
            thta[(up, edat, jail)]['antihip'] += rantihip
        else:
            thta[(up, edat, jail)] = {'n': 1, 'antihip': rantihip}
    if id in dislip:
        if (up, edat, jail) in tdislip:
            tdislip[(up, edat, jail)]['n'] += 1
            tdislip[(up, edat, jail)]['hipolip'] += rhipolip
        else:
            tdislip[(up, edat, jail)] = {'n': 1, 'hipolip': rhipolip}
        
upload = []
for (up, edat, jail), v in total.items():
    upload.append([up, edat, jail, v['pob'], v['aag'], v['aag_aco'], v['betabloq'], v['hipolip'], v['antihip']])
file = tempFolder + 'Tractaments_totals.txt'
writeCSV(file, upload, sep=';')

upload = []
for (up, edat, jail), v in tacxfa.items():
    upload.append([up, edat, jail, v['n'], v['aag_aco']])
file = tempFolder + 'Tractaments_acfa.txt'
writeCSV(file, upload, sep=';')

upload = []
for (up, edat, jail), v in tavc_ci.items():
    upload.append([up, edat, jail, v['n'], v['aag']])
file = tempFolder + 'Tractaments_AVC_CI.txt'
writeCSV(file, upload, sep=';')

upload = []
for (up, edat, jail), v in tci.items():
    upload.append([up, edat, jail, v['n'], v['betabloq']])
file = tempFolder + 'Tractaments_CI.txt'
writeCSV(file, upload, sep=';')

upload = []
for (up, edat, jail), v in thta.items():
    upload.append([up, edat, jail, v['n'], v['antihip']])
file = tempFolder + 'Tractaments_HTA.txt'
writeCSV(file, upload, sep=';')

upload = []
for (up, edat, jail), v in tdislip.items():
    upload.append([up, edat, jail, v['n'], v['hipolip']])
file = tempFolder + 'Tractaments_dislip.txt'
writeCSV(file, upload, sep=';')

