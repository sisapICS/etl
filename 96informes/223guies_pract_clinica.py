# coding: utf8

"""
Ús de guies de pràctica clínica
No mirem la guia pel codi 0, ja que no té denominador
"""

import sys,datetime

import sisapUtils as u

#Conversió de usua_cip a hash
def getCipToHash():
    #cip_ant=getCipAnt()
    sql = 'select usua_cip, usua_cip_cod from pdptb101'
    return {cip: hash for cip, hash in u.getAll(sql, 'pdp') }
        
#conversió de hash a id_cip_sec    
def getHashToId():
    hashId={}
    for id, hash_d in u.getAll("select id_cip_sec, hash_d from import.u11", "import"):
        hashId[hash_d]=id
    return hashId

#sectors=['6734']

class gpc(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.pat_prev={}
        self.pats_guies = {}
        self.ids_gpc={1: set(), 2: set(), 3: set(), 4: set(), 5: set(), 6: set(), 7: set()}
        self.id_pato={1: set(), 2: set(), 3: set(), 4: set(), 5: set(), 6: set(), 7: set()}
        self.guia = [1,2,3,4,5,6,7]

        self.get_ass()
        self.get_probs_gpc()
        self.get_visitats_2019()
        self.get_problemes_visitats_2019()
        self.get_ids_gpc()
        self.get_res()
        self.export_data()

    def get_ass(self):
        """
        Obtenim assignada a ICS
        """
        self.id=set()
        sql = "select id_cip_sec from assignada_tot"
        for id, in u.getAll(sql, 'nodrizas'):
            self.id.add(id)
        print "id's saved"
   
    def get_probs_gpc(self):
        """Obtenim llista de codis de patologies prevalents per gpc
        """
        print "llista de pats. prevalents"
        pat_prev = {1: set(), 2: set(), 3: set(), 4: set(), 5: set(), 6: set(), 7: set()}
        resultat = u.multiprocess(SectorProbs, u.sectors)
        for sector in resultat:            
            for i in self.guia:
                pat_prev[i]=pat_prev[i].union(sector.pat_prev[i]) 
        pats_guies={}
        for i in self.guia:
            for element in pat_prev[i]:
                if element not in pats_guies:
                    pats_guies[element]=set()
                    pats_guies[element].add(i)
                else:
                    pats_guies[element].add(i)
        self.pats_guies=pats_guies
        self.pat_prev=pat_prev
        #u.writeCSV(u.tempFolder + 'id.csv', self.id) 
        #print "imprimir patologies prevalents per guia"
        #u.writeCSV(u.tempFolder + 'pat_prev.csv', self.pat_prev)
        print "imprimir relació de patologies i guies"
        print(self.pats_guies)
   
    def get_visitats_2019(self):
        """Obtenim id's dels individus assignats a l'ICS visitats el 2019"""
        print "inici get_visitats"
        self.id_visitats=set()
        i=0
        sql = "select id_cip_sec from visites where year(visi_data_visita)=2019"
        for id, in u.getAll(sql, 'import'):
            self.id_visitats.add(id)
            i+=1
            if(i%100000==0):
                print(i)
        print(len(self.id_visitats))
        
    def get_problemes_visitats_2019(self):
        """Obtenim els id's dels pacients amb les patologies visitats el 2019"""
        print "inici get_pato"
        i=0
        data_ini=datetime.datetime.strptime('2019-01-01','%Y-%m-%d').date()
        sql = "select id_cip_sec, pr_cod_ps, pr_dba from problemes"
        for id, codi, data_baixa in u.getAll(sql, 'import'):
            if data_baixa is None or data_baixa >= data_ini:
                if id in self.id_visitats and codi in self.pats_guies:
                    i+=1
                    if(i%100000==0):
                        print(i)
                    for guia in self.pats_guies[codi]:
                        self.id_pato[guia].add(id)
                    #print id, codi, data_baixa
        for i in self.guia:
            print(len(self.id_pato[i]))
            print "primers 5 elements de la llista"
            print(list(self.id_pato[i])[1:5])  
    
    def get_ids_gpc(self):
        """Obtenim id's de pacients per guia amb les patologies de gpc 
        """
        print "llista de pats. prevalents"
        ids_gpc = {1: set(), 2: set(), 3: set(), 4: set(), 5: set(), 6: set(), 7: set()}
        resultat = u.multiprocess(SectorIds, u.sectors)
        for sector in resultat:            
            for i in self.guia:
                ids_gpc[i]=ids_gpc[i].union(sector.id_gpc[i])
        #Creem un diccionari per convertir de usua_cip a id_cip_sec
        print "Creació diccionaris usua_cip a id_cip_sec"
        hash_to_id = getHashToId()
        usua_cip_to_hash = getCipToHash() 
        print "Diccionaris creats"
        
        j=0
        no_ids=0
        self.ids_gpc=ids_gpc
        self.ids_aux={1: set(), 2: set(), 3: set(), 4: set(), 5: set(), 6: set(), 7: set()}
        for i in self.guia:
            for usua_cip in self.ids_gpc[i]:
                j+=1
                if(j%100000==0):
                    print(j)
                if usua_cip in usua_cip_to_hash:
                    if usua_cip_to_hash[usua_cip] in hash_to_id: 
                        self.ids_aux[i].add(hash_to_id[usua_cip_to_hash[usua_cip]])
                    else:
                        no_ids+=1
                else:
                    no_ids+=1
        print "usua_cip sense correspondència"
        print(no_ids)
        for i in self.guia:
            print "número en gpc"
            print(len(self.ids_aux[i]))
            self.ids_aux[i] = self.ids_aux[i].intersection(self.id_pato[i])
            print "número en gpc en denom"
            print(len(self.ids_aux[i]))
        self.ids_gpc=self.ids_aux
        #self.ids_gpc=ids_gpc 
    
    def get_res(self):
        """Crear vector resultat amb num's i den's"""
        self.res={}
        for i in self.guia:
            self.res[i] = len(self.ids_gpc[i]), len(self.id_pato[i]), \
                     len(self.ids_gpc[i])/float(len(self.id_pato[i])) 
        print(self.res)
             
    def export_data(self):
        """."""
        u.writeCSV(u.tempFolder + 'res.csv', self.res.items()) 
        #u.writeCSV(u.tempFolder + 'id_pato.csv', self.id_pato)
        #u.writeCSV(u.tempFolder + 'id_visitats.csv', self.id_visitats)

class SectorProbs(object):
    """Procés a executar per sector, per treure llista malalties"""
    def __init__(self, sector):
        """."""
        self.sector = sector
        self.get_codis()
        print self.sector
        return

    def get_codis(self):
        """Mirem a cada sector les malaties per guia"""
        self.pat_prev = {0: set(), 1: set(), 2: set(), 3: set(), 4: set(), 5: set(), 6: set(), 7: set()}
        sql = "select dx_pscod, dx_gpccodi from gpctb003"
        for codi, guia in u.getAll(sql, self.sector):
            self.pat_prev[guia].add(codi)
            
class SectorIds(object):
    """Procés a executar per sector, per treure ids malalts amb gpc"""
    def __init__(self, sector):
        """."""
        self.sector = sector
        self.get_ids()
        print self.sector
        return

    def get_ids(self):
        """Mirem a cada sector els id's dels pacients per guia"""
        self.id_gpc = {1: set(), 2: set(), 3: set(), 4: set(), 5: set(), 6: set(), 7: set()}
        sql = "select hrec_usuacip, hrec_gpccodi, hrec_datacreacio from gpctb021 \
               WHERE extract(YEAR from hrec_datacreacio)=2019"
        for usua_cip, guia, data in u.getAll(sql, self.sector):
            self.id_gpc[guia].add(usua_cip)
   
if __name__ == '__main__':
    gpc()

# de cadascuna de les guies existents:
# den: número de persones visitades l'últim any amb la patologia prevalent
# num: número de persones a qui s'ha usat la GPC de l'ECAP durant l'any
# període: 2019

# definicions:
# - persones assignades a l'ICS
# - guies: tens el catàleg a GPCTB001
# - patologia prevalent: els codis que activen cada guia aquí: GPCTB003, ho mirem de problemes de salut
# - persones a qui s'ha usat la GPC es treu de la taula gpctb021, usant la variable hrec_datacreacio i identifcant quina guia s'ha 'tocat'
    