# -*- coding: utf-8 -*-

# Indicadors resid�ncies i hospitalitzacions

import collections as c
import sisapUtils as u
import sisaptools as t
import datetime as d

from dateutil.relativedelta import relativedelta
db = 'altres'
class Alzheimer():
    def __init__(self):
        self.idcipsec2hash()
        u.printTime('idcipsec2hash')
        self.get_demo()
        u.printTime('get_demo')
        self.get_clinic()
        '''u.printTime('get_clinic')
        self.get_farmacs()
        u.printTime('get_farmacs')
        self.get_hospitalitzacions()
        u.printTime('get_hospitalitzacions')'''
    
    def idcipsec2hash(self):
        self.pacients = {}
        self.usua_cip = {}
        self.hash_a = {}
        self.nias = {}
        sql = """
        SELECT id_cip_sec, hash_d from u11
        """
        # volem relaci� de id_cip_sec a nia
        for id_cip_sec, hash_d in u.getAll(sql, "import"):
            self.pacients[hash_d] = id_cip_sec
        print('despres 1r sql')
        sql = """
        SELECT * FROM pdptb101
        """
        for cip, usua_cip_codi in u.getAll(sql, "pdp"):
            if usua_cip_codi in self.pacients:
                self.usua_cip[cip] = usua_cip_codi
        print('despres 2n sql')
        sql = """
            SELECT cip, nia FROM dwsisap.RCA_CIP_NIA
        """
        
        for cip, nia in u.getAll(sql, 'exadata'):
            if cip[:13] in self.usua_cip:
                # tenim en un mateix diccionari nia i id_cip_sec
                id_cip_sec = self.pacients[self.usua_cip[cip[:13]]]
                self.hash_a[nia] = id_cip_sec
                self.nias[id_cip_sec] = nia
        u.printTime('despres 3r sql')

    def get_demo(self):
        #es pot fer tot amb id_cip_sec i canviar nom�s en el moment en qu� es puja a les taules
        #self.nias = list(self.hash_a.keys())
        #self.ids = list(self.hash_a.values())
        self.diagnostic = {}
        dates_d = {}
        self.demografia = c.defaultdict(list)
        edats = {}
        self.any_diagnostic = {}
        ups = c.defaultdict(list)
        viu_sol = {}
        sql = "select id_cip_sec, year(dde), dde, ps, edat from eqa_problemes where ps in (86, 1007)"
        u.printTime('abans sql')
        for (id_cip_sec, any_diagnostic, data_diagnostic, diagnostic, edat) in u.getAll(sql, 'nodrizas'):
            self.diagnostic[(id_cip_sec, diagnostic)] = (any_diagnostic, data_diagnostic)
            edats[id_cip_sec] = edat
        u.printTime('despres 1r sql')
        sql = """
                SELECT
                    id_cip_sec,
                    codi_sector,
                    up,
                    upinf,
                    uba,
                    ubainf,
                    edat,
                    sexe,
                    ates,
                    maca,
                    institucionalitzat
                FROM
                    assignada_tot
                WHERE
                    edat >= 15
            """
        poblacio = c.defaultdict(dict)
        for id_cip_sec, sector, up, upinf, uba, ubainf, edat, sexe, ates, maca, institucionalitzat in u.getAll(sql, "nodrizas"):
            if id_cip_sec in edats:
                poblacio[id_cip_sec] = {"up": up,
                                        "edat": edat,
                                        "sexe": sexe,
                                        "ates": ates,
                                        "maca": maca,
                                        "institucionalitzat": institucionalitzat}
        for (id_cip_sec, data) in poblacio.items():
                #print(id_cip_sec)
                if data["ates"] == 1:
                    insat = data["institucionalitzat"]
                else:
                    insat = None
                #print(insat)
                self.demografia[id_cip_sec] = [data["edat"], data["sexe"], insat, data["institucionalitzat"]]
                #print(self.demografia[id_cip_sec])
                ups[data["up"]].append(id_cip_sec)
                #print(ups[up])
        u.printTime('despres 2n sql')
        sql = "select medea, scs_codi from cat_centres"
        for medea, codi in u.getAll(sql, 'nodrizas'):
            if codi in ups:
                for id_cip_sec in ups[codi]:
                    self.demografia[id_cip_sec].append(medea)
        u.printTime('despres 3r sql')
        sql = "select id_cip_sec from eqa_problemes where ps = 107"
        for id_cip_sec in u.getAll(sql, 'nodrizas'):
            viu_sol[id_cip_sec] = 1
        for id_cip_sec in self.demografia:
            if id_cip_sec in viu_sol:
                self.demografia[id_cip_sec].append(1)
            else:
                self.demografia[id_cip_sec].append(0)
        u.printTime('despres 4t sql')
        for (id_cip_sec, diagnostic), (any_diagnostic, data_diagnostic) in self.diagnostic.items():
            if id_cip_sec not in self.any_diagnostic:
                self.any_diagnostic[id_cip_sec] = any_diagnostic
                dates_d[id_cip_sec] = data_diagnostic
            elif any_diagnostic < self.any_diagnostic[id_cip_sec]:
                self.any_diagnostic[id_cip_sec] = any_diagnostic
                dates_d[id_cip_sec] = data_diagnostic
        upload = []
        u.printTime('abans bucle')
        for id_cip_sec in self.demografia:
            if id_cip_sec in self.nias:
                v = self.demografia[id_cip_sec]
                upload.append((self.nias[id_cip_sec], v[0], v[1], v[2], v[3], v[4], v[5], dates_d[id_cip_sec]))
        print('despres bucle')
        #upload = [(k, v[0], v[1], v[2], v[3], v[4], v[5], self.any_diagnostic[k]) for (k,v) in self.demografia.items()]
        #upload t� (id_cip_sec, edat, sexe, insat, insass, medea, viu_sol)
        #upload = [(k, v[0], v[1], v[2], v[3]) for (k,v) in self.demografia.items()]
        cols = '(nia int(38), edat int(11), sexe varchar(1), insat int, insass int, se_zona varchar(2), viu_sol int, data_diagnostic date)'
        u.createTable('dades_demo_alzheimer', cols, 'altres', rm=True)
        u.printTime('despres crear taula')
        u.listToTable(upload, 'dades_demo_alzheimer', db)
        u.printTime('despres list to table')

    def get_clinic(self):
        dates = {}
        clinic = {}
        upload = []
        farmacs = c.defaultdict(list)
        tipus = {
            55: "Hipertensi�",
            16: "PAS",
            17: "PAD",
            9: "LDL",
            422: "HDL",
            31: "triglic�rids",
            18: "Diabetes 1",
            24: "Diabetes 2",
            20: "Hemoglobina 1AC",
            425: "Depressi�",
            486: "Ansietat",
            968: "Ansietat",
            998: "Tabac",
            84: "Alcohol",
            85: "Alcohol",
            10: "Regicor",
            19: "IMC"
        }

        sql = "SELECT id_cip_sec, agrupador, data_var as data_var, year(data_var), valor, usar FROM eqa_variables WHERE agrupador IN (55,18,24,425,486,998,84,85,10,19,16,17,9,422,31,20)"
        for id_cip_sec, agrupador, data_prova, any_prova, valor, usar in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.demografia:
                farmacs[id_cip_sec].append((agrupador, data_prova, any_prova, valor, usar))
        u.printTime('despres sql')
        for (id_cip_sec, data_arr) in farmacs.items():
            for (agrupador, data_var, any_var, valor, usar) in data_arr:
                if agrupador in (55,18,24,425,486,998,84,85, 10, 19) and usar == 1 and id_cip_sec in self.nias:
                    upload.append((self.nias[id_cip_sec], tipus[agrupador], data_var, valor))
                elif any_var >= self.any_diagnostic[id_cip_sec]-3:
                    if (id_cip_sec, tipus[agrupador], any_var) not in dates:
                        dates[(id_cip_sec, tipus[agrupador], any_var)] = data_var

                    if data_var < dates[(id_cip_sec, tipus[agrupador], any_var)]:
                        dates[(id_cip_sec, tipus[agrupador], any_var)] = data_var

                    if (id_cip_sec, tipus[agrupador]) not in clinic:
                        clinic[(id_cip_sec, tipus[agrupador])] = []

                    clinic[(id_cip_sec, tipus[agrupador])].append((data_var, valor))
        u.printTime('despres processament')
        for (id_cip_sec, tipus[agrupador], any_var), data in dates.items():
            for (data_var, valor) in clinic[(id_cip_sec, tipus[agrupador])]:
                if data == data_var and id_cip_sec in self.nias:
                    upload.append((self.nias[id_cip_sec], tipus[agrupador], data_var, valor))
        
        cols = '(nia int(38), tipus varchar(20), data date, valor double)'
        u.createTable('dades_cliniques_alzheimer', cols, 'altres', rm=True)
        u.listToTable(upload, 'dades_cliniques_alzheimer', db)
    
    def get_farmacs(self):
        upload = []
        tipus = {
            473: "depressi�",
            474: "depressi�",
            900: "depressi�",
            901: "depressi�",
            898: "depressi�",
            899: "depressi�",
            786: "depressi�",
            801: "ansietat",
            802: "ansietat",
            738: "pressi� arterial",
            749: "pressi� arterial",
            656: "pressi� arterial",
            657: "pressi� arterial",
            57: "pressi� arterial",
            56: "pressi� arterial",
            967: "pressi� arterial",
            955: "pressi� arterial",
            735: "pressi� arterial",
            231: "pressi� arterial",
            86: "colesterol",
            216: "colesterol",
            658: "colesterol",
            659: "colesterol",
            22: "insulina i antidiab�tics",
            23: "insulina i antidiab�tics",
            445: "insulina i antidiab�tics",
            592: "insulina i antidiab�tics",
            850: "insulina i antidiab�tics",
            1005: "insulina i antidiab�tics",
            1006: "insulina i antidiab�tics",
            589: "insulina i antidiab�tics",
            175: "insulina i antidiab�tics",
            588: "insulina i antidiab�tics",
            413: "insulina i antidiab�tics",
            587: "insulina i antidiab�tics",
            412: "insulina i antidiab�tics",
            954: "insulina i antidiab�tics",
            173: "insulina i antidiab�tics",
            206: "insulina i antidiab�tics",
            414: "insulina i antidiab�tics",
            174: "insulina i antidiab�tics",
            207: "insulina i antidiab�tics"
        }
        sql = "select id_cip_sec, farmac, pres_orig, year(pres_orig) from eqa_tractaments where farmac in (473,474,898,786,801,802,738,749,656,657,57,56,955,735,231,86,658,22,445,592,850,1005,1006,589,175,588,413,587,412,954,173,414,174)"
        for (id_cip_sec, farmac, data_pres, pres_orig) in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.any_diagnostic and pres_orig == self.any_diagnostic[id_cip_sec] and id_cip_sec in self.nias:
                upload.append((self.nias[id_cip_sec], tipus[farmac], data_pres))
        print('despres sql')
        farmacs_demencia = ['N06DA03', 'N06DA02', 'N06DA04', 'N06DX01']
        sql = "select id_cip_sec, ppfmc_pmc_data_ini, year(ppfmc_pmc_data_ini) from tractaments where ppfmc_atccodi in {}".format(tuple(farmacs_demencia))
        for (id_cip_sec, data_pres, pres_orig) in u.getAll(sql, "import"):
            if id_cip_sec in self.any_diagnostic and pres_orig == self.any_diagnostic[id_cip_sec] and id_cip_sec in self.nias:
                upload.append((self.nias[id_cip_sec], 'dem�ncia', data_pres))
        cols = '(nia int(38), farmac varchar(20), data_presc date)'
        u.createTable('dades_farmacs_alzheimer', cols, 'altres', rm=True)
        u.listToTable(upload, 'dades_farmacs_alzheimer', db)
    
    def get_hospitalitzacions(self):
        # dades han de tenir: data ingr�s, data alta, diagnostic principal, procediment principal
        # mirar nom�s data ingr�s com a m�nim 3 anys abans del diagn�stic d'Alzheimer
        hospitalitzacions = {}
        upload = []
        
        sql = """
        SELECT nia, data_ingres, data_alta, DP, PP from dwcatsalut.TF_CMBDHA
        """
        for nia, ingres, alta, diagnostic, procediment in u.getAll(sql, "exadata"):
            if nia in self.hash_a:
                id_cip_sec = self.hash_a[nia]
                if id_cip_sec and id_cip_sec in self.any_diagnostic and ingres and ingres.year >= self.any_diagnostic[id_cip_sec] - 3:
                    if nia not in hospitalitzacions:
                        hospitalitzacions[nia] = [(ingres, alta, diagnostic, procediment)]
                    else:
                        hospitalitzacions[nia].append((ingres, alta, diagnostic, procediment))
        for nia, values in hospitalitzacions.items():
            for (ingres, alta, diagnostic, proc) in values:
                upload.append((nia, ingres, alta, diagnostic, proc))
        print('despres sql final')
        cols = '(nia int(38), data_ingres date, data_alta date, diagnostic varchar(255),procediment varchar(255))'
        u.createTable('dades_hosp_alzheimer', cols, 'altres', rm=True)
        u.listToTable(upload, 'dades_hosp_alzheimer', db)
        
if __name__ == "__main__":
    u.printTime('Inici')
    Alzheimer()
    u.printTime('Fi')