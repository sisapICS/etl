# -*- coding: utf8 -*-

import sisapUtils as u
import datetime as d
import os

sql = """SELECT
            c.sisap_class_des,
            a.residencia,
            b.des,
            D.abs_cod,
            D.abs_des,
            D.sap_cod,
            D.sap_des,
            D.AMBIT_COD,
            D.ambit_des,
            D.regio_cod,
            D.regio_des,
            a.perfil,
            round(100 * sum(DIA_VACUNATS_4) / sum(dia_poblacio), 1) covid,
            round(100 * sum(DIA_vag) / sum(dia_poblacio), 1) grip
        FROM
            preduffa.sisap_covid_res_web_resum a
        INNER JOIN preduffa.sisap_covid_cat_residencia b ON
            a.RESIDENCIA = b.COD
        INNER JOIN preduffa.sisap_covid_cat_residencia_tip c ON
            b.TIPUS = c.SISAP_CLASS
        INNER JOIN (
            SELECT
                DISTINCT abs_cod,
                abs_des,
                sap_cod,
                sap_des,
                AMBIT_COD,
                ambit_des,
                regio_cod,
                regio_des
            FROM
                SISAP_COVID_DBC_CENTRES) d ON
            b.ABS = D.abs_cod
        WHERE
            DATA = (
            SELECT
                max(DATA)
            FROM
                PREDUFFA.sisap_covid_res_web_resum)
            AND PERFIL = 'Resident'
        GROUP BY
            c.SISAP_CLASS_des,
            a.RESIDENCIA ,
            b.DES ,
            D.abs_cod,
            D.abs_des,
            D.sap_cod,
            D.sap_des,
            D.AMBIT_COD,
            D.ambit_des,
            D.regio_cod,
            D.regio_des,
            a.PERFIL"""
upload = []
for row in u.getAll(sql, 'redics'):
    upload.append(row)

SMS_DIA = d.datetime.now().date()
file_eap = u.tempFolder + 'Seguiment_coberures_vacunals_residencies_{}.csv'.format(SMS_DIA)
u.writeCSV(file_eap, [('CLASSE', 'RESIDENCIA', 'DESCRIPCIO', 'ABS', 'DESC_ABS', 'SAP', 'DESC_SAP', 
                'AMBIT', 'DESC_AMBIT', 'REGIO', 'DESC_REGIO', 'PERFIL', 'COVID', 'GRIP')] 
            + upload, sep=";")

subject = 'Seguiment cobertures vacunals residències'
text = "Bon dia.\n\nAdjuntem arxiu .csv amb les dades de seguiment setmanals de cobertures vacunals en residències.\n"
text += "\n\nSalutacions."

u.sendGeneral('SISAP <sisap@gencat.cat>',
                    ('jccontel@gencat.cat'),
                    ('roser.cantenys@catsalut.cat'),
                    subject,
                    text,
                    file_eap)