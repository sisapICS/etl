# -*- coding: utf8 -*-

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *

nod = "nodrizas"
imp = "import"

dext, = getOne('select data_ext from dextraccio', nod)

centres = {}

sql = "select scs_codi, ics_desc from cat_centres where ep='0208'"
for up, desc in getAll(sql, nod):
    centres[up] = desc
    
obesitat_ps = {}
sql = 'select id_cip_sec from eqa_problemes where ps=239'
for id, in getAll(sql, nod):
    obesitat_ps[id] = True
    
obesitat_morbida_ps = {}
sql = "select  id_cip_sec from problemes where pr_cod_ps = 'E66.8' and pr_th in ('5349','5690')\
        and pr_cod_o_ps='C' and pr_hist=1 and pr_dde<='{0}' and (pr_data_baixa is null or pr_data_baixa>'{0}') and (pr_dba is null or pr_dba>'{0}')".format(dext)
for id, in getAll(sql, imp):
    obesitat_morbida_ps[id] = True
    
obesitat_IMC, obesitat_morbida_IMC = {}, {}

sql = 'select id_cip_sec, valor from eqa_variables where agrupador=19 and usar=1'
for id, valor in getAll(sql, nod):
    if valor > 30:
        obesitat_IMC[id] = True
    if valor >40:
        obesitat_morbida_IMC[id] = True
        
recomptes = Counter()

sql = 'select id_cip_sec, ates, up from assignada_tot where edat>14'
for id, ates, up in getAll(sql, nod):
    if up in centres:
        desc = centres[up]
        ob, obm = 0, 0
        if id in obesitat_IMC:
            ob = 1
        if id in obesitat_ps :
            ob = 1
        if id in obesitat_morbida_IMC:
            obm = 1
        if id in obesitat_morbida_ps:
            obm = 1
        recomptes[(up, desc, ates, ob, obm)] += 1
 
upload = []
for (up, desc, ates, ob, obm), r in recomptes.items():
    upload.append([up, desc, ates, ob, obm, r])
    
file = tempFolder + 'Obesitat.txt'
writeCSV(file, upload, sep=';') 

       