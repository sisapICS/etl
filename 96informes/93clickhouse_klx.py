import datetime
import urllib2

import sisapUtils as u


tb = 'klx_uln_p004_cells'
db = 'khalix'
engine = 'Memory'


def upload():
    query = 'INSERT INTO {} FORMAT TabSeparated'.format(tb)
    stm = 'http://10.80.217.84:8123?query={}'.format(query).replace(' ', '%20')
    response = urllib2.urlopen(stm, '\n'.join(dades))


def print_info():
    print i, j, datetime.datetime.now()


def send_query(query):
    stm = 'http://10.80.217.84:8123?query={}'.format(query.replace(' ', '%20'))
    response = urllib2.urlopen(stm)
    return response.read()

'''
for stm in (
    'DROP TABLE IF EXISTS {}'.format(tb),
    'CREATE TABLE {} (ind String, mes String, br String, analisis String, \
                      edat String, pob String, sex String, sap String, \
                      amb String, ep String, val Float32) \
     ENGINE = {}'.format(tb, engine),
):
    response = urllib2.urlopen('http://10.80.217.84:8123', stm)


centres = {br: [sap, amb, ep] for (br, sap, amb, ep)
           in u.getAll('select ics_codi, sap_codi, amb_codi, ep \
                        from cat_centres', 'nodrizas')}
cataleg = {(dim, sym): des for (dim, sym, des)
           in u.getAll('select dim_index, sym_index, sym_name \
                        from klx_master_symbol', db)}

i = 0
j = 0
dades = []
print_info()
for row in u.getAll('select * from klx_uln_p004_cells', db):
    i += 1
    if i % 10**6 == 0:
        print_info()
    if cataleg[(2, row[2])] in centres:
        dades.append('\t'.join(map(str,
                                   [cataleg[(k, row[k])] for k in range(7)] +
                                   centres[cataleg[(2, row[2])]] +
                                   [row[7]])))
        j += 1
        if j % 10**6 == 0:
            upload()
            dades = []
upload()
print_info()
'''

for query in ("show create table {}",
              "select count() from {}",
              "select amb, sum(val), count(), count(distinct edat) \
               from {} where ind = 'EQA0201A' group by amb"):
    print send_query(query.format(tb))
