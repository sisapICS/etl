from sisapUtils import getAll, writeCSV, tempFolder
from collections import defaultdict


db = 'import'
dext = '20151231'


cim2ciap = {cim: ciap for (cim, ciap) in getAll('select codi_cim10, codi_ciap from cat_md_ct_cim10_ciap', db)}
ciap2des = {cod: des for (cod, des) in getAll('select codi_ciap, desc_ciap from cat_md_ct_cim10_ciap', db)}

prevalents = defaultdict(set)
incidents = defaultdict(set)
sql = "select id_cip, pr_cod_ps, date_format(pr_dde, '%Y%m%d'), date_format(pr_dba, '%Y%m%d') from problemes"
for id, cim, dde, dba in getAll(sql, db):
    if cim in cim2ciap:
        if dde <= dext and (dba == '00000000' or dba > dext):
            prevalents[cim2ciap[cim]].add(id)
        if '{}0101'.format(dext[:4]) <= dde <= dext:
            incidents[cim2ciap[cim]].add(id)

writeCSV(tempFolder + 'prevalents.csv', sorted([(ciap2des[ciap], len(ids)) for ciap, ids in prevalents.items()], key=lambda x: x[1], reverse=True), sep=';')
writeCSV(tempFolder + 'incidents.csv', sorted([(ciap2des[ciap], len(ids)) for ciap, ids in incidents.items()], key=lambda x: x[1], reverse=True), sep=';')
