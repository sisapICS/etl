import sisapUtils as u

print('geting_info')
sql = """SELECT codi_ambit, visi_sap, codi_sector, visi_up, s_descripcio,
		    cent_nom_centre, visi_tipus_visita, tv_des, 
		    visi_dia_peticio, visi_data_visita, 
		    (visi_data_visita-visi_dia_peticio) AS dies_demora
        FROM redics.VISTB043R
        WHERE visi_up IN (231,515,516,517,693,856,857,858,859,860,861,863,867,
            868,870,871,872,873,874,876,877,879,880,881,883,884,886,887,889,890,892,
            893,896,897,898,900,902,918,925,926,927,947,950,955,963,967,973,977,1020,
            1035,1036,1037,1038,1039,1040,1041,1042,1043,1044,1075,1081,1086,1119,
            1806,1808,1889,1980,2011,3034,3039,3041,3043,3049,3050,3051,3065,3066,
            3068,3072,3076,3077,3080,3082,3107,3236,3249,3368,3582,3584,3585,3586,
            3587,3588,3589,3590,3591,3592,3593,3594,3595,3597,3598,3599,3600,3601,
            3602,3604,3622,3623,3631,3853,3855,3984,4228,4269,4270,4286,4287,4290,
            4291,4323,4393,4426,4438,4559,4579,4656,4676,4719,4720,4721,4722,5276,
            5645,5646,5647,5648,6148,6160,7280,7438,7717,7718,7724,8095,8116)
            AND EXTRACT(YEAR FROM visi_data_visita) IN (2014,2015,2016,2017,2018,2019,2020,2021)"""

upload = []    
for codi_ambit, visi_sap, codi_sector, visi_up, s_descripcio, cent_nom_centre, visi_tipus_visita, tv_des, visi_dia_peticio, visi_data_visita, dies_demora in u.getAll(sql, "redics"):
    upload.append((codi_ambit, visi_sap, codi_sector, visi_up, s_descripcio, cent_nom_centre, visi_tipus_visita, tv_des, visi_dia_peticio, visi_data_visita, dies_demora))

print('creating database')
cols = "(codi_ambit varchar2(2), visi_sap varchar2(2), codi_sector varchar2(4), \
        visi_up varchar2(5), s_descripcio varchar2(40), cent_nom_centre varchar2(40), \
        visi_tipus_visita varchar2(4), tv_des varchar2(20), \
        visi_dia_peticio date, visi_data_visita date, dies_demora int)"

u.createTable('demores_salut_mental', cols, 'exadata', rm=True)
u.listToTable(upload, 'demores_salut_mental', "exadata")
u.grantSelect('demores_salut_mental','DWSISAP_ROL','exadata')