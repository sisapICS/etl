# coding: iso-8859-1


import tweepy
from tweepy import OAuthHandler
import collections 
from datetime import *
import time
from sisapUtils import *
from wordpress_xmlrpc import Client, WordPressPost
from wordpress_xmlrpc.methods.posts import GetPosts, NewPost, GetPost
from wordpress_xmlrpc.methods.users import GetUserInfo
from wordpress_xmlrpc.methods.comments import GetComments, GetCommentCount, GetComment, GetCommentStatusList
import csv
import requests


 
consumer_key = 'cQLbwVlWr2sahc4TBF8W9B0YJ'
consumer_secret = 'xcM3zEbiMdguBVuUrswgRBZNXI7wRunHJwfu6XpSrLWzEjCRJf'
access_token = '1100653153-HPwkynv9aJKeS74n3FzHejZ2CWpn5wRoKWuPxkX'
access_secret = '6xiZipI3JS61ZUmHsE9TojkS6Q3vFs4PmB57TbnJGuts2'
 
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)
 
api = tweepy.API(auth)

avui = date.today()
fa1mes = avui.month -1
anyActual = avui.year
if avui.month == 1:
    fa1mes = 12
    anyActual = avui.year - 1


csv_url = 'https://stats.wordpress.com/csv.php?api_key=3a483a4cfbcf&blog_uri=https://si9sapics.wordpress.com/&days=20000'


def get_mesenTEXT(mes):

    if mes == 1:
        mes_actual = 'Gener'
    elif mes == 2:
        mes_actual = 'Febrer'
    elif mes == 3:
        mes_actual = 'Mar�'
    elif mes == 4:
        mes_actual = 'Abril'
    elif mes == 5:
        mes_actual = 'Maig'
    elif mes == 6:
        mes_actual = 'Juny'
    elif mes == 7:
        mes_actual = 'Juliol'
    elif mes == 8:
        mes_actual = 'Agost'
    elif mes == 9:
        mes_actual = 'Setembre'
    elif mes == 10:
        mes_actual = 'Octubre'
    elif mes == 11:
        mes_actual = 'Novembre'
    elif mes == 12:
        mes_actual = 'Desembre'

    return mes_actual
  
#Twitter

dades_twitter = {}

dades_twitter['1. Data obertura: '] = '21/01/2013'


seguidors = 0
for follower in tweepy.Cursor(api.followers, screen_name = 'sisapics', count = 200).items():
    seguidors += 1
    
dades_twitter['2. Seguidors: '] = seguidors


friends = 0
for friend in tweepy.Cursor(api.friends).items():
    friends += 1
    
dades_twitter['3. Seguint: '] = friends
    
tuitsMes, tuitsTotal, retuits = 0, 0, 0

testTweet = tweepy.Cursor(api.user_timeline, screen_name = 'sisapics').items() 
for tweet in testTweet:
    data = tweet.created_at
    mes = data.month
    anys = data.year
    retweets = tweet.retweet_count
    if mes == fa1mes and anys == anyActual:
        retuits += retweets
        tuitsMes += 1
    tuitsTotal += 1
    
dades_twitter['4. Tuits del mes: '] = tuitsMes
dades_twitter['5. Total tuits: '] = tuitsTotal
dades_twitter['6. Retuits del mes: '] = retuits

mencions = 0
mentions = tweepy.Cursor(api.mentions_timeline, q ='@sisapics', screen_name = 'sisapics').items() 
for mention in mentions:
    #print mention.text
    data = mention.created_at
    mes = data.month
    anys = data.year
    if mes == fa1mes and anys == anyActual:
       mencions += 1
     
dades_twitter['7. Mencions del mes: '] = mencions


#blog

dades_si9sap = {}

dades_si9sap['1. Data obertura: '] = '21/01/2013'

wp = Client('https://si9sapics.wordpress.com/xmlrpc.php', 'sisap@gencat.cat', 'S19s4pics2')
posts = wp.call(GetPosts())


entrades = 0
offset = 0
increment = 10
while True:
    filter = { 'offset' : offset }
    posts = wp.call(GetPosts(filter))
    if len(posts) == 0:
        break # no more posts returned
    for post in posts:
        data = post.date
        mes = data.month
        anys = data.year
        if mes == fa1mes and anys == anyActual:
            entrades += 1
    offset = offset + increment


dades_si9sap['2. Entrades del mes: '] = entrades


visites_mes = 0
visites_totals = 0

with requests.Session() as s:
    download = s.get(csv_url)
    
    decoded_content = download.content.decode('utf-8')

    cr = csv.reader(decoded_content.splitlines(), delimiter=',')
    my_list = list(cr)
    for data, visites in my_list:
        try:
            visites_totals += int(visites)
        except:
            pass
        mes = data[5:-3]
        anys = data[:-6]
        if anys == str(anyActual):
            mes = int(mes)
            anys = int(anys)
        if mes == fa1mes and anys == anyActual:
            visites_mes += int(visites)

dades_si9sap['3. Visites totals acumulades: '] = visites_totals
dades_si9sap['4. Visites del mes: '] = visites_mes

offset = 0
increment = 10
comentaris = 0
while True:
    filter = { 'offset' : offset }
    comments = wp.call(GetComments(filter))
    if len(comments) == 0:
        break
    for comment in comments:
        data = comment.date_created
        mes = data.month
        anys = data.year
        if mes == fa1mes and anys == anyActual:
            comentaris += 1
    offset = offset + increment


dades_si9sap['5. Comentaris del mes: '] = comentaris


#enviar
mesentext = get_mesenTEXT(fa1mes)

text = 'Twitter:\r\n'
for a, b in  sorted(dades_twitter.items()):
    text += '{0} {1}\r\n'.format(a,b)

text += '\r\nBlog:\r\n'
for a, b in  sorted(dades_si9sap.items()):
    text += '{0} {1}\r\n'.format(a,b)  
  
assumpte = 'M�triques SISAP ' + mesentext + ' ' + str(anyActual)
sendPolite('premsa.ics@gencat.cat', assumpte,text)    
