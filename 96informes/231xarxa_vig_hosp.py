# coding: latin1

"""
.
"""

import collections as c
import datetime as d
import os

import sisapUtils as u


DEBUG = False

D_FI = d.date.today() - d.timedelta(days=(d.date.today().weekday() - 2) % 7)
# D_FI = d.date(2020, 1, 8)  # descomentar per forçar data
D_INI = D_FI - d.timedelta(days=6)

FILE = u.tempFolder + "res_master_per_enviar_setmanal.csv"

CODIS = {"respiratori":
            set(["C01-J00", "J00", "C01-J01", "C01-J01.0", "C01-J01.00",
                 "C01-J01.01", "C01-J01.1", "C01-J01.10", "C01-J01.11",
                 "C01-J01.2", "C01-J01.20", "C01-J01.21", "C01-J01.3",
                 "C01-J01.30", "C01-J01.31", "C01-J01.4", "C01-J01.40",
                 "C01-J01.41", "C01-J01.8", "C01-J01.80", "C01-J01.81",
                 "C01-J01.9", "C01-J01.90", "C01-J01.91", "J01", "J01.0",
                 "J01.1", "J01.2", "J01.3", "J01.4", "J01.8", "J01.9",
                 "C01-J02", "C01-J02.8", "C01-J02.9", "J02", "J02.8", "J02.9",
                 "C01-J03", "C01-J03.8", "C01-J03.80", "C01-J03.9",
                 "C01-J03.90", "J03", "J03.8", "J03.9", "C01-J04", "C01-J04.0",
                 "C01-J04.1", "C01-J04.10", "C01-J04.11", "C01-J04.2",
                 "C01-J04.3", "C01-J04.30", "C01-J04.31", "J04", "J04.0",
                 "J04.1", "J04.2", "C01-J05", "C01-J05.0", "C01-J05.1",
                 "C01-J05.10", "C01-J05.11", "J05", "J05.0", "J05.1",
                 "C01-J06", "C01-J06.0", "C01-J06.9", "J06", "J06.0", "J06.8",
                 "J06.9", "C01-J12", "C01-J12.0", "C01-J12.1", "C01-J12.2",
                 "C01-J12.3", "C01-J12.8", "C01-J12.81", "C01-J12.89",
                 "C01-J12.9", "J12", "J12.0", "J12.1", "J12.2", "J12.8",
                 "J12.9", "C01-J16", "C01-J16.8", "J16", "J16.8", "C01-J17",
                 "J17", "J17.1", "J17.8", "C01-J18", "C01-J18.0", "C01-J18.1",
                 "C01-J18.8", "C01-J18.9", "J18", "J18.0", "J18.1", "J18.8",
                 "J18.9", "C01-J20", "C01-J20.3", "C01-J20.4", "C01-J20.5",
                 "C01-J20.6", "C01-J20.7", "C01-J20.8", "C01-J20.9", "J20",
                 "J20.3", "J20.4", "J20.5", "J20.6", "J20.7", "J20.8", "J20.9",
                 "C01-J40", "J40", "C01-J21", "C01-J21.0", "C01-J21.1",
                 "C01-J21.8", "C01-J21.9", "J21", "J21.0", "J21.8", "J21.9",
                 "C01-J22", "J22", "B97.4", "C01-B97.4", "C01-J09",
                 "C01-J09.X", "C01-J09.X1", "C01-J09.X2", "C01-J09.X3",
                 "C01-J09.X9", "J09", "C01-J10", "C01-J10.0", "C01-J10.00",
                 "C01-J10.01", "C01-J10.08", "C01-J10.1", "C01-J10.2",
                 "C01-J10.8", "C01-J10.81", "C01-J10.82", "C01-J10.83",
                 "C01-J10.89", "J10", "J10.0", "J10.1", "J10.8", "C01-J11",
                 "C01-J11.0", "C01-J11.00", "C01-J11.08", "C01-J11.1",
                 "C01-J11.2", "C01-J11.8", "C01-J11.81", "C01-J11.82",
                 "C01-J11.83", "C01-J11.89", "J11", "J11.0", "J11.1", "J11.1",
                 "J11.8", "B34.2", "C01-B34.2", "B34.0", "C01-B34.0", "B97.0",
                 "C01-B97.0", "C01-B97.11", "C01-B97.12", "C01-B97.19",
                 "B97.2", "C01-B97.2", "C01-B97.21", "C01-B97.29",
                 "C01-B97.81"]),
         "enterovirus":
            set(["B97.1", "C01-B97.1", "C01-B97.10", "C01-B97.11",
                 "C01-B97.12", "C01-B97.19", "B08.4", "C01-B08.4", "B08.5",
                 "C01-B08.5", "C01-B08.8", "A85.0", "C01-A85.0", "B30.3",
                 "C01-B30.3", "B34.1", "C01-B34.1", "A88.0", "C01-A88.0",
                 "A87.0", "C01-A87.0"])}


class XVH(object):
    """."""

    def __init__(self):
        """."""
        self.get_problemes()
        self.get_cataleg()
        self.get_master()
        self.export_data()
        self.send_file()

    def get_problemes(self):
        """."""
        self.dades = c.Counter()
        if DEBUG:
            dades = [self._worker("6837")]
        else:
            dades = u.multiprocess(self._worker, u.sectors)
        for sector in dades:
            for ps, th, dde, naix in sector:
                key = (ps, th, dde.date(), u.yearsBetween(naix, dde))
                self.dades[key] += 1

    def _worker(self, sector):
        """."""
        sql = "select pr_cod_ps, pr_th, pr_dde, \
                      to_date(usua_data_naixement, 'J') \
               from prstb015, usutb040 \
               where pr_dde between date '{}' and date '{}' and \
                     pr_cod_ps in {} and \
                     pr_cod_u = usua_cip and \
                     usua_uab_up is not null and \
                     usua_uab_codi_uab is not null and \
                     usua_situacio = 'A'".format(
                                            D_INI.strftime("%Y-%m-%d"),
                                            D_FI.strftime("%Y-%m-%d"),
                                            tuple(set.union(*CODIS.values())))
        return [row for row in u.getAll(sql, sector)]

    def get_cataleg(self):
        """."""
        origen = {"ps": "select ps_cod, ps_des from prstb001",
                  "th": "select pst_th, pst_des from prstb305"}
        self.cataleg = {key: {row[0]: row[1]
                              for row in u.getAll(value, "6837")}
                        for key, value in origen.items()}
        self.cataleg["tip"] = {}
        for key, codis in CODIS.items():
            for codi in codis:
                self.cataleg["tip"][codi] = key

    def get_master(self):
        """."""
        self.master = []
        for (ps, th, dde, edat), n in self.dades.items():
            this = (ps, th, dde, edat, self.cataleg["ps"].get(ps),
                    self.cataleg["th"].get(th), self.cataleg["tip"][ps], n)
            self.master.append(this)

    def export_data(self):
        """."""
        header = [("codi", "codi_th", "data", "edat", "desc_codi", "desc_th",
                   "tipus", "recompte")]
        u.writeCSV(FILE, header + self.master, sep=";")

    def send_file(self):
        """."""
        to = ["aanton@vhebron.net", "mfabregase@gencat.cat"]
        subject = "Dades setmanals de Xarxa de Vigilància Hospitalaria de VRS a Catalunya"  # noqa
        text = "Us adjuntem les dades de la darrera setmana"
        u.sendPolite(to, subject, text, file=FILE)
        os.remove(FILE)


if __name__ == "__main__":
    XVH()
