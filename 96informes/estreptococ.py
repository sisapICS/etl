
import sisapUtils as u
import collections as c

cols = '(val int, month int, year int, N int)'
u.createTable('estreptococ', cols, 'test')

proves = c.defaultdict(set)
print('adults')
sql = """SELECT id_cip_sec, valor, month(data_var), year(data_var) FROM nodrizas.eqa_variables
            where agrupador = 904
            and valor in (0, 1)
            and year(data_var) >= 2018"""
for id, val, month, year in u.getAll(sql, 'nodrizas'):
    val = int(val)
    proves[(val, month, year)].add(id)
print('pedia')
sql = """select ID_CIP_SEC, VAL, MONTH(dat), year(dat) from nodrizas.ped_variables
            where agrupador = 760
            and year(dat) >= 2018
            AND VAL IN ('0.0', '1.0', '1', '0')"""
for id, val, month, year in u.getAll(sql, 'nodrizas'):
    if val in ('0.0', '0'): val = 0
    else: val = 1
    proves[(val, month, year)].add(id)

upload = []
for (val, month, year), conjunt in proves.items():
    upload.append((val, month, year, len(conjunt)))

u.listToTable(upload, 'estreptococ', 'test')