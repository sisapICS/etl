import sisapUtils as u
import datetime as d
import collections as c
import sisaptools as t

DEXTD = u.getOne('select data_ext from dextraccio', 'nodrizas')[0]

sql = """
    SELECT id_cip_sec, up
    FROM assignada_tot
    WHERE edat < 75
"""
pob = {id: up for id, up in u.getAll(sql, 'nodrizas')}
print('pob')
sql = "select scs_codi, ics_codi from cat_centres"
centres = {up: br for up, br in u.getAll(sql, 'nodrizas')}
print('centres')
tables = u.getSubTables('tractaments', 'import')
sql = """
    SELECT id_cip_sec
    FROM {tb} t
    WHERE ppfmc_data_fi > DATE '{DEXTD}'
    AND (ppfmc_durada > 360 OR ppfmc_seg_evol = 'S')
    AND length(ppfmc_atccodi) = 7
    AND NOT EXISTS
        (SELECT 1
        FROM cat_cpftb006 c
        WHERE
        c.pf_codi = t.ppfmc_pf_codi
        AND (c.pf_ff_codi in ('GE','PO','CL')
        OR c.pf_via_adm = 'B31'
        OR c.pf_gt_codi like '23C%'))
"""
tractaments = c.Counter()
for tb in tables:
    for id, in u.getAll(sql.format(tb=tb, DEXTD=DEXTD), 'import'):
        if id in pob:
            tractaments[id] += 1
    print(tb)
print('tractaments')
res = c.defaultdict(c.Counter)
for id in pob:
    up = pob[id]
    if up in centres:
        br = centres[up]
        res[br]['DEN'] += 1
        if id in tractaments and tractaments[id] > 10:
            res[br]['NUM'] += 1
print('res')
upload = []
for br, values in res.items():
    num = values['NUM'] if 'NUM' in values else 0
    den = values['DEN']
    upload.append((br, num, den))
cols = [('EAP', 'NUM', 'DEN')]
u.writeCSV('polif_atc.csv', cols+upload, sep=';')
print('fi')