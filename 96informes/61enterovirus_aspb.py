from sisapUtils import multiprocess, sectors, getAll, writeCSV, tempFolder
from os import remove


file_dada = tempFolder + 'B08_4.txt'
file_up = tempFolder + 'cat_up.txt'
file_abs = tempFolder + 'cat_abs.txt'


def get_data(sector):
    poblacio = {row[0]: row[1:] for row in getAll("select id_cip, date_format(usua_data_naixement, '%Y-%m-%d'), usua_sexe, usua_abs_codi_abs from assignada_s{}".format(sector), 'import')}
    problemes = [row + poblacio[row[1]] for row in getAll("select codi_sector, id_cip, date_format(pr_dde, '%Y-%m-%d'), pr_up from problemes_s{} \
                                                                                      where pr_cod_ps = 'B08.4' and pr_up <> '' and pr_dde between '20000101' and '20160630'".format(sector), 'import')]
    return problemes


if __name__ == '__main__':
    dades = multiprocess(get_data, [sector for sector in sectors if sector[:2] == '68'])
    writeCSV(file_dada, [['sector', 'pacient', 'data_dx', 'up_dx', 'data_naix', 'sexe', 'abs_actual']])
    for dada in dades:
        writeCSV(file_dada, dada, 'a')
    writeCSV(file_up, [row for row in getAll("select up_codi_up_scs, up_desc_up_ics from cat_gcctb008 a inner join cat_gcctb007 b on a.up_codi_up_ics = b.up_codi_up_ics \
                                              where a.up_data_baixa = 0", 'import')])
    writeCSV(file_abs, [row for row in getAll("select abs_codi_abs, abs_nom_abs from cat_rittb001 where abs_data_baixa = '47120101'", 'import')])
