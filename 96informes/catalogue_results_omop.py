import sisapUtils as u
import csv


# Open the CSV file
with open('catalogue_results-2024-09-19.csv', mode='r') as file:
    csv_reader = csv.reader(file)
    data = [row for row in csv_reader]

# 110
analisis_110 = []
sql = """SELECT id, stratum_1, 'NA', 'NA', 'NA', 'NA', COUNT_VALUE,  
            'NA','NA','NA','NA','NA','NA','NA','NA','NA' 
            FROM dwsisap.krfyhu5gs_tmpach_110
            ORDER BY stratum_1 asc"""
for row in u.getAll(sql, 'exadata'):
    analisis_110.append(row)


# 117
analisis_117 = []
sql = """SELECT 117 as ids, stratum_1, 'NA', 'NA', 'NA', 'NA', COUNT_VALUE,  
            'NA','NA','NA','NA','NA','NA','NA','NA','NA' 
            FROM dwsisap.krfyhu5gs_tmpach_110
            ORDER BY stratum_1 asc"""
for row in u.getAll(sql, 'exadata'):
    analisis_117.append(row)


upload = []
last_109 = False
last_113 = False
First = True
for e in data:
    if First == True:
        upload.append(tuple(e))
        First = False
    elif int(e[0]) < 110:
        upload.append(tuple(e))
        last_109 = True
    elif int(e[0]) == 111 and last_109 == True:
        last_109 = False
        for e110 in analisis_110:
            upload.append(e110)
    elif (int(e[0]) >= 111 and int(e[0]) <= 113) and last_109 == False:
        upload.append(tuple(e))
    elif int(e[0]) == 200 and last_113 == True:
        last_113 = False
        for e117 in analisis_117:
            upload.append(e117)
    elif int(e[0]) >= 200 and last_113 == False:
        upload.append(tuple(e))

# Write to CSV
with open('catalogue_results.csv', mode='wb') as file:
    writer = csv.writer(file,quoting=csv.QUOTE_MINIMAL)
    for row in upload:
        writer.writerow(row)




