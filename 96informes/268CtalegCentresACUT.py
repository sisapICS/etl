# coding: latin1

import sisapUtils as u
import collections as c

"""peticions carles valero"""

cols = """(scs_cod varchar(10), up_cod varchar(100), up_des varchar(100), 
        dap varchar(100), dap_desc varchar(100), ambit varchar(100), ambit_desc varchar(100), sap varchar(100),
        es_ics varchar(100), tip_cod varchar(100), tip_des varchar(100), 
        abs_cod varchar(100), abs_des varchar(100), sector_cod varchar(100), sector_des varchar(100), aga_cod varchar(100), 
        aga_des varchar(100), regio_cod varchar(100),
        regio_des varchar(100), ep_cod varchar(100), ep_des varchar(100), tit_cod varchar(100), tit_des varchar(100),
        codi_centre varchar(100), classe_centre varchar(100))"""
u.createTable('centres_acut', cols, 'permanent', rm=True)

centre_classe = {}
sql = """select cent_codi_centre, cent_classe_centre, cent_codi_up from import.cat_pritb010"""
for codi_centre, classe_centre, up in u.getAll(sql, 'import'):
    centre_classe[up] = (codi_centre, classe_centre)

info_catsalut = {}
sql = """SELECT up_cod, es_ics, tip_cod, tip_des, 
        abs_cod, abs_des, sector_cod ,sector_des, aga_cod, aga_des, regio_cod,
        regio_des, ep_cod, ep_des, tit_cod, tit_des FROM dwsisap.DBC_RUP WHERE subtip_cod = '24'"""
for row in u.getAll(sql, 'exadata'):
    if row[0] in centre_classe:
        up = row[0]
        row = row[1:] + centre_classe[row[0]]
        info_catsalut[up] = row

agregacions = {}
sql = """SELECT a.up_codi_up_ics, a.up_desc_up_ics, b.up_codi_up_scs,
                c.dap_codi_dap, c.dap_desc_dap, c.amb_codi_amb, d.amb_desc_amb,
                d.sap_codi_sap
                FROM gcctb007 a
                INNER JOIN gcctb008 b ON a.UP_CODI_UP_ICS = b.up_codi_up_ics
                INNER JOIN gcctb006 c ON a.DAP_CODI_DAP = c.dap_codi_dap
                INNER JOIN gcctb005 d ON c.AMB_CODI_AMB = d.AMB_CODI_AMB 
                WHERE a.UP_DATA_BAIXA IS NULL AND c.DAP_DATA_BAIXA IS NULL"""
for up_ics, up_desc, up_scs, dap, dap_desc, amb, amb_desc, sap_codi in u.getAll(sql, 'redics'):
    agregacions[up_scs] = [up_scs, up_ics, up_desc, dap, dap_desc, amb, amb_desc, sap_codi]

upload = []
for up, info in info_catsalut.items():
    if up in agregacions:
        upl = agregacions[up] + list(info)
        upload.append(upl)

u.listToTable(upload, 'centres_acut', 'permanent')



# Roser, necessitaria explotaci� del darrer any, un "count" dels diagnostics 
# (codi cim10 i descriptiu"  de l'informe d'alta dels CUAP de Catalunya. 
# Variables: UP, centre, classe, descritiu centre/classe, data visita, 
# codi CIM10 diagn�stic , dexriptiu del diagn�stic i count. Aix� esta`en la taula PRSTB152. 
# En fitxera djunt et passo UP, centre i classe dels centres CUAPS




# interes = [
#         ('E25007101', '10'),
#         ('E43008183', '02'),
#         ('E43007687', '01'),
#         ('E08000360', '04'),
#         ('E08000392', '05'),
#         ('E08602220', '05'),
#         ('E08897648', '04'),
#         ('E08571621', '01'),
#         ('E08027429', '05'),
#         ('E17006718', '01'),
#         ('E08003080', '02'),
#         ('E08C19347', '03'),
#         ('E08627948', '11'),
#         ('E08004460', '11'),
#         ('E08003380', '02'),
#         ('E08019467', '11'),
#         ('E08005383', '06'),
#         ('E08000089', '05'),
#         ('E08003940', '06'),
#         ('E08004647', '11'),
#         ('E08015896', '11'),
#         ('A08000890', '11'),
#         ('E08003763', '11'),
#         ('E08002752', '11'),
#         ('E08023992', '11')
# ]

# cuaps = {}
# sql = """select codi_centre, classe_centre, up_cod, up_des from permanent.centres_acut"""
# for codi, classe, up, desc_up in u.getAll(sql, 'permanent'):
#         if (codi, classe) in interes:
#                 cuaps[(codi, classe)] = (up, desc_up)

# descs_cim10 = {}
# sql = """select ps_cod, ps_def from prstb001"""
# for codi, desc in u.getAll(sql, '6837'):
#         descs_cim10[codi] = desc


# # cols = '(cim varchar(50), desc_cim varchar(1000), centre_codi varchar(100), classe_codi varchar(5), up varchar(5), desc_up varchar(1000), N int)'
# # u.createTable('count_cim10_cuap', cols, 'permanent')

# # problemes = c.Counter()
# # sql = """SELECT IAU_PROB_PRINCIPAL, IAU_CODI_CENTRE, IAU_CLASSE_CENTRE 
# #                 FROM PRSTB152
# #                 WHERE EXTRACT(YEAR FROM IAU_DATA_ENTR) = 2022"""
# # for sector in u.sectors:
# #         print(sector)
# #         for cim, centre, classe in u.getAll(sql, sector):
# #                 if (centre, classe) in cuaps:
# #                         if cim in descs_cim10:
# #                                 cim_d = descs_cim10[cim]
# #                                 up, desc = cuaps[(centre, classe)]
# #                                 problemes[(cim, cim_d, centre, classe, up, desc)] += 1

# # upload = []
# # for (cim, cim_d, centre, classe, up, desc), N in problemes.items():
# #         upload.append((cim, cim_d, centre, classe, up, desc, N))


# # u.listToTable(upload, 'count_cim10_cuap', 'permanent')






# # cols = """(cim varchar(50), desc_cim varchar(1000), VCU_PROB_S_TRIATGE varchar(100), centre_codi varchar(100), classe_codi varchar(5), 
# #                 up varchar(5), desc_up varchar(1000), dia date, N int)"""
# # u.createTable('count_INF_ALTES', cols, 'permanent', rm=True)

# # descs_cim10 = {}
# # sql = """select ps_cod, ps_def from prstb001"""
# # for codi, desc in u.getAll(sql, '6837'):
# #         descs_cim10[codi] = desc

# # problemes = c.Counter()
# # sql = """SELECT vcu_centre_codi_centre, VCU_CENTRE_CLASSE_CENTRE, 
# #                 VCU_CIM10_TRIATGE, VCU_PROB_S_TRIATGE, TRUNC(VCU_DIA_PETICIO)
# #                 FROM VISTBU43
# #                 WHERE EXTRACT(YEAR FROM VCU_DIA_PETICIO) = 2022
# #                 AND VCU_CIM10_TRIATGE IS NOT NULL
# #                 AND VCU_SERVEI ='URGEN'"""
# # for sector in u.sectors:
# #         print(sector)
# #         for centre, classe, cim, VCU_PROB_S_TRIATGE, dia in u.getAll(sql, sector):
# #                 if (centre, classe) in cuaps:
# #                         if cim in descs_cim10:
# #                                 cim_d = descs_cim10[cim]
# #                                 up, desc = cuaps[(centre, classe)]
# #                                 problemes[(cim, cim_d, VCU_PROB_S_TRIATGE, centre, classe, up, desc, dia)] += 1

# # upload = []
# # for (cim, cim_d, VCU_PROB_S_TRIATGE, centre, classe, up, desc, dia), N in problemes.items():
# #         upload.append((cim, cim_d, VCU_PROB_S_TRIATGE, centre, classe, up, desc, dia, N))


# cols = """(cim varchar(50), desc_cim varchar(1000), centre_codi varchar(100), classe_codi varchar(5), 
#                 SERVEI VARCHAR(5), MODUL VARCHAR(5),  up varchar(5), desc_up varchar(1000), dia date, N int)"""
# u.createTable('INFORMES_ALTES_2022', cols, 'permanent', rm=True)

# descs_cim10 = {}
# sql = """select ps_cod, ps_def from prstb001"""
# for codi, desc in u.getAll(sql, '6837'):
#         descs_cim10[codi] = desc

# problemes = c.Counter()
# sql = """SELECT
#                 iau_prob_principal,
#                 IAU_CODI_CENTRE,
#                 IAU_CLASSE_CENTRE,
#                 IAU_CODi_SERVEI,
#                 IAU_CODI_MODUL,
#                 to_date(IAU_DATA_VISITA, 'j')
#         FROM
#                 prstb152
#         WHERE
#                 iau_prob_principal IS NOT NULL
#                 AND extract(YEAR FROM to_date(IAU_DATA_VISITA, 'j')) = 2022"""
# for sector in u.sectors:
#         print(sector)
#         for cim, centre, classe, servei, modul, dia in u.getAll(sql, sector):
#                 if (centre, classe) in cuaps:
#                         if cim in descs_cim10:
#                                 cim_d = descs_cim10[cim]
#                                 up, desc = cuaps[(centre, classe)]
#                                 problemes[(cim, cim_d, centre, classe, up, desc, servei, modul, dia)] += 1

# upload = []
# for (cim, cim_d, centre, classe, up, desc, servei, modul, dia), N in problemes.items():
#         upload.append((cim, cim_d, centre, classe, up, desc, servei, modul, dia, N))


# u.listToTable(upload, 'INFORMES_ALTES_2022', 'permanent')

