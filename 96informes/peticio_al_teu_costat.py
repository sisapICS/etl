import sisapUtils as u
import collections as c

# Carreguem professionals

def creacio():
    cat_professionals = dict()
    cat_uba = dict()

    sql = """
            SELECT
                ide_usuari,
                up,
                uab,
                tipus,
                NOM || ' ' || COGNOM1 || ' ' || COGNOM2 AS nom_sencer
            FROM
                dwsisap.PROFESSIONALS
            WHERE
                uab IS NOT NULL
        """
    rel_prof_dni = c.defaultdict()
    for professional, up, uba, cat_usuari, nom in u.getAll(sql, "exadata"):
        cat_professionals[professional] = {"nom": nom, "cat_usuari": cat_usuari, "up": up, "uba": uba}
        cat_uba[(up, uba)] = professional

    # print("Carreguem professionals")


    # Carreguem dades dels pacients

    # cat_pacients = dict()
    # cat_pcc, cat_maca, cat_atdom = set(), set(), set()

    # sql = """
    #         SELECT
    #             C_CIP,
    #             C_UP_ORIGEN,
    #             C_METGE,
    #             C_INFERMERA,
    #             CASE WHEN PR_PCC_DATA IS NOT NULL THEN 1 ELSE 0 END AS PCC,
    #             CASE WHEN PR_MACA_DATA IS NOT NULL THEN 1 ELSE 0 END AS MACA,
    #             CASE WHEN PS_ATDOM_DATA IS NOT NULL THEN 1 ELSE 0 END AS ATDOM
    #         FROM
    #             dwsisap.DBS
    #         WHERE
    #             c_data_naix < TO_DATE('2008-01-01', 'YYYY-MM-DD')
    #     """
    # # podem veure a qui esta assignat cada pacient
    # for pacient, up, professional_mf, professional_inf, pcc, maca, atdom in u.getAll(sql, "exadata"):
    #     if (up, professional_mf) in cat_uba and (up, professional_inf) in cat_uba:
    #         professional_mf = cat_uba[(up, professional_mf)]
    #         professional_inf = cat_uba[(up, professional_inf)]
    #         cat_pacients[pacient] = (up, professional_mf, professional_inf)
    #         if pcc:
    #             cat_pcc.add(pacient)
    #         if maca:
    #             cat_maca.add(pacient)
    #         if atdom:
    #             cat_atdom.add(pacient)

    # print("Carreguem dades dels pacients")


    # Agrupem els pacients pcc, maca i/o atdom

    # cat_pma = cat_pcc | cat_maca | cat_atdom

    # print("Agrupem els pacients pcc, maca i/o atdom")


    # Carreguem les dades referents a visites

    # n_visites, visites_fetes_propis_pacients, visites_fetes_total_pacients = c.defaultdict(c.Counter), c.defaultdict(c.Counter), c.defaultdict(c.Counter)
    conv_tipus_visita = {"9T": "9T",
                        "C9C": "9C+9R",
                        "C9R": "9C+9R",
                        "CALTRE": "9C+9R",
                        "D9D": "9D",
                        "DALTRE": "9D"
                        }

    sql = """
            SELECT
                UP,
                PACIENT,
                USUARI,
                TIPUS_CLASS,
                SISAP_CATPROF_CLASS,
                ATRIBUT1,
                DATA
            FROM
                dwsisap.sisap_master_visites
            WHERE
                SITUACIO = 'R'
                -- AND ATRIBUT1 IN (1, 2)
                AND sisap_catprof_class IN ('MF', 'INF')
                AND TIPUS_CLASS IN ('9T', 'C9C', 'C9R', 'CALTRE', 'D9D', 'DALTRE')
                AND DATA BETWEEN TO_DATE('2022-01-01', 'YYYY-MM-DD') AND TO_DATE('2023-12-12', 'YYYY-MM-DD')
        """
    # inf3 -> numero de pacients amb -3 visites en qualsevol professional, pero assignats a un concret
    # inf3_propies -> quantes visites dels pacients assignats s'han fet amb el metge assignat
    # inf3_totals -> quantes visites dels pacients assignats s'han fet

    # dades que necessitem:
    # - pacients per professional
    # - visites per professional
    dades_compteig = c.defaultdict(c.Counter)
    # set_pacients = c.defaultdict(set)
    visites_totals = c.defaultdict(c.Counter)
    visites_propies = c.defaultdict(c.Counter)
    for up, pacient, professional, tipus_visita, professional_cat, relacio_proveidor, data_visita in u.getAll(sql, "exadata"):
        # if pacient in cat_pacients:
        tipus_visita = conv_tipus_visita[tipus_visita]
        year = data_visita.year
        periodes = ["bianual"]
        if year == 2023:
            periodes.append("anual")
        for periode in periodes:
            if relacio_proveidor == '1':
                visites_propies[pacient][(periode, professional_cat, professional, tipus_visita)] += 1
            visites_totals[pacient][(periode, professional_cat, professional, tipus_visita)] += 1
            # up, mf, inf = cat_pacients[pacient]
            # if professional_cat == 'MF':
            #     # set_pacients[(periode, professional_cat, mf, tipus_visita)].add(pacient)
            #     visites_totals[pacient][(periode, professional_cat, mf, tipus_visita)] += 1
            #     # rel_prof_dni[dni] = professional
            #     if mf == professional:
            #         visites_propies[pacient][(periode, professional_cat, mf, tipus_visita)] += 1
            # else:
            #     # set_pacients[(periode, professional_cat, inf, tipus_visita)].add(pacient)
            #     visites_totals[pacient][(periode, professional_cat, inf, tipus_visita)] += 1
            #     if inf == professional:
            #         visites_propies[pacient][(periode, professional_cat, inf, tipus_visita)] += 1
                    
            
            # tipus_visita = conv_tipus_visita[tipus_visita]
            # year = data_visita.year
            # periodes = ["bianual"]
            # if year == 2023:
            #     periodes.append("anual")
            # for periode in periodes:
            #     n_visites[pacient][(periode, professional_cat, tipus_visita)] += 1
            #     if relacio_proveidor == '1':
            #         visites_fetes_propis_pacients[pacient][(periode, professional_cat, tipus_visita)] += 1
            #     visites_fetes_total_pacients[pacient][(periode, professional_cat, tipus_visita)] += 1

    print("Carreguem les dades referents a visites")

    dades = c.defaultdict(c.Counter)
    for pacient in visites_totals:
        for (periode, cat, prof, tipus_visita) in visites_totals[pacient]:
            if visites_totals[pacient][(periode, cat, prof, tipus_visita)] >= 3:
                dades[(periode, cat, prof, tipus_visita)]['sup3_pac'] += 1
                dades[(periode, cat, prof, tipus_visita)]['sup3_total'] += visites_totals[pacient][(periode, cat, prof, tipus_visita)]
            else:
                dades[(periode, cat, prof, tipus_visita)]['inf3_pac'] += 1
                dades[(periode, cat, prof, tipus_visita)]['inf3_total'] += visites_totals[pacient][(periode, cat, prof, tipus_visita)]
    
    for pacient in visites_propies:
        for (periode, cat, prof, tipus_visita) in visites_propies[pacient]:
            if visites_propies[pacient][(periode, cat, prof, tipus_visita)] >= 3:
                dades[(periode, cat, prof, tipus_visita)]['sup3_propies'] += visites_propies[pacient][(periode, cat, prof, tipus_visita)]
            else:
                dades[(periode, cat, prof, tipus_visita)]['inf3_propies'] += visites_propies[pacient][(periode, cat, prof, tipus_visita)]
    dades_taula = []
    for (periode, cat, prof, tipus_visita) in dades:
        sup3_pac = dades[(periode, cat, prof, tipus_visita)]['sup3_pac']
        sup3_total = dades[(periode, cat, prof, tipus_visita)]['sup3_total']
        sup3_propies = dades[(periode, cat, prof, tipus_visita)]['sup3_propies']
        inf3_pac = dades[(periode, cat, prof, tipus_visita)]['inf3_pac']
        inf3_total = dades[(periode, cat, prof, tipus_visita)]['inf3_total']
        inf3_propies = dades[(periode, cat, prof, tipus_visita)]['inf3_propies']
        up = cat_professionals[prof]['up'] if prof in cat_professionals else None
        nom = cat_professionals[prof]['nom'] if prof in cat_professionals else None
        dades_taula.append((periode, tipus_visita, up, nom, cat, sup3_pac, sup3_propies, sup3_total, inf3_pac, inf3_propies, inf3_total))
    # Creacio dels indicadors

    # dades = c.defaultdict(c.Counter)

    # for pacient in n_visites:
    #     for (periode, professional_cat, tipus_visita) in n_visites[pacient]:
    #         up, professional_mf, professional_inf = cat_pacients[pacient]
    #         n_visites_pacient = "sup3" if n_visites[pacient][(periode, professional_cat, tipus_visita)] > 3 else "inf3"
    #         professional = professional_mf if professional_cat == 'MF' else professional_inf
    #         professional_nom = cat_professionals[professional]["nom"]
    #         if n_visites_pacient == "sup3":
    #             dades[(periode, tipus_visita, up, professional_nom, professional_cat)]["pac_sup3_visites"] += 1
    #             dades[(periode, tipus_visita, up, professional_nom, professional_cat)]["n_visites_propies_pac_sup3_visites"] += 1
    #             dades[(periode, tipus_visita, up, professional_nom, professional_cat)]["n_visites_totals_pac_sup3_visites"] += 1
    #         else:
    #             dades[(periode, tipus_visita, up, professional_nom, professional_cat)]["pac_inf3_visites"] += 1
    #             dades[(periode, tipus_visita, up, professional_nom, professional_cat)]["n_visites_propies_pac_inf3_visites"] += 1
    #             dades[(periode, tipus_visita, up, professional_nom, professional_cat)]["n_visites_totals_pac_inf3_visites"] += 1

    # print("Creacio dels indicadors")

    # # Creacio dades taules

    # dades_taula = list()

    # for key, values in dades.items():
    #     periode, tipus_visita, up, professional_nom, professional_cat = key
    #     pac_sup3_visites = values.get("pac_sup3_visites", 0)
    #     n_visites_propies_pac_sup3_visites = values.get("n_visites_propies_pac_sup3_visites", 0)
    #     n_visites_totals_pac_sup3_visites = values.get("n_visites_totals_pac_sup3_visites", 0)
    #     pac_inf3_visites = values.get("pac_inf3_visites", 0)
    #     n_visites_propies_pac_inf3_visites = values.get("n_visites_propies_pac_inf3_visites", 0)
    #     n_visites_totals_pac_inf3_visites = values.get("n_visites_totals_pac_inf3_visites", 0)

    #     dades_taula.append((periode, tipus_visita, up, professional_nom, professional_cat, pac_sup3_visites, n_visites_propies_pac_sup3_visites, n_visites_totals_pac_sup3_visites, pac_inf3_visites, n_visites_propies_pac_inf3_visites, n_visites_totals_pac_inf3_visites))

    # print("Creacio dades taules")

    # Exportar taules

    tb = "peticio_al_teu_costat_v2"
    db = "test"
    cols = "(periode varchar(10), tipus_visita varchar(10), up varchar(10), professional_nom varchar(200), professional_cat varchar(10), pac_sup3_visites int, n_visites_propies_pac_sup3_visites int, n_visites_totals_pac_sup3_visites int, pac_inf3_visites int, n_visites_propies_pac_inf3_visites int, n_visites_totals_pac_inf3_visites int)"
    u.createTable(tb, cols, db, rm=1)
    u.listToTable(dades_taula, tb, db)

    print("Exportar taules")

def carrega():
    file_name = 'peticio_al_teu_costat_v2.csv'
    sql = "select scs_codi, ics_desc from cat_centres"
    ups = c.defaultdict()
    for up, desc in u.getAll(sql, 'nodrizas'):
        ups[up] = desc
    sql = """
        SELECT up, professional_nom, professional_cat, pac_inf3_visites, n_visites_propies_pac_inf3_visites,
        n_visites_totals_pac_inf3_visites, pac_sup3_visites, n_visites_propies_pac_sup3_visites, n_visites_totals_pac_sup3_visites,
        if(periode = 'anual', '1 ANY', '2 ANYS'), tipus_visita
        FROM test.peticio_al_teu_costat_v2
        WHERE periode = '{periode}'
        AND tipus_visita = '{tipus}'
        AND up <> ''
    """
    periodes = ['anual', 'bianual']
    visites = [('9C+9R'), ('9D'), ('9T')]
    upload = []
    for periode in periodes:
        print(periode)
        for tip in visites:
            # print(tip)
            # if periode == 'anual':
            #     print('1 any')
            #     col1 = ('1 ANY', '', '', '', '', '', '', '', '')
            # else:
            #     print('2 anys')
            #     col1 = ('2 ANYS', '', '', '', '', '', '', '', '')
            # if '9C+9R' in tip:
            #     print('presencial')
            #     col2 = ('Visites presencials (9C + 9R)', '', '', '', '', '', '', '', '')
            # elif '9D' in tip:
            #     print('domicili')
            #     col2 = ('Visites domicili 9D', '', '', '', '', '', '', '', '')
            # else:
            #     print('telefoniques')
            #     col2 = ('Visites telefoniques 9T', '', '', '', '', '', '', '', '')
            # upload.append(col1)
            # upload.append(col2)
            for up, prof, cat, inf3, propinf3, totinf3, sup3, propsup3, totsup3, periode, visita in u.getAll(sql.format(periode=periode, tipus=tip), 'test'):
                # print(up)
                if up in ups:
                    upload.append((ups[up], prof, cat, inf3, propinf3, totinf3, sup3, propsup3, totsup3, periode, visita))
    u.writeCSV(file_name, upload, sep=';')

    sql = """
        SELECT up, professional_cat, sum(pac_inf3_visites), sum(n_visites_propies_pac_inf3_visites),
        sum(n_visites_totals_pac_inf3_visites), sum(pac_sup3_visites), sum(n_visites_propies_pac_sup3_visites), sum(n_visites_totals_pac_sup3_visites),
        if(periode = 'anual', '1 ANY', '2 ANYS'), tipus_visita
        FROM test.peticio_al_teu_costat_v2
        WHERE up <> ''
        GROUP BY periode, tipus_visita, up, professional_cat
    """
    upload = []
    for up, cat, inf3, propinf3, totinf3, sup3, propsup3, totsup3, periode, visita in u.getAll(sql.format(periode=periode, tipus=tip), 'test'):
        if up in ups:
            upload.append((ups[up], cat, inf3, propinf3, totinf3, sup3, propsup3, totsup3, periode, visita))
    file_name = 'al_teu_costat_agrupacio_v2.csv'
    u.writeCSV(file_name, upload, sep=',')

carrega()