# coding: utf8

"""
Procés per extreure dades del projecte de recerca del pafes (departament)
"""

import urllib as w
import os

import sisapUtils as u


dict_codis = {18: 'DM2', 55: 'HTA', 47: 'hipercolesterolemia', 239: 'Obesitat', 197: 'colesterol total', 9: 'LDL',
                'TT102': 'Pes', 'TT101': 'Talla', 'TT103': 'IMC', 'LBB127': 'HbA1C', 'EK201': 'TAS', 'EK202': 'TAD',
                'Q32036': 'HbA1C','BB127': 'HbA1C','Q32136': 'HbA1C'}


class Pafes(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_sedentaris()
        self.get_poblacio()
        self.get_gma()
        self.get_tabac()
        self.get_problemes()
        self.get_variables()
        self.get_altres_vars()
        self.get_lab()
        self.get_pafes()
        self.get_medea()
        self.export_data()      


    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, ics_desc \
                from cat_centres where ep='0208'", "nodrizas")
        self.centres = {up: (br, desc) for (up, br,desc)
                        in u.getAll(*sql)}
    
    def get_sedentaris(self):
        """."""
        self.sedentaris = {}
        for table in u.getSubTables('variables'):
            dat, = u.getOne('select year(vu_dat_act) from {} limit 1'.format(table), 'import')
            if 2010 <= dat <= 2013:
                print table
                sql = "select id_cip_sec, date_format(vu_dat_act,'%Y%m%d'), vu_cod_vs, vu_val from {} where vu_cod_vs in ('VP1101','EP4012')".format(table)
                for id, dat, var, val in u.getAll(sql, "import"):
                    if 3 <= int(val) <= 5:
                        if (id) in self.sedentaris:
                            dat2 = self.sedentaris[(id)]['dat']
                            if dat < dat2:
                                self.sedentaris[(id)]['dat'] = dat
                                self.sedentaris[(id)]['valor'] = val
                                self.sedentaris[(id)]['var'] = var
                        else:
                            self.sedentaris[(id)] = {'var': var, 'dat': dat, 'valor': val}
    
    def get_poblacio(self):
        """Majors de 14 anys i actius actualment i sendentaris entre 2010-2013"""
        self.dades = {}
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = ("select id_cip_sec, codi_sector, data_naix, sexe, \
                        nacionalitat, nivell_cobertura, up, abs  \
                from assignada_tot \
                where edat > 14 and institucionalitzat=0", "nodrizas")
        for id, sec, edt, sex, nac, cob, up, abs in u.getAll(*sql):
            if up in self.centres:
                br, desc = self.centres[up][0], self.centres[up][1]
                pensio = 1 if cob == 'PN' else 0
                sed = 1 if (id) in self.sedentaris else 0
                rent = 1 if nac in renta else 0
                if sed == 1:
                    valor, data, var= self.sedentaris[id]['valor'], self.sedentaris[id]['dat'], self.sedentaris[id]['var']
                    self.dades[id] = {
                              'edat': edt,'sex': sex, 'gma_cod': None, 'gma_cmplx': None,
                              'gma_num': None, 'medea': None, 
                              'renta': rent, 'pensio': pensio, 'up': up, 'br': br, 'desc': desc, 'abs': abs,
                              'sed_var': var, 'sed_valor': valor,'sed_data': data}
                              
    def get_gma(self):
        """Obté els tres valors de GMA."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        for id, cod, cmplx, num in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['gma_cod'] = cod
                self.dades[id]['gma_cmplx'] = cmplx
                self.dades[id]['gma_num'] = num

    def get_tabac(self):
        """obtenim tabac dels pacients"""
        self.tabac = []
        sql = "select id_cip_sec, tab, dalta, dbaixa from eqa_tabac"
        for id, tab, alta, baixa in u.getAll(sql, 'nodrizas'):
            if id in self.dades:
                self.tabac.append([id, tab, alta, baixa])
    
    def get_problemes(self):
        """obtenim alguns problemes de salut: hta, dm2, hipercolesterolemia"""
        self.prob = []
        sql = "select id_cip_sec, ps, dde from eqa_problemes where ps in (18, 55, 47, 239)"
        for id, ps, dde in u.getAll(sql, 'nodrizas'):
            if id in self.dades:
                desc_p = dict_codis[ps]
                self.prob.append([id, desc_p, dde])
        
    def get_variables(self):
        """obtenim algunes variables (de les que ja tenim agrupadors) de eqa_variables: colesterol i LDL"""
        self.vars = []
        sql = "select id_cip_sec, agrupador, date_format(data_var,'%Y%m%d'), valor from eqa_variables where agrupador in (197, 9)"
        for id, var, dat, val in u.getAll(sql, 'nodrizas'):
            if id in self.dades:
                desc_v = dict_codis[var]
                self.vars.append([id, desc_v, dat, val]) 
    
    def get_altres_vars(self):
        """Altres variables no de nodrizas"""
        self.sedentaris = {}
        for table in u.getSubTables('variables'):
            dat, = u.getOne('select year(vu_dat_act) from {} limit 1'.format(table), 'import')
            if 2010 <= dat <= 2018:
                print table
                sql = "select id_cip_sec, date_format(vu_dat_act,'%Y%m%d'), vu_cod_vs, vu_val from {} where vu_cod_vs in ('TT102','TT101','TT103','LBB127','EK201','EK202')".format(table)
                for id, dat, var, val in u.getAll(sql, "import"):
                    if id in self.dades:
                        desc_v = dict_codis[var]
                        self.vars.append([id, desc_v, dat, val]) 
                        
    def get_lab(self):
        """Lab de HbA1c"""
        self.sedentaris = {}
        for table in u.getSubTables('laboratori'):
            try:
                dat, = u.getOne('select year(cr_data_reg) from {} limit 1'.format(table), 'import')
                if 2010 <= dat <= 2018:
                    sql = "select id_cip_sec, date_format(cr_data_reg,'%Y%m%d'), cr_codi_prova_ics, trim(replace(replace(replace(replace(replace(cr_res_lab, ',', '.'), '>', ''), '<', ''), '+', ''), CHAR(9), '')) from {} where cr_codi_prova_ics in ('Q32036','BB127','Q32136') and cr_codi_lab <> 'RIMAP'".format(table)
                    for id, dat, var, val in u.getAll(sql, "import"):
                        if id in self.dades:
                            desc_v = dict_codis[var]
                            self.vars.append([id, desc_v, dat, val]) 
            except TypeError:
                print table
                continue
    
    def get_pafes(self):
        """."""
        for table in u.getSubTables('variables'):
            dat, = u.getOne('select year(vu_dat_act) from {} limit 1'.format(table), 'import')
            if 2010 <= dat <= 2018:
                print table
                sql = "select id_cip_sec, date_format(vu_dat_act,'%Y%m%d'), vu_cod_vs, vu_val from {} where vu_cod_vs in ('VP1101','EP4012', 'AL110')".format(table)
                for id, dat, var, val in u.getAll(sql, "import"):
                    if id in self.dades:
                        dat2 = self.dades[id]['sed_data']
                        if dat > dat2:
                            self.vars.append([id, var, dat, val]) 
                        
    
    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if id in self.dades and sector in valors:
                self.dades[id]['medea'] = valors[sector]
    
    def export_data(self):
        """."""
        upload = []
        upload.append(["ID","EDAT","SEXE", "GMA_COD", "GMA_CMPLX", "GMA_NUM", "MEDEA", "IMMIGRACIO_RENTA_BAIXA", "PENSIONISTA", "UP", "BR", "DESCRIPCIO_EAP", "ABS", "VARIABLE_SEDENTARI", "VALOR_SEDENTARI", "DATA_SEDENTARI"])
        for id, n in self.dades.items():
            upload.append([id, n['edat'],n['sex'],n['gma_cod'], n['gma_cmplx'], n['gma_num'], n['medea'], n['renta'],n['pensio'], n['up'], n['br'], n['desc'],n['abs'],n['sed_var'], n['sed_valor'], n['sed_data']])
        u.writeCSV(u.tempFolder + 'pafes_poblacio_estudi.txt', upload)
        
        u.writeCSV(u.tempFolder + 'pafes_tabac.txt', self.tabac)
        u.writeCSV(u.tempFolder + 'pafes_problemes.txt', self.prob)
        u.writeCSV(u.tempFolder + 'pafes_variables.txt', self.vars)
        
        
if __name__ == '__main__':
    Pafes()
