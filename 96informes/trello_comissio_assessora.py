
# -*- coding: utf8 -*-

# compte! obrir i guardar sempre en utf8!!!

from sisapUtils import *
import sisaptools as u
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import datetime as d
import pytz


def set_wekan():
    """."""
    # comissió assessora
    avui = d.datetime.today()
    card = WekanCard("SISAP", "Carril principal", "Rebotiga",
                     "Preparar comi assessora de {} {}".format(avui.strftime("%B"), avui.strftime("%Y"))) # noqa
    card.add_members("Souhel")
    card.add_labels("Comissió Assessora", "Definició")
    checklist = "Possibles temes a tractar"
    card.add_to_checklist(checklist, "TBD")
    checklist = "Temes a tractar"
    card.add_to_checklist(checklist, "TBD")
    card.add_peticionari("ICS")

    # revisió umi
    avui = d.datetime.today()
    card = WekanCard("SISAP", "Carril principal", "Rebotiga",
                     "Revisar nous codis CIM-10-MC a UMI {} {}".format(avui.strftime("%B"), avui.strftime("%Y"))) # noqa
    card.add_members("Souhel")
    card.add_labels("Definició")
    card.add_peticionari("ICS")

    # ts i odn
    avui = d.datetime.today()
    if avui.month in (6, 11):
        card = WekanCard("SISAP", "Carril principal", "Rebotiga",
                         "Catàleg amb TS i ODN per revisar els tècnics.")
        card.add_members("Souhel")
        card.add_labels("Definició")
        card.add_peticionari("ICS")
        text = """
        CREUAMENT:
        TSODOTIP
        DEF202X
        AMBITOS
        NOCLI
        NOCAT
        NOIMP
        DIM6SET

        RECORDATORI DE VALORS:
        - AMBTS: amb ts i amb odonto
        - SENSETS: sense ts i amb odonto
        - SENSEODO: amb ts i sense odonto
        - SENSETSODO: sense ts i sense odonto
        """
        card.add_description(text)
        card.add_comment("@Souhel targeta nova.")


if __name__ == "__main__":
    set_wekan()
