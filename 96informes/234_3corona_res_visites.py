# coding: latin1

"""
.
"""

import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta

import sisapUtils as u

tb = "sisap_covid_res_visites"
db = 'redics'


          
class Coronavirus_ind(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_pob()
        self.get_visites()
        self.export_ind()
        self.dona_grants()

    
    def get_pob(self):
        """pob residencia"""
        self.pob = {}
        sql = "select hash, residencia, data from sisap_covid_res_historic"
        for hash, resi, data in u.getAll(sql, db):
            self.pob[(hash,data)] = resi
            
    def get_visites(self):
        """."""
        self.upload = []
        sql = "select pacient, data, up, servei, tipus, etiqueta, atribut1, atribut2, estat from sisap_covid_pac_visites"
        for hash, data, up, servei, tipus, etiqueta, a1, a2, estat in u.getAll(sql, db):
            if (hash, data) in self.pob:
                resi = self.pob[(hash, data)]
                self.upload.append([hash, data, up, resi, servei, tipus, etiqueta, a1, a2, estat])
                
    
    def export_ind(self):
        """fem export dels indicadors"""

        columns = ["hash varchar2(100)", "data date", "up varchar2(5)", "resi varchar2(5)","servei varchar2(10)","tipus varchar2(10)","etiqueta varchar2(10)", "atribut1 varchar2(5)", "atribut2 varchar2(5)", "estat varchar2(20)"] 
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)

        u.listToTable(self.upload, tb, db)


    def dona_grants(self):
        """."""
        users= ["PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB"]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db)
    
if __name__ == '__main__':
    u.printTime("Inici")
    
    Coronavirus_ind()
    
    u.printTime("Final")
