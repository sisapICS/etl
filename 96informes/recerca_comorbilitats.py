# coding: utf-8

import sisapUtils as u
import collections as c
import datetime as d
from datetime import datetime

ps_codis = { # Viu sol
            'C01-Z60.2', 'Z60.2',
            # Malnutricio
            'C01-R63.4','C01-E43','C01-E44.1','C01-E44.0','C01-E46',
            'R63.4','E43','E44.1','E44.0','E46',
            # Fatiga
            'C01-R53',
            # Deteiorament cognitiu
            'C01-F03.90','C01-F01.50','C01-F03','C01-F03.9','C01-F01',
            'C01-F01.5','R53','F03.90','F01.50','F03','F03.9','F01','F01.5',
            'F06.7','C01-G30','C01-G30.0','C01-G30.1','C01-G30.9','C01-G30.8',
            'C01-G31.83','C01-R41.3','G30.9',
            # Úlceres a les cames i úlceres per pressió
            'C01-L89.90','C01-L97.909','C01-I70.25','C01-I83.009',
            # Caigudes
            'C01-W19.XXXA','W19.9',
            # Incontinència
            'C01-N39.41','C01-N39.46','C01-N39.490','C01-N39.498','C01-R32',
            'C01-R15','C01-R15.1','C01-F98.1','C01-F98.0','R32','R15','F98.1',
            'N39.3',
            # Sd confusional
            'C01-F05','F05',
            # Infeccions: via catàleg
            # Dèficits sensorials: vía catàleg
            # Obesitat
            'C01-E66.8','C01-E66.9','E66.8','E66.9',            
            # Restrenyiment
            'C01-K59.0','C01-K59.01','C01-K59.03','C01-K59.04','C01-K59.02','C01-K59.00','C01-K59.9',
            # Depressió: via catàleg
            # Insomni
            'C01-F51.09','C01-G47.0',
            # PADES
            'C01-Z51.5','Z51.5',
            # Exclusió social
            'C01-Z60.4','Z60.4',
            # Problemes de relació
            'C01-Z60.9','C01-Z63','Z60.9','Z63',
            # Problemes socials
            'C01-Z60','Z60',
            # Institucionalitzat
            'C01-Z59.3','Z59.3'
            }

var_codis = {# Risc mal nutrició (dades)
             'MNA', 'MNA2',
             # Barthel ()
             'ABVDB',
             # Lawton y Brody (dades)
             'AIVDL',
             # Barber (dades)
             'TBAR',
             # Braden (dades)
             'BRADE',
             # Sobrecàrrega cuidador (dades) 
             'VZ2011',
             # TIRS (dades)
             'TIRS',
             # Pfeiffer (dades)
             'VMTPF',
             # IMC (dades)
             'TT103',
             # Alcohol (Grau Risc) (dades)
             'ALRIS',
             # Drogues (no dades) (codis correctes)
             'MP2601', 'MP2201', 'EP2901', 'MP2501', 'MP2801', 'MP2101', 'MP2301'
             }

act_codis = {# Atdom (dades)
             # 'A2060',
             # Maca (dades)
             # 'VA212',
             # PCC (dades)
             # 'VA211',
             # Cures Pal.liatives (no dades)
             'CVSC03', 'CVSD03',
             # Activitat Física (dades)
             'CA331',
             # Tabac (dades)
             'ATA2', 'EDU07',
             # Alcohol (dades)
             'ALHAB', 'CP201'}


class comorbilitats(object):
    """ . """

    def __init__(self):
        """ . """

        self.get_cohort()
        # # # # self.get_prstb101()
        self.get_cip()
        self.get_u11()
        # self.get_nia()
        self.get_nia_rca()
        # self.get_assignada()
        # self.get_codis_problemes()
        # self.get_problemes()
        self.get_tractaments()
        # self.get_cat_vacunes()
        # self.get_vacunes()
        # self.get_visites()
        # self.get_variables()
        # self.get_activitats()

        # self.get_institucionalitzat()        
        # self.get_maca()
        # self.get_pcc()
        # self.get_piic()
        # self.get_cuidador()
        # self.get_social()

        # self.get_complexitat()

        # self.get_gescasos()
        
        # self.get_upload()

    def get_cohort(self):
        """ Obtenció de la cohort de pacients de l'estudi (AQUAS: EXADATA) """
        print("--------------------------------------------------- get_cohort")
        self.cohort = {}
        sql = """select cip from dwaquas.p713"""
        for cip, in u.getAll(sql, 'exadata'):
            cip13 = cip[0:13]
            self.cohort[cip13] = {'cip': cip}
        print(len(self.cohort))

    # def get_prstb101(self):
    #     """ . """
    #     print("------------------------------------------------- get_prstb101")
    #     ts = datetime.now()
    #     self.prstb101 = {}
    #     sql = "select usua_cip_cod, usua_cip from pdptb101"
    #     db = "pdp"
    #     for hash_d, cip13 in u.getAll(sql, db):
    #         if cip13 in self.cohort:
    #             self.prstb101[hash_d] = cip13
    #     #print(self.prstb101)
    #     print(len(self.prstb101))
    #     print('Time execution {} / nrow {}'.format(datetime.now() - ts, len(self.prstb101)))

    def get_cip(self):
        """
        Executa 16 workers en paral·lel
        segons el primer dígit del HASH.
        """
        print("------------------------------------------------------ get_cip")
        ts = datetime.now()
        jobs = [format(digit, "x").upper() for digit in range(16)]
        resultat = u.multiprocess(self.get_cip_worker, jobs)
        self.prstb101 = {}
        for worker in resultat:
            for cip13, hash_d in worker:
                if cip13 in self.cohort:
                    self.prstb101[hash_d] = cip13
        print('Time execution {} / nrow {}'.format(datetime.now() - ts, len(self.prstb101)))

    def get_cip_worker(self, subgroup):
        """Worker de get_cip"""
        sql = """select usua_cip, usua_cip_cod from pdptb101
               where substr(usua_cip_cod, 1, 1) = '{}'""".format(subgroup)
        db = "pdp"
        converter = []
        for cip13, hash_d in u.getAll(sql, db):
            converter.append((cip13, hash_d))
        return converter

    def get_u11(self):
        '''.'''
        print("------------------------------------------------------ get_u11")
        ts = datetime.now()
        self.u11 = {}
        db = 'import'
        tb = 'u11'
        sql = 'select id_cip, hash_d, codi_sector from {}.{}'.format(db, tb)
        for id_cip, hash_ics, sector in u.getAll(sql, db):
            if hash_ics in self.prstb101:
                self.u11[id_cip] = {'hash_d': hash_ics, 'sector': sector}
        print('Time execution {} / nrow {}'.format(datetime.now() - ts, len(self.u11)))

    # def get_nia_old(self):
    #     """ . """
    #     print("------------------------------------------------------ get_nia")
    #     ts = datetime.now()
    #     self.nia = {}
    #     db = 'redics'
    #     sql = "select usua_nia, usua_cip from md_poblacio \
    #                where usua_nia is not null"
    #     for nia, hash_d in u.getAll(sql, 'redics'):
    #         if hash_d in self.prstb101:
    #             self.nia[hash_d] = {'nia': nia}
    #     print('Time execution {} / nrow {}'.format(datetime.now() - ts, len(self.nia)))

    # def get_nia(self):
    #     """
    #     Executa 16 workers en paral·lel
    #     segons el primer dígit del HASH.
    #     """
    #     print("------------------------------------------------------ get_nia")
    #     ts = datetime.now()
    #     jobs = [format(digit, "x").upper() for digit in range(16)]
    #     resultat = u.multiprocess(self.get_nia_worker, jobs)
    #     self.nia = {}
    #     for worker in resultat:
    #         for nia, hash_d in worker:
    #             #if hash_d in self.prstb101:
    #             self.nia[hash_d] = {'nia': nia}
    #     print('Time execution {} / nrow {}'.format(datetime.now() - ts, len(self.nia)))

    # def get_nia_worker(self, subgroup):
    #     """Worker de get_nia"""
    #     sql = """select usua_nia, usua_cip from md_poblacio \
    #                where usua_nia is not null \
    #                and substr(usua_cip, 1, 1) = '{}'""".format(subgroup)
    #     db = "redics"
    #     converter = []
    #     for nia, hash_d in u.getAll(sql, db):
    #         converter.append((nia, hash_d))
    #     return converter

    def get_nia_rca(self):
        """ . """
        print("-------------------------------------------------- get_nia_rca")
        self.nia_rca = {}
        sql = "select ass_cip, ass_nia \
            from dwcatsalut.rca_pacients"
        db = "exadata"
        for cip, nia in u.getAll(sql, db):
            cip13 = cip[0:13]
            if cip13 in self.cohort:
                self.nia_rca[cip13] = {"nia": nia}
        print('Time execution {} / nrow {}'.format(datetime.now() - ts, len(self.nia_rca)))


    def get_assignada(self):
        """ . """
        print("------------------------------------------------ get_assignada")
        ts = datetime.now()
        self.assignada = {}
        sql = "select \
                    id_cip, usua_data_naixement, usua_sexe, usua_abs_codi_abs \
                from assignada"
        for id_cip, dnaix, sexe, codi_abs in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                self.assignada[id_cip] = {'dnaix': dnaix, 'sexe': sexe,
                                          'codi_abs': codi_abs}
        #print(self.assignada)
        print(len(self.assignada))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts, len(self.assignada)))
        self.upload = []
        for id_cip in self.assignada:
            hash_d = self.u11[id_cip]['hash_d']
            cip13 = self.prstb101[hash_d]
            if cip13 in self.nia_rca:
                nia = self.nia_rca[cip13]['nia']
            # if hash_d in self.nia:
            #     nia = self.nia[hash_d]['nia']
                dnaix = self.assignada[id_cip]['dnaix']
                sexe = self.assignada[id_cip]['sexe']
                codi_abs = self.assignada[id_cip]['codi_abs']
                self.upload.append((nia, dnaix, sexe, codi_abs))
        ts = datetime.now()
        table = "sisap_recerca_p713_poblacio"
        cols = "(nia int, \
                dnaix date, \
                sexe varchar2(1), \
                codi_abs varchar2(10))"
        db = "redics"
        u.createTable(table, cols, db, rm=True)
        u.listToTable(self.upload, table, db)
        u.grantSelect(table, ("PREDULMB","PREDUPRP","PREDUMMP","PREDUEHE",
                              "PREDUMBO"), db)
        print('Time execution upload {}'.format(datetime.now() - ts))        

    def get_institucionalitzat(self):
        """ . """
        print("--------------------------------------- get_institucionalitzat")
        self.ins = []
        sql = """select distinct(id_cip) from institucionalitzats"""
        for id_cip, in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']
                    dat = d.date(2021, 7, 5)
                    cod = 'INSTIT'
                    val = 1
                    self.ins.append((nia, dat, cod, val))
        print(len(self.ins))

    def get_codis_problemes(self):
        """ . """
        self.ps_codis_bycat = []
        sql = "select distinct(ps_cod) from import.cat_prstb001 \
                where ps_cod_o = 'C' \
                and (ps_cod like '%C01-A%' or \
                     ps_cod like 'A0%' or ps_cod like 'A1%' or \
                     ps_cod like 'A2%' or ps_cod like 'A3%' or \
                     ps_cod like 'A4%' or ps_cod like 'A5%' or \
                     ps_cod like 'A6%' or ps_cod like 'A7%' or \
                     ps_cod like 'A8%' or ps_cod like 'A9%' or \
                     ps_cod like '%C01-B%' or \
                     ps_cod like 'B0%' or ps_cod like 'B1%' or \
                     ps_cod like 'B2%' or ps_cod like 'B3%' or \
                     ps_cod like 'B4%' or ps_cod like 'B5%' or \
                     ps_cod like 'B6%' or ps_cod like 'B7%' or \
                     ps_cod like 'B8%' or ps_cod like 'B9%' or \
                     ps_cod like '%H65%' or \
                     ps_cod like '%H66%' or \
                     ps_cod like '%H67%' or \
                     ps_cod like '%I01%' or \
                     ps_cod like '%I30%' or \
                     ps_cod like '%I80%' or \
                     ps_cod like '%J00%' or \
                     ps_cod like '%J01%' or ps_cod like '%J02%' or \
                     ps_cod like '%J03%' or ps_cod like '%J04%' or \
                     ps_cod like '%J05%' or ps_cod like '%J06%' or \
                     ps_cod like '%J07%' or ps_cod like '%J08%' or \
                     ps_cod like '%J09%' or ps_cod like '%J10%' or \
                     ps_cod like '%J11%' or ps_cod like '%J12%' or \
                     ps_cod like '%J13%' or ps_cod like '%J14%' or \
                     ps_cod like '%J15%' or ps_cod like '%J16%' or \
                     ps_cod like '%J17%' or ps_cod like '%J18%' or \
                     ps_cod like '%J19%' or ps_cod like '%J20%' or \
                     ps_cod like '%J21%' or ps_cod like '%J22%' or \
                     ps_cod like '%K04%' or \
                     ps_cod like '%K05%' or \
                     ps_cod like '%K35%' or \
                     ps_cod like '%K85%' or \
                     ps_cod like '%L03%' or \
                     ps_cod like '%M00%' or \
                     ps_cod like '%M01%' or ps_cod like '%M02%' or \
                     ps_cod like '%M03%' or ps_cod like '%M04%' or \
                     ps_cod like '%M05%' or ps_cod like '%M06%' or \
                     ps_cod like '%M07%' or ps_cod like '%M08%' or \
                     ps_cod like '%M09%' or \
                     ps_cod like '%N10%' or \
                     ps_cod like 'T81.4' or \
                     ps_cod like 'Z21%' or \
                     ps_cod like '%H53%' or \
                     ps_cod like '%H54%' or \
                     ps_cod like '%H90%' or \
                     ps_cod like '%H91%' or \
                     ps_cod like '%H93%' or \
                     ps_cod like '%F32%' or \
                     ps_cod like '%F33%')"
        for ps_cod, in u.getAll(sql, 'import'):
            self.ps_codis_bycat.append(ps_cod)

    def get_problemes(self):
        """ . """
        print("------------------------------------------------ get_problemes")
        ts = datetime.now()
        self.ps = set()
        # sql = "select id_cip, pr_cod_ps, pr_dde, pr_dba \
        #        from problemes \
        #        where pr_cod_o_ps = 'C' \
        #        and pr_hist = 1 \
        #        and pr_cod_ps in {} \
        #        and (pr_dde <= '2018-12-31' and \
        #             (pr_dba = 0 or pr_dba >= '2017-01-01'))".format(tuple(ps_codis) + tuple(self.ps_codis_bycat))
        sql = "select id_cip, pr_cod_ps, pr_dde, pr_dba \
               from problemes \
               where pr_cod_o_ps = 'C' \
               and pr_hist = 1 \
               and (pr_dde <= '2018-12-31' and \
                    (pr_dba = 0 or pr_dba >= '2017-01-01'))"
        for id_cip, cod, dat, dfi in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']
                    self.ps.add((nia, cod, dat, dfi))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts,
                                                   len(self.ps)))
        self.upload = []
        for nia, cod, dat, dfi in self.ps:
            self.upload.append((nia, cod, dat, dfi))
        ts = datetime.now()
        table = "sisap_recerca_p713_problemes"
        cols = "(nia int, \
                cod varchar2(15), \
                dat date, \
                dfi date)"
        db = "redics"
        u.createTable(table, cols, db, rm=True)
        u.listToTable(self.upload, table, db)
        u.grantSelect(table, ("PREDULMB","PREDUPRP","PREDUMMP","PREDUEHE",
                              "PREDUMBO"), db)
        print('Time execution upload {}'.format(datetime.now() - ts)) 

    def get_tractaments(self):
        """ . """
        print("---------------------------------------------- get_tractaments")
        print("----------------------------------------- get_viaadministracio")
        self.va = {}
        sql = """select va_codi, va_desc from cpftb015"""
        for va_codi, va_desc in u.getAll(sql, '6838'):
            self.va[va_codi] = va_desc
        print(len(self.va))

        print("------------------------------------------------------- get_pf")
        self.pf = {}
        sql = """select pf_codi, pf_via_adm
                 from cat_cpftb006
                 where pf_via_adm <> ''"""
        for pf_codi, va_codi in u.getAll(sql, 'import'):
            self.pf[pf_codi] = va_codi
        print(len(self.pf))

        print("---------------------------------------------- get_tractaments")
        ts = datetime.now()
        self.tx = set()
        sql = """select
                    id_cip, 
                    ppfmc_pf_codi, 
                    ppfmc_pmc_data_ini, ppfmc_data_fi, 
                    ppfmc_atccodi, 
                    ppfmc_durada
                 from
                    import.tractaments 
		         where
                    ppfmc_pf_codi <> 0 and
                    ppfmc_atccodi <> '' and
                    ppfmc_pmc_data_ini <= '2018-12-31' and
                    ppfmc_data_fi >= '2017-01-01'"""
        for id_cip, pf, dini, dfi, atc, dur in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                    va_codi = self.pf[pf] if pf in self.pf else ''
                    if va_codi <> '':
                        va_desc = self.va[va_codi]
                    else:
                        va_desc = ''
                    self.tx.add((nia, dini, dfi, atc, dur, va_desc))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts, len(self.tx)))
        self.upload = []
        for nia, dini, dfi, atc, dur, va_desc in self.tx:
            self.upload.append((nia, dini, dfi, atc, dur, va_desc))
        ts = datetime.now()
        table = "sisap_recerca_p713_tractaments"
        cols = "(nia int, \
                 dat date, \
                 dfi date, \
                 atc varchar2(10), \
                 dur int, \
                 va_desc varchar2(50))"
        db = "redics"
        u.createTable(table, cols, db, rm=True)
        u.listToTable(self.upload, table, db)
        u.grantSelect(table, ("PREDULMB","PREDUPRP","PREDUMMP","PREDUEHE",
                              "PREDUMBO"), db)
        print('Time execution upload {}'.format(datetime.now() - ts))

    def get_variables(self):
        """ . """
        print("------------------------------------------------ get_variables")
        ts = datetime.now()
        self.variables = set()
        sql = "select id_cip, vu_dat_act, vu_cod_vs, vu_val \
                from variables \
                where vu_cod_vs in {0} \
                and vu_dat_act between '2015-01-01' \
                                   and '2018-12-31'".format(tuple(var_codis))
        for id_cip, dat, cod, val in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']
                    self.variables.add((nia, dat, cod, val))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts,
                                                   len(self.variables)))

    def get_activitats(self):
        """ . """
        print("----------------------------------------------- get_activitats")
        ts = datetime.now()
        self.activitats = set()
        sql = "select id_cip, au_dat_act, au_cod_ac, au_val \
               from activitats \
               where au_cod_ac in {0} \
               and au_val <> '' \
               and au_dat_act between '2015-01-01' \
                                   and '2018-12-31'".format(tuple(act_codis))
        for id_cip, dat, cod, val in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']
                    self.activitats.add((nia, dat, cod, val))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts,
                                                   len(self.activitats)))

    def get_maca(self):
        """ . """
        print("----------------------------------------------------- get_maca")
        ts = datetime.now()
        self.maca = []
        sql = "select id_cip, min(es_dde) as dat, min(es_cod) as cod \
               from estats \
               where es_cod='ER0002' and es_dde <= '2018-12-31' group by id_cip \
               union \
               select id_cip, min(pr_dde) as dat, 'ER0002' as cod \
               from problemes \
               where pr_cod_ps in ('Z51.5', 'C01-Z51.5') \
               and pr_dde <= '2018-12-31' and pr_hist=1 group by id_cip"
        for id_cip, dat, cod in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']
                    val = 1
                    self.maca.append((nia, dat, cod, val))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts, len(self.maca)))

    def get_pcc(self):
        """ . """
        print("------------------------------------------------------ get_pcc")
        ts = datetime.now()
        self.pcc = []
        sql = "select id_cip, min(es_dde) as dat, min(es_cod) as cod \
                from estats \
                where es_cod='ER0001' and es_dde <= '2018-12-31' \
                group by id_cip"
        for id_cip, dat, cod in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']            
                    val = 1
                    self.pcc.append((nia, dat, cod, val))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts,
                                                   len(self.pcc)))

    def get_piic(self):
        """ . """
        print("----------------------------------------------------- get_piic")
        ts = datetime.now()
        self.piic = []
        sql = "select id_cip, min(mi_data_reg) \
                from piic \
                where mi_data_reg <= '2018-12-31' group by id_cip"
        for id_cip, dat in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']
                    cod = 'PIIC'
                    val = 1
                    self.piic.append((nia, dat, cod, val))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts,
                                                   len(self.piic)))

    def get_cuidador(self):
        """ . """
        print("------------------------------------------------- get_cuidador")
        ts = datetime.now()
        self.cuidador = []
        sql = "select distinct(id_cip) from cuidador where cui_ref = 'S'"
        for id_cip, in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']            
                    dat = d.date(2021, 7, 5)
                    cod = 'CUIDADOR'
                    val = 1
                    self.cuidador.append((nia, dat, cod, val))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts,
                                                   len(self.cuidador)))

    def get_social(self):
        """ . """
        print("--------------------------------------------------- get_social")
        ts = datetime.now()
        self.social = []
        sql = "select id_cip, val_var, min(val_data) as dat from social \
                where val_data <= '2018-12-31' \
                and val_var in ('VSHA18', 'VCHA20') and val_val = 'S' \
                group by id_cip, val_var"
        for id_cip, cod, dat in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']         
                    val = 1
                    self.social.append((nia, dat, cod, val))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts,
                                                   len(self.social)))

    def get_gescasos(self):
        """ . """
        print("------------------------------------------------- get_gescasos")
        ts = datetime.now()
        self.gescasos = set()
        sql = "select id_cip, gesc_data_alta, gesc_data_sortida \
               from import.gescasos \
               where (gesc_data_alta <= '2018-12-31' \
                       and gesc_data_sortida = '') \
                     or (gesc_data_sortida between '2017-01-01'\
                                          and '2018-12-31')"
        for id_cip, dat, dfi in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']
                    cod = "GESCASOS"
                    val = 1
                    self.gescasos.add((nia, dat, dfi, cod, val))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts,
                                                   len(self.gescasos)))                    
        self.upload = []
        for nia, dat, dfi, cod, val in self.gescasos:
            self.upload.append((nia, dat, dfi, cod, val))
        ts = datetime.now()
        table = "sisap_recerca_p713_gestorcasos"
        cols = "(nia int, \
                 dat date, \
                 dfi date, \
                 cod varchar2(10), \
                 val int)"
        db = "redics"
        u.createTable(table, cols, db, rm=True)
        u.listToTable(self.upload, table, db)
        u.grantSelect(table, ("PREDULMB","PREDUPRP","PREDUMMP","PREDUEHE",
                              "PREDUMBO"), db)
        print('Time execution upload {}'.format(datetime.now() - ts))        

    def get_gestorcasos(self):
        """ . """
        print("---------------------------------------------- get_gestorcasos")
        ts = datetime.now()
        self.ssoc = set()
        sql = "select id_cip, gesc_data_alta, gesc_data_sortida \
               from import.gescasos \
               where (gesc_data_alta <= '2018-12-31' \
                       and gesc_data_sortida = '') \
                     or (gesc_data_sortida between '2017-01-01'\
                                          and '2018-12-31')"
        for id_cip, dat, dfi in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']
                    cod = "GESCASOS"
                    val = 1
                    self.gescasos.add((nia, dat, dfi, cod, val))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts,
                                                   len(self.gescasos)))                    
        self.upload = []
        for nia, dat, dfi, cod, val in self.gescasos:
            self.upload.append((nia, dat, dfi, cod, val))
        ts = datetime.now()
        table = "sisap_recerca_p713_gestorcasos"
        cols = "(nia int, \
                 dat date, \
                 dfi date, \
                 cod varchar2(10), \
                 val int)"
        db = "redics"
        u.createTable(table, cols, db, rm=True)
        u.listToTable(self.upload, table, db)
        u.grantSelect(table, ("PREDULMB","PREDUPRP","PREDUMMP","PREDUEHE",
                              "PREDUMBO"), db)
        print('Time execution upload {}'.format(datetime.now() - ts))

    def get_cat_vacunes(self):
        """ . """
        print("---------------------------------------------- get_cat_vacunes")
        self.cat_vacunes = {}
        sql = """select vacuna, antigen
                from cat_prstb040_new
                where antigen in ('A00027','A00028','A-GRIP')"""
        for vacuna, antigen in u.getAll(sql, 'import'):
            if antigen == 'A00027':
                vacuna_desc = 'PN13'
            if antigen == 'A00028':
                vacuna_desc = 'PN23'
            if antigen == 'A-GRIP':
                vacuna_desc = 'GRIPE'
            self.cat_vacunes[vacuna] = vacuna_desc

    def get_vacunes(self):
        """ . """
        print("-------------------------------------------------- get_vacunes")
        ts = datetime.now()
        self.vac = set()
        sql = "select id_cip, va_u_data_vac, va_u_cod from vacunes \
            where va_u_cod in {} \
            and va_u_data_vac <= '2018-12-31'".format(tuple(self.cat_vacunes))
        for id_cip, dat, cod in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']
                    vacuna_desc = self.cat_vacunes[cod]
                    self.vac.add((nia, dat, vacuna_desc))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts,
                                                   len(self.vac)))
        self.upload = []
        for nia, dat, vacuna_desc in self.vac:
            self.upload.append((nia, dat, vacuna_desc))
        ts = datetime.now()
        table = "sisap_recerca_p713_vacunes"
        cols = "(nia int, \
                 dat date, \
                 cod varchar2(10))"
        db = "redics"
        u.createTable(table, cols, db, rm=True)
        u.listToTable(self.upload, table, db)
        u.grantSelect(table, ("PREDULMB","PREDUPRP","PREDUMMP","PREDUEHE",
                              "PREDUMBO"), db)
        print('Time execution upload {}'.format(datetime.now() - ts))

    def get_visites(self):
        """ . """
        print("-------------------------------------------------- get_visites")
        ts = datetime.now()
        self.visites = set()
        sql = "select \
                id_cip, \
                visi_data_visita, visi_servei_codi_servei, visi_lloc_visita \
                from import.visites \
                where visi_situacio_visita = 'R' \
                and visi_data_visita between '2015-01-01' and '2019-03-31'"
        for id_cip, dat, serv, lloc in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']
                    self.visites.add((nia, dat, serv, lloc))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts, len(self.visites)))
        self.upload = []
        for nia, dat, serv, lloc in self.visites:
            self.upload.append((nia, dat, serv, lloc))
        ts = datetime.now()
        table = "sisap_recerca_p713_visites"
        cols = "(nia int, \
                dat date, \
                servei varchar2(15), \
                lloc varchar2(10))"
        db = "redics"
        u.createTable(table, cols, db, rm=True)
        u.listToTable(self.upload, table, db)
        u.grantSelect(table, ("PREDULMB","PREDUPRP","PREDUMMP","PREDUEHE",
                              "PREDUMBO"), db)
        print('Time execution upload {}'.format(datetime.now() - ts)) 

    def get_complexitat(self):
        """ . """
        print("------------------------------------------ get_complexitat/gma")
        ts = datetime.now()
        self.gma = set()
        sql = """select id_cip, gma_cod, gma_ind_cmplx from gma"""
        for id_cip, cod, val in u.getAll(sql, 'import'):
            if id_cip in self.u11:
                hash_d = self.u11[id_cip]['hash_d']
                cip13 = self.prstb101[hash_d]
                if cip13 in self.nia_rca:
                    nia = self.nia_rca[cip13]['nia']
                # hash_d = self.u11[id_cip]['hash_d']
                # if hash_d in self.nia:
                #     nia = self.nia[hash_d]['nia']
                    dat = ''
                    self.gma.add((nia, dat, cod, val))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts,
                                                   len(self.gma)))
        self.upload = []
        for nia, dat, cod, val in self.gma:
            self.upload.append((nia, dat, cod, val))
        ts = datetime.now()
        table = "sisap_recerca_p713_complexitat"
        cols = "(nia int, \
                dat date, \
                cod varchar2(15), \
                val number(6,3))"
        db = "redics"
        u.createTable(table, cols, db, rm=True)
        u.listToTable(self.upload, table, db)
        u.grantSelect(table, ("PREDULMB","PREDUPRP","PREDUMMP","PREDUEHE",
                              "PREDUMBO"), db)
        print('Time execution upload {}'.format(datetime.now() - ts))

    def get_upload(self):
        """ . """
        print("--------------------------------------------------- get_upload")
        ts = datetime.now()        
        self.upload = []
        for nia, dat, cod, val in self.variables:
            self.upload.append((nia, dat, cod, val))
        for nia, dat, cod, val in self.activitats:
            self.upload.append((nia, dat, cod, int(val)))
        for nia, dat, cod, val in self.ins:
            self.upload.append((nia, dat, cod, val))            
        for nia, dat, cod, val in self.maca:
            self.upload.append((nia, dat, cod, val))
        for nia, dat, cod, val in self.pcc:
            self.upload.append((nia, dat, cod, val))
        for nia, dat, cod, val in self.piic:
            self.upload.append((nia, dat, cod, val))
        for nia, dat, cod, val in self.cuidador:
            self.upload.append((nia, dat, cod, val))
        for nia, dat, cod, val in self.social:
            self.upload.append((nia, dat, cod, val))
        # print(self.upload)
        table = "sisap_recerca_p713_variables"
        cols = "(nia int, \
                dat date, \
                cod varchar2(15), \
                val number(6,3))"
        db = "redics"
        u.createTable(table, cols, db, rm=True)
        u.listToTable(self.upload, table, db)
        u.grantSelect(table, ("PREDULMB","PREDUPRP","PREDUMMP","PREDUEHE",
                              "PREDUMBO"), db)
        print('Time execution {} / nrow {}'.format(datetime.now() - ts,
                                                   len(self.upload)))

if __name__ == '__main__':
    ts = datetime.now()
    comorbilitats()
    print('Time execution {}'.format(datetime.now() - ts))