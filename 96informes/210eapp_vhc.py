# coding: latin1

"""
.
"""

import sisapUtils as u
import sisaptools as t


PERIODE = (20190101, 20190430)
FILE_OUT = u.tempFolder + "eapp.csv"


class EAPP(object):
    """."""

    def __init__(self):
        """."""
        self.db = "import_jail"
        self.get_poblacio()
        self.get_u11()
        self.get_codis()
        self.get_laboratori()
        self.get_resultat()
        self.export_data()

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec \
               from moviments a \
               where huab_data_ass between {} and {} and \
                     not exists (select 1 from moviments b \
                                 where a.id_cip_sec = b.id_cip_sec and \
                                       a.huab_data_ass = b.huab_data_final)".format(*PERIODE)  # noqa
        self.poblacio = set([id for id, in u.getAll(sql, self.db)])

    def get_u11(self):
        """."""
        sql = "select id_cip_sec, hash_d from u11"
        self.u11 = {id: hash for (id, hash) in u.getAll(sql, self.db)
                    if id in self.poblacio}

    def get_codis(self):
        """."""
        sql = "select codi from cat_dbscat where agrupador = 'V_VHC_CARREGA'"
        self.codis = tuple([cod for cod, in u.getAll(sql, "import")])

    def get_laboratori(self):
        """."""
        sql = "select id_cip_sec from laboratori \
               where cr_codi_prova_ics in {} and \
                     cr_data_reg <= {}".format(self.codis, PERIODE[1])
        self.laboratori = set([self.u11[id] for id, in u.getAll(sql, self.db)
                               if id in self.u11])

    def get_resultat(self):
        """."""
        sql = "select usua_cip from pdptb101 where usua_cip_cod = '{}'"
        with t.Database("redics", "pdp") as pdp:
            self.resultat = [pdp.get_one(sql.format(hash))
                             for hash in self.laboratori]

    def export_data(self):
        """."""
        u.writeCSV(FILE_OUT, sorted(self.resultat), sep=";")


if __name__ == "__main__":
    EAPP()
