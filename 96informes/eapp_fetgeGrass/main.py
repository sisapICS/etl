"""
    estudi Fetge Grass a pressons 4 actualitzacions
    + 1de variables de laboratori de seguiment  en acabar
"""

import sisapUtils as u
# import sisaptools as t
import datetime as d
import collections as c
import pprint as pp
import pandas as pd
import os

DATE_ARGS = (2021, 12, 15)
DEXT = d.date(*DATE_ARGS)
DEXT_T = str(DEXT)
NOD = "nodrizas"
JAIL = "import_jail"
JAIL_SRC = '6951a'
file_name = 'eapp_fetgeGrass_{DEXT}.xlsx'.format(DEXT=DEXT_T)
file_name = os.path.join(u.tempFolder, file_name)
# DEXTD = t.Database("p2262",
#                    NOD).get_one("select data_ext from dextraccio")[0]
# DEXTD = d.date(2021, 9, 15)
# DEXTD_T = str(DEXTD)


def calculate_age(born):
    return (
        DEXT.year -
        born.year -
        ((DEXT.month, DEXT.day) < (born.month, born.day))
        )


class Main(object):
    " clase principal a instanciar "

    def __init__(self):
        self.get_idConv()
        self.get_ingres()
        self.get_poblacio()
        # print(self.pob[-20931])
        self.get_personals()
        self.get_origen()
        self.get_fibroscan()
        self.get_var_ingres()
        self.get_tall_labs()
        self.get_tall_tto()
        self.get_tall_problemes()
        self.get_tall_atic()
        # pp.pprint(self.pob[-20931])
        for key in self.pob.keys()[:2]:
            print(key)
            pp.pprint(self.pob[key])
        columns = [
            'id',
            'up',
            'modul',
            'dat',
            'naix',
            'origen',
            'sex',
            'talla',
            'pes',
            'imc',
            'ac_vhc',
            'ac_vhc_dat',
            'ac_vih',
            'ac_vih_dat',
            'tto_vhc',
            'incl_hta',
            'incl_dm2',
            'incl_dislipemia',
            'incl_obesitat',
            'incl_imc_30',
            'incl_tto_antihta',
            'incl_tto_antidm',
            'incl_tto_estatines',
            'incl_obesitat_atic',
            'excl_hbsag',
            'excl_hbsag_dat',
            'excl_cortis_3mesos',
            'excl_hepatitis_alcoholica',
            'excl_hepatitis_autoimmune',
            'excl_cirrosi_hep',
            'excl_hepatocarcinoma',
            'excl_hemocromatosi',
            'excl_trasplant_fetge',
            'fibroscan_fet',
            'fibroscan_comm',
        ]
        self.data = pd.DataFrame.from_dict(self.pob,
                                           orient="index",
                                           columns=columns,
                                           )
        print(self.data)
        self.data['fibroscan_comm'] = self.data['fibroscan_comm'].str.decode('iso_8859-1').str.encode('utf-8')  # noqa
        self.data['id'] = self.data['id'].str.decode('iso_8859-1').str.encode('utf-8')  # noqa
        print(self.data[self.data['naix'].isnull()])
        self.data = self.data[self.data['naix'].notnull()]
        self.data['naix'] = self.data['naix'].apply(calculate_age)
        self.data.rename(columns={'naix': 'edat'}, inplace=True)
        self.data.to_excel(file_name, index=False)
        # pp.pprint(self.pob[-15458])
        # pp.pprint(self.pob[-10430])
        # pp.pprint(self.pob[-4094])

    def get_idConv(self):
        sql = """
            SELECT
                hash_d,
                id_cip_sec
            FROM
                u11
        """
        self.hsh2id = {hsh: id_cip for (hsh, id_cip) in u.getAll(sql, JAIL)}
        self.id2hsh = {id_cip: hsh for (hsh, id_cip) in self.hsh2id.items()}
        sql = """
            SELECT
                usua_cip,
                usua_cip_cod
            FROM
                pdptb101
        """
        self.hsh2cip = {hsh: cip for (cip, hsh) in u.getAll(sql, JAIL_SRC)}

    def get_ingres(self):
        sql = """
            SELECT
                id_cip_sec,
                ingres
            FROM
                jail_atesa
        """
        self.ingres = {id_cip: dat for (id_cip, dat) in u.getAll(sql, NOD)}

    def get_poblacio(self):
        # u.getTableColumns('moviments','import_jail')
        sql = """
            SELECT
                id_cip_sec,
                huab_up_codi,
                huab_uab_codi
                -- ,huab_data_ass
            FROM
                moviments
            WHERE
                DATE '{DEXT}' BETWEEN
                    huab_data_ass AND
                    if(huab_data_final = 0, DATE '{DEXT}', huab_data_final)
            ORDER BY
                id_cip_sec,
                huab_data_ass,
                if(huab_data_final = 0, DATE '{DEXT}', huab_data_final)
        """
        self.pob = {id_cip: {
            'id': None,
            'up': up,
            'modul': modul,
            'dat': None,
            'naix': None,
            'origen': None,
            'sex': None,
            'talla': None,
            'pes': None,
            'imc': None,
            'ac_vhc': None,
            'ac_vhc_dat': None,
            'ac_vih': None,
            'ac_vih_dat': None,
            'tto_vhc': None,
            'incl_hta': None,
            'incl_dm2': None,
            'incl_dislipemia': None,
            'incl_obesitat': None,
            'incl_imc_30': None,
            'incl_tto_antihta': None,
            'incl_tto_antidm': None,
            'incl_tto_estatines': None,
            'incl_obesitat_atic': None,
            'excl_hbsag': None,
            'excl_hbsag_dat': None,
            'excl_cortis_3mesos': None,
            'excl_hepatitis_alcoholica': None,
            'excl_hepatitis_autoimmune': None,
            'excl_cirrosi_hep': None,
            'excl_hepatocarcinoma': None,
            'excl_hemocromatosi': None,
            'excl_trasplant_fetge': None,
            'fibroscan_fet': None,
            'fibroscan_comm': None,
            }
                    for (id_cip, up, modul) in u.getAll(
                        sql.format(DEXT=DEXT_T), JAIL)
                    }
        for id_cip in self.pob:
            if id_cip in self.id2hsh:
                hsh = self.id2hsh[id_cip]
                self.pob[id_cip]['id'] = self.hsh2cip[hsh]
        for id_cip in self.pob:
            if id_cip in self.ingres:
                self.pob[id_cip]['dat'] = self.ingres[id_cip]

    def get_personals(self):
        # u.getTableColumns('assignada', 'import_jail')
        # u.getTableColumns('assignada_tot_with_jail', 'nodrizas')
        sql = """
            SELECT
                id_cip_sec,
                usua_data_naixement,
                usua_sexe
            FROM
                assignada
        """
        for id_cip, naix, sex in u.getAll(sql, JAIL):
            if id_cip in self.pob:
                self.pob[id_cip]['naix'] = naix
                self.pob[id_cip]['sex'] = sex

    def get_origen(self):
        sql = """
            SELECT
                usua_cip_cod,
                nvl(usua_pais_origen, usua_nacionalitat)
            FROM
                usutb040 a INNER JOIN pdptb101 u
                    ON a.usua_cip = u.usua_cip
        """
        for hsh, origen in u.getAll(sql, JAIL_SRC):
            id_cip = self.hsh2id.get(hsh)
            if id_cip in self.pob:
                self.pob[id_cip]['origen'] = origen

    def get_fibroscan(self):
        sql = """
            SELECT
                usua_cip_cod,
                vu_dat_act,
                vu_val,
                vu_com
            FROM
                prstb017 a INNER JOIN pdptb101 u
                    ON a.vu_cod_u = u.usua_cip
            WHERE
                vu_cod_vs = 'YC0001'
            ORDER BY
                usua_cip_cod,
                vu_dat_act
        """
        for hsh, dat, val, comm in u.getAll(sql, JAIL_SRC):
            id_cip = self.hsh2id.get(hsh)
            if id_cip in self.pob:
                self.pob[id_cip]['fibroscan_fet'] = val
                self.pob[id_cip]['fibroscan_comm'] = comm

    def get_var_ingres(self):
        u.getTableColumns('variables', JAIL)
        cods = {
            'TT101': 'talla',
            'TT102': 'pes',
            'TT103': 'imc',
            # 'YC0001': 'fibroscan_fet',
            }
        sql = """
            SELECT
                id_cip_sec,
                vu_cod_vs,
                vu_dat_act,
                vu_val
            FROM
                variables
            WHERE
                vu_cod_vs in {}
            ORDER BY
                id_cip_sec,
                vu_cod_vs,
                vu_dat_act
        """
        for id_cip, cod, dat, val in u.getAll(sql.format(tuple(cods)), JAIL):
            if id_cip in self.pob and self.pob[id_cip]['dat']:
                dat_ini = self.pob[id_cip]['dat']
                dat_fin = dat_ini + d.timedelta(days=7)
                if dat_ini <= dat <= dat_fin:
                    # print(id_cip, cod)
                    self.pob[id_cip][cods[cod]] = val
                if cod == 'TT103' and val >= 30:
                    self.pob[id_cip]['incl_imc_30'] = val

    def get_tall_atic(self):
        cod = "incl_obesitat_atic"
        sql = """
            SELECT id_cip_sec
            FROM import_jail.atic
            WHERE eli_codi_element = 'IRE03222'
            """
        for id_cip, in u.getAll(sql, JAIL):
            if id_cip in self.pob:
                self.pob[id_cip][cod] = True

    def get_tall_labs(self):
        u.getTableColumns('jail_serologies', 'nodrizas')
        vhc_cods = tuple([row for row, in u.getAll("select codi from cat_dbscat where agrupador = 'V_VHC_SEROLOGIA'","import")])  # noqa
        vhb_cods = tuple([row for row, in u.getAll("select codi from cat_dbscat where agrupador = 'V_VHB_SEROLOGIA_AG_S'","import")])  # noqa
        vih_cods = tuple([row for row, in u.getAll("select codi from cat_dbscat where agrupador = 'V_VIH_SEROLOGIA'","import")])  # noqa
        vhc = c.defaultdict(dict)
        vih = c.defaultdict(dict)
        vhb = c.defaultdict(dict)
        sql = """
            SELECT
                id_cip_sec,
                dat,
                max(val)
            FROM
                jail_serologies
            WHERE
                cod in {CODS} AND
                dat <= DATE '{DEXT}'
            group by
                id_cip_sec,
                dat
            order by
                id_cip_sec,
                dat
        """
        for id_cip, dat, val in u.getAll(sql.format(CODS=vhc_cods, DEXT=DEXT_T), NOD):  # noqa
            if id_cip in self.pob:
                val = int(val)
                dat = d.datetime.strptime(dat, '%Y%m%d')
                vhc[id_cip][dat] = val
        for id_cip, dat, val in u.getAll(sql.format(CODS=vih_cods, DEXT=DEXT_T), NOD):  # noqa
            if id_cip in self.pob:
                val = int(val)
                dat = d.datetime.strptime(dat, '%Y%m%d')
                vih[id_cip][dat] = val
        for id_cip, dat, val in u.getAll(sql.format(CODS=vhb_cods, DEXT=DEXT_T), NOD):  # noqa
            if id_cip in self.pob:
                val = int(val)
                dat = d.datetime.strptime(dat, '%Y%m%d')
                vhb[id_cip][dat] = val
        # print(vhc)
        # pp.pprint(vhc[-20931])
        # print(max(vhc[-20931]))
        max_vhc = {}
        max_vih = {}
        max_vhb = {}
        for id_cip in vhc:
            m_dat = max(vhc[id_cip])
            val = vhc[id_cip][m_dat]
            max_vhc[id_cip] = (m_dat, val)
        # print(max_vhc.get(-20931))
        for id_cip in vih:
            m_dat = max(vih[id_cip])
            val = vih[id_cip][m_dat]
            max_vih[id_cip] = (m_dat, val)
        # print(max_vih.get(-20931))
        for id_cip in vhb:
            m_dat = max(vhb[id_cip])
            val = vhb[id_cip][m_dat]
            max_vhb[id_cip] = (m_dat, val)
        for id_cip, (dat, val) in max_vhc.items():
            self.pob[id_cip]['ac_vhc'] = val
            self.pob[id_cip]['ac_vhc_dat'] = dat
        for id_cip, (dat, val) in max_vih.items():
            self.pob[id_cip]['ac_vih'] = val
            self.pob[id_cip]['ac_vih_dat'] = dat
        for id_cip, (dat, val) in max_vhb.items():
            self.pob[id_cip]['excl_hbsag'] = val
            self.pob[id_cip]['excl_hbsag_dat'] = dat
        # print(max_vhb.get(-20931))

    def get_tall_tto(self):
        recode = {
            'F_HIPOLIPEMIANTS': 'incl_tto_estatines',
            'F_MHDA_VHC': 'tto_vhc',
            'F_CORTIC_SISTEM': 'excl_cortis_3mesos',
            'F_HTA_ALFABLOQ': 'incl_tto_antihta',
            'F_HTA_ALTRES': 'incl_tto_antihta',
            'F_HTA_BETABLOQ': 'incl_tto_antihta',
            'F_HTA_CALCIOA': 'incl_tto_antihta',
            'F_HTA_COMBINACIONS': 'incl_tto_antihta',
            'F_HTA_DIURETICS': 'incl_tto_antihta',
            'F_HTA_IECA_ARA2': 'incl_tto_antihta',
            'F_DIAB_ADO': 'incl_tto_antidm',
            'F_DIAB_INSULINA': 'incl_tto_antidm',
            }
        tto_cods = {
            row: agr
            for row, agr in u.getAll(
                """
                select
                    codi, agrupador
                from
                    cat_dbscat
                where
                    agrupador in {}
                """.format(tuple(recode.keys())),
                "import")
            }
        sql = """
            SELECT
                id_cip_sec,
                ppfmc_atccodi,
                ppfmc_data_fi
            FROM
                tractaments
            WHERE
                ppfmc_atccodi in {}
        """
        for id_cip, cod, fi in u.getAll(
                sql.format(tuple(tto_cods.keys())), JAIL):
            new_cod = recode[tto_cods[cod]]
            dat_ref = DEXT + d.timedelta(days=-90) if new_cod == 'excl_cortis_3mesos' else DEXT  # noqa
            if id_cip in self.pob:
                # dat_ini = self.pob[id_cip]['dat']
                if new_cod in ('incl_tto_antihta',
                               'incl_tto_antidm',
                               'excl_cortis_3mesos'):
                    if fi >= dat_ref:
                        self.pob[id_cip][new_cod] = True
                # if new_cod == 'excl_cortis_3mesos':
                #     dat_pre = DEXT + d.timedelta(days=-90)
                #     if fi >= dat_pre and id_cip in (-10430, -4094):
                #         print(id_cip, cod, fi, dat_ini, dat_pre)
                #         self.pob[id_cip][new_cod] = True
                # if new_cod in ('incl_tto_antihta', 'incl_tto_antidm'):
                #     if fi >= DEXT:
                #         self.pob[id_cip][new_cod] = True
                else:
                    self.pob[id_cip][new_cod] = True

    def get_tall_problemes(self):
        """."""
        recode = {
            'PS_HTA': 'incl_hta',
            'PS_DIABETIS2': 'incl_dm2',
            'PS_DISLIPEMIA': 'incl_dislipemia',
            'PS_OBESITAT': 'incl_obesitat',
            'PS_DEFICIT_GH': 'excl_hemocromatosi',
            'PS_HEPATOPATIA': 'PS_HEPATOPATIA',
            'PS_NEOPLASIA_M': 'PS_NEOPLASIA_M',
            'D28': 'excl_trasplant_fetge',
            }
        cirrosis = (
            'C01-K70.3',
            'C01-K70.30',
            'C01-K70.31',
            'C01-K71.7',
            'C01-K74',
            'C01-K74.3',
            'C01-K74.4',
            'C01-K74.5',
            'C01-K74.6',
            'C01-K74.60',
            'C01-K74.69',
        )
        ps_cods = {
            cod: agr
            for cod, agr in u.getAll("""
                SELECT
                    codi_cim10,
                    if(codi_cim10 = 'C01-Z94.4',
                       codi_ciap_m,
                       agrupador) AS agrupador
                FROM
                    cat_md_ct_cim10_ciap m LEFT JOIN
                    cat_dbscat c
                        ON m.codi_ciap_m = c.codi
                WHERE
                    c.agrupador in {}
                    OR codi_cim10 = 'C01-Z94.4'
                """.format(tuple(recode.keys())),
                "import")
            }
        sql = """
            SELECT
                id_cip_sec, pr_cod_ps
            FROM
                problemes
            WHERE
                pr_cod_ps in {}
        """.format(tuple(ps_cods.keys()))
        for id_cip, cod in u.getAll(sql, JAIL):
            if id_cip in self.pob:
                if recode[ps_cods[cod]] == 'excl_hemocromatosi':
                    if 'C01-E83.11' in cod:
                        new_cod = recode[ps_cods[cod]]
                        self.pob[id_cip][new_cod] = True
                elif recode[ps_cods[cod]] == 'PS_NEOPLASIA_M':
                    if 'C22' in cod:
                        new_cod = 'excl_hepatocarcinoma'
                        self.pob[id_cip][new_cod] = True
                elif recode[ps_cods[cod]] == 'PS_HEPATOPATIA':
                    if cod in cirrosis:
                        new_cod = 'excl_cirrosi_hep'
                        self.pob[id_cip][new_cod] = True
                    if 'K70' in cod:
                        new_cod = 'excl_hepatitis_alcoholica'
                        self.pob[id_cip][new_cod] = True
                    if 'K75.4' in cod:
                        new_cod = 'excl_hepatitis_autoimmune'
                        self.pob[id_cip][new_cod] = True
                else:
                    new_cod = recode[ps_cods[cod]]
                    self.pob[id_cip][new_cod] = True


if __name__ == '__main__':
    Main()
