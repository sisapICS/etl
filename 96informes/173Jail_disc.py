# coding: iso-8859-1

from sisapUtils import *
from datetime import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

codis = "('F79.9', 'R62.0', 'Z73.6', 'F79', 'F79.0', 'F79.1', 'F79.8')"

tb = "SISAP_DISCAPACITAT_EAPP"

class jail_disc(object):
    """."""
    
    def __init__(self):
        """."""
        self.dades = []
        self.get_catalegs()
        self.cip_to_hash()
        self.id_to_hash()
        self.get_centres()
        self.get_problemes()
        self.get_pacients()
        self.upload_data()
        
        
    def get_catalegs(self):
        """."""
        sql = ("select ps_cod, ps_des from cat_prstb001 where ps_cod in {}".format(codis), "import")
        self.cim10 = {(ps_cod): (ps_des) for (ps_cod, ps_des)
                        in getAll(*sql)}
        
        sql = ("select pst_th, pst_des from cat_prstb305", "import")
        self.thes = {(ps_cod): (ps_des) for (ps_cod, ps_des)
                        in getAll(*sql)} 
                        
    def cip_to_hash(self):
        """."""
        self.hashos1 = {}
        sql = "select usua_cip, usua_cip_cod from pdptb101"
        for c,h in getAll(sql, 'pdp'):
            self.hashos1[(c)] = h
    
    def id_to_hash(self):
        """."""
        self.hashos2 = {}
        sql = "select id_cip_sec, hash_d from u11"
        for i, h in getAll(sql, "import_jail"):
            self.hashos2[(i)] = h
            
    def get_centres(self):
        """."""
        self.centres = {}
        sql = "select scs_codi, ics_desc from jail_centres"
        for up, desc in getAll(sql, "nodrizas"):
            self.centres[up] = desc
    
    def get_problemes(self):
        """."""
        a, b = 0, 0
        self.problemes=defaultdict(set)
        sql = "select pr_cod_u, pr_cod_ps, pr_dde, pr_com, pr_th, pr_data_baixa, pr_dba from prstb015 \
                    where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_data_baixa is null  and pr_cod_ps in {}".format(codis)
        for cip, ps, dde, com, th, baixa, db in getAll(sql, "6951"):
            b += 1
            try:
                hash = self.hashos1[cip]
            except KeyError:
                continue
            self.problemes[hash].add((ps, dde, com, th, db))
            a += 1
        print b, a
    
    def get_pacients(self):
        """."""
        a = 0
        sql = "select id_cip_sec, up, edat, sexe from jail_assignada"
        for id, up, edat, sexe in getAll(sql, "nodrizas"):
            hash = self.hashos2[id]
            up_desc = self.centres[up] 
            if hash in self.problemes:
                a += 1
                for ps, dde, com, th, db in self.problemes[hash]:
                    ps_des = self.cim10[ps]
                    th_des = self.thes[th] if th in self.thes else None
                    self.dades.append([hash, up, up_desc, int(edat), sexe, ps, ps_des, th, th_des, com, dde,  db])
        print a        
        
    def upload_data(self):
        """."""
        columns = ["hash varchar(40)", "up varchar2(5)", "up_desc varchar2(150)",  "edat number",  "sexe varchar2(4)", "cim10 varchar2(10)", "cim10_des varchar(2150)", "th varchar2(10)", "th_des varchar2(150)", "comentari varchar2(500)", "dde date", "d_baixa date"]
        createTable(tb, "({})".format(", ".join(columns)), "redics", rm=True)
        listToTable(self.dades, tb, "redics")
        grantSelect(tb, ("PREDUMMP", "PREDUPRP", "PREDUECR"), "redics")
        
if __name__ == '__main__':
    jail_disc()