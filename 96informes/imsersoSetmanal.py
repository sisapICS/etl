# coding: utf-8

"""
 - TIME EXECUTION m

 - PETICIÓ:
    - Trello: https://trello.com/c/dpLyFW0j
    - Drive:

"""

import sisapUtils as u
# import csv, os, sys
import collections as c
from datetime import datetime
import datetime as d
from dateutil.relativedelta import relativedelta
from os import remove

# TODAY = d.datetime.now().date() - relativedelta(days = 1)
# DIAEXEC = d.date(2021, 2, 10)
DIAEXEC = d.datetime.now().date()
DATE1 = DIAEXEC - relativedelta(days=3)
DATE0 = DIAEXEC - relativedelta(days=9)
DATE0METRICA = DATE1 - relativedelta(days=21)
DATE1METRICA = DATE1 - relativedelta(days=15)

DATES = (DATE0, DATE1)
print(DIAEXEC)
print(DATES)
print(DATE0METRICA)
print(DATE1METRICA)


class imsersoSetmanal(object):
    """."""
    def __init__(self):
        """."""

        self.get_cat_res()
        self.get_indicador_11()
        self.get_indicador_12()
        self.get_indicador_13()
        self.get_indicador_14()
        self.get_indicador_15()
        self.get_indicador_16()
        self.get_indicador_21()
        self.get_indicador_22()
        self.get_exitus()
        self.get_indicador_32()
        self.get_upload()
        self.export_upload()

    def get_cat_res(self):
        """."""
        print("------------------------------------------ get_cat_residencies")
        self.cat_res = {}
        sql = """select
                    cod
                    , tipus
                 from
                    sisap_covid_cat_residencia
              """
        for cod, tip in u.getAll(sql, "redics"):
            self.cat_res[cod] = (tip)
        print("RESIDENCIES CATALEG: N de registros:", len(self.cat_res))

    def get_res_cens(self):
        """ . """
        print("------------------------------------------------- get_res_cens")
        self.res_cens = {}
        sql = """select
                    data, perfil, cip, residencia
                 from
                    dwsisap.residencies_cens
                 where
                    data >= to_date('20200314','yyyymmdd')
                    and perfil = 'Resident'"""
        for data, perfil, cip13, residencia in u.getAll(sql, 'exadata'):
            self.res_cens[(cip13, data)] = {'perfil': perfil,
                                            'residencia': residencia}
        print("RESIDENCIES CENS: N de registros:", len(self.res_cens))

    def get_indicador_11(self):
        """ . """
        print("--------------------------------------------- get_indicador_11")
        self.ind_11 = []
        sql = """
            select
                'R1' as res_tip,
                sum(dia_poblacio)
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Resident'
                and data = date '{}'
            UNION
            select
                'R2' as res_tip,
                sum(dia_poblacio)
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Resident'
                and data = date '{}'
            """.format(DATE1.strftime('%Y-%m-%d'),
                       DATE1.strftime('%Y-%m-%d'))
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '1.1 - N de residentes en el periodo'
            self.ind_11.append((ind, res_tip, N))
        print("INDICADOR 1.1: N de registros:", len(self.ind_11))
        print(self.ind_11)

    def get_indicador_12(self):
        """ . """
        print("--------------------------------------------- get_indicador_12")
        self.ind_12 = []
        sql = """
            select
                'R1' as res_tip,
                sum(setmana_casos) as casos
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Resident'
                and data = date '{}'
            union
            select
                'R2' as res_tip,
                sum(setmana_casos) as casos
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Resident'
                and data = date '{}'
            """.format(DATE1.strftime('%Y-%m-%d'),
                       DATE1.strftime('%Y-%m-%d'))
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '1.2 - N de casos confirmados por PDIA de COVID-19 en residentes en el periodo'  # noqa
            self.ind_12.append((ind, res_tip, N))
        print("INDICADOR 1.2: N de registros:", len(self.ind_12))
        print(self.ind_12)

    def get_indicador_13(self):
        """ . """
        print("--------------------------------------------- get_indicador_13")
        self.dbc_metrica = []
        self.ind_13_resultat = c.Counter()
        self.ind_13 = []
        sql = """
            SELECT
                b.CIP, data, pdia_primer_positiu, residencia
            FROM
                dwsisap.DBC_METRIQUES b,
                (select * from dwsisap.residencies_cens where data = to_date('{}','yyyymmdd') and perfil='Resident') c 
            WHERE
                c.hash=b.hash(+)
                AND pdia_primer_positiu BETWEEN to_date('{}','yyyymmdd')
                AND to_date('{}','yyyymmdd')
                AND (exitus_covid IS null 
                    or exitus_covid >= to_date('{}','yyyymmdd'))""".format(DATE1.strftime('%Y%m%d'),
                                                                           DATE0METRICA.strftime('%Y%m%d'),
                                                                           DATE1METRICA.strftime('%Y%m%d'),
                                                                           DATE1.strftime('%Y%m%d'))
        print(sql)        
        for cip13, data, pdia_data, residencia in u.getAll(sql, 'exadata'):
            self.dbc_metrica.append((cip13, data, pdia_data, residencia))
        print("INDICADOR 1.3: DBC METRICA:", len(self.dbc_metrica))            
        res_tipc = {1: "R1",
                    7: "R2",
                    8: "R2"}
        for cip13, _data, _pdia_data, residencia in self.dbc_metrica:
            self.ind_13_resultat[('DBC')] += 1
            if residencia in self.cat_res:
                self.ind_13_resultat[('DBC + CAT_RES')] += 1
                if self.cat_res[residencia] in (1, 7, 8):
                    self.ind_13_resultat[('DBC + CAT_RES + 178')] += 1
                    res_tip = res_tipc[self.cat_res[residencia]]
                    self.ind_13_resultat[(res_tip)] += 1
        print(self.ind_13_resultat)
        ind = '1.3 - N total de residentes que han superado la inf. en el periodo'  # noqa
        for res_tip, n in self.ind_13_resultat.items():
            if res_tip in ('R1','R2'):
                self.ind_13.append((ind, res_tip, n))
        print(self.ind_13)
        print("INDICADOR 1.3: N de registros:", len(self.ind_13))

    def get_indicador_14(self):
        """ . """
        print("--------------------------------------------- get_indicador_14")
        self.ind_14 = []
        sql = """
            select
                'R1' as res_tip,
                count(distinct(residencia))
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Resident'
                and data = date '{}'
                and setmana_casos > 0
            UNION
            select
                'R2' as res_tip,
                count(distinct(residencia))
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Resident'
                and data = date '{}'
                and setmana_casos > 0
            """.format(DATE1.strftime('%Y-%m-%d'),
                       DATE1.strftime('%Y-%m-%d'))
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '1.4 - N de centros que han tenido casos confirmados (PDIA) de residentes en el periodo'  # noqa
            self.ind_14.append((ind, res_tip, N))
        print("INDICADOR 1.4: N de registros:", len(self.ind_14))
        print(self.ind_14)

    def get_indicador_15(self):
        """ . """
        print("--------------------------------------------- get_indicador_15")
        self.ind_15 = []
        sql = """
            select
                'R1' as res_tip,
                count(distinct(residencia)) as n
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Resident'
                and data between date '{}' and date '{}'
            UNION
            select
                'R2' as res_tip,
                count(distinct(residencia)) as n
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Resident'
                and data between date '{}' and date '{}'
            """.format(DATE0.strftime('%Y-%m-%d'),
                       DATE1.strftime('%Y-%m-%d'),
                       DATE0.strftime('%Y-%m-%d'),
                       DATE1.strftime('%Y-%m-%d'))
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '1.5 - N de centros sobre los que se reporta en el periodo'
            self.ind_15.append((ind, res_tip, N))
        print("INDICADOR 1.5: N de registros:", len(self.ind_15))
        print(self.ind_15)

    def get_indicador_16(self):
        """ . """
        print("--------------------------------------------- get_indicador_16")
        self.ind_16 = []
        sql = """
            select
                'R1' as res_tip,
                (sum(setmana_pcr) + sum(setmana_tar)) as N
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Resident'
                and data = date '{}'
            UNION
            select
                'R2' as res_tip,
                (sum(setmana_pcr) + sum(setmana_tar)) as N
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Resident'
                and data = date '{}'
            """.format(DATE1.strftime('%Y-%m-%d'),
                       DATE1.strftime('%Y-%m-%d'))
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '1.6 - N de residentes a los que se les ha realizado prueba de COVID-19 (PCR o TAR) en el periodo'  # noqa
            self.ind_16.append((ind, res_tip, N))
        print("INDICADOR 1.6: N de registros:", len(self.ind_16))
        print(self.ind_16)

    def get_indicador_21(self):
        """ . """
        print("--------------------------------------------- get_indicador_21")
        self.ind_21 = []
        sql = """
            select
                'R1' as res_tip,
                sum(setmana_casos) as casos
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Personal'
                and data = date '{}'
            UNION
            select
                'R2' as res_tip,
                sum(setmana_casos) as casos
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Personal'
                and data = date '{}'
            """.format(DATE1.strftime('%Y-%m-%d'),
                       DATE1.strftime('%Y-%m-%d'))
        # print(sql)
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '2.1 - N de casos confirmados COVID-19 por PDIA del personal en el periodo'  # noqa
            self.ind_21.append((ind, res_tip, N))
        print("INDICADOR 2.1: N de registros:", len(self.ind_21))
        print(self.ind_21)

    def get_indicador_22(self):
        """ . """
        print("--------------------------------------------- get_indicador_22")
        self.ind_22 = []
        sql = """
            select
                'R1' as res_tip,
                sum(dia_poblacio)
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Personal'
                and data = date '{}'
            UNION
            select
                'R2' as res_tip,
                sum(dia_poblacio)
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Personal'
                and data = date '{}'
            """.format(DATE1.strftime('%Y-%m-%d'),
                       DATE1.strftime('%Y-%m-%d'))
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '2.2 - Plantilla total'
            self.ind_22.append((ind, res_tip, N))
        print("INDICADOR 2.2: N de registros:", len(self.ind_22))
        print(self.ind_22)

    def get_indicador_31(self):
        """ . """
        print("--------------------------------------------- get_indicador_31")
        self.ind_31 = []
        ind = '3.1 - Fallecimientos (acumulado desde el 14 de marzo por todas las causas hasta la fecha de reporte)'  # noqa
        res_tip = ''
        N = 'NO DISPONIBLE'
        self.ind_31.append((ind, res_tip, N))
        print("INDICADOR 3.1: N de registros:", len(self.ind_31))
        print(self.ind_31)

    def get_exitus(self):
        """ . """
        print("--------------------------------------------------- get_exitus")
        self.exitus = {}
        sql = """select
                    substr(a.cip, 0, 13) as cip13,
                    afectat, data_exitus, lloc_exitus_categoritzat,
                    perfil, residencia
               from
                    dwaquas.f_covid19_v2 a, dwsisap.residencies_cens b
                where
                    substr(a.cip, 0, 13) = b.cip
                    and a.data_exitus = b.data
                    and exitus='Si'
                    and data_exitus between date '{}'
                                        and date '{}'
                    and perfil ='Resident'
                """.format(DATE0.strftime('%Y-%m-%d'),
                           DATE1.strftime('%Y-%m-%d'))
        for cip13, afectat, data, lloc, perfil, residencia in u.getAll(sql, 'exadata'):  # noqa
            self.exitus[(cip13, data, afectat, lloc, perfil, residencia)] = True  # noqa
        print("EXITUS: N de registros:", len(self.exitus))

    def get_indicador_32(self):
        """ . """
        print("--------------------------------------------- get_indicador_32")
        self.resultat = c.Counter()
        self.res_tip_no178 = set()
        self.ind_32 = []
        afectatc = {"Sospit\xf3s": "Compatible",
                    "Sospitós": "Compatible",
                    "Positiu per ELISA": "Confirmat",
                    "Positiu": "Confirmat",
                    "Positiu Epidemiol\xf2gic": "Compatible",
                    "Positiu Epidemiològic": "Compatible",
                    "Positiu per Test R\xe0pid": "Confirmat",
                    "Positiu per Test Ràpid": "Confirmat",
                    "PCR probable": "Compatible"}
        res_tipc = {1: "R1",
                    7: "R2",
                    8: "R2"}
        for cip13, _data, afectat, lloc, _perfil, residencia in self.exitus:
            if residencia in self.cat_res:
                if self.cat_res[residencia] in (1, 7, 8):
                    res_tip = res_tipc[self.cat_res[residencia]]
                    if afectatc[afectat] == 'Confirmat':                        
                        self.resultat[(res_tip, lloc,'Confirmat')] += 1
                    else:
                        self.resultat[(res_tip, lloc,'No Confirmat')] += 1
                else:
                    self.res_tip_no178.add((cip13))
        # print(self.resultat)
        print("No Tip 1-7-8:", len(self.res_tip_no178))
        for (res_tip, lloc, confirmat), N in self.resultat.items():
            if confirmat == 'Confirmat':
                if lloc == 'Hospital':
                    ind = '3.2b - N de fallecimientos de residentes por COVID-19 confirmado en el periodo - HOSPITAL'  # noqa
                    self.ind_32.append((ind, res_tip, N))
                if lloc in ('Residència', 'Resid\xe8ncia'):
                    ind = '3.2a - N de fallecimientos de residentes por COVID-19 confirmado en el periodo - CENTRO'  # noqa
                    self.ind_32.append((ind, res_tip, N))
                if lloc == 'Domicili':
                    ind = '3.2c - N de fallecimientos de residentes por COVID-19 confirmado en el periodo - DOMICILIO'  # noqa
                    self.ind_32.append((ind, res_tip, N))
                if lloc == 'Sociosanitari':
                    ind = '3.2d - N de fallecimientos de residentes por COVID-19 confirmado en el periodo - SOCIOSANITARIO'  # noqa
                    self.ind_32.append((ind, res_tip, N))
                if lloc == 'No classificat':
                    ind = '3.2e - N de fallecimientos de residentes por COVID-19 confirmado en el periodo - NO CLASIFICADO'  # noqa
                    self.ind_32.append((ind, res_tip, N))
            if confirmat == "No Confirmat":
                if lloc == 'Hospital':
                    ind = '3.2x - N de fallecimientos de residentes por COVID-19 No confirmado en el periodo - HOSPITAL'  # noqa
                    self.ind_32.append((ind, res_tip, N))
                if lloc in ('Residència', 'Resid\xe8ncia'):
                    ind = '3.2x - N de fallecimientos de residentes por COVID-19 No confirmado en el periodo - CENTRO'  # noqa
                    self.ind_32.append((ind, res_tip, N))
                if lloc == 'Domicili':
                    ind = '3.2x - N de fallecimientos de residentes por COVID-19 No confirmado en el periodo - DOMICILIO'  # noqa
                    self.ind_32.append((ind, res_tip, N))
                if lloc == 'Sociosanitari':
                    ind = '3.2x - N de fallecimientos de residentes por COVID-19 No confirmado en el periodo - SOCIOSANITARIO'  # noqa
                    self.ind_32.append((ind, res_tip, N))
                if lloc == 'No classificat':
                    ind = '3.2x - N de fallecimientos de residentes por COVID-19 No confirmado en el periodo - NO CLASIFICADO'  # noqa
                    self.ind_32.append((ind, res_tip, N))
        print("INDICADOR 3.2: N de registros:", len(self.ind_32))
        print(self.ind_32)

    def get_upload(self):
        """ . """
        print("--------------------------------------------------- get_upload")
        self.upload = []
        for ind, res_tip, N in self.ind_11:
            self.upload.append((ind, res_tip, N))
        for ind, res_tip, N in self.ind_12:
            self.upload.append((ind, res_tip, N))
        for ind, res_tip, N in self.ind_13:
            self.upload.append((ind, res_tip, N))
        for ind, res_tip, N in self.ind_14:
            self.upload.append((ind, res_tip, N))
        for ind, res_tip, N in self.ind_15:
            self.upload.append((ind, res_tip, N))
        for ind, res_tip, N in self.ind_16:
            self.upload.append((ind, res_tip, N))
        for ind, res_tip, N in self.ind_21:
            self.upload.append((ind, res_tip, N))
        for ind, res_tip, N in self.ind_22:
            self.upload.append((ind, res_tip, N))
        for ind, res_tip, N in self.ind_32:
            self.upload.append((ind, res_tip, N))

    def export_upload(self):
        """."""
        print("------------------------------------------------ export_upload")
        file = u.tempFolder + 'imsersoSetmanal_{}.csv'
        file = file.format(DATE1.strftime('%Y%m%d'))
        u.writeCSV(file,
                   [('Indicador', 'Tipus de centre', 'N')] + self.upload,
                   sep=";")
        print("Send Mail")
        subject = 'Imserso - Fitxa setmanal'
        text = 'Adjuntem arxiu amb les dades per la fitxa setmanal'
        u.sendGeneral('SISAP <sisap@gencat.cat>',
                      'avilar@gencat.cat',
                      'ehermosilla@idiapjgol.info',
                      subject,
                      text,
                      file)
        remove(file)


if __name__ == '__main__':
    print(datetime.now())
    ts = datetime.now()
    imsersoSetmanal()
    print('Time execution {}'.format(datetime.now() - ts))
