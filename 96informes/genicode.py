# coding: latin1


import sisapUtils as u
import collections as c


# cols = """(patient varchar2(40), up varchar2(40), up_desc varchar2(400), uba varchar2(5), tabaquisme varchar2(100))"""
# u.createTable('rcv_genicode', cols, 'exadata', rm=True)

# sql = """select id_cip_sec, up, uba 
#     from nodrizas.assignada_tot
#     where edat between 45 and 65
#     and maca = 0"""
# poblacio = {}
# for id, up, uba in u.getAll(sql, 'nodrizas'):
#     poblacio[id] = [up, uba]

# exclusions = set()
# sql = """select ID_CIP_sEC from nodrizas.eqa_problemes ep 
#         where ps in (1, 211, 7, 212, 621, 11, 213, 996, 528, 439, 53)"""
# for id, in u.getAll(sql, 'nodrizas'):
#     exclusions.add(id)

# sql = """select id_cip_sec from nodrizas.eqa_variables ev 
#     where agrupador = 10
#     and usar = 1
#     and valor between 5 and 9.9
#     and data_var >= '2023-09-25'"""
# rcv = set()
# for id, in u.getAll(sql, 'nodrizas'):
#     rcv.add(id)

# sql = """select id_cip_sec from nodrizas.eqa_variables ev 
#     where agrupador = 30
#     and usar = 1
#     and valor < 29
#     and data_var >= '2022-09-25'"""
# fg = set()
# for id, in u.getAll(sql, 'nodrizas'):
#     fg.add(id)

# up_2_desc = {}
# sql = """select ics_desc, scs_codi from nodrizas.cat_centres where sap_codi  in ('24', '25', '28') or amb_codi = '01'"""
# for desc, up in u.getAll(sql, 'nodrizas'):
#     up_2_desc[up] = desc 

# tabaquisme = {}
# sql = """select id_cip_sec, tab,
#             case when dlast >= date_add(data_ext, interval - 12 month) then 'menys' else 'mes' end temporalitat
#             from nodrizas.eqa_tabac et, nodrizas.dextraccio d  
#             where last = 1"""
# for id, tabac, data in u.getAll(sql, 'nodrizas'):
#     if tabac == 0: txt = 'mai'
#     elif tabac == 1: txt = 'fumador'
#     elif tabac == 2:
#         if data == 'menys': txt = 'exfumador menys 1 any'
#         else: txt = 'exfumador mes 1 any'
#     tabaquisme[id] = txt

# converters = {}
# sql = """SELECT HASH_REDICS, HASH_COVID FROM dwsisap.PDPTB101_RELACIO"""
# redics_2_exadata = {hash_redics: hash_exadata for (hash_redics, hash_exadata) in u.getAll(sql, 'exadata')}
# sql = """select id_cip_sec, hash_d from import.u11"""
# for id, hash in u.getAll(sql, 'import'):
#     converters[id] = hash

# uploading = c.Counter()
# patients_list = []
# for id in rcv:
#     if id in converters:
#         hash = converters[id]
#         if hash in redics_2_exadata:
#             hash_covid = redics_2_exadata[hash]
#             if id in poblacio:
#                 up, uba = poblacio[id]
#                 if id in tabaquisme:
#                     tabac = tabaquisme[id]
#                 else:
#                     tabac = ''
#                 if up in up_2_desc:
#                     desc = up_2_desc[up]
#                     if id not in exclusions:
#                         if id not in fg:
#                             uploading[up] += 1
#                             patients_list.append((hash_covid, up, desc, uba, tabac))

# # upload = [(sap, n) for sap, n in uploading.items()]
# u.listToTable(patients_list, 'rcv_genicode', 'exadata')
# grant_sql = "grant select on rcv_genicode to DWSISAP_ROL"
# u.execute(grant_sql, 'exadata')
# sql = """CREATE  INDEX pac_genincode ON dwsisap.rcv_genicode (patient)"""
# u.execute(sql, 'exadata')
# sql = """CREATE INDEX pac_dbs ON dwsisap.dbs (c_cip)"""
# u.execute(sql, 'exadata')
# sql = """CREATE INDEX prof_dbs ON dwsisap.dbs (c_metge)"""
# u.execute(sql, 'exadata')
# sql = """CREATE INDEX up_dbs ON dwsisap.dbs (c_up)"""
# u.execute(sql, 'exadata')
# sql = """CREATE INDEX prof_prof ON dwsisap.PROFESSIONALS (uab)"""
# u.execute(sql, 'exadata')
# sql = """CREATE INDEX up_prof ON dwsisap.PROFESSIONALS (up)"""
# u.execute(sql, 'exadata')

# file_name = 'genincode.txt'
# sql = """SELECT
#             c_cip,
#             c_up,
#             up_desc,
#             c_metge,
#             uab_descripcio,
#             c_sexe,
#             C_DATA_NAIX,
#             CASE
#                 WHEN PS_DIABETIS1_DATA IS NOT NULL THEN 'DM1'
#                 WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 'DM2'
#                 ELSE 'no'
#             END DM,
#             CASE
#                 WHEN V_TAS_DATA >=  sysdate - 180 THEN v_TAS_VALOR
#                 ELSE 0
#             END TAS,
#             CASE
#                 WHEN V_TAD_DATA >=  sysdate - 180 THEN v_TAD_VALOR
#                 ELSE 0
#             END TAD,
#             v_peso_valor,
#             v_talla_valor,
#             CASE
#                 WHEN V_COL_TOTAL_DATA >=  sysdate - 180 THEN v_col_total_valor
#                 ELSE 0
#             END COLESTEROL_TOTAL,
#             CASE
#                 WHEN V_COL_LDL_DATA >=  sysdate - 180 THEN v_col_LDL_valor
#                 ELSE 0
#             END COLESTEROL_LDL,
#             CASE
#                 WHEN V_COL_HDL_DATA >=  sysdate - 180 THEN v_col_HDL_valor
#                 ELSE 0
#             END COLESTEROL_HDL,
#             CASE
#                 WHEN v_triglic_DATA >=  sysdate - 180 THEN v_triglic_valor
#                 ELSE 0
#             END TRIGLICERIDS,
#             tabaquisme
#         FROM
#             (
#             SELECT
#                 *
#             FROM
#                 dwsisap.dbs
#             WHERE
#                 V_COL_TOTAL_DATA >= sysdate - 180
#                 AND V_COL_LDL_DATA >= sysdate - 180
#                 AND V_COL_HDL_DATA >=  sysdate - 180
#                 AND v_triglic_DATA >= sysdate - 180
#                 AND V_TAS_DATA >= sysdate - 180
#                 AND V_TAD_DATA >= sysdate - 180
#                 AND c_sap IN ('24', '25', '28', '03', '04')) dbs
#         INNER JOIN
#                     dwsisap.rcv_genicode a ON
#             dbs.c_cip = a.patient
#         INNER JOIN (
#             SELECT
#                 *
#             FROM
#                 dwsisap.PROFESSIONALS
#             WHERE
#                 tipus = 'M') p
#                     ON
#             dbs.c_metge = p.uab
#             AND dbs.c_up = p.up"""
# upload = [row for row in u.getAll(sql, 'exadata')]
# u.writeCSV(u.tempFolder+file_name, upload, sep=',')

# # u.ftp_upload('sitabapp.genincode.com', 'catsalut', '65?Kc9YPwj', u.tempFolder, file_name, ftp_folder='/', rm=False, TLS=True)







cols = """(hash varchar2(40), up varchar2(5), up_desc varchar2(500), uba varchar2(5), uba_desc varchar2(500),
            ubainf varchar2(5), inf_desc varchar2(500),
            edat number, data_naix date, sexe varchar2(5),
            col_total number, hdl number, ldl number, tas number, tad number, tabac varchar2(500),
            dm varchar2(50), pes number, talla number, triglic number, rcv number)"""
u.createTable('rcv_genicode_1_2', cols, 'redics', rm=True)


print('dbs')
pacients_candidats = {}
sql = """
        SELECT 
        C_CIP,
        C_UP,
        C_METGE,
        C_INFERMERA,
        (SYSDATE - C_DATA_NAIX)/365.25 AS edat,
        C_DATA_NAIX,
        C_SEXE,
        V_COL_TOTAL_VALOR,
        V_COL_HDL_VALOR,
        V_COL_LDL_valor,
        V_TAS_VALOR,
        V_TAD_VALOR,
        CASE WHEN VC_TABAC_ARA_VALOR = 'S�' THEN 'fumador'
        	WHEN VC_TABAC_ARA_VALOR = 'No' THEN 'no fumador'
        	WHEN VC_TABAC_ARA_VALOR = 'Exfumador' AND vc_tabac_ara_data < sysdate -365 THEN 'exfumador mes 1 any'
        	ELSE 'exfumador menys 1 any'
        	END tabac,
        CASE WHEN PS_DIABETIS1_DATA IS NOT NULL THEN 1 
            WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 1
            ELSE 0 END DM_global,
        CASE
                    WHEN PS_DIABETIS1_DATA IS NOT NULL THEN 'DM1'
            WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 'DM2'
            ELSE 'no'
        END DM,
        v_peso_valor,
        v_talla_valor,
        v_triglic_valor
        FROM DBS
        WHERE
        V_COL_TOTAL_DATA >= sysdate - 180
        AND V_COL_LDL_DATA >= sysdate - 180
        AND V_COL_HDL_DATA >=  sysdate - 180
        AND v_triglic_DATA >= sysdate - 180
        AND V_TAS_DATA >= sysdate - 180
        AND V_TAD_DATA >= sysdate - 180
        AND v_triglic_data >= sysdate - 180
        AND c_sap IN ('24', '25', '28', '03', '04', '38', '39', '59')
        AND C_EDAT_ANYS BETWEEN 45 AND 65
        """
for hash, up, uba, ubainf, edat, data_naix, sexe, col_total, hdl, ldl, tas, tad, tabac, dm_global, dm, pes, talla, triglic in u.getAll(sql, 'redics'):
    if sexe == 'H':
        p_col_total = 0
        if col_total < 160: p_col_total = -0.65945
        elif col_total >= 160 and col_total < 200: p_col_total = 0
        elif col_total >= 200 and col_total < 240: p_col_total = 0.17692
        elif col_total >= 240 and col_total < 280: p_col_total = 0.50539 
        elif col_total >= 280: p_col_total = 0.65713
        p_hdl = 0
        if hdl < 35: p_hdl = 0.49744
        elif hdl >= 35 and hdl < 45: p_hdl = 0.2431
        elif hdl >= 45 and hdl < 50: p_hdl = 0
        elif hdl >= 50 and hdl < 60: p_hdl = -0.05107 
        elif hdl >= 60: p_hdl = -0.4866
        p_tensio = 0
        if tas < 120 and tad < 80: p_tensio = -0.00226
        elif tas < 130 and tad < 85: p_tensio = 0
        elif tas < 140 and tad < 90: p_tensio = 0.2832
        elif tas < 160 and tad < 100: p_tensio = 0.52168
        elif tas >= 160 or tad >= 100: p_tensio = 0.61859
        p_diabetic = 0.42839 if dm_global else 0
        p_fumador = 0.52337 if tabac in ('fumador', 'exfumador menys 1 any') else 0
        rcv = (1 - 0.951**(2.71828**((edat*0.04826 + p_col_total + p_hdl + p_tensio + p_diabetic + p_fumador) - 3.489)))*100
    else:
        p_col_total = 0
        if col_total < 160: p_col_total = -0.26138
        elif col_total >= 160 and col_total < 200: p_col_total = 0
        elif col_total >= 200 and col_total < 240: p_col_total = 0.20771
        elif col_total >= 240 and col_total < 280: p_col_total = 0.24385
        elif col_total >= 280: p_col_total = 0.53513
        p_hdl = 0
        if hdl < 35: p_hdl = 0.84312
        elif hdl >= 35 and hdl < 45: p_hdl = 0.37796
        elif hdl >= 45 and hdl < 50: p_hdl = 0.19785
        elif hdl >= 50 and hdl < 60: p_hdl = 0
        elif hdl >= 60: p_hdl = -0.42951
        p_tensio = 0
        if tas < 120 and tad < 80: p_tensio = -0.53363
        elif tas < 130 and tad < 85: p_tensio = 0
        elif tas < 140 and tad < 90: p_tensio = -0.06773
        elif tas < 160 and tad < 100: p_tensio = 0.26288
        elif tas >= 160 or tad >= 100: p_tensio = 0.46573
        p_diabetic = 0.59626 if dm_global else 0
        p_fumador = 0.29246 if tabac in ('fumador', 'exfumador menys 1 any') else 0
        rcv = (1 - 0.978**(2.71828**((edat*0.33766 + (edat**2)*(-0.00268) + p_col_total + p_hdl + p_tensio + p_diabetic + p_fumador) - 10.279)))*100

    if rcv >= 5 and rcv < 10:
        pacients_candidats[hash] = [up, uba, ubainf, edat, data_naix, sexe, col_total, 
                                    hdl, ldl, tas, 
                                    tad, tabac, dm_global, dm, pes, talla, triglic, rcv]

print('exclusions')
exclusions = set()
sql = """select ID_CIP_sEC from nodrizas.eqa_problemes ep 
        where ps in (1, 211, 7, 212, 621, 11, 213, 996, 528, 439, 53)"""
for id, in u.getAll(sql, 'nodrizas'):
    exclusions.add(id)

sql = """select id_cip_sec from nodrizas.eqa_variables ev 
    where agrupador = 30
    and usar = 1
    and valor < 29
    and data_var >= '2022-09-25'"""
fg = set()
for id, in u.getAll(sql, 'nodrizas'):
    fg.add(id)

print('converters, centres')
up_2_desc = {}
sql = """select ics_desc, scs_codi from nodrizas.cat_centres where sap_codi  in ('24', '25', '28', '03', '04', '38', '39', '59')"""
for desc, up in u.getAll(sql, 'nodrizas'):
    up_2_desc[up] = desc 

print('converters, looong')
converters = {}
sql = """select id_cip_sec, hash_d from import.u11"""
for id, hash in u.getAll(sql, 'import'):
    converters[hash] = id

print('converters, professionals')
desc_inf = {}
desc_med = {}
sql = """SELECT up, uab, tipus, UAB_DESCRIPCIO 
            FROM PROFESSIONALS
            WHERE tipus IN ('I', 'M')"""
for up, uba, tipus, nom in u.getAll(sql, 'pdp'):
    if tipus == 'I':
        desc_inf[(up, uba)] = nom
    else:
        desc_med[(up, uba)] = nom

print('cooking')
uploading = c.Counter()
patients_list = []
for hash in pacients_candidats:
    up, uba, ubainf, edat, data_naix, sexe, col_total, hdl, ldl, tas, tad, tabac, dm_global, dm, pes, talla, triglic, rcv = pacients_candidats[hash]
    if hash in converters:
        id = converters[hash]
        if up in up_2_desc:
            desc = up_2_desc[up]
            uba_desc = desc_med[(up, uba)] if (up, uba) in desc_med else ''
            inf_desc = desc_inf[(up, ubainf)] if (up, ubainf) in desc_inf else ''
            if id not in exclusions:
                if id not in fg:
                    uploading[up] += 1
                    patients_list.append((hash, up, desc, uba, uba_desc, ubainf, inf_desc,
                                        edat, data_naix, sexe,
                                        col_total, hdl, ldl, tas, tad, tabac,
                                        dm, pes, talla, triglic, rcv))

# upload = [(sap, n) for sap, n in uploading.items()]
u.listToTable(patients_list, 'rcv_genicode_1_2', 'redics')
u.execute('grant select on rcv_genicode_1_2 to pdp', 'redics')


# file_name = 'genincode.txt'
# sql = """SELECT
#             c_cip,
#             c_up,
#             up_desc,
#             c_metge,
#             uab_descripcio,
#             c_sexe,
#             C_DATA_NAIX,
#             CASE
#                 WHEN PS_DIABETIS1_DATA IS NOT NULL THEN 'DM1'
#                 WHEN PS_DIABETIS2_DATA IS NOT NULL THEN 'DM2'
#                 ELSE 'no'
#             END DM,
#             CASE
#                 WHEN V_TAS_DATA >=  sysdate - 180 THEN v_TAS_VALOR
#                 ELSE 0
#             END TAS,
#             CASE
#                 WHEN V_TAD_DATA >=  sysdate - 180 THEN v_TAD_VALOR
#                 ELSE 0
#             END TAD,
#             v_peso_valor,
#             v_talla_valor,
#             CASE
#                 WHEN V_COL_TOTAL_DATA >=  sysdate - 180 THEN v_col_total_valor
#                 ELSE 0
#             END COLESTEROL_TOTAL,
#             CASE
#                 WHEN V_COL_LDL_DATA >=  sysdate - 180 THEN v_col_LDL_valor
#                 ELSE 0
#             END COLESTEROL_LDL,
#             CASE
#                 WHEN V_COL_HDL_DATA >=  sysdate - 180 THEN v_col_HDL_valor
#                 ELSE 0
#             END COLESTEROL_HDL,
#             CASE
#                 WHEN v_triglic_DATA >=  sysdate - 180 THEN v_triglic_valor
#                 ELSE 0
#             END TRIGLICERIDS,
#             tabaquisme
#         FROM
#             (
#             SELECT
#                 *
#             FROM
#                 dwsisap.dbs
#             WHERE
#                 V_COL_TOTAL_DATA >= sysdate - 180
#                 AND V_COL_LDL_DATA >= sysdate - 180
#                 AND V_COL_HDL_DATA >=  sysdate - 180
#                 AND v_triglic_DATA >= sysdate - 180
#                 AND V_TAS_DATA >= sysdate - 180
#                 AND V_TAD_DATA >= sysdate - 180
#                 AND c_sap IN ('24', '25', '28', '03', '04')) dbs
#         INNER JOIN
#                     dwsisap.rcv_genicode a ON
#             dbs.c_cip = a.patient
#         INNER JOIN (
#             SELECT
#                 *
#             FROM
#                 dwsisap.PROFESSIONALS
#             WHERE
#                 tipus = 'M') p
#                     ON
#             dbs.c_metge = p.uab
#             AND dbs.c_up = p.up"""
# upload = [row for row in u.getAll(sql, 'exadata')]
# u.writeCSV(u.tempFolder+file_name, upload, sep=',')

# u.ftp_upload('sitabapp.genincode.com', 'catsalut', '65?Kc9YPwj', u.tempFolder, file_name, ftp_folder='/', rm=False, TLS=True)



# rcv_genicode_1_2: dones: bo corregit
# rcv_genicode_1_1: homes: bo corregit

