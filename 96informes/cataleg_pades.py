import sisapUtils as u

file = './Relacio_EAP_PADES.csv'

header = True
dades = []
for row in u.readCSV(file, sep=';'):
    if header:
        header = False
        continue
    dades.append(tuple(row))

cols = "(eap_up varchar2(5), eap_desc varchar2(100), pades_up varchar2(5), pades_desc varchar2(100))"
u.createTable('cataleg_pades', cols, 'exadata', rm=True)
u.listToTable(dades, 'cataleg_pades', 'exadata')
u.grantSelect('cataleg_pades', 'DWSISAP_ROL', 'exadata')