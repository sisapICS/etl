# coding: latin1

import sisapUtils as u
import sisaptools as t
import datetime as d
import collections as c

DEXTD = u.getOne("select data_ext from dextraccio", 'nodrizas')[0]

class Visites():
    def __init__(self):
        self.upload = c.Counter()
        u.printTime('inici')
        self.get_visites()
        u.printTime('visites')
        self.get_it()
        u.printTime('it')
        self.upload_data()
        u.printTime('fi')

    def upload_data(self):
        cols = "(tipus varchar(10), up varchar(10), analisi varchar(3), val int)"
        u.createTable('peticio_it', cols, 'altres', rm=True)
        llistat = [(tipus, up, analisi, val) for ((tipus, up, analisi), val) in self.upload.items()]
        u.listToTable(llistat, 'peticio_it', 'altres')

    def comparacio(self, baixa, alta, visites):
        for data, tipus, up in visites:
            if isinstance(alta, d.datetime):
                alta = alta.date()
            if baixa and alta and data and baixa.date() <= data.date() and data.date() <= alta:
                self.upload[(tipus, up, 'NUM')] += 1

    def get_it(self):
        sql = """
            SELECT cip, data_baixa, data_alta
            FROM dwsisap.master_it
        """
        for cip, baixa, alta in u.getAll(sql, 'exadata'):
            if cip in self.visites:
                self.comparacio(baixa, alta, self.visites[cip])


    def get_visites(self):
            """passar a multiprocess"""
            # periodes = []
            # for month in range(1, 13):
            #     if month <= d.date.today().month:  # noqa
            #         periodes.append((2023, month))
            # mesos_a_tractar = []
            # for any, mes in periodes:
            #     if mes < 10:
            #         date_inf = str(any) + '-0' + str(mes) + '-01'
            #         if mes+1 == 10:
            #             date_sup = str(any) + '-' + str(mes+1) + '-01'
            #         else:
            #             date_sup = str(any) + '-0' + str(mes+1) + '-01'
            #     elif mes < 12:
            #         date_inf = str(any) + '-' + str(mes) + '-01'
            #         date_sup = str(any) + '-' + str(mes+1) + '-01'
            #     else:
            #         date_inf = str(any) + '-' + str(mes) + '-01'
            #         date_sup = str(any+1) + '-01-01'
            #     mesos_a_tractar.append((date_inf, date_sup))
            sql = """
                SELECT cip, peticio, tipus, up
                FROM dwsisap.sisap_master_visites a
                INNER JOIN dwsisap.PDPTB101_RELACIO_NIA b
                ON b.hash_covid = a.pacient
                WHERE data >= DATE '2024-01-01'
                and data <= DATE '2025-01-01'
                and peticio is not NULL
                and sisap_servei_class = 'MF'
            """
            self.visites = c.defaultdict(set)
            for cip, data, tipus, up in u.getAll(sql, 'exadata'):
                if tipus in ('9C', '9D', '9R'):
                    self.visites[cip].add((data, 'presencial', up))
                    self.upload[('presencial', up, 'DEN')] += 1
                elif tipus in ('9T', '9E'):
                    self.visites[cip].add((data, 'virtual', up))
                    self.upload[('virtual', up, 'DEN')] += 1

            # jobs = [(sql.format(mes_inf=mes_inf, mes_sup=mes_sup), mes_inf, mes_sup) for (mes_inf, mes_sup) in mesos_a_tractar]
            # self.visites = c.defaultdict(set)
            # for dades, llistat in u.multiprocess(get_visites_worker, jobs, 6, close=True):
            #     # visites[cip].append((data, visita, up))
            #     for cip in dades:
            #         for data, visita, up in dades[cip]:
            #             self.visites[cip].add((data, visita, up))
            #     for visita, up, val in llistat.items():
            #             self.upload[(visita, up, 'DEN')] += val
            #     u.printTime('multiprocess')
            #  for mes_inf, mes_sup in mesos_a_tractar:
            #     for cip, data, tipus, up in u.getAll(sql.format(mes_inf=mes_inf, mes_sup=mes_sup), 'exadata'):
            #         if tipus in ('9C', '9D', '9R'):
            #             self.visites[cip].append((data, 'presencial', up))
            #             self.upload[('presencial', up, 'DEN')] += 1
            #         elif tipus in ('9T', '9E'):
            #             self.visites[cip].append((data, 'virtual', up))
            #             self.upload[('virtual', up, 'DEN')] += 1
            #     print(mes_inf + ' ' + mes_sup)
            
            # jobs = [(sql, mes_inf, mes_sup) for (mes_inf, mes_sup) in mesos_a_tractar]  # noqa
            # pool = m.Pool(4)
            # dades = pool.starmap(get_visites_worker, jobs)
            # pool.close()
            # for visites in dades:
            #     for id, visits in visites.items():
            #         for data, tipus, up in visits:
            
                
            # print(str(len(self.visites)))
            # for id in self.visites:
            #     for data, tipus, up in self.visites[id]:
            #         self.upload[(tipus, up, 'DEN')] += 1


def get_visites_worker(params):
    sql, mes_inf, mes_sup = params
    visites = c.defaultdict(set)
    upload = c.Counter()
    with t.Database('exadata', 'data') as conn:
        for cip, data, tipus, up in conn.get_all(sql):
            if tipus in ('9C', '9D', '9R'):
                visites[cip].add((data, 'presencial', up))
                upload[('presencial', up)] += 1
            elif tipus in ('9T', '9E'):
                visites[cip].add((data, 'virtual', up))
                upload[('virtual', up)] += 1
    print(mes_inf + ' ' + mes_sup)
    return (visites, upload)

Visites()