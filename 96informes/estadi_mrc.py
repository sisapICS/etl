import sisapUtils as u
import sisaptools as t
import collections as c
import datetime as d
from dateutil import relativedelta as rd

DEXTD = u.getOne('select data_ext from dextraccio', 'nodrizas')[0]
menys1 = DEXTD - rd.relativedelta(years=1)

class MRC():
    def __init__(self):
        self.dades = c.defaultdict(set)
        u.printTime('inici')
        self.get_pob()
        u.printTime('pob')
        self.get_mrc()
        u.printTime('mrc')
        self.get_estadiatge()
        u.printTime('estadiatge')
        self.get_fge()
        u.printTime('fge')
        self.get_indicadors()
        u.printTime('fi')

    def get_indicadors(self):
        res = c.Counter()
        pacients = []
        for id in self.dades['mrc']:
            (up, uba, sexe) = self.pob[id]
            res[up, uba, sexe, 'DEN'] += 1
            num = 0
            if id in self.dades['3a'] and id in self.fge and 45 <= self.fge[id] <= 59:
                num = 1
            elif id in self.dades['3b'] and id in self.fge and 30 <= self.fge[id] <= 44:
                num = 1
            elif id in self.dades['4'] and id in self.fge and 15 <= self.fge[id] <= 29:
                num = 1
            elif id in self.dades['5'] and id in self.fge and self.fge[id] < 15:
                num = 1
            if num:
                res[up, uba, sexe, 'NUM'] += 1
            pacients.append((id, up, uba, sexe, num))
        cols = "(up varchar(10), uba varchar(8), sexe varchar(1), analisi varchar(3), val int)"
        u.createTable('estadiatge_mrc', cols, 'altres', rm=True)
        upload = []
        for (up, uba, sexe, analisi), val in res.items():
            upload.append((up, uba, sexe, analisi, val))
        u.listToTable(upload, 'estadiatge_mrc', 'altres')
        cols = "(id int, up varchar(10), uba varchar(8), sexe varchar(1), num int)"
        u.createTable('estadiatge_mrc_pac', cols, 'altres', rm=True)
        u.listToTable(pacients, 'estadiatge_mrc_pac', 'altres')

    def get_fge(self):
        sql = """
            SELECT id_cip_sec, valor
            FROM eqa_variables
            WHERE agrupador = 30
            AND usar = 1
            AND data_var >= DATE '{menys1}'
        """.format(menys1=menys1)
        self.fge = {}
        for id, valor in u.getAll(sql, 'nodrizas'):
            if id in self.pob:
                self.fge[id] = valor

    def get_estadiatge(self):
        # self.estadiatge = c.defaultdict(set)
        estadis =  {
            'C01-N18.31': '3a',
            'C01-N18.32': '3b',
            'C01-N18.4': '4',
            'C01-N18.5': '5'
        }
        sql = """
            SELECT id_cip_sec, pr_cod_ps
            FROM {tb}
            WHERE pr_dde >= DATE '{menys1}'
            AND pr_cod_ps in {probs}
            AND (pr_dba = '' OR pr_dba is null)
        """
        for tb in u.getSubTables('problemes'):
            for id, ps in u.getAll(sql.format(tb=tb, menys1=menys1, probs=tuple(estadis.keys())), 'import'):
                if id in self.pob:
                    self.dades[estadis[ps]].add(id)
            u.printTime(tb)
        
        estadis_var = {
            4: '4',
            5: '5',
            6: '3a',
            7: '3b'
        }
        sql = """
            SELECT id_cip_sec, vu_val
            FROM {tb}
            WHERE vu_dat_act >= DATE '{menys1}'
            AND vu_cod_vs = 'VU2000'
        """
        for tb in u.getSubTables('variables'):
            for id, val in u.getAll(sql.format(tb=tb, menys1=menys1), 'import'):
                if id in self.pob and val in estadis_var:
                    self.dades[estadis_var[val]].add(id)
            u.printTime(tb)

    def get_mrc(self):
        sql = """
            SELECT id_cip_sec
            FROM eqa_problemes
            WHERE ps = 53
        """
        for id, in u.getAll(sql, 'nodrizas'):
            if id in self.pob:
                self.dades['mrc'].add(id)

    def get_pob(self):
        sql = """
            SELECT id_cip_sec, up, uba, sexe
            FROM assignada_tot
            WHERE edat > 14
            AND edat < 80
        """
        self.pob = {}
        for id, up, uba, sexe in u.getAll(sql, 'nodrizas'):
            self.pob[id] = (up, uba, sexe)

MRC()