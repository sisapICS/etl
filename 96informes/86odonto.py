# -*- coding: utf8 -*-

"""
Cartera de serveis ICS.
"""

import collections as c

import sisaptools as u


year = 2016
debug = False
table = 'sisap_odontologia_{}'.format(year)


def create():
    """Crea l'estructura a REDICS per guardar les dades."""
    with u.Database('redics', 'data') as db:
        db.create_table(table, ('sector varchar(4)', 'fitxa int',
                                'up varchar2(5)', 'edat int', 'caod int',
                                'cod int', 'ird int', 'irt int', 'c_def int',
                                'o_def int', 'a_def int', 'c_tmp int',
                                'o_tmp int', 'alert int'), remove=True)
        db.set_grants('select', table, 'PREDUPRP')


def main():
    """Executa el procés a cada sector instanciant la classe Sector."""
    if debug:
        Sector('6837')
    else:
        sectors = ('6102', '6209', '6211', '6310', '6416', '6519', '6520',
                   '6521', '6522', '6523', '6625', '6626', '6627', '6728',
                   '6731', '6734', '6735', '6837', '6838', '6839', '6844')
        pool = u.NoDaemonPool(len(sectors), maxtasksperchild=1)
        for sector in sectors:
            pool.apply_async(Sector, args=(sector,))
        pool.close()
        pool.join()


class Sector(object):
    """Classe principal a instanciar per cada sector."""

    def __init__(self, sector):
        """Execució seqüencial."""
        self.sector = sector
        with u.Database(self.sector, 'data') as self.db:
            self.get_poblacio()
            self.get_fitxes()
            self.get_tipus()
            self.get_reconstruides()
            self.get_no_cariades()
            self.get_dents()
        self.get_upload()
        self.upload_data()

    def get_poblacio(self):
        """Població d'estudi."""
        sql = "select usua_cip, usua_uab_up, \
               to_char(to_date(usua_data_naixement, 'J'), 'YYYY') \
               from usutb040 \
               where usua_situacio = 'A' \
               and to_char(to_date(usua_data_naixement, 'J'), 'YYYY') \
               in ('{}', '{}')".format(year - 12, year - 7)
        self.poblacio = {cip: (up, year - int(naix)) for (cip, up, naix)
                         in self.db.get_all(sql) if up}

    def get_fitxes(self):
        """Fitxes (prstb505)."""
        sql = 'select cfd_cod, cfd_cip, cfd_ind_caod, cfd_ind_cod, \
               cfd_ind_rest, cfd_ind_rest_tmp from prstb505 \
               where cfd_ddb is null'
        self.fitxes = {id: self.poblacio[cip] + (caod, cod, ird, irt)
                       for (id, cip, caod, cod, ird, irt)
                       in self.db.get_all(sql) if cip in self.poblacio}

    def get_tipus(self):
        """Temporal / definitiva."""
        sql = 'select pd_cod, pd_tipus from prstb500'
        self.tipus = {codi: tipus for (codi, tipus) in self.db.get_all(sql)}

    def get_reconstruides(self):
        """Peces aparentment obturades que no conten per índex."""
        sql = "select dtd_cod, dtd_cod_p from prstb508 \
               where dtd_tra = 'CON' and dtd_ddb is null"
        self.reconstruides = set([row for row in self.db.get_all(sql)])

    def get_no_cariades(self):
        """Peces amb patologia diferent de caries."""
        self.cariades = set()
        altres = set()
        sql = "select dpd_cod, dpd_cod_p, dpd_pat from prstb507 \
               where dpd_ddb is null"
        for id, dent, patologia in self.db.get_all(sql):
            if patologia == 'CA':
                self.cariades.add((id, dent))
            else:
                altres.add((id, dent))
        self.no_cariades = altres - self.cariades

    def get_dents(self):
        """Càries, absències i obturacions."""
        self.dents = c.defaultdict(lambda: c.Counter())
        sql = 'select dfd_cod, dfd_cod_p, dfd_p_a, dfd_mot_a, dfd_estat \
               from prstb506'
        for id, dent, presencia, motiu, estat in self.db.get_all(sql):
            if id in self.fitxes:
                tipus = self.tipus[dent]
                if (presencia == 'A' and motiu in ('P', 'E') and
                        (id, dent) not in self.no_cariades):
                    situacio = 'A'
                elif (presencia == 'P' and estat == 'O' and
                        ((id, dent) not in self.reconstruides or
                         (id, dent) in self.cariades)):
                    situacio = 'O'
                elif (presencia == 'P' and estat == 'C' and
                        (id, dent) not in self.no_cariades):
                    situacio = 'C'
                else:
                    situacio = 'X'
                self.dents[id][(tipus, situacio)] += 1

    def get_upload(self):
        """Preparar i executar upload."""
        self.upload = []
        for id, dades in self.fitxes.items():
            dents = self.dents[id]
            that = (dents[('D', 'C')], dents[('D', 'O')], dents[('D', 'A')],
                    dents[('T', 'C')], dents[('T', 'O')])
            this = (self.sector, id) + dades + that + (0,)
            self.upload.append(this)

    def upload_data(self):
        """Puja a REDICS."""
        with u.Database('redics', 'data') as redics:
            redics.list_to_table(self.upload, table)
            redics.execute(
              'update {} set alert = 1 where \
               caod <> c_def + o_def + a_def or \
               cod <> c_tmp + o_tmp or \
               ird <> round(decode(caod, 0, 0, 100 * o_def / caod, 0)) or \
               irt <> round(decode(cod, 0, 0, 100 * o_tmp / cod), 0)'.format(
                                                                        table))


if __name__ == '__main__':
    create()
    main()
