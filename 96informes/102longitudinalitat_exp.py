# -*- coding: utf8 -*-

"""
.
"""

import collections as c

import sisapUtils as u


tb = 'sisap_longitudinalitat_exp'
db = 'redics'
ini = 20171001
fi = 20171031


class Manolo(object):
    """."""

    def __init__(self):
        """."""
        self.get_poblacio()
        self.get_atdom()
        self.get_especialitats()
        self.get_professionals()
        self.get_moduls()
        self.get_responsable()
        self.get_delegats()
        self.get_gestors()
        self.get_tipus()
        self.get_serveis()
        self.get_visites()
        self.upload_data()

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, up, uba, ubainf, pcc, maca \
               from assignada_tot"
        self.poblacio = {row[0]: row[1:] for row in u.getAll(sql, 'nodrizas')}

    def get_atdom(self):
        """."""
        sql = "select id_cip_sec from eqa_problemes where ps = 45"
        self.atdom = set([id for id, in u.getAll(sql, 'nodrizas')
                          if id in self.poblacio])

    def get_especialitats(self):
        """."""
        sql = 'select espe_codi_especialitat, espe_especialitat \
               from pritb023'
        self.especialitats = {cod: des for (cod, des) in u.getAll(sql, '6734')}

    def get_professionals(self):
        """."""
        sql = "select codi_sector, prov_dni_proveidor, prov_categoria \
               from cat_pritb031"
        self.professionals = {(sector, dni): categ for (sector, dni, categ)
                              in u.getAll(sql, 'import')}

    def get_moduls(self):
        """."""
        sql = "select modu_codi_up, modu_servei_codi_servei, \
                      modu_codi_modul, modu_codi_uab \
               from cat_vistb027"
        self.moduls = {row[:3]: row[3] for row in u.getAll(sql, 'import')}

    def get_responsable(self):
        """."""
        sql = "select lloc_codi_up, lloc_codi_lloc_de_treball, lloc_numcol \
               from cat_pritb025"
        llocs = {(up, lloc): col for (up, lloc, col)
                 in u.getAll(sql, 'import')}
        sql = "select {}, '{}', {}, {} from {}"
        self.responsable = {}
        for param in (('uab_codi_up', 'M', 'uab_codi_uab',
                       'uab_lloc_de_tr_codi_lloc_de_tr', 'cat_vistb039'),
                      ('uni_codi_up', 'I', 'uni_codi_unitat',
                       'uni_ambit_treball', 'cat_vistb059')):
            this = sql.format(*param)
            for up, tipus, uba, lloc in u.getAll(this, 'import'):
                if uba and lloc and (up, lloc) in llocs:
                    self.responsable[(up, tipus, uba)] = llocs[(up, lloc)]

    def get_delegats(self):
        """."""
        sql = "select codi_sector, delm_numcol, delm_numcol_del \
               from cat_pritb777"
        self.delegats = c.defaultdict(list)
        for sector, delegador, delegat in u.getAll(sql, 'import'):
            self.delegats[(sector, delegador)].append(delegat)

    def get_gestors(self):
        """."""
        sql = "select a.codi_sector, ide_numcol \
              from cat_pritb799 a inner join cat_pritb992 b \
              on a.rol_usu = b.ide_usuari and a.codi_sector = b.codi_sector \
              where rol_rol = 'ECAP_GEST' and ide_numcol <> ''"
        self.gestors = set([row for row in u.getAll(sql, 'import')])

    def get_tipus(self):
        """."""
        sql = "select codi_sector, tv_tipus, tv_cita, tv_homol, tv_model_nou \
               from cat_vistb206"
        self.tipus = {(sector, tipus, cita): tipus if model == 'S' else homol
                      for (sector, tipus, cita, homol, model)
                      in u.getAll(sql, 'import')}

    def get_serveis(self):
        """."""
        sql = "select codi_sector, s_codi_servei, \
                      s_descripcio, s_espe_codi_especialitat \
               from cat_pritb103"
        self.serveis = {row[:2]: row[2:] for row in u.getAll(sql, 'import')}

    def get_visites(self):
        """."""
        sql = "select id_cip_sec, \
                      codi_sector, \
                      visi_data_visita, \
                      visi_up, \
                      visi_dni_prov_resp, \
                      visi_col_prov_resp, \
                      visi_servei_codi_servei, \
                      visi_modul_codi_modul, \
                      visi_rel_proveidor, \
                      visi_tipus_visita, \
                      visi_tipus_citacio \
               from visites1 \
               where visi_data_visita between {} and {} and \
                     visi_situacio_visita = 'R'".format(ini, fi)
        self.upload = []
        for (id, sector, dat, up, dni, col, servei,
             modul, rel, tipus, citacio) in u.getAll(sql, 'import'):
            if id in self.poblacio:
                up_pob, uba, ubainf, pcc, maca = self.poblacio[id]
                atdom = 1 * (id in self.atdom)
                categ_c = self.professionals.get((sector, dni))
                categ_d = self.especialitats.get(categ_c)
                responsables = (self.responsable.get((up, 'M', uba)),
                                self.responsable.get((up, 'I', ubainf)))
                delegats = []
                for resp in responsables:
                    delegats.extend(self.delegats.get((sector, resp), []))
                ubamod = self.moduls.get((up, servei, modul))
                lab = 1 * u.isWorkingDay(dat)
                homol = self.tipus.get((sector, tipus, citacio))
                servei_d, espe_c = self.serveis.get((sector, servei),
                                                    [None, None])
                espe_d = self.especialitats.get(espe_c)
                this = (id, up_pob, uba, ubainf, pcc, maca, atdom,
                        dni, col, categ_c, categ_d,
                        1 * (col in responsables), 1 * (col in delegats),
                        1 * ((sector, col) in self.gestors),
                        servei, servei_d, espe_c, espe_d,
                        1 * (servei in ('INFG', 'UGC', 'GCAS')),
                        modul, ubamod, dat, lab, up, homol, rel)
                self.upload.append(this)

    def upload_data(self):
        """."""
        cols = ('pac_id int', 'pac_up varchar(5)', 'pac_uba varchar(5)',
                'pac_ubainf varchar(5)', 'pac_pcc int', 'pac_maca int',
                'pac_atdom int', 'prof_dni varchar(10)',
                'prof_numcol varchar(10)', 'prof_categ_codi varchar(5)',
                'prof_categ_desc varchar(150)', 'prof_responsable int',
                'prof_delegat int', 'prof_rol_gestor int',
                'modul_serv_codi varchar(5)', 'modul_serv_desc varchar(150)',
                'modul_espe_codi varchar(5)', 'modul_espe_desc varchar(150)',
                'modul_serv_gestor int', 'modul_codi varchar(5)',
                'modul_uba varchar(5)', 'vis_data date', 'vis_laborable int',
                'vis_up varchar(5)', 'vis_tipus_homol varchar(5)',
                'vis_relacio varchar(1)')
        sql = ', '.join(cols)
        u.createTable(tb, '({})'.format(sql), db, rm=True)
        u.listToTable(self.upload, tb, db)
        u.grantSelect(tb, ('PREDUMMP', 'PREDUPRP', 'PREDUJVG'), db)


if __name__ == '__main__':
    Manolo()
