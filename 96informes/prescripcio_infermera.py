import sisapUtils as u
import sisaptools as t
import datetime as d
import collections as c
from dateutil import relativedelta as rd

data_inici = d.date(2021,1,1)
data_fi = t.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]

class Prescripcio():
    def __init__(self):
        u.printTime('inici')
        self.get_conversor()
        u.printTime('get_conversor')
        self.get_prescripcio()
        u.printTime('get_prescripcio')
        self.get_up()
        u.printTime('get_up')
        self.get_pob()
        u.printTime('get_pob')
        self.get_atc()
        u.printTime('get_atc')
        self.get_relacionats()
        u.printTime('get_relacionats')
        self.get_prescriptiu_rel()
        u.printTime('get_prescriptiu_rel')
        self.get_pcc_maca()
        u.printTime('get_pcc_maca')
        self.get_atdom()
        u.printTime('get_atdom')
        self.get_residencies_cens()
        u.printTime('get_residencies_cens')
        self.get_exitus()
        u.printTime('get_exitus')
        self.get_unions()
        u.printTime('get_unions')
        self.get_upload()
        u.printTime('get_upload')

    def get_upload(self):
        cols ="""(
            id_presc int,
            pacient varchar(40),
            sector varchar(4),
            dataany int,
            up varchar(5),
            sap varchar(40),
            amb varchar(40),
            codi_terapeutic varchar(7),
            desc_terapeutic varchar(120),
            codi_prob varchar(50),
            desc_prob varchar(60),
            pcc int,
            maca int,
            atdom int,
            institucionalitzat int,
            edat int,
            sexe varchar(1)
        )"""
        u.createTable('prescripcio_inf', cols, 'altres', rm=True)
        u.listToTable(self.upload, 'prescripcio_inf', 'altres')
        sql = "select * from prescripcio_inf"
        rows = [row for row in u.getAll(sql, 'altres')]
        u.createTable('prescripcio_inf', cols, 'exadata', rm=True)
        u.listToTable(rows, 'prescripcio_inf', 'exadata')
        u.grantSelect('prescripcio_inf', 'DWSISAP_ROL', 'exadata')

    def get_unions(self):
        self.upload = []
        for presc, sector in self.prescripcions:
            atc = self.prescripcions[(presc,sector)]['atc']
            id = self.prescripcions[(presc,sector)]['id']
            data_presc = self.prescripcions[(presc, sector)]['data']
            up = self.prescripcions[(presc, sector)]['up']
            sap = None
            amb = None
            pcc = 0
            maca = 0
            atdom = 0
            institucionalitzat = 0
            edat = None
            sexe = None
            desc_atc = None
            desc_prob = None
            prob = None
            if (presc, sector) in self.relacions:
                for prob, dataany, data in self.relacions[(presc, sector)]:
                    # if dataany in self.ups and id in self.ups[dataany]:
                    #     up = self.ups[dataany][id]
                    if up and up in self.saps:
                        sap = self.saps[up]['sap']
                        amb = self.saps[up]['amb']
                    if atc in self.atcs:
                        desc_atc = self.atcs[atc]
                    elif atc in self.codis_pf:
                        desc_atc = self.codis_pf[atc]
                    if id in self.pcc and dataany in self.pcc[id]:
                        pcc = 1
                    if id in self.maca and dataany in self.maca[id]:
                        maca = 1
                    if id in self.atdom and dataany in self.atdom[id]:
                        atdom = 1
                    if id in self.resis_cens and dataany in self.resis_cens[id]:
                        institucionalitzat = 1
                    if id in self.pob:
                        naixement = self.pob[id]['naixement']
                        if isinstance(data, d.datetime):
                            data = data.date()
                        if isinstance(self.pob[id]['naixement'], d.datetime):
                            naixement = self.pob[id]['naixement'].date()
                        edat = rd.relativedelta(data, naixement).years
                        sexe = self.pob[id]['sexe']
                    if prob in self.prescriptiu_rel:
                        desc_prob = self.prescriptiu_rel[prob]
                    if id in self.hashos_final:
                        covid = self.hashos_final[id]
                        self.upload.append((presc, covid, sector, dataany, up, sap, amb, atc, desc_atc, prob, desc_prob, pcc, maca, atdom, institucionalitzat, edat, sexe))
            else:
                dataany = self.prescripcions[(presc,sector)]['any']
                # if dataany in self.ups and id in self.ups[dataany]:
                #     up = self.ups[dataany][id]
                if up and up in self.saps:
                    sap = self.saps[up]['sap']
                    amb = self.saps[up]['amb']
                if atc in self.atcs:
                    desc_atc = self.atcs[atc]
                elif atc in self.codis_pf:
                    desc_atc = self.codis_pf[atc]
                if id in self.pcc and dataany in self.pcc[id]:
                    pcc = 1
                if id in self.maca and dataany in self.maca[id]:
                    maca = 1
                if id in self.atdom and dataany in self.atdom[id]:
                    atdom = 1
                if id in self.resis_cens and dataany in self.resis_cens[id]:
                    institucionalitzat = 1
                if id in self.pob:
                    if isinstance(data_presc, d.datetime):
                        data_presc = data_presc.date()
                    if isinstance(self.pob[id]['naixement'], d.datetime):
                        naixement = self.pob[id]['naixement'].date()
                    edat = rd.relativedelta(data_presc, naixement).years
                    sexe = self.pob[id]['sexe']
                if id in self.hashos_final:
                    covid = self.hashos_final[id]
                    self.upload.append((presc, covid, sector, dataany, up, sap, amb, atc, desc_atc, prob, desc_prob, pcc, maca, atdom, institucionalitzat, edat, sexe))
        u.printTime('upload ' + str(len(self.upload)))
        # file = 'prescripcio_infermera_backup.csv'
        # u.writeCSV(file, self.upload[:5000], sep=',')
        # u.printTime('csv')

    def get_exitus(self):
        sql = """
            SELECT hash, data_naixement, edat_final, sexe
            FROM dwsisap.dbc_poblacio
            -- WHERE situacio = 'D'
        """
        for hash, data, edat, sexe in u.getAll(sql, 'exadata'):
            try:
                self.pob[self.covid_redics[hash]] = {
                    'naixement': data,
                    'edat': edat,
                    'sexe': sexe
                }
            except KeyError:
                continue
        print('pob_exitus ' + str(len(self.pob)))

    def get_prescriptiu_rel(self):
        self.prescriptiu_rel = c.defaultdict()
        sql = """
            SELECT ps_cod, ps_des
            FROM cat_prstb001
        """
        for codi, des in u.getAll(sql, 'import'):
            self.prescriptiu_rel[codi] = des
        print('prescriptiu_rel ' + str(len(self.prescriptiu_rel)))

    def get_relacionats(self):
        rel = c.defaultdict(set)
        self.relacions = c.defaultdict(set)
        sql = """
            SELECT dgppf_pmc_codi, dgppf_ps_cod, codi_sector, extract(year from dgppf_data_alta), dgppf_data_alta
            FROM ppftb023
            WHERE dgppf_data_alta between DATE '{data_inici}' AND DATE '{data_fi}'
        """.format(data_inici=data_inici, data_fi=data_fi)
        for id, codi, sector, dataany, data in u.getAll(sql, 'redics'):
            if (id, sector) in self.prescripcions:
                self.relacions[(id, sector)].add((codi, dataany, data))
        print('relacions ' + str(len(self.relacions)))
        # u.printTime('sql 1')
        # for id, sector in rel:
        #     max_data = None
        #     max_codi = None
        #     for codi, alta in rel[(id, sector)]:
        #         if not max_data or (max_data and alta > max_data):
        #             max_data = alta
        #             max_codi = codi
        #     self.relacions[(id, sector)] = max_codi

    def get_pcc_maca(self):
        self.pcc = c.defaultdict(set)
        self.maca = c.defaultdict(set)
        sql = """
            SELECT up_cod_u, extract(year from up_data_ini), up_cod_pg_sp
            FROM prstb014
            WHERE up_data_ini >= DATE '{data_inici}'
            AND UP_COD_PG_SP in ('A0017', 'A0018')
        """.format(data_inici=data_inici)
        for e, sector in enumerate(u.sectors):
            for id, data, codi in u.getAll(sql, sector):
                try:
                    if codi == 'A0017':
                        self.pcc[self.cips[id]].add(data)
                    else:
                        self.maca[self.cips[id]].add(data)
                except KeyError:
                    continue
        print('pcc ' + str(len(self.pcc)))
        print('maca ' + str(len(self.maca)))

    def get_atdom(self):
        self.atdom = c.defaultdict(set)
        sql = """
            SELECT pr_cod_u, extract(year from pr_dde)
            FROM prstb015 
            WHERE PR_COD_PS = 'C01-Z74.9'
            AND pr_dde <= DATE '2022-12-31'
            AND (pr_dba >= DATE '2021-01-01'
            OR pr_dba is null)
        """
        for id, data in u.getAll(sql, 'redics'):
            self.atdom[id].add(data)
        print('atdom ' + str(len(self.atdom)))

    def get_residencies_cens(self):
        self.resis_cens = c.defaultdict(set)
        sql = """
            SELECT distinct hash, extract(year from data)
            FROM dwsisap.residencies_cens
            WHERE data between DATE '{data_inici}' AND DATE '{data_fi}'
        """.format(data_inici=data_inici, data_fi=data_fi)
        for hash, data in u.getAll(sql, 'exadata'):
            # falta fer el canvi de hash_covid a hash_redics
            try:
                self.resis_cens[self.covid_redics[hash]].add(data)
            except Exception as e:
                pass
        print('resis_cens ' + str(len(self.resis_cens)))

    def get_atc(self):
        self.atcs = c.defaultdict()
        sql = """
            SELECT atc_codi, atc_desc
            FROM cpftb010
        """
        for codi, desc in u.getAll(sql, '6102'):
            self.atcs[codi] = desc
        print('atcs ' + str(len(self.atcs)))

    def get_conversor(self):
        self.conversor = c.defaultdict()
        sql = """
            SELECT id_cip_sec, hash_d
            FROM u11
        """
        hashos = set()
        for id, hash in u.getAll(sql, 'import'):
            self.conversor[id] = hash
            hashos.add(hash)
        print('conversor ' + str(len(self.conversor)))
        u.printTime('sql 1')
        self.covid_redics = c.defaultdict()
        self.hashos_final = c.defaultdict()
        self.cips = {}
        sql = """
            SELECT substr(cip, 0, 13), hash_redics, hash_covid
            FROM pdptb101_relacio
            -- where hash_redics like '{}%'
        """
        # for redics, covid in s.get_data(sql, model='redics', redics='pdp'):
        for cip, redics, covid in u.getAll(sql, 'pdp'):
            self.covid_redics[covid] = redics
            self.hashos_final[redics] = covid
            self.cips[cip] = redics
        
        print('covid_redics ' + str(len(self.covid_redics)))
        u.printTime('sql 2')

    def get_up(self):
        self.ups = c.defaultdict(dict)
        self.saps = c.defaultdict()
        sql = """
            SELECT id_cip_sec, codi_sector, up, dataany
            FROM assignadahistorica_s{any}
        """
        for a in ['2021', '2022', '2023']:
            for id, sector, up, data in u.getAll(sql.format(any=a), 'import'):
                try:
                    self.ups[data][self.conversor[id]] = up
                except:
                    pass
        print('ups ' + str(len(self.ups)))
        u.printTime('sql 1')
        sql = """
            SELECT scs_codi, sap_desc, amb_desc
            FROM cat_centres
        """
        for up, sap, amb in u.getAll(sql, 'nodrizas'):
            self.saps[up] = {
                'sap': sap,
                'amb': amb
            }
        print('saps ' + str(len(self.saps)))
        u.printTime('sql 2')

    def get_pob(self):
        self.pob = c.defaultdict()
        sql = """
            SELECT id_cip_sec, up, data_naix, edat, sexe, '2024'
            FROM assignada_tot
        """
        for id, up, data, edat, sexe, any in u.getAll(sql, 'nodrizas'):
            if id in self.conversor:
                hash = self.conversor[id]
                self.ups[any][hash] = up
                self.pob[hash] = {
                    'naixement': data,
                    'edat': edat,
                    'sexe': sexe
                }
        print('pob ' + str(len(self.pob)))

    def get_prescripcio(self):
        # s'haura d'accedir a sectors perque tarda massa a redics
        self.prescripcions = c.defaultdict()
        sql = """
            SELECT ppfmc_pmc_usuari_cip, ppfmc_pmc_amb_cod_up, pf_cod_atc, ppfmc_pf_codi, codi_sector, ppfmc_pmc_codi, ppfmc_data_ini_sire, extract(year from ppfmc_data_ini_sire)
            FROM ppftb016
            WHERE substr(PPFMC_PMC_AMB_NUM_COL, 0, 1) = '3'
            AND ppfmc_data_ini_sire between DATE '{data_inici}' AND DATE '{data_fi}'
        """.format(data_inici=data_inici, data_fi=data_fi)
        for cip, up, atc, pf, sector, codi, data, any in u.getAll(sql, 'redics'):
            if atc is None or atc == '' and pf and pf != '':
                atc = pf
            self.prescripcions[(codi, sector)] = {
                'atc': atc,
                'id': cip,
                'any': any,
                'data': data,
                'up': up
            }
        
        self.codis_pf = {}
        sql = """
            SELECT pf_codi, pf_desc_gen
            FROM cpftb006
        """
        for codi, desc in u.getAll(sql, 'redics'):
            self.codis_pf[codi] = desc
        print('prescripcions ' + str(len(self.prescripcions)))
if __name__=='__main__':
    try:
        inici = d.datetime.now()
        Prescripcio()
        fi = d.datetime.now()
        mail = t.Mail()
        txt = """Hora inici {},
                hora fi {},
                total de {}""".format(str(inici), str(fi), str(fi-inici))
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "prescripcio_infermera supeeeer"
        mail.text = txt
        mail.send()
    except Exception as e:
        mail = t.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "prescripcio_infermera malament"
        mail.text = str(e)
        mail.send()
        raise
    u.printTime("End")