import sisapUtils as u
import sisaptools as t
import collections as c
from dateutil import relativedelta as rd
import os

u.printTime('inici')
DEXTD = u.getOne("select data_ext from dextraccio", "nodrizas")[0]
menys7 = DEXTD - rd.relativedelta(days=7)

sql = """
    SELECT pacient, sector, up, visi_id, sisap_catprof_codi, sisap_catprof_class, origen
    FROM dwsisap.sisap_master_visites
    WHERE sisap_servei_codi is null
    AND data BETWEEN DATE '{menys7}'
    AND DATE '{DEXTD}'
""".format(menys7=menys7, DEXTD=DEXTD)

file_name = u.tempFolder + 'no_homologats.csv'
visites = [('pacient', 'sector', 'up', 'id_visita', 'codi_categoria_professional', 'classe_categoria_professional', 'origen')]
for row in u.getAll(sql, 'exadata'):
    visites.append(tuple(row))
u.printTime('visites')

u.writeCSV(file_name, visites, sep = ";")
u.printTime('export')

u.sendGeneral(
me='sisap@gencat.cat',
to=['nuriacantero@gencat.cat'],
cc=[],
subject='prova visites',
text= """
Nous serveis no homologats a master_visites dels ultims 7 dies""",
file=file_name
)
os.remove(file_name)
u.printTime('fi')
