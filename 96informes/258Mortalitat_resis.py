# -*- coding: utf8 -*-

"""
Mortalitat residències
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

tb = 'sisap_res_mort_dpt'


upload = []

sql = """
        SELECT resis.hash, rcn.abs, municipi, resis.codi, resis.nom, resis.abs, rcn.DATA_NAIXEMENT , decode(rcn.sexe, 0, 'Home', 1, 'Dona', 'No classificat') , entrada, sortida, data_defuncio, exitus_covid, cas_data_cas, cas_reinfeccio from
        (SELECT rc.hash, rc.codi, rc.nom, rc.abs, min(data) entrada, max(data) sortida 
        FROM dwsisap.RESIDENCIES_CENS rc 
        INNER join dwsisap.RESIDENCIES_CATALEG rc
        ON residencia=codi
        WHERE perfil='Resident' AND tipus= 1 GROUP BY rc.hash, rc.codi, rc.nom, rc.abs)resis
        left JOIN dwsisap.RCA_CIP_NIA rcn 
        ON resis.hash=rcn.hash 
        LEFT JOIN dwsisap.DBC_METRIQUES dm 
        ON resis.hash=dm.hash"""
for hash, abs, municipi, codi, nom, abs2, naix, sexe, entrada, sortida, defuncio, covid, cas, reinfeccio in u.getAll(sql, 'exadata2'):
    upload.append([hash, abs, municipi, codi, nom, abs2, naix, sexe, entrada, sortida, defuncio, covid, cas, reinfeccio])
    
db = 'redics'

cols = ("hash VARCHAR2(40)", "abs VARCHAR2(10)" , "municipi VARCHAR2(10)", "codi_residencia VARCHAR2(10)", "nom_residencia VARCHAR2(400)", "abs_residencia VARCHAR2(10)", "data_naixement date", "sexe VARCHAR2(20)", "entrada date", "sortida date",
"data_defuncio date", "exitus_covid date", "cas_covid date", "reinfeccio_covid date")
u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
u.listToTable(upload, tb, db)

users= ['PREDUEHE', 'PREDUMBO', 'PREDUMMP','PREDULMB']
for user in users:
    u.execute("grant select on {} to {}".format(tb,user),db)
