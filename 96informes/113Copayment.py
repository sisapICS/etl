# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

debug = False

nod = 'nodrizas'
imp = 'import'
farmacs = '(713, 714, 719, 720, 717, 718, 715, 716)'

exclusions_p = '(79, 710, 711, 712, 376, 383)'

def farmconverter(farmac):
    
    if farmac in (82, 658, 216, 659):
        far = 'Estatines'
    elif farmac in (713, 714):
        far = 'MSRE'
    elif farmac in (715, 716):
        far = 'Denosumab'
    elif farmac in (717, 718):
        far = 'Bifosfonats'
    elif farmac in (719, 720):
        far = 'Altres aOP'
    else:
        far = 'error'
    return far

def psconverter(ps):
    
    if ps in ('M16','M16.0','M16.1','M16.2','M16.3','M16.5','M16.6','M16.7','M16.9'):
        prob = 'ARTR_CAD'   
    elif ps in ('M17','M17.0','M17.1','M17.2','M17.3','M17.4','M17.5','M17.9'):
        prob = 'ARTR_GEN'
    elif ps in ('M54.3','M54.4'):
        prob = 'LUMBOCIATALGIA'
    elif ps in ('S72','S72.0','S72.1','S72.9'):
        prob = 'FX_FRAG1'
    elif ps in ('S32.1','S32.2','S32.3','S32.4','S32.5','S32.8'):
        prob = 'FX_FRAG12'
    elif ps in ('S82.2', 'S82.3'):
        prob = 'FX_FRAG13'
    elif ps in ('S22.0','S22.1','S32','S32.0','S32.7','T08'):
        prob = 'FX_FRAG2'
    elif ps in ('S72.4'):
        prob = 'FX_FRAG3'
    elif ps in ('S82.1'):
        prob = 'FX_FRAG4'
    elif ps in ('S42','S42.2'):
        prob = 'FX_FRAG6'
    elif ps in ('S52','S52.2','S52.3','S52.4','S52.5','S52.6','S52.7','S52.8','S52.9','S62'):
        prob = 'FX_FRAG7'
    elif ps in ('S22','S22.2','S22.3','S22.8','S22.9','S42.0','S42.1','S42.3','S42.4','S42.7','S42,8','S42.9','S52.0','S52.1','S62.0','S62.1','S72.7','S72.8','S82','S82.0','S82.4','S82.5','S82.6','S82.7','S82.8','S82.9','S92','S92.0','S92.1','S92.2','S92.7','S92.9'):
        prob = 'FX_FRAG8'
    else:
        prob = 'error'
    return prob


anys = [2009, 2010, 2011, 2012, 2013,2014,2015,2016]
#anys = [2016]
#mesos = ['01']
mesos = ['01','02','03','04','05','06','07','08','09','10','11','12']
'''
u11 = {}
all_cips = {}
sql = 'select id_cip, id_cip_sec,hash_d from u11{0}'.format( ' limit 10' if debug else '')
for id, idsec,hash_d in getAll(sql, imp):
    u11[hash_d] = id
    all_cips[idsec] = id
    
id_sidiap = {}
sql = 'select id, id_persistent from poblacio {}'.format( ' limit 10' if debug else '')
for id, idP in getAll(sql, ('SISAP_estatines','nym_proj')):
    a = tuple(idP.split(':'))
    idP = a[0]
    id_sidiap[int(id)] = idP
'''

centres = {}

sql = "select sha1(concat(scs_codi,'EQA')), medea from cat_centres where ep='0208'"
for up, medea in getAll(sql, nod):
    centres[up] = medea
    
#dates naixement tota la pob

all_pob = {}
sql = 'select id_cip, usua_sexe, year(usua_data_naixement) from assignada {0}'. format(' limit 10' if debug else '')
for id,  sexe, naix in getAll(sql, imp):
    all_pob[id] = {'sexe':sexe, 'naix': naix}

#problemes salut:

agrs = []
conv = {}

in_crit = "('M16','M16.0','M16.1','M16.2','M16.3','M16.5','M16.6','M16.7','M16.9','M17','M17.0','M17.1','M17.2','M17.3','M17.4','M17.5','M17.9','M54.3','M54.4','S72','S72.0','S72.1','S72.9', 'S32.1','S32.2','S32.3','S32.4','S32.5','S32.8','S82.2', 'S82.3','S22.0','S22.1','S32','S32.0','S32.7','T08','S72.4','S82.1','S42','S42.2','S52','S52.2','S52.3','S52.4','S52.5','S52.6','S52.7','S52.8','S52.9','S62', 'S22','S22.2','S22.3','S22.8','S22.9','S42.0','S42.1','S42.3','S42.4','S42.7','S42,8','S42.9','S52.0','S52.1','S62.0','S62.1','S72.7','S72.8','S82','S82.0','S82.4','S82.5','S82.6','S82.7','S82.8','S82.9','S92','S92.0','S92.1','S92.2','S92.7','S92.9')"
incidents = defaultdict(list)
sql = "select id_cip, pr_cod_ps, pr_dde, extract(year_month from pr_dde) from problemes where  pr_cod_o_ps = 'C' and pr_hist = 1 and pr_cod_ps in {0}  and pr_data_baixa is null {1}".format(in_crit, ' limit 10' if debug else '')
for id, ps, dde, Ydde in getAll(sql, imp):
    prob = psconverter(ps)
    Ydde = str(Ydde)
    incidents[id, prob].append(Ydde)

    

    
for ane in anys:
    resultat1, resultat2, resultat3, resultat45, resultat5, resultat6, resultat7, resultat8, resultat9, resultat10, resultat11, resultat12 = Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter()
    resultat13, resultat14, resultat15, resultat16, resultat17, resultat18, resultat19, resultat20, resultat21, resultat22, resultat23, resultat24 =Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter()
    assignada = {}
    sql = "select id_cip, sha1(concat(up,'EQA')), ates from assignadahistorica where dataany={0} {1}".format(ane, ' limit 1' if debug else '')
    for id, up, ates in getAll(sql, imp):
        if up in centres:
            medea = centres[up]
            assig = 1
            sexe = 'NS'
            edat, edat5 = 'NS', 'NS'
            try:
                sexe = all_pob[id]['sexe']
                naix = all_pob[id]['naix']
                edat = ane - naix
            except KeyError:
                pass
            if edat < 15:
                continue
            if edat <>'NS':
                edat5 = ageConverter(edat)
            for mes in mesos:
                periode = str(ane) + mes
                ARTR_CAD,ARTR_GEN, LUMBOCIATALGIA,FX_FRAG1,FX_FRAG12,FX_FRAG13, FX_FRAG2,FX_FRAG3, FX_FRAG4, FX_FRAG6, FX_FRAG7,FX_FRAG8 = 0,0,0,0,0,0,0,0,0,0,0,0
                if (id, 'ARTR_CAD') in incidents:
                    for inicialM in incidents[id, 'ARTR_CAD']:
                        if inicialM == periode:
                            ARTR_CAD = 1
                if (id, 'ARTR_GEN') in incidents:
                    for inicialM in incidents[id, 'ARTR_GEN']:
                        if inicialM == periode:
                            ARTR_GEN = 1
                if (id, 'LUMBOCIATALGIA') in incidents:
                    for inicialM in incidents[id, 'LUMBOCIATALGIA']:
                        if inicialM == periode:
                            LUMBOCIATALGIA = 1
                if (id, 'FX_FRAG1') in incidents:
                    for inicialM in incidents[id, 'FX_FRAG1']:
                        if inicialM == periode:
                            FX_FRAG1 = 1
                if (id, 'FX_FRAG12') in incidents:
                    for inicialM in incidents[id, 'FX_FRAG12']:
                        if inicialM == periode:
                            FX_FRAG12 = 1
                if (id, 'FX_FRAG13') in incidents:
                    for inicialM in incidents[id, 'FX_FRAG13']:
                        if inicialM == periode:
                            FX_FRAG13 = 1
                if (id, 'FX_FRAG2') in incidents:
                    for inicialM in incidents[id, 'FX_FRAG2']:
                        if inicialM == periode:
                            FX_FRAG2 = 1
                if (id, 'FX_FRAG3') in incidents:
                    for inicialM in incidents[id, 'FX_FRAG3']:
                        if inicialM == periode:
                            FX_FRAG3 = 1
                if (id, 'FX_FRAG4') in incidents:
                    for inicialM in incidents[id, 'FX_FRAG4']:
                        if inicialM == periode:
                            FX_FRAG4 = 1
                if (id, 'FX_FRAG6') in incidents:
                    for inicialM in incidents[id, 'FX_FRAG6']:
                        if inicialM == periode:
                            FX_FRAG6 = 1
                if (id, 'FX_FRAG7') in incidents:
                    for inicialM in incidents[id, 'FX_FRAG7']:
                        if inicialM == periode:
                            FX_FRAG7 = 1
                if (id, 'FX_FRAG8') in incidents:
                    for inicialM in incidents[id, 'FX_FRAG8']:
                        if inicialM == periode:
                            FX_FRAG8 = 1
                resultat11[(periode, up, medea, edat5, sexe)] += assig
                resultat13[(periode, up, medea, edat5, sexe)] += ARTR_CAD
                resultat14[(periode, up, medea, edat5, sexe)] += LUMBOCIATALGIA
                resultat15[(periode, up, medea, edat5, sexe)] += FX_FRAG1
                resultat16[(periode, up, medea, edat5, sexe)] += FX_FRAG12
                resultat17[(periode, up, medea, edat5, sexe)] += FX_FRAG13
                resultat18[(periode, up, medea, edat5, sexe)] += FX_FRAG2
                resultat19[(periode, up, medea, edat5, sexe)] += FX_FRAG3
                resultat20[(periode, up, medea, edat5, sexe)] += FX_FRAG4
                resultat21[(periode, up, medea, edat5, sexe)] += FX_FRAG6
                resultat22[(periode, up, medea, edat5, sexe)] += FX_FRAG7
                resultat23[(periode, up, medea,  edat5, sexe)] += FX_FRAG8
                resultat24[(periode, up, medea,  edat5, sexe)] += ARTR_GEN
                resultat45[(periode, up, medea, edat5, sexe)] += assig
    upload = []
    for (periode, up, medea, edat5, sexe), res45 in resultat45.items():
        res11 = resultat11[(periode, up, medea, edat5, sexe)]
        res13 = resultat13[(periode, up, medea, edat5, sexe)]
        res14 = resultat14[(periode, up, medea, edat5, sexe)]
        res15 = resultat15[(periode, up, medea, edat5, sexe)]
        res16 = resultat16[(periode, up, medea, edat5, sexe)]
        res17 = resultat17[(periode, up, medea, edat5, sexe)]
        res18 = resultat18[(periode, up, medea, edat5, sexe)]
        res19 = resultat19[(periode, up, medea, edat5, sexe)]
        res20 = resultat20[(periode, up, medea, edat5, sexe)]
        res21 = resultat21[(periode, up, medea, edat5, sexe)]
        res22 = resultat22[(periode, up, medea, edat5, sexe)]
        res23 = resultat23[(periode, up, medea, edat5, sexe)]
        res24 = resultat24[(periode, up, medea, edat5, sexe)]
        upload.append([periode, up, medea, edat5, sexe, res11,res13,res24, res14,res15,res16,res17,res18,res19,res20,res21,res22,res23])
    file = tempFolder + 'projecte_copayment' + str(ane) + '.txt'
    writeCSV(file, upload, sep=';')

    