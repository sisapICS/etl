#  coding: latin1

import sisapUtils as u
import collections as c
import psutil as p
from dateutil import relativedelta as rd
from datetime import *
import calendar
import time


tb = "optimap"
db = "altres"
file = "OPTIMAP"

data_inicial_calcul_optimap = date(2021, 9, 1)  
data_final_calcul_optimap = date(2022, 7, 31)  

ANYS_CONSIDERATS = (2021,2022,)
MESOS_CONSIDERATS = ((10,2021), (11,2021), (12,2021), (1,2022), (2,2022), (3,2022), (4,2022), (5,2022), (6,2022))
MESOS_CONSIDERATS_AMPLIAT = ((9,2021), (10,2021), (11,2021), (12,2021), (1,2022), (2,2022), (3,2022), (4,2022), (5,2022), (6,2022), (7,2022))

class Optimap(object):
    """ . """

    def __init__(self):
        """ . """

        self.resultat = list()
        self.resultat_counter = c.defaultdict(c.Counter)
        self.resultat_detall = c.defaultdict(lambda : c.defaultdict(set))

        self.get_centres();                             print("Fet: self.get_centres()")
        self.get_professionals();                       print("Fet: self.get_professionals()")
        self.get_assignada();                           print("Fet: self.get_assignada()")
        self.get_visites_subtaules();                   print("Fet: self.get_visites_subtaules()")
        self.get_visites();                             print("Fet: self.get_visites()")
        self.get_urg_visites();                         print("Fet: self.get_urg_visites()")
        self.get_urg_visites_hospital();                print("Fet: self.get_urg_visites_hospital()")
        self.get_problemes();                           print("Fet: self.get_problemes()")
        self.get_mg_dosi();                             print("Fet: self.get_mg_dosi()")
        self.get_taxa_prescripcio_antibiotics();        print("Fet: self.get_taxa_prescripcio_antibiotics()")
        self.get_indicadors_s();                        print("Fet: self.get_indicadors_s()")
        self.get_resultat();                            print("Fet: self.get_resultat()")
        self.get_r3();                                  print("Fet: self.get_r3()")
        self.export_klx();                              print("Fet: self.export_klx()")

    def get_centres(self):
        """."""

        sql = """
                SELECT
                    scs_codi,
                    ics_desc,
                    ics_codi
                FROM
                    cat_centres
              """
        self.centres = {up: {"br": br, "up_desc": up_desc} for (up, up_desc, br) in u.getAll(sql, "nodrizas")}


    def get_professionals(self):
        """."""

        self.upuba2numcol = dict()
        centres_tots_pediatres = set()
        self.ideusu2numcol = dict()
        self.professionals = dict()
        self.numcol2nomcomplet = dict()

        FILE = "optimap_professionals.txt"
        for up, numcol in u.readCSV(FILE, sep="@"):
            if numcol != "*":
                if numcol[0] == "1":
                    numcol = str(numcol)
                    self.professionals[numcol] = up
            else:
                centres_tots_pediatres.add(up)

        sql_1 = """
                    SELECT
                        up,
                        uab,
                        LEFT(ide_numcol, 8),
                        concat(nom, ' ', cognom1, ' ', cognom2) nom_complet
                    FROM
                        cat_professionals
                """
        for up, uba, numcol, nom_complet in u.getAll(sql_1, "import"):
            if numcol:
                numcol = str(numcol)
                if numcol[0] == "1":
                    if up in centres_tots_pediatres:
                        self.professionals[numcol] = up
                    if numcol in self.professionals:
                        self.upuba2numcol[(up,uba)] = numcol
                        self.numcol2nomcomplet[numcol] = nom_complet
       
        sql_2 = """
                    SELECT
                        ide_usuari,
                        LEFT(ide_numcol, 8)
                    FROM
                        cat_pritb992
                """
        self.ideusu2numcol = {ide_usuari: str(numcol) for ide_usuari, numcol in u.getAll(sql_2, "import") if numcol in self.professionals}

        FILE_2 = "optimap_professionals_restants.txt"
        for numcol, ide_usuari, up, uba, nom_complet in u.readCSV(FILE_2, sep="@"):
            self.professionals[numcol] = up
            self.upuba2numcol[(up,uba)] = numcol
            self.numcol2nomcomplet[numcol] = nom_complet
            self.ideusu2numcol[ide_usuari] = str(numcol)


    def get_assignada(self):
        """."""
        
        self.dates_naixement = dict()
        D1_poblacio_assignada = c.defaultdict(set)
        self.D1_poblacio_assignada = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(dict)))
        self.hash_2_idcipsec = dict()
        self.cipreal_2_hash = dict()
        self.nia_2_cip_real = dict()

        sql_0 = """
                    SELECT
                        id_cip_sec,
                        usua_data_naixement
                    FROM
                        assignada
                    WHERE
                        YEAR(usua_data_naixement) >= 2006
                """
        for id_cip_sec, data_naix in u.getAll(sql_0, "import"):
            try:
                data_naix = data_naix.date()
            except:
                data_naix = data_naix
            self.dates_naixement[id_cip_sec] = data_naix

        # sql_1 = """
        #             SELECT
        #                 id_cip_sec,
        #                 up,
        #                 uba
        #             FROM
        #                 assignada_tot
        #             WHERE
        #                 YEAR(data_naix) >= 2006                        
        #         """
        # for id_cip_sec, up, uba in u.getAll(sql_1, "nodrizas"):
        #     data_naix = self.dates_naixement[id_cip_sec]
        #     if (up, uba) in self.upuba2numcol and data_naix.year >= self.any_actual - 16:
        #         numcol = self.upuba2numcol[(up,uba)]
        #         for mes in range(1, self.mes_actual + 1):
        #             final_de_mes = date(self.any_actual, mes, calendar.monthrange(self.any_actual, mes)[1])
        #             data_naixement = self.dates_naixement[id_cip_sec]
        #             edat_al_final_de_mes = u.yearsBetween(data_naixement, final_de_mes)
        #             if data_naixement <= final_de_mes and edat_al_final_de_mes < 15:
        #                 self.D1_poblacio_assignada[self.any_actual][mes][id_cip_sec] = {"up": up, "numcol": numcol}        
        #                 self.resultat_counter[(self.any_actual, mes, numcol)]["D1_poblacio_assignada"] += 1
           

        any_calcul = ANYS_CONSIDERATS[-1]
        sql_2 = """
                    SELECT
                        id_cip_sec,
                        up,
                        uab
                    FROM
                        assignadahistorica_s{}
                """.format(any_calcul-1)    # Utilitzaré assignadahistorica_s2021 per l'any 2022
        for id_cip_sec, up, uba in u.getAll(sql_2, "import"):
            if (up, uba) in self.upuba2numcol and id_cip_sec in self.dates_naixement:
                data_naixement = self.dates_naixement[id_cip_sec]
                numcol = self.upuba2numcol[(up,uba)]
                for (mes, any) in MESOS_CONSIDERATS:
                    final_de_mes = date(any, mes, calendar.monthrange(any, mes)[1])
                    edat_al_final_de_mes = u.yearsBetween(data_naixement, final_de_mes)
                    if data_naixement <= final_de_mes and edat_al_final_de_mes < 15:
                        self.D1_poblacio_assignada[any][mes][id_cip_sec] = {"up": up, "numcol": numcol}
                        self.D1_poblacio_assignada[any]["total"][id_cip_sec] = {"up": up, "numcol": numcol}
                        D1_poblacio_assignada[(any, mes, numcol)].add(id_cip_sec)
                        D1_poblacio_assignada[(any, "total", numcol)].add(id_cip_sec)

        for (any, mes, numcol), pacients in D1_poblacio_assignada.items():
            self.resultat_counter[(any, mes, numcol)]["D1_poblacio_assignada"] += len(pacients)

        sql_3 = """
                    SELECT
                        id_cip_sec,
                        hash_d
                    FROM
                        u11
                """
        for id_cip_sec, hash_codi in u.getAll(sql_3, "import"):
            self.hash_2_idcipsec[hash_codi] = id_cip_sec

        sql_4 = """
                    SELECT
                        usua_cip AS cip_real,
                        usua_cip_cod AS hash
                    FROM
                        pdptb101
                """
        for cip_real, hash_codi in u.getAll(sql_4, "pdp"):
            self.cipreal_2_hash[cip_real[:13]] = hash_codi

        sql_5 = """
                    SELECT
                        nia,
                        cip AS cip_real
                    FROM
                        dwsisap.RCA_CIP_NIA
                """
        for nia, cip_real in u.getAll(sql_5, "exadata"):
            self.nia_2_cip_real[nia] = cip_real[:13]


    def get_visites_subtaules(self):
        """ . """

        self.visit_subtaules = set()
        self.subtaules_any = dict()     

        for taula, any_contingut_taula, mes_contingut_taula in u.multiprocess(get_visites_subtaules_particio, u.getSubTables("visites"), 8):
            if taula[-6:] != '_s6951' and (mes_contingut_taula, any_contingut_taula) in MESOS_CONSIDERATS_AMPLIAT:
                self.visit_subtaules.add(taula)
                self.subtaules_any[taula] = any_contingut_taula            


    def get_visites(self):
        """."""
        
        D2_poblacio_atesa = c.defaultdict(set)
        vars_funcio = [(subtaula, self.professionals, self.dates_naixement) for subtaula in self.visit_subtaules]       
        workers = 8 if (p.virtual_memory()[0] / 1024**3) > 50 else 4

        # for resultat_counter, resultat_detall in u.multiprocess(get_visites_subtaula, vars_funcio, workers):
        for input in vars_funcio:
            sub_D2_poblacio_atesa, resultat_detall = get_visites_subtaula(input)
            for elem_1 in sub_D2_poblacio_atesa:
                D2_poblacio_atesa[elem_1].update(sub_D2_poblacio_atesa[elem_1])
            for elem_1 in resultat_detall:
                for elem_2 in resultat_detall[elem_1]:
                    self.resultat_detall[elem_1][elem_2].update(resultat_detall[elem_1][elem_2])

        for (any_visita, mes_visita, numcol), pacients in D2_poblacio_atesa.items():
            self.resultat_counter[(any_visita, mes_visita, numcol)]["D2_poblacio_atesa"] = len(pacients)


    def get_urg_visites(self):
        """."""
        
        self.urg_visites = c.defaultdict(set)

        sql_1 = """
                    SELECT
                        id_cip_sec,
                        vcu_dia_peticio
                    FROM
                        urg_visites
                    WHERE
                        vcu_dia_peticio BETWEEN '{}' AND '{}'
                """.format(data_inicial_calcul_optimap, data_final_calcul_optimap)
        for id_cip_sec, dia_visita in u.getAll(sql_1, "import"):
            self.urg_visites[id_cip_sec].add(dia_visita)


    def get_urg_visites_hospital(self):
        """."""

        self.urg_visites_hospital = c.defaultdict(set)

        sql = """
                SELECT
                    c_nia,
                    c_data_entr_f
                FROM
                    dwcatsalut.CS_URGENCIES
                WHERE
                    c_data_entr_f BETWEEN TO_DATE('{}', 'yyyy/mm/dd') AND TO_DATE('{}', 'yyyy/mm/dd')
              """.format(data_inicial_calcul_optimap, data_final_calcul_optimap)
        
        for nia, data in u.getAll(sql, "exadata"): 
            try:
                try:
                    data = data.date()
                except:
                    data = data
                cip_real = self.nia_2_cip_real[nia]
                hash_codi = self.cipreal_2_hash[cip_real]
                id_cip_sec = self.hash_2_idcipsec[hash_codi]
                self.urg_visites_hospital[id_cip_sec].add(data)
            except:
                next


    def get_problemes(self):
        """."""

        self.D3_dx = c.defaultdict(lambda : c.defaultdict(set))
        vars_funcio = [(sector, self.ideusu2numcol) for sector in u.sectors]       
        workers = 8 if (p.virtual_memory()[0] / 1024**3) > 50 else 4

        # for D3_dx, resultat_counter, resultat_detall in u.multiprocess(get_problemes_sector, vars_funcio, workers):
        #     for elem_1 in resultat_counter:
        #         for elem_2, n in resultat_counter[elem_1].items():
        #             self.resultat_counter[elem_1][elem_2] += n
        #     for elem_1 in D3_dx:
        #         for elem_2 in D3_dx[elem_1]:
        #             self.D3_dx[elem_1][elem_2].update(D3_dx[elem_1][elem_2])
        #     for elem_1 in resultat_detall:
        #         for elem_2 in resultat_detall[elem_1]:
        #             self.resultat_detall[elem_1][elem_2].update(resultat_detall[elem_1][elem_2])    

        for elem in vars_funcio:
            D3_dx, resultat_counter, resultat_detall = get_problemes_sector(elem)
            for elem_1 in resultat_counter:
                for elem_2, n in resultat_counter[elem_1].items():
                    self.resultat_counter[elem_1][elem_2] += len(n)
            for elem_1 in D3_dx:
                for elem_2 in D3_dx[elem_1]:
                    self.D3_dx[elem_1][elem_2].update(D3_dx[elem_1][elem_2])
            for elem_1 in resultat_detall:
                for elem_2 in resultat_detall[elem_1]:
                    self.resultat_detall[elem_1][elem_2].update(resultat_detall[elem_1][elem_2])   


    def get_mg_dosi(self):
        """."""

        self.mg_dosi, self.dosis_diaries_definides = dict(), dict()
        
        FILE = "optimap_ddd.txt"
        for ddd, pf_codi, ce_quantitat in u.readCSV(FILE, sep="@"):
            ddd, pf_codi, ce_quantitat = float(ddd), int(pf_codi), float(ce_quantitat)
            self.mg_dosi[pf_codi] = ce_quantitat
            self.dosis_diaries_definides[pf_codi] = ddd                                                                          


    def get_taxa_prescripcio_antibiotics(self):
        """."""

        self.resultat_r3 = list()
        vars_funcio = [(sector, self.professionals, self.D3_dx, self.mg_dosi, self.dosis_diaries_definides) for sector in u.sectors]       
        workers = 8 if (p.virtual_memory()[0] / 1024**3) > 50 else 4

        # for resultat_counter in u.multiprocess(get_taxa_prescripcio_antibiotics_sector, vars_funcio, workers):
        #     for elem_1 in resultat_counter:
        #         for elem_2, n in resultat_counter[elem_1].items():
        #             self.resultat_counter[elem_1][elem_2] += n   

        for elem in vars_funcio:
            resultat_counter, resultat_r3 = get_taxa_prescripcio_antibiotics_sector(elem)
            for elem_1 in resultat_counter:
                for elem_2, n in resultat_counter[elem_1].items():
                    self.resultat_counter[elem_1][elem_2] += n
                    self.resultat_counter[elem_1][elem_2] += n
            for any_prescripcio, mes_prescripcio, numcol, codi_antibiotic, durada, frequencia, posologia_mg, dosis_diaria_prescrita, n_ddd, num_recs in resultat_r3:
                if numcol in self.numcol2nomcomplet:                
                    up = self.professionals[numcol]
                    if up in self.centres:
                        up_desc = self.centres[up]["up_desc"]
                        nom_complet = self.numcol2nomcomplet[numcol]
                        self.resultat_r3.append([any_prescripcio, mes_prescripcio, up, up_desc, numcol, nom_complet, codi_antibiotic, durada, frequencia, posologia_mg, dosis_diaria_prescrita, n_ddd, num_recs])


    def get_r3(self):
        """."""
     
        col_create = "(any varchar(4), mes varchar(5), up varchar(5), up_desc varchar(200), numcol varchar(8), nom_complet varchar(200), codi_atc varchar(7), durada int(11), freq int(11), posologia_mg double, dosis_diaria_prescrita double, n_ddd double, num_recs int(11))"

        u.createTable("optimap2", col_create, "test", rm=True)
        u.listToTable(self.resultat_r3, "optimap2", "test")


    def get_indicadors_s(self):
        """."""

        dades_s = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(set)))

        for id_cip_sec in self.resultat_detall["IRA"]:
            if id_cip_sec in self.resultat_detall["S1_complicacio_IRA"]:
                for data_1, numcol in self.resultat_detall["IRA"][id_cip_sec]:
                    for data_2 in self.resultat_detall["S1_complicacio_IRA"][id_cip_sec]:
                        if u.daysBetween(data_1, data_2) <= 30 and (data_2.month, data_2.year) in MESOS_CONSIDERATS:
                            dades_s["S1_total"][numcol][(data_2.month, data_2.year)].add(id_cip_sec)
                            dades_s["S1_total"][numcol][("total", data_2.year)].add(id_cip_sec)
            if id_cip_sec in self.resultat_detall["S1_pneumonia"]:
                for data_1, numcol in self.resultat_detall["IRA"][id_cip_sec]:
                    for data_2 in self.resultat_detall["S1_pneumonia"][id_cip_sec]:
                        if u.daysBetween(data_1, data_2) <= 30 and (data_2.month, data_2.year) in MESOS_CONSIDERATS:
                            dades_s["S1_pneumonia"][numcol][(data_2.month, data_2.year)].add(id_cip_sec)
                            dades_s["S1_pneumonia"][numcol][("total", data_2.year)].add(id_cip_sec)
            if id_cip_sec in self.resultat_detall["S1_mastoiditis"]:
                for data_1, numcol in self.resultat_detall["IRA"][id_cip_sec]:
                    for data_2 in self.resultat_detall["S1_mastoiditis"][id_cip_sec]:
                        if u.daysBetween(data_1, data_2) <= 30 and (data_2.month, data_2.year) in MESOS_CONSIDERATS:
                            dades_s["S1_mastoiditis"][numcol][(data_2.month, data_2.year)].add(id_cip_sec)
                            dades_s["S1_mastoiditis"][numcol][("total", data_2.year)].add(id_cip_sec)
            if id_cip_sec in self.resultat_detall["S1_sinusitis"]:
                for data_1, numcol in self.resultat_detall["IRA"][id_cip_sec]:
                    for data_2 in self.resultat_detall["S1_sinusitis"][id_cip_sec]:
                        if u.daysBetween(data_1, data_2) <= 30 and (data_2.month, data_2.year) in MESOS_CONSIDERATS:
                            dades_s["S1_sinusitis"][numcol][(data_2.month, data_2.year)].add(id_cip_sec)
                            dades_s["S1_sinusitis"][numcol][("total", data_2.year)].add(id_cip_sec)
            if id_cip_sec in self.resultat_detall["S1_otitis"]:
                for data_1, numcol in self.resultat_detall["IRA"][id_cip_sec]:
                    for data_2 in self.resultat_detall["S1_otitis"][id_cip_sec]:
                        if u.daysBetween(data_1, data_2) <= 30 and (data_2.month, data_2.year) in MESOS_CONSIDERATS:
                            dades_s["S1_otitis"][numcol][(data_2.month, data_2.year)].add(id_cip_sec)
                            dades_s["S1_otitis"][numcol][("total", data_2.year)].add(id_cip_sec)
            if id_cip_sec in self.resultat_detall["S2_visites"]:
                for data_1, numcol in self.resultat_detall["IRA"][id_cip_sec]:
                    for data_2 in self.resultat_detall["S2_visites"][id_cip_sec]:
                        if u.daysBetween(data_1, data_2) <= 30 and (data_2.month, data_2.year) in MESOS_CONSIDERATS:
                            dades_s["S2_visita_primaria"][numcol][(data_2.month, data_2.year)].add((id_cip_sec, data_2))
                            dades_s["S2_visita_primaria"][numcol][("total", data_2.year)].add((id_cip_sec, data_2))
            if id_cip_sec in self.urg_visites:
                for data_1, numcol in self.resultat_detall["IRA"][id_cip_sec]:
                    for data_2 in self.urg_visites[id_cip_sec]:
                        if u.daysBetween(data_1, data_2) <= 30 and (data_2.month, data_2.year) in MESOS_CONSIDERATS:
                            dades_s["S2_visita_urgencies"][numcol][(data_2.month, data_2.year)].add((id_cip_sec, data_2))
                            dades_s["S2_visita_urgencies"][numcol][("total", data_2.year)].add((id_cip_sec, data_2))
            if id_cip_sec in self.urg_visites_hospital:
                for data_1, numcol in self.resultat_detall["IRA"][id_cip_sec]:
                    for data_2 in self.urg_visites_hospital[id_cip_sec]:
                        if u.daysBetween(data_1, data_2) <= 30 and (data_2.month, data_2.year) in MESOS_CONSIDERATS:
                            dades_s["S3_visita_urgencies_hospital"][numcol][(data_2.month, data_2.year)].add((id_cip_sec, data_2))
                            dades_s["S3_visita_urgencies_hospital"][numcol][("total", data_2.year)].add((id_cip_sec, data_2))
        for concepte in dades_s:
            for numcol in dades_s[concepte]:
                for (mes_total, any_total), dades in dades_s[concepte][numcol].items():
                    self.resultat_counter[(any_total, mes_total, numcol)][concepte] = len(dades)

    def get_resultat(self):
        """."""

        # self.columnes_valors = ["D1_poblacio_assignada", "D2_poblacio_atesa", "D3_dx_IRA", "D3_dx_IRA1", "D3_dx_IRA2", "D3_dx_IRA3", "D3_dx_IRA4", "D3_dx_IRA5", "D3_dx_IRA6", "D3_dx_IRA7", "D3_dx_IRA9", "R1_prescripcio_antibiotics_per_pediatra", "R2_pacients_amb_antibiotic_prescrit", "R4_prescripcio_antibiotics_per_pediatra_pacients_IRA", "R4_prescripcio_antibiotics_per_pediatra_pacients_IRA1", "R4_prescripcio_antibiotics_per_pediatra_pacients_IRA2", "R4_prescripcio_antibiotics_per_pediatra_pacients_IRA3", "R4_prescripcio_antibiotics_per_pediatra_pacients_IRA4", "R4_prescripcio_antibiotics_per_pediatra_pacients_IRA5", "R4_prescripcio_antibiotics_per_pediatra_pacients_IRA6", "R4_prescripcio_antibiotics_per_pediatra_pacients_IRA7", "R4_prescripcio_antibiotics_per_pediatra_pacients_IRA9"]

        for (any, mes, numcol) in self.resultat_counter:
             for concepte in self.resultat_counter[(any, mes, numcol)]:
                if numcol in self.numcol2nomcomplet:
                    up = self.professionals[numcol]
                    if up in self.centres:
                        up_desc = self.centres[up]["up_desc"]
                        nom_complet = self.numcol2nomcomplet[numcol]
                        n = self.resultat_counter[(any, mes, numcol)][concepte]
                        self.resultat.append([any, mes, up, up_desc, numcol, nom_complet, concepte, n])

    def export_klx(self):
        """."""

        # col_create = "(any int, mes varchar(5), numcol varchar(20), "
        # for colname in self.columnes_valors:
        #     col_create += "{} int, ".format(colname)
        # col_create = col_create[:-2] + ")"

        col_create = "(any int, mes varchar(5), up varchar(5), up_desc varchar(200), numcol varchar(20), nom_complet varchar(200), concepte varchar(100), n int)"

        u.createTable("optimap1", col_create, "test", rm=True)
        u.listToTable(self.resultat, "optimap1", "test")
        sql = """
                SELECT
                    ind,
                    "Aperiodo",
                    br,
                    analisis,
                    "NOCAT",
                    "NOIMP",
                    "DIM6SET",
                    "N",
                    n
                FROM
                    {}.{}
              """.format(db, tb)
        # u.exportKhalix(sql, file)


def get_visites_subtaules_particio(taula):

    sql = """
            SELECT
                YEAR(visi_data_visita),
                MONTH(visi_data_visita)
            FROM
                {}
            LIMIT 1
          """
    any_contingut_taula, mes_contingut_taula = u.getOne(sql.format(taula), "import")
    return(taula, any_contingut_taula, mes_contingut_taula)        

def get_visites_subtaula(input):
    """."""

    table, professionals, dates_naixement = input

    D2_poblacio_atesa = c.defaultdict(set)
    resultat_detall = c.defaultdict(lambda : c.defaultdict(set))

    sql = """
            SELECT
                id_cip_sec,
                visi_data_visita,
                month(visi_data_visita),
                year(visi_data_visita),
                visi_up,
                LEFT(visi_col_prov_resp, 8)
            FROM
                {}
            WHERE
                visi_situacio_visita = 'R'
                AND visi_col_prov_resp != ''
                AND visi_data_visita BETWEEN '{}' AND '{}'
            """.format(table, data_inicial_calcul_optimap, data_final_calcul_optimap)
    for id_cip_sec, data_visita, mes_visita, any_visita, up, numcol in u.getAll(sql, "import"):
        try:
            data_visita = data_visita.date()
        except:
            data_visita = data_visita
            numcol = str(numcol)
        if numcol in professionals and id_cip_sec in dates_naixement:
            data_naixement = dates_naixement[id_cip_sec]
            edat = u.yearsBetween(data_naixement, data_visita)
            if edat < 15 and (mes_visita, any_visita) in MESOS_CONSIDERATS:
                D2_poblacio_atesa[(any_visita, mes_visita, numcol)].add(id_cip_sec)
                D2_poblacio_atesa[(any_visita, "total", numcol)].add(id_cip_sec)
        resultat_detall["S2_visites"][id_cip_sec].add(data_visita)

    print("      Fet: get_visites_subtaula({})".format(table))

    return D2_poblacio_atesa, resultat_detall


def get_problemes_sector(input):
    """."""

    sector, ideusu2numcol = input

    D3_dx = c.defaultdict(lambda : c.defaultdict(set))
    resultat_counter = c.defaultdict(lambda : c.defaultdict(set))
    resultat_detall = c.defaultdict(lambda : c.defaultdict(set))

    sql = """
            SELECT
                id_cip_sec,
                pr_v_dat,
                month(pr_v_dat),
                year(pr_v_dat),
                pr_cod_ps,
                pr_usu
            FROM
                problemes_s{}
            WHERE
                    (pr_cod_ps LIKE '%C01-H65%'
                     OR pr_cod_ps LIKE '%C01-H66%'
                     OR pr_cod_ps LIKE '%C01-H67%'
                     OR pr_cod_ps LIKE '%C01-J00%'
                     OR pr_cod_ps LIKE '%C01-J01%'
                     OR pr_cod_ps LIKE '%C01-J02%'
                     OR pr_cod_ps LIKE '%C01-J03%'
                     OR pr_cod_ps LIKE '%C01-J04%'
                     OR pr_cod_ps LIKE '%C01-J05%'
                     OR pr_cod_ps = 'C01-J06.0'
                     OR pr_cod_ps LIKE '%C01-J06.9%'
                     OR pr_cod_ps LIKE '%C01-J09%'
                     OR pr_cod_ps LIKE '%C01-J10%'
                     OR pr_cod_ps LIKE '%C01-J11%'
                     OR pr_cod_ps LIKE '%C01-J20%'
                     OR pr_cod_ps LIKE '%C01-J21%'
                     OR pr_cod_ps LIKE '%C01-R69%'
                     )
                and pr_v_dat BETWEEN '{}' AND '{}'
            """.format(sector, data_inicial_calcul_optimap, data_final_calcul_optimap)
    for id_cip_sec, data_dx, mes_dx, any_dx, cod_dx, ide_usu in u.getAll(sql, "import"):
        try:
            data_dx = data_dx.date()
        except:
            data_dx = data_dx
        # if any_dx not in range(data_inicial_calcul_optimap.year, any_actual + 1):
        #     continue
        # if any_dx == any_actual:
        #     if mes_dx > mes_actual:
        #         continue
        # elif any_dx == data_inicial_calcul_optimap.year:
        #     if mes_dx < 10:
        #         continue        
        if ide_usu in ideusu2numcol:
            numcol = ideusu2numcol[ide_usu]
            D3_dx["IRA"][(any_dx, mes_dx)].add(id_cip_sec)
            D3_dx["IRA"][(any_dx, "total")].add(id_cip_sec)
            resultat_detall["IRA"][id_cip_sec].add((data_dx, numcol))
            if (mes_dx, any_dx) in MESOS_CONSIDERATS:
                resultat_counter[(any_dx, mes_dx, numcol)]["D3_dx_IRA"].add(id_cip_sec)
                resultat_counter[(any_dx, "total", numcol)]["D3_dx_IRA"].add(id_cip_sec)
            if 'C01-J09' in cod_dx or 'C01-J10' in cod_dx or 'C01-J11' in cod_dx:
                D3_dx["IRA1"][(any_dx, mes_dx)].add(id_cip_sec)
                D3_dx["IRA1"][(any_dx, "total")].add(id_cip_sec)
                if (mes_dx, any_dx) in MESOS_CONSIDERATS:
                    resultat_counter[(any_dx, mes_dx, numcol)]["D3_dx_IRA1"].add(id_cip_sec)
                    resultat_counter[(any_dx, "total", numcol)]["D3_dx_IRA1"].add(id_cip_sec)
            elif 'C01-J00' in cod_dx or 'C01-J06.9' in cod_dx:
                D3_dx["IRA2"][(any_dx, mes_dx)].add(id_cip_sec)
                D3_dx["IRA2"][(any_dx, "total")].add(id_cip_sec)
                if (mes_dx, any_dx) in MESOS_CONSIDERATS:
                    resultat_counter[(any_dx, mes_dx, numcol)]["D3_dx_IRA2"].add(id_cip_sec)
                    resultat_counter[(any_dx, "total", numcol)]["D3_dx_IRA2"].add(id_cip_sec)
            elif 'C01-J01' in cod_dx:
                D3_dx["IRA3"][(any_dx, mes_dx)].add(id_cip_sec)
                D3_dx["IRA3"][(any_dx, "total")].add(id_cip_sec)
                if (mes_dx, any_dx) in MESOS_CONSIDERATS:
                    resultat_counter[(any_dx, mes_dx, numcol)]["D3_dx_IRA3"].add(id_cip_sec)
                    resultat_counter[(any_dx, "total", numcol)]["D3_dx_IRA3"].add(id_cip_sec)
            elif 'C01-J02' in cod_dx or 'C01-J03' in cod_dx:
                D3_dx["IRA4"][(any_dx, mes_dx)].add(id_cip_sec)
                D3_dx["IRA4"][(any_dx, "total")].add(id_cip_sec)
                if (mes_dx, any_dx) in MESOS_CONSIDERATS:
                    resultat_counter[(any_dx, mes_dx, numcol)]["D3_dx_IRA4"].add(id_cip_sec)
                    resultat_counter[(any_dx, "total", numcol)]["D3_dx_IRA4"].add(id_cip_sec)
            elif 'C01-J04' in cod_dx or 'C01-J05' in cod_dx or cod_dx == 'C01-J06.0':
                D3_dx["IRA5"][(any_dx, mes_dx)].add(id_cip_sec)                 
                D3_dx["IRA5"][(any_dx, "total")].add(id_cip_sec)   
                if (mes_dx, any_dx) in MESOS_CONSIDERATS:              
                    resultat_counter[(any_dx, mes_dx, numcol)]["D3_dx_IRA5"].add(id_cip_sec)                                
                    resultat_counter[(any_dx, "total", numcol)]["D3_dx_IRA5"].add(id_cip_sec)                                
            elif cod_dx == 'C01-J20' or 'C01-J21' in cod_dx:
                D3_dx["IRA6"][(any_dx, mes_dx)].add(id_cip_sec)
                D3_dx["IRA6"][(any_dx, "total")].add(id_cip_sec)
                if (mes_dx, any_dx) in MESOS_CONSIDERATS:
                    resultat_counter[(any_dx, mes_dx, numcol)]["D3_dx_IRA6"].add(id_cip_sec)
                    resultat_counter[(any_dx, "total", numcol)]["D3_dx_IRA6"].add(id_cip_sec)
            elif cod_dx == 'C01-H65' or 'C01-H66' in cod_dx or 'C01-H67' in cod_dx:
                D3_dx["IRA7"][(any_dx, mes_dx)].add(id_cip_sec)
                D3_dx["IRA7"][(any_dx, "total")].add(id_cip_sec)
                if (mes_dx, any_dx) in MESOS_CONSIDERATS:
                    resultat_counter[(any_dx, mes_dx, numcol)]["D3_dx_IRA7"].add(id_cip_sec)
                    resultat_counter[(any_dx, "total", numcol)]["D3_dx_IRA7"].add(id_cip_sec)
            elif 'C01-R69' in cod_dx:
                D3_dx["IRA9"][(any_dx, mes_dx)].add(id_cip_sec)
                D3_dx["IRA9"][(any_dx, "total")].add(id_cip_sec)
                if (mes_dx, any_dx) in MESOS_CONSIDERATS:
                    resultat_counter[(any_dx, mes_dx, numcol)]["D3_dx_IRA9"].add(id_cip_sec)     
                    resultat_counter[(any_dx, "total", numcol)]["D3_dx_IRA9"].add(id_cip_sec)     

    sql = """
            SELECT
                id_cip_sec,
                pr_v_dat,
                pr_cod_ps,
                pr_usu
            FROM
                problemes_s{}
            WHERE
                pr_v_dat BETWEEN '{}' AND '{}'
                AND (pr_cod_ps LIKE '%C01-H65%'
                     OR pr_cod_ps LIKE '%C01-H66%'
                     OR pr_cod_ps LIKE '%C01-H70%'
                     OR pr_cod_ps LIKE '%C01-J01%'
                     OR pr_cod_ps LIKE '%C01-J12%'
                     OR pr_cod_ps LIKE '%C01-J13%'
                     OR pr_cod_ps LIKE '%C01-J14%'                     
                     OR pr_cod_ps LIKE '%C01-J15%'
                     )
            """.format(sector, data_inicial_calcul_optimap, data_final_calcul_optimap)
    for id_cip_sec, data_dx, cod_dx, ide_usu in u.getAll(sql, "import"):
        try:
            data_dx = data_dx.date()
        except:
            data_dx = data_dx
        if ide_usu in ideusu2numcol:
            resultat_detall["S1_complicacio_IRA"][id_cip_sec].add(data_dx)
            if 'C01-H65' in cod_dx or 'C01-H66' in cod_dx:
                resultat_detall["S1_otitis"][id_cip_sec].add(data_dx)
            elif 'C01-J01' in cod_dx:
                resultat_detall["S1_sinusitis"][id_cip_sec].add(data_dx)
            elif 'C01-H70' in cod_dx:
                resultat_detall["S1_mastoiditis"][id_cip_sec].add(data_dx)                
            elif 'C01-J12' in cod_dx or 'C01-J13' in cod_dx or 'C01-J14' in cod_dx or 'C01-J15' in cod_dx:
                resultat_detall["S1_pneumonia"][id_cip_sec].add(data_dx)
            
    print("      Fet: get_problemes_sector({})".format(sector)) 

    return D3_dx, resultat_counter, resultat_detall


def get_taxa_prescripcio_antibiotics_sector(input):

    sector, professionals, D3_dx, mg_dosi, dosis_diaries_definides = input

    resultat_counter = c.defaultdict(c.Counter)
    resultat_r3 = list()
    R2_pacients_amb_antibiotic_prescrit = c.defaultdict(set)
    R4_prescripcio_antibiotics_per_pediatra_pacients = c.defaultdict(lambda: c.defaultdict(set))

    sql = """
            SELECT
                id_cip_sec,
                month(ppfmc_pmc_data_ini),
                year(ppfmc_pmc_data_ini),
                ppfmc_pf_codi,
                ppfmc_atccodi,
                LEFT(ppfmc_pmc_amb_num_col, 8),
                ppfmc_durada,
                ppfmc_freq,
                ppfmc_posologia,
                ppfmc_num_recs               
            FROM
                tractaments_s{}
            WHERE
                ppfmc_pmc_data_ini BETWEEN '{}' AND '{}'
                AND ppfmc_atccodi LIKE '%J01%'
            """.format(sector, data_inicial_calcul_optimap, data_final_calcul_optimap)
    for id_cip_sec, mes_prescripcio, any_prescripcio, codi_especialitat_farmaceutica, codi_antibiotic, numcol, durada, frequencia, posologia, num_recs in u.getAll(sql, "import"):
        codi_especialitat_farmaceutica = int(codi_especialitat_farmaceutica)
        # if any_prescripcio not in range(data_inicial_calcul_optimap.year, any_actual + 1):
        #     continue
        # if any_prescripcio == any_actual:
        #     if mes_prescripcio > mes_actual:
        #         continue
        # elif any_prescripcio == data_inicial_calcul_optimap.year:
        #     if mes_prescripcio < 10:
        #         continue     

        numcol = str(numcol)
        if numcol in professionals:
            if (mes_prescripcio, any_prescripcio) in MESOS_CONSIDERATS:
                resultat_counter[(any_prescripcio, mes_prescripcio, numcol)]["R1_prescripcio_antibiotics_per_pediatra"] += 1       
                resultat_counter[(any_prescripcio, "total", numcol)]["R1_prescripcio_antibiotics_per_pediatra"] += 1       
                R2_pacients_amb_antibiotic_prescrit[(any_prescripcio, mes_prescripcio, numcol)].add(id_cip_sec)
                R2_pacients_amb_antibiotic_prescrit[(any_prescripcio, "total", numcol)].add(id_cip_sec)

            if mes_prescripcio in range(2,12):
                mesos = [(any_prescripcio, mes_prescripcio - 1), (any_prescripcio, mes_prescripcio), (any_prescripcio, mes_prescripcio + 1)]
            elif mes_prescripcio == 1:
                mesos = [(any_prescripcio - 1, 12), (any_prescripcio, 1), (any_prescripcio, 2)]
            elif mes_prescripcio == 12:
                mesos = [(any_prescripcio, 11), (any_prescripcio, 12), (any_prescripcio + 1, 1)]

            for i in (1,2,3,4,5,6,7,9):
                for (any_prescripcio_p, mes_prescripcio_p) in mesos:
                    if (mes_prescripcio_p, any_prescripcio_p) in MESOS_CONSIDERATS:
                        if id_cip_sec in D3_dx["IRA{0}".format(i)][(any_prescripcio_p, mes_prescripcio_p)]:
                            R4_prescripcio_antibiotics_per_pediatra_pacients[(any_prescripcio_p, mes_prescripcio_p, numcol)]["IRA{0}".format(i)].add(id_cip_sec)
                            R4_prescripcio_antibiotics_per_pediatra_pacients[(any_prescripcio_p, "total", numcol)]["IRA{0}".format(i)].add(id_cip_sec)
            if codi_especialitat_farmaceutica in mg_dosi:
                posologia_mg = posologia * mg_dosi[codi_especialitat_farmaceutica]
                dosis_diaria_prescrita = (posologia_mg) * (24.0/frequencia)
                dosis_diaria_definida = dosis_diaries_definides[codi_especialitat_farmaceutica]
                n_ddd = dosis_diaria_prescrita / dosis_diaria_definida if dosis_diaria_definida != 0 else None
                resultat_r3.append((any_prescripcio, mes_prescripcio, numcol, codi_antibiotic, durada, frequencia, posologia_mg, dosis_diaria_prescrita, n_ddd, num_recs))                

    for (any_prescripcio, mes_prescripcio, numcol), pacients in R2_pacients_amb_antibiotic_prescrit.items():
            resultat_counter[(any_prescripcio, mes_prescripcio, numcol)]["R2_pacients_amb_antibiotic_prescrit"] += len(pacients)

    for (any_prescripcio, mes_prescripcio, numcol) in R4_prescripcio_antibiotics_per_pediatra_pacients:
            for concepte, pacients in R4_prescripcio_antibiotics_per_pediatra_pacients[(any_prescripcio, mes_prescripcio, numcol)].items():
                resultat_counter[(any_prescripcio, mes_prescripcio, numcol)]["R4_prescripcio_antibiotics_per_pediatra_pacients_{}".format(concepte)] += len(pacients)

    print("      Fet: get_taxa_prescripcio_antibiotics({})".format(sector))

    return resultat_counter, resultat_r3


if True:
    Optimap()