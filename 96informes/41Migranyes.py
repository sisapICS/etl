# coding: iso-8859-1
#Joan Lozano,  (ion 7630), febrer16, Ermengol

from sisapUtils import *
import csv, os, sys
from time import strftime
from collections import defaultdict,Counter

imp = 'import'
nod = 'nodrizas'

migranya = "('G43', 'G43.0', 'G43.1', 'G43.3', 'G43.8', 'G43.9')"

farmac_dict = {
                'C07AA05': {'nom': 'Propanolol', 'grup2': 'Betabloc', 'grup': 'PREVENTIU'},
                'C07AB52': {'nom': 'Metoprolol', 'grup2': 'Betabloc', 'grup': 'PREVENTIU'},
                'C07AB02': {'nom': 'Metoprolol', 'grup2': 'Betabloc', 'grup': 'PREVENTIU'},
                'C07AA12': {'nom': 'Nadolol', 'grup2': 'Betabloc', 'grup': 'PREVENTIU'},
                'C07AB03': {'nom': 'Atenolol', 'grup2': 'Betabloc', 'grup': 'PREVENTIU'},
                'N03AX11': {'nom': 'Topiramat', 'grup2': 'antiepileptics', 'grup': 'PREVENTIU'},
                'N03AG01': {'nom': 'Acid Valproic', 'grup2': 'antiepileptics', 'grup': 'PREVENTIU'},
                'N03AX09': {'nom': 'Lamotrigina', 'grup2': 'antiepileptics', 'grup': 'PREVENTIU'},
                'N07CA03': {'nom': 'Flunarizina', 'grup2': 'no', 'grup': 'PREVENTIU'},
                'N06AA09': {'nom': 'Amitriptilina', 'grup2': 'no', 'grup': 'PREVENTIU'},
                'N02CC05': {'nom': 'Almotriptan', 'grup2': 'no', 'grup': 'TRIPTANS'},
                'N02CC06': {'nom': 'Eletriptan', 'grup2': 'no', 'grup': 'TRIPTANS'},
                'N02CC07': {'nom': 'Frovatriptan', 'grup2': 'no', 'grup': 'TRIPTANS'},
                'N02CC02': {'nom': 'Naratriptan', 'grup2': 'no', 'grup': 'TRIPTANS'},
                'N02CC04': {'nom': 'Rizatriptan', 'grup2': 'no', 'grup': 'TRIPTANS'},
                'N02CC01': {'nom': 'Sumatriptan', 'grup2': 'no', 'grup': 'TRIPTANS'},
                'N02CC03': {'nom': 'Zolmitriptan', 'grup2': 'no', 'grup': 'TRIPTANS'},
                }

proves_dict = {
                'RX050': {'grup': 'TAC'},
                'RX051': {'grup': 'TAC'},
                'RA00203': {'grup': 'TAC'},
                'RA00204': {'grup': 'TAC'},
                'CN014': {'grup': 'RM'},
                'CN016': {'grup': 'RM'},
                'RA00304': {'grup': 'RM'},
                'RA00303': {'grup': 'RM'},
                'RX301': {'grup': 'RM'},
                'RX302': {'grup': 'RM'},
                }

def get_centres():
    centres = {}
    sql = "select scs_codi, ics_codi from cat_centres where ep='0208'"
    for up, br in getAll(sql, nod):
        centres[up] = br
    return centres
    
def get_crits():
    farmacs = []
    proves = []
        
    for farmac in farmac_dict:
        farmacs.append(farmac)
    in_crit_farmac = tuple(farmacs)
    for prova in proves_dict:
        proves.append(prova)
    in_crit_prova = tuple(proves)
    
    return in_crit_farmac, in_crit_prova

def get_poblacio(centres):
    poblacio = {}
    total = 0
    sql = 'select id_cip_sec, edat, up from assignada_tot'
    for id, edat, up in getAll(sql, nod):
        if up in centres:
            poblacio[int(id)] = True
            total += 1
    return poblacio, total
    
def get_tipus_migranya(cim10):
    if cim10 == 'G43.3':
        return 'Migranya cronica'
    else:
        return 'Migranya no cronica'

def get_migranya(poblacio, total):
    sql = "select id_cip_sec, pr_cod_ps,if(pr_dde > date_add(data_ext, interval -1 year), 1, 0)  from problemes, nodrizas.dextraccio \
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa is null or pr_data_baixa > data_ext) and (pr_dba is null or pr_dba > data_ext) \
            and pr_cod_ps  in {} ".format(migranya)

    migranyes = {}
    incidents = {}
    
    for id, ps, incid in getAll(sql, imp):
        if int(id) in poblacio:
            tip_migranya = get_tipus_migranya(ps)
            migranyes[id] = {'tip': tip_migranya, 'cim10': ps}
            if incid == 1:
                incidents[id] = True
    recompte = Counter()
    for id, dades in migranyes.items():
        tip = dades['tip']
        cim10 = dades['cim10']
        recompte[tip] += 1
    for tip, recomptes in recompte.items():
        print tip, recomptes
    print total
    inci = 0
    for id, rec in incidents.items():
        inci += 1
    print 'migranyes incidents: ', inci        
    return migranyes, incidents
    
def get_farmacs(in_crit, migranyes):
    far_migranya = {}
    preventius = {}
    tipus_farmac = {}
    sql = 'select id_cip_sec,pf_cod_atc from tractaments,nodrizas.dextraccio where pf_cod_atc in {0} and ppfmc_pmc_data_ini<=data_ext and ppfmc_data_fi>data_ext'.format(in_crit)
    for id, atc in getAll(sql, imp):
        if id in migranyes:
            far = farmac_dict[atc]['nom']
            prev = farmac_dict[atc]['grup']
            tipusF = farmac_dict[atc]['grup2']
            far_migranya[id, far] = True
            preventius[id, prev] = True
            tipus_farmac[id, tipusF] = True
    recompte = Counter()
    recomptep = Counter()
    recomptef = Counter()
    for (id, far), rec in far_migranya.items():
        recompte[far] += 1
    for far, recomptes in recompte.items():
        print far, recomptes
    for (id, prev), rec in preventius.items():
        recomptep[prev] += 1
    for prev, recomptesp in recomptep.items():
        print prev, recomptesp
    for (id, tipusF), rec in tipus_farmac.items():
        recomptef[tipusF] += 1
    for tipusF, recomptesf in recomptef.items():
        print tipusF, recomptesf

def get_proves(incidents, in_crit):
    proves_incid = {}
    sql ='select id_cip_sec, inf_codi_prova from nodrizas.nod_proves, nodrizas.dextraccio \
            where oc_data between date_add(data_ext, interval -1 year) and data_ext and inf_codi_prova in {}'.format(in_crit)
    for id, prv in getAll(sql, imp):
        if id in incidents:
            tipus = proves_dict[prv]['grup']
            proves_incid[id, tipus] = True
    recompte = Counter()
    for (id, tipus), rec in proves_incid.items():
        recompte[tipus] += 1
    for tipus, recomptes in recompte.items():
        print tipus, recomptes
            
printTime('inici')
in_crit_farmac, in_crit_prova = get_crits()
centres = get_centres()
poblacio, pobs = get_poblacio(centres)
do_it, incidents = get_migranya(poblacio, pobs)
do_it_far = get_farmacs(in_crit_farmac, do_it)
do_it_proves = get_proves(incidents, in_crit_prova)
printTime('fi')

