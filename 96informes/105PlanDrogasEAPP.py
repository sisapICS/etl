# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter


imp = 'import_jail'
nod = 'nodrizas'

toxics = [
        ('Heroina', ['F11.0', 'F11.1', 'F11.2', 'F11.3']),
        ('Cocaina', ['F14.0', 'F14.1', 'F14.2', 'F14.3']),
        ('Cannabis', ['F12.0', 'F12.1', 'F12.2', 'F12.3']),
        ('Anfetamines', ['F15.0', 'F15.1', 'F15.2', 'F15.3']),
        ('Altres', ['F16.0', 'F16.1', 'F16.2', 'F16.3','F18.0', 'F18.1', 'F18.2', 'F18.3','F19.0', 'F19.1', 'F19.2', 'F19.3'])
        ]

psalut = '(101, 667, 639, 550, 12, 682)'
farmacs = '(556, 814)'

casosp = {}
sql = "select id_cip_sec, ps from eqa_problemes where id_cip_sec <0 and ps in {}".format(psalut)
for id, ps in getAll(sql, nod):
    casosp[(id, ps)] = True

casosi = {}
sql = "select id_cip_sec, ps from eqa_problemes_incid, nodrizas.dextraccio where id_cip_sec <0 and ps in {} and (dde between date_add(date_add(data_ext,interval - 12 month),interval + 1 day) and data_ext)".format(psalut)
for id, ps in getAll(sql, nod):
    casosi[(id, ps)] = True
    
presc = {}
sql = "select id_cip_sec, farmac from eqa_tractaments where id_cip_sec<0 and farmac in {}".format(farmacs)
for id, farmac in getAll(sql, nod):
    presc[(id, farmac)] = True

#metadona quan no es te a nodrizas
sql = "select id_cip_sec,pf_cod_atc from tractaments,nodrizas.dextraccio where pf_cod_atc in ('N02AC52','N07BC02') and ppfmc_pmc_data_ini<=data_ext and ppfmc_data_fi>data_ext"
for id, atc in getAll(sql, imp):
    presc[(id, 814)] = True



problemes_toxics = {}

for toxic, codis in toxics:
    t_cod = tuple(codis)
    sql = "select id_cip_sec\
        from problemes, nodrizas.dextraccio \
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa is null or pr_data_baixa > data_ext) and (pr_dba is null or pr_dba > data_ext) and pr_cod_ps in {}". format(t_cod)
    for id, in getAll(sql, imp):
        problemes_toxics[(id, toxic)] = True

drogues = {}
sql = "select id_cip_sec, vu_val, vu_dat_act, year(vu_dat_act) from variables where vu_cod_vs='YC0117'"
for id, valor, data, Yvar in getAll(sql, imp):
    if (id) in drogues:
        data2 = drogues[(id)]['data']
        if data2 < data:
            drogues[(id)]['data'] = data
            drogues[(id)]['valor'] = valor
    else:
        drogues[(id)] = {'data': data, 'valor': valor}
   
upload = []   
sql = 'select id_cip_sec, up, sexe, edat from assignada_tot_with_jail where id_cip_sec <0'
for id, up, sexe, edat in getAll(sql, nod):
    consumDrogues, consumVP = 0, 0
    if id in drogues:
        consum = drogues[id]['valor']
        if consum in (1, 5):
            consumDrogues = 1
        if consum == 1:
            consumVP = 1
    heroina, cocaina, cannabis, anfetes, altresd = 0, 0, 0, 0,0 
    if (id, 'Heroina') in problemes_toxics:
        heroina = 1
    if (id, 'Cocaina') in problemes_toxics:
        cocaina = 1
    if (id, 'Cannabis') in problemes_toxics:
        cannabis = 1
    if (id, 'Anfetamines') in problemes_toxics:
        anfetes = 1
    if (id, 'Altres') in problemes_toxics:
        altresd = 1
    hiv, sida, tbc, vhc = 0, 0, 0, 0
    if (id, 101) in casosp:
        hiv = 1
    if (id, 667) in casosp:
        sida = 1
    if (id, 639) in casosp:
        tbc = 1
    if (id, 550) in casosp:
        vhc = 1
    if (id, 12) in casosp:
        vhc = 1
    noussida = 0
    if (id, 667) in casosi:
        noussida = 1
    if (id, 682) in casosi:
        noussida = 1
    arv, metadona = 0, 0
    if (id, 556) in presc:
        arv = 1
    if (id, 814) in presc:
        metadona = 1
    upload.append([id, up, sexe, edat, consumDrogues,consumVP,heroina,cocaina,cannabis,anfetes,altresd,hiv,sida,tbc,vhc, noussida, arv, metadona])
    
db = 'altres'
table = 'drogues_eapp'
createTable(table, '(id_cip_sec int, up varchar(5), sexe varchar(1), edat int, consumdrogues int, consumvp int, heroina int, cocaina int, cannabis int, anfetes int, altresd int, hiv int, sida int, tbc int, vhc int, noussida int, arv int, metadona int)', db, rm=True)
listToTable(upload, table, db)

indicadors = {
                'PLANDRO001': {'desc': 'Consum drogues en homes', 'tip' : 'perc', 'den': "sexe='H'", 'num': "consumdrogues = 1"},
                'PLANDRO002': {'desc': 'Consum drogues en dones', 'tip' : 'perc', 'den': "sexe='D'", 'num': "consumdrogues = 1"},
                'PLANDRO003': {'desc': 'Consum drogues', 'tip' : 'perc', 'den': "sexe in ('H', 'D')", 'num': "consumdrogues = 1"},
                'PLANDRO004': {'desc': 'Mitjana edat consumidors homes','tip' : 'mitjana edat', 'den': "sexe = 'H' and consumdrogues = 1"},
                'PLANDRO005': {'desc': 'Mitjana edat consumidors dones','tip' : 'mitjana edat', 'den': "sexe = 'D' and consumdrogues = 1"},
                'PLANDRO006': {'desc': 'Mitjana edat consumidors','tip' : 'mitjana edat', 'den': "consumdrogues = 1"},
                'PLANDRO007': {'desc': 'Usuaris drogues VP en homes', 'tip' : 'perc', 'den': "sexe='H'", 'num': "consumvp = 1"},
                'PLANDRO008': {'desc': 'Usuaris drogues VP  en dones', 'tip' : 'perc', 'den': "sexe='D'", 'num': "consumvp = 1"},
                'PLANDRO009': {'desc': 'Usuaris drogues VP', 'tip' : 'perc', 'den': "sexe in ('H', 'D')", 'num': "consumvp = 1"},
                'PLANDRO010': {'desc': 'Mitjana edat Usuaris drogues VP homes','tip' : 'mitjana edat', 'den': "sexe = 'H' and consumvp = 1"},
                'PLANDRO011': {'desc': 'Mitjana edat Usuaris drogues VP dones','tip' : 'mitjana edat', 'den': "sexe = 'D' and consumvp = 1"},
                'PLANDRO012': {'desc': 'Mitjana edat Usuaris drogues VP','tip' : 'mitjana edat', 'den': "consumvp = 1"},
                'PLANDRO013': {'desc': 'VIH+ en usuaris drogues VP homes', 'tip' : 'perc',  'den': "sexe='H'", 'num': "consumvp = 1 and hiv = 1"},
                'PLANDRO014': {'desc': 'VIH+ en usuaris drogues VP dones', 'tip' : 'perc',  'den': "sexe='D'", 'num': "consumvp = 1 and hiv = 1"},
                'PLANDRO015': {'desc': 'VIH+ en usuaris drogues VP ', 'tip' : 'perc',  'den': "sexe in ('H', 'D')", 'num': "consumvp = 1 and hiv = 1"},
                'PLANDRO016': {'desc': 'Nous casos sida en usuaris drogues VP','tip' : 'n', 'den': "consumvp = 1 and noussida=1"},
                'PLANDRO017': {'desc': 'Nous casos sida en no usuaris drogues VP','tip' : 'n', 'den': "consumvp = 0 and noussida=1"},
                'PLANDRO018': {'desc': 'Nous casos sida','tip' : 'n', 'den': "noussida=1"},
                'PLANDRO019': {'desc': 'Prevalenša VIH en usuaris drogues VP', 'tip' : 'perc', 'den': "consumvp = 1", 'num': "hiv = 1"},
                'PLANDRO020': {'desc': 'Prevalenša VIH en no usuaris drogues VP', 'tip' : 'perc', 'den': "consumvp = 0", 'num': "hiv = 1"},
                'PLANDRO021': {'desc': 'Prevalenša VIH', 'tip' : 'perc', 'den': "consumvp in (1, 0)", 'num': "hiv = 1"},
                'PLANDRO022': {'desc': 'Prevalenša TBC en usuaris drogues VP', 'tip' : 'perc', 'den': "consumvp = 1", 'num': "tbc = 1"},
                'PLANDRO023': {'desc': 'Prevalenša TBC en no usuaris drogues VP', 'tip' : 'perc', 'den': "consumvp = 0", 'num': "tbc = 1"},
                'PLANDRO024': {'desc': 'Prevalenša TBC', 'tip' : 'perc', 'den': "consumvp in (1, 0)", 'num': "tbc = 1"},
                'PLANDRO025': {'desc': 'Prevalenša VHC en usuaris drogues VP', 'tip' : 'perc', 'den': "consumvp = 1", 'num': "vhc = 1"},
                'PLANDRO026': {'desc': 'Prevalenša VHC en no usuaris drogues VP', 'tip' : 'perc', 'den': "consumvp = 0", 'num': "vhc = 1"},
                'PLANDRO027': {'desc': 'Prevalenša VHC', 'tip' : 'perc', 'den': "consumvp in (1, 0)", 'num': "vhc = 1"},
                'PLANDRO028': {'desc': 'Prevalenša tractament antriretroviral en usuaris drogues VP', 'tip' : 'perc', 'den': "consumvp = 1", 'num': "arv = 1"},
                'PLANDRO029': {'desc': 'Prevalenša tractament antriretroviral en no usuaris drogues VP', 'tip' : 'perc', 'den': "consumvp = 0", 'num': "arv = 1"},
                'PLANDRO030': {'desc': 'Prevalenša tractament antriretroviral', 'tip' : 'perc', 'den': "consumvp in (1, 0)", 'num': "arv = 1"},
                'PLANDRO031': {'desc': 'Consum de drogues en presˇ (heroina)', 'tip' : 'perc', 'den':  "sexe in ('H', 'D')", 'num': "heroina = 1"},
                'PLANDRO032': {'desc': 'Consum de drogues en presˇ (cocaina)', 'tip' : 'perc', 'den':  "sexe in ('H', 'D')", 'num': "cocaina = 1"},
                'PLANDRO033': {'desc': 'Consum de drogues en presˇ (cannabis)', 'tip' : 'perc', 'den':  "sexe in ('H', 'D')", 'num': "cannabis = 1"},
                'PLANDRO034': {'desc': 'Consum de drogues en presˇ (amfetamines)', 'tip' : 'perc', 'den':  "sexe in ('H', 'D')", 'num': "anfetes = 1"},
                'PLANDRO035': {'desc': 'Consum de drogues en presˇ (altres)', 'tip' : 'perc', 'den':  "sexe in ('H', 'D')", 'num': "altresd = 1"},
                'PLANDRO036': {'desc': 'Via principal injectada (heroina)', 'tip' : 'perc', 'den':  "heroina=1", 'num': "consumvp = 1"},
                'PLANDRO037': {'desc': 'Via principal injectada(cocaina)', 'tip' : 'perc', 'den':  "cocaina=1", 'num': "consumvp = 1"},
                'PLANDRO038': {'desc': 'Via principal injectada (cannabis)', 'tip' : 'perc', 'den':  "cannabis=1", 'num': "consumvp = 1"},
                'PLANDRO039': {'desc': 'Via principal injectada (amfetamines)', 'tip' : 'perc', 'den':  "anfetes=1", 'num': "consumvp = 1"},
                'PLANDRO040': {'desc': 'Via principal injectada (altres)', 'tip' : 'perc', 'den':  "altresd=1", 'num': "consumvp = 1"},
                'PLANDRO041': {'desc': 'Via principal altres (heroina)', 'tip' : 'perc', 'den':  "heroina=1", 'num': "consumvp = 0 and consumdrogues = 1"},
                'PLANDRO042': {'desc': 'Via principal altres(cocaina)', 'tip' : 'perc', 'den':  "cocaina=1", 'num': "consumvp = 0 and consumdrogues = 1"},
                'PLANDRO043': {'desc': 'Via principal altres (cannabis)', 'tip' : 'perc', 'den':  "cannabis=1", 'num': "consumvp = 0 and consumdrogues = 1"},
                'PLANDRO044': {'desc': 'Via principal altres (amfetamines)', 'tip' : 'perc', 'den':  "anfetes=1", 'num': "consumvp = 0 and consumdrogues = 1"},
                'PLANDRO045': {'desc': 'Via principal altres (altres)', 'tip' : 'perc', 'den':  "altresd=1", 'num': "consumvp = 0 and consumdrogues = 1"},
                'PLANDRO046': {'desc': 'PPM homes', 'tip' : 'n', 'den':  " sexe = 'H' and metadona =1"},
                'PLANDRO047': {'desc': 'PPM dones', 'tip' : 'n', 'den':  " sexe = 'D' and metadona =1"}
                }
  
resultats_indicadors = Counter()  
for indicador in indicadors:
    descripcio = indicadors[indicador]['desc']
    tipus = indicadors[indicador]['tip']
    den = indicadors[indicador]['den']
    if tipus == 'perc' or tipus == 'n':
        sql_d= 'select id_cip_sec, up from {0} where {1}'.format(table, den)
        if tipus == 'perc':
            num = indicadors[indicador]['num']
            sql_n = 'select id_cip_sec, up from {0} where {1} and {2}'.format(table, den, num)
        else:
            sql_n = False
        for id, up in getAll(sql_d, db):
            resultats_indicadors[(indicador, descripcio, up, 'DEN')] += 1
        if sql_n:
            for id, up in getAll(sql_n, db):
                resultats_indicadors[(indicador, descripcio, up, 'NUM')] += 1
    elif tipus == 'mitjana edat':
        sql = 'select id_cip_sec, up, edat from {0} where {1}'.format(table, den)
        for id, up, edat in getAll(sql, db):
             resultats_indicadors[(indicador, descripcio, up, 'DEN')] += 1
             resultats_indicadors[(indicador, descripcio, up, 'NUM')] += edat
        
upload1 = {}
for (indicador, descripcio, up, parametre), d in resultats_indicadors.items():
   upload1[(indicador, descripcio, up)] = {'den': None, 'num': None}
   
for (indicador, descripcio, up, parametre), d in resultats_indicadors.items():
    if parametre == 'DEN':
        upload1[(indicador, descripcio, up)]['den'] = d
    if parametre == 'NUM':
        upload1[(indicador, descripcio, up)]['num'] = d        

upload = []
for (indicador, descripcio, up), valor in upload1.items():
    den = valor['den']
    num = valor['num']
    upload.append([indicador, descripcio, up, num, den])
    
file = tempFolder + 'PlanNacionalDrogas.txt'
writeCSV(file, upload, sep=';')

                
                