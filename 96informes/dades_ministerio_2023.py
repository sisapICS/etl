# -*- coding: utf-8 -*-

"""
---
"""

from sisapUtils import *
import multiprocessing as m
import sisaptools as u
import pandas as pd
import collections as c



### PACIENTS ###
def pacients():
    # obtenir defuncions
    print('poblacio')
    sql = """SELECT
                cip,
                DATA_NAIXEMENT,
                sexe,
                DATA_DEFUNCIO,
                NACIONALITAT,
                UP_RESIDENCIA
            FROM
                DWSISAP.RCA_CIP_NIA
            WHERE
                (DATA_DEFUNCIO IS NULL
                    OR
                        EXTRACT(YEAR FROM DATA_DEFUNCIO) = 2023)
                AND
                        EXTRACT(YEAR FROM DATA_naixement) <= 2023
                AND situacio != 'T'
                AND cip NOT IN (
                SELECT
                    cip
                FROM
                    DWSISAP.RCA_CIP_NIA
                WHERE
                    DATA_DEFUNCIO IS NULL
                    AND situacio = 'D')"""

    persones = []
    with u.Database("exadata", "data") as exadata:
        for cip, naix, sexe, defuncio, nacionalitat, up in exadata.get_all(sql):  # noqa
            if sexe == '0':
                sex = 1
            elif sexe == '1':
                sex = 2
            else:
                sex = 3
            persones.append((str(cip),
                             naix,
                             sex,
                             defuncio,
                             nacionalitat,
                             str(up)))
    print(len(persones))
    print('creating database')
    table_name = 'bdcap_pacientes_2023'
    with u.Database("exadata", "data") as exadata:
        # exadata.execute("drop table {}".format(table_name))
        exadata.execute("""create table {} as
                           select * from bdcap_pacientes_2023
                           WHERE 1 = 0
                           """.format(table_name))
        exadata.set_grants("select",
                           table_name,
                           "DWSISAP_ROL",
                           inheritance=False)
        exadata.list_to_table(persones, table_name, chunk=10 ** 5)



### HASH A CIP ###
def get_cips():
    print('cips')
    cips = dict()
    sql = """SELECT
                usua_cip,
                usua_cip_cod
            FROM
                pdptb101"""
    with u.Database("redics", "pdp") as pdp:
        for cip, hash in pdp.get_all(sql):
            cips[hash] = cip
    return cips


### CREAR CATÀLEG ###
def definicions(x, proc_dic, param_dic):
    p = proc_dic.keys()
    p = [str(e)for e in p]
    v = param_dic.keys()
    v = [str(e) for e in v]
    if str(x) in p:
        return proc_dic[x] 
    elif str(x) in v:
        return param_dic[x]
    else: 
        return ''

def cataleg():
    equiv = pd.read_excel(open('Novetats_2022.xlsx', 'rb'),
                          sheet_name='Parámetros amb camps ECAP')
    equiv = equiv[['TIPUS', 'CODI_ECAP', 'CONCEPTID',
                   'ORIGEN_DADES', 'DES_MINIS', 'PROCEDIMIENTO',
                   'VARIABLES', 'Unidades']]
    upload = []
    for i in range(len(equiv)):
        if str(equiv['Unidades'][i]) not in ('0/1', '1 a 14', 'segundos'):
            unitats = str(equiv['Unidades'][i])
        else:
            unitats = None
        # ATENCIÓ, ORDRE CANVIAT PER ERROR A LES DADES!!!
        v = [str(equiv['TIPUS'][i]),
             str(equiv['CODI_ECAP'][i]),
             str(equiv['CONCEPTID'][i]),
             str(equiv['ORIGEN_DADES'][i]),
             str(equiv['DES_MINIS'][i]),
             int(equiv['VARIABLES'][i]),
             int(equiv['PROCEDIMIENTO'][i]),
             unitats]
        # if v[5] == 1 and v[0] not in ('PRO', 'VARIPRO'):
        #     v[5] = 0
        # if v[6] == 1 and v[0] not in ('VAR', 'VARIPRO'):
        #     v[6] = 0
        v = list(v)
        upload.append(v)
    upload.append(('PRO', 'OBT', '20181000122106', 'PRSTB508', 'obturación dental (procedimiento)', 1, 0, None))  # noqa
    upload.append(('PRO', 'TT', '234696006', 'PRSTB508', 'remoción de sarro y pulido dental (procedimiento)', 1, 0, None))  # noqa
    upload.append(('PRO', 'CON', '16191005', 'PRSTB508', 'reparación de una corona dental (procedimiento)', 1, 0, None))  # noqa
    upload.append(('PRO', 'FC', '234723000', 'PRSTB511', 'Aplicación tópica de flúor dental', 1, 0, None))  # noqa
    upload.append(('PRO', 'FSC', '234723000', 'PRSTB511', 'Aplicación tópica de flúor dental', 1, 0, None))  # noqa
    upload.append(('PRO', 'EX', '55162003', 'PRSTB508', 'Extracción de diente', 1, 0, None))  # noqa
    upload.append(('PRO', 'SEG', '234713009', 'PRSTB508', 'Sellado de fisura dental', 1, 0, None))  # noqa
    upload.append(('PRO', 'END', '265328005', 'PRSTB508', 'Cirugía dental periapical', 1, 0, None))  # noqa
    upload.append(('PRO', '3230', '34431008', 'PRSTB218', 'Fisioterapia respiratoria', 1, 0, None))  # noqa
    table_name = 'BDCAP_CATALEG_2022'
    with u.Database("exadata", "data") as exadata:
        exadata.execute("drop table {}".format(table_name))
        exadata.execute("""create table {} as
                           select * from BDCAP_CATALEG_2021
                           """.format(table_name))
        exadata.set_grants("select",
                           table_name,
                           "DWSISAP_ROL",
                           inheritance=False)
        exadata.list_to_table(upload, table_name, chunk=10 ** 5)



def get_PRSTB218(inf):
    sector, origen_codi = inf
    print('get_PRSTB218', sector)
    procediments = set()
    coses = set()
    codis = list(origen_codi.keys())
    sql = """SELECT
                mi_cip,
                mi_cod_var,
                mi_data_reg
            FROM
                PRSTB218
            WHERE
                mi_cod_var in ('3230','11014','11025')
                AND EXTRACT(YEAR FROM mi_data_reg) = 2023"""
    with u.Database(sector, "data") as thewhole:
        for id, prova, data in thewhole.get_all(sql):
            coses.add(prova)
            if prova in codis:
                procediments.add((id,
                                  origen_codi[prova],
                                  data,
                                  'FICTICIO',
                                  'PRSTB017'))
    print(len(procediments))
    if len(procediments) > 0:
        with u.Database("exadata", "data") as exadata:
            exadata.list_to_table(list(procediments),
                                  'bdcap_procedimientos_2023',
                                  chunk=10 ** 5)
    print('escrit i acabat', sector)


def get_PRSTB508(inf):
    sector, origen_codi = inf
    print('get_PRSTB508', sector)
    procediments = set()
    codis = tuple(origen_codi.keys())
    sql = """SELECT
                cfd_cip,
                dtd_tra,
                dtd_data
            FROM
                PRSTB508 a,
                prstb505 b
            WHERE
                a.dtd_cod = b.cfd_cod
                AND dtd_et = 'E'
                AND EXTRACT(YEAR FROM dtd_data) = 2023
                AND dtd_tra IN {}""".format(codis)
    print(sql)
    with u.Database(sector, "data") as thewhole:
        for id, prova, data in thewhole.get_all(sql):
            procediments.add((id,
                              origen_codi[prova],
                              data,
                              'FICTICIO',
                              'PRSTB017'))
    print(len(procediments))
    if len(procediments) > 0:
        with u.Database("exadata", "data") as exadata:
            exadata.list_to_table(list(procediments),
                                  'bdcap_procedimientos_2023',
                                  chunk=10 ** 5)
    print('escrit i acabat', sector)


def get_PRSTB511(inf):
    sector, origen_codi = inf
    print('get_PRSTB511', sector)
    procediments = set()
    codis = tuple(origen_codi.keys())
    sql = """SELECT
                cfd_cip,
                tb_trac,
                tb_data
            FROM
                PRSTB511 a,
                prstb505 b
            WHERE
                a.tb_cod = b.cfd_cod
                AND EXTRACT(YEAR FROM tb_data) = 2023
                AND tb_trac IN {}""".format(codis)
    print(sql)
    with u.Database(sector, "data") as thewhole:
        for id, prova, data in thewhole.get_all(sql):
            procediments.add((id,
                              origen_codi[prova],
                              data,
                              'FICTICIO',
                              'PRSTB017'))
    print(len(procediments))
    if len(procediments) > 0:
        with u.Database("exadata", "data") as exadata:
            exadata.list_to_table(list(procediments),
                                  'bdcap_procedimientos_2023',
                                  chunk=10 ** 5)
    print('escrit i acabat', sector)




### CREAR PROCEDIMIENTO ###
def procediment(cips):
    print('procediments')
    cols = "(id varchar2(14), prova varchar2(200), data date, \
            episodio varchar2(100), taula varchar2(15))"
    createTable('bdcap_procedimientos_2023', cols, 'exadata', rm=True)
    grantSelect('bdcap_procedimientos_2023','DWSISAP_ROL','exadata')

    origen_codi = c.defaultdict(dict)
    sql = """SELECT
                codi_ecap,
                CONCEPTID,
                origen_dades
            FROM
                dwsisap.BDCAP_CATALEG_2022
            WHERE
                procedimiento = 1
                OR TIPUS = 'PROVAR'"""
    with u.Database("exadata", "data") as exadata:
        for ecap, ministerio, origen in exadata.get_all(sql):
            origen_codi[origen][ecap] = ministerio
    

    execute("""
            INSERT INTO dwsisap.bdcap_procedimientos_2023(                
            SELECT
                ID, CONCEPTID, DATA, 'FICTICIO', 'LABTB101'
            FROM
                dwtw.LABORATORI_DETALL ld INNER JOIN dwsisap.BDCAP_CATALEG_2022 ON PROVA = CODI_eCAP
            WHERE
                DATA_MODI >= DATE '2023-01-01'
                AND DATA >= DATE '2023-01-01'
                AND DATA <= DATE '2023-12-31'
                AND (procedimiento = 1 OR prova = 'PROVAR'))
        """, 'exadata') 
        
    execute("""insert into dwsisap.bdcap_procedimientos_2023 (SELECT
                    ID,
                    CONCEPTID,
                    DATA_VACUNA,
                    'FICTICIO',
                    'PRSTB051'
                FROM
                    DWTW.VACUNES
                INNER JOIN dwsisap.BDCAP_CATALEG_2022
                ON
                    CODI = CODI_ECAP
                WHERE
                    procedimiento = 1
                    AND origen_dades = 'PRSTB051'
                    AND DATA >= DATE '2023-01-01'
                    and DATA_VACUNA BETWEEN DATE '2023-01-01' AND DATE '2023-12-31')""", 'exadata')
    
    # vacunes de antigens mirar les q falten
    execute("""insert into dwsisap.bdcap_procedimientos_2023 (SELECT
                    ID,
                    CONCEPTID,
                    DATA_VACUNA,
                    'FICTICIO',
                    'PRSTB051'
                FROM
                    DWTW.VACUNES
                INNER JOIN dwsisap.BDCAP_CATALEG_2022
                ON
                    ANTIGEN = CODI_ECAP
                WHERE
                    procedimiento = 1
                    AND origen_dades = 'PRSTB051'
                    AND DATA >= date '2023-01-01' and data_vacuna BETWEEN DATE '2023-01-01' AND DATE '2023-12-31')""", 'exadata')
    
    execute("""insert into dwsisap.bdcap_procedimientos_2023 (
        SELECT
            ID,
            CONCEPTID,
            DATA,
            'FICTICIO',
            'PRSTB016'
        FROM
            dwtw.ACTIVITATS a 
        INNER JOIN dwsisap.BDCAP_CATALEG_2022
        ON
            CODI_ECAP = COD_AC
        WHERE
            DATA BETWEEN DATE '2023-01-01' AND DATE '2023-12-31'
            AND (procedimiento = 1
                OR TIPUS = 'PROVAR')
            AND ORIGEN_DADES = 'PRSTB016')""", 'exadata')

    execute("""insert into dwsisap.bdcap_procedimientos_2023 (
        SELECT
            ID,
            CONCEPTID,
            DATA,
            'FICTICIO',
            'PRSTB017'
        FROM
            dwtw.VARIABLES
        INNER JOIN dwsisap.BDCAP_CATALEG_2022
        ON
            CODI_ECAP = CODI
        WHERE
            DATA BETWEEN DATE '2023-01-01' AND DATE '2023-12-31'
            AND (procedimiento = 1
                OR TIPUS = 'PROVAR')
            AND ORIGEN_DADES = 'PRSTB017')""", 'exadata')

    for taula, dict_codis in origen_codi.items():
        codis = list(dict_codis.keys())
        if taula == 'GPITB004':
            print('---------------comencem amb la taula: ', taula, '----------------------')  # noqa
            convert = dict()
            procediments = []
            coses = set()
            with u.Database("redics", "data") as redics:
                sql = """SELECT
                            pro_codi_prova,
                            PRO_CODI_REALITZABLE
                        FROM
                            gcctb003
                        WHERE
                            PRO_CODI_REALITZABLE IS NOT NULL"""
                for vell, nou in redics.get_all(sql):
                    convert[vell] = nou
                codis1 = set([convert[cv] if cv in convert.keys() else 0 for cv in codis])  # noqa

                sql = """SELECT
                            a.oc_data,
                            a.OC_USUA_CIP,
                            b.INF_CODI_PROVA,
                            b.inf_cod_ps
                        FROM
                            gpitb104 a,
                            gpitb004 b
                        WHERE
                            b.INF_NUMID = a.OC_NUMID
                            AND EXTRACT(YEAR FROM a.oc_data) = 2023"""
                print(sql)
                print(codis1)
                # codis vells i nous van al revés!!!!
                for data, id, prova, dx in redics.get_all(sql):
                    coses.add(prova)
                    if prova in codis1 and prova in dict_codis:
                        procediments.append((cips[id],
                                             dict_codis[prova],
                                             data,
                                             dx,
                                             'gpitb004'))
            with u.Database("exadata", "data") as exadata:
                exadata.list_to_table(list(procediments),
                                      'bdcap_procedimientos_2023',
                                      chunk=10 ** 5)
        if taula == 'GPITB003':
            print('---------------comencem amb la taula: ', taula, '----------------------')  # noqa
            convert = dict()
            procediments = []
            coses = set()
            with u.Database("redics", "data") as redics:
                sql = """SELECT
                            pro_codi_prova,
                            PRO_CODI_REALITZABLE
                        FROM
                            gcctb003
                        WHERE
                            PRO_CODI_REALITZABLE IS NOT NULL"""
                for vell, nou in redics.get_all(sql):
                    convert[vell] = nou
                codis1 = set([convert[cv] if cv in convert.keys() else 0 for cv in codis])  # noqa

                sql = """SELECT
                            DER_USUA_CIP,
                            DER_PROVA,
                            DER_DATA_DER
                        FROM
                            gpitb003
                        WHERE
                            EXTRACT(YEAR FROM DER_DATA_DER) = 2023"""
                print(sql)
                print(codis1)
                # codis vells i nous van al revés!!!!
                for id, prova, data in redics.get_all(sql):
                    coses.add(prova)
                    if prova in codis1 and prova in dict_codis:
                        procediments.append((cips[id],
                                             dict_codis[prova],
                                             data,
                                             'FICTICIO',
                                             'gpitb003'))
            with u.Database("exadata", "data") as exadata:
                if len(list(procediments)) > 0:
                    exadata.list_to_table(list(procediments),
                                          'bdcap_procedimientos_2023',
                                          chunk=10 ** 5)

        if taula == 'PRSTB218':
            print('---------------comencem amb la taula: ', taula, '----------------------')  # noqa
            pool = m.Pool(4)
            pool.map(get_PRSTB218,
                     [(sector, dict_codis)
                      for sector in u.constants.SECTORS_ECAP],
                     chunksize=1)
            pool.close()

        if taula == 'PRSTB508':
            print('---------------comencem amb la taula: ', taula, '----------------------')  # noqa
            pool = m.Pool(4)
            pool.map(get_PRSTB508,
                     [(sector, dict_codis)
                      for sector in u.constants.SECTORS_ECAP],
                     chunksize=1)
            pool.close()

        if taula == 'PRSTB511':
            print('---------------comencem amb la taula: ', taula, '----------------------')  # noqa
            pool = m.Pool(4)
            pool.map(get_PRSTB511,
                     [(sector, dict_codis)
                      for sector in u.constants.SECTORS_ECAP],
                     chunksize=1)
            pool.close()



### CREAR VARIABLES ###
def variables():
    print('variables')
    cols = "(id varchar2(40), prova varchar2(200), valor varchar2(2000), data date, \
            episodio varchar2(100), taula varchar2(15))"
    createTable('bdcap_variables_2023', cols, 'exadata', rm=True)
    grantSelect('bdcap_variables_2023','DWSISAP_ROL','exadata')

    # laboratori
    execute("""
            INSERT INTO bdcap_variables_2023(                
            SELECT
                ID,
                CONCEPTID,
                RESULTAT,
                DATA,
                'FICTICIO',
                'LABTB101'
            FROM
                dwtw.LABORATORI_DETALL ld
            INNER JOIN dwsisap.BDCAP_CATALEG_2022 
                            ON
                PROVA = CODI_eCAP
            WHERE
                origen_dades = 'LABTB101'
                AND unitats_bdcap IS NULL
                AND DATA_MODI >= DATE '2023-01-01'
                AND DATA >= DATE '2023-01-01'
                AND DATA <= DATE '2023-12-31'
                AND variables = 1)
        """, 'exadata') 
    
    execute("""
            INSERT INTO bdcap_variables_2023(                
            SELECT
            ID,
            CONCEPTID,
            RESULTAT,
            DATA,
            'FICTICIO',
            'LABTB101'
            FROM
                dwtw.LABORATORI_DETALL ld
            INNER JOIN dwsisap.BDCAP_CATALEG_2022 
                            ON
                PROVA = CODI_eCAP
                AND UPPER(UNITATS_BDCAP) = UPPER(UNITATS)
            WHERE
                origen_dades = 'LABTB101'
                AND unitats_bdcap IS NOT NULL
                AND DATA_MODI >= DATE '2023-01-01'
                AND DATA >= DATE '2023-01-01'
                AND DATA <= DATE '2023-12-31'
                AND variables = 1)
        """, 'exadata') 

    # prova especial que va amb dos codis
    execute("""
            INSERT INTO bdcap_variables_2023(                
            SELECT
                ID,
                CONCEPTID,
                RESULTAT,
                DATA,
                'FICTICIO',
                'LABTB101'
            FROM
                dwtw.LABORATORI_DETALL ld
            INNER JOIN dwsisap.BDCAP_CATALEG_2022 
                            ON
                PROVA = CODI_eCAP
            WHERE
                origen_dades = 'LABTB101'
                AND DATA_MODI >= DATE '2023-01-01'
                AND DATA >= DATE '2023-01-01'
                AND DATA <= DATE '2023-12-31'
                AND variables = 1  
                AND PROVA = 'Q48372'
                AND UPPER(UNITATS) IN ('PG/ML', 'NG/ML'))
        """, 'exadata') 

    execute("""insert into bdcap_variables_2023 (
        SELECT
            ID,
            CONCEPTID,
            VAL,
            DATA,
            'FICTICIO',
            'PRSTB016'
        FROM
            dwtw.ACTIVITATS a 
        INNER JOIN dwsisap.BDCAP_CATALEG_2022
        ON
            CODI_ECAP = COD_AC
        WHERE
            DATA BETWEEN DATE '2023-01-01' AND DATE '2023-12-31'
            AND variables = 1
            AND ORIGEN_DADES = 'PRSTB016')""", 'exadata')

    execute("""insert into bdcap_variables_2023 (
        SELECT
            ID,
            CONCEPTID,
            VALOR,
            DATA,
            'FICTICIO',
            'PRSTB017'
        FROM
            dwtw.VARIABLES
        INNER JOIN dwsisap.BDCAP_CATALEG_2022
        ON
            CODI_ECAP = CODI
        WHERE
            DATA BETWEEN DATE '2023-01-01' AND DATE '2023-12-31'
            AND variables = 1
            AND ORIGEN_DADES = 'PRSTB017')""", 'exadata')
    

#### CREAR EPISODIO ####
def episodio():
    print('episodio')
    cols = """(id varchar2(40), problema_salud varchar2(15),
            clasificacion int, fecha_apertura date,
            fecha_cierre date)"""
    createTable('bdcap_episodio_2023', cols, 'exadata', rm=True)
    grantSelect('bdcap_episodio_2023','DWSISAP_ROL','exadata')
    execute("""INSERT INTO bdcap_episodio_2023 (SELECT   id,
                CASE
                    WHEN substr(codi, 1, 4) = 'C01-' THEN substr(codi, 5)
                    ELSE codi
                END prob,
                4,
                DATA,
                fi
            FROM
                dwtw.problemes
            WHERE
                (DATA BETWEEN DATE '2023-01-01' AND DATE '2023-12-31')
                OR (FI IS NULL OR FI >= DATE '2023-01-01'))""", "exadata")


def get_visites(sector):
    print('vamos a workear')
    transform_visit = {
        '9C': 1,
        '9D': 2,
        '9T': 3,
        '9R': 1,
        '9E': 4
    }
    categoria_prof = {}
    sql = """SELECT b.IDE_DNI,
            CASE WHEN CATPROF_MAP IN ('30084', '30085') THEN 4
            WHEN CATPROF_MAP = '03005' THEN 6
            WHEN CATPROF_MAP = '10777' THEN 8
            WHEN catprof_class = 'MF' THEN 1
            WHEN catprof_class = 'PED' THEN 2
            WHEN catprof_class IN ('INF', 'EXTRA') THEN 3
            WHEN catprof_class = 'TS' THEN 5
            WHEN catprof_class = 'ODO' THEN 7
            ELSE 9 END CATEG
            FROM DWSISAP.SISAP_MAP_CATPROF a, DWSISAP.LOGINS_ECAP b
            WHERE b.ide_categ_prof_c = a.cod
            and b.IDE_DNI is not null"""
    for dni, categoria in u.Database("exadata", "data").get_all(sql):
        categoria_prof[dni[0:8]] = categoria
    print('tinc categories')
    id_visites = {}
    sql = """SELECT VISI_ID, visi_tipus_visita
            FROM vistb043 WHERE VISI_SITUACIO_VISITA = 'R'
            AND extract(YEAR FROM to_date(visi_data_visita, 'j')) =2023
            AND VISI_TIPUS_VISITA IN ('9C', '9E', '9T', '9R', '9D')"""
    for id, tipus_visita in u.Database(sector, "data").get_all(sql):
        id_visites[id] = transform_visit[tipus_visita]
    upload = []
    sql = """SELECT MC_CIP, MC_DATA, MC_MOTIU, MC_PORTA_ID, MC_DNI_PROF
            FROM vistb042
            WHERE extract(YEAR FROM mc_data) = 2023
            AND mc_porta = 'VISITA' AND MC_SUB_CAT = 'CIM10MC'
            and MC_DNI_PROF is not null"""
    for id, data, codi, visi, dni_prof in u.Database(sector, "data").get_all(sql):  # noqa
        if visi in id_visites:
            tipus = id_visites[visi]
            if codi[0:4] == 'C01-':
                codi = codi[4:]
            if dni_prof[0:8] in categoria_prof:
                professional = categoria_prof[dni_prof[0:8]]
                upload.append((id, visi, codi, 4, data, tipus, professional))
    u.Database("exadata", "data").list_to_table(upload,
                                                'bdcap_visitas_2023',
                                                chunk=10**5)
    cim10_mc = dict()
    sql = """SELECT ps_cod_o_ap, cim10mc FROM prstb001cmig"""
    for old, new in u.Database(sector, "data").get_all(sql):
        cim10_mc[old] = new
    upload = []
    sql = """SELECT MC_CIP, MC_DATA, MC_MOTIU, MC_PORTA_ID, MC_DNI_PROF
            FROM vistb042
            WHERE extract(YEAR FROM mc_data) = 2023
            AND mc_porta = 'VISITA' AND MC_SUB_CAT = 'CIM10'
            and MC_DNI_PROF is not null"""
    for id, data, codi, visi, dni_prof in u.Database(sector, "data").get_all(sql):  # noqa
        if codi in cim10_mc:
            codi = cim10_mc[codi]
        if visi in id_visites:
            tipus = id_visites[visi]
            if codi[0:4] == 'C01-':
                codi = codi[4:]
            if dni_prof[0:8] in categoria_prof:
                professional = categoria_prof[dni_prof[0:8]]
                upload.append((id, visi, codi, 4, data, tipus, professional))
    u.Database("exadata", "data").list_to_table(upload,
                                                'bdcap_visitas_2023',
                                                chunk=10**5)
    print('un menysss:))))')
    return 1


def visitas():
    """CREACIÓ TAULA VISITES"""
    cols = ("id varchar2(40)", "id_visita int", "problema_salud varchar2(40)",
            "clasificacion int", "fecha_visita date",
            "tipus_visita int", "profesional int")
    with u.Database("exadata", "data") as exadata:
        exadata.create_table('bdcap_visitas_2023', cols, remove=True)
        exadata.set_grants("select", "bdcap_visitas_2023", "DWSISAP_ROL",
                           inheritance=False)
    pool = m.Pool(2)
    pool.map(get_visites, sectors, chunksize=1)
    pool.close()


def interconsultes(cips):
    print('interconsultes')
    table_name = 'bdcap_interconsultas_2023'
    with u.Database("exadata", "data") as exadata:
        exadata.execute("drop table {}".format(table_name))
        exadata.execute("""create table {} as
                           select * from bdcap_interconsultas_2021
                           WHERE 1 = 0
                           """.format(table_name))
        exadata.set_grants("select",
                           table_name,
                           "DWSISAP_ROL",
                           inheritance=False)
        ecap_ministerio = dict()
        sql = """SELECT codi_ecap, conceptid
                FROM dwsisap.BDCAP_CATALEG_2022
                WHERE tipus = 'INT'"""
        for ecap, mini in exadata.get_all(sql):
            ecap_ministerio[ecap] = mini
    print('pritb023')
    sire = set()
    sql = """SELECT ESPE_CODI_SIRE FROM cat_pritb023"""
    with u.Database("p2262", "import") as import_:
        for codi, in import_.get_all(sql):
            sire.add(codi)
    print('info')
    upload = []
    sql = """SELECT a.oc_data, a.OC_USUA_CIP, b.inf_espec_sire
            FROM gpitb104 a, gpitb004 b
            WHERE b.INF_NUMID=a.OC_NUMID
            AND extract(YEAR FROM a.oc_data) = 2023
            AND inf_espec_sire IS NOT NULL"""
    with u.Database("redics", "data") as redics:
        for data, usuari, espe in redics.get_all(sql):
            if espe in sire and espe in ecap_ministerio.keys() and usuari in cips:  # noqa
                upload.append((cips[usuari], ecap_ministerio[espe], data))
    print('afegir dades 1')
    with u.Database("exadata", "data") as exadata:
        exadata.list_to_table(upload,
                              'bdcap_interconsultas_2023',
                              chunk=10 ** 5)
        print('ints')
        ecap_ministerio = dict()
        sql = """SELECT codi_ecap, conceptid
                FROM dwsisap.BDCAP_CATALEG_2022
                WHERE tipus = 'INTS'"""
        for ecap, mini in exadata.get_all(sql):
            ecap_ministerio[ecap] = mini
    print('pritb000')
    sire = set()
    sql = """SELECT rv_low_value
                FROM import.cat_pritb000
                WHERE rv_TABLE ='GPITB004'
                AND RV_COLUMN ='INF_SERVEI_D_CODI'"""
    with u.Database("p2262", "import") as import_:
        for codi, in import_.get_all(sql):
            sire.add(codi)

    print('info')
    upload = []
    sql = """SELECT a.oc_data, a.OC_USUA_CIP, b.inf_servei_d_codi
            FROM gpitb104 a, gpitb004 b
            WHERE b.INF_NUMID=a.OC_NUMID
            AND extract(YEAR FROM a.oc_data) = 2023
            AND inf_servei_d_codi IS NOT NULL"""
    with u.Database("redics", "data") as redics:
        for data, usuari, espe in redics.get_all(sql):
            if espe in sire and espe in ecap_ministerio.keys() and usuari in cips:  # noqa
                upload.append((cips[usuari], ecap_ministerio[espe], data))
    print('afegir dades 2')
    with u.Database("exadata", "data") as exadata:
        exadata.list_to_table(upload,
                              'bdcap_interconsultas_2023',
                              chunk=10 ** 5)

    
def Cataleg():
    canvis = [
            ('31013','OTR'),
            ('10666','OTR'),
            ('10148','OTR'),
            ('10146','OTR'),
            ('10143','OTR'),
            ('10139','OTR'),
            ('10128','OTR'),
            ('10111','ORL'),
            ('10110','OFT'),
            ('10106','nan'),
            ('10104','END'),
            ('10102','ACL'),
            ('10101','RAD'),
            ('20210','ORL'),
            ('10155','RAD'),
            ('30306','PED'),
            ('70121','RHB'),
            ('30308','HEM'),
            ('30319','URG'),
            ('30318','PED'),
            ('30310','NEF'),
            ('30302','PED'),
            ('20209','OFT'),
            ('20201','CIR'),
            ('10107','HEM'),
            ('20207','ACV'),
            ('10109','NEF'),
            ('30215','CPE'),
            ('30316','DIG'),
            ('30314','END'),
            ('30304','PED'),
            ('10108','NRL'),
            ('30313','INM'),
            ('10112','ONC'),
            ('30312','ONC'),
            ('30305','PED'),
            ('30315','CAR'),
            ('10149','GRT'),
            ('30301','NEO'),
            ('30320','PED'),
            ('30317','NRL'),
            ('30303','PED'),
            ('21211','TRA'),
            ('40101','OBG'),
            ('50115','PSQ'),
            ('20204','CMF'),
            ('20205','CPL'),
            ('30321','PED'),
            ('20212','URO'),
            ('10105','DIG'),
            ('20208','NRC'),
            ('20202','OTR'),
            ('20206','OTR')]
    for (cod_ecap, especialitat) in canvis:
        execute("""update dwsisap.BDCAP_CATALEG_2022 set conceptid = '{esp}' where
                tipus = 'INT' AND CONCEPTID in ('nan', 'OTR') 
                AND CODI_ECAP = '{ecap}'""".format(esp=especialitat, ecap=cod_ecap), 'exadata')
    execute("""update dwsisap.BDCAP_CATALEG_2022 set origen_dades = 'GPITB003' 
                where CONCEPTID = '426945003'""", 'exadata')
    execute("""update dwsisap.BDCAP_CATALEG_2022 set origen_dades = 'PRSTB016' 
                where CONCEPTID = '34431008'""", 'exadata')
    for cod in ('12029', '10397', '12502', '11164', '02182', '02183', '09440', '08145', '10688', '08146', '11171', '12028', '02015', '11169'):
        execute("""insert into dwsisap.BDCAP_CATALEG_2022 values 
            ('PRO', '{cod}', '34431008', 'PRSTB016', 'Fisioterapia respiratoria', 1, 0, Null)""".format(cod=cod),
            'exadata')
    execute("""insert into dwsisap.BDCAP_CATALEG_2022 values 
            ('PRO', '07868', '172630005', 'PRSTB016', 'Extraccion tapon oido', 1, 0, Null)""".format(cod=cod),
            'exadata')
    for cod in ('03241', '03242', '01937', '01960'):
        execute("""insert into dwsisap.BDCAP_CATALEG_2022 values 
            ('PRO', '{cod}', '410024004', 'PRSTB016', 'Sondaje vesical', 1, 0, Null)""".format(cod=cod),
            'exadata')
    execute("""insert into dwsisap.BDCAP_CATALEG_2022 values 
            ('PRO', 'EXO', '55162003', 'PRSTB508', 'Exodoncia', 1, 0, Null)""".format(cod=cod),
            'exadata')
    execute("""insert into dwsisap.BDCAP_CATALEG_2022 values 
            ('PRO', 'SEG', '234713009', 'PRSTB508', 'Sellado de fisuras/obturación', 1, 0, Null)""".format(cod=cod),
            'exadata')
    execute("""insert into dwsisap.BDCAP_CATALEG_2022 values 
            ('PRO', 'OBT', '234713009', 'PRSTB508', 'Sellado de fisuras/obturación', 1, 0, Null)""".format(cod=cod),
            'exadata')
    execute("""update dwsisap.BDCAP_CATALEG_2022 set DES_MINIS = 'Endodoncia en permanentes' 
                where CONCEPTID = '265328005'""", 'exadata')
    execute("""update dwsisap.BDCAP_CATALEG_2022 set TIPUS = 'PRO' 
                where ORIGEN_DADES = 'PRSTB051'""", 'exadata')
    execute("""update dwsisap.BDCAP_CATALEG_2022 set PROCEDIMIENTO = 1 
                where ORIGEN_DADES = 'PRSTB051'""", 'exadata')
    

if __name__ == "__main__":
    try:
        pacients()  # DONE
        variables()  # DONE
        episodio()  # DONE
        visitas()  # DONE
        cips = get_cips()
        interconsultes(cips)
        procediment(cips)
    except Exception as e:
        mail = u.Mail()
        mail.to.append("roser.cantenys@catsalut.cat")
        mail.subject = "BDCAP 2023!!!"
        mail.text = str(e)
        mail.send() 