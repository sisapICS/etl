# coding: utf-8

"""
 - TIME EXECUTION 26m

 - PETICIÓ:
    - Trello: https://trello.com/c/MUrKZ6BU
    - Drive:

"""

import sisapUtils as u
from datetime import datetime
import datetime as d


class mpocMinisterio(object):
    """ . """

    def __init__(self):
        """ . """
        self.get_poblacio()
        self.get_variables()
        self.get_denominador()
        self.get_numerador()

    def get_poblacio(self):
        """ . """
        print("------------------------------------------------- get_poblacio")
        self.poblacio = {}
        sql = """select
                    id_cip
                    , sexe
                 from
                    assignada_tot
                 where
                    ep = '0208'
                    and edat > 40"""
        for id_cip, _sexe in u.getAll(sql, 'nodrizas'):
            self.poblacio[id_cip] = True
        print("N registres:", len(self.poblacio))

    def get_variables(self):
        """ . """
        print("------------------------------------------------ get_variables")
        self.variables = {}
        self.paqany = set()
        self.paqany2018 = set()
        self.paqany10 = set()
        self.espiro = set()
        sql = """select
                    id_cip
                    , vu_cod_vs
                    , vu_val
                    , vu_dat_act
                 from
                    variables
                 where
                    vu_cod_vs in ('PAQAN2','PAQANY',
                                  'TR3030','TR3031','TR3032','TR3033',
                                  'TR3034','TR3035','TR3036','TR3037','PBD'
                                  ,'BF/F')
              """
        for id_cip, cod, val, _dat in u.getAll(sql, 'import'):
            if id_cip in self.poblacio:
                if cod in ('PAQAN2', 'PAQANY'):
                    self.paqany.add(id_cip)
                    #if dat >= d.date(2017,1,1):
                    #    self.paqany2017.add(id_cip)
                    if val > 10:
                        self.paqany10.add(id_cip)
                if cod in ('TR3030','TR3031','TR3032','TR3033','TR3034','TR3035','TR3036','TR3037','PBD','BF/F'):  # noqa
                    self.espiro.add(id_cip)
        print("N registres PAQANY:", len(self.paqany))
        #print("N registres PAQANY >= 2017:", len(self.paqany2017))
        print("N registres PAQANY10:", len(self.paqany10))
        print("N registres ESPIRO:", len(self.espiro))

    def get_denominador(self):
        """ . """
        print("---------------------------------------------- get_denominador")
        self.den = set()
        for id_cip in self.paqany10:
            self.den.add(id_cip)
        print("N registres DENOMINADOR:", len(self.den))

    def get_numerador(self):
        """ . """
        print("------------------------------------------------ get_numerador")
        self.num = set()
        for id_cip in self.den:
            if id_cip in self.espiro:
                self.num.add(id_cip)
        print("N registres NUMERADOR:", len(self.num))


if __name__ == '__main__':
    print(datetime.now())
    ts = datetime.now()
    mpocMinisterio()
    print('Time execution {}'.format(datetime.now() - ts))
