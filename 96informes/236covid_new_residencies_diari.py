import sisapUtils as u
from datetime import datetime as d
from dateutil import relativedelta as rd
from shutil import copyfile
import collections as c
import numpy as np
import statistics as st
import re
import os
import sys
import hashlib
# import pandas as pd

PAQUI, LEO = "PREDUPRP", "PREDULMB"
WATCHERS = ("PREDULMB", "PREDUPRP", "PREDUMMP")
DB_DESTI = 'redics'

# Tipus de UP per cataleg rup_up a la taula SISAP_CAT_GRUP_SRC_CODES
CUP_TUP = ('7071', '7075', '7076', '7077', '7078', '7079', '7171', '7179')

LV_RENAME_SYMBOL_FILE = u.tempFolder + 'LV_RENAME_RESI_SYMBOL.txt'
LV_NO_CHANGE_SYMBOL_FILE = u.tempFolder + 'LV_NO_CHANGE_RESI_SYMBOL.txt'
LV_NO_CHANGE_SYMBOL_LIST = [['resi_code', 'name', 'up_cs', 'up_ics']]
LV_RENAME_SYMBOL_LIST = [['resi_code', 'resi_code_old']]

def birthday(date):
    # Get the current date
    now = d.utcnow().date()
    # Get the difference between the current date and the birthday
    age = rd.relativedelta(now, date)
    age = age.years
    return age


tb_desti = 'SISAP_CAT_GRUPSUSUARIS'
cols = ['sector_cod varchar2(4)',
        'grup_cod varchar2(8)',
        'grup_des VARCHAR2(30)',
        'grup_up varchar2(5)',
        'grup_n NUMBER',
        'grup_edat NUMBER',
        'eap1 varchar2(5)',
        'eap1_n NUMBER',
        'eaps NUMBER',
        ]

sql = """
        select
            '{}',
            cip_usuari_cip,
            ug_codi_grup||'',
            gu_up_residencia,
            gu_desc,
            to_date(usua_data_naixement, 'J'),
            usua_uab_up
        from ppftb012,
            ppftb011,
            usutb011,
            usutb040
        where
            ug_usua_cip = cip_cip_anterior
            and ug_codi_grup = gu_codi_grup
            and usua_cip = cip_usuari_cip
            and ug_dba is null
            and gu_dba is null
        """

grups = c.defaultdict(list)
for sector in u.sectors:
    print(sector)
    for sec, cip, grup, grup_up, grup_des, dnaix, eap in u.getAll(sql.format(sector), sector):  # noqa
        edat = birthday(dnaix)
        grups[(sec, grup, grup_des, grup_up), 'edats'].append(edat)
        grups[(sec, grup, grup_des, grup_up), 'eaps'].append(eap)

grups_agr = c.defaultdict(dict)
for k in grups:
    if k[1] == 'edats':
        grups_agr[k[0]]["grup_n"] = len(grups[k])
        grups_agr[k[0]]["grup_edat"] = int(np.mean(grups[k]))
    elif k[1] == 'eaps':
        grups_agr[k[0]]["eaps"] = len(set(grups[k]))
        try:
            grups_agr[k[0]]["eap1"] = eap1 = st.mode(grups[k])
            grups_agr[k[0]]["eap1_n"] = len([eap for eap in grups[k] if eap == eap1])  # noqa
        except st.StatisticsError:
            grups_agr[k[0]]["eap1"] = None
            grups_agr[k[0]]["eap1_n"] = None
            continue

upload = []
for k, v in grups_agr.items():
    sec, grup, grup_des, grup_up = k[0], k[1], k[2], k[3]
    grup_n = v["grup_n"]
    grup_edat = v["grup_edat"]
    eap1 = v["eap1"]
    eap1_n = v["eap1_n"]
    eaps = v["eaps"]
    upload.append((sec, grup, grup_des, grup_up, grup_n,
                   grup_edat, eap1, eap1_n, eaps))

print(upload[0])
print(tb_desti, DB_DESTI)
print(", ".join(cols))

u.createTable(tb_desti, "({})".format(", ".join(cols)), DB_DESTI, rm=True)
u.listToTable(upload, tb_desti, DB_DESTI)
u.grantSelect(tb_desti, WATCHERS, DB_DESTI)
sql = """
    SELECT DISTINCT
        'sec_grup' AS src_code_type
        , sector_cod||'-'||grup_cod AS src_code
        , grup_des AS src_code_desc
        , sector_cod AS src_type
    FROM
        SISAP_CAT_GRUPSUSUARIS gu
    UNION
    SELECT DISTINCT
        'grup_up' AS src_code_type
        , grup_up AS src_code
        , cup_nup AS src_code_desc
        , cup_tup AS src_type
    FROM
        SISAP_CAT_GRUPSUSUARIS gu
        LEFT JOIN REPTBCUP cup ON gu.grup_up = cup.cup_cup
    WHERE
        grup_up IS NOT NULL
    UNION
    SELECT DISTINCT
        'rup_up' AS src_code_type
        , cup_cup AS src_code
        , cup_nup AS src_code_desc
        , cup_tup AS src_type
    FROM
        REPTBCUP cup
    WHERE
        cup_tup IN {cup_tup}
""".format(cup_tup=CUP_TUP)

upload = []
for row in u.getAll(sql, 'redics'):
    upload.append(row)

cols = [
    'src_code_type VARCHAR(10)',
    'src_code  VARCHAR(13)',
    'src_code_desc varchar(50)',
    'src_type varchar(10)'
    ]

u.createTable('SISAP_CAT_GRUP_SRC_CODES', "({})".format(", ".join(cols)),
              'redics', rm=True)
u.listToTable(upload, 'SISAP_CAT_GRUP_SRC_CODES', 'redics')
u.grantSelect("SISAP_CAT_GRUP_SRC_CODES", WATCHERS, 'redics')

pathname = os.path.dirname(sys.argv[0])
print(pathname)

UPSERT_GRUPS = 'upsert_SISAP_MAP_GRUP_RUP.txt'
UPSERT_GRUPS_OLD = 'upsert_SISAP_MAP_GRUP_RUP_OLD.txt'
UPSERT_GRUPS_FSN = os.path.join(pathname, UPSERT_GRUPS)
UPSERT_GRUPS_FSN_OLD = os.path.join(pathname, UPSERT_GRUPS_OLD)
col_names = "(sec, grup, sisap_class, src_code_type, src_code, target_code_type, target_code, reses_code, src_code_old)"  # noqa

delete_query = "delete from {} where sec = '{}' and grup = '{}'"
insert_query = "insert into {} {} values {}"

send_rename_file = False
send_no_change_file = False
MD5_CURRENT = hashlib.md5(open(UPSERT_GRUPS_FSN,'rb').read()).hexdigest()
MD5_OLD = hashlib.md5(open(UPSERT_GRUPS_FSN_OLD,'rb').read()).hexdigest()

sql = "select resi_cod, resi_cod_type from sisap_map_residencies where resi_type = '7071'"
codes = []
for (code, tipus) in u.getAll(sql, 'redics'):
    codes.append((code, tipus))

if MD5_CURRENT != MD5_OLD:
    i = 0
    for row in u.readCSV(UPSERT_GRUPS_FSN, sep='|'):
        i += 1
        row = [re.sub(r"\s", "", field) for field in row]
        sec, grup, = row[0], row[1],
        clas, src_code, src_code_old = row[2], row[4], row[8]
        print i, tuple(row)
        u.execute(delete_query.format('SISAP_MAP_GRUP_RUP', sec, grup), 'redics')
        u.execute(insert_query.format('SISAP_MAP_GRUP_RUP', col_names, tuple(row)),
                'redics')
        if src_code != src_code_old and clas == '1':
            send_rename_file = True
            LV_RENAME_SYMBOL_LIST.append([src_code, src_code_old])
        
        if src_code == src_code_old and clas == '1':
            #if (src_code, row[3]) not in codes:
            send_no_change_file = True
            sql = "select src_code_desc from SISAP_CAT_GRUP_SRC_CODES where src_code = '{}' and src_code_type = '{}'".format(str(src_code), row[3])
            sql_2 = "select ics_codi from cat_centres where scs_codi = '{}'".format(row[6])
            print(sql)
            name = u.getOne(sql, "redics")
            up, = u.getOne(sql_2, 'nodrizas')
            if name:
                print('dins 1')
                LV_NO_CHANGE_SYMBOL_LIST.append([src_code, name[0], row[6], up])
            # else:
            #     sql = "select src_code_desc from SISAP_CAT_GRUP_SRC_CODES where src_code = '{}' and src_code_type = '{}'".format(str(row[6]), row[3])
            #     print(sql)
            #     name = u.getOne(sql, "redics")
            #     if name:
            #         print('dins 2')
            #         LV_NO_CHANGE_SYMBOL_LIST.append([src_code, name[0], row[6]])
        
    if send_no_change_file and len(LV_NO_CHANGE_SYMBOL_LIST) > 1:
        print('send_no_change_file')
        u.writeCSV(LV_NO_CHANGE_SYMBOL_FILE, LV_NO_CHANGE_SYMBOL_LIST, sep='{')
        u.sendGeneral(
            me='sisap@gencat.cat',
            to=['bpons@gencat.cat'],
            cc=['framospe@gencat.cat', 'nuriacantero@gencat.cat'],
            subject='Simbols residencies 2.0',
            text= """
            Belen,
            
            Adjuntem codis de noves residencies al cataleg""",
            file=LV_NO_CHANGE_SYMBOL_FILE
            )
        

    if send_rename_file and len(LV_RENAME_SYMBOL_LIST) > 1:
        print('send_rename_file')
        u.writeCSV(LV_RENAME_SYMBOL_FILE, LV_RENAME_SYMBOL_LIST, sep='{')
        u.sendGeneral(
            me='sisap@gencat.cat',
            to=['bpons@gencat.cat', 'lmendezboo@gencat.cat',],
            cc=['framospe@gencat.cat', 'nuriacantero@gencat.cat'],
            subject='Renombrar simbols de residencies',
            text= """
            Belen,
            
            Adjuntem residencies per renombrar simbols""",
            file=LV_RENAME_SYMBOL_FILE
            )
    copyfile(UPSERT_GRUPS_FSN, UPSERT_GRUPS_FSN_OLD)

u.createTable("SISAP_MAP_RESIDENCIES",
              """ as select r.*, c.cep_cep as resi_ep, e.cep_nep as resi_ep_des
              from
                SISAP_VW_RESIDENCIES r
                left join REPTBCUP c on r.resi_cod = c.cup_cup
                left join REPTBCEP e on c.cep_cep = e.cep_cep
              """,
              "redics",
              rm=True)
u.grantSelect("SISAP_MAP_RESIDENCIES", WATCHERS, "redics")

# reclass_tb = "SISAP_CAT_RECLASS_RESINOECAP"
tb_desti = 'SISAP_CAT_RECLASS_RESINOECAP'
cols = ['resi_up varchar2(5)',
        'sisap_class NUMBER',
        ]
u.createTable(tb_desti, "({})".format(", ".join(cols)), 'redics', rm=False)
col_names = "(resi_up, sisap_class)"

UPSERT_RECLASS = 'upsert_SISAP_RECLASS_RESINOECAP.txt'
UPSERT_RECLASS_FSN = os.path.join(pathname, UPSERT_RECLASS)

delete_query = "delete from {} where resi_up = '{}'"

i = 0
for row in u.readCSV(UPSERT_RECLASS_FSN, sep='|'):
    i += 1
    row = [re.sub(r"\s", "", field) for field in row]
    resi_up, sisap_class = row[0], row[1]
    print i, tuple(row)
    u.execute(delete_query.format(tb_desti, resi_up), 'redics')
    u.execute(insert_query.format(tb_desti, col_names, tuple(row)), 'redics')

u.grantSelect(tb_desti, WATCHERS, DB_DESTI)
