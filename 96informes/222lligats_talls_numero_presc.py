# coding: latin1

"""
Prescripcions amb diagnòstic lligat.
"""

import sisapUtils as u

#sectors=['6837', '6734']

class Main(object):
    """."""

    def __init__(self):
        """."""
        num_diag={}
        lligats={}
        resultat = u.multiprocess(Sector, u.sectors)
        for sector in resultat:
            for row in sector.num_diag:
                num_diag[row, sector]=sector.num_diag[row]

            lligats=sector.lligats
        print len(num_diag)
        print len(lligats)
        
        num_diag_agrupats={}
        for codi in num_diag:
            num=num_diag[codi]
            if num not in num_diag_agrupats:
                num_diag_agrupats[num]=1
            else:
                num_diag_agrupats[num]+=1
        
        #upload = lligats.items()
        #u.writeCSV(u.tempFolder + "lligats.csv", upload, sep=";")
        #upload = num_diag.items()
        #u.writeCSV(u.tempFolder + "num_diag.csv", upload, sep=";")
        upload = num_diag_agrupats.items()
        u.writeCSV(u.tempFolder + "num_diag_agrupats.csv", upload, sep=";")

class Sector(object):
    """."""

    def __init__(self, sector):
        """."""
        self.sector = sector
        self.get_lligats()
        #self.get_prescripcions()
        print self.sector

    def get_lligats(self):
        """."""
        self.lligats = {}
        self.num_diag= {}
        #Compta número de diagnòstics per cada codi
        sql = "select dgppf_pmc_codi, dgppf_num_prod from ppftb023"
        for row in u.getAll(sql, self.sector):
            if row not in self.lligats:
                self.lligats[row] = 1
            else:
                self.lligats[row] = self.lligats[row]+1

        sql = "select ppfmc_pmc_codi, ppfmc_num_prod from ppftb016 \
               where ppfmc_pmc_data_ini < date '2019-09-18' and \
               ppfmc_data_fi > date '2019-09-18'"
        
        for row in u.getAll(sql, self.sector):
            if row not in self.lligats:
                self.num_diag[row]=0
            else:
                self.num_diag[row]=self.lligats[row]        

if __name__ == "__main__":
    Main()
