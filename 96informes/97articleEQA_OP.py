# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

debug = False

nod = 'nodrizas'
imp = 'import'
farmacs = '(713, 714, 719, 720, 717, 718, 715, 716)'

problems ='(79, 710, 711, 712)'

exclusions_p = '(79, 710, 711, 712, 376, 383)'



def farmconverter(farmac):
    
    if farmac in (82, 658, 216, 659):
        far = 'Estatines'
    elif farmac in (713, 714):
        far = 'MSRE'
    elif farmac in (715, 716):
        far = 'Denosumab'
    elif farmac in (717, 718):
        far = 'Bifosfonats'
    elif farmac in (719, 720):
        far = 'Altres aOP'
    else:
        far = 'error'
    return far

def psconverter(ps):
    
    if ps in (726,727):
        prob = 'CI'
    elif ps in (728,729):
        prob = 'AVC'
    elif ps in (79, 710):
        prob = 'Fractura osteoporòtica'
    elif ps in (711, 712):
        prob = 'Fractura maluc'
    else:
        prob = 'error'
    return prob

anys = [2017, 2018]
#anys = [2016]
#mesos = ['01']
mesos = ['01','02','03','04','05','06','07','08','09','10','11','12']

u11 = {}
all_cips = {}
sql = 'select id_cip, id_cip_sec,hash_d from u11{0}'.format( ' limit 10' if debug else '')
for id, idsec,hash_d in getAll(sql, imp):
    u11[hash_d] = id
    all_cips[idsec] = id
    
id_sidiap = {}
sql = 'select id, id_persistent from poblacio {}'.format( ' limit 10' if debug else '')
for id, idP in getAll(sql, ('SISAP_estatines','nym_proj')):
    a = tuple(idP.split(':'))
    idP = a[0]
    id_sidiap[int(id)] = idP


centres = {}

sql = "select sha1(concat(scs_codi,'EQA')), medea from cat_centres where ep='0208'"
for up, medea in getAll(sql, nod):
    centres[up] = medea
    
#dates naixement tota la pob

all_pob = {}
sql = 'select id_cip, usua_sexe, year(usua_data_naixement) from assignada {0}'. format(' limit 10' if debug else '')
for id,  sexe, naix in getAll(sql, imp):
    all_pob[id] = {'sexe':sexe, 'naix': naix}

#farmacs

minfarmac = {}
prevfarmac = {}
sql = 'select id, dat, agr from farmacs_facturats where agr in {0} {1}'.format(farmacs, ' limit 10' if debug else '')
for id, dat, farmac in getAll(sql, ('SISAP_OP','nym_proj')):
    far = farmconverter(int(farmac))
    try:
        hashP = id_sidiap[int(id)]
    except KeyError:
        continue
    try:
        id = u11[hashP]
    except KeyError:
        continue
    if (id, far) in minfarmac:
        inici2 = minfarmac[(id, far)]
        if float(dat) < float(inici2):
            minfarmac[(id, far)] = str(dat)
    else:
        minfarmac[(id, far)] = str(dat)
    if str(dat)[4:] == '12':
        unmesabans = int(dat) + 89
        dosmesosbans = int(dat) + 90
    elif str(dat)[4:] == '11':
        unmesabans = int(dat) + 1
        dosmesosbans = int(dat) + 90
    else:
        unmesabans = int(dat) + 1
        dosmesosbans = int(dat) + 2
    dat = str(dat)
    unmesabans = str(unmesabans)
    dosmesosbans = str(dosmesosbans)
    prevfarmac[(id, far, dat)] = True
    prevfarmac[(id, far, unmesabans)] = True
    prevfarmac[(id, far, dosmesosbans)] = True

    

#problemes salut:

agrs = []
conv = {}
sql = "select agrupador,criteri_codi from eqa_criteris where agrupador in {0}{1}".format(problems, ' limit 10' if debug else '')
for agrupador, criteri in getAll(sql, nod):
    agrs.append(criteri)
    conv[criteri] = agrupador
in_crit = tuple(agrs)

incidents = defaultdict(list)
sql = "select id_cip, pr_cod_ps, pr_dde, extract(year_month from pr_dde) from problemes where  pr_cod_o_ps = 'C' and pr_hist = 1 and pr_cod_ps in {0} {1}".format(in_crit, ' limit 10' if debug else '')
for id, ps1, dde, Ydde in getAll(sql, imp):
    ps = conv[ps1]
    prob = psconverter(ps)
    Ydde = str(Ydde)
    incidents[id, prob].append(Ydde)


#risc: baix = 0; alt = 1 inhibidors 2; 3 = risc si el té abans

riscfract =  defaultdict(list)
sql = "select agrupador,criteri_codi from eqa_criteris where agrupador in {0}{1}".format('(237,78, 79)', ' limit 10' if debug else '')
for agrupador, criteri in getAll(sql, nod):
    agrs.append(criteri)
    conv[criteri] = agrupador
in_crit = tuple(agrs)


sql = "select id_cip, pr_cod_ps, pr_dde, extract(year_month from pr_dde) from problemes where  pr_cod_o_ps = 'C' and pr_hist = 1 and pr_cod_ps in {0} {1}".format(in_crit, ' limit 10' if debug else '')
for id, ps1, dde, Ydde in getAll(sql, imp):
    Ydde = str(Ydde)
    riscfract[id].append([Ydde, 3])


sql = 'select id, dat, agr from farmacs_facturats where agr in {0} {1}'.format('(81,25)', ' limit 10' if debug else '')
for id, dat, far in getAll(sql, ('SISAP_OP','nym_proj')):
    try:
        hashP = id_sidiap[int(id)]
    except KeyError:
        continue
    try:
        id = u11[hashP]
    except KeyError:
        continue
    if str(dat)[4:] == '12':
        unmesabans = int(dat) + 89
        dosmesosbans = int(dat) + 90
    elif str(dat)[4:] == '11':
        unmesabans = int(dat) + 1
        dosmesosbans = int(dat) + 90
    else:
        unmesabans = int(dat) + 1
        dosmesosbans = int(dat) + 2       
    dat = str(dat)
    unmesabans = str(unmesabans)
    dosmesosbans = str(dosmesosbans)
    if far == '25':
        riscfract[id].append([dat, 1])
        riscfract[id].append([unmesabans, 1])
        riscfract[id].append([dosmesosbans, 1])
    elif far == '81':
        riscfract[id].append([dat, 2])
        riscfract[id].append([unmesabans, 2])
        riscfract[id].append([dosmesosbans, 2])
    
    
#exclusions


agrs = []
sql = "select agrupador,criteri_codi from eqa_criteris where agrupador in {0}".format(exclusions_p)
for agrupador, criteri in getAll(sql, nod):
    agrs.append(criteri)
in_crit = tuple(agrs)

exclusions = defaultdict(list)
sql = "select id_cip, pr_cod_ps, pr_dde, extract(year_month from pr_dde) from problemes where  pr_cod_o_ps = 'C' and pr_hist = 1 and pr_cod_ps in {0} {1}".format(in_crit, ' limit 10' if debug else '')
for id, ps, dde, Ydde in getAll(sql, imp):
    Ydde = str(Ydde)
    exclusions[(id, 'N')] = {Ydde}
    



visites = {}
"""
for ane in anys:
    for table in getSubTables('visites'):
        if ane < 2014:
            dat, = getOne('select year(visi_data_visita) from {} limit 1'.format(table), 'import')
            if dat == ane:
                for id, up in getAll("select id_cip,  visi_up  from {} where visi_situacio_visita = 'R'".format(table), 'import'):
                    visites[(id, ane)] = True
"""

for ane in anys:
    resultat1, resultat2, resultat3, resultat4, resultat5, resultat6, resultat7, resultat8, resultat9, resultat10, resultat11, resultat12 = Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter()
    resultat13, resultat14, resultat15, resultat16, resultat17, resultat18, resultat19, resultat20, resultat21, resultat22, resultat23, resultat24,resultat25,resultat26,resultat27  = Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter()
    resultat28, resultat29,resultat30,resultat31,resultat32,resultat33,resultat34,resultat35,resultat36,resultat37,resultat38,resultat39 = Counter(), Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),
    resultat40,resultat41, resultat42,resultat43,resultat44 ,resultat45,resultat46 ,resultat47,resultat48,resultat49 = Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter()
    resultat50,resultat51, resultat52,resultat53,resultat54 ,resultat55,resultat56 ,resultat57,resultat58,resultat59 = Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter()
    resultat60,resultat61, resultat62,resultat63,resultat64 ,resultat65,resultat66 ,resultat67,resultat68,resultat69 = Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter(),Counter()
    resultat70,resultat71, resultat72 =  Counter(),Counter(),Counter()
    assignada = {}
    sql = "select id_cip, sha1(concat(up,'EQA')), ates from assignadahistorica where dataany={0} {1}".format(ane, ' limit 1' if debug else '')
    for id, up, ates in getAll(sql, imp):
        if up in centres:
            medea = centres[up]
            assig = 1
            if ane < 2014:
                if (id, ane) in visites:
                    ates = 1
                else:
                    ates = 0
            sexe = 'NS'
            edat, edat5 = 'NS', 'NS'
            try:
                sexe = all_pob[id]['sexe']
                naix = all_pob[id]['naix']
                edat = ane - naix
            except KeyError:
                pass
            if edat < 15:
                continue
            if edat <>'NS':
                edat5 = ageConverter(edat)
            for mes in mesos:
                periode = str(ane) + mes
                excl = 0
                if (id, 'N') in exclusions:
                    for data_excl in exclusions[id, 'N']:
                        if data_excl < periode:
                            excl = 1      
                if excl == 0:
                    MSREP = 0
                    if (id, 'MSRE', periode) in prevfarmac:
                        MSREP = 1     
                    MSREI = 0
                    if (id, 'MSRE') in minfarmac:
                        iniciest = minfarmac[(id, 'MSRE')]
                        if iniciest == periode:
                            MSREI = 1    
                    DenosumabP = 0
                    if (id, 'Denosumab', periode) in prevfarmac:
                        DenosumabP = 1     
                    DenosumabI = 0
                    if (id, 'Denosumab') in minfarmac:
                        iniciest = minfarmac[(id, 'Denosumab')]
                        if iniciest == periode:
                            DenosumabI = 1 
                    BifosfonatsP = 0
                    if (id, 'Bifosfonats', periode) in prevfarmac:
                        BifosfonatsP = 1     
                    BifosfonatsI = 0
                    if (id, 'Bifosfonats') in minfarmac:
                        iniciest = minfarmac[(id, 'Bifosfonats')]
                        if iniciest == periode:
                            BifosfonatsI = 1
                    altresOPP = 0
                    if (id, 'Altres aOP', periode) in prevfarmac:
                        altresOPP = 1     
                    altresOPI = 0
                    if (id, 'Altres aOP') in minfarmac:
                        iniciest = minfarmac[(id, 'Altres aOP')]
                        if iniciest == periode:
                            altresOPI = 1
                    fractFragI, FractMalI = 0,0
                    if (id, 'Fractura osteoporòtica') in incidents:
                        for inicialM in incidents[id, 'Fractura osteoporòtica']:
                            if inicialM == periode:
                                fractFragI = 1
                    if (id, 'Fractura maluc') in incidents:
                        for inicialM in incidents[id, 'Fractura maluc']:
                            if inicialM == periode:
                                FractMalI = 1
                    rfbaixmsre, rfaltmsre, rfaromamsre = 0,0,0
                    rfbaixmsreP, rfaltmsreP, rfaromamsreP = 0,0,0
                    rfbaixdenos, rfaltdenos, rfaromadenos = 0,0,0
                    rfbaixdenosP, rfaltdenosP, rfaromadenosP = 0,0,0
                    rfbaixbifos, rfaltbifos, rfaromabifos = 0,0,0
                    rfbaixbifosP, rfaltbifosP, rfaromabifosP = 0,0,0
                    rfbaixaltOP, rfaltaltOP, rfaromaaltOP = 0,0,0
                    rfbaixaltOPP, rfaltaltOPP, rfaromaaltOPP = 0,0,0
                    rfbaixNOF, rfaltNOF, rfaromaNOF = 0,0,0
                    rfbaixeveFrOP, rfalteveFrOP, rfaromaeveFrOP = 0,0,0
                    rfbaixeveFrMaluc, rfalteveFrMaluc, rfaromaeveFrMaluc = 0,0,0
                    rfbaixass, rfaltass, rfaromaass = 0,0,0
                    rfbaixat, rfaltat, rfaromaat = 0,0,0
                    risky = 0
                    if id in riscfract:
                        rows = riscfract[id]
                        for drcv, tipusRisc in rows:
                            if tipusRisc == 1:
                                if drcv == periode:
                                    risky = 1
                            elif tipusRisc == 3:
                                if drcv < periode:
                                    risky = 1
                            elif tipusRisc == 2:
                                if drcv == periode:
                                    if risky == 0:
                                        risky = 2
                    if risky == 0:
                        rfbaixass = 1
                        if ates == 1:
                            rfbaixat = 1
                        if  MSREI == 1:
                            rfbaixmsre = 1
                        if MSREP == 1:
                            rfbaixmsreP = 1
                        if  DenosumabI == 1:
                            rfbaixdenos = 1
                        if DenosumabP == 1:
                            rfbaixdenosP = 1
                        if  BifosfonatsI == 1:
                            rfbaixbifos = 1
                        if BifosfonatsP == 1:
                            rfbaixbifosP = 1
                        if  altresOPI == 1:
                            rfbaixaltOP = 1
                        if altresOPP == 1:
                            rfbaixaltOPP = 1
                        if MSREP == 0 and DenosumabP == 0 and BifosfonatsP == 0 and altresOPP == 0:
                            rfbaixNOF = 1
                        if fractFragI == 1:
                            rfbaixeveFrOP = 1
                        if FractMalI == 1:
                            rfbaixeveFrMaluc = 1
                    elif risky == 1:
                        rfaltass = 1
                        if ates == 1:
                            rfaltat = 1
                        if  MSREI == 1:
                            rfaltmsre = 1
                        if MSREP == 1:
                            rfaltmsreP = 1
                        if  DenosumabI == 1:
                            rfaltdenos = 1
                        if DenosumabP == 1:
                            rfaltdenosP = 1
                        if  BifosfonatsI == 1:
                            rfaltbifos = 1
                        if BifosfonatsP == 1:
                            rfaltbifosP = 1
                        if  altresOPI == 1:
                            rfaltaltOP = 1
                        if altresOPP == 1:
                            rfaltaltOPP = 1
                        if MSREP == 0 and DenosumabP == 0 and BifosfonatsP == 0 and altresOPP == 0:
                            rfaltNOF = 1
                        if fractFragI == 1:
                            rfalteveFrOP = 1
                        if FractMalI == 1:
                            rfalteveFrMaluc = 1
                    elif risky == 2:
                        rfaromaass = 1
                        if ates == 1:
                            rfaromaat = 1
                        if  MSREI == 1:
                            rfaromamsre = 1
                        if MSREP == 1:
                            rfaromamsreP = 1
                        if  DenosumabI == 1:
                            rfaromadenos = 1
                        if DenosumabP == 1:
                            rfaromadenosP = 1
                        if  BifosfonatsI == 1:
                            rfaromabifos = 1
                        if BifosfonatsP == 1:
                            rfaromabifosP = 1
                        if  altresOPI == 1:
                            rfaromaaltOP = 1
                        if altresOPP == 1:
                            rfaromaaltOPP = 1
                        if MSREP == 0 and DenosumabP == 0 and BifosfonatsP == 0 and altresOPP == 0:
                            rfaromaNOF = 1
                        if fractFragI == 1:
                            rfaromaeveFrOP = 1
                        if FractMalI == 1:
                            rfaromaeveFrMaluc = 1
                    resultat1[(periode, up, medea, edat5, sexe)] += MSREP
                    resultat2[(periode, up, medea, edat5, sexe)] += MSREI
                    resultat9[(periode, up, medea, edat5, sexe)] += BifosfonatsI
                    resultat27[(periode, up, medea, edat5, sexe)] += BifosfonatsP
                    resultat11[(periode, up, medea, edat5, sexe)] += assig
                    resultat12[(periode, up, medea, edat5, sexe)] += ates
                    resultat13[(periode, up, medea, edat5, sexe)] += DenosumabI
                    resultat14[(periode, up, medea, edat5, sexe)] += DenosumabP
                    resultat15[(periode, up, medea, edat5, sexe)] += altresOPI
                    resultat16[(periode, up, medea, edat5, sexe)] += altresOPP
                    resultat40[(periode, up, medea, edat5, sexe)] += fractFragI
                    resultat41[(periode, up, medea, edat5, sexe)] += FractMalI
                    resultat42[(periode, up, medea, edat5, sexe)] += rfbaixmsre
                    resultat43[(periode, up, medea, edat5, sexe)] += rfbaixmsreP
                    resultat17[(periode, up, medea, edat5, sexe)] += rfbaixdenos
                    resultat18[(periode, up, medea, edat5, sexe)] += rfbaixdenosP
                    resultat19[(periode, up, medea,  edat5, sexe)] += rfbaixbifos
                    resultat20[(periode, up, medea, edat5, sexe)] += rfbaixbifosP
                    resultat21[(periode, up, medea, edat5, sexe)] += rfbaixaltOP
                    resultat22[(periode, up, medea, edat5, sexe)] += rfbaixaltOPP
                    resultat23[(periode, up, medea, edat5, sexe)] += rfbaixNOF
                    resultat24[(periode, up, medea, edat5, sexe)] += rfbaixeveFrOP
                    resultat28[(periode, up, medea, edat5, sexe)] += rfbaixeveFrMaluc
                    resultat29[(periode, up, medea, edat5, sexe)] += rfbaixass
                    resultat30[(periode, up, medea, edat5, sexe)] += rfbaixat
                    resultat31[(periode, up, medea, edat5, sexe)] += rfaltmsre
                    resultat32[(periode, up, medea, edat5, sexe)] += rfaltmsreP
                    resultat33[(periode, up, medea, edat5, sexe)] += rfaltdenos
                    resultat34[(periode, up, medea, edat5, sexe)] += rfaltdenosP
                    resultat35[(periode, up, medea, edat5, sexe)] += rfaltbifos
                    resultat36[(periode, up, medea, edat5, sexe)] += rfaltbifosP
                    resultat37[(periode, up, medea, edat5, sexe)] += rfaltaltOP
                    resultat38[(periode, up, medea, edat5, sexe)] += rfaltaltOPP
                    resultat39[(periode, up, medea, edat5, sexe)] += rfaltNOF
                    resultat50[(periode, up, medea, edat5, sexe)] += rfalteveFrOP
                    resultat51[(periode, up, medea, edat5, sexe)] += rfalteveFrMaluc
                    resultat52[(periode, up, medea, edat5, sexe)] += rfaltass
                    resultat53[(periode, up, medea, edat5, sexe)] += rfaltat
                    resultat54[(periode, up, medea, edat5, sexe)] += rfaromamsre
                    resultat55[(periode, up, medea, edat5, sexe)] += rfaromamsreP
                    resultat56[(periode, up, medea, edat5, sexe)] += rfaromadenos
                    resultat57[(periode, up, medea, edat5, sexe)] += rfaromadenosP
                    resultat58[(periode, up, medea, edat5, sexe)] += rfaromabifos
                    resultat59[(periode, up, medea, edat5, sexe)] += rfaromabifosP
                    resultat60[(periode, up, medea, edat5, sexe)] += rfaromaaltOP
                    resultat61[(periode, up, medea, edat5, sexe)] += rfaromaaltOPP
                    resultat62[(periode, up, medea, edat5, sexe)] += rfaromaNOF
                    resultat63[(periode, up, medea, edat5, sexe)] += rfaromaeveFrOP
                    resultat64[(periode, up, medea, edat5, sexe)] += rfaromaeveFrMaluc
                    resultat65[(periode, up, medea, edat5, sexe)] += rfaromaass
                    resultat66[(periode, up, medea, edat5, sexe)] += rfaromaat
                else:
                    resultat44[(periode, up, medea, edat5, sexe)] += excl
                    MSREPexcl = 0
                    if (id, 'MSRE', periode) in prevfarmac:
                        MSREPexcl = 1
                    MSREIexcl = 0
                    if (id, 'MSRE') in minfarmac:
                        iniciest = minfarmac[(id, 'MSRE')]
                        if iniciest == periode:
                            MSREIexcl = 1    
                    dennosumabPexcl= 0
                    if (id, 'Denosumab', periode) in prevfarmac:
                        dennosumabPexcl = 1
                    DenosumabeIexcl = 0
                    if (id, 'Denosumab') in minfarmac:
                        iniciest = minfarmac[(id, 'Denosumab')]
                        if iniciest == periode:
                            DenosumabeIexcl = 1  
                    BifosfonatsPexcl= 0
                    if (id, 'Bifosfonats', periode) in prevfarmac:
                        BifosfonatsPexcl = 1
                    BifosfonatsIexcl = 0
                    if (id, 'Bifosfonats') in minfarmac:
                        iniciest = minfarmac[(id, 'Bifosfonats')]
                        if iniciest == periode:
                            BifosfonatsIexcl = 1  
                    AltresOPPexcl= 0
                    if (id, 'Altres aOP', periode) in prevfarmac:
                        AltresOPPexcl = 1
                    AltresOPIexcl = 0
                    if (id, 'Altres aOP') in minfarmac:
                        iniciest = minfarmac[(id, 'Altres aOP')]
                        if iniciest == periode:
                            AltresOPIexcl = 1  
                    FractfragIexcl, FractmalucIexcl = 0,0
                    if (id, 'Fractura osteoporòtica') in incidents:
                        for inicialM in incidents[id, 'Fractura osteoporòtica']:
                            if inicialM == periode:
                                FractfragIexcl = 1
                    if (id, 'Fractura maluc') in incidents:
                        for inicialM in incidents[id, 'Fractura maluc']:
                            if inicialM == periode:
                                FractmalucIexcl = 1
                    resultat46[(periode, up, medea, edat5, sexe)] += MSREPexcl
                    resultat47[(periode, up, medea, edat5, sexe)] += MSREIexcl
                    resultat48[(periode, up, medea, edat5, sexe)] += dennosumabPexcl
                    resultat49[(periode, up, medea, edat5, sexe)] += DenosumabeIexcl
                    resultat67[(periode, up, medea, edat5, sexe)] += BifosfonatsPexcl
                    resultat68[(periode, up, medea, edat5, sexe)] += BifosfonatsIexcl
                    resultat69[(periode, up, medea, edat5, sexe)] += AltresOPPexcl
                    resultat70[(periode, up, medea, edat5, sexe)] += AltresOPIexcl
                    resultat71[(periode, up, medea, edat5, sexe)] += FractfragIexcl
                    resultat72[(periode, up, medea, edat5, sexe)] += FractmalucIexcl
                resultat45[(periode, up, medea, edat5, sexe)] += assig
    upload = []
    for (periode, up, medea, edat5, sexe), res45 in resultat45.items():
        res1 = resultat1[(periode, up, medea, edat5, sexe)]
        res2 = resultat2[(periode, up, medea, edat5, sexe)]
        res9 = resultat9[(periode, up, medea, edat5, sexe)]
        res11 = resultat11[(periode, up, medea, edat5, sexe)]
        res12 = resultat12[(periode, up, medea, edat5, sexe)]
        res13 = resultat13[(periode, up, medea, edat5, sexe)]
        res14 = resultat14[(periode, up, medea, edat5, sexe)]
        res15 = resultat15[(periode, up, medea, edat5, sexe)]
        res16 = resultat16[(periode, up, medea, edat5, sexe)]
        res17 = resultat17[(periode, up, medea, edat5, sexe)]
        res18 = resultat18[(periode, up, medea, edat5, sexe)]
        res19 = resultat19[(periode, up, medea, edat5, sexe)]
        res20 = resultat20[(periode, up, medea, edat5, sexe)]
        res21 = resultat21[(periode, up, medea, edat5, sexe)]
        res22 = resultat22[(periode, up, medea, edat5, sexe)]
        res23 = resultat23[(periode, up, medea, edat5, sexe)]
        res24 = resultat24[(periode, up, medea, edat5, sexe)]
        res27  = resultat27[(periode, up, medea, edat5, sexe)]
        res28  = resultat28[(periode, up, medea, edat5, sexe)]
        res29  = resultat29[(periode, up, medea, edat5, sexe)]
        res30  = resultat30[(periode, up, medea, edat5, sexe)]
        res31  = resultat31[(periode, up, medea, edat5, sexe)]
        res32  = resultat32[(periode, up, medea, edat5, sexe)]
        res33  = resultat33[(periode, up, medea, edat5, sexe)]
        res34  = resultat34[(periode, up, medea, edat5, sexe)]
        res35  = resultat35[(periode, up, medea, edat5, sexe)]
        res36  = resultat36[(periode, up, medea, edat5, sexe)]
        res37  = resultat37[(periode, up, medea, edat5, sexe)]
        res38  = resultat38[(periode, up, medea, edat5, sexe)]
        res39  = resultat39[(periode, up, medea, edat5, sexe)]
        res40  = resultat40[(periode, up, medea, edat5, sexe)]
        res41  = resultat41[(periode, up, medea, edat5, sexe)]
        res42  = resultat42[(periode, up, medea, edat5, sexe)]
        res43  = resultat43[(periode, up, medea, edat5, sexe)]
        res44  = resultat44[(periode, up, medea, edat5, sexe)]
        res46  = resultat46[(periode, up, medea, edat5, sexe)]
        res47  = resultat47[(periode, up, medea, edat5, sexe)]
        res48  = resultat48[(periode, up, medea, edat5, sexe)]
        res49  = resultat49[(periode, up, medea, edat5, sexe)]
        res50  = resultat50[(periode, up, medea, edat5, sexe)]
        res51  = resultat51[(periode, up, medea, edat5, sexe)]
        res52  = resultat52[(periode, up, medea, edat5, sexe)]
        res53  = resultat53[(periode, up, medea, edat5, sexe)]
        res54  = resultat54[(periode, up, medea, edat5, sexe)]
        res55  = resultat55[(periode, up, medea, edat5, sexe)]
        res56  = resultat56[(periode, up, medea, edat5, sexe)]
        res57  = resultat57[(periode, up, medea, edat5, sexe)]
        res58  = resultat58[(periode, up, medea, edat5, sexe)]
        res59  = resultat59[(periode, up, medea, edat5, sexe)]
        res60  = resultat60[(periode, up, medea, edat5, sexe)]
        res61  = resultat61[(periode, up, medea, edat5, sexe)]
        res62  = resultat62[(periode, up, medea, edat5, sexe)]
        res63  = resultat63[(periode, up, medea, edat5, sexe)]
        res64  = resultat64[(periode, up, medea, edat5, sexe)]
        res65  = resultat65[(periode, up, medea, edat5, sexe)]
        res66  = resultat66[(periode, up, medea, edat5, sexe)]
        res67  = resultat67[(periode, up, medea, edat5, sexe)]
        res68  = resultat68[(periode, up, medea, edat5, sexe)]
        res69  = resultat69[(periode, up, medea, edat5, sexe)]
        res70  = resultat70[(periode, up, medea, edat5, sexe)]
        res71  = resultat71[(periode, up, medea, edat5, sexe)]
        res72  = resultat72[(periode, up, medea, edat5, sexe)]
        upload.append([periode, up, medea, edat5, sexe, res1, res2,res9,res27,res11,res12,res13,res14,res15,res16,res40,res41,res42,res43,res17,res18,res19,res20,res21,res22,res23,res24,res28,res29,res30,res31, res32, res33,res34,res35,res36,res37,res38,res39, res50, res51, res52, res53, res54, res55, res56, res57, res58, res59,res60,res61, res62,res63,res64,res65,res66,res44, res46, res47, res48, res49, res67, res68,res69,res70,res71,res72])
    file = tempFolder + 'projecte_OP_' + str(ane) + '.txt'
    writeCSV(file, upload, sep=';')

    