import sisapUtils as u
from datetime import timedelta
# import pandas as pd
from collections import defaultdict, Counter

class Main():
    def __init__(self):
        self.get_poblacio()
        self.get_obesos()
        self.sortida()

    def get_poblacio(self):
        sql = """
        select
            a.id_cip_sec,
            d.sector_censal,
            a.edat_a,
            a.sexe
        from
            nodrizas.ped_assignada a inner join
            import.md_poblacio_activa d
              on a.id_cip_sec = d.id_cip_sec
        where
            a.codi_sector in ('6837', '6839', '6838', '6844')
        """
        self.poblacio = {}
        self.contar_sujetos = Counter()
        for id, sector_censal, edat, sexe in u.getAll(sql, 'import'):
            self.poblacio[id] = (sector_censal, edat, sexe)
            self.contar_sujetos[(sector_censal, edat, sexe)] += 1
        print(len(self.poblacio))

    def get_obesos(self):
        self.obesos = Counter() 
        print('anem a buscar els nens amb obesitat') 
        # Total nens amb malalties (filtrar per 239)
        sql = """
                select
                    distinct id_cip_sec
                from
                    ped_problemes
                where
                    ps=239
                """
        for id, in u.getAll(sql, 'nodrizas'):
            if id in self.poblacio:
                self.obesos[self.poblacio[id]] += 1
        print(len(self.obesos))

    def sortida(self):
        print('anem a processar')
        self.upload = []
        for e in self.contar_sujetos:
            try:
                x = self.obesos[e]
            except:
                x = 0
            v = (e[0], e[1], e[2], self.contar_sujetos[e], x)
            self.upload.append(v)

        print('pengem resultats')
        columns = """
            (sector_censal varchar(15),
             edat int,
             sexe varchar(5),
             total int,
             obesos int)
        """
        u.createTable(
            'peticio_nens_obesos', 
             columns,
            'permanent',
             rm=True)
        u.listToTable(self.upload, 'peticio_nens_obesos', 'permanent')

if __name__ == "__main__":
    Main()
