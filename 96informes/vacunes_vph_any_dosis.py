# coding: iso-8859-1
from sisapUtils import *
import sys,datetime
from time import strftime
import random
from collections import defaultdict,Counter
from dateutil.relativedelta import relativedelta
import csv
import re


nod="nodrizas"
imp="import"
alt="altres"

cohorts=range(1994,2004)
years_dosis=range(2008,2019)

agr_vph=782

def get_up_ambit():
    """Asocia a cada up el ambito al que pertenece. Filtra por las up donde ep= '0208'.
       Devuelve un diccionario {up:amb_desc}
    """
    sql="select scs_codi,amb_desc from {}.cat_centres where ep = '0208'".format(nod)
    return {up:amb_desc for up,amb_desc in getAll(sql,nod)}



def get_dosis_id(table):
    dosis_id=defaultdict(set)
    sql="select id_cip_sec,va_u_dosi,va_u_data_vac from  {}, nodrizas.dextraccio where va_u_cod in ('VPH2-S','VPH2','VPH4','VPH4-S','P00123','P00125','P00126','P00193','P00122','P00234','P00124','P00185') and va_u_dosi in (1,2,3) \
        and va_u_data_alta <= data_ext and (va_u_data_baixa=0 or va_u_data_baixa > data_ext)".format(table)
    for id, dosis,dat in getAll(sql,imp):
        if dat.year in years_dosis:
            dosis_id[(id,dosis)].add(dat)
   
    return dosis_id

def get_rows(dosis_id,up_ambit):
    pob_ass,rows_dict=Counter(),defaultdict(lambda: defaultdict(Counter))
    sql='select id_cip_sec,up,data_naix,sexe,ates from {}.assignada_tot'.format(nod)
    for id,up,data_naix,sexe,ates in getAll(sql,nod):
        if data_naix.year in cohorts and up in up_ambit:
            pob_ass[(data_naix.year,sexe)]+=1
            for dosis,year_dos in dosis_id:
                rows_dict[(data_naix.year,sexe)][dosis][year_dos]+=0
                if id in dosis_id[(dosis,year_dos)]:
                    rows_dict[(data_naix.year,sexe)][dosis][year_dos]+=1
    return pob_ass,rows_dict

def export_txt_file(name,header,rows):
    with open(name,"wb") as out_file:
        print(rows[0])
        row_format ="{:>35}" * len(rows[0])+"\n"
        out_file.write(row_format.format(*header))
        for row in rows:
            out_file.write(row_format.format(*row))


if __name__ == '__main__':

    printTime('inici')
    up_ambit=get_up_ambit()
    printTime('up_ambit')


    dosis_id_total=defaultdict(set)
    for dosis_id in multiprocess(get_dosis_id,getSubTables('vacunes')):
        for key in dosis_id:
            dosis_id_total[key]|=dosis_id[key]
    printTime('dosis_id')
    dosis_id_year=defaultdict(set)
    for id, dosis in dosis_id_total:
        year_dos= min(dosis_id_total[(id,dosis)]).year
        dosis_id_year[(dosis,year_dos)].add(id)
        
        
    printTime('dosis_id_year')
    pob_ass,rows_dict=get_rows(dosis_id_year,up_ambit)
    printTime('rows_dict')

    """
    duplic=rows_dict[(2001,'D')][1][2012] & rows_dict[(2001,'D')][1][2013]
    print random.sample(duplic,2)
    #name='vacunats_vph_all.txt'
    print carmen
    """
    for dosis in (1,2,3):
        rows=[]
        name='vacunats_{}dosi_vph.txt'.format(dosis)

        rows= [ [year,sexe, pob_ass[(year,sexe)] ] + [ rows_dict[(year,sexe)][dosis][year_dos] for year_dos in years_dosis] for year,sexe in sorted(rows_dict)]

        header=['Any de naixement','Sexe','Pob assignada']+['Any {} dosi: {}'.format(dosis,year) for year in years_dosis]
        print header
        printTime('rows')
        export_txt_file(name,header,rows)
