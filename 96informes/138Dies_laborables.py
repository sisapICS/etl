# coding: utf8

debug = False

from sisapUtils import *
from collections import defaultdict,Counter
import datetime



imp = 'import'
nod = 'nodrizas'

inici = datetime.date(2016, 01, 01)
final = datetime.date(2029, 01, 01)

recomptes = Counter()
for single_date in dateRange(inici, final):
    mes = single_date.strftime('%m')
    anys = single_date.year
    periode = str(anys) + str(mes)
    work = 0
    if isWorkingDay(single_date):
        work = 1
    recomptes[(periode)] += work
    
    
upload = []
for (periode),r in recomptes.items():
    upload.append([periode, r])

file = tempFolder + 'Dies_laborables_2016_2017_2018.txt'
writeCSV(file, upload, sep=';')  
        