from sisapUtils import *

eqa = 'eqa_ind'
imp = 'import'
nod = 'nodrizas'


def get_hashos():
    hashos = {}
    sql = 'select id_cip_sec,codi_sector,hash_d from u11withjail'
    for id, sector, hash in getAll(sql, imp):
        hashos[hash, sector] = id
    return hashos
    
hashos = get_hashos() 
llistats = []
with open('hashos_tabac.txt', 'rb') as file:
    p=csv.reader(file, delimiter='@', quotechar='|')
    for ind in p:
        hash, sector, anys = ind[0],ind[1], ind[2]
        try:
            id = hashos[hash,sector]
        except KeyError:
            continue
        llistats.append([id, anys])
        
Table_excl = 'exclusions_tabac'
create = "(id_cip_sec int, anys int, index(id_cip_sec))"
createTable(Table_excl,create,eqa, rm=True)
listToTable(llistats, Table_excl, eqa)
   
def get_centres_Girona():
    up_Girona = {}
    sql = "select scs_codi from nodrizas.cat_centres where amb_codi='04'"
    for up, in getAll(sql, nod):
        up_Girona[(up)] = True
    return up_Girona
    
def get_excl():
    exclusions = {}
    sql = 'select id_cip_sec, anys from exclusions_tabac'
    for id, anys in getAll(sql, eqa):
        exclusions[id] = {'anys': anys}
    return exclusions
    
def get_poblacio(centres, exclusions):
    poblacio = {}
    pob_cess = {}
    any2015 = {}
    sql = 'select id_cip_sec, edat, up from assignada_tot where ates=1 and edat >14'
    for id, edat, up in getAll(sql, nod):
        if (up) in centres:
            poblacio[id] = {'fuma': 0, 'nofuma': 0, 'exfuma': 0, 'noinfo': 1}
            if 15<= int(edat) <= 79:
                if id in exclusions:
                    anys = exclusions[id]['anys']
                    if int(anys) == 2015:
                        any2015[id] = True
                else:
                    pob_cess[id] = True
    return poblacio, pob_cess, any2015


def get_fumadorsFa1any(pob_cess, any2015):
    fumadors = {}
    sql = 'select id_cip_sec from eqa_tabac,dextraccio where tab=1 and (date_add(data_ext,interval - 12 month) between dalta and dbaixa)'
    for id, in getAll(sql, nod):
        if id in pob_cess:
            fumadors[id] = {'cessa':  0}
    for id in any2015:
        fumadors[id] = {'cessa':  0}
                        
    return fumadors
    
    
def do_things(poblacio, fumadors):
    uploadEQA = []
    uploadTabac = []
    sql = 'select id_cip_sec, tab from eqa_tabac, dextraccio where data_ext between dalta and dbaixa'
    for id, tabac in getAll(sql, nod):
        tabac = int(tabac)
        if id in fumadors:
            if tabac == 2:
                fumadors[id]['cessa'] = 1
        if id in poblacio:
            poblacio[id]['noinfo'] = 0
            if tabac == 0:
                poblacio[id]['nofuma'] = 1
            elif tabac == 1:
                poblacio[id]['fuma'] = 1
            elif tabac == 2:
                poblacio[id]['exfuma'] = 1
    for (id), cessa in fumadors.items():
        uploadEQA.append([id, cessa['cessa']])
    for (id),valors in poblacio.items():
        uploadTabac.append([id, valors['fuma'],valors['nofuma'],valors['exfuma'],valors['noinfo']])
    listToTable(uploadEQA, TableEQA, eqa)
    listToTable(uploadTabac, TableTabac, eqa)
              
centres = get_centres_Girona()
exclusions = get_excl()
poblacio, pob_cess, any2015 = get_poblacio(centres, exclusions)
fumadors = get_fumadorsFa1any(pob_cess, any2015)

TableEQA = 'Gir_eqa0305'
create = "(id_cip_sec int, cessa int)"
createTable(TableEQA,create,eqa, rm=True)

TableTabac = 'Gir_tabac'
create = "(id_cip_sec int, fuma int, nofuma int, exfuma int, noinfo int)"
createTable(TableTabac,create,eqa, rm=True)

do_things(poblacio, fumadors)

