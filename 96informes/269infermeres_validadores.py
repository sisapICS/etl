
import sisapUtils as u 
import datetime as d


def dades():
    sql = """SELECT *
                FROM sisap_cat_ppftb126 a
                LEFT JOIN DBC_RUP b
                on b.up_cod = a.usu_cod_up
                where
                nvl(usu_data_baixa,sysdate)>=sysdate
                AND USU_NUM_COL LIKE '3%'
                AND instr(usu_perfils,'2')>'0'"""
    upload = []
    for row in u.getAll(sql, 'exadata'):
        upload.append(row)

    SMS_DIA = (d.datetime.now().date() - d.timedelta(days=3))
    file_eap = u.tempFolder + 'Infermeres_validadores_{}.csv'.format(SMS_DIA)
    u.writeCSV(file_eap, [('codi_sector', 'login', 'nom', 'cognom1', 'cognom2', 'email', 'tipus_doc', 'nif', 'perfil', 
                            'usu_cat', 'num_col', 'origen', 'up', 'cat_prof', 'desc_cat_prof',
                            'telf1', 'telf2', 'data_alta', 'data_modi', 'usuari_modi', 'data_baixa', 
                            'coment', 'flag_dni', 'seg_servidor', 'autonom', 'up', 'up_desc', 'es_ics',
                            'tip_cod', 'tip_des', 'subtipus_cod', 'subtipus_des', 'abs_cod',
                            'ab_des', 'sector_cod', 'sector_des', 'aga_cod', 'aga_des', 'ep_cod',
                            'ep_des', 'tit_cod', 'tit_des')] + upload, sep=";")

    # email
    subject = 'Seguiment Infermeres Validadores'
    text = "Bon dia.\n\nAdjuntem arxiu .csv amb les dades de seguiment quinzenals d'infermeres validadores.\n"
    text += "\n\nSalutacions."

    u.sendGeneral('SISAP <sisap@gencat.cat>',
                        ('farmacia.ics@gencat.cat',
                        'aponsmesquida@gencat.cat',
                        'cplanasc@catsalut.cat',
                        'nurianadal@gencat.cat',
                        'a.rios@gencat.cat',
                        'ambonet.ics@gencat.cat'),
                        ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat', 'sflayeh.bnm.ics@gencat.cat'),
                        subject,
                        text,
                        file_eap)

if __name__ == "__main__":
    try:
        dades()
    except Exception as e:
        e = str(e)
        subject = 'ERROR Seguiment Infermeres Validadores'
        text = e
        u.sendGeneral('SISAP <sisap@gencat.cat>',
                ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat', 'sflayeh.bnm.ics@gencat.cat'),
                ('cguiriguet.bnm.ics@gencat.cat', 'roser.cantenys@catsalut.cat', 'sflayeh.bnm.ics@gencat.cat'),
                subject,
                text,
                file_eap)
