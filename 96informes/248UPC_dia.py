# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta
from time import time,sleep

import sisapUtils as u



class upc_dia(object):
    """."""

    def __init__(self):
        """."""
        self.get_sivic()
        self.get_sequenciacio()
        # self.get_dades()
        # self.get_ucis()
        # self.get_medianes()
        # self.export_mail()
        
    def get_sivic(self):
        """."""
        self.upload_sivic = []
        sql = """SELECT 
                    DATA, diagnostic, sexe, edat, sum(diari) 
                FROM
                    dwsisap.SIVIC_ECAP_DIAGNOSTIC sed 
                WHERE 
                    diagnostic IN ('ILI','IT','Bronquiolitis', 'COVID-19', 'Grip', 'COVID exitus')
                GROUP BY
                    DATA, diagnostic, sexe, edat"""
        for dat, dx, sexe, edat, n in u.getAll(sql, 'exadata'):
            self.upload_sivic.append([dat, dx, sexe, edat, n])
        
        file = u.tempFolder + "dades_sivic.csv"
        u.writeCSV(file, self.upload_sivic, sep=';')
        text= "Us fem arribar el fitxer SIVIC actualitzat."
        u.sendPolite(['clara.prats@upc.edu', 'victor.lopez.de.rioja@upc.edu','aida.perramon@upc.edu','mario.bravo.masferrer@upc.edu',  'informescovidcat@gmail.com'],'Dades SIVIC',text,file)
    
    def get_sequenciacio(self):
        """."""
        self.upload_seq = []
        sql = """select 
                    data_recollida_mostra, variant_cataleg_grafic, CODI_RESULTAT_PROVA_TRANSF_ETL, count(*)
                from 
                    dwrsa.varco a 
                where 
                    codi_loinc = '94764-8' and 
                    variant_cataleg_grafic <> 'SECNOPOS' and 
                    data_recollida_mostra >= date '2021-01-18'
                GROUP BY 
                    data_recollida_mostra, variant_cataleg_grafic, CODI_RESULTAT_PROVA_TRANSF_ETL"""
        for dat, cataleg, varaint, n in u.getAll(sql, 'exadata'):
            self.upload_seq.append([dat, cataleg, varaint, n])
        
        file = u.tempFolder + "sequenciacio_sivic.csv"
        u.writeCSV(file, self.upload_seq, sep=';')
        text= "Us fem arribar el fitxer SIVIC - sequenciacio actualitzat."
        u.sendPolite(['clara.prats@upc.edu', 'victor.lopez.de.rioja@upc.edu',  'informescovidcat@gmail.com'],'Sequenciacio SIVIC',text,file)
    
    def get_dades(self):
        """."""
        self.upload = [] 
        sql = """ select data, residencia, grup, sum(casos_confirmat) casos, sum(casos_confirmat_only_tar),
            sum(ingressats_total) ingressats_total, sum(ingressats_critic), sum(ingressos_total),
            sum(ingressos_critic), sum(pcr), sum(tar), sum(pcr_no_cas_pos),sum(pcr_no_cas), sum(tar_no_cas_pos), sum(tar_no_cas), sum(poblacio),
            sum(casos_confirmat_only_tar_simpt), sum(tar_simpt_no_cas_pos), sum(tar_simpt_no_cas), sum(visites), sum(dx_covid), sum(dx_ili), sum(dx_grip)
            from preduffa.sisap_covid_web_master
            group by data, residencia, grup"""
        for dat, resi, grup, casos, casos_tar, ing_t, uci, ingt2, uci2,pcr, tar, pcr_no_cas_pos, pcr_no_cas, tar_no_cas_pos, tar_no_cas, pob, tarsimpt, tar_simpt_no_cas_pos, tar_simpt_no_cas, visites, covid_dx, ili_dx, grip_dx in u.getAll(sql, 'redics'):
            self.upload.append([dat, resi, grup, casos, casos_tar, ing_t, uci, ingt2, uci2,pcr, tar, pcr_no_cas_pos, pcr_no_cas, tar_no_cas_pos, tar_no_cas, pob, tarsimpt,
            tar_simpt_no_cas_pos, tar_simpt_no_cas, visites, covid_dx, ili_dx, grip_dx])

    def get_ucis(self):
        """declaracio uci"""
        self.ucis = []
        sql = """select dia, sum(declaracio) from preduffa.SISAP_COVID_WEB_CRITICS group by dia"""
        for dia, declara in u.getAll(sql, 'redics'):
            self.ucis.append([dia,declara])
            
    def get_medianes(self):
        """treure medianes de hx i uci"""
        self.medianes = []
        sql = """SELECT 
                    10*grup,
                    CASE WHEN IMMUNITZAT IS NULL THEN 0 ELSE immunitzat END AS immunitzat,  count(*), avg(dies), median(dies), PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY dies) AS p25,
                    PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY dies) AS p75 FROM (  
                    SELECT floor(floor(EXTRACT(DAY FROM SYSDATE - d.data_naixement) / 365.25) / 10) AS grup, 
                    CASE WHEN b.IMMUNITZAT_data + 14 <a.INGRES_PRIMER THEN 1 ELSE 0 END AS immunitzat, (a.INGRES_ULTIM - a.ingres_primer) + 1 AS dies  
                FROM 
                    dwsisap.rca_cip_nia d 
                LEFT join
                    dwsisap.DBC_METRIQUES a
                 ON
                    d.hash=a.hash
                 left JOIN 
                    dwsisap.DBC_VACUNA b 
                ON 
                    a.hash=b.hash 
                WHERE
                    a.ingres_ultim < sysdate - 2 
                    AND a.ingres_primer > sysdate - 90)
                GROUP  BY 
                    10*grup ,CASE WHEN IMMUNITZAT IS NULL THEN 0 ELSE immunitzat END"""
        for grup, imm, n, mitjana, mediana, p25, p75 in u.getAll(sql, 'exadata'):
            self.medianes.append(['HX',grup, imm, n, mitjana, mediana, p25, p75])
    
        sql = """SELECT 
                    10*grup,
                    CASE WHEN IMMUNITZAT IS NULL THEN 0 ELSE immunitzat END AS immunitzat,  count(*), avg(dies), median(dies), PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY dies) AS p25,
                    PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY dies) AS p75 FROM (  
                    SELECT floor(floor(EXTRACT(DAY FROM SYSDATE - d.data_naixement) / 365.25) / 10) AS grup, 
                    CASE WHEN b.IMMUNITZAT_data + 14 <a.INGRES_UCI_PRIMER THEN 1 ELSE 0 END AS immunitzat, (a.INGRES_UCI_ULTIM - a.ingres_UCI_primer) + 1 AS dies  
                    FROM dwsisap.rca_cip_nia d 
                    LEFT join
                    dwsisap.DBC_METRIQUES a
                    ON
                    d.hash=a.hash
                    left JOIN dwsisap.DBC_VACUNA b 
                    ON a.hash=b.hash 
                    WHERE
                    a.ingres_UCI_ultim < sysdate - 2 
                     AND a.ingres_UCI_primer >  sysdate - 90)
                    GROUP  BY 10*grup ,CASE WHEN IMMUNITZAT IS NULL THEN 0 ELSE immunitzat END"""
        for grup, imm, n, mitjana, mediana, p25, p75 in u.getAll(sql, 'exadata'):
            self.medianes.append(['UCI',grup, imm, n, mitjana, mediana, p25, p75])
            
    def export_mail(self):
        """Enviem fitxer"""
        
        file = u.tempFolder + "dades_covid.csv"
        u.writeCSV(file, self.upload, sep=';')
        text= "Us fem arribar el fitxer actualitzat."
        u.sendPolite(['clara.prats@upc.edu', 'victor.lopez.de.rioja@upc.edu','aida.perramon@upc.edu','mario.bravo.masferrer@upc.edu',  'informescovidcat@gmail.com'],'Dades covid',text,file)
        
        file = u.tempFolder + "UCIs.csv"
        u.writeCSV(file, self.ucis, sep=';')
        text= "Us fem arribar el fitxer actualitzat de critics."
        u.sendPolite(['clara.prats@upc.edu', 'victor.lopez.de.rioja@upc.edu','aida.perramon@upc.edu','mario.bravo.masferrer@upc.edu',  'informescovidcat@gmail.com'],'Dades covid UCI',text,file)
        
        file = u.tempFolder + "Medianes_estada.csv"
        u.writeCSV(file, self.medianes, sep=';')
        text= "Us fem arribar el fitxer actualitzat de medianes estada hospitalaria."
        u.sendPolite(['clara.prats@upc.edu', 'victor.lopez.de.rioja@upc.edu','aida.perramon@upc.edu','mario.bravo.masferrer@upc.edu', 'informescovidcat@gmail.com'],'Dades covid Medianes estada',text,file)
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    upc_dia()

    u.printTime("Final")