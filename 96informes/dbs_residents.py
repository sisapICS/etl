import sisapUtils as u2
import collections as c
import sisaptools as u


class DBS_resis():
    def __init__(self):
        self.dbs()
    
    def dbs(self):
        print('20')
        dbs = c.defaultdict(set)
        sql_2020 = """SELECT c_cip, c_up, c_data_naix, C_EDAT_ANYS, C_SEXE, C_NACIONALITAT, C_GMA_COMPLEXITAT, '' as C_GMA_PNIV,
                        C_VISITES_ANY, C_ULTIMA_VISITA_EAP, PS_DIABETIS2_DATA,
                        F_DIAB_ADO, PS_ATDOM_DATA, PR_MACA_DATA, PR_PCC_DATA,
                        V_RCV_VALOR, V_RCV_DATA, V_ALCOHOL_VALOR,
                        V_ALCOHOL_DATA, PS_ALCOHOL_DATA, V_HBA1C_VALOR,
                        V_HBA1C_DATA, V_IMC_VALOR, V_IMC_DATA, PS_OBESITAT_DATA,
                        V_TAS_VALOR, V_TAS_DATA, V_TAD_VALOR, V_TAD_DATA,
                        PS_HTA_DATA, V_COL_LDL_VALOR, V_COL_LDL_DATA,
                        V_COL_HDL_VALOR, V_COL_HDL_DATA, V_COL_TOTAL_VALOR,
                        V_COL_TOTAL_DATA, PS_DISLIPEMIA_DATA, V_TABAC_VALOR,
                        V_TABAC_DATA, VC_TABAC_ARA_VALOR, VC_TABAC_ARA_DATA
                        FROM dbs.dbs_2020 d 
                        WHERE
                        c_proveidor='0208'
                        AND C_INSTITUCIONALITZAT = ''
                        AND PS_DIABETIS2_DATA IS NOT NULL"""
        for row in u.Database("sidics", "dbs").get_all(sql_2020):
            dbs[2020].add(row)
        upload = []
        for year, rows in dbs.items():
            for row in rows:
                upload.append(tuple([year,])+ row)
        u2.listToTable(upload, 'dbs_acumulats_residents', ('backup', 'analisi'))
        dbs = c.defaultdict(set)
        print('21')
        sql_2021 = """SELECT c_cip, c_up, c_data_naix, C_EDAT_ANYS, C_SEXE, C_NACIONALITAT, C_GMA_COMPLEXITAT, '' as C_GMA_PNIV,
                        C_VISITES_ANY, C_ULTIMA_VISITA_EAP, PS_DIABETIS2_DATA,
                        F_DIAB_ADO, PS_ATDOM_DATA, PR_MACA_DATA, PR_PCC_DATA,
                        V_RCV_VALOR, V_RCV_DATA, V_ALCOHOL_VALOR,
                        V_ALCOHOL_DATA, PS_ALCOHOL_DATA, V_HBA1C_VALOR,
                        V_HBA1C_DATA, V_IMC_VALOR, V_IMC_DATA, PS_OBESITAT_DATA,
                        V_TAS_VALOR, V_TAS_DATA, V_TAD_VALOR, V_TAD_DATA,
                        PS_HTA_DATA, V_COL_LDL_VALOR, V_COL_LDL_DATA,
                        V_COL_HDL_VALOR, V_COL_HDL_DATA, V_COL_TOTAL_VALOR,
                        V_COL_TOTAL_DATA, PS_DISLIPEMIA_DATA, V_TABAC_VALOR,
                        V_TABAC_DATA, VC_TABAC_ARA_VALOR, VC_TABAC_ARA_DATA
                        FROM dbs.dbs_2021 d 
                        WHERE
                        c_proveidor='0208'
                        AND C_INSTITUCIONALITZAT = ''
                        AND PS_DIABETIS2_DATA IS NOT NULL"""
        for row in u.Database("sidics", "dbs").get_all(sql_2021):
            dbs[2021].add(row)
        upload = []
        for year, rows in dbs.items():
            for row in rows:
                upload.append(tuple([year,])+ row)
        u2.listToTable(upload, 'dbs_acumulats_residents', ('backup', 'analisi'))
        dbs = c.defaultdict(set)
        print('22')
        sql_2022 = """SELECT c_cip, c_up, c_data_naix, C_EDAT_ANYS, C_SEXE, C_NACIONALITAT, C_GMA_COMPLEXITAT, '' as C_GMA_PNIV,
                        C_VISITES_ANY, C_ULTIMA_VISITA_EAP, PS_DIABETIS2_DATA,
                        F_DIAB_ADO, PS_ATDOM_DATA, PR_MACA_DATA, PR_PCC_DATA,
                        V_RCV_VALOR, V_RCV_DATA, V_ALCOHOL_VALOR,
                        V_ALCOHOL_DATA, PS_ALCOHOL_DATA, V_HBA1C_VALOR,
                        V_HBA1C_DATA, V_IMC_VALOR, V_IMC_DATA, PS_OBESITAT_DATA,
                        V_TAS_VALOR, V_TAS_DATA, V_TAD_VALOR, V_TAD_DATA,
                        PS_HTA_DATA, V_COL_LDL_VALOR, V_COL_LDL_DATA,
                        V_COL_HDL_VALOR, V_COL_HDL_DATA, V_COL_TOTAL_VALOR,
                        V_COL_TOTAL_DATA, PS_DISLIPEMIA_DATA, V_TABAC_VALOR,
                        V_TABAC_DATA, VC_TABAC_ARA_VALOR, VC_TABAC_ARA_DATA
                        FROM dbs.dbs_2022 d 
                        WHERE
                        c_proveidor='0208'
                        AND C_INSTITUCIONALITZAT = ''
                        AND PS_DIABETIS2_DATA IS NOT NULL"""
        for row in u.Database("sidics", "dbs").get_all(sql_2022):
            dbs[2022].add(row)
        upload = []
        for year, rows in dbs.items():
            for row in rows:
                upload.append(tuple([year,])+ row)
        u2.listToTable(upload, 'dbs_acumulats_residents', ('backup', 'analisi'))
        print('23')
        dbs = c.defaultdict(set)
        sql_2023 = """SELECT c_cip, c_up, c_data_naix, C_EDAT_ANYS, C_SEXE, C_NACIONALITAT, C_GMA_COMPLEXITAT, C_GMA_PNIV,
                        C_VISITES_ANY, C_ULTIMA_VISITA_EAP, PS_DIABETIS2_DATA,
                        F_DIAB_ADO, PS_ATDOM_DATA, PR_MACA_DATA, PR_PCC_DATA,
                        V_RCV_VALOR, V_RCV_DATA, V_ALCOHOL_VALOR,
                        V_ALCOHOL_DATA, PS_ALCOHOL_DATA, V_HBA1C_VALOR,
                        V_HBA1C_DATA, V_IMC_VALOR, V_IMC_DATA, PS_OBESITAT_DATA,
                        V_TAS_VALOR, V_TAS_DATA, V_TAD_VALOR, V_TAD_DATA,
                        PS_HTA_DATA, V_COL_LDL_VALOR, V_COL_LDL_DATA,
                        V_COL_HDL_VALOR, V_COL_HDL_DATA, V_COL_TOTAL_VALOR,
                        V_COL_TOTAL_DATA, PS_DISLIPEMIA_DATA, V_TABAC_VALOR,
                        V_TABAC_DATA, VC_TABAC_ARA_VALOR, VC_TABAC_ARA_DATA
                        FROM dbs.dbs_2023 d 
                        WHERE
                        c_proveidor='0208'
                        AND C_INSTITUCIONALITZAT = ''
                        AND PS_DIABETIS2_DATA IS NOT NULL"""
        for row in u.Database("sidics", "dbs").get_all(sql_2023):
            dbs[2023].add(row)
        
        upload = []
        for year, rows in dbs.items():
            for row in rows:
                upload.append(tuple([year,])+ row)
        
        u2.listToTable(upload, 'dbs_acumulats_residents', ('backup', 'analisi'))
        print('24')
        dbs = c.defaultdict(set)
        sql_2024 = """SELECT c_cip, c_up, c_data_naix, C_EDAT_ANYS, C_SEXE, C_NACIONALITAT, C_GMA_COMPLEXITAT, C_GMA_PNIV,
                        C_VISITES_ANY, C_ULTIMA_VISITA_EAP, PS_DIABETIS2_DATA,
                        F_DIAB_ADO, PS_ATDOM_DATA, PR_MACA_DATA, PR_PCC_DATA,
                        V_RCV_VALOR, V_RCV_DATA, V_ALCOHOL_VALOR,
                        V_ALCOHOL_DATA, PS_ALCOHOL_DATA, V_HBA1C_VALOR,
                        V_HBA1C_DATA, V_IMC_VALOR, V_IMC_DATA, PS_OBESITAT_DATA,
                        V_TAS_VALOR, V_TAS_DATA, V_TAD_VALOR, V_TAD_DATA,
                        PS_HTA_DATA, V_COL_LDL_VALOR, V_COL_LDL_DATA,
                        V_COL_HDL_VALOR, V_COL_HDL_DATA, V_COL_TOTAL_VALOR,
                        V_COL_TOTAL_DATA, PS_DISLIPEMIA_DATA, V_TABAC_VALOR,
                        V_TABAC_DATA, VC_TABAC_ARA_VALOR, VC_TABAC_ARA_DATA
                        FROM dbs d 
                        WHERE
                        c_proveidor='0208'
                        AND C_INSTITUCIONALITZAT IS null
                        AND PS_DIABETIS2_DATA IS NOT NULL"""
        for row in u.Database("redics", "data").get_all(sql_2024):
            dbs[2024].add(row)
        
        upload = []
        for year, rows in dbs.items():
            for row in rows:
                upload.append(tuple([year,])+ row)
        
        u2.listToTable(upload, 'dbs_acumulats_residents', ('backup', 'analisi'))


if __name__ == "__main__":
    DBS_resis()