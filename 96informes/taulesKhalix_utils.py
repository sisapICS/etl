from sisapUtils import *

fitxers=[['EQPF_ICS_'],['EQPF_AMB_'],['EQPF_SAP_'],['EQPF_UP_'],['EQPF_IS_ICS_'],['EQPF_IS_AMB_'],['EQPF_IS_SAP_'],['EQPF_IS_UP_'],['EQPF_UP_ASSIR_'],
         ['VIS_ICS_'],['VIS_NOICS_'],['PRO_ICS_'],['PRO_NOICS_'],['GRUP_ICS_'],['GRUP_NOICS_'],
         ['SISAP_DESPESES_EAP_'],['SISAP_DESPESES_NOEAP_'],['SISAP_INGRESSOS_EAP_'],['SISAP_INGRESSOS_NOEAP_'],
         ['MADS_'],
         ['PI_QC_UP_EAP_'],['PI_QC_UP_EAP_F_'],['PI_QC_UP_NOEAP_'],['PI_QC_UP_NOEAP_F_'],
         ['LIQ_UP_GFA_'],
         ['LIQ_UP_GFA_'],
         ['REC_EAP_'],['REC_NEAP_'],['RECF_']
         ]

dimensions = [['NUM'],['DEN'],['AGRESULT'],['AGASSOL'],['AGNOCAT']]

period = [['01'],['02'],['03'],['04'],['05'],['06'],['07'],['08'],['09'],['10'],['11'],['12']]

def getKhxFitxer(fitxer):
    
    if fitxer.startswith("EQPF_"):
        return fitxer[:-10]
    elif fitxer.startswith("VIS_"):
        return fitxer[:-11]
    elif fitxer.startswith("PRO_"):
        return fitxer[:-11]
    elif fitxer.startswith("GRUP_"):
        return fitxer[:-11]
    elif fitxer.startswith("SISAP_DESPESES"):
        return fitxer[:-8]
    elif fitxer.startswith("SISAP_INGRESSOS"):
        return fitxer[:-8]
    elif fitxer.startswith("MADS"):
        return fitxer[:-11]
    elif fitxer.startswith("PI_QC_UP"):
        return fitxer[:-8]
    elif fitxer.startswith("LIQ_UP_GFA"):
        return fitxer[:-10]
    elif fitxer.startswith("REC"):
        return fitxer[:-8]
        
def getKhxdataany(fitxer):
    
    if fitxer.startswith("EQPF_"):
        return fitxer[-10:-6]
    elif fitxer.startswith("VIS_"):
        return fitxer[-8:-4]
    elif fitxer.startswith("PRO_"):
        return fitxer[-8:-4]
    elif fitxer.startswith("GRUP_"):
        return fitxer[-8:-4]
    elif fitxer.startswith("SISAP_DESPESES"):
        return fitxer[-8:-4]
    elif fitxer.startswith("SISAP_INGRESSOS"):
        return fitxer[-8:-4]
    elif fitxer.startswith("MADS"):
        return fitxer[-8:-4]
    elif fitxer.startswith("PI_QC_UP"):
        return fitxer[-8:-4]
    elif fitxer.startswith("LIQ_UP_GFA"):
        return fitxer[-10:-6]
    elif fitxer.startswith("REC"):
        return fitxer[-8:-4]
        
def getKhxmes(fitxer):
    
    if fitxer.startswith("EQPF_"):
        return fitxer[-6:-4]
    elif fitxer.startswith("VIS_"):
        return fitxer[-11:-9]
    elif fitxer.startswith("PRO_"):
        return fitxer[-11:-9]
    elif fitxer.startswith("GRUP_"):
        return fitxer[-11:-9]
    elif fitxer.startswith("MADS"):
        return fitxer[-11:-9]
    elif fitxer.startswith("LIQ_UP_GFA"):
        return fitxer[-6:-4]       
    else:
        return '12MES'

def getKhxTipDada(fitxer):

    if fitxer.startswith("EQPF_"):
        return 'EQPF'
    elif fitxer.startswith("VIS_"):
        return 'VISITES'
    elif fitxer.startswith("PRO_"):
        return 'VISITES PROCEDIMENTS'
    elif fitxer.startswith("GRUP_"):
        return 'VISITES GRUPALS'
    elif fitxer.startswith("SISAP_DESPESES"):
        return 'ECONOMIC'
    elif fitxer.startswith("SISAP_INGRESSOS"):
        return 'ECONOMIC'
    elif fitxer.startswith("MADS"):
        return 'MADS'
    elif fitxer.startswith("PI_QC_UP"):
        return 'PROVES'
    elif fitxer.startswith("LIQ_UP_GFA"):
        return 'DESPESES FARMA'
    elif fitxer.startswith("REC_"):
        return 'RECEPTES'
    elif fitxer.startswith("RECF_"):
        return 'RECEPFACT'
        
def getKhxUP(fitxer):

    if fitxer.startswith("MADS_"):
        return 1
    elif fitxer.startswith("PI_QC_UP"):
        return 1
    elif fitxer.startswith("REC_"):
        return 1
    else:
        return 0
    
def getUpFromAbs(fitxer,TipusDada):

    if TipusDada == 'MADS':
        return True
    else:
        return False

def getDescAbs(fitxer,TipusDada):

     if TipusDada == 'MADS':
        return 0

def CalculUP(fitxer,TipusDada):

    if fitxer.startswith("EQPF_IS_UP") or fitxer.startswith("EQPF_UP"):
        return True
    elif TipusDada.startswith("VISITES"):
        return True
    elif TipusDada.startswith("ECONOM"):
        return True
    elif TipusDada.startswith("MADS"):
        return True
    elif TipusDada.startswith("PROVES"):
        return True
    elif TipusDada.startswith("DESPESES"):
        return True
    elif TipusDada.startswith("RECEPTES"):
        return True
    elif TipusDada.startswith("RECEPFACT"):
        return True
    else:
        return False
        
def getCompte(fitxer,TipusDada):

    if fitxer.startswith("EQPF_IS_"):
        return "FARMIND"
    elif fitxer.startswith("EQPF_"):
        return 1
    elif TipusDada.startswith("VISITES"):
        return 1
    elif TipusDada.startswith("ECONOM"):
        return 1
    elif TipusDada.startswith("MADS"):
        return 5
    elif TipusDada.startswith("PROVES"):
        return 0
    elif TipusDada.startswith("DESPESES"):
        return 1
    elif TipusDada.startswith("RECEPTES"):
        return 0
    elif TipusDada.startswith("RECEPFACT"):
        return 1
    else:
        return "Sense Compte"
        
def getTipus(fitxer,TipusDada):

    if fitxer.startswith("VIS_"):
        return 2
    elif fitxer.startswith("PI_QC_UP"):
        return 2
    elif fitxer.startswith("MADS"):
        return 10
    else:
        return "NOCAT"
        
def getDetalls(fitxer,TipusDada):

    return "NOIMP"
    
def getControls(fitxer,TipusDada):

    return "DIM6SET"
    
def CalcAnalisi(fitxer,TipusDada):

    if TipusDada == "EQPF" or TipusDada.startswith("DESPESES"):
        return True
    else:
        return False
    
def getAnalisiClassic(fitxer,TipusDada):

    if fitxer.startswith("VIS_"):
        return 3
    elif fitxer.startswith("GRUP_"):
        return 2
    elif fitxer.startswith("SISAP_DESPESES"):
        return 'AGNOCAT'
    elif fitxer.startswith("SISAP_INGRESSOS"):
        return 'AGIMPORTE'
    elif fitxer.startswith("MADS"):
        return 'AGRESULT'
    elif fitxer.startswith("PI_QC_UP"):
        return 3
    elif fitxer.startswith("REC_"):
        return 2
    else:
        return "NOCLI"
        

def getCalcValor(fitxer,dimensio): 

    if fitxer.startswith("EQPF_IS_"):
        if dimensio == "AGASSOL":
            return True
        else:
            return False
    elif fitxer.startswith("EQPF_UP_ASSIR_"):
        if dimensio == "AGASSOL" or dimensio == "AGNOCAT":
            return False
        else:
            return True
    elif fitxer.startswith("EQPF_"):
        if dimensio == "AGASSOL" or dimensio == "AGRESULT":
            return True
        else:
            return False
    elif fitxer.startswith("LIQ_UP_GFA"):
        if dimensio == "AGNOCAT" or dimensio == "NUM":
            return True
        else:
            return False
    

def getNeedSum(fitxer,TipusDada):

    if TipusDada.startswith("VISITES"):
        return True
    else:
        return False            

def getValor(fitxer,dimensio): 

    if fitxer.startswith("EQPF_IS_"):
        if dimensio == "AGASSOL":
            return 1
    elif fitxer.startswith("EQPF_UP_ASSIR_"):
        if dimensio == "NUM":
            return 2
        elif dimensio == "DEN":
            return 3
        elif dimensio == "AGRESULT":
            return 4
    elif fitxer.startswith("EQPF_"):
        if dimensio == "AGASSOL": 
            return 3
        elif dimensio == "AGRESULT":
            return 2
    elif fitxer.startswith("LIQ_UP_GFA"):
        if dimensio == "NUM":
            return 3
        elif dimensio == "AGNOCAT":
            return 2
       
def getValorclassic(fitxer,TipusDada):

    if fitxer.startswith("VIS_"):
        return 4
    elif fitxer.startswith("PRO_"):
        return 2
    elif fitxer.startswith("GRUP_"):
        return 3
    elif fitxer.startswith("MADS"):
        return 7
        
def getValor12m(fitxer,TipusDada,mesos):

    if TipusDada.startswith("ECONOM"):
        if mesos == '01':
            return 2
        if mesos == '02':
            return 3
        if mesos == '03':
            return 4
        if mesos == '04':
            return 5
        if mesos == '05':
            return 6
        if mesos == '06':
            return 7
        if mesos == '07':
            return 8
        if mesos == '08':
            return 9
        if mesos == '09':
            return 10
        if mesos == '10':
            return 11
        if mesos == '11':
            return 12
        if mesos == '12':
            return 13
    elif TipusDada.startswith("PROVES"):
        if mesos == '01':
            return 4
        if mesos == '02':
            return 5
        if mesos == '03':
            return 6
        if mesos == '04':
            return 7
        if mesos == '05':
            return 8
        if mesos == '06':
            return 9
        if mesos == '07':
            return 10
        if mesos == '08':
            return 11
        if mesos == '09':
            return 12
        if mesos == '10':
            return 13
        if mesos == '11':
            return 14
        if mesos == '12':
            return 15
    elif TipusDada.startswith("RECEPTES"):
        if mesos == '01':
            return 3
        if mesos == '02':
            return 4
        if mesos == '03':
            return 5
        if mesos == '04':
            return 6
        if mesos == '05':
            return 7
        if mesos == '06':
            return 8
        if mesos == '07':
            return 9
        if mesos == '08':
            return 10
        if mesos == '09':
            return 11
        if mesos == '10':
            return 12
        if mesos == '11':
            return 13
        if mesos == '12':
            return 14
    elif TipusDada.startswith("RECEPFACT"):
        if mesos == '01':
            return 2
        if mesos == '02':
            return 3
        if mesos == '03':
            return 4
        if mesos == '04':
            return 5
        if mesos == '05':
            return 6
        if mesos == '06':
            return 7
        if mesos == '07':
            return 8
        if mesos == '08':
            return 9
        if mesos == '09':
            return 10
        if mesos == '10':
            return 11
        if mesos == '11':
            return 12
        if mesos == '12':
            return 13
            
def getInKhalix(file,TipusDada): 

    if file.startswith("EQPF_UP_20"):
        return 0
    elif file.startswith("EQPF_IS_UP_"):
        return 0    
    else:
        return 1
        
def getInKhalixExt(file,TipusDada): 

    if file.startswith("LIQ_UP_GFA"):
        return 0
    else:
        return 1

def getFolder(TipusDada):

    if TipusDada == "EQPF" or TipusDada.startswith("DESPESES"):
        return 'Farmacia_Originals'
    elif TipusDada.startswith("VISITES"):
        return 'Visites_Originals'
    elif TipusDada.startswith("ECONOM") or TipusDada.startswith("PROVES"):
        return 'Economic_Originals'
    elif TipusDada == "MADS":
        return 'MADS_Originals'
    elif TipusDada == "RECEPTES" or TipusDada == "RECEPFACT":
        return 'Receptes_Originals'
    else:
        return 'No_folder'

def getName12m(TipusDada):

    if TipusDada == "ECONOMIC" or TipusDada == "PROVES" or TipusDada == "RECEPTES" or TipusDada == "RECEPFACT":
        return True
    else:
        return False
        
def getNameFile(TipusDada):

    if TipusDada == "EQPF":
        return "EQPF_ALL_UP_"
    elif TipusDada == "VISITES":
        return "VISITES_"
    elif TipusDada == "VISITES PROCEDIMENTS":
        return "PROCEDIMENTS_"
    elif TipusDada == "VISITES GRUPALS":
        return "GRUPALS_"
    elif TipusDada == "ECONOMIC":
        return "ECONOMIC_ALL_UP_"
    elif TipusDada == "MADS":
        return "QUALITAT_"
    elif TipusDada == "PROVES":
        return "PROVES_ALL_UP_"
    elif TipusDada == "DESPESES FARMA":
        return "DESPESES_FARMA_ALL_UP_"
    elif TipusDada == "RECEPTES":
        return "RECEPTES_ALL_UP_"
    elif TipusDada == "RECEPFACT":
        return "RECEPTESF_ALL_UP_"
    else:
        return "NO NAME"
   