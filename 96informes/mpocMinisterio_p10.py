# coding: utf-8

"""
 - TIME EXECUTION 26m

 - PETICIÓ:
    - Trello: https://trello.com/c/MUrKZ6BU
    - Drive:

"""

import sisapUtils as u
from datetime import datetime


class mpocMinisterio(object):
    """ . """

    def __init__(self):
        """ . """
        self.get_poblacio()
        self.get_problemes()
        self.get_activitats()
        self.get_denominador()
        self.get_numerador()

    def get_poblacio(self):
        """ . """
        print("------------------------------------------------- get_poblacio")
        self.poblacio = {}
        sql = """select
                    id_cip_sec
                    , sexe
                 from
                    assignada_tot
                 where
                    ep = '0208'"""
        for id_cip_sec, _sexe in u.getAll(sql, 'nodrizas'):
            self.poblacio[id_cip_sec] = True
        print("N pacients POBLACIO:", len(self.poblacio))

    def get_problemes(self):
        """ . """
        print("------------------------------------------------ get_problemes")
        self.mpoc = set()
        self.mpoc_modgra = set()
        sql = """
            select
                id_cip_sec
                , pr_gra
            from
                eqa_problemes
            where
                ps = 62
            """
        for id_cip_sec, gra in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.poblacio:
                self.mpoc.add(id_cip_sec)
                if gra > 1:
                    self.mpoc_modgra.add(id_cip_sec)
        print("N pacients MPOC:", len(self.mpoc))
        print("N pacients MPOC - MOD/GRAVE:", len(self.mpoc_modgra))

    def get_activitats(self):
        """ . """
        print("----------------------------------------------- get_activitats")
        self.activitats = {}
        self.evamulmpoc = set()
        sql = """select
                    id_cip_sec
                    , au_cod_ac
                    , au_val
                    , au_dat_act
                 from
                    activitats
                 where
                    au_cod_ac in ('VR300')
                    and au_val != ''
              """
        for id_cip_sec, cod, _val, _dat in u.getAll(sql, 'import'):
            if id_cip_sec in self.poblacio:
                if id_cip_sec in self.mpoc:
                    if cod in ('VR300'):
                        self.evamulmpoc.add(id_cip_sec)
        print("N pacients EVAL MULT MPOC:", len(self.evamulmpoc))

    def get_denominador(self):
        """ . """
        print("---------------------------------------------- get_denominador")
        self.den = set()
        for id_cip in self.mpoc_modgra:
            self.den.add(id_cip)
        print("N pacients DENOMINADOR:", len(self.den))

    def get_numerador(self):
        """ . """
        print("------------------------------------------------ get_numerador")
        self.num = set()
        for id_cip in self.den:
            if id_cip in self.evamulmpoc:
                self.num.add(id_cip)
        print("N pacients NUMERADOR:", len(self.num))


if __name__ == '__main__':
    print(datetime.now())
    ts = datetime.now()
    mpocMinisterio()
    print('Time execution {}'.format(datetime.now() - ts))
