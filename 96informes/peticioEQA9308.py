import sisapUtils as u
import datetime as d
import sisaptools as t
import collections as c

u.printTime('inici')
sql = """
    SELECT id_cip_sec, up
    FROM assignada_tot
    WHERE ates = 1
"""

pob = {id: up for id, up in u.getAll(sql, 'nodrizas')}
u.printTime('pob')
print(len(pob))
sql = """
    SELECT id_cip_sec
    FROM eqa_problemes
    WHERE ps = 86
"""
demencia = set()
for id, in u.getAll(sql, 'nodrizas'):
    if id in pob:
        demencia.add(id)
u.printTime('demencia')
print(len(demencia))
sql = """
    SELECT id_cip_sec, valor
    FROM eqa_variables
    WHERE agrupador = 1079
    AND usar = 1
"""
barthel = {}
for id, val in u.getAll(sql, 'nodrizas'):
    if id in pob:
        barthel[id] = val
u.printTime('barthel')
print(len(barthel))
sql = """
    SELECT id_cip_sec, gma_pniv
    FROM gma
"""
gma = {}
for id, val in u.getAll(sql, 'import'):
    if id in pob:
        gma[id] = val
u.printTime('gma')
print(len(gma))
res = c.Counter()
for id in pob:
    up = pob[id]
    if id in barthel:
        valb = barthel[id]
        valg = gma[id] if id in gma else None
        if valb < 20 or (21 <= valb <= 60 and id in demencia) or (21 <= valb <= 60 and id not in demencia and valg and valg == 4):
            res[(up, 'alta')] += 1
        if (21 <= valb <= 60 and id not in demencia and valg and valg < 4) or (valb > 60 and id in demencia) or (valb > 60 and id not in demencia and valg and valg == 4):
            res[(up, 'mitja')] += 1
        if valb > 60 and id not in demencia and valg and valg < 4:
            res[(up, 'baixa')] += 1
print(len(res))
u.printTime('cuinetes')
upload = []
for (up, ind), val in res.items():
    upload.append((up, ind, val))

cols = "(up varchar(10), ind varchar(5), val int)"
u.createTable('peticio_eqa9308', cols, 'altres', rm=True)
u.listToTable(upload, 'peticio_eqa9308', 'altres')
u.printTime('fi')