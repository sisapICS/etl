# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

debug = False

imp = 'import'
nod = 'nodrizas'


dd = ['F10', 'F11', 'F12', 'F13', 'F14', 'F15', 'F16', 'F17', 'F18', 'F19']

cim10Desc = {}

sql = 'select ps_cod, ps_des from cat_prstb001'
for cod, des in getAll(sql, imp):
        cim10Desc[cod] = des
        
thesaurusDesc = {}

sql = 'select pst_th, pst_des_norm from cat_prstb305'
for cod, des in getAll(sql, imp):
        thesaurusDesc[cod] = des

totals = Counter()
prevalenca = Counter()
incidencia = Counter()

sql = "select id_cip_sec, pr_cod_ps, pr_th, date_format(pr_dde,'%Y%m%d'), pr_dde, date_format(pr_dba, '%Y%m%d')\
        ,if(pr_dde > date_add(data_ext, interval -1 year), 1, 0) \
        ,if(pr_dba > 0 and pr_dba <= data_ext, 1, 0) \
        from problemes, nodrizas.dextraccio \
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa is null or pr_data_baixa > data_ext) and pr_cod_ps like 'F%'{}".format(' limit 10' if debug else '')
for id, cim10, th, dde, pr_dde, dba, incident, tancat in getAll(sql, imp):
    try:
        descCim10 = cim10Desc[cim10]
    except KeyError:
        continue
    try:
        descTh = thesaurusDesc[th]
    except KeyError:
        descTh= None
    tipus = 'SM'
    if cim10[0:3] in dd:
        tipus = 'DD'
    totals[(cim10, th, descCim10, descTh, 'SM')] += 0
    totals[(cim10, th, descCim10, descTh, 'DD')] += 0
    if incident == 1:
        incidencia[(cim10, th, descCim10, descTh, tipus)] += 1
    if tancat == 0:
        prevalenca[(cim10, th, descCim10, descTh, tipus)] += 1

    
upload = []
   
for (cim10, th, descCim10, descTh, tipus), val in totals.items():
    inc = incidencia[(cim10, th, descCim10, descTh, tipus)]
    prev = prevalenca[(cim10, th, descCim10, descTh, tipus)]
    upload.append([cim10, th, descCim10, descTh, tipus, inc, prev])

file = tempFolder + 'DX_Salut_Mental.txt'
writeCSV(file, upload, sep=';')