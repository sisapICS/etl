import sisapUtils as u


def multi_worker(params):
    sector, sql = params
    print(sector)
    upload = []
    for row in u.getAll(sql, sector):
        upload.append(row)
    u.listToTable(upload, 'tires_global', ('exadata'))


def Tires():
    cols = """(cip varchar(14),
                fit_cod number,
                Fit_cont_rec_s number,
                fit_per_rec_s number,
                fit_data_alta date,
                fit_dat_baixa date,
                fit_tip_diab varchar2(35),
                fit_porta_bomba varchar2(35),
                fit_mcg varchar2(35),
                fit_dispensacio_domicili varchar2(35),
                fit_glucometro varchar2(200),
                fit_cod_glucometro varchar2(35))"""
    u.createTable('tires_global', cols, ('exadata'), rm=True)
    u.grantSelect('tires_global', 'DWSISAP_ROL', 'exadata')
    print('done')
    sql = """SELECT
                FIT_CIP,
                FIT_COD,
                Fit_cont_rec_s,
                fit_per_rec_s,
                fit_data_alta,
                fit_dat_baixa,
                fit_tip_diab,
                fit_porta_bomba,
                fit_mcg,
                fit_dispensacio_domicili,
                fit_glucometro,
                fit_cod_glucometro
            FROM
                ppftb300"""
    jobs = [(sector, sql) for sector in u.sectors]
    u.multiprocess(multi_worker, jobs, 4, close=True)


if __name__ == "__main__":
    Tires()