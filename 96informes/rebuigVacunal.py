# coding: utf8

"""

Rebuig de Vacunes

- Trello: https://trello.com/c/VCKRrW2M

Sorry, hay que mejorar mucho el script, está hecho a lo "bruto" por la urgencia
 de la petición.
Revisar este script para utilizar sólo los antígenos: 247ASP_vacunes_DM_VIH.py

"""

# import urllib as w
# import os
# import sys
import datetime as d
from datetime import datetime
# from time import strftime
from collections import Counter

import sisapUtils as u


class RebuigVacunes(object):
    """ . """

    def __init__(self):
        """ . """

        self.get_centres()
        self.get_poblacio()
        self.get_vacunes()
        self.get_rebuig()
        self.get_resultats()
        self.export_upload()

    def get_centres(self):
        """ . """
        self.centres = {}
        sql = """select
                    scs_codi, ics_codi, ics_desc, right(rs, 2), amb_desc, ep
                from cat_centres"""
        for up, br, _desc, _rs, _ambit, _ep in u.getAll(sql, 'nodrizas'):
            self.centres[up] = br

    def get_poblacio(self):
        """ . """
        self.poblacio = {}
        sql = """select id_cip_sec, edat, sexe, year(data_naix), up
                from assignada_tot where ates=1"""
        for id, edat, _sexe, any_naix, up in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                self.poblacio[id] = {'year': any_naix, 'edat': edat}
        print(len(self.poblacio))

    def get_vacunes(self):
        """ . """
        self.vacunes = {}
        sql = """select
                    id_cip_sec, va_u_data_vac, va_u_cod, va_u_dosi
                from
                    vacunes
                where
                    va_u_data_baixa = 0"""
        for id, dat, vac, dosi in u.getAll(sql, 'import'):
            if id in self.poblacio:
                any_naix = self.poblacio[id]['year']
                edat = self.poblacio[id]['edat']
                if vac in ('P00145', 'P-G-AR', 'P-G-NE', 'P-G-AD', 'P-G-60',
                           'P00144'):
                    if edat >= 65:
                        if dat >= d.date(2021, 1, 10) and dat <= d.date(2022, 3, 31):  # noqa
                            vacpet = 'GRIP'
                            any_naix_grip = '2021-2022'
                            self.vacunes[(id, any_naix_grip, vacpet)] = dosi
                if vac in ('DTP5M', 'DTP4M', 'DTPW', 'DTP5M', 'DTP4M', 'DTPW',
                           'TD-2', 'TD-1', 'TDP-E', 'TDP', 'TDP-A', 'TDP-E',
                           'TDP', 'TDP-A', 'HP AD', 'HEPA+B', 'HB-DI',
                           'HB-4M', 'HEP-B', 'H A+B', 'HEPTB', 'HIB-NS',
                           'HIB-5', 'HIB-3', 'HIB-4', 'HIB-2', 'HIB',
                           'DT-1', 'DT', 'P00008', 'P00009', 'P00018',
                           'P00004', 'P00202', 'P00205', 'P00206', 'P00207',
                           'P00208', 'P00209', 'P00230', 'P00001', 'P00007',
                           'P00216', 'P00229', 'P00074', 'P00132', 'P00151',
                           'P00197', 'P00217', 'P00003', 'P00073', 'P00196',
                           'P00198', 'P00175', 'P00176', 'P00177', 'P00178',
                           'P00120', 'P00138', 'P00117', 'P00118', 'P00119',
                           'P00127', 'P00128', 'P00002', 'P00076', 'P00210',
                           'P00121', 'P00129', 'P00135', 'P00218', 'P00005',
                           'P00075', 'P00116', 'P00130', 'P00137', 'P00159',
                           'P00041', 'P00204'):
                    vacpet = 'HEXAVALENT'
                    if any_naix in (2019, 2020):
                        if dosi <= 3:
                            self.vacunes[(id, any_naix, vacpet)] = dosi
                if vac in ('XRP-4', 'XRP-3', 'XRP-2', 'XRP-1',
                           'XARAMP', 'RUBEOL', 'PARODI',
                           'P00239', 'P00238', 'P00237', 'P00096', 'P00095',
                           'P00094', 'P00093', 'P00092', 'P00091', 'P00090',
                           'P00089', 'P00088', 'P00087', 'P00086', 'P00085',
                           'P00084', 'P00083', 'P00082'):
                    vacpet = 'TRIPLE VIRICA'
                    if any_naix in (2019, 2020):
                        if dosi == 1:
                            self.vacunes[(id, any_naix, vacpet)] = dosi
                if vac in ('VPH4-S', 'VPH4', 'VPH2-S', 'VPH2',
                           'P00236', 'P00235', 'P00234', 'P00233', 'P00232',
                           'P00231', 'P00193', 'P00185', 'P00126', 'P00125',
                           'P00124', 'P00123', 'P00122'):
                    vacpet = 'VPH'
                    if any_naix in (2008, 2009):
                        self.vacunes[(id, any_naix, vacpet)] = dosi
        print(len(self.vacunes))

    def get_rebuig(self):
        """ . """
        self.rebuig = {}
        sql = """select
                    id_cip_sec, ief_data_alta, ief_cod_v, ief_exc, ief_mot_novac
                from
                    exclusions2
                where
                    ief_inc_exc='E'
                    and (ief_exc in (2,3) or ief_mot_novac = '591000119102')
                    and ief_data_baixa = 0"""
        for id, dat, vac, motiu_cod, motiu_nou in u.getAll(sql, 'import'):
            if id in self.poblacio:
                motiu = ""
                if motiu_cod == 2:
                    motiu = 'C01-Z28.1'
                if motiu_cod == 3:
                    motiu = 'C01-Z28.2'
                if motiu_nou == '591000119102':
                    motiu = 'C01-Z28.21'
                any_naix = self.poblacio[id]['year']
                edat = self.poblacio[id]['edat']
                if vac in ('P-G-AR', 'P00144'):
                    if dat >= d.date(2021, 1, 10) and dat <= d.date(2022, 3, 31):  # noqa
                        if edat >= 65:
                            vacpet = 'GRIP'
                            any_naix_grip = '2021-2022'
                            self.rebuig[(id, any_naix_grip, vacpet)] = motiu
                if vac in ('P00001', 'P00229', 'P00018', 'P00116', 'P00073'):
                    if any_naix in (2019, 2020):
                        vacpet = 'HEXAVALENT'
                        self.rebuig[(id, any_naix, vacpet)] = motiu
                if vac in ('P00082', 'P00087', 'P00092'):
                    if any_naix in (2019, 2020):
                        vacpet = 'TRIPLE VIRICA'
                        self.rebuig[(id, any_naix, vacpet)] = motiu
                if vac in ('P00122', 'P00125', 'P00231'):
                    if any_naix in (2008, 2009):
                        vacpet = 'VPH'
                        self.rebuig[(id, any_naix, vacpet)] = motiu
        print(len(self.rebuig))

    # def get_resultats(self):
    #     """ . """
    #     self.resultats = Counter()
    #     for id in self.poblacio:
    #         any_naix = self.poblacio[id]['year']
    #         for vacpet in ('GRIP', 'HEXAVALENT', 'TRIPLE VIRICA', 'VPH'):
    #             if vacpet in ('GRIP'):
    #                 edat = self.poblacio[id]['edat']
    #                 if edat >= 65:
    #                     any_naix_grip = '2021-2022'
    #                     self.resultats[(any_naix_grip, vacpet, 'den')] += 1
    #                     if (id, vacpet) in self.rebuig:
    #                       self.resultats[(any_naix_grip, vacpet, 'num')] += 1  # noqa
    #             if vacpet in ('HEXAVALENT'):
    #                 if any_naix in (2019,2020):
    #                     self.resultats[(any_naix, vacpet, 'den')] += 1
    #                     if (id, vacpet) in self.rebuig:
    #                         self.resultats[(any_naix, vacpet, 'num')] += 1
    #             if vacpet in ('TRIPLE VIRICA'):
    #                 if any_naix in (2019,2020):
    #                     self.resultats[(any_naix, vacpet, 'den')] += 1
    #                     if (id, vacpet) in self.rebuig:
    #                         self.resultats[(any_naix, vacpet, 'num')] += 1
    #             if vacpet in ('VPH'):
    #                 if any_naix in (2008,2009):
    #                     self.resultats[(any_naix, vacpet, 'den')] += 1
    #                     if (id, vacpet) in self.rebuig:
    #                         self.resultats[(any_naix, vacpet, 'num')] += 1
    #     print(self.resultats)

    def get_resultats(self):
        """ . """
        self.resultats = Counter()
        for _id, any_naix, vacpet in self.vacunes:
            self.resultats[(any_naix, vacpet, 'den')] += 1
        for _id, any_naix, vacpet in self.rebuig:
            self.resultats[(any_naix, vacpet, 'num')] += 1
        print(self.resultats)

    def export_upload(self):
        """ . """
        upload = []
        for (any_naix, vacpet, tip), n in self.resultats.items():
            if tip == 'den':
                num = self.resultats[(any_naix, vacpet, 'num')]
                upload.append([any_naix, vacpet, num, n])
        u.writeCSV(u.tempFolder + "RebuigVacunes.csv",
                   [('ANY', 'VACUNA', 'N_REBUIG', 'N_VACUNATS')] + upload)


if __name__ == '__main__':
    u.printTime("Inici")
    RebuigVacunes()
    u.printTime("Fi")
