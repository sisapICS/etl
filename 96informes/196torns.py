# coding: latin1

"""
Torns de visita.
"""

import sisapUtils as u


TABLE = "torns"
DATABASE = "test"
DATES = (u.gcal2jd(2019, 1, 14), u.gcal2jd(2019, 1, 18))


def main():
    """."""
    u.createTable(TABLE, "(ambit varchar(255), sap varchar(255), \
                           up varchar(5), eap varchar(255), \
                           uba varchar(5), uba_d varchar(255), mati int, \
                           tarda int, lliscant int)", DATABASE, rm=True)
    # Sector("6837")
    u.multiprocess(Sector, u.sectors)


class Sector(object):
    """."""

    def __init__(self, sector):
        """."""
        self.sector = sector
        self.get_moduls()
        self.get_centres()
        self.get_ubas()
        self.get_visites()
        self.get_dies()
        self.get_resultat()
        self.upload_data()

    def get_moduls(self):
        """."""
        sql = "select modu_centre_codi_centre, modu_centre_classe_centre, \
                      modu_servei_codi_servei, modu_codi_modul, \
                      modu_codi_up, modu_codi_uab \
               from vistb027 \
               where modu_servei_codi_servei = 'MG' and \
                     modu_codi_uab is not null"
        self.moduls = {row[:4]: row[4:] for row in u.getAll(sql, self.sector)}

    def get_centres(self):
        """."""
        sql = "select scs_codi, amb_desc, sap_desc, ics_desc \
               from cat_centres \
               where ep = '0208'"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_ubas(self):
        """."""
        sql = "select uab_codi_up, uab_codi_uab, uab_descripcio from vistb039"
        self.ubas = {row[:2]: row[2] for row in u.getAll(sql, self.sector)}

    def get_visites(self):
        """."""
        self.visites = {}
        torns = {8: "M", 9: "M", 10: "M", 11: "M", 12: "M", 13: "M", 14: "M",
                 15: "T", 16: "T", 17: "T", 18: "T", 19: "T", 20: "T"}
        sql = "select visi_centre_codi_centre, visi_centre_classe_centre, \
                      visi_servei_codi_servei, visi_modul_codi_modul, \
                      visi_data_visita, trunc(visi_hora_visita / 3600) \
               from vistb043 \
               where visi_data_visita between {} and {} and \
                     visi_servei_codi_servei = 'MG' and \
                     visi_forcada_s_n <> 'S'".format(*DATES)
        for row in u.getAll(sql, self.sector):
            modul = row[:4]
            dia, hora = row[4:]
            torn = torns.get(hora, "N")
            if modul in self.moduls:
                up, uba = self.moduls[modul]
                if up in self.centres:
                    id = (up, uba, dia)
                    if id not in self.visites:
                        self.visites[id] = {"M": 0, "T": 0, "N": 0}
                    self.visites[id][torn] += 1

    def get_dies(self):
        """."""
        self.dies = {}
        for (up, uba, dia), dades in self.visites.items():
            mati = dades["M"]
            tarda = dades["T"]
            nit = dades["N"]
            totes = float(mati + tarda + nit)
            if mati / totes >= 0.7:
                torn_dia = "M"
            elif tarda / totes >= 0.7:
                torn_dia = "T"
            else:
                torn_dia = "L"
            id = (up, uba)
            if id not in self.dies:
                self.dies[id] = {"M": 0, "T": 0, "L": 0}
            self.dies[id][torn_dia] += 1

    def get_resultat(self):
        """."""
        self.upload = []
        for (up, uba), dades in self.dies.items():
            mati = dades["M"]
            tarda = dades["T"]
            lliscant = dades["L"]
            amb, sap, eap = self.centres[up]
            uba_d = self.ubas[(up, uba)]
            this = (amb, sap, up, eap, uba, uba_d, mati, tarda, lliscant)
            self.upload.append(this)

    def upload_data(self):
        """."""
        u.listToTable(self.upload, TABLE, DATABASE)


if __name__ == "__main__":
    main()
