import sisapUtils as u
import collections as c


class PxM_freq():
    def __init__(self):
        self.get_data()
        self.get_most_freq()
        self.upload()
    
    def get_data(self):
        self.data = c.defaultdict(c.Counter)
        sql = """SELECT UP, MOTIU_PRIOR FROM dwsisap.sisap_master_visites
                WHERE DATA >= DATE '2022-10-08'
                AND situacio = 'R'
                AND UP IN (SELECT UP_COD FROM DWSISAP.DBC_CENTRES
                WHERE SAP_COD = '23')
                AND FLAG_MOTIU_PRIOR = 1"""
        for up, motiu in u.getAll(sql, 'exadata'):
            for m in motiu.split(";"):
                if m != '':
                    self.data[up][m] += 1
    
    def get_most_freq(self):
        self.most_common = c.defaultdict(list)
        for up in self.data:
            for i, (k,v) in enumerate(self.data[up].most_common(10)):
                self.most_common[up].append((k, v, i+1))
    
    def upload(self):
        cols = '(up varchar(5), motiu varchar(500), value int, ranking int)'
        u.createTable('pxm_mes_usats', cols, 'permanent', rm=True)
        result = []
        for up in self.most_common:
            for (k, v, i) in self.most_common[up]:
                result.append((up, k, v, i))
        u.listToTable(list(result), 'pxm_mes_usats', 'permanent')

if __name__ == "__main__":
    PxM_freq()
