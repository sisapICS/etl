import sisapUtils as u


def get_variables(particio):
    sql = """select
                id_cip,
                vu_dat_act,
                case
                    when VU_COD_VS in ('EP2004', 'EP2007') then 'AUDIT-C'
                    when VU_COD_VS in ('EP2005', 'EP2008') then 'AUDIT 10'
                    when VU_COD_VS = 'EP2009' then 'ALCOHOL'
                    when VU_COD_VS = 'VP2003' then 'ASSIST ALCOHOL'
                    when VU_COD_VS = 'VP2705' then 'ASSIST TABAC'
                    when VU_COD_VS = 'VP2201' then 'ASSIST CANNABIS'
                    when VU_COD_VS = 'VP2401' then 'ASSIST COCAINA'
                    when VU_COD_VS = 'VP2501' then 'ASSIST ESTIMULANTS'
                    when VU_COD_VS = 'VP2801' then 'ASSIST INHALANTS'
                    when VU_COD_VS = 'VP2301' then 'ASSIST SEDANTS'
                    when VU_COD_VS = 'VP2601' then 'ASSIST ALUCINOGENS'
                    when VU_COD_VS = 'VP2101' then 'ASSIST OPIOIDES'
                end VARIABLE,
                VU_VAL,
                case
                    when vu_cod_vs = 'EP2009' and VU_VAL = 1 then 1
                    when vu_cod_vs in ('EP2008', 'EP2005') and VU_VAL >= 13 then 1
                    when vu_cod_vs in ('EP2008', 'EP2005') and VU_VAL >= 20 then 2
                    when vu_cod_vs in ('EP2007', 'EP2004') and VU_VAL >= 4 then 1
                    when VU_COD_VS = 'VP2003' and 365 < datediff(date '2023-12-31', vu_dat_act) then 1
                    when vu_cod_vs = 'VP2705' and VU_VAL >= 26 then 2
                    when vu_cod_vs = 'VP2705' and VU_VAL > 4 then 1
                    when vu_cod_vs = 'VP2003' and VU_VAL >= 26 then 2
                    when vu_cod_vs = 'VP2003' and VU_VAL > 10 then 1
                    when vu_cod_vs = 'VP2201' and VU_VAL >= 26 then 2
                    when vu_cod_vs = 'VP2201' and VU_VAL > 4 then 1
                    when vu_cod_vs = 'VP2401' and VU_VAL >= 26 then 2
                    when vu_cod_vs = 'VP2401' and VU_VAL > 4 then 1
                    when vu_cod_vs = 'VP2501' and VU_VAL >= 26 then 2
                    when vu_cod_vs = 'VP2501' and VU_VAL > 4 then 1
                    when vu_cod_vs = 'VP2801' and VU_VAL >= 26 then 2
                    when vu_cod_vs = 'VP2801' and VU_VAL > 4 then 1
                    when vu_cod_vs = 'VP2301' and VU_VAL >= 26 then 2
                    when vu_cod_vs = 'VP2301' and VU_VAL > 4 then 1
                    when vu_cod_vs = 'VP2601' and VU_VAL >= 26 then 2
                    when vu_cod_vs = 'VP2601' and VU_VAL > 4 then 1
                    when vu_cod_vs = 'VP2101' and VU_VAL >= 26 then 2
                    when vu_cod_vs = 'VP2101' and VU_VAL > 4 then 1
                    else 0
                end POS
            from
                {particio}
            where
                vu_cod_vs in ('EP2004', 'EP2005', 'EP2007',
                                    'EP2008', 'EP2009', 'VP2705',
                                    'VP2003', 'VP2201', 'VP2401', 'VP2501',
                                    'VP2801', 'VP2301', 'VP2601', 'VP2101')
            UNION
            select
                id_cip,
                vu_dat_act,
                case
                    when VU_COD_VS = 'VP2003' then 'ASSIST ALCOHOL LY'
                end VARIABLE,
                VU_VAL,
                case
                    when VU_COD_VS = 'VP2003' and 365 < datediff(date '2023-12-31', vu_dat_act) then 1 else 0
                end POS
            from
                {particio}
            where
                vu_cod_vs = 'VP2003'
                and 365 < datediff(date '2023-12-31', vu_dat_act)
            """
    return(list(u.getAll(sql.format(particio=particio), "import")))

if __name__ == "__main__":
    particions = []
    for particio in u.getSubTables("variables"):
        sql = "select vu_dat_act from {} limit 1".format(particio)
        if u.getOne(sql, "import")[0].year > 2017:
            particions.append(particio)
    variables = u.multiprocess(get_variables, particions, 16)
    upload = []
    for chunk in variables:
        upload.extend(chunk)
    cols = "(id int, dat date, var varchar(16), val double, pos int)"
    u.createTable("embaras_audit_valors_raw", cols, "permanent", rm=True)
    u.listToTable(upload, "embaras_audit_valors_raw", "permanent")
    sqls = (
        "create or replace table embaras_audit_embarassos as \
             select cast(id_cip as integer) id, year(emb_d_fi) periode, \
                    adddate(emb_d_fi, interval - emb_durada week) inici, \
                    emb_d_fi fi, emb_up up, \
                    row_number() over (partition by id_cip_sec, year(emb_d_fi) \
                                       order by emb_d_fi) ordre \
             from import.embaras \
             where emb_c_tanca in ('P', 'C') and \
                   emb_durada between 35 and 42 and \
                   emb_d_fi between 20190101 and 20231231",
            "create or replace table embaras_audit_valors as \
             select id, dat, var, max(pos) val \
             from embaras_audit_valors_raw a \
             where id in (select id from embaras_audit_embarassos) \
             group by id, dat, var",
            "alter table embaras_audit_embarassos add index(id, inici, fi)",
            "alter table embaras_audit_valors add index(id, dat)", 
            "create or replace table embaras_audit_master as \
             select a.id, a.periode, a.up, \
                    sum(if(var = 'AUDIT-C', 1, 0)) audit_c_registres, \
                    max(if(var = 'AUDIT-C', val, 0)) audit_c_positiu, \
                    sum(if(var = 'AUDIT 10', 1, 0)) audit_registres, \
                    max(if(var = 'AUDIT 10', val, 0)) audit_positiu, \
                    sum(if(var = 'ALCOHOL', 1, 0)) alcohol_registres, \
                    max(if(var = 'ALCOHOL', val, 0)) alcohol_positiu, \
                    sum(if(var = 'ASSIST ALCOHOL', 1, 0)) assist_alcohol_registres, \
                    max(if(var = 'ASSIST ALCOHOL', val, 0)) assist_alcohol_positiu, \
                    sum(if(var = 'ASSIST ALUCINOGE', 1, 0)) assist_alucinoge_registres, \
                    max(if(var = 'ASSIST ALUCINOGE', val, 0)) assist_alucinoge_positiu, \
                    sum(if(var = 'ASSIST CANNABIS', 1, 0)) assist_cannabis_registres, \
                    max(if(var = 'ASSIST CANNABIS', val, 0)) assist_cannabis_positiu, \
                    sum(if(var = 'ASSIST COCAINA', 1, 0)) assist_cocaina_registres, \
                    max(if(var = 'ASSIST COCAINA', val, 0)) assist_cocaina_positiu, \
                    sum(if(var = 'ASSIST ESTIMULAN', 1, 0)) assist_estimulant_registres, \
                    max(if(var = 'ASSIST ESTIMULAN', val, 0)) assist_estimulant_positiu, \
                    sum(if(var = 'ASSIST INHALANTS', 1, 0)) assist_inhalants_registres, \
                    max(if(var = 'ASSIST INHALANTS', val, 0)) assist_inhalants_positiu, \
                    sum(if(var = 'ASSIST OPIOIDES', 1, 0)) assist_opioides_registres, \
                    max(if(var = 'ASSIST OPIOIDES', val, 0)) assist_opioides_positiu, \
                    sum(if(var = 'ASSIST SEDANTS', 1, 0)) assist_sedants_registres, \
                    max(if(var = 'ASSIST SEDANTS', val, 0)) assist_sedants_positiu, \
                    sum(if(var = 'ASSIST TABAC', 1, 0)) assist_tabac_registres, \
                    max(if(var = 'ASSIST TABAC', val, 0)) assist_tabac_positiu, \
                    sum(if(var = 'ASSIST ALCOHOL L', 1, 0)) assist_alcohol_l_registres, \
                    max(if(var = 'ASSIST ALCOHOL L', val, 0)) assist_alcohol_l_positiu \
             from embaras_audit_embarassos a \
             left join embaras_audit_valors b \
                  on a.id = b.id and b.dat between a.inici and a.fi \
             where ordre = 1 \
             group by a.id, a.periode, a.up",
            "create or replace table embaras_audit_indicadors as \
             select periode, up, count(1) embarassos, \
             round(100 * avg(if(audit_c_registres = 1, 1, 0)), 1) indicador_1,\
             round(100 * avg(if(audit_c_positiu = 1, 1, 0)), 1) indicador_2, \
             round(100 * avg(if(audit_c_registres = 2, 1, 0)), 1) indicador_3,\
             round(100 * avg(if(audit_c_registres > 2, 1, 0)), 1) indicador_4,\
             round(100 * avg(if(audit_registres = 1, 1, 0)), 1) indicador_5, \
             round(100 * avg(if(audit_positiu = 1, 1, 0)), 1) indicador_6, \
             round(100 * avg(if(audit_positiu = 2, 1, 0)), 1) indicador_7, \
             round(100 * avg(if(audit_registres = 2, 1, 0)), 1) indicador_8, \
             round(100 * avg(if(audit_registres > 2, 1, 0)), 1) indicador_9, \
             round(100 * avg(if(assist_alcohol_l_registres + assist_alcohol_registres + \
             assist_alucinoge_registres + assist_cannabis_registres + assist_cocaina_registres \
             + assist_estimulant_registres + assist_inhalants_registres + assist_opioides_registres \
             + assist_sedants_registres + assist_tabac_registres > 0, 1, 0))) indicador_10, \
             round(100 * avg(if(assist_alcohol_l_registres >= 1, 1, 0)), 1) indicador_11, \
             round(100 * avg(if(assist_tabac_positiu = 1, 1, 0)), 1) indicador_12, \
             round(100 * avg(if(assist_alcohol_positiu = 1, 1, 0)), 1) indicador_13, \
             round(100 * avg(if(assist_cannabis_positiu = 1, 1, 0)), 1) indicador_14, \
             round(100 * avg(if(assist_cocaina_positiu = 1, 1, 0)), 1) indicador_15, \
             round(100 * avg(if(assist_estimulant_positiu = 1, 1, 0)), 1) indicador_16, \
             round(100 * avg(if(assist_inhalants_positiu = 1, 1, 0)), 1) indicador_17, \
             round(100 * avg(if(assist_sedants_positiu = 1, 1, 0)), 1) indicador_18, \
             round(100 * avg(if(assist_alucinoge_positiu = 1, 1, 0)), 1) indicador_19, \
             round(100 * avg(if(assist_opioides_positiu = 1, 1, 0)), 1) indicador_20, \
             round(100 * avg(if(assist_tabac_positiu = 2, 1, 0)), 1) indicador_21, \
             round(100 * avg(if(assist_alcohol_positiu = 2, 1, 0)), 1) indicador_22, \
             round(100 * avg(if(assist_cannabis_positiu = 2, 1, 0)), 1) indicador_23, \
             round(100 * avg(if(assist_cocaina_positiu = 2, 1, 0)), 1) indicador_24, \
             round(100 * avg(if(assist_estimulant_positiu = 2, 1, 0)), 1) indicador_25, \
             round(100 * avg(if(assist_inhalants_positiu = 2, 1, 0)), 1) indicador_26, \
             round(100 * avg(if(assist_sedants_positiu = 2, 1, 0)), 1) indicador_27, \
             round(100 * avg(if(assist_alucinoge_positiu = 2, 1, 0)), 1) indicador_28, \
             round(100 * avg(if(assist_opioides_positiu = 2, 1, 0)), 1) indicador_29, \
             from embaras_audit_master \
             group by periode, up \
             order by periode, up")
    for sql in sqls:
        u.execute(sql, "permanent")



