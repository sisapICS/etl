# coding: latin1

import sisapUtils as u

def age_converter(edat):
    if edat < 30: return 'MENOR DE 30'
    else: return 'MAJOR DE 30'

age = {}
sql = """select id_cip_sec, edat from nodrizas.assignada_tot at2
            where sexe = 'D'"""
for id, edat in u.getAll(sql, 'nodrizas'):
    age[id] = edat

descriptiu = {
    'PAC1058': 'Cap',
    'PAC1059': 'Preservatiu mascul�',
    'PAC1060': 'Preservatiu femen�',
    'PAC1061': 'Anticonceptius orals combinats',
    'PAC1062': 'Anticonceptius orals de gest�gens',
    'PAC1063': 'Implants',
    'PAC1064': 'Anells vaginals',
    'PAC1065': 'Pegats',
    'PAC1066': 'DIU',
    'PAC1067': 'DIU amb levonorgestrel',
    'PAC1068': 'Diafragma',
    'PAC1069': 'Espermicides',
    'PAC1070': 'Ogino',
    'PAC1071': 'Billings',
    'PAC1072': 'Simptot�rmic',
    'PAC1073': 'Coitus interruptus',
    'PAC1074': 'Esterilitzaci� tub�rica',
    'PAC1075': 'Vasectomia',
    'PAC1076': 'Altres',
    'PAPI017': 'Contracepci�',
    'PACP007': 'M�tode contraceptiu escollit'
}

sql ="""select id_cip_sec, val_var, val_data from import.assir216
        where val_var in (
        'PAC1058',
        'PAC1059',
        'PAC1060',
        'PAC1061',
        'PAC1062',
        'PAC1063',
        'PAC1064',
        'PAC1065',
        'PAC1066',
        'PAC1067',
        'PAC1068',
        'PAC1069',
        'PAC1070',
        'PAC1071',
        'PAC1072',
        'PAC1073',
        'PAC1074',
        'PAC1075',
        'PAC1076')
        and val_hist = 1
        and val_val = 1"""
upload = []
for id, val, data in u.getAll(sql, 'import'):
    if id in age:
        edat = age[id]
        grup_edat = age_converter(edat)
        upload.append((id, edat, grup_edat, val, descriptiu[val], '', data))


sql = """select id_cip_sec, val_var, val_val, val_data from import.assir216
        where val_var in (
        'PAPI017',
        'PACP007') 
        and val_hist = 1"""
for id, val, comentari, data in u.getAll(sql, 'import'):
    if id in age:
        edat = age[id]
        grup_edat = age_converter(edat)
        upload.append((id, edat, grup_edat, val, descriptiu[val], comentari, data))

cols = """(id varchar(20), edat int, grup_edat varchar(20), variable varchar(10), descripcio varchar(50), comentari varchar(50), data date)"""
u.createTable('anticoncepcio', cols, 'test', rm=True)
u.listToTable(upload, 'anticoncepcio', 'test')