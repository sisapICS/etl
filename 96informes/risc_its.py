# coding: iso-8859-1
from sisapUtils import *
import random
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

nod="nodrizas"
imp="import"

## Lo siento!!, esta peticion no se porque se me complico bastante, para la tonteria que es!! Espero haberlo explicado bien, por si
## alguna vez teneis que adaptarlo, aunque yo lo haria de nuevo xD

#Codigos de las variables
codi_its_risc_var='EA6001'
codi_barrera_var='EA6002'
codi_consell_act='CA6001'
codi_its_risc_act='CA601'

#Diccionario para traducir que significa cada una de las variables en el archivo output
codis_dict_table= { 
    ('CA601', 'EA6001') :"Risc ITS/VIH i/o Conducta sexual de risc ITS/VIH (CA601, EA6001)",  
    'EA6002': "Utilització mètodes de barrera (EA6002)"	,
    "CA6001": 'Consells ITS (CA6001)',
    'CA601':"Risc ITS/VIH (CA601)",
    "EA6001":"Conducta sexual de risc en ITS (EA6001)"
}

#Años en los que se calcula el proceso
periodos=(2016,2017,2018)

##Diccionarios que indican que tablas se han de mirar y que codigos se han de calcular:
#Para generar el archivo risc_its_2_3_4.txt
codi_table= {
    "activitats": (codi_its_risc_act,), 
    "variables":(codi_barrera_var,codi_its_risc_var,codi_consell_act)
}

#Para generar el archivo risc_its_1.txt
codi_table_1= {
    "activitats": (codi_its_risc_act,), 
    "variables":(codi_its_risc_var,)
}

#variables numerador del indicador
var_num_ind=('CA601', 'EA6001')

#Diccionario para calcular el numerador del indicador. Las keys es un tuple, donde cada elemento responde al valor de las variables de var_num_ind. 
#El valor es 1 si la combinacion de valores de las variables se considera risc its y 0 si no se considera risc its.
risc_val= {
    (2,"null"):1, 
    (2,1):1, 
    ("null",1):1, 
    (1,"null"):0,
    (1,0):0,
    ("null",0):0
}

#Diccionario para saber que asignada coger y que campos y db segun el any que se quiera
assignada= { 
    2016:(imp,"assignadahistorica","and dataany= 2016"),
    2017:(imp,"assignadahistorica","and dataany= 2017"),
    2018:(nod,"assignada_tot",'')
}


class Risc_Its(object):
    def __init__(self,year,table_codi,codis,amb):
        """ Objeto para calcular datos sobre actividades o variables relacionados con el risc its. Recive los siguientes argumentos:
            year: int, el any en el que se requieren los datos
            table_codi: str, la tabla en la que se buscan los datos (activitats o variables)
            codis: Los codigos de las variables a buscar
            amb: bool, si los datos se tienen que agrupar a nivel de amb o no.

            Ademas, tiene los siguientes atributos:
            up_ambit: dict, asocia a cada up, la descripcion del ambito al que pertenece
            table_field: dict, asocia a cada tabla, los campos que se van a usar en las querys de sql

        """
        self.amb=amb
        self.year=year
        self.table_codi=table_codi
        self.codis=codis
        self.up_ambit=self.get_up_ambit()
        self.table_field= {"activitats":("au_cod_ac","au"), "variables":("vu_cod_vs","vu")}
        self.tables=self.get_tables_year()
        self.amb_total=None
        self.n_reg_total=None
        self.id_cod_valor=None
        
    
    def get_up_ambit(self):
        """Asigna a cada up la descripcion del ambito al que pertenece.
           Devuelve un dict {up: amb_desc}
        """
        sql="select scs_codi,amb_desc from {}.cat_centres".format(nod)
        return {up:amb_desc for up,amb_desc in getAll(sql,nod)}

    def get_tables_year(self):
        """ Consigue las tablas del any in self.year
            Para ello selecciona una fecha de visita de cada una de las particiones y comprueba que el any de esta fecha sea el any deseado.
            Si es asi, se anyade a una lista.
            Devuelve una lista de tablas.
        """
        tables = []
        for table in getSubTables(self.table_codi):
            dat = [dat for dat, in getAll("select {}u_dat_act from {} limit 1".format(self.table_codi[0],table), imp)][0]
            #dat, = getOne("select visi_data_visita from {} limit 1".format(table), 'import')
            if dat.year == self.year:
                tables.append(table)
        printTime("Tables")
        return tables

    def get_id_cod_valor(self,table):
        """Selecciona para cada id en la tabla el codigo y el valor de la acti/var realizada.
           Si amb es False, se crea una key con la up de registro y el codigo, sino se guarda como key solo el codigo, ya que el amb
           lo queremos de asignacion del paciente.
           El valor de esta key sera un diccionario del formato {id: (fecha act/var, valor de la act/var)}. Si ya se le ha añadido un valor al id,
           y la fecha del valor es mas reciente que la que esta guardada, se actualiza el diccionario. Es decir, para cada id SOLO SE LE GUARDA EL 
           VALOR DE ACTIVIDAD/VARIABLE MAS RECIENTE
           Devuelve un diccionario { key: {id : (dat, valor) }}
        """
        codi_field,up_field=self.table_field[self.table_codi]
        id_cod_valor=defaultdict(dict)

        sql="select id_cip_sec,{0}_up,{0}_val,{0}_dat_act,{2} from {1} where {2} in ({3})".format(up_field,table,codi_field,','.join(["'{}'".format(codi) for codi in self.codis]))
        for id,up_reg,val,dat,cod in getAll(sql,imp):
            #if id in id_edat:
            if id in id_naix and 15 <= self.year - id_naix[id].year <= 64: 
                key=(up_reg,cod)
                if self.amb:
                    key=cod
                if id in id_cod_valor[key]:
                    if dat > id_cod_valor[key][id][0]:
                        id_cod_valor[key][id]=(dat,val)
                else:
                    id_cod_valor[key][id]=(dat,val)
        return id_cod_valor


    def get_multiprocess_total_2(self):
        """ Consigue las filas para la tabla final. Para ello, para cada una de las tablas obtiene n_reg, que es un diccionario { key: {id : (dat, valor) }}
            y se van juntando en n_reg_total. Si un mismo id se encuentra en mas de una tabla, nos quedamos con el que tiene la fecha mas reciente de act/var.
            Devuelve un diccionario { key: {id : (dat, valor) }}
        """
        n_reg_total=defaultdict(dict)
        #for n_reg in multiprocess(self.get_id_cod_valor, self.tables):
        for table in self.tables:
            n_reg=self.get_id_cod_valor(table)
            for key in n_reg:
                for id in n_reg[key]:
                    if id in n_reg_total[key]: 
                        if n_reg[key][id][0] > n_reg_total[key][id][0]:
                            n_reg_total[key][id] = n_reg[key][id]
                    else:
                        n_reg_total[key][id] = n_reg[key][id]

        printTime("n reg done {}".format(len(n_reg_total)))
        self.n_reg_total=n_reg_total

    def get_ids_per_amb(self):
        """ Agrupa por ambito de asignacion segun el year. Busca en la tabla assignada correspondiente, cada paciente y su up de asignacion.
            Si la up tiene un ambito desc asociado, itera por cada uno de los codigos en n_reg_total y si el id ha hecho alguna act/var asociada a ese
            codigo, se anyade a un set de ids.
            Asigna al atributo amb_total un diccionario {amb:{cod: set(ids)}}
        """
        sql="select up, id_cip_sec from {}.{} where ates=1 {}".format(*assignada[self.year])
       
        amb_cod_id=defaultdict(lambda: defaultdict(set))
        for up,id in getAll(sql,nod):
            if up in self.up_ambit:
                amb=self.up_ambit[up] 
                for cod in self.n_reg_total:
                    if id in self.n_reg_total[cod]:
                        amb_cod_id[amb][cod].add(id)
        printTime("Amb cod id",len(amb_cod_id))
        self.amb_total=amb_cod_id



    def get_ids_per_up(self):
        """ Agrupa n_reg_total por up de registro de al act/var. 
            Asigna al atributo amb_total un diccionario {up: {cod: {id: (dat,valor)}}}
        """
        up_cod_id=defaultdict(lambda: defaultdict(dict))
        
        for up,cod in self.n_reg_total:
            for id in self.n_reg_total[(up,cod)]:
                up_cod_id[up][cod][id]  = self.n_reg_total[(up,cod)][id]
        printTime("Up cod id",len(up_cod_id))
        self.amb_total=up_cod_id



def get_key_tuple(ind_codi,id):
    """ Obtiene el valor de ind_codi[id] (el valor de la var/act en cuestion). Sino tiene valor o el valor es '', val pasa a ser null. 
        En el caso contrario, es el valor guardado para el id es el que se utiliza.
        Devuelve "val" que puede ser 'null' o un integer
    """
    val="null"
    if id in ind_codi:
        if ind_codi[id][1] != '':
            val=int(ind_codi[id][1])
    return  val

def get_indicador(codis,indicador_n_reg,year,up_ambit):
    """ Obtiene los dos indicadores agrupados por la up de asignacion del paciente, agrupando por ambito.
        Los datos de las var/act se obtienen de indicador_n_reg, un dict con {cod: {id: (dat,val)}}
        Devuelve dos dict de la siguiente estructura : {amb: {"NUM":n,"DEN":n}}
    """
    codi_1,codi_2=codis
    sql="select up, id_cip_sec from {}.{} where ates=1 {}".format(*assignada[year])  
    indicador_1=defaultdict(Counter)
    indicador_2=defaultdict(Counter)
    for up,id in getAll(sql,imp):
        if up in up_ambit:
            if id in id_naix and 15 <= year - id_naix[id].year <= 64: 
                amb=up_ambit[up]
                indicador_1[amb]["DEN"]+=1
                if id in indicador_n_reg[codi_1] or id in indicador_n_reg[codi_2]:
                    indicador_1[amb]["NUM"]+=1
                    indicador_2[amb]["DEN"]+=1
                    key_tuple=(get_key_tuple(indicador_n_reg[codi_1],id),get_key_tuple(indicador_n_reg[codi_2],id))
                    if key_tuple in risc_val and risc_val[key_tuple]:
                        indicador_2[amb]["NUM"]+=1
    return indicador_1,indicador_2

def get_dades(codi_table,amb_field,ind=False):
    """Saca los datos requeridos por codi_table ( de donde se tiene las tablas en las que hay que buscar los datos asociadas a los codigos que se requieren) 
       y agrupa por ambito si amb_field es True, sino lo hace por up. Si ind es True, calcula los indicador para las variables en var_num_din.
       Para sacar los datos, itera sobre los periodos y sobre los elementos en codi_table. Con estos datos crea un objeto risc its en el que se introduce 
       la tabla (key de codi_table), los codigos de las variables (valor en codi_table) y el amb_field. Si este es True, se llama al metodo get_ids_per_amb y si
       es False, el gets_ids_per_up. Si ind=True, se
       Devuelve tres estrucuturas:
       -> el objeto risc_its
       -> data_cod_year_amb: {amb: {cod: {year: n}}}
       -> indicador_year : {nombre_ind: {year: {up: {"NUM":n,"DEN":n}}}}
    """
    tree=lambda: defaultdict(tree)
    indicador_year=defaultdict(dict)
    data_cod_year_amb=tree()
    for year in periodos:
        indicador_n_reg=defaultdict(dict)
        printTime(year)
        for table_codi,codis in codi_table.iteritems():
            risc_its=Risc_Its(year,table_codi,codis,amb=amb_field)
            risc_its.get_multiprocess_total_2()
            if amb_field:
                risc_its.get_ids_per_amb()
            else:
                risc_its.get_ids_per_up()
            #una vez obtenidos todos los datos, ya sea a nivel de amb o up, estos se guardan en el atributo amb_total del objeto risc_its,
            #donde estan agrupados por amb/up y codigo (cod) de variable. Ahora se va anyadiendo el año tambien como nivel de agrupacion y 
            #se guarda en data_cod_year.
            #En esta iteracion se guardan tambien los registros totales, sin agrupacion up/amb para ser utilizados en el calculo del indicador,
            #si ind=True 
            for amb in risc_its.amb_total:
                for cod in risc_its.amb_total[amb]:
                    data_cod_year_amb[amb][cod][year] = risc_its.amb_total[amb][cod]
                    indicador_n_reg[cod]=risc_its.n_reg_total[cod]

        if ind:
            #Obtiene los dos indicadores con formato {up: {"NUM": n, "DEN":n}} y los guarda en indicador_year, donde se guardan los datos de los dos
            #indicadores para cada uno de los años. 
            indicador_1,indicador_2=get_indicador( var_num_ind,indicador_n_reg,year,risc_its.up_ambit)
            printTime("Indicadores")
            indicador_year["ind1"][year]=indicador_1
            indicador_year["ind2"][year]=indicador_2
                
        printTime("Year done {}".format(len(data_cod_year_amb)))
    return risc_its,data_cod_year_amb, indicador_year

def get_rows_txt(data_cod_year_amb,codis_opt):
    """Obtiene las filas del archivo usando los datos en data_cod_year_amb. Desagrupa el diccionario por ambito, codigo (los codigos que hay
       en codis_opt) y los year que hay en periodos. Si alguna de las opciones de codigo es un tuple, hace la union de los registros (que son sets de ids) 
       para cada codigo. El numero de registros se obtiene calculando la longitud del set de ids.
       Devuelve una lista de tuples, que son las filas del archivo final.
    """
    rows=[]
    for amb in data_cod_year_amb:
        for codis in codis_opt:
            n_year={}
            for year in periodos:
                if type(codis) == tuple:
                    n_year[year]=len(set.union(*[set(data_cod_year_amb[amb][codi][year]) for codi in codis]))
                else:
                    n_year[year]=len(data_cod_year_amb[amb][codis][year])
            rows.append([amb,codis_dict_table[codis]]+[n_year[year] for year in periodos])
    return rows

def get_rows_ind(indicador_year,ind_desc):
    """Obtiene las filas del archivo utilizando los datos de indicador_year, donde estan los indicadores para cada any.
       Devuelve una lista de tuples(filas).
    """
    rows=[]
    for year in indicador_year:
        for amb,num_den in indicador_year[year].iteritems():
            rows.append([amb,year,ind_desc,num_den["NUM"], num_den["DEN"]])
    return rows

def export_txt_file(name,header,rows):
    """Funcion para formatear las filas rows y sacar un archivo con nombre name y con un cabecero (header).
    """
    with open(name,"wb") as out_file:
        print(rows[0])
        row_format ="{:>20}\t" * len(rows[0])+"\n"
        out_file.write(row_format.format(*header))
        for row in rows:
            out_file.write(row_format.format(*row))

def run(codi_table,codis_opt,name,amb,ind):
    """ Funcion que engloba el flujo del proceso. Recibe los siguientes argumentos:
        codi_table: dict, diccionario que asocia cada tabla en la que se han de buscar los datos, con los codigos que se pueden encontrar en la tabla en cuestion
                    y que son los que se quieren sacar
        codis_opt: list, lista con los codigos para los que se quieren sacar los datos (se usa para ordenar las columnas del archivo output)
        amb: bool, si se agrupan los datos a nive de ambito
        ind: bool, si se van a calcular indicadores con estos datos.
    """
    #Sacar datos
    risc_its,data_cod_year_amb,indicador_year=get_dades(codi_table,amb,ind)
    
    #Montar txt
    rows_amb=get_rows_txt(data_cod_year_amb,codis_opt)
    header=["ÁMBIT","CODI","2016","2017","2018"]
    export_txt_file(name,header,rows_amb)
    if ind:
        header_ind=["ÁMBIT","PERIODE","DESCRIPCIÓ","NUM","DEN"]
        for ind_desc in indicador_year:
            rows_ind=get_rows_ind(indicador_year[ind_desc],ind_desc)
            export_txt_file(ind_desc+"txt",header_ind,rows_ind)

    return risc_its


def get_id_edat(year):
    """Calcula la edad de cada paciente en import.assignada para un any (year) determinado y selecciona aquellos ids que esten entre 15 y 64.
       Devuelve un set de ids.
    """
    fecha_fin=datetime.date(day=31,month=12,year=year)
    sql = "select id_cip_sec,  (year({fecha})-year(usua_data_naixement))-(date_format({fecha},'%m%d')<date_format(usua_data_naixement,'%m%d')) from {db}.assignada,".format(db=imp,fecha=fecha_fin)
    return {id for id,edat in getAll(sql, imp) if 15 <= edat <= 64}

def get_id_naix():
    """ Asocia a cada id de paciente su fecha de nacimiento.
        Devuelve un diccionario {id: data_naix}
    """
    sql = "select id_cip_sec,usua_data_naixement from {}.assignada".format(imp)
    return {id: dat_naix for id, dat_naix in getAll(sql, imp)  }

#Para hacerla variable global, se crea el diccionario fuera del if __name__= 'main'
#id_edat = {year:get_id_edat(year) for year in periodos}
#printTime('naix ')

id_naix=get_id_naix()

if __name__ == '__main__':

    printTime('inici')
   

    codis_opt_amb=[("CA601","EA6001"),"CA6001","EA6002"]
    name_amb="risc_its_2_3_4.txt"
    risc_its_2=run(codi_table,codis_opt_amb,name_amb,amb=True,ind=True)

    codis_opt_up=["CA601","EA6001"]
    name_up="risc_its_1.txt"
    risc_its_1=run(codi_table_1,codis_opt_up,name_up,amb=False,ind=False)
    
    

   