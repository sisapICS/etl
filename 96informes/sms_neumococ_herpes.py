# -*- coding: utf-8 -*-

import sisapUtils as u
import collections as c
import sisaptools as t
import xlwt
import os

DATABASE = ("exadata", "thewhole")


class SMS_vacunacio():
    def __init__(self):
        cols = '(cip varchar2(13), up varchar2(5), vacuna varchar2(15))'
        u.createTable('SMS_ZOS_PNEU', cols, 'exadata', rm=True)
        u.grantSelect('SMS_ZOS_PNEU','DWSISAP_ROL','exadata')
        self.get_poblacio_interes()
        print('pob')
        self.get_converters()
        print('converters')
        self.get_ps()
        print('ps')
        self.get_vacunes_ecap()
        print('vacunes ecap ok')
        self.get_vacunes_hc3()
        print('vacunes hc3')
        self.get_a_vacunar()
        print('a vacunar')
        self.generate_sms()
        print('sms generats, anem a enviar-los')
        self.send_sms()
    
    def get_poblacio_interes(self):
        self.poblacio = {}
        sql = """select id_cip, up, 
                case when data_naix between date '1957-01-01' and date '1958-05-15' then '1957'
                when data_naix between date '1942-01-01' and date '1943-05-15' then '1943' end any_na
                from nodrizas.assignada_tot
                where (data_naix between date '1957-01-01' and date '1958-05-15'
                or data_naix between date '1942-01-01' and date '1943-05-15')"""
        for id_cip, up, d_n in u.getAll(sql, 'nodrizas'):
            self.poblacio[id_cip] = [up, d_n]
    
    def get_converters(self):
        self.id_cip_sec_2_id_cip = {}
        self.id_cip_2_cip = {}
        hash_2_id_cip = {}
        sql = """select id_cip, id_cip_sec, hash_d from import.u11"""
        for id_cip, id_cip_sec, hash in u.getAll(sql, 'import'):
            if id_cip in self.poblacio:
                hash_2_id_cip[hash] = id_cip
                self.id_cip_sec_2_id_cip[id_cip_sec] = id_cip
        
        cip_2_hash = {}
        hash_c_2_cip = {}
        sql = """SELECT cip, HASH_REDICS, HASH_COVID  FROM PDPTB101_RELACIO"""
        for cip, hash_r, hash_c in u.getAll(sql, 'pdp'):
            cip_2_hash[cip] = hash_r
            hash_c_2_cip[hash_c] = cip
        
        self.nia_2_id_cip = {}
        sql = """SELECT nia, cip FROM dwsisap.rca_cip_nia"""
        for nia, cip in u.getAll(sql, 'exadata'):
            if cip[:-1] in cip_2_hash:
                hash = cip_2_hash[cip[:-1]]
                if hash in hash_2_id_cip:
                    self.nia_2_id_cip[nia] = hash_2_id_cip[hash]
                    self.id_cip_2_cip[hash_2_id_cip[hash]] = cip[:-1]

        self.visitats6838 = set()
        sql = """select PACIENT FROM DWSISAP.SISAP_MASTER_VISITES
                where DATA between date '2023-04-13' and date '2023-04-23'
                and SITUACIO = 'R' AND SECTOR = '6838'"""
        for hash_c in u.getAll(sql, 'exadata'):
            if hash_c in hash_c_2_cip:
                self.visitats6838.add(hash_c_2_cip[hash_c])
    
    def get_ps(self):
        # herpes
        self.herpes_6m = set()
        sql = """SELECT id FROM DWTW.PROBLEMES 
                    WHERE DATA > DATE '2022-05-15' AND codi IN (
                    'B02',
                    'B02.0',
                    'B02.1',
                    'B02.2',
                    'B02.3',
                    'B02.7',
                    'B02.8',
                    'B02.9',
                    'C01-B02',
                    'C01-B02.0',
                    'C01-B02.1',
                    'C01-B02.29',
                    'C01-B02.39',
                    'C01-B02.7',
                    'C01-B02.8',
                    'C01-B02.9'
                    )"""
        with t.Database(*DATABASE) as the_whole:
            for cip, in the_whole.get_all(sql):
                self.herpes_6m.add(cip)

    
    def get_vacunes_ecap(self):
        self.vacunes = c.defaultdict(set)
        sql = """select id_cip_sec, 
                case when agrupador = 920 then 'HERPES'
                    WHEN agrupador = 48 THEN 'PN23'
                    WHEN agrupador = 977 THEN 'PN13'
                    WHEN agrupador = 978 THEN 'PN20'
                end VACUNES
                from nodrizas.eqa_vacunes
                where agrupador in (920, 48, 977, 978)"""
        for id_cip_sec, vacuna in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.id_cip_sec_2_id_cip:
                id_cip = self.id_cip_sec_2_id_cip[id_cip_sec]
                self.vacunes[vacuna].add(id_cip)

    def get_vacunes_hc3(self):
        sql = """select pac_nia,
            CASE WHEN TVA_SID = '407737004' THEN 'HERPES' 
                WHEN TVA_SID = '41000135101' THEN 'PN23'
                WHEN TVA_SID = '448964007' THEN 'PN13'
                WHEN TVA_SID ='161000135109' THEN 'PN13'
                WHEN TVA_SID ='77811000140105' THEN 'PN20'
            END VACUNA
            FROM BISLT_PRO_H.FT_VAC
            WHERE icl_bad IS NULL
            and TVA_SID in ('407737004', '41000135101', '448964007', '161000135109', '77811000140105')"""
        for nia, vacuna in u.getAll(sql, 'exadata'):
            if nia in self.nia_2_id_cip:
                id_cip = self.nia_2_id_cip[nia]
                self.vacunes[vacuna].add(id_cip)
    
    def get_a_vacunar(self):
        self.upload = []
        self.a_vacunar = set()
        for id_cip, (up, n_d) in self.poblacio.items():
            if id_cip in self.id_cip_2_cip:
                cip = self.id_cip_2_cip[id_cip]
                if id_cip not in self.vacunes['HERPES']:
                    if cip not in self.herpes_6m:
                        self.upload.append((cip, up, 'herpes'))
                        self.a_vacunar.add(cip)
                if n_d == '1957':
                    if id_cip not in self.vacunes['PN20']:
                        if id_cip not in self.vacunes['PN13']:
                            if id_cip not in self.vacunes['PN23']:
                                self.upload.append((cip, up, 'pneumococ'))
                                self.a_vacunar.add(cip)
        u.listToTable(self.upload, 'SMS_ZOS_PNEU', 'exadata')

    def generate_sms(self):
        print('generant sms')
        sql = """SELECT usua_cip, usua_nom, USUA_TELEFON_PRINCIPAL, USUA_TELEFON_SECUNDARI, USUA_IDIOMA   
                FROM usutb040
                WHERE usua_situacio = 'A'"""
        a_enviar = set()
        for sector in u.sectors:
            print(sector)
            if sector == '6838':
                for cip, nom ,telf1, telf2, idioma in u.getAll(sql, sector):
                    if cip in self.a_vacunar:
                        a_enviar.add((cip, nom ,telf1, telf2, idioma))
            else:
                for cip, nom ,telf1, telf2, idioma in u.getAll(sql, sector):
                    if cip in self.a_vacunar:
                        if cip in self.visitats6838:
                            a_enviar.add((cip, nom ,telf1, telf2, idioma))

        txt_cat = "Hola {}, ens consta a la teva hist�ria cl�nica que et falten per administrar alguna vacuna del calendari sistem�tic, et recomanem posar-te en contacte amb el teu centre d'atenci� prim�ria i demanar cita amb la infermera per tal d'actualitzar la vacunaci�. Recorda portar el carnet de vacunaci� en el moment de la visita, si �s que el tens. Les vacunes eviten moltes malalties infeccioses a Catalunya i salven vides."
        
        txt_caste = "Hola {}, nos consta en tu historia cl�nica que te faltan por administrar alguna vacuna del calendario sistematico, te recomendamos contactar con tu centro de atenci�n primaria y pedir cita con la enfermera para actualizar la vacunaci�n. Recuerda traer el carn� de vacunaci�n en el momento de la visita, si lo tienes. Las vacunas evitan muchas enfermedades infecciosas en \Catalu�a y salvan vidas."

        print('cooking')
        self.upload = set()
        for (cip, nom ,telf1, telf2, idioma) in a_enviar:
            telf = ''
            if telf1:
                telf1 = telf1.replace('.', '')
                telf1 = str(telf1)
                if len(telf1) == 9:
                    if telf1[0] in ('6', '7'):
                        telf = telf1
            if telf2:
                telf2 = str(telf2)
                telf2 = telf2.replace('.', '')
                if telf == '':
                    if len(telf2) == 9:
                        if telf2[0] in ('6', '7'):
                            telf = telf2
            if idioma[0:4] == 'CATA':
                txt = txt_cat.format(nom)
            if idioma[0:4] == 'CAST':
                txt = txt_caste.format(nom)
            if telf != '':
                self.upload.add((telf, txt))

    def send_sms(self):
        wb1 = xlwt.Workbook()
        ws_1 = wb1.add_sheet(u'1')
        wb2 = xlwt.Workbook()
        ws_2 = wb2.add_sheet(u'1')
        wb3 = xlwt.Workbook()
        ws_3 = wb3.add_sheet(u'1')
        wb4 = xlwt.Workbook()
        ws_4 = wb4.add_sheet(u'1')
        wb5 = xlwt.Workbook()
        ws_5 = wb5.add_sheet(u'1')
        wb6 = xlwt.Workbook()
        ws_6 = wb6.add_sheet(u'1')
        wb7 = xlwt.Workbook()
        ws_7 = wb7.add_sheet(u'1')
        i = 1
        l = len(self.upload)/6
        iter = 1
        print('els pacients que rebran sms s�n:', len(self.upload))
        for e in self.upload:
            if iter == 1:
                ws_1.write(i, 0, e[0].decode('iso-8859-1'))
                ws_1.write(i, 1, e[1].decode('iso-8859-1'))
                i += 1
            elif iter == 2:
                ws_2.write(i, 0, e[0].decode('iso-8859-1'))
                ws_2.write(i, 1, e[1].decode('iso-8859-1'))
                i += 1
            elif iter == 3:
                ws_3.write(i, 0, e[0].decode('iso-8859-1'))
                ws_3.write(i, 1, e[1].decode('iso-8859-1'))
                i += 1
            elif iter == 4:
                ws_4.write(i, 0, e[0].decode('iso-8859-1'))
                ws_4.write(i, 1, e[1].decode('iso-8859-1'))
                i += 1
            elif iter == 5:
                ws_5.write(i, 0, e[0].decode('iso-8859-1'))
                ws_5.write(i, 1, e[1].decode('iso-8859-1'))
                i += 1
            elif iter == 6:
                ws_6.write(i, 0, e[0].decode('iso-8859-1'))
                ws_6.write(i, 1, e[1].decode('iso-8859-1'))
                i += 1
            elif iter == 7:
                ws_7.write(i, 0, e[0].decode('iso-8859-1'))
                ws_7.write(i, 1, e[1].decode('iso-8859-1'))
                i += 1
            if i%l == 0: 
                i = 1
                iter += 1
        
        wb1.save(u.tempFolder + 'vacunacio_pneumo_herpes_1_6838.xls')
        wb2.save(u.tempFolder + 'vacunacio_pneumo_herpes_2_6838.xls')
        wb3.save(u.tempFolder + 'vacunacio_pneumo_herpes_3_6838.xls')
        wb4.save(u.tempFolder + 'vacunacio_pneumo_herpes_4_6838.xls')
        wb5.save(u.tempFolder + 'vacunacio_pneumo_herpes_5_6838.xls')
        wb6.save(u.tempFolder + 'vacunacio_pneumo_herpes_6_6838.xls')
        wb7.save(u.tempFolder + 'vacunacio_pneumo_herpes_7_6838.xls')
        with t.SFTP("sisap") as sftp:
            for file in ('vacunacio_pneumo_herpes_1_6838.xls',
                        'vacunacio_pneumo_herpes_2_6838.xls',
                        'vacunacio_pneumo_herpes_3_6838.xls',
                        'vacunacio_pneumo_herpes_4_6838.xls',
                        'vacunacio_pneumo_herpes_5_6838.xls',
                        'vacunacio_pneumo_herpes_6_6838.xls',
                        'vacunacio_pneumo_herpes_7_6838.xls'):
                sftp.put(u.tempFolder + file, "sms/{}".format(file))
                os.remove(u.tempFolder + file)


if __name__ == "__main__":
    SMS_vacunacio()

    

