# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
import datetime

nod = 'nodrizas'
imp = 'import'

debug = False

anys = [2015, 2016, 2017]

codis_cim = {}
sql = 'select criteri_codi from eqa_criteris where agrupador=24'
for cim, in getAll(sql, nod):
    codis_cim[cim] = True
   
in_crit = tuple(codis_cim)


centres = {}
sql = "select scs_codi, ics_desc, abs from cat_centres where ep='0208'"
for up, desc, abs in getAll(sql, nod):
    centres[up] = {'desc': desc, 'abs': abs}

all_pob = {}
sql = 'select id_cip_sec, year(usua_data_naixement)from assignada'
for id, naix in getAll(sql, imp):
    all_pob[id] =  naix

resultat = Counter()
for anyc in anys:
    datC = str(anyc) + '-12-31'
    pob = {}
    sql = 'select id_cip_sec, up, dataany from assignadahistorica where dataany = {0} {1}'.format(anyc, ' limit 10' if debug else '')
    for id, up, datany in getAll(sql, imp):
        if up in centres:
            desc = centres[up]['desc']
            abs = centres[up]['abs']
            if id in all_pob:
                naix = all_pob[id]
                edat = int(anyc) - naix
                pob[(id)] = {'up': up, 'desc': desc, 'edat': edat, 'abs': abs}
    posologia = {}
    sql = "select   id_cip_sec,max(tires) from    (    select        id_cip_sec ,round(if(fit_per_rec_s = 0,0,7 * fit_cont_rec_s / fit_per_rec_s),1) tires \
    from   import.tires where        fit_data_alta <= '{0}'       and (fit_dat_baixa = 0 or fit_dat_baixa > '{0}')) t where tires > 0 group by id_cip_sec  {1}".format(datC, ' limit 10' if debug else '')
    for  id, tires in getAll(sql, imp):
        posologia[(id)] = int(tires)

    sql = "select id_cip_sec from problemes where  pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= '{0}' and (pr_data_baixa is null or pr_data_baixa > '{0}') \
            and (pr_dba is null or pr_dba> '{0}') and pr_cod_ps in {1}".format(datC, in_crit)
    for id, in getAll(sql, imp):
        if (id) in pob:
            desc = pob[(id)]['desc']
            up = pob[(id)]['up']
            edat = pob[(id)]['edat']
            abs = pob[(id)]['abs']
            if int(edat) >= 4:
                edat = int(edat)
                if (id) in posologia:
                    ntires = posologia[(id)]
                    ambtires = 1
                else:
                    ntires = 0
                    ambtires = 0
                t42, t63 = 0,0
                if int(ntires) >= 42:
                    t42 = 1
                if (ntires) >= 63:
                    t63 = 1
                resultat[(anyc, abs, up, desc, edat, 'rec')] += 1
                resultat[(anyc, abs, up, desc, edat, 'ambtires')] += ambtires
                resultat[(anyc, abs, up, desc, edat, 'ntires')] += int(ntires)
                resultat[(anyc, abs, up, desc, edat, 't42')] += t42
                resultat[(anyc, abs, up, desc, edat, 't63')] += t63

upload = []
for (anyc, abs, up, desc, edat, indicador), values in resultat.items():
    if indicador == 'rec':
        ambtires = resultat[(anyc, abs, up, desc, edat, 'ambtires')]
        ntires = resultat[(anyc, abs, up, desc, edat, 'ntires')]
        t42 = resultat[(anyc, abs, up, desc, edat, 't42')]
        t63 = resultat[(anyc, abs, up, desc, edat, 't63')]
        mitjana = float(int(ntires)/int(values))
        try:
            mitjana2 = float(int(ntires)/int(ambtires))
        except:
            mitjana2 = 0
        upload.append([anyc,abs,  up, desc, edat, values, mitjana, ambtires, ntires, mitjana2, t42, t63])

            
file = tempFolder + 'Tires_DM1.txt'
writeCSV(file, upload, sep=';')

