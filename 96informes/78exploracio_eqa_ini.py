from sisapUtils import getAll, createTable, listToTable, grantSelect
from collections import defaultdict
from numpy import mean, std, percentile


dates = ((2016, 11), (2017, 0))
tipus = ('M', 'P', 'T', 'O', 'G')
eqas = {'eqa': 'adults', 'ped': 'nens', 'soc': 'tsocial', 'odn': 'odonto', 'ass': 'assir'}
db = 'pdp'
tb = 'sisap_exploracio_ini'
resultat = []


for eqa in eqas:
    sql = "select indicador, literal from {}Cataleg where dataAny = {}".format(eqa, dates[0][0])
    literal = {cod: des for (cod, des) in getAll(sql, db)}
    sql = "select indicador from {}CatalegPunts where dataAny = {} and tipus in {} and punts > 0".format(eqa, dates[0][0], tipus)
    is_individual = set([ind for ind, in getAll(sql, db)])
    dades = {}
    for data in dates:
        sql = "select indicador, detectats from {}Indicadors \
               where dataAny = {} and dataMes = {} and tipus in {}".format(eqa, data[0], data[1], tipus)
        for indicador, detectats in getAll(sql, db):
            if indicador not in dades:
                dades[indicador] = defaultdict(list)
            dades[indicador][data[0]].append(detectats)
    for indicador, d in dades.items():
        this = [indicador, literal[indicador], eqas[eqa], bool(indicador in is_individual)]
        for data in dates:
            valors = d[data[0]]
            this.extend([round(mean(valors), 2),
                         round(percentile(valors, 20), 2),
                         round(percentile(valors, 50), 2),
                         round(percentile(valors, 80), 2),
                         round(100.0 * std(valors) / mean(valors), 2) if mean(valors) > 0 else 0])
        resultat.append(this)

createTable(tb, '(indicador varchar2(15), literal varchar2(250), eqa varchar2(10), individual int, \
                  mitjana_pre number, p20_pre number, p50_pre number, p80_pre number, cv_pre number, \
                  mitjana_post number, p20_post number, p50_post number, p80_post number, cv_post number)', db, rm=True)
listToTable(sorted(resultat), tb, db)
grantSelect(tb, ('PREDUFFA', 'PREDUPRP', 'PREDUMMP'), db)
