# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
import datetime

debug = False

nod = 'nodrizas'
imp = 'import'

file = 'se.csv'
setmanes_epi = {}
for (data1,anys,mes,dia_natural,dia_setmana,setmana_natural,primer_dissabte,setmana_epi,any_epi1,any_epi) in readCSV(file, sep=';'):
    setmanes_epi[str(data1)] = {'anys': any_epi1, 'set': setmana_epi}

l_cuap = {
            '01482':'RS78',
            '07111':'RS78',
            '01480':'RS78',
            '01345':'RS78',
            '01342': 'RS78',
            '04880': 'RS78',
            '01483': 'RS78',
            '01341': 'RS78',
            '01479':'RS78',
            '07208':'RS78',
            '07207': 'RS78',
            '07365':'RS78',
            '07367':'RS78',
            '03303': 'RS67',
            '04846': 'RS78',
            '04913':'RS78',
            '04912':'RS78',
            '04911':'RS78',
            '04910':'RS78',
            '01481':'RS78',
            '04842':'RS78',
         }   
         
rsdicc = {}
sql = 'select distinct rs_cod, rs_des from cat_sisap_agas'
for cod, desc in getAll(sql, imp):
    cod = 'RS' + str(cod)
    rsdicc[cod] = desc
    
centres, saps = {}, {}
sql = 'select scs_codi, rs, sap_desc from cat_centres'
for up, rscod, sap in getAll(sql, nod):
    rsdesc = rsdicc[rscod]
    centres[up] = rsdesc
    saps[sap] =  rscod
    
sql = "select up_codi_up_scs,a.up_codi_up_ics,up_desc_up_ics, dap_desc_dap  from cat_gcctb007 a inner join cat_gcctb008 b on a.up_codi_up_ics=b.up_codi_up_ics inner join cat_gcctb006 c on a.dap_codi_dap = c.dap_codi_dap \
    where (a.up_desc_up_ics like 'ACUT%' or a.up_desc_up_ics like 'CUAP%' or a.up_desc_up_ics like 'PAC %' or a.up_desc_up_ics like 'CAC %' or a.up_desc_up_ics like 'DISPOSITIU%') \
    and a.up_data_baixa = 0 and b.up_data_baixa = 0"
for up, be, desc, sap in getAll(sql, imp):   
    if sap in saps:
        rsc = saps[sap]
        rs = rsdicc[rsc]
    else:
        rsc = l_cuap[up]
        rs = rsdicc[rsc]
    centres[up] = rs

codis_cim = {}
codis_dx = []
sql = "select codi_cim10, desc_ciap from cat_md_ct_cim10_ciap where codi_ciap in ('R74', 'R78', 'R81')"
for cim, ciap in getAll(sql, imp):
    codis_cim[cim] = ciap
    codis_dx.append(cim)
    
in_crit = tuple(codis_dx)

pob = {}
sql = 'select id_cip_sec, usua_data_naixement from assignada'
for id, naix in getAll(sql, imp):
    pob[id] = naix


nopob = 0 
recomptes = Counter()
sql = "select id_cip_sec, pr_cod_ps, pr_dde, year(pr_dde), pr_up from problemes where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_data_baixa is null and pr_cod_ps in {}".format(in_crit)
for id, ps, dde,  anys, up in getAll(sql, imp):
    if ps in codis_cim:
        ciap2 = codis_cim[ps]
        if str(dde) in setmanes_epi:
            se = setmanes_epi[str(dde)]['set']
            anyse =setmanes_epi[str(dde)]['anys']
            if up in centres:
                rs = centres[up]
                if id in pob:
                    naix = pob[id]
                    edat = yearsBetween(naix, dde)
                    if int(anyse) < 2018:
                        recomptes[(anyse, se, rs, edat, ciap2)] += 1  
                else:
                    nopob += 1

upload = []  
for (anyse, se, rs, edat, ciap2), n in recomptes.items():
    upload.append([anyse, se, rs, edat, ciap2, n])
            
file = tempFolder + 'Infeccions_per_SE.txt'
writeCSV(file, upload, sep=';')
    