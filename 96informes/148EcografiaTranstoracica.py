# -*- coding: utf8 -*-

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *

imp = 'import'
nod = 'nodrizas'

ecos = "('RA00470', 'RA00468', 'RA00416')"

def get_ecos(ecografia):

    if ecografia == 'RA00470':
        desc_eco = 'Ecografia fetal'
    elif ecografia == 'RA00468':
        desc_eco = 'Ecografia transEsofàgica'    
    elif ecografia == 'RA00416':
        desc_eco = 'Ecografia transToràcica'    
    else:
        desc_eco = 'error'
    
    return desc_eco


centres = {}
sql = "select scs_codi, amb_desc from cat_centres where ep='0208'"
for up, ambit in getAll(sql, nod):
    centres[up] = ambit
    
recomptes = Counter()
sql = "select id_cip_sec, year(oc_data), oc_servei_ori, inf_codi_prova, oc_up_ori from nod_proves where inf_codi_prova in {}".format(ecos)
for id, anys, servei, prova, up in getAll(sql, nod):
    if anys == 2017:
        if up in centres:
            ambit = centres[up]
            desc_prova = get_ecos(prova)
            recomptes[(ambit, servei, desc_prova)] += 1
            
upload = []
for (ambit, servei, desc_prova),r in recomptes.items():
    upload.append([ambit, servei, desc_prova, r])

file = tempFolder + 'Ecografies.txt'
writeCSV(file, upload, sep=';')  
        
    
