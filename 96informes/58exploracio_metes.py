from sisapUtils import getAll, tempFolder, writeCSV
from collections import defaultdict
from numpy import mean, std, percentile
from math import ceil


interval = 2
periode = (2016, 0)
out = ('EQA04', 'EQA06')
ics = set([up for up, in getAll("select scs_codi from cat_centres where ep = '0208'", 'nodrizas')])
file = tempFolder + 'exploracio_metes.csv'


class Metes(object):

    def __init__(self):
        self.get_resolucio_uba()
        self.get_resolucio_up()
        self.get_resolucio_indicador()
        self.get_metes_indicador()
        self.get_pacients_uba()
        self.get_assoliment_uba()
        self.get_detectats_ics()
        self.get_metes_ics()

    def get_resolucio_uba(self):
        self.resolucio_uba = {}
        sql = "select indicador, up, uab, resolts, detectats from eqaindicadors where dataany = {} and datames = {} and tipus = 'M'".format(*periode)
        for indicador, up, uba, resolts, detectats in getAll(sql, 'pdp'):
            if indicador[:5] not in out and up in ics:
                self.resolucio_uba[(indicador, up, uba)] = (resolts, detectats)

    def get_resolucio_up(self):
        self.resolucio_up = {}
        for (indicador, up, uba), valors in self.resolucio_uba.items():
            id = (indicador, up)
            if id not in self.resolucio_up:
                self.resolucio_up[id] = [0, 0]
            self.resolucio_up[id] = [sum(i) for i in zip(self.resolucio_up[id], valors)]

    def get_resolucio_indicador(self):
        self.resolucio_indicador = defaultdict(list)
        for (indicador, up), (resolts, detectats) in self.resolucio_up.items():
            resolucio = 0 if detectats == 0 else (float(resolts) / float(detectats))
            self.resolucio_indicador[indicador].append(resolucio)

    def get_metes_indicador(self):
        self.metes_indicador = {}
        for indicador, valors in self.resolucio_indicador.items():
            cv = std(valors) / mean(valors)
            pmin = 20
            pmax = self.get_pmax(cv)
            mmin = percentile(valors, pmin)
            mmax = percentile(valors, pmax)
            self.metes_indicador[indicador] = (mmin, mmax)

    def get_pmax(self, coef):
        return 50 if coef < 0.1 else 60 if coef < 0.2 else 70 if coef < 0.3 else 80

    def get_pacients_uba(self):
        self.pacients_uba = {}
        for (indicador, up, uba), (resolts, detectats) in self.resolucio_uba.items():
            mmin, mmax = self.metes_indicador[indicador]
            nmin, nmax = detectats * mmin, detectats * mmax
            minim = ceil(nmin)
            maxim = 1 if nmax < 1 else round(nmax)
            if (maxim - minim) < interval:
                minim = max(minim - interval, 0)
            self.pacients_uba[(indicador, up, uba)] = (int(minim), int(maxim))

    def get_assoliment_uba(self):
        self.assoliment_uba = {}
        for id, (resolts, detectats) in self.resolucio_uba.items():
            minim, maxim = self.pacients_uba[id]
            if resolts >= minim:
                assoliment = 1
            elif resolts <= maxim:
                assoliment = 0
            else:
                assoliment = (resolts - minim) / (maxim - minim)
            self.assoliment_uba[id] = (resolts, detectats, minim, maxim, assoliment)

    def get_detectats_ics(self):
        self.detectats_ics = defaultdict(int)
        for (indicador, up), (resolts, detectats) in self.resolucio_up.items():
            self.detectats_ics[indicador] += detectats

    def get_metes_ics(self):
        self.pacients_ics = {}
        for (indicador, up, uba), valors in self.pacients_uba.items():
            if indicador not in self.pacients_ics:
                self.pacients_ics[indicador] = [0, 0]
            self.pacients_ics[indicador] = [sum(i) for i in zip(self.pacients_ics[indicador], valors)]
        self.metes_ics_actuals = {indicador: (mmin, mmax) for (indicador, mmin, mmax) in getAll('select indicador, mmin, mmax from exp_ecap_cataleg', 'eqa_ind')}
        self.metes_ics = []
        for indicador, (minim, maxim) in self.pacients_ics.items():
            detectats = self.detectats_ics[indicador]
            mmin_actual, mmax_actual = self.metes_ics_actuals[indicador]
            mmin = float(minim) / float(detectats)
            mmax = float(maxim) / float(detectats)
            self.metes_ics.append([indicador, self.format_number(mmin), self.format_number(mmax), self.format_number(mmin_actual), self.format_number(mmax_actual)])

    def format_number(self, num):
        return str(round(100 * num, 2)).replace('.', ',') if num > 0 else ''


metes = Metes().metes_ics
header = [['indicador', 'mmin nova', 'mmax nova', 'mmin actual', 'mmax actual']]
writeCSV(file, header + sorted(metes), sep=';')
