#coding: utf-8


"""
import collections as c
import sisapUtils as u

USUARIS = ("04104", "04416", "04302")

class TEST(object):
    "".""

    def __init__(self):
        "".""
        self.get_usuaris()

    def get_usuaris(self):
        "".""
        sql = "select codi_sector, ide_usuari from cat_pritb992 \
               where ide_categ_prof_c in {}".format(USUARIS)
        self.usuaris = set(u.getAll(sql, "import"))
        # print(self.usuaris)
        print("USUARIS: ", len(self.usuaris))
"""

import collections as c
import sisapUtils as u
import csv

#ANY = 2019

class TEST(object):

    def __init__(self):
        """."""

        #self.get_jobs()
        #self.get_visites()
        print 'Start'
        self.get_poblacio()
        self.get_hash_idcipsec()
        print 0
        self.get_xlsx_centres()
        print 1
        self.get_atdom()
        print 2
        #self.get_csv_centres()
        print 3
        #self.get_taules()
        #self.get_visites_atdom()
        self.get_visites_nou()
        print 4
        #self.get_visites_realitzades()
        self.get_resum_dades_fase1()
        self.get_resum_dades_fase2()
        self.export_resum_dades_fase1()
        self.export_resum_dades_fase2()
        #self.get_motius()
        #self.export_dades_fase2()
        print 1
        """
        print 0
        self.get_xlsx_centres()
        print 1
        self.get_diagnostics()
        print 2
        self.export_dades_diagnostics()
        print 3
        """

    def get_poblacio(self):
        self.poblacio = {}
        sql = "select id_cip_sec, usua_data_naixement, usua_sexe from assignada"
        for id, naix, sexe in u.getAll(sql, "import"):
            self.poblacio[id] = {'data_naix': naix, 'sexe': sexe}

    def get_hash_idcipsec(self):
        print 'Hash_Start'
        self.hash_idcipsec = {}
        sql = "select id_cip_sec, hash_d from u11"
        count = 0
        for id_cip_sec, hash_d in u.getAll(sql, "import"):
            self.hash_idcipsec[hash_d] = id_cip_sec
            count +=1
            if count%2000==0:
                print 'Hash_'+str(count)

    def get_atdom(self):
        self.atdom = set()
        sql = "select id_cip_sec from problemes where pr_cod_ps in ('C01-Z74.9', 'Z74.9')"
        for id, in u.getAll(sql, "import"):
            if id in self.poblacio:
                self.atdom.add(id)

    def get_centres_visites(self):
        self.centres_visites = {}
        sql = "SELECT * FROM SISAP_CENTRES_VISITES"
        for mes, any, cent_codi_oficial, n_visites in u.getAll(sql, "pdp"):
            self.centres_visites[cent_codi_oficial] = {"mes": mes, "any": any, "n_visites": n_visites}

    def get_taules(self):
        """."""
        self.tables = []
        for table in u.getSubTables('vistb043'):
            try:
                dat, = getOne("select date_format(cr_data_reg,'%Y%m') from {} limit 1".format(table), imp)
                if self.past_date <= dat <= self.current_date:
                    print table
                    self.tables[table]
            except TypeError:
                continue

    def get_jobs(self):
        """."""
        inici = (2021, 6)
        final = u.getOne("select year(data_ext), month(data_ext) from dextraccio", "nodrizas")
        self.jobs = []
        sql = "select year(visi_data_visita), month(visi_data_visita) from {} limit 1"  # noqa
        for taula in u.getSubTables("visites"):
            this = u.getOne(sql.format(taula), "import")
            if inici <= this <= final:
                self.jobs.append((taula, this))

    def _get_visites_worker(self, params):
        """."""
        taula, (any, mes) = params
        visites = c.defaultdict(dict)
        sql = """SELECT visi_centre_codi_centre, visi_data_visita, id_cip_sec, visi_servei_codi_servei, visi_tipus_citacio, visi_lloc_visita, s_espe_codi_especialitat, visi_tipus_visita, visi_etiqueta
                 FROM {}
                 WHERE visi_situacio_visita = 'R'
              """.format(taula)
        for visi_centre_codi_centre, visi_data_visita, id_cip_sec, visi_servei_codi_servei, visi_tipus_citacio, visi_lloc_visita, s_espe_codi_especialitat, visi_tipus_visita, visi_etiqueta in u.getAll(sql, "import"):
            count += 1
            if visi_centre_codi_centre in self.csv_centres and id_cip_sec in self.poblacio:
                sexe = self.poblacio[id_cip_sec]["sexe"]
                edat = (visi_data_visita.date() - self.poblacio[id_cip_sec]["data_naix"]).days / 365
                if edat <= 14:
                    grup_edat = "E_00_14"
                elif edat >= 15 and edat <= 44:
                    grup_edat = "E_15_44"
                elif edat >= 45 and edat <= 64:
                    grup_edat = "E_45_64"
                elif edat >= 65 and edat <= 74:
                    grup_edat = "E_65_74"
                elif edat >= 75:
                    grup_edat = "E_M75"
                if visi_tipus_visita in ("9C", "9R", "9D", "9T", "9E"):
                    if visi_tipus_visita == "9E" and visi_etiqueta == "ECTA":
                        visi_tipus_visita = "9Ec"
                else:
                    visi_tipus_visita = "Altre"
                if not visi_centre_codi_centre in visites:
                    visites[visi_centre_codi_centre] = c.defaultdict(dict)
                if id_cip_sec in visites[visi_centre_codi_centre]:
                    visites[visi_centre_codi_centre][id_cip_sec].append(
                        {"visi_tipus_visita": visi_tipus_visita, "visi_servei_codi_servei": visi_servei_codi_servei,
                         "grup_edat": grup_edat, "sexe": sexe, "visi_tipus_citacio": visi_tipus_citacio,
                         "visi_lloc_visita": visi_lloc_visita, "s_espe_codi_especialitat": s_espe_codi_especialitat})
                else:
                    visites[visi_centre_codi_centre][id_cip_sec] = [
                        {"visi_tipus_visita": visi_tipus_visita, "visi_servei_codi_servei": visi_servei_codi_servei,
                         "grup_edat": grup_edat, "sexe": sexe, "visi_tipus_citacio": visi_tipus_citacio,
                         "visi_lloc_visita": visi_lloc_visita, "s_espe_codi_especialitat": s_espe_codi_especialitat}]
        return (visites, [any, mes])

    def get_visites(self):
        """."""
        self.visites_realitzades = c.defaultdict()
        visites = u.multiprocess(self._get_visites_worker, self.jobs, 1)  # noqa
        for worker in visites:
            for row in worker:
                self.visites[row[:-1]] += row[-1]


    def get_visites_realitzades(self):
        #TODO: DATA_NAIXEMENT_CIP calcular edat en el moment de la visita (: <14, 15 a 44, 45 a 64, 65 a 74, 75 o més anys)
        #TODO: VISI_TIPUS_CITACIO
        #TODO: lloc visita (: centre (C), domicili (D))
        #TODO: Colegiat fora (: 9C, 9R, 9D, 9T, 9E (=9E que no són etiqueta = ECTA), 9Ec (=9E amb etiqueta ECTA en VISI_ETIQUETA), altres (les que no es puguin classificar en les anteriors))
        #TODO: Guardar ESPE_ESPECIALITAT
        self.visites_realitzades = c.defaultdict(dict)
        sql = """SELECT visi_centre_codi_centre, visi_data_visita, id_cip_sec, visi_servei_codi_servei, visi_tipus_citacio, visi_lloc_visita, s_espe_codi_especialitat, visi_tipus_visita, visi_etiqueta
                 FROM visites
                 WHERE visi_situacio_visita = 'R'
              """
        count=0
        for visi_centre_codi_centre, visi_data_visita, id_cip_sec, visi_servei_codi_servei, visi_tipus_citacio, visi_lloc_visita, s_espe_codi_especialitat, visi_tipus_visita, visi_etiqueta in u.getAll(sql, "import"):
            any = visi_data_visita.year
            if visi_centre_codi_centre in self.csv_centres and id_cip_sec in self.poblacio:
                count+=1
                mes = visi_data_visita.month
                sexe=self.poblacio[id_cip_sec]["sexe"]
                edat=(visi_data_visita - self.poblacio[id_cip_sec]["data_naix"]).days/365
                if edat <= 14:
                    grup_edat = "E_00_14"
                elif edat >= 15 and edat <= 44:
                    grup_edat = "E_15_44"
                elif edat >= 45 and edat <= 64:
                    grup_edat = "E_45_64"
                elif edat >= 65 and edat <= 74:
                    grup_edat = "E_65_74"
                elif edat >=75:
                    grup_edat = "E_M75"
                if visi_tipus_visita in ("9C", "9R", "9D", "9T", "9E"):
                    if visi_tipus_visita == "9E" and visi_etiqueta == "ECTA":
                            visi_tipus_visita = "9Ec"
                else:
                    visi_tipus_visita = "Altre"
                if not visi_centre_codi_centre in self.visites_realitzades:
                    self.visites_realitzades[visi_centre_codi_centre] = c.defaultdict(dict)
                if not any in self.visites_realitzades[visi_centre_codi_centre]:
                    self.visites_realitzades[visi_centre_codi_centre][any] = c.defaultdict(dict)
                if not mes in self.visites_realitzades[visi_centre_codi_centre][any]:
                    self.visites_realitzades[visi_centre_codi_centre][any][mes] = c.defaultdict(dict)
                if id_cip_sec in self.visites_realitzades[visi_centre_codi_centre][any][mes]:
                    self.visites_realitzades[visi_centre_codi_centre][any][mes][id_cip_sec].append({"visi_tipus_visita": visi_tipus_visita, "visi_servei_codi_servei": visi_servei_codi_servei, "grup_edat": grup_edat, "sexe": sexe, "visi_tipus_citacio": visi_tipus_citacio, "visi_lloc_visita": visi_lloc_visita, "s_espe_codi_especialitat": s_espe_codi_especialitat})
                else:
                    self.visites_realitzades[visi_centre_codi_centre][any][mes][id_cip_sec] = [{"visi_tipus_visita": visi_tipus_visita, "visi_servei_codi_servei": visi_servei_codi_servei, "grup_edat": grup_edat, "sexe": sexe, "visi_tipus_citacio": visi_tipus_citacio, "visi_lloc_visita": visi_lloc_visita,"s_espe_codi_especialitat": s_espe_codi_especialitat}]
                if count%500==0:
                    print 'Count_'+str(count)
                if count==2000:
                    print 'Count_'+str(count)

    def get_visites_atdom_1(self):
        #TODO: DATA_NAIXEMENT_CIP calcular edat en el moment de la visita (: <14, 15 a 44, 45 a 64, 65 a 74, 75 o més anys)
        #TODO: VISI_TIPUS_CITACIO
        #TODO: lloc visita (: centre (C), domicili (D))
        #TODO: Colegiat fora (: 9C, 9R, 9D, 9T, 9E (=9E qºue no són etiqueta = ECTA), 9Ec (=9E amb etiqueta ECTA en VISI_ETIQUETA), altres (les que no es puguin classificar en les anteriors))
        #TODO: Guardar ESPE_ESPECIALITAT
        self.visites_realitzades_atdom = []
        sql = """SELECT visi_centre_codi_centre, visi_data_visita, id_cip_sec, visi_servei_codi_servei, visi_tipus_citacio, visi_lloc_visita, s_espe_codi_especialitat, visi_tipus_visita, visi_etiqueta
                 FROM visites
                 WHERE visi_situacio_visita = 'R'
              """
        count=0
        for visi_centre_codi_centre, visi_data_visita, id_cip_sec, visi_servei_codi_servei, visi_tipus_citacio, visi_lloc_visita, s_espe_codi_especialitat, visi_tipus_visita, visi_etiqueta in u.getAll(sql, "import"):
            any = visi_data_visita.year
            if visi_centre_codi_centre in self.csv_centres and id_cip_sec in self.atdom:
                count += 1
                visi_centre_nom_centre = self.csv_centres[visi_centre_codi_centre]['nom']
                visi_centre_up_centre = self.csv_centres[visi_centre_codi_centre]['up']
                if count%500==0:
                    print 'Count_'+str(count)
                mes = visi_data_visita.month
                if mes == 5 and any in (2019, 2021):
                    sexe=self.poblacio[id_cip_sec]["sexe"]
                    edat=(visi_data_visita - self.poblacio[id_cip_sec]["data_naix"]).days/365
                    if edat <= 14:
                        grup_edat = "E_00_14"
                    elif edat >= 15 and edat <= 44:
                        grup_edat = "E_15_44"
                    elif edat >= 45 and edat <= 64:
                        grup_edat = "E_45_64"
                    elif edat >= 65 and edat <= 74:
                        grup_edat = "E_65_74"
                    elif edat >=75:
                        grup_edat = "E_M75"
                    if visi_tipus_visita in ("9C", "9R", "9D", "9T", "9E"):
                        if visi_tipus_visita == "9E" and visi_etiqueta == "ECTA":
                                visi_tipus_visita = "9Ec"
                    else:
                        visi_tipus_visita = "Altre"
                    self.visites_realitzades_atdom.append([id_cip_sec, visi_centre_codi_centre,visi_centre_nom_centre,visi_centre_up_centre,any,mes,visi_tipus_visita,visi_servei_codi_servei,grup_edat,sexe,visi_tipus_citacio,visi_lloc_visita,s_espe_codi_especialitat])

    def get_visites_atdom(self):
        #TODO: DATA_NAIXEMENT_CIP calcular edat en el moment de la visita (: <14, 15 a 44, 45 a 64, 65 a 74, 75 o més anys)
        #TODO: VISI_TIPUS_CITACIO
        #TODO: lloc visita (: centre (C), domicili (D))
        #TODO: Colegiat fora (: 9C, 9R, 9D, 9T, 9E (=9E que no són etiqueta = ECTA), 9Ec (=9E amb etiqueta ECTA en VISI_ETIQUETA), altres (les que no es puguin classificar en les anteriors))
        #TODO: Guardar ESPE_ESPECIALITAT
        self.visites_realitzades_atdom = c.defaultdict(dict)
        sql = """SELECT visi_centre_codi_centre, visi_data_visita, visi_usuari_cip, visi_servei_codi_servei, visi_tipus_citacio, visi_lloc_visita, s_espe_codi_especialitat, visi_tipus_visita, visi_etiqueta, espe_especialitat
                 FROM vistb043
                 WHERE visi_situacio_visita = 'R' AND to_char(visi_data_visita, 'YYYY') IN (2019,2021) AND to_char(visi_data_visita, 'MM') = 5
              """
        count=0
        count_hash = 0
        for visi_centre_codi_centre, visi_data_visita, hash, visi_servei_codi_servei, visi_tipus_citacio, visi_lloc_visita, s_espe_codi_especialitat, visi_tipus_visita, visi_etiqueta, espe_especialitat in u.getAll(sql, "redics"):
            if hash in self.hash_idcipsec:
                id_cip_sec = self.hash_idcipsec[hash]
                if id_cip_sec in self.poblacio:
                    do = True
                else:
                    do = False
            else:
                count_hash +=1
                do = False
            any = visi_data_visita.year
            if visi_centre_codi_centre in self.csv_centres and do:
                count += 1
                visi_centre_nom_centre = self.csv_centres[visi_centre_codi_centre]['nom']
                visi_centre_up_centre = self.csv_centres[visi_centre_codi_centre]['up']
                if id_cip_sec in self.atdom:
                    atdom = "NO_ATDOM"
                else:
                    atdom = "SI_ATDOM"
                if count%500==0:
                    print 'Count_'+str(count)
                mes = visi_data_visita.month
                if mes == 5 and any in (2019, 2021):

                    if visi_tipus_visita in ("9C", "9R", "9D", "9T", "9E"):
                        if visi_tipus_visita == "9E" and visi_etiqueta == "ECTA":
                                visi_tipus_visita = "9Ec"
                    else:
                        visi_tipus_visita = "Altre"

                    if not visi_centre_codi_centre in self.visites_realitzades_atdom:
                        self.visites_realitzades_atdom[visi_centre_codi_centre] = c.defaultdict(dict)
                    if not any in self.visites_realitzades_atdom[visi_centre_codi_centre]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any] = c.defaultdict(dict)
                    if not mes in self.visites_realitzades_atdom[visi_centre_codi_centre][any]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes] = c.defaultdict(dict)
                    if not atdom in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom] = c.defaultdict(dict)
                    if not grup_edat in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat] = c.defaultdict(dict)
                    if not visi_lloc_visita in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita] = c.defaultdict(dict)
                    if not visi_tipus_visita in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita] = c.defaultdict(dict)
                    if not espe_especialitat in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita][espe_especialitat] = c.defaultdict(dict)
                    if not sexe in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita][espe_especialitat]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita][espe_especialitat][sexe] = c.defaultdict(dict)
                    if not visi_tipus_citacio in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita][espe_especialitat][sexe]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita][espe_especialitat][sexe][visi_tipus_citacio] = c.defaultdict(dict)
                    if id_cip_sec in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita][espe_especialitat][sexe][visi_tipus_citacio]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita][espe_especialitat][sexe][visi_tipus_citacio][id_cip_sec].append(
                            {"visi_centre_nom_centre": visi_centre_nom_centre,
                             "visi_centre_up_centre": visi_centre_up_centre,
                             "visi_servei_codi_servei": visi_servei_codi_servei})
                    else:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita][espe_especialitat][sexe][visi_tipus_citacio][id_cip_sec] = [
                            {"visi_centre_nom_centre": visi_centre_nom_centre,
                             "visi_centre_up_centre": visi_centre_up_centre,
                             "visi_servei_codi_servei": visi_servei_codi_servei}]

                    #self.visites_realitzades_atdom.append([id_cip_sec,atdom,visi_centre_codi_centre,visi_centre_nom_centre,visi_centre_up_centre,any,mes,visi_tipus_visita,visi_servei_codi_servei,grup_edat,sexe,visi_tipus_citacio,visi_lloc_visita,s_espe_codi_especialitat,espe_especialitat])
        print "Lost hash: " + str(count_hash)

    def get_visites_nou(self):
        self.visites_realitzades = c.defaultdict(dict)
        self.visites_realitzades_atdom = c.defaultdict(dict)
        sql = """SELECT visi_centre_codi_centre, visi_data_visita, visi_usuari_cip, visi_servei_codi_servei, visi_tipus_citacio, visi_lloc_visita, s_espe_codi_especialitat, visi_tipus_visita, visi_etiqueta, espe_especialitat
                 FROM vistb043
                 WHERE visi_situacio_visita = 'R' AND to_char(visi_data_visita, 'YYYY') IN (2019,2020,2021)   
              """
        count_hash = 0
        count = 0
        for visi_centre_codi_centre, visi_data_visita, hash, visi_servei_codi_servei, visi_tipus_citacio, visi_lloc_visita, s_espe_codi_especialitat, visi_tipus_visita, visi_etiqueta, espe_especialitat in u.getAll(
                sql, "redics"):
            if hash in self.hash_idcipsec:
                id_cip_sec = self.hash_idcipsec[hash]
                if id_cip_sec in self.poblacio:
                    do = True
                else:
                    do = False
            else:
                count_hash += 1
                do = False
            if visi_centre_codi_centre in self.csv_centres and do:
                count += 1
                visi_centre_nom_centre = self.csv_centres[visi_centre_codi_centre]['nom']
                visi_centre_up_centre = self.csv_centres[visi_centre_codi_centre]['up']

                any = visi_data_visita.year
                mes = visi_data_visita.month

                sexe = self.poblacio[id_cip_sec]["sexe"]  # 2348454
                edat = (visi_data_visita.date() - self.poblacio[id_cip_sec]["data_naix"]).days / 365
                if edat <= 14:
                    grup_edat = "E_00_14"
                elif edat >= 15 and edat <= 44:
                    grup_edat = "E_15_44"
                elif edat >= 45 and edat <= 64:
                    grup_edat = "E_45_64"
                elif edat >= 65 and edat <= 74:
                    grup_edat = "E_65_74"
                elif edat >= 75:
                    grup_edat = "E_M75"

                if id_cip_sec in self.atdom:
                    atdom = "NO_ATDOM"
                else:
                    atdom = "SI_ATDOM"

                if visi_tipus_visita in ("9C", "9R", "9D", "9T", "9E"):
                    if visi_tipus_visita == "9E" and visi_etiqueta == "ECTA":
                        visi_tipus_visita = "9Ec"
                else:
                    visi_tipus_visita = "Altre"

                if not visi_centre_codi_centre in self.visites_realitzades:
                    self.visites_realitzades[visi_centre_codi_centre] = c.defaultdict(dict)
                if not any in self.visites_realitzades[visi_centre_codi_centre]:
                    self.visites_realitzades[visi_centre_codi_centre][any] = c.defaultdict(dict)
                if not mes in self.visites_realitzades[visi_centre_codi_centre][any]:
                    self.visites_realitzades[visi_centre_codi_centre][any][mes] = c.defaultdict(dict)
                if id_cip_sec in self.visites_realitzades[visi_centre_codi_centre][any][mes]:
                    self.visites_realitzades[visi_centre_codi_centre][any][mes][id_cip_sec].append(
                        {"visi_tipus_visita": visi_tipus_visita, "visi_servei_codi_servei": visi_servei_codi_servei,
                         "grup_edat": grup_edat, "sexe": sexe, "visi_tipus_citacio": visi_tipus_citacio,
                         "visi_lloc_visita": visi_lloc_visita,
                         "s_espe_codi_especialitat": s_espe_codi_especialitat})
                else:
                    self.visites_realitzades[visi_centre_codi_centre][any][mes][id_cip_sec] = [
                        {"visi_tipus_visita": visi_tipus_visita, "visi_servei_codi_servei": visi_servei_codi_servei,
                         "grup_edat": grup_edat, "sexe": sexe, "visi_tipus_citacio": visi_tipus_citacio,
                         "visi_lloc_visita": visi_lloc_visita,
                         "s_espe_codi_especialitat": s_espe_codi_especialitat}]

                if mes == 5 and any in (2019, 2021):

                    if not visi_centre_codi_centre in self.visites_realitzades_atdom:
                        self.visites_realitzades_atdom[visi_centre_codi_centre] = c.defaultdict(dict)
                    if not any in self.visites_realitzades_atdom[visi_centre_codi_centre]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any] = c.defaultdict(dict)
                    if not mes in self.visites_realitzades_atdom[visi_centre_codi_centre][any]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes] = c.defaultdict(dict)
                    if not atdom in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom] = c.defaultdict(
                            dict)
                    if not grup_edat in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][
                            grup_edat] = c.defaultdict(
                            dict)
                    if not visi_lloc_visita in \
                           self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][
                               grup_edat]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][
                            visi_lloc_visita] = c.defaultdict(dict)
                    if not visi_tipus_visita in \
                           self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][
                               visi_lloc_visita]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][
                            visi_lloc_visita][
                            visi_tipus_visita] = c.defaultdict(dict)
                    if not espe_especialitat in \
                           self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][
                               visi_lloc_visita][visi_tipus_visita]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][
                            visi_lloc_visita][
                            visi_tipus_visita][espe_especialitat] = c.defaultdict(dict)
                    if not sexe in \
                           self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][
                               visi_lloc_visita][visi_tipus_visita][espe_especialitat]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][
                            visi_lloc_visita][
                            visi_tipus_visita][espe_especialitat][sexe] = c.defaultdict(dict)
                    if not visi_tipus_citacio in \
                           self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][
                               visi_lloc_visita][visi_tipus_visita][espe_especialitat][sexe]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][
                            visi_lloc_visita][
                            visi_tipus_visita][espe_especialitat][sexe][visi_tipus_citacio] = c.defaultdict(dict)
                    if id_cip_sec in \
                            self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][
                                visi_lloc_visita][visi_tipus_visita][espe_especialitat][sexe][visi_tipus_citacio]:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][
                            visi_lloc_visita][
                            visi_tipus_visita][espe_especialitat][sexe][visi_tipus_citacio][id_cip_sec].append(
                            {"visi_centre_nom_centre": visi_centre_nom_centre,
                             "visi_centre_up_centre": visi_centre_up_centre,
                             "visi_servei_codi_servei": visi_servei_codi_servei})
                    else:
                        self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][
                            visi_lloc_visita][
                            visi_tipus_visita][espe_especialitat][sexe][visi_tipus_citacio][id_cip_sec] = [
                            {"visi_centre_nom_centre": visi_centre_nom_centre,
                             "visi_centre_up_centre": visi_centre_up_centre,
                             "visi_servei_codi_servei": visi_servei_codi_servei}]

                if count % 500 == 0:
                    print 'Count_' + str(count)

    def get_resum_dades_fase1(self):
        self.resum_dades_fase1 = []
        for visi_centre_codi_centre in self.visites_realitzades:
            for any in self.visites_realitzades[visi_centre_codi_centre]:
                for mes in self.visites_realitzades[visi_centre_codi_centre][any]:
                    distints_pacients = len(self.visites_realitzades[visi_centre_codi_centre][any][mes])
                    distintes_visites = 0
                    for pacient in self.visites_realitzades[visi_centre_codi_centre][any][mes]:
                        distintes_visites += len(
                            self.visites_realitzades[visi_centre_codi_centre][any][mes][pacient])
                    self.resum_dades_fase1.append(
                        (visi_centre_codi_centre, any, mes, distints_pacients, distintes_visites))

    def get_resum_dades_fase2(self):
        self.resum_dades_fase2 = []
        for visi_centre_codi_centre in self.visites_realitzades_atdom:
            for any in self.visites_realitzades_atdom[visi_centre_codi_centre]:
                for mes in self.visites_realitzades_atdom[visi_centre_codi_centre][any]:
                    for atdom in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes]:
                        for grup_edat in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom]:
                            for visi_lloc_visita in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat]:
                                for visi_tipus_visita in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita]:
                                    for espe_especialitat in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita]:
                                        for sexe in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita][espe_especialitat]:
                                            for visi_tipus_citacio in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita][espe_especialitat][sexe]:
                                                distints_pacients = len(self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita][espe_especialitat][sexe][visi_tipus_citacio])
                                                distintes_visites = 0
                                                for pacient in self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita][espe_especialitat][sexe][visi_tipus_citacio]:
                                                    distintes_visites += len(self.visites_realitzades_atdom[visi_centre_codi_centre][any][mes][atdom][grup_edat][visi_lloc_visita][visi_tipus_visita][espe_especialitat][sexe][visi_tipus_citacio][pacient])
                                                self.resum_dades_fase2.append((visi_centre_codi_centre, any, mes, atdom, grup_edat, visi_lloc_visita, visi_tipus_visita, espe_especialitat, sexe, visi_tipus_citacio, distints_pacients, distintes_visites))

    def get_motius(self):
        self.motius = {2019: {}, 2020: {}, 2021: {}, 'total': {}}
        sql = "select mc_data, mc_motiu from vistb042"
        for mc_data, mc_motiu in u.getAll(sql, "redics"):
            any = mc_motiu.year
            if any > 2019:
                count+=1
                mes = mc_motiu.month

    """
    consultori_desc, UP, desc, sap_desc, ambit_desc, grup_edat, sexe, motiu_consulta_codi_ciap, motiu_de_consulta_desc_ciap, mes, any, count
    visi_centre_codi_centre, grup_edat, sexe, motiu_consulta_codi_ciap, motiu_de_consulta_desc_ciap, mes, any, count
    """

    def export_resum_dades_fase1(self):
        with open('fase1_nou.csv', 'wb') as f:
            writer = csv.writer(f)
            for elem in self.resum_dades_fase1:
                writer.writerow(elem)

    def export_resum_dades_fase2(self):
        with open('fase2_nou.csv', 'wb') as f:
            writer = csv.writer(f)
            for elem in self.resum_dades_fase2:
                writer.writerow(elem)

    def export_dades_fase2(self):
        with open('fase2_ok.csv', 'wb') as f:
            writer = csv.writer(f)
            for elem in self.visites_realitzades_atdom:
                writer.writerow(elem)


    def get_csv_centres(self):
        #TODO: mirar columna up
        self.csv_centres = {}
        with open(u.tempFolder + 'cerca_carol_consultoris_locals_per_descriptiu.xlsx', mode='r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=';', quotechar='|')
            for row in csv_reader:
                if row[0] != "":
                    self.csv_centres[row[-2]] = {'id': row[0], 'Gerencia teritorial': row[1], 'Codi ambit': row[2], 'Ambit': row[3], 'Codi SAP': row[4], 'SAP': row[5], 'up': row[5]}

    def get_atdom(self):
        self.atdom = set()
        sql_atdom = "SELECT id_cip_sec FROM problemes WHERE pr_cod_ps in ('C01-Z74.9', 'Z74.9')"
        for id_cip_sec, in u.getAll(sql_atdom, "import"):
            self.atdom.add(id_cip_sec)

    #TODO: Mirar tabla de problemas a ver quien es ATDOM (C01-Z74.9; Z74.9)

    def get_xlsx_centres(self):
        #TODO: mirar columna up
        import pandas as pd
        import xlrd
        self.csv_centres = {}
        with open('cerca_carol_consultoris_locals_per_descriptiu.csv', mode='r') as xlsx_file:
            csv_reader = csv.reader(xlsx_file, delimiter=';', quotechar='|')
            for row in csv_reader:
                if row[0] != "":
                    self.csv_centres[row[1]] = {'nom': row[9], 'up': row[8]}

    def get_diagnostics(self):
        self.diagnostics = []
        sectors = u.sectors
        sql = """
              select sc_v_cen, sc_cip, sc_cupseg, to_char(sc_datseg, 'YYYY') AS year, to_char(sc_datseg, 'MM') AS month, sc_coddiag
              from prstb318
              where to_char(sc_datseg, 'YYYY') in (2020,2021)
              """
        for sec in sectors:
            print "Doing_" + str(sec)
            for sc_v_cen, sc_cip, sc_cupseg, any, month, sc_coddiag in u.getAll(sql, sec):
                if sc_v_cen in self.csv_centres:
                    self.diagnostics.append([sc_v_cen, sc_cip, sc_cupseg, any, month, sc_coddiag])

    def export_dades_diagnostics(self):
        with open('diagnostics_2020_2021.csv', 'wb') as f:
            writer = csv.writer(f)
            for elem in self.diagnostics:
                writer.writerow(elem)




TEST()





