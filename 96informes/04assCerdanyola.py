#Oscar Hernandez (ion 3342), desembre14, Ermengol
#assir cerdanyola es el codi: 00406
from sisapUtils import *
import csv,os
from time import strftime

debug = False

imp = 'zimport'
ass = 'zass'
db ='test'

tAssir = 'mst_indicadors_pacient'
tu11= 'u11'
tAssig = 'assignada'
Acerda ="('00406')"
OutFile= tempFolder + 'ass_cerdanyola.txt'

printTime('Cerdanyola')
cerdanyola= {}
i=0

sql="select distinct id_cip_sec from {0} where up in {1} {2}".format(tAssir,Acerda,' limit 10' if debug else '')
for id, in getAll(sql,ass):
    cerdanyola[int(id)]=int(id)
    i+=1

printTime('U11')
u11= {}
i1=0
sql= "select id_cip_sec,id_cip from {0}".format(tu11)
for idsec, id in getAll(sql,imp):
    idsec=int(idsec)
    if idsec in cerdanyola:
        u11[int(id)]=int(id)
        i1+=1
    else:
        ok=1

cerdanyola.clear()    
printTime('pob')
pob= {}
i2=0
sql= "select id_cip,codi_sector from {0}".format(tu11)
for  id,sector in getAll(sql,imp):
    id=int(id)
    if id in u11 and sector=='6844':
        pob[id]=id
        i2+=1
    else:
        ok=1

u11.clear()
printTime('ass')
assir= {}
i3=0
sql= "select id_cip,id_cip_sec from {0}".format(tu11)
for  id,idsec in getAll(sql,imp):
    id=int(id)
    if id in pob:
        assir[int(idsec)]=int(idsec)
        i3+=1
    else:
        ok=1

        
printTime('Final')
with open(OutFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    sql="select distinct id_cip_sec,indicador,up,dni,tipus,edat,sexe,num,den,excl,llistat from {0} where up in {1} {2}".format(tAssir,Acerda,' limit 10' if debug else '')
    for id,b,c,d,e,f,g,h,i,j,k in getAll(sql,ass):
        id=int(id)
        if id in assir:
            ok=1
        else:
            w.writerow([id,b,c,d,e,f,g,h,i,j,k])
   
sql="drop table if exists test.mst_indicadors_pacient"
execute(sql,db)
sql="create table test.mst_indicadors_pacient (id_cip_sec double null,indicador varchar(10) not null default'',up varchar(5) not null default'',dni varchar(10) not null default'',tipus varchar(1) not null default'',edat double,sexe varchar(1) not null default'',num double,den double,excl double,llistat double)"
execute(sql,db)
loadData(OutFile,tAssir,db)
pob.clear()

printTime('Recalcul')
#repliquem aqui el calcul dels indicadors eqa assir pero nomes per cerdanyola
uba5="uba5"
pac = "mst_indicadors_pacient"
condicio_ecap_ind = "excl=0"
ecap_ind = "exp_ecap_uba"  
metesOrig = "ass_metes_subind_anual"
metes = "eqa_metes"
ponderacio = "ass_ponderacio"
uba_in = "zass.mst_ubas"
pathmetes="./10ass/dades_noesb/eqaass_metes_subind_anual.txt"
pathpond="./10ass/dades_noesb/ass_ponderacio_anual.txt"
pathc="./10ass/dades_noesb/ass_ind.txt"

sql="drop table if exists %s" 
execute(sql % uba5,db)
sql="create table %s (up varchar(5) not null default '',uba varchar(10) not null default '',tipus varchar(1) not null default'',indicador varchar(8) not null default '',detectats int,resolts int)" 
execute(sql % uba5,db)
sql="insert into %s select up,dni uba,tipus,indicador,sum(den) detectats,sum(num) resolts from %s where %s group by 1,2,3,4" 
execute(sql % (uba5,pac,condicio_ecap_ind),db)

sql="drop table if exists %s" 
execute(sql % ecap_ind,db)
sql="create table %s (up varchar(5) not null default '',uba varchar(10) not null default '',tipus varchar(1) not null default '', indicador varchar(8) not null default '',esperats double,detectats int,resolts int,deteccio double,deteccioPerResultat double,resolucio double,resultat double,resultatPerPunts double,punts double,llistat int)" 
execute(sql % ecap_ind,db)
sql="insert into %s select a.up, uba,tipus, a.indicador,0 esperats,if(detectats is null,0,detectats),if(resolts is null,0,resolts),0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat from %s a"
execute(sql % (ecap_ind,uba5),db)
sql="update %s set deteccio=if((detectats/esperats) is null,0,detectats/esperats),resolucio=if(detectats=0,0,resolts/detectats)" 
execute(sql % ecap_ind,db)


sql="update %s set resultat=resolucio" 
execute(sql % ecap_ind,db)
sql="drop table if exists %s" 
execute(sql % metes,db)
sql="create table %s (indicador varchar(8) not null default '',mmin double,mint double,mmax double,unique (indicador,mmin,mmax))" 
execute(sql % metes,db)
sql="drop table if exists %s" 
execute(sql % metesOrig,db)
sql="create table %s(indicador varchar(8) not null default '',z1 varchar(10) not null default '',z2 varchar(10) not null default '',meta varchar(8) not null default '',z3 varchar(10) not null default '',z4 varchar(10) not null default '',z5 varchar(10) not null default '',z6 varchar(10) not null default '',valor double)" 
execute(sql % metesOrig,db)
with open(pathmetes, 'rb') as file:
   m=csv.reader(file, delimiter='{', quotechar='|')
   for met in m:
      indicador,z1,z2,meta,z3,z4,z5,z6,valor = met[0],met[1],met[2],met[3],met[4],met[5],met[6],met[7],met[8]
      sql="insert into %s Values('%s','%s','%s','%s','%s','%s','%s','%s',%s)" 
      execute(sql % (metesOrig,indicador,z1,z2,meta,z3,z4,z5,z6,valor),db)
sql="insert into %s select a.indicador,a.valor/100,b.valor/100,c.valor/100 from (select indicador,valor from %s where meta='AGMMIN') a inner join (select indicador,valor from %s where meta='AGMINT') b on a.indicador=b.indicador inner join (select indicador,valor from %s where meta='AGMMAX') c on a.indicador=c.indicador" 
execute(sql % (metes,metesOrig,metesOrig,metesOrig),db)
sql="update %s a inner join %s b on a.indicador=b.indicador set resultatPerPunts=if(resultat>=mmax,1,if(resultat<mmin,0,(resultat-mmin)/(mmax-mmin)))" 
execute(sql % (ecap_ind,metes),db) 
sql="update %s a set llistat=detectats-resolts" 
execute(sql % ecap_ind,db)


sql="drop table if exists %s"
execute(sql % ponderacio,db)
sql="create table %s (indicador varchar(8) not null default '',tipus varchar(1) not null default '',valor double,unique (indicador,tipus,valor))" 
execute(sql % ponderacio,db)
with open(pathpond, 'rb') as file:
   pon=csv.reader(file, delimiter='@', quotechar='|')
   for pond in pon:
      indicador,mf,inf= pond[0],pond[1],pond[2]
      sql="insert into %s values('%s','G',%s)" 
      execute(sql % (ponderacio,indicador,mf),db)
      sql="insert into %s values('%s','L',%s)" 
      execute(sql % (ponderacio,indicador,inf),db) 
sql="update %s a inner join %s b on a.indicador=b.indicador and a.tipus=b.tipus set punts=resultatPerPunts*valor"
execute(sql % (ecap_ind,ponderacio),db)  
sql="alter table %s add unique (up,uba,tipus,indicador)"
execute(sql % ecap_ind,db)


with open(pathc, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9]
      sql="insert ignore into %s select up,uba,tipus,'%s' indicador,0 esperats,0 detectats,0 resolts,0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat from (select up,uba,tipus from %s where up in %s) a" 
      execute(sql % (ecap_ind,i,uba_in,Acerda),db)

#Agrupem per UP per donar resultat assir Cerdanyola

sql="drop table if exists mst_indicadors_up1"
execute(sql,db)
sql="create table mst_indicadors_up1(id_cip_sec int,indicador varchar(10) not null default'',num double,den double,excl double)"    
execute(sql,db)
sql="insert into mst_indicadors_up1 select id_cip_sec,indicador,max(num),max(den),max(excl) from mst_indicadors_pacient group by id_cip_sec,indicador"
execute(sql,db)
sql="drop table if exists mst_indicadors_up"
execute(sql,db)
sql="create table mst_indicadors_up(indicador varchar(10) not null default'',num double,den double)"    
execute(sql,db)
sql="insert into mst_indicadors_up select indicador,sum(num),sum(den) from mst_indicadors_up1 where excl=0 group by indicador"
execute(sql,db)
 
printTime('Fi')
