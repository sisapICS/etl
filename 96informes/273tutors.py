# coding: latin1

import sisapUtils as u 
import pandas as pd


class Tutors():
    def __init__(self):
        # no es pot carregar b� arxiu x tema python 2, est� la c�rrega al x002
        # self.carregar_cataleg()
        # self.get_info()
        # self.upload_taula()
        self.to_khalix()

    def carregar_cataleg(self):
        taula = pd.read_excel("2023_Tutor2022_RRHH_AP.xlsx", sheet_name='TUTORS AP', )
        taula = taula.reset_index()
        self.data = []
        for index, row in taula.iterrows():
            self.data.append((
                unicode(row['UNITAT_DOCENT']),
                unicode(row['Entitat']),
                unicode(row['TUTOR/COORDINADOR']),
                unicode(row['Cognoms i nom del tutor']),
                unicode(row['NIF SIP']),
                int(row['Nmesos']),
                int(row['Nresis']),
                unicode(row['DivUP']),
                unicode(row['Descdivup']),
                unicode(row['UP']),
                unicode(row['Descup']),
                unicode(row['DivUPsec']),
                unicode(row['DescdivUPsec']),
                unicode(row['UPsec']),
                unicode(row['Descupsec']),
                unicode(row['Categoria']),
                unicode(row['Desccategoria']),
                unicode(row['Fi'])
            ))
    
    def get_info(self):
        self.professionals = {}
        sql = """SELECT ide_dni, uab, tipus, up FROM professionals"""
        for dni, uba, tipus, up in u.getAll(sql, 'pdp'):
            self.professionals[dni[:8]] = (uba, tipus, up)
    
    def upload_taula(self):
        cols = """(unitat_docent varchar(200),
                    entitat varchar(20),
                    tutor_coordinador varchar(20),
                    nom_cog varchar(200),
                    nif varchar(20),
                    nmesos int,
                    nresis int,
                    divup varchar(20),
                    desc_divup varchar(200),
                    up varchar(20),
                    descup varchar(200),
                    divupsec varchar(20),
                    desc_divupsec varchar(200),
                    upsec varchar(20),
                    desc_upsec varchar(200),
                    categoria varchar(20),
                    desc_categoria varchar(200),
                    fi varchar(20),
                    uba varchar(20),
                    tipus varchar(20), 
                    up_sisap varchar(20))"""
        u.createTable('tutors_2023', cols, 'permanent', rm=True)
        upload = []
        for row in self.data:
            print(row)
            dni = row[4][1:9]
            if dni in self.professionals:
                uba, tipus, up = self.professionals[dni]
                up_row = list(row) + list((uba, tipus, up))
                upload.append(up_row)
        print(upload)
        u.listToTable(upload, 'tutors_2023', 'permanent')
    
    def to_khalix(self):
        sql = """SELECT distinct up, tipus, uba, nresis, NMESOS  FROM dwsisap.tutors_2023 
                    WHERE up != 'nan'
                    and tipus in ('M', 'I')"""
        upload = set()
        for up, tipus, uba, nresis, nmesos in u.getAll(sql, 'exadata'):
            try:
                upload.add(('DOCTUTOR', 'DEF2023', up + tipus + uba, 'NOCLI', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', 1))
                upload.add(('DOCNMESOS', 'DEF2023', up + tipus + uba, 'NOCLI', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', nmesos))
                upload.add(('DOCNRESIS', 'DEF2023', up + tipus + uba, 'NOCLI', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', nresis))
            except:
                print(up, tipus, uba)
        cols = """(indicador varchar(15), periode varchar(10), entitat varchar(15), d1 varchar(15),
                    d2 varchar(15), d3 varchar(15), d4 varchar(15), d5 varchar(15), val int)"""
        u.createTable('tutors_2023', cols, 'permanent', rm=True)
        u.listToTable(list(upload), 'tutors_2023', 'permanent')

        u.exportKhalix('select * from permanent.tutors_2023', 'TUTORS')
    

if __name__ == "__main__":
    Tutors()
    
