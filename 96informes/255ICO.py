from sisapUtils import *

tableResults = 'dones_sap_muntanya'
dbResult = 'pdp'
tableResultsColumns = "(id_cip varchar(10), hash_d varchar(45))"

def csv_to_dbeaver():
    createTable(tableResults, tableResultsColumns, dbResult, rm=True)
    lines = []
    with open('dones_sap_muntanya_noves.csv', mode='r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                lines.append(row)

    listToTable(lines, tableResults, dbResult)


if __name__ == "__main__":
    csv_to_dbeaver()
