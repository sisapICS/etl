

import sisapUtils as u
import collections as c


econs = set()
print('econs')
sql = """SELECT
            u.MSG_AUTOR
        FROM
            pritb941 u,
            pritb940 z
        WHERE
            u.msg_conv_id = z.conv_id
            AND u.msg_tipus_autor = 'ADM'
            AND EXTRACT(YEAR FROM conv_dini)= 2022
            AND EXTRACT(MONTH FROM conv_dini) BETWEEN 9 AND 12"""
for s in u.sectors:
    print(s)
    for adm, in u.getAll(sql, s):
        econs.add(adm[:-1])

print('admis')
num_admins = c.defaultdict(set)
sql = """SELECT
	nif,
	up_ics
FROM
	dwsisap.CAT_ADMINISTRATIUS
WHERE
	up_ics IS NOT null"""
for nif, up in u.getAll(sql, 'exadata'):
    num_admins[up].add(nif)

print('calcul')
upload = []
for up, nifs in num_admins.items():
    den = len(nifs)
    num = 0
    for admi in econs:
        if admi in nifs:
            num += 1
    upload.append((up, num, den))

cols = '(up varchar(10), num int, den int)'
u.createTable('econsulta_admi', cols, 'altres', rm=True)
u.listToTable(upload, 'econsulta_admi', 'altres')





