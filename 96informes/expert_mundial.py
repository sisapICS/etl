# -*- coding: utf8 -*-

"""
Bronquiolitis i VRS en nens
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

file = u.tempFolder + "proves.txt"
db = "redics"

resis = {}
sql = "select hash,actual,entrada,sortida from preduffa.sisap_covid_res_master"
for hash, actual, entrada, sortifa in u.getAll(sql, db):
    resis[hash] = True
    
recomptes = c.Counter()
sql = """
select data,case when residencia=1 then 'residencia' else 'No residencia' end as resi,'PCR', sum(pcr) from preduffa.sisap_covid_web_master group by data, case when residencia=1 then 'residencia' else 'No residencia' end 
union
select data,case when residencia=1 then 'residencia' else 'No residencia' end as resi,'TAR', sum(tar) from preduffa.sisap_covid_web_master group by data, case when residencia=1 then 'residencia' else 'No residencia' end 
"""
for data, resi, prova, rec in u.getAll(sql, db):
    recomptes[(data, resi, prova)] += rec
    


sql = "select hash, data,prova from preduffa.sisap_covid_pac_prv_aux where origen in ('RSA','Orfeu') and prova in ('Especific','Generic','ELISA')"
for hash, data, prova in u.getAll(sql, db):
    resi = 'residencia' if hash in resis else 'No residencia'
    recomptes[(data, resi, prova)] += 1
    
upload = []

for (data, resi, prova), n in recomptes.items():
    upload.append([data, resi, prova, n])

u.writeCSV(file, upload, sep='@')

