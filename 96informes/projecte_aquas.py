
import sisapUtils as u
import collections as c
import sisaptools as t

SIDICS_DB = ("sidics", "dbs")
DATABASE = ("exadata", "thewhole")

class EvaluacioEconomica():
    def __init__(self):
        self.get_converters()
        # self.get_population()
        # print('poblacio')
        # self.get_problemes()
        # self.get_variables()
        # self.get_eqa_atdom()
        # self.get_visites_dbs()
        # self.get_lab()
        # self.get_farmacs()
        # self.get_proves()
        # self.get_derivacions()
        self.get_visites_master_visites()
        
    def get_converters(self):
        print('pdptb101')
        sql = """SELECT cip, HASH_REDICS, HASH_COVID  FROM PDPTB101_RELACIO pr """
        self.hash_r_2_cip = {}
        self.hash_c_2_cip = {}
        for cip, hash_r, hash_c in u.getAll(sql, 'pdp'):
            self.hash_r_2_cip[hash_r] = cip  
            self.hash_c_2_cip[hash_c] = cip  
        print('u11')
        sql = """select id_cip, id_cip_sec, hash_d from import.u11"""
        self.id_cip_sec_2_id_cip = {}
        self.id_cip_2_hash = {}
        self.id_cip_sec_2_cip = {}
        for id_cip, id_cip_sec, hash in u.getAll(sql, 'import'):
            self.id_cip_sec_2_id_cip[id_cip_sec] = id_cip
            self.id_cip_2_hash[id_cip] = hash
            self.id_cip_sec_2_cip[id_cip_sec] = self.hash_r_2_cip[hash]
        print('cip to nia')
        sql = """SELECT cip, nia FROM dwsisap.RCA_CIP_NIA"""
        self.cip_2_nia = {}
        for cip, nia in u.getAll(sql, 'exadata'):
            self.cip_2_nia[cip[:-1]] = nia
    
    def get_population(self):
        print('creating')
        cols = """(nia int, data_naix date, sexe varchar2(1), 
                    gma_complexitat NUMBER(7,3), gma_n_croniques int, up varchar2(5), 
                    up_origen varchar2(5), up_abs varchar2(5), data_alta_atdom date, 
                    data_baixa_atdom date, data_alta_pcc date, data_baixa_pcc date, 
                    data_alta_maca date, data_baixa_maca date, institucionalitzat2021 varchar2(13),
                    institucionalitzat2022 varchar2(13), visites_any int, exitus2022 int)"""
        u.createTable('avaluacio_aquas_poblacio', cols, 'exadata', rm=True)
        u.grantSelect('avaluacio_aquas_poblacio','DWSISAP_ROL','exadata')
        print('population')
        self.poblacio = {}
        self.poblacio_q_entra = set()
        sql = """select c_cip, c_data_naix, c_sexe, C_GMA_COMPLEXITAT,
                C_GMA_N_CRONIQUES, c_up, c_up_origen, 
                c_up_abs, C_INSTITUCIONALITZAT, C_VISITES_ANY  
                from dbs_2021"""
        with t.Database(*SIDICS_DB) as sidics:
            for hash_r, naix, sexe, complexitat, croniques, up, up_ori, up_abs, insti, visites_any in sidics.get_all(sql):
                if hash_r in self.hash_r_2_cip:
                    cip = self.hash_r_2_cip[hash_r]
                    self.poblacio[cip] = (naix, sexe, complexitat, croniques, up, up_ori, up_abs, insti, visites_any)
        
        sql = """select c_cip, C_INSTITUCIONALITZAT from dbs.dbs_2022"""
        insti_2022 = set()
        with t.Database(*SIDICS_DB) as sidics:
            for hash_r, insti in sidics.get_all(sql):
                if insti != '':
                    if hash_r in self.hash_r_2_cip:
                        cip = self.hash_r_2_cip[hash_r]
                        insti_2022.add(cip)
        # exitus 
        exitus = set()
        sql = """select c_cip from dbs.dbs_2021 
                    where c_cip not in (select c_cip from dbs.dbs_2022)"""
        with t.Database(*SIDICS_DB) as sidics:
            for hash_r, in sidics.get_all(sql):
                if hash_r in self.hash_r_2_cip:
                    cip = self.hash_r_2_cip[hash_r]
                    exitus.add(cip)
        
        # ATOM
        print('atdoms')
        atdoms = {}
        sql = """select id, DATA, fi from dwtw.problemes
                    where DATA < DATE '2021-12-31'
                    AND codi = 'C01-Z74.9'
                    """
        with t.Database(*DATABASE) as the_whole:
            for cip, data_i, data_fi in the_whole.get_all(sql):
                atdoms[cip] = (data_i, data_fi)
                self.poblacio_q_entra.add(cip)

        # PCC MACA
        print('pcc maca')
        pcc_maca = c.defaultdict(dict)
        sql = """SELECT es_cod_u, es_dde, es_dba,
                CASE WHEN es_coD = 'ER0001' THEN 'PCC'
                WHEN es_coD = 'ER0002' THEN 'MACA' END PCC_MACA 
                FROM PRSTB715
                WHERE es_dde < DATE '2021-12-31' """
        for hash_r, dda, dba, ps in u.getAll(sql, 'redics'):
            if hash_r in self.hash_r_2_cip:
                cip = self.hash_r_2_cip[hash_r]
                pcc_maca[cip][ps] = (dda, dba)
                self.poblacio_q_entra.add(cip)
        
        print('cooking')
        upload = []
        for cip, (naix, sexe, complexitat, croniques, up, up_ori, up_abs, insti21, visites_any) in self.poblacio.items():
            if cip in self.poblacio_q_entra:
                if cip in self.cip_2_nia:
                    nia = self.cip_2_nia[cip]
                    insti22 = 1 if cip in insti_2022 else 0
                    exi = 1 if cip in exitus else 0
                    if cip in atdoms:
                        alta_atdom, baixa_atdom = atdoms[cip]
                    else:
                        alta_atdom, baixa_atdom = None, None
                    if cip in pcc_maca:
                        if 'PCC' in pcc_maca[cip]:
                            alta_pcc, baixa_pcc = pcc_maca[cip]['PCC']
                        else:
                            alta_pcc, baixa_pcc = None, None
                        if 'MACA' in pcc_maca[cip]:
                            alta_maca, baixa_maca = pcc_maca[cip]['MACA']
                        else:
                            alta_maca, baixa_maca = None, None
                    else:
                        alta_maca, baixa_maca = None, None
                        alta_pcc, baixa_pcc = None, None
                    upload.append((nia, naix, sexe, complexitat, croniques, up, up_ori, up_abs, 
                                    alta_atdom, baixa_atdom, alta_pcc, baixa_pcc, alta_maca, 
                                    baixa_maca, insti21, insti22, visites_any, exi))
        u.listToTable(upload, 'avaluacio_aquas_poblacio', 'exadata')

    def get_problemes(self):
        print('problemes')
        cols = """(nia int, ps  varchar2(5), data date)"""
        u.createTable('avaluacio_aquas_problemes', cols, 'exadata', rm=True)
        u.grantSelect('avaluacio_aquas_problemes','DWSISAP_ROL','exadata')
        problemes = []
        sql = """select id_cip_sec, ps, dde from nodrizas.eqa_problemes
                    where ps in (833, 834, 91, 92, 885, 886, 184, 79, 710, 711, 712, 141, 142, 532, 533, 107)"""
        for id_cip_sec, ps, dde in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.id_cip_sec_2_cip:
                cip = self.id_cip_sec_2_cip[id_cip_sec]
                problemes.append((cip, str(ps), dde))
        sql = """select id, DATA from dwtw.problemes
                    where codi IN (
                    'C01-F53.0',
                    'C01-F53.1',
                    'C01-F48',
                    'C01-F48.8',
                    'C01-F48.9',
                    'C01-F53',
                    'C01-F54',
                    'C01-F59',
                    'C01-F84',
                    'C01-F84.0',
                    'C01-F84.2',
                    'C01-F84.3',
                    'C01-F84.5',
                    'C01-F84.8',
                    'C01-F84.9',
                    'C01-F88',
                    'C01-F89',
                    'C01-F99',
                    'C01-Z72.6',
                    'F48.1',
                    'F48.8',
                    'F48.9',
                    'F53',
                    'F53.8',
                    'F53.9',
                    'F54',
                    'F59',
                    'F84',
                    'F84.0',
                    'F84.1',
                    'F84.2',
                    'F84.3',
                    'F84.4',
                    'F84.5',
                    'F84.8',
                    'F84.9',
                    'F88',
                    'F89',
                    'F99') AND (fi IS NULL OR (extract(YEAR FROM fi) = 2022) OR (EXTRACT(YEAR FROM data) = 2022))"""
        with t.Database(*DATABASE) as the_whole:
            for cip, data_i in the_whole.get_all(sql):
                problemes.append((cip, 'P99', data_i))
        upload = []
        for (cip, ps, data_i) in problemes:
            if cip in self.cip_2_nia:
                nia = self.cip_2_nia[cip]
                upload.append((nia, ps, data_i))
        u.listToTable(upload, 'avaluacio_aquas_problemes', 'exadata')

    def get_variables(self):
        print('variables')
        cols = """(nia int, variable varchar2(6), data date, valor NUMBER(7,3))"""
        u.createTable('avaluacio_aquas_variables', cols, 'exadata', rm=True)
        u.grantSelect('avaluacio_aquas_variables','DWSISAP_ROL','exadata')
        sql = """select id_cip_sec, agrupador, data_var, valor 
                from nodrizas.eqa_variables
                where agrupador in (87, 334, 89, 330, 105, 908, 909, 751)
                and data_var between '2021-01-01' and '2022-12-31'"""
        variables = set()
        for id_cip_sec, agrupador, data_var, valor in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.id_cip_sec_2_cip:
                cip = self.id_cip_sec_2_cip[id_cip_sec]
                variables.add((cip, str(agrupador), data_var, valor))
        
        sql = """select id_cip_sec, vu_cod_vs, vu_val, vu_dat_act 
                from import.variables2
                where vu_cod_vs = '6494'"""
        for id_cip_sec, agrupador, valor, data_var in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.id_cip_sec_2_cip:
                cip = self.id_cip_sec_2_cip[id_cip_sec]
                variables.add((cip, agrupador, data_var, valor))
        upload = []
        for (cip, variable, data, valor) in variables:
            if cip in self.cip_2_nia:
                nia = self.cip_2_nia[cip]
                upload.append((nia, variable, data, valor))
        u.listToTable(upload, 'avaluacio_aquas_variables', 'exadata')

    def get_eqa_atdom(self):
        print('eqa_atdom')
        cols = """(nia int, indicador varchar2(10), numerador int, data date)"""
        u.createTable('avaluacio_aquas_eqa_atdom', cols, 'exadata', rm=True)
        u.grantSelect('avaluacio_aquas_eqa_atdom','DWSISAP_ROL','exadata')
        eqa_atdom = set()
        sql = """select id_cip_sec, ind_codi, num, data_ext
                from eqa_ind.mst_indicadors_pacient, nodrizas.dextraccio b
                where ind_codi in ('EQA0401A', 'EQA0402A', 'EQA0403A', 'EQA0404A')"""
        for id_cip_sec, indi, num, dext in u.getAll(sql, 'eqa_ind'):
            if id_cip_sec in self.id_cip_sec_2_cip:
                cip = self.id_cip_sec_2_cip[id_cip_sec]
                eqa_atdom.add((cip, indi, num, dext))
        sql = """SELECT idpac, indicador FROM altllistats WHERE INDICADOR in ('ATD024A', 'ATD023A') AND exclos = 0"""
        for hash_r, indi in u.getAll(sql, 'pdp'):
            if hash_r in self.hash_r_2_cip:
                cip = self.hash_r_2_cip[hash_r]
                eqa_atdom.add((cip, indi, 1, dext))
        upload = []
        for (cip, indi, num, dext) in eqa_atdom:
            if cip in self.cip_2_nia:
                nia = self.cip_2_nia[cip]
                upload.append((nia, indi, num, dext))
        u.listToTable(upload, 'avaluacio_aquas_eqa_atdom', 'exadata')
    
    def get_visites_dbs(self):
        print('visites dbs')
        upload = []
        cols = """(nia int, vi_m_9c9r int, vi_m_9d int, vi_m_9t int, 
                vi_m_9e_ecta int, vi_i_9c9r int, vi_i_9d int, vi_i_9t int, 
                vi_i_9e_ecta int)"""
        u.createTable('avaluacio_aquas_n_visites', cols, 'exadata', rm=True)
        u.grantSelect('avaluacio_aquas_n_visites','DWSISAP_ROL','exadata')
        sql = """select c_cip, vi_m_9c9r, vi_m_9d, vi_m_9t, 
                vi_m_9e_ecta, vi_i_9c9r, vi_i_9d, vi_i_9t, 
                vi_i_9e_ecta from dbs.dbs_2022"""
        with t.Database(*SIDICS_DB) as sidics:
            for row in sidics.get_all(sql):
                row = list(row)
                if row[0] in self.hash_r_2_cip:
                    cip = self.hash_r_2_cip[row[0]]
                    if cip in self.cip_2_nia:
                        row[0] = self.cip_2_nia[cip]
                        row = tuple(row)
                        upload.append(row)
        u.listToTable(upload, 'avaluacio_aquas_n_visites', 'exadata')

    def get_lab(self):
        print('lab')
        # cols = """(nia int, lab varchar2(5), prova varchar2(6), data date)"""
        # u.createTable('avaluacio_aquas_laboratori', cols, 'exadata', rm=True)
        # u.grantSelect('avaluacio_aquas_laboratori','DWSISAP_ROL','exadata')
        # laboratori = []
        # sql = """SELECT id, lab, prova, data FROM dwtw.LABORATORI_DETALL
        #         WHERE DATA_MODI >= DATE '2022-01-01'
        #         AND DATA BETWEEN DATE '2022-01-01' AND DATE '2023-01-01'"""
        # with t.Database(*DATABASE) as the_whole:
        #     for row in the_whole.get_all(sql):
        #         row = list(row)
        #         if row[0] in self.cip_2_nia:
        #             row[0] = self.cip_2_nia[row[0]]
        #             row = tuple(row)
        #             laboratori.append((row))
        # u.listToTable(laboratori, 'avaluacio_aquas_laboratori', 'exadata')
        # executar des de terminal / dbeaver
        # CREATE TABLE lab_2022 AS (
        # SELECT id, lab, prova, data FROM dwtw.LABORATORI_DETALL
        #                 WHERE DATA_MODI >= DATE '2022-01-01'
        #                 AND DATA BETWEEN DATE '2022-01-01' AND DATE '2022-12-31')
        # CREATE INDEX pacients ON dwtw.lab_2022(id);
        # GRANT SELECT ON DWTW.lab_2022 TO DWSISAP_ROL;
        # CREATE TABLE cip_nia AS (SELECT cip, nia FROM dwsisap.rca_cip_nia);
        # GRANT SELECT ON cip_nia TO DWSISAP_ROL
        # CREATE INDEX cip ON dwsisap.cip_nia(cip);
        # CREATE TABLE avaluacio_aquas_laboratori AS (SELECT n.nia, l.lab, l.prova, l.DATA FROM DWTW.lab_2022 l INNER JOIN dwsisap.cip_nia n ON n.cip = l.id)

    
    def get_visites_master_visites(self):
        print('visites master')
        cols = """(nia int,
                    VI_GC_9C9R int,
                    VI_GC_9D int,
                    VI_GC_9T int,
                    VI_GC_9E_ECTA int,
                    VI_PADES_M_9C9R int,
                    VI_PADES_M_9D int,
                    VI_PADES_M_9T int,
                    VI_PADES_M_9E_ECTA int,
                    VI_PADES_I_9C9R int,
                    VI_PADES_I_9D int,
                    VI_PADES_I_9T int,
                    VI_PADES_I_9E_ECTA int,
                    VI_AC_9C9R int,
                    VI_AC_9D int,
                    VI_AC_9T int,
                    VI_AC_9E_ECTA int,
                    VI_TS_9C9R int,
                    VI_TS_9D int,
                    VI_TS_9T int,
                    VI_TS_9E_ECTA int)"""
        u.createTable('avaluacio_aquas_visites', cols, 'exadata', rm=True)
        u.grantSelect('avaluacio_aquas_visites','DWSISAP_ROL','exadata')
        sql = """SELECT PACIENT, DATA, SERVEI, TIPUS, ETIQUETA, SISAP_CATPROF_CLASS 
                    FROM dwsisap.sisap_master_visites
                    WHERE DATA BETWEEN DATE '2022-01-01' AND DATE '2023-01-01'
                    AND tipus IN ('9C', '9R', '9D', '9T', '9E')
                    AND SERVEI IN ('GCAS', 'UGC', 'INFG', 'PADES', 'AC', 'TS')
                    AND situacio = 'R'
                    """
        visites_master = c.defaultdict(c.Counter)
        for pacient, data, servei, tipus, etiqueta, prof in u.getAll(sql, 'exadata'):
            if servei in ('GCAS', 'UGC', 'INFG'):
                servei = 'GC'
            elif servei == 'PADES':
                if prof in ('INF', 'MF'):
                    servei = servei + '_' + prof
            if tipus == '9E':
                if etiqueta == 'ECTA':
                    visites_master[pacient][(servei, '9E_ECTA')] += 1
            elif tipus in ('9C', '9R'):
                visites_master[pacient][(servei, '9C9R')] += 1
            else:
                visites_master[pacient][(servei, tipus)] += 1
        
        upload = []
        for pacient, visites in visites_master.items():
            if pacient in self.hash_c_2_cip:
                info = [0]*21
                cip = self.hash_c_2_cip[pacient]
                if cip in self.cip_2_nia:
                    nia = self.cip_2_nia[cip]
                    info[0] = nia
                    for (servei, tipus), val in visites.items():
                        if servei == 'GC':
                            if tipus == '9C9R': info[1] = val
                            if tipus == '9D': info[2] = val
                            if tipus == '9T': info[3] = val
                            if tipus == '9E_ECTA': info[4] = val
                        elif servei == 'PADES_MF':
                            if tipus == '9C9R': info[5] = val
                            if tipus == '9D': info[6] = val
                            if tipus == '9T': info[7] = val
                            if tipus == '9E_ECTA': info[8] = val
                        elif servei == 'PADES_INF':
                            if tipus == '9C9R': info[9] = val
                            if tipus == '9D': info[10] = val
                            if tipus == '9T': info[11] = val
                            if tipus == '9E_ECTA': info[12] = val
                        elif servei == 'AC':
                            if tipus == '9C9R': info[13] = val
                            if tipus == '9D': info[14] = val
                            if tipus == '9T': info[15] = val
                            if tipus == '9E_ECTA': info[16] = val
                        elif servei == 'TS':
                            if tipus == '9C9R': info[17] = val
                            if tipus == '9D': info[18] = val
                            if tipus == '9T': info[19] = val
                            if tipus == '9E_ECTA': info[20] = val
                    info = tuple(info)
                    upload.append(info)
        u.listToTable(upload, 'avaluacio_aquas_visites', 'exadata')
    
    def get_derivacions(self):
        print('derivacions')
        cols = """(nia int, 
                    oc_data date,
                    INF_DATA_BAIXA date,
                    inf_codi_prova varchar2(10),
                    agrupador_prova varchar2(40),
                    INF_SERVEI_D_CODI varchar2(5),
                    INF_SERVEI_D_CODI_DESC varchar2(240),
                    CODI_ESPECIALITAT_FINAL varchar2(40),
                    ESPECIALITAT_FINAL varchar2(40),
                    der_data_prog date,
                    der_data_real date,
                    der_data_baix date,
                    der_up_scs_des varchar2(5),
                    DER_UP_SCS_DES_DESC varchar2(200),
                    der_tipus_fluxe varchar2(10),
                    FLAG_PETICIO_PROGRAMADA int,
                    FLAG_PETICIO_REALITZADA int,
                    FLAG_PETICIO_REBUTJADA int,
                    flag_peticio_pendent int)"""
        u.createTable('avaluacio_aquas_derivacions', cols, 'exadata', rm=True)
        u.grantSelect('avaluacio_aquas_derivacions','DWSISAP_ROL','exadata')
        proves = []
        sql = """SELECT 
                    hash, 
                    oc_data,
                    inf_data_baixa,
                    inf_codi_prova,
                    agrupador_prova,
                    INF_SERVEI_D_CODI,
                    INF_SERVEI_D_CODI_DESC,
                    CODI_ESPECIALITAT_FINAL,
                    ESPECIALITAT_FINAL,
                    der_data_prog,
                    DER_DATA_REAL,
                    DER_DATA_BAIX,
                    der_up_scs_des,
                    DER_UP_SCS_DES_DESC,
                    der_tipus_fluxe, 
                    FLAG_PETICIO_PROGRAMADA,
                    FLAG_PETICIO_REALITZADA,
                    FLAG_PETICIO_REBUTJADA,
                    FLAG_PETICIO_PENDENT
                    FROM dwsisap.master_derivacions
                    WHERE agrupador_prova IN ('Primera presencial', 'No presencial')
                    AND FLAG_PETICIO_SOLLICITADA = 1
                    AND oc_data BETWEEN DATE '2021-01-01' AND DATE '2022-12-31'"""
        for row in u.getAll(sql, 'exadata'):
            row = list(row)
            if row[0] in self.hash_c_2_cip:
                cip = self.hash_c_2_cip[row[0]]
                if cip in self.cip_2_nia:
                    row[0] = self.cip_2_nia[cip]
                    row = tuple(row)
                    proves.append((row))
        u.listToTable(proves, 'avaluacio_aquas_derivacions', 'exadata')

    def get_proves(self):
        print('proves')
        cols = """(nia int, 
                    oc_data date,
                    INF_DATA_BAIXA date,
                    INF_CODI_PROVA varchar2(10),
                    INF_DES_PROVA varchar2(70),
                    der_data_prog date,
                    der_data_real date,
                    der_data_baix date,
                    der_up_scs_des varchar2(5),
                    DER_UP_SCS_DES_DESC varchar2(200),
                    der_tipus_fluxe varchar2(10),
                    FLAG_PETICIO_PROGRAMADA int,
                    FLAG_PETICIO_REALITZADA int,
                    FLAG_PETICIO_REBUTJADA int,
                    flag_peticio_pendent int)"""
        u.createTable('avaluacio_aquas_proves', cols, 'exadata', rm=True)
        u.grantSelect('avaluacio_aquas_proves','DWSISAP_ROL','exadata')
        proves = []
        sql = """SELECT
                    hash,
                    oc_data,
                    INF_DATA_BAIXA,
                    INF_CODI_PROVA,
                    INF_DES_PROVA,
                    der_data_prog,
                    der_data_real,
                    der_data_baix,
                    der_up_scs_des,
                    DER_UP_SCS_DES_DESC,
                    der_tipus_fluxe,
                    FLAG_PETICIO_PROGRAMADA,
                    FLAG_PETICIO_REALITZADA,
                    FLAG_PETICIO_REBUTJADA,
                    flag_peticio_pendent
                FROM
                    dwsisap.master_proves
                WHERE
                    FLAG_PETICIO_SOLLICITADA = 1
                    AND oc_data BETWEEN DATE '2021-01-01' AND DATE '2022-12-31'"""
        for row in u.getAll(sql, 'exadata'):
            row = list(row)
            if row[0] in self.hash_c_2_cip:
                cip = self.hash_c_2_cip[row[0]]
                if cip in self.cip_2_nia:
                    row[0] = self.cip_2_nia[cip]
                    row = tuple(row)
                    proves.append((row))
        u.listToTable(proves, 'avaluacio_aquas_proves', 'exadata')
        

    def get_farmacs(self):
        print('farmacs')
        farmacs = set()
        cols = """(nia int, farmac varchar2(7), data_inici date, data_fi date)"""
        u.createTable('avaluacio_aquas_farmacs', cols, 'exadata', rm=True)
        u.grantSelect('avaluacio_aquas_farmacs','DWSISAP_ROL','exadata')
        sql = """select id_cip_sec, ppfmc_atccodi, ppfmc_pmc_data_ini, ppfmc_data_fi 
                from import.tractaments
                where ppfmc_pmc_data_ini between DATE '2022-01-01' and DATE '2022-12-31'
                or ppfmc_data_fi between DATE '2022-01-01'and DATE '2022-12-31'
                or (ppfmc_pmc_data_ini < DATE '2022-01-01' and ppfmc_data_fi > DATE '2022-12-31')"""
        for id_cip_sec, atccodi, inici, fi in u.getAll(sql, 'import'):
            if id_cip_sec in self.id_cip_sec_2_cip:
                cip = self.id_cip_sec_2_cip[id_cip_sec]
                farmacs.add((cip, atccodi, inici, fi))
        upload = []
        for (cip, atccodi, inici, fi) in farmacs:
            if cip in self.cip_2_nia:
                nia = self.cip_2_nia[cip]
                upload.append((nia, atccodi, inici, fi))
        u.listToTable(upload, 'avaluacio_aquas_farmacs', 'exadata')


if __name__ == "__main__":
    try:
        EvaluacioEconomica()
    except Exception as e:
        mail = t.Mail()
        mail.to.append("roser.cantenys@catsalut.cat")
        mail.subject = "PROJECTE AQUAS!!!"
        mail.text = str(e)
        mail.send()    







