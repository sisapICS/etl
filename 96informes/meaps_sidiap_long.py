import sisapUtils as u
import sisaptools as t
import collections as c
import pandas as pd

# Carreguem Excel
file_path = '562.xlsx'
df = pd.read_excel(file_path, header=None)
df[['hash', 'sector']] = df[0].str.split(':', expand=True)
hashos = dict(zip(df['hash'], df['sector']))


sectors_cips = c.defaultdict(set)
cip_2_hash = {}
sql = """SELECT usua_cip, usua_cip_cod FROM PDPTB101"""
for cip, hash in u.getAll(sql, 'pdp'):
    if hash in hashos:
        sectors_cips[hashos[hash]].add(cip)
        cip_2_hash[cip] = hash

TMP_ECAP = "REC_AVISOS_BKUP"

upload = [(cip, "66666") + (None,) * 10 for cip in cip_2_hash.keys()]
for sector in u.sectors:
    print(sector)
    if sector != "6951":
        with t.Database(sector, "data") as conn:
            conn.execute("delete {} where usua_uab_up = '66666'".format(TMP_ECAP))  # noqa
            conn.list_to_table(upload, TMP_ECAP)

info = []
sql = """ SELECT sc_cip, sc_datseg, sc_tipus, sc_textseg FROM prstb318
            INNER JOIN REC_AVISOS_BKUP on sc_cip = USUA_CIP WHERE USUA_UAB_UP = '66666'"""
for sector in sectors_cips.keys():
    print(sector)
    for cip, data, tipus, txt in u.getAll(sql, str(sector)):
        txt = str(txt).replace('\r\n', ' ')
        info.append((cip_2_hash[cip], data, tipus, txt))

for sector in u.sectors:
    if sector != "6951":
        with t.Database(sector, "data") as conn:
            conn.execute("delete {} where usua_uab_up = '66666'".format(TMP_ECAP))  # noqa

print(info[0])
u.writeCSV('MEAPS_SISAP562.csv', info, sep=';')




