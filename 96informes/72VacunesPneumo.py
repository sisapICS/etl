# coding: iso-8859-1
#Carmen Cabezas penumococ, Ermengol, Novembre 2016
from sisapUtils import *
from collections import defaultdict, Counter

imp = 'import'
db = 'test'
nod = 'nodrizas'

temp = 'cat_vacunes'

dini ='20150101'
dfi = '20161031'

sql = 'select year(data_ext) from dextraccio'
dext, = getOne(sql, nod)

centres = {}
sql = "select scs_codi, ics_codi, ics_desc, sap_desc, amb_desc from cat_centres where ep='0208' and ics_codi like 'BR%'"
for up, br, desc, sap, ambit in getAll(sql, nod):
    centres[up] = {'eap': desc, 'sap': sap, 'amb': ambit}

pob = {}
sql = 'select id_cip_sec, up, edat from assignada_tot where edat>14'
for id, up, edat in getAll(sql, nod):
    gedat = 0
    if edat <60:
        gedat = 'Menors 60'
    elif 60 <= edat <= 64:
        gedat = 'Entre 60 i 64'
    elif edat >64:
        gedat = 'Majors 65'
    else:
        gedat = 'error'
    try:
        eap = centres[up]['eap']
        sap = centres[up]['sap']
        ambit = centres[up]['amb']
    except KeyError:
        continue
    pob[id] = {'up': up, 'edat': gedat, 'eap': eap, 'sap': sap, 'ambit': ambit}


#vacunes administrades
vacpneumo = '(48)'

sql = 'drop table if exists {}'.format(temp)
execute(sql, db)
sql = "create table {} as select vacu_cod vacuna,if(vacu_an_cod = '',hom_an_cod,vacu_an_cod) antigen,if(vacu_tipus_dosis = 0,300,vacu_tipus_dosis) dosis,if(vacu_model_nou='S','NEW','OLD') model from import.cat_prstb040 a left join import.cat_prstb062 b on a.vacu_cod_hom=b.hom_ch_cod".format(temp)
execute(sql, db)

Antigen = {}
sql = "select vacuna from {0} where antigen='A00028'".format(temp)
for vac, in getAll(sql, db):
    Antigen[vac] = True

nVacunes = Counter()
sql = "select table_name from tables where table_schema='{0}' and table_name like '{1}_%'".format(imp, 'vacunes_s')
for particio, in getAll(sql, ('information_schema', 'aux')):
    printTime(particio)
    vacunes = {}
    sql = "select id_cip_sec,va_u_cod,date_format(va_u_data_vac,'%Y%m'),date_format(va_u_data_vac,'%Y%m%d') from {0} where va_u_data_baixa=0".format(particio)
    for id, vac, anys, dat in getAll(sql, imp):
        if dini <= dat <= dfi:
            try:
                Antigen[vac]
            except KeyError:
                continue
            try:
                up = pob[id]['up']
                edat = pob[id]['edat']
                eap = pob[id]['eap']
                sap = pob[id]['sap']
                ambit = pob[id]['ambit']
            except KeyError:
                continue
            nVacunes[(up, eap, sap, ambit, edat, anys)] += 1

upload = []
for (up, eap, sap, ambit,edat, anys), d in nVacunes.items():
    upload.append([up, eap, sap, ambit, edat, anys, d])

file = tempFolder + 'pneumococ_per mes.txt'
writeCSV(file, upload, sep=';')


#Cobertura actual

frisc = {}
sql = 'select id_cip_sec from eqa_problemes where ps in (62, 1, 21, 275, 180, 53, 24, 18,101, 40)'
for id, in getAll(sql, nod):
    frisc[id] = True
ndosis = {}    
sql = 'select id_cip_sec, dosis from eqa_vacunes where agrupador=48'
for id, dosis in getAll(sql, nod):
    if dosis >2:
        dosis = 'mes de dues'
    ndosis[id] = dosis

cobertura = Counter()
for (id), valors in pob.items():
    up = valors['up']
    eap = valors['eap']
    sap = valors['sap']
    ambit = valors['ambit']
    edat = valors['edat']
    factorsrisc = 'No'
    if id in frisc:
        factorsrisc = 'Si'
    nd = 0
    if (id) in ndosis:
        nd = ndosis[id]
    cobertura[(up, eap, sap, ambit, edat, factorsrisc, nd)] += 1

upload = []
for (up, eap, sap, ambit,edat, factorsrisc, nd), d in cobertura.items():
    upload.append([up, eap, sap, ambit, edat, factorsrisc, nd, d])

file = tempFolder + 'CoberturaPneumococ_' + str(dext) + '.txt'
writeCSV(file, upload, sep=';')
