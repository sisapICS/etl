import sisapUtils as u
import sisaptools as t
import datetime as d
import collections as c
from dateutil import relativedelta as rd

u.printTime('inici')
DEXTD = u.getOne('select data_ext from dextraccio', 'nodrizas')[0]
menys1 = DEXTD - rd.relativedelta(years=1)


sql = """
    SELECT id_cip_sec, up
    FROM assignada_tot
"""
pob = {id: up for id, up in u.getAll(sql, 'nodrizas')}
u.printTime('pob')
fragil = set()
sql = """
    SELECT id_cip_sec, if(es_cod = 'ER0001', 1, 0), if (es_cod = 'ER0002', 1, 0)
    FROM estats
"""
for id, pcc, maca in u.getAll(sql, 'import'):
    if id in pob:
        fragil.add(id)
u.printTime('estats')
sql = """
    SELECT id_cip_sec
    FROM assignada_tot
    WHERE atdom = 1
    OR institucionalitzat = 1
    OR pcc = 1
    OR maca = 1
"""
for id, in u.getAll(sql, 'nodrizas'):
    if id in pob:
        fragil.add(id)
u.printTime('fragil')
sql = """
    SELECT id_cip_sec
    FROM eqa_variables
    WHERE agrupador = 918
    AND valor >= 0.2
    AND data_var >= DATE '{menys1}'
""".format(menys1=menys1)
for id, in u.getAll(sql, 'nodrizas'):
    if id in pob:
        fragil.add(id)
u.printTime('vig')

tables = u.getSubTables('tractaments', 'import')
sql = """
    SELECT id_cip_sec
    FROM {tb} t
    WHERE ppfmc_data_fi > DATE '{DEXTD}'
    AND (ppfmc_durada > 360 OR ppfmc_seg_evol = 'S')
    AND length(ppfmc_atccodi) = 7
    AND NOT EXISTS
        (SELECT 1
        FROM cat_cpftb006 c
        WHERE
        c.pf_codi = t.ppfmc_pf_codi
        AND (c.pf_ff_codi in ('GE','PO','CL')
        OR c.pf_via_adm = 'B31'
        OR c.pf_gt_codi like '23C%'))
"""

tract = c.Counter()
for tb in tables:
    for id, in u.getAll(sql.format(tb=tb, DEXTD=DEXTD), 'import'):
        if id in pob:
            tract[id] += 1
    u.printTime(tb)
u.printTime('tractaments')
polif = set()
for id in tract:
    if id in pob and tract[id] >= 10:
        polif.add(id)
u.printTime('cuinetes')
res = c.defaultdict(c.Counter)
for id in fragil:
    if id in pob:
        up = pob[id]
        res[('polif_fragil', up)]['DEN'] += 1
        if id in polif:
            res[('polif_fragil', up)]['NUM'] += 1

upload = [('indicador', 'up', 'numerador', 'denominador', 'resultat')]
for (ind, up) in res:
    num = res[(ind, up)]['NUM'] if 'NUM' in res[(ind, up)] else 0
    den = res[(ind, up)]['DEN'] if 'DEN' in res[(ind, up)] else 0
    resultat = float(num)*100/float(den) if den != 0 else 0
    upload.append((ind, up, num, den, resultat))
u.writeCSV('polif_fragil.csv', upload, sep=',')
u.printTime('fi')
# CODI POLIMEDICATS 2025
# sql = """
#     SELECT id_cip_sec, up, uba
#     FROM assignada_tot
# """
# pob = {id: (up, uba) for id, up, uba in u.getAll(sql, 'nodrizas')}
# u.printTime('pob')
# fragil = set()
# sql = """
#     SELECT id_cip_sec, if(es_cod = 'ER0001', 1, 0), if (es_cod = 'ER0002', 1, 0)
#     FROM estats
# """
# for id, pcc, maca in u.getAll(sql, 'import'):
#     if id in pob:
#         fragil.add(id)
# u.printTime('estats')
# sql = """
#     SELECT id_cip_sec
#     FROM assignada_tot
#     WHERE atdom = 1
#     OR institucionalitzat = 1
#     OR pcc = 1
#     OR maca = 1
# """
# for id, in u.getAll(sql, 'nodrizas'):
#     if id in pob:
#         fragil.add(id)
# u.printTime('fragil')
# sql = """
#     SELECT id_cip_sec
#     FROM variables1
#     WHERE vu_cod_vs = 'VA0302'
#     AND vu_val > 0.2
# """
# for id, in u.getAll(sql, 'import'):
#     if id in pob:
#         fragil.add(id)
# u.printTime('vig')
# gerontopole = set()
# sql = """
#     SELECT id_cip_sec
#     FROM eqa_variables
#     WHERE agrupador = 917
#     AND valor >= 1
#     AND data_var >= DATE '{menys1}'
# """.format(menys1=menys1)
# for id, in u.getAll(sql, 'nodrizas'):
#     if id in pob:
#         gerontopole.add(id)
# u.printTime('gerontopole')

# sql = """
#     SELECT id_cip_sec
#     FROM {tb} t
#     WHERE ppfmc_data_fi > DATE '{DEXTD}'
#     AND (ppfmc_durada >= 84 OR ppfmc_seg_evol = 'S' OR ppfmc_durada is null)
#     AND length(ppfmc_atccodi) = 7
#     AND NOT EXISTS
#         (SELECT 1
#         FROM cat_cpftb006 c
#         WHERE
#         c.pf_codi = t.ppfmc_pf_codi
#         AND c.pf_gt_codi like '23C%')
# """

# polif = c.Counter()
# for tb in u.getSubTables('tractaments'):
#     for id, in u.getAll(sql.format(tb=tb, DEXTD=DEXTD), 'import'):
#         polif[id] += 1
#     u.printTime(tb)
# u.printTime('polif')
# polif10 = set()
# polifm10 = set()
# for id in polif:
#     if polif[id] >= 10:
#         polif10.add(id)
#     if polif[id] > 10:
#         polifm10.add(id)
# u.printTime('cuinetes')
# indicadors = {
#     '>10': (polifm10, None),
#     '>=10': (polif10, None),
#     'fragil >10': (polifm10, fragil | gerontopole),
#     'fragil >=10': (polif10, fragil | gerontopole),
#     'frag_no_gfst >10': (polifm10, fragil - gerontopole),
#     'frag_no_gfst >=10': (polif10, fragil - gerontopole),
# }
# recompte = c.defaultdict(c.Counter)
# for ind in indicadors:
#     (num, den) = indicadors[ind]
#     if not den:
#         den = pob
#     for id in den:
#         if id in pob:
#             recompte[ind]['DEN'] += 1
#             if id in num:
#                 recompte[ind]['NUM'] += 1
# u.printTime('fi')
# for ind in recompte:
#     num = recompte[ind]['NUM'] if 'NUM' in recompte[ind] else 0
#     den = recompte[ind]['DEN'] if 'DEN' in recompte[ind] else 0
#     res = float(num)*100/float(den) if den != 0 else 0
#     print(str(ind),str(num), str(den), str(res))