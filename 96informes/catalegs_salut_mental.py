# -*- coding: utf-8 -*-

from sisapUtils import *
from collections import defaultdict, Counter
import datetime
import pandas as pd

up = pd.read_excel("entitats_proveidores.xlsx")
up = up.to_numpy()

upload = []
for e in up:
    e[0] = str(e[0])
    if len(e[0]) < 4:
        while len(e[0]) != 4:
            e[0] = '0' + e[0]
    e[2] = str(e[2])
    if len(e[2]) != 5:
        while len(e[2]) != 5:
            e[2] = '0' + e[2]
    upload.append((e[0], e[1], e[2], e[3]))

create = '(codi_ep varchar(4), desc_ep varchar(60), codi_up varchar(5), nom_complet_up varchar(60))'
createTable('sisap_cat_ep_up_sm',create,'redics', rm=True)
listToTable(upload, 'sisap_cat_ep_up_sm', 'redics')
print('taula 1 done')


up = pd.read_excel("centres_sm.xlsx")
up = up.to_numpy()

upload = []
for e in up:
    e[4] = str(e[4][0:5])
    if len(e[4]) != 5:
        while len(e[4]) < 5:
            e[4] = '0' + e[4]
    e[6] = str(e[6])
    if len(e[6]) != 5:
        while len(e[6]) < 5:
            e[6] = '0' + e[6]
    e[8] = str(e[8])
    if len(e[8]) != 5:
        while len(e[8]) < 5:
            e[8] = '0' + e[8]
    upload.append((e[4], e[5], e[6], e[7], e[8], e[9]))

create = '(sm_eap_up varchar(5), sm_eap_desc varchar(60), sm_csma_up varchar(5), sm_csma_desc varchar(50), sm_csmij_up varchar(5), sm_csmij_desc varchar(40))'
createTable('sisap_cat_centres_sm',create,'redics', rm=True)
listToTable(upload, 'sisap_cat_centres_sm', 'redics')
print('taula 2 done')





