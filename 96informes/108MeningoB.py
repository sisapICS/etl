# coding: iso-8859-1

from sisapUtils import *
import csv
import os
import sys
import math
from time import strftime
from collections import defaultdict, Counter

nod = 'nodrizas'
imp = 'import'

meningoB = 'A00035'

anyCalcul = '2017'

centres = {}
sql = "select scs_codi, sap_codi, sap_desc, amb_codi, amb_desc, ics_desc from cat_centres where ep='0208'"
for up, sap, sapdesc, amb, ambdesc, desc in getAll(sql, nod):
    centres[up] = {'sap':sap, 'sapdesc':sapdesc, 'amb':amb, 'ambdesc':ambdesc,'desc':desc}
    
pob = {}
sql = 'select id_cip_sec, up, data_naix from ped_assignada'
for id, up, datnaix in getAll(sql, nod):
    try:
        sap = centres[up]['sap']
        sapdesc = centres[up]['sapdesc']
        amb = centres[up]['amb']
        ambdesc = centres[up]['ambdesc']
        desc = centres[up]['desc']
    except KeyError:
        continue
    pob[id] = {'up': up, 'sap':sap, 'sapdesc':sapdesc, 'amb':amb, 'ambdesc':ambdesc, 'naix':datnaix, 'desc':desc}
    
EMI = "('M03.0','M01.0','A39.0','A39.4','A39.9','A39','A39.2','A39.3','A39.5','A39.8','G01')"
rec_emi = Counter()
sql = "select id_cip_sec, pr_cod_ps, pr_dde from problemes, nodrizas.dextraccio where  pr_cod_o_ps = 'C' and pr_hist = 1 and pr_cod_ps in {0} and   pr_dde <= data_ext and (pr_data_baixa is null or pr_data_baixa > data_ext)".format(EMI)
for id, ps, dde in getAll(sql, imp):
    rec_emi[id] += 1
grupr = "('D84.1', 'D57.0','D57.1','D57.2','D57.8','D57')"   
risc = {}
sql = "select id_cip_sec, pr_cod_ps, pr_dde from problemes, nodrizas.dextraccio where  pr_cod_o_ps = 'C' and pr_hist = 1 and pr_cod_ps in {0} and   pr_dde <= data_ext and (pr_data_baixa is null or pr_data_baixa > data_ext) \
        and (pr_dba is null or pr_dba > data_ext)".format(grupr)
for id, ps, dde in getAll(sql, imp):
    risc[id] = True
    
Antigen = {}
sql = "select vacuna from cat_prstb040_new where antigen='{0}'".format(meningoB)
for vac, in getAll(sql, imp):
    Antigen[vac] = True
    
recompte = Counter()
sql = "select table_name from tables where table_schema='{0}' and table_name like '{1}%'".format(imp, 'vacunes_s')
for particio, in getAll(sql, ('information_schema', 'aux')):
    vacunes = {}
    sql = "select id_cip_sec,va_u_cod, va_u_data_vac, year(va_u_data_vac), va_u_stock_ext from {0} where va_u_data_baixa=0".format(particio)
    for id, vac, dataVac, anyVac, extern in getAll(sql, imp):
        try:
            Antigen[vac]
        except KeyError:
            continue
        try:
            sap = pob[id]['sap']
            sapdesc = pob[id]['sapdesc']
            amb = pob[id]['amb']
            ambdesc = pob[id]['ambdesc']
            desc = pob[id]['desc']
            up = pob[id]['up']
            datanaix = pob[id]['naix']
        except KeyError:
            continue
        edat_m = monthsBetween(datanaix, dataVac)
        edat_a = edat_m/12
        edat_a = math.trunc(edat_a)
        frisc = 0
        if id in risc:
            frisc = 1
        if id in rec_emi:
            nrec = rec_emi[id]
            if nrec>2:
                frisc = 1
        vacext = 0
        if extern <> '':
            vacext = 1
        if anyVac == 2017 or anyVac == 2018:
            recompte[(amb, ambdesc, sap,sapdesc,up, desc,anyVac, edat_m, edat_a, vacext, frisc)] += 1
        
        
upload = []
for (amb, ambdesc, sap,sapdesc,up, desc,anyVac, edat_m, edat_a, vacext, frisc), valor in recompte.items():
    upload.append([amb, ambdesc, sap,sapdesc,up, desc,anyVac, edat_m, edat_a, vacext, frisc, valor])
    
file = tempFolder + 'MeningoB_2017_2018.txt'
writeCSV(file, upload, sep=';')    
