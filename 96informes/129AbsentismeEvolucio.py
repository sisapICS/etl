
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

debug = False

centres = {scs_c: (ics_c, amb_d, sap_d, ics_d) for (scs_c, ics_c, amb_d, sap_d, ics_d) in getAll("select scs_codi, ics_codi, amb_desc, sap_desc, ics_desc from cat_centres where ep='0208'", 'nodrizas')}

tipus_visita = ['9C', '9R', 'CP', 'ECOC', 'PM','ECOD']

def get_servei(servei):

    if servei == 'MG':
        serdesc = 'Medicina Familia'
    elif servei == 'PED':
        serdesc = 'Pediatria'
    elif servei in ('INFP', 'INFPD'):
        serdesc = 'Infermeria pediatria'
    elif servei in ('INF','INFMG','ENF','INFG','ATS','GCAS'):
        serdesc = 'Infermeria'
    elif servei in ('ODO','ODN','ODN-P','ODON','RNODN','ODO2','EXO','HODO','00492','ODN2'):
        serdesc = 'Odontologia'
    elif servei in ('DTS','ASS','TSOC','TS','AS','ASO','ASI','ASIS','SS','RNAS','AASS','05999'):
        serdesc = 'Treball social'
    else:
        serdesc = 'ALTRES'
        
    return serdesc

sql = "select concat(codi_sector, tv_cita, tv_tipus), if(tv_model_nou = 'S', tv_tipus, tv_homol) \
                from cat_vistb206"
hm = {}
for tipus, homol in getAll(sql, 'import'):
    hm[tipus] = homol
    
programades = Counter()
realitzades = Counter() 
for table in getSubTables('visites'):
    dat, = getOne('select year(visi_data_visita) from {} limit 1'.format(table), 'import')
    if dat == 2016 or dat == 2017:
        sql = "select  codi_sector, extract(year_month from visi_data_visita), visi_up, visi_situacio_visita, visi_servei_codi_servei, concat(codi_sector, visi_tipus_citacio, visi_tipus_visita),visi_data_visita,visi_dia_peticio\
               from {} where visi_data_baixa = 0 and visi_lloc_visita = 'C' and visi_data_visita<>'2017-10-03' {}".format(table, ' limit 10' if debug else '')
        for sec,  periode, up,sit, servei, tipus,datav,datap in getAll(sql, 'import'):
            if up in centres:
                try:
                    b = daysBetween(datap,datav)
                except:
                    b = -1
                amb = centres[up][1]
                try:
                    servdesc = get_servei(servei)
                except KeyError:
                    continue
                try:
                    tip = hm[tipus]
                except KeyError:
                    continue
                if tip in tipus_visita:    
                    programades[amb, periode, servdesc] += 1
                    if sit == 'R':
                        realitzades[amb, periode, servdesc] += 1

data_file = []
for (amb, periode, servdesc), count in programades.items():
    fetes = realitzades[amb, periode, servdesc]
    data_file.append([amb, periode, servdesc, fetes,count])
    
if data_file:
    file = tempFolder + 'Visites_realitzades_2017_2016_ en visites del dia.txt'
    writeCSV(file, data_file, sep=';')