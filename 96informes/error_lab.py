
import sisapUtils as u
import collections as c
import sisaptools as t
import os

cols = '(periode varchar(10), codi varchar(20), N int)'
u.createTable('errors_lab', cols, 'test', rm=True)


for anys in ('2020', '2021', '2022'):
    for mes in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'):
        for quinzena in ('01', '16'):
            codis_mesos = c.defaultdict(c.Counter)
            periode = 't' + anys + mes + quinzena
            print(periode)
            sql = """select cr_codi_PROVA_ICS from sidics.labtb101 partition({})
                        where cr_codi_PROVA_ICS IN (
                            'MMB55',
                            '012901',
                            '012902',
                            '012903',
                            '012904',
                            '012905',
                            '012906',
                            '012907',
                            '012908',
                            'MMB11',
                            '012213',
                            '012214',
                            '012215',
                            '012216',
                            '012217',
                            '012218',
                            '012219',
                            '012220',
                            '012221',
                            '012222',
                            '012223',
                            '012224',
                            '012225',
                            '012226',
                            '012227',
                            '012228',
                            '012229',
                            '012230',
                            '012231',
                            '012232',
                            '012233',
                            '012234',
                            '012235',
                            '012236',
                            '012237',
                            '012238',
                            '012239',
                            '012240',
                            '012241',
                            '012242',
                            '012243',
                            '012244',
                            '012245',
                            '012246',
                            '012247',
                            '012248',
                            '012249',
                            '012250'
                        )
                        AND CODI_UP IN ('00179',
                        '00234',
                        '00232',
                        '00252',
                        '00251',
                        '04390',
                        '00233',
                        '00253',
                        '00254',
                        '07733')""".format(periode)
            for codi, in u.getAll(sql, ("sidics", "x0002")):
                codis_mesos[periode][codi] += 1

            upload = []
            for periode, dic in codis_mesos.items():
                for codi, val in dic.items():
                    upload.append((periode, codi, val))

            u.listToTable(upload, 'errors_lab', 'test')



# cols = '(periode varchar(10), codi varchar(20), N int)'
# u.createTable('errors_lab_nord', cols, 'test', rm=True)
# for anys in ('2020', '2021', '2022'):
#     for mes in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'):
#         for quinzena in ('01', '16'):
#             codis_mesos = c.defaultdict(c.Counter)
#             periode = 't' + anys + mes + quinzena
#             print(periode)
#             sql = """select cr_codi_PROVA_ICS from sidics.labtb101 partition({})
#                         where cr_codi_PROVA_ICS IN ('S190CLISE',
#                         'S190CLISEU',
#                         'S276CLISE',
#                         'S277CLISEU')
#                         AND CODI_UP IN ('00318',
#                         '00406',
#                         '00409',
#                         '00407',
#                         '00319',
#                         '00411',
#                         '01412',
#                         '06754',
#                         '07073',
#                         '06378')""".format(periode)
#             print(sql)
#             for codi, in u.getAll(sql, ("sidics", "x0002")):
#                 codis_mesos[periode][codi] += 1
#             upload = []
#             for periode, dic in codis_mesos.items():
#                 for codi, val in dic.items():
#                     upload.append((periode, codi, val))

#             u.listToTable(upload, 'errors_lab_nord', 'test')
