# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

nod = 'nodrizas'
imp = 'import'

cAlzheimer = "('G30', 'G30.0', 'G30.1', 'G30.8','G30.9','F00', 'F00.0', 'F00.1','F00.2','F00.9')"
ccog = "('F06.7')"


def grup_edat(edat):

    if edat < 65:
        grupedat = 'Menors de 65 anys'
    elif 65<= edat <75:
        grupedat = 'Entre 65 i 74'
    elif 75<= edat <85:
        grupedat = 'Entre 75 i 84'
    elif 85<= edat:
        grupedat = 'Majors de 84 anys'
    else:
        grupedat = 'error'
    
    return grupedat
    
dext, = getOne('select data_ext from dextraccio', nod)

centres = {}
sql = "select scs_codi from cat_centres where ep='0208'"
for up, in getAll(sql, nod):
    centres[up] = True

dAlzheimer = {}    
sql = "select  id_cip_sec from problemes where pr_cod_ps in {0} \
        and pr_cod_o_ps='C' and pr_hist=1 and pr_dde<='{1}' and (pr_data_baixa is null or pr_data_baixa>'{1}') and (pr_dba is null or pr_dba>'{1}')".format(cAlzheimer, dext)
for id, in getAll(sql, imp):
    dAlzheimer[id] = True
    
dcog = {}    
sql = "select  id_cip_sec from problemes where pr_cod_ps in {0} \
        and pr_cod_o_ps='C' and pr_hist=1 and pr_dde<='{1}' and (pr_data_baixa is null or pr_data_baixa>'{1}') and (pr_dba is null or pr_dba>'{1}')".format(ccog, dext)
for id, in getAll(sql, imp):
    dcog[id] = True
    
ddemencia = {}
sql = 'select id_cip_sec from eqa_problemes where ps=86'
for id, in getAll(sql, nod):
    ddemencia[id] = True
    
cuidador ={}
sql = 'select id_cip_sec, val from ts_variables where agrupador=103'
for id, assal in getAll(sql, nod):
    cuidador[id] = assal
    
assal ={}
sql = 'select id_cip_sec from ts_variables where agrupador=103 and val=1'
for id,  in getAll(sql, nod):
    assal[id] = True
   
atdom = {}
sql = 'select id_cip_sec from eqa_problemes where ps=45'
for id, in getAll(sql, nod):
    atdom[id] = True   
    
valoracio = {}
sql = 'select id_cip_sec from eqa_variables where agrupador in (336, 88)'
for id, in getAll(sql, nod):
    valoracio[id] = True
    
valcuidador = {}
sql = 'select id_cip_sec from ts_variables where agrupador = 332'
for id, in getAll(sql, nod):
    valcuidador[id] = True
   
upload = []  
sql = 'select id_cip_sec, up, edat, sexe, ates, institucionalitzat from assignada_tot'
for id, up, edat, sexe, ates, insti in getAll(sql, nod):
    if up in centres:
        if edat > 14:
            demencia, deteriorament, alzh, valcog, atd, cuid, valcuid, assalariat  = 0, 0, 0, 0, 0, 0, 0, 0
            if id in dcog:
                deteriorament = 1
            if id in ddemencia:
                demencia = 1
            if id in dAlzheimer:
                alzh = 1
            if id in valoracio:
                valcog = 1
            if id in atdom:
                atd = 1
            if id in cuidador:
                cuid = 1
            if id in assal:
                assalariat = 1
            if id in valcuidador:
                valcuid = 1
            grup = grup_edat(edat)
            upload.append([id,up, edat, grup, sexe, ates, insti, deteriorament, demencia, alzh, valcog, atd, cuid, assalariat, valcuid])
            
table = 'nota_alzh'      
createTable(table, '(id_cip_sec int, up varchar(5), edat int, grup varchar(150), sexe varchar(1), ates int, insti int, deterioriament int, demencia int, alzh int, val_cognitiu int, atdom int, cuidador int, assalariat int, val_cuidador int)', 'test', rm=True)
listToTable(upload, table, 'test')

                