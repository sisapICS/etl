# -*- coding: utf8 -*-

import hashlib as h

"""
Vacunes en majors de 80 20210218
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


db = 'permanent'

class vacunes(object):
    """."""

    def __init__(self):
        """."""

        self.get_laboratori()
        self.export_files()

    def get_laboratori(self):
        """."""
        u.printTime("laboratori")
        self.pacients = {}
        for table in u.getSubTables('laboratori'):
            print table
            sql = """select id_cip_sec,cr_codi_prova_ics, cr_data_reg, extract(year_month from cr_data_reg) from {} where cr_codi_prova_ics in ('H10172','LCI014','BB063','Q32674','Q32685','T05861','TF036') and cr_codi_lab <>('RIMAP')""".format(table)
            for id, codi, data, anys in u.getAll(sql, 'import'):
                self.pacients[(id, data, anys)] = True
                   
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        self.recomptes = c.Counter()
		
        for (id, data, periode), n in self.pacients.items():
            self.recomptes[periode] += 1
        
        upload = []
        for periode, n in self.recomptes.items():
            upload.append([periode, n])
        
        file = u.tempFolder + "lab.csv"
        u.writeCSV(file, upload, sep='@')
                         
if __name__ == '__main__':
    u.printTime("Inici")
     
    vacunes()
    
    u.printTime("Final") 