# coding: iso-8859-1

from sisapUtils import *
from datetime import *
import csv,os,sys
from time import strftime

codis_vhc = "('001995', 'V01485', 'V01385')"



class HEPATC(object):
    """."""

    def __init__(self):
        """."""
        self.dades = []
        self.get_centres()
        self.get_hash()
        self.get_cip()
        self.get_silicon()
        self.get_RPT()
        self.get_dx()
        self.get_carrega()
        self.get_poblacio()
        self.export_data()

        
    def get_centres(self):
        """Trec centres ICS i presons"""
        
        self.centres = {}
        sql = "select  scs_codi, amb_desc from cat_centres where ep='0208'"
        for up, desc in getAll(sql, 'nodrizas'):
            self.centres[up] = {'amb': desc, 'jail': 0}

    
    def get_hash(self):
        """Obtenim els hashos dels pacients"""
        
        self.hashos = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        for id, sec, hash in getAll(sql, 'import'):
            self.hashos[id] = {'s': sec, 'h': hash}
    
    def get_cip(self):
        """Aconseguim els cips per passar a farmàcia"""
        
        self.cips = {}
        self.cip_to_hash = {}
        sql = "select usua_cip, usua_cip_cod from pdptb101"
        for c,h in getAll(sql, 'pdp'):
            self.cip_to_hash[(c)] = h
    
    def get_silicon(self):
        """A partir excel que ens passa per pen drive que li ha passat el toni fuentes"""
        file = tempFolder + 'Silicon.csv'
        
        self.nhc_to_cip = {}
        self.silicon = {}
        
        for (any_c, centre, nhc, cip, codi_m, desc_m) in readCSV(file, sep=';'):
            if cip != 'null':
                self.nhc_to_cip[int(nhc)] = cip[:13]
                any_c = int(any_c)
                if cip[:13] in self.cip_to_hash:
                    hash = self.cip_to_hash[cip[:13]]
                if hash in self.silicon:
                    any_c2 = self.silicon[hash]
                    if any_c < any_c2:
                        self.silicon[hash] = any_c
                else:
                    self.silicon[hash] = any_c
        
    def get_RPT(self):
        """A partir excel que ens passa per pen drive que li ha passat el toni fuentes"""
        file = tempFolder + 'RPT.csv'
        
        a = 0
        self.rpt = {}
        for row in readCSV(file, sep=';'):
            fibrosi=row[15]
            cirrosi = row[16]
            nhc = int(row[1])
            data = row[4][6:]
            if nhc in self.nhc_to_cip:
                cip = self.nhc_to_cip[nhc]
                if cip in self.cip_to_hash:
                    hash = self.cip_to_hash[cip]
                if hash in self.rpt:
                    d2 = self.rpt[hash]['data']
                    if data > d2:
                        self.rpt[hash]['data'] = data
                        self.rpt[hash]['fibrosi'] = data
                        self.rpt[hash]['cirrosi'] = data
                else:
                    self.rpt[hash] = {'data': data, 'fibrosi': fibrosi, 'cirrosi': cirrosi}
            else:
                a +=1
        print a
    
    def get_dx(self):
        """Agafem diagnòstics actius hepatitis C"""
        
        self.diagnostics = set([id for id, in getAll("select id_cip_sec from eqa_problemes where ps=12",
                                                    'nodrizas')])

        
    def get_carrega(self):
        """carrega viral de nod_serologies"""
        
        self.seros = {}
        sql = "select id_cip_sec, dat, val from nod_serologies where cod in {}".format(codis_vhc)
        for id, dat, val in getAll(sql, 'nodrizas'):
            if id in self.seros:
                dat1 = self.seros[id]['dat']
                if dat> dat1:
                    self.seros[id]['dat'] = dat
                    self.seros[id]['val'] = val
            else:
                self.seros[id] = {'dat': dat, 'val': val}
    
    def get_poblacio(self):
        """Trec assignació pacients sense jail"""
        
        self.poblacio = {}
        sql = "select id_cip_sec, up from assignada_tot"
        for id, up in getAll(sql, 'nodrizas'):
            if up in self.centres:
                hash = self.hashos[id]['h']
                ambit = self.centres[up]['amb']
                dx, sero, serop, val, dats, silic, silicondat, rp_t, rptdat, rptf, rptc  = 0, 0, 0, None, None, 0, None, 0, None, None, None
                if id in self.diagnostics:
                    dx = 1
                if id in self.seros:
                    sero = 1
                    val = self.seros[id]['val']
                    dats = self.seros[id]['dat']
                    if val > 1:
                        serop = 1
                if hash in self.silicon:
                    silic, silicondat = 1, self.silicon[hash]
                if hash in self.rpt:
                    rp_t, rptdat, rptf, rptc = 1, self.rpt[hash]['data'], self.rpt[hash]['fibrosi'], self.rpt[hash]['cirrosi']
                if dx == 1 or sero == 1 or silic == 1 or rp_t == 1:
                    self.dades.append([hash, ambit, dx, sero, serop, dats, val,silic, silicondat, rp_t, rptdat, rptf, rptc])
                    
                    
    def export_data(self):
        """."""
        dades_farm = []
        header_full = [('hash', 'ambit', 'diagnostic_VHC', 'Carrega', 'Carrega_positiva', 'Data_carrega_last', 'valor_carrega_last', 'Silicon', 'Silicon_any', 'RPT', 'RPT_any' ,'RPT_fibrosi', 'RPT_cirrosi')]
        writeCSV(tempFolder + 'pacients_VHC.txt', header_full + sorted(self.dades))
             
if __name__ == '__main__':
    HEPATC()
