
import sisaptools as u
import datetime


if __name__ == "__main__":
    try:
        DATABASE = ("exadata", "thewhole")

        # EXP

        # with u.Database(*DATABASE) as exadata_pre:
        #     # exadata_pre.execute("""drop TABLE alertes_exp""")
        #     sql = """CREATE TABLE alertes_exp 
        #             (id varchar2(20), cip varchar2(14), tipus varchar2(6), 
        #             data_alta date, estat varchar2(1), data_estat date, 
        #             entregat varchar2(1), fet varchar2(1), up varchar2(5), uba varchar2(5), 
        #             servei varchar2(5), data_modi date, txt1 varchar2(200), 
        #             txt2 varchar2(200), txt3 varchar2(200), txt4 varchar2(200))"""
        #     exadata_pre.execute(sql)

        upload = []
        sql = """SELECT ALV_ID, ALV_CIP, ALV_TIPUS, 
                    ALV_DATA_AL, ALV_ESTAT, ALV_DATA_ESTAT,
                    ALV_ENTREGAT, ALV_FET, ALV_UAB_UP,
                    ALV_UAB_CODI_UAB, ALV_UAB_SERVEI,
                    ALV_DATA_MODIF, ALV_TEXT1, ALV_TEXT2,
                    ALV_TEXT3, ALV_TEXT4, sysdate
                    FROM prstb550
                    WHERE alv_tipus = 'RES'
                    OR alv_tipus LIKE 'IT%'"""
        # for id, cip, tipus, data_alta, estat, data_estat, entregat, fet, up, uba, servei, data_modi, txt1, txt2, txt3, txt4 in u.getAll(sql, 'sector'):
        #     upload.append((id, cip, tipus, data_alta, estat, data_estat, entregat, fet, up, uba, servei, data_modi, txt1, txt2, txt3, txt4))
        for row in u.Database('6837', 'data', retry=1).get_all(sql):
            upload.append(row)

        u.Database(*DATABASE, retry=5).list_to_table(upload, 'alertes_exp')

        # PROD

        upload = []
        sql = """SELECT ALV_ID, ALV_CIP, ALV_TIPUS, 
                    ALV_DATA_AL, ALV_ESTAT, ALV_DATA_ESTAT,
                    ALV_ENTREGAT, ALV_FET, ALV_UAB_UP,
                    ALV_UAB_CODI_UAB, ALV_UAB_SERVEI,
                    ALV_DATA_MODIF, ALV_TEXT1, ALV_TEXT2,
                    ALV_TEXT3, ALV_TEXT4, sysdate
                    FROM prstb550
                    WHERE alv_tipus = 'RES'
                    OR alv_tipus LIKE 'IT%'"""
        # for id, cip, tipus, data_alta, estat, data_estat, entregat, fet, up, uba, servei, data_modi, txt1, txt2, txt3, txt4 in u.getAll(sql, 'sector'):
        #     upload.append((id, cip, tipus, data_alta, estat, data_estat, entregat, fet, up, uba, servei, data_modi, txt1, txt2, txt3, txt4))
        for row in u.Database('6837', 'prod', retry=1).get_all(sql):
            upload.append(row)

        u.Database(*DATABASE, retry=5).list_to_table(upload, 'alertes_prod')

        mail = u.Mail()
        mail.to.append("roser.cantenys@catsalut.cat")
        mail.subject = "proves alertes NAISSSS!!!"
        mail.text = "WELL DONE:)))"
        mail.send()  
        
    except Exception as e:
        mail = u.Mail()
        mail.to.append("roser.cantenys@catsalut.cat")
        mail.subject = "proves alertes mal!!!"
        mail.text = str(e)
        mail.send()    