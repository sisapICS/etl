import sisapUtils as u
import datetime as d
import collections as c
import sisaptools as t

u.printTime('inici')
sql = """
    SELECT up_cod, up_des, gap_cod, gap_des, regio_cod, regio_des
    FROM dwsisap.dbc_centres
"""

centres = {up: (up_desc, gap, gap_desc, rs, rs_desc) for up, up_desc, gap, gap_desc, rs, rs_desc in u.getAll(sql, 'exadata')}
u.printTime('centres')
sql = """
    SELECT id_cip_sec, up
    FROM assignada_tot
"""
pob = {id: up for id, up in u.getAll(sql, 'nodrizas')}
u.printTime('assignada')
sql = """
    SELECT id_cip_sec
    FROM estats
    WHERE es_cod = 'ER0001'
"""
pcc = set([id for id, in u.getAll(sql, 'import')])
# PCC de l'ultim any o historic?
u.printTime('pcc')
problemes = c.defaultdict(set)
codis = {
    86: 'demencia',
    344: 'ELA',
    68: 'parkinson'
}
sql = """
    SELECT id_cip_sec, ps
    FROM eqa_problemes
    WHERE ps in (86, 344, 68)
"""
for id, ps in u.getAll(sql, 'nodrizas'):
    problemes[codis[ps]].add(id)
u.printTime('problemes')
resultat = c.defaultdict(c.Counter)
for id in pcc:
    if id in pob:
        up = pob[id]
        if up in centres:
            resultat[(up, 'Demencia')]['DEN'] += 1
            resultat[(up, 'ELA')]['DEN'] += 1
            resultat[(up, 'Parkinson')]['DEN'] += 1
            if id in problemes['demencia']:
                resultat[(up, 'Demencia')]['NUM'] += 1
            elif id in problemes['ELA']:
                resultat[(up, 'ELA')]['NUM'] += 1
            elif id in problemes['parkinson']:
                resultat[(up, 'Parkinson')]['NUM'] += 1
u.printTime('cuinetes')
upload = []
for (up, ind) in resultat:
    if up in centres:
        (up_desc, gap, gap_desc, rs, rs_desc) = centres[up]
        upload.append((ind, up, up_desc, gap, gap_desc, rs, rs_desc, resultat[(up, ind)]['NUM'], resultat[(up, ind)]['DEN'], (float(resultat[(up, ind)]['NUM'])/float(resultat[(up, ind)]['DEN']))*100))

cols = "(indicador varchar(10), up varchar(10), up_desc varchar(50), gap varchar(5), gap_desc varchar(50), regio varchar(5), regio_desc varchar(50), numerador int, denominador int, resultat double)"
u.createTable('peticio_pcc', cols, 'altres', rm=True)
u.listToTable(upload, 'peticio_pcc', 'altres')
u.printTime('fi')