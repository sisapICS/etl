# -*- coding: utf8 -*-

import collections as c
import sisaptools as u
import sisapUtils as su
import datetime as d
from dateutil.relativedelta import relativedelta

now = d.date.today()
minus3 = now - relativedelta(years=3)
DATABASE = ("exadata", "thewhole")

class Vitamina():
    def __init__(self):
        # self.get_poblacio()
        # su.printTime('get_poblacio')
        # su.printTime(str(len(self.poblacio)))
        # self.get_problemes()
        # su.printTime('get_problemes')
        # su.printTime(str(len(self.poblacio)))
        # self.get_lab()
        # su.printTime('get_lab')
        # su.printTime(str(len(self.poblacio)))
        # self.get_variables()
        # su.printTime('get_variables')
        # su.printTime(str(len(self.poblacio)))
        # self.get_prescripcions()
        # su.printTime('get_prescripcions')
        self.read_vitamines()
        su.printTime('vitamines')

    def get_poblacio(self):
        #agafem nomes de 6209 i 6211
        # filtre de edat >= 16 i pertany a una de les ups especificades al trello
        tables = ['assignada_s6209', 'assignada_s6211']
        sql = """
            SELECT id_cip_sec
            FROM {}
            WHERE year(current_date) - year(usua_data_naixement) >= 16
            and (usua_uab_up in ('00705','01326','01327','01328','01522','03527','01489',
                    '03109','03110','06215','02791','01329','01330','01485','01928',
                    '01929','01486','01488','03913')
            or usua_uab_ep in ('1313','3516','0094','2598'))
            """
        self.poblacio = set()
        for t in tables:
            for cip, in su.getAll(sql.format(t), 'import'):
                self.poblacio.add(cip)
            su.printTime(t)
        

    def get_prescripcions(self):
        upload = []
        tables = ['tractaments_s6209', 'tractaments_s6211']

        # poblacio prescrita actualment amb vitamina D
        # ('A11CC05', 'A11CC06', 'A12AX92', 'A12AX93', 'A12AX94', 'A12AX91', 'A12AX96')
        # i no prescrita actualment amb 
        # ('A10BD03', 'A10BD05', 'A10BD06', 'A10BD09', 'C03CA01', 'C03CA02', 'C03EB01',
        # 'L04AD01', 'L02AB02', 'L02BG03', 'L02BG04', 'L02BG06', 'H02AB01', 'H02AB02',
        # 'H02AB03', 'H02AB04', 'H02AB06', 'H02AB07', 'H02AB09', 'H02AB10', 'H02AB13',
        # 'L01BA01', 'L04AX03', 'N03AA02', 'N03AA0', 'N03AB02', 'N03AF01', 'N03AG01')

        sql_presc = """
            SELECT id_cip_sec, ppfmc_atccodi, ppfmc_pmc_amb_num_col, ppfmc_desc_esp
            from {tb}
            where ppfmc_atccodi in ('A11CC05', 'A11CC06', 'A12AX92', 'A12AX93', 'A12AX94', 'A12AX91', 'A12AX96')
            and id_cip_sec not in (select id_cip_sec from {tb} where ppfmc_atccodi in
            ('A10BD03', 'A10BD05', 'A10BD06', 'A10BD09', 'C03CA01', 'C03CA02', 'C03EB01',
            'L04AD01', 'L02AB02', 'L02BG03', 'L02BG04', 'L02BG06', 'H02AB01', 'H02AB02',
            'H02AB03', 'H02AB04', 'H02AB06', 'H02AB07', 'H02AB09', 'H02AB10', 'H02AB13',
            'L01BA01', 'L04AX03', 'N03AA02', 'N03AA0', 'N03AB02', 'N03AF01', 'N03AG01')
            and ppfmc_data_fi > current_date)
            and ppfmc_data_fi > current_date
        """
        # obtenim dades id_cip_sec, num colegiat, especialitat i codi de vitamina D
        for t in tables:
            for cip, codi, col, esp in su.getAll(sql_presc.format(tb=t), 'import'):
                if cip in self.poblacio:
                    upload.append((cip, col, esp, codi))
            su.printTime(str(len(upload)))
            su.printTime(t)
        #cols = "(id_cip_sec int(11), num_colegiat varchar(9), especialitat varchar(5), codi_vitamina varchar(7))"
        su.listToTable(upload, 'vitamines', 'altres')
    
    def get_problemes(self):
        tables = ['problemes_s6209', 'problemes_s6211']
        # poblacio que no tingui un dels dxs escollits actualment
        sql_probl = """
            SELECT id_cip_sec
            FROM {}
            WHERE (pr_dba > current_date or pr_dba = '')
            and pr_data_baixa = ''
            and (pr_cod_ps like '%M80%'
            or pr_cod_ps like '%M81%'
            or pr_cod_ps like '%M83%'
            or pr_cod_ps like '%M84%'
            or pr_cod_ps like '%M85%'
            or pr_cod_ps like '%Z87.310%'
            or pr_cod_ps like '%T14.8XXA%'
            or pr_cod_ps like '%S52.90XA%'
            or pr_cod_ps like '%S72.90X%'
            or pr_cod_ps like '%S42.309A%'
            or pr_cod_ps like '%N18.1%'
            or pr_cod_ps like '%N18.2%'
            or pr_cod_ps like '%N18.3%'
            or pr_cod_ps like '%N18.4%'
            or pr_cod_ps like '%N18.5%'
            or pr_cod_ps like '%N18.6%'
            or pr_cod_ps like '%N18.7%'
            or pr_cod_ps like '%N18.8%'
            or pr_cod_ps like '%N18.9%'
            or pr_cod_ps like '%K70%'
            or pr_cod_ps like '%K71%'
            or pr_cod_ps like '%K72%'
            or pr_cod_ps like '%K73%'
            or pr_cod_ps like '%K74%'
            or pr_cod_ps like '%K75%'
            or pr_cod_ps like '%K76%'
            or pr_cod_ps like '%K77%'
            or pr_cod_ps like '%Z98.84%'
            or pr_cod_ps like '%K50%'
            or pr_cod_ps like '%K86%'
            or pr_cod_ps like '%K90%'
            or pr_cod_ps like '%K91%'
            or pr_cod_ps like '%K92%'
            or pr_cod_ps like '%K93%'
            or pr_cod_ps like '%K94%'
            or pr_cod_ps like '%K95%'
            or pr_cod_ps like '%C01-E21%')
        """
        for t in tables:
            for cip, in su.getAll(sql_probl.format(t), 'import'):
                if cip in self.poblacio:
                    self.poblacio.remove(cip)
            su.printTime(t)
    
    def get_lab(self):
        self.labs = set()
        tables = ['laboratori_t20220216',
                'laboratori_t20201016',
                'laboratori_t20230301',
                'laboratori_t20210316',
                'laboratori_t20220316',
                'laboratori_t20210216',
                'laboratori_t20230901',
                'laboratori_t20221216',
                'laboratori_t20230516',
                'laboratori_t20211201',
                'laboratori_t20211216',
                'laboratori_t20210901',
                'laboratori_t20220116',
                'laboratori_t20220301',
                'laboratori_t20201216',
                'laboratori_t20201101',
                'laboratori_t20211116',
                'laboratori_t20230916',
                'laboratori_t20210301',
                'laboratori_t20221201',
                'laboratori_t20210101',
                'laboratori_t20220916',
                'laboratori_t20210116',
                'laboratori_t20220416',
                'laboratori_t20210916',
                'laboratori_t20201201',
                'laboratori_t20201116',
                'laboratori_t20211101',
                'laboratori_t20210501',
                'laboratori_t20230701',
                'laboratori_t20210616',
                'laboratori_t20221016',
                'laboratori_t20230116',
                'laboratori_t20210201',
                'laboratori_t20210516',
                'laboratori_t20220901',
                'laboratori_t20230716',
                'laboratori_t20211001',
                'laboratori_t20221001',
                'laboratori_t20220701',
                'laboratori_t20230801',
                'laboratori_t20210601',
                'laboratori_t20230101',
                'laboratori_t20230416',
                'laboratori_t20220101',
                'laboratori_t20230216',
                'laboratori_t20211016',
                'laboratori_t20230616',
                'laboratori_t20230816',
                'laboratori_t20220716',
                'laboratori_t20230201',
                'laboratori_t20220516',
                'laboratori_t20230401',
                'laboratori_t20220201',
                'laboratori_t20210816',
                'laboratori_t20210416',
                'laboratori_t20221101',
                'laboratori_t20220601',
                'laboratori_t20210701',
                'laboratori_t20230601',
                'laboratori_t20220801',
                'laboratori_t20230501',
                'laboratori_t20210801',
                'laboratori_t20220501',
                'laboratori_t20210401',
                'laboratori_t20220616',
                'laboratori_t20210716',
                'laboratori_t20221116',
                'laboratori_t20220816',
                'laboratori_t20230316',
                'laboratori_t20220401',
                ]
        # tables = []
        # sql_t = "select '{tb}', min(cr_data_reg) from {tb}"
        # for table in su.getSubTables('laboratori').keys():
        #     for tb, data in su.getAll(sql_t.format(tb=table), 'import'):
        #         if data >= minus3:
        #             tables.append(table)
        # filtrat glomerular a laboratori > 60
        sql_lab = """
            SELECT id_cip_sec
            from {tb}
            where codi_sector in ('6209', '6211')
            and cr_codi_prova_ics in ('R90361', 'Q69761', 'Q90361', 'W18161', 'W18261')
            and (cr_res_lab > 60
            or cr_res_lab like '>%')
        """
        for table in tables:
            for cip, in su.getAll(sql_lab.format(tb=table), 'import'):
                if cip in self.poblacio:
                    self.labs.add(cip)
            su.printTime(str(len(self.labs)))
            su.printTime(table)

    def get_variables(self):
        # tables = []
        # sql_t = "select '{st}', min(vu_dat_act) from {st}"
        # for table in su.getSubTables('variables').keys():
        #     for t, data in su.getAll(sql_t.format(st=table), 'import'):
        #         if data >= minus3:
        #             tables.append(t)
        tables = [
            'variables_sys_p11286',
            'variables_sys_p2542',
            'variables_sys_p1561',
            'variables_sys_p3181',
            'variables_sys_p3501',
            'variables_sys_p2661',
            'variables_sys_p3261',
            'variables_sys_p9666',
            'variables_sys_p2941',
            'variables_sys_p3101',
            'variables_sys_p2781',
            'variables_sys_p3061',
            'variables_sys_p3722',
            'variables_sys_p7486',
            'variables_sys_p3642',
            'variables_sys_p2602',
            'variables_sys_p3441',
            'variables_sys_p3681',
            'variables_sys_p2901',
            'variables_sys_p13046',
            'variables_sys_p3321',
            'variables_sys_p2501',
            'variables_sys_p3582',
            'variables_sys_p3541',
            'variables_sys_p3221',
            'variables_sys_p2981',
            'variables_sys_p2421',
            'variables_sys_p3021',
            'variables_sys_p2821',
            'variables_sys_p3141',
            'variables_sys_p5906',
            'variables_sys_p15306',
            'variables_sys_p2722',
            'variables_sys_p3401',
            'variables_sys_p3361',
        ]
        # sql_var = """
        #     SELECT id_cip_sec
        #     from {tb}
        #     where codi_sector in ('6209', '6211')
        #     and ((vu_cod_vs = 'LUF001' and (vu_val  = 60
        #     or vu_val  like '>%'))"""
        # a self.labs hi han d'anar les condicions (un o l'altre):
        # - filtrat glomerular a lab > 60 (FET a get_lab)
        # - filtrat glomerular a variables > 60
        # a banda, s'han d'afegir les condicions:
        # - no hi hagi cap IMC
        # - l'ultim valor < 30
        sql_glomerular = "select id_cip_sec from {tb} where vu_cod_vs = 'LUF001' and (vu_val > 60 or vu_val like '>%')"
        sql_imc = """select id_cip_sec, vu_cod_vs, vu_val, vu_dat_act
        from {tb} where vu_cod_vs = 'TT103' or id_cip_sec
        not in (select id_cip_sec from {tb} where vu_cod_vs = 'TT103')"""
        
        # data_var = set()
        dates = c.defaultdict()
        pob = set()
        for table in tables:
            for cip, in su.getAll(sql_glomerular.format(tb=table), 'import'):
                if cip in self.poblacio:
                    self.labs.add(cip)
            for cip, codi, val, data in su.getAll(sql_imc.format(tb=table), 'import'):
                if codi == 'TT103':
                    if (cip in dates and data > dates[cip][0]) or cip not in dates:
                        dates[cip] = (data, val)
                else:
                    pob.add(cip)
                # data_var.add((cip, codi, val, data))
            su.printTime(str(len(self.poblacio)))
            su.printTime(str(len(list(dates.keys()))))
            su.printTime(table)
        su.printTime('despres variables')
        # for cip, codi, val, data in data_var:
        #     if cip in self.poblacio:
        #         if codi == 'TT103':
        #             if cip in dates and data > dates[cip][0]:
        #                 dates[cip] = (data,val)
        #             elif cip not in dates:
        #                 dates[cip] = (data,val)
        #         else:
        #             self.poblacio.add(cip)

        su.printTime(str(len(self.labs)))
        su.printTime(str(len(self.poblacio)))
        for cip in list(self.poblacio):
            if cip not in self.labs:
                self.poblacio.remove(cip)
        # for cip in dates:
        #     if cip in self.poblacio and dates[cip][1] >= 30:
        #         self.poblacio.remove(cip)
        for cip in list(self.poblacio):
            if cip in dates and dates[cip][1] >= 30 and cip not in pob:
                self.poblacio.remove(cip)
        # for cip in list(self.poblacio):
        #     if cip not in pob:
        #         self.poblacio.remove(cip)
        su.printTime('despres sql variables')
    def read_vitamines(self):
        sql = "select id_cip_sec, num_colegiat, especialitat, codi_vitamina from vitamines"
        vitamines = c.defaultdict(list)
        for cip, col, esp, codi in su.getAll(sql, 'altres'):
            vitamines[cip].append((col, esp, codi))
        su.printTime('vitamines')
        sql = "select id_cip_sec, hash_d from u11"
        hashes = c.defaultdict()
        for cip, hash_d in su.getAll(sql, 'import'):
            hashes[hash_d] = cip
        su.printTime('hash')
        conversor = c.defaultdict()
        sql = "select usua_cip, usua_cip_cod from pdptb101"
        for cip, hash_d in su.getAll(sql, 'pdp'):
            if hash_d in hashes:
                # relacio key = id_cip_sec, value = cip
                conversor[hashes[hash_d]] = cip
        su.printTime('conversor')
        upload = []
        for id_cip_sec in list(vitamines.keys()):
            if id_cip_sec in conversor:
                for t in vitamines[id_cip_sec]:
                    upload.append((conversor[id_cip_sec], t[0], t[1], t[2]))
        su.printTime('upload')
        cols = "(cip varchar(13), num_colegiat varchar(9), especialitat varchar(5), codi_vitamina varchar(7))"
        su.createTable('cips_vitamines', cols, 'altres', rm=True)
        su.listToTable(upload, 'cips_vitamines', 'altres')
if __name__ == '__main__':
    try:
        inici = d.datetime.now()
        Vitamina()
        fi = d.datetime.now()
        txt = """Hora inici {},
                 hora fi {},
                 total de {}""".format(str(inici), str(fi), str(fi-inici))
        mail = u.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "Vitamina D yaaay, you da best:)))!!!"
        mail.text = txt
        mail.send()
    except Exception as e:
        mail = u.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "VITAMINA D MAL!!!"
        mail.text = str(e)
        mail.send()
        raise