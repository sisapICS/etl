# -*- coding: utf-8 -*-

import collections as c
import sisapUtils as u
import datetime as d
import sisaptools as su
from dateutil.relativedelta import relativedelta
import csv

file = './429_pob.csv'
exp = './nlp_sidiap_export.csv'
hash_d = []
sectors = c.defaultdict()
with open(file, 'r') as csvfile:
    data = csv.reader(csvfile, delimiter='|')
    line_count = 0
    for row in data:
        if line_count != 0:
            hash_d.append(row[0])
            sectors[row[0]] = row[1]
        else:
            line_count += 1
u.printTime(str(len(hash_d)))

sql = """
    SELECT usua_cip, substr(usua_cip_cod,0,13)
    FROM pdptb101
"""
cips = c.defaultdict()
for cip, codi in u.getAll(sql, 'pdp'):
    if codi in hash_d:
        cips[cip] = codi

u.printTime(str(len(cips)))

sql = """
    SELECT sc_cip
    FROM prstb318
    WHERE (lower(sc_textseg) LIKE 'rebutj%partici%'
    OR lower(sc_textseg) LIKE 'rebuig%partici%'
    OR lower(sc_textseg) LIKE 'rechaz%partici%')
    and SC_DHORA_SEG  >= DATE '2022-01-01'
"""

texts = []

for e, sector in enumerate(u.sectors):
    for cip, in u.getAll(sql, sector):
        if cip in cips:
            texts.append(cips[cip])

u.printTime(str(len(texts)))

export = []
for p in hash_d:
    if p in texts:
        export.append([p, sectors[p], 1])
    else:
        export.append([p, sectors[p], 0])

u.printTime(str(len(export)))

with open(exp, 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(export)
