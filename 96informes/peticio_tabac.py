
import sisapUtils as u
import collections as c

cols = '(id_cip_sec int, up varchar(5), up_desc varchar(1000), sap_desc varchar(100), edat varchar(20), sexe varchar(10), paquets_any varchar(15), estat_tabac varchar(15), N int)'
u.createTable('tabaquisme_peris', cols, 'test', rm = True)


people = {}
print('assignada')
sql = """select
                a.id_cip_sec,
                a.up,
                c.ics_desc,
                c.sap_desc,
                a.edat,
                a.sexe
        from
                nodrizas.assignada_tot a,
                nodrizas.cat_centres c
        where
                a.up = c.scs_codi
                and up in ('00172', '00438')
                and edat between 60 and 79"""

for id, up, up_desc, sap_desc, edat, sexe in u.getAll(sql, 'nodrizas'):
        people[id] = [up, up_desc, sap_desc, edat, sexe]

fumadors = set()
exfumadors = set()
print('fumadors')
sql = """select 
                id_cip_sec,
                case 
                        when last = 1 and tab = 1 then 'fumador'
                        when tab = 2 and dbaixa > '2014-09-18' then 'exfumador'
                end fumador
        from 
                nodrizas.eqa_tabac
        where 
                (last = 1 and tab = 1)
                or (tab = 2 and dbaixa > '2014-10-30')"""
for id, fum in u.getAll(sql, 'nodrizas'):
        if id in people:
                if fum == 'fumador': fumadors.add(id)
                elif fum == 'exfumador': exfumadors.add(id)

exfumadors = exfumadors.difference(fumadors)

paquets = {}
print('paquets')
sql = """select 
                id_cip_sec, 
                case 
                        when vu_val < 25 then '<25'
                        when vu_val >= 25 and vu_val < 30 then '25-29'
                        when vu_val >= 30 and vu_val < 35 then  '30-34'
                        when vu_val >= 35 then '>35'
                end n_paquets,
                vu_dat_act
        from 
                import.variables
        where 
                vu_cod_vs in ('PAQANY', 'PAQAN2')"""
for id, n_paquets, data in u.getAll(sql, 'nodrizas'):
        if id in fumadors or id in exfumadors:
                if id in paquets:
                        if paquets[id][1] < data:
                                paquets[id] = (n_paquets, data)
                else: paquets[id] = (n_paquets, data)

cols = '(up varchar(5), up_desc varchar(1000), sap_desc varchar(100), edat varchar(20), sexe varchar(10), paquets varchar(15), estat_tabac varchar(15), N int)'
upload = []
for id in fumadors:
        if id in people:
                up, up_desc, sap_desc, edat, sexe = people[id]
                if id in paquets:
                        n_paquets, data = paquets[id] 
                else: n_paquets = None
                upload.append((id, up, up_desc, sap_desc, edat, sexe, n_paquets, 'fumador'))

for id in exfumadors: 
        if id in people:
                up, up_desc, sap_desc, edat, sexe = people[id]
                if id in paquets:
                        n_paquets, data = paquets[id] 
                else: n_paquets = None
                upload.append((id, up, up_desc, sap_desc, edat, sexe, n_paquets, 'ex fumador'))

u.listToTable(upload, 'tabaquisme_peris', 'test')
