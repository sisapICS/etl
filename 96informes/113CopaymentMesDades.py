# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

debug = False

nod = 'nodrizas'
imp = 'import'
farmacs = "(82, 658, 216, 659)"

problems ='(726,727,728,729)'

def farmconverter(farmac):
    
    if farmac in (82, 658, 216, 659):
        far = 'Estatines'
    elif farmac in (713, 714):
        far = 'MSRE'
    elif farmac in (715, 716):
        far = 'Denosumab'
    elif farmac in (717, 718):
        far = 'Bifosfonats'
    elif farmac in (719, 720):
        far = 'Altres aOP'
    else:
        far = 'error'
    return far

def psconverter(ps):
    
    if ps in ('I20', 'I20.0', 'I20.8', 'I20.9', 'I21', 'I21.0', 'I21.1', 'I21.2', 'I21.3', 'I21.4', 'I21.9', 'I22', 'I22.0', 'I22.1', 'I22.8', 'I22.9'):
        prob = 'CI'
    elif ps in ('G45', 'G45.0', 'G45.1', 'G45.2', 'G45.8', 'G45.9', 'G46', 'G46.0', 'G46.1', 'G46.2', 'G46.3', 'G46.4', 'G46.5', 'G46.6', 'G46.7', 'G46.8', 'I63', 'I63.0', 'I63.1', 'I63.2', 'I63.3', 'I63.4', 'I63.5', 'I63.6', 'I63.8', 'I63.9', 'I64', 'I67.8', 'I67.9', 'I61', 'I61.0', 'I61.1', 'I61.2', 'I61.3', 'I61.4', 'I61.5', 'I61.6', 'I61.8', 'I61.9'):        
        prob = 'AVC'
    elif ps in (79, 710, 711, 712):
        prob = 'Fractura fragilitat'
    else:
        prob = 'error'
    return prob
    
anys = [2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013,2014,2015,2016]
mesos = ['01','02','03','04','05','06','07','08','09','10','11','12']

centres = {}

sql = "select sha1(concat(scs_codi,'EQA')), medea from cat_centres where ep='0208'"
for up, medea in getAll(sql, nod):
    centres[up] = medea
    
periodes = {}   
for ane in anys:
    for mes in mesos:
        periode = str(ane) + mes
        periodes[periode] = True

agrs = []
conv = {}
sql = "select agrupador,criteri_codi from eqa_criteris where agrupador in {0}{1}".format(problems, ' limit 10' if debug else '')
for agrupador, criteri in getAll(sql, nod):
    agrs.append(criteri)
    conv[criteri] = agrupador
in_crit = tuple(agrs)

incidents = Counter()
sql = "select id_cip, pr_cod_ps, pr_dde, extract(year_month from pr_dde), sha1(concat(pr_up,'EQA')) from problemes where  pr_cod_o_ps = 'C' and pr_hist = 1 and pr_cod_ps in {0}  and pr_data_baixa is null {1}".format(in_crit, ' limit 10' if debug else '')
for id, ps, dde, Ydde, up in getAll(sql, imp):
    prob = psconverter(ps)
    Ydde = str(Ydde)
    if Ydde in periodes:
        if up in centres:
            medea= centres[up]
            incidents[(Ydde, prob, up, medea)] += 1
            
fincidents = Counter()
sql = "select id, dat, agr,  sha1(concat(up_disp,'EQA')) from farmacs_facturats where agr in {0} {1}".format(farmacs, ' limit 10' if debug else '')
for id, dat, farmac, up in getAll(sql, ('SISAP_estatines','nym_proj')):
    far = farmconverter(int(farmac))
    if up in centres:
        medea = centres[up]
        fincidents[(dat, far, up, medea)] += 1
               
upload = []
for (Ydde, prob, up, medea), res in incidents.items():
    upload.append([Ydde, prob, up, medea, res])
file = tempFolder + 'projecte_copayment_ps_incidents.txt'
writeCSV(file, upload, sep=';')
    
upload = []
for (Ydde, far, up, medea), res in fincidents.items():
    upload.append([Ydde, far, up, medea, res])
file = tempFolder + 'projecte_copayment_far_incidents.txt'
writeCSV(file, upload, sep=';')   
    
    