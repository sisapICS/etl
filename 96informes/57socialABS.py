from sisapUtils import getAll, writeCSV, tempFolder
from collections import defaultdict

db = 'social'

taules = ['mst_indicadors_pacient_back','mst_indicadors_pacient']
indicadors = {}
for taula in taules:
    sql = 'select up, indicador,sum(num),sum(den),round((sum(num)/sum(den))*100,2) from {} where ates=1 and institucionalitzat=0 and excl=0 group by up, indicador'.format(taula)
    for up, ind, num, den, res in getAll(sql, db):
        if (up, ind) in indicadors:
            ok = 1
        else:
            indicadors[(up, ind)] = {'num': 0, 'den': 0, 'res': 0, 'num_act': 0, 'den_act':0, 'res_act': 0}
        if taula == 'mst_indicadors_pacient':
            indicadors[(up, ind)]['num_act'] = num
            indicadors[(up, ind)]['den_act'] = den
            indicadors[(up, ind)]['res_act'] = res
        elif taula == 'mst_indicadors_pacient_back':
            indicadors[(up, ind)]['num'] = num
            indicadors[(up, ind)]['den'] = den
            indicadors[(up, ind)]['res'] = res
writeCSV(tempFolder + 'social_simulacio.txt', [(up, ind, val['num'], val['den'], val['res'], val['num_act'], val['den_act'], val['res_act']) for (up, ind), val in indicadors.items()], sep=';')
