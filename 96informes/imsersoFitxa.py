# coding: utf-8

"""
 - TIME EXECUTION m

 - PETICIÓ:
    - Trello: https://trello.com/c/dpLyFW0j
    - Drive:

"""

import sisapUtils as u
# import csv, os, sys
import collections as c
from datetime import datetime
import datetime as d
# from dateutil.relativedelta import relativedelta
from os import remove

TODAY = d.datetime.now().date()


class imsersoFitxa(object):
    """."""
    def __init__(self):
        """."""

        self.get_cat_res()
        self.get_indicador_11()
        self.get_indicador_12()
        self.get_indicador_13a()
        self.get_indicador_13b()
        self.get_indicador_14a()
        self.get_indicador_14b()
        self.get_indicador_21()
        self.get_exitus()
        # self.get_indicador_31() NO DISPONIBLE EXITUS POR TODAS LAS CAUSAS
        # self.get_indicador_32() NO DISPONIBLE EXITUS POR TODAS LAS CAUSAS
        self.get_resultat()
        self.get_indicador_33()
        self.get_indicador_34()
        self.get_indicador_35()
        self.get_upload()
        self.export_upload()

    def get_cat_res(self):
        """."""
        print("------------------------------------------ get_cat_residencies")
        self.cat_res = {}
        sql = """select
                    cod
                    , tipus
                 from
                    sisap_covid_cat_residencia
              """
        for cod, tip in u.getAll(sql, "redics"):
            self.cat_res[cod] = (tip)
        print("RESIDENCIES CATALEG: N de registros:", len(self.cat_res))

    def get_res_cens(self):
        """ . """
        print("------------------------------------------------- get_res_cens")
        self.res_cens = {}
        sql = """select
                    data, perfil, cip, residencia
                 from
                    dwsisap.residencies_cens
                 where
                    data >= to_date('20200314','yyyymmdd')
                    and perfil = 'Resident'"""
        for data, perfil, cip13, residencia in u.getAll(sql, 'exadata'):
            self.res_cens[(cip13, data)] = {'perfil': perfil,
                                            'residencia': residencia}
        print("RESIDENCIES CENS: N de registros:", len(self.res_cens))

    def get_indicador_11(self):
        """ . """
        print("--------------------------------------------- get_indicador_11")
        self.ind_11 = []
        sql = """
                select
                    'R1' as Residencia_tipus,
                    sum(setmana_casos) as N
                from
                    preduffa.SISAP_COVID_RES_WEB_RESUM_HST a,
                    preduffa.SISAP_COVID_CAT_RESIDENCIA b
                where
                    a.RESIDENCIA = b.COD
                    and tipus = 1
                    and perfil='Resident'
                    and data between to_date('14,03,2020','DD,MM,YYYY')
                                 and to_date('22,06,2020','DD,MM,YYYY')
                    and to_char(DATA,'d')='1'
                UNION
                select
                    'R2' as Residencia_tipus,
                    sum(setmana_casos) as N
                FROM
                    preduffa.SISAP_COVID_RES_WEB_RESUM_HST a,
                    preduffa.SISAP_COVID_CAT_RESIDENCIA b
                WHERE
                    a.RESIDENCIA = b.COD
                    and tipus in (7,8)
                    and perfil='Resident'
                    and data between to_date('14,03,2020','DD,MM,YYYY')
                                 and to_date('22,06,2020','DD,MM,YYYY')
                    and to_char(data,'d')='1'"""
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '1.1 - N acumulado de casos confirmados (PDIA) COVID-19 y casos de sospecha (periodo 1)'  # noqa
            periode = '14/03/2020 - 22/06/2020'
            self.ind_11.append((ind, periode, res_tip, N))
        print("INDICADOR 1.1: N de registros:", len(self.ind_11))
        print(self.ind_11)

    def get_indicador_12(self):
        """ . """
        print("--------------------------------------------- get_indicador_12")
        self.ind_12 = []
        sql = """
                select
                    Residencia_tipus
                    , sum(N) as N
                from
                (select
                    'R1' as Residencia_tipus,
                    sum(setmana_casos) as N
                from
                    preduffa.SISAP_COVID_RES_WEB_RESUM_HST a,
                    preduffa.SISAP_COVID_CAT_RESIDENCIA b
                where
                    a.RESIDENCIA = b.COD
                    and tipus = 1
                    and perfil='Resident'
                    and data between to_date('23,06,2020','DD,MM,YYYY')
                                 and to_date('30,06,2020','DD,MM,YYYY')
                    and to_char(data,'d')='1'
                union
                select
                    'R1' as Residencia_tipus,
                    sum(setmana_casos) as N
                from
                    preduffa.SISAP_COVID_RES_WEB_RESUM a,
                    preduffa.SISAP_COVID_CAT_RESIDENCIA b
                where
                    a.RESIDENCIA = b.COD
                    and tipus = 1
                    and perfil='Resident'
                    and data between to_date('01,07,2020','DD,MM,YYYY')
                                 and to_date('31,12,2020','DD,MM,YYYY')
                    and to_char(data,'d')='1'
                ) r1 group by Residencia_tipus
                UNION
                select
                    Residencia_tipus
                    , sum(N) as N
                from
                (select
                    'R2' as Residencia_tipus,
                    sum(setmana_casos) as N
                FROM
                    preduffa.SISAP_COVID_RES_WEB_RESUM_HST a,
                    preduffa.SISAP_COVID_CAT_RESIDENCIA b
                WHERE
                    a.RESIDENCIA = b.COD
                    and tipus in (7,8)
                    and perfil='Resident'
                    and data between to_date('23,06,2020','DD,MM,YYYY')
                                 and to_date('30,06,2020','DD,MM,YYYY')
                    and to_char(data,'d')='1'
                union
                select
                    'R2' as Residencia_tipus,
                    sum(setmana_casos) as N
                from
                    preduffa.SISAP_COVID_RES_WEB_RESUM a,
                    preduffa.SISAP_COVID_CAT_RESIDENCIA b
                where
                    a.RESIDENCIA = b.COD
                    and tipus in (7,8)
                    and perfil='Resident'
                    and data between to_date('01,07,2020','DD,MM,YYYY')
                                 and to_date('31,12,2020','DD,MM,YYYY')
                    and to_char(data,'d')='1'
                ) r2 group by Residencia_tipus
                """
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '1.2 - N acumulado de casos confirmados (PDIA) COVID-19 (periodo 2)'  # noqa
            periode = '23/06/2020 - 31/12/2020'
            self.ind_12.append((ind, periode, res_tip, N))
        print("INDICADOR 1.2: N de registros:", len(self.ind_12))
        print(self.ind_12)

    def get_indicador_13a(self):
        """ . """
        print("-------------------------------------------- get_indicador_13a")
        self.ind_13a = []
        sql = """
            select
                'R1' as res_tip,
                (sum(dia_immunes) + sum(dia_antics)) as NP1
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM_HST a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Resident'
                and data = to_date('22,06,2020','DD,MM,YYYY')
            UNION
            select
                'R2' as res_tip,
                (sum(dia_immunes) + sum(dia_antics)) as NP1
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM_HST a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Resident'
                and data = to_date('22,06,2020','DD,MM,YYYY')
            """
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '1.3a - N total de casos que han superado la inf. (Periodo: 14.03.20 a 22.06.20)'  # noqa
            periode = '14/03/2020 - 22/06/2020'
            self.ind_13a.append((ind, periode, res_tip, N))
        print("INDICADOR 1.3a: N de registros:", len(self.ind_13a))
        print(self.ind_13a)

    def get_indicador_13b(self):
        """ . """
        print("-------------------------------------------- get_indicador_13b")
        self.ind_13b = []
        sql = """
            select
                p1.res_tip
                , np1p2 - np1 as np2
            from
            (select
                'R1' as res_tip,
                (sum(dia_immunes) + sum(dia_antics)) as NP1
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM_HST a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Resident'
                and data = to_date('22,06,2020','DD,MM,YYYY')
            UNION
            select
                'R2' as res_tip,
                (sum(dia_immunes) + sum(dia_antics)) as NP1
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM_HST a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Resident'
                and data = to_date('22,06,2020','DD,MM,YYYY')) p1,
            (select
                'R1' as res_tip,
                (sum(dia_immunes) + sum(dia_antics)) as NP1P2
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Resident'
                and data = to_date('31,12,2020','DD,MM,YYYY')
            UNION
            select
                'R2' as res_tip,
                (sum(dia_immunes) + sum(dia_antics)) as NP1P2
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Resident'
                and data = to_date('31,12,2020','DD,MM,YYYY')) p1p2
             where
                p1.res_tip = p1p2.res_tip
            """
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '1.3b - N total de casos que han superado la inf. (Periodo: 23.06.20 a 31.12.2020)'  # noqa
            periode = '23/06/2020 - 31/12/2020'
            self.ind_13b.append((ind, periode, res_tip, N))
        print("INDICADOR 1.3b: N de registros:", len(self.ind_13b))
        print(self.ind_13b)

    def get_indicador_14a(self):
        """ . """
        print("-------------------------------------------- get_indicador_14a")
        self.ind_14a = []
        sql = """
            select
                'R1' as Residencia_tipus,
                count(distinct(residencia))
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM_HST a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Resident'
                and data between to_date('14,03,2020','DD,MM,YYYY')
                             and to_date('22,06,2020','DD,MM,YYYY')
                and to_char(data,'d')='1'
                and setmana_casos > 0
            union
            select
                'R2' as Residencia_tipus,
                count(distinct(residencia))
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM_HST a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Resident'
                and data between to_date('14,03,2020','DD,MM,YYYY')
                             and to_date('22,06,2020','DD,MM,YYYY')
                and to_char(data,'d')='1'
                and setmana_casos > 0
            """
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '1.4a - N de centros que han tenido casos confirmados (PDIA) de residentes (Periodo: 14.03.20 a 22.06.20)'  # noqa
            periode = '14/03/2020 - 22/06/2020'
            self.ind_14a.append((ind, periode, res_tip, N))
        print("INDICADOR 1.4a: N de registros:", len(self.ind_14a))
        print(self.ind_14a)

    def get_indicador_14b(self):
        """ . """
        print("-------------------------------------------- get_indicador_14b")
        self.ind_14b = []
        sql = """
            select
                Residencia_tipus
                , count(distinct(residencia)) as N
            from
            (select
                'R1' as Residencia_tipus,
                residencia
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM_HST a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Resident'
                and data between to_date('23,06,2020','DD,MM,YYYY')
                             and to_date('30,06,2020','DD,MM,YYYY')
                and to_char(data,'d')='1'
                and setmana_casos > 0
            union
            select
                'R1' as Residencia_tipus,
                residencia
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Resident'
                and data between to_date('01,07,2020','DD,MM,YYYY')
                             and to_date('31,12,2020','DD,MM,YYYY')
                and to_char(data,'d')='1'
                and setmana_casos > 0) a
            group by Residencia_tipus
            UNION
            select
                Residencia_tipus
                , count(distinct(residencia)) as N
            from
            (select
                'R2' as Residencia_tipus,
                residencia
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM_HST a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Resident'
                and data between to_date('23,06,2020','DD,MM,YYYY')
                             and to_date('30,06,2020','DD,MM,YYYY')
                and to_char(data,'d')='1'
                and setmana_casos > 0
            union
            select
                'R2' as Residencia_tipus,
                residencia
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Resident'
                and data between to_date('01,07,2020','DD,MM,YYYY')
                             and to_date('31,12,2020','DD,MM,YYYY')
                and to_char(data,'d')='1'
                and setmana_casos > 0) a
            group by Residencia_tipus
            """
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '1.4b - N de centros que han tenido casos confirmados (PDIA) de residentes (Periodo: 23.06.20 a 31.12.20)'  # noqa
            periode = '23/06/2020 - 31/12/2020'
            self.ind_14b.append((ind, periode, res_tip, N))
        print("INDICADOR 1.4b: N de registros:", len(self.ind_14b))
        print(self.ind_14b)

    def get_indicador_21(self):
        """ . """
        print("--------------------------------------------- get_indicador_21")
        self.ind_21 = []
        sql = """
            select
                'R1' as Residencia_tipus,
                sum(setmana_casos) as N
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus = 1
                and perfil='Personal'
                and data between to_date('01.07,2020','DD,MM,YYYY')
                             and to_date('31,12,2020','DD,MM,YYYY')
                and to_char(data,'d')='1'
            UNION
            select
                'R2' as Residencia_tipus,
                sum(setmana_casos) as N
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (7,8)
                and perfil='Personal'
                and data between to_date('01,07,2020','DD,MM,YYYY')
                             and to_date('31,12,2020','DD,MM,YYYY')
                and to_char(data,'d')='1'
            """
        for res_tip, N in u.getAll(sql, 'redics'):
            ind = '2.1 - N acumulado de casos confirmados COVID-19 por PDIA del personal (ambos periodos: 01.07.20 a 31.12.20)'  # noqa
            periode = '01/07/2020 - 31/12/2020'
            self.ind_21.append((ind, periode, res_tip, N))
        print("INDICADOR 2.1: N de registros:", len(self.ind_21))
        print(self.ind_21)

    def get_exitus(self):
        """ . """
        print("--------------------------------------------------- get_exitus")
        self.exitus_pac = set()
        self.exitus_pac_socio = set()
        self.exitus_pac_domi = set()
        self.exitus_pac_hosp = set()
        self.exitus_pac_res = set()
        self.exitus_pac_nd = set()
        self.exitus = {}
        self.exitus_upload = []
        sql = """select
                    a.cip,
                    data_exitus,
                    perfil,
                    lloc_exitus_categoritzat,
                    afectat,
                    residencia
                from
                    dwaquas.f_covid19_v2 a, residencies_cens b
                where
                    substr(a.cip, 0, 13) = b.cip
                    and a.data_exitus = b.data
                    and exitus = 'Si'
                    and data_exitus between to_date('14,3,2020','DD,MM,YYYY')
                                        and to_date('31,12,2020','DD,MM,YYYY')
                    and perfil = 'Resident'"""
        for cip13, data, perfil, lloc, afectat, residencia in u.getAll(sql, 'exadata'):  # noqa
            self.exitus_pac.add(cip13)
            self.exitus[(cip13, data, lloc, perfil, afectat, residencia)] = True  # noqa
        #     self.exitus_upload.append((cip13, data, lloc, perfil, afectat, residencia))  # noqa
        #     if lloc == 'Sociosanitari':
        #         self.exitus_pac_socio.add(cip13)
        #     if lloc == 'Domicili':
        #         self.exitus_pac_domi.add(cip13)
        #     if lloc == 'Hospital':
        #         self.exitus_pac_hosp.add(cip13)
        #     if lloc in ('Residència','Resid\xe8ncia'):
        #         self.exitus_pac_res.add(cip13)
        #     if lloc == 'No classificat':
        #         self.exitus_pac_nd.add(cip13)
        print("EXITUS: N de registros:", len(self.exitus))
        # print(self.exitus)
        # print(len(self.exitus_pac))
        # print(len(self.exitus_pac_socio))
        # print(len(self.exitus_pac_domi))
        # print(len(self.exitus_pac_hosp))
        # print(len(self.exitus_pac_res))
        # print(len(self.exitus_pac_nd))
        # file = u.tempFolder + 'prueba_{}.csv'
        # file = file.format(TODAY.strftime('%Y%m%d'))
        # u.writeCSV(file,
        #            self.exitus_upload,  # noqa
        #            sep = ";")

    def get_resultat(self):
        """ . """
        print("------------------------------------------------- get_resultat")
        self.resultat = c.Counter()
        afectatc = {"Sospit\xf3s": "Compatible",
                    "Sospitós": "Compatible",
                    "Positiu per ELISA": "Confirmat",
                    "Positiu": "Confirmat",
                    "Positiu Epidemiol\xf2gic": "Compatible",
                    "Positiu Epidemiològic": "Compatible",
                    "Positiu per Test R\xe0pid": "Confirmat",
                    "Positiu per Test Ràpid": "Confirmat",
                    "PCR probable": "Compatible"}
        res_tipc = {1: "R1",
                    7: "R2",
                    8: "R2"}
        for _cip13, data, lloc, perfil, afectat, residencia in self.exitus:
            if residencia in self.cat_res:
                if self.cat_res[residencia] in (1, 7, 8):
                    if data >= d.datetime(2020,3,14,0,0) and data <= d.datetime(2020,6,22,0,0):  # noqa
                        periode = "14/03/2020 - 22/06/2020"
                    else:
                        periode = "23/06/2020 - 31/12/2020"
                    res_tip = res_tipc[self.cat_res[residencia]]
                    self.resultat[(periode, res_tip, afectatc[afectat], lloc, perfil)] += 1  # noqa

    def get_indicador_33(self):
        """ . """
        print("--------------------------------------------- get_indicador_33")
        self.ind_33 = []
        for (periode, res_tip, afectat, lloc, _perfil), N in self.resultat.items():  # noqa
            if afectat == 'Confirmat':
                if periode == '14/03/2020 - 22/06/2020':
                    if lloc == 'Hospital':
                        ind = '3.3 - Fallecimientos (acumulado) por COVID-19 confirmado en periodo 1 (14.03.20 a 22.06.20) - HOSPITAL'  # noqa
                        self.ind_33.append((ind, periode, res_tip, N))
                    if lloc in ('Residència', 'Resid\xe8ncia'):
                        ind = '3.3 - Fallecimientos (acumulado) por COVID-19 confirmado en periodo 1 (14.03.20 a 22.06.20) - CENTRO'  # noqa
                        self.ind_33.append((ind, periode, res_tip, N))
                    if lloc == 'Domicili':
                        ind = '3.3 - Fallecimientos (acumulado) por COVID-19 confirmado en periodo 1 (14.03.20 a 22.06.20) - DOMICILIO'  # noqa
                        self.ind_33.append((ind, periode, res_tip, N))
                    if lloc == 'Sociosanitari':
                        ind = '3.3 - Fallecimientos (acumulado) por COVID-19 confirmado en periodo 1 (14.03.20 a 22.06.20) - SOCIOSANITARIO'  # noqa
                        self.ind_33.append((ind, periode, res_tip, N))
                    if lloc == 'No classificat':
                        ind = '3.3 - Fallecimientos (acumulado) por COVID-19 confirmado en periodo 1 (14.03.20 a 22.06.20) - NO CLASIFICADO'  # noqa
                        self.ind_33.append((ind, periode, res_tip, N))
        print(self.ind_33)

    def get_indicador_34(self):
        """ . """
        print("--------------------------------------------- get_indicador_34")
        self.ind_34 = []
        for (periode, res_tip, afectat, lloc, _perfil), N in self.resultat.items():  # noqa
            if afectat == 'Compatible':
                if periode == '14/03/2020 - 22/06/2020':
                    if lloc == 'Hospital':
                        ind = '3.4 - Fallecimientos (acumulado) compatibles con COVID-19 en periodo 1 (14.03.20 a 22.06.20) - HOSPITAL'  # noqa
                        self.ind_34.append((ind, periode, res_tip, N))
                    if lloc in ('Residència', 'Resid\xe8ncia'):
                        ind = '3.4 - Fallecimientos (acumulado) compatibles con COVID-19 en periodo 1 (14.03.20 a 22.06.20) - CENTRO'  # noqa
                        self.ind_34.append((ind, periode, res_tip, N))
                    if lloc == 'Domicili':
                        ind = '3.4 - Fallecimientos (acumulado) compatibles con COVID-19 en periodo 1 (14.03.20 a 22.06.20) - DOMICILIO'  # noqa
                        self.ind_34.append((ind, periode, res_tip, N))
                    if lloc == 'Sociosanitari':
                        ind = '3.4 - Fallecimientos (acumulado) compatibles con COVID-19 en periodo 1 (14.03.20 a 22.06.20) - SOCIOSANITARIO'  # noqa
                        self.ind_34.append((ind, periode, res_tip, N))
                    if lloc == 'No classificat':
                        ind = '3.4 - Fallecimientos (acumulado) compatibles con COVID-19 en periodo 1 (14.03.20 a 22.06.20) - NO CLASIFICADO'  # noqa
                        self.ind_34.append((ind, periode, res_tip, N))
        print(self.ind_34)

    def get_indicador_35(self):
        """ . """
        print("--------------------------------------------- get_indicador_35")
        self.ind_35 = []
        for (periode, res_tip, afectat, lloc, _perfil), N in self.resultat.items():  # noqa
            if afectat == 'Confirmat':
                if periode == '23/06/2020 - 31/12/2020':
                    if lloc == 'Hospital':
                        ind = '3.5 - Fallecimientos (acumulado) por COVID-19 en periodo 2 (23/06/2020 a 31/12/2020) - HOSPITAL'  # noqa
                        self.ind_35.append((ind, periode, res_tip, N))
                    if lloc in ('Residència', 'Resid\xe8ncia'):
                        ind = '3.5 - Fallecimientos (acumulado) por COVID-19 en periodo 2 (23/06/2020 a 31/12/2020) - CENTRO'  # noqa
                        self.ind_35.append((ind, periode, res_tip, N))
                    if lloc == 'Domicili':
                        ind = '3.5 - Fallecimientos (acumulado) por COVID-19 en periodo 2 (23/06/2020 a 31/12/2020) - DOMICILIO'  # noqa
                        self.ind_35.append((ind, periode, res_tip, N))
                    if lloc == 'Sociosanitari':
                        ind = '3.5 - Fallecimientos (acumulado) por COVID-19 en periodo 2 (23/06/2020 a 31/12/2020) - SOCIOSANITARIO'  # noqa
                        self.ind_35.append((ind, periode, res_tip, N))
                    if lloc == 'No classificat':
                        ind = '3.5 - Fallecimientos (acumulado) por COVID-19 en periodo 2 (23/06/2020 a 31/12/2020) - NO CLASIFICADO'  # noqa
                        self.ind_35.append((ind, periode, res_tip, N))
        print(self.ind_35)

    def get_upload(self):
        """ . """
        print("--------------------------------------------------- get_upload")
        self.upload = []
        for ind, periode, res_tip, N in self.ind_11:
            self.upload.append((ind, periode, res_tip, N))
        for ind, periode, res_tip, N in self.ind_12:
            self.upload.append((ind, periode, res_tip, N))
        for ind, periode, res_tip, N in self.ind_13a:
            self.upload.append((ind, periode, res_tip, N))
        for ind, periode, res_tip, N in self.ind_13b:
            self.upload.append((ind, periode, res_tip, N))
        for ind, periode, res_tip, N in self.ind_14a:
            self.upload.append((ind, periode, res_tip, N))
        for ind, periode, res_tip, N in self.ind_14b:
            self.upload.append((ind, periode, res_tip, N))
        for ind, periode, res_tip, N in self.ind_21:
            self.upload.append((ind, periode, res_tip, N))
        for ind, periode, res_tip, N in self.ind_33:
            self.upload.append((ind, periode, res_tip, N))
        for ind, periode, res_tip, N in self.ind_34:
            self.upload.append((ind, periode, res_tip, N))
        for ind, periode, res_tip, N in self.ind_35:
            self.upload.append((ind, periode, res_tip, N))
        print(self.upload)

    def export_upload(self):
        """."""
        print("------------------------------------------------ export_upload")
        file = u.tempFolder + 'imsersoFitxa1a_{}.csv'
        file = file.format(TODAY.strftime('%Y%m%d'))
        u.writeCSV(file,
                   [('Indicador', 'Periode','Tipus de centre', 'N')] + self.upload,  # noqa
                   sep=";")
        print("Send Mail")
        subject = 'Imserso - Fitxa 1a'
        text = 'Adjuntem arxiu amb les dades per la 1a fitxa'
        u.sendGeneral('SISAP <sisap@gencat.cat>',
                      'avilar@gencat.cat',
                      'ehermosilla@idiapjgol.info',
                      subject,
                      text,
                      file)
        remove(file)


if __name__ == '__main__':
    print(datetime.now())
    ts = datetime.now()
    imsersoFitxa()
    print('Time execution {}'.format(datetime.now() - ts))
