from sisapUtils import getSubTables, getOne, getAll, multiprocess, createTable, listToTable, grantSelect
from collections import Counter


data = 2015
redics_table = 'sisap_determinacions'


def get_tables():
    tables = []
    for table in getSubTables('laboratori'):
        dat, = getOne("select cr_data_reg from {} where cr_codi_lab <> 'RIMAP' limit 1".format(table), 'import')
        if dat.year == data:
            tables.append(table)
    return tables


def get_data(param):
    table, pacients = param
    dades = Counter()
    sql = "select id_cip_sec, cr_codi_prova_ics from {}".format(table)
    for id, cod in getAll(sql, 'import'):
        if id in pacients:
            dades[cod] += 1
    return [(cod, n) for (cod, n) in dades.items()]


if __name__ == '__main__':
    # per evitar contar repetides vegades un mateix registre original (per id_cip / id_cip_sec) hi ha dues opcions: fer sets de id_cip + data + prova, o creuar per assignats actius
    pacients = set([id for id, in getAll("select id_cip_sec from assignada_tot where up in (select scs_codi from cat_centres where ep = '0208')", 'nodrizas')])
    resul = Counter()
    for dades in multiprocess(get_data, [(table, pacients) for table in get_tables()]):
        for cod, n in dades:
            resul[cod] += n
    cataleg = {cod: des for (cod, des) in getAll('select cpi_codi_prova cod, cpi_desc_prova des from labtb112 union select cpr_codi_prova cod, cpr_desc des from labtb120', 'redics')}
    upload = []
    for cod, n in resul.items():
        des = cataleg[cod] if cod in cataleg else ''
        upload.append((cod, des, n))
    createTable(redics_table, '(codi varchar2(10), descripcio varchar2(255), recompte integer)', 'redics', rm=True)
    listToTable(sorted(upload, key=lambda x: x[2], reverse=True), redics_table, 'redics')
    grantSelect(redics_table, ('PREDUMMP', 'PREDUPRP'), 'redics')
