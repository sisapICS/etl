# coding: iso-8859-1
"""
Petici�
    - Drive:
    - Trello: https://trello.com/c/PU75ohFf

Fase PILOT 1:

Fase PILOT 2:

Fase IMPLEMENTACI� (Inici 22 de febrer de 2021):
 - Data: 22 de febrer
 - Centres:
    - Terres de l?Ebre:
        - ABS Flix UP 00043
        - EAP Mora de Nova-Mora d?Ebre UP 00046
    - Tarragona:
        - EAP Tarragona-3 (Jaume I) UP 00066
        - ABS Montroig del Camp UP 00045

Fase IMPLEMENTACI�:
 - Data: 1 de mar�
 - Centres:
    - Terres de l?Ebre:

    - Tarragona:
        - ABS TARRAGONA-2 (Torreforta) UP 00051
        - ABS REUS-4 (Rambla Mir�) UP 00048

Fase IMPLEMENTACI�:
 - Data: 8 de mar�
 - Centres
    - Tarragona:
        - EAP Valls UP 00054
        - EAP Montblanc UP00044
                - PAC Montblanc UP 04909
                - PAC Torreforta UP 04904
                - PAC Mont- roig UP 04898

Fase IMPLEMENTACI�:
 - Data: 15 de mar�
 - Centres
    - Terres de l?Ebre:
        - ABS Terra Alta (GandesaA)    UP 00053
    - Tarragona:
        - EAP llibertat-Reus 3 UP 00062
        - EAP Bonavista_ Tarragona 1 UP 00050
                - PAC Bonavista UP 04905
        - EAP El Morell UP 00060
"""

import sisapUtils as u
import sisaptools as t
import collections as c
import datetime as d
from datetime import datetime
from dateutil.relativedelta import relativedelta
from os import remove


# TODAY = d.date(2021, 4, 26)
TODAY = d.datetime.now().date()
print(TODAY)
DATE_INICI = d.date(2021, 1, 12)
DATE1 = TODAY - relativedelta(days=5)
DATE0 = TODAY - relativedelta(days=11)
print((DATE0, DATE1))

ARXIU_PROFESSIONALS = "./plantilla_infermeres_per_PI001_bis.csv"

class PrescripcioInfermeria(object):
    """ . """

    def __init__(self):
        """ . """
        self.get_centres()
        self.get_professionals()
        self.get_poblacio()
        self.get_sisap_ppftb126()
        """
        self.get_ppftb016()
        self.get_cpftb006()
        """
        self.ind_eap = c.Counter()
        self.ind_assir = c.Counter()
        self.ind_cuap = c.Counter()
        self.get_PI001()
        """
        self.get_PI002()
        self.get_PI003_4_5()
        self.get_PI006()
        self.get_PI007()
        self.get_PI008()
        self.get_PI009()
        """
        self.get_upload()
        self.export_upload()

    def get_centres(self):
        """ . """
        self.cat_centres_eap = {}
        self.br_up = {}
        sql_eap = """
                select
                    sector
                    , scs_codi
                    , ics_codi
                    , ics_desc
                    , amb_desc
                    , sap_desc
                from
                    cat_centres
                """
        for sector, up, br, up_desc, amb_desc, sap_desc in u.getAll(sql_eap, 'nodrizas'):  # noqa
            if amb_desc != "�MBIT ALI� A L'ICS":
                self.cat_centres_eap[up] = {'sector': sector,
                                            'br': br,
                                            'up_desc': up_desc,
                                            'amb_desc': amb_desc,
                                            'sap_desc': sap_desc}
                self.br_up[br] = up
        print("N CENTRES EAP:", len(self.cat_centres_eap))

        self.cat_centres_ass = {}
        sql_assir = """
                select
                    up
                    , br
                    , up_assir
                    , assir
                    , amb_desc
                    , sap_desc
                from
                   ass_centres
                """
        for up, br, ass_up, ass_desc, amb_desc, sap_desc in u.getAll(sql_assir, 'nodrizas'):  # noqa
            self.cat_centres_ass[up] = {'ass_up': ass_up,
                                        'br': br,
                                        'ass_desc': ass_desc,
                                        'amb_desc': amb_desc,
                                        'sap_desc': sap_desc}
            self.br_up[br] = up
        print("N CENTRES ASSIR:", len(self.cat_centres_ass))

        self.cat_centres_cuap = {}
        sql_cuap = """
                select 
                    cup_cup
                    , cup_nup
                from
                    REPTBCUP 
                where 
                    cup_nup like '%CUAP%'
                """
        sql_cuap = """select up_cod
                            , up_des
                            , sap_des
                            , amb_des 
                        from DWSISAP.SISAP_CORONAVIRUS_CAT_UP 
                        where 
                            UP_TIP like '%continuada%'
                    """

        for up, up_desc, sap_desc, amb_desc in t.Database("exadata", "data").get_all(sql_cuap):
            if up != "00536":
                self.cat_centres_cuap[up] = {'up_desc': up_desc,
                                             'amb_desc': amb_desc,
                                             'sap_desc': sap_desc}
        """
        for up, nom_up in u.getAll(sql_cuap, 'redics'):  # noqa
            self.cat_centres_cuap[up] = {'nom_up': nom_up}
        """
        print("N CENTRES CUAP:", len(self.cat_centres_cuap))
    

    def get_professionals(self):
        """ . """
        self.cat_professionals_eap = c.defaultdict(dict)
        self.set_professionals_eap = set()
        self.cat_professionals_assir = c.defaultdict(dict)
        self.set_professionals_assir = set()
        self.set_dnis=c.defaultdict(set)
        self.ups_ics = set()
        self.dni_numcol = {}

        sql_numcol_dni = "select ide_dni, ide_numcol from cat_pritb992 where ide_numcol <> '' and ide_dni <> ''"
        for nif, numcol in u.getAll(sql_numcol_dni, "import"):
            nif = nif.replace(" ","")
            if len(nif) == 9:
                nif = nif[:8]
            elif len(nif) == 10:
                nif = nif[1:9]
            self.dni_numcol[nif] = numcol

        for nif, categoria, br, br_dedicacio in u.readCSV(ARXIU_PROFESSIONALS, sep= ","):
            if br in self.br_up:
                nif = nif.replace(" ","")
                self.ups_ics.add(self.br_up[br])
                if len(nif) == 9:
                    nif = nif[:8]
                elif len(nif) == 10:
                    nif = nif[1:9]
                if nif in self.dni_numcol:
                    up = self.br_up[br]
                    numcol = self.dni_numcol[nif]
                    if categoria != "AGDLLE":
                        self.cat_professionals_eap[up][numcol] = {'dni': nif, 'cat': categoria, 'ponderacio': br_dedicacio}
                        self.set_professionals_eap.add(numcol)
                    elif categoria == "AGDLLE":
                        self.cat_professionals_assir[up][numcol] = {'dni': nif, 'cat': categoria, 'ponderacio': br_dedicacio}
                        self.set_professionals_assir.add(numcol)

        sql_eap = """
                select
                    ide_numcol,
                    up,
                    ide_dni,
                    tipus,
                    servei
                from
                    cat_professionals
                where
                    servei in ('INF','INFC','INFG','INFGR','INFP','INFPD','GCAS')
                """
        for numcol, up, dni, tipus, servei in u.getAll(sql_eap, 'import'):
            if up not in self.ups_ics:
                numcol = numcol[:8]
                self.cat_professionals_eap[up][numcol] = {'dni': dni,
                                                          'tipus': tipus,
                                                          'servei': servei}
                self.set_professionals_eap.add(numcol)
                if up in self.cat_centres_eap:
                    br = self.cat_centres_eap[up]['br']
                    if self.cat_centres_eap[up]["amb_desc"] != "�MBIT ALI� A L'ICS":
                        self.set_dnis[br].add(dni)
        print("N PROFESSIONALS INFERMERES EAP:", len(self.set_professionals_eap))

        sql_assir = """
                select
                    ide_numcol,
                    up,
                    ide_dni,
                    tipus,
                    servei
                from
                    cat_professionals_assir
                where
                    tipus = 'L'
                """
        for numcol, up, dni, tipus, servei in u.getAll(sql_assir, 'import'):
            if up not in self.ups_ics:
                numcol = numcol[:8]
                self.cat_professionals_assir[up][numcol] = {'dni': dni,
                                                            'tipus': tipus,
                                                            'servei': servei}
                self.set_professionals_assir.add(numcol)
        print("N PROFESSIONALS LLEVADORES ASSIR:", len(self.set_professionals_assir))

        self.cat_professionals_cuap = c.defaultdict(set)
        self.set_professionals_cuap = set()
        sql_cuap = """
                        select 
                            visi_col_prov_resp, visi_up 
                        from
                            visites1
                        """.format("','".join(self.cat_centres_cuap.keys()))
        for numcol, up in u.getAll(sql_cuap, "import"):
            numcol = numcol[:8]
            self.cat_professionals_cuap[up].add(numcol)
            self.set_professionals_cuap.add(numcol)
        print("N PROFESSIONALS INFERMERES CUAP:", len(self.set_professionals_cuap))


    def get_poblacio(self):
        """ . """
        self.poblacio_eap = c.defaultdict(dict)
        self.set_poblacio_eap = set()

        sql_eap = """
        select
            id_cip_sec
            , up
            , uba
        from
            assignada_tot
        """
        for id_cip_sec, up, uba in u.getAll(sql_eap, 'nodrizas'):
            self.poblacio_eap[up][id_cip_sec] = {'uba': uba}
            self.set_poblacio_eap.add(id_cip_sec)
        print("N PACIENTS POBLACIO EAP:", len(self.set_poblacio_eap))

        self.poblacio_assir = c.defaultdict(set)
        self.set_poblacio_assir = set()
        sql_assir = """
                select id_cip_sec, visi_up from nodrizas.ass_visites, nodrizas.dextraccio
                where visi_data_visita between date_add(date_add(data_ext,interval - 12 month),interval +1 day) and data_ext
            """
        for id_cip_sec, up in u.getAll(sql_assir, 'nodrizas'):
            self.poblacio_assir[up].add(id_cip_sec)
            self.set_poblacio_assir.add(id_cip_sec)
        print("N PACIENTS POBLACIO ASSIR:", len(self.set_poblacio_assir))

        self.poblacio_cuap = c.defaultdict(set)
        self.set_poblacio_cuap = set()
        sql_cuap = """
                select 
                    id_cip_sec
                    , visi_up 
                from 
                    visites1
                where 
                    s_espe_codi_especialitat like '3%'
            """
        for id_cip_sec, up in u.getAll(sql_cuap, 'import'):
            self.poblacio_cuap[up].add(id_cip_sec)
            self.set_poblacio_cuap.add(id_cip_sec)
        print("N PACIENTS POBLACIO CUAP:", len(self.set_poblacio_cuap))


    def get_sisap_ppftb126(self):
        """ . """
        self.ppftb126_eap = c.defaultdict(dict)
        self.set_ppftb126_eap = set()
        self.ppftb126_assir = c.defaultdict(dict)
        self.set_ppftb126_assir = set()
        self.ppftb126_cuap = c.defaultdict(dict)
        self.set_ppftb126_cuap = set()

        sql = """
                   select
                       usu_cod_up
                       , usu_nif
                       , usu_num_col
                       , usu_data_alta
                       , usu_data_modif
                       , usu_cod_catprofecap
                   from
                       sisap_ppftb126
                   where
                       usu_autonom = 'S'
               """
        for up, nif, numcol, dat, dat_mod, cod_catprofecap in u.getAll(sql, 'redics'):
            numcol = numcol[:8]
            if dat_mod is not None:
                dat_f = dat_mod.date()
            else:
                dat_f = dat.date()
            if up in self.cat_centres_eap:
                if numcol in self.cat_professionals_eap[up]:
                    self.ppftb126_eap[up][numcol] = {'nif': nif,'dat': dat_f}
                    self.set_ppftb126_eap.add(numcol)
            if up in self.cat_centres_ass:
                if numcol in self.cat_professionals_assir[up]:
                    self.ppftb126_assir[up][numcol] = {'nif': nif,'dat': dat_f}
                    self.set_ppftb126_assir.add(numcol)
            if up in self.cat_centres_cuap:
                if numcol in self.cat_professionals_cuap[up]:
                    self.ppftb126_cuap[up][numcol] = {'nif': nif, 'dat': dat_f}
                    self.set_ppftb126_cuap.add(numcol)
        print("N NUMCOL (INF) cat_professionals ", len(self.set_ppftb126_eap))
        print("N NUMCOL (LLEVADORES) cat_professionals ", len(self.set_ppftb126_assir))
        print("N NUMCOL (CUAP) cat_professionals ", len(self.set_ppftb126_cuap))


    def get_ppftb016(self):
        """ . """
        ts = d.datetime.now()
        self.ppftb016_eap = []
        self.ppftb016_assir = []
        self.ppftb016_cuap = []
        sql = """
        select
            id_cip_sec
            , ppfmc_pf_codi
            , ppfmc_atccodi
            , ppfmc_pmc_data_ini
            , ppfmc_data_fi
            , ppfmc_pmc_amb_cod_up
            , ppfmc_pmc_amb_num_col
            , ppfmc_desc_esp
        from
            tractaments
        where
            ppfmc_data_fi >= '2021-01-12'
        """
        for id_cip_sec, pf, atc, dat, datfi, up, numcol, esp in u.getAll(sql, 'import'):
            if id_cip_sec in self.poblacio_eap[up]:
                self.ppftb016_eap.append((id_cip_sec, pf, atc, dat, datfi, up, numcol, esp))
            if id_cip_sec in self.poblacio_assir[up]:
                self.ppftb016_assir.append((id_cip_sec, pf, atc, dat, datfi, up, numcol, esp))
            if id_cip_sec in self.poblacio_cuap[up]:
                self.ppftb016_cuap.append((id_cip_sec, pf, atc, dat, datfi, up, numcol, esp))

        print("N PRESCRIPCIONS (EAP):", len(self.ppftb016_eap))
        print("N PRESCRIPCIONS (ASSIR):", len(self.ppftb016_assir))
        print("N PRESCRIPCIONS (CUAP):", len(self.ppftb016_cuap))
        print('Time execution: {}'.format(d.datetime.now() - ts))


    def get_cpftb006(self):
        """ . """
        self.cpftb006 = {}
        sql = """
            select
                pf_codi
                , pf_gt_codi
            from
                cpftb006
            where
                pf_gt_codi in ('01D00',
                               '23C01', '23C04', '23C00', '23C06', '23C05',
                               '23C03', '23C02', '23C')
        """
        for pf, gt in u.getAll(sql, 'redics'):
            self.cpftb006[pf] = {'gt': gt}
        print("CPFTB006 N PF:", len(self.cpftb006))


    def get_PI001(self):
        """ . """
        self.pi001_eap = c.Counter()
        for up in self.cat_professionals_eap:
            for numcol in self.cat_professionals_eap[up]:
                if up in self.cat_centres_eap:
                    up_desc = self.cat_centres_eap[up]['up_desc']
                    amb_desc = self.cat_centres_eap[up]['amb_desc']
                    sap_desc = self.cat_centres_eap[up]['sap_desc']
                    self.pi001_eap[(up, 'PI001', 'DEN')] += 1
                    self.ind_eap[('PI001', '% INF actives', amb_desc, sap_desc, up, up_desc, 'DEN')] += 1
                    if up in self.ppftb126_eap:
                        if numcol in self.ppftb126_eap[up]:
                            dat = self.ppftb126_eap[up][numcol]['dat']
                            if dat <= DATE1:
                                self.pi001_eap[(up, 'PI001', 'NUM')] += 1
                                self.ind_eap[('PI001', '% INF actives', amb_desc, sap_desc, up, up_desc, 'NUM')] += 1
        print("PI001 (EAP):", self.pi001_eap)

        self.pi001_assir = c.Counter()
        for up in self.cat_professionals_assir:
            for numcol in self.cat_professionals_assir[up]:
                if up in self.cat_centres_ass:
                    ass_up = self.cat_centres_ass[up]['ass_up']
                    ass_desc = self.cat_centres_ass[up]['ass_desc']
                    amb_desc = self.cat_centres_ass[up]['amb_desc']
                    sap_desc = self.cat_centres_ass[up]['sap_desc']
                    self.pi001_assir[(up, 'PI001', 'DEN')] += 1
                    self.ind_assir[('PI001', '% INF actives', amb_desc, sap_desc, up, ass_desc, 'DEN')] += 1
                    if up in self.set_ppftb126_assir:
                        if numcol in self.set_ppftb126_assir[up]:
                            dat = self.ppftb126_assir[up][numcol]['dat']
                            if dat <= DATE1:
                                self.pi001_assir[(up, 'PI001', 'NUM')] += 1
                                self.ind_assir[('PI001', '% INF actives', amb_desc, sap_desc, up, ass_desc, 'NUM')] += 1
        print("PI001 (ASSIR):", self.pi001_assir)

        self.pi001_cuap = c.Counter()
        for up in self.cat_professionals_cuap:
            for numcol in self.cat_professionals_cuap[up]:
                if up in self.cat_centres_cuap:
                    up_desc = self.cat_centres_cuap[up]['up_desc']
                    amb_desc = self.cat_centres_cuap[up]['amb_desc']
                    sap_desc = self.cat_centres_cuap[up]['sap_desc']
                    self.pi001_cuap[(up, 'PI001', 'DEN')] += 1
                    self.ind_cuap[('PI001', '% INF actives', amb_desc, sap_desc, up, up_desc, 'DEN')] += 1
                    if up in self.set_ppftb126_cuap:
                        if numcol in self.set_ppftb126_cuap[up]:
                            dat = self.ppftb126_cuap[up][numcol]['dat']
                            if dat <= DATE1:
                                self.pi001_cuap[(up, 'PI001', 'NUM')] += 1
                                self.ind_cuap[('PI001', '% INF actives', amb_desc, sap_desc, up, up_desc, 'NUM')] += 1
        print("PI001 (CUAP):", self.pi001_cuap)


    def get_PI002(self):
        """ . """
        self.pi002 = c.Counter()
        for numcol in self.ppftb126_eap:
            up = self.cat_professionals_eap[numcol]['up']
            dat = self.ppftb126_eap[numcol]['dat']
            if up in self.cat_centres_eap:
                up_desc = self.cat_centres_eap[up]['up_desc']
                amb_desc = self.cat_centres_eap[up]['amb_desc']
                sap_desc = self.cat_centres_eap[up]['sap_desc']
                self.ind_eap[('PI002', 'INF actives darrera setmana', amb_desc, sap_desc, up, up_desc, 'DEN')] = 0
                self.pi002[(up, 'PI002', 'DEN')] = 0
                if DATE0 <= dat <= DATE1:
                    self.ind_eap[('PI002', 'INF actives darrera setmana', amb_desc, sap_desc, up, up_desc, 'NUM')] += 1
                    self.pi002[(up, 'PI002', 'NUM')] += 1
                    # print("PI002:", self.pi002)

    def get_PI003_4_5(self):
        """ . """
        self.pi003_up = set()
        self.pi003_up_cat = set()
        self.pi003_upnumcol = set()
        self.pi003_numcol = set()
        self.pi003_numcol_cat = set()
        self.pi003_llista = []
        self.pi003 = c.Counter()
        self.pi004 = c.Counter()
        self.pi005 = c.Counter()
        for id_cip_sec, pf, atc, dat, datfi, up, numcol, esp in self.ppftb016:  # noqa
            self.pi003_up.add((up))
            self.pi003_numcol.add((numcol))
            if up in self.cat_centres_eap:
                up_desc = self.cat_centres_eap[up]['up_desc']
                amb_desc = self.cat_centres_eap[up]['amb_desc']
                sap_desc = self.cat_centres_eap[up]['sap_desc']
                self.ind_eap[('PI003', 'INF prescripcio darrera setmana', amb_desc, sap_desc, up, up_desc, 'DEN')] = 0
                self.ind_eap[
                    ('PI004', 'Noves prescripcions darrera setmana INF', amb_desc, sap_desc, up, up_desc, 'DEN')] = 0
                self.ind_eap[('PI005', 'Noves prescripcions acumulades INF', amb_desc, sap_desc, up, up_desc, 'DEN')] = 0
                if numcol in self.cat_professionals_eap:
                    self.pi003_up_cat.add((up))
                    self.pi003_numcol_cat.add((numcol))
                    if DATE_INICI <= dat <= DATE1:
                        self.pi005[(up, 'PI005')] += 1
                        self.ind_eap[(
                        'PI005', 'Noves prescripcions acumulades INF', amb_desc, sap_desc, up, up_desc, 'NUM')] += 1
                    if DATE0 <= dat <= DATE1:
                        self.pi003_llista.append((id_cip_sec, pf, atc, dat, datfi, up, numcol, esp))
                        self.pi003_upnumcol.add((up, numcol))
                        self.pi004[(up, 'PI004')] += 1
                        self.ind_eap[('PI004', 'Noves prescripcions darrera setmana INF', amb_desc, sap_desc, up, up_desc,
                                  'NUM')] += 1
        # print("PI003:", self.pi003_upnumcol)
        # print("PI003 LLISTA:", self.pi003_llista)

        for up, _numcol in self.pi003_upnumcol:
            up_desc = self.cat_centres_eap[up]['up_desc']
            amb_desc = self.cat_centres_eap[up]['amb_desc']
            sap_desc = self.cat_centres_eap[up]['sap_desc']
            self.pi003[(up, 'PI003')] += 1
            self.ind_eap[('PI003', 'INF prescripcio darrera setmana', amb_desc, sap_desc, up, up_desc, 'NUM')] += 1
        # print("PRESCRIPCIONS INF N UP", len(self.pi003_up), self.pi003_up)
        # print("PRESCRIPCIONS INF CAT N UP", len(self.pi003_up_cat), self.pi003_up_cat)
        # print("PRESCRIPCIONS INF N NUMCOL", len(self.pi003_numcol), self.pi003_numcol)  # noqa
        # print("PRESCRIPCIONS INF CAT N NUMCOL", len(self.pi003_numcol_cat), self.pi003_numcol_cat)  # noqa
        # print("PI003:", self.pi003)
        # print("PI003 DATE0-DATE1:", self.pi003_upnumcol)
        # print("PI004 DATE0-DATE1:", self.pi004)
        # print("PI005:", self.pi005)

    def get_PI006(self):
        """ . """
        self.pi006 = c.Counter()
        for _id_cip_sec, pf, _atc, dat, _datfi, up, numcol, _esp in self.ppftb016:  # noqa
            if up in self.cat_centres_eap:
                if DATE0 <= dat <= DATE1:
                    up_desc = self.cat_centres_eap[up]['up_desc']
                    amb_desc = self.cat_centres_eap[up]['amb_desc']
                    sap_desc = self.cat_centres_eap[up]['sap_desc']
                    gt = self.cpftb006[pf]['gt'] if pf in self.cpftb006 else ''
                    if gt in ('23C01', '23C04', '23C00', '23C06', '23C05', '23C03', '23C02',
                              '23C') and up in self.cat_centres_eap:  # noqa
                        self.pi006[(up, 'PI006', 'DEN')] += 1
                        self.ind_eap[(
                        'PI006', '% Nous absorbents INF darrera setmana', amb_desc, sap_desc, up, up_desc, 'DEN')] += 1
                        if numcol in self.cat_professionals_eap:
                            self.pi006[(up, 'PI006', 'NUM')] += 1
                            self.ind_eap[('PI006', '% Nous absorbents INF darrera setmana', amb_desc, sap_desc, up, up_desc,
                                      'NUM')] += 1
                            # print((id_cip_sec, pf, atc, dat, datfi, up, numcol, esp))
        # print("PI006:", self.pi006)

    def get_PI007(self):
        """ . """
        self.pi007 = c.Counter()
        for _id_cip_sec, pf, _atc, _dat, datfi, up, numcol, _esp in self.ppftb016:  # noqa
            if up in self.cat_centres_eap:
                if DATE0 <= datfi:
                    up_desc = self.cat_centres_eap[up]['up_desc']
                    amb_desc = self.cat_centres_eap[up]['amb_desc']
                    sap_desc = self.cat_centres_eap[up]['sap_desc']
                    gt = self.cpftb006[pf]['gt'] if pf in self.cpftb006 else ''
                    if gt in ('23C01', '23C04', '23C00', '23C06', '23C05', '23C03', '23C02',
                              '23C') and up in self.cat_centres_eap:  # noqa
                        self.pi007[(up, 'PI007', 'DEN')] += 1
                        self.ind_eap[(
                        'PI007', '% Absorbents INF en plans med. actius', amb_desc, sap_desc, up, up_desc, 'DEN')] += 1
                        if numcol in self.cat_professionals_eap:
                            self.pi007[(up, 'PI007', 'NUM')] += 1
                            self.ind_eap[('PI007', '% Absorbents INF en plans med. actius', amb_desc, sap_desc, up, up_desc,
                                      'NUM')] += 1
        # print("PI007:", self.pi007)

    def get_PI008(self):
        """ . """
        self.pi008 = c.Counter()
        self.pi008_gt = c.Counter()
        self.pi008_pf = c.Counter()
        for _id_cip_sec, pf, _atc, dat, _datfi, up, numcol, _esp in self.ppftb016:  # noqa
            if up in self.cat_centres_eap:
                if DATE0 <= dat <= DATE1:
                    up_desc = self.cat_centres_eap[up]['up_desc']
                    amb_desc = self.cat_centres_eap[up]['amb_desc']
                    sap_desc = self.cat_centres_eap[up]['sap_desc']
                    gt = self.cpftb006[pf]['gt'] if pf in self.cpftb006 else ''
                    if gt == '01D00' and up in self.cat_centres_eap:
                        self.pi008_pf[(pf)] += 1
                        self.pi008_gt[(gt)] += 1
                        self.pi008[(up, 'PI008', 'DEN')] += 1
                        self.ind_eap[(
                        'PI008', '% Nous aposits INF darrera setmana', amb_desc, sap_desc, up, up_desc, 'DEN')] += 1
                        if numcol in self.cat_professionals_eap:
                            self.pi008[(up, 'PI008', 'NUM')] += 1
                            self.ind_eap[(
                            'PI008', '% Nous aposits INF darrera setmana', amb_desc, sap_desc, up, up_desc, 'NUM')] += 1
        # print("PI008:", self.pi008)
        # print("PI008 N PF:", len(self.pi008_pf))
        # print("PI008 N GT:", len(self.pi008_gt))

    def get_PI009(self):
        """ . """
        self.pi009 = c.Counter()
        for _id_cip_sec, pf, _atc, _dat, datfi, up, numcol, _esp in self.ppftb016:  # noqa
            if up in self.cat_centres_eap:
                if DATE0 <= datfi:
                    up_desc = self.cat_centres_eap[up]['up_desc']
                    amb_desc = self.cat_centres_eap[up]['amb_desc']
                    sap_desc = self.cat_centres_eap[up]['sap_desc']
                    gt = self.cpftb006[pf]['gt'] if pf in self.cpftb006 else ''
                    if gt == '01D00' and up in self.cat_centres_eap:
                        self.pi009[(up, 'PI009', 'DEN')] += 1
                        self.ind_eap[(
                        'PI009', '% Aposits INF en plans med. actius', amb_desc, sap_desc, up, up_desc, 'DEN')] += 1
                        if numcol in self.cat_professionals_eap:
                            self.pi009[(up, 'PI009', 'NUM')] += 1
                            self.ind_eap[(
                            'PI009', '% Aposits INF en plans med.actius', amb_desc, sap_desc, up, up_desc, 'NUM')] += 1
        # print("PI009:", self.pi009)

    def get_upload(self):
        """ . """
        self.upload_eap = []
        for (ind, ind_desc, amb_desc, sap_desc, up, up_desc, tip), n in self.ind_eap.items():
            if tip == 'DEN':
                num = self.ind_eap[(ind, ind_desc, amb_desc, sap_desc, up, up_desc, 'NUM')]
                ind_desc = ind + " - " + ind_desc
                self.upload_eap.append((DATE1.strftime('%Y-%m-%d'), ind_desc, amb_desc, sap_desc, up, up_desc, num, n))

        self.upload_assir = []
        for (ind, ind_desc, amb_desc, sap_desc, up, ass_desc, tip), n in self.ind_assir.items():
            if tip == 'DEN':
                num = self.ind_assir[(ind, ind_desc, amb_desc, sap_desc, up, ass_desc, 'NUM')]
                ind_desc = ind + " - " + ind_desc
                self.upload_assir.append((DATE1.strftime('%Y-%m-%d'), ind_desc, amb_desc, sap_desc, up, ass_desc, num, n))

        self.upload_urg = []
        for (ind, ind_desc, amb_desc, sap_desc, up, ass_desc, tip), n in self.ind_cuap.items():
            if tip == 'DEN':
                num = self.ind_cuap[(ind, ind_desc, amb_desc, sap_desc, up, ass_desc, 'NUM')]
                ind_desc = ind + " - " + ind_desc
                self.upload_urg.append((DATE1.strftime('%Y-%m-%d'), ind_desc, amb_desc, sap_desc, up, ass_desc, num, n))


    def export_upload(self):
        """ . """
        file_eap =  'PrescripcioInfermeria_EAP_{}.csv'
        file_eap = file_eap.format(DATE1.strftime('%Y%m%d'))
        u.writeCSV(file_eap,
                   [('DATA', 'INDICADOR', 'AMBIT', 'SAP', 'EAP_CODI', 'EAP', 'NUM', 'DEN')] + self.upload_eap,
                   sep=";")
        file_ass = 'PrescripcioInfermeria_ASSIR_{}.csv'
        file_ass = file_ass.format(DATE1.strftime('%Y%m%d'))
        u.writeCSV(file_ass,
                   [('DATA', 'INDICADOR', 'AMBIT', 'SAP', 'ASS_CODI', 'ASSIR', 'NUM', 'DEN')] + self.upload_assir,
                   sep=";")
        file_cuap = 'PrescripcioInfermeria_URG_{}.csv'
        file_cuap = file_cuap.format(DATE1.strftime('%Y%m%d'))
        u.writeCSV(file_cuap,
                   [('DATA', 'INDICADOR', 'AMBIT', 'SAP', 'CUAP_CODI', 'CENTRE', 'NUM', 'DEN')] + self.upload_urg,
                   sep=";")
        subject = 'Prescripci� Infermeria'
        text = 'Adjuntem arxiu amb les dades'
        # u.sendGeneral('SISAP <sisap@gencat.cat>',
        #               'cguiriguet.bnm.ics@gencat.cat',
        #               'ehermosilla@idiapjgol.info',
        #               subject,
        #               text,
        #               file)
        # remove(file)


if __name__ == '__main__':
    # for i in (0,7,14,21,28,35,42,49,56,63,70,77,84,91,98,105,112,119):
    #     TODAY = d.date(2021, 1, 18) + relativedelta(days=i)
    #     print(TODAY)
    #     DATE_INICI = d.date(2021, 1, 12)
    #     DATE1 = TODAY - relativedelta(days=5)
    #     DATE0 = TODAY - relativedelta(days=11)
    #     print((DATE0, DATE1))
    PrescripcioInfermeria()
