# coding: utf8

"""

Trello: https://trello.com/c/loIN0EXS

"""

import sisapUtils as u
import collections as c


class webinar(object):
    """ . """

    def __init__(self):
        """ . """

        self.get_atc()
        self.get_poblacio()
        self.get_tractament()
        self.get_problemes()
        self.get_variables()
        self.get_upload()
        self.export_upload()

    def get_atc(self):
        """ . """
        print("------------------------------------------------------ get_atc")
        self.atc = {}
        db = "import"
        tb = "cat_cpftb010"
        sql = "select atc_codi, atc_desc from {}.{}".format(db, tb)
        for atc, atc_desc in u.getAll(sql, db):
            self.atc[atc] = {'atc_desc': atc_desc}
        print('N de registros ATC: {}', len(self.atc))
        print('Ejemplo ATC: ', next(iter(self.atc.items())))

    def get_poblacio(self):
        """ . """
        self.poblacio = {}
        sql = """select id_cip_sec, edat, sexe, maca
                from
                    assignada_tot
                where
                    ep='0208'
                    and edat > 14"""
        for id, edat, sexe, maca in u.getAll(sql, 'nodrizas'):
            self.poblacio[id] = (edat, sexe, maca)
        print(len(self.poblacio))

    def get_tractament(self):
        """ . """
        self.tractament = {}
        sql = """select
                    id_cip_sec, ppfmc_atccodi,
                    ppfmc_pmc_data_ini
                from
                    tractaments, nodrizas.dextraccio
                where
                    ppfmc_pmc_data_ini <= data_ext
                    and ppfmc_data_fi >= data_ext
                    and substring(ppfmc_atccodi,1,3) in ('C10')"""
        for id, atc, dat in u.getAll(sql, 'import'):
            if id in self.poblacio:
                self.tractament[(id, atc)] = (dat)
        print(len(self.tractament))

    def get_problemes(self):
        """ . """
        self.problemes = {}
        sql = """select
                    id_cip_sec,
                    ps
                from
                    eqa_problemes
                where
                    ps in (1,7,11,18,53)"""
        self.problemes = {}
        for id, ps in u.getAll(sql, 'nodrizas'):
            self.problemes[(id, ps)] = True
        print(len(self.problemes))

    def get_variables(self):
        """ . """
        self.variables = {}
        sql = """select
                    id_cip_sec
                from
                    eqa_variables
                where
                    agrupador = 9
                    and usar = 1
                    and valor <= 100"""
        for id, in u.getAll(sql, 'nodrizas'):
            self.variables[id] = True
        print(len(self.variables))

    def get_upload(self):
        """ . """
        self.resultat = c.Counter()
        self.upload = []
        for id, atc in self.tractament:
            atc_desc = self.atc[atc]['atc_desc']
            atc5 = atc[:5]
            atc5_desc = self.atc[atc5]['atc_desc']
            edat, _sexe, macanum = self.poblacio[id]
            maca = 'maca' if macanum==1 else 'no maca'
            edat75 = 'edat>=75' if edat >= 75 else 'edat<75'
            ci = 'ci' if (id, 1) in self.problemes else 'no ci'
            avc = 'avc' if (id, 7) in self.problemes else 'no avc'
            ap = 'ap' if (id, 11) in self.problemes else 'no ap'
            mcv = 'mcv' if ci == 'ci' or avc == 'avc' or ap == 'ap' else 'no mcv'
            dm2 = 'dm2' if (id, 18) in self.problemes else 'no dm2'
            mrc = 'mrc' if (id, 53) in self.problemes else 'no mrc'
            ldl = 'ldl<=100' if id in self.variables else 'no ldl<=100'
            self.resultat[(atc5_desc, atc, atc_desc, edat75,
                           mcv, ci, avc, ap, dm2, mrc, ldl, maca)] += 1
        for (atc5_desc, atc, atc_desc, edat75,
             mcv, ci, avc, ap, dm2, mrc,
             ldl, maca), n in self.resultat.items():
            self.upload.append(('C10', atc5_desc, atc, atc_desc,
                                edat75,
                                mcv, ci, avc, ap, dm2, mrc, ldl, maca, n))
        # print(self.upload)

    def export_upload(self):
        """ . """
        file = u.tempFolder + 'webinar.csv'
        u.writeCSV(file, [('c10', 'atc5_desc', 'atc', 'atc_desc',
                           'edat75',
                           'mcv', 'ci', 'avc', 'ap', 'dm2', 'mrc',
                           'ldl100', 'maca', 'n')] + self.upload)


if __name__ == '__main__':
    u.printTime("Inici")
    webinar()
    u.printTime("Fi")
