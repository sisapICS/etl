import sisapUtils as u
import time
import collections as c
from datetime import *

db_1 = "pdp"
sql_1 = """
            SELECT
                concat(codi_sector, codi_grup) AS codi_sector_grup
            FROM
                grups_validacio
            WHERE
                compleix_criteris = 1
                AND es_benestar = 1
        """
codis_sector_grup_benestar = set([codi_sector_grup for codi_sector_grup, in u.getAll(sql_1, db_1)])

print("codis_sector_grup_benestar")

db_2 = "pdp"
sql_2 = """
            SELECT
                concat(codi_sector, codi_grup) AS codi_sector_grup
            FROM
                grups_validacio
            WHERE
                compleix_criteris = 1
                AND es_nutricionista = 1
        """
codis_sector_grup_nutricio = set([codi_sector_grup for codi_sector_grup, in u.getAll(sql_2, db_2)])

print("codis_sector_grup_nutricio")

db_3 = "pdp"
sql_3 = """
            SELECT
                usua_cip AS cip,
                usua_cip_cod AS hash
            FROM
                pdptb101
        """
cip2hash = {cip: hash for cip, hash in u.getAll(sql_3, db_3)}

print("cip2hash")

db_4 = u.sectors
sql_4 = """
            SELECT
                pagr_cip,
                concat({}, pagr_num_grup) AS codi_grup_sector
            FROM
                prstb286
        """
hashs_participants_benestar = set()
hashs_participants_nutricio = set()
for sector in db_4:
    sql = sql_4.format(sector)
    for cip, codi_grup_sector in u.getAll(sql, sector):
        if codi_grup_sector in codis_sector_grup_benestar:
            hash = cip2hash[cip]
            hashs_participants_benestar.add(hash)
        if codi_grup_sector in codis_sector_grup_nutricio:
            hash = cip2hash[cip]
            hashs_participants_nutricio.add(hash)
    print("cips_participants_sector_{}".format(sector))


hashs_participants_benestar_input = list()
for hash in hashs_participants_benestar:
    hashs_participants_benestar_input.append([hash,])

hashs_participants_nutricio_input = list()
for hash in hashs_participants_nutricio:
    hashs_participants_nutricio_input.append([hash,])      

db_export = "pdp"
tb_benestar = "PACIENTS_GRUPS_REBE"
tb_nutricio = "PACIENTS_GRUPS_NUT"

cols = "(hash varchar(40))"

u.createTable(tb_benestar, cols, db_export, rm=True)
u.createTable(tb_nutricio, cols, db_export, rm=True)

u.listToTable(list(hashs_participants_benestar_input), tb_benestar, db_export)
u.listToTable(list(hashs_participants_nutricio_input), tb_nutricio, db_export)

u.grantSelect(tb_benestar, ('PREDUMMP'), db_export)
u.grantSelect(tb_nutricio, ('PREDUMMP'), db_export)
u.grantSelect(tb_benestar, ('PREDUFFA'), db_export)
u.grantSelect(tb_nutricio, ('PREDUFFA'), db_export)

print("exportat")