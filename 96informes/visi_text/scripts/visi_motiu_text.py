import sisapUtils as u
import pandas as pd


SECTOR = '6626'
UP = '00281'
VISITES = 'vistb043'


# u.getTableColumns(VISITES, SECTOR)


sql = """
    WITH visi as (
    SELECT
        VISI_SERVEI_CODI_SERVEI as codi_servei,
        REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
            VISI_USUARI_TEXT, CHR(10), ' '),
            CHR(13), ' '),
            CHR(9), ' '),
            '|', ' '),
            '"','') as usuari_text,
        VISI_MOTIU_PRIOR as motiu_prior
    from {tb}
    where
        visi_up = {up} and
        extract(YEAR from to_date(visi_data_visita, 'J')) = 2022 and
        extract(MONTH from to_date(visi_data_visita, 'J')) = 1
        -- and visi_usuari_text not like '"%'
    )
select count(*), codi_servei, usuari_text, motiu_prior from visi
    group by
        codi_servei, usuari_text, motiu_prior
    order by
        count(*) desc
"""


upload = []
for (count, codi_servei, usuari_text, motiu_prior) in u.getAll(sql.format(tb=VISITES, up=UP), SECTOR):
    # usu_text = usuari_text.replace("\n", "").replace("\r", "")
    # usu_text = usuari_text.decode('latin1').encode('utf8')
    # upload.append((count, codi_servei, usu_text, motiu_prior))
    upload.append((count, codi_servei, usuari_text, motiu_prior))

df = pd.DataFrame(upload, columns=('count', 'CODI_SERVEI', 'USUARI_TEXT', 'MOTIU_PRIOR'))
df.to_csv(u.tempFolder + 'visi_text_00281_2022-01.xls', index=False, sep='|')
# df['USUARI_TEXT'].str.decode('iso_8859-1').str.encode('utf-8')
# df.to_excel(u.tempFolder + 'visi_text_00281_2022-01.xls')
