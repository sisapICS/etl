# coding: latin1

"""
Número de visites anuals per les agendes del servei MG dels EAP de l'ICS.
Ho tirem contra visites1 amb la bd de tancament de 2017, si es volen altres
dates és possible que calgui introduir filtres adicionals.
"""

import collections as c

import sisapUtils as u


tb = "SISAP_VISITES_PER_AGENDA"
db = "redics"


class Visites(object):
    """."""

    def __init__(self):
        """."""
        self.limits = (0, 5, 10, 15, 20, 25, 30, 60, 90, 120, 150, 180)
        self.get_centres()
        self.get_agendes()
        self.get_visites()
        self.get_visites_dia()
        self.upload_data()

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi, ics_desc from cat_centres where ep = '0208'"
        self.centres = set([row[0] for row in u.getAll(sql, "nodrizas")])
        self.up_to_br = {row[0]: list(row[1:]) for row in u.getAll(sql, "nodrizas")}

    def get_agendes(self):
        """."""
        sql = "select codi_sector, modu_centre_codi_centre, \
                      modu_centre_classe_centre, modu_servei_codi_servei, \
                      modu_codi_modul, modu_id_modul, modu_codi_up, modu_codi_uab, modu_descripcio \
               from cat_vistb027"
        self.agendes = {row[:5]: (row[0], row[5]) for row in u.getAll(sql, "import")
                        if row[6] in self.centres}
        self.son_uba = set([(row[0], row[5]) for row in u.getAll(sql, "import") if row[7]])
        self.agenda_descripcio = {(row[0], row[5]): [row[8]] + self.up_to_br[row[6]] for row in u.getAll(sql, "import") if row[6] in self.centres}

    def get_visites(self):
        """."""
        self.visites = c.Counter()
        sql = "select codi_sector, visi_centre_codi_centre, \
                      visi_centre_classe_centre, visi_servei_codi_servei, \
                      visi_modul_codi_modul, visi_data_visita \
               from visites1 \
               where visi_servei_codi_servei = 'MG' and \
                     visi_situacio_visita = 'R'"
        for row in u.getAll(sql, "import"):
            id = self.agendes.get(row[:5])
            if id:
                self.visites[(id, row[5])] += 1

    def get_visites_dia(self):
        """."""
        self.visites_dia = c.Counter()
        for (id, dia), n in self.visites.items():
            for limit in self.limits:
                if n >= limit:
                    self.visites_dia[(id, limit)] += 1

    def upload_data(self):
        """."""
        self.upload = [[id[0], id[1], 1 * (id in self.son_uba)] + self.agenda_descripcio[id] + [self.visites_dia[(id, limit)] for limit in self.limits]
                       for id in self.agendes.values() if (id, 0) in self.visites_dia]
        columns = ["codi_sector varchar2(4)", "id int", "uba int", "descripcio varchar2(55)", "br varchar2(5)", "br_desc varchar2(255)"] + ["limit_{} int".format(limit) for limit in self.limits]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        u.grantSelect(tb, ("PREDUMMP", "PREDUPRP", "PREDUECR"), db)


if __name__ == "__main__":
    Visites()
