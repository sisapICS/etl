# -*- coding = utf-8 -*-

import sisapUtils as u
from datetime import datetime as d
from dateutil.relativedelta import relativedelta
import pandas as pd
import pprint as pp

print(d.now())

"""
Número diari de prescripcions que caduquen #covid-19
LMB 2020-03-17
card# 378 https://trello.com/c/H5HuRgHC
Destinatari: Trello @mfabregase
Format: xlsx, sql-table


# Pomodoros: 6p (3h)
2020-03-17 11:17:18.882000 Start
2020-03-17 17:49:33.125000 End
"""

# PETICIÓ
"""

**petició**: número de persones assignades als equips d'atenció primària a qui se'ls caduca la medicació dia a dia durant el proper mes

entenc que es pot fer amb import.tractaments ja que és prescripció activa?

**població**: població assignada ICS i no ICS que tenen alguna prescripció activa indefinida (durada=365)

**taula**: amb dades agregades
- up
- sap
- àmbit
- dia en què es caduca la primera prescripció activa indefinida que tenen prescrita
- n 

amb aquesta taula es pot fer una taula dinàmica i mirar per dia el número de persones a qui se'ls ha d'activar la medicació
"""

# recompte de pacients.
# criteri: caduca la medicació durante el següent mes

# assignats (ics i no-ics)
# prescripcio activa indefinida (durada = 365)

# granularitat: up, sap, ambit, dia

GAP = relativedelta(months=1)
# DEXT_NEXT, = u.getOne("select date_add(data_ext, interval 1 day) from dextraccio", "nodrizas")
DEXT_NEXT = d.now().date()
LOOKAHEAD = DEXT_NEXT + GAP
GAP, DEXT_NEXT, LOOKAHEAD

meta ={
    "poblacio":{
        "db": "nodrizas",
        "tb": "assignada_tot",
        "colset": "id_cip_sec, up",
        "flt": ""},
    "tractaments":{ # 56210 filas, WTF?, 55445 en primera fecha
        "db": "import",
        "tb": "tractaments",
        "colset": "id_cip_sec, ppfmc_data_fi",
        "flt": "WHERE ppfmc_durada = 365 and ppfmc_data_fi between date '{}' and date '{}'".format(DEXT_NEXT, LOOKAHEAD)},
    "centres":{
        "db": "nodrizas",
        "tb": "cat_centres",
        "colset": "scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc",
        "rename": {"scs_codi":"up"},
        "flt": ""},
}

def descTable(targetTable, n=3):
    db, tb, cols = meta[targetTable]["db"], meta[targetTable]["tb"], meta[targetTable]["colset"] if "colset" in meta[targetTable] else "*"
    print("\n {} <- {}.{} \n".format(targetTable, meta[targetTable]["db"], meta[targetTable]["tb"]))
    print("{} ->\n".format([col for col in u.getTableColumns(tb, db)]))
    print(cols)
    flt = "" if "flt" not in meta[targetTable] else meta[targetTable]["flt"]
    sql = """select {} from {} {} limit {}""".format(cols, tb, flt, n)
    print(sql)
    for row in u.getAll(sql, db):
       print(row)

def objectify(targetTable):
    db, tb, cols = meta[targetTable]["db"], meta[targetTable]["tb"], meta[targetTable]["colset"]
    flt = "" if "flt" not in meta[targetTable] else meta[targetTable]["flt"]
    sql = """select distinct {} from {} {}""".format(cols, tb, flt)
    print(sql)
    tb_df = pd.DataFrame.from_records(data = [row for row in u.getAll(sql, db)], columns = cols.split(', '))
    meta[targetTable]["tb_df"] = tb_df
    if "rename" in meta[targetTable]:
        meta[targetTable]["tb_df"].rename(columns = meta[targetTable]["rename"], inplace= True)
    print(tb_df.info())
    print("{} size: {}".format(targetTable, meta[targetTable]['tb_df'].shape))


# CONTROL DEL PROCESO
## para describir tablas y almacenar dataFrames en meta[table]["tb_df"]
for table in meta:
    inici = d.now()
    descTable(table, 2)
    objectify(table)
    final = d.now()
    print("{}: {}".format(final - inici, table))

for table in meta:
    meta[table]["tb_df"].info()
    meta[table]["tb_df"][:5]
    if table == 'centres':
        for descField in ['ics_desc','sap_desc','amb_desc']:
            meta[table]["tb_df"][descField] = meta[table]["tb_df"][descField].str.decode('iso_8859-1').str.encode('utf-8')

tractaments = meta["tractaments"]["tb_df"].copy()

tractaments.set_index(['id_cip_sec'], inplace= True)
meta["poblacio"]["tb_df"].set_index('id_cip_sec', inplace= True)

# tractaments.groupby("id_cip_sec").filter(lambda x: len(x) >1).groupby("id_cip_sec").agg(["min","count","max"])
tractaments = tractaments.groupby("id_cip_sec").agg("min").copy()
tractaments["up"] = meta["poblacio"]["tb_df"]["up"]
tractaments.reset_index(inplace= True)
# tractaments_up = tractaments.merge(meta["poblacio"]["tb_df"], on= "id_cip_sec").groupby(["up", "ppfmc_data_fi"]).agg("count").copy()
tractaments_up = tractaments.groupby(["up", "ppfmc_data_fi"]).agg("count").copy()
tractaments_up.head()

tractaments_up.reset_index(inplace = True)

# output_cols = ['up', 'ics_desc', 'ppfmc_data_fi', 'id_cip_sec', 'sap_codi', 'sap_desc', 'amb_codi', 'amb_desc']
tractaments_up.merge(meta["centres"]["tb_df"], on= "up").to_excel(u.tempFolder + "presc_caducitats_pre_{}.xlsx".format(LOOKAHEAD), index= False)
