# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime, timedelta
from time import time,sleep

import sisapUtils as u



class tests_virus(object):
    """."""

    def __init__(self):
        """."""
        self.get_dades()
        self.export_mail()
        
    def get_dades(self):
        """."""
        
        SIDICS_DB = ("diagnosticat", "x0002")
        
        recomptes = c.Counter()
        self.upload = [] 
        sql = """ select data, edat, regio, diari_test, diari_adenovirus, diari_vrs, diari_influenza_a, diari_influenza_b, diari_sars_cov2 
        from  diagnosticat.multitest"""
        for  data, edt, regio, test, adenov, vrs, infA, infB, sars2 in  u.getAll(sql, SIDICS_DB):
            recomptes[(data, edt, regio, 'ntest')] += test
            recomptes[(data, edt, regio, 'adenovirus')] += adenov
            recomptes[(data, edt, regio, 'VRS')] += vrs
            recomptes[(data, edt, regio, 'influenzaA')] += infA
            recomptes[(data, edt, regio, 'influenzaB')] += infB
            recomptes[(data, edt, regio, 'sars2')] += sars2
        
        self.upload.append(['Data', 'Grup_edat', 'Regio', 'Tests', 'Adenovirus', 'VRS', 'InfluenzaA', 'InfluenzaB', 'sars2'])
        
        for (data, edt, regio, tipus), n in recomptes.items():
            if tipus == 'ntest':
                adeno = recomptes[(data, edt, regio, 'adenovirus')] if (data, edt, regio, 'adenovirus') in recomptes else 0
                vrs = recomptes[(data, edt, regio, 'VRS')] if (data, edt, regio, 'VRS') in recomptes else 0
                infA = recomptes[(data, edt, regio, 'influenzaA')] if (data, edt, regio, 'influenzaA') in recomptes else 0
                infB = recomptes[(data, edt, regio, 'influenzaB')] if (data, edt, regio, 'influenzaB') in recomptes else 0
                sars2 = recomptes[(data, edt, regio, 'sars2')] if (data, edt, regio, 'sars2') in recomptes else 0
                self.upload.append([data, edt, regio, n, adeno, vrs, infA, infB, sars2])

    
    def export_mail(self):
        """Enviem fitxer"""
        
        file = u.tempFolder + "tests_virus_AP.csv"
        u.writeCSV(file, self.upload, sep=';')
        text= "Us fem arribar el fitxer amb els tests rapids per virus realitzats a AP"
        u.sendPolite(['tsorianoarandes@gmail.com', 'clara.prats@upc.edu','aanton@vallhebron.cat','pere.soler@vallhebron.cat', 'aida.perramon@upc.edu'],'Dades tests',text,file)
        
        
if __name__ == '__main__':
    u.printTime("Inici")
      
    tests_virus()
    
    u.printTime("Final")