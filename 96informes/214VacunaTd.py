# coding: utf8

"""
Vacuna Td en majors de 65 (nascuts al 1953 i 1952)
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

"""Demanen i afegim pneumo23 i pneumo13 valent"""

import sisapUtils as u

class vacunesTd(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_vacunes()
        self.get_pob()        

    def get_centres(self):
        """centres ICS"""
        self.centres = {}
        sql = "select scs_codi, ics_codi from cat_centres where ep='0208'"
        for up, br in u.getAll(sql, 'nodrizas'):
            self.centres[up] = br
    
    def get_vacunes(self):
        """aconseguim vacunes tètanus"""
        self.vacunes = {}
        sql = "select id_cip_sec, agrupador, year(datamax) from eqa_vacunes where agrupador in (49, 48, 698)"
        for id, agr, dat in u.getAll(sql, 'nodrizas'):
            self.vacunes[(id, agr)] = dat
        
    def get_pob(self):
        """agafem població segons criteris edat"""
        self.resultats = Counter()
        sql = "select id_cip_sec, year(data_naix), up, data_naix from assignada_tot where ates=1 and year(data_naix) in ('1952','1953')"
        for id, any_naix, up, dnaix in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                self.resultats[(any_naix,'den')] += 1
                if (id, 49) in self.vacunes:
                    dat_vac = self.vacunes[(id, 49)]
                    if dat_vac > 2013:
                        self.resultats[(any_naix,'tetanus')] += 1
                if (id, 48) in self.vacunes:
                    dat_vac = self.vacunes[(id, 48)]
                    if dat_vac > 2013:
                        self.resultats[(any_naix,'PNC23')] += 1
                if (id, 698) in self.vacunes:
                    dat_vac = self.vacunes[(id, 698)]
                    if dat_vac > 2013:
                        self.resultats[(any_naix,'PNC13')] += 1
                            
        for (any_naix, tip), n in self.resultats.items():
            print any_naix, tip, n
            
   
if __name__ == '__main__':
    vacunesTd()