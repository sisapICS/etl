
# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

nod = 'nodrizas'
imp = 'import'

dext, = getOne('select data_ext from dextraccio', nod)


def grup_edat(edat):

    if edat < 15:
        grupedat = 'Menors de 14 anys'
    elif 15<= edat <45:
        grupedat = 'Entre 15 i 44'
    elif 45<= edat <65:
        grupedat = 'Entre 45 i 64'
    elif 65<= edat <75:
        grupedat = 'Entre 65 i 74'
    elif edat>74:
        grupedat = 'Majors de 74 anys'
    else:
        grupedat = 'error'
    
    return grupedat
    
centres = {}
sql = "select scs_codi from cat_centres where ep='0208'"
for up, in getAll(sql, nod):
    centres[up] = True

problems = Counter()
sql = "select  id_cip_sec, pr_cod_ps from problemes where \
         pr_cod_o_ps='C' and pr_hist=1 and pr_dde<='{0}' and (pr_data_baixa is null or pr_data_baixa>'{0}') and (pr_dba is null or pr_dba>'{0}')".format(dext)
for id, pr in getAll(sql, imp):
    problems[id] += 1
    
resultats = Counter()
sql = 'select id_cip_sec, up, edat from assignada_tot where ates=1'
for id, up, edat in getAll(sql, nod):
    if up in centres:
        grupE = grup_edat(edat)
        resultats[grupE, 'pob'] += 1
        prs = 0
        if id in problems:
            prs = problems[id]
        resultats[grupE, 'ps'] += prs
for (grup, tip), res in resultats.items():
    print grup,tip,res

        