# -*- coding: utf8 -*-

"""
Refredats
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


def get_nivell(edat):
    """."""
    nivell = 'INF1C' if edat < 3 else  'INF2C' if edat <6 else 'PRI' if edat < 12 else 'ESO' if edat <16 else 'BAX' if edat <18 else 'ADULTS'
    return (nivell)

tb = 'sisap_covid_pneumo'
db = 'permanent'

CODIS = {("C01-J18.9", 12176): 5, ("C01-J12.89", None): 3,
         ("C01-J12.89", 16154): 3, ("C01-J18.9", 15373): 5,
         ("C01-J18.9", None): 5, ("C01-J18.9", 15565): 5,
         ("C01-J12.9", 12183): 2, ("C01-J12.9", 12182): 2,
         ("C01-J15.9", 12181): 4, ("C01-J12.9", 15381): 2,
         ("C01-J12.9", 15382): 2, ("C01-J18.9", 15374): 5,
         ("C01-J15.9", 15375): 4, ("C01-J12.9", None): 2,
         ("C01-J12.81", None): 3, ("C01-J18.0", 10170): 5,
         ("C01-J18.1", 12184): 5, ("C01-J12.89", 16155): 3,
         ("C01-J15.9", None): 4, ("C01-J18.1", 15377): 5,
         ("C01-J18.0", None): 5, ("C01-J10.00", 12178): 1,
         ("C01-J18.1", None): 5, ("C01-J16.8", None): 5,
         ("C01-J13", None): 4, ("C01-J10.00", 15378): 1,
         ("C01-J18.8", None): 5, ("C01-J15.8", None): 4,
         ("C01-J15.1", None): 4, ("C01-J10.00", 15379): 1,
         ("C01-J10.00", None): 1, ("C01-J10.00", 12177): 1,
         ("C01-J12.2", None): 1, ("C01-J15.7", None): 4,
         ("C01-J12.1", None): 1, ("C01-J15.6", None): 4,
         ("C01-J10.08", None): 1, ("C01-J11.00", None): 1,
         ("C01-J12.0", None): 1, ("C01-J12.3", None): 1,
         ("C01-J14", None): 4, ("C01-J15.211", None): 4,
         ("C01-J15.29", None): 4, ("C01-J15.4", None): 4,
         ("C01-J18.2", None): 5, ("C01-J16.8", 10474): 5}

class Pneumonies(object):
    """."""

    def __init__(self):
        """."""
        self.pneumo = c.Counter()
        for self.sector in u.sectors:
            print self.sector
            self.get_assignada()
            self.get_refredats()
        self.export_data()
    
    def get_assignada(self):
        """."""
        self.pob = {}
        sql = "select usua_cip, usua_sexe, to_date(usua_data_naixement, 'J') from usutb040"
        for id, sexe, naix in u.getAll(sql, self.sector):
            self.pob[id] = {'sexe': sexe, 'naix': naix}
    
    def get_refredats(self):
        """."""
        u.printTime("problemes")
        
        codis = tuple(set([row[0] for row in CODIS]))
        sql = "select cip_usuari_cip, to_date(pr_dde), pr_cod_ps, pr_th \
           from prstb015, usutb011 \
            where pr_cod_u=cip_cip_anterior and pr_cod_o_ps = 'C' and \
                 pr_cod_ps in {} and \
                 to_char(pr_dde, 'YYYYMM') >'200908' and \
                 pr_data_baixa is null \
                 group by cip_usuari_cip, pr_dde, pr_cod_ps, pr_th".format(codis)
        for id, dde, ps, th in u.getAll(sql, self.sector):
            sexe = self.pob[id]['sexe'] if id in self.pob else None
            naix = self.pob[id]['naix'] if id in self.pob else None
            try:
                ed = u.yearsBetween(naix, dde)
            except:
                ed = None
            nivell = 'ADULTS'    
            grup = None
            if ed != None:
                if int(ed) < 15:
                    grup = 0
                elif 15 <= int(ed) < 65:
                    grup = 1
                elif 64 < int(ed) < 75:
                    grup = 2
                elif int(ed)>74:
                    grup = 3
                else:
                    grup = 9
                nivell = get_nivell(ed) 
            if (ps, th) in CODIS:
                tip = CODIS[(ps, th)]
                self.pneumo[(dde, grup, sexe, tip, nivell)] += 1

    def export_data(self):
        """."""                
        cols = ("data date", "grup_edat int", "sexe varchar(10)", "grip int", "nivell varchar(10)","recompte int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)

        upload = []
        for (dde, grup, sexe, tip, nivell ), n in self.pneumo.items():
            n = int (n)
            upload.append([dde, grup, sexe, tip, nivell, n])
        u.listToTable(upload, tb, db)

                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Pneumonies()
    
    u.printTime("Final")                 
