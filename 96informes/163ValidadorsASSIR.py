# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

nod = 'nodrizas'
imp = 'import'

def get_ids(up, dni):
    """  Aconsegueix els id dels pacients de cada validador
    """
    sql = "select id_cip_sec from ass_imputacio_dni where visi_up = '{}' and visi_dni_prov_resp='{}'".format(up, dni)
    return {id for id, in getAll(sql,nod)}
 
def get_cips():
    """Aconsegueix el hash sector
    """
    hash_to_cip = {hash: cip for (hash, cip) in getAll('select usua_cip_cod, usua_cip from pdptb101', 'pdp')}
    hashos = {}
    sql = 'select id_cip_sec, codi_sector, hash_d from u11'
    for id, sector, hash in getAll(sql, imp):
        if hash in hash_to_cip:
            hashos[id] = {'sector': sector, 'hash': hash_to_cip[hash]}
    return hashos


validadors = {'ENCARNA LOPEZ': {'UP':'01695', 'NIF': '52679686A'},
              'ASTRID FRANCESCH': {'UP': '01421', 'NIF':'46354016T'},
              'ASTRID FRANCESCH 2': {'UP': '04678', 'NIF':'46354016T'},
              'ASTRID FRANCESCH 3': {'UP': '01422', 'NIF':'46354016T'},
              'JUDIT PELEGRI': {'UP': '01417', 'NIF':'46986395V'},
              'ANGELS YUS': {'UP': '00411', 'NIF':'37359571G'},
              'M. CARMEN ECHEVARRIA': {'UP': '00254', 'NIF':'73080344Y'},
              'NURIA BOADAS': {'UP': '01094', 'NIF':'40292770'},
              'YOLANDA FLORENSA': {'UP': '00012', 'NIF':'43740336'},
              'ROSER VIDAL': {'UP': '00076', 'NIF':'02190572'},
              'ANNA CASTILLO': {'UP': '00412', 'NIF':'43450016A'},
              'IRENE AGUILAR': {'UP': '00076', 'NIF':'37360452'},
            }
            
            
hashos = get_cips()            
ids = {}         
for validador in validadors:
    idcips = get_ids(validadors[validador]['UP'], validadors[validador]['NIF'])
    for id in idcips:
        ids[id] = validador


validaEQA = [] 
sql = 'select id_cip_sec, num, den from ass_anticoncep'
for id, num, den in getAll(sql, 'ass'):
    if id in ids:
        if id in hashos:
            if num == 1:
                validaEQA.append(['EQA9222', 'COMPLEIX', ids[id], validadors[ids[id]]['UP'], hashos[id]['sector'], hashos[id]['hash']])
            else:
                validaEQA.append(['EQA9222', 'NO COMPLEIX', ids[id], validadors[ids[id]]['UP'], hashos[id]['sector'], hashos[id]['hash']])
                        

sql = 'select id_cip_sec, EQA9221 from ass_embaras'
for id,  num in getAll(sql, 'ass'):
    if id in ids:
        if id in hashos:
            if num == 1:
                validaEQA.append(['EQA9221', 'COMPLEIX', ids[id], validadors[ids[id]]['UP'], hashos[id]['sector'], hashos[id]['hash']])
            else:
                validaEQA.append(['EQA9221', 'NO COMPLEIX', ids[id], validadors[ids[id]]['UP'], hashos[id]['sector'], hashos[id]['hash']])

  
file = tempFolder + 'PACIENTS per validar.txt'
writeCSV(file, validaEQA, sep=';')  







  
  