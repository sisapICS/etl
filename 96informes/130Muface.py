# coding: utf8


from sisapUtils import *
from collections import defaultdict,Counter

grups_garanties = ['0009','0010','0011','0012','0013','0014']

centres = {}
sql = 'select	a.amb_codi_amb	,a.amb_desc_amb	,s.dap_codi_dap	,s.dap_desc_dap	,c.up_codi_up_scs	,u.up_codi_up_ics,u.up_desc_up_ics \
        from	cat_gcctb006 s,cat_gcctb008 c,cat_gcctb007 u,cat_gcctb005 a\
        where u.dap_codi_dap = s.dap_codi_dap\
        and c.up_codi_up_ics = u.up_codi_up_ics\
		and s.amb_codi_amb=a.amb_codi_amb\
        and c.up_data_baixa = 0 \
        and dap_data_baixa = 0 \
        and u.up_data_baixa = 0 \
        and amb_data_baixa = 0'
        
print 'centres fets'

for amb, ambdesc,dap,dapdesc,up,br,desc in getAll(sql, 'import'):
    centres[up] = {'ambit':amb, 'ambdesc':ambdesc,'dap':dap,'dapdesc':dapdesc,'br':br,'desc':desc}
    
moduls = {}
for sec, cen, cla, ser, cod, desc in getAll("select codi_sector, modu_centre_codi_centre, modu_centre_classe_centre, modu_servei_codi_servei, modu_codi_modul, modu_descripcio from cat_vistb027", 'import'):
    moduls[(sec, cen, cla, ser, cod)] = desc
    
grup_g = {}
sql = "select DISTINCT RV_LOW_VALUE,RV_MEANING from cat_pritb000 where rv_column='CART_GRUP_GARANTIES'"
for codi, descripcio in getAll(sql, 'import'):
    grup_g[codi] = descripcio

u11 = {}    
sql = 'select id_cip_sec,codi_sector, hash_d from u11'
for id, sector, hash in getAll(sql, 'import'):
    u11[id] = {'sector':sector, 'hash': hash}

    
muface = {}
sql = "select usua_cip,codi_sector,cart_ent_cotitzacio from usutb040 where  CART_ENT_COTITZACIO in ('03','04','05')"
for cip, sector, cart_ent in getAll(sql, 'redics'):
    muface[(sector,cip)] = {'cart':cart_ent, 'farmacia': 0, 'grup_g': None, 'desc_gg':None}
    
serveis = {}
sql = 'select codi_sector, s_codi_servei, s_descripcio from cat_pritb103'
for sec, servei, desc in getAll(sql, 'import'):
    serveis[(sec, servei)] = desc


    
for sector in sectors:
    print sector
    sector2 = sector + 'a'
    etiquetes = {}
    for  cen, cla, cod, desc in getAll("select eti_centre,eti_classe,eti_codi,eti_descripcio from vistb079", sector):
        etiquetes[(sector,  cen, cla, cod)] = desc
    cips = {}
    sql = 'select USUA_CIP,USUA_CIP_COD from PDPTB101'
    for cip, hash in getAll(sql, sector2):
        cips[cip] = hash
    sql = "select usua_cip,USUA_IND_FARMACIA from usutb040 where  USUA_IND_FARMACIA = 'TSI 006'"
    for cip, farmacia in getAll(sql, sector):
        try:
            hash = cips[cip]
        except KeyError:
            continue
        if (sector, hash) in muface:
            muface[(sector, hash)]['farmacia'] = 1
        else:
             muface[(sector, hash)] = {'cart': None, 'farmacia': 1,'grup_g': None, 'desc_gg':None}
 
print 'Cart_grup'
sql = 'select id_cip_sec, cart_grup_garanties from  assignada'
for id, gg in getAll(sql, 'import'):
    try:
        sector=u11[id]['sector']
        cip=u11[id]['hash']
    except KeyError:
        continue
    try:
        desc_gg = grup_g[gg]
    except KeyError:
        desc_gg = ''
    if (sector, cip) in muface:
        muface[(sector,cip)]['grup_g'] = gg
        muface[(sector,cip)]['desc_gg'] = desc_gg
    else:
        if gg in grups_garanties:
            muface[(sector,cip)] = {'cart':None, 'farmacia': 0, 'grup_g': gg, 'desc_gg':desc_gg} 
 
recomptes = Counter()   
pacients = {}
sql = "select partition_name from all_tab_partitions where table_name='VISTB043R'"
for taula, in getAll(sql, 'redics'):
    dat, = getOne("select to_char(visi_data_visita, 'YYYY') from VISTB043 PARTITION ({}) s where rownum<2".format(taula), 'redics')
    if dat in ('2015', '2016', '2017'):
        print taula
        sql = "select visi_usuari_cip, codi_sector, visi_centre_codi_centre, visi_centre_classe_centre,visi_modul_codi_modul,visi_up,  visi_tipus_visita , to_char(visi_data_visita,'YYYY'), visi_servei_codi_servei, visi_etiqueta\
                    from VISTB043 PARTITION ({}) s where visi_situacio_visita='R'".format(taula)
        for cip, sector, cen,cla,cod, up, tipus, anyv, servei, etiqueta in getAll(sql, 'redics'):
            if anyv in ('2015','2016','2017'):
                if (sector, cip) in muface:
                    ent, far, gg, desc_gg = muface[(sector, cip)]['cart'],muface[(sector, cip)]['farmacia'],muface[(sector, cip)]['grup_g'],muface[(sector, cip)]['desc_gg']
                    try:
                        ambc, amb,sap,desc, br = centres[up]['ambit'],centres[up]['ambdesc'],centres[up]['dapdesc'],centres[up]['desc'], centres[up]['br']
                    except KeyError:
                        ambc, amb,sap,desc, br = '', '','','',up   
                    try:
                        descservei = serveis[(sector, servei)]
                    except KeyError:
                        descservei = ''
                    if (sector, cen, cla, servei, cod) in moduls:
                        descmodul = moduls[(sector, cen, cla, servei, cod)]
                    else:
                        descmodul=''
                    if tipus in ('9C', '9D', '9T', '9R'):
                        tipus = tipus
                    else:
                        tipus = 'sequencials'
                    try:
                        descetiqueta = etiquetes[(sector,cen, cla, etiqueta)]
                    except KeyError:
                        descetiqueta = ''
                    recomptes[(ent, far, gg, desc_gg, anyv, ambc,amb,sap,desc,up, br, tipus,etiqueta,descetiqueta,servei,descservei,sector,cen,cla,cod,descmodul)] += 1
                    if gg in ('0009','0011','0013'):
                        pacients[(up, gg, cip, anyv)] = True
'''                        
upload = []
for (ent, far,  gg, desc_gg, anyv,ambc, amb,sap,desc,up, br, tipus,etiqueta,descetiqueta,servei,descservei,sector,cen,cla,modul,descmodul), count in recomptes.items():
    upload.append([ent, far,  gg, desc_gg, anyv,ambc, amb,sap,up, br, desc, tipus,etiqueta,descetiqueta,servei,descservei,sector,cen,cla,modul,descmodul,count])
    
table = 'SISAP_VISITES_MUFACE'
try:
    execute('drop table {}'.format(table),'redics')
except:
    pass
    
print 'pugem dades'
sql = "create table {0}(CART_ENT_COTITZACIO varchar2(3),USUA_IND_FARMACIA int, cart_grup_garanties varchar2(10), desc_grup_garanties varchar2(150), anys varchar2(4), ambit_cod varchar2(10), ambit varchar2(150),sap varchar2(150),\
 scs_codi varchar2(5), ics_codi varchar2(5), desc_UP varchar2(150), tipus varchar2(20),visi_etiqueta varchar2(150),visi_etiqueta_desc varchar2(150),\
    servei varchar2(150), servei_desc varchar2(150), codi_sector varchar2(4), centre varchar2(40),classe varchar2(40), modul varchar2(100), modul_desc varchar2(150), recomptes int)".format(table)
execute(sql,'redics')
uploadOra(upload,table,'redics')

users= ['PREDUECR','PREDUMMP','PREDUPRP']
for user in users:
    execute("grant select on {} to {}".format(table,user),'redics')
'''
recomptes = Counter()
for (up, gg, cip, anyv), r in pacients.items():
    recomptes[(anyv, up, gg)] += 1
    
upload = []
for (anyv, up, gg), n in recomptes.items():
    upload.append([anyv, up, gg, n])
    
file = tempFolder + 'Pacients_Muface.txt'
writeCSV(file, upload, sep=';')
