import sisapUtils as u
import os
import pandas as pd

"""
Carga agrupador CCS a tabla de redics
https://catsalut.gencat.cat/web/.content/minisite/catsalut/proveidors_professionals/registres_catalegs/catalegs/cim-10-mc-scp/documents/fitxers-2020/CIM10MC-2020-2021-codis-malalties.zip
"""

# Parametros
TB_DESTI = 'SISAP_CAT_CCS'
DB_DESTI = 'redics'
WATCHERS = ("PREDULMB", "PREDUPRP", "PREDUMMP")
ORIGINAL = os.path.join(
    u.tempFolder,
    "CCS",
    "CIM10MC-2020-2021-codis-malalties",
    "CIM10MC_2020_2021_20201223.txt")
FWIDTHS = [10, 5, 1, 15, 255, 120, 8, 1, 4, 255, 5, 255, 2, 255]
WORKING_FILE = os.path.join(u.tempFolder, "ccs.txt")


def convert_cim10(cim):
    if len(cim) == 3:
        pass
    else:
        cim = "{}.{}".format(cim[:3], cim[3:])
    cim10 = "C01-{}".format(cim)
    return cim10


# captura ORIGINAL; produce y captura WORKING_FILE
names = ["{}{}".format(col, n) for col, n in zip(['col']*14, range(14))]
df = pd.read_fwf(ORIGINAL,
                 widths=FWIDTHS,
                 names=names,
                 converters={
                     'col3': convert_cim10,
                     'col7': str,
                     'col12': str}
                 )
df.to_csv(WORKING_FILE, index=False, header=False, sep="@")
upload = [row for row in u.readCSV(WORKING_FILE)]

# carga en TB_DESTI
cols = ["{} varchar2({})".format(col, n) for (col, n) in zip(names, FWIDTHS)]
u.createTable(TB_DESTI, "({})".format(", ".join(cols)), DB_DESTI, rm=True)
u.listToTable(upload, TB_DESTI, DB_DESTI)
u.grantSelect(TB_DESTI, WATCHERS, DB_DESTI)
