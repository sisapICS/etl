# coding: iso-8859-1

import collections as c
import sisapUtils as u
import hashlib as h
from datetime import datetime
import csv,os
#import pandas as pd

'''

'''


USERS = ("PREDUFFA", "PREDUMMP", "PREDUPRP", "PDP", "PREDUECR", "PREDULMB",
         "PREDUEHE")
         
USERSECAP = ("PDP","PREDUEHE")



class covid19GestioContactes(object):
    def __init__(self):
        '''.'''

        print('Inici: ', datetime.now())

        ts = datetime.now()
        self.get_gcontactes()
        print('Time execution G. CONTACTES: time {} / nrow {}'.format(datetime.now() - ts, len(self.gc)))

        ts = datetime.now()
        self.get_cip()
        print('Time execution CIP: time {} / nrow {}'.format(datetime.now() - ts, len(self.ciphash)))

        ts = datetime.now()
        self.get_hash()
        print('Time execution HASH: time {} / nrow {}'.format(datetime.now() - ts, len(self.hashidcip)))

        ts = datetime.now()
        self.get_poblacio()
        print('Time execution POBLACIO: time {} / nrow {}'.format(datetime.now() - ts, len(self.poblacio)))

        ts = datetime.now()
        self.get_casos()
        print('Time execution CASOS: time {} / nrow {}'.format(datetime.now() - ts, len(self.casos)))

        ts = datetime.now()
        self.get_rca()
        print('Time execution RCA: time {} / nrow {}'.format(datetime.now() - ts, len(self.rca)))

        ts = datetime.now()
        self.get_vis()
        print('Time execution VISITES: time {} / nrow {}'.format(datetime.now() - ts, len(self.vis)))

        ts = datetime.now()
        self.get_visp()
        print('Time execution VISITES P: time {} / nrow {}'.format(datetime.now() - ts, len(self.visp)))

        ts = datetime.now()
        self.upload()
        print('Time execution UPLOAD PAC: time {} / nrow {}'.format(datetime.now() - ts, len(self.uploadpac)))
        print('Time execution UPLOAD ECAP: time {} / nrow {}'.format(datetime.now() - ts, len(self.uploadecap)))

        self.get_tecapgescon()





    def get_gcontactes(self):
        """FLT: NO"""
        print("--------------------------------------------- get_gcontactes")

        self.gcset = c.defaultdict(set)
        self.gc = {}

        sql = "select cip, \
                      to_date(dataderivacio, 'yyyy-mm-dd') \
                      from dwaquas.sem_contacte_simptomatic"
        for cip14, der_data in u.getAll(sql, 'exadata'):
            etiqueta = "contacte simptomatic"
            cip13 = cip14[:13]
            hashcovid = h.sha1(cip13).hexdigest().upper()
            self.gcset[(cip13,etiqueta)].add((cip14, hashcovid, der_data))

        sql = "select cip, \
                      to_date(dataderivacio, 'yyyy-mm-dd') \
                      from dwaquas.sem_aillament_social"
        for cip14, der_data in u.getAll(sql, 'exadata'):
            etiqueta = "aillament social"
            cip13 = cip14[:13]
            hashcovid = h.sha1(cip13).hexdigest().upper()
            self.gcset[(cip13,etiqueta)].add((cip14, hashcovid, der_data))

        sql = "select cip, \
                      to_date(dataderivacio, 'yyyy-mm-dd') \
                      from dwaquas.sem_contacte_vulnerable"
        for cip14, der_data in u.getAll(sql, 'exadata'):
            etiqueta = "contacte vulnerable"
            cip13 = cip14[:13]
            hashcovid = h.sha1(cip13).hexdigest().upper()
            self.gcset[(cip13,etiqueta)].add((cip14, hashcovid, der_data))

        sql = "select cip, \
                      to_date(dataderivacio, 'yyyy-mm-dd') \
                      from dwaquas.sem_contacte_pcr_extern"
        for cip14, der_data in u.getAll(sql, 'exadata'):
            etiqueta = "contacte pcr extern"
            cip13 = cip14[:13]
            hashcovid = h.sha1(cip13).hexdigest().upper()
            self.gcset[(cip13,etiqueta)].add((cip14, hashcovid, der_data))

        for cip13, etiqueta in self.gcset:
            cip14, hashcovid, der_data = max(self.gcset[(cip13,etiqueta)])
            self.gc[(cip13,etiqueta)]={'cip14': cip14, 'hashcovid': hashcovid,'der_data': der_data}
        #print(self.gc)

    def get_cip(self):
        """
        Executa 16 workers en paral·lel
        segons el primer dígit del HASH.
        """
        print("--------------------------------------------- get_cip")
        jobs = [format(digit, 'x').upper() for digit in range(16)]
        resultat = u.multiprocess(self.get_cip_worker, jobs)
        self.ciphash = {}
        for worker in resultat:
            for cip, hash in worker:
                self.ciphash[(cip)] = hash
        print("N registros (CIP) = HASH:", len(self.ciphash))

    def get_cip_worker(self, subgroup):
        """Worker de get_cip"""
        sql = "select usua_cip, usua_cip_cod from pdptb101 \
               where usua_cip_cod like '{}%'".format(subgroup)
        db = 'pdp'
        converter = []
        for cip, hash in u.getAll(sql, db):
            converter.append((cip, hash))
        return converter

    def get_hash(self):
        '''.'''
        print("--------------------------------------------- get_u11")
        self.hashidcip = {}
        db = 'import'
        tb = 'u11'
        sql = 'select id_cip, hash_d, codi_sector from {}.{}'.format(db, tb)
        for id_cip, hash_d, sector in u.getAll(sql, db):
            self.hashidcip[hash_d] = {'id_cip': id_cip, 'sector': sector}
        print("Ejemplo HASH:", next(iter( self.hashidcip.items() )) )
        print("N de registros HASH to IDCIP: {}", len(self.hashidcip))

    def get_poblacio(self):
        """."""
        print("--------------------------------------------- get_poblacio")
        self.poblacio={}
        sql = "select id_cip, id_cip_sec, up, uba, ubainf, edat \
               from assignada_tot"
        for id_cip, id_cip_sec, up, uba, ubainf, edat in u.getAll(sql, "nodrizas"):
            self.poblacio[id_cip] = {'id_cip_sec': id_cip_sec, 'up': up, 'uba': uba, 'ubainf': ubainf, 'edat': edat}
        print('Ejemplo self.poblacio: ', next(iter( self.poblacio.items() )) )

    def get_rca(self):
        """ RCA N=11.000.000 ¡¡ojo!! tarda 10m -> multiprocesso?? """
        print("--------------------------------------------- get_rca")
        self.rca = {}
        sql = "select hash, decode(eap, 'SES', NULL, eap) from preduffa.sisap_covid_pac_rca"
        for hash, up in u.getAll(sql,'redics'):
            self.rca[hash] = {'up': up}
        print("RCA:", len(self.rca))

    def get_casos(self):
        """FLT: NO"""
        print("--------------------------------------------- get_casos")
        self.casos = {}
        sql = "select hash, up, uba, inf, edat, \
                      estat, cas_data, dx_sit, \
                      pcr_res, pcr_data, fr_numero, vis_data \
               from preduffa.sisap_covid_pac_master"
        for hash, up, uba, inf, edat, estat, cas_data, dx_sit, pcr_res, pcr_data, fr_numero, vis_data in u.getAll(sql, 'redics'):
            self.casos[hash] = {'up': up, 'uba': uba, 'inf': inf, 'edat': edat,
                                'estat': estat, 'cas_data': cas_data, 'dx_sit': dx_sit,
                                'pcr_res': pcr_res, 'pcr_data': pcr_data, 'fr_numero': fr_numero, 'vis_data': vis_data}

    def get_vis(self):
        '''.'''
        print("--------------------------------------------- get_vis")
        self.visdat= {}
        self.vis = c.defaultdict(set)
        db = 'redics'
        tb = 'sisap_covid_pac_visites'
        sql = 'select pacient, data from preduffa.{}'.format(tb)
        for hash, dat in u.getAll(sql, db):
            self.vis[hash].add(dat)
        for hash in self.vis:
            maxdat = max(self.vis[hash])
            self.visdat[hash]= {'dat': maxdat}

    def get_visp(self):
        ''' 11/06/2020 N=425.000 '''
        print("--------------------------------------------- get_visp")
        self.vispdat= {}
        self.visp = c.defaultdict(set)
        db = 'redics'
        tb = 'sisap_covid_pac_visites_p'
        sql = 'select hash, data from preduffa.{}'.format(tb)
        for hash, dat in u.getAll(sql, db):
            self.visp[hash].add(dat)
        for hash in self.visp:
            maxdat = max(self.visp[hash])
            self.vispdat[hash]= {'dat': maxdat}


    def upload(self):
        """ . """
        print("--------------------------------------------- upload")
        self.uploadpac = []
        self.uploadecap = []
        for cip13, etiqueta in self.gc:
            hashcovid = self.gc[(cip13,etiqueta)]['hashcovid']
            up = self.rca[hashcovid]['up'] if hashcovid in self.rca else None
            der_data = self.gc[(cip13,etiqueta)]['der_data']
            cip14 = self.gc[(cip13,etiqueta)]['cip14']
            estat = self.casos[hashcovid]['estat'] if hashcovid in self.casos else None
            cas_data = self.casos[hashcovid]['cas_data'] if hashcovid in self.casos else None
            dx_sit = self.casos[hashcovid]['dx_sit'] if hashcovid in self.casos else None
            pcr_res = self.casos[hashcovid]['pcr_res'] if hashcovid in self.casos else None
            pcr_data = self.casos[hashcovid]['pcr_data'] if hashcovid in self.casos else None
            fr_numero = self.casos[hashcovid]['fr_numero'] if hashcovid in self.casos else None
            vis_data = self.visdat[hashcovid]['dat'] if hashcovid in self.visdat else None
            visp_data = self.vispdat[hashcovid]['dat'] if hashcovid in self.vispdat else None
            hash = self.ciphash[cip13] if cip13 in self.ciphash else None
            id_cip = self.hashidcip[hash]['id_cip'] if hash in self.hashidcip else None
            sector = self.hashidcip[hash]['sector'] if hash in self.hashidcip else None
            uba = self.poblacio[id_cip]['uba'] if id_cip in self.poblacio else None
            ubainf = self.poblacio[id_cip]['ubainf'] if id_cip in self.poblacio else None
            edat = self.poblacio[id_cip]['edat'] if id_cip in self.poblacio else None
            self.uploadpac.append((hashcovid, up, uba, ubainf,
                                   etiqueta, der_data,
                                   edat, estat, cas_data, dx_sit, pcr_res, pcr_data, fr_numero, vis_data, visp_data))
            self.uploadecap.append((sector, cip14, cip13, up, uba, ubainf,
                                    etiqueta, der_data,
                                    edat, estat, cas_data, dx_sit, pcr_res, pcr_data, fr_numero, vis_data, visp_data))
        print(self.uploadpac[0])

        tb = "SISAP_COVID_PAC_GESCON"
        db = "redics"
        columns = ["hash varchar2(40)",
                   "up varchar2(5)",
                   "uba varchar2(5)",
                   "inf varchar2(5)",
                   "etiqueta varchar2(50)",
                   "der_data date",
                   "edat number",
                   "estat varchar2(16)",
                   "cas_data date",
                   "dx_sit varchar2(10)",
                   "pcr_res varchar2(20)",
                   "pcr_data date",
                   "fr_numero number",
                   "vis_data date",
                   "visp_data date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.uploadpac, tb, db)
        u.grantSelect(tb, USERS, db)

        tb = "SISAP_COVID_ECAP_GESCON"
        db = "redics"
        columns = ["sector varchar2(6)",
                   "cip14 varchar2(14)",
                   "cip varchar2(13)",
                   "up varchar2(5)",
                   "uba varchar2(5)",
                   "inf varchar2(5)",
                   "etiqueta varchar2(50)",
                   "der_data date",
                   "edat number",
                   "estat varchar2(16)",
                   "cas_data date",
                   "dx_sit varchar2(10)",
                   "pcr_res varchar2(20)",
                   "pcr_data date",
                   "fr_numero number",
                   "vis_data date",
                   "visp_data date"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.uploadecap, tb, db)
        u.grantSelect(tb, USERSECAP, db)

    def get_tecapgescon(self):
        """FLT: NO"""
        print("--------------------------------------------- get_tecapgescon")

        self.tecapgescon = []
        sql = "select * from preduffa.sisap_covid_ecap_gescon where (vis_data <= der_data or vis_data is null) and (visp_data <= der_data or visp_data is null)"
        for row in u.getAll(sql, 'redics'):
            self.tecapgescon.append((row))
        #print(self.tecapgescon)
        u.writeCSV(u.tempFolder + 'gestioContactes.csv', self.tecapgescon)



if __name__ == "__main__":
    clock_in = datetime.now()
    covid19GestioContactes()
    clock_out = datetime.now()
    print("---------------------------------------------- TIME EXECUTION")
    print('''START: {}'''.format(clock_in))
    print('''END: {}'''.format(clock_out))
    print('''DELTA: {}'''.format (clock_out - clock_in))

