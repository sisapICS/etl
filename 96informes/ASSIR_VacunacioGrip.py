# coding: iso-8859-1

import collections as c
import sisapUtils as u
#from datetime import dadetime
import datetime as d
import dateutil.relativedelta as r

TODAY = d.datetime.now().date()
YEAR = 2020

TEST_UP='00276' # ABS BADALONA-6 (Llefià)
TEST_ASSIR='01415' # ASSIR Esquerra
TEST_IDCIPSEC = 7811604


class embvacgrip(object):
    def __init__(self):
        '''.'''

        ts = d.datetime.now()
        self.get_assir()
        print('Time execution assir time {} / nrow {}'.format(d.datetime.now() - ts, len(self.assir)))

        ts = d.datetime.now()
        self.get_assir_imputacio()
        print('Time execution assir_imputacio time {} / nrow {}'.format(d.datetime.now() - ts, len(self.assir_imputacio)))

        ts = d.datetime.now()
        self.get_embarassos()
        print('Time execution embarassos: {} / nrow {}'.format(d.datetime.now() - ts, len(self.embarassos)))

        ts = d.datetime.now()
        self.get_assirvis()
        print('Time execution visites: {} / nrow {}'.format(d.datetime.now() - ts, len(self.assirvis)))

        ts = d.datetime.now()
        self.get_vacunes()
        print('Time execution get_vacunes: {} / nrow {}'.format(d.datetime.now() - ts, len(self.vacunes)))

        ts = d.datetime.now()
        self.contar()
        print('Time execution contar: {}'.format(d.datetime.now()))

        ts = d.datetime.now()
        self.get_upload()
        print('Time execution get_upload: {} / nrow {}'.format(d.datetime.now() - ts, len(self.upload)))

        ts = d.datetime.now()
        self.export_data()
        print('Time execution export_data: {}'.format(d.datetime.now() - ts))





    def get_assir(self):
        '''.'''
        print("--------------------------------------------- get_assir")
        self.assir = {}
        db = "nodrizas"
        tb = "ass_centres"
        sql = "select up, up_desc, up_assir, assir from {}.{}".format(db, tb)
        for up, up_desc, up_assir, up_assir_desc in u.getAll(sql, db):
            self.assir[up] = (up_desc, up_assir, up_assir_desc)
        print('Ejemplo self.assir: ', next(iter( self.assir.items() )) )
        print("Validation assir TEST:", self.assir[TEST_ASSIR])


    def get_assir_imputacio(self):
        '''.'''
        print("--------------------------------------------- get_assir_imputacio")
        self.assir_imputacio = {}
        self.assir_imputacioup = {}
        db = "nodrizas"
        tb = "ass_imputacio_up"
        sql = "select id_cip_sec, visi_up from {}.{}".format(db, tb)
        for id_cip_sec, up in u.getAll(sql, db):
            #if up=='00023':
            self.assir_imputacioup[up] =  True
            self.assir_imputacio[id_cip_sec] = up
        print('Ejemplo self.assir_imputacio: ', next(iter( self.assir_imputacio.items() )) )
        #print("Validation assir_imputacio TEST:", self.assir[TEST_IDCIPSEC])


    def get_embarassos(self):
        print("--------------------------------------------- get_embarassos")
        """ Obtenim embarassos del darrer any
        """
        self.embarassosid = {}
        self.embarassos = {}
        sql = "select id_cip_sec, emb_d_ini, emb_d_tanca from embaras"
        for id_cip_sec, data_ini, data_fi in u.getAll(sql, "import"):
            if id_cip_sec in self.assir_imputacio:
                #up = self.assir_imputacio[id_cip_sec]
                #if up=='00023':
                if data_fi is None:
                    datafi = data_ini + r.relativedelta(months=9)
                else:
                    datafi = data_fi
                if d.datetime(2018,1,1).date() <= data_ini <= d.datetime(2018,10,1).date() and (data_fi == None or data_fi >= d.datetime(2018,10,1).date()):
                    self.embarassos[(id_cip_sec,'2018')] = {'datini': data_ini, 'datfi': datafi}
                    self.embarassosid[id_cip_sec]=True
                    #print(id_cip_sec, ': 2018',self.embarassos[(id_cip_sec,'2018')])
                if d.datetime(2019,1,1).date() <= data_ini <= d.datetime(2019,10,1).date() and (data_fi == None or data_fi >= d.datetime(2019,10,1).date()):
                    self.embarassos[(id_cip_sec,'2019')] = {'datini': data_ini, 'datfi': datafi}
                    self.embarassosid[id_cip_sec]=True
                    #print(id_cip_sec, ': 2019',self.embarassos[(id_cip_sec,'2019')])
                if d.datetime(2020,1,1).date() <= data_ini <= d.datetime(2020,10,1).date() and (data_fi == None or data_fi >= d.datetime(2020,10,1).date()):
                    self.embarassos[(id_cip_sec,'2020')] = {'datini': data_ini, 'datfi': datafi}
                    self.embarassosid[id_cip_sec]=True
        #print(self.embarassos)
        #print(self.embarassosid)
        print('Ejemplo self.embarassos: ', next(iter( self.embarassos.items() )) )
        print('Numero de registres embarassos: ', len(self.embarassos))
        print('Numero de pacients embarassos: ', len(self.embarassosid))
        #print("Validation embarassosid TEST:", self.embarassosid[TEST_IDCIPSEC])
        #print("Validation embarassos TEST:", "/ 2019:", self.embarassos[TEST_IDCIPSEC,'2019'])


    def get_assirvis(self):
        print("--------------------------------------------- get_assirvisites")
        """ Obtenim les visites a l'ASSIR de les pacients amb embarassos
        """
        self.assirvisid = {}
        self.assirvis = {}
        sql = "select id_cip_sec, visi_data_visita, visi_up, visi_tipus_visita from ass_visites where visi_tipus_citacio='P'"
        for id_cip_sec, dat, up, tip in u.getAll(sql, "nodrizas"):
            if (id_cip_sec,'2018') in self.embarassos:
                if self.embarassos[(id_cip_sec,'2018')]['datini'] <= dat <= self.embarassos[(id_cip_sec,'2018')]['datfi']:
                    self.assirvis[(id_cip_sec,'2018')] = {'dat': dat, 'up': up, 'tip': tip}
                    self.assirvisid[id_cip_sec]=True
                    #print(id_cip_sec, ': 2018',self.assirvis[(id_cip_sec,'2018')])
            if (id_cip_sec,'2019') in self.embarassos:
                if self.embarassos[(id_cip_sec,'2019')]['datini'] <= dat <= self.embarassos[(id_cip_sec,'2019')]['datfi']:
                    self.assirvis[(id_cip_sec,'2019')] = {'dat': dat, 'up': up, 'tip': tip}
                    self.assirvisid[id_cip_sec]=True
                    #print(id_cip_sec, ': 2019',self.assirvis[(id_cip_sec,'2019')])
            if (id_cip_sec,'2020') in self.embarassos:
                if self.embarassos[(id_cip_sec,'2020')]['datini'] <= dat <= self.embarassos[(id_cip_sec,'2020')]['datfi']:
                    self.assirvis[(id_cip_sec,'2020')] = {'dat': dat, 'up': up, 'tip': tip}
                    self.assirvisid[id_cip_sec]=True
                    #print(id_cip_sec, ': 2019',self.assirvis[(id_cip_sec,'2019')])
        #print(self.assirvis)
        #print(self.assirvisid)
        print('Ejemplo self.assirvis: ', next(iter( self.assirvis.items() )) )
        print('Numero de registres assir visites: ', len(self.assirvis))
        print('Numero de pacients assir visites: ', len(self.assirvisid))


    def get_vacunes(self):
        """."""
        print("--------------------------------------------- get_vacunes")
        self.vacunesid = {}
        self.vacunes = {}
        # sql = "select id_cip_sec, datamax from eqa_vacunes where agrupador=99 and datamax >= '20181001'"
        sql = "select id_cip_sec, va_u_data_vac from vacunes where \
                va_u_cod in ('GRIP-N','GRIP','GRIP-A', \
                'G(A)-4','G(A)-2','G(A)-3','G(A)-1','P-G-AR','P-G-NE','P-G-AD','P-G-60') \
                and va_u_dosi > 0"
        for id_cip_sec, dat in u.getAll(sql, "import"):
            if id_cip_sec in self.assir_imputacio:
                if (id_cip_sec,'2018') in self.embarassos:
                    if (id_cip_sec,'2018') in self.assirvis:
                        if dat <= self.embarassos[(id_cip_sec,'2018')]['datfi']:
                            if d.datetime(2018,10,1).date() <= dat <= d.datetime(2018,12,31).date():
                                self.vacunes[(id_cip_sec,'2018')] = {'dat': dat}
                                self.vacunesid[id_cip_sec] = True
                                #print(id_cip_sec, ': 2018',self.vacunes[(id_cip_sec,'2018')])
                if (id_cip_sec,'2019') in self.embarassos:
                    if (id_cip_sec,'2019') in self.assirvis:
                        if dat <= self.embarassos[(id_cip_sec,'2019')]['datfi']:
                            if d.datetime(2019,10,1).date() <= dat <= d.datetime(2020,1,31).date():
                                self.vacunes[(id_cip_sec,'2019')] = {'dat': dat}
                                self.vacunesid[id_cip_sec] = True
                                #print(id_cip_sec, ': 2019',self.vacunes[(id_cip_sec,'2019')])
                if (id_cip_sec,'2020') in self.embarassos:
                    if (id_cip_sec,'2020') in self.assirvis:
                        if dat <= self.embarassos[(id_cip_sec,'2020')]['datfi']:
                            if d.datetime(2020,10,1).date() <= dat <= d.datetime(2021,1,31).date():
                                self.vacunes[(id_cip_sec,'2020')] = {'dat': dat}
                                self.vacunesid[id_cip_sec] = True
                                #print(id_cip_sec, ': 2020',self.vacunes[(id_cip_sec,'2020')])                                
        #print(self.vacunes)
        #print(self.vacunesid)
        print('Ejemplo self.vacunes: ', next(iter( self.vacunes.items() )) )
        print('Numero de registres vacunes: ', len(self.vacunes))
        print('Numero de pacients vacunes: ', len(self.vacunesid))
        #print("Validation vacunesid TEST:", self.vacunesid[TEST_IDCIPSEC])
        #print("Validation vacunes TEST:", " / 2018:", self.vacunes[TEST_IDCIPSEC,'2018'], "/ 2019:", self.vacunes[TEST_IDCIPSEC,'2019'])


    def contar(self):
        '''.'''
        print("--------------------------------------------- contar")
        self.recompte = c.Counter()
        for id_cip_sec in self.embarassosid:
            up = self.assir_imputacio[id_cip_sec] if id_cip_sec in self.assir_imputacio else None
            up_desc = self.assir[up][0] if up in self.assir else None
            assir = self.assir[up][1] if up in self.assir else None
            assir_desc = self.assir[up][2] if up in self.assir else None
            if (id_cip_sec,'2018') in self.embarassos:
                self.recompte[up, up_desc, assir, assir_desc, '2018', 'DEN'] += 1
            if (id_cip_sec,'2019') in self.embarassos:
                self.recompte[up, up_desc, assir, assir_desc, '2019', 'DEN'] += 1
            if (id_cip_sec,'2020') in self.embarassos:
                self.recompte[up, up_desc, assir, assir_desc, '2020', 'DEN'] += 1                
            if id_cip_sec in self.vacunesid:
                if (id_cip_sec,'2018') in self.vacunes:
                    self.recompte[up, up_desc, assir, assir_desc, '2018', 'NUM'] += 1
                if (id_cip_sec,'2019') in self.vacunes:
                    self.recompte[up, up_desc, assir, assir_desc, '2019', 'NUM'] += 1
                if (id_cip_sec,'2020') in self.vacunes:
                    self.recompte[up, up_desc, assir, assir_desc, '2020', 'NUM'] += 1                    
        #print(self.recompte)


    def get_upload(self):
        '''.'''
        print("----------------------------------------------- get_upload")
        self.uploadassircat = []
        self.uploadassirimputacio = []
        self.upload = []

        for up in self.assir:
            up_desc = self.assir[up][0]
            assir = self.assir[up][1]
            assir_desc = self.assir[up][2]
            self.uploadassircat.append([up, up_desc, assir, assir_desc])
        
        for up in self.assir_imputacioup:
            self.uploadassirimputacio.append([up])
        
        for (up, up_desc, assir, assir_desc, ind, tip), rec in self.recompte.items():
            if ind == '2020':
                self.upload.append([up, up_desc, assir, assir_desc, ind, tip, rec])
        print("Ejemplo upload", self.upload[0])
        print("N de registros upload:", len(self.upload))

    def export_data(self):
        """."""
        u.writeCSV(u.tempFolder + 'ASSIR_VacunacioGrip_{}_{}.csv'.format(YEAR, TODAY.strftime('%Y%m%d')),  # noqa
                   [('up', 'up_desc', 'assir', 'assir_desc',
                    'ind', 'tip', 'n')] + self.upload,
                   sep=";")


if __name__ == "__main__":
    clock_in = d.datetime.now()
    embvacgrip()
    clock_out = d.datetime.now()
    print("---------------------------------------------- TIME EXECUTION")
    print('''START: {}'''.format(clock_in))
    print('''END: {}'''.format(clock_out))
    print('''DELTA: {}'''.format(clock_out - clock_in))
