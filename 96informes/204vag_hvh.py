# coding: latin1

"""
Petició HVH vacuna grip. Hi ha d'haver un txt amb els CIP d'estudi a temp.
"""


import collections as c

import sisapUtils as u
import sisaptools as t


PERIODE = (20170901, 20190331)
FILE_IN = u.tempFolder + "vacunes.txt"
FILE_OUT = u.tempFolder + "sisap_vag_{}.csv"


class VAG(object):
    """."""

    def __init__(self):
        """."""
        self.get_pacients()
        self.get_catalegs()
        self.get_vacunes()
        self.get_resultat()
        self.export_data()
        self.assert_resultat()

    def get_pacients(self):
        """."""
        self.input = 0
        self.pacients = {}
        self.not_found = []
        with t.Database("redics", "pdp") as pdp:
            for cip, in u.readCSV(FILE_IN):
                self.input += 1
                sql = "select usua_cip_cod from pdptb101 where usua_cip = '{}'"
                try:
                    hash, = pdp.get_one(sql.format(cip[:13]))
                except TypeError:
                    self.not_found.append((cip,))
                else:
                    sql = "select id_cip from preduffa.sisap_u11 where hash_a = '{}'"  # noqa
                    id, = pdp.get_one(sql.format(hash))
                    self.pacients[id] = cip

    def get_catalegs(self):
        """."""
        sql = "select vacuna from cat_prstb040_new where antigen = 'A-GRIP'"
        self.cat_antigen = tuple([vac for vac, in u.getAll(sql, "import")])
        sql = "select pf_codi, pf_desc_gen||pf_desc_par from cpftb006"
        self.cat_pf = {cod: des for (cod, des) in u.getAll(sql, "redics")}

    def get_vacunes(self):
        """."""
        self.vacunes = c.defaultdict(set)
        sql = "select id_cip, date_format(va_u_data_vac, '%Y%m%d'), \
                      va_u_pf_cod \
               from vacunes \
               where va_u_cod in {} and \
                     va_u_data_vac between {} and {} and \
                     id_cip in {}".format(self.cat_antigen,
                                          PERIODE[0], PERIODE[1],
                                          tuple(self.pacients.keys()))
        for id, dat, pf in u.getAll(sql, "import"):
            cip = self.pacients[id]
            vac = self.cat_pf.get(pf, "")
            self.vacunes[cip].add((dat, vac))

    def get_resultat(self):
        """."""
        self.vacunats = []
        self.no_vacunats = []
        for cip in self.pacients.values():
            if cip in self.vacunes:
                for dat, vac in self.vacunes[cip]:
                    self.vacunats.append((cip, dat, vac))
            else:
                self.no_vacunats.append((cip,))

    def export_data(self):
        """."""
        objects = (("vacunats", self.vacunats),
                   ("no_vacunats", self.no_vacunats),
                   ("no_trobats", self.not_found))
        for file, object in objects:
            u.writeCSV(FILE_OUT.format(file), sorted(object), sep=";")

    def assert_resultat(self):
        """."""
        no_trobats = len(self.not_found)
        no_vacunats = len(self.no_vacunats)
        vacunats = len(set([id for (id, dat, vac) in self.vacunats]))
        assert self.input == no_trobats + no_vacunats + vacunats


if __name__ == "__main__":
    VAG()
