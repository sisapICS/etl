# coding: latin1

"""
Embarassades amb incontinčncia.
"""

import sisapUtils as u


class Embaras(object):
    """."""

    def __init__(self):
        """."""
        self.get_assignada()
        self.get_embarassos()
        self.get_problemes()
        self.get_tipus_part()
        self.get_tipus_activitat()
        self.taula_final()
        self.export_taula()

    def get_assignada(self):
        """."""
        self.assignada = {}
        sql = "select id_cip_sec, edat from ass_poblacio"
        for id, edat in u.getAll(sql, "nodrizas"):
            self.assignada[id] = edat

    def get_embarassos(self):
        """."""
        self.embarassos = {}
        sql = "select id_cip_sec, fi from ass_embaras where year(fi) = 2018"
        for id, fi in u.getAll(sql, "nodrizas"):
            if id in self.assignada:
                self.embarassos[id] = fi

    def get_problemes(self):
        """."""
        self.problemes = {}
        sql = "select id_cip_sec, ps from eqa_problemes where ps = 454"
        for id, prob in u.getAll(sql, "nodrizas"):
            if id in self.embarassos:
                self.problemes[id] = prob

    def get_tipus_part(self):
        """."""
        self.tipus_part = {}
        sql = "select id_cip_sec, val_val from assir216 \
               where val_var = 'PART003' and year(val_data) = 2018"
        for id, tipus_part in u.getAll(sql, "import"):
            if id in self.problemes:
                self.tipus_part[id] = tipus_part

    def get_tipus_activitat(self):
        """."""
        self.tipus_activitat = {}
        sql = "select id_cip_sec, val_var, val_val, year(val_data) \
               from assir216 where val_var = 'PAED002' or val_var = 'PAED005'"
        for id, tipus_activitat, n_act, data in u.getAll(sql, "import"):
            if id in self.problemes:
                if tipus_activitat == 'PAED002' and n_act > 0 and data >= 2017:
                    self.tipus_activitat[id] = "Grupal de preparació"
                elif tipus_activitat == 'PAED005' and n_act > 0 and data >= 2018:  # noqa
                    self.tipus_activitat[id] = "Grupal pospart"
                else:
                    self.tipus_activitat[id] = "No hi ha activitat grupal"

    def taula_final(self):
        """."""
        self.final_table = {}

        for id in self.tipus_part:
            if self.assignada[id] < 25:
                grup_edat = "<25"
            elif self.assignada[id] < 31:
                grup_edat = "25-30"
            elif self.assignada[id] < 36:
                grup_edat = "31-35"
            elif self.assignada[id] < 41:
                grup_edat = "36-40"
            elif self.assignada[id] < 46:
                grup_edat = "41-45"
            else:
                grup_edat = ">46"

            if id not in self.tipus_activitat:
                tipus_activitat = "No hi ha activitat grupal"
            else:
                tipus_activitat = self.tipus_activitat[id]

            self.final_table[id] = (id, grup_edat, self.tipus_part[id],
                                    tipus_activitat)

        print len(self.problemes)
        print len(self.tipus_part)
        print len(self.final_table)

    def export_taula(self):
        """."""
        upload = self.final_table.values()
        u.writeCSV(u.tempFolder + "incontinencia.csv", upload, sep=";")


if __name__ == "__main__":
    Embaras()
