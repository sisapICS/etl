# coding: utf-8

"""
 - TIME EXECUTION m

 - PETICIÓ:
    - Trello: https://trello.com/c/f1eTPyUT/183-imsersoagain 
    - Drive: No hay Trello

"""

import sisapUtils as u
# import csv, os, sys
import collections as c
from datetime import datetime
import datetime as d
from dateutil.relativedelta import relativedelta
from os import remove


class imsersoMensual(object):
    """."""
    def __init__(self):
        """."""

        self.get_cat_res()
        self.get_exitus()
        self.get_poblacio_residents()
        self.get_casos_residents()
        self.get_proves_residents()
        self.get_exitus_residents()        
        self.get_poblacio_personal()
        self.get_casos_personal()
        self.get_proves_personal()        
        self.get_upload()
        self.export_upload()

    def get_cat_res(self):
        """."""
        # print("------------------------------------------ get_cat_residencies")
        self.cat_res = {}
        sql = """select
                    cod
                    , des
                    , tipus
                    , reses
                 from
                    sisap_covid_cat_residencia
              """
        for cod, des, tip, reses in u.getAll(sql, "redics"):
            self.cat_res[cod] = {'des': des, 'tip': tip, 'reses': reses}
        # print("RESIDENCIES CATALEG: N de registros:", len(self.cat_res))

    def get_exitus(self):
        """ . """
        # print("--------------------------------------------------- get_exitus")
        self.exitus = {}
        sql = """select
                    substr(a.cip, 0, 13) as cip13,
                    afectat, data_exitus, lloc_exitus_categoritzat,
                    perfil, residencia
               from
                    dwaquas.f_covid19_v2 a, dwsisap.residencies_cens b
                where
                    substr(a.cip, 0, 13) = b.cip
                    and a.data_exitus = b.data
                    and exitus='Si'
                    and data_exitus between date '{}'
                                        and date '{}'
                    and perfil ='Resident'
                """.format(DATE0.strftime('%Y-%m-%d'),
                           DATE1.strftime('%Y-%m-%d'))
        for cip13, afectat, data, lloc, perfil, residencia in u.getAll(sql, 'exadata'):  # noqa            
            self.exitus[(cip13, data, afectat, lloc, perfil, residencia)] = True  # noqa
        # print("EXITUS: N de registros:", len(self.exitus))

    def get_res_tipc(self, cod):
        res_tipc = ''
        if cod == 1:
            res_tipc = "R1"
        if cod in (7,8):
            res_tipc = "R2"
        return res_tipc

    def get_poblacio_residents(self):
        """ . """
        # print("--------------------------------------- get_poblacio_residents")
        self.pob_resi = []
        sql = """
            select
                reses,
                cod, des,
                tipus,
                perfil,
                dia_poblacio
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (1,7,8)
                and perfil='Resident'
                and data = date '{}'
            """.format(DATE1.strftime('%Y-%m-%d'))
        for reses, cod, des, tipus, perfil, N in u.getAll(sql, 'redics'):
            ind = 'Poblacio'
            periodo = DATE0.strftime('%Y-%m-%d') + ' - ' + DATE1.strftime('%Y-%m-%d')
            res_tip = self.get_res_tipc(tipus)
            self.pob_resi.append((periodo, ind, reses, cod, des, res_tip, perfil, N))
        # print("POBLACIO RESIDENTS: N de registros:", len(self.pob_resi))
        # print(self.pob_resi)

    def get_casos_residents(self):
        """ . """
        # print("------------------------------------------ get_casos_residents")
        self.casos_resi = []
        sql = """
            select
                reses,
                cod, des,
                tipus,
                perfil,
                setmana_casos as casos
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (1,7,8)
                and perfil='Resident'
                and data = date '{}'
            """.format(DATE1.strftime('%Y-%m-%d'))
        for reses, cod, des, tipus, perfil, N in u.getAll(sql, 'redics'):
            ind = 'Casos'
            periodo = DATE0.strftime('%Y-%m-%d') + ' - ' + DATE1.strftime('%Y-%m-%d')
            res_tip = self.get_res_tipc(tipus)
            self.casos_resi.append((periodo, ind, reses, cod, des, res_tip, perfil, N))
        # print("POBLACIÓ RESIDENTS: N de registros:", len(self.casos_resi))
        # print(self.casos_resi)

    def get_proves_residents(self):
        """ . """
        # print("----------------------------------------- get_proves_residents")
        self.proves_resi = []
        sql = """
            select
                reses,
                cod, des,
                tipus,
                perfil,
                setmana_pcr,
                setmana_tar
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (1,7,8)
                and perfil='Resident'
                and data = date '{}'
            """.format(DATE1.strftime('%Y-%m-%d'))
        for reses, cod, des, tipus, perfil, npcr, ntar in u.getAll(sql, 'redics'):            
            periodo = DATE0.strftime('%Y-%m-%d') + ' - ' + DATE1.strftime('%Y-%m-%d')
            res_tip = self.get_res_tipc(tipus)
            ind = 'Proves (PCR)'
            self.proves_resi.append((periodo, ind, reses, cod, des, res_tip, perfil, npcr))
            ind = 'Proves (TAR)'
            self.proves_resi.append((periodo, ind, reses, cod, des, res_tip, perfil, ntar))
        # print("PROVES RESIDENTS: N de registros:", len(self.proves_resi))
        # print(self.proves_resi)

    def get_poblacio_personal(self):
        """ . """
        # print("---------------------------------------- get_poblacio_personal")
        self.pob_pers = []
        sql = """
            select
                reses,
                cod, des,
                tipus,
                perfil,
                dia_poblacio
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (1,7,8)
                and perfil='Personal'
                and data = date '{}'
            """.format(DATE1.strftime('%Y-%m-%d'))
        for reses, cod, des, tipus, perfil, N in u.getAll(sql, 'redics'):
            ind = 'Poblacio'
            periodo = DATE0.strftime('%Y-%m-%d') + ' - ' + DATE1.strftime('%Y-%m-%d')
            res_tip = self.get_res_tipc(tipus)
            self.pob_pers.append((periodo, ind, reses, cod, des, res_tip, perfil, N))
        # print("POBLACIO PERSONAL: N de registros:", len(self.pob_pers))
        # print(self.pob_pers)

    def get_casos_personal(self):
        """ . """
        # print("---------------------------------------- get_casos_personal")
        self.casos_pers = []
        sql = """
            select
                reses,
                cod, des,
                tipus,
                perfil, 
                setmana_casos as N
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (1,7,8)
                and perfil='Personal'
                and data = date '{}'
            """.format(DATE1.strftime('%Y-%m-%d'))
        for reses, cod, des, tipus, perfil, N in u.getAll(sql, 'redics'):
            ind = 'Casos'  # noqa
            periodo = DATE0.strftime('%Y-%m-%d') + ' - ' + DATE1.strftime('%Y-%m-%d')
            res_tip = self.get_res_tipc(tipus)
            self.casos_pers.append((periodo, ind, reses, cod, des, res_tip, perfil, N))
        # print("CASOS PERSONAL: N de registros:", len(self.casos_pers))
        # print(self.pob_pers)

    def get_proves_personal(self):
        """ . """
        # print("------------------------------------------ get_proves_personal")
        self.proves_pers = []
        sql = """
            select
                reses,
                cod, des,
                tipus,
                perfil,
                setmana_pcr,
                setmana_tar
            from
                preduffa.SISAP_COVID_RES_WEB_RESUM a,
                preduffa.SISAP_COVID_CAT_RESIDENCIA b
            where
                a.RESIDENCIA = b.COD
                and tipus in (1,7,8)
                and perfil='Personal'
                and data = date '{}'
            """.format(DATE1.strftime('%Y-%m-%d'))
        for reses, cod, des, tipus, perfil, npcr, ntar in u.getAll(sql, 'redics'):            
            periodo = DATE0.strftime('%Y-%m-%d') + ' - ' + DATE1.strftime('%Y-%m-%d')
            res_tip = self.get_res_tipc(tipus)
            ind = 'Proves (PCR)'
            self.proves_pers.append((periodo, ind, reses, cod, des, res_tip, perfil, npcr))
            ind = 'Proves (TAR)'
            self.proves_pers.append((periodo, ind, reses, cod, des, res_tip, perfil, ntar))
        # print("PROVES PERSONAL: N de registros:", len(self.proves_pers))
        # print(self.proves_pers)

    def get_exitus_residents(self):
        """ . """
        # print("----------------------------------------- get_exitus_residents")
        self.exitus_resi = c.Counter()
        self.res_tip_no178 = set()
        # self.ind_32 = []
        afectatc = {"Sospit\xf3s": "Compatible",
                    "Sospitós": "Compatible",
                    "Positiu per ELISA": "Confirmat",
                    "Positiu": "Confirmat",
                    "Positiu Epidemiol\xf2gic": "Compatible",
                    "Positiu Epidemiològic": "Compatible",
                    "Positiu per Test R\xe0pid": "Confirmat",
                    "Positiu per Test Ràpid": "Confirmat",
                    "PCR probable": "Compatible"}
        for cip13, data, afectat, lloc, perfil, residencia in self.exitus:
            if residencia in self.cat_res:
                if self.cat_res[residencia]['tip'] in (1,7,8):
                    ind = 'Exitus'
                    periodo = DATE0.strftime('%Y-%m-%d') + ' - ' + DATE1.strftime('%Y-%m-%d')
                    des = self.cat_res[residencia]['des']
                    reses = self.cat_res[residencia]['reses']
                    tipus = self.cat_res[residencia]['tip']
                    res_tip = self.get_res_tipc(tipus)
                    if afectatc[afectat] == 'Confirmat':
                        self.exitus_resi[(periodo, ind, reses, residencia, des, res_tip, perfil, lloc, 'Confirmat')] += 1
                    else:
                        self.exitus_resi[(periodo, ind, reses, residencia, des, res_tip, perfil, lloc, 'No Confirmat')] += 1
                else:
                    self.res_tip_no178.add((cip13))

    def get_upload(self):
        """ . """
        # print("--------------------------------------------------- get_upload")
        self.upload = []
        for periodo, ind, reses, cod, des, res_tip, perfil, N in self.pob_resi:
            self.upload.append((periodo, ind, reses, cod, des, res_tip, perfil, '', '', N))
        for periodo, ind, reses, cod, des, res_tip, perfil, N in self.casos_resi:
            self.upload.append((periodo, ind, reses, cod, des, res_tip, perfil, '', '', N))
        for periodo, ind, reses, cod, des, res_tip, perfil, N in self.proves_resi:
            self.upload.append((periodo, ind, reses, cod, des, res_tip, perfil, '', '', N))
        for (periodo, ind, reses, cod, des, res_tip, perfil, lloc, afectat), N in self.exitus_resi.items():
            self.upload.append((periodo, ind, reses, cod, des, res_tip, perfil, lloc, afectat, N))
        for periodo, ind, reses, cod, des, res_tip, perfil, N in self.pob_pers:
            self.upload.append((periodo, ind, reses, cod, des, res_tip, perfil, '', '', N))
        for periodo, ind, reses, cod, des, res_tip, perfil, N in self.casos_pers:
            self.upload.append((periodo, ind, reses, cod, des, res_tip, perfil, '', '', N))
        for periodo, ind, reses, cod, des, res_tip, perfil, N in self.proves_pers:
            self.upload.append((periodo, ind, reses, cod, des, res_tip, perfil, '', '', N))
        #print(self.upload)

    def export_upload(self):
        """."""
        # print("------------------------------------------------ export_upload")
        file = u.tempFolder + 'imserso_NivelResidencia_Semanal_{}.csv'
        file = file.format(DATE1.strftime('%Y%m%d'))
        u.writeCSV(file,
                   [('Periodo', 'Indicador', 'Reses_codi', 'Res_codi', 'Res_desc', 'Res_tipus', 'Perfil', 'Exitus_LLoc', 'Exitus_Afectat', 'N')] + self.upload,
                   sep=";")
        print("Send Mail")
        subject = 'Imserso - RESES - Setmanal'
        text = 'Adjuntem arxiu amb les dades per RESES Setmanal'
        u.sendGeneral('SISAP <sisap@gencat.cat>',
                      'avilar@gencat.cat',
                      'ehermosilla@idiapjgol.info',
                      subject,
                      text,
                      file)
        remove(file)


if __name__ == '__main__':
    print(datetime.now())
    ts = datetime.now()
    DIAEXEC = d.datetime.now().date()
    print(DIAEXEC)
    DATE1 = DIAEXEC - relativedelta(days=3)
    DATE0 = DIAEXEC - relativedelta(days=9)
    DATES = (DATE0, DATE1)
    print(DATES)
    imsersoMensual()
    print('Time execution {}'.format(datetime.now() - ts))
