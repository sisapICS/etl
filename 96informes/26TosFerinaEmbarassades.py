# coding: iso-8859-1
# Carmen Cabezas,  (ion 5060), novembre15, Ermengol
# Demana vacunes dTpa en embaràs

from sisapUtils import *
import csv
import os
import sys
from time import strftime
from collections import defaultdict, Counter
from taulesKhalix_utils import *

printTime('Inici')

debug = False

imp = 'import'
db = 'test'
nod = 'nodrizas'

temp = 'cat_vacunes'

# criteris
TosFerinaAg = 'A00003'
ics = "0208"

OutFile = tempFolder + 'vacunaTF.txt'
OutFile2 = tempFolder + 'vacunaTF_embaras.txt'
tableMy = 'tosFerina'

centres = {}

sql = "select scs_codi,ics_codi from cat_centres where ep={}".format(ics)
for up, br in getAll(sql, nod):
    centres[up] = {'br': br}

assig = {}
sql = 'select id_cip_sec,up, ates from assignada_tot'
for id, up, ates in getAll(sql, nod):
    try:
        br = centres[up]
    except KeyError:
        continue
    assig[id] = {'ates': ates, 'up': up, 'br': br}

sql = 'drop table if exists {}'.format(temp)
execute(sql, db)
sql = "create table {} as select vacu_cod vacuna,if(vacu_an_cod = '',hom_an_cod,vacu_an_cod) antigen,if(vacu_tipus_dosis = 0,300,vacu_tipus_dosis) dosis,if(vacu_model_nou='S','NEW','OLD') model from import.cat_prstb040 a left join import.cat_prstb062 b on a.vacu_cod_hom=b.hom_ch_cod".format(temp)
execute(sql, db)

Antigen = {}
sql = "select vacuna from {0} where antigen='{1}'".format(temp, TosFerinaAg)
for vac, in getAll(sql, db):
    Antigen[vac] = True

execute("drop table if exists {}".format(tableMy), db)
execute("create table {} (id_cip_sec int, data date, vacuna varchar(20), anys double)".format(tableMy), db)

sql = "select table_name from tables where table_schema='{0}' and table_name like '{1}_%'".format(imp, 'vacunes')
for particio, in getAll(sql, ('information_schema', 'aux')):
    printTime(particio)
    vacunes = {}
    sql = "select id_cip_sec,va_u_cod,year(va_u_data_vac),va_u_data_vac from {0} where va_u_data_baixa=0 {1}".format(particio, ' limit 10' if debug else '')
    for id, vac, anys, dat in getAll(sql, imp):
        anys = int(anys)
        try:
            Antigen[vac]
        except KeyError:
            continue
        vacunes[id, dat] = {'vac': vac, 'anys': anys}

    with openCSV(OutFile) as c:
        for (id, data), d in vacunes.items():
            c.writerow([id, data, d['vac'], d['anys']])
    loadData(OutFile, tableMy, db)

sql = 'drop table if exists embaras'
execute(sql, db)
sql = "CREATE TABLE embaras (id_cip_sec int,emb_d_ini date,inici date,fi date,temps double,risc varchar(15),emb_c_tanca varchar(15), index (temps)) \
    select  id_cip_sec,emb_d_ini,if(emb_dur > data_ext, emb_d_ini, if(year(emb_dur) < 2000, emb_d_ini, emb_dur)) as inici,if(if(emb_d_fi > 0,emb_d_fi \
    ,if(emb_d_tanca>0,emb_d_tanca,if(year(emb_dur) < 2000, date_add(emb_d_ini, interval 42 week),date_add(emb_dur, interval 42 week))))	> data_ext,data_ext, \
    if(emb_d_fi > 0,emb_d_fi,if(emb_d_tanca>0,emb_d_tanca,if(year(emb_dur) < 2000, date_add(emb_d_ini, interval 42 week),date_add(emb_dur, interval 42 week))))) as fi \
    ,datediff(if(if(emb_d_fi > 0,emb_d_fi,if(emb_d_tanca>0,emb_d_tanca,if(year(emb_dur) < 2000, date_add(emb_d_ini, interval 42 week),date_add(emb_dur, interval 42 week)))) > data_ext,data_ext, \
    if(emb_d_fi > 0,emb_d_fi,if(emb_d_tanca>0,emb_d_tanca,if(year(emb_dur) < 2000, date_add(emb_d_ini, interval 42 week),date_add(emb_dur, interval 42 week))))),if(emb_dur > data_ext, emb_d_ini, if(year(emb_dur) < 2000, emb_d_ini, emb_dur))\
    ) as temps  ,emb_risc risc   ,emb_c_tanca from	import.embaras e, nodrizas.dextraccio where	if(emb_dur > data_ext, emb_d_ini, if(year(emb_dur) < 2000, emb_d_ini, emb_dur))"
execute(sql, db)

embarasTF = {}
sql = "select id_cip_sec, year(fi), inici, fi from embaras,nodrizas.dextraccio where  temps>90 and emb_c_tanca not in ('A','Al','IV','MF','MH','EE') and fi<data_ext and fi > date_add(data_ext,interval - 1 year)"
for id, anys, inici, fi in getAll(sql, db):
    embarasTF[id] = {'inici': inici, 'fi': fi, 'num': 0}

sql = 'select id_cip_sec, data, anys from {}'.format(tableMy)
for id, data, anys in getAll(sql, db):
    try:
        inici = embarasTF[id]['inici']
        fi = embarasTF[id]['fi']
    except KeyError:
        continue
    if inici <= data <= fi:
        embarasTF[id]['num'] = 1

recompte = Counter()
for (id), d in embarasTF.items():
    num = d['num']
    try:
        ates = assig[id]['ates']
    except KeyError:
        continue
    recompte[ates, num] += 1

try:
    remove(OutFile2)
except:
    pass

with openCSV(OutFile2) as c:
    for (ates, num), d in recompte.items():
        c.writerow([ates, num, d])

printTime('Fi')
