import sisapUtils as u

sql = """SELECT rv_low_value, rv_meaning, rv_domain
FROM PRITB000 WHERE RV_TABLE ='ESCOLES'"""

redics = []
for codi, desc, up in u.getAll(sql, '6837'):
    redics.append((int(codi), desc, up))

sql = """SELECT distinct codi_centre, denominaci_completa FROM dwsisap.cat_escoles_do"""

dades_obertes = []
for codi, desc in u.getAll(sql, 'exadata'):
    try: 
        int(str(codi))
        dades_obertes.append((int(codi), desc))
    except:
        pass

u.createTable('escoles_redics', '(codi int, descrip varchar(200), up varchar(5))', 'test', rm=True)
u.listToTable(redics, 'escoles_redics', 'test')

u.createTable('escoles_dades_obertes', '(codi int, descrip varchar(200))', 'test', rm=True)
u.listToTable(dades_obertes, 'escoles_dades_obertes', 'test')
