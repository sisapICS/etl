# coding: iso-8859-1
#Carol Validacio ITS, Ermengol, Desembre 2016

from sisapUtils import *
from collections import defaultdict, Counter

db = 'altres'
nod = 'nodrizas'
imp = 'import'

indicadors = ('ITS03', 'ITS04', 'ITS05', 'ITS06', 'ITS07', 'ITS0105')
saps = '(51, 52, 53, 18, 57)'
LaModel = "('07676')"


hash_to_cip = {hash: cip for (hash, cip) in getAll('select usua_cip_cod, usua_cip from pdptb101', 'pdp')}
hashos = {}
sql = 'select id_cip_sec, codi_sector, hash_d from u11'
for id, sector, hash in getAll(sql, imp):
    if hash in hash_to_cip:
        hashos[id] = {'sector': sector, 'hash': hash_to_cip[hash]}
    
sql = 'select id_cip_sec, codi_sector, hash_d from u11'
for id, sector, hash in getAll(sql, 'import_jail'):
    if hash in hash_to_cip:
        hashos[id] = {'sector': sector, 'hash': hash_to_cip[hash]}

centres = {}
sql = 'select scs_codi, ics_desc, sap_codi, sap_desc from cat_centres where sap_codi in {}'.format(saps)
for up, desc, sap, sapdesc in getAll(sql, nod):
    centres[up] = {'desc': desc, 'sap': sap, 'sapdesc': sapdesc}
sql = 'select  scs_codi, ics_desc, sap_codi, sap_desc from jail_centres where scs_codi in {}'.format(LaModel)
for up, desc, sap, sapdesc in getAll(sql, nod):
    centres[up] = {'desc': desc, 'sap': sap, 'sapdesc': sapdesc}
    
table = 'mst_indicadors_its_pacient'

upload = []
sql = 'select id_cip_sec, indicador, up, num, den from {0} where indicador in {1}'.format(table, indicadors)
for id, indicador, up, num, den in getAll(sql, db):
    try:
        sap = centres[up]['sap']
        desc = centres[up]['desc']
        sapdesc = centres[up]['sapdesc']
    except KeyError:
        continue
    try:
        hash = hashos[id]['hash']
        sector = hashos[id]['sector']
    except KeyError:
        continue
    upload.append([hash, sector, up, desc, sap, sapdesc, indicador, int(num)])

file = tempFolder + 'validacio_ITS.txt'
writeCSV(file, upload, sep=';')

        

    
    