# -*- coding: utf8 -*-

import hashlib as h

"""
Escoles sentinella
"""

import collections as c
import datetime as d
import sys


import sisapUtils as u

def get_nivell(nivell):

    if nivell == 'EINFLOE':
        niv = 'Educació infantil'
    elif nivell == 'EPRILOE':
        niv = 'Educació primària'
    elif nivell == 'ESO':
        niv = 'Educació secundària obligatòria'
    elif nivell == 'BATXLOE':
        niv = 'Batxillerat'
    elif nivell == 'EE':
        niv = 'Educació especial'
    elif nivell in ('CFPS', 'CFPM', 'CFAM', 'CFAS'):
        niv = 'Cicles formatius'
    else:
        niv= 'Altres'
    
    return niv

class sentinella(object):
    """."""

    def __init__(self):
        """."""
        self.get_data()
        self.get_periodes()
        self.get_poblacionals()
        self.get_pob4()
        self.get_perfil()
        self.get_nivells()
        self.get_positius()
        self.get_pcr()
        self.get_cens_escola()
        self.export_file()
        
        
    def get_data(self):
        """agafem data calcul"""
        u.printTime("Data càlcul")
        sql = "select min(data), max(data) from sisap_covid_web_master_t where to_char(data, 'YYYYMMDD')>= '20200914'"
        for mind, maxd in u.getAll(sql, 'redics'):
            self.calcul_avui = maxd
            self.ini = mind
            self.fi = maxd + d.timedelta(days=1)
        print self.calcul_avui, self.ini, self.fi
    
    def get_periodes(self):
        """Agafem els dies de període mensual"""
        u.printTime("Període mensual")
        self.mensuals = {}
        self. periodes = {}
        for dia in u.dateRange(self.ini, self.fi):
            dia28 = dia - d.timedelta(days=27)
            self.mensuals[dia] = dia28
        d15 = self.ini
        self.periodes[d15] = True
        for i in range(25):
            print d15
            d15_u = d15 + d.timedelta(days=15)
            self.periodes[d15_u] = True
            d15 = d15_u
            
    def get_poblacionals(self):
        """Indicadors casos, pcr i tar poblacionals mensuals"""
        self.poblacional = c.Counter()
        sql = """SELECT DATA, abs, grup,sum(CASOS_CONFIRMAT), sum(pcr), sum(tar) 
                FROM sisap_covid_web_master_t 
                WHERE residencia=0 
                GROUP BY DATA, abs, grup"""
        for data, abs, grup, casos, pcr, tar in u.getAll(sql, 'redics'):
            self.poblacional[(data, abs, grup, 'casos')] += casos
            self.poblacional[(data, abs, grup, 'pcr')] += pcr
            self.poblacional[(data, abs, grup, 'tar')] += tar
        
        self.recomptes = c.Counter()
        for (data, abs, grup, indi), n in self.poblacional.items():
            if self.ini<= data <= self.fi:
                if data in self.periodes:
                    try:
                        min_mensual = self.mensuals[data]
                    except KeyError:
                        continue
                    self.recomptes[(data, abs, grup, indi)] += n
                    for i in u.dateRange(min_mensual, data):
                        nrec = self.poblacional[(i, abs, grup, indi)]
                        self.recomptes[(data, abs, grup, indi)] += nrec
                
                
    def get_pob4(self):
        """poblacio pel denominador"""
        
        sql = """SELECT DATA, abs, grup,sum(poblacio) 
            FROM preduffa.sisap_covid_web_master_t 
            WHERE residencia=0 
            GROUP BY DATA, abs, grup"""
        for data, abs, grup, pob in u.getAll(sql, 'redics'):
            if data in self.periodes:
                self.recomptes[(data, abs, grup, 'pob')] = pob
    
    def get_perfil(self):
        """."""
        u.printTime("perfils")
        self.perfils = {}
        sql = "select id, nom from dwsisap_escola.PERSONES_PERFIL"
        for id, nom in u.getAll(sql, 'exadata'):
            self.perfils[id] = nom
    
    def get_nivells(self):
        """."""
        u.printTime("nivells")
        self.nivells = {}
        sql = """SELECT a.codi, b.codi 
            FROM dwsisap_escola.grups_ensenyament a 
            INNER JOIN  dwsisap_escola.grups_tipusensenyament b 
            ON a.tipus_id=b.id"""
        for codi, nom in u.getAll(sql, 'exadata'):
            nivell = get_nivell(nom)
            self.nivells[codi] = nivell
    
    def get_positius(self):
        """Positius"""
        u.printTime("positius")
        self.positius1 = {}
        sql = """SELECT persona_id, data_prova 
        FROM dwsisap_escola.CLINICA_POSITIU"""
        for id, data in u.getAll(sql, 'exadata'):
            self.positius1[id] = data
            
        self.positius = {}
        self.perfil_persona = {}
        sql = """SELECT id, cip, perfil_id
        FROM DWSISAP_ESCOLA.PERSONES_PERSONA"""
        for id, cip, perfil_id in u.getAll(sql, 'exadata'):
            perfil_desc = self.perfils[perfil_id] if perfil_id in self.perfils else 'Desconegut'
            self.perfil_persona[cip] = perfil_desc
            if id in self.positius1:
                data = self.positius1[id]
                self.positius[cip] = data
    
    def get_pcr(self):
        """Agafem pcr"""
        u.printTime("pcr")
        self.pcrs = c.defaultdict(set)
        self.tars = c.defaultdict(set)    
        
        sql = """select cip, data_validacio_mostra dat, count(*)
                from dwaquas.covid19_aut_resul_pcr a INNER JOIN dwaquas.COVID19_EDU_INFOEDUCACIO  b ON a.cip=b.tis
                where data_validacio_mostra is not null
                and resultat_codi in (0, 1, 3, 4) group by cip, data_validacio_mostra"""
        for cip, dat, nnn in u.getAll(sql, 'exadata'):
            self.pcrs[cip].add(dat)
            
        sql = """select cip, data_prova , count(*)
            from dwaquas.covid19_aut_resul_antigen a INNER JOIN dwaquas.COVID19_EDU_INFOEDUCACIO  b ON a.cip=b.tis
                where resultat_codi in (0, 1) group by cip, data_prova"""
        for cip, dat, nnn in u.getAll(sql, 'exadata'):
            self.tars[cip].add(dat)
    
    def get_cens_escola(self):
        """Cens escoles"""
        u.printTime("cens")
        self.rec_esc= c.Counter()
        self.pob_esc = c.Counter()
        sql = """SELECT tis, ensenyament_codi, abs_id  FROM dwaquas.COVID19_EDU_INFOEDUCACIO c
            INNER JOIN dwsisap_escola.centres_centre d ON c.centre_codi = d.id"""
        for cip, nivell, abs in u.getAll(sql, 'exadata'):
            if cip in self.perfil_persona:
                perfil = self.perfil_persona[cip] 
                nivell_desc = self.nivells[nivell] if nivell in self.nivells else 'Altres'
                self.pob_esc[(abs, perfil, nivell_desc, 'pob')] += 1
                if cip in self.positius:
                    data = self.positius[cip]
                    self.rec_esc[(abs, perfil, nivell_desc, data, 'casos')] += 1
                if cip in self.pcrs:
                    dates = self.pcrs[cip]
                    for dat in dates:
                        self.rec_esc[(abs, perfil, nivell_desc, dat, 'pcr')] += 1
                if cip in self.tars:
                    dates = self.pcrs[cip]
                    for dat in dates:
                        self.rec_esc[(abs, perfil, nivell_desc, dat, 'tar')] += 1
                        
        self.recomptes2 = c.Counter()
        for data in self.periodes:
            try:
                min_mensual = self.mensuals[data]
            except KeyError:
                continue
            d2 = data + d.timedelta(days=1)
            for i in u.dateRange(min_mensual, d2):
                for (abs, perfil, nivell_desc, dat, indi), n in self.rec_esc.items():
                    if dat == i:
                        self.recomptes2[(data, abs, perfil, nivell_desc, indi)] += n
                    
        self.upload2 = []
        
        for (abs, perfil, nivell_desc, ind), n in self.pob_esc.items():
            for periode in self.periodes:
                if periode <= self.fi:
                    casos = self.recomptes2[(periode, abs, perfil, nivell_desc, 'casos')] if (periode, abs, perfil, nivell_desc, 'casos') in self.recomptes2 else 0
                    pcr = self.recomptes2[(periode, abs, perfil, nivell_desc, 'pcr')] if (periode, abs, perfil, nivell_desc, 'pcr') in self.recomptes2 else 0
                    tar = self.recomptes2[(periode, abs, perfil, nivell_desc, 'tar')] if (periode, abs, perfil, nivell_desc, 'tar') in self.recomptes2 else 0
                    self.upload2.append([periode, abs, perfil, nivell_desc, n, casos, pcr, tar])
            
    def export_file(self):
        """."""
        u.printTime("export files")
        
        file = u.tempFolder + "escola_sentinella_pob.csv"
        
        self.upload1 = []
        for (data, abs, grup, indi), n in self.recomptes.items():
            if indi == 'pob':
                casos = self.recomptes[(data, abs, grup, 'casos')] if (data, abs, grup, 'casos') in self.recomptes else 0
                pcr = self.recomptes[(data, abs, grup, 'pcr')] if (data, abs, grup, 'pcr') in self.recomptes else 0
                tar = self.recomptes[(data, abs, grup, 'tar')] if (data, abs, grup, 'tar') in self.recomptes else 0
                self.upload1.append([data, abs, grup, n, casos, pcr, tar])
        
        u.writeCSV(file, self.upload1, sep='@')   

        file = u.tempFolder + "escola_sentinella_escolar.csv"
        
        u.writeCSV(file, self.upload2, sep='@')
        
       
if __name__ == '__main__':
    u.printTime("Inici")
     
    sentinella()
    
    u.printTime("Final")    