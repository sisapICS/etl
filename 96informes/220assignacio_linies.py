# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


TABLE = "ASSIGNACIO_LINIES"
DATABASE = "permanent"


class Linies(object):
    """."""

    def __init__(self):
        """."""
        self.get_linies()
        self.get_comuncacio()
        self.get_abs()
        self.get_poblacio()
        self.upload_data()

    def get_linies(self):
        """."""
        sql = "select sector, scs_codi from cat_centres where tip_eap = 'N'"
        self.linies = tuple([up for (sec, up) in u.getAll(sql, "nodrizas")])
        self.sectors = set([sec for (sec, up) in u.getAll(sql, "nodrizas")])

    def get_comuncacio(self):
        """."""
        self.comunicacio = {}
        self.relacio = c.defaultdict(set)
        sql = "select distinct lloc_codi_up up, lloc_uab_codi_uab_assignat__a,\
                               lloc_up_rca \
               from cat_pritb025_def \
               where lloc_codi_up in {}".format(self.linies)
        for up, uba, com in u.getAll(sql, "import"):
            self.comunicacio[(up, uba)] = com
            self.relacio[up].add(com)

    def get_abs(self):
        """."""
        sql = "select abs_codi_abs, abs_codi_up from cat_rittb001"
        self.abs = {row[0]: row[1] for row in u.getAll(sql, "import")}

    def get_poblacio(self):
        """."""
        self.poblacio = []
        sql = "select usua_cip_cod, usua_uab_up, usua_uab_codi_uab, \
                      usua_up_rca, usua_abs_codi_abs, \
                      usua_nass1||usua_nass2||usua_nass3, usua_relacio \
               from usutb040 a, pdptb101 b \
               where usua_situacio = 'A' and a.usua_cip = b.usua_cip"
        jobs = [(sql, sector + "a") for sector in self.sectors]
        for sector in u.multiprocess(u.get_data, jobs):
            nass_to_up = {}
            for hash, up, uba, rca, abs, nass, rel in sector:
                if rel == "T":
                    nass_to_up[nass] = up
            for hash, up, uba, rca, abs, nass, rel in sector:
                if up in self.linies:
                    up_com = self.comunicacio.get((up, uba))
                    up_abs = self.abs.get(abs)
                    up_nass = nass_to_up.get(nass)
                    this = (hash, up, uba, up_com, up_abs, rca, up_nass)
                    self.poblacio.append(this)

    def upload_data(self):
        """."""
        cols = "(pacient varchar(40), linia varchar(5), uba varchar(5), \
                 comunicacio varchar(5), abs varchar(5), rca varchar(5), \
                 titular varchar(5))"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.poblacio, TABLE, DATABASE)


if __name__ == "__main__":
    Linies()
