# -*- coding: utf-8 -*-

import sisapUtils as u
import collections as c


class Visites():
    def __init__(self):
        self.create_table()
        self.get_professionals_diaris()
        self.get_visites_diaries()
        self.upload_to_exa()

    def get_professionals_diaris(self):
        print('professionals')
        self.professionals_diaris = c.defaultdict(dict)
        sql = """SELECT
                    DATA,
                    SISAP_SERVEI_CLASS,
                    count(DISTINCT PROFESSIONAL)
                FROM
                    dwsisap.SISAP_MASTER_VISITES smv
                INNER JOIN 
                    dwsisap.dbc_centres_tots
                    ON up = up_cod
                WHERE
                    SISAP_SERVEI_CLASS IN ('MF', 'INF', 'PED')
                    AND SITUACIO = 'R'
                    AND TIPUS IN ('9C', '9T', '9R', '9D', '9E')
                    AND EXTRACT(YEAR FROM DATA) IN (2019)
                GROUP BY
                    DATA,
	                SISAP_SERVEI_CLASS"""
        for data, servei, n_prof in u.getAll(sql, 'exadata'):
            self.professionals_diaris[data][servei] = n_prof

    def get_visites_diaries(self):
        print('visites')
        self.visites_diaries = c.defaultdict(dict)
        sql = """SELECT
                    DATA,
                    SISAP_SERVEI_CLASS,
                    TIPUS,
                    COUNT(1)
                FROM
                    dwsisap.SISAP_MASTER_VISITES
                INNER JOIN 
                    dwsisap.dbc_centres_tots
                    ON up = up_cod
                WHERE
                    SISAP_SERVEI_CLASS IN ('MF', 'INF', 'PED')
                    AND SITUACIO = 'R'
                    AND TIPUS IN ('9C', '9T', '9R', '9D', '9E')
                    AND EXTRACT(YEAR FROM DATA) IN (2019)
                GROUP BY
                    DATA,
                    SISAP_SERVEI_CLASS,
                    TIPUS""" 
        for data, servei, tipus_visita, n_visites in u.getAll(sql, 'exadata'):
            self.visites_diaries[data][servei, tipus_visita] = n_visites

    def create_table(self):
        print('create table')
        cols = """(data date, tipus_dia varchar2(1), N_metges int, vi_mf_9c int, vi_mf_9t int, 
                        vi_mf_9r int, vi_mf_9d int, vi_mf_9e int, N_infs int, 
                        vi_inf_9c int, vi_inf_9t int, vi_inf_9r int,
                        vi_inf_9d int, vi_inf_9e int, N_pedia int, 
                        vi_ped_9c int, vi_ped_9t int, vi_ped_9r int, 
                        vi_ped_9d int, vi_ped_9e int)"""
        self.table = 'tipus_de_visita_per_dia'
        self.db = 'exadata'
        # u.createTable(self.table, cols, self.db, rm=True)
        u.grantSelect('tipus_de_visita_per_dia', 'DWSISAP_ROL', 'exadata')

    def upload_to_exa(self):
        """Sortida: per cada dia per servei i 
            tipus de visita i quants professionals 
            de cada servei han visitat"""
        print('upload')
        upload = []
        for data in self.visites_diaries:
            mf_9c = 0
            mf_9t = 0
            mf_9r = 0
            mf_9d = 0
            mf_9e = 0
            inf_9c = 0
            inf_9t = 0
            inf_9r = 0
            inf_9d = 0
            inf_9e = 0
            ped_9c = 0
            ped_9t = 0
            ped_9r = 0
            ped_9d = 0
            ped_9e = 0
            for (servei, tipus_visita), n_visites in self.visites_diaries[data].items():
                if servei == 'MF':
                    if tipus_visita == '9C': mf_9c = n_visites
                    elif tipus_visita == '9T': mf_9t = n_visites
                    elif tipus_visita == '9R': mf_9r = n_visites
                    elif tipus_visita == '9D': mf_9d = n_visites
                    elif tipus_visita == '9E': mf_9e = n_visites
                elif servei == 'INF':
                    if tipus_visita == '9C': inf_9c = n_visites
                    elif tipus_visita == '9T': inf_9t = n_visites
                    elif tipus_visita == '9R': inf_9r = n_visites
                    elif tipus_visita == '9D': inf_9d = n_visites
                    elif tipus_visita == '9E': inf_9e = n_visites
                elif servei == 'PED':
                    if tipus_visita == '9C': ped_9c = n_visites
                    elif tipus_visita == '9T': ped_9t = n_visites
                    elif tipus_visita == '9R': ped_9r = n_visites
                    elif tipus_visita == '9D': ped_9d = n_visites
                    elif tipus_visita == '9E': ped_9e = n_visites
            
            mf, inf, ped = 0, 0, 0
            for servei, n_prof in self.professionals_diaris[data].items():
                if servei == 'MF': mf = n_prof
                elif servei == 'INF': inf = n_prof
                elif servei == 'PED': ped = n_prof
            
            if u.isWorkingDay(data): working_day = 'L'
            else: working_day = 'F'

            upload.append((data, working_day, mf, mf_9c, mf_9t, mf_9r, mf_9d, 
                            mf_9e, inf, inf_9c, inf_9t, inf_9r, 
                            inf_9d, inf_9e, ped, ped_9c, ped_9t, 
                            ped_9r, ped_9d, ped_9e))
        u.listToTable(upload, self.table, self.db)


if __name__ == "__main__":
    Visites()