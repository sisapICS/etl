import sisapUtils as u


db = ('backup', 'analisi')
tb_name = "farma_resi"

class dades_farma_resi():
    def __init__(self):
        columnes = """(
                    pac varchar(40), 
                    atc varchar(120), 
                    cod_atc varchar(7), 
                    up varchar(5), 
                    sexe varchar(1), 
                    nacionalitat varchar(100), 
                    data_naix date, 
                    ambit varchar(40), 
                    medea varchar(2)
                    )"""
        u.createTable(tb_name, columnes, db, rm=1)
        self.get_centres()
        self.get_pob()
        self.get_prescripcions()
        self.create_table()
    
    def get_centres(self):
        sql = """select scs_codi, amb_desc, medea from nodrizas.cat_centres cc 
                    where ep = '0208'"""
        self.centres = {up: [ambit, medea] for up, ambit, medea in u.getAll(sql, 'nodrizas')}
    
    def get_pob(self):
        sql = """SELECT c_cip, C_UP, C_SEXE, C_NACIONALITAT, C_DATA_NAIX
                    FROM dbs"""
        self.pob = {id: [up, sexe, nacionalitat, data_naix] for id, up, sexe, nacionalitat, data_naix in u.getAll(sql, 'redics')}

    def get_prescripcions(self):
        sql = """SELECT REC_CIP, ATC_DESC, PF_COD_ATC FROM fartb201
            WHERE rec_any_mes = '202410'
            AND (PF_COD_ATC like 'N05B%' or PF_COD_ATC like 'N05C%')"""
        self.prescripcions = set([(pac, atc, cod_atc) for pac, atc, cod_atc in u.getAll(sql, 'redics')])
    
    def create_table(self):
        upload = []
        for pac, atc, cod_atc in self.prescripcions:
            if pac in self.pob:
                up, sexe, nacionalitat, data_naix = self.pob[pac]
                if up in self.centres:
                    ambit, medea = self.centres[up]
                    upload.append((pac, atc, cod_atc, up, sexe, nacionalitat, data_naix, ambit, medea))
        u.listToTable(upload, tb_name, db)


if __name__ == "__main__":
    dades_farma_resi()