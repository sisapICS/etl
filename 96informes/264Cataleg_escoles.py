# -*- coding: utf-8 -*-


import sodapy
import sisapUtils as u
import datetime as d

SERVER = "analisi.transparenciacatalunya.cat"
TOKEN = "2NvQRTDJA6n8ZG1X78BKCYtwh"
DATASET = "xvme-26kg"



class Dades_obertes():
    def __init__(self):
        self.get_client()
        self.get_columns()
        self.get_dades()

    def get_client(self):
        """."""
        self.client = sodapy.Socrata(SERVER, TOKEN) # CONNEXI� DADES OBERTES
        self.client.timeout = 5

    def get_columns(self):
        conversion = {"text": "varchar2(1024)", "number": "decimal(25, 8)",
                      "calendar_date": "date", "point": "varchar2(1024)"}
        self.columns = []
        self.son_data = set()
        dades = self.client.get_metadata(DATASET)["columns"]
        for i, column in enumerate(dades):
            if "@" not in column["fieldName"]:
                tipus = conversion[column["dataTypeName"]]
                this = " ".join((column["fieldName"], tipus))
                self.columns.append(this)
                if tipus == "date":
                    self.son_data.add(i)

    def get_dades(self):
        """."""
        self.upload = []
        where = "any = 2021"
        for i, row in enumerate(self.client.get_all(DATASET, content_type="csv", where=where)):  # noqa
            if i > 0 and row[0] != "id":
                for position in self.son_data:
                    if row[position]:
                        row[position] = d.datetime.fromisoformat(row[position])
                    else:
                        row[position] = None
                self.upload.append(row)
        print(self.upload[0:5])

    def create_table(self):
        """."""
        cols = ", ".join(self.columns)
        cols = '(' + cols + ')'
        u.crateTabe('cat_escoles', cols, 'altres')
        u.listToTable(upload, 'cat_escoles', 'altres')
    

if __name__ == "__main__":
    Dades_obertes()




########## BUSCAR GEOLOCALITZACI�

# -*- coding: utf-8 -*-

"""
10 7-19/2 * * * bash /home/sisap/covid/execute.sh standalone/escoles
"""

import collections as c
import csv
import datetime as d
import geopandas as gdp
import hashlib as h
import os
import pandas as pd
import shapely.geometry as g
import urllib.request as w
import utm
import zipfile as z

import shared as s
import sisaptools as t


REFRESH_CENS = d.datetime.now().hour == 9
DO_BACKUP = d.datetime.now().hour == 19
TABLE_OWNER = "DWSISAP_ESCOLA"
INICI_CURS = "2021-09-13"


def _update_table(table, model, info, is_cens):
    """."""
    u.Database("exadata", "escola").execute("truncate table {}_A".format(table))  # noqa
    table_name = "{}.{}".format(TABLE_OWNER, table)
    with u.Database("exadata", "data") as exadata:
        if model == "sql":
            exadata.execute("insert into {}_A {}".format(table_name, info))
        elif model == "data" and info:
            exadata.list_to_table(info, table_name + "_A", chunk=10 ** 5)
    _rotate_table(table)
    if DO_BACKUP or (is_cens and REFRESH_CENS):
        _backup_table(table)


def _rotate_table(table):
    """."""
    rename = [("", "_B"), ("_A", ""), ("_B", "_A")]
    with u.Database("exadata", "escola") as escola:
        for origen, desti in rename:
            sql = "rename {0}{1} to {0}{2}".format(table, origen, desti)
            escola.execute(sql)
        escola.set_grants("select", table, "DWSISAP_ROL", inheritance=False)
        escola.set_statistics(table)


def _backup_table(table):
    """."""
    table_h = table + "_h"
    with u.Database("exadata", "escola") as escola:
        try:
            sql = "insert into {} select trunc(sysdate), a.* from {} a"
            escola.execute(sql.format(table_h, table))
        except Exception:
            sql = "create table {} as \
                   select trunc(sysdate) dia_h, a.* from {} a"
            escola.execute(sql.format(table_h, table))
        escola.set_grants("select", table_h, "DWSISAP_ROL", inheritance=False)
        # escola.set_statistics(table_h)


def _get_hash(*params):
    """."""
    return h.sha1("".join(map(str, params)).encode("latin1")).hexdigest().upper()  # noqa


class Escoles(object):
    """."""

    def __init__(self):
        """."""
        self.get_mapa() # ABS
        self.get_manuals() # CANVIS MENSUALS
        self.get_centres() # �S EL Q BUSCA ABS --> LO IMPORTANT
        self.get_abs()

    def get_mapa(self):
        """."""
        folder = t.constants.TMP_FOLDER
        file = "ABS.zip"
        map = "ABS_2018_2021.shp"
        url = "https://salutweb.gencat.cat/web/.content/_departament/estadistiques-sanitaries/cartografia/ABS_2018-2021.zip"  # noqa
        local, headers = w.urlretrieve(url, os.path.join(folder, file))
        with z.ZipFile(os.path.join(folder, file), 'r') as zip_ref:
            zip_ref.extractall(folder)
        self.shapes = gdp.read_file(os.path.join(folder, map))
        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            if os.path.isfile(file_path):
                os.remove(file_path)

    def _get_abs_from_point(self, point):
        """."""
        for ix, area in self.shapes.iterrows():
            is_in_area = point.within(area['geometry'])
            if is_in_area:
                if area["CODIABS"] in ("381", "382"):
                    abs = "302"  # Montcada i Reixac
                else:
                    abs = area["CODIABS"]
                return abs
        return None

    def get_manuals(self):
        """."""
        file = "escoles_relacio_abs.csv"
        u.SFTP("sisap").get("sms/{}".format(file), file)
        self.manuals = {centre.zfill(8): abs.zfill(3)
                        for (centre, abs)
                        in csv.reader(open(file, encoding="latin1"), delimiter=";")}  # noqa
        os.remove(file)
        self.chapuzas = {"08059524": "211", "08015961": "365",
                         "08053996": "336"}

    def get_centres(self):
        """."""
        file = os.path.dirname(os.path.abspath(__file__)) + "/sve.txt"
        sves = {row[0]: (int(row[1]), row[2]) for row
                in csv.reader(open(file, encoding="utf8"), delimiter=";")}
        upload = c.defaultdict(set)
        sql = "select lpad(centre_codi, 8, 0), centre_desc, telefon, email, \
                      adreca, localitat_desc, lpad(municipi_codi, 5, 0), \
                      municipi_desc, lpad(comarca_codi, 2, 0), comarca_desc, \
                      lpad(delegacio_codi, 4, 0), delegacio_desc, \
                      coor_geo_latitud, coor_geo_longitud \
               from dwaquas.covid19_edu_centres \
               where lpad(centre_codi, 8, 0) in (select centre_id \
                                                 from {}.grups_gce) and \
                     naturalesa_codi is not null and \
                     municipi_codi is not null".format(TABLE_OWNER)
        for (centre_c, centre_d, telefon, email, adreca, loc_d, muni_c,
             muni_d, com_c, com_d, deleg_c, deleg_d,
             lat, long) in u.Database("exadata", "data").get_all(sql):
            if muni_c == "08258":
                com_c = "42"
                com_d = "Moian�s"
            if com_c == "40":
                com_d = "Vall�s Occidental"
            if com_c == "16":
                com_d = "Conca de Barber�"
            if com_c == "13":
                com_d = "Barcelon�s"
            if muni_c in ("08256", "08261"):
                deleg_c = "0508"
                deleg_d = "Maresme - Vall�s Oriental"
            if muni_c == "08019":
                deleg_c = "0108"
                deleg_d = "Consorci d'Educaci� de Barcelona"
            if centre_c in self.manuals:
                abs = self.manuals[centre_c]
            else:
                try:
                    pair = utm.from_latlon(lat, long)[:2]
                    point = g.Point(*pair)
                    abs = self._get_abs_from_point(point)
                except utm.error.OutOfRangeError:
                    abs = None
            if not abs:
                abs = self.chapuzas[centre_c]
            sve_id, sve = sves[muni_c]
            upload["centre"].add((centre_c, centre_d, telefon, email, adreca,
                                  loc_d, abs, muni_c))
            upload["municipi"].add((muni_c, muni_d, com_c, deleg_c, sve_id))
            upload["comarca"].add((com_c, com_d))
            if deleg_d[:2] != "ST":
                upload["delegacio"].add((deleg_c, deleg_d))
            upload["sve"].add((sve_id, sve))
        for key, data in upload.items():
            _update_table("centres_" + key, "data", list(data), is_cens=True)

    def get_abs(self):
        """."""
        upload = c.defaultdict(set)
        sql = "select a.abs_cod, a.abs_des, \
                      a.sector_cod, a.sector_des, \
                      case when b.abs_cod in ('302', '312') \
                           then 12 else b.regio_cod end, b.regio_des, \
                      a.eap, d.des, \
                      nvl(c.sap_cod, '99'), sap_des, \
                      nvl(c.amb_cod, '00'), amb_des, \
                      decode(e.ep, '0208', 1, 0), decode(e.up, NULL, 0, 1) \
               from {0}cat_abs a, {0}cat_aga b, {0}cat_up c, \
                    {0}cat_territori d, {0}cat_up_ecap e \
               where a.abs_cod = b.abs_cod and \
                     a.EAP = c.up_cod(+) and \
                     a.eap = d.up and \
                     a.eap = e.up(+)".format(s.PREFIX)
        for (abs_c, abs_d, sec_c, sec_d, reg_c, reg_d,
             up_c, up_d, sap_c, sap_d, amb_c, amb_d,
             ics, ecap) in u.Database("redics", "data").get_all(sql):
            upload["abs"].add((abs_c, abs_d, up_c, up_d, ics, ecap, sap_c, sec_c))  # noqa
            if abs_c not in ("302", "312"):
                upload["sector"].add((sec_c, sec_d, reg_c))
                upload["regio"].add((reg_c, reg_d))
            if sap_d:
                upload["sap"].add((sap_c, sap_d, amb_c))
                upload["ambit"].add((amb_c, amb_d))
        for key, data in upload.items():
            _update_table("centres_" + key, "data", list(data), is_cens=True)


# if __name__ == "__main__":
#     try:
#         Escoles()
#     except Exception as e:
#         mail = u.Mail()
#         mail.to.append("roser.cantenys@catsalut.cat")
#         mail.subject = "Escoles bad!!!"
#         mail.text = str(e)
#         mail.send()




