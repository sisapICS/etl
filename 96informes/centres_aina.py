import sisapUtils as u

'''sql = """
SELECT a.up_cod, a.up_des, a.abs_cod, a.abs_des, a.subtip_cod, a.subtip_des,
CASE WHEN b.desc_tipus_via is null THEN '' ELSE
concat(b.desc_tipus_via, ' ', b.nom_via, ' ', b.num_via) as direccio,
CASE WHEN b.desc_localitat is null then '' else
b.desc_localitat,
CASE WHEN b.codi_postal is null then '' else
b.codi_postal 
FROM dwsisap.dbc_rup a
LEFT JOIN dwdimics.DIM_UP_CATSALUT b on a.up_cod = b.codi_up 
WHERE a.subtip_cod 
IN (21, 49) OR (a.subtip_cod = 10 AND a.up_des LIKE 'EAPT%') OR a.up_des LIKE '%CAP%'
"""'''

sql = """
SELECT a.up_cod, a.up_des, a.abs_cod, a.abs_des, a.subtip_cod, a.subtip_des
FROM dwsisap.dbc_rup a
WHERE a.subtip_cod 
IN (21, 49) OR (a.subtip_cod = 10 AND a.up_des LIKE 'EAPT%') OR a.up_des LIKE '%CAP%'
"""
dbc = {}
upload = []
for up, up_des, abs_cod, abs_des, subtipus, subtip_des in u.getAll(sql, 'exadata'):
    #upload.append((up, up_des, abs_cod, abs_des, subtipus, subtip_des))
    dbc[up] = (up_des, abs_cod, abs_des, subtipus, subtip_des)
sql = """
select codi_up, desc_tipus_via, nom_via, num_via, desc_localitat, codi_postal, latitud, longitud
FROM dwdimics.DIM_UP_CATSALUT
"""
ups = []
for up, tipus, nom, num, loc, cp, lat, lon in u.getAll(sql, 'exadata'):
    if up in dbc:
        ups.append(up)
        t = (up, dbc[up][0], dbc[up][1], dbc[up][2], dbc[up][3], dbc[up][4], tipus + ' ' + nom + ' ' + num, loc, cp, lat, lon)
        upload.append(t) 

for up in dbc:
    if up not in ups:
        t = (up, dbc[up][0], dbc[up][1], dbc[up][2], dbc[up][3], dbc[up][4], None, None, None, None, None)
        upload.append(t)

'''for (up, up_des, abs_cod, abs_des, subtipus, subtipus_des, direccio, localitat, cp) in u.getAll(sql, 'exadata'):
    upload.append((up, up_des, abs_cod, abs_des, subtipus, subtipus_des, direccio, localitat, cp))
'''
u.createTable('centres_cp', '(up varchar(5), up_des varchar(100), abs_cod varchar(3), abs_des varchar(255),\
subtip_cod varchar(10), subtip_des varchar(100), direccio varchar(255), localitat varchar(100), codi_postal varchar(20), latitud varchar(20), longitud varchar(20))', 'altres', rm=True)
u.listToTable(upload, 'centres_cp', 'altres')
'''sql = """
        SELECT
            sector,
            scs_codi,
            ics_codi,
            ics_desc
        FROM
            nodrizas.cat_centres
      """
cat_centres = {up: {"sector": sector, "br": br, "desc": ics_desc} for sector, up, br, ics_desc in u.getAll(sql, "nodrizas")}   


sql = """
        SELECT
            codi_up,
            codi_postal,
            desc_tipus_via,
            nom_via,
            num_via,
            desc_localitat
        FROM
            dwdimics.DIM_UP_CATSALUT
      """
count = 0
for up, codi_postal, tipus, nom, num, localitat in u.getAll(sql, "exadata"):
    if up in cat_centres:
        cat_centres[up]["codi_postal"] = codi_postal
        cat_centres[up]['direccio'] = tipus + ' ' + nom + ' ' + num
        cat_centres[up]['localitat'] = localitat
        count+=1
print("{}/{}".format(count, len(cat_centres)))
upload = []
for up, codis in cat_centres.items():
    upload.append((up, codis['sector'], codis['br'], codis['desc'], codis['codi_postal'], codis['direccio'], codis['localitat']))
'''