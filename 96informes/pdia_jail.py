# -*- coding: utf-8 -*-

"""
0 7,15 * * * bash -c /home/sisap/covid/pdia.sh
"""

import datetime as d

import sisapUtils as u

TODAY = d.datetime.now().date()


class presons_pdia(object):
    """."""

    def __init__(self):
        """."""
        self.get_poblacio_hist()
        self.get_proves()
        self.export_llistat()

    def get_poblacio_hist(self):
        """ . """

        self.poblacio_hist = {}
        sql = "select a.huab_usua_cip, huab_ep_codi, huab_up_codi, \
                      huab_uab_codi, huab_data_ass, huab_data_final \
                from vistb050 a \
                inner join \
                    (select huab_usua_cip, max(huab_data_final) as datmax \
                    from vistb050 \
                    where huab_data_final>=to_date('2020-03-01','YYYY-MM-DD') \
                          and huab_usua_cip not in \
                          (select huab_usua_cip from vistb050 \
                           where huab_data_final is null) \
                    group by huab_usua_cip) b \
                on a.huab_usua_cip=b.huab_usua_cip \
                   and a.huab_data_final = b.datmax \
                UNION \
                select huab_usua_cip, huab_ep_codi, \
                       huab_up_codi, huab_uab_codi, \
                       huab_data_ass, huab_data_final \
                from vistb050 \
                where huab_data_final is null"
        for cip, ep, up, uba, dat_ass, dat_fin in u.getAll(sql, '6951'):
            self.poblacio_hist[cip] = {'ep': ep,
                                       'up': up,
                                       'uba': uba,
                                       'dat_ass': dat_ass,
                                       'dat_fin': dat_fin}
        print(len(self.poblacio_hist))

    def get_proves(self):
        """."""
        sql = """
        select cip, prova, entorn, motiu, data, resultat_des
        from preduffa.sisap_covid_pac_prv_raw
        """
        self.llistat = []
        for cip, prv, entorn, mprv, dprv, res in u.getAll(sql, "redics"):  # noqa
            if cip in self.poblacio_hist:
                uppac = self.poblacio_hist[cip]['up']
                uba = self.poblacio_hist[cip]['uba']
                dat_ass = self.poblacio_hist[cip]['dat_ass'].date()
                dat_fin = self.poblacio_hist[cip]['dat_fin'].date() if self.poblacio_hist[cip]['dat_fin'] is not None else None  # noqa
                dprv = dprv.date()
                this = (cip, uppac, uba, dat_ass, dat_fin, prv, entorn, mprv, dprv, res)  # noqa
                self.llistat.append(this)
        print(len(self.llistat))

    def export_llistat(self):
        """."""
        print("----------------------------------------------- export_llistat")
        file = u.tempFolder + 'pdia_presons_{}.csv'
        file = file.format(TODAY.strftime('%Y%m%d'))
        u.writeCSV(file,
                   [('CIP', 'Up','Uba', 'Data Ass', 'Data Final', 'Prova', 'Entorn prova', 'Motiu prova', 'Data prova', 'Resultat prova')] + self.llistat,  # noqa
                   sep=";")
        # print("Send Mail")
        # subject = 'Imserso - Fitxa 1a'
        # text = 'Adjuntem arxiu amb les dades per la 1a fitxa'
        # u.sendGeneral('SISAP <sisap@gencat.cat>',
        #               'avilar@gencat.cat',
        #               'ehermosilla@idiapjgol.info',
        #               subject,
        #               text,
        #               file)
        # remove(file)

    # def upload_sms(self):
    #     """."""
    #     with u.SFTP("sisap") as sftp:
    #         for key, values in self.sms.items():
    #             file = "sms_{}_{}.csv".format(key, SMS_DIA.strftime("%Y_%m_%d"))  # noqa
    #             u.TextFile(file).write_iterable(sorted(values), ";", "\n")
    #             sftp.put(file, "sms/{}".format(file))
    #             os.remove(file)


if __name__ == "__main__":
    presons_pdia()
