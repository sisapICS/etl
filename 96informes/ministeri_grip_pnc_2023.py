# executat contra bd mensual febrer'24

import collections as c

import sisaptools as u


REFERENCIA = 2023
TAULA = "MINISTERI_VACUNES_{}".format(REFERENCIA)


sql = "select vacuna, antigen from cat_prstb040_new \
       where antigen in ('A-GRIP', 'A00033', 'A00046', 'A00028', \
                         'A00027', 'A00048')"
codis = {}
grip = set()
neumo = set()
for codi, antigen in u.Database("p2262", "import").get_all(sql):
    vacuna = "GRIP" if antigen in ("A-GRIP", "A00033") \
             else "VNP" if antigen in ("A00046", "A00028") \
             else "VNC" if antigen in ("A00027", "A00048") \
             else "error"
    codis[codi] = vacuna
    if vacuna == "GRIP":
        grip.add(codi)
    else:
        neumo.add(codi)
vacunes = c.defaultdict(set)
sql = "select id_cip_sec, va_u_cod from import.vacunes \
       where (va_u_cod in {0} or \
              (va_u_cod in {1} and year(va_u_data_vac) between \
                                                    {2} and {2} + 1)) and \
             va_u_data_baixa = 0".format(tuple(neumo), tuple(grip), REFERENCIA)
for id, codi in u.Database("p2262", "import").get_all(sql):
    vacunes[id].add(codis[codi])


sql = "select id_cip_sec, data_naix from assignada_tot"
poblacio = {id: naix.year for id, naix
            in u.Database("p2262", "nodrizas").get_all(sql)}


resultat = c.defaultdict(c.Counter)
sql = "select id_cip_sec, ps, dde from eqa_problemes where ps in (18, 24, 666)"
for id, agr, dde in u.Database("p2262", "nodrizas").get_all(sql):
    if id in poblacio and dde.year <= REFERENCIA:
        keys = set()
        naix = poblacio[id]
        edat = REFERENCIA - naix
        ps = "VIH" if agr == 666 else "DM"
        if 0 <= edat <= 14:
            keys.add((ps, "0-14"))
        elif 15 <= edat <= 64:
            keys.add((ps, "15-64"))
        else:
            keys.add((ps, "65+"))
        if 2 <= edat <= 14 and ps == "VIH":
            keys.add((ps, "2-14"))
        for key in keys:
            resultat[key]["DEN"] += 1
            for vac in ("GRIP", "VNP", "VNC"):
                if vac in vacunes[id]:
                    resultat[key][vac] += 1
            if all(vac in vacunes[id] for vac in ("VNP", "VNC")):
                resultat[key]["VNP+VNC"] += 1


upload = []
for (ps, edat), dades in resultat.items():
    den = dades["DEN"]
    this = [(ps, edat, vac, dades[vac], den) for vac in dades if vac != "DEN"]
    upload.extend(this)
cols = ("patologia varchar2(3)", "edat varchar2(5)", "vacuna varchar2(7)",
        "numerador int", "denominador int")
with u.Database("redics", "data") as redics:
    redics.create_table(TAULA, cols, remove=True)
    redics.list_to_table(sorted(upload), TAULA)
    redics.set_grants("select", TAULA, ("PREDUPRP", "PDP"))
