# coding: utf8


from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter
from datetime import *


imp = 'import'
nod = 'nodrizas'

tabac = {}
sql = 'select id_cip_sec, tab, dlast from eqa_tabac where last=1'
for id, tab, dlast in getAll(sql, nod):
    tabac[id] = {'tab': tab, 'dlast': dlast}
  
  
upload = []
sql = "select id_cip_sec, pr_th, pr_dde, pr_dba from problemes where pr_cod_ps='Z72.0' and pr_dba in ('2017-04-07','2017-04-14','2017-03-25','2017-03-31') and pr_hist=1"
for id, th, dde, dba in getAll(sql, imp):
    tab, dlast, canvi = None, None, 0
    if id in tabac:
        tab = tabac[id]['tab']
        dlast = tabac[id]['dlast']
        if dlast > dba:
            canvi = 1
    upload.append([id, th, tab, dlast, canvi])
    
file = tempFolder + 'Tabac_z72.txt'
writeCSV(file, upload, sep=';') 
