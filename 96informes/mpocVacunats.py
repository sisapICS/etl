# coding: utf8

"""

Trello:

"""

import sisapUtils as u
import collections as c
import datetime as d

years = [2020, 2019, 2018, 2017, 2016]

class mpoc(object):
    """ """

    def __init__(self):
        """ . """

        self.resultat = c.Counter()
        for self.year in years:
            print(self.year)
            self.get_centres()
            self.get_poblaciohist()
            self.get_poblacio()
            self.get_problemes()
            self.get_cat_vacunes()
            self.get_vacunes()
            self.get_resultat()
        self.get_upload()
        self.export_upload()

    def get_centres(self):
        """ . """
        self.cat_centres = {}
        sql = """
        select
            scs_codi
            , ics_desc
            , amb_desc
            , sap_desc
        from
            cat_centres
        where
            aga in ('AGA00046','AGA00047','AGA00070','AGA00071')
        """
        for up, up_desc, amb_desc, sap_desc in u.getAll(sql, 'nodrizas'):
            self.cat_centres[up] = {'up_desc': up_desc,
                                    'amb_desc': amb_desc,
                                    'sap_desc': sap_desc}
        print("N CENTRES PILOT (UP):", len(self.cat_centres))

    def get_poblaciohist(self):
        """ . """
        self.poblaciohist = {}
        sql = """select id_cip_sec, up
                 from assignadahistorica_s{}""".format(self.year)
        for id_cip_sec, up in u.getAll(sql, 'import'):
            if up in self.cat_centres:
                self.poblaciohist[id_cip_sec] = up
        print(len(self.poblaciohist))

    def get_poblacio(self):
        """ S'utilitza assignada perque es necesita la informació històrica"""
        self.poblacio = {}
        sql = """select id_cip_sec, usua_data_naixement, usua_sexe,
                 (year('{}')-year(usua_data_naixement))-(date_format('{}','%m%d')<date_format(usua_data_naixement,'%m%d')) edat
                 from assignada""".format(d.date(self.year, 12, 31).strftime("%Y-%m-%d"),  # noqa
                                          d.date(self.year, 12, 31).strftime("%Y-%m-%d"))  # noqa
        for id_cip_sec, _dnaix, sexe, edat in u.getAll(sql, 'import'):
            if id_cip_sec in self.poblaciohist:
                up = self.poblaciohist[id_cip_sec]
                if edat > 14:
                    edatc = u.ageConverter(edat)
                    self.poblacio[id_cip_sec] = (up, sexe, edatc)
        print(len(self.poblacio))

    def get_problemes(self):
        """ . """
        self.mpoc = set()
        sql = """select id_cip_sec
                from problemes
                where pr_cod_o_ps='C'
                and pr_cod_ps in ('C01-J43.0','C01-J43.1','C01-J43.2',
                                  'C01-J43.8','C01-J43.9',
                                  'C01-J44','C01-J44.0','C01-J44.1',
                                  'C01-J44.9',
                                  'J43','J43.0','J43.1','J43.2','J43.8',
                                  'J43.9',
                                  'J44','J44.0','J44.1','J44.8','J44.9')
                    and pr_dde <= date '{}' and
                    (pr_dba = 0 or pr_dba >= date '{}')""".format(d.date(self.year, 12, 31).strftime("%Y-%m-%d"),  # noqa
                                                                  d.date(self.year, 12, 31).strftime("%Y-%m-%d"))  # noqa
        for id_cip_sec, in u.getAll(sql, 'import'):
            if id_cip_sec in self.poblacio:
                self.mpoc.add(id_cip_sec)
        print(len(self.mpoc))

    def get_cat_vacunes(self):
        """ . """
        self.cat_vacunes = {}
        sql = """select vacuna, antigen
                from cat_prstb040_new
                where antigen in ('A00027','A00028','A-GRIP')"""
        for vacuna, antigen in u.getAll(sql, 'import'):
            if antigen == 'A00027':
                vacuna_desc = 'PN13'
            if antigen == 'A00028':
                vacuna_desc = 'PN23'
            if antigen == 'A-GRIP':
                vacuna_desc = 'GRIPE'
            self.cat_vacunes[vacuna] = vacuna_desc
        # print(self.cat_vacunes)

    def get_vacunes(self):
        """ . """
        self.vacunes = c.defaultdict(set)
        sql = """select id_cip_sec, va_u_cod, va_u_data_vac
        from vacunes
        where va_u_cod in {}
        and (va_u_data_vac <= date '{}')
        and (va_u_data_baixa = 0 or va_u_data_baixa >= date '{}')""".format(tuple(self.cat_vacunes), # noqa
                                                                            d.date(self.year, 12, 31).strftime("%Y-%m-%d"),  # noqa
                                                                            d.date(self.year, 12, 31).strftime("%Y-%m-%d"))  # noqa
        for id_cip_sec, vacuna, dat in u.getAll(sql, 'import'):
            if id_cip_sec in self.mpoc:
                vacuna_desc = self.cat_vacunes[vacuna]
                if vacuna_desc in ('GRIPE'):
                    if dat >= d.date(self.year, 1, 1):
                        self.vacunes[vacuna_desc].add(id_cip_sec)
                else:
                    self.vacunes[vacuna_desc].add(id_cip_sec)

    def get_resultat(self):
        """ . """
        for id_cip_sec, (up, sexe, edat) in self.poblacio.items():
            up_desc = self.cat_centres[up]['up_desc']
            if id_cip_sec in self.mpoc:
                self.resultat[(self.year, up, up_desc, edat, sexe, 'MPOC')] += 1  # noqa
                for vacuna_desc in self.vacunes:
                    if id_cip_sec in self.vacunes[vacuna_desc]:
                        self.resultat[(self.year, up, up_desc, edat, sexe, vacuna_desc)] += 1  # noqa

    def get_upload(self):
        """" . """
        self.upload = []
        for (year, up, up_desc, edat, sexe, vacuna_desc), n in self.resultat.items():  # noqa
            if vacuna_desc == 'MPOC':
                pn13 = self.resultat[(year, up, up_desc, edat, sexe, 'PN13')]
                pn23 = self.resultat[(year, up, up_desc, edat, sexe, 'PN23')]
                gripe = self.resultat[(year, up, up_desc, edat, sexe, 'GRIPE')]
                self.upload.append((year, up, up_desc, edat, sexe, n, pn13, pn23, gripe))  # noqa

    def export_upload(self):
        """ . """
        file = u.tempFolder + 'mpocVacunats.csv'
        u.writeCSV(file,
                   [('any', 'up_codi', 'up_desc',
                     'edat', 'sexe',
                     'mpoc', 'pn13', 'pn23', 'gripe')] + self.upload,
                   sep="@")


if __name__ == "__main__":
    print(d.datetime.now())
    ts = d.datetime.now()
    mpoc()
    print('Time execution {}'.format(d.datetime.now() - ts))
