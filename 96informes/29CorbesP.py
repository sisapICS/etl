from sisapUtils import *
from collections import defaultdict
from datetime import *

printTime()

imp = 'import'
test = 'test'
table = 'variables'
dest = 'mst_corbes'
dest2 = 'mst_corbes_all_years'
agrupadors = "('TT102','TT101','TL201','TT103')"
create = "(id_cip_sec int, agrupador varchar(10), val double, dat date, naix date, sexe varchar(1), edat_a int, edat_m int, edat_s int,  edat_3d int, edat_d int)"

anysCorbes = [2013, 2014, 2015, 2016, 2017]


def getPartitions(table, db):
    partitions = {}
    sql = "select table_name from information_schema.tables where table_name like '{0}%' and engine not like ('MRG%') and table_schema='{1}'".format(table, db)
    for name, in getAll(sql, db):
        partitions[name] = True
    return partitions

def get_jobs(naixement, sexe):
    jobs = []
    jobs.extend([(table, partition, naixement, sexe) for partition in getPartitions(table, imp)])
    return jobs
    
def get_poblacio(sector):
    naixement, sexe = {}, {}
    sql = 'select id_cip_sec, usua_sexe, usua_data_naixement from assignada'
    for id, sex, naix in getAll(sql, imp):
        naixement[id] = naix
        sexe[id] = sex
    return  naixement, sexe

def process_it(params):
    table, partition, naixement, sexe = params
    sql = "select id_cip_sec, vu_cod_vs, date_format(vu_dat_act, '%Y%m%d'), vu_val from {0} where vu_cod_vs in {1}".format(partition, agrupadors)
    process_nens(sql, naixement, sexe, dest, dest2)

def process_nens(sql, naixement, sexe, dest, dest2):
    data = []
    data2 = []
    for id, agr, dat, val in getAll(sql, imp):
        try:
            naix = naixement[id]
        except KeyError:
            naix = date(0001,1,1)
        try:
            sex  = sexe[id]
        except KeyError:
            sex = '9'
        _dat = datetime.strptime(dat, '%Y%m%d').date()
        if _dat >= naix:
            anys = yearsBetween(naix, _dat)
            mesos = monthsBetween(naix, _dat)
            dies = daysBetween(naix, _dat)
            setmanes = int(daysBetween(naix, _dat)/7)
            tresDies = int(daysBetween(naix, _dat)/3)
            if anys < 20:
                data2.append([id, agr, val, dat, naix, sex, anys, mesos, setmanes, tresDies, dies])
                if _dat.year in anysCorbes:
                    data.append([id, agr, val, dat, naix, sex, anys, mesos, setmanes, tresDies, dies])
    listToTable(data, dest, test)
    listToTable(data2, dest2, test)
    
if __name__ == '__main__':
    naixement, sexe = get_poblacio(sectors)
    createTable(dest,create,test, rm=True)
    createTable(dest2,create,test, rm=True)
    jobs = get_jobs(naixement, sexe)
    for partition in getPartitions(table, imp):
        sql = "select id_cip_sec, vu_cod_vs, date_format(vu_dat_act, '%Y%m%d'), vu_val from {0} where vu_cod_vs in {1}".format(partition, agrupadors)
        process_nens(sql, naixement, sexe, dest, dest2)

printTime()