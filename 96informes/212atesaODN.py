# coding: utf8

"""
Població atesa per odonto
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

DEBUG = False

etiquetes = "('RODN', 'RODON')"
moduls = "( 'RODN', 'RODN1', 'RODN2', 'RODN3', 'RODN4', 'RODN5', 'RODON', 'RODON1', 'RODON2', 'RODON3', 'RODON4', 'RODON5')"


imp = "import"

class atesa_odn(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_escolars()
        self.get_visites()
        self.get_pob()
        self.export_data()
        
    def get_centres(self):
        """EAP ICS"""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres where ep='0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

    def get_escolars(self):
        """
        Obtenim dades de les revisions escolars segons el que ens va dir A Juvany
        """
        self.rev_escolars = {}
        sql = "select id_cip_sec, date_format(ro_data,'%Y%m%d') from odn510 where ro_lloc='E'"
        for id, dat in u.getAll(sql, imp):
            self.rev_escolars[(id, dat)] = True
    
    def get_visites(self):
        """."""
        self.visites = Counter()
        sql = "select id_cip_sec, \
                      visi_up, \
                      date_format(visi_data_visita,'%Y%m%d'), \
                      if(visi_modul_codi_modul in {} or visi_etiqueta in {}, 1, 0) \
               from visites1 \
               where visi_situacio_visita = 'R' and \
                     s_espe_codi_especialitat in ('10106','10777')".format( moduls, etiquetes)
        for id, up, dat, escolar in u.getAll(sql, imp):
            escolar = 1 if (id, dat) in self.rev_escolars else escolar
            if escolar != 1:
                self.visites[id] += 1
    
       
    def get_pob(self):
        """Obtenim pob atesa odn"""
        self.pob = Counter()
        sql = "select id_cip_sec, up, edat, sexe from assignada_tot"
        for id, up, edat, sexe in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                br = self.centres[up][0]
                visita_odn = 1 if (id) in self.visites else 0
                self.pob[(br, u.ageConverter(edat), u.sexConverter(sexe), 'den')] += 1
                self.pob[(br, u.ageConverter(edat), u.sexConverter(sexe), 'num')] += visita_odn
        
             
    def export_data(self):
        """."""
        self.upload = []
        for (br, edat, sexe, tip), n in self.pob.items():
            if tip == 'den':
                num = self.pob[(br, edat, sexe, 'num')]
                self.upload.append([br, edat, sexe, num, n])
                
        u.writeCSV(u.tempFolder + 'atesa_odn.txt', self.upload)  
   
if __name__ == '__main__':
    atesa_odn()
    