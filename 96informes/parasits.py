# coding: iso-8859-1
from sisapUtils import *
import random
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

nod="nodrizas"
imp="import"

periodos=(2015,2016,2017)
codis=('P010F5', 'P01524', 'P01295', 'P01194', 'P01294', 'P01195', 'P01224', 'P02824', 'P02724', 'P01124', 'P01024', 'P00524', 'P01800', 'P02524', 'P02424', 'M019F0')

def get_id_edat():
    """Asocia a cada id en nodrizas.assignada, el grupo de edad (si es ped o adl) en el momento de extraccion
       Devuelve un diccionario {id:grupo de edat} 
    """
    sql = "select id_cip_sec,  if((year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')) < 14,'ped','adl')  from {}.assignada, {}.dextraccio".format(imp,nod)
    id_edat=defaultdict()
    for id,edat in getAll(sql, imp):
        id_edat[id]=edat       
    return id_edat

def get_codis_desc():
    """Asocia a cada codigo de laboratorio de parasitos su descripcion
       Devuelve un diccionario {codi:desc}
    """
    sql="select CPR_CODI_PROVA,CPR_DESC_ABR FROM labtb120 WHERE CPR_CODI_PROVA in {}".format(codis)
    return {codi:desc for codi,desc in getAll(sql,"redics")}

def get_up_ambit():
    sql="select scs_codi,amb_desc from {}.cat_centres where ep = '0208'".format(nod)
    return {up:amb_desc for up,amb_desc in getAll(sql,nod)}

def get_tables_year(year):
    """Obtiene las particiones de laboratorio en las que el nombre tiene el any year en cuestion.
       Devuelve una list de tablas.
    """
    tables = []
    for table in getSubTables("laboratori"):
        if str(year) in table:
            tables.append(table)
    printTime("Tables")
    return tables

def get_parasits(table,up_ambit,id_edat):
    """ Obtiene de una particion de laboratorio, table, el numero de pruebas y las agrupa por ambito (del ics), codigo de prueba y grupo de edat (ped/adl)
        Devuelve un diccionario {(amb,codi_prova,grupo_edat): n}
    """
    sql='select codi_up,id_cip_sec,cr_codi_prova_ics from {} where cr_codi_prova_ics in {}'.format(table,codis)
    parasits=Counter()
    for up,id, codi_prova in getAll(sql,imp):
        if up in up_ambit:
            if id in id_edat:
                parasits[(up_ambit[up],codi_prova,id_edat[id])]+=1
    return parasits

def get_pob_ass_ates(year,up_ambit):
    """Obtiene la poblacion assignada y atendida de la tabla assignadahistorica segun el any year para las up del ics y agrupa por ambito.
       Devuelve dos diccionarios,
        -> {amb: pob_ass}
        -> {amb: pob_ates}
    """
    sql="select up, id_cip_sec,ates from assignadahistorica where dataany= {} ".format(year)
    pob_ass=Counter()
    pob_ates=Counter()
    for up,id,ates in getAll(sql,imp):
        if up in up_ambit:
            amb=up_ambit[up]
            pob_ass[amb]+=1
            if ates == 1:
                pob_ates[amb]+=1 
    return pob_ass,pob_ates
    

def export_txt_file(name,header,rows):
    with open(name,"wb") as out_file:
        print(rows[0])
        row_format ="{}\t" * len(rows[0])+"\n"
        out_file.write(row_format.format(*header))
        for row in rows:
            out_file.write(row_format.format(*row))





if __name__ == '__main__':

    printTime('inici')
    codi_desc=get_codis_desc()
    printTime('codis desc')
    up_ambit=get_up_ambit()
    print(len(up_ambit))
    id_edat=get_id_edat()
    printTime("id_edat")

    rows_final=[]
    for year in periodos:
        tables=get_tables_year(year)
        printTime(tables)
        parasits_total=Counter()
        for table in tables:
            parasits= get_parasits(table,up_ambit,id_edat)
            printTime("{},{}".format(table,len(parasits)))
            for key in parasits:
                parasits_total[key] += parasits[key]
        pob_ass,pob_ates=get_pob_ass_ates(year,up_ambit)
    
        rows_final.extend([(year,amb, codi_desc[codi_lab], edat, n,pob_ass[amb],pob_ates[amb]) for ((amb, codi_lab, edat), n) in parasits_total.iteritems()])
        printTime("ROWS")
    
    header=["PERIODO","AMB","CODIGO","EDAT","N","POB ASSIGNADA","POB ATESA"]
    export_txt_file("parasits.txt",header,rows_final)
    
    