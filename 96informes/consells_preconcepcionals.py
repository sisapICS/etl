# -*- coding: utf-8 -*-

import sisapUtils as u
import collections as c

class CONSELLS_PRECONCEPCIONALS():
    def __init__(self):
        self.poblacio()
        self.embarassos()
        self.consells()
        self.numeradors()
        self.upload()

    def poblacio(self):
        print('pob')
        self.pop = {}
        sql = """select id_cip_sec, visi_up from nodrizas.ass_imputacio_up"""
        for id, up in u.getAll(sql, 'nodrizas'):
            self.pop[id] = up

    def embarassos(self):
        print('embara')
        self.embarassades = {}
        sql = """select id_cip_sec, inici
                from nodrizas.ass_embaras 
                where year(inici) = 2021 or year(fi) = 2021"""
        for id, inici in u.getAll(sql, 'nodrizas'):
            self.embarassades[id] = inici
    
    def consells(self):
        print('consells')
        self.consells = {}
        self.num0 = c.Counter()
        sql = """select id_cip_sec, xml_data_alta, camp_valor
                from import.xml_detall
                where xml_origen = 'VARIABLES'
                and year(xml_data_alta) = 2021
                and xml_tipus_orig = 'CW2003'
                and camp_codi = '4'"""
        for id, data, valor in u.getAll(sql, 'import'):
            if id not in self.embarassades:
                self.consells[id] = data
                if id in self.pop: self.num0[self.pop[id], valor] += 1
            elif self.embarassades[id] > data:
                self.consells[id] = data
                if id in self.pop: self.num0[self.pop[id], valor] += 1
    
    def numeradors(self):
        print('numeradors')
        self.numA = {}
        self.numB = {}
        self.numC = {}
        sql = """select id_cip, au_val, au_cod_ac, au_dat_act, au_up 
                from import.activitats2
                where year(au_dat_act) = 2021
                and au_cod_ac in ('ATA2','CP201','P2901')"""
        for cip, val, cod, data, up in u.getAll(sql, 'import'):
            if cip in self.consells and cip in self.pop:
                if cod == 'ATA2':
                    self.numA[cip] = [val, data]
                elif cod == 'CP201':
                    self.numB[cip] = [val, data]
                elif cod == 'P2901':
                    self.numC[cip] = [val, data]
    
    def upload(self):
        print('upload')
        self.den = c.Counter()
        for e in self.consells:
            try:
                self.den[self.pop[e]] += 1
            except:
                pass
        self.numsA, self.numsB, self.numsC = c.Counter(), c.Counter(), c.Counter()
        print('abc 1')
        for e in self.numA:
            try:
                self.numsA[(self.pop[e], self.numA[e][0])] += 1
            except:
                pass
        for e in self.numB:
            try:
                self.numsB[(self.pop[e], self.numB[e][0])] += 1
            except:
                pass
        for e in self.numC:
            try:
                self.numsC[(self.pop[e], self.numC[e][0])] += 1
            except:
                pass
        upload = []
        print('abc 2')
        for e in self.numsA: 
            up = e[0]
            value = e[1]
            num = self.numsA[e]
            den = self.den[up]
            upload.append(('A', up, value, num, den))
        for e in self.numsB: 
            up = e[0]
            value = e[1]
            num = self.numsB[e]
            den = self.den[up]
            upload.append(('B', up, value, num, den))
        for e in self.numsC: 
            up = e[0]
            value = e[1]
            num = self.numsC[e]
            den = self.den[up]
            upload.append(('C', up, value, num, den))
        print('indicador 0')
        for e in self.num0:
            up = e[0]
            value = e[1]
            num = self.num0[e]
            den = self.den[up]
            upload.append(('0', up, value, num, den))

        print('create')
        cols = """(ind varchar(1), up varchar(5), valor varchar(20),
                        num int, den int)"""
        u.createTable('consells_preconc', cols, 'test', rm=True)
        u.listToTable(upload, 'consells_preconc', 'test')
            
            
        
if __name__ == "__main__":
        CONSELLS_PRECONCEPCIONALS()    