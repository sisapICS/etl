# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

"""
Copiat directament de procés Alícia del 2016 i adaptat a 2017 i semipythonitzat
"""

nod = 'nodrizas'
imp = 'import'

descproblemes = {}
sql = 'select ps_cod,ps_des from cat_prstb001'
for ps,des in getAll(sql, imp):
    descproblemes[ps] = des
    
labtb203 = defaultdict(list)
dic_orienta = {}
sql = "SELECT PPP_CODI_PROBLEMA, PPP_ORIENTACIO, PPP_NUMPET, OC_NUMID \
    FROM LABTB203 \
        WHERE OC_UP_ORI in  ('00055','00054','00044')"
for problema, orientacio, numpet, numid in getAll(sql, 'redics'):
    labtb203[numid].append(problema)
    dic_orienta[problema]=orientacio

derivacions = {}
sql ="SELECT GPITB104.OC_CENTRE_ORI, GPITB104.OC_DATA, GPITB104.OC_COLLEGIAT, GPITB104.OC_MOTIU, GPITB104.OC_MOTIU2, GPITB104.OC_NUMID, \
    GPITB004.INF_SERVEI_D, GPITB004.INF_SERVEI_D_CODI, GPITB004.INF_NUMID, GPITB004.INF_DATA, GPITB004.INF_CODI_PROVA, GPITB004.INF_DES_PROVA, GPITB004.INF_PRIORITAT, GPITB104.OC_UP_ORI \
     FROM GPITB104, GPITB004 \
     WHERE (GPITB104.OC_NUMID=GPITB004.INF_NUMID) AND (GPITB104.OC_DATA BETWEEN TO_DATE ('01/01/2017', 'dd/mm/yyyy') AND TO_DATE ('31/12/2017', 'dd/mm/yyyy'))\
            AND GPITB104.OC_UP_ORI in ('00055','00054','00044')"
            
for centre_ori, oc_data, oc_collegiat,oc_motiu,oc_motiu2, oc_numid, serveid, dcodi,numid,infdata,prova,desprova,prioritat, up in getAll(sql,'6211'):
    derivacions[numid] = {'up':up, 'a':prova, 'b': desprova, 'c': prioritat,'d':serveid,'e':dcodi,'f':oc_numid,'g':oc_data,'h':oc_collegiat,'ps1': None, 'ps1O': None,
                            'ps2': None, 'ps2O': None,'ps3': None, 'ps3O': None,'ps4': None, 'ps4O': None,'ps5': None, 'ps5O': None,'ps6': None, 'ps6O': None,'ps7': None, 'ps7O': None,'ps8': None, 'ps8O': None,'ps9': None, 'ps9O': None}
    for ps in labtb203[numid]:
        descps = dic_orienta[ps]
        ps1,ps2,ps3,ps4,ps5,ps6,ps7,ps8, ps9 = derivacions[numid]['ps1'], derivacions[numid]['ps2'], derivacions[numid]['ps3'], derivacions[numid]['ps4'], derivacions[numid]['ps5'], derivacions[numid]['ps6'], derivacions[numid]['ps7'],derivacions[numid]['ps8'],derivacions[numid]['ps9']
        if ps1 == None:
            derivacions[numid]['ps1'] = ps
            derivacions[numid]['ps1O'] = descps
        elif ps2 == None:
            derivacions[numid]['ps2'] = ps
            derivacions[numid]['ps2O'] = descps
        elif ps3 == None:
            derivacions[numid]['ps3'] = ps
            derivacions[numid]['ps3O'] = descps
        elif ps4 == None:
            derivacions[numid]['ps4'] = ps
            derivacions[numid]['ps4O'] = descps
        elif ps5 == None:
            derivacions[numid]['ps5'] = ps
            derivacions[numid]['ps5O'] = descps
        elif ps6 == None:
            derivacions[numid]['ps6'] = ps
            derivacions[numid]['ps6O'] = descps
        elif ps7 == None:
            derivacions[numid]['ps7'] = ps
            derivacions[numid]['ps7O'] = descps
        elif ps8 == None:
            derivacions[numid]['ps8'] = ps
            derivacions[numid]['ps8O'] = descps
        elif ps9 == None:
            derivacions[numid]['ps9'] = ps
            derivacions[numid]['ps9O'] = descps
        else:
            print ps, descps
upload = []
for (numid), d in derivacions.items():
    upload.append([numid, d['up'], d['a'],d['b'],d['c'],d['d'],d['e'],d['f'],d['g'],d['h'],d['ps1'],d['ps1O'],d['ps2'],d['ps2O'],d['ps3'],d['ps3O'],d['ps4'],d['ps4O'],d['ps5'],d['ps5O'],d['ps6'],d['ps6O'],d['ps7'],d['ps7O'],d['ps8'],d['ps8O'],d['ps9'],d['ps9O']])


file = tempFolder + 'Derivacions_Alt_Camp_17.txt'
writeCSV(file, upload, sep='|')