# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter


imp = 'import_jail'

nisConvertor = {}
nissos = []
sql = 'select * from preduffa.sisap_eapp_usutb040'
for hash, sector, nis, t in getAll(sql, 'redics'):
    if nis <> None:
        nisConvertor[int(nis)] = {'hash':hash}
    nissos.append([nis])
file = tempFolder + 'nissos.txt'
writeCSV(file, nissos, sep=';')

u11 = {}    
sql = 'select id_cip_sec,  hash_d from u11'
for id,  hash in getAll(sql, imp):
    u11[hash] = id
   

problems = [
        ('Psicosi', ['F29', 'F39', 'F25.9', 'F31.9', 'F20.9', 'F23.0', 'F23.1']),
        ('Esquizofrenia', ['F20', 'F20.0', 'F20.1', 'F20.2', 'F20.3', 'F20.4', 'F20.5', 'F20.6', 'F20.8', 'F20.9']),
        ('Depressio', ['F32','F32.0','F32.1','F32.2','F32.3','F32.8','F32.9','F33','F33.0','F33.1','F33.2','F33.3','F33.4','F33.8','F33.9','F41.2','F53.0','F53.9']),
        ('Limit', ['F60.3', 'res'])
    ]

codis_dx = []
malaltia = {}  
for dx, codis in problems:
    for codi in codis:
       codis_dx.append(codi)
       malaltia[codi] = dx
in_crit = tuple(codis_dx)

m_dx = {}
sql = "select id_cip_sec, pr_cod_ps, pr_th \
        from problemes \
        where pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= '2016/12/31' and (pr_data_baixa is null or pr_data_baixa > '2016/12/31') and (pr_dba is null or pr_dba > '2016/12/31') \
        and pr_cod_ps in {}".format(in_crit)
for id, ps, th in getAll(sql, imp):
    ok = 0
    if ps == 'F60.3':
        if th in ('2088', '5674'):
            ok = 1
    elif ps == 'F53.0':
        if th in ('5412', '2082'):
            ok = 1
    else:
        ok = 1
    desc = malaltia[ps]
    if ok == 1:
        m_dx[(id, desc)] = True
        if ps == 'F20.9':
            m_dx[(id, 'Psicosi')] = True
            m_dx[(id, 'Esquizofrenia')] = True

#ens passen pacients que han estat al modul pq no podem diferenciar els que estan molt o poc temps


ups = {}
sql = "select id_cip_sec, huab_up_codi, huab_data_ass from moviments where huab_data_ass <='2016/12/31'"
for id,modul, inici in getAll(sql, imp):
    if id in ups:
        data2 = ups[id]['data']
        if inici> data2:
            ups[id]['data'] = inici
            ups[id]['up'] = modul
    else:
        ups[id] = {'up': modul, 'data': inici}

pacients = {}    
with open('103DERT_NIS.txt', 'rb') as file:
    p=csv.reader(file, delimiter='@', quotechar='|')
    for nis in p:
        try:
            hash = nisConvertor[int(nis[0])]['hash']
            id = u11[hash]
        except KeyError:
            continue
        try:
            up = ups[id]['up']
        except KeyError:
            up = 'Sense UP al 2016'
            print id
        psicosi, esquizo, depre, limit = 0, 0, 0, 0
        if (id, 'Psicosi') in m_dx:
            psicosi = 1
        if (id, 'Esquizofrenia') in m_dx:
            esquizo = 1
        if (id, 'Depressio') in m_dx:
            depre = 1
        if (id, 'Limit') in m_dx:
            limit = 1
        pacients[(id)] = {'up': up, 'psicosi': psicosi, 'esquizo': esquizo, 'depre': depre, 'limit': limit}
            
recomptes = Counter()
upload = []

for (id), valors in pacients.items():
    recomptes[(valors['up'],valors['psicosi'], valors['esquizo'], valors['depre'], valors['limit'])] += 1

for (up, psico, esquizo, depre, limit), d in recomptes.items():
    upload.append([up, psico, esquizo, depre, limit, d])
file = tempFolder + 'DERT_EAPP.txt'
writeCSV(file, upload, sep=';')
