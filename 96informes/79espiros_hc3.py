from sisapUtils import getAll, createTable, listToTable, grantSelect


tb = 'sisap_espiros_hc3'
db = 'redics'


id_to_hash = {id: (hash, sector) for (id, hash, sector) in getAll('select id_cip_sec, hash_d, codi_sector from u11', 'import')}
resultat = [id_to_hash[id] for id, in getAll("select id_cip_sec from mst_indicadors_pacient where indicador = 'IT23' and num = 0", 'catsalut')]

createTable(tb, '(usua_cip varchar2(40), codi_sector varchar2(4))', db, rm=True)
listToTable(resultat, tb, db)
grantSelect(tb, ('PREDUMMP', 'REDICS'), db)
