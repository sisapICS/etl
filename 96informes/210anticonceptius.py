# coding: latin1

"""
M�todes anticonceptius any 2018, dones <50 anys
"""

import sisapUtils as u


class Anticonceptius(object):
    """."""

    def __init__(self):
        """."""
        self.get_ids()
        self.get_id_max_data()
        self.get_variables()
        self.taula_anticonc()
        self.taula_final()
        self.export_taula()
        
    def get_ids(self):
        """."""
        self.ids = {}
        sql = "select id_cip_sec, edat from assignada_tot where sexe = 'D' \
               and edat <50"
        for id, edat in u.getAll(sql, "nodrizas"):
			self.ids[id] = edat
                
    def get_id_max_data(self):
        """."""
        self.id_max_data = {}
        
        sql = "select id_cip_sec, max(xml_data_alta) from xml_detall \
               where xml_tipus_orig = 'EW2003' and year(xml_data_alta) = 2018 \
               group by id_cip_sec"
        for id, max_data in u.getAll(sql, "import"):
            if id in self.ids:
                self.id_max_data[id] = max_data
        
    def get_variables(self):
        """."""
        self.variables = {}
        self.recompte_var = {}
        sql = "select id_cip_sec, xml_data_alta, camp_codi \
               from xml_detall where xml_tipus_orig = 'EW2003' and \
               year(xml_data_alta) = 2018 and camp_valor = 1"
        for id, data, camp_codi in u.getAll(sql, "import"):
            if id in self.ids and data == self.id_max_data[id]:
                    self.variables[id, data, camp_codi] = camp_codi
         
        for key in self.variables: 
            if self.variables[key] not in self.recompte_var:
                self.recompte_var[self.variables[key]] = 1
            else:
                self.recompte_var[self.variables[key]] += 1

    def taula_anticonc(self):
        """."""
        self.taula_anticonc = {'0': 'No utilitza cap m�tode comentat',\
                               '1': 'Preservatiu mascul�',\
                               '2': 'Preservatiu femen�',\
                               '3': 'AO combinada',\
                               '4': 'AO gestagen',\
                               '5': 'A injectable gestagen',\
                               '6': 'Implants',\
                               '7': 'Anell vaginal',\
                               '8': 'Pegats',\
                               '9': 'DIU',\
                               '10': 'DIU LNG',\
                               '11': 'Diafragma',\
                               '12': 'Espermicides',\
                               '13': 'Esterilitzaci� tub�rica',\
                               '14': 'Vasectomia',\
                               '15': 'Coitus interruptus',\
                               '16': 'Ogino',\
                               '17': 'Billing',\
                               '18': 'Simptot�rmic',\
                               '19': 'MELA (m�tode amenorrea de lact�ncia)',\
                               '20': 'No precisa'}
    
    def taula_final(self):
        """."""      
        self.taula_final = {}
        for id in self.recompte_var:
            self.taula_final[id] = (self.taula_anticonc[id], \
                                    self.recompte_var[id])
    def export_taula(self):
        """."""
        upload = self.taula_final.values()
        u.writeCSV(u.tempFolder + "anticonc.csv", upload, sep=";")
      
if __name__ == "__main__":
    Anticonceptius()
