import collections

import sisapUtils as u


imp = 'import'
nod = 'nodrizas'
exp_db = 'redics'
exp_tb = 'sisap_atdom'


class ATDOM(object):

    def __init__(self):
        self.get_centres()
        self.get_atdom()
        self.get_laboratori()
        self.get_serveis()
        self.get_tipus()        
        self.get_visites()
        self.get_especialitats()
        self.get_virtuals()
        self.get_poblacio()
        self.get_ambulancia()
        self.get_derivacio()
        self.get_piic()
        self.get_hash()
        self.get_exploracio()
        self.upload_exploracio()

    def get_centres(self):
        sql = 'select scs_codi, ics_codi, \
                amb_desc, sap_desc, ics_desc \
                from cat_centres'
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, nod)}

    def get_atdom(self):
        sql = 'select id_cip_sec from eqa_problemes \
               where ps = 45 and edat > 14'
        self.atdom = set((id for id, in u.getAll(sql, nod)))

    def get_laboratori(self):
        # create = u.getOne('show create table laboratori', imp)[1]
        # tables = create.split('UNION=(')[1][:-1].split(',')
        # raw = u.multiprocess(self.get_laboratori_worker, tables)
        # self.laboratori = set.union(*raw)
        sql = "select id_cip_sec \
               from laboratori \
               where year(cr_data_reg)=2019"
        self.laboratori = set()
        for id, in u.getAll(sql, imp):
            if id in self.atdom:
                self.laboratori.add(id)

    # def get_laboratori_worker(self, taula):
    #     sql = "select id_cip_sec \
    #            from {} \
    #            where cr_data_reg between \
    #             date_add(date_sub(to_date('31,12,2019','DD,MM,YYYY'), interval 1 year), interval 1 day) \
    #             and to_date('31,12,2019','DD,MM,YYYY')".format(taula)
    #     laboratori = set()
    #     for id, in u.getAll(sql, imp):
    #         if id in self.atdom:
    #             laboratori.add(id)
    #     return laboratori

    def get_serveis(self):
        serveis = {'MG': 'MG', 'INF': 'INF', 'INFP': 'INF', 'TS': 'TS'}
        sql = 'select codi_sector, s_codi_servei, s_codi_servei_hom \
               from cat_pritb103'
        self.serveis = {row[:2]: (serveis[row[2]] if row[2] in serveis
                                  else 'ALTRE')
                        for row in u.getAll(sql, imp)}

    def get_tipus(self):
        self.tipus = {'9C': 'CENTRE', '9R': 'CENTRE',
                      '9D': 'DOMI', '9T': 'TLF'}

    def get_visites(self):
        # create = u.getOne('show create table visites', imp)[1]
        # tables = create.split('UNION=(')[1][:-1].split(',')
        # raw = u.multiprocess(self.get_visites_worker, tables)
        # self.get_serveis()
        # self.get_tipus()
        # self.visites = collections.defaultdict(collections.Counter)
        # for taula in raw:
        #     for id, sector, servei, tipus, relacio in taula:
        #         servei_hom = self.serveis.get((sector, servei), 'ALTRE')
        #         tipus_hom = self.tipus.get(tipus, 'ALTRE')
        #         self.visites[id][(servei_hom, tipus_hom)] += 1
        #         if relacio == '1':
        #             self.visites[id][('UBA', tipus_hom)] += 1
        self.visites = collections.defaultdict(collections.Counter)
        sql = "select id_cip_sec, codi_sector, visi_servei_codi_servei, \
               visi_tipus_visita, visi_rel_proveidor \
               from visites \
               where year(visi_data_visita) = 2019 \
               and visi_situacio_visita = 'R'"
        for id, sector, servei, tipus, relacio in u.getAll(sql, imp):
            if id in self.atdom:
                servei_hom = self.serveis.get((sector, servei), 'ALTRE')
                tipus_hom = self.tipus.get(tipus, 'ALTRE')
                self.visites[id][(servei_hom, tipus_hom)] += 1
                if relacio == '1':
                    self.visites[id][('UBA', tipus_hom)] += 1

    # def get_visites_worker(self, taula):
    #     sql = "select id_cip_sec, codi_sector, visi_servei_codi_servei, \
    #            visi_tipus_visita, visi_rel_proveidor \
    #            from {} \
    #            where visi_data_visita between \
    #             date_add(date_sub(to_date('31,12,2019','DD,MM,YYYY'), interval 1 year), interval 1 day) \
    #             and to_date('31,12,2019','DD,MM,YYYY') \
    #            and visi_situacio_visita = 'R' limit 10".format(taula)
    #     visites = [row for row in u.getAll(sql, imp) if row[0] in self.atdom]
    #     return visites

    def get_especialitats(self):
        self.especialitats = {'10999': 'MG', '10117': 'MG',
                              '10113': 'MG', '10116': 'MG',
                              '30999': 'INF', '05999': 'TS'}

    def get_virtuals(self):
        codi = '44'
        sql = "select id_cip_sec, cp_ecap_categoria \
               from cmbdap \
               where year(cp_ecap_dvis) = 2019 \
               and cp_t_act = '{}'".format(codi)
        for id, espe in u.getAll(sql, imp):
            espe_hom = self.especialitats.get(espe, 'ALTRE')
            self.visites[id][(espe_hom, 'VIRTUAL')] += 1

    def get_poblacio(self):
        sql = 'select id_cip_sec, up, uba, ubainf, \
               ates, institucionalitzat, edat, sexe, pcc, maca \
               from assignada_tot'
        self.poblacio = {row[0]: row[1:] for row in u.getAll(sql, nod)
                         if row[0] in self.atdom}

    def get_ambulancia(self):
        codis = ('TN00002', 'TN00003')
        sql = "select id_cip_sec from nod_oc_altres \
               where year(oc_data) = 2019 \
               and inf_codi_prova in {}".format(codis)
        self.ambulancia = set((id for id, in u.getAll(sql, nod)))

    def get_derivacio(self):
        sql = "select id_cip_sec from nod_derivacions \
               where year(oc_data) = 2019"
        self.derivacio = set((id for id, in u.getAll(sql, nod)))

    def get_piic(self):
        codis = ('T410101401', 'T4101402', 'T410101403', 'T410101404',
                 'T410101501', 'T4101502', 'T410101503', 'T410101504',
                 'T4101001')
        sql = 'select id_cip_sec, mi_cod_var \
               from piic \
               where mi_cod_var in {} \
               and mi_ddb is null'.format(codis)
        self.piic = set()
        piic = collections.defaultdict(lambda: collections.defaultdict(set))
        for id, var in u.getAll(sql, imp):
            if id in self.atdom:
                if var == 'T4101001':
                    self.piic.add(id)
                else:
                    piic[id][var[-2:]].add(var)
        for id, dades in piic.items():
            if any([len(n) == 2 for codi, n in dades.items()]):
                self.piic.add(id)

    def get_hash(self):
        sql = 'select id_cip_sec, hash_d, codi_sector from u11'
        self.hash = {id: '{}:{}'.format(hash, sector)
                     for (id, hash, sector) in u.getAll(sql, imp)
                     if id in self.atdom}

    def get_exploracio(self):
        self.exploracio = []
        for id in self.atdom:
            if id in self.poblacio:
                up, uba, ubainf, ates, instit, \
                 edat, sexe, pcc, maca = self.poblacio[id]
                br, ambit, sap, eap = self.centres[up]
                visites = self.visites[id] if id in self.visites else {}
                ambulancia = 1 * (id in self.ambulancia)
                derivacio = 1 * (id in self.derivacio)
                laboratori = 1 * (id in self.laboratori)
                piic = 1 * (id in self.piic)
                self.exploracio.append((self.hash[id], br,
                                        ambit, sap, eap,
                                        uba, ubainf,
                                        edat, sexe, ates, instit,
                                        pcc, maca,
                                        visites.get(('MG', 'CENTRE'), 0),
                                        visites.get(('MG', 'DOMI'), 0),
                                        visites.get(('MG', 'TLF'), 0),
                                        visites.get(('MG', 'ALTRE'), 0),
                                        visites.get(('MG', 'VIRTUAL'), 0),
                                        visites.get(('INF', 'CENTRE'), 0),
                                        visites.get(('INF', 'DOMI'), 0),
                                        visites.get(('INF', 'TLF'), 0),
                                        visites.get(('INF', 'ALTRE'), 0),
                                        visites.get(('INF', 'VIRTUAL'), 0),
                                        visites.get(('UBA', 'CENTRE'), 0),
                                        visites.get(('UBA', 'DOMI'), 0),
                                        visites.get(('UBA', 'TLF'), 0),
                                        visites.get(('UBA', 'ALTRE'), 0),
                                        visites.get(('TS', 'CENTRE'), 0),
                                        visites.get(('TS', 'DOMI'), 0),
                                        visites.get(('TS', 'TLF'), 0),
                                        visites.get(('TS', 'ALTRE'), 0),
                                        visites.get(('TS', 'VIRTUAL'), 0),
                                        visites.get(('ALTRE', 'CENTRE'), 0),
                                        visites.get(('ALTRE', 'DOMI'), 0),
                                        visites.get(('ALTRE', 'TLF'), 0),
                                        visites.get(('ALTRE', 'ALTRE'), 0),
                                        visites.get(('ALTRE', 'VIRTUAL'), 0),
                                        ambulancia, derivacio,
                                        laboratori, piic))

    def upload_exploracio(self):
        columns = '(pacient varchar(45), br varchar(5), \
                    ambit varchar(150), sap varchar(150), eap varchar(150), \
                    uba varchar(5), ubainf varchar(5), \
                    edat int, sexe varchar(1), ates int, instit int, \
                    pcc int, maca int, \
                    mf_centre int, mf_domi int, mf_tlf int, \
                    mf_altre int, mf_virtual int, \
                    inf_centre int, inf_domi int, inf_tlf int, \
                    inf_altre int, inf_virtual int, \
                    uba_centre int, uba_domi int, uba_tlf int, uba_altre int, \
                    ts_centre int, ts_domi int, ts_tlf int, \
                    ts_altre int, ts_virtual int, \
                    alt_centre int, alt_domi int, alt_tlf int, \
                    alt_altre int, alt_virtual int, \
                    ambulancia int, derivacio int, \
                    laboratori int, piic int)'
        u.createTable(exp_tb, columns, exp_db, rm=True)
        u.listToTable(self.exploracio, exp_tb, exp_db)
        u.grantSelect(exp_tb, ('PREDUPRP','PREDUEHE'), exp_db)


if __name__ == '__main__':
    ATDOM()
