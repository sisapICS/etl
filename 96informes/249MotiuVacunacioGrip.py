# -*- coding: utf-8 -*-

"""
- 2020/11/05 : Petició: https://trello.com/c/4gShAZVo

- 2020/11/16 ERROR: No encuentra la UP 00915 de assignada_tot en cat_centres.
  Es una UP de la EP 1586 CORPORACIÓ DE SALUT MARESME I SELVA.
  Pongo UP "a mano".
  Leo elimino esa UP de cat_centres por un error de Khalix

"""

import sisapUtils as u
import collections as c
import datetime as d
from os import remove

TODAY = d.datetime.now().date()

vacunes_grip = ('GRIP-N',
                'GRIP',
                'GRIP-A',
                'G(A)-4',
                'G(A)-2',
                'G(A)-3',
                'G(A)-1',
                'P-G-AR',
                'P-G-NE',
                'P-G-AD',
                'P-G-60')


class MotiuVacunacioGrip(object):
    """ . """

    def __init__(self):
        """ . """

        self.get_vacunes()
        self.get_centres()
        self.get_poblacio()
        self.get_u11()
        self.get_upload()
        self.export_upload()

    def get_vacunes(self):
        """
        cols = u.getTableColumns('prstb051','6844')
        print(cols)
        """
        print("--------------------------------------------- get_vacunes")
        ts = d.datetime.now()
        resultat = u.multiprocess(self.get_vacunes_worker, u.sectors)
        self.vacunes = {}
        for dbsector in resultat:
            for sector, hash_d, cod, dat, motiu in dbsector:
                self.vacunes[(hash_d, sector)] = {'cod': cod, 'dat': dat,
                                                  'motiu': motiu}
        print("Time execution {} / nrow {}".format(
            d.datetime.now() - ts, len(self.vacunes)))

    def get_vacunes_worker(self, sector):
        """ Worker de prstb051 """
        sql = """select
                    va_u_usua_cip,
                    va_u_cod,
                    va_u_data_vac,
                    va_u_mot_vac
               from
                    prstb051
               where va_u_cod in {}
               and va_u_data_vac >= to_date('01/09/2020','dd/mm/yyyy')
               and va_u_dosi > 0
               """.format(vacunes_grip)
        db = sector + 'af'
        converter = []
        for hash_d, cod, dat, motiu in u.getAll(sql, db):
            converter.append((sector, hash_d, cod, dat, motiu))
        print(db)
        return converter

    def get_centres(self):
        """ . """
        print("--------------------------------------------- get_centres")
        sql = "select scs_codi, ics_desc, amb_desc  from cat_centres"
        db = "nodrizas"
        self.cat_centres = {}
        for up, up_desc, amb_desc in u.getAll(sql, db):
            self.cat_centres[up] = {'up_desc': up_desc, 'amb_desc': amb_desc}

    def get_poblacio(self):
        """ . """
        print("--------------------------------------------- get_poblacio")

        sql = "select id_cip_sec, up, edat from assignada_tot"
        db = "nodrizas"
        self.poblacio = {}
        for id_cip_sec, up, edat in u.getAll(sql, db):
            self.poblacio[id_cip_sec] = {'up': up, 'edat': edat}

    def get_u11(self):
        """ . """
        print("------------------------------------------------------ get_u11")

        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        db = "import"
        self.u11 = {}
        for id_cip_sec, sector, hash_d in u.getAll(sql, db):
            self.u11[id_cip_sec] = {'sector': sector, 'hash_d': hash_d}

    def get_upload(self):
        """ . """
        print("--------------------------------------------------- get_upload")
        self.upload_agr = c.Counter()
        for id_cip_sec in self.poblacio:
            sector = self.u11[id_cip_sec]['sector']
            hash_d = self.u11[id_cip_sec]['hash_d']
            up = self.poblacio[id_cip_sec]['up']
            if up == '00915':
                amb_desc = "ÀMBIT ALIÉ A L'ICS"
                up_desc = "EAP LLORET DE MAR"
            else:
                amb_desc = self.cat_centres[up]['amb_desc']
                up_desc = self.cat_centres[up]['up_desc']
            edat = self.poblacio[id_cip_sec]['edat']
            edatc = u.ageConverter(edat)
            if edatc == 'EC01':
                edatc = 'EC0001'
            elif edatc == 'EC24':
                edatc = 'EC0204'
            elif edatc == 'EC59':
                edatc = 'EC0509'
            else:
                edatc = edatc
            motiu = self.vacunes[(hash_d, sector)]['motiu'] if (hash_d, sector) in self.vacunes else None  # noqa
            if motiu == 'AL':
                motiu = 'Altres'
            elif motiu == 'IM':
                motiu = 'Immunodepressio'
            elif motiu == 'MC':
                motiu = 'Malaltia Cronica'
            elif motiu == 'EM':
                motiu = 'Embaras'
            elif motiu == 'SA':
                motiu = 'Sanitari'
            else:
                motiu = motiu
            self.upload_agr[(amb_desc, up, up_desc, edatc, motiu)] += 1

    def export_upload(self):
        """ . """
        print("------------------------------------------------ export_upload")
        self.uploadtxt = []
        for (amb_desc, up, up_desc,
             edatc, motiu), n in self.upload_agr.items():
            self.uploadtxt.append((amb_desc, up, up_desc, edatc, motiu, n))
        file = u.tempFolder + 'MotiuVacunacioGrip_{}.csv'
        file = file.format(TODAY.strftime('%Y%m%d'))
        u.writeCSV(file,
                   [('amb_desc', 'codi_up', 'desc_up', 'edat', 'motiu', 'n')]
                   + self.uploadtxt,
                   sep=';')
        print("Send Mail")
        subject = 'Motius vacunació grip'
        text = 'Arxiu amb els motius de vacunació de grip'
        u.sendGeneral('SISAP <sisap@gencat.cat>',
                      ('montserrat.martinezm@gencat.cat',
                       'arene.girona.ics@gencat.cat'),
                      ('ehermosilla@idiapjgol.info', 'eduboniqueta@gmail.com'),
                      subject,
                      text,
                      file)
        remove(file)


if __name__ == "__main__":
    u.printTime(": Inici")
    MotiuVacunacioGrip()
    u.printTime(": Fi")
