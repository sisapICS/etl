from sisapUtils import *
from collections import defaultdict,Counter

visitats = {'sec':defaultdict(list),'cip':defaultdict(list)}
sql = 'select id_cip_sec,id_cip,cp_up_ecap from cmbdap,nodrizas.dextraccio where cp_ecap_dvis between date_add(date_add(data_ext,interval -1 year), interval +1 day) and data_ext'
for sec,cip,up in getAll(sql,'import'):
    if up not in visitats['sec'][sec]:
        visitats['sec'][sec].append(up)
    if up not in visitats['cip'][cip]:
        visitats['cip'][cip].append(up)
        
resultat = {'assig':Counter(),'ics':Counter(),'sec':Counter(),'up':Counter()}
sql = 'select id_cip_sec,id_cip,up from assignada_tot'
for sec,cip,up in getAll(sql,'nodrizas'):
    resultat['assig'][up] += 1
    if len(visitats['cip'][cip]) > 0:
        resultat['ics'][up] += 1
    if len(visitats['sec'][sec]) > 0:
        resultat['sec'][up] += 1
        if up in visitats['sec'][sec]:
            resultat['up'][up] += 1

export = []
for up in resultat['assig']:
    assig = resultat['assig'][up]
    ics = resultat['ics'][up]
    sec = resultat['sec'][up]
    eap = resultat['up'][up]
    export.append([up,assig,ics,sec,eap])
    
file = tempFolder + 'atesa.txt'
writeCSV(file,export)
