# coding: latin1

"""
.
"""


import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

class CoronavirusPob(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_covid()
        self.get_pob()
        self.get_taxes()
        
    def get_centres(self):
        """."""
        
        sql = ("select scs_codi, amb_desc \
                from cat_centres" , "nodrizas")
        self.centres = {up: amb for (up,amb)
                        in u.getAll(*sql)}
    def get_covid(self):
        """agafem casos sospitosos per edat"""
        self.totals =  c.Counter()
        sql = "select  edat,  data,setmanals \
                from sisap_coronavirus_corba \
                where estat = 'Possible'"
        for  edat, data, n in u.getAll(sql, 'redics'):
           self.totals[(data, edat)] += n
            
            
    def get_pob(self):
        """Poblacio per taxes"""
        self.pob = c.Counter()
        sql = "select up, edat from assignada_tot"
        for up, edat in u.getAll(sql, 'nodrizas'):
            ambit = self.centres[up]
            if 0 <= edat < 15:
                ed = 'Menors de 15 anys'
            elif 15 <= edat < 65:
                ed = 'Entre 15 i 64 anys'
            elif edat > 64:
                ed = 'Majors de 64 anys'
            self.pob[(ambit, ed)] += 1

    def get_taxes(self):
        """Taxes"""
        upload = []
        for (amb, ed), n in self.pob.items():
            upload.append([amb, ed, n])

        file = u.tempFolder + 'Taxes_global.txt'
        u.writeCSV(file, upload, sep='@')
        
        upload = []
        for (amb, ed), n in self.totals.items():
            upload.append([amb, ed, n])

        file = u.tempFolder + 'Taxes_coronavirus.txt'
        u.writeCSV(file, upload, sep='@')
        
if __name__ == '__main__':
    u.printTime("Inici")
    
    CoronavirusPob()
    
    u.printTime("Final") 