# coding: iso-8859-1


from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter


class Fragilitat(object):
    """."""
    
    def __init__(self):
        """."""
        self.get_centres()
        self.get_problemes()
        self.get_poblacio()
        self.export_data()
        
        
    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_desc from cat_centres"
        self.centres = {up: desc for (up, desc) in getAll(sql, 'nodrizas')}
        
    def get_problemes(self):
        """."""
        self.casos=defaultdict(set)
        sql = "select id_cip_sec, pr_cod_ps, year(pr_dde), if(pr_dba > 0 and pr_dba <= data_ext, 1, 0) from problemes, nodrizas.dextraccio where  pr_cod_ps in ('C01-R54', 'C01-Z73.9', 'C01-Z73.91', 'C01-Z91.81','C01-R41.81') \
                   and pr_cod_o_ps = 'C' and pr_hist = 1 and pr_dde <= data_ext and (pr_data_baixa = 0 or pr_data_baixa > data_ext)"
        for id, ps, anys, baixa in getAll(sql, "import"):
            if baixa == 0:
                self.casos[id].add(ps)

    def get_poblacio(self):
        """."""
        self.pacients =  Counter()
        sql = "select id_cip_sec, up, edat from assignada_tot where edat>14"
        for id, up, edat in getAll(sql, "nodrizas"):
            if up in self.centres:
                desc = self.centres[up]
                self.pacients[up, desc, 'denAdults'] += 1
                if id in self.casos:
                    for ps in self.casos[id]:
                        self.pacients[up, desc, ps] += 1
                if edat >= 65:
                    self.pacients[up, desc, 'den>65'] += 1
                    for ps in self.casos[id]:
                        ps2 = ps + '_65'
                        self.pacients[up, desc, ps2] += 1
    
    def export_data(self):
        """."""
        rec = []
        for (up, desc, tipus), n in self.pacients.items():
            rec.append([up, desc, tipus, n])
        writeCSV(tempFolder + 'Fragilitat_per_UP.txt', rec)
        
if __name__ == '__main__':
    Fragilitat()