# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

agrv = "('832','8321','ALCOHO','ALDIA','ALHAB','ALRIS','ALSET','ANTIAL','A-PCT','AUDIT','CAGE','CAGEC','CALCO','CP202','EDU10','EDU11','EDU25','NALCO')"
agra = "('CP201','VP201')"
eps = "('EP2001','EP2002', 'EP2003', 'EP2004', 'EP2007', 'EP2008')"


class Alcohol(object):
    """."""
    
    def __init__(self):
        """."""
        
        self.get_centres()
        self.get_variables()
        self.get_actuacions()
        self.export_data()

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres where ep='0208'"
        self.centres = {up: br for (up, br) in u.getAll(sql, 'nodrizas')}
    
    def get_variables(self):
        """."""
        self.variables = c.Counter()
        self.variablesEP = c.Counter()
        for table in u.getSubTables('variables'):
            dat, = u.getOne('select year(vu_dat_act) from {} limit 1'.format(table), 'import')
            if dat >= 2015:
                print table
                sql = "select  date_format(vu_dat_act, '%Y%m'), vu_up \
                from {}, nodrizas.dextraccio where vu_dat_act <= data_ext and vu_cod_vs in {}".format(table, agrv)
                for periode, up in u.getAll(sql, 'import'):
                    if up in self.centres:
                        self.variables[(periode)] += 1
                sql = "select  date_format(vu_dat_act, '%Y%m'), vu_up \
                from {}, nodrizas.dextraccio where vu_dat_act <= data_ext and vu_cod_vs in {}".format(table, eps)
                for periode, up in u.getAll(sql, 'import'):
                    if up in self.centres:
                        self.variablesEP[(periode)] += 1
    
    def get_actuacions(self):
        """."""
        self.actuacions = c.Counter()
        for table in u.getSubTables('activitats'):
            dat, = u.getOne('select year(au_dat_act) from {} limit 1'.format(table), 'import')
            if dat >= 2015:
                print table
                sql = "select  date_format(au_dat_act, '%Y%m'), au_up \
                from {}, nodrizas.dextraccio where au_dat_act <= data_ext and au_cod_ac in {}".format(table, agra)
                for periode, up in u.getAll(sql, 'import'):
                    if up in self.centres:
                        self.actuacions[(periode)] += 1


    def export_data(self):
        """."""
        upload = [] 
        for (periode),d in self.variables.items():
            if periode in self.actuacions:
                d1 = self.actuacions[(periode)]
                d2 = d + d1
            else:
                d2 = d
            upload.append([periode, 'indicador', d2])
        for (periode),d in self.variablesEP.items():
            upload.append([periode, 'EPs', d])
        file = u.tempFolder + 'Variables_alcohol.csv'
        u.writeCSV(file, upload)

if __name__ == '__main__':
    Alcohol()