from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import random

nod="nodrizas"
imp="import"
export=True

def get_servei(servei):

    if servei == 'MG':
        serdesc = 'Medicina Familia'
    elif servei == 'PED':
        serdesc = 'Pediatria'
    elif servei in ('INF','INFMG','ENF','INFG','ATS','GCAS', 'INFP', 'INFPD'):
        serdesc = 'Infermeria'
    elif servei in ('ODO','ODN','ODN-P','ODON','RNODN','ODO2','EXO','HODO','00492','ODN2'):
        serdesc = 'Odontologia'
    elif servei in ('DTS','ASS','TSOC','TS','AS','ASO','ASI','ASIS','SS','RNAS','AASS','05999'):
        serdesc = 'Treball social'
    else:
        serdesc = 'ALTRES'
        
    return serdesc
    
def get_grupE(edat):

    if 0 <= edat <= 2:
        eds = '0 -2 anys'
    elif 3 <= edat <= 7:
        eds = '3 - 7 anys'
    elif 8 <= edat <= 14:
        eds = '8 - 14 anys'
    elif 15 <= edat <= 44:
        eds = '15 - 44 anys'
    elif 45 <= edat <= 64:
        eds = '45 - 64 anys'
    elif 65 <= edat <= 74:
        eds = '65 - 74 anys'
    elif edat >74:
        eds = 'Majors de 74 anys'
    else:
        eds = 'error'
        
    return eds
    

def get_centres_ics():
    sql="select scs_codi from cat_centres where ep='0208'"
    up_ics={}
    for up, in getAll(sql,nod):
        up_ics[(up)] = True
    return up_ics

def get_pacient_sexe(up_i):
    sql = "select id_cip_sec, up, sexe, edat from assignada_tot"
    edats = {}
    recomptes = Counter()
    for id, up, sexe, edat in getAll(sql,nod):
        ed = get_grupE(int(edat))
        if up in up_i:
            edats[id] = {'sex': sexe, 'edat': ed}
            recomptes[sexe, ed] += 1
            
    return edats, recomptes

def get_visites(sexe_ass,up_i):
    """
    """
    sql = "select id_cip_sec,visi_up,visi_servei_codi_servei from visites1 where visi_situacio_visita='R' and visi_data_baixa = 0"

    
    vis= Counter()
    for id,up,servei in getAll(sql,imp):
        if up in up_i and id in sexe_ass:
            sexe = sexe_ass[id]['sex']
            edat = sexe_ass[id]['edat']
            servei_cat = get_servei(servei)
            if servei_cat != 'ALTRES':
                vis[(sexe, edat)] += 1  
      
    upload = [ (serv, sex, d)
             for (serv, sex), d in vis.items()]
             
    return upload


if __name__ == '__main__':
    printTime('inici')

    up_i=get_centres_ics()
    printTime("Get centres")
    
    sexe_ass, recomptes=get_pacient_sexe(up_i)
    printTime("Get pacients")

    upload=get_visites(sexe_ass,up_i)  
    printTime('Get Rows')
	
    if export:
        file = tempFolder + 'Visites_2017_servei.txt'
        writeCSV(file, upload, sep=';')
        
        upload = [ (a, b, d)
             for (a, b), d in recomptes.items()]
             
        file = tempFolder + 'Visites_2017_servei_poblacio.txt'
        writeCSV(file, upload, sep=';')
    