

import sisapUtils as u

sql = """select distinct a.codi_centre, a.denominaci_completa, 
            a.nom_municipi, b.abs_d, b.up_c, 
            DATE '2023-02-01' AS data, 'SISAP' 
            from dwsisap.cat_escoles_do a,
            dwsisap.centres_escolars_abs b,
            dwsisap.centres_escolars_centre c
            where a.codi_centre = c.centre_c
            and c.abs = b.abs_c	"""
upload = []
for row in u.getAll(sql, 'exadata'):
    upload.append(row)


u.listToTable(upload, 'DIM_ESCOLES', 'pdp')
u.listToTable(upload, 'DIM_ESCOLES_TEST', 'pdp')

