# coding: latin1

"""
Traspàs del txt de la meva salut amb dades 2018, rebut el 19 nov 2019
"""

import collections as c
import datetime as d
import math as m

import sisapUtils as u


TB = "SISAP_ACCESSOS_LMS"
DB = "redics"
USERS = ("PREDUMMP", "PREDUECR", "PREDUPRP", "PREDULMB")
LMS_file = u.tempFolder + "Accessos_LMS.txt"


class Estructura(object):
    """."""

    def __init__(self):
        """."""
        self.create_table()
        self.get_upload()
        self.upload_data()

    def create_table(self):
        """."""
        cols = ["AVC_NIA varchar2(100)", "AVC_PES varchar2(200)", "AVC_CID number"]
        u.createTable(TB, "({})".format(", ".join(cols)), DB, rm=True)
        u.grantSelect(TB, USERS, DB)

    def get_upload(self):
        """."""
        self.upload = []
        for (nia, pes, cid) in u.readCSV(LMS_file, sep=';'):
            self.upload.append([nia, pes, cid])
    
    def upload_data(self):
        """."""
        u.listToTable(self.upload, TB, DB)
        
if __name__ == "__main__":
    Estructura()