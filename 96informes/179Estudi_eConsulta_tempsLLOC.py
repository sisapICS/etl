# -*- coding: utf8 -*-

"""
Ens demanen el temps que porten en un cupo (fem el del lloc de treball
"""

import collections as c
import datetime as d
import csv,os,sys
import pandas as pd
from datetime import datetime

import sisapUtils as u

tb = "SISAP_ECONSULTA_PROF_temps"
db = "redics"

class EconsLlocs(object):
    """."""

    def __init__(self):
        """."""
        
        self.get_llocs()
        self.get_prof_econs()
        self.upload_data()
        
    def get_llocs(self):
        """Mirem temps en lloc de treball"""
        self.llocs = c.defaultdict(list)
        sql = "select lloc_proveidor_dni_proveidor, lloc_codi_lloc_de_treball, lloc_codi_up, to_char(to_date(lloc_data_inici,'J'),'YYYYMM'), to_char(to_date(lloc_data_baixa, 'J'),'YYYYMM') from pritb025 where lloc_data_baixa>0"
        for dni, lloc, up, inici, fi in u.getAll(sql, db):
            if fi > '201803':
                self.llocs[(dni, up)].append(inici)
                
    def get_prof_econs(self):
        """agafem els professionals de econsulta"""
        self.dates = {}
        sql = "select dni, up from SISAP_ECONSULTA_PROF_vars"
        for dni, up in u.getAll(sql, db):
            if (dni, up) in self.llocs:
                for data in self.llocs[(dni, up)]:
                    if (dni, up) in self.dates:
                        data2 = self.dates[(dni, up)]
                        if data < data2:
                            self.dates[(dni, up)] = data
                    else:
                        self.dates[(dni, up)] = data
        
    def upload_data(self):
        """Pugem a taula de redics"""
        upload = []
        for (dni, up), d in self.dates.items():
            upload.append([dni, up, d])
            
        columns = ["dni varchar2(14)", "up varchar2(5)", "data varchar(10)"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb, db)
        u.grantSelect(tb, ("PREDUMMP", "PREDUPRP", "PREDUEHE"), db)
        
if __name__ == '__main__':
    EconsLlocs()
