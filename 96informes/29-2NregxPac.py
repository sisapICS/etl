#necessitem que a test hi hagi mst_corbes
from sisapUtils import *
from collections import defaultdict, Counter
from numpy import *
from CorbesUtils import *
'''
def getValorsPac():
    valorsP = Counter()
    sql = "select id_cip_sec, val, year(dat), agrupador from {}".format(table)
    for id, val, anys, agr in getAll(sql, test):
        if val != 0:
            valorsP[(id, anys, agr)] += 1
    return valorsP

def getCombinacions(valorsP):
    valorsPac = defaultdict(list)
    for (id, anys, agr), recompte in valorsP.items():
        valorsPac[(anys, agr)].append(recompte)
    return valorsPac

def getdades(variable, anys, valorsPac):
    dades = []
    recomp = 0
    agr = getAgrupador(variable)
    valors = valorsPac[(anys, variable)]
    p00001 = getPercentil(valors, 0.01)
    p0001 = getPercentil(valors, 0.05)
    p001 = getPercentil(valors, 0.1)
    p3 = getPercentil(valors, 3)
    p25 = getPercentil(valors, 25)
    p50 = getPercentil(valors, 50)
    p75 = getPercentil(valors, 75)
    p97 = getPercentil(valors, 97)
    p9999 = getPercentil(valors, 99.9)
    p99999 = getPercentil(valors, 99.95)
    p999999 = getPercentil(valors, 99.99)
    minim = round(min(valors), 2)
    maxim = round(max(valors), 2)
    for val in valors:
        recomp += 1
    print agr, anys, minim, p00001, p0001, p001, p3, p25, p75, p97,  p9999, p99999, p999999, maxim      
   
    
printTime()
for variable in variables:
        for anys in anysCorbes:
            valorsP = getValorsPac()
            valorsPac = getCombinacions(valorsP)
            getdades(variable, anys, valorsPac)
''' 
dades = []
file_data = 'mst_corbes.txt'
sql = 'select id_cip_sec, agrupador, val, sexe, edat_a, edat_m, edat_s, edat_3d, edat_d from mst_corbes'
for id, agr, val, sexe, anys, mesos, set, tresd, dies in getAll(sql, test):
    agrup = getAgrupador(agr)
    dades.append([id, agrup, val, sexe, anys, mesos, set, tresd, dies])
writeCSV(tempFolder + file_data, dades, sep='|')
printTime()
