# -*- coding: utf8 -*-

"""
Cartera de serveis ICS.
"""

import collections as c

import sisapUtils as u


db = 'test'
table = 'proves_cartera'
create = "(cuentas varchar(300), br varchar(5), tipus varchar(10), poblacio varchar(10),  recompte int)"
u.createTable(table,create,db, rm=True)

Fa1year = '2016-12-31'
actual = '2016-01-01'




serveis = [
    ('01', 'Retinografia', [('variables', ['RGRAD', 'RGRAE', 'VF3001', 'VF3002']),
                            ('activitats', ['434', 'CROC', 'RGRAD', 'RGRAE'])], None),
   ('08', 'MAPA', [('variables', ['EK2081', 'EK2082', 'EK2083',
                                   'EK2084', 'EK2085', 'EK2086',
                                   'EK4012', 'EK4013', 'EK4014'])], None),
   ('13', 'Espirometria', [('variables', ['TR3030', 'TR3031', 'TR3032',
                                           'TR3033', 'TR3034', 'TR3035',
                                           'TR3036', 'TR3037', 'PBD', 'BF/F', 'PESPE',
                                            'TR3037', 'TR3017', 'TR302B','EF/F']),
                            ('activitats', ['R301'])], None),
   ('32', 'Ecografia abdominal', [('activitats', ['AA701']),('variables', ['X00102']),
                       ('xml', ['ECO_P_ABDO', 'cutre'])], None),
]


origens = {'variables': [('import', 'variables'),
                         ('vu_cod_vs', 'vu_dat_act', 'vu_up')],
           'activitats': [('import', 'activitats'),
                          ('au_cod_ac', 'au_dat_act', 'au_up')],
            }
            

class Cartera(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_codis()
        self.get_centres()
        self.get_poblacio()
        self.get_dades()
        self.export_data()

    def get_codis(self):
        """Obté els codis dels diferents serveis."""
        self.codis = c.defaultdict(lambda: c.defaultdict(set))
        for servei, des, dades, denominador in serveis:
            for origen, codis in dades:
                for codi in codis:
                    self.codis[origen][codi].add(des)
    
    def get_centres(self):
        """EAP ICS."""
        sql = "select scs_codi, ics_codi from cat_centres where ep = '0208'"
        self.centres = {up: br for up, br in u.getAll(sql, 'nodrizas')}

    def get_poblacio(self):
        """Pacients ICS."""
        sql = 'select id_cip_sec, up from assignada_tot'
        self.poblacio = {id: self.centres[up]
                         for id, up
                         in u.getAll(sql, 'nodrizas')
                         if up in self.centres}
        
    def get_dades(self):
        """Captura les dades de les taules d'origen."""
        self.dades = {'REG': c.defaultdict(set),
                      'ASS': c.defaultdict(set)}
        for origen, info in origens.items():
            (db, tb), columns = info
            sql_columns = ', '.join(('id_cip_sec',) + columns)
            codis = self.codis[origen]
            sql = "select {0} from {1} where {2} in {3} \
                   and {4} between '{5}' and '{6}'".format(
                                                                sql_columns,
                                                                tb,
                                                                columns[0],
                                                                tuple(codis),
                                                                columns[1],
                                                                actual,
                                                                Fa1year)
            for id, cod, dat, up in u.getAll(sql, db):
                unic = (id, dat)
                if up in self.centres:
                    br = self.centres[up]
                    for servei in codis[cod]:
                        self.dades['REG'][(servei, br)].add(unic)
                if id in self.poblacio:
                    br = self.poblacio[id]
                    for servei in codis[cod]:
                        self.dades['ASS'][(servei, br)].add(unic)

    def export_data(self):
        """Export a CSV."""
        fix = ('NUM', 'NOINSAT')
        self.export = []
        for key in ('REG', 'ASS'):
            for (servei, br), conjunt in self.dades[key].items():
                var = (servei + key, br)
                self.export.append(var + fix + (len(conjunt),))
        u.listToTable(self.export, table, db)
        


if __name__ == '__main__':
    Cartera()

