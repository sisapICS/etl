# -*- coding: utf8 -*-

import hashlib as h

"""
peti manolo
"""

import collections as c
import datetime as d
import sys
from datetime import timedelta

import sisapUtils as u


sql = " SELECT min(CAS_DATA), max(CAS_DATA) FROM dwsisap.DBC_VACUNA dv where cas_data >= DATE '2022-01-07'"
for ini, fi in u.getAll(sql, 'exadata'):
    ini=ini
    fi = fi  + timedelta(days=1)

upload = []  
for dat in u.dateRange(ini, fi):
    dat1 = dat.strftime("%Y-%m-%d")
    print dat1
    sql = """ SELECT grup, vacuna, sum(n) FROM (( 
                  SELECT z.grup grup, z.vacuna vacuna, count(*) AS n FROM (   
                    SELECT 
                        CASE WHEN a.EDAT_FINAL >90 THEN 90
                        WHEN mod(a.edat_final, 10) < 5
                        THEN 10 * floor(a.edat_final / 10)
                         else 10 * ceil(a.edat_final / 10) - 5 END AS grup,
                        CASE WHEN immunitzat=0 OR vacuna_1_data + 14 > DATE '{0}' THEN 'no_vacunat'
                        WHEN   DATE '{0}' - BOOSTER_ADMINISTRACIO_DATA> 7 THEN 'booster'
                        WHEN DATE '{0}' - IMMUNITZAT_DATA> 14 THEN 'completa'
                        ELSE 'parcial' END vacuna,
                        CASE WHEN exitus_covid IS NULL OR exitus_covid> DATE '{0}' THEN 0 ELSE 1 END AS EXITUS
                        FROM dwsisap.dbc_vacuna a
                        LEFT JOIN
                        dwsisap.dbc_metriques b
                        ON a.hash=b.hash
                        where
                        A.DATA_NAIXEMENT IS NOT NULL AND A.DATA_NAIXEMENT < DATE '{0}')  z
                WHERE z.exitus=0
                   GROUP BY 
                   z.grup, z.vacuna)   
                UNION (  
                SELECT y.grup grup, y.vacuna vacuna, count(*) AS n FROM 
                   (SELECT 
                        CASE WHEN a.EDAT_FINAL >90 THEN 90
                        WHEN mod(a.edat_final, 10) < 5
                        THEN 10 * floor(a.edat_final / 10)
                         else 10 * ceil(a.edat_final / 10) - 5 END AS grup,
                         'no_vacunat' vacuna,
                        CASE WHEN exitus_covid IS NULL OR exitus_covid> DATE '{0}' THEN 0 ELSE 1 END AS EXITUS
                        FROM dwsisap.dbc_metriques a
                        where
                        exitus_covid IS NOT NULL AND hash NOT IN (SELECT hash FROM dwsisap.DBC_VACUNA dv)
                       and DATA_NAIXEMENT IS NOT NULL
                        ) y
                    WHERE y.exitus = 0
                  GROUP BY 
                  y.grup, y.vacuna
                  ))
                  GROUP BY 
                  grup, vacuna""".format(dat1)
    for  grup, vacuna, rec in u.getAll(sql, 'exadata'):
        upload.append([dat1, grup, vacuna, rec])

file = u.tempFolder + "evolucio_pob2022.txt"
u.writeCSV(file, upload, sep='@')