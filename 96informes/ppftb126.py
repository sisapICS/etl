# coding: utf-8

""" 

"""


import sisapUtils as u
import csv, os, sys
from time import strftime
import collections as c
import datetime as d
from datetime import timedelta
from datetime import datetime
import hashlib as h


USERS = ("PDP",
         "PREDUFFA", "PREDUMMP", "PREDUPRP",
         "PREDUECR", "PREDULMB", "PREDUEHE")

TODAY = d.datetime.now().date()

class ppftb126(object):
    """.""" 
    def __init__(self):
        """."""
        self.get_ppftb126()
        self.export_txt()
        self.upload_table()


    def get_ppftb126(self):
        """."""
        print("------------------------------------------------- get_ppftb126")
        ts = datetime.now()
        print(u.sectors)
        #resultat = u.multiprocess(self.get_ppftb126_worker, u.sectors)
        self.ppftb126 = []
        for sector in u.sectors:
            print(sector)
            if sector in ('6102','6209','6519','6520','6627','6734','6839'):
                sql = """select *
                         from OPS$US2.ppftb126"""
            elif sector in ('6211','6310','6416','6521','6522','6625','6626','6731','6735','6837','6844'):
                sql = """select *
                         from OPS$USU.ppftb126"""
            elif sector in ('6523','6728','6838'):
                sql = """select *
                         from OPS$US3.ppftb126"""
            #if sector not in ('6209','6519','6520','6521','6522','6625','6627','6728','6734','6735'):
            for usu_codi_usuari_ecap, usu_nom_usuari, usu_cogn1_usuari, usu_cogn2_usuari, usu_email, \
                    usu_tip_doc, usu_nif, usu_perfils, usu_cat, usu_num_col, usu_org, usu_cod_up, \
                        usu_cod_catprofecap, usu_des_catprofecap, usu_tel_princ, usu_tel_secun, usu_data_alta, \
                            usu_data_modif, usu_usuari_modif, usu_data_baixa, usu_coment, usu_flag_dni, usu_seg_servidor, usu_autonom in u.getAll(sql, sector):
                self.ppftb126.append((sector, usu_codi_usuari_ecap, usu_nom_usuari, usu_cogn1_usuari, usu_cogn2_usuari, usu_email, usu_tip_doc, usu_nif, usu_perfils, usu_cat, usu_num_col, usu_org, usu_cod_up, usu_cod_catprofecap, usu_des_catprofecap, usu_tel_princ, usu_tel_secun, usu_data_alta, usu_data_modif, usu_usuari_modif, usu_data_baixa, usu_coment, usu_flag_dni, usu_seg_servidor, usu_autonom))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts, len(self.ppftb126)))

    # def get_ppftb126_worker(self, sector):        
    #     """Worker de ppftb126"""
    #     db = sector
    #     converter = []
    #     for row in u.getAll(sql, db):
    #         converter.append((row))
    #     print(sector)
    #     return converter

    def export_txt(self):
        print("--------------------------------------------------- export_txt")
        """."""
        u.writeCSV(u.tempFolder + 'ppftb126_{}.txt'.format(TODAY),
                   self.ppftb126)

    def upload_table(self):
        """."""
        print("------------------------------------------------- upload_table")
        tb = "sisap_ppftb126"
        db = "redics"
        columns = ["codi_sector varchar2(4)",
                    "usu_codi_usuari_ecap varchar2(15)",
                    "usu_nom_usuari varchar2(15)",
                    "usu_cogn1_usuari varchar2(25)",
                    "usu_cogn2_usuari varchar2(25)",
                    "usu_email varchar2(50)",
                    "usu_tip_doc varchar2(15)",
                    "usu_nif varchar2(15)",
                    "usu_perfils varchar2(15)",
                    "usu_cat varchar2(15)",
                    "usu_num_col varchar2(15)",
                    "usu_org varchar2(16)",
                    "usu_cod_up varchar2(15)",
                    "usu_cod_catprofecap varchar2(15)",
                    "usu_des_catprofecap varchar2(40)",
                    "usu_tel_princ varchar2(20)",
                    "usu_tel_secun varchar2(20)",
                    "usu_data_alta date",
                    "usu_data_modif date",
                    "usu_usuari_modif varchar2(15)",
                    "usu_data_baixa date",
                    "usu_coment varchar2(150)",
                    "usu_flag_dni varchar2(1)",
                    "usu_seg_servidor varchar2(15)",
                    "usu_autonom varchar2(15)"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.ppftb126, tb, db)
        u.grantSelect(tb, USERS, db)

if __name__ == '__main__':
    print(datetime.now())
    ts = datetime.now()
    ppftb126()
    print('Time execution time {}'.format(datetime.now() - ts))