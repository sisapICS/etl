# -*- coding: utf8 -*-

"""
sivic a cuap
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u

ass = {}
sql = """select id_cip_sec, usua_data_naixement from assignada"""
for id, naix in u.getAll(sql, 'import'):
    ass[id] = naix
    
recomptes = c.Counter()    
sql = """select id_cip_sec, pr_up, pr_dde, year(pr_dde), week(pr_dde) from problemes where pr_cod_o_ps = 'C' and pr_hist = 1
and pr_data_baixa = 0 and Pr_cod_ps in ( 'C01-J00', 'C01-J02.9', 'C01-J03.9', 'C01-J03.90', 'C01-J04.0', 'C01-J04.1', 'C01-J04.2', 'C01-J06.9', 'C01-J20', 'C01-J20.3',
'C01-J20.4', 'C01-J20.5', 'C01-J20.6', 'C01-J20.7', 'C01-J20.8', 'C01-J20.9', 'C01-J22',
'C01-J09', 'C01-J09.X', 'C01-J09.X1', 'C01-J09.X2', 'C01-J09.X3', 'C01-J09.X9', 'C01-J10', 'C01-J10.0', 'C01-J10.00', 'C01-J10.01',
'C01-J10.08', 'C01-J10.1', 'C01-J10.8', 'C01-J10.81', 'C01-J10.82', 'C01-J10.83', 'C01-J10.89', 'C01-J11', 'C01-J11.0', 'C01-J11.00', 'C01-J11.08',
'C01-J11.1', 'C01-J11.8', 'C01-J11.81', 'C01-J11.82', 'C01-J11.83', 'C01-J11.89',
'C01-J12.82', 'C01-U07.1', 'C01-B34.2', 'C01-Z20.828',
'C01-J21', 'C01-J21.0', 'C01-J21.1', 'C01-J21.9')
and pr_up in ('07111','07207') and extract(year_month from pr_dde) >'201708'"""
for id, up, dde, anys, mes in u.getAll(sql, 'import'):
    if id in ass:
        naix = ass[id]
        edat = u.yearsBetween(naix, dde)
        ed = 'err'
        if edat < 5:
            ed = '0-4 anys'
        elif 5 <= edat  <= 14:
            ed = '5-14 anys'
        elif 15 <= edat  <= 44:
            ed = '15-44 anys'
        elif 45 <= edat  <= 59:
            ed = '45-59 anys'
        elif edat > 59:
            ed = '>59 anys'
        else:
            ed = 'err'
        recomptes[(up, anys, mes, ed)] += 1
        
    
upload = []
   
for (up, anys, mes, ed), val in recomptes.items():
    upload.append([up, anys, mes, ed, val])

file = u.tempFolder + 'cuaps_sivic.txt'
u.writeCSV(file, upload, sep=';')