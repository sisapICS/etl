# -*- coding: latin1 -*-


import sisapUtils as u
import collections as c
from datetime import datetime, date

cat_nacionalitats = {
    '4': ('AFGANISTAN', 'ASIA: ALTRES'),
    '8': ('ALB�NIA', 'EUROPA EST'),
    '276': ('ALEMANYA', 'EUROPA OEST'),
    '12': ('ALG�RIA', 'MAGREB'),
    '20': ('ANDORRA', 'EUROPA OEST'),
    '24': ('ANGOLA', 'SUBSAHARIA'),
    '660': ('ANGUILLA', 'ALTRES'),
    '10': ('ANT�RTIDA', 'ALTRES'),
    '28': ('ANTIGA I BARBUDA', 'AMERICA LLATINA'),
    '532': ('ANTILLES NEERLANDESES', 'ALTRES'),
    '682': ('AR�BIA SAUDITA', 'ASIA: ALTRES'),
    '32': ('ARGENTINA', 'AMERICA LLATINA'),
    '51': ('ARM�NIA', 'EUROPA EST'),
    '36': ('AUSTR�LIA', 'ALTRES'),
    '40': ('�USTRIA', 'EUROPA OEST'),
    '31': ('AZERBAIDJAN', 'ASIA: ALTRES'),
    '44': ('BAHAMES', 'ALTRES'),
    '50': ('BANGLA DESH', 'SUBCONTINENT INDI'),
    '52': ('BARBADOS', 'ALTRES'),
    '56': ('B�LGICA', 'EUROPA OEST'),
    '84': ('BELIZE', 'AMERICA LLATINA'),
    '204': ('BEN�N', 'ASIA: ALTRES'),
    '60': ('BERMUDES', 'ALTRES'),
    '112': ('BIELOR�SSIA', 'EUROPA EST'),
    '68': ('BOL�VIA', 'BOLIVIA'),
    '70': ('B�SNIA I HERCEGOVINA', 'EUROPA EST'),
    '72': ('BOTSWANA', 'SUBSAHARIA'),
    '74': ('BOUVET, ILLA (NORUEGA)', 'ALTRES'),
    '76': ('BRASIL', 'AMERICA LLATINA'),
    '96': ('BRUNEI', 'ASIA: ALTRES'),
    '100': ('BULG�RIA', 'EUROPA EST'),
    '854': ('BURKINA', 'SUBSAHARIA'),
    '108': ('BURUNDI', 'SUBSAHARIA'),
    '136': ('CAIMAN, ILLES', 'ALTRES'),
    '116': ('CAMBODJA', 'ASIA: ALTRES'),
    '120': ('CAMERUN', 'SUBSAHARIA'),
    '124': ('CANAD�', 'ALTRES'),
    '132': ('CAP VERD', 'SUBSAHARIA'),
    '140': ('CENTRAFRICANA, REP�BLICA', 'SUBSAHARIA'),
    '162': ('CHRISTMAS, ILLES', 'ALTRES'),
    '166': ('COCOS, ILLES', 'ALTRES'),
    '170': ('COL�MBIA', 'COLOMBIA'),
    '174': ('COMORES', 'SUBSAHARIA'),
    '178': ('CONGO', 'SUBSAHARIA'),
    '184': ('COOK, ILLES', 'ALTRES'),
    '410': ('COREA, REP�BLICA DE', 'ASIA: ALTRES'),
    '408': ('COREA, REP�BLICA DEMOCR�TICA POPULAR DE', 'ASIA: ALTRES'),
    '384': ("COSTA D'IVORI", 'SUBSAHARIA'),
    '188': ('COSTA RICA', 'AMERICA LLATINA'),
    '191': ('CRO�CIA', 'EUROPA EST'),
    '192': ('CUBA', 'AMERICA LLATINA'),
    '208': ('DINAMARCA', 'EUROPA OEST'),
    '262': ('DJIBOUTI', 'SUBSAHARIA'),
    '212': ('DOMINICA', 'AMERICA LLATINA'),
    '214': ('DOMINICANA, REP�BLICA', 'AMERICA LLATINA'),
    '818': ('EGIPTE', 'ALTRES'),
    '218': ('EQUADOR', 'EQUADOR'),
    '232': ('ERITREA', 'ALTRES'),
    '703': ('ESLOV�QUIA', 'EUROPA EST'),
    '705': ('ESLOV�NIA', 'EUROPA EST'),
    '724': ('ESPANYA', 'AUTOCTON'),
    '840': ("ESTATS UNITS D'AM�RICA", 'ALTRES'),
    '233': ('EST�NIA', 'EUROPA EST'),
    '231': ('ETI�PIA', 'ALTRES'),
    '242': ('FIJI', 'ALTRES'),
    '246': ('FINL�NDIA', 'EUROPA OEST'),
    '250': ('FRAN�A', 'EUROPA OEST'),
    '249': ('FRANCE, METROPOLITAN', 'EUROPA OEST'),
    '266': ('GABON', 'SUBSAHARIA'),
    '270': ('G�MBIA', 'SUBSAHARIA'),
    '268': ('GE�RGIA', 'EUROPA EST'),
    '288': ('GHANA', 'SUBSAHARIA'),
    '292': ('GIBRALTAR', 'EUROPA OEST'),
    '300': ('GR�CIA', 'EUROPA OEST'),
    '304': ('GREL�NDIA', 'ALTRES'),
    '308': ('GRENADA', 'ALTRES'),
    '254': ('GUAIANA FRANCESA', 'AMERICA LLATINA'),
    '320': ('GUATEMALA', 'AMERICA LLATINA'),
    '324': ('GUINEA', 'SUBSAHARIA'),
    '624': ('GUINEA BISSAU', 'SUBSAHARIA'),
    '226': ('GUINEA EQUATORIAL', 'SUBSAHARIA'),
    '328': ('GUYANA', 'AMERICA LLATINA'),
    '332': ('HAIT�', 'AMERICA LLATINA'),
    '340': ('HONDURES', 'AMERICA LLATINA'),
    '344': ('HONG KONG', 'ASIA: ALTRES'),
    '348': ('HONGRIA', 'EUROPA EST'),
    '887': ('IEMEN', 'ASIA: ALTRES'),
    '356': ('�NDIA', 'SUBCONTINENT INDI'),
    '360': ('INDON�SIA', 'ASIA: ALTRES'),
    '364': ('IRAN', 'ASIA: ALTRES'),
    '368': ('IRAQ', 'ASIA: ALTRES'),
    '372': ('IRLANDA', 'EUROPA OEST'),
    '352': ('ISL�NDIA', 'EUROPA OEST'),
    '376': ('ISRAEL', 'ASIA: ALTRES'),
    '380': ('IT�LIA', 'EUROPA OEST'),
    '891': ('IUGOSL�VIA', 'EUROPA EST'),
    '388': ('JAMAICA', 'AMERICA LLATINA'),
    '392': ('JAP�', 'ASIA: ALTRES'),
    '400': ('JORD�NIA', 'ASIA: ALTRES'),
    '398': ('KAZAKHSTAN', 'EUROPA EST'),
    '404': ('KENYA', 'SUBSAHARIA'),
    '417': ('KIRGUIZISTAN', 'ASIA: ALTRES'),
    '414': ('KUWAIT', 'ASIA: ALTRES'),
    '418': ('LAOS', 'ASIA: ALTRES'),
    '428': ('LATVIA', 'EUROPA EST'),
    '426': ('LESOTHO', 'SUBSAHARIA'),
    '422': ('L�BAN', 'ASIA: ALTRES'),
    '430': ('LIB�RIA', 'SUBSAHARIA'),
    '434': ('L�BIA', 'ASIA: ALTRES'),
    '438': ('LIECHTENSTEIN', 'EUROPA OEST'),
    '440': ('LITU�NIA', 'EUROPA EST'),
    '442': ('LUXEMBURG', 'EUROPA OEST'),
    '446': ('MACAU', 'ASIA: ALTRES'),
    '807': ('MACED�NIA', 'EUROPA EST'),
    '450': ('MADAGASCAR', 'ALTRES'),
    '458': ('MAL�ISIA', 'ASIA: ALTRES'),
    '462': ('MALDIVES', 'ASIA: ALTRES'),
    '466': ('MALI', 'SUBSAHARIA'),
    '470': ('MALTA', 'EUROPA OEST'),
    '504': ('MARROC', 'MARROC'),
    '584': ('MARSHALL, ILLES', 'ALTRES'),
    '474': ('MARTINICA', 'AMERICA LLATINA'),
    '480': ('MAURICI', 'ALTRES'),
    '478': ('MAURIT�NIA', 'MAGREB'),
    '484': ('M�XIC', 'AMERICA LLATINA'),
    '508': ('MO�AMBIC', 'SUBSAHARIA'),
    '498': ('MOLD�VIA', 'EUROPA EST'),
    '492': ('M�NACO', 'EUROPA OEST'),
    '496': ('MONG�LIA', 'ASIA: ALTRES'),
    '500': ('MONTSERRAT', 'ALTRES'),
    '516': ('NAM�BIA', 'SUBSAHARIA'),
    '520': ('NAURU', 'ALTRES'),
    '524': ('NEPAL', 'SUBCONTINENT INDI'),
    '558': ('NICARAGUA', 'AMERICA LLATINA'),
    '562': ('N�GER', 'SUBSAHARIA'),
    '566': ('NIG�RIA', 'SUBSAHARIA'),
    '574': ('NORFOLK, ILLES', 'ALTRES'),
    '578': ('NORUEGA', 'EUROPA OEST'),
    '540': ('NOVA CALED�NIA', 'ALTRES'),
    '554': ('NOVA ZELANDA', 'ALTRES'),
    '512': ('OMAN', 'ASIA: ALTRES'),
    '528': ('PA�SOS BAIXOS', 'EUROPA OEST'),
    '586': ('PAKISTAN', 'PAKISTAN'),
    '585': ('PALAU', 'ALTRES'),
    '275': ('PALESTINA', 'ASIA: ALTRES'),
    '591': ('PANAM�', 'AMERICA LLATINA'),
    '600': ('PARAGUAI', 'AMERICA LLATINA'),
    '604': ('PER�', 'AMERICA LLATINA'),
    '258': ('POLIN�SIA FRANCESA', 'ALTRES'),
    '616': ('POL�NIA', 'EUROPA EST'),
    '620': ('PORTUGAL', 'EUROPA OEST'),
    '630': ('PUERTO RICO', 'AMERICA LLATINA'),
    '634': ('QATAR', 'ASIA: ALTRES'),
    '826': ('REGNE UNIT', 'EUROPA OEST'),
    '638': ('REUNI�', 'ALTRES'),
    '642': ('ROMANIA', 'ROMANIA'),
    '643': ('R�SSIA', 'EUROPA EST'),
    '646': ('RWANDA', 'SUBSAHARIA'),
    '732': ('S�HARA OCCIDENTAL', 'MAGREB'),
    '222': ('SALVADOR, EL', 'AMERICA LLATINA'),
    '16': ('SAMOA NORD-AMERICANA', 'ALTRES'),
    '882': ('SAMOA OCCIDENTAL', 'ASIA: ALTRES'),
    '678': ('SAO TOM� I PR�NCIPE', 'AMERICA LLATINA'),
    '686': ('SENEGAL', 'SUBSAHARIA'),
    '690': ('SEYCHELLES', 'ALTRES'),
    '694': ('SIERRA LEONE', 'SUBSAHARIA'),
    '702': ('SINGAPUR', 'ASIA: ALTRES'),
    '760': ('S�RIA', 'ASIA: ALTRES'),
    '706': ('SOM�LIA', 'ALTRES'),
    '144': ('SRI LANKA', 'SUBCONTINENT INDI'),
    '710': ('SUD-�FRICA, REP�BLICA DE', 'ALTRES'),
    '736': ('SUDAN', 'SUBSAHARIA'),
    '239': ('SUD-GEORGIA I ILLES SANDWICH SUD', 'ALTRES'),
    '752': ('SU�CIA', 'EUROPA OEST'),
    '756': ('SU�SSA', 'EUROPA OEST'),
    '740': ('SURINAM', 'ASIA: ALTRES'),
    '744': ('SVALBARD, ARXIP�LAG (I JAN MAYEN, ILLA)', 'ALTRES'),
    '762': ('TADJIKISTAN', 'ASIA: ALTRES'),
    '764': ('TAIL�NDIA', 'ASIA: ALTRES'),
    '158': ('TAIWAN', 'ASIA: ALTRES'),
    '834': ('TANZ�NIA', 'SUBSAHARIA'),
    '86': ("TERRITORI BRIT�NIC DE L'OCE� �NDIC", 'ALTRES'),
    '260': ('TERRITORIS FRANCESOS DEL SUD', 'ALTRES'),
    '626': ('TIMOR ORIENTAL', 'ASIA: ALTRES'),
    '768': ('TOGO', 'SUBSAHARIA'),
    '772': ('TOKELAU', 'ALTRES'),
    '780': ('TRINITAT I TOBAGO', 'AMERICA LLATINA'),
    '788': ('TUN�SIA', 'MAGREB'),
    '792': ('TURQUIA', 'ASIA: ALTRES'),
    '148': ('TXAD', 'SUBSAHARIA'),
    '203': ('TXECA, REP�BLICA', 'EUROPA EST'),
    '804': ('UCRA�NA', 'EUROPA EST'),
    '800': ('UGANDA', 'SUBSAHARIA'),
    '784': ('UNI� DELS EMIRATS �RABS', 'ASIA: ALTRES'),
    '858': ('URUGUAI', 'AMERICA LLATINA'),
    '860': ('UZBEKISTAN', 'ASIA: ALTRES'),
    '862': ('VENE�UELA', 'AMERICA LLATINA'),
    '92': ('VERGES, ILLES (GRAN BRETANYA)', 'ALTRES'),
    '704': ('VIETNAM', 'ASIA: ALTRES'),
    '876': ('WALLIS I FUTUNA, ILLES', 'ALTRES'),
    '152': ('XILE', 'AMERICA LLATINA'),
    '156': ('XINA', 'XINA'),
    '196': ('XIPRE', 'EUROPA OEST'),
    '180': ('ZAIRE', 'SUBSAHARIA'),
    '894': ('Z�MBIA', 'SUBSAHARIA'),
    '716': ('ZIMBABWE', 'SUBSAHARIA'),
    '662': ('SAINT LUCIA', 'ALTRES'),
    '674': ('SAN MARINO', 'ALTRES'),
    '748': ('SWAZIL�NDIA', 'ALTRES'),
    '795': ('TURKMENISTAN', 'ALTRES'),
    '533': ('ARUBA', 'ALTRES'),
    '570': ('NIUE', 'ALTRES'),
    '580': ('MARIANNES DEL NORD, ILLES', 'ALTRES'),
    '581': ('ILLES PERIF�RIQUES MENORS DELS EUA', 'ALTRES'),
    '583': ('MICRON�SIA, ESTATS FEDERATS DE', 'ALTRES'),
    '598': ('PAPUA NOVA GUINEA', 'ALTRES'),
    '612': ('PITCAIRN', 'ALTRES'),
    '296': ('KIRIBATI', 'ALTRES'),
    '316': ('GUAM', 'ALTRES'),
    '454': ('MALAWI', 'ALTRES'),
    '234': ('F�ROE, ILLES', 'ALTRES'),
    '48': ('B�HRAIN', 'ALTRES'),
    '64': ('BHUTAN', 'ALTRES'),
    '90': ('SALOM�', 'ALTRES'),
    '104': ('MYANMAR', 'ALTRES'),
    '499': ('MONTENEGRO', 'ALTRES'),
    '530': ('ANTILLES NEERLANDESES', 'ALTRES'),
    '688': ('S�RBIA', 'ALTRES'),
    '833': ('MAN, ILLA DE', 'ALTRES'),
    '659': ('SAINT CHRISTOPHER I NEVIS', 'ALTRES'),
    '831': ('GUERNSEY', 'ALTRES')
}

cat_ups = {
        '00002': ("LLEIDA", "00034"), 
        '00020': ("LLEIDA", "00034"), 
        '00003': ("LLEIDA", "00034"), 
        '00089': ("Terres de l'Ebre", "05143"), 
        '01322': ("ASSIR Selva Interior", "01315"), 
        '00005': ("ASSIR Aran", "BBB"), 
        '00101': ("ASSIR Selva Interior", "01315"), 
        '00273': ("ASSIR MATAR�", "00319"), 
        '01796': ("ASSIR MATAR�", "00319"), 
        '00373': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00023': ("LLEIDA", "00034"), 
        '00040': ("REUS", "00075"), 
        '00006': ("LLEIDA", "00034"), 
        '00114': ("GIRON�S-PLA DE L'ESTANY", "01094"), 
        '00284': ("LA MINA", "05916"), 
        '00352': ("MUNTANYA", "01422"), 
        '00437': ("PARC SALUT MAR", "04093"), 
        '00438': ("PARC SALUT MAR", "04093"), 
        '00439': ("PARC SALUT MAR", "04093"), 
        '00440': ("PARC SALUT MAR", "01413"), 
        '00441': ("PARC SALUT MAR", "01413"), 
        '00442': ("PARC SALUT MAR", "01413"), 
        '00443': ("PARC SALUT MAR", "01413"), 
        '00444': ("ESQUERRA", "01415"), 
        '00445': ("ESQUERRA", "01415"), 
        '00446': ("DRETA", "01695"), 
        '00447': ("ESQUERRA", "01415"), 
        '00448': ("MUNTANYA", "01422"), 
        '00449': ("MUNTANYA", "01422"), 
        '00450': ("MUNTANYA", "01422"), 
        '00451': ("MUNTANYA", "01422"), 
        '00452': ("MUNTANYA", "01422"), 
        '00453': ("MUNTANYA", "01422"), 
        '00454': ("PARC SALUT MAR", "04093"), 
        '00455': ("PARC SALUT MAR", "04093"), 
        '00456': ("GUINARD�", "04094"), 
        '00457': ("GUINARD�", "04094"), 
        '00458': ("PARC SALUT MAR", "04093"), 
        '00459': ("PARC SALUT MAR", "04093"), 
        '00460': ("ESQUERRA", "01415"), 
        '00461': ("ESQUERRA", "01415"), 
        '00462': ("ESQUERRA", "01415"), 
        '00464': ("DRETA", "01695"), 
        '00465': ("DRETA", "01695"), 
        '00466': ("DRETA", "01695"), 
        '00467': ("DRETA", "01695"), 
        '00468': ("ESQUERRA", "01415"), 
        '00469': ("ESQUERRA", "01415"), 
        '00470': ("ESQUERRA", "01415"), 
        '00471': ("ESQUERRA", "01415"), 
        '00473': ("ESQUERRA", "01415"), 
        '00474': ("ESQUERRA", "01415"), 
        '00475': ("ESQUERRA", "01415"), 
        '00477': ("ESQUERRA", "01415"), 
        '00478': ("ESQUERRA", "01415"), 
        '00479': ("ESQUERRA", "01415"), 
        '00480': ("ESQUERRA", "01415"), 
        '00481': ("ESQUERRA", "01415"), 
        '00482': ("DRETA", "01695"), 
        '00483': ("DRETA", "01695"), 
        '00484': ("DRETA", "01695"), 
        '00485': ("DRETA", "01695"), 
        '00486': ("DRETA", "01695"), 
        '00488': ("MUNTANYA", "01422"), 
        '00489': ("MUNTANYA", "01422"), 
        '00490': ("MUNTANYA", "01422"), 
        '00491': ("GUINARD�", "04094"), 
        '00494': ("MUNTANYA", "01422"), 
        '00497': ("MUNTANYA", "01422"), 
        '00498': ("DRETA", "01695"), 
        '00115': ("ASSIR Alt Empord�", "01323"), 
        '00007': ("LLEIDA", "00034"), 
        '00339': ("ASSIR BERGUED�", "00408"), 
        '00102': ("ASSIR Garrotxa", "01884"), 
        '00696': ("ASSIR Baix Empord�", "01095"), 
        '00116': ("ASSIR Selva Mar�tima/Alt Maresme", "01314"), 
        '00008': ("LLEIDA", "00034"), 
        '00041': ("TARRAGONA", "00076"), 
        '00185': ("ASSIR ANOIA", "00250"), 
        '00340': ("ASSIR MOLLET", "00407"), 
        '00299': ("ASSIR Selva Mar�tima/Alt Maresme", "01314"), 
        '00042': ("REUS", "00075"), 
        '00103': ("ASSIR Ripoll�s", "01885"), 
        '00277': ("ASSIR Selva Mar�tima/Alt Maresme", "01314"), 
        '00374': ("ASSIR GRANOLLERS", "00409"), 
        '00341': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00342': ("ASSIR SABADELL", "00411"), 
        '00385': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '01886': ("ASSIR Selva Interior", "01315"), 
        '00119': ("GIRON�S-PLA DE L'ESTANY", "01094"), 
        '01932': ("ASSIR OSONA", "00412"), 
        '00024': ("ASSIR Puigcerd�", "AAA"), 
        '00343': ("ASSIR CERDANYOLA", "00406"), 
        '00376': ("ASSIR CERDANYOLA", "00406"), 
        '00009': ("LLEIDA", "00034"), 
        '00044': ("VALLS", "01405"), 
        '00151': ("ASSIR BAIX LLOBREGAT CENTRE - Cornell�", "00232"), 
        '00152': ("ASSIR BAIX LLOBREGAT CENTRE - Cornell�", "00232"), 
        '00153': ("ASSIR BAIX LLOBREGAT CENTRE - Cornell�", "00232"), 
        '00154': ("ASSIR BAIX LLOBREGAT CENTRE - Cornell�", "00232"), 
        '00045': ("REUS", "00075"), 
        '00090': ("Terres de l'Ebre", "05143"), 
        '01919': ("ASSIR Alt Empord�", "01323"), 
        '00155': ("ASSIR BAIX LLOBREGAT NORD (Martorell)", "00251"), 
        '00156': ("ASSIR BAIX LLOBREGAT CENTRE - Cornell�", "00232"), 
        '00187': ("ASSIR BAIX LLOBREGAT CENTRE - Cornell�", "00232"), 
        '00047': ("REUS", "00075"), 
        '00120': ("ASSIR Alt Empord�", "01323"), 
        '00043': ("Terres de l'Ebre", "05143"), 
        '00345': ("ASSIR GRANOLLERS", "00409"), 
        '00158': ("ASSIR DELTA - Gav�", "00234"), 
        '00159': ("ASSIR DELTA - Gav�", "00234"), 
        '00121': ("GIRON�S-PLA DE L'ESTANY", "01094"), 
        '00122': ("GIRON�S-PLA DE L'ESTANY", "01094"), 
        '00104': ("GIRON�S-PLA DE L'ESTANY", "01094"), 
        '00105': ("GIRON�S-PLA DE L'ESTANY", "01094"), 
        '00337': ("ASSIR BERGUED�", "00408"), 
        '00025': ("LLEIDA", "00034"), 
        '00336': ("ASSIR BERGUED�", "00408"), 
        '00149': ("ASSIR ANOIA", "00250"), 
        '00106': ("ASSIR Alt Empord�", "01323"), 
        '00107': ("ASSIR Alt Empord�", "01323"), 
        '00030': ("LLEIDA", "00034"), 
        '00028': ("LLEIDA", "00034"), 
        '00010': ("LLEIDA", "00034"), 
        '00011': ("LLEIDA", "00034"), 
        '00029': ("LLEIDA", "00034"), 
        '00012': ("LLEIDA", "00034"), 
        '00026': ("LLEIDA", "00034"), 
        '00915': ("ASSIR Selva Mar�tima/Alt Maresme", "01314"), 
        '01790': ("ASSIR Selva Mar�tima/Alt Maresme", "01314"), 
        '00347': ("ASSIR OSONA", "00412"), 
        '00382': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00348': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00383': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00349': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00168': ("ASSIR BAIX LLOBREGAT NORD (Martorell)", "00251"), 
        '00350': ("ASSIR MOLLET", "00407"), 
        '00279': ("ASSIR MATAR�", "00319"), 
        '00301': ("ASSIR MATAR�", "00319"), 
        '00302': ("ASSIR MATAR�", "00319"), 
        '00697': ("ASSIR MATAR�", "00319"), 
        '00280': ("ASSIR MATAR�", "00319"), 
        '00303': ("ASSIR MATAR�", "00319"), 
        '00281': ("ASSIR MATAR�", "00319"), 
        '00351': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00169': ("ASSIR BAIX LLOBREGAT CENTRE - St Feliu", "00252"), 
        '00014': ("LLEIDA", "00034"), 
        '00048': ("REUS", "00075"), 
        '00050': ("TARRAGONA", "00076"), 
        '00353': ("ASSIR GRANOLLERS", "00409"), 
        '00046': ("Terres de l'Ebre", "05143"), 
        '00051': ("TARRAGONA", "00076"), 
        '00386': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00698': ("ASSIR M�TUA TERRASSA - TERRASSA", "06754"), 
        '00004': ("Pirineu-Aran", "00530"), 
        '00124': ("ASSIR Garrotxa", "01884"), 
        '00125': ("ASSIR Baix Empord�", "01095"), 
        '01798': ("ASSIR Baix Empord�", "01095"), 
        '00013': ("Pirineu-Aran", "00530"), 
        '00354': ("ASSIR MOLLET", "00407"), 
        '00127': ("ASSIR Alt Empord�", "01323"), 
        '00088': ("Terres de l'Ebre", "05143"), 
        '00015': ("Pirineu-Aran", "00530"), 
        '00022': ("Pirineu-Aran", "00530"), 
        '00016': ("LLEIDA", "00034"), 
        '00195': ("ASSIR DELTA - El Prat", "04390"), 
        '00172': ("ASSIR DELTA - El Prat", "04390"), 
        '00173': ("ASSIR DELTA - El Prat", "04390"), 
        '00381': ("ASSIR OSONA", "00412"), 
        '00196': ("ASSIR MATAR�", "00319"), 
        '00129': ("ASSIR Ripoll�s", "01885"), 
        '00052': ("TARRAGONA", "00076"), 
        '00130': ("ASSIR Alt Empord�", "01323"), 
        '00388': ("ASSIR SABADELL", "00411"), 
        '00389': ("ASSIR SABADELL", "00411"), 
        '00356': ("ASSIR SABADELL", "00411"), 
        '00357': ("ASSIR SABADELL", "00411"), 
        '00358': ("ASSIR SABADELL", "00411"), 
        '00359': ("ASSIR SABADELL", "00411"), 
        '00360': ("ASSIR SABADELL", "00411"), 
        '00361': ("ASSIR SABADELL", "00411"), 
        '00108': ("ASSIR Selva Interior", "01315"), 
        '00362': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00174': ("ASSIR BAIX LLOBREGAT NORD (Martorell)", "00251"), 
        '00197': ("ASSIR BAIX LLOBREGAT CENTRE - St Boi", "00733"), 
        '00175': ("ASSIR BAIX LLOBREGAT CENTRE - St Boi", "00733"), 
        '00176': ("ASSIR BAIX LLOBREGAT CENTRE - St Boi", "00733"), 
        '00198': ("ASSIR BAIX LLOBREGAT CENTRE - St Boi", "00733"), 
        '00091': ("Terres de l'Ebre", "05143"), 
        '00363': ("ASSIR GRANOLLERS", "00409"), 
        '00109': ("ASSIR Baix Empord�", "01095"), 
        '00199': ("ASSIR BAIX LLOBREGAT CENTRE - St Feliu", "00252"), 
        '00200': ("ASSIR BAIX LLOBREGAT CENTRE - St Feliu", "00252"), 
        '00364': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00365': ("ASSIR OSONA", "00412"), 
        '00201': ("ASSIR BAIX LLOBREGAT CENTRE - Cornell�", "00232"), 
        '00177': ("ASSIR BAIX LLOBREGAT CENTRE - Cornell�", "00232"), 
        '00366': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00178': ("ASSIR BAIX LLOBREGAT CENTRE - Cornell�", "00232"), 
        '00391': ("ASSIR OSONA", "00412"), 
        '00202': ("ASSIR ALT PENED�S", "00253"), 
        '00367': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00112': ("ASSIR Selva Interior", "01315"), 
        '00285': ("ASSIR SANTA COLOMA", "01412"), 
        '00306': ("ASSIR SANTA COLOMA", "01412"), 
        '00307': ("ASSIR SANTA COLOMA", "01412"), 
        '00286': ("ASSIR SANTA COLOMA", "01412"), 
        '04054': ("ASSIR SANTA COLOMA", "01412"), 
        '00049': ("ASSIR ANOIA", "00250"), 
        '00180': ("ASSIR ANOIA", "00250"), 
        '00369': ("ASSIR MOLLET", "00407"), 
        '00110': ("GIRON�S-PLA DE L'ESTANY", "01094"), 
        '00031': ("LLEIDA", "00034"), 
        '00017': ("Pirineu-Aran", "00530"), 
        '00111': ("ASSIR Selva Interior", "01315"), 
        '00203': ("ASSIR GARRAF", "00254"), 
        '01324': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00370': ("ASSIR BAGES-SOLSON�S", "00410"), 
        '00368': ("ASSIR OSONA", "00412"), 
        '00054': ("VALLS", "01405"), 
        '00055': ("VALLS", "01405"), 
        '00059': ("REUS", "00075"), 
        '00060': ("TARRAGONA", "00076"), 
        '00061': ("REUS", "00075"), 
        '00062': ("REUS", "00075"), 
        '00018': ("LLEIDA", "00034"), 
        '00053': ("Terres de l'Ebre", "05143"), 
        '00701': ("ASSIR CST - TERRASSA", "06746"), 
        '00702': ("ASSIR CST - TERRASSA", "06746"), 
        '00371': ("ASSIR M�TUA TERRASSA - TERRASSA", "06754"), 
        '01785': ("ASSIR M�TUA TERRASSA - TERRASSA", "06754"), 
        '01786': ("ASSIR M�TUA TERRASSA - TERRASSA", "06754"), 
        '00703': ("ASSIR CST - TERRASSA", "06746"), 
        '00395': ("ASSIR OSONA", "00412"), 
        '00372': ("ASSIR OSONA", "00412"), 
        '00093': ("Terres de l'Ebre", "05143"), 
        '00094': ("Terres de l'Ebre", "05143"), 
        '00065': ("TARRAGONA", "00076"), 
        '00704': ("ASSIR Baix Empord�", "01095"), 
        '00019': ("Pirineu-Aran", "00530"), 
        '00092': ("Terres de l'Ebre", "05143"), 
        '00181': ("ASSIR BAIX LLOBREGAT CENTRE - St Feliu", "00252"), 
        '00066': ("TARRAGONA", "00076"), 
        '00068': ("TARRAGONA", "00076"), 
        '00182': ("ASSIR DELTA - Gav�", "00234"), 
        '00183': ("ASSIR DELTA - Gav�", "00234"), 
        '00133': ("ASSIR Alt Empord�", "01323"), 
        '00184': ("ASSIR ANOIA", "00250"), 
        '00309': ("ASSIR MATAR�", "00319"), 
        '00292': ("ASSIR BADALONA", "00318"), 
        '00293': ("ASSIR BADALONA", "00318"), 
        '00294': ("ASSIR BADALONA", "00318"), 
        '00274': ("ASSIR SANT ADRI�", "06378"), 
        '00275': ("ASSIR SANT ADRI�", "06378"), 
        '00276': ("ASSIR SANT ADRI�", "06378"), 
        '00297': ("ASSIR BADALONA", "00318"), 
        '01325': ("ASSIR BADALONA", "00318"), 
        '00290': ("ASSIR BADALONA", "00318"), 
        '00291': ("ASSIR BADALONA", "00318"), 
        '00695': ("ASSIR BADALONA", "00318"), 
        '00377': ("ASSIR GRANOLLERS", "00409"), 
        '00379': ("ASSIR GRANOLLERS", "00409"), 
        '00380': ("ASSIR GRANOLLERS", "00409"), 
        '00378': ("ASSIR GRANOLLERS", "00409"), 
        '00160': ("ASSIR DELTA - Hospitalet", "00233"), 
        '00163': ("ASSIR DELTA - Hospitalet", "00233"), 
        '00191': ("ASSIR DELTA - Hospitalet", "00233"), 
        '00192': ("ASSIR DELTA - Hospitalet", "00233"), 
        '00164': ("ASSIR DELTA - Hospitalet", "00233"), 
        '00165': ("ASSIR DELTA - Hospitalet", "00233"), 
        '00166': ("ASSIR DELTA - Hospitalet", "00233"), 
        '00167': ("ASSIR DELTA - Hospitalet", "00233"), 
        '00193': ("ASSIR DELTA - Hospitalet", "00233"), 
        '00161': ("ASSIR DELTA - Hospitalet", "00233"), 
        '00162': ("ASSIR DELTA - Hospitalet", "00233"), 
        '00189': ("ASSIR DELTA - Hospitalet", "00233"), 
        '00346': ("ASSIR MOLLET", "00407"), 
        '00500': ("GUINARD�", "04094"), 
        '00705': ("XARXA TECLA", "CCC"), 
        '01097': ("TARRAGONA", "00076"), 
        '01326': ("XARXA TECLA", "CCC"), 
        '01327': ("XARXA TECLA", "CCC"), 
        '01328': ("XARXA TECLA", "CCC"), 
        '00699': ("ASSIR M�TUA TERRASSA - RUB�", "06754"), 
        '00355': ("ASSIR CST - RUB�", "06746"), 
        '00283': ("ASSIR SANT ADRI�", "06378"), 
        '00598': ("PARC SALUT MAR", "07262"), 
        '01329': ("REUS", "00075"), 
        '00396': ("ASSIR GRANOLLERS", "00409"), 
        '00205': ("ASSIR GARRAF", "00254"), 
        '00206': ("ASSIR GARRAF", "00254"), 
        '01791': ("ASSIR BADALONA", "00318"), 
        '00338': ("ASSIR CERDANYOLA", "00406"), 
        '00344': ("ASSIR CERDANYOLA", "00406"), 
        '01330': ("REUS", "00075"), 
        '00282': ("ASSIR Selva Mar�tima/Alt Maresme", "01314"), 
        '01004': ("ESQUERRA", "01415"), 
        '01273': ("PARC SALUT MAR", "07263"), 
        '00087': ("Terres de l'Ebre", "05143"), 
        '00027': ("LLEIDA", "00034"), 
        '00117': ("ASSIR Selva Interior", "01315"), 
        '01933': ("DRETA", "01695"), 
        '01485': ("REUS", "00075"), 
        '00021': ("LLEIDA", "00034"), 
        '00305': ("ASSIR MATAR�", "00319"), 
        '00288': ("ASSIR Selva Mar�tima/Alt Maresme", "01314"), 
        '00186': ("ASSIR ANOIA", "00250"), 
        '00171': ("ASSIR ANOIA", "00250"), 
        '01522': ("XARXA TECLA", "CCC"), 
        '01928': ("REUS", "00075"), 
        '00295': ("ASSIR BADALONA", "00318"), 
        '00296': ("ASSIR BADALONA", "00318"), 
        '01321': ("ASSIR Ripoll�s", "01885"), 
        '00390': ("ASSIR SABADELL", "00411"), 
        '02038': ("ASSIR SABADELL", "00411"), 
        '00131': ("ASSIR Garrotxa", "01884"), 
        '01883': ("ASSIR Garrotxa", "01884"), 
        '01122': ("ASSIR MOLLET", "00407"), 
        '01123': ("ASSIR MOLLET", "00407"), 
        '00397': ("ASSIR OSONA", "00412"), 
        '01077': ("ASSIR OSONA", "00412"), 
        '01929': ("REUS", "00075"), 
        '01083': ("ASSIR MATAR�", "00319"), 
        '02791': ("VALLS", "01405"), 
        '01121': ("ASSIR GRANOLLERS", "00409"), 
        '01128': ("ASSIR M�TUA TERRASSA - TERRASSA", "06754"), 
        '03527': ("XARXA TECLA", "CCC"), 
        '02000': ("DRETA", "01695"), 
        '03449': ("ASSIR GARRAF", "00254"), 
        '04819': ("ASSIR BAIX LLOBREGAT NORD (Martorell)", "00251"), 
        '00278': ("ASSIR BADALONA", "00318"), 
        '05166': ("ASSIR BADALONA", "00318"), 
        '04548': ("ASSIR SABADELL", "00411"), 
        '03913': ("TARRAGONA", "00076"), 
        '04376': ("ASSIR DELTA - Gav�", "00234"), 
        '04374': ("ASSIR DELTA - Gav�", "00234"), 
        '04056': ("ASSIR BAIX LLOBREGAT NORD (Martorell)", "00251"), 
        '04055': ("ASSIR SANTA COLOMA", "01412"), 
        '04547': ("ASSIR BAIX LLOBREGAT CENTRE - St Feliu", "00252"), 
        '04546': ("ASSIR BAIX LLOBREGAT NORD (Martorell)", "00251"), 
        '00194': ("ASSIR ANOIA", "00250"), 
        '06175': ("ASSIR ANOIA", "00250"), 
        '05239': ("ASSIR DELTA - Gav�", "00234"), 
        '00179': ("ASSIR BAIX LLOBREGAT CENTRE - St Boi", "00733"), 
        '04957': ("ASSIR BAIX LLOBREGAT CENTRE - St Boi", "00733"), 
        '04704': ("ASSIR GRANOLLERS", "00409"), 
        '04713': ("ASSIR OSONA", "00412"), 
        '04863': ("ASSIR CST - RUB�", "06746"), 
        '07927': ("LLEIDA", "00034"), 
        '06009': ("ASSIR GARRAF", "00254"), 
        '05132': ("ESQUERRA", "01415"), 
        '05945': ("ASSIR CERDANYOLA", "00406"), 
        '06156': ("MUNTANYA", "01422"), 
        '00387': ("ASSIR CERDANYOLA", "00406"), 
        '06187': ("ASSIR CERDANYOLA", "00406"), 
        '07084': ("ASSIR M�TUA TERRASSA - SANT CUGAT", "06754"), 
        '07085': ("ASSIR M�TUA TERRASSA - SANT CUGAT", "06754"), 
        '07086': ("ASSIR M�TUA TERRASSA - SANT CUGAT", "06754"), 
        '06188': ("ASSIR MOLLET", "00407"), 
        '06189': ("ASSIR SABADELL", "00411"), 
        '08117': ("ASSIR GARRAF", "00254"), 
        '08118': ("ASSIR GARRAF", "00254"), 
        '07504': ("MUNTANYA", "01422"), 
        '07505': ("MUNTANYA", "01422"), 
        '07961': ("ASSIR ALT PENED�S", "00253"), 
        '07962': ("ASSIR ALT PENED�S", "00253"), 
        '14276': ("ASSIR M�TUA TERRASSA - TERRASSA", "06754"), 
        '08208': ("ESQUERRA", "01415"), 
        '08096': ("ASSIR DELTA - Gav�", "00234"), 
        '08311': ("MUNTANYA", "01422"), 
        '08312': ("MUNTANYA", "01422"), 
        '15386': ("ASSIR M�TUA TERRASSA - TERRASSA", "06754"), 
        '15838': ("ASSIR ALT PENED�S", "00253"), 
        '15839': ("ASSIR ALT PENED�S", "00253"), 
        '15443': ("LLEIDA", "00034"), 
}

dates_naix = dict()
nacionalitats = c.defaultdict(set)
conversor_idcip = dict()

sql = """
        SELECT id_cip, id_cip_sec, usua_data_naixement, usua_nacionalitat FROM import.assignada
      """
for id_cip, id_cip_sec, data_naix, nacionalitat in u.getAll(sql, "nodrizas"):
    try:
        nacionalitat = str(int(nacionalitat))
    except:
        nacionalitat = nacionalitat
    dates_naix[id_cip] = data_naix
    nacionalitats[id_cip].add(nacionalitat)
    conversor_idcip[id_cip_sec] = id_cip


assignada_2021, assignada_2022, assignada_2023 = c.defaultdict(set), c.defaultdict(set), c.defaultdict(set)

sql = """SELECT id_cip, up FROM import.assignadahistorica_s2021"""
for id_cip, up in u.getAll(sql, "nodrizas"):
    if up in cat_ups:
        assignada_2021[id_cip] = cat_ups[up]

sql = """SELECT id_cip, up FROM import.assignadahistorica_s2022"""
for id_cip, up in u.getAll(sql, "nodrizas"):
    if up in cat_ups:
        assignada_2022[id_cip] =  cat_ups[up]
            
sql = """SELECT id_cip, up FROM import.assignadahistorica_s2023"""
for id_cip, up in u.getAll(sql, "nodrizas"):
    if up in cat_ups:
        assignada_2023[id_cip] =  cat_ups[up]

nacionalitats_fix = dict()

for id_cip in nacionalitats:
    if any(nacionalitat not in ('724', '') for nacionalitat in nacionalitats[id_cip]):
        nacionalitat_fix = next(nacionalitat for nacionalitat in nacionalitats[id_cip] if nacionalitat not in ('724', ''))
    elif '724' in nacionalitats[id_cip]:
        nacionalitat_fix = '724'
    else:
        nacionalitat_fix = ''
        
    try:     
        nacionalitat_desc, nacionalitat_grup = cat_nacionalitats[str(nacionalitat_fix)] if nacionalitat_fix != '' else ('', '')
    except:
        nacionalitat_desc, nacionalitat_grup = ('', '')
    
    nacionalitats_fix[id_cip] = (nacionalitat_fix, nacionalitat_desc, nacionalitat_grup)


chagas_serologies = c.defaultdict(lambda: c.defaultdict(set))

def sub_get_laboratori(sql):

    sub_laboratori = set()

    for id_cip, data_lab, resultat in u.getAll(sql, "import"):


        resultat_replaced_comma = resultat.split(",")
        resultat_replaced_dot = resultat.split(".")
        if resultat_replaced_comma[0].isdigit():
            if int(resultat_replaced_comma[0])<1:
                chaga_serologia = 0
            else:
                chaga_serologia = 1
        elif resultat_replaced_dot[0].isdigit():
            if int(resultat_replaced_dot[0])<1:
                chaga_serologia = 0
            else:
                chaga_serologia = 1
        elif resultat[0] == ",":
            chaga_serologia = 0
        elif resultat == "N":
            chaga_serologia = 0
        elif "posi" in resultat.lower():
            chaga_serologia = 1
        elif "neg" in resultat.lower():
            chaga_serologia = 0
        else:
            chaga_serologia = -1

        sub_laboratori.add((id_cip, data_lab, chaga_serologia))
    
    return sub_laboratori

codis_lab_chagas = ('006763','S20785','S39685','S39785')

sql = """
        SELECT
            id_cip,
            cr_data_reg,
            cr_res_lab
        FROM
            import.{}
        WHERE
            cr_codi_prova_ics IN {}
      """
jobs = [sql.format(table, tuple(codis_lab_chagas)) for table in u.getSubTables("laboratori") if table[-6:] != '_s6951']
count = 0
for sub_laboratori in u.multiprocess(sub_get_laboratori, jobs, 4):
    for id_cip, data_lab, resultat in sub_laboratori:
        chagas_serologies[id_cip][data_lab] = resultat
    count+=1
    print("    labor: {}/{}. {}".format(count, len(jobs), datetime.now()))


codis_ps_chagas = set()

sql = """
        select
            criteri_codi 
        from
            nodrizas.eqa_criteris
        where
            agrupador_desc = 'chagas'
      """ 
for codi_ps, in u.getAll(sql, "nodrizas"):
    codi_ps = codi_ps.replace("C01-", "")
    codis_ps_chagas.add(codi_ps)
    codis_ps_chagas.add("C01-"+codi_ps)

def sub_get_problemes(sql):

    sub_problemes = set()

    for id_cip, data_ps, cod_ps in u.getAll(sql, "import"):
        sub_problemes.add((id_cip, data_ps, cod_ps))
    
    return sub_problemes

problemes = c.defaultdict(lambda: c.defaultdict(set))

codis_ps_chagas = set(codis_ps_chagas)

sql = """
        SELECT
            id_cip,
            pr_dde,
            pr_cod_ps
        FROM
            {}
        WHERE
            pr_cod_ps IN {}
      """
jobs = [sql.format(table, tuple(codis_ps_chagas)) for table in u.getSubTables("problemes") if table[-6:] != '_s6951']
count = 0
for sub_problemes in u.multiprocess(sub_get_problemes, jobs, 4):
    for id_cip, data_ps, cod_ps in sub_problemes:
        problemes[id_cip][data_ps] = cod_ps
    count+=1
    print("    probs: {}/{}. {}".format(count, len(jobs), datetime.now()))

sql = """
        select
            id_cip,
            hash_a
        from
            import.u11_all
      """
idcipsec2hash = {id_cip: hash_a for id_cip, hash_a in u.getAll(sql, "import")}

sql = """
        select
            hash_redics hash_a,
            hash_covid hash_d
        from
            pdptb101_relacio
      """
hash2hash = {hash_a: hash_d for hash_a, hash_d in u.getAll(sql, "pdp")}


embarasos_2021, embarasos_2022, embarasos_2023 = set(), set(), set()

sql = """
        SELECT
            id_cip_sec,
            inici,
            fi,
            emb_tepal
        FROM
            nodrizas.ass_embaras_2021
      """
for id_cip_sec, inici, fi, tepal in u.getAll(sql, "nodrizas"):
    try:
        id_cip = conversor_idcip[id_cip_sec]
        if fi.year == 2021 and id_cip in assignada_2021:
            embarasos_2021.add((id_cip, inici, fi, tepal))
    except:
        next
        
sql = """
        SELECT
            id_cip_sec,
            inici,
            fi,
            emb_tepal
        FROM
            nodrizas.ass_embaras_2022
      """
for id_cip_sec, inici, fi, tepal in u.getAll(sql, "nodrizas"):
    try:
        id_cip = conversor_idcip[id_cip_sec]
        if fi.year == 2022 and id_cip in assignada_2022:
            embarasos_2022.add((id_cip, inici, fi, tepal))
    except:
        next
        
sql = """
        SELECT
            id_cip_sec,
            inici,
            fi,
            emb_tepal
        FROM
            nodrizas.ass_embaras_2023
      """
for id_cip_sec, inici, fi, tepal in u.getAll(sql, "nodrizas"):
    try:
        id_cip = conversor_idcip[id_cip_sec]
        if fi.year == 2023 and id_cip in assignada_2023:
            embarasos_2023.add((id_cip, inici, fi, tepal))
    except:
        next

cols = """(hash_d varchar2(50), data_naix date, assir_desc varchar2(50), up_assir varchar2(5), nacionalitat_codi varchar2(5),
           nacionalitat_desc varchar2(50), nacionalitat_grup varchar2(50), emb_inici date, emb_fi date, 
           tepal varchar2(50), n_serologies int, n_serologies_positives int, n_serologies_negatives int, n_serologies_neutres int, data_dx date, cod_dx varchar2(15))"""

dades_2021 = list()
for id_cip, emb_inici, emb_fi, tepal in embarasos_2021:
    hash_a = idcipsec2hash.get(id_cip, None)
    hash_d = hash2hash.get(hash_a, None)
    if hash_d:
        data_naix = dates_naix[id_cip]
        nacionalitat_fix, nacionalitat_desc, nacionalitat_grup = nacionalitats_fix[id_cip]
        assir_desc, up_assir = assignada_2021[id_cip]
        try:
            data_dx = min(problemes[id_cip]) if min(problemes[id_cip]) <= emb_fi else None
            max_data_dx = min(problemes[id_cip]) if id_cip in problemes and min(problemes[id_cip]) <= emb_fi else None
            cod_dx = problemes[id_cip][max_data_dx] if id_cip in problemes else None
        except:
            data_dx = None
            max_data_dx = None
            cod_dx = None    
        n_serologies = sum(data <= emb_fi for data in chagas_serologies[id_cip])
        try:
            n_serologies_positives = sum(data <= emb_fi for data, resultat in chagas_serologies[id_cip].items() if resultat == 1)
            n_serologies_negatives = sum(data <= emb_fi for data, resultat in chagas_serologies[id_cip].items() if resultat == 0)
            n_serologies_neutres = sum(data <= emb_fi for data, resultat in chagas_serologies[id_cip].items() if resultat == -1)
        except:
            n_serologies_positives = 0
            n_serologies_negatives = 0
            n_serologies_neutres = 0
        if cod_dx == set():
            cod_dx = None
        dades_2021.append((hash_d, data_naix, assir_desc, up_assir, nacionalitat_fix, 
                           nacionalitat_desc, nacionalitat_grup, emb_inici, emb_fi, 
                           tepal, n_serologies, n_serologies_positives, n_serologies_negatives, n_serologies_neutres, data_dx, cod_dx))

u.createTable("peticio_assir_chagas_2021", cols, "exadata", rm=1)
u.listToTable(dades_2021, "peticio_assir_chagas_2021", "exadata")       

cols = """(hash_d varchar2(50), data_naix date, assir_desc varchar2(50), up_assir varchar2(5), nacionalitat_codi varchar2(5),
           nacionalitat_desc varchar2(50), nacionalitat_grup varchar2(50), emb_inici date, emb_fi date, 
           tepal varchar2(50), n_serologies int, n_serologies_positives int, n_serologies_negatives int, n_serologies_neutres int, data_dx date, cod_dx varchar2(15))"""

dades_2022 = list()
for id_cip, emb_inici, emb_fi, tepal in embarasos_2022:
    hash_a = idcipsec2hash.get(id_cip, None)
    hash_d = hash2hash.get(hash_a, None)
    if hash_d:
        data_naix = dates_naix[id_cip]
        nacionalitat_fix, nacionalitat_desc, nacionalitat_grup = nacionalitats_fix[id_cip]
        assir_desc, up_assir = assignada_2022[id_cip]
        try:
            data_dx = min(problemes[id_cip]) if min(problemes[id_cip]) <= emb_fi else None
            max_data_dx = min(problemes[id_cip]) if id_cip in problemes and min(problemes[id_cip]) <= emb_fi else None
            cod_dx = problemes[id_cip][max_data_dx] if id_cip in problemes else None
        except:
            data_dx = None
            max_data_dx = None
            cod_dx = None    
        n_serologies = sum(data <= emb_fi for data in chagas_serologies[id_cip])
        try:
            n_serologies_positives = sum(data <= emb_fi for data, resultat in chagas_serologies[id_cip].items() if resultat == 1)
            n_serologies_negatives = sum(data <= emb_fi for data, resultat in chagas_serologies[id_cip].items() if resultat == 0)
        except:
            n_serologies_positives = 0
            n_serologies_negatives = 0
        if cod_dx == set():
            cod_dx = None
        dades_2022.append((hash_d, data_naix, assir_desc, up_assir, nacionalitat_fix, 
                           nacionalitat_desc, nacionalitat_grup, emb_inici, emb_fi, 
                           tepal, n_serologies, n_serologies_positives, n_serologies_negatives, n_serologies_neutres, data_dx, cod_dx))

u.createTable("peticio_assir_chagas_2022", cols, "exadata", rm=1)
u.listToTable(dades_2022, "peticio_assir_chagas_2022", "exadata")       

cols = """(hash_d varchar2(50), data_naix date, assir_desc varchar2(50), up_assir varchar2(5), nacionalitat_codi varchar2(5),
           nacionalitat_desc varchar2(50), nacionalitat_grup varchar2(50), emb_inici date, emb_fi date, 
           tepal varchar2(50), n_serologies int, n_serologies_positives int, n_serologies_negatives int, n_serologies_neutres int, data_dx date, cod_dx varchar2(15))"""

dades_2023 = list()
for id_cip, emb_inici, emb_fi, tepal in embarasos_2023:
    hash_a = idcipsec2hash.get(id_cip, None)
    hash_d = hash2hash.get(hash_a, None)
    if hash_d:
        data_naix = dates_naix[id_cip]
        nacionalitat_fix, nacionalitat_desc, nacionalitat_grup = nacionalitats_fix[id_cip]
        assir_desc, up_assir = assignada_2023[id_cip]
        try:
            data_dx = min(problemes[id_cip]) if min(problemes[id_cip]) <= emb_fi else None
            max_data_dx = min(problemes[id_cip]) if id_cip in problemes and min(problemes[id_cip]) <= emb_fi else None
            cod_dx = problemes[id_cip][max_data_dx] if id_cip in problemes else None
        except:
            data_dx = None
            max_data_dx = None
            cod_dx = None    
        n_serologies = sum(data <= emb_fi for data in chagas_serologies[id_cip])
        try:
            n_serologies_positives = sum(data <= emb_fi for data, resultat in chagas_serologies[id_cip].items() if resultat == 1)
            n_serologies_negatives = sum(data <= emb_fi for data, resultat in chagas_serologies[id_cip].items() if resultat == 0)
        except:            
            n_serologies_positives = 0
            n_serologies_negatives = 0
        if cod_dx == set():
            cod_dx = None
        dades_2023.append((hash_d, data_naix, assir_desc, up_assir, nacionalitat_fix, 
                           nacionalitat_desc, nacionalitat_grup, emb_inici, emb_fi, 
                           tepal, n_serologies, n_serologies_positives, n_serologies_negatives, n_serologies_neutres, data_dx, cod_dx))


u.createTable("peticio_assir_chagas_2023", cols, "exadata", rm=1)
u.listToTable(dades_2023, "peticio_assir_chagas_2023", "exadata")       

u.createTable("peticio_assir_chagas_2021", cols, "exadata", rm=1)
u.listToTable(dades_2021, "peticio_assir_chagas_2021", "exadata") 

u.createTable("peticio_assir_chagas_2022", cols, "exadata", rm=1)
u.listToTable(dades_2022, "peticio_assir_chagas_2022", "exadata") 

u.createTable("peticio_assir_chagas_2023", cols, "exadata", rm=1)
u.listToTable(dades_2023, "peticio_assir_chagas_2023", "exadata") 

u.grantSelect("peticio_assir_chagas_2021", "DWSISAP_ROL", "exadata")
u.grantSelect("peticio_assir_chagas_2022", "DWSISAP_ROL", "exadata")
u.grantSelect("peticio_assir_chagas_2023", "DWSISAP_ROL", "exadata")