# coding: latin1

"""
.
"""

import sisapUtils as u


FILE = u.tempFolder + "consultoris_no_ics.csv"


class Consultoris(object):
    """."""

    def __init__(self):
        """."""
        self.get_ep()
        self.get_up()
        self.get_cs()
        self.get_poblacio()
        self.export_data()

    @staticmethod
    def _get_cataleg(sql, db):
        """."""
        return {cod: des for (cod, des) in u.getAll(sql, db)}

    def get_ep(self):
        """."""
        self.ep = {}
        sql = "select e_p_cod_ep, e_p_descripcio from pritb009"
        for sector in u.sectors:
            self.ep[sector] = {cod: des for cod, des in u.getAll(sql, sector)}

    def get_up(self):
        """."""
        self.up = {sector: {} for sector in u.sectors}
        sql = "select sector, scs_codi, ics_desc from cat_centres"
        for sec, cod, des in u.getAll(sql, "nodrizas"):
            self.up[sec][cod] = des

    def get_cs(self):
        """."""
        self.cs = {sector: {} for sector in u.sectors}
        sql = "select codi_sector, cent_codi_centre, \
                      cent_classe_centre, cent_nom_centre \
               from cat_pritb010"
        for sec, cod, cla, des in u.getAll(sql, "import"):
            self.cs[sec][(cod, cla)] = des

    def get_poblacio(self):
        """."""
        self.resultat = []
        sql = "select codi_sector, if(ep='2566', '2455', ep), up, \
                      centre_codi, centre_classe, count(1) \
               from assignada_tot \
               where ep <> '0208' \
               group by 1, 2, 3, 4, 5"
        for sec, ep, up, cod, cla, n in u.getAll(sql, "nodrizas"):
            this = (ep, self.ep[sec].get(ep),
                    up, self.up[sec].get(up),
                    cod, cla, self.cs[sec].get((cod, cla)),
                    n)
            self.resultat.append(this)

    def export_data(self):
        """."""
        header = [("ep_cod", "ep_des",
                   "up_cod", "up_des",
                   "cs_cod", "cs_cla", "cs_des",
                   "pob")]
        u.writeCSV(FILE, header + sorted(self.resultat), sep=";")


if __name__ == "__main__":
    Consultoris()
