# coding: utf8

"""
.
"""

import sisapUtils as u


codis = ('S00', 'S00.3', 'S00.4', 'S00.5', 'S00.7', 'S00.8', 'S00.9', 'S02',
         'S02.0', 'S02.1', 'S02.2', 'S02.3', 'S02.4', 'S02.5', 'S02.6',
         'S02.7', 'S02.8', 'S02.9', 'S05', 'S05.8', 'S05.9', 'S06', 'S06.2',
         'S06.3', 'S06.7', 'S06.8', 'S06.9', 'S07', 'S07.0', 'S07.1', 'S07.8',
         'S07.9', 'S09', 'S09.7', 'S09.8', 'S09.9', 'S10', 'S19', 'S19.7',
         'S19.8', 'S19.9', 'T00.0', 'T04.0', 'T06.0', 'T90', 'T90.0', 'T90.4',
         'T90.5', 'T90.8', 'T90.9')


class Jutjat(object):
    """."""

    def __init__(self):
        """."""
        self.pacients = []
        for sector in u.sectors:
            self.get_sector(sector)
        self.export()

    def get_sector(self, sector):
        """."""
        sql = "select usua_cognom1, usua_cognom2, usua_nom, usua_sexe, \
                      to_char(to_date(usua_data_naixement, 'J'), 'DD-MM-YYYY'), \
                      usua_dni, pr_cod_ps, ps_des, cent_nom_centre, \
                      to_char(pr_dde, 'DD-MM-YYYY'), \
                      to_char(to_date(pr_v_dat, 'J'), 'DD-MM-YYYY') \
               from prstb015, usutb040, prstb001, pritb010 \
               where pr_cod_ps in {} and \
                     pr_ep = '0208' and \
                     pr_dde between to_date('20170716', 'YYYYMMDD') and \
                                    to_date('20170726', 'YYYYMMDD') and \
                     pr_cod_ps = ps_cod and \
                     pr_v_cen = cent_codi_centre and \
                     pr_v_cla = cent_classe_centre and \
                     pr_cod_u = usua_cip".format(codis)
        self.pacients.extend([row for row in u.getAll(sql, sector)])

    def export(self):
        """."""
        header = [('cognom1', 'cognom2', 'nom', 'sexe', 'data_naixement',
                   'dni', 'diagnostic_codi', 'diagnostic_literal', 'centre',
                   'data_diagnostic', 'data_visita')]
        u.writeCSV(u.tempFolder + 'traumatismes_ap_ics.csv',
                   header + sorted(self.pacients), sep=';')


if __name__ == '__main__':
    Jutjat()
