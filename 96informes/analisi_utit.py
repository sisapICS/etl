import sisapUtils as u
import sisaptools as t
import collections as c
import datetime as d
from dateutil import relativedelta as rd

u.printTime('inici')

def treat_visites(its, visita):
    for id in its:
        (edat, baixa, alta, dies, up, tipus) = its[id]
        if isinstance(visita, d.datetime):
            visita = visita.date()
        if isinstance(baixa, d.datetime):
            baixa = baixa.date()
        if isinstance(alta, d.datetime):
            alta = alta.date()
        if baixa <= visita <= alta:
            return (1, id)
    return (0, None)

def treat_dgn(dgn):
    artro, conn, dorso, teixits, ossis = 0, 0, 0, 0, 0
    if dgn < 'M26':
        artro = 1
    elif 'M30' <= dgn < 'M37':
        conn = 1
    elif 'M40' <= dgn < 'M55':
        dorso = 1
    elif 'M60' <= dgn < 'M80':
        teixits = 1
    elif 'M80' <= dgn < 'M96':
        ossis = 1
    return (artro, conn, dorso, teixits, ossis)

def treat_derivacions(derivacions, baixa, alta):
    if isinstance(baixa, d.datetime):
        baixa = baixa.date()
    if isinstance(alta, d.datetime):
        alta = alta.date()
    datarehab, datarealrehab, datatrauma, datarealtrauma = None, None, None, None
    for (oc, datareal, rehab) in derivacions:
        if isinstance(oc, d.datetime):
            oc = oc.date()
        if baixa <= oc <= alta:
            if rehab:
                datarehab = oc
                datarealrehab = datareal
            else:
                datatrauma = oc
                datarealtrauma = datareal
    return (datarehab, datarealrehab, datatrauma, datarealtrauma)

def treat_proves(proves, baixa, alta):
    if isinstance(baixa, d.datetime):
        baixa = baixa.date()
    if isinstance(alta, d.datetime):
        alta = alta.date()
    for (oc, datareal) in proves:
        if isinstance(oc, d.datetime):
            oc = oc.date()
        if baixa <= oc <= alta:
            return (oc, datareal)
    return (None, None)

# ups_entra = set()
# sql = "select scs_codi from cat_centres where amb_codi in ('06')"
# ups_nord = [up for up, in u.getAll(sql, 'nodrizas')]
# sql = "select scs_codi from cat_centres where amb_codi in ('05')"
# ups_sud = [up for up, in u.getAll(sql, 'nodrizas')]
sql = "select scs_codi, amb_desc, medea from cat_centres"
ups = {up: (amb, medea) for up, amb, medea in u.getAll(sql, 'nodrizas')}

sql = "select id_cip_sec, hash_d from u11"
idcips = {id: hash for id, hash in u.getAll(sql, 'import')}
u.printTime('u11')

sql = "select cip, hash_redics, hash_covid, nia from dwsisap.pdptb101_relacio_nia"
conversor = {}
hashos = {}
covid = {}
for cip, redics, hash_c, nia in u.getAll(sql, 'exadata'):
    conversor[cip] = nia
    hashos[redics] = nia
    covid[hash_c] = nia
u.printTime('pdptb101_relacio')

sql = """
    SELECT id_it, cip, edat_baixa, data_baixa, data_alta, num_dies, up, tipus_it
    FROM dwsisap.master_it
    WHERE osteomuscular = 1
    AND data_alta is not null
    AND data_alta >= DATE '2024-01-01'
    -- AND data_alta <= DATE '2024-05-31'
    AND num_dies >= 45
    AND num_dies <= 545
"""
its = c.defaultdict(dict)
ids = {}
for id, cip, edat, baixa, alta, dies, up, tipus in u.getAll(sql, 'exadata'):
    # if up in ups_nord or up in ups_sud:
    try:
        its[conversor[cip]][id] = (edat, baixa, alta, dies, up, tipus)
    except KeyError:
        continue
u.printTime('master_it')
sql = """
    SELECT eit_nsre, dgn_cdi_darrer
    FROM bislt_pro_h.ft_icam_sictbcei
    WHERE dgn_cdi_darrer like 'M%'
"""
flags = {}
exclosos = ['M79.7', 'M79.2', 'M54.81']
for id, dgn in u.getAll(sql, 'exadata'):
    flags[id] = dgn
u.printTime('diagnostics')
# sql = "select scs_codi, medea from cat_centres"
# medea = {up: medea for up, medea in u.getAll(sql, 'nodrizas')}
# u.printTime('cat_centres')
sql = """
    SELECT c_cip, c_up, c_sexe, c_gma_complexitat, c_gma_pniv
    FROM dwsisap.{}
"""
socials = {}
assignacio = {}

for hash, up, sexe, comp, pniv in u.getAll(sql.format('dbs'), 'exadata'):
    try:
        socials[(covid[hash], '2024')] = (sexe, comp, pniv)
        assignacio[covid[hash]] = up
    except KeyError:
        continue
u.printTime('dbs')
for hash, up, sexe, comp, pniv in u.getAll(sql.format('dbs_2023'), 'exadata'):
    try:
        socials[(covid[hash], '2023')] = (sexe, comp, pniv)
    except KeyError:
        continue
u.printTime('dbs_2023')
sql = """
    SELECT nia, c_sexe, c_gma_Complexitat
    FROM dwsisap.dbs_2022
"""
for nia, sexe, comp in u.getAll(sql, 'exadata'):
    try:
        socials[(nia, '2022')] = (sexe, comp)
    except KeyError:
        continue
u.printTime('dbs_2022')

sql = """
    SELECT pacient, data, up, sisap_servei_codi, sisap_servei_class, tipus_class, sisap_catprof_class
    FROM dwsisap.sisap_master_visites
    WHERE data = DATE '{data}'
"""
pre_visites = set()
visites = []
visites_ut = {}
inici = d.date(2023,1,1)
fi = d.date(2024,7,21)
while inici < fi:
    for hash_c, data, up, scodi, sclass, tclass, profclass in u.getAll(sql.format(data=inici), 'exadata'):
        try:
            if covid[hash_c] in its:
                pre_visites.add((covid[hash_c], data, up, scodi, sclass, tclass, profclass))
        except KeyError:
            continue
    # u.printTime(inici)
    inici = inici + rd.relativedelta(days=1)
u.printTime('visites')

# sql = """
#     SELECT id_cip_sec, visi_data_visita, visi_servei_codi_servei, visi_up, visi_tipus_visita
#     FROM {tb} a
#     WHERE visi_situacio_visita = 'R'
# """
# tables = u.getSubTables('visites')

# d1 = d.date(2023,6,1)
# d2 = d.date(2024,5,31)
# for tb in tables:
#     dat, = u.getOne('select visi_data_visita from {} limit 1'.format(tb), 'import')
#     if d1 <= dat <= d2:
#         u.printTime('inici ' + tb)
#         for id, data, servei, up, tipus in u.getAll(sql.format(tb=tb), 'import'):
#             try:
#                 if hashos[idcips[id]] in its:
#                     pre_visites.add((id, data, servei, up, tipus))
#                     # (entra, id_it) = treat_visites(its[hashos[idcips[id]]], data)
#                     # if entra:
#                     #     visites.append((id_it, hashos[idcips[id]], data, servei, up, tipus))
#                     #     if up == '15795':
#                     #         visites_ut.add(id_it)
#             except KeyError:
#                 continue
#         u.printTime('fi ' + tb)
# u.printTime('visites')
UTITS = ['15978', '15795', '15774', '15981']
for (id, data, up, scodi, sclass, tclass, profclass) in pre_visites:
    try:
        (entra, id_it) = treat_visites(its[id], data)
        if entra:
            visites.append((id_it, id, data, up, scodi, sclass, tclass, profclass))
            if up in UTITS:
                visites_ut[id_it] = up
    except KeyError:
        continue
u.printTime('treat visites')

pre_visites = None
cols = """(id_it int,
            nia int,
            data_visita date,
            up_visita varchar2(5),
            servei_codi varchar2(15),
            servei_class varchar2(15),
            tipus_class varchar2(10),
            prof_class varchar2(15))
        """
u.createTable('visites_utit_v21', cols, 'exadata', rm=True)
u.listToTable(visites, 'visites_utit_v21', 'exadata')
u.grantSelect('visites_utit_v21', 'DWSISAP_ROL', 'exadata')
u.printTime('visites')
visites = None

# sql = """
#     SELECT ilt_usua_cip, ilt_professio
#     FROM prttb105
#     WHERE ilt_professio is not null
# """
# professions = {}
# sectors = ['6102']
# for sector in sectors:
#     u.printTime('inici ' + sector)
#     for cip, prof in u.getAll(sql, sector):
#         try:
#             professions[conversor[cip[:13]]] = prof
#         except KeyError:
#             continue
#     u.printTime('fi ' + sector)
# u.printTime('professio')

sql = """
    SELECT hash, oc_data, der_data_real, case when codi_especialitat_final = '10123' then 1 else 0 end rehab
    FROM dwsisap.master_derivacions
    WHERE codi_especialitat_final in ('10097', '10123')
"""
derivacions = c.defaultdict(set)
for hash, oc, datareal, rehab in u.getAll(sql, 'exadata'):
    try:
        nia = covid[hash]
        derivacions[nia].add((oc, datareal, rehab))
    except KeyError:
        continue
u.printTime('derivacions')
sql = """
    SELECT hash, oc_data, der_data_real
    FROM dwsisap.master_proves
    WHERE AGRUPADOR_NIVELL_DOS LIKE 'RM musculo%'
    AND AGRUPADOR_NIVELL_DOS LIKE '%sense contrast'
"""
proves = c.defaultdict(set)
for hash, oc, datareal in u.getAll(sql, 'exadata'):
    try:
        nia = covid[hash]
        proves[nia].add((oc, datareal))
    except KeyError:
        continue
u.printTime('proves')
upload = []
for nia in its:
    for id_it in its[nia]:
        comp22, comp23, pniv23, comp24, pniv24, visita_ut, artro, conn, dorso, teixits, ossis = 0,0,0,0,0,0,0,0,0,0,0
        ambit, medea, up_ass, dgn, professio, visita_up_ut = None, None, None, None, None, None
        datarehab, datarealrehab, datatrauma, datarealtrauma, dataproves, datarealproves = None, None, None, None, None, None
        sexe22, sexe23, sexe24 = None, None, None
        if id_it in flags:
            if flags[id_it] in exclosos:
                continue
            dgn = flags[id_it]
            (artro, conn, dorso, teixits, ossis) = treat_dgn(dgn)
        (edat, baixa, alta, dies, up, tipus_it) = its[nia][id_it]
        if (nia, '2022') in socials:
            (sexe22, comp22) = socials[(nia, '2022')]
        if (nia, '2023') in socials:
            (sexe23, comp23, pniv23) = socials[(nia, '2023')]
        if (nia, '2024') in socials:
            (sexe24, comp24, pniv24) = socials[(nia, '2024')]
        if nia in assignacio:
            up_ass = assignacio[nia]
        if up_ass in ups:
            (ambit, medea) = ups[up_ass]
        if id_it in visites_ut:
            visita_ut = 1
            visita_up_ut = visites_ut[id_it]
        # if nia in professions:
        #     professio = professions[nia]
        if nia in derivacions:
            (datarehab, datarealrehab, datatrauma, datarealtrauma) = treat_derivacions(derivacions[nia], baixa, alta)
        if nia in proves:
            (dataproves, datarealproves) = treat_proves(proves[nia], baixa, alta)
        upload.append((id_it, nia, sexe24, edat, baixa, alta, dies, up_ass, ambit, comp22, comp23, pniv23, comp24, pniv24, medea, visita_ut, visita_up_ut, dgn, artro, conn, dorso, teixits, ossis, tipus_it, datarehab, datarealrehab, datatrauma, datarealtrauma, dataproves, datarealproves))
u.printTime('fi cuinetes')
cols = """(
    id_it int,
    nia int,
    sexe varchar2(1),
    edat_baixa int,
    data_baixa date,
    data_alta date,
    num_dies int,
    up_ass varchar2(5),
    ambit varchar2(40),
    complexitat_2022 decimal,
    complexitat_2023 decimal,
    pniv_2023 decimal,
    complexitat_2024 decimal,
    pniv_2024 decimal,
    medea varchar2(2),
    visita_ut int,
    up_visita_ut varchar2(5),
    diagnostic varchar2(8),
    artropatia int,
    vasculopatia int,
    dorsopatia int,
    teixits_tous int,
    ossis int,
    tipus_it varchar2(2),
    oc_data_rehab date,
    data_real_rehab date,
    oc_data_trauma date,
    data_real_trauma date,
    oc_data_proves date,
    data_real_proves date
)"""
u.createTable('analisi_utit_v21', cols, 'exadata', rm=True)
u.listToTable(upload, 'analisi_utit_v21', 'exadata')
u.grantSelect('analisi_utit_v21', 'DWSISAP_ROL', 'exadata')

u.printTime('fi')