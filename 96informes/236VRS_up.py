# -*- coding: utf8 -*-

"""
Bronquiolitis i VRS en nens
"""

import collections as c
import datetime as d
import sys
from datetime import datetime

import sisapUtils as u


def get_nivell(edat):
    """."""
    nivell = 'INF1C' if edat < 3 else  'INF2C' if edat <6 else 'PRI' if edat < 12 else 'ESO' if edat <16 else 'BAX' if edat <18 else 'ADULTS'
    return (nivell)

tb = 'sisap_vrs_surveillance_up'
db = 'permanent'

codis = '("J21.9","J21","B97.4","J21.0","C01-B97.4","C01-J21.0","C01-J21.9","C01-J21")'

class Pneumonies(object):
    """."""

    def __init__(self):
        """."""
        self.pneumo = c.Counter()
        for self.sector in u.sectors:
            print self.sector
            self.get_assignada()
            self.get_refredats()
        self.export_data()
    
    def get_assignada(self):
        """."""
        self.pob = {}
        sql = "select usua_cip, usua_sexe,  to_date(usua_data_naixement, 'J'), usua_uab_up from usutb040"
        for id, sexe, naix, up in u.getAll(sql, self.sector):
            self.pob[id] = {'sexe': sexe, 'naix': naix, 'up': up}
    
    def get_refredats(self):
        """."""
        u.printTime("problemes")
        
        sql = "select cip_usuari_cip, to_date(pr_dde), pr_cod_ps, pr_th \
           from prstb015, usutb011 \
           where pr_cod_u=cip_cip_anterior and pr_cod_o_ps = 'C' and \
                 pr_cod_ps in ('J21.9','J21','B97.4','J21.0','C01-B97.4','C01-J21.0','C01-J21.9','C01-J21') and \
                 to_char(pr_dde, 'YYYYMM') >'202009' and \
                 pr_data_baixa is null \
                 group by cip_usuari_cip, pr_dde, pr_cod_ps, pr_th".format(codis)
        
        for id, dde, ps, th in u.getAll(sql, self.sector):
            sexe = self.pob[id]['sexe'] if id in self.pob else None
            naix = self.pob[id]['naix'] if id in self.pob else None
            up = self.pob[id]['up'] if id in self.pob else None
            try:
                ed = u.yearsBetween(naix, dde)
            except:
                ed = None 
            grup = None
            nivell = None
            if ed != None:
                if int(ed) < 15:
                    grup = 0
                elif 15 <= int(ed) < 65:
                    grup = 1
                elif 64 < int(ed) < 75:
                    grup = 2
                elif int(ed)>74:
                    grup = 3
                else:
                    grup = 9
                nivell = get_nivell(ed) 
            self.pneumo[(dde, ed, sexe, up, nivell)] += 1

    def export_data(self):
        """."""
        cols = ("data date", "grup_edat int", "sexe varchar(10)", "nivell varchar(10)","up varchar(10)","recompte int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        
        upload = []
        for (dde,  grup, sexe,up,  nivell), n in self.pneumo.items():
            n = int (n)
            upload.append([dde, grup, sexe, up, nivell, n])
        u.listToTable(upload, tb, db)

                     
if __name__ == '__main__':
    u.printTime("Inici")
     
    Pneumonies()
    
    u.printTime("Final")                 
