# -*- coding: utf8 -*-

"""
Traiem dades per abs de residència dels nens que estan en LP o equips que no tenen nens
"""

import collections as c

import sisapUtils as u


class HEPAT(object):
    """."""

    def __init__(self):
        """."""
        self.dades = []
        self.municipis = {}
        self.nacionalitats = {}
        self.get_centres()
        for sector in u.sectors:
            self.get_municipis(sector)
            self.get_nacionalitat(sector)
            self.get_dades(sector)
        self.export_data()

    def get_centres(self):
        """Trec descripció centre de cat_Centres i de abs"""
        self.centres = {}
        self.cat_abs = {}
        sql = 'select ics_desc, scs_codi, abs from cat_centres'
        for desc, up, abs in u.getAll(sql, 'nodrizas'):
            self.centres[up] = desc
            self.cat_abs[abs] = desc
        
    def get_municipis(self, sector):
        """Trec els municipis del catàleg ECAP"""
        
        sql = "select muni_codi, muni_descripcio from rittb028 where muni_data_baixa=1"
        for codi, desc in u.getAll(sql, sector):
            self.municipis[codi] = desc
    
    def get_nacionalitat(self, sector):
        """Trec la descripció de la nacionalitat del catàleg"""
        
        sql = "select resi_codi,resi_descripcio from rittb034 where resi_data_baixa=1"
        for codi, desc in u.getAll(sql, sector):
            self.nacionalitats[codi] = desc
    
    
    def get_dades(self, sector):
        """."""

        sql2 = "select usua_uab_up as up, usua_cip as cip, usua_cognom1 as cognom1, usua_cognom2 as cognom2, usua_nom as nom, \
                usua_telefon_principal, usua_telefon_secundari, \
                usua_sexe as sexe, \
                2017-extract(YEAR FROM  to_date(usua_data_naixement,'J')) as edat_17, \
                usua_nacionalitat as nacionalitat,\
                usua_abs_codi_abs, \
                substr(usua_localitat,1,5) as municipi,\
                usua_uab_up \
                from usutb040 where usua_abs_codi_abs in ( \
                    387, 218, 384, 40, 342, 61, 305  \
                    ) AND usua_situacio = 'A' AND usua_usuari= 'S' \
                    AND extract(YEAR FROM  to_date(usua_data_naixement,'J'))  BETWEEN 1937 AND 2015 \
                    and usua_telefon_principal is not null and usua_uab_up is not null"
        for row in u.getAll(sql2, sector):
            if row[8] < 15:
                desc_abs = self.cat_abs[row[10]]
                desc_up = self.centres[row[12]]
                municipi = self.municipis[row[11]] if row[11] in self.municipis else row[11]
                nacionalitat = self.nacionalitats[row[9]] if row[9] in self.nacionalitats else row[9]
                self.dades.append([row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7], row[8], municipi, nacionalitat, row[10], desc_abs, row[12], desc_up])

        

    def export_data(self):
        """."""
        #header_full = [('up', 'cip', 'cognom1', 'cognom2', 'nom', 'tlf1', 'tlf2', 'sexe', 'edat_17', 'municipi', 'nacionalitat', 'abs', 'up', 'desc_abs')]
        #u.writeCSV(u.tempFolder + 'seroprev_full.txt', header_full + sorted(self.dades))
        header = [('tlf','tlf2','sexe','edat_17',  'municipi', 'nacionalitat', 'abs', 'desc_abs', 'up', 'desc_up')]
        u.writeCSV(u.tempFolder + 'seroprev2018_infantil.txt', header + sorted([row[5:15] for row in self.dades]))


if __name__ == '__main__':
    HEPAT()
