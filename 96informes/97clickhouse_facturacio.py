# -*- coding: utf8 -*-

"""
.
"""

import datetime as d
import multiprocessing as m
import urllib2

import sisaptools as u


tb = 'facturacio'
db_o = ('nymeria_d', 'sidiap_data')
db_d = 'testing'
engine = 'Memory'
url = 'http://10.80.217.84:8123'
nod = ('p2262', 'nodrizas')


def create_clickhouse():
    """."""
    for stm in (
        'CREATE DATABASE IF NOT EXISTS {}'.format(db_d),
        'DROP TABLE IF EXISTS {}.{}'.format(db_d, tb),
        'CREATE TABLE {}.{} (id UInt32, any UInt16, mes UInt8, \
                             atc1 String, atc3 String, atc5 String, \
                             atc7 String, envasos UInt8, import Float32, \
                             br String) \
                            ENGINE = {}'.format(db_d, tb, engine),
    ):
        response = urllib2.urlopen(url, stm)


def mariadb_to_clickhouse():
    """."""
    pool = m.Pool(16)
    for p in range(32):
        pool.apply_async(Partition, args=(p,))
    pool.close()
    pool.join()


class Partition(object):
    """."""

    def __init__(self, partition):
        """."""
        self.partition = partition
        self.get_centres()
        self.get_data()
        self.put_data()

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres where ep = '0208'"
        self.centres = {up: br for (up, br) in u.Database(*nod).get_all(sql)}

    def get_data(self):
        """."""
        sql = 'select id_cip, rec_any_mes, pf_cod_atc, rec_envasos, \
                      rec_impliq, rec_up_agr \
               from facturacio partition (p{})'.format(self.partition)
        self.dades = []
        i = 0
        for id, dat, atc, env, pvp, up in u.Database(*db_o).get_all(sql):
            if up in self.centres:
                self.dades.append('\t'.join(map(str,
                                                [id, dat[:4], int(dat[4:]),
                                                 atc[:1], atc[:3], atc[:5],
                                                 atc, env, pvp,
                                                 self.centres[up]])))
                i += 1
                if i % 10**6 == 0:
                    self.put_data()
        self.put_data()

    def put_data(self):
        """."""
        query = 'INSERT INTO {}.{} FORMAT TabSeparated'.format(db_d, tb)
        stm = '{}?query={}'.format(url, query).replace(' ', '%20')
        response = urllib2.urlopen(stm, '\n'.join(self.dades))
        self.dades = []


if __name__ == '__main__':
    inici = d.datetime.now()
    create_clickhouse()
    mariadb_to_clickhouse()
    print d.datetime.now() - inici
