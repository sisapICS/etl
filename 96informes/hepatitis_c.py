# flake8: noqa

import sisaptools as u

import collections as c
import datetime as d

TABLE_NAME_1 = 'RPT_CMBD_COPIA'
TABLE_NAME = 'HEPATITIS_C'
DATABASE = ("exadata", "data")
DATABASE_PDP = ("redics", "pdp")
ESTAT = '3'

def create_table_copy():
    cols = ("nia varchar2(22)", "data_naixement date", "diagnostic_primer varchar2(5)",
                "data_primer date",
                "font_cmbd_primer varchar2(2)", "mncp varchar2(5)",
                "estat varchar2(1)", "sexe varchar2(1)",
                "diagnostic_darrer varchar2(5)", "data_darrer date",
                "font_cmbd_darrer varchar2(2)", "eap varchar2(4)",
                "data_importacio date")
    sql = "CREATE TABLE {} as select * from dwrsa.RPT_CMBD".format(TABLE_NAME_1)
    u.Database(*DATABASE).execute(sql)
    sql = ("grant select on {} to DWSISAP_ROL".format(TABLE_NAME_1))
    u.Database(*DATABASE).execute(sql)

def create_table_hepatits():
    cols = ("cip varchar2(13)", "ecap_up varchar2(5)", "ecap_uba varchar2(5)", "ecap_inf varchar2(5)", "sector varchar2(5)")
    sql = "CREATE TABLE {} ({})".format(TABLE_NAME, ", ".join(cols))
    u.Database(*DATABASE).execute(sql)
    sql = ("grant select on {} to DWSISAP_ROL".format(TABLE_NAME))
    u.Database(*DATABASE).execute(sql)

def insert_data_hepatitis():
    sql = 'INSERT INTO {} SELECT substr(b.cip,1,13), c.ecap_up, c.ECAP_UBA, c.ecap_inf, c.ecap_sector \
            FROM dwsisap.rpt_cmbd_copia a, dwsisap.rca_cip_nia B, dwsisap.dbc_POBLACIO c \
            WHERE a.nia = b.nia \
            AND a.estat = {} \
            AND substr(b.cip,1,13) = c.CIP \
            AND c.ECAP_UBA IS NOT NULL \
            AND c.ECAP_UP IS NOT NULL'.format(TABLE_NAME, ESTAT)
    u.Database(*DATABASE).execute(sql)

def delete_table():
    sql = 'DROP TABLE {}'.format(TABLE_NAME)
    u.Database(*DATABASE).execute(sql)

def to_pdp():
    sql = """SELECT * FROM DWSISAP.HEPATITIS_C"""
    upload = []
    for cip, ecap_up, ecap_uba, uba_inf, sector in u.Database("exadata", "data").get_all(sql):          
        this = (cip, ecap_up, ecap_uba, uba_inf, sector)
        upload.append(this)
        
    with u.Database("redics", "pdp") as pdp:
        sql = ("DROP TABLE SISAP_COVID_HEPATITIS_C")
        pdp.execute(sql)
        cols = ("cip varchar2(40)", "up varchar2(5)", "uba varchar2(10)", "uba_inf varchar2(10)", "sector varchar2(4)")
        sql = ("CREATE TABLE {} ({})".format('SISAP_COVID_HEPATITIS_C', ", ".join(cols)))  # noqa
        pdp.execute(sql)
        pdp.list_to_table(upload, 'SISAP_COVID_HEPATITIS_C')
        

if __name__ == '__main__':
    delete_table()
    create_table_hepatits()
    insert_data_hepatitis()
    to_pdp()
