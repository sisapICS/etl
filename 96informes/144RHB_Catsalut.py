# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
import datetime
from collections import defaultdict,Counter


nod = 'nodrizas'
imp = 'import'


assig = {}

sql = 'select id_cip_sec, usua_uab_up from assignada'
for id, up in getAll(sql, imp):
    assig [id] = up
    
descripcions = {}  
sql = 'select codi_sector,ps_cod, ps_des,ps_grup3 from cat_sisap_rehtb001'
for sector, cod, d, g4 in getAll(sql, imp):
    descripcions[(sector, cod)] = {'desc': d, 'g4':g4}
    
c_rhb = {}
sql = 'select	a.amb_codi_amb	,a.amb_desc_amb	,s.dap_codi_dap	,s.dap_desc_dap	,c.up_codi_up_scs	,u.up_codi_up_ics,u.up_desc_up_ics \
        from	cat_gcctb006 s,cat_gcctb008 c,cat_gcctb007 u,cat_gcctb005 a\
        where u.dap_codi_dap = s.dap_codi_dap\
        and c.up_codi_up_ics = u.up_codi_up_ics\
		and s.amb_codi_amb=a.amb_codi_amb\
        and c.up_data_baixa = 0 \
        and dap_data_baixa = 0 \
        and u.up_data_baixa = 0 \
        and amb_data_baixa = 0'    
 
for amb, ambdesc,dap,dapdesc,up,br,desc in getAll(sql, imp):
    c_rhb[up] = {'br':br, 'desc':desc}
    
sql = 'select u_pr_cod_up, u_pr_descripcio from cat_pritb008'
for up, desc in getAll(sql, imp):
     c_rhb[up] = {'br':'', 'desc':desc}
    

no_ass = 0
sessions = Counter()
sql = "select codi_sector, id_cip_sec, vis_fit_id, vis_up, fit_tipus_prof, fit_probs_salut_first, extract(year_month from vis_data) from nod_rhb where year(vis_data)='2016' AND VIS_REALITZADA='S'"
for sec, id, fitid, up, tipus, dx, periode in getAll(sql, nod):
    try:
        up_ass = assig[id]
    except KeyError:
        no_ass += 1
    sessions[(periode, sec, up, up_ass, tipus,dx)] += 1
 

upload = []
 
for (periode, sec, up, up_ass, tipus,dx), t in sessions.items():
    desc1 = c_rhb[up]['desc']
    try:
        desc2 = c_rhb[up_ass]['desc']
    except KeyError:
        desc2 = ''
    descp = descripcions[(sec, dx)]['desc']
    g4 = descripcions[(sec, dx)]['g4']
    upload.append([periode, tipus, up,desc1, up_ass, desc2, dx,descp,g4, t])
  
file = tempFolder + 'RHB.txt'
writeCSV(file, upload, sep=';')
print 'Pacients no assignats: ', no_ass
