import sisapUtils as u
import sisaptools as t
import datetime as d
import collections as c

print('inici')

sql = """
    SELECT id_cip_sec, up
    FROM assignada_tot
    WHERE maca = 1
"""

pob = {id: up for id, up in u.getAll(sql, 'nodrizas')}
print('pob')
sql = """
    SELECT id_cip_sec
    FROM eqa_problemes
    WHERE ps in (521, 722)
"""

neos = {id for id, in u.getAll(sql, 'nodrizas')}
print('neos')
sql = """
    SELECT up_cod, up_des, aga_cod, aga_des, regio_cod, regio_des
    FROM dwsisap.dbc_centres
"""
centres = {up: (up_des, aga, aga_des, rs, rs_des) for up, up_des, aga, aga_des, rs, rs_des in u.getAll(sql, 'exadata')}
print('centres')
res = c.Counter()
for id in pob:
    up = pob[id]
    if up in centres:
        (up_des, aga, aga_des, rs, rs_des) = centres[up]
        res[(up, up_des, aga, aga_des, rs, rs_des, 'DEN')] += 1
        if id in neos:
            res[(up, up_des, aga, aga_des, rs, rs_des, 'NUM')] += 1
upload = []
for (up, up_des, aga, aga_des, rs, rs_des, dim), val in res.items():
    upload.append((up, up_des, aga, aga_des, rs, rs_des, dim, val))

cols = [("UP", 'UP_DES', 'AGA', 'AGA_DES', 'RS', 'RS_DES', 'ANALISI', 'VALOR')]

u.writeCSV('aiss_maca.csv', cols+upload, sep=';')
print('fi')
    