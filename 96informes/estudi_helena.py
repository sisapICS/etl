import sisapUtils as u
from datetime import datetime, timedelta


class Dades_Helena():
    def __init__(self):
        self.pob_interes()
        self.get_dades()

    def pob_interes(self):
        # poblaci�
        # CREATE TABLE POB_ESTUDI_HELENA AS (
        # SELECT DISTINCT id FROM dwtw.VARIABLES v WHERE 
        # DATA >= DATE '2022-11-01'
        # AND codi IN ('VK405', 'YK2002')
        # AND id IN (SELECT CIP FROM dwsisap.DBC_POBLACIO dp WHERE EDAT >= 50))
        sql = """SELECT id, HASH_REDICS  FROM dwtw.POB_ESTUDI_HELENA a
                    INNER JOIN dwsisap.PDPTB101_RELACIO_nia b
                    ON a.id = b.cip"""
        self.pob_cip = set()
        self.pob_hash = set()
        self.hash_2_cip = {}
        for cip, hash in u.getAll(sql, 'exadata'):
            self.pob_cip.add(cip)
            self.pob_hash.add(hash)
            self.hash_2_cip[hash] = cip
        
        self.id_cip_2_hash = {}
        sql = """select id_cip_sec, hash_d from import.u11"""
        for id_cip, hash in u.getAll(sql, 'import'):
            if hash in self.pob_hash:
                self.id_cip_2_hash[id_cip] = hash

    def get_dades(self):
        db = 'exadata'
        dades_up = []
        tb_name = 'up_assignada_estudi_helena'
        cols = """(id varchar(13), up varchar(5), data_ass date, data_final date)"""
        sql = """select id_cip_sec, huab_up_codi, huab_data_ass, huab_data_final  from import.moviments"""
        sql_sectors = """SELECT HUAB_USUA_CIP, huab_up_codi, huab_data_ass, huab_data_final FROM vistb050"""
        for id, up, data_ass, data_final in u.getAll(sql, 'import'):
            if id in self.id_cip_2_hash:
                hash = self.id_cip_2_hash[id]
                if hash in self.hash_2_cip:
                    cip = self.hash_2_cip[hash]
                    dades_up.append((cip, up, data_ass, data_final))
        u.createTable(tb_name, cols, db, rm=True)
        u.listToTable(dades_up, tb_name, db)
        u.execute("grant select on {} to DWSISAP_ROL".format(tb_name), 'exadata')

        print('lab')
        dades_up = []
        tb_name = 'lab_estudi_helena'
        cols = """(id varchar2(13), prova varchar2(15), data_ass date, valor number)"""
        sql = """SELECT id_cip_sec, 
                    case when agrupador = 9 then 'LDL' 
                        when agrupador = 31 then 'TGL'
                        when agrupador = 197 then 'CT'
                        when agrupador = 875 then 'CREAT'
                        when agrupador = 30 then 'FG' end prova,
                        data_var, valor
                        FROM nodrizas.eqa_variables where agrupador
                        in (9, 31, 197, 875, 30)
                        and data_var >= date '2022-11-01'"""
        for id, prova, data_prova, valor in u.getAll(sql, 'import'):
            if id in self.id_cip_2_hash:
                hash = self.id_cip_2_hash[id]
                if hash in self.hash_2_cip:
                    cip = self.hash_2_cip[hash]
                    dades_up.append((cip, prova, data_prova, valor))
        u.createTable(tb_name, cols, db, rm=True)
        u.listToTable(dades_up, tb_name, db)
        u.execute("grant select on {} to DWSISAP_ROL".format(tb_name), 'exadata')
        u.execute("grant all on LAB_ESTUDI_HELENA to DWTW", 'exadata')
        # CREATE TABLE laboratori_hdl_helena AS (
        # SELECT substr(id, 1, 13) AS id, 'HDL' AS prova, data_alta, RESULTAT 
        # FROM dwtw.LABORATORI_DETALL 
        # WHERE DATA >= DATE '2022-11-01'
        # AND DATA_ALTA >= date '2022-11-01'
        # AND PROVA IN ('Q12785', 'R12785')
        # AND substr(id, 1, 13) IN (SELECT id FROM dwtw.POB_ESTUDI_HELENA));

        # INSERT INTO dwsisap.LAB_ESTUDI_HELENA
        # SELECT * FROM dwtw.laboratori_hdl_helena;

        print('tabac')
        dades_up = []
        tb_name = 'tabac_estudi_helena'
        cols = """(id  varchar(13), valor number, dalta date, dbaixa date, dlast date)"""
        sql = """select id_cip_sec, tab, dalta, dbaixa, dlast from nodrizas.eqa_tabac"""
        for id, prova, dataalta, darabaixa, datalast in u.getAll(sql, 'import'):
            if id in self.id_cip_2_hash:
                hash = self.id_cip_2_hash[id]
                if hash in self.hash_2_cip:
                    cip = self.hash_2_cip[hash]
                    dades_up.append((cip, prova, dataalta, darabaixa, datalast))
        u.createTable(tb_name, cols, db, rm=True)
        u.listToTable(dades_up, tb_name, db)
        u.execute("grant select on {} to DWSISAP_ROL".format(tb_name), 'exadata')

        print('tractaments_dispensats')
        tb_name = 'tract_dispensats_estudi_helena'
        cols = """(hash_redics  varchar(40), mes varchar2(6), atc varchar2(7), codi_pf number, envasos number)"""
        u.createTable(tb_name, cols, db, rm=True)
        u.execute("grant select on {} to DWSISAP_ROL".format(tb_name), 'exadata')
        # Start and end dates
        start_date = datetime(2022, 11, 1)
        end_date = datetime(2024, 12, 1)

        # Generate the sequence
        date_sequence = []
        current_date = start_date
        while current_date <= end_date:
            date_sequence.append(current_date.strftime("%Y%m"))  # Format as YYYYMM
            # Increment by one month
            next_month = current_date.month % 12 + 1
            next_year = current_date.year + (current_date.month // 12)
            current_date = datetime(next_year, next_month, 1)
        sql = """select rec_cip, REC_ANY_MES, PF_COD_ATC, REC_PF_CODI, REC_ENVASOS  
                    from sidics.fartb201
                    where REC_ANY_MES = '{}'"""
        for p in date_sequence:
            dades_up = []
            print(p, sql.format(p))
            for hash, mes, atc, codi_pf, envasos in u.getAll(sql.format(p), ("dbs", "x0002")):
                if hash in self.pob_hash:
                    dades_up.append((hash, mes, atc, codi_pf, envasos))
            u.listToTable(dades_up, tb_name, db)
        

        # prescripcions
        # CREATE TABLE dwtw.prescripcions_estudi_helena AS (
        # SELECT PMC_USUARI_CIP, DATA_INICI, ATCCODI, PF_CODI, durada, NUM_ENV, FREQ, POSOLOGIA FROM dwtw.PRESCRIPCIONS p 
        # WHERE PMC_USUARI_CIP IN (SELECT id FROM dwtw.POB_ESTUDI_HELENA)
        # AND DATA >= DATE '2022-11-01' AND DATA_INICI >= DATE '2022-11-01')
        
        # variables
        # select id, 
        # CASE WHEN codi = 'EK201' THEN 'TAS'
        #         WHEN codi = 'EK202' THEN 'TAD'
        # WHEN codi = 'TT103' THEN 'IMC'
        # WHEN codi = 'TT102' THEN 'Pes'
        # WHEN codi = 'TT101' THEN 'Talla' 
        # WHEN codi = 'VK405' THEN 'REGICOR' 
        # WHEN codi = 'YK2002' THEN 'NOU REGICOR' 
        # WHEN codi = 'YK2001' THEN 'REASON' 
        # ELSE 'ITB'
        # END variable,
        # data_act, VALOR from dwtw.VARIABLES v WHERE DATA >= DATE '2022-11-01'
        # AND data_act >= DATE '2022-11-01'
        # and codi IN ('EK201', 'EK202', 'TT103', 'TT102', 'TT101', 'VK405', 'TK201D', 'TK201E', 'YK2002', 'YK2001')
        # AND ID IN (SELECT id FROM DWTW.POB_ESTUDI_HELENA))
        # sql = """insert into dwtw.variables_estudi_helena (SELECT SUBSTR(CIP,1,13), 'ITB_IS3', DATA, VALUE_ARCH fROM
        # DWCATSALUT.FORMULARIS_REGICOR WHERE codi = 'V000000015190' AND SUBSTR(CIP,1,13) IN (SELECT ID FROM DWTW.POB_ESTUDI_HELENA))"""


        # sql = """CREATE TABLE hosp_no_ics_estudi_helena AS (
        #     SELECT RAWTOHEX(STANDARD_HASH(b.cip, 'SHA256')) AS id_hash, a.data_ingres FROM DWCATSALUT.TF_CMBDHA a
        #     INNER JOIN dwsisap.PDPTB101_RELACIO_nia b
        #     ON a.numero_id = b.nia
        #     WHERE DATA_INGRES >= DATE '2022-11-01'
        #     AND NUMERO_ID IN (SELECT nia FROM dwtw.POB_ESTUDI_HELENA a
        #     INNER JOIN dwsisap.PDPTB101_RELACIO_nia B
        #     ON a.id = b.cip
        #     )
        #     AND b.cip IN 
        #     (SELECT CIP FROM DWSISAP.DBC_POBLACIO WHERE RCA_UP IN (
        #     SELECT UP_COD FROM DWSISAP.DBC_CENTRES dc WHERE ES_ICS = 1))
        #     AND UP_ASSISTENCIA IN (SELECT up_cod FROM dwsisap.DBC_RUP dr WHERE TIP_COD = 10 AND es_ics = 0))"""

        # sql = """grant select on hosp_no_ics_estudi_helena to DWSISAP_ROL"""
        # sql = """grant select on hosp_no_ics_estudi_helena to DWAQUAS"""

if __name__ == "__main__":
    Dades_Helena()

