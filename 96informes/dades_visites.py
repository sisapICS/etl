import sisapUtils as u
import collections as c

u.printTime('inici')
sql = """
    SELECT UP, MODUL, EXTRACT(MONTH FROM DATA) AS MONTH, EXTRACT(YEAR FROM DATA) AS YEAR, count(1)
    FROM dwsisap.sisap_master_visites
    WHERE DATA BETWEEN DATE '2022-01-01' AND DATE '2023-12-31'
    AND situacio = 'R'
    AND atribut1 = '2'
    AND atribut2 = '4'
    GROUP BY UP, MODUL, EXTRACT(MONTH FROM DATA), EXTRACT(YEAR FROM DATA)
    ORDER BY UP, MODUL, EXTRACT(YEAR FROM DATA), EXTRACT(MONTH FROM DATA)
"""
visites = c.defaultdict(set)
for up, modul, mes, any, val in u.getAll(sql, 'exadata'):
    visites[up].add((modul, mes, any, val))
u.printTime('visites')
sql = """
    SELECT scs_codi, ics_desc, sap_desc, amb_desc
    FROM cat_centres
"""
ups = c.defaultdict()
for up, up_desc, sap, amb in u.getAll(sql, 'nodrizas'):
    ups[up] = (up_desc, sap, amb)
u.printTime('ups')
upload = []
for up in visites:
    if up in ups:
        up_desc = ups[up][0]
        sap = ups[up][1]
        amb = ups[up][2]
        print(up_desc + ' ' + sap + ' ' + amb)
        for (modul, mes, any, val) in visites[up]:
            upload.append((up, up_desc, sap, amb, modul, mes, any, val))
u.printTime('join')
cols = "(up varchar(10), up_desc varchar(40), sap_desc varchar(40), amb_desc varchar(40), modul varchar(5), mes int, year int, valor int)"
u.createTable('peticio_visites', cols, 'altres', rm=True)
u.listToTable(upload, 'peticio_visites', 'altres')
u.printTime('fi')