# coding: latin1

"""
Variables de viol�ncia de g�nere 2018
"""

import sisapUtils as u


class Violencia(object):
    """."""

    def __init__(self):
        """."""
        self.get_up()
        self.get_variables()
        self.taula_final()
        self.export_taula()
        
    def get_up(self):
        """."""
        self.up = {}
        self.recompte_pob_up = {}
        sql = "select id_cip_sec, up from assignada_tot"
        for id, up in u.getAll(sql, "nodrizas"):
            self.up[id] = up
            key = self.up[id]
            if key not in self.recompte_pob_up:
                self.recompte_pob_up[key] = 1
            else:
                self.recompte_pob_up[key] += 1
                
    def get_variables(self):
        """."""
        self.variables = {}
        self.recompte_var = {}
        
        sql = "select id_cip_sec, vu_cod_vs from variables1 \
               where vu_cod_vs in ('EP3001','VP3001') and year(vu_dat_act)=2018 \
               and vu_dat_act >= 2018-10-03 and vu_val > 0"
        for id, var in u.getAll(sql, "import"):
            if id in self.up:
                self.variables[id] = (var, self.up[id])
            else:
                self.variables[id] = (var, '00000')
            key = self.variables[id]
            if key not in self.recompte_var:
                self.recompte_var[key] = 1
            else:
                self.recompte_var[key] += 1

    def taula_final(self):
        """."""      
        self.taula_final = {}
        for id in self.recompte_var:
            if id[1] == '00000':
                self.taula_final[id] = (id[0], id[1], self.recompte_var[id], \
                                        '00000')
            else:
                self.taula_final[id] = (id[0], id[1], self.recompte_var[id], \
                                        self.recompte_pob_up[id[1]])

    def export_taula(self):
        """."""
        upload = self.taula_final.values()
        u.writeCSV(u.tempFolder + "violencia_vars.csv", upload, sep=";")
      
if __name__ == "__main__":
    Violencia()
