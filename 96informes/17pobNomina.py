# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

db = "altres"
nod = "nodrizas"
imp = "import"

origens = {
    'general': {'assig':'assignada_tot','centres':'cat_centres'},
    'jail': {'assig':'jail_assignada','centres':'jail_centres'}
}

OutFile = tempFolder + 'nomina.txt'
taula = "mst_nomina"

centres = {}
sql = "select scs_codi from cat_centres where ep='0208'"
for up, in getAll(sql,nod):
    centres[up] = True

professionals = {}
sql = "select sector,ide_numcol,ide_dni,up,uab,nom,cognom1,cognom2,lloc_treball from cat_professionals where servei in ('MG','PED')"
for sector,numcol,dni,up,uba,nom,cognom1,cognom2,lltreball in getAll(sql,imp):
    try:
        if centres[up]:
            professionals[(up,uba)] = {'numcol':numcol,'dni':getNif(dni),'sector':sector,'nom':nom,'cognom1':cognom1,'cognom2':cognom2,'lloc':lltreball}
    except KeyError:
        continue
    
def siapConverter(edat):
    if edat > 74:
        return 75
    elif 65<= edat <= 74:
        return 65
    elif 45<= edat <= 64:
        return 45
    elif 15<= edat <= 44:
        return 15
    elif 8<= edat <= 14:
        return 8
    elif 3<= edat <= 7:
        return 3
    elif 0<= edat <= 2:
        return 0

nomina = Counter()     
nominaTot = Counter()
sql = 'select up, uba, edat from assignada_tot'
for up,uba,edat in getAll(sql,nod):
    grupEdat = siapConverter(edat)
    if (up,uba) in nomina:
        nomina[(up,uba)][grupEdat] += 1
    else:
        nomina[(up,uba)] = {0:0,3:0,8:0,15:0,45:0,65:0,75:0}
        nomina[(up,uba)][grupEdat] += 1
    nominaTot[(up,uba)] += 1
 
try:
    remove(OutFile)
except:
    pass 
    
with openCSV(OutFile) as c:
    for (up,uba),count in nomina.items():
        total = nominaTot[(up,uba)]
        try:
            sector = professionals[(up,uba)]['sector']
            numcol = professionals[(up,uba)]['numcol']
            lloc = professionals[(up,uba)]['lloc']
            dni = professionals[(up,uba)]['dni']
            nom = professionals[(up,uba)]['nom']
            cognom1 = professionals[(up,uba)]['cognom1']
            cognom2 = professionals[(up,uba)]['cognom2']
        except KeyError:
            continue
        c.writerow([sector,up,lloc,dni,numcol,nom,cognom1,cognom2,count[0],count[3],count[8],count[15],count[45],count[65],count[75],total,uba])


        
    
