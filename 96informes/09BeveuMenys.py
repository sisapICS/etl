# coding: iso-8859-1
#Joan Colom Farran, Departament (ion 3558), abril15, Ermengol
from sisapUtils import *
import csv,os,sys
from time import strftime

debug = False
historic = False
anyCalcul = '2010'

nod="nodrizas"
imp="import"
db="test"
eqa="eqa_ind"

dext="dextraccio"
catcen="cat_centres"
variables="variables"
teqa="mst_indicadors_pacient"

sql='select data_ext from {}'.format(dext)
for d in getOne(sql, nod):
    dback=d

dCalcul = anyCalcul +'/12/31'

print dCalcul
sql = "update {0} set data_ext='{1}'".format(dext,dCalcul)
execute(sql,nod)

ics='where ep=0208'
varOH = "('ALRIS','ALSET','ALHAB','ALDIA','CP202')"
psOH = "('F10','F10.1','F10.2','F10.0','F10.3','F10.4','F10.5','F10.6','F10.7','F10.8','F10.9','T51.9','Z50.2','Z71.4','Z72.1')"
indEQA='EQA0301A'

f10={}
altF10=['F10.0','F10.3','F10.4','F10.5','F10.6','F10.7','F10.8','F10.9']
for a in altF10:
    f10[a]=True
   
sql="select date_format(data_ext,'%Y%m') from {}".format(dext)
for d in getOne(sql, nod):
    dcalcul=d

OutFile= tempFolder + '09PetiBeveuMenys_' + dcalcul + '.txt'
OutFilePS= tempFolder + '09PetiBeveuMenysPS_' + dcalcul + '.txt'
OutFileEQA= tempFolder + '09PetiBeveuMenysEQA_' + dcalcul + '.txt'
OutFilePob= tempFolder + '09PetiBeveuMenysPob_' + dcalcul + '.txt'

printTime('Pob')

centres={}
c=0
sql="select scs_codi,ics_codi,ics_desc,amb_desc from {0} {1}".format(catcen,ics)
for up,br,ics_desc,amb_desc in getAll(sql,nod):
    centres[up]={'br':br,'desc':ics_desc, 'amb':amb_desc}
    
sql='select data_ext from {}'.format(dext)
for d in getOne(sql, nod):
    dara=d

dadesHist = {}
if historic:
    sql = "select id_cip_sec,(year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')),usua_sexe from assignada,nodrizas.dextraccio"
    for id,edat,sexe in getAll(sql,imp):
        edat = int(edat)
        dadesHist[id] = {'edat':edat,'sexe':sexe}

def getPacients():

    assig= {}
    if historic:
        sql= "select id_cip_sec,up from assignadaHistorica where dataany = '{}' {}".format(anyCalcul,' limit 10' if debug else '')
        for id_cip_sec,up in getAll(sql,imp):
            try:
                edat = dadesHist[id_cip_sec]['edat']
                sexe = dadesHist[id_cip_sec]['sexe']
            except KeyError:
                continue
            edat=int(edat)
            if edat > 14:
                if 15 <= edat <= 29:
                    gedat='Entre 15 i 29'
                if 30<=edat<=45:
                    gedat='Entre 30 i 45'
                if 46<=edat<=64:
                    gedat='Entre 46 i 64'
                if 65<=edat<=79:
                    gedat='Entre 65 i 79'
                if edat >79:
                    gedat='80 o +'
                try:
                    br=centres[up]['br']
                    idesc=centres[up]['desc']
                    ambit=centres[up]['amb']
                    assig[int(id_cip_sec)]= {'up':up,'br':br,'desc':idesc,'amb':ambit,'edat':edat,'sex':sexe,'GrupEdat':gedat}
                except KeyError:
                    continue
    else:
        sql= "select id_cip_sec,up,edat,sexe from assignada_tot"
        for id_cip_sec,up,edat,sexe in getAll(sql,nod):
            edat=int(edat)
            if edat > 14:
                if 15 <= edat <= 29:
                    gedat='Entre 15 i 29'
                if 30<=edat<=45:
                    gedat='Entre 30 i 45'
                if 46<=edat<=64:
                    gedat='Entre 46 i 64'
                if 65<=edat<=79:
                    gedat='Entre 65 i 79'
                if edat >79:
                    gedat='80 o +'
                try:
                    br=centres[up]['br']
                    idesc=centres[up]['desc']
                    ambit=centres[up]['amb']
                    assig[int(id_cip_sec)]= {'up':up,'br':br,'desc':idesc,'amb':ambit,'edat':edat,'sex':sexe,'GrupEdat':gedat}
                except KeyError:
                    continue
    return assig

ass = getPacients()

printTime('Variables')
alris={}
sql="select id_cip_sec,vu_cod_vs,vu_val,vu_dat_act from {0} where vu_cod_vs in {1} {2}".format(variables,varOH,' limit 10' if debug else '')
for id_cip_sec,agr,val,data in getAll(sql,imp):
    id_cip_sec=int(id_cip_sec)
    b=monthsBetween(data,dara)  
    if 0<=int(b)<=23:
        if agr=='ALRIS':
            if id_cip_sec in alris:
                if alris[id_cip_sec]['ALRISd']==0:
                    alris[id_cip_sec]['ALRISd']=data
                    alris[id_cip_sec]['ALRISv']=val
                elif data>alris[id_cip_sec]['ALRISd']:
                   alris[id_cip_sec]['ALRISd']=data
                   alris[id_cip_sec]['ALRISv']=val
            else:
                try:
                    alris[id_cip_sec]={'up':ass[id_cip_sec]['br'],'desc':ass[id_cip_sec]['desc'],'amb':ass[id_cip_sec]['amb'],'edat':ass[id_cip_sec]['GrupEdat'],'sex':ass[id_cip_sec]['sex'],'ALRISd':data,'ALRISv':val,'ALSETd':0,'ALSETv':'\N','ALHABd':0,'ALHABv':'\N','ALDIAd':0,'ALDIAv':'\N','CP202d':0,'CP202v':'\N'}
                except KeyError:
                    ok=1
        elif agr=='ALSET':
            if id_cip_sec in alris:
                if alris[id_cip_sec]['ALSETd']==0:
                    alris[id_cip_sec]['ALSETd']=data
                    alris[id_cip_sec]['ALSETv']=val
                elif data>alris[id_cip_sec]['ALSETd']:
                    alris[id_cip_sec]['ALSETd']=data
                    alris[id_cip_sec]['ALSETv']=val
            else:
                try:
                    alris[id_cip_sec]={'up':ass[id_cip_sec]['br'],'desc':ass[id_cip_sec]['desc'],'amb':ass[id_cip_sec]['amb'],'edat':ass[id_cip_sec]['GrupEdat'],'sex':ass[id_cip_sec]['sex'],'ALRISd':0,'ALRISv':'\N','ALSETd':data,'ALSETv':val,'ALHABd':0,'ALHABv':'\N','ALDIAd':0,'ALDIAv':'\N','CP202d':0,'CP202v':'\N'}
                except KeyError:
                    ok=1
        elif agr=='ALHAB':
            if id_cip_sec in alris:
                if alris[id_cip_sec]['ALHABd']==0:
                    alris[id_cip_sec]['ALHABd']=data
                    alris[id_cip_sec]['ALHABv']=val
                elif data>alris[id_cip_sec]['ALHABd']:
                    alris[id_cip_sec]['ALHABd']=data
                    alris[id_cip_sec]['ALHABv']=val
            else:
                try:
                    alris[id_cip_sec]={'up':ass[id_cip_sec]['br'],'desc':ass[id_cip_sec]['desc'],'amb':ass[id_cip_sec]['amb'],'edat':ass[id_cip_sec]['GrupEdat'],'sex':ass[id_cip_sec]['sex'],'ALRISd':0,'ALRISv':'\N','ALSETd':0,'ALSETv':'\N','ALHABd':data,'ALHABv':val,'ALDIAd':0,'ALDIAv':'\N','CP202d':0,'CP202v':'\N'}
                except KeyError:
                    ok=1
        elif agr=='ALDIA':
            if id_cip_sec in alris:
                if alris[id_cip_sec]['ALDIAd']==0:
                    alris[id_cip_sec]['ALDIAd']=data
                    alris[id_cip_sec]['ALDIAv']=val
                elif data>alris[id_cip_sec]['ALDIAd']:
                    alris[id_cip_sec]['ALDIAd']=data
                    alris[id_cip_sec]['ALDIAv']=val
            else:
                try:
                    alris[id_cip_sec]={'up':ass[id_cip_sec]['br'],'desc':ass[id_cip_sec]['desc'],'amb':ass[id_cip_sec]['amb'],'edat':ass[id_cip_sec]['GrupEdat'],'sex':ass[id_cip_sec]['sex'],'ALRISd':0,'ALRISv':'\N','ALSETd':0,'ALSETv':'\N','ALHABd':0,'ALHABv':'\N','ALDIAd':data,'ALDIAv':val,'CP202d':0,'CP202v':'\N'}
                except KeyError:
                    ok=1
        elif agr=='CP202':
            if id_cip_sec in alris:
                if alris[id_cip_sec]['CP202d']==0:
                    alris[id_cip_sec]['CP202d']=data
                    alris[id_cip_sec]['CP202v']=val
                elif data>alris[id_cip_sec]['CP202d']:
                    alris[id_cip_sec]['CP202d']=data
                    alris[id_cip_sec]['CP202v']=val
            else:
                try:
                    alris[id_cip_sec]={'up':ass[id_cip_sec]['br'],'desc':ass[id_cip_sec]['desc'],'amb':ass[id_cip_sec]['amb'],'edat':ass[id_cip_sec]['GrupEdat'],'sex':ass[id_cip_sec]['sex'],'ALRISd':0,'ALRISv':'\N','ALSETd':0,'ALSETv':'\N','ALHABd':0,'ALHABv':'\N','ALDIAd':0,'ALDIAv':'\N','CP202d':data,'CP202v':val}
                except KeyError:
                    ok=1
                    
with open(OutFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for (id),d in alris.items():
        try:
            w.writerow([id,d['amb'],d['up'],d['desc'],d['edat'],d['sex'],d['ALRISd'],d['ALRISv'],d['ALSETd'],d['ALSETv'],d['ALHABd'],d['ALHABv'],d['ALDIAd'],d['ALDIAv'],d['CP202d'],d['CP202v']])
        except KeyError:
            continue

table="alcohol" 
sql= 'drop table if exists {}'.format(table)
execute(sql,db)
sql= "create table {} (id_cip_sec int,regio varchar(500) not null default'',up varchar(5) not null default'',updesc varchar(500) not null default'',edat varchar(50) not null default'',sexe varchar(1) not null default'',ALRISd date not null default 0,ALRISv double,ALSETd date not null default 0,\
            ALSETv double,ALHABd date not null default 0,ALHABv double,ALDIAd date not null default 0,ALDIAv double,CP202d date not null default 0,CP202v double)".format(table)
execute(sql,db)
loadData(OutFile,table,db)
alris.clear() 

printTime('Problemes')
diagnostics={}
sql= "select id_cip_sec,pr_cod_ps from problemes,nodrizas.dextraccio \
    where pr_cod_o_ps='C' and pr_hist=1 and pr_dde<=data_ext and (pr_data_baixa is null or pr_data_baixa>data_ext) and pr_cod_ps in {}".format(psOH)
for id_cip_sec,ps in getAll(sql,imp):
    id_cip_sec=int(id_cip_sec)
    try:
        if f10[ps]:
            ps='Altres F10'
    except KeyError:
        ok=1
    try:
        diagnostics[(id_cip_sec,ps)]={'up':ass[id_cip_sec]['br'],'desc':ass[id_cip_sec]['desc'],'amb':ass[id_cip_sec]['amb'],'edat':ass[id_cip_sec]['GrupEdat'],'sex':ass[id_cip_sec]['sex']}
    except KeyError:
        continue
                    
with open(OutFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for (id,ps),d in diagnostics.items():
        try:
            w.writerow([id,d['amb'],d['up'],d['desc'],d['edat'],d['sex'],ps])
        except KeyError:
            continue

tableps="PSalcohol" 
sql= 'drop table if exists {}'.format(tableps)
execute(sql,db)
sql= "create table {} (id_cip_sec int,regio varchar(500) not null default'',up varchar(5) not null default'',updesc varchar(500) not null default'',edat varchar(50) not null default'',sexe varchar(1) not null default'',ps varchar(20) not null default'')".format(tableps)
execute(sql,db)
loadData(OutFile,tableps,db)
diagnostics.clear()  

printTime('Inici Agrupar')
bMenysV={}
sql="select id_cip_sec,regio,up,updesc,edat,sexe,alrisd,alrisv,alsetd,alsetv,alhabd,alhabv,aldiad,aldiav,cp202d,cp202v from {}".format(table)
for id_cip_sec,regio,up,updesc,edat,sexe,alrisd,alrisv,alsetd,alsetv,alhabd,alhabv,aldiad,aldiav,cp202d,cp202v in getAll(sql,db):
    if alsetv==None:
        alsetv=0
    if alhabv==None:
        alhabv=0
    if aldiav==None:
        aldiav=0
    try:    
        alrisv=int(alrisv)
    except TypeError:
        ok=1
    try:
        cp202v=int(cp202v)
    except TypeError:
        ok=1        
    if (up,regio,updesc,edat,sexe,alrisv,cp202v) in bMenysV:
        bMenysV[(up,regio,updesc,edat,sexe,alrisv,cp202v)]['n']+=1
        bMenysV[(up,regio,updesc,edat,sexe,alrisv,cp202v)]['Alset']+=alsetv
        bMenysV[(up,regio,updesc,edat,sexe,alrisv,cp202v)]['Alhab']+=alhabv
        bMenysV[(up,regio,updesc,edat,sexe,alrisv,cp202v)]['Aldia']+=aldiav
    else:
        try:
            bMenysV[(up,regio,updesc,edat,sexe,alrisv,cp202v)]={'n':1,'Alset':alsetv,'Alhab':alhabv,'Aldia':aldiav}
        except KeyError:
            continue
with open(OutFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for (up,regio,updesc,edat,sexe,alrisv,cp202v),d in bMenysV.items():
        try:
            w.writerow([dcalcul,regio,up,updesc,edat,sexe,alrisv,cp202v,d['n'],d['Alset'],d['Alhab'],d['Aldia']])
        except KeyError:
            continue   
bMenysV.clear()            
bMenysVPS={}
sql="select id_cip_sec,regio,up,updesc,edat,sexe,ps from {}".format(tableps)
for id_cip_sec,regio,up,updesc,edat,sexe,ps in getAll(sql,db):
    if (regio,up,updesc,edat,sexe,ps) in bMenysVPS:
        bMenysVPS[(regio,up,updesc,edat,sexe,ps)]['n']+=1
    else:
        try:
            bMenysVPS[(regio,up,updesc,edat,sexe,ps)]={'n':1}
        except KeyError:
            conitnue

with open(OutFilePS,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for (regio,up,updesc,edat,sexe,ps),d in bMenysVPS.items():
        try:
            w.writerow([dcalcul,regio,up,updesc,edat,sexe,ps,d['n']])
        except KeyError:
            continue
bMenysVPS.clear()


printTime('Inici EQA') 

def PsConverter(agr):

    return 'HTA' if int(agr) == 55 else 'DEPRESSIO' 
    
infoEQA,PsEQA={},{}
sql = "select id_cip_sec,ps from eqa_problemes where ps in ('55','425')"
for id, ps in getAll(sql,nod):
    agr = PsConverter(ps)
    if agr == 'HTA':
        if id in PsEQA:
            PsEQA[id]['HTA'] = 1
        else:
            PsEQA[id] = {'HTA':1,'DEP':0}
    elif agr=='DEPRESSIO':
        if id in PsEQA:
            PsEQA[id]['DEP'] = 1
        else:
            PsEQA[id] = {'HTA':0,'DEP':1}
    
sql="select id_cip_sec,num,den from {0} where ates=1 and excl_edat=0 and institucionalitzat=0 and maca=0 and excl=0 and ci=0 and clin=0 and ind_codi='{1}'".format(teqa,indEQA)
for id_cip_sec,num,den in getAll(sql,eqa):
    try:
        up=ass[id_cip_sec]['br']
        updesc=ass[id_cip_sec]['desc']
        edat=ass[id_cip_sec]['GrupEdat']
        sexe=ass[id_cip_sec]['sex']
        regio=ass[id_cip_sec]['amb']
    except KeyError:
        continue
    try:
        hta = PsEQA[id_cip_sec]['HTA']
    except KeyError:
        hta = 0
    try:
        dep = PsEQA[id_cip_sec]['DEP']
    except KeyError:
        dep = 0
    if (regio,up,updesc,hta,dep,edat,sexe) in infoEQA:
        infoEQA[(regio,up,updesc,hta,dep,edat,sexe)]['num']+=num
        infoEQA[(regio,up,updesc,hta,dep,edat,sexe)]['den']+=den
    else:
        try:
            infoEQA[(regio,up,updesc,hta,dep,edat,sexe)]={'num':num,'den':den}
        except KeyError:
            ok=1 
            
with open(OutFileEQA,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for (regio,up,updesc,hta,dep,edat,sexe),d in infoEQA.items():
        try:
            w.writerow([dcalcul,regio,up,updesc,hta,dep,edat,sexe,d['num'],d['den']])
        except KeyError:
            continue
infoEQA.clear()

printTime('Inici Pob')  
infoPob={}
sql="select id_cip_sec,ates from assignada_tot"
for id_cip_sec,ates in getAll(sql,nod):
    id_cip_sec=int(id_cip_sec)
    try:
        up=ass[id_cip_sec]['br']
        updesc=ass[id_cip_sec]['desc']
        edat=ass[id_cip_sec]['GrupEdat']
        sexe=ass[id_cip_sec]['sex']
        regio=ass[id_cip_sec]['amb']
    except KeyError:
        continue
    if (regio,up,updesc,edat,sexe,ates) in infoPob:
        infoPob[(regio,up,updesc,edat,sexe,ates)]['n']+=1
    else:
        try:
            infoPob[(regio,up,updesc,edat,sexe,ates)]={'n':1}
        except KeyError:
            ok=1 
        
with open(OutFilePob,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for (regio,up,updesc,edat,sexe,ates),d in infoPob.items():
        try:
            w.writerow([dcalcul,regio,up,updesc,edat,sexe,ates,d['n']])
        except KeyError:
            continue
infoPob.clear()

sql = "update {0} set data_ext='{1}'".format(dext,dback)
execute(sql,nod)

printTime('Fi procés')