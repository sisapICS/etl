# -*- coding: utf8 -*-

# Beveu menys slvia cordomi

import sisapUtils as u
import sisaptools as t
import xlwt
import os
import pandas as pd


upload = []
sql = """SELECT USUA_CIP, USUA_TELEFON_PRINCIPAL, USUA_TELEFON_SECUNDARI, USUA_UAB_UP, to_date(USUA_DATA_NAIXEMENT, 'J')
        FROM USUTB040
        WHERE to_date(USUA_DATA_NAIXEMENT, 'J') <  add_months(sysdate , -12*18)
        AND USUA_SITUACIO = 'A'
        AND USUA_UAB_CODI_UAB IS NOT NULL
        """
for s in ('6102','6211','6310','6519','6728','6839'):
    print(s)
    for cip, telf1, telf2, up, USUA_DATA_NAIXEMENT in u.getAll(sql, s):
        if up in ('00004','00089','00478','00065','00029','00171','00373','00364'):
            try:
                upload.append((str(cip), telf1, telf2, USUA_DATA_NAIXEMENT))
            except:
                pass

# df = pd.DataFrame(upload, columns =['CIP', 'TELF1', 'TELF2'])
# df.to_csv('prevencio_trastorns_cons_alcohol.csv')

cols = '(cip varchar2(15), telf1 varchar2(10), telf2 varchar2(10), USUA_DATA_NAIXEMENT date)'
u.createTable('prevencio_trastorns_alcohol_2', cols, 'exadata', rm=True)
u.listToTable(upload, 'prevencio_trastorns_alcohol_2', 'exadata')
u.grantSelect('prevencio_trastorns_alcohol_2', 'DWSISAP_ROL', 'exadata')


