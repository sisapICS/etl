# coding: latin1


import sisapUtils as u
import collections as c
from datetime import datetime
from dateutil.relativedelta import relativedelta


"""
CODI SQL per anar r�pid
-------------- COLONOSCOPIES ---------------

CREATE table colonoscopies_positives as (      
SELECT id, codi, DATA 
FROM dwtw.PROBLEMES
WHERE DATA >= DATE '2023-01-01' and
codi IN ('C01-D37.4', 'C01-K62.1', 'C01-C18.8', 'C01-C18.9', 'C01-K62.5', 'C01-K62.7', 'C01-K62.6', 'C01-C18.2', 'C01-C18.3', 'C01-C18.6', 'C01-C18.7', 'C01-C18.4', 'C01-C18.5', 'C01-K51.9', 'C01-K51.8', 'C01-K51.3', 'C01-K51.2', 'C01-K51.0', 'C01-K51.5', 'C01-K51.4', 'C01-D12.3', 'C01-D12.2', 'C01-D12.7', 'C01-D12.6', 'C01-D12.5', 'C01-D12.4', 'C01-D12.8', 'C01-K50', 'C01-K62.3', 'C01-D3A.022', 'C01-D3A.023', 'C01-C20', 'C01-D3A.026', 'C01-D3A.024', 'C01-D3A.025', 'C01-D3A.029', 'C01-K62.9', 'C01-D3', 'C01-C7A.029', 'C01-K62.8', 'C01-C7A.025', 'C01-C7A.024', 'C01-C7A.026', 'C01-C7A.023', 'C01-C7A.022', 'C01-D3A.096', 'C01-K63.5', 'C01-D01.2', 'C01-D01.1', 'C01-D01.0', 'C01-K57.5', 'C01-K57.4', 'C01-C19', 'C01-K57.3', 'C01-K57.2', 'C01-K57.9', 'C01-K57.8')
);

GRANT SELECT ON colonoscopies_positives TO DWSISAP_ROL;

SELECT * FROM dwtw.colonoscopies_positives



SELECT id, codi, min_data, max_data, n_repes FROM dwtw.infeccions_previes_colon

GRANT SELECT ON infeccions_previes_colon TO DWSISAP_ROL;


CREATE TABLE infeccions_previes_colon AS (
SELECT id, codi, min(DATA) min_data, max(data) max_data, count(1) n_repes FROM dwtw.PROBLEMES p
WHERE codi IN (
'C01-B20',
'C01-Z21',
'C01-A04.72',
'C01-A04.71',
'C01-B96.81',
'C01-B07.9',
'C01-A63.0',
'C01-I33.0',
'C01-B95.4',
'C01-A40.8',
'C01-A81.2',
'C01-B16',
'C01-B18.0',
'C01-B18.1',
'C01-B17.1',
'C01-B18.2',
'C01-A06',
'C01-A07',
'C01-B66',
'C01-B68',
'C01-B70.0',
'C01-B71.0',
'C01-B71.1',
'C01-B75',
'C01-B76.0',
'C01-B76.1',
'C01-B77',
'C01-B79',
'C01-B80',
'C01-B81',
'C01-B82'
)
GROUP BY id, codi)
"""

dx = {
    "C01-C18.2": "Neopl�sia maligna, c�lon ascendent",
    "C01-C18.3": "Neopl�sia maligna, angle hep�tic",
    "C01-C18.4": "Neopl�sia maligna, c�lon transvers",
    "C01-C18.5": "Neopl�sia maligna, angle espl�nic",
    "C01-C18.6": "Neopl�sia maligna, c�lon descendent",
    "C01-C18.7": "Neopl�sia maligna, c�lon sigmoide",
    "C01-C18.8": "Neopl�sia maligna, localitzacions contig�es o sobreposades del c�lon",
    "C01-C18.9": "Neopl�sia maligna, localitzaci� no especificada del c�lon - carcinoma/adenocarcinoma",
    "C01-C19": "Neopl�sia maligna d'uni� rectosigmoide",
    "C01-C20": "Neopl�sia maligna de recte",
    "C01-D01.0": "Carcinoma in situ de c�lon",
    "C01-D01.1": "Carcinoma in situ d'uni� rectosigmoide",
    "C01-D01.2": "Carcinoma in situ de recte",
    "C01-D12.2": "Neopl�sia benigna, c�lon ascendent",
    "C01-D12.3": "Neopl�sia benigna, c�lon transvers (angle hep�tic i angle espl�nic)",
    "C01-D12.4": "Neopl�sia benigna, c�lon descendent",
    "C01-D12.5": "Neopl�sia benigna, c�lon sigmoide",
    "C01-D12.6": "Neopl�sia benigna, localitzaci� no especificada del c�lon",
    "C01-D12.7": "Neopl�sia benigna, uni� rectosigmoide",
    "C01-D12.8": "Neopl�sia benigna, recte",
    "C01-D37.4": "Neopl�sia de comportament incert de c�lon",
    "C01-D3": "Neopl�sia de comportament incert de recte",
    "C01-C7A.022": "Tumor carcinoide maligne, c�lon ascendent",
    "C01-C7A.023": "Tumor carcinoide maligne, c�lon transvers",
    "C01-C7A.024": "Tumor carcinoide maligne, c�lon descendent",
    "C01-C7A.025": "Tumor carcinoide maligne, c�lon sigmoide",
    "C01-C7A.026": "Tumor carcinoide maligne, recte",
    "C01-C7A.029": "Tumor carcinoide maligne, porci� no especificada d'intest� gros",
    "C01-K51.4": "P�lips inflamatoris del c�lon",
    "C01-K63.5": "P�lip de colon",
    "C01-K51.0": "Pancolitis ulcerosa (cr�nica)",
    "C01-K51.2": "Proctitis ulcerosa (cr�nica)",
    "C01-K51.3": "Rectosigmo�ditis ulcerosa (cr�nica)",
    "C01-K51.5": "Colitis de localitzaci� esquerra",
    "C01-K51.8": "Altres tipus de colitis ulcerosa",
    "C01-K51.9": "Colitis ulcerosa no especificada",
    "C01-K50": "Malaltia de Crohn",
    "C01-K62.1": "P�lip rectal",
    "C01-D3A.029": "Tumor carcinoide benigne, porci� no especificada de l'intest� gros",
    "C01-D3A.022": "Carcinoide c�lon ascendent",
    "C01-D3A.024": "Carcinoide c�lon descendent",
    "C01-D3A.025": "Carcinoide c�lon sigmoide",
    "C01-D3A.023": "Carcinoide c�lon transvers",
    "C01-D3A.096": "Tumor carcinoide benigne, localitzaci� no especificada de l'intest� posterior",
    "C01-D3A.026": "Tumor carcinoide benigne, recte",
    "C01-K57.2": "Diverticulitis d'intest� gros amb perforaci� i absc�s",
    "C01-K57.3": "Malaltia diverticular d'intest� gros sense perforaci� ni absc�s",
    "C01-K57.4": "Diverticulitis d'intest� prim i gros amb perforaci� i absc�s",
    "C01-K57.5": "Malaltia diverticular d'intest� prim i gros sense perforaci� ni absc�s",
    "C01-K57.8": "Diverticulitis intestinal, part no especificada, amb perforaci� i absc�s",
    "C01-K57.9": "Malaltia diverticular d'intest�, part no especificada, sense perforaci� ni absc�s",
    "C01-K62.3": "Prolapse rectal",
    "C01-K62.5": "Hemorr�gia d'anus i recte",
    "C01-K62.6": "�lcera d'anus i recte",
    "C01-K62.7": "Proctitis per radiaci�",
    "C01-K62.8": "Altres malalties especificades d'anus i recte",
    "C01-K62.9": "Malaltia d'anus i recte no especificada"
}

class Colonoscopies():
    def __init__(self):
        self.table = 'estudi_colonoscopies'
        cols = """(any_registre number,
                    nia varchar2(100),
                    tip_llista varchar2(10),
                    up_declarant varchar2(10),
                    abs varchar2(10), 
                    data_naix date, 
                    sexe_bo varchar2(10),
                    up varchar2(10),
                    abs_bo varchar2(10),
                    nacionalitat varchar2(10),
                    municipi varchar2(10), 
                    localitat varchar2(10),
                    ccaa varchar2(10),
                    pais varchar2(10), 
                    niv_prio_nom varchar2(10),
                    data_real varchar2(10), 
                    immigrant_i varchar2(10),
                    renta varchar2(10),
                    gma varchar2(10),
                    colon_pos varchar2(10),
                    dx_desc varchar2(100),
                    data_sida date,
                    data_VIH date,
                    data_cd date,
                    n_cd number, 
                    data_ult_cd date,
                    data_1_hp date,
                    data_ult_hp date,
                    n_hp number,
                    data_1_vph date, 
                    data_ult_dx_vph date,
                    codi_vph varchar2(10),
                    n_vph number,
                    data_ult_eist date,
                    data_ult_st date,
                    data_ult_sep_dst date,
                    data_1_jc date,
                    data_ult_vhb_ag date, 
                    data_1_vhb_cron date,
                    data_ult_vhc_ag date,
                    data_1_vhc_cron date, 
                    data_ult_parlt date,
                    dx_ult_parlt varchar2(10))"""
        u.createTable(self.table, cols, 'exadata', rm=True)
        u.execute("GRANT SELECT ON ESTUDI_COLONOSCOPIES TO DWSISAP_ROL", 'exadata')

        self.get_pacients_interes()
        self.get_demografiques()
    
    def get_pacients_interes(self):
        self.pac_estudi = set()
        self.pacients_2023 = {}
        for tip_llista, up_declarant, abs, edat, sexe, localitat, ccaa, pais, niv_prio_nom, nia, nina, data_real in u.readCSV('colon_2023.csv', sep=';'):
            self.pacients_2023[nia] = [str(tip_llista), str(up_declarant), str(abs), str(edat), str(sexe), str(localitat), str(ccaa), str(pais), str(niv_prio_nom), str(nina), str(data_real)]
            self.pac_estudi.add(nia)
        self.pacients_2024 = {}
        for tip_llista, up_declarant, abs, edat, sexe, localitat, ccaa, pais, niv_prio_nom, nia, nina, data_real in u.readCSV('colon_2024.csv', sep=';'):
            self.pacients_2024[nia] = [str(tip_llista), str(up_declarant), str(abs), str(edat), str(sexe), str(localitat), str(ccaa), str(pais), str(niv_prio_nom), str(nina), str(data_real)]
            self.pac_estudi.add(nia)

    def get_demografiques(self):
        upload = []
        colonoscopies_positives = c.defaultdict(dict)
        sql = """SELECT nia, codi, data FROM dwsisap.RCA_CIP_NIA rcn 
                    INNER JOIN dwtw.colonoscopies_positives cp 
                    on cp.id = substr(rcn.cip,1,13)"""
        for nia, codi, data in u.getAll(sql, 'exadata'):
            if str(nia) in self.pac_estudi:
                colonoscopies_positives[str(nia)][data] = codi

        demos = {}
        sql = """SELECT nia, SEXE, DATA_NAIXEMENT, RCA_UP, rca_abs,
            NACIONALITAT, RCA_MUNICIPI  FROM dwsisap.DBC_POBLACIO"""
        for nia, sexe, data_naix, up, abs, nacionalitat, municipi in u.getAll(sql, 'exadata'):
            if str(nia) in self.pac_estudi:
                demos[str(nia)] = [sexe, data_naix, up, abs, nacionalitat, municipi]

        sql = """select 
                    C_ANY_ASSEGURAT
                    , C_NIA
                from 
                    DWCATSALUT.RCA_POB_OFICIAL
                where 
                    c_any_assegurat IN (2023, 2024)
                    and C_GRUP_GARANTIES_COTITZACIO IN('0017', '0026')"""
        immigrants_il = c.defaultdict(set)
        for year, nia in u.getAll(sql, 'exadata'):
            if str(nia) in self.pac_estudi:
                immigrants_il[year].add(str(nia))
        
        renta_anys = c.defaultdict(dict)
        sql = """select 
                        any_referencia
                        , nia
                        , renta
                    from 
                        dwcatsalut.morbiditat_assegurats
                    where 
                        any_referencia in (2023, 2024)"""
        for any_ref, nia, renta in u.getAll(sql, 'exadata'):
            if str(nia) in self.pac_estudi:
                renta_anys[int(any_ref)][str(nia)] = renta

        gma_grup = c.defaultdict(dict)
        sql = """select 
                    any_referencia
                    , nia
                    , risc_grup5
                from 
                    dwcatsalut.morbiditat_gma
                where 
                    any_referencia in (2024, 2023)"""
        for any_ref, nia, risc in u.getAll(sql, 'exadata'):
            if str(nia) in self.pac_estudi:
                gma_grup[int(any_ref)][str(nia)] = risc

        previs = c.defaultdict(dict)
        sql = """SELECT NIA, codi, min_data, max_data, n_repes FROM dwSISAP.RCA_CIP_NIA rcn
                    INNER JOIN dwtw.infeccions_previes_colon cp 
                    on cp.id = substr(rcn.CIP,1,13)"""
        for nia, codi, min_data, max_data, n_repes in u.getAll(sql, 'exadata'):
            if str(nia) in self.pac_estudi:
                previs[str(nia)][codi] = [min_data, max_data, n_repes]

        for nia in self.pacients_2023:
            tip_llista, up_declarant, abs, edat, sexe, localitat, ccaa, pais, niv_prio_nom, nina, data_real = self.pacients_2023[nia]
            if nia in demos:
                immigrant_i = 1 if nia in immigrants_il[2023] else 0
                renta = renta_anys[2023][str(nia)] if str(nia) in renta_anys[2023] else 99
                gma = gma_grup[2023][str(nia)] if str(nia) in gma_grup[2023] else 99
                sexe_bo, data_naix, up, abs_bo, nacionalitat, municipi = demos[nia]
                colon_pos, dx_desc = '', ''
                data_sida, data_VIH, data_cd, n_cd, data_ult_cd, data_1_hp = None, None, None, 0, None, None
                data_ult_hp, n_hp, data_1_vph, data_ult_dx_vph, codi_vph = None, 0, None, None, ''
                n_vph, data_ult_eist, data_ult_st = 0, None, None
                data_ult_sep_dst, data_1_jc, data_ult_vhb_ag, data_1_vhb_cron = None, None, None, None
                data_ult_vhc_ag, data_1_vhc_cron, data_1_vhc_cron = None, None, None
                data_ult_parlt, dx_ult_parlt = None, ''
                if nia in colonoscopies_positives:
                    data_real_date = datetime.strptime(data_real, "%Y%m%d").date()
                    for data in colonoscopies_positives[nia]:
                        data_date = data.date()
                        if data_real_date <= data_date and data_date <= data_real_date + relativedelta(months=3):
                            colon_pos = colonoscopies_positives[nia][data]
                            dx_desc = dx[colon_pos]
                if nia in previs:
                    data_sida = previs[nia]['C01-B20'][0] if 'C01-B20' in previs[nia] else None
                    data_VIH = previs[nia]['C01-Z21'][0] if 'C01-Z21' in previs[nia] else None
                    data_cd = previs[nia]['C01-A04.72'][0] if 'C01-A04.72' in previs[nia] else None
                    n_cd = previs[nia]['C01-A04.71'][2] if 'C01-A04.71' in previs[nia] else 0
                    data_ult_cd = previs[nia]['C01-A04.71'][1] if 'C01-A04.71' in previs[nia] else None
                    data_1_hp = previs[nia]['C01-B96.81'][0] if 'C01-B96.81' in previs[nia] else None
                    data_ult_hp = previs[nia]['C01-B96.81'][1] if 'C01-B96.81' in previs[nia] else None
                    n_hp = previs[nia]['C01-B96.81'][2] if 'C01-B96.81' in previs[nia] else None
                    data_1_vph = None
                    data_ult_dx_vph = None
                    codi_vph = ''
                    n_vph = 0
                    for dx_varis in ('C01-97.7', 'C01-B07.9', 'C01-A63.0'):
                        if dx_varis in previs[nia]:
                            if data_1_vph:
                                if data_1_vph > previs[nia][dx_varis][0]:
                                    data_1_vph = previs[nia][dx_varis][0]
                            else:
                                data_1_vph = previs[nia][dx_varis][0]
                            n_vph += previs[nia][dx_varis][2]
                            if data_ult_dx_vph:
                                if data_ult_dx_vph < previs[nia][dx_varis][1]:
                                    data_ult_dx_vph = previs[nia][dx_varis][1]
                                    codi_vph = dx_varis
                            else:
                                data_ult_dx_vph = previs[nia][dx_varis][1]
                                codi_vph = dx_varis
                    data_ult_eist = previs[nia]['C01-I33.0'][1] if 'C01-I33.0' in previs[nia] else None
                    data_ult_st = previs[nia]['C01-B95.4'][1] if 'C01-B95.4' in previs[nia] else None
                    data_ult_sep_dst = previs[nia]['C01-A40.8'][1] if 'C01-A40.8' in previs[nia] else None
                    data_1_jc = previs[nia]['C01-A81.2'][0] if 'C01-A81.2' in previs[nia] else None
                    data_ult_vhb_ag = previs[nia]['C01-B16'][1] if 'C01-B16' in previs[nia] else None
                    if 'C01-B18.0' in previs[nia]:
                        data_1_vhb_cron = previs[nia]['C01-B18.0'][0]
                    if 'C01-B18.1' in previs[nia]:
                        if data_1_vhb_cron is None:
                            data_1_vhb_cron = previs[nia]['C01-B18.1'][0]
                        elif previs[nia]['C01-B18.1'][0] < data_1_vhb_cron:
                            data_1_vhb_cron = previs[nia]['C01-B18.1'][0]
                    data_1_vhb_cron = previs[nia]['C01-B18.0'][0] if 'C01-B18.0' in previs[nia] else None
                    data_ult_vhc_ag = previs[nia]['C01-B17.1'][1] if 'C01-B17.1' in previs[nia] else None
                    data_1_vhc_cron = previs[nia]['C01-B18.2'][0] if 'C01-B18.2' in previs[nia] else None
                    for dx_varis in ("C01-A06", "C01-A07", "C01-B66", "C01-B68", "C01-B70.0",
                                    "C01-B71.0", "C01-B71.1", "C01-B75", "C01-B76.0", "C01-B76.1",
                                    "C01-B77", "C01-B79", "C01-B80", "C01-B81", "C01-B82"):
                        if dx_varis in previs[nia]:
                            if data_ult_parlt:
                                if dx_ult_parlt < previs[nia][dx_varis][1]:
                                    data_1_vph = previs[nia][dx_varis][1]
                                    dx_ult_parlt = previs[nia][dx_varis][2]
                            else:
                                data_ult_parlt = previs[nia][dx_varis][1]
                                dx_ult_parlt = previs[nia][dx_varis][2]
                upload.append((2023, nia, tip_llista, up_declarant, abs, data_naix, 
                                sexe_bo, up, abs_bo, nacionalitat, municipi, 
                                localitat, ccaa, pais, niv_prio_nom, data_real, 
                                str(immigrant_i), str(renta), str(gma), colon_pos, dx_desc,
                                data_sida, data_VIH, data_cd, n_cd, data_ult_cd, data_1_hp,
                                data_ult_hp, n_hp, data_1_vph, data_ult_dx_vph, codi_vph,
                                n_vph, data_ult_eist, data_ult_st,
                                data_ult_sep_dst, data_1_jc, data_ult_vhb_ag, data_1_vhb_cron,
                                data_ult_vhc_ag, data_1_vhc_cron, data_ult_parlt, dx_ult_parlt))
        print('upload1', len(upload))

        for nia in self.pacients_2024:
            tip_llista, up_declarant, abs, edat, sexe, localitat, ccaa, pais, niv_prio_nom, nina, data_real = self.pacients_2024[nia]
            if nia in demos:
                immigrant_i = 1 if nia in immigrants_il[2024] else 0
                renta = renta_anys[2024][str(nia)] if str(nia) in renta_anys[2024] else 99
                gma = gma_grup[2024][str(nia)] if str(nia) in gma_grup[2024] else 99
                sexe_bo, data_naix, up, abs_bo, nacionalitat, municipi = demos[nia]
                colon_pos, dx_desc = '', ''
                data_sida, data_VIH, data_cd, n_cd, data_ult_cd, data_1_hp = None, None, None, 0, None, None
                data_ult_hp, n_hp, data_1_vph, data_ult_dx_vph, codi_vph = None, 0, None, None, ''
                n_vph, data_ult_eist, data_ult_st = 0, None, None
                data_ult_sep_dst, data_1_jc, data_ult_vhb_ag, data_1_vhb_cron = None, None, None, None
                data_ult_vhc_ag, data_1_vhc_cron, data_1_vhc_cron = None, None, None
                data_ult_parlt, dx_ult_parlt = None, ''
                if nia in colonoscopies_positives:
                    data_real_date = datetime.strptime(data_real, "%Y%m%d").date()
                    for data in colonoscopies_positives[nia]:
                        data_date = data.date()
                        if data_real_date <= data_date and data_date <= data_real_date + relativedelta(months=3):
                            colon_pos = colonoscopies_positives[nia][data]
                            dx_desc = dx[colon_pos]
                if nia in previs:
                    data_sida = previs[nia]['C01-B20'][0] if 'C01-B20' in previs[nia] else None
                    data_VIH = previs[nia]['C01-Z21'][0] if 'C01-Z21' in previs[nia] else None
                    data_cd = previs[nia]['C01-A04.72'][0] if 'C01-A04.72' in previs[nia] else None
                    n_cd = previs[nia]['C01-A04.71'][2] if 'C01-A04.71' in previs[nia] else 0
                    data_ult_cd = previs[nia]['C01-A04.71'][1] if 'C01-A04.71' in previs[nia] else None
                    data_1_hp = previs[nia]['C01-B96.81'][0] if 'C01-B96.81' in previs[nia] else None
                    data_ult_hp = previs[nia]['C01-B96.81'][1] if 'C01-B96.81' in previs[nia] else None
                    n_hp = previs[nia]['C01-B96.81'][2] if 'C01-B96.81' in previs[nia] else None
                    data_1_vph = None
                    data_ult_dx_vph = None
                    codi_vph = ''
                    n_vph = 0
                    for dx_varis in ('C01-97.7', 'C01-B07.9', 'C01-A63.0'):
                        if dx_varis in previs[nia]:
                            if data_1_vph:
                                if data_1_vph > previs[nia][dx_varis][0]:
                                    data_1_vph = previs[nia][dx_varis][0]
                            else:
                                data_1_vph = previs[nia][dx_varis][0]
                            n_vph += previs[nia][dx_varis][2]
                            if data_ult_dx_vph:
                                if data_ult_dx_vph < previs[nia][dx_varis][1]:
                                    data_ult_dx_vph = previs[nia][dx_varis][1]
                                    codi_vph = dx_varis
                            else:
                                data_ult_dx_vph = previs[nia][dx_varis][1]
                                codi_vph = dx_varis
                    data_ult_eist = previs[nia]['C01-I33.0'][1] if 'C01-I33.0' in previs[nia] else None
                    data_ult_st = previs[nia]['C01-B95.4'][1] if 'C01-B95.4' in previs[nia] else None
                    data_ult_sep_dst = previs[nia]['C01-A40.8'][1] if 'C01-A40.8' in previs[nia] else None
                    data_1_jc = previs[nia]['C01-A81.2'][0] if 'C01-A81.2' in previs[nia] else None
                    data_ult_vhb_ag = previs[nia]['C01-B16'][1] if 'C01-B16' in previs[nia] else None
                    data_1_vhb_cron = None
                    if 'C01-B18.0' in previs[nia]:
                        data_1_vhb_cron = previs[nia]['C01-B18.0'][0]
                    if 'C01-B18.1' in previs[nia]:
                        if data_1_vhb_cron is None:
                            data_1_vhb_cron = previs[nia]['C01-B18.1'][0]
                        elif previs[nia]['C01-B18.1'][0] < data_1_vhb_cron:
                            data_1_vhb_cron = previs[nia]['C01-B18.1'][0]
                    data_1_vhb_cron = previs[nia]['C01-B18.0'][0] if 'C01-B18.0' in previs[nia] else None
                    data_ult_vhc_ag = previs[nia]['C01-B17.1'][1] if 'C01-B17.1' in previs[nia] else None
                    data_1_vhc_cron = previs[nia]['C01-B18.2'][0] if 'C01-B18.2' in previs[nia] else None
                    dx_ult_parlt = ''
                    for dx_varis in ("C01-A06", "C01-A07", "C01-B66", "C01-B68", "C01-B70.0",
                                    "C01-B71.0", "C01-B71.1", "C01-B75", "C01-B76.0", "C01-B76.1",
                                    "C01-B77", "C01-B79", "C01-B80", "C01-B81", "C01-B82"):
                        if dx_varis in previs[nia]:
                            if data_ult_parlt:
                                if dx_ult_parlt < previs[nia][dx_varis][1]:
                                    data_1_vph = previs[nia][dx_varis][1]
                                    dx_ult_parlt = previs[nia][dx_varis][2]
                            else:
                                data_ult_parlt = previs[nia][dx_varis][1]
                                dx_ult_parlt = previs[nia][dx_varis][2]

                upload.append((2024, nia, tip_llista, up_declarant, abs, data_naix, 
                                sexe_bo, up, abs_bo, nacionalitat, municipi, 
                                localitat, ccaa, pais, niv_prio_nom, data_real, 
                                str(immigrant_i), str(renta), str(gma), colon_pos, dx_desc,
                                data_sida, data_VIH, data_cd, n_cd, data_ult_cd, data_1_hp,
                                data_ult_hp, n_hp, data_1_vph, data_ult_dx_vph, codi_vph,
                                n_vph, data_ult_eist, data_ult_st,
                                data_ult_sep_dst, data_1_jc, data_ult_vhb_ag, data_1_vhb_cron,
                                data_ult_vhc_ag, data_1_vhc_cron, data_ult_parlt, dx_ult_parlt))
        print(upload)
        print('upload2', len(upload))
        u.writeCSV('colonoscopies_afegit.csv', upload, sep=';')
        u.listToTable(upload, self.table, 'exadata')

if __name__ == "__main__":
    Colonoscopies()


