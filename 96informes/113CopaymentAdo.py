# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

debug = False

nod = 'nodrizas'
imp = 'import'
farmacs = "(445)"

def farmconverter(farmac):
    
    if farmac in (82, 658, 216, 659):
        far = 'Estatines'
    elif farmac in (713, 714):
        far = 'MSRE'
    elif farmac in (715, 716):
        far = 'Denosumab'
    elif farmac in (717, 718):
        far = 'Bifosfonats'
    elif farmac in (719, 720):
        far = 'Altres aOP'
    elif farmac == 445:
        far = 'ado'
    else:
        far = 'error'
    return far

   
anys = [2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013,2014,2015,2016]
mesos = ['01','02','03','04','05','06','07','08','09','10','11','12']

centres = {}

sql = "select sha1(concat(scs_codi,'EQA')), medea from cat_centres where ep='0208'"
for up, medea in getAll(sql, nod):
    centres[up] = medea
    
periodes = {}   
for ane in anys:
    for mes in mesos:
        periode = str(ane) + mes
        periodes[periode] = True


fincidents = Counter()
sql = "select id, dat, agr,  sha1(concat(up_disp,'EQA')) from farmacs_facturats where agr in {0} {1}".format(farmacs, ' limit 10' if debug else '')
for id, dat, farmac, up in getAll(sql, ('sisap_antidiab','nym_proj')):
    far = farmconverter(int(farmac))
    if up in centres:
        medea = centres[up]
        fincidents[(dat, far, up, medea)] += 1
               
upload = []
for (Ydde, far, up, medea), res in fincidents.items():
    upload.append([Ydde, far, up, medea, res])
file = tempFolder + 'projecte_copayment_ado_incidents.txt'
writeCSV(file, upload, sep=';')   
    
    