import sisapUtils as u
import collections as c


def infermera_enllac():
    u.createTable('recuperacio_sisap_ecap', '(indicador varchar(100), periode varchar(10), up varchar(10), a varchar(10), tip varchar(10), d varchar(10), analisi varchar(10), val double)', 'altres', rm=True)
    ups = c.defaultdict(dict)
    query = "INFENLLAC###;AYR23###;PRESONS#99;NUM,DEN,AGRESULT;NOCAT;NOIMP;DIM6SET"
    for ind, _a, br, analisi, _c, tipprof, _d, value in u.LvClient().query(query):
        ups[(ind, _a, br, _c, tipprof, _d)][analisi] =  value
    upload = []
    for (a,b,c_,d,e,f), val in ups.items():
        for analisi, v in val.items():
            upload.append((a,b,c_,d,e,f, analisi, v))
    u.listToTable(upload, 'recuperacio_sisap_ecap', 'altres')


def iep():
    ups = c.defaultdict(dict)
    query = "IEP#99;AYR23###;PRESONS#99;NUM,DEN,AGRESULT;EDATS5;POBLATIP###;SEXE"
    for ind, _a, br, analisi, _c, tipprof, _d, value in u.LvClient().query(query):
        ups[(ind, _a, br, _c, tipprof, _d)][analisi] =  value
    upload = []
    for (a,b,c_,d,e,f), val in ups.items():
        for analisi, v in val.items():
            upload.append((a,b,c_,d,e,f, analisi, v))
    u.listToTable(upload, 'recuperacio_sisap_ecap', 'altres')

def to_pdp():
    sql = """select CAST(concat('20', substr(a.periode,2,2)) as INT) ano,
                cast(substr(a.periode,4,2) as INT) mes, 
                scs_codi, 'PRS', a.indicador, a.val, c.val, a.val/c.val 
                from altres.recuperacio_sisap_ecap a
                inner join nodrizas.cat_centres_with_jail b
                on a.up = b.ics_codi 
                inner join altres.recuperacio_sisap_ecap c
                on a.indicador = c.indicador and a.periode = c.periode and a.up = c.up 
                where a.analisi ='NUM' and c.analisi = 'DEN'
                and a.periode != 'A2312'"""
    upload = []
    for row in u.getAll(sql, 'altres'):
        upload.append(row)
    u.listToTable(upload, 'prsindicadors', 'pdp')

    


if __name__ == "__main__":
    # infermera_enllac()
    # iep()
    to_pdp()