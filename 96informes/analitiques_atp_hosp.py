
import sisapUtils as u
import collections as c

class Analitiques():
    def __init__(self):
        cols = '(up varchar(10), edat varchar(10), pri int, hosp int, den int)'
        u.createTable('peticions_x_origen', cols, 'test', rm=True)
        self.get_pob()
        self.get_analitiques()
        self.upload()
    
    def get_pob(self):
        self.pob = {}
        self.dens = c.Counter()
        sql = """SELECT cip, ecap_up, edat 
                FROM dwsisap.DBC_POBLACIO dp 
                WHERE ecap_up IS NOT null"""
        for cip, up, edat in u.getAll(sql, 'exadata'):
            self.pob[cip[0:13]] = [up, edat]
            if edat >= 90:
                age_group = '>=90'
            else:
                age_group = '>=' + str((edat/5)*5) 
            self.dens[(up, age_group)] += 1
        
    def get_analitiques(self):
        self.lab = []
        sql = """SELECT id, CASE WHEN up_cod IS NULL THEN 1 ELSE 0 END primaria
                FROM DWTW.lab_pet_2023 p
                LEFT JOIN dwsisap.dbc_centres c
                ON p.up = c.up_cod"""
        for id, primaria in u.getAll(sql, 'exadata'):
            self.lab.append((id[0:13], primaria))
    
    def upload(self):
        to_load = c.defaultdict(c.Counter)
        for (id, pri) in self.lab:
            if id in self.pob:
                up, edat = self.pob[id]
                if edat >= 90:
                    age_group = '>=90'
                else:
                    age_group = '>=' + str((edat/5)*5) 
                if pri == 1:
                    to_load[(up, age_group)]['PRI'] += 1
                else:
                    to_load[(up, age_group)]['ALTRES'] += 1
        
        final = []
        for (up, age), den in self.dens.items():
            if (up, age) in to_load:
                uploadiiing = [0]*5
                uploadiiing[0] = up
                uploadiiing[1] = age
                uploadiiing[4] = den
                for tipus, N in to_load[(up, age)].items():
                    if tipus == 'PRI':
                        uploadiiing[2] = N
                    else:
                        uploadiiing[3] = N
            final.append(uploadiiing)
        
        u.listToTable(final, 'peticions_x_origen', 'test')

class Analitiques_eqa0239():
    def __init__(self):
        cols = '(id_cip_sec double, up_variable varchar(5), up_lab varchar(5), up varchar(5), uba varchar(5), edat double)'
        u.createTable('nums_eqa0239_ori', cols, 'test', rm=True)
        self.pacients()
        self.labs()
        self.upload()
    
    def pacients(self):
        self.pop = {}
        sql = """select id_cip_sec, up, uba, edat from EQA_IND.mst_indicadors_pacient
                WHERE IND_CODI = 'EQA0239A'
                and NUM = 1
                and EXCL_EDAT = 0
                and CI = 0
                and LLISTAT = 1"""
        for id, up, uba, edat in u.getAll(sql, 'eqa_ind'):
            self.pop[id] = [up, uba, edat]
        
    def labs(self):
        self.proves = c.defaultdict(list)
        sql = """select id_cip_sec, codi_up 
                from import.laboratori1 
                where cr_codi_prova_ics = 'Q06285'
                and codi_up != ''"""
        for id, up in u.getAll(sql, 'import'):
            self.proves[id].append((up, 'L'))
        sql = """select id_cip_sec, vu_up from import.variables1
                where vu_cod_vs in ('IPSA',
                'IPSA2',
                'LMT010',
                'PSAL',
                'PSA-L',
                'PSAL2')"""
        for id, up in u.getAll(sql, 'import'):
            self.proves[id].append((up, 'V'))
    
    def upload(self):
        final = []
        for id, (up_o, uba, edat) in self.pop.items():
            if id in self.proves:
                upl = [0]*6
                upl[0] = id
                upl[3] = up_o
                upl[4] = uba
                upl[5] = edat
                for (up, prova) in self.proves[id]:
                    if prova == 'V':
                        upl[1] = up
                        print('some')
                    elif prova == 'L':
                        upl[2] = up
                final.append(upl)
        u.listToTable(final, 'nums_eqa0239_ori', 'test')


if __name__ == "__main__":
    Analitiques_eqa0239()