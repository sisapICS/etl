import sisapUtils as u
import collections as c
from dateutil.relativedelta import relativedelta

import csv


class Analisi_sms():
    def __init__(self):
        # self.time_series()
        # self.time_series1()
        self.nens_amb_sms()
        self.get_id_cip_2_cip()
        # self.vacunes_registrades_despres_sms()
        self.vacunes_mancants()
        # self.nens_vacunats_311222()

    def time_series(self):
        print('pob')
        nens = {}
        ups = {}
        sql = """select
                    id_cip_sec,
                    data_naix,
                    up
                from
                    nodrizas.assignada_tot
                where
                    edat < 18"""
        for id, data_naix, up in u.getAll(sql, 'nodrizas'):
            nens[id] = data_naix
            ups[id] = up

        print('vacunes')
        N_vacunats = c.Counter()
        sql = """select
                    id_cip_sec,
                    va_u_cod,
                    va_u_data_vac
                from
                    import.vacunes
                where
                    va_u_data_baixa = '' and 
                    va_u_data_vac between cast('2021-01-01' as DATE) and cast('2022-12-31' as DATE)"""
        for id, cod, data in u.getAll(sql, 'import'):
            if id in nens:
                data_naix = nens[id]
                edat = relativedelta(data, data_naix).years
                up = ups[id]
                N_vacunats[(cod, edat, up, data)] += 1

        print('upload')
        upload = []
        for (cod, edat, up, data), N in N_vacunats.items():
            upload.append((cod, edat, up, data, N))

        cols = '(cod varchar(6), edat int, up varchar(5), data date, N int)'
        u.createTable('time_series_vac', cols, 'test', rm=True)
        u.listToTable(upload, 'time_series_vac', 'test')
    
    def time_series1(self):
        print('pob')
        nens = {}
        ups = {}
        sql = """select
                    id_cip_sec,
                    data_naix,
                    up
                from
                    nodrizas.assignada_tot
                where
                    edat < 18"""
        for id, data_naix, up in u.getAll(sql, 'nodrizas'):
            nens[id] = data_naix
            ups[id] = up

        print('vacunes')
        N_vacunats = c.Counter()
        sql = """select
                    id_cip_sec,
                    va_u_cod,
                    va_u_data_alta
                from
                    import.vacunes
                where
                    va_u_data_baixa = '' and 
                    va_u_data_alta between cast('2021-01-01' as DATE) and cast('2022-12-31' as DATE)"""
        for id, cod, data in u.getAll(sql, 'import'):
            if id in nens:
                data_naix = nens[id]
                edat = relativedelta(data, data_naix).years
                up = ups[id]
                N_vacunats[(cod, edat, up, data)] += 1

        print('upload')
        upload = []
        for (cod, edat, up, data), N in N_vacunats.items():
            upload.append((cod, edat, up, data, N))

        cols = '(cod varchar(6), edat int, up varchar(5), data date, N int)'
        u.createTable('time_series_vac_alta', cols, 'test', rm=True)
        u.listToTable(upload, 'time_series_vac_alta', 'test')

    def nens_amb_sms(self):
        print('nens amb sms')
        self.nens = set()
        sql = """SELECT cip FROM dwsisap.estudi_sms"""
        for nen, in u.getAll(sql, 'exadata'):
            self.nens.add(nen)

    def get_id_cip_2_cip(self):
        print('id_cip_2_cip')
        self.id_cip_2_cip = {}
        hash_2_cip = {}
        sql = """SELECT usua_cip, usua_cip_cod  FROM pdptb101"""
        for cip, hash in u.getAll(sql, 'pdp'):
            if cip in self.nens:
                hash_2_cip[hash] = cip
        sql = """select hash_d, id_cip_sec from import.u11"""
        for hash, id_cip in u.getAll(sql, 'import'):
            if hash in hash_2_cip:
                self.id_cip_2_cip[id_cip] = hash_2_cip[hash]

    def vacunes_registrades_despres_sms(self):
        print('vacs registrades after')
        upload = []
        sql = """select
                    id_cip_sec,
                    va_u_data_vac,
                    va_u_data_alta,
                    va_u_cod,
                    va_u_dosi
                from
                    import.vacunes
                where
                    year(va_u_data_alta) = 2022
                    and month(va_u_data_alta) >= 9
                    and va_u_data_baixa = ''"""
        for id_cip, data_vac, data_alta, codi, dosi in u.getAll(sql, 'import'):
            if id_cip in self.id_cip_2_cip:
                cip = self.id_cip_2_cip[id_cip]
                upload.append((cip, data_vac, data_alta, codi, dosi))
        cols = """(cip varchar2(15), data_vacuna date, data_registre date, codi varchar2(15), dosi int)"""
        u.createTable('nens_vacunats_periode', cols, 'exadata', rm=True)
        u.grantSelect('nens_vacunats_periode', 'DWSISAP_ROL', 'exadata')
        u.listToTable(upload, 'nens_vacunats_periode', 'exadata')

    def vacunes_mancants(self):
        cols = """(cip varchar2(15), po int, DTP int)"""
        u.createTable('vacunes_mancants_reals', cols, 'exadata', rm=True)
        sql = """select ID_CIP_SEC, po, DTP 
                from pedia.mst_vacsist_set"""
        upload = []
        for id_cip, po, DTP in u.getAll(sql, 'pedia'):
            if id_cip in self.id_cip_2_cip:
                cip = self.id_cip_2_cip[id_cip]
                if cip in self.nens:
                    upload.append((cip, po,DTP))
        u.listToTable(upload, 'vacunes_mancants_reals', 'exadata')
        u.grantSelect('vacunes_mancants_reals', 'DWSISAP_ROL', 'exadata')
    
    def nens_vacunats_311222(self):
        print('taula')
        cols = """(cip varchar2(15), up varchar2(10),
                    uba varchar2(10), upinf varchar2(10), ubainf varchar2(10),
                    sexe varchar2(10), ates int,
                    institucionalitzat int, maca int,
                    VHB int, MCC int, HIB int, po int,
                    XRP int, DTP int, VARICE int, VHA int, 
                    MCB int, MACWY int, VPH int, VNC int,
                    num int, excl int)"""
        u.createTable('estat_vacunacio_311222', cols, 'exadata', rm=True)
        u.grantSelect('estat_vacunacio_311222', 'DWSISAP_ROL', 'exadata')
        print('conversio')
        sql = """SELECT usua_cip, usua_cip_cod FROM pdptb101"""
        hash_2_cip = {}
        for cip, hash in u.getAll(sql, 'pdp'):
            if cip in self.nens:
                hash_2_cip[hash] = cip
        # columns = ["comentari","id_cip_sec","indicador","up",
        #             "uba","upinf","ubainf","data_naix","edat_a",
        #             "edat_m","edat_d","sexe","ates",
        #             "institucionalitzat","maca","VHB","MCC",
        #             "HIB","PO","XRP","DTP","VARICE","VHA",
        #             "MCB","MACWY","VPH","VNC","num","excl","hash_d"]
        upload = []
        print('read & upload')
        with open("263master_vacunes_31122.csv", 'r') as file:
            csvreader = csv.reader(file) 
            for row in csvreader: 
                if row[-1] in hash_2_cip:
                    cip = hash_2_cip[row[-1]]
                    comentari, id_cip, indi, up, uba, upinf, ubainf, data_naix, edat_a, edat_m, edat_d, sexe, ates, insti, maca, vhb, mcc, hib, po, xrp, dtp, varice, vha, mcb, macwy, vph, vnc, num, exc, hash = row
                    upload.append((cip, up, uba, upinf, ubainf, sexe, ates, insti, maca, vhb, mcc, hib, po, xrp, dtp, varice, vha, mcb, macwy, vph, vnc, num, exc))
        u.listToTable(upload, 'estat_vacunacio_311222', 'exadata')

        

class Historia_vacunes():
    def __init__(self):
        self.get_nens_amb_sms()
        self.cip_to_id_cip()
        self.historia_vacunes()
    
    def get_nens_amb_sms(self):
        print('nens amb sms')
        self.nens = set()
        sql = """SELECT cip FROM dwsisap.estudi_sms"""
        for nen, in u.getAll(sql, 'exadata'):
            self.nens.add(nen)

    def cip_to_id_cip(self):
        print('id_cip_2_cip')
        self.id_cip_2_cip = {}
        hash_2_cip = {}
        sql = """SELECT usua_cip, usua_cip_cod  FROM pdptb101"""
        for cip, hash in u.getAll(sql, 'pdp'):
            if cip in self.nens:
                hash_2_cip[hash] = cip
        sql = """select hash_d, id_cip from import.u11"""
        for hash, id_cip in u.getAll(sql, 'import'):
            if hash in hash_2_cip:
                self.id_cip_2_cip[id_cip] = hash_2_cip[hash]
    
    def historia_vacunes(self):
        print('vacs registrades')
        cols = """(cip varchar2(15), data_vacuna date, data_registre date, codi varchar2(15), dosi int)"""
        u.createTable('historia_vac_nens_sms', cols, 'exadata', rm=True)
        u.grantSelect('historia_vac_nens_sms', 'DWSISAP_ROL', 'exadata')
        upload = []
        sql = """select
                    id_cip,
                    va_u_data_vac,
                    va_u_data_alta,
                    va_u_cod,
                    va_u_dosi
                from
                    import.vacunes
                where
                    va_u_data_baixa = ''"""
        for id_cip, data_vac, data_alta, codi, dosi in u.getAll(sql, 'import'):
            if id_cip in self.id_cip_2_cip:
                cip = self.id_cip_2_cip[id_cip]
                upload.append((cip, data_vac, data_alta, codi, dosi))
                if len(upload) == 10**4:
                    u.listToTable(upload, 'historia_vac_nens_sms', 'exadata')
                    upload = []
        if len(upload) != 0:
            u.listToTable(upload, 'historia_vac_nens_sms', 'exadata')


if __name__ == "__main__":
    Historia_vacunes()