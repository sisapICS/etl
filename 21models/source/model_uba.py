# coding: utf8

"""
Manolo demana calcular els models a nivell de UBA.
Apliquem les beta del model de EAP a les dades de la uba
excepte en la gma que agafem la mitjana de EAP perquè la gma actual està inflada per canvi de david a 202209
i excepte n de centres que sumem a cada uba el temps de eap
i excepte la població assignada que en el fons a nivell de EAP tradueix la mida de l equip. Apliquem la dada de EAP a uba
I no fem la part de consultoris ni direcció
Al final atdom també ho agafem a nivell EAP perquè a nivell uba hi ha moltes uba d atdom"""

any_encurs = False

import sisapUtils as u
from collections import Counter


ind_khalix = {'MA0001': {'num': 'ASSNOI', 'den': False, 'perc': False},
              'MA0004': {'num': 'edat', 'den': 'ASSNOI', 'perc': False},
              'MA0005': {'num': 'dones', 'den': 'ASSNOI', 'perc': True},
              'MA0006': {'num': 'gma', 'den': False, 'perc': False},
              'MA0007': {'num': 'aquas', 'den': False, 'perc': False},
              'MA0008': {'num': 'pens', 'den': 'ass65', 'perc': True},
              'MA0009': {'num': 'atdom', 'den': False, 'perc': True},
              'MA0010': {'num': 'ncentres', 'den': False, 'perc': False},
              'MA0002': {'num': 'insti', 'den': False, 'perc': False}
              }

            
"""
Coeficients per model adults de 45 minuts les domicili i 6 les no presencials:

'gma': {'MA0011': 1.68},
'edat': {'MA0011': 1.51},
'dones': {'MA0011': - 1.32},
'pens': {'MA0011': 0.84},
'aquas': {'MA0011': 0.12},
'ncentres': {'MA0011': 2.41},
'ASSNOI': {'MA0011': -0.18},
'atdom': {'MA0011': 3.89},
'constant': {'MA0011': 22.17}
"""
              
model_dict = {'A':{
                'gma': {'MA0011': 2.69},
                'edat': {'MA0011': 1.37},
                'dones': {'MA0011': - 1.30},
                'pens': {'MA0011': 0.99},
                'aquas': {'MA0011': 0.11},
                'ncentres': {'MA0011': 2.23},
                'ASSNOI': {'MA0011': -0.20},
                'atdom': {'MA0011': 3.37},
                'constant': {'MA0011': 27.29}
                },
             'B':{   
                'gma': {'MA0011': 2.72},
                'edat': {'MA0011': 1.27},
                'dones': {'MA0011': - 1.25},
                'pens': {'MA0011': 0.97},
                'aquas': {'MA0011': 0.11},
                'ncentres': {'MA0011': 2.13},
                'ASSNOI': {'MA0011': -0.19},
                'atdom': {'MA0011': 2.88},
                'constant': {'MA0011': 29.33}
                }
            }



indicadors_mitjana = ['MA0007', 'MA0011', 'MA0017', 'MA0018']
db = "models"

albert = ("centres", "x0001")


class Model_assignacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.dades = Counter()
        self.get_centres()
        self.get_gma()
        self.get_consultoris()
        self.get_all_centres()
        self.get_atdom()
        self.get_pob()
        self.get_consultoris_pob()
        self.get_indicadors()
        self.get_calculs_model()

    
    def get_centres(self):
        """."""
        u.printTime("centres")
        self.centres = {}
        self.regions = {}
        sql = """
            SELECT scs_codi, ics_codi, sap_codi, amb_codi, ep, aquas
              FROM cat_centres
              """
        for up, br, sap, ambit, ep, aquas in u.getAll(sql, 'nodrizas'):
            self.centres[br] = up
            self.dades[up, None, 'aquas'] += aquas
            if ep == '0208':
                ics = 'AMBITOS'
            else:
                ics = 0
            self.regions[up] = {'sap': 'SAP'+sap,
                                'ambit': 'AMB'+ambit,
                                'ics': ics}

    def get_gma(self):
        """Obté la complexitat de GMA de setembre del 2022 a nivell UP perquè la GMA actual ha augmentat molt per canvi de David que inclou problemes de salut tancats. Agafem dada de EAP"""
        u.printTime("gma")
        gma_file =  u.tempFolder + "gma_2209.txt"
        for (br,cmplx) in u.readCSV(gma_file, sep='@'):
            up = self.centres[br] if br in self.centres else 0
            self.dades[up, None, 'gma'] += float(cmplx)
            
    def get_consultoris(self):
        """."""
        u.printTime("consultoris")
        self.centres_albert = {}
        sql = """
            SELECT codi_departament, br, mf_hores, pob_a
              FROM preduffa.sisap_model_consultoris
             WHERE mf_hores > 0
               AND setmanes > 0 """
        for oficial,br, hores, pob_a in u.getAll(sql, 'redics'):
            up = self.centres[br] if br in self.centres else 0
            self.centres_albert[(up, oficial)] = True
            if hores < 20 and pob_a > 0:
                self.dades[up, None, 'consult20'] += pob_a
    
    def get_all_centres(self):
        """Agafem els que no són consultoris de taula Albert"""
        u.printTime("all_centres Albert")
        
        sql = """select a.codi_departament,  c.codi
                  from centres_centre a 
                  inner join serveis_ocupacio b on a.id=b.centre_id inner join serveis_servei c on b.servei_id=c.id 
                  where a.tipus_id in (3, 15) and donat_de_baixa <> 1"""
        for oficial, br  in u.getAll(sql, albert):
            up = self.centres[br] if br in self.centres else 0
            self.centres_albert[(up, oficial)] = True
        
        self.all_centres = Counter()
        for (up, oficial), r in self.centres_albert.items():
            self.all_centres[up] += 1   
    
    def get_atdom(self):
        """Agafem dades d atdom de setembre 2022"""
        u.printTime("atdom")
        gma_file =  u.tempFolder + "atdom_2209.txt"
        for (br,perc) in u.readCSV(gma_file, sep='@'):
            up = self.centres[br] if br in self.centres else 0
            self.dades[up, None, 'atdom'] += float(perc)
            
    def get_pob(self):
        """."""
        u.printTime("poblacio")
        self.pob_up = Counter()
        self.pob_uba = Counter()

        sql = """
            SELECT id_cip_sec, up, uba, edat, sexe, institucionalitzat,
                   nivell_cobertura, centre_codi, centre_classe 
              FROM assignada_tot
             """
        self.ambulatoris = Counter()
        for id, up, uba, edat, sexe,  insti, cob, cod, cla in u.getAll(sql, "nodrizas"):
            if insti == 0:
                self.dades[up, None, 'ASSNOIT'] += 1
                self.dades[up, uba, 'ASSNOIT'] += 1
            if edat > 14:
                self.pob_up[up] += 1
                self.pob_uba[(up,uba)] += 1
                self.pob_uba[(up,None)] += 1
                if insti == 0:
                    self.dades[up, None, 'ASSNOI'] += 1
                    self.dades[up, uba, 'ASSNOI'] += 1
                    dones = 1 if sexe == 'D' else 0
                    pens = 1 if cob == 'PN' else 0
                    self.dades[up, uba, 'edat'] += edat
                    self.dades[up, uba, 'dones'] += dones
                    if edat < 65:
                        self.dades[up, uba, 'ass65'] += 1
                        self.dades[up, uba, 'pens'] += pens
                else:
                    self.dades[up, uba, 'insti'] += 1
                self.ambulatoris[(up, cod, cla)] += 1
            
    def get_consultoris_pob(self):
        """."""
        u.printTime("poblacio consultoris")
        self.consultorispob = Counter()
        for (up, cod, cla), recomptes in self.ambulatoris.items():
            self.consultorispob[up] += 1
    
    def get_indicadors(self):
        """."""
        u.printTime("indicadors")
        upload = []
        self.parametres = {}
        for indicador, valors in ind_khalix.items():
            for (up, uba), pobtot in self.pob_uba.items():
                n = ind_khalix[indicador]['num']
                if n == 'ncentres':
                    ncent =  self.all_centres[up]
                    if ncent == 0:
                        num = self.consultorispob[up]
                    else:
                        num = self.all_centres[up]
                else:    
                    num = self.dades[up, uba, n] if (up, uba, n) in self.dades else 0
                d = ind_khalix[indicador]['den']
                perc = ind_khalix[indicador]['perc']
                if d:
                    den = self.dades[up, uba,  d] if (up, uba, d) in self.dades else 0
                    try:
                        resultat = float(num)/float(den)
                    except ZeroDivisionError:
                        resultat = 0
                    if perc:
                        resultat = resultat*100
                    upload.append([up, uba, indicador, num, den, resultat])
                else:
                    upload.append([up, uba, indicador, num, 0, num])

        self.tb = "mst_model_uba_indicadors_{}".format(tipmodel)
        columns = ["up varchar(5)", "uba varchar(10)",  "indicador varchar(10)", "num double",
                   "den double", "resultat double"]
        u.createTable(self.tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, self.tb, db)

    def get_calculs_model(self):
        """Estimem el temps mig per uba segons:
        les beta del model, aplicades a la uba o a la dada de la up en funcio de la variable (per up en gma, aquas, ncentres, atdom i pob assignada que en realitat és mida de l'eap """
        u.printTime("model")
        self.resultats_models_up = {}
        self.resultats_model = Counter()
        self.metgesesperats = {}
        upload = []
        sql = "select up,  indicador, resultat from {} where  indicador in ('MA0006','MA0007','MA0010','MA0001', 'MA0009') and uba=''".format(self.tb)
        for up,  indicador, resultat in u.getAll(sql, db):
            parametre = ind_khalix[indicador]['num']
            try:
                beta = model_dict[tipmodel][parametre]['MA0011']
            except KeyError:
                continue
            if parametre == 'ncentres':
                resultat = 1 if resultat >= 3 else 0
            elif parametre == 'ASSNOI':
                resultat = float(resultat)/float(1000)
            beta_model = float(beta)*float(resultat)
            self.resultats_models_up[(up, parametre)] = beta_model
        
        sql = "select up, uba, indicador, resultat from {} where uba <>''".format(self.tb)
        for up, uba, indicador, resultat in u.getAll(sql, db):
            parametre = ind_khalix[indicador]['num']
            try:
                beta = model_dict[tipmodel][parametre]['MA0011']
            except KeyError:
                continue
            if parametre in ('ncentres', 'atdom', 'gma','aquas', 'ASSNOI'):
                beta_model = self.resultats_models_up[(up, parametre)]
                self.resultats_model[up, uba, 'MA0011'] += beta_model
            else:
                beta_model = float(beta)*float(resultat)
                self.resultats_model[up, uba, 'MA0011'] += beta_model

        for (up, uba, tip), resultat in self.resultats_model.items():
            beta = model_dict[tipmodel]['constant'][tip]
            res = resultat + beta
            cupo = 66000/res
            pob = self.pob_uba[(up, uba)]
            mf = (pob*res)/66000
            upload.append([up,uba, tip, res, 0, res])
            upload.append([up,uba,  'MA0017', cupo, 0,  cupo])
            upload.append([up,uba,   'MA0012', mf, 0,  mf])

        u.listToTable(upload, self.tb, db)

    
        
if __name__ == '__main__':
    for tipmodel in model_dict:
        print tipmodel
        Model_assignacio()
