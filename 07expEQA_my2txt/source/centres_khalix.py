from sisapUtils import *

conn = connect(('export','aux'))
c = conn.cursor()

c.execute("drop table if exists khx_centres")
c.execute("create table khx_centres (scs_codi varchar(5),ics_codi varchar(5),unique(scs_codi,ics_codi))")
c.execute("insert into khx_centres select distinct scs_codi,ics_codi from nodrizas.cat_centres")

c.execute("drop table if exists khx_ubas")
c.execute("create table khx_ubas (up varchar(5),uba varchar(5),tipus varchar(1),unique(up,uba,tipus))")
c.execute("insert into khx_ubas select up,uba,'M' as tipus from nodrizas.assignada_tot group by up,uba having count(*)>49")
c.execute("insert ignore into khx_ubas select upinf up,ubainf uba,'I' as tipus from nodrizas.assignada_tot group by upinf,ubainf having count(*)>49")

conn.close()