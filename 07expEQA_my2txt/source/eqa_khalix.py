# coding: iso-8859-1

from sisapUtils import *
import sys

try:
    db = sys.argv[1]
except:
    sys.exit("no m'has passat arguments")

error = []
nom = {'eqa_ind': '', 'pedia': 'PED', 'social': 'SOC', 'odn': 'ODN'}
pob = {'eqa_ind': '', 'pedia': '', 'social': 'TS', 'odn': 'ODN'}
do = {'eqa_ind': True, 'pedia': True, 'social': False, 'odn': False}
eqd = {'eqa_ind': True, 'pedia': False, 'social': False, 'odn': False}
centres = "export.khx_centres"
centres_jail = "nodrizas.jail_centres"

taula = "%s.exp_khalix_up_ind" % db
query = "select indicador,concat('A','periodo'),ics_codi,conc,edat,comb,sexe,'N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi" % {'taula': taula,'centres':centres}
if db in ("eqa_ind", "pedia", "social"):
    taula = "{}.exp_khalix_up_det".format(db)
    query += " union select indicador,concat('A','periodo'),ics_codi,conc,edat,comb,sexe,'N',if(conc = 'DEN', round(n, 4), n) from %(taula)s a inner join %(centres)s b on a.up=scs_codi" % {'taula': taula, 'centres': centres}
file = "EQA%s_NOU" % nom[db]
error.append(exportKhalix(query, file))

if db == 'odn':
    taula = "%s.exp_khalix_up_ind_jail" % db
    query = "select indicador,concat('A','periodo'),ics_codi,conc,edat,comb,sexe,'N',n from %(taula)s a inner join %(centres_jail)s b on a.up=scs_codi" % {'taula': taula,'centres_jail':centres_jail}
    file = "EQA%s_JAIL" % nom[db]
    error.append(exportKhalix(query, file))

if db == 'social':
    taula = "%s.exp_khalix_piram_ind" % db
    query = "select indicador,concat('A','periodo'),piram,conc,edat,comb,sexe,'N',n from %(taula)s" % {'taula': taula}
    file = "EQA%s_PIRAM" % nom[db]
    error.append(exportKhalix(query, file))

taula = "%s.exp_khalix_up_pob" % db
varpob = "EQAPOBL%s" % pob[db]
query = "select '%(varpob)s',concat('A','periodo'),ics_codi,'NOCLI',edat,comb,sexe,'N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi"  % {'varpob': varpob, 'taula': taula, 'centres': centres}
file = "EQAPOB%s_NOU" % nom[db]
error.append(exportKhalix(query,file))

if db == 'odn':
    taula = "%s.exp_khalix_up_pob_jail" % db
    varpob = "EQAPOBL%s" % pob[db]
    query = "select '%(varpob)s',concat('A','periodo'),ics_codi,'NOCLI',edat,comb,sexe,'N',n from %(taula)s a inner join %(centres_jail)s b on a.up=scs_codi"  % {'varpob': varpob, 'taula': taula, 'centres_jail': centres_jail}
    file = "EQAPOB%s_JAIL" % nom[db]
    error.append(exportKhalix(query,file))

if db == 'eqa_ind':
    taula = "exp_khalix_up_ind_proc"
    query = "select indicador,concat('A','periodo'),ics_codi,analisis,dimensio,proces,'DIM6SET','N',n from {}.{} a inner join {} b on a.up=scs_codi".format(db, taula, centres)
    file = "EQAPROC"
    error.append(exportKhalix(query, file))
    
    taula = "exp_khalix_uba_ind_proc"
    query = "select indicador,concat('A','periodo'),concat(ics_codi,tipus,uba),analisis,dimensio,proces,'DIM6SET','N',n from {}.{} a inner join {} b on a.up=scs_codi".format(db, taula, centres)
    file = "EQAPROC_UBA"
    error.append(exportKhalix(query, file))

if db == 'eqa_ind' or db == 'pedia':
    taula = "%s.exp_khalix_up_ind" % db
    query = "select indicador,concat('A','periodo'),aga,conc,'NOCAT',comb,'DIM6SET','N',sum(n) from %(taula)s a inner join nodrizas.cat_centres b on a.up=scs_codi where aga != '' group by indicador,aga,conc,comb" % {'taula': taula}
    file = "EQA%s_AGA" % nom[db]
    error.append(exportKhalix(query, file))  

if do[db]:
    taula = "%s.exp_khalix_uba_ind" % db
    query = "select indicador,concat('A','periodo'),concat(ics_codi,tipus,uba),analisis,'NOCAT',detalle,'DIM6SET','N',valor from %(taula)s a inner join %(centres)s b on a.up=scs_codi" % {'taula': taula,'centres': centres}
    file = "EQA%s_UBA_NOU" % nom[db]
    error.append(exportKhalix(query, file))

if eqd[db]:
    taula = "%s.exp_khalix_uba_indEQD" % db
    query = "select indicador,concat('A','periodo'),concat(ics_codi,tipus,uba),analisis,'NOCAT',detalle,'DIM6SET','N',valor from %(taula)s a inner join %(centres)s b on a.up=scs_codi where valor<>0" % {'taula': taula,'centres': centres}
    file = "EQD%s_UBA_NOU" % nom[db]
    error.append(exportKhalix(query, file))

if db == 'eqa_ind':
    if any(error):
        text= 'S\'han generat els arxius de l\'EQA per Khalix, per� malauradament no s\'han pogut escriure a Cargas Pendientes; queden en local al servidor SISAP.'
    else:
        text = 'S\'han generat els arxius de l\'EQA per Khalix, i s\'han copiat a Cargas Pendientes.'
    sendSISAP('jcamus@gencat.cat', 'Arxius EQA', 'Joan', text)

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")