# coding: utf8

"""
Procés d'obtenció de les dades necessàries per a l'estudi de les variables
del model d'odontologia d'adults. Dades dels últims 12 mesos.
Les visites es treuen segons especialitat i no servei!
"""

import urllib as w
import os

import sisapUtils as u


class Frequentacio(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.dext, self.dext1a = u.getOne("select date_format(data_ext,'%Y%m%d'), date_format(date_add(date_add(data_ext, interval - 1 year),interval + 1 day), '%Y%m%d') from dextraccio", "nodrizas")
        self.get_centres()
        self.get_poblacio()
        self.get_hash()
        self.get_gma()
        self.get_medea()
        self.get_visites()
        self.export_dades_pacient()


    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres \
                where ep = '0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}

   
    def get_poblacio(self):
        """
        Obté la població d'estudi:
           - actius al tacament
           - majors de 14 anys a l'inici del període
        """
        self.dades = {}
        self.ambulatoris = set()
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = ("select id_cip_sec, codi_sector, edat, sexe, \
                       institucionalitzat, nacionalitat, nivell_cobertura, \
                       up, centre_codi, centre_classe \
                from assignada_tot \
                where edat > 14", "nodrizas")
        for id, sec, edt, sex, insti, nac, cob, up, cod, cla in u.getAll(*sql):
            if up in self.centres:
                pensio = 1 if cob == 'PN' else 0
                nac1 = 1 if nac in renta else 0
                self.dades[id] = {
                              'hash': None, 'sector': sec, 'edat': edt,
                              'sex': sex, 'gma_cod': None, 'gma_cmplx': None,
                              'gma_num': None, 'medea': None, 'insti': insti,
                              'renta': nac1, 'pensio': pensio, 'up': up, 'c_codi': cod,
                              'c_classe': cla,  '9C': 0, '9D': 0,
                              '9T': 0, '9R': 0, '9E':0, 'C': 0, 'D': 0}
                self.ambulatoris.add((cod, cla))

    def get_hash(self):
        """Conversió ID-HASH."""
        sql = ("select id_cip_sec, hash_d from eqa_u11", "nodrizas")
        for id, hash in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['hash'] = hash

    def get_gma(self):
        """Obté els tres valors de GMA."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        for id, cod, cmplx, num in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['gma_cod'] = cod
                self.dades[id]['gma_cmplx'] = cmplx
                self.dades[id]['gma_num'] = num

    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if id in self.dades and sector in valors:
                self.dades[id]['medea'] = valors[sector]


    def get_visites(self):
        """
        Agafem visites de qualsevol EAP de l'ICS i fetes en una
        agenda del servei ODN.
        Si l'agenda és per capes, separem les visites segons el seu tipus.
        En el cas de les agendes seqüencials, classifiquem les visites
        segons el lloc de realització.
        """
        sql = ("select id_cip_sec, visi_tipus_visita, \
                       visi_lloc_visita, date_format(visi_data_visita,'%Y%m%d') \
               from visites1 \
               where visi_situacio_visita = 'R' and \
                     s_espe_codi_especialitat in ('10106','10777')", "import")
        for id, tipus, lloc, dat in u.getAll(*sql):
            if  self.dext1a <= dat <= self.dext:
                if id in self.dades:
                    if tipus in ('9C', '9D', '9T', '9R', '9E'):
                        self.dades[id][tipus] += 1
                    elif lloc in ('C', 'D'):
                        self.dades[id][lloc] += 1

    @staticmethod
    def export_dades(tb, columns, dades):
        """Mètode per crear i omplir taules."""
        """Mètode per crear i omplir taules."""
        table = 'SISAP_MODEL_ODN_ADULTS_{}'.format(tb)
        columns_str = "({})".format(', '.join(columns))
        db = "permanent"
        u.createTable(table, columns_str, db, rm=True)
        u.listToTable(dades, table, db)

    def export_dades_pacient(self):
        """Exporta les dades de nivell pacient."""
        dades = [(d['hash'], d['edat'], d['sex'], d['gma_cod'], d['gma_cmplx'],
                  d['gma_num'], d['medea'], d['insti'], d['renta'],
                  d['pensio'], d['up'], d['c_codi'], d['c_classe'],  
                  d['9C'], d['9D'], d['9T'], d['9R'],  d['9E'] ,d['C'], d['D'])
                 for id, d in self.dades.items()]
        columns = ('id varchar(40)', 'edat int', 'sexe varchar(1)',
                   'gma_codi varchar(3)', 'gma_complexitat double',
                   'gma_num_croniques int', 'medea_seccio double',
                   'institucionalitzat int', 'immigracio_baixa_renta int',
                    'pensionista int', 'up varchar(5)',
                   'centre_codi varchar(10)', 'centre_classe varchar(2)', 
                   'visites_capes_9c int','visites_capes_9d int', 'visites_capes_9t int',
                   'visites_capes_9r int', 'visites_capes_9e int', 'visites_sequencials_centre int',
                   'visites_sequencials_domicili int')
        Frequentacio.export_dades("PACIENT", columns, dades)


if __name__ == '__main__':
    Frequentacio()
