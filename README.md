## Procés d'instal·lació de programari per desenvolupament SISAP ##

En **negreta** els arxius que estan a la [zona de descàrregues](http://codi.sisap.cat/etl/downloads) (Windows7 64-bit)

1. neteja inicial 
    1. desinstalar tota la porqueria per panel de control 
    1. eliminar subcarpetes de c:\Oracle (versions instantclient)
1. client oracle 
    1. descomprimir **instantclient-basic** a c:\Oracle (quedarà c:\Oracle\instantclient_x_y\***)
    1. descomprimir **instantclient-sqlplus** dins la subcarpeta anterior
    1. afegir la subcarpeta a la variable d’entorn de sistema: PATH
    1. modificar la variable d’entorn de sistema: TNS_ADMIN per apuntar a la subcarpeta
    1. copiar **TNSNAMES.ORA** a la subcarpeta
    1. crear la variable d'entorn de sistema: NLS_LANG i el seu valor: AMERICAN_AMERICA.WE8MSWIN1252
1. python
    1. instal·lar **python**
    1. comprovar que està a la variable d’entorn PATH
    1. instal·lar **cx_Oracle**
    1. instal·lar **MySQLdb**
    1. instal·lar **psutil**
    1. ~~instal·lar **six**~~
    1. instal·lar **python-dateutil** (pip install python-dateutil)
    1. ~~instal·lar **sqlAlchemy**~~
    1. instal·lar **visual c** (necessari pel següent)
    1. pip install paramiko (compte que pip només està disponible *de fàbrica* a partir de python 2.7.9; altres paquets també s'haguessin pogut instal·lar amb pip)
    1. pip install redis (idem)
    1. pip install pep8 (idem)
        1. copiar **.pep8** a l'arrel de la carpeta d'usuari
    1. pip install python-hglib (idem)
    1. pip install **numpy-1.9.3+mkl-cp27-none-win_amd64.whl**
    1. pip install tabulate
    1. pip install pydruid
    1. pip install pandas (ojo, requiere haber instalado antes numpy)
1. hg (i configuració del path.pth de python)
    1. instal·lar **tortoise** (és manera fàcil d’instalar hg, kdiff3, etc.)
    1. instal·lar **sourceTree** (suposadament més visual)
    1. editar **mercurial.ini** i col·locar-lo a l’arrel de la carpeta d’usuari (sobrescriure si s’ha instal·lat sourceTree)
    1. clonar respositori etl (reanomenar a sisap)
    1. clonar respositoris sisapUtils i secret
    1. editar **path.pth** i col·locar-lo a c:\Python27\Lib\site-packages
1. MySQL
    1. instal·lar **mariaDB**, desactivant totes les opcions i triant només Client Programs
    1. afegir subcarpeta bin a variable d’entorn PATH
    1. copiar **my.ini** a l’arrel de mariaDB
1. other
    1. instal·lar **npp**; canviar configuració tabulació (4 espais) i tema (zenburn)
    1. instal·lar **sshClient** (port 443)
    1. instal·lar **toadOracle**
    1. instal·lar **toadMySQL**
    1. instal·lar **cygwin** (o gnuwin32 si es vol més senzill) i afegir subcarpeta bin de cygwin64 a variable d’entorn PATH
1. config (sau o [gdrive leo](https://docs.google.com/document/d/1-pFimMUzBSq9T1psAGZmnTSUBlXHwNr2Ge7KR2K2E1Y/edit#heading=h.4ylne84slk56))
    1. correu
    1. carpetes compartides (\\srvfs01\Atenprim i \\srvfs01\user)
    1. impressores (ATENPRIM a BDC01 i AFERS01 a BDC01)
1. opt
    1. dropbox
    1. dual monitor
    1. 'sql*loader':

    * ADVERTENCIA! pretendes usar sql*loader?: debes seguir este bloque de instrucciones despues del paso 1. y antes de continuar desde el paso 2
     * debes saber que:
          * no se puede conseguir el sql*loader sin utilizar el "Oracle Universal Installer" y solo esta disponible facilmente la versión 11.2.1.0
          * no se puede ejecutar Oracle Universal Installer correspondiente a versiones anteriores a las ya instaladas (y desinstalar limpiamente son muchos pasos)
          * conviene tener una unica HOME y RAIZ de Oracle, por lo que escogeremos las mismas donde DESPUES instalaremos el "2.1 - instantclient 11.2.4.0" y el "2.2 - sqlplus"
     * por tanto:
          * PRIMERO debes instalar las 'database utilities' desde el "oracle universal installer" pillalo aqui: http://www.oracle.com/technetwork/database/enterprise-edition/downloads/112010-win64soft-094461.html se llama: win64_11gR2_client.zip
          * selecciona 'instalación personalizada' home:'c:\Oracle' raiz:'c:\Oracle\instantclient_11_2' y el componente:'database utilities' 
          * continua desde el paso 2 (instalación del instantclient y el sqlplus)