# coding: latin1

"""
.
"""

import collections as c
import datetime as d
import sisapUtils as u
import xlwt
import os
import sisaptools as t

def validacio():
    sql = """select id_cip_sec, hash_d, codi_sector from import.u11"""
    id_hash = {}
    for id, hash, sector in u.getAll(sql, 'import'):
        id_hash[id] = (hash, sector)

    sql = """SELECT id_cip_sec, up  FROM pedia.mst_vacsist_2023
                where up = '00105'
                and num = 0
                and ates = 1
                and id_cip_sec not in (select id_cip_sec FROM pedia.mst_vacsist
                where up = '00105'
                and num = 0
                and ates = 1)"""
    upload_llistat = []
    for id, up in u.getAll(sql, 'pedia'):
        if id in id_hash:
            upload_llistat.append((up, 'INFL', 'I', 'CAT0702', id_hash[id][0], id_hash[id][1], 0))

    u.listToTable(upload_llistat, 'altllistats', 'pdp')

def info_nens_sms():
    # id_cip_sec a hash redics
    print('id hash')
    sql = """select id_cip_sec, hash_d, codi_sector from import.u11"""
    id_hash = {}
    for id, hash, sector in u.getAll(sql, 'import'):
        id_hash[id] = hash

    # hash redics a cip
    print('hash cip')
    hash_cip = {}
    sql = """SELECT usua_cip, usua_cip_cod FROM pdptb101"""
    for cip, hash in u.getAll(sql, 'pdp'):
        hash_cip[hash] = cip
    
    # nens mal vacunats
    print('mal vacunats')
    nens = set()
    sql = """SELECT id_cip_sec FROM pedia.mst_vacsist_2023
            where num = 0
            and ates = 1
            and edat_a >= 4 and edat_a <=10"""
    for id, in u.getAll(sql, 'pedia'):
        if id in id_hash:
            hash = id_hash[id]
            if hash in hash_cip:
                nens.add(hash_cip[hash])
    
    # telefons i idiomes
    print('telefons i idiomes')
    cols = "(cip varchar(13), nom varchar(15), idioma varchar(8), telf1 varchar(10), telf2 varchar(10))"
    u.createTable('sms_vacunes_pedia', cols, 'pedia', rm=True)
    upload = dict()
    for e, sector in enumerate(u.sectors):
        print(e, sector)
        sql = """SELECT usua_cip, usua_nom, 
                    USUA_IDIOMA, USUA_TELEFON_PRINCIPAL, USUA_TELEFON_SECUNDARI 
                    FROM USUTB040"""
        for id, nom, idioma, telf1, telf2 in u.getAll(sql, sector):
            if id in nens:
                upload[id] =  (nom, idioma, telf1, telf2)
        print(len(upload))
    to_load = []
    for k, v in upload.items():
        to_load.append((k, v[0], v[1], v[2], v[3]))
    u.listToTable(to_load, 'sms_vacunes_pedia', 'pedia')

def sms():
    sql = """select nom, idioma, telf1, telf2 from pedia.sms_vacunes_pedia"""
    upload = []
    for nom, idioma, telf1, telf2 in u.getAll(sql, 'pedia'):
        telf = ''
        if telf1 != '' and len(telf1) > 0:
            if telf1[0] in ('6','7'):
                telf = telf1
        elif telf2 != '' and len(telf2) > 0:
            if telf2[0] in ('6','7'):
                telf = telf2
        if idioma[0:5] == 'CASTE':
            txt = """Hola, nos consta en la historia cl�nica de {} que le """.format(nom)
            txt += """faltan vacunas para administrar, os recomendamos """ 
            txt += """contactar con vuestro centro de atenci�n primaria y pedir cita con """
            txt += """la enfermera para actualizar la vacunaci�n de {}. """.format(nom)
            txt += """Recuerden traer el carn� de salud/vacunaci�n en el momento de la visita. """
            txt += """Si hab�is vacunado a vuestro hijo/a fuera del CAP, os agredecer�amos que nos """
            txt += """facilit�rais la informaci�n actualizada. Las vacunas evitan """
            txt += """muchas enfermedades infecciosas en Catalu�a y salvan vidas."""
        else:
            txt = """Hola, ens consta a la hist�ria cl�nica de {} que li """.format(nom)
            txt += """falten vacunes per administrar, us recomanem posar-vos """ 
            txt += """en contacte amb el vostre centre d'atenci� prim�ria i demanar cita amb """
            txt += """la infermera per tal d'actualitzar la vacunaci� de  {}. """.format(nom)
            txt += """Recordeu portar el carnet de salut/vacunaci� en el moment de la visita. """
            txt += """Si heu vacunat al vostre fill/a fora del CAP, us agrair�em que ens """
            txt += """f�ssiu arribar la informaci� actualitzada. Les vacunes eviten """
            txt += """moltes malalties infeccioses a Catalunya i salven vides."""
        if telf != '':
            upload.append((telf, txt))
    print('upload', len(upload))
    
    wb1 = xlwt.Workbook()
    ws_1 = wb1.add_sheet(u'1')
    wb2 = xlwt.Workbook()
    ws_2 = wb2.add_sheet(u'1')
    wb3 = xlwt.Workbook()
    ws_3 = wb3.add_sheet(u'1')
    wb4 = xlwt.Workbook()
    ws_4 = wb4.add_sheet(u'1')
    i = 1
    l = len(upload)/3
    iter = 1
    for e in upload:
        if iter == 1:
            ws_1.write(i, 0, e[0].decode('iso-8859-1'))
            ws_1.write(i, 1, e[1].decode('iso-8859-1'))
            i += 1
        elif iter == 2:
            ws_2.write(i, 0, e[0].decode('iso-8859-1'))
            ws_2.write(i, 1, e[1].decode('iso-8859-1'))
            i += 1
        elif iter == 3:
            ws_3.write(i, 0, e[0].decode('iso-8859-1'))
            ws_3.write(i, 1, e[1].decode('iso-8859-1'))
            i += 1
        elif iter == 4:
            ws_4.write(i, 0, e[0].decode('iso-8859-1'))
            ws_4.write(i, 1, e[1].decode('iso-8859-1'))
            i += 1
        if i%l == 0: 
            i = 1
            iter += 1
    
    wb1.save(u.tempFolder + 'vacunacio_pedia_1.xls')
    wb2.save(u.tempFolder + 'vacunacio_pedia_2.xls')
    wb3.save(u.tempFolder + 'vacunacio_pedia_3.xls')
    wb4.save(u.tempFolder + 'vacunacio_pedia_4.xls')
    with t.SFTP("sisap") as sftp:
        for file in ('vacunacio_pedia_1.xls', 'vacunacio_pedia_2.xls','vacunacio_pedia_3.xls', 'vacunacio_pedia_4.xls'):
            sftp.put(u.tempFolder + file, "sms/{}".format(file))
            os.remove(u.tempFolder + file)

def sms_disculpes():
    sql = """select nom, idioma, telf1, telf2 from pedia.sms_disculpes"""
    upload = []
    for nom, idioma, telf1, telf2 in u.getAll(sql, 'pedia'):
        telf = ''
        if telf1 !=  0:
            telf1 = str(telf1)
            if telf1[0] in ('6','7'):
                telf = telf1
        elif telf2 !=  0:
            telf2 = str(telf2)
            if telf2[0] in ('6','7'):
                telf = telf2
        if idioma[0:5] == 'CASTE':
            txt = """Hola, os informamos que hemos revisado los registros de vacunas """
            txt += """de {} y estan actualizados correctamente. Disculpad las molestias ocasionadas.""".format(nom)
        else:
            txt = """Hola, us volem informar que hem revisat el registre de les vacunes """
            txt += """de {} i est� correctament actualitzat. """.format(nom)
            txt += """Disculpeu les mol�sties ocasionades."""
        if telf != '':
            upload.append((telf, txt))
    print('upload', len(upload))
    
    wb1 = xlwt.Workbook()
    ws_1 = wb1.add_sheet(u'1')
    i = 1
    for e in upload:
        ws_1.write(i, 0, e[0].decode('iso-8859-1'))
        ws_1.write(i, 1, e[1].decode('iso-8859-1'))
        i += 1
    
    wb1.save(u.tempFolder + 'vacunacio_pedia_disculpes.xls')
    with t.SFTP("sisap") as sftp:
        file = 'vacunacio_pedia_disculpes.xls'
        sftp.put(u.tempFolder + file, "sms/{}".format(file))
        os.remove(u.tempFolder + file)

if __name__ == "__main__":
    # info_nens_sms()
    sms_disculpes()