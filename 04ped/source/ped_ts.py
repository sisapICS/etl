# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import dateutil
import dateutil.relativedelta

import sisapUtils as u


agrupadors_diagnostics_discapacitat = (7, 61, 301, 302, 303, 304, 305, 306)
codis_actuacions_treball_social = "('AZ1001','AZ1002','AZ1003','VZ1003','VZ101')"


class EQAPEDTreballSocial():
    """."""

    def __init__(self):
        """."""

        self.get_dextraccio();
        self.get_centres();                                                     print("get_centres(): executat")
        self.get_poblacio();                                                    print("get_poblacio(): executat")
        self.get_diagnostics();                                                 print("get_diagnostics(): executat")
        self.get_valoracions_socials();                                         print("get_valoracions_socials(): executat")
        self.get_variables();                                                   print("get_variables(): executat")
        self.get_indicadors();                                                  print("get_indicadors(): executat")
        self.upload_table();                                                    print("upload_master(): executat")

    
    def get_dextraccio(self):
        """ . """

        sql = """
                SELECT
                    data_ext
                FROM
                    dextraccio
	          """
        self.dextraccio, = u.getOne(sql, "nodrizas")

        print("Data d'extraccio: {}".format(self.dextraccio))


    def get_centres(self):
        """."""

        self.centres = dict()

        sql = """
                SELECT
                    scs_codi,
                    ics_codi
                FROM
                    cat_centres
              """
        for (up, br) in u.getAll(sql, "nodrizas"):
            self.centres[up] = br


    def get_poblacio(self):
        """
        entitats: (br, br + 'M' + uba, br + 'I' + ubainf
        """

        self.poblacio = dict()

        sql = """
                SELECT
                    id_cip_sec,
                    up,
                    uba,
                    upinf,
                    ubainf,
                    edat,
                    sexe,
                    ates,
                    seccio_censal,
                    institucionalitzat,
                    up_residencia,
                    maca,
                    trasplantat
                FROM
                    assignada_tot
                WHERE
                    ates = 1
                    AND edat <= 14
              """
        for id_cip_sec, up, uba, upinf, ubainf, edat, sexe, ates, seccio_censal, institucionalitzat, up_residencia, maca, trasplantat in u.getAll(sql, "nodrizas"):
            if up in self.centres:
                br = self.centres[up]
                self.poblacio[id_cip_sec] = {"up": up, "br": br, "edat": edat, "uba": uba, "upinf": upinf, "ubainf": ubainf, 
                                             "sexe": sexe, "ates": ates, "seccio_censal": seccio_censal, "maca": maca, "trasplantat": trasplantat,
                                             "institucionalitzat": institucionalitzat, "up_residencia": up_residencia}


    def get_diagnostics(self):
        """ Ens quedem amb la primera de les dates d'un dx lligat amb discapacitats """

        self.dates_diagnostics_discapacitat, self.dates_diagnostics_demencia, self.dates_diagnostics_ATDOM = c.defaultdict(set), c.defaultdict(set), c.defaultdict(set)
        self.viu_sol, self.infants_adolescents_amb_risc, self.dependencia = set(), set(), set()


        sql = """
                SELECT
                    id_cip_sec,
                    dde
                FROM
                    eqa_problemes
                WHERE
                    ps IN {}
              """.format(agrupadors_diagnostics_discapacitat)
        for id_cip_sec, data_diagnostic in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                self.dates_diagnostics_discapacitat[id_cip_sec].add(data_diagnostic)


    def get_valoracions_socials(self):
        """ Ens quedem amb la última de les dates a la qual s'ha realitzar una valoració social """
        
        self.valoracions_socials = c.defaultdict(lambda: c.defaultdict(set))

        sql = """
                SELECT
                    id_cip_sec,
                    xml_data_alta
                FROM
                    xml_detall
                WHERE
                    camp_codi IN ('Identitat_Result', 'Xarxa_Result', 'Habitatge_Result',
                                  'Economia_Result', 'Laboral_Result', 'Formacio_Result',
                                  'Nivell_Result', 'Juridica_Result', 'Capacitat_Result',
                                  'Rif_Result', 'Recursos_Result', 'Prestacions_Result')
                    AND camp_valor <> 'Sense Valorar'   
              """
        for id_cip_sec, data_valoracio_social in u.getAll(sql, "import"):
            if id_cip_sec in self.poblacio:
                self.valoracions_socials[id_cip_sec]["data_valoracio_social"].add(data_valoracio_social)


    def get_variables(self):
        """ . """

        self.dates_actuacions_treball_social = c.defaultdict(set)   

        for particio in u.multiprocess(get_variables_single, u.getSubTables("variables"), 8):
            for id_cip_sec, vu_cod_vs, data_vs in particio:
                if id_cip_sec in self.poblacio:
                    if vu_cod_vs in codis_actuacions_treball_social:
                        self.dates_actuacions_treball_social[id_cip_sec].add(data_vs)


    def get_indicadors(self):
        """ . """

        self.indicador_1202 = list()

        dextraccio_menys_36_messos = self.dextraccio - dateutil.relativedelta.relativedelta(months=36)

        # 1202

        for id_cip_sec in self.dates_diagnostics_discapacitat:
            ates = self.poblacio[id_cip_sec]["ates"]
            if ates:
                num = 0
                dde = max(self.dates_diagnostics_discapacitat[id_cip_sec])
                institucionalitzat = self.poblacio[id_cip_sec]["institucionalitzat"]
                maca = 0  # no es volen excloure i no tenim preparat eqa-ped per fer-ho com a adults
                if id_cip_sec in self.dates_actuacions_treball_social and id_cip_sec in self.valoracions_socials:
                    data_ultima_valoracio_social = max(self.valoracions_socials[id_cip_sec]["data_valoracio_social"])
                    data_ultima_actuacio_treball_social = max(self.dates_actuacions_treball_social[id_cip_sec])
                    if dextraccio_menys_36_messos < min(data_ultima_valoracio_social, data_ultima_actuacio_treball_social):
                        num = 1
                self.indicador_1202.append((id_cip_sec, dde, 'EQA1202A', ates, institucionalitzat, maca, num, 0))


    def upload_table(self):
        """."""
        cols = "(id_cip_sec int, dde date, indicador varchar(8), ates int, institucionalitzat int, maca int, num int, excl int)"
        u.createTable("eqa_ts", cols, "pedia", rm=True)
        u.listToTable(self.indicador_1202, "eqa_ts", "pedia")       


def get_variables_single(taula):
    """ . """
    
    print(taula)
    sql = """
            SELECT
                id_cip_sec,
                vu_cod_vs,
                vu_dat_act
            FROM
                {}
            WHERE
                vu_cod_vs IN {}
            """.format(taula, codis_actuacions_treball_social)
    return list(u.getAll(sql, "import"))


if __name__ == "__main__":
    EQAPEDTreballSocial()