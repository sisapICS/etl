
import sisapUtils as u
import collections as c

class EQD():
    def __init__(self):
        self.get_prev_anual()

    def get_prev_anual(self):
        """."""
        pob = {}
        Ns = c.Counter()
        sql = """select id_cip_sec, UP, EDAT, SEXE, COMB
                from PEDIA.eqa_poblacio_khx"""
        for id, up, edat, sexe, comb in u.getAll(sql)
            pob[id] = (up, edat, sexe, comb)
            Ns[(up, edat, sexe, comb)] += 1

        asmatics = c.Counter()
        sql = """select id_cip_sec 
                from NODRIZAS.EQA_PROBLEMES
                where ps = 58
                and edat < 15"""
        for id, in u.getAll(sql, 'nodrizas'):
            if id in pob:
                asmatics[pob[id]] += 1

        obesos = c.Counter()
        sql = """select id_cip_sec 
                from NODRIZAS.EQA_PROBLEMES
                where ps in (239, 267)
                and edat < 15"""
        for id, in u.getAll(sql, 'nodrizas'):
            if id in pob:
                obesos[pob[id]] += 1

        for o in obesos:
            num = obesos[o]
            if o in Ns:
                den = Ns[o]
                

        for row in u.Database(*CONNEXIO).get_all(sql):
            for nou in self.deteccio_inv[row[0]]:
                dades[(nou,) + row[1:-2]]["num"] += row[-2]
                dades[(nou,) + row[1:-2]]["den"] += row[-1]
        upload = [key + (vals["num"] / float(vals["den"]),)
                  for key, vals in dades.items()]
        u.TextFile(self.deteccio_file).write_iterable(upload, delimiter="{",
                                                      endline="\r\n")
    
    def get_pedia(self):
        sql = """select id_cip_sec, up, uba, upinf, ubainf,
                ates, institucionalitzat, maca 
                from nodrizas.assignada_tot
                where edat < 15"""
        nens = {}
        for id, up, uba, upinf, ubainf, ates, insti, maca in u.getAll(sql, 'nodrizas'):
            nens[id] = [up, uba, upinf, ubainf, ates, insti, maca]
    
    def create_table(self):
        tb = 'eqd_pedia'
        db = 'pedia'
        u.createTable(tb, '(id_cip_sec int, up varchar(5), uba varchar(5), upinf varchar(5), ubainf varchar(5), indicador varchar(8), \
                            ates int, institucionalitzat int, maca int, \
                            num int, excl int)', db, rm=True)
    
    def get_EQD0911(self):
        sql = """select id_cip_sec 
                from NODRIZAS.EQA_PROBLEMES
                where ps = 58
                and edat < 15"""
        asmatics_obs = set()
        for id, in u.getAll(sql, 'nodrizas'):
            asmatics_obs.add(id)
        
    def get_EQD0910(self):
        sql = """select id_cip_sec 
                from NODRIZAS.EQA_PROBLEMES
                where ps in (239, 267)
                and edat < 15"""
        obesos_obs = set()
        for id, in u.getAll(sql, 'nodrizas'):
            obesos_obs.add(id)
        
    def upload(self):
        pass


if __name__ == "__main__":
    EQD()