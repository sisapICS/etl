from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
from validacio import run_validacio

nod="nodrizas"
imp="import"
list_source_dbs=[nod,imp]
validation=False

indicador="EQA0718"
table_name="ped_indicador_alergias"
db_out="pedia"


class AllergiesNens(object):
    """Percentage of children between 0 and 14 years old, treated in the last 12 months with pharmacologic prescription, that have allergic medications registered."""

    def __init__(self):
        self.date_dextraccio= self.get_date_dextraccio()
        self.pacient_prescripcio=None
        self.pacient_allergies=None
        self.pacient_no_allergies=None

    def get_date_dextraccio(self):
        """Get current extraction date. Returns date in datetime.date format"""
        sql="select data_ext from {0}.dextraccio;".format(nod)
        return getOne(sql,nod)[0]
        
    def get_pacient_without_allergies(self):
        sql="select id_cip_sec from import.prealt2 where val_var= 'AL_NO_CON' AND val_hist = 1"
        self.pacient_no_allergies={id for (id,) in getAll(sql,imp)}

    def get_patiens_allergies(self):
        """Selection of those patients who are registered with drug allergies.
           The pacient_allergies attribute is defined as a set of these ids.
        """
        sql="select id_cip_sec from {0}.ram;".format(imp)
        self.pacient_allergies={id for (id,) in getAll(sql,nod)}
    
    def get_prescripcio_dates_by_pacient(self,taula):
        """Selection of those patients who have a drug prescription at the current date.
            Returns a set of identifiers of these patients."""

        sql="select id_cip_sec, ppfmc_pmc_data_ini, ppfmc_data_fi from {0};".format(taula)
        pacient_prescripcio=set(id for id,ini,fin in getAll(sql,imp) if (monthsBetween(ini,self.date_dextraccio) < 12 or monthsBetween(fin,self.date_dextraccio) < 12 ))
        return pacient_prescripcio
    
    def multiprocess_prescripcio(self):
        """get_prescripcio_dates_by_pacient method is performed for each import.tractaments partition.
           Returns the union of all the sets that have resulted from each partition.
        """
        tables = getSubTables('tractaments')
        resul_by_taula = multiprocess(self.get_prescripcio_dates_by_pacient, tables)

        self.pacient_prescripcio=set.union(*resul_by_taula)
        
    def get_indicador(self):
        """Adaptado de: sisap\08alt\source\prof_od_inr.py. Obtaining final table rows.
           From the ids of children between 0 and 14 years old, select those who are receiving prescription (their id is in self.pacient_prescripcio) and mark as numerator
           those who have drug allergies (their id is in self.pacient_allergies).
           Return rows list grouped by id.
        """
        table_rows=[]
        sql = "select id_cip_sec, ates, institucionalitzat,maca from {0}.ped_assignada where edat_a>=0 and edat_a<=14;".format(nod)
        
        for id, ates, inst, maca in getAll(sql,nod):
            if id in self.pacient_prescripcio:
                numerador=0
                if id in self.pacient_allergies or id in self.pacient_no_allergies:
                    numerador=1
                table_rows.append((id,indicador,ates,inst,maca,numerador,0))
        return table_rows
    
    def export_table(self,table,columns,db,rows):
        """Create a table from a list of rows in the specified db."""
        createTable(table, columns, db, rm=True)
        listToTable(rows, table, db)

if __name__=="__main__":
    
    eqa=AllergiesNens()

    eqa.get_date_dextraccio()
    printTime("Dia de hoy")
    
    eqa.multiprocess_prescripcio()
    printTime("Datos prescripcion")
    
    eqa.get_patiens_allergies()
    printTime("Alergias")

    eqa.get_pacient_without_allergies()
    printTime("No alergias")
    
    rows=eqa.get_indicador()
    printTime("Indicador")
    
    eqa.export_table("ped_indicador_alergias",
                     "(id_cip_sec int(11), indicador varchar(11), ates int, institucionalitzat int,maca int,num int, exclos int)",
                     "pedia",
                     rows)

    if validation:
        validation_dict= {
        1: {'tip': 'M','up': '00349', 'uba': '5'},
        2: {'tip': 'M','up': '05827', 'uba': 'MOVI'},
        3: {'tip': 'I','up': '00280', 'uba': 'INF06'},
        4: {'tip': 'M','up': '07446', 'uba': '408T'},
        5: {'tip': 'I','up': '00120', 'uba': 'INFV'},
        6: {'tip': 'I','up': '05090', 'uba': 'INF5'},
        7: {'tip': 'I','up': '00309', 'uba': 'INF08'},
        8: {'tip': 'I','up': '00055', 'uba': '11'}
        }

        run_validacio(validation_dict,db_out,table_name,indicador)

