from sisapUtils import *


ped = 'pedia'
nod = 'nodrizas'

indicador ='EQA0702'

vacunes = {'VHB': {'propostes': 569  , 'exclusions': 579}
            ,'PO': {'propostes': 568  , 'exclusions': 578}
            ,'DTP': {'propostes': 566  , 'exclusions': 576}
            ,'Hib': {'propostes': 574  , 'exclusions': 584}
            ,'xar': {'propostes': 571  , 'exclusions': 581}
            ,'rub': {'propostes': 572  , 'exclusions': 582}
            ,'par': {'propostes': 573  , 'exclusions': 583}
            ,'mcc': {'propostes': 567  , 'exclusions': 577}
            ,'vha': {'propostes': 570  , 'exclusions': 580}
           }

def get_propostes():
    propostes = {}
    sql = 'select id_cip_sec, agrupador, mesos from ped_propostes'
    for id, agr, mesos in getAll(sql, nod):
        if int(mesos) > 1:
            propostes[(id, int(agr))] = mesos
    return propostes
    
def get_exclusions():
    exclusions = {}
    sql = "select id_cip_sec, agrupador from ped_exclusions where motiu NOT in ('Z28.0', '1')"
    for id, agr in getAll(sql, nod):
        exclusions[(id, int(agr))] = True
    return exclusions
 
def get_poblacio():
    pob = {}
    sql = "select id_cip_sec, up, uba, upinf, ubainf, data_naix, edat_a, edat_m, edat_d, sexe, ates, institucionalitzat, maca from ped_assignada"
    for id, up, uba, upinf, ubainf, data_naix, edat_a, edat_m, edat_d, sexe, ates, insti, maca in getAll(sql, nod):
        pob[id] = {'up': up, 'uba': uba, 'upinf': upinf, 'ubainf': ubainf, 'data_naix': data_naix, 'edat_a': edat_a, 'edat_m': edat_m, 'edat_d': edat_d, 'sexe': sexe, 'ates': ates, 'insti': insti, 'maca': maca}
    return pob

def get_excl():
    vac_excl = {}
    sql = 'select id_cip_sec from ped_problemes where ps=557'
    for id, in getAll(sql, nod):
        vac_excl[(id)] = True
    return vac_excl
    
def do_vaccine(propostes, exclusions, pob, vac_excl):
    vacsist = {}
    for (id), rows in pob.items():
        vacsist[(id)] = {'VHB': 1, 'PO': 1, 'DTP': 1, 'Hib': 1, 'xar': 1, 'rub': 1, 'par': 1, 'mcc': 1, 'vha': 1, 'excl': 0}
        if (id) in vac_excl:
            vacsist[(id)]['excl'] = 1
        for vacuna in vacunes:
            proposta = vacunes[vacuna]['propostes']
            exclusio = vacunes[vacuna]['exclusions']
            if (id, proposta) in propostes:
                mesos = propostes[(id, proposta)]
                if int(pob[id]['edat_m']) > 18:
                    if int(mesos) > 2:
                        vacsist[(id)][vacuna] = 0
                else:
                    vacsist[(id)][vacuna] = 0
            if (id, exclusio) in exclusions:
                vacsist[(id)][vacuna] = 0
    upload = []
    for (id), valors in vacsist.items():
        x = valors['xar']
        r = valors['rub']
        p = valors['par']
        if x == 1 and r == 1 and p == 1:
            tv = 1
        else:
            tv = 0
        upload.append([id, indicador, pob[id]['up'], pob[id]['uba'], pob[id]['upinf'], pob[id]['ubainf'], pob[id]['data_naix'], pob[id]['edat_a'], pob[id]['edat_m'], pob[id]['edat_d'], pob[id]['sexe'], pob[id]['ates'], pob[id]['insti'], pob[id]['maca'], valors['VHB'], valors['mcc'], valors['Hib'], valors['PO'], tv, valors['DTP'], valors['vha'], 0, valors['excl']])
    listToTable(upload, TableMy, ped)

TableMy = 'mst_vacsist'
create = "(id_cip_sec int, indicador varchar(10) not null default'', up VARCHAR(5) NOT NULL DEFAULT '', uba VARCHAR(5) NOT NULL DEFAULT '', upinf VARCHAR(5) NOT NULL DEFAULT '',  ubainf VARCHAR(5) NOT NULL DEFAULT '',\
        data_naix DATE NOT NULL DEFAULT 0, edat_a double NULL, edat_m double NULL, edat_d double NULL, sexe VARCHAR(1) NOT NULL DEFAULT '',  ates double null,  institucionalitzat double null,\
         maca double null, VHB INT NOT NULL DEFAULT 0, MCC INT NOT NULL DEFAULT 0, HIB INT NOT NULL DEFAULT 0, PO INT NOT NULL DEFAULT 0,  XRP INT NOT NULL DEFAULT 0,  DTP INT NOT NULL DEFAULT 0,\
          VHA INT NOT NULL DEFAULT 0,  num INT NOT NULL DEFAULT 0,  excl INT NOT NULL DEFAULT 0)"
createTable(TableMy,create,ped, rm=True)

propostes = get_propostes()
exclusions = get_exclusions()
pob = get_poblacio()
vac_excl = get_excl()
do_vaccine(propostes, exclusions, pob, vac_excl)
