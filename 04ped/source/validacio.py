from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter        


#Global variables
nod="nodrizas"


def get_patients_by_up_uba(up_cod,uba_name,uba_cod):
    sql="select id_cip_sec from nodrizas.ped_assignada where up={} and {}=\"{}\";".format(up_cod,uba_name,uba_cod)
    return {id for (id,) in getAll(sql,nod)}

def get_hashd(id_cip_sec):
    sql="select hash_d from import.u11 where id_cip_sec={};".format(id_cip_sec)
    return getOne(sql,"import")[0]

def get_hashd_dict():
    sql="select id_cip_sec,hash_d from import.u11;"
    return {id:hashd for (id,hashd) in getAll(sql,"import")}

def write_rows(id,tip, up, uba):
    #row_format ="{:>15}" * 5+"\n"
    if tip =="M":
        return (id,up,uba,"")
    if tip =="I":
        return (id,up,"",uba)        
        
        
def run_validacio(validation_dict,db_out,table_name,indicador):
    med_or_enf= {
    "M":"uba",
    "I":"ubainf"
        }
    cumplen_rows=[]
    no_cumplen_rows=[]
    hash_dict=get_hashd_dict()
    printTime("Get hash dict")  

    for validation_group in validation_dict.values():
        tip=validation_group["tip"]
            
        up_cod=validation_group["up"]

        uba_name=med_or_enf[tip]
        uba_cod=validation_group["uba"]
            
        printTime(up_cod,uba_name,uba_cod)
        id_by_up_uba=get_patients_by_up_uba(up_cod,uba_name,uba_cod)

        sql="select id_cip_sec,num from {}.{};".format(db_out,table_name)

        
        cumplen=0
        no_cumplen=0

        for id,num in getAll(sql,nod):
            if id in id_by_up_uba:
                if num==1:
                    if cumplen < 10:
                        cumplen+=1
                        id_hash=hash_dict[id]
                        cumplen_rows.append(write_rows(id_hash,tip,up_cod,uba_cod))
                if num==0:
                    if no_cumplen < 10:
                         no_cumplen+=1
                         id_hash=hash_dict[id]
                         no_cumplen_rows.append(write_rows(id_hash,tip,up_cod,uba_cod))
            
        #Write a CSV final
        file_cumplen="{}_validacion_cumplen.txt".format(indicador)
        file_no_cumplen="{}_validacion_no_cumplen.txt".format(indicador)
        writeCSV(file_cumplen,cumplen_rows)
        writeCSV(file_no_cumplen,no_cumplen_rows)
    