# -*- coding: utf8 -*-

import sisapUtils as u


def comptatge():
    print('hash cip pdptb101')
    cip_to_hash = {}
    sql = """SELECT usua_cip, usua_cip_cod FROM pdptb101"""
    for cip, hash in u.getAll(sql, 'pdp'):
        cip_to_hash[cip] = hash

    print('sms')
    hashos = set()
    sql = """select cip from test.sms_vacunes_pedia"""
    for cip, in u.getAll(sql, 'test'):
        if cip in cip_to_hash:
            hashos.add(cip_to_hash[cip])

    print('hash to cip_sec')
    id_cip = set()
    sql = """select id_cip_sec, hash_d from nodrizas.eqa_u11_with_jail"""
    for id, hash in u.getAll(sql, 'nodrizas'):
        if hash in hashos:
            id_cip.add(id)

    print('variceles')
    sql = """select id_cip_sec from nodrizas.eqa_problemes
            where ps in (317,318)"""
    varicela = set()
    for cip, in u.getAll(sql, 'nodrizas'):
        if cip in id_cip:
            varicela.add(cip)

    print('errors')
    mal_identificats = 0
    sql = """select id_cip_sec from pedia.mst_vacsist_2023
            where num = 0 and varice = 0
            and vhb+mcc+hib+po+xrp+dtp+vha+mcb+macwy+vph+vnc = 11"""
    for id_cip_sec, in u.getAll(sql, 'pedia'):
        if id_cip_sec in varicela:
            mal_identificats += 1

    print('mal identificats', mal_identificats)



def sms_disculpes():
    print('hash cip pdptb101')
    cip_to_hash = {}
    hash_to_cip = {}
    sql = """SELECT usua_cip, usua_cip_cod FROM pdptb101"""
    for cip, hash in u.getAll(sql, 'pdp'):
        cip_to_hash[cip] = hash
        hash_to_cip[hash] = cip

    print('sms')
    hashos = set()
    info = {}
    sql = """select cip, nom, idioma, telf1, telf2 from test.sms_vacunes_pedia"""
    for cip, nom, idioma, telf1, telf2 in u.getAll(sql, 'test'):
        if cip in cip_to_hash:
            hashos.add(cip_to_hash[cip])
            info[cip] = [nom, idioma, telf1, telf2]

    print('hash to cip_sec')
    cip_hash = {}
    hash_sector = {}
    sql = """select id_cip_sec, codi_sector, hash_d from nodrizas.eqa_u11_with_jail"""
    for id, sector, hash in u.getAll(sql, 'nodrizas'):
        if hash in hashos:
            cip_hash[id] = hash
            hash_sector[hash] = sector

    print('variceles')
    sql = """select id_cip_sec from nodrizas.eqa_problemes
            where ps in (317,318)"""
    varicela = set()
    for cip, in u.getAll(sql, 'nodrizas'):
        if cip in cip_hash:
            varicela.add(cip)

    print('errors')
    upload = []
    sql = """select id_cip_sec from pedia.mst_vacsist_2023
            where num = 0 and varice = 0
            and vhb+mcc+hib+po+xrp+dtp+vha+mcb+macwy+vph+vnc = 11"""
    for id_cip_sec, in u.getAll(sql, 'pedia'):
        if id_cip_sec in varicela:
            hash = cip_hash[id_cip_sec]
            cip = hash_to_cip[hash]
            nom, idioma, telf1, telf2 = info[cip]
            upload.append((cip, nom, idioma, telf1, telf2))
    cols = """(cip varchar(16), nom varchar(16), idioma varchar(8), telf1 int, telf2 int)"""
    u.createTable('sms_disculpes', cols, 'pedia', rm = True)
    u.listToTable(upload, 'sms_disculpes', 'pedia')




def taules_pdp():
    print('hash cip pdptb101')
    cip_to_hash = {}
    sql = """SELECT usua_cip, usua_cip_cod FROM pdptb101"""
    for cip, hash in u.getAll(sql, 'pdp'):
        cip_to_hash[cip] = hash
    
    print('sms')
    hashos = set()
    cips = set()
    sql = """select cip from test.sms_vacunes_pedia"""
    for cip, in u.getAll(sql, 'test'):
        cips.add(cip)
        if cip in cip_to_hash:
            hashos.add(cip_to_hash[cip])
    
    print('hash to cip_sec')
    cip_hash = {}
    hash_sector = {}
    sql = """select id_cip_sec, codi_sector, hash_d from nodrizas.eqa_u11_with_jail"""
    for id, sector, hash in u.getAll(sql, 'nodrizas'):
        if hash in hashos:
            cip_hash[id] = hash
            hash_sector[hash] = sector

    print('vacunes')
    vacunes_mal_posades = {}
    sql = """select id_cip_sec, up, uba, upinf, ubainf, edat_a,
            vhb, mcc, hib, po, xrp, dtp, varice, vha, mcb, macwy,
            vph, vnc, 12-(vhb+mcc+hib+po+xrp+dtp+vha+mcb+macwy+vph+vnc+varice) as num
            from pedia.mst_vacsist_2023
            where num = 0 and edat_a between 4 and 10
            and ates = 1
            and vhb+mcc+hib+po+xrp+dtp+vha+mcb+macwy+vph+vnc < 12"""
    for id_cip_sec, up, uba, upinf, ubainf, edat, vhb, mcc, hib, po, xrp, dtp, varice, vha, mcb, macwy, vph, vnc, num in u.getAll(sql, 'pedia'):
        txt = []
        if vhb == 0: txt.append('hepatitis B')
        if mcc == 0: txt.append('meningitis C')
        if hib == 0: txt.append('haemophilus influenzae')
        if po == 0: txt.append('poliomielitis')
        if xrp == 0: txt.append('triple virica (xarampio+parotiditis+rubeola)')
        if dtp == 0: txt.append('difteria, tetanus i tosferina')
        if varice == 0: txt.append('varicella')
        if vha == 0: txt.append('hepatitis A')
        if mcb == 0: txt.append('meningitis B')
        if macwy == 0: txt.append('Antineningococcica tetravalent')
        if vph == 0: txt.append('papiloma huma')
        if vnc == 0: txt.append('antineumococcica conjugada')
        txt = tuple(txt)
        txt = ",".join(txt)
        vacunes_mal_posades[id_cip_sec] = [up, uba, upinf, ubainf, edat, num, txt] 

    sql = """select data_ext from nodrizas.dextraccio"""
    for data, in u.getAll(sql, 'nodrizas'):
        today = data

    print('pengem')
    upload = []
    upload1 = []
    for id_cip_sec in cip_hash:
        hash = cip_hash[id_cip_sec]
        sector = hash_sector[hash]
        if id_cip_sec in vacunes_mal_posades:
            up, uba, upinf, ubainf, edat, num, txt = vacunes_mal_posades[id_cip_sec]
            upload.append((hash, sector, up, uba, upinf, ubainf, edat, num, txt))
            upload1.append((hash, sector, up, uba, upinf, ubainf, edat, num, txt, today))
    
    cols = """(hash varchar(40), sector varchar(5), up varchar(5), uba varchar(10), 
            upinf varchar(5), ubainf varchar(5), edat int, num int, txt varchar(400))"""
    u.createTable('sms_pedia_pantalla', cols, 'pdp', rm=True)
    u.listToTable(upload, 'sms_pedia_pantalla', 'pdp')
    u.listToTable(upload1, 'historic_sms_pedia_pantalla', 'pdp')

    u.execute("CREATE INDEX index_1 ON pdp.sms_pedia_pantalla(up, uba)", 'pdp')
    u.execute("CREATE INDEX index_2 ON pdp.sms_pedia_pantalla(upinf, ubainf)", 'pdp')

if __name__ == "__main__":
    taules_pdp()
    