##vacsistemic
#creo variables amb els agrupadors de les vacunes
use pedia;


set @ind='EQA0702';
set @vhb=15;
set @PO=311;
set @dtp=49;
set @hib=312;
set @x=36;
set @r=37;
set @p=38;
set @mcc=313;
set @varic=314;
set @a=34;
set @mcb = 548;
set @macwy = 883;
set @vph = 782;
set @vnc = 698;
set @rot = 2001;


DROP TABLE IF EXISTS mst_vacsist;
CREATE TABLE mst_vacsist (
  id_cip_sec int,
  indicador varchar(10) not null default'',
  up VARCHAR(5) NOT NULL DEFAULT '',
  uba VARCHAR(5) NOT NULL DEFAULT '',
  upinf VARCHAR(5) NOT NULL DEFAULT '',
  ubainf VARCHAR(5) NOT NULL DEFAULT '',
  data_naix DATE NOT NULL DEFAULT 0,
  edat_a double NULL,
  edat_m double NULL,
  edat_d double NULL,
  sexe VARCHAR(1) NOT NULL DEFAULT '',
  ates double null,
  institucionalitzat double null,
  maca double null,
  VHB INT NOT NULL DEFAULT 0,
  MCC INT NOT NULL DEFAULT 0,
  HIB INT NOT NULL DEFAULT 0,
  PO INT NOT NULL DEFAULT 0,
  XRP INT NOT NULL DEFAULT 0,
  DTP INT NOT NULL DEFAULT 0,
  VARICE INT NOT NULL DEFAULT 0,
  VHA INT NOT NULL DEFAULT 0,
  MCB INT NOT NULL DEFAULT 0,
  MACWY INT NOT NULL DEFAULT 0,
  VPH INT NOT NULL DEFAULT 0,
  VNC INT NOT NULL DEFAULT 0,
  ROT INT NOT NULL DEFAULT 0,
  num INT NOT NULL DEFAULT 0,
  excl INT NOT NULL DEFAULT 0,
  comentari varchar(1000),
  unique (id_cip_sec)
)
SELECT
	id_cip_sec
   ,@ind as indicador
   ,up
   ,uba
   ,upinf
   ,ubainf
   ,data_naix
   ,edat_a
   ,edat_m
   ,edat_d
   ,sexe
   ,ates
   ,institucionalitzat
   ,maca
	,0 AS VHB
	,0 AS MCC
	,0 AS HIB
	,0 AS PO
	,0 AS XRP
	,0 AS DTP
	,0 AS VARICE
    ,0 AS VHA
	,0 AS MCB
	,0 AS MACWY
	,0 AS VPH
	,0 AS VNC
	,0 AS ROT
    ,0 as num
    ,0 as excl
	
FROM
	nodrizas.ped_assignada
;


UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.vhb = 1
WHERE
	edat_m<4
	OR (edat_m>=4 and edat_m<6 and agrupador=@vhb and dosis>=1)
	OR (edat_m>=6 and edat_m<13 and agrupador=@vhb and dosis>=2)
	OR (edat_m>=13 and agrupador=@vhb and dosis>=3)
	OR data_naix<'2003/01/01'
	OR (agrupador=@vhb and temps<=2 and dosis=1)
	OR (agrupador=@vhb and temps<=7 and dosis>=2)
	OR (agrupador=@vhb and dosisind<=dosis)
;

UPDATE mst_vacsist a INNER JOIN nodrizas.ped_immunitzats b  ON a.id_cip_sec=b.id_cip_sec
SET a.vhb = 1
WHERE
    agrupador = 515
;

UPDATE mst_vacsist a INNER JOIN nodrizas.ped_problemes b  ON a.id_cip_sec=b.id_cip_sec
SET a.vhb = 1
WHERE
    ps in (13, 14)   
;

UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.po = 1
WHERE
	edat_m<4
	OR (edat_m>=4 And edat_m<6 AND agrupador=@PO AND dosis>=1) 
	OR (edat_m>=6 And edat_m<13 AND agrupador=@PO AND dosis>=2)
	OR (edat_m>=13 and edat_m<84 AND agrupador=@PO AND dosis>=3)
	OR (edat_m>=84 AND agrupador=@PO AND dosis>=4)
	OR (maxedat>=48 AND agrupador=@PO AND dosis>=3)
	OR (agrupador=@PO AND temps<=3)
    OR (agrupador=@PO AND temps<=7 and dosis>=2)
	OR (agrupador=@PO AND dosisind<=dosis)
;



UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.dtp = 1
WHERE
	edat_m<4
	OR (edat_m>=4 AND edat_m<6 AND agrupador=@dtp AND dosis>=1)
	OR (edat_m>=6 AND edat_m<13 AND agrupador=@dtp AND dosis>=2)
	OR (edat_m>=13 and edat_m<79 AND agrupador=@dtp AND dosis>=3)
	OR (edat_m>=79 AND agrupador=@dtp AND dosis>=4)
	OR (maxedat>=48 AND agrupador=@dtp AND dosis>=3)
	OR (agrupador=@dtp AND temps<=3)
	OR (agrupador=@dtp AND temps<=7 and dosis>=2)
	OR (agrupador=@dtp AND dosisind<=dosis)
;



UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.hib = 1
WHERE
	edat_m<4
	OR (edat_m>=4 And edat_m<6 AND agrupador=@hib AND dosis>=1)
	OR (edat_m>=6 And edat_m<13 AND agrupador=@hib AND dosis>=2)
	OR (edat_m>=13 AND agrupador=@hib AND dosis>=3)
	OR edat_m>=60
	OR (maxedat>=15 AND agrupador=@hib)
	OR (maxedat>=12 and maxedat<15 AND agrupador=@hib and dosis=3)
	OR (agrupador=@hib AND dosis=1 and temps<=3)
	OR (agrupador=@hib AND dosis>=2 and temps<=7)
	OR (agrupador=@hib AND dosis>=2 and minedat>=12)
    OR (agrupador=@hib AND dosis>=1 and minedat>=15)
    OR (agrupador=@hib AND dosisind<=dosis)
;



UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.xrp = xrp+1
WHERE
	edat_m<15
	OR (edat_m>=15 And edat_m<43 AND agrupador=@x AND dosis>=1)
	OR (edat_m>=43 AND agrupador=@x  AND dosis>=2)
	OR (agrupador=@x  AND temps<=2)
	OR (agrupador=@x  AND dosisind<=dosis)
;


UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.xrp = xrp+1
WHERE
	edat_m<15
	OR (edat_m>=15 And edat_m<43 AND agrupador=@r AND dosis>=1)
	OR (edat_m>=43 AND agrupador=@r  AND dosis>=2)
	OR (agrupador=@r  AND temps<=2)
	OR (agrupador=@r  AND dosisind<=dosis)
;

UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.xrp = xrp+1
WHERE
	edat_m<15
	OR (edat_m>=15 And edat_m<43 AND agrupador=@p AND dosis>=1)
	OR (edat_m>=43 AND agrupador=@p  AND dosis>=2)
	OR (agrupador=@p  AND temps<=2)
	OR (agrupador=@p  AND dosisind<=dosis)
;

update mst_vacsist
set xrp=0 where xrp=1
;

update mst_vacsist
set xrp=1 where xrp>=3
;

update mst_vacsist
set xrp=0 where xrp<>1
;

#es considera ben vacunat de XRP si te una serie de PS (segons wiki)
alter table mst_vacsist add index(id_cip_sec)
;

update mst_vacsist a inner join nodrizas.ped_problemes b on a.id_cip_sec=b.id_cip_sec
set xrp=1 where ps in (39, 40, 42, 529)
;

alter table pedia.mst_vacsist add index(id_cip_sec);
alter table nodrizas.ped_problemes  add index(id_cip_sec);
alter table nodrizas.ped_immunitzats add index(id_cip_sec);


update pedia.mst_vacsist
set xrp=1
where (id_cip_sec in (select id_cip_sec from nodrizas.ped_problemes where ps in (108 or 109)) 
or id_cip_sec in (select id_cip_sec from nodrizas.ped_immunitzats where agrupador = 516))
and (id_cip_sec in (select id_cip_sec from nodrizas.ped_problemes where ps in (110 or 111)) 
or id_cip_sec in (select id_cip_sec from nodrizas.ped_immunitzats where agrupador = 517))
and (id_cip_sec in (select id_cip_sec from nodrizas.ped_problemes where ps in (112 or 113)) 
or id_cip_sec in (select id_cip_sec from nodrizas.ped_immunitzats where agrupador = 518));


UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.mcc = 1
WHERE
	edat_m<6
	OR (edat_m>=6 And edat_m<14 AND agrupador=@mcc AND dosis>=1)
	OR (edat_m>=14 And edat_m<168 AND agrupador=@mcc AND dosis>=2)
	OR (edat_m>=168)
	OR data_naix<'2001/01/01' #sin? fa fallar molta gent
	OR (maxedat>=12 AND agrupador=@mcc)
	OR (agrupador=@mcc AND temps<=7)
    OR (agrupador=@mcc AND dosis>=1 and minedat>=12)
	OR (agrupador=@mcc AND dosisind<=dosis)
;


UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.VARICE = 1
WHERE
	edat_m<18
	OR (data_naix>'2016/01/01' and 
	 	((edat_m>=18 AND edat_m<48 AND agrupador=@varic AND dosis>=1)
		OR (edat_m>=48 AND agrupador=@varic AND dosis>=2)
		OR (agrupador=@varic AND temps<=4)))
	OR (data_naix<='2016/01/01' and 
	 	((edat_m>=168 AND agrupador=@varic AND dosis>=2)
		OR (edat_m<168)
		OR (agrupador=@varic AND temps<=4)))
	OR (agrupador=@varic AND dosisind<=dosis)
;	

UPDATE mst_vacsist a INNER JOIN nodrizas.ped_immunitzats b ON a.id_cip_sec=b.id_cip_sec
SET a.VARICE = 1
WHERE
agrupador = 467
;

UPDATE mst_vacsist a INNER JOIN nodrizas.ped_problemes b ON a.id_cip_sec=b.id_cip_sec
SET a.VARICE = 1
WHERE
ps in (317,318,529)
;


UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.vha = 1
WHERE
	edat_m<18
	OR (data_naix>'2014/01/01' and 
	 	((edat_m>=18 AND edat_m<84 AND agrupador=@a AND dosis>=1)
		OR (edat_m>=84 AND agrupador=@a AND dosis>=2)
		OR (agrupador=@a AND temps<=7)))
	OR (data_naix<='2014/01/01' and 
	 	((edat_m>=168 AND agrupador=@a AND dosis>=2)
		OR (edat_m<168)
		OR (agrupador=@a AND temps<=7)))
	OR (agrupador=@a AND dosisind<=dosis)
;	


UPDATE mst_vacsist a INNER JOIN nodrizas.ped_problemes b ON a.id_cip_sec=b.id_cip_sec
SET a.vha = 1
WHERE
ps = 32
;

UPDATE mst_vacsist a INNER JOIN nodrizas.ped_immunitzats b  ON a.id_cip_sec=b.id_cip_sec
SET a.vha = 1
WHERE
    agrupador = 514
;

UPDATE mst_vacsist a INNER JOIN nodrizas.ped_immunitzats b  ON a.id_cip_sec=b.id_cip_sec
SET a.vha = 1
WHERE
    agrupador = 33
;


UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.mcb = 1
WHERE
	edat_m<4
	OR (data_naix>'2022/01/01' and 
	 	((edat_m>=4 AND edat_m<6 AND agrupador=@mcb AND dosis>=1)
		OR (edat_m>=6 AND edat_m<15 AND agrupador=@mcb AND dosis>=2)
		OR (edat_m>=15 AND agrupador=@mcb AND dosis>=3)
		OR (minedat>=12 AND agrupador=@mcb AND dosis>=2)
		OR (agrupador=@mcb AND temps<=3)))
	OR data_naix<='2022/01/01'
	OR (agrupador=@mcb AND dosisind<=dosis)
;


UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.vnc = 1
WHERE
	edat_m<4
	OR (data_naix>'2015/08/01' and 
	 	((edat_m>=4 AND edat_m<6 AND agrupador=@vnc AND dosis>=1)
		OR (edat_m>=6 AND edat_m<12 AND agrupador=@vnc AND dosis>=2)
		OR (edat_m>=12 AND agrupador=@vnc AND dosis>=3)
		OR (maxedat>=12 AND agrupador=@vnc AND dosis>=1)
		OR (minedat>=12 AND agrupador=@vnc AND dosis>=1)
		OR (agrupador=@vnc AND temps<=3)))
	OR (data_naix<='2015/08/01')
	OR (agrupador=@vnc AND dosisind<=dosis)
	OR edat_m >= 60
;


UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.macwy = 1
WHERE
	edat_m<168
	OR (edat_m>=168 AND agrupador=@macwy AND dosis>=1 AND maxedat>=120)
	OR (agrupador=@macwy AND dosisind<=dosis)
;


UPDATE mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.vph = 1
WHERE
	(sexe = 'H' AND data_naix < '2011/01/01')
	OR edat_m < 168
	OR (edat_m>=168 AND agrupador=@vph AND dosis>=1)
;

update mst_vacsist a LEFT JOIN nodrizas.ped_vacunes b ON a.id_cip_sec=b.id_cip_sec
set a.rot = 1
where
	data_naix < 20250101
	or edat_m < 3
	or edat_m >= 6
	or (edat_m in (3, 4) and agrupador = @rot and dosis >= 1)
	or (edat_m = 5 and agrupador = @rot and dosis = 2)
	or (agrupador = @rot and temps = 0)
;

UPDATE mst_vacsist a INNER JOIN nodrizas.ped_problemes b ON a.id_cip_sec=b.id_cip_sec
SET a.rot = 1
WHERE
    ps = 2002
;

update mst_vacsist a
set num=1 where VHB+MCC+HIB+po+XRP+DTP+VARICE+VHA+MCB+MACWY+VPH+VNC+ROT = 13
;

update mst_vacsist a inner join nodrizas.ped_problemes b on a.id_cip_sec=b.id_cip_sec
set excl=1 where ps in (557, 40, 529, 677, 787)
;

update mst_vacsist a inner join nodrizas.eqa_tractaments b on a.id_cip_sec=b.id_cip_sec
set excl=1 where farmac = 41
;

alter table pedia.mst_vacsist
add column vhb_t varchar(10);

update pedia.mst_vacsist
set vhb_t = case when vhb = 0 then 'vhb' else null end;

alter table pedia.mst_vacsist
add column mcc_t varchar(10);

update pedia.mst_vacsist
set mcc_t = case when mcc = 0 then 'mcc' else null end;

alter table pedia.mst_vacsist
add column hib_t varchar(10);

update pedia.mst_vacsist
set hib_t = case when hib = 0 then 'hib' else null end;

alter table pedia.mst_vacsist
add column po_t varchar(10);

update pedia.mst_vacsist
set po_t = case when po = 0 then 'po' else null end;

alter table pedia.mst_vacsist
add column xrp_t varchar(10);

update pedia.mst_vacsist
set xrp_t = case when xrp = 0 then 'xrp' else null end;

alter table pedia.mst_vacsist
add column dtp_t varchar(10);

update pedia.mst_vacsist
set dtp_t = case when dtp = 0 then 'dtp' else null end;

alter table pedia.mst_vacsist
add column varice_t varchar(10);

update pedia.mst_vacsist
set varice_t = case when varice = 0 then 'v' else null end;

alter table pedia.mst_vacsist
add column vha_t varchar(10);

update pedia.mst_vacsist
set vha_t = case when vha = 0 then 'vha' else null end;

alter table pedia.mst_vacsist
add column mcb_t varchar(10);

update pedia.mst_vacsist
set mcb_t = case when mcb = 0 then 'mcb' else null end;

alter table pedia.mst_vacsist
add column macwy_t varchar(10);

update pedia.mst_vacsist
set macwy_t = case when (mcc = 0 or macwy = 0) and edat_m >= 12 then 'macwy' else null end;

update pedia.mst_vacsist
set mcc_t = case when (mcc = 0 or macwy = 0) and edat_m < 12 then 'mcc' else null end;

alter table pedia.mst_vacsist
add column vph_t varchar(10);

update pedia.mst_vacsist
set vph_t = case when vph = 0 then 'vph' else null end;

alter table pedia.mst_vacsist
add column vnc_t varchar(10);

update pedia.mst_vacsist
set vnc_t = case when vnc = 0 then 'vnc' else null end;

alter table pedia.mst_vacsist
add column rot_t varchar(10);

update pedia.mst_vacsist
set rot_t = case when rot = 0 then 'rot' else null end;

update pedia.mst_vacsist 
set comentari = concat_ws(',', vhb_t, macwy_t, mcc_t, hib_t, po_t, xrp_t, dtp_t, varice_t, vha_t, mcb_t, vph_t, vnc_t, rot_t);

ALTER TABLE pedia.mst_vacsist
DROP COLUMN vhb_t, 
DROP column mcc_t, 
DROP column hib_t, 
DROP column po_t, 
DROP column xrp_t, 
DROP column dtp_t, 
DROP column varice_t, 
DROP column vha_t, 
DROP column mcb_t, 
DROP column macwy_t, 
DROP column vph_t, 
DROP column vnc_t,
DROP column rot_t;
