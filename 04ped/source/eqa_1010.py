# coding: iso-8859-1
from sisapUtils import *
import sys, datetime
from time import strftime
from collections import defaultdict, Counter
from dateutil.relativedelta import relativedelta
import csv
import re
from validacio import run_validacio

db_nod = "nodrizas"
validation = False

indicador = "EQA1010"
table_out = "ped_indicador_faring"
db_out = "pedia"

agrupadors_faring = (250, 251)
agrupadors_antib = (139, 140)
agrupadors_macrolids = (161, 162)
agrupador_alergia_penicilina = 163
agrupadors_num = 760
agrupadors_excl = (40, 761)

# Classe per calcular l'indicador de faringitis en nens
class FaringNens(object):

    def __init__(self):
        
        self.date_dextraccio = self.get_date_dextraccio()
        self.dates_dx_faringitis_ultim_any = defaultdict(set)
        self.dates_dx_faringitis_amb_antibiotic_ultim_any = defaultdict(set)
        self.num = defaultdict(lambda: defaultdict(set))
        self.table_rows = []

    def get_date_dextraccio(self):
        """Obt� la data d'extracci� actual. Retorna la data en format datetime.date"""

        sql = """
                SELECT
                    data_ext
                FROM
                    dextraccio
              """
        return getOne(sql, db_nod)[0]

    def get_diagnostics(self):
        """Recupera diagn�stics de faringitis"""

        dates_dx_faringitis_ultim_any = defaultdict(set)
        self.alergia_penicilina = set()

        sql = """
                SELECT
                    id_cip_sec,
                    dde,
                    ps
                FROM
                    ped_problemes
                WHERE
                    ps IN {}
                    OR ps = {}
              """.format(agrupadors_faring, agrupador_alergia_penicilina)
        for (id_cip_sec, data_dx, agr) in getAll(sql, db_nod):
            if agr in agrupadors_faring and monthsBetween(data_dx, self.date_dextraccio) < 12:
                dates_dx_faringitis_ultim_any[id_cip_sec].add(data_dx)
            elif agr == agrupador_alergia_penicilina:
                self.alergia_penicilina.add(id_cip_sec)

        # Filtra els diagn�stics de faringitis segons el criteri de 30 dies entre episodis
        for id_cip_sec, set_dates_dx in dates_dx_faringitis_ultim_any.iteritems():
            sorted_dates_dx = sorted(set_dates_dx)
            for i, data_dx in enumerate(sorted_dates_dx):
                if i < len(set_dates_dx) - 1:
                    if daysBetween(sorted_dates_dx[i], sorted_dates_dx[i + 1]) > 30:
                        self.dates_dx_faringitis_ultim_any[id_cip_sec].add(sorted_dates_dx[i])
                else:
                    self.dates_dx_faringitis_ultim_any[id_cip_sec].add(sorted_dates_dx[i])
        del dates_dx_faringitis_ultim_any

    def get_tractaments(self):
        """Recupera els tractaments antibi�tics prescrits junt amb els diagn�stics de faringitis"""

        sql = """
                SELECT
                    id_cip_sec,
                    pres_orig,
                    farmac
                FROM
                    eqa_tractaments
                WHERE
                    farmac IN {}
                    OR farmac in {}
              """.format(agrupadors_antib, agrupadors_macrolids)
        for (id_cip_sec, pres_orig, farmac) in getAll(sql, db_nod):
            if id_cip_sec in self.dates_dx_faringitis_ultim_any:
                for data_dx in self.dates_dx_faringitis_ultim_any[id_cip_sec]:
                    if -2 <= daysBetween(data_dx, pres_orig) <= 2:
                        if farmac in agrupadors_antib or (id_cip_sec in self.alergia_penicilina and farmac in agrupadors_macrolids):
                            self.dates_dx_faringitis_amb_antibiotic_ultim_any[id_cip_sec].add(data_dx)
        
        del self.dates_dx_faringitis_ultim_any

    def get_numerador(self):
        """Recupera el numerador dels pacients que compleixen el criteri de l'indicador"""

        sql = """
                SELECT
                    id_cip_sec,
                    agrupador,
                    dat,
                    val
                FROM
                    ped_variables
                WHERE
                    agrupador = {}
              """.format(agrupadors_num)
        for id_cip_sec, agr, dat, val in getAll(sql, db_nod):
            val = int(float(val))
            trad = {760: val == 1}
            if trad[agr]:
                self.num[agr][id_cip_sec].add(dat)

    def get_exclusions(self):
        """Recupera les exclusions dels pacients que no compleixen els criteris per ser inclosos en l'indicador"""

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    eqa_problemes
                WHERE
                    ps IN {}
              """.format(agrupadors_excl)
        self.excl = set(id_cip_sec for id_cip_sec, in getAll(sql, db_nod))

    def get_indicador(self):
        """Calcula l'indicador per als pacients que compleixen els criteris"""

        sql = """
                SELECT
                    id_cip_sec,
                    ates,
                    institucionalitzat,
                    maca
                FROM
                    ped_assignada
                WHERE
                    edat_a BETWEEN 3 AND 14
              """
        num_intersect = set.intersection(*[set(self.num[agrupadors_num].iterkeys())])
        for id_cip_sec, ates, inst, maca in getAll(sql, db_nod):
            if id_cip_sec in self.dates_dx_faringitis_amb_antibiotic_ultim_any and id_cip_sec not in self.excl:
                denominador = 1
                numerador = 0
                if id_cip_sec in num_intersect:
                    compleix_tot = dict()
                    for data_dx_far in self.dates_dx_faringitis_amb_antibiotic_ultim_any[id_cip_sec]:
                        compleix = dict()
                        compleix[agrupadors_num] = False
                        for data_dx_num in self.num[agrupadors_num][id_cip_sec]:
                            if -2 <= daysBetween(data_dx_far, data_dx_num) <= 2 or compleix[agrupadors_num]:
                                compleix[agrupadors_num] = True
                        if all(compleix.values()):
                            compleix_tot[data_dx_far] = True
                        else:
                            compleix_tot[data_dx_far] = False
                    if all(compleix_tot.values()):
                        numerador = 1
                self.table_rows.append((id_cip_sec, indicador, ates, inst, maca, denominador, numerador, 0))

    def export_table(self, table, columns, db):
        """Crea una taula a partir d'una llista de files a la base de dades especificada."""

        createTable(table, columns, db, rm=True)
        listToTable(self.table_rows, table, db)

    def execute(self):
        """Executa el proc�s per calcular l'indicador"""

        self.get_diagnostics()
        printTime('faring')
        self.get_tractaments()
        printTime('antib')
        self.get_exclusions()
        printTime('exclos')
        self.get_numerador()
        printTime('num')
        self.get_indicador()
        printTime('indicador')

if __name__ == "__main__":

    eqa = FaringNens()
    eqa.execute()
    eqa.export_table(table_out,
                     "(id_cip_sec int(11), indicador varchar(11), ates int, institucionalitzat int, maca int, den int, num int, exclos int)",
                     db_out)

    if validation:
        validation_dict = {
            1: {'tip': 'M', 'up': '07446', 'uba': '408T'},
            2: {'tip': 'M', 'up': '07446', 'uba': '403T'},
            3: {'tip': 'M', 'up': '05827', 'uba': 'MOVI'},
            4: {'tip': 'M', 'up': '00349', 'uba': '5'},
            5: {'tip': 'M', 'up': '00276', 'uba': 'UAB-G'},
            6: {'tip': 'I','up': '00120', 'uba': 'INFV'},
            7: {'tip': 'I','up': '00309', 'uba': 'INF08'},
            8: {'tip': 'I','up': '00055', 'uba': '11'},
            9: {'tip': 'I','up': '05090', 'uba': 'INF5'}
            }

        run_validacio(validation_dict,db_out,table_out,indicador)