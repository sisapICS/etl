import sisaptools as u

sql = "select id_cip_sec, edat_m, ates, institucionalitzat, maca \
       from ped_assignada where edat_m < 12"
poblacio = {row[0]: row[1:] for row in u.Database("p2262", "nodrizas").get_all(sql)}  # noqa

dades = []
sql = "select id_cip_sec, vacunat from vrs"
for id, num in u.Database("p2262", "import").get_all(sql):
    if id in poblacio:
        this = (id, "EQA0722") + poblacio[id][1:] + (1, num, 0)
        dades.append(this)

with u.Database("p2262", "pedia") as p2262:
    p2262.execute("create or replace table ped_vrs like ped_indicador_faring")
    p2262.list_to_table(dades, "ped_vrs")
