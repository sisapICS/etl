import sisaptools as u

sql = "select id_cip_sec, edat_m, ates, institucionalitzat, maca \
       from ped_assignada where edat_m > 5"
poblacio = {row[0]: row[1:] for row in u.Database("p2262", "nodrizas").get_all(sql)}  # noqa

dades = []
sql = "select id_cip_sec, vacunat, edat from vag where edat < 3 \
       union \
       select id_cip_sec, vacunat, edat from import_jail.vag where edat < 3"
for id, num, motiu in u.Database("p2262", "import").get_all(sql):
    if id in poblacio:
        edat_m, ates, instit, maca = poblacio[id]
        indicadors = set()
        if 6 <= edat_m <= 59:
              indicadors.add("EQA0721")
        if motiu == 2:
              indicadors.add("EQA0712")
        for indicador in indicadors:
              this = (id, indicador, ates, instit, maca, 1, num, 0)
              dades.append(this)

with u.Database("p2262", "pedia") as p2262:
    p2262.execute("create or replace table ped_vag like ped_indicador_faring")  # noqa
    p2262.list_to_table(dades, "ped_vag")
