import sisaptools as u

sql = "select id_cip_sec, ates, institucionalitzat, maca \
              from ped_assignada where edat_a > 11"
poblacio = {row[0]: row[1:] for row in u.Database("p2262", "nodrizas").get_all(sql)}  # noqa

dades = []
sql = "select id_cip_sec, vacunat, if(vacunat + vacunable = 0, 1, 0) \
       from covid where motiu = 2 \
       union \
       select id_cip_sec, vacunat, if(vacunat + vacunable = 0, 1, 0) \
       from import_jail.covid where motiu = 2"
for id, num, excl in u.Database("p2262", "import").get_all(sql):
    if id in poblacio:
        this = (id, "EQA0720") + poblacio[id] + (1, num, excl)
        dades.append(this)

with u.Database("p2262", "pedia") as p2262:
    p2262.execute("create or replace table ped_covid like ped_indicador_faring")  # noqa
    p2262.list_to_table(dades, "ped_covid")
