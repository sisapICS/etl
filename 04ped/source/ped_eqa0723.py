# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime
from dateutil.relativedelta import relativedelta

# Definici� de l'indicador
INDICADOR = "EQA0723"
TB_NAME = "ped_indicador_{}".format(INDICADOR)
DB_NAME = 'pedia'
COL_NAMES = "(id_cip_sec int, indicador varchar(10), ates int, institucionalitzat int, maca int, den int, num int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants.

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys22mesos': Fa 22 mesos des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys22mesos

    sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
            date_add(date_add(data_ext, INTERVAL -22 MONTH), INTERVAL + 1 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys22mesos = u.getOne(sql, "nodrizas")

def get_poblacio_ped():
    """
    Obt� la poblaci� assignada pedi�trica.

    Defineix:
        - 'poblacio_ped': Conjunt d'identificadors ('id_cip_sec') de les persones assignades.
    """
    global poblacio_ped

    sql = """
            SELECT
                id_cip_sec,
                ates,
                institucionalitzat,
                maca
            FROM
                ped_assignada
          """
    poblacio_ped = {id_cip_sec: (ates, institucionalitzat, maca) for id_cip_sec, ates, institucionalitzat, maca in u.getAll(sql, "nodrizas")}

def get_visites_ped():
    """
    """
    global visites_ped
    compteig_visites = c.defaultdict(set)

    sql = """
            SELECT
                id_cip_sec,
                edat_m
            FROM
                ped_visites
          """
    for id_cip_sec, edat_m in u.getAll(sql, "nodrizas"):
        if edat_m < 2:
            compteig_visites["menys 2 mesos"].add(id_cip_sec)
        if 6 <= edat_m <= 9:
            compteig_visites["entre 6-9 mesos"].add(id_cip_sec)

    visites_ped = compteig_visites["menys 2 mesos"] & compteig_visites["entre 6-9 mesos"]

def get_denominador():
    """
    Calcula el denominador de l'indicador.

    Defineix:
        - 'denominador': Conjunt d'identificadors de les dones embarassades ateses pel servei ASSIR durant el per�ode d'avaluaci�.
    """
    global denominador

    denominador = {id_cip_sec: poblacio_ped[id_cip_sec] for id_cip_sec in visites_ped if id_cip_sec in poblacio_ped}

def get_numerador():
    """
    """
    global numerador
    numerador = set()

    # 
    sql = """
            SELECT
                id_cip_sec
            FROM
                ped_variables
            WHERE
                (agrupador = 764 AND (edat_m BETWEEN 6 AND 15) AND (val BETWEEN 6 AND 100))
                OR (agrupador = 765 AND (edat_m BETWEEN 6 AND 15) AND (val BETWEEN 6 AND 100))
          """
    numerador = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [(id_cip_sec, INDICADOR, ates, institucionalitzat, maca, 1, 1 if id_cip_sec in numerador else 0, 0) for id_cip_sec, (ates, institucionalitzat, maca) in denominador.items()]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ped();         print("get_poblacio_ped(): {}".format(datetime.datetime.now()))
    get_visites_ped();          print("get_visites_ped(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))