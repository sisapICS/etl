# -*- coding: utf-8 -*-


import sisapUtils as u


cols = """(CODI varchar(7),DESCRIPCIO varchar(50),ES_ICS int,TIP_COD int,TIP_DES varchar(50),SUBTIP_COD int,SUBTIP_DES varchar(50),ABS_COD int,ABS_DES varchar(50),SECTOR_COD varchar(5),SECTOR_DES varchar(50),AGA_COD int,AGA_DES varchar(50),REGIO_COD int,REGIO_DES varchar(50),EP_COD int,EP_DES varchar(50))"""


upload = [
    ("USM00856", "CSM Adults Badalona 1 Est", 0,	60,	"ASSISTÈNCIA SALUT MENTAL", 61, "AMBULATÒRIA", 273, "BADALONA 2", 7867, "BARCELONÈS NORD I MARESME", 30, "Barcelonès Nord i Baix Maresme",12,"METROPOLITANA NORD",186,"Badalona Serveis Assistencials, SA"),
    ("USM00889","CSMIJ Badalona 1 Est Joan Obiols",0,60,"ASSISTÈNCIA SALUT MENTAL",61,"AMBULATÒRIA",273,"BADALONA 2",7867,"BARCELONÈS NORD I MARESME",30,"Barcelonès Nord i Baix Maresme",12,"METROPOLITANA NORD",186,"Badalona Serveis Assistencials SA"),
    ("USM01020","CSM Adults Badalona 2 Oest",0,60,"ASSISTÈNCIA SALUT MENTAL",61,"AMBULATÒRIA",277,"BADALONA 6",7867,"BARCELONÈS NORD I MARESME",30,"Barcelonès Nord i Baix Maresme",12,"METROPOLITANA NORD",186,"Badalona Serveis Assistencials, SA"),
    ("USM03249","CSM Infantil i Juvenil Badalona 2 Oest",0,60,"ASSISTÈNCIA SALUT MENTAL",61,"AMBULATÒRIA",277,"BADALONA 6",7867,"BARCELONÈS NORD I MARESME",30,"Barcelonès Nord i Baix Maresme",12,"METROPOLITANA NORD",186,"Badalona Serveis Assistencials, SA"),
    ("USM00231","CSM Adults Hospitalet de Llobregat",1,60,"ASSISTÈNCIA SALUT MENTAL",61,"AMBULATÒRIA",290,"L\'HOSPITALET DE LLOBREGAT 3",7866,"BAIX LLOBREGAT CENTRE-LITORAL I L'H N",24,"Baix Llobregat Centre i Fontsanta -L'H N",11,"METROPOLITANA SUD",208,"Institut Català de la Salut"),
    ("USM03601","CSM Adults Badia", 1,60,"ASSISTÈNCIA SALUT MENTAL",61,"AMBULATÒRIA",322,"CIUTAT BADIA",7844,"VALLÈS OCCIDENTAL EST",35,"Vallès Occidental Est",12,"METROPOLITANA NORD",208,"Institut Català de la Salut"),
    ("USM05645","CSM Infantil i Juvenil Pallars",0,60,"ASSISTÈNCIA SALUT MENTAL",61,"AMBULATÒRIA",259,"TREMP",7164,"ALT PIRINEU",3,"Pallars",71,"ALT PIRINEU I ARAN",1835,"Sant Joan de Déu Terres de Lleida"),
    ("USM07717","CSM Infantil i Juvenil Alt Urgell",0,60,"ASSISTÈNCIA SALUT MENTAL",61,"AMBULATÒRIA",233,"LA SEU D'URGELL",7164,"ALT PIRINEU",1,"Alt Urgell",71,"ALT PIRINEU I ARAN",1835,"Sant Joan de Déu Terres de Lleida"),
    ("USM07718","CSM Infantil i Juvenil Alta Ribagorça",0,60,"ASSISTÈNCIA SALUT MENTAL",61,"AMBULATÒRIA",180,"ALTA RIBAGORÇA",7164,"ALT PIRINEU",37,"Alta Ribagorça",71,"ALT PIRINEU I ARAN",1835,"Sant Joan de Déu Terres de Lleida"),
    ("USM07724","CSM Infantil i Juvenil Vielha",0,60,"ASSISTÈNCIA SALUT MENTAL",61,"AMBULATÒRIA",7,"ARAN",7107,"ARAN",4,"Aran",71,"ALT PIRINEU I ARAN",1822,"Aran Salut, Servicis Assistenciaus Int.")
]

u.createTable('cat_centres', cols, 'salutmental', rm=True)
u.listToTable(upload, 'cat_centres', 'salutmental')


upload = [("SMAC","VISITA D\'ACOLLIDA",1,"P","0","I","O"),	
        ("SMPV","PRIMERA VISITA ORDINARIA",1,"P","1","I","O"),
        ("SMTF","PRIMERA VISITA TELEFÒNICA",1,"T","1","I","O"),	
        ("SMVO","PRIMERA VISITA PER VIDEOTRUCADA",1,"V","1","I","O"),	
        ("SMUP","PRIMERA VISITA URGENT",1,"P","1","I","U"),
        ("SMAH","PRIMERA VISITA POST ALTA HOSPITALARIA",1,"P","1","I","O"),
        ("SMPF","PRIMERA VISITA PREFERENT",1,"P","1","I","P"),
        ("SMSI","VISITA SUCCESSIVA INDIVIDUAL",1,"P","2","I","O"),	
        ("SMUS","VISITA SUCCESSIVA INDIVIDUAL URGENT",1,"P","2","I","U"),	
        ("SMSS","VISITA SUCCESSIVA PROGRAMES",1,"P","2","I","O"),
        ("SMVS","SUCCESIVA/VALORACIONS",1,"P","2","I","O"),
        ("SMAL","VISITA SUCCESSIVA POST-ALTA HOSPITALARIA",1,"P","2","I","O"),
        ("SMTI","PLA DE TRACTAMENT INDIVIDUALITZAT",1,"P","2","I","O"),
        ("SMAM","ADMINISTRACIÓ TRACTAMENT METADONA",1,"P","2","I","O"),	
        ("SMVA","VISITA INTERNA DE VALORACIÓ",1,"P","2","I","O"),
        ("SMSF","VISITA SUCCESIVA FAMILIAR",1,"P","2","F","O"),
        ("SMSV","VISITA SUCCESIVA INDIVIDUAL PER VIDEOTRUCADA",1,"V","2","I","O"),
        ("SMST","VISITA SUCCESSIVA INDIVIDUAL TELEFÒNICA",1,"T","2","I","O"),
        ("SMFT","VISITA SUCCESSIVA FAMÍLIAR TELEFÒNICA",1,"T","2","F","O"),
        ("SMFV","VISITA SUCCESSIVA FAMILIAR PER VIDEOTRUCADA",1,"V","2","F","O"),	
        ("SMAP","SUCCESSIVA CONJUNTA PCP",1,"P","2","I","O"),
        ("SMPI","PSICOTERAPIA INDIVIDUAL",1,"P","2","I","O"),	
        ("SMPT","PSICOTERAPIA INDIVIDUAL TELEFÒNICA",1,"T","2","I","O"),
        ("SMVI","PSICOTERAPIA INDIVIDUAL VIDEOCONFERÈNCIA",1,"V","2","I","O"),	
        ("SMFP","PSICOTERAPIA FAMILIAR",1,"P","2","F","O"),
        ("SMPG","PSICOTERAPIA GRUPAL",1,"P","2","G","O"),
        ("SMGV","PSICOTERAPIA GRUPAL VIDEOCONFERENCIA",1,"V","2","G","O"),
        ("SMVG","ATENCIÓ GRUPAL",1,"P","2","G","O"),
        ("SMVV","ATENCIÓ GRUPAL VIDEOCONFERÈNCIA",1,"V","2","G","O"),
        ("SMDM","VISITA A DOMICILI",1,"P","","C","O"),
        ("SMSC","ATENCIÓ COMUNITÀRIA",1,"P","","C","O"),	
        ("SMIN","INTERCONSULTA EXTERNA",0,"P","","INT","O"),	
        ("SMCO","COORDINACIÓ EXTERNA",0,"P","","COOR","O"),
        ("SMEG","ESPAI DE GESTIÓ",0,"P","","COOR","O"),
        ("SMEO","ESPAI ORGANITZATIU",0,"P","","COOR","O")]	

cols = "(codi_visita varchar2(5), codi_visita_desc varchar2(100), assistencial int, presencialitat varchar2(5), primera_successiva varchar2(5), tipus_visita varchar2(10), prioritat varchar2(5))"
u.createTable('XSM_visites_cat', cols, 'exadata', rm=True)
u.listToTable(upload, 'XSM_visites_cat', 'exadata')
u.grantSelect('XSM_visites_cat', 'DWSISAP_ROL', 'exadata')

u.execute("COMMENT ON COLUMN XSM_visites_cat.assistencial IS '1 sí 0 no'", 'exadata')
u.execute("COMMENT ON COLUMN XSM_visites_cat.presencialitat IS 'P presencial, T telefònica, V videotrucada'", 'exadata')
u.execute("COMMENT ON COLUMN XSM_visites_cat.primera_successiva IS '1 primera, 2 successiva'", 'exadata')
u.execute("COMMENT ON COLUMN XSM_visites_cat.tipus_visita IS 'I individual, G grupal, F familiar, C comunitària, INT interconsulta, COOR coordinació'", 'exadata')
u.execute("COMMENT ON COLUMN XSM_visites_cat.prioritat IS 'O ordinària, P preferent, U urgent'", 'exadata')