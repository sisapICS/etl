import sisapUtils as u
import sisaptools as t
from datetime import datetime
from dateutil.relativedelta import relativedelta

cols = """(
    indi varchar(50),
    periode varchar(50),
    up varchar(50),
    analisi varchar(50),
    edat varchar(50),
    noimp varchar(50),
    sexe varchar(50),
    n varchar(50),
    val int
    )"""


# Creating tables
u.createTable('cpr_sm', cols, 'salutmental', rm=True)
u.createTable('cpr_sm_2', cols, 'salutmental', rm=True)
u.createTable('cpr_sm_VPCP', cols, 'salutmental', rm=True)
u.createTable('cpr_sm_VPCP_eap', cols, 'salutmental', rm=True)

# UP converters
up_2_br = {}
sql = """select scs_codi, ics_codi from nodrizas.cat_centres cc """
for up, br in u.getAll(sql, 'nodrizas'):
    up_2_br[up] = br


# We export the last 4 months if current month >= May, otherwise just the current year
current_date  = datetime.now() - relativedelta(months=1)
last_month = current_date.month
last_year = current_date.year
periodes = set()
iters = 0
if last_month >= 4:
    while iters <= 3:
        p_mes = str(last_month-iters) if len(str(last_month-iters)) == 2 else '0' + str(last_month-iters)
        p_any = str(last_year)[2:]
        periodes.add('A' + p_any + p_mes)
        iters += 1
else:
    for month in range(1, last_month + 1):
        p_mes = '0' + str(month)
        p_any = str(last_year)[2:]
        periodes.add('A' + p_any + p_mes)
        iters += 1

for periode in periodes:
    # CPR
    sql = """SELECT dim_0, dim_1, concat('CSM', DIM_2), 'NUM',
            DIM_4, DIM_5, DIM_6, 'N', NUMERADOR 
            FROM dwcatsalut.sm_sisap_cpr WHERE dim_6 IS not null
            AND dim_1 = '{periode}' 
            UNION 
            SELECT dim_0, dim_1, concat('CSM', DIM_2), 'DEN',
            DIM_4, DIM_5, DIM_6, 'N', DENOMINADOR  
            FROM dwcatsalut.sm_sisap_cpr WHERE dim_6 IS not null
            AND dim_1 = '{periode}'""".format(periode=periode)
    upload = []
    for row in u.getAll(sql, 'exadata'):
        upload.append(row)
    u.listToTable(upload, 'cpr_sm', 'salutmental')
    
    # CPR 2
    sql = """SELECT dim_0, dim_1, concat('CSM', DIM_2), 'NUM',
            'NOCAT', DIM_5, 'DIM6SET', 'N', NUMERADOR 
            FROM dwcatsalut.sm_sisap_cpr WHERE dim_6 IS null
            AND dim_1 = '{periode}'
            UNION 
            SELECT dim_0, dim_1, concat('CSM', DIM_2), 'DEN',
            'NOCAT', DIM_5, 'DIM6SET', 'N', DENOMINADOR  
            FROM dwcatsalut.sm_sisap_cpr WHERE dim_6 IS null
            AND dim_1 = '{periode}'""".format(periode=periode)
    upload = []
    for row in u.getAll(sql, 'exadata'):
        upload.append(row)
    u.listToTable(upload, 'cpr_sm_2', 'salutmental')
    
    # VPCP
    sql = """SELECT
                dim_0,
                dim_1,
                concat('CSM', DIM_2),
                'NOCLI',
                DIM_4,
                'NOIMP',
                DIM_6,
                'N',
                sum(NUMERADOR)
            FROM
                DWCATSALUT.SM_SISAP_PCP_CSM
            WHERE dim_1 like '{periode}%' and dim_6 IS not null
            group by
            dim_0,
                dim_1,
                concat('CSM', DIM_2),
                'NOCLI',
                DIM_4,
                'NOIMP',
                DIM_6,
                'N'
            """.format(periode=periode[:5])
    upload = []
    for row in u.getAll(sql, 'exadata'):
        upload.append(row)
    u.listToTable(upload, 'cpr_sm_VPCP', 'salutmental')
    
    # VPCP EAP
    sql = """SELECT
        dim_0,
        dim_1,
        dim_2,
        'NOCLI',
        dim_4,
        'NOIMP',
        DIM_6,
        'N',
        sum(NUMERADOR)
    FROM
        DWCATSALUT.SM_SISAP_PCP_EAP
    WHERE dim_1 like '{periode}%' and dim_6 IS not null
    group by
        dim_0,
        dim_1,
        dim_2,
        'NOCLI',
        dim_4,
        'NOIMP',
        DIM_6,
        'N'""".format(periode=periode[:5])
    print(sql)
    upload = []
    for indi, periode, up, nocli, edat, noimp, sexe, n, num in u.getAll(sql, 'exadata'):
        if up in up_2_br:
            br = up_2_br[up]
            upload.append((indi, periode, br, nocli, edat, noimp, sexe, n, num))
    u.listToTable(upload, 'cpr_sm_VPCP_eap', 'salutmental')


# Export to Khalix
u.exportKhalix('select * from salutmental.cpr_sm', 'CPRSM', force=True)
u.exportKhalix('select * from salutmental.cpr_sm_2', 'CPRSM2', force=True)
u.exportKhalix('select * from salutmental.cpr_sm_VPCP', 'VPCPTOT', force=True)    
u.exportKhalix('select * from salutmental.cpr_sm_VPCP_eap', 'VPCPTOT_EAP', force=True)


# Mailing
mail = t.Mail()
mail.to.append("roser.cantenys@catsalut.cat")
mail.to.append("bpons@gencat.cat")
mail.to.append("nmorenom.bnm.ics@gencat.cat")
mail.subject = "UPLOADING CPR SalutMental"
mail.text = 'Hola Belen!. \n Could you mirar de carregar l\'arxiu de CPR Salut mental? Thank you:))))). \n Atentament el friendy reminder, \n Roser'
mail.send()