# -*- coding: utf-8 -*-

from email import message
from typing import Any
import sisapUtils as u
from collections import defaultdict, Counter
import datetime
import time

cataleg = 'cat_salut_mental'
db = 'salutmental'

ups = ('CSM00231', 'CSM00856', 'CSM00889', 'CSM01020', 'CSM01021', 'CSM03249', 'CSM03601',
        'CSM05645', 'CSM07717', 'CSM07718', 'CSM07724')

v_totals = ('SMAC', 'SMPV', 'SMTF', 'SMVO', 'SMST', 'SMSV', 'SMSF', 'SMFT', 
        'SMFV', 'SMSI', 'SMVS', 'SMAL', 'SMTI', 'SMAM', 'SMPI', 'SMPG', 
        'SMVG', 'SMDM', 'SMSC', 'SMSS', 'SMUP', 'SMUS', 'SMCO', 'SMIN')

v_assistencials = ('SMAC', 'SMPV', 'SMTF', 'SMVO', 'SMST', 'SMSV', 'SMFT', 
                'SMFV', 'SMSI', 'SMVS', 'SMAL', 'SMTI', 'SMAM', 'SMPI', 
                'SMPG', 'SMVG', 'SMSF', 'SMDM', 'SMSC', 'SMSS', 'SMUP', 'SMUS')

v_assistencials_presencials = ('SMAC', 'SMPV', 'SMSI', 'SMVS', 'SMAL', 'SMSF', 
                        'SMTI', 'SMAM', 'SMPI', 'SMPG', 'SMVG', 'SMDM', 'SMSC', 
                        'SMSS', 'SMUP', 'SMUS')

v_assistencials_no_presencials = ('SMTF', 'SMVO', 'SMST', 'SMSV', 'SMFT', 'SMFV')

v_videotrucada = ('SMVO', 'SMSV', 'SMFV')

v_telefonica = ('SMTF', 'SMST', 'SMFT')

v_acollida = ('SMAC')

v_primeres = ('SMPV', 'SMTF', 'SMVO')

v_successives = ('SMST', 'SMSV', 'SMSF', 'SMFT', 'SMFV', 'SMSI', 'SMVS', 'SMAL', 
                'SMTI', 'SMAM', 'SMPI', 'SMPG', 'SMVG', 'SMDM', 'SMSC', 'SMSS')

v_successives_individuals = ('SMST', 'SMSV', 'SMSI', 'SMVS', 'SMAL', 'SMTI', 'SMAM', 'SMPI')

v_successives_familiars = ('SMSF', 'SMFV', 'SMFT')

v_successives_grupals = ('SMPG', 'SMVG')

v_successives_comunitaries = ('SMDM', 'SMSC', 'SMSS')

v_successives_no_presencials = ('SMST', 'SMSV', 'SMFT', 'SMFV')

v_urgents = ('SMUP', 'SMUS')


def create_cataleg():
    print('cataleg')
    u.createTable(cataleg, 
                '(indicador varchar(20),literal varchar(300),ordre int,pare varchar(10),\
                llistat int,invers int,mmin double,mint double,mmax double,\
                toShow int,wiki varchar(250),tipusvalor varchar(5))',
                db, rm=True)
    upload = [
        ('XSMVISITES01', 'Nombre de visites totals realitzades a la Xarxa de Salut Mental i Adiccions', 1, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3944/ver/', 'N'),
        ('XSMVISITES01A', 'Nombre de visites assistencials totals realitzades a la Xarxa de Salut Mental i Adiccions', 2, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3945/ver/', 'N'),
        ('XSMVISITES01AA', 'Nombre de visites d\'acollida realitzades a la Xarxa de Salut Mental i Adiccions', 3, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3923/ver/', 'N'),
        ('XSMVISITES01AB', 'Nombre de primeres visites programades realitzades a la Xarxa de Salut Mental i Adiccions', 4, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3922/ver/', 'N'),
        ('XSMVISITES01AC', 'Nombre de visites successives programades realitzades a Xarxa Salut Mental i Adiccions', 5, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3938/ver/', 'N'),
        ('XSMVISITES01ACA', 'Nombre de visites successives individuals realitzades a Xarxa de Salut Mental i Adiccions', 6, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3987/ver/', 'N'),
        ('XSMVISITES01ACB', 'Nombre de visites successives familiars realitzades a Xarxa de Salut Mental i Adiccions', 7, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3940/ver/', 'N'),
        ('XSMVISITES01ACC', 'Nombre de visites successives grupals realitzades a Xarxa de Salut Mental i Adiccions', 8, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3986/ver/', 'N'),
        ('XSMVISITES01ACD', 'Nombre de visites comunitàries realitzades a Xarxa de Salut Mental i Adiccions', 9, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3989/ver/', 'N'),
        ('XSMVISITES01AD', 'Nombre d\'urgències realitzades a Xarxa Salut Mental i Adiccions', 10, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4006/ver/', 'N'),
        ('XSMVISITES01ADA', 'Nombre de primeres visites urgents realitzades a Xarxa de Salut Mental i Adiccions', 11, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4007/ver/', 'N'),
        ('XSMVISITES01ADB', 'Nombre de visites successives urgents realitzades a Xarxa de Salut Mental i Adiccions', 12, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4008/ver/', 'N'),
        ('XSMVISITES01B', 'Nombre de visites no assistencials totals realitzades a la Xarxa de Salut Mental i Adiccions', 13, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4038/ver/', 'N'),
        ('XSMVISITES01BA', 'Nombre de coordinacions realitzades a la Xarxa de Salut Mental i Adiccions', 14, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3937/ver/', 'N'),
        ('XSMVISITES01BB', 'Nombre d\'informes realitzats a la Xarxa de Salut Mental i Adiccions', 15, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3958/ver/', 'N'),
        ('XSMVISITES02', ' Nombre de visites assistencials presencials a la Xarxa de Salut Mental i Adiccions', 16, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3949/ver/', 'N'),
        ('XSMVISITES03', 'Nombre de visites assistencials no presencials a la Xarxa de Salut Mental i Adiccions', 17, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3943/ver/', 'N'),
        ('XSMVISITES03A', 'Nombre de visites assistencials mitjançant videotrucada a la Xarxa de Salut Mental i Adiccions', 18, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3941/ver/', 'N'),
        ('XSMVISITES03B', 'Nombre de visites assistencials telefòniques realitzades a la Xarxa de Salut Mental i Adiccions', 19, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3942/ver/', 'N'),
        ('XSMVISITES04', 'Nombre de primeres visites presencials realitzades a la Xarxa de Salut Mental i Adiccions', 20, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3919/ver/', 'N'),
        ('XSMVISITES05', 'Nombre de primeres visites no presencials realitzades a la Xarxa Salut Mental i Adiccions', 21, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4077/ver/', 'N'),
        ('XSMVISITES05A', 'Nombre de primeres visites realitzades mitjançant videotrucada a la Xarxa Salut Mental i Adiccions', 22, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3921/ver/', 'N'),
        ('XSMVISITES05B', 'Nombre de primeres visites telefòniques realitzades a la Xarxa de Salut Mental i Adiccions', 23, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3920/ver/', 'N'),
        ('XSMVISITES06', 'Nombre de visites successives no presencials totals realitzades a la Xarxa de Salut Mental', 24, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3939/ver/', 'N'),
        ('XSMVISITES07', 'Percentatge de visites assistencials respecte al total de visites realitzades a Salut Mental', 25, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3967/ver/', 'RATI'),
        ('XSMVISITES07A', 'Percentatge de visites d\'acollida respecte al total de visites realitzades', 26, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4018/ver/', 'RATI'),
        ('XSMVISITES07B', 'Percentatge de primeres visites respecte al total de visites realitzades', 27, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3967/ver/', 'RATI'),
        ('XSMVISITES07C', 'Percentatge de visites successives respecte al total de visites realitzades', 28, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3968/ver/', 'RATI'),
        ('XSMVISITES07CA', 'Percentatge de visites successives individuals respecte al total de visites realitzades', 29, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4023/ver/', 'RATI'),
        ('XSMVISITES07CB', 'Percentatge de visites successives familiars respecte al total de visites realitzades', 30, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4024/ver/', 'RATI'),
        ('XSMVISITES07CC', 'Percentatge de visites successives grupals respecte al total de visites realitzades', 31, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4025/ver/', 'RATI'),
        ('XSMVISITES07CD', 'Percentatge de visites comunitàries respecte al total de visites realitzades', 32, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4026/ver/', 'RATI'),
        ('XSMVISITES07D', 'Percentatge de visites urgents respecte al total de visites realitzades', 33, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4028/ver/', 'RATI'),
        ('XSMVISITES07DA', 'Percentatge de primeres visites urgents respecte al total de visites realitzades', 34, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4029/ver/', 'RATI'),
        ('XSMVISITES07DB', 'Percentatge de visites successives urgents respecte al total de visites realitzades', 35, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4030/ver/', 'RATI'),
        ('XSMVISITES08', 'Percentatge de visites no assistencials respecte al total de visites realitzades a Salut Mental', 36, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4032/ver/', 'RATI'),
        ('XSMVISITES08A', 'Percentatge d\'informes realitzats respecte al total de visites realitzades', 37, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4031/ver/', 'RATI'),
        ('XSMVISITES08B', 'Percentatge de coordinacions realitzades respecte al total de visites realitzades', 38, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3966/ver/', 'RATI'),
        ('XSMVISITES09', 'Rati primeres visites/ visites successives realitzades a la Xarxa de Salut Mental i Adiccions', 39, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3960/ver/', 'RATI'),
    ]            
    u.listToTable(upload, cataleg, db)

def get_extraccio():
    print('extraccio')
    sql = """select data_ext from nodrizas.dextraccio"""
    for data, in u.getAll(sql, 'nodrizas'):
        mes = data.strftime('%m')
        any = data.year
        dia = data.strftime('%d')
        print(type(data), dia, mes, any)
    return mes, any, dia

class SalutMental():
    def __init__(self, mes, any, dia, codis):
        self.mes = mes
        self.any = any
        self.dia = dia
        self.codis = codis
        self.create_table()
        self.get_edats()
        #self.UP_ICS()
        self.INDI()
        self.to_khalix()

    def create_table(self):
        cols = '(indicador varchar(20), up varchar(8), professional varchar(9), especialitat varchar(40), \
                modul varchar(5), servei varchar(5), edat varchar(10), sexe varchar(10), tipus varchar(3),  valor int)'
        u.createTable('XSMVISITES_indicadors', cols, 'salutmental', rm = True)

    def get_edats(self):
        print('edats')
        self.edats = set()
        self.pob = dict()
        sql = """SELECT C_CIP, C_EDAT_ANYS, C_SEXE FROM DBS"""
        for id, edat, sexe in u.getAll(sql, 'redics'):
            sexe = u.sexConverter(sexe)
            age = u.ageConverter(edat)
            self.pob[id] = (age, sexe)
            if edat <= 35 and edat >= 14:
                self.edats.add(id)

    # def UP_ICS(self):
    #     print('ups')
    #     self.ups = {}
    #     sql = """select scs_codi, ics_codi from nodrizas.cat_centres"""
    #     for up, ics in u.getAll(sql, 'nodrizas'):
    #         self.ups[up] = ics

    def INDI(self):
        print('indicadors')
        XSMVISITES01AA, XSMVISITES01AB = Counter(), Counter()
        XSMVISITES01ACA, XSMVISITES01ACB, XSMVISITES01ACC, XSMVISITES01ACD = Counter(), Counter(), Counter(), Counter()
        XSMVISITES01ADA, XSMVISITES01ADB = Counter(), Counter()
        XSMVISITES01BA, XSMVISITES01BB = Counter(), Counter()
        XSMVISITES02, XSMVISITES03A, XSMVISITES03B = Counter(), Counter(), Counter()
        XSMVISITES04, XSMVISITES05A, XSMVISITES05B = Counter(), Counter(), Counter()
        XSMVISITES06, XSMVISITES01AD = Counter(), Counter()
        sql = """select VISI_USUARI_CIP, concat('CSM',visi_up), visi_tipus_visita, 
                espe_especialitat, VISI_SITUACIO_VISITA, VISI_DNI_PROV_RESP,
                VISI_MODUL_CODI_MODUL, VISI_SERVEI_CODI_SERVEI 
                from redics.vistb043r
                WHERE VISI_DATA_VISITA >= add_months(to_date('{}/{}/{}', 'dd/mm/yyyy'), -12)
                AND VISI_TIPUS_VISITA IN {}""".format(self.dia, self.mes, self.any, self.codis)
        print(sql)

        for id, up, tipus, especialitat, situacio, professional, modul, servei in u.getAll(sql, 'redics'):
            if id in self.pob:
                age, sex = self.pob[id]
                if up in ups:
                    if situacio == 'R':
                        if tipus in v_assistencials:
                            if tipus in v_acollida:
                                XSMVISITES01AA[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                            elif tipus in v_primeres:
                                XSMVISITES01AB[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                            elif tipus in v_successives:
                                if tipus in v_successives_individuals:
                                    XSMVISITES01ACA[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                                elif tipus in v_successives_familiars:
                                    XSMVISITES01ACB[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                                elif tipus in v_successives_grupals:
                                    XSMVISITES01ACC[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                                elif tipus in v_successives_comunitaries:
                                    XSMVISITES01ACD[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més # POTSER AIXÒ NO ÉS SUCCESSIVA!!!!
                            elif tipus in v_urgents:
                                if tipus == 'SMUP':
                                    XSMVISITES01ADA[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                                    XSMVISITES01AD[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                                elif tipus == 'SMUS':
                                    XSMVISITES01ADB[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                                    XSMVISITES01AD[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                        elif tipus == 'SMCO':
                            XSMVISITES01BA[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                        elif tipus == 'SMIN':
                            XSMVISITES01BB[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                        if tipus in v_assistencials_presencials:
                            XSMVISITES02[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                        if tipus in v_assistencials_no_presencials:
                            if tipus in v_videotrucada:
                                XSMVISITES03A[(up, professional, especialitat, modul, servei, age, sex)] += 1
                            elif tipus in v_telefonica:
                                XSMVISITES03B[(up, professional, especialitat, modul, servei, age, sex)] += 1
                        if tipus == 'SMPV':
                            XSMVISITES04[(up, professional, especialitat, modul, servei, age, sex)] += 1
                        elif tipus == 'SMVO':
                            XSMVISITES05A[(up, professional, especialitat, modul, servei, age, sex)] += 1
                        elif tipus == 'SMTF':
                            XSMVISITES05B[(up, professional, especialitat, modul, servei, age, sex)] += 1
                        if tipus in v_successives_no_presencials:
                            XSMVISITES06[(up, professional, especialitat, modul, servei, age, sex)] += 1

        print('XSMVISITES01AA', len(XSMVISITES01AA), 'XSMVISITES01AB', len(XSMVISITES01AB),
                'XSMVISITES01ACA', len(XSMVISITES01ACA), 'XSMVISITES01ACB', len(XSMVISITES01ACB), 
                'XSMVISITES01ACC', len(XSMVISITES01ACC), 'XSMVISITES01ACD', len(XSMVISITES01ACD),
                'XSMVISITES01ADA', len(XSMVISITES01ADA), 'XSMVISITES01ADB', len(XSMVISITES01ADB),
                'XSMVISITES01BA', len(XSMVISITES01BA), 'XSMVISITES01BB', len(XSMVISITES01BB),
                'XSMVISITES02', len(XSMVISITES02), 'XSMVISITES03A', len(XSMVISITES03A), 'XSMVISITES03B', len(XSMVISITES03B),
                'XSMVISITES04', len(XSMVISITES04), 'XSMVISITES05A', len(XSMVISITES05A), 'XSMVISITES05B', len(XSMVISITES05B),
                'XSMVISITES06', len(XSMVISITES06))

        print('anem a penjar')
        upload = []
        XSMVISITES01 = Counter()
        for k, value in XSMVISITES01AA.items():
            upload.append(('XSMVISITES01AA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07A', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) # no den
            upload.append(('XSMVISITES07', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) 
            XSMVISITES01[k] += value
        for k, value in XSMVISITES01AB.items():
            upload.append(('XSMVISITES01AB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07B', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) # no den
            upload.append(('XSMVISITES07', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES09', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01[k] += value
        XSMVISITES01AC = Counter()
        for k, value in XSMVISITES01ACA.items():
            upload.append(('XSMVISITES01ACA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07CA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) # no den
            upload.append(('XSMVISITES07C', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01[k] += value
            XSMVISITES01AC[k] += value
        for k, value in XSMVISITES01ACB.items():
            upload.append(('XSMVISITES01ACB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07CB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) # no den
            upload.append(('XSMVISITES07C', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01[k] += value
            XSMVISITES01AC[k] += value
        for k, value in XSMVISITES01ACC.items():
            upload.append(('XSMVISITES01ACC', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07CC', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) # no den
            upload.append(('XSMVISITES07C', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01[k] += value
            XSMVISITES01AC[k] += value
        for k, value in XSMVISITES01ACD.items():
            upload.append(('XSMVISITES01ACD', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07CD', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) # no den
            upload.append(('XSMVISITES07C', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01[k] += value
            XSMVISITES01AC[k] += value
        for k, value in XSMVISITES01AD.items():
            upload.append(('XSMVISITES01AD', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES01ADA.items():
            upload.append(('XSMVISITES01ADA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07DA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) # no den
            upload.append(('XSMVISITES07D', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01[k] += value
        for k, value in XSMVISITES01ADB.items():
            upload.append(('XSMVISITES01ADB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07DB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) # no den
            upload.append(('XSMVISITES07D', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) # no den
            upload.append(('XSMVISITES07', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01[k] += value
        for k, value in XSMVISITES01BA.items():
            upload.append(('XSMVISITES01BA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES08B', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) # no den
            upload.append(('XSMVISITES08', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01[k] += value
        for k, value in XSMVISITES01BB.items():
            upload.append(('XSMVISITES01BB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES08A', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) # no den
            upload.append(('XSMVISITES08', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) 
            XSMVISITES01[k] += value
        for k, value in XSMVISITES02.items():
            upload.append(('XSMVISITES02', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES03A.items():
            upload.append(('XSMVISITES03A', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES03B.items():
            upload.append(('XSMVISITES03B', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES04.items():
            upload.append(('XSMVISITES04', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES05A.items():
            upload.append(('XSMVISITES05A', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES05B.items():
            upload.append(('XSMVISITES05B', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES06.items():
            upload.append(('XSMVISITES06', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES01AC.items():
            upload.append(('XSMVISITES01AC', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES09', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))
        for k, value in XSMVISITES01.items():
            for indi in ('XSMVISITES08', 'XSMVISITES08A', 'XSMVISITES08B', 'XSMVISITES07D', 'XSMVISITES07DA', 'XSMVISITES07DB', 'XSMVISITES07CD',
                    'XSMVISITES07C', 'XSMVISITES07CC', 'XSMVISITES07CB', 'XSMVISITES07CA', 'XSMVISITES07A', 'XSMVISITES07B', 'XSMVISITES07'):
                upload.append((indi, k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))

        u.listToTable(upload, 'XSMVISITES_indicadors', 'salutmental')

    def to_khalix(self):
        print('khalix')
        sql = """ 
                SELECT indicador, 'Aperiodo', up, tipus, edat, 'NOIMP', sexe, 'N', sum(valor)
                                    FROM salutmental.XSMVISITES_indicadors
                                    where up in {}
                                    group by indicador, up, tipus, edat, sexe""".format(ups)
        
        u.exportKhalix(sql, 'XSMVISITES')


if __name__ == "__main__":
    if u.IS_MENSUAL:
        create_cataleg()
        mes, any, dia = get_extraccio()
        codis = ('SMDM', 'SMUS', 'SMUP', 'SMFV', 'SMFT', 
                'SMSI', 'SMSC', 'SMSF', 'SMIN', 
                'SMSS', 'SMST', 'SMSV', 'SMVS', 
                'SMAC', 'SMTI', 'SMCO', 'SMTF', 
                'SMVG', 'SMAM', 'SMAL', 'SMPG', 
                'SMPI', 'SMPV', 'SMVO')
        SalutMental(mes, any, dia, codis)

    # create_cataleg()
    # print('gener')
    # # u.execute("UPDATE DEXTRACCIO SET DATA_EXT = '2022-01-31'", "NODRIZAS")
    # # u.execute("UPDATE DEXTRACCIO SET MENSUAL = 1", "NODRIZAS")
    # # mes, any, dia = get_extraccio()
    # codis = ('SMDM', 'SMUS', 'SMUP', 'SMFV', 'SMFT', 
    #         'SMSI', 'SMSC', 'SMSF', 'SMIN', 
    #         'SMSS', 'SMST', 'SMSV', 'SMVS', 
    #         'SMAC', 'SMTI', 'SMCO', 'SMTF', 
    #         'SMVG', 'SMAM', 'SMAL', 'SMPG', 
    #         'SMPI', 'SMPV', 'SMVO')
    # SalutMental(1, 2022, 31, codis)
    # print('febrer')
    # # u.execute("UPDATE DEXTRACCIO SET DATA_EXT = '2022-02-28'", "NODRIZAS")
    # # u.execute("UPDATE DEXTRACCIO SET MENSUAL = 1", "NODRIZAS")
    # # mes, any, dia = get_extraccio()
    # # codis = ('SMDM', 'SMUS', 'SMUP', 'SMFV', 'SMFT', 
    # #         'SMSI', 'SMSC', 'SMSF', 'SMIN', 
    # #         'SMSS', 'SMST', 'SMSV', 'SMVS', 
    # #         'SMAC', 'SMTI', 'SMCO', 'SMTF', 
    # #         'SMVG', 'SMAM', 'SMAL', 'SMPG', 
    # #         'SMPI', 'SMPV', 'SMVO')
    # SalutMental(2, 2022, 28, codis)
    # print('març')
    # # u.execute("UPDATE DEXTRACCIO SET DATA_EXT = '2022-03-31'", "NODRIZAS")
    # # u.execute("UPDATE DEXTRACCIO SET MENSUAL = 1", "NODRIZAS")
    # # mes, any, dia = get_extraccio()
    # # codis = ('SMDM', 'SMUS', 'SMUP', 'SMFV', 'SMFT', 
    # #         'SMSI', 'SMSC', 'SMSF', 'SMIN', 
    # #         'SMSS', 'SMST', 'SMSV', 'SMVS', 
    # #         'SMAC', 'SMTI', 'SMCO', 'SMTF', 
    # #         'SMVG', 'SMAM', 'SMAL', 'SMPG', 
    # #         'SMPI', 'SMPV', 'SMVO')
    # SalutMental(3, 2022, 31, codis)
    # print('abril')
    # # u.execute("UPDATE DEXTRACCIO SET DATA_EXT = '2022-04-30'", "NODRIZAS")
    # # u.execute("UPDATE DEXTRACCIO SET MENSUAL = 1", "NODRIZAS")
    # # mes, any, dia = get_extraccio()
    # # codis = ('SMDM', 'SMUS', 'SMUP', 'SMFV', 'SMFT', 
    # #         'SMSI', 'SMSC', 'SMSF', 'SMIN', 
    # #         'SMSS', 'SMST', 'SMSV', 'SMVS', 
    # #         'SMAC', 'SMTI', 'SMCO', 'SMTF', 
    # #         'SMVG', 'SMAM', 'SMAL', 'SMPG', 
    # #         'SMPI', 'SMPV', 'SMVO')
    # SalutMental(4, 2022, 30, codis)
    # print('maig')
    # # u.execute("UPDATE DEXTRACCIO SET DATA_EXT = '2022-05-31'", "NODRIZAS")
    # # u.execute("UPDATE DEXTRACCIO SET MENSUAL = 1", "NODRIZAS")
    # # mes, any, dia = get_extraccio()
    # # codis = ('SMDM', 'SMUS', 'SMUP', 'SMFV', 'SMFT', 
    # #         'SMSI', 'SMSC', 'SMSF', 'SMIN', 
    # #         'SMSS', 'SMST', 'SMSV', 'SMVS', 
    # #         'SMAC', 'SMTI', 'SMCO', 'SMTF', 
    # #         'SMVG', 'SMAM', 'SMAL', 'SMPG', 
    # #         'SMPI', 'SMPV', 'SMVO')
    # SalutMental(5, 2022, 31, codis)
    # # u.execute("UPDATE DEXTRACCIO SET DATA_EXT = '2022-06-29'", "NODRIZAS")