
import sisapUtils as u
import collections as c


TIPUS_VISITES = {
    'primera': ('SMPV', 'SMTF', 'SMVO'),
    'seguiment': ('SMST', 'SMSV', 'SMSF', 'SMFT', 'SMFV', 'SMSI', 'SMVS', 'SMAL', 'SMTI', 'SMAM', 'SMVG', 'SMSC', 'SMSS', 'SMPT'),
    'psico_individual': ('SMPI'),
    'psico_grupal': ('SMPG', 'SMVG', 'SMGV', 'SMVV'),
    'psico_familiar': ('SMSF', 'SMFV', 'SMFT'),
    'domicilaries': ('SMDM'),
    'no_programades': ('SMUP', 'SMUS')
}


class VisitesSM():
    def __init__(self):
        self.createStructure()
        self.get_centres()
        self.get_pacients()
        self.get_professional()
        self.get_info_sectors()

    def createStructure(self):
        cols = """(up_visi varchar2(5), regio varchar2(2), nia number(38,0), sexe number(38,0), 
                    naix date, abs varchar2(3), ambit varchar2(5), rs varchar2(2), 
                    afiliacio varchar2(2), localitat varchar2(7), data_visi date, tipus_visita varchar2(30), 
                    prof varchar2(5), prof_desc varchar2(40), 
                    flag_prova_c number(38,0), d_inici date, d_fi date, 
                    up_deriv varchar2(5), dx1 varchar2(50), 
                    dx2 varchar2(50), dx3 varchar2(50), dx4 varchar2(50), dx5 varchar2(50), 
                    dx6 varchar2(50), dx7 varchar2(50), dx8 varchar2(50), dx9 varchar2(50), 
                    dx10 varchar2(50))"""
        u.createTable('visites_sm', cols, 'exadata', rm=True)
        u.grantSelect('visites_sm', 'DWSISAP_ROL', 'exadata')
        u.grantSelect('visites_sm', 'DWRENV_SALUT_MENTAL_SELECT', 'exadata')
    
    def get_centres(self):
        self.centres = {}
        sql = """SELECT up_cod, up_des, regio_cod, regio_des FROM dwsisap.CAT_CENTRES_XSM"""
        for up, up_desc, regio, regio_desc in u.getAll(sql, 'exadata'):
            self.centres[up] = [up_desc, regio, regio_desc]
    
    def get_pacients(self):
        sql = """SELECT up_cod, ambit_cod, ambit_des, regio_cod, regio_des 
                FROM dwsisap.dbc_centres_tots"""
        inf_centres = {}
        for up, ambit, ambit_desc, regio, regio_desc in u.getAll(sql, 'exadata'):
            inf_centres[up] = ambit
        self.pacients = {}
        sql = """SELECT c_nia, c_genere, c_cip, c_data_naixement, c_abs, c_rs, c_tipus_afiliacio, c_localitat 
                FROM dwcatsalut.RCA_POB_OFICIAL WHERE c_any_assegurat = 2023"""
        for nia, sexe, cip, naix, abs, rs, afiliacio, localitat in u.getAll(sql, 'exadata'):
            if up in inf_centres:
                ambit = inf_centres[up]
            else:
                ambit = ''
            self.pacients[cip[:13]] = [nia, sexe, naix, abs, rs, afiliacio, localitat, ambit]
    
    def get_professional(self):
        self.professionals = {}
        sql = """SELECT ide_dni, ide_categ_prof_c, ide_categ_prof 
                FROM dwsisap.LOGINS_ECAP"""
        for dni, cat, desc in u.getAll(sql, 'exadata'):
            self.professionals[dni] = cat, desc
    
    def get_info_sectors(self):
        print('sector info')
        for sector in u.sectors:
            Explotacio_sector(self.professionals, self.pacients, self.centres, sector)
        # Explotacio_sector(self.professionals, self.pacients, self.centres, '6522')


class Explotacio_sector():
    def __init__(self, prof, pacients, centres, sector):
        self.professionals = prof
        self.pacients = pacients
        self.centres = centres
        self.sector = sector
        self.get_visites()
        # nomes ho calculo pels que tenim dades d'episodis a lligar
        if self.visites:
            print(self.sector)
            self.get_episodis()
            self.get_problemes()
            self.uploading()
    
    def get_visites(self):
        self.visites = c.defaultdict(set)
        sql = """SELECT visi_usuari_cip, trunc(to_date(visi_data_visita, 'J')), VISI_DNI_PROV_RESP, 
                    CASE WHEN visi_servei_origen IN ('EXTRA', 'LAB') THEN 1 ELSE 0 END flag_prova_complementaria,
                    visi_epi_codi_intern, visi_up,
                    case when visi_tipus_visita in ('SMPV', 'SMTF', 'SMVO') then 'primera visita'
                    WHEN visi_tipus_visita in ('SMST', 'SMSV', 'SMSF', 'SMFT', 'SMFV', 'SMSI', 'SMVS', 'SMAL', 'SMTI', 'SMAM', 'SMVG', 'SMSC', 'SMSS', 'SMPT') then 'visita de seguiment'
                    WHEN visi_tipus_visita in ('TBD') then 'proves complementaries'
                    WHEN visi_tipus_visita in ('SMPI') then 'psicoterapia individual'
                    WHEN visi_tipus_visita in ('SMPG', 'SMVG', 'SMGV', 'SMVV') then 'psicoterapia grupal'
                    WHEN visi_tipus_visita in ('SMSF', 'SMFV', 'SMFT') then 'psicoterapia familiar'
                    WHEN visi_tipus_visita in ('TBD') then 'atencio infermeria'
                    WHEN visi_tipus_visita in ('TBD') then 'treball social'
                    WHEN visi_tipus_visita in ('SMDM') then 'visites domiciliaries'
                    WHEN visi_tipus_visita in ('SMUP', 'SMUS') then 'visites no programades'
                    else 'altres'
                    end agrupador_tipus
                    from vistb043
                    where VISI_SITUACIO_VISITA = 'R'
                    AND visi_up in {}
                    and visi_epi_codi_intern IS NOT null""".format(tuple(self.centres.keys()))
        for cip, data, dni, flag_prova_complementaria, episodi, up, tipus_visita in u.getAll(sql, self.sector):
            self.visites[cip].add((data, dni, flag_prova_complementaria, episodi, up, tipus_visita))
    
    def get_episodis(self):
        self.episodis = c.defaultdict(dict)
        sql = """SELECT epi_codi_intern, epi_cip, epi_motiu_consulta, 
                trunc(epi_data_inci_assist), epi_up_derivacio, trunc(epi_data_final)
                FROM  vistb055"""
        for codi_epi, cip, dx1, d_inici, up_deri, d_fi in u.getAll(sql, self.sector):
            self.episodis[cip][codi_epi] = [dx1, d_inici, up_deri, d_fi]
    
    def get_problemes(self):
        self.problemes = c.defaultdict(lambda: c.defaultdict(set))
        sql = """SELECT PR_COD_U, trunc(PR_DDE), pr_cod_ps FROM prstb015
                WHERE pr_cod_ps LIKE 'F%'
                OR PR_COD_PS LIKE 'C01-F%'"""
        for id, data, prob in u.getAll(sql, self.sector):
            self.problemes[id][data].add(prob)

    def uploading(self):
        upload = []
        for cip in self.visites:
            if cip in self.pacients:
                nia, sexe, naix, abs, rs, afiliacio, localitat, ambit = self.pacients[cip]
                for data_visi, dni, flag_prova_complementaria, episodi, up_visi, tipus_visita in self.visites[cip]:
                    if up_visi in self.centres:
                        regio = self.centres[up_visi][1]
                    else: 
                        regio = ''
                    if dni in self.professionals:
                        prof, prof_desc =  self.professionals[dni]
                    else:
                        prof, prof_desc = '', ''
                    dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10 = '', '', '', '', '', '', '', '', ''
                    if cip in self.problemes:
                        if data_visi in self.problemes[cip]:
                            problems = list(self.problemes[cip][data_visi])
                            for i, prob in enumerate(problems):
                                if i == 0: dx2 = prob
                                elif i == 1: dx3 = prob
                                elif i == 2: dx4 = prob
                                elif i == 3: dx5 = prob
                                elif i == 4: dx6 = prob
                                elif i == 5: dx7 = prob
                                elif i == 6: dx8 = prob
                                elif i == 7: dx9 = prob
                                elif i == 8: dx10 = prob
                    if cip in self.episodis:
                        if episodi in self.episodis[cip]:
                            dx1, d_inici, up_deri, d_fi = self.episodis[cip][episodi]
                            upload.append((up_visi, regio, nia, sexe, naix, abs, ambit, rs, 
                                            afiliacio, localitat, data_visi, tipus_visita, prof, prof_desc, 
                                            flag_prova_complementaria, d_inici, d_fi, up_deri, dx1, 
                                            dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10))
        u.listToTable(upload, 'visites_sm', 'exadata')


if __name__ == "__main__":
    VisitesSM()

