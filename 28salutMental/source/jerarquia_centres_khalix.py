import sisapUtils as u


sql = """SELECT REGIO_COD, aga_cod, ep_cod, ep_des, up_cod, up_des FROM DWSISAP.CAT_CENTRES_XSM WHERE ecap = 1"""

upload = []
for regio, aga, ep, ep_desc, up, up_cod in u.getAll(sql, 'exadata'):
    upload.append((regio, aga, ep, ep_desc, 'CSM' + str(up), up_cod))

cols = '(regio varchar(2), aga varchar(2), ep varchar(5), ep_desc varchar(200), up varchar(50), up_desc varchar(200))'
u.createTable('centres_csm', cols, 'salutmental', rm=True)
u.listToTable(upload, 'centres_csm', 'salutmental')

sql = """select csm.*, '' from salutmental.centres_csm csm"""

u.exportKhalix(sql, 'CENTRES_CSM', force=True)