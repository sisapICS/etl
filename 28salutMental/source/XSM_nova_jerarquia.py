# -*- coding: utf-8 -*-

from email import message
from typing import Any
import sisapUtils as u
from collections import defaultdict, Counter
import datetime
import time

cataleg = 'cat_salut_mental_nou'
db = 'salutmental'


ups = ('CSM00870',
'CSM01806',
'CSM04269',
'CSM04270',
'CSM04426',
'CSM00923',
'CSM08019',
'CSM00860',
'CSM03065',
'CSM03066',
'CSM00861',
'CSM03068',
'CSM00863',
'CSM03072',
'CSM08308',
'CSM00879',
'CSM03034',
'CSM04676',
'CSM06878',
'CSM01025',
'CSM07061',
'CSM00872',
'CSM01075',
'CSM03236',
'CSM00868',
'CSM01118',
'CSM00873',
'CSM03049',
'CSM03599',
'CSM04656',
'CSM07006',
'CSM00693',
'CSM01297',
'CSM00896',
'CSM00898',
'CSM03050',
'CSM01296',
'CSM00871',
'CSM03043',
'CSM00876',
'CSM03602',
'CSM03604',
'CSM00877',
'CSM03076',
'CSM03077',
'CSM00867',
'CSM00880',
'CSM00881',
'CSM01715',
'CSM04558',
'CSM07007',
'CSM07009',
'CSM07010',
'CSM07012',
'CSM07013',
'CSM00859',
'CSM00886',
'CSM03582',
'CSM03584',
'CSM03585',
'CSM04286',
'CSM04290',
'CSM04291',
'CSM04559',
'CSM05646',
'CSM01290',
'CSM08037',
'CSM01021',
'CSM00856',
'CSM00889',
'CSM01020',
'CSM03249',
'CSM00890',
'CSM00950',
'CSM00963',
'CSM00973',
'CSM03041',
'CSM03368',
'CSM00424',
'CSM00425',
'CSM00518',
'CSM03973',
'CSM04052',
'CSM06304',
'CSM07325',
'CSM00231',
'CSM03600',
'CSM03601',
'CSM05276',
'CSM01574',
'CSM01058',
'CSM03984',
'CSM01537',
'CSM01293',
'CSM04469',
'CSM04534',
'CSM05015',
'CSM05016',
'CSM06346',
'CSM00892',
'CSM03587',
'CSM03588',
'CSM03589',
'CSM03590',
'CSM03591',
'CSM03592',
'CSM03593',
'CSM03594',
'CSM03595',
'CSM04719',
'CSM04720',
'CSM04721',
'CSM04722',
'CSM01292',
'CSM01294',
'CSM05104',
'CSM06305',
'CSM06306',
'CSM07002',
'CSM07003',
'CSM07004',
'CSM07005',
'CSM01035',
'CSM01036',
'CSM01038',
'CSM01039',
'CSM01040',
'CSM01041',
'CSM01043',
'CSM01044',
'CSM03107',
'CSM07438',
'CSM05648',
'CSM03843',
'CSM01080',
'CSM00874',
'CSM01081',
'CSM03039',
'CSM00858',
'CSM00900',
'CSM00955',
'CSM01086',
'CSM01119',
'CSM01889',
'CSM01980',
'CSM03631',
'CSM04228',
'CSM00977',
'CSM01037',
'CSM01042',
'CSM03586',
'CSM03853',
'CSM03855',
'CSM08116',
'CSM00958',
'CSM05030',
'CSM07011',
'CSM04287',
'CSM07724',
'CSM00902',
'CSM05645',
'CSM07717',
'CSM07718',
'CSM08095',
'CSM05286',
'CSM07488',
'CSM06021',
'CSM00917',
'CSM00918',
'CSM04438',
'CSM06395',
'CSM06396',
'CSM07001',
'CSM07014',
'CSM05647',
'CSM06160',
'CSM00938',
'CSM04611',
'CSM08323',
'CSM00897',
'CSM03051',
'CSM03622',
'CSM03623',
'CSM07997',
'CSM00515',
'CSM00516',
'CSM00517',
'CSM00857',
'CSM00893',
'CSM03597',
'CSM03598',
'CSM04393',
'CSM06148',
'CSM01059',
'CSM07179',
'CSM08036',
'CSM00887',
'CSM03082',
'CSM07280',
'CSM07712',
'CSM03080',
'CSM00961',
'CSM04440',
'CSM08309',
'CSM00883',
'CSM00925',
'CSM00926',
'CSM00927',
'CSM02011',
'CSM04323',
'CSM00969',
'CSM01868',
'CSM04405',
'CSM00947',
'CSM00967',
'CSM01808',
'CSM04579',
'CSM08038',
'CSM00884')

v_totals = ('SMAC', 'SMPV', 'SMTF', 'SMVO', 'SMST', 'SMSV', 'SMSF', 'SMFT', 
        'SMFV', 'SMSI', 'SMVS', 'SMAL', 'SMTI', 'SMAM', 'SMPI', 'SMPG', 
        'SMVG', 'SMDM', 'SMSC', 'SMSS', 'SMUP', 'SMUS', 'SMCO', 'SMIN',
        'MAH', 'SMPF', 'SMVA', 'SMPT', 'SMVI', 'SMGV', 'SMVV', 'SMAH',
        'SMEG', 'SMEO')

v_assistencials = ('SMAC', 'SMPV', 'SMTF', 'SMVO', 'SMST', 'SMSV', 'SMFT', 
                'SMFV', 'SMSI', 'SMVS', 'SMAL', 'SMTI', 'SMAM', 'SMPI', 
                'SMPG', 'SMVG', 'SMSF', 'SMDM', 'SMSC', 'SMSS', 'SMUP', 'SMUS',
                'MAH', 'SMPF', 'SMVA', 'SMPT', 'SMVI', 'SMGV', 'SMVV', 'SMAH')

v_assistencials_presencials = ('SMAC', 'SMPV', 'SMSI', 'SMVS', 'SMAL', 'SMSF', 
                        'SMTI', 'SMAM', 'SMPI', 'SMPG', 'SMVG', 'SMDM', 'SMSC', 
                        'SMSS', 'SMUP', 'SMUS', 'SMAH', 'SMPF', 'SMSS', 'SMVA')

v_primeres_presencials = ('SMAH', 'SMPF', 'SMPV', 'SMUP')

v_seguiment_presencials = ('SMSI', 'SMUS', 'SMSS', 'SMVS', 'SMSF', 'SMAL', 
                        'SMTI', 'SMAM', 'SMPI', 'SMPG', 'SMVG', 'SMSC', 'SMDM', 'SMVA')

v_assistencials_no_presencials = ('SMTF', 'SMVO', 'SMST', 'SMSV', 'SMFT', 'SMFV',
                                    'SMPT', 'SMVI', 'SMGV', 'SMVV')

v_videotrucada = ('SMVO', 'SMSV', 'SMFV', 'SMGV', 'SMVV', 'SMVI')

v_seguiment_video = ('SMSV', 'SMVI', 'SMFV', 'SMGV', 'SMVV')

v_telefonica = ('SMTF', 'SMST', 'SMFT', 'SMPT')

v_seguiment_telf = ('SMST', 'SMPT', 'SMFT')

v_acollida = ('SMAC')

v_primeres = ('SMPV', 'SMTF', 'SMVO', 'SMAC', 'SMUP', 'SMAH', 'SMPF')

v_successives = ('SMST', 'SMSV', 'SMSF', 'SMFT', 'SMFV', 'SMSI', 'SMVS', 'SMAL', 
                'SMTI', 'SMAM', 'SMPI', 'SMPG', 'SMVG', 'SMDM', 'SMSC', 'SMSS',
                'SMUS', 'SMPT', 'SMVI', 'SMGV', 'SMVV')

v_successives_individuals = ('SMST', 'SMSV', 'SMSI', 'SMVS', 'SMAL', 'SMTI', 
                'SMAM', 'SMPI', 'SMSS', 'SMPT', 'SMVI', 'SMUS', 'SMVA')

v_successives_familiars = ('SMSF', 'SMFV', 'SMFT')

v_successives_grupals = ('SMPG', 'SMVG', 'SMGV', 'SMVV')

v_successives_comunitaries = ('SMDM', 'SMSC')

v_successives_no_presencials = ('SMST', 'SMSV', 'SMFT', 'SMFV')

v_urgents = ('SMUP', 'SMUS')

v_ordinaries = ('SMPV', 'SMTF', 'SMVO')

v_preferents = ('SMPF')

v_primeres_urgents = ('SMUP')

v_primeres_post_hosp = ('SMAH')

v_succes_post_hosp = ('SMAL')

v_succes_dom = ('SMDM')

v_succ_urgents = ('SMUS')


def create_cataleg():
    print('cataleg')
    u.createTable(cataleg, 
                '(indicador varchar(20),literal varchar(300),ordre int,pare varchar(10),\
                llistat int,invers int,mmin double,mint double,mmax double,\
                toShow int,wiki varchar(250),tipusvalor varchar(5))',
                db, rm=True)
    upload = [
        ('XSMVISITES01', 'Nombre de visites totals realitzades a la Xarxa de Salut Mental i Adiccions', 1, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3944/ver/', 'N'),
        ('XSMVISITES01A', 'Visites assistencials totals realitzades a la Xarxa de Salut Mental i Adiccions', 2, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3945/ver/', 'N'),
        ('XSMVISITES01AA', 'Primeres visites realitzades a la Xarxa de Salut Mental i Adiccions', 3, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4457/ver/', 'N'),
        ('XSMVISITES01AAA', 'Visites d\'acollida realitzades a la Xarxa de Salut Mental i Adiccions', 3, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4458/ver/', 'N'),
        ('XSMVISITES01AAB', 'Primeres visites ordinàries realitzades a la Xarxa de Salut Mental i Adiccions', 3, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4459/ver/', 'N'),
        ('XSMVISITES01AAC', 'Primeres visites preferents realitzades a la Xarxa de Salut Mental i Adiccions', 3, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4460/ver/', 'N'),
        ('XSMVISITES01AAD', 'Primeres visites urgents realitzades a la Xarxa de Salut Mental i Adiccions', 3, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4461/ver/', 'N'),
        ('XSMVISITES01AAE', 'Primeres visites post alta hospitalària a la Xarxa de Salut Mental i Adiccions', 3, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4462/ver/', 'N'),
        ('XSMVISITES01AB', 'Visites successives realitzades a Xarxa Salut Mental i Adiccions', 4, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4463/ver/', 'N'),
        ('XSMVISITES01ABA', 'Visites successives individuals realitzades a Xarxa de Salut Mental i Adiccions', 4, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4464/ver/', 'N'),
        ('XSMVISITES01ABAA', 'Visites successives post alta hospitalària realitzades a Xarxa de Salut Mental i Adiccions', 4, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3965/ver/', 'N'),
        ('XSMVISITES01ABB', 'Visites successives familiars realitzades a Xarxa de Salut Mental i Adiccions', 4, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4466/ver/', 'N'),
        ('XSMVISITES01ABC', 'Visites successives grupals realitzades a Xarxa de Salut Mental i Adiccions', 4, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4467/ver/', 'N'),
        ('XSMVISITES01ABD', 'Visites successives comunitàries realitzades a Xarxa de Salut Mental i Adiccions', 4, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4468/ver/', 'N'),
        ('XSMVISITES01ABDA', 'Visites successives domiciliàries realitzades a Xarxa de Salut Mental i Adiccions', 4, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4469/ver/', 'N'),
        ('XSMVISITES01ABE', 'Visites successives urgents realitzades a Xarxa de Salut Mental i Adiccions', 4, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4470/ver/', 'N'),
        ('XSMVISITES01B', 'Visites no assistencials totals realitzades a la Xarxa de Salut Mental i Adiccions', 13, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4038/ver/', 'N'),
        ('XSMVISITES01BA', 'Coordinacions realitzades a la Xarxa de Salut Mental i Adiccions', 14, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3937/ver/', 'N'),
        ('XSMVISITES01BB', 'Interconsultes realitzades a la Xarxa de Salut Mental i Adiccions', 15, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3958/ver/', 'N'),
        ('XSMVISITES01C', 'Altres tipus de visita no identificats', 14, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4472/ver/', 'N'),
        ('XSMVISITES02', 'Visites assistencials presencials a la Xarxa de Salut Mental i Adiccions', 16, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3949/ver/', 'N'),
        ('XSMVISITES02A', 'Primeres visites presencials realitzades a la Xarxa de Salut Mental i Adiccions', 16, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4473/ver/', 'N'),
        ('XSMVISITES02B', 'Visites successives presencials realitzades a la Xarxa de Salut Mental i Adiccions', 16, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4474/ver/', 'N'),
        ('XSMVISITES03', 'Visites assistencials no presencials a la Xarxa de Salut Mental i Adiccions', 17, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3943/ver/', 'N'),
        ('XSMVISITES03A', 'Visites assistencials mitjançant videotrucada a la Xarxa de Salut Mental i Adiccions', 18, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3941/ver/', 'N'),
        ('XSMVISITES03AA', 'Primeres visites realitzades mitjançant videotrucada a la Xarxa Salut Mental i Adiccions', 18, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4475/ver/', 'N'),
        ('XSMVISITES03AB', 'Visites de seguiment realitzades mitjançant videotrucada a la Xarxa Salut Mental i Adiccions', 18, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4477/ver/', 'N'),
        ('XSMVISITES03B', 'Visites assistencials telefòniques realitzades a la Xarxa de Salut Mental i Adiccions', 19, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3942/ver/', 'N'),
        ('XSMVISITES03BA', 'Primeres visites telefòniques realitzades a la Xarxa de Salut Mental i Adiccions', 18, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4476/ver/', 'N'),
        ('XSMVISITES03BB', 'Visites de seguiment telefòniques realitzades a la Xarxa de Salut Mental i Adiccions', 18, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4478/ver/', 'N'),
        ('XSMVISITES07', 'Percentatge de visites assistencials respecte al total de visites realitzades', 25, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3965/ver/', 'RATI'),
        ('XSMVISITES07A', 'Percentatge de primeres visites respecte al total de visites realitzades', 26, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4479/ver/', 'RATI'),
        ('XSMVISITES07AA', 'Percentatge de visites d\'acollida respecte al total de visites realitzades', 26, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4480/ver/', 'RATI'),
        ('XSMVISITES07AB', 'Percentatge de primeres visites ordinàries realitzades a la Xarxa de Salut Mental i Adiccions', 26, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4482/ver/', 'RATI'),
        ('XSMVISITES07AC', 'Percentatge de primeres visites preferents realitzades a la Xarxa de Salut Mental i Adiccions', 26, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4483/ver/', 'RATI'),
        ('XSMVISITES07AD', 'Percentatge de primeres visites urgents realitzades a la Xarxa de Salut Mental i Adiccions', 26, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4484/ver/', 'RATI'),
        ('XSMVISITES07AE', 'Percentatge de primeres visites post alta hospitalàra realitzades a la Xarxa de Salut Mental i Adiccions', 26, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4485/ver/', 'RATI'),
        ('XSMVISITES07C', 'Percentatge de visites successives respecte al total de visites realitzades', 28, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3968/ver/', 'RATI'),
        ('XSMVISITES07CA', 'Percentatge de visites successives individuals respecte al total de visites realitzades', 29, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4023/ver/', 'RATI'),
        ('XSMVISITES07CAA', 'Percentatge de visites successives post alta hospitalària respecte al total de visites realitzades a la Xarxa de Salut Mental i Adiccions', 29, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4486/ver/', 'RATI'),
        ('XSMVISITES07CB', 'Percentatge de visites successives familiars respecte al total de visites realitzades', 30, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4024/ver/', 'RATI'),
        ('XSMVISITES07CC', 'Percentatge de visites successives grupals respecte al total de visites realitzades', 31, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4025/ver/', 'RATI'),
        ('XSMVISITES07CD', 'Percentatge de visites comunitàries respecte al total de visites realitzades', 32, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4026/ver/', 'RATI'),
        ('XSMVISITES07CDA', 'Percentatge de visites domiciliàries respecte al total de visites realitzades', 32, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4488/ver/', 'RATI'),
        ('XSMVISITES07CE', 'Percentatge de visites successives urgents respecte al total de visites realitzades', 32, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4487/ver/', 'RATI'),
        ('XSMVISITES08', 'Percentatge de visites no assistencials respecte al total de visites realitzades', 36, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4032/ver/', 'RATI'),
        ('XSMVISITES08A', 'Percentatge d\'informes realitzats respecte al total de visites realitzades', 37, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4031/ver/', 'RATI'),
        ('XSMVISITES08B', 'Percentatge de coordinacions realitzades respecte al total de visites realitzades', 38, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3966/ver/', 'RATI'),
        ('XSMVISITES09', 'Rati primeres visites/ visites successives realitzades', 39, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3960/ver/', 'RATI'),
        ('XSMVISITES10', 'Percentatge de visites assistencials presencials respecte al total de visites assistencials', 40, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4081/ver/', 'RATI'),
        ('XSMVISITES10A', 'Percentatge de primeres visites presencials respecte al total de visites assistencials realitzades', 41, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4019/ver/', 'RATI'),
        ('XSMVISITES10B', 'Percentatge de visites de seguiment presencials realitzades a la Xarxa de Salut Mental i Adiccions', 42, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5018/ver/', 'RATI'),
        ('XSMVISITES11', 'Percentatge de visites assistencials no presencials respecte el total de visites assistencials', 43, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/3964/ver/', 'RATI'),
        ('XSMVISITES11A', 'Percentatge de visites realitzades mitjançant videotrucada respecte al total d\'assistencials', 44, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4016/ver/', 'RATI'),
        ('XSMVISITES11AA', 'Percentatge primeres visites mitjançant videotrucada respecte al total de visites assistencials', 45, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4021/ver/', 'RATI'),
        ('XSMVISITES11AB', 'Percentatge de visites successives mitjançant videotrucada respecte total de visites assistencials', 46, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4027/ver/', 'RATI'),
        ('XSMVISITES11B', 'Percentatge de visites assistencials telefòniques respecte al total de visites assistencials', 47, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4083/ver/', 'RATI'),
        ('XSMVISITES11BA', 'Percentatge de primeres visites telefòniques respecte al total de visites assistencials realitzades', 48, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4022/ver/', 'RATI'),
        ('XSMVISITES11BB', 'Percentatge de visites successives telefòniques respecte al total de visites assistencials', 49, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5023/ver/', 'RATI'),
        ('XSMVISITES12', 'Rati visites presencials/ visites no presencials realitzades a la Xarxa de Salut Mental i Adiccions', 50, 'XSMVISITES', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/3959/ver/', 'RATI'),
    ]            
    u.listToTable(upload, cataleg, db)

def get_extraccio():
    print('extraccio')
    sql = """select data_ext from nodrizas.dextraccio"""
    for data, in u.getAll(sql, 'nodrizas'):
        mes = data.strftime('%m')
        any = data.year
        dia = data.strftime('%d')
        print(type(data), dia, mes, any)
    return mes, any, dia

class SalutMental():
    def __init__(self, mes, any, dia):
        self.mes = mes
        self.any = any
        self.dia = dia
        self.create_table()
        self.get_edats()
        self.INDI()
        self.to_khalix()

    def create_table(self):
        cols = '(indicador varchar(20), up varchar(8), professional varchar(9), especialitat varchar(40), \
                modul varchar(5), servei varchar(5), edat varchar(10), sexe varchar(10), tipus varchar(3),  valor int)'
        u.createTable('XSMVISITES_indicadors_nous', cols, 'salutmental', rm = True)

    def get_edats(self):
        print('edats')
        self.edats = set()
        self.pob = dict()
        sql = """select id_cip_sec, edat, sexe from nodrizas.assignada_tot"""
        for id, edat, sexe in u.getAll(sql, 'nodrizas'):
            sexe = u.sexConverter(sexe)
            age = u.ageConverter(edat)
            self.pob[id] = (age, sexe)
            if edat <= 35 and edat >= 14:
                self.edats.add(id)

    def INDI(self):
        print('indicadors')
        XSMVISITES01AAA, XSMVISITES01AAB, XSMVISITES01AAC = Counter(), Counter(), Counter()
        XSMVISITES01AAD, XSMVISITES01AAE = Counter(), Counter()
        XSMVISITES01AB, XSMVISITES01ABA, XSMVISITES01ABAA = Counter(), Counter(), Counter()
        XSMVISITES01ABB, XSMVISITES01ABC, XSMVISITES01ABD = Counter(), Counter(), Counter()
        XSMVISITES01ABDA, XSMVISITES01ABE = Counter(), Counter()
        XSMVISITES01BA, XSMVISITES01BB = Counter(), Counter()
        XSMVISITES01C = Counter()
        XSMVISITES02, XSMVISITES02A, XSMVISITES02B = Counter(), Counter(), Counter()
        XSMVISITES03A, XSMVISITES03AB, XSMVISITES03B = Counter(), Counter(), Counter()
        XSMVISITES03AA, XSMVISITES03BA = Counter(), Counter()
        XSMVISITES03BB = Counter()
        sql = """select id_cip_sec, concat('CSM',visi_up), visi_tipus_visita, 
                s_espe_codi_especialitat, VISI_DNI_PROV_RESP,
                VISI_MODUL_CODI_MODUL, VISI_SERVEI_CODI_SERVEI 
                from visites1
                where VISI_SITUACIO_VISITA = 'R'
                AND concat('CSM',visi_up) in {}
                and month(VISI_DATA_VISITA) = {} and year(VISI_DATA_VISITA) = {}""".format(ups, self.mes, self.any)
        print(sql)
        dades = []
        for id, up, tipus, especialitat, professional, modul, servei in u.getAll(sql, 'import'):
            dades.append((id, up, tipus, especialitat, professional, modul, servei))

        for e in dades:
            id, up, tipus, especialitat, professional, modul, servei = e
            if id in self.pob and up in ups:
                age, sex = self.pob[id]
                if up in ups:
                    if tipus in v_assistencials:
                        if tipus in v_acollida:
                            XSMVISITES01AAA[(up, professional, especialitat, modul, servei, age, sex)] += 1
                        elif tipus in v_primeres:
                            if tipus in v_ordinaries:
                                XSMVISITES01AAB[(up, professional, especialitat, modul, servei, age, sex)] += 1
                            elif tipus in v_preferents:
                                XSMVISITES01AAC[(up, professional, especialitat, modul, servei, age, sex)] += 1
                            elif tipus == 'SMUP':
                                XSMVISITES01AAD[(up, professional, especialitat, modul, servei, age, sex)] += 1
                            elif tipus in v_primeres_post_hosp:
                                XSMVISITES01AAE[(up, professional, especialitat, modul, servei, age, sex)] += 1
                        elif tipus in v_successives:
                            XSMVISITES01AB[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                            if tipus in v_successives_individuals:
                                XSMVISITES01ABA[(up, professional, especialitat, modul, servei, age, sex)] += 1
                                if tipus in v_succes_post_hosp:
                                    XSMVISITES01ABAA[(up, professional, especialitat, modul, servei, age, sex)] += 1
                            elif tipus in v_successives_familiars:
                                XSMVISITES01ABB[(up, professional, especialitat, modul, servei, age, sex)] += 1 
                            elif tipus in v_successives_grupals:
                                XSMVISITES01ABC[(up, professional, especialitat, modul, servei, age, sex)] += 1 
                            elif tipus in v_successives_comunitaries:
                                XSMVISITES01ABD[(up, professional, especialitat, modul, servei, age, sex)] += 1 
                                if tipus in v_succes_dom:
                                    XSMVISITES01ABDA[(up, professional, especialitat, modul, servei, age, sex)] += 1 
                            if tipus == 'SMUS':
                                XSMVISITES01ABE[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                    if tipus in ('SMCO', 'SMEG', 'SMEO'):
                        XSMVISITES01BA[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                    if tipus == 'SMIN':
                        XSMVISITES01BB[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                    if tipus not in v_totals:
                        XSMVISITES01C[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                    if tipus in v_assistencials_presencials:
                        XSMVISITES02[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                        if tipus in v_primeres_presencials:
                            XSMVISITES02A[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                        if tipus in v_seguiment_presencials:
                            XSMVISITES02B[(up, professional, especialitat, modul, servei, age, sex)] += 1 # posar més
                    if tipus in v_assistencials_no_presencials:
                        if tipus in v_videotrucada:
                            XSMVISITES03A[(up, professional, especialitat, modul, servei, age, sex)] += 1
                            if tipus in v_seguiment_video:
                                XSMVISITES03AB[(up, professional, especialitat, modul, servei, age, sex)] += 1
                        elif tipus in v_telefonica:
                            XSMVISITES03B[(up, professional, especialitat, modul, servei, age, sex)] += 1
                            if tipus in v_seguiment_telf:
                                XSMVISITES03BB[(up, professional, especialitat, modul, servei, age, sex)] += 1
                    if tipus == 'SMVO':
                        XSMVISITES03AA[(up, professional, especialitat, modul, servei, age, sex)] += 1
                    if tipus == 'SMTF':
                        XSMVISITES03BA[(up, professional, especialitat, modul, servei, age, sex)] += 1

        print('XSMVISITES01AAA', len(XSMVISITES01AAA), 'XSMVISITES01AB', len(XSMVISITES01AB),
                'XSMVISITES01ABA', len(XSMVISITES01ABA), 'XSMVISITES01ABB', len(XSMVISITES01ABB), 
                'XSMVISITES01ABC', len(XSMVISITES01ABC), 'XSMVISITES01ABD', len(XSMVISITES01ABD),
                'XSMVISITES01ABE', len(XSMVISITES01ABE),
                'XSMVISITES01BA', len(XSMVISITES01BA), 'XSMVISITES01BB', len(XSMVISITES01BB),
                'XSMVISITES02', len(XSMVISITES02), 'XSMVISITES03A', len(XSMVISITES03A), 'XSMVISITES03B', len(XSMVISITES03B),
                'XSMVISITES03AA', len(XSMVISITES03AA), 'XSMVISITES03BA', len(XSMVISITES03BA))

        print('anem a penjar')
        upload = []
        XSMVISITES01A_B= Counter()
        XSMVISITES01A = Counter()
        XSMVISITES01AA = Counter()
        XSMVISITES01B = Counter()
        XSMVISITES03 = Counter()
        for k, value in XSMVISITES01AAA.items():
            upload.append(('XSMVISITES01AAA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07AA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value)) # no den
            XSMVISITES01A_B[k] += value
            XSMVISITES01AA[k] += value
        for k, value in XSMVISITES01AAB.items():
            upload.append(('XSMVISITES01AAB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07AB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01A_B[k] += value
            XSMVISITES01AA[k] += value
        for k, value in XSMVISITES01AAC.items():
            upload.append(('XSMVISITES01AAC', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07AC', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01A_B[k] += value
            XSMVISITES01AA[k] += value
        for k, value in XSMVISITES01AAD.items():
            upload.append(('XSMVISITES01AAD', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07AD', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01A_B[k] += value
            XSMVISITES01AA[k] += value
        for k, value in XSMVISITES01AAE.items():
            upload.append(('XSMVISITES01AAE', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07AE', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01A_B[k] += value
            XSMVISITES01AA[k] += value
        for k, value in XSMVISITES01ABA.items():
            upload.append(('XSMVISITES01ABA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07CA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01A_B[k] += value
        for k, value in XSMVISITES01ABAA.items():
            upload.append(('XSMVISITES01ABAA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07CAA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES01ABB.items():
            upload.append(('XSMVISITES01ABB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07CB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01A_B[k] += value
        for k, value in XSMVISITES01ABC.items():
            upload.append(('XSMVISITES01ABC', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07CC', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01A_B[k] += value
        for k, value in XSMVISITES01ABD.items():
            upload.append(('XSMVISITES01ABD', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07CD', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01A_B[k] += value
        for k, value in XSMVISITES01ABDA.items():
            upload.append(('XSMVISITES01ABDA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07CDA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES01ABE.items():
            upload.append(('XSMVISITES01ABE', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07CE', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01A_B[k] += value
        for k, value in XSMVISITES01AA.items():
            upload.append(('XSMVISITES01AA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07A', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES09', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01A[k] += value
        for k, value in XSMVISITES01AB.items():
            upload.append(('XSMVISITES01AB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07C', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES09', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))
            XSMVISITES01A_B[k] += value
            XSMVISITES01A[k] += value
        for k, value in XSMVISITES01BA.items():
            upload.append(('XSMVISITES01BA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES08B', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01A_B[k] += value
            XSMVISITES01B[k] += value
        for k, value in XSMVISITES01BB.items():
            upload.append(('XSMVISITES01BB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES08A', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES01A_B[k] += value
            XSMVISITES01B[k] += value
        for k, value in XSMVISITES01C.items():
            upload.append(('XSMVISITES01C', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES02.items():
            upload.append(('XSMVISITES02', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES10', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES12', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES02A.items():
            upload.append(('XSMVISITES02A', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES10A', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES02B.items():
            upload.append(('XSMVISITES02B', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES10B', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES03A.items():
            upload.append(('XSMVISITES03A', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES03[(k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM')] += value
            upload.append(('XSMVISITES11A', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES03AB.items():
            upload.append(('XSMVISITES03AB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES11AB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES03B.items():
            upload.append(('XSMVISITES03B', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            XSMVISITES03[(k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM')] += value
            upload.append(('XSMVISITES11B', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES03AA.items():
            upload.append(('XSMVISITES03AA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES11AA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES03BA.items():
            upload.append(('XSMVISITES03BA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES11BA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES03BB.items():
            upload.append(('XSMVISITES03BB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES11BB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES03.items():
            upload.append(('XSMVISITES11', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES12', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))
        for k, value in XSMVISITES01A.items():
            upload.append(('XSMVISITES01A', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES07', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES10', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))
            upload.append(('XSMVISITES10A', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))
            upload.append(('XSMVISITES10B', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))
            upload.append(('XSMVISITES11', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))
            upload.append(('XSMVISITES11A', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))
            upload.append(('XSMVISITES11AA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))
            upload.append(('XSMVISITES11AB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))
            upload.append(('XSMVISITES11B', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))
            upload.append(('XSMVISITES11BA', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))
            upload.append(('XSMVISITES11BB', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))
        for k, value in XSMVISITES01B.items():
            upload.append(('XSMVISITES01B', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
            upload.append(('XSMVISITES08', k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'NUM', value))
        for k, value in XSMVISITES01A_B.items():
            for indi in ('XSMVISITES07', 'XSMVISITES07A', 'XSMVISITES07AA', 'XSMVISITES07AB', 'XSMVISITES07AC', 'XSMVISITES07AD', 'XSMVISITES07AE',
                    'XSMVISITES07C', 'XSMVISITES07CA', 'XSMVISITES07CAA', 'XSMVISITES07CB', 'XSMVISITES07CC', 'XSMVISITES07CD', 'XSMVISITES07CDA',
                    'XSMVISITES07CE', 'XSMVISITES08', 'XSMVISITES08A', 'XSMVISITES08B'):
                upload.append((indi, k[0], k[1], k[2], k[3], k[4], k[5], k[6], 'DEN', value))

        u.listToTable(upload, 'XSMVISITES_indicadors_nous', 'salutmental')

    def to_khalix(self):
        print('khalix')
        sql = """ 
                SELECT indicador, 'Aperiodo', up, tipus, edat, 'NOIMP', sexe, 'N', sum(valor)
                                    FROM salutmental.XSMVISITES_indicadors_nous
                                    where indicador not in ('XSMVISITES01A',
                                            'XSMVISITES01AA',
                                            'XSMVISITES01AB',
                                            'XSMVISITES01B',
                                            'XSMVISITES02',
                                            'XSMVISITES03')
                                    group by indicador, up, tipus, edat, sexe"""
        
        u.exportKhalix(sql, 'XSMVISITES')


if __name__ == "__main__":
    if u.IS_MENSUAL:
        create_cataleg()
        mes, any, dia = get_extraccio()
        print(mes, any, dia)
        SalutMental(mes, any, dia)
