#  coding: latin1

import sisapUtils as u

cat_serveis = {"LAB":   {"desc": "Extraccions", "servei_homologat": "EXTRA"}, 
               "EXTRA": {"desc": "Extraccions", "servei_homologat": "EXTRA"}, 
               "CSMA":  {"desc": "Salut Mental Adults", "servei_homologat": "CSMA"}, 
               "CSMIJ": {"desc": "Salut Mental Infanto-Juvenil", "servei_homologat": "CSMIJ"}, 
               "CAS":   {"desc": "Atencio drogodependencies", "servei_homologat": "CAS"}, 
               "CASD":   {"desc": "Atencio drogodependencies", "servei_homologat": "CAS"}, 
               "PATPI": {"desc": "Trastorn Psicotic Incipient", "servei_homologat": "PATPI"}, 
               "PSIN":  {"desc": "Pla de Serveis Individualitzat", "servei_homologat": "PSIN"}, 
               "HDA":   {"desc": "Hospital Dia Adults", "servei_homologat": "HDA"}, 
               "HDIJ":  {"desc": "Hospital Dia Infantil i Juvenil", "servei_homologat": "HDIJ"}, 
               "SRC":   {"desc": "Servei Rehabilitacio Comunitaria", "servei_homologat": "SRC"}, 
               "SMEG":  {"desc": "Equip Guia", "servei_homologat": "SMEG"}, 
               "SMDI":  {"desc": "Salut Mental i Discapacitat Intelectual", "servei_homologat": "SMDI"},
               "AIS":   {"desc": "Salut Mental Infanto-Juvenil", "servei_homologat": "CSMIJ"}
              }

serveis_no_considerats_email = tuple(cat_serveis.keys()) + ('PSIC', 'PSQ', 'PSICO', 'PSP', 'SM', 'AIS', 'PSI')

taula = "cat_centres_serveis_XSM"

def get_vells_registres():
    """."""

    sql = """
            SELECT
                DISTINCT up, servei
            FROM
                {}
            """.format(taula)
    vells_registres = [(up, servei) for up, servei in u.getAll(sql, "exadata")]

    return vells_registres

def get_centres_ecap():

    sql = """
            SELECT
                up_cod,
                up_des
            FROM
                cat_centres_xsm
            WHERE
                ecap = 1
          """
    centres_ecap = {up: up_desc for up, up_desc in u.getAll(sql, "exadata")}
    
    return centres_ecap

def get_centres_visites():
    
    sql = """
            SELECT
                up,
                servei
            FROM
                dwsisap.sisap_master_visites
            WHERE
                data >= ADD_MONTHS(current_date, -12)
          """
    centres_visites = {(up, servei) for up, servei in u.getAll(sql, "exadata")}
    
    return centres_visites
    
def create_table():
       
    columnes = "(up varchar2(5), up_desc varchar2(200), servei varchar2(20), servei_desc varchar2(200), servei_homologat varchar(10))"
    u.createTable(taula, columnes, "exadata", rm = True)
    u.grantSelect(taula, 'DWSISAP_ROL', "exadata")
    u.grantSelect(taula, 'DWPFABREGAT', "exadata")

    files = list()

    for (up, servei) in centres_visites:
        if up in centres_ecap:
            up_desc = centres_ecap[up]
            servei_desc = cat_serveis.get(servei, "")
            if servei_desc != "":
                servei_desc = servei_desc["desc"]
            else:
                servei_desc = ""
            servei_homologat = cat_serveis[servei]["servei_homologat"] if servei in cat_serveis else ""
            files.append((up, up_desc, servei, servei_desc, servei_homologat))
    
    u.listToTable(files, taula, "exadata")
    
def get_nous_registres():

    sql = """
            SELECT
                DISTINCT up, servei
            FROM
                {}
            """.format(taula)
    nous_registres = ["\n     - UP {} i servei '{}'".format(up, servei) for (up, servei) in u.getAll(sql, "exadata") if (up, servei) not in vells_registres and servei not in serveis_no_considerats_email]

    return nous_registres

def send_email_nous_registres():
    
    if nous_registres:
        me = "SISAP <sisap@gencat.cat>"
        to = "mquintana.apms.ics@gencat.cat"
        cc = "alejandrorivera@gencat.cat"
        subject = "Nous registres a cat_centres_serveis_XSM"
        text = "Bon dia\n\nTrobem nous registres a cat_centres_serveis_XSM, s�n els seg�ents:\n{}.\n\n Salutacions,\n\nEl teu bot de confian�a *__*".format(str(tuple(nous_registres)))
        u.sendGeneral(me, to, cc, subject, text)

        
if __name__ == '__main__':
    vells_registres = get_vells_registres()
    centres_ecap = get_centres_ecap()
    centres_visites = get_centres_visites()
    create_table = create_table()
    nous_registres = get_nous_registres()
    send_email_nous_registres = send_email_nous_registres()