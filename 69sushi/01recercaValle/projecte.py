# encoding: utf-8
import sisapUtils as u
from datetime import timedelta, datetime as d
from dateutil import relativedelta as rd
import hashlib as h
from recercaValleCodes import up_cohort, dx_cohort, drugs_definition
from recercaValleCodes import dx_definition, var_map, lab_map
from recercaValleCodes import dx_cohort_parents, dx_altres_parents
from recercaValleCodes import drug_parents
# import pandas as pd

import os
print(os.getcwd())

SIDICS_DB = ("sidics", "x0002")

"""
sortides:
----------
DONE    up_cohort_dict: (up, cohort)
DONE    drug_dict: (atc, grup)
DONE    poblacio.txt: (id_cip, sexe, age, up, medea, data_sit_d)
DONE    covid.txt: (id_cip, estat, data, ing_alta, ing_desti, exitus)
DONE    problemes.txt: (id_cip, cod, data)
DONE    variables.txt: (id_cip, cod, data, val) (cod = ['PES', 'TALLA', 'IMC', 'PAS', 'PAD', 'hba1c','tabac'])
DONE    prescripcions.txt: (id_cip, atc, data_ini, data_fi, ddd_prescrites) (ddd_prescrites: ddd que se prescribe por dia, para conseguir las totales habria que multiplicarlas por el numero de dias entre data_ini y data_fi)
DONE    dispensacions.txt: (id_cip, atc, data_ini, data_fi, ddd_dispensades) (ddd_dispensades: ddd TOTALES que se lleva de la farmacia, data_ini: dia 01 del mes de facturació, data_fi = ini + ddd_disp)
DONE    altes.txt: (id_cip, up, t_act, d_ingres, c_ingres, pr_ingres, d_alta, c_alta, up_desti, dp,
                    dp, ds1, ds2, ds3, ds4, ds5, ds6, ds7, ds8, ds9, ds10, ds11, ds12, ds13, ds14,
                    pp, ps1, ps2, ps3, ps4, ps5, ps6, ps7, ps8, ps9, ps10, ps11, ps12, ps13, ps14,
                    ps15, ps16, ps17, ps18, ps19, uci, d_ini_uci, d_fi_uci, dies_uci)

---
CANVI[x]	hem acordat fer servir els agrupadors sisap de patologias.
DONE[+]	hi han 2 cohorts:
        a.) cohort valldehbron: tot >18 anys a data index, de eaps de l’area d’influencia de vallhebron [estimat uns 350k].
        b.) cohort qualsevol-hx-ics: mateixa edata, d'aquesta area d'influencia i restringit als que tinguin alguna de aquestes: dm, hta o mcv.
DONE[+]	medea (socioeconomic) de l'equip (cuartil del medea pels eap urbans, indicarem ruralitat en la resta)
DONE[+]	tabac, l’estatus en el moment índex (1 de març).
DONE[+]	iecas y ara2 son els medicaments d’interés per definir exposició: d’aquests: prescripcio y facturació.
DONE[+]	de la resta de medicaments només facturació (anys 2019 i 2020)
DONE[+]	dades de cmbd-ah-ics, que siguin de la cohort a) o b) i hagin estat covid. (covid segons el que hagi definit sisap, us he de dir però els criteris sisap per què els sàpigueu).
NO	[x]  si podem pasar de la mateixa població el cmbd-urgencies, el voldrieu, tot i que entenc que no es prioritari.
DONE[+]	i mortalitat la de morts de RSA (que confirmaré si son els morts covid)
DONE[+] altres variables: glicada, IMC, talla, pes, PAS, PAD (crec que hem quedat la darrera circa data index: 1 març)

DONE[+]:   u11
DONE[+]: els problemes que no son de filtratge (atributs)
DONE[+]: sacar ficheros a HASH: cohort_cip_map
"""  # noqa
project_name = 'P630_S0'

up_cohort_file = 'up_cohort_dict.txt'
drug_dict_file = 'drug_dict.txt'
poblacio_file = 'poblacio.txt'
covid_file = 'covid.txt'
problemes_file = 'problemes.txt'
variables_file = 'variables.txt'
tractaments_file = 'prescripcions.txt'
dispensacions_file = 'dispensacions.txt'
altes_file = 'altes.txt'

up_cohort_outfile = os.path.join(u.tempFolder, project_name, up_cohort_file)
drug_dict_outfile = os.path.join(u.tempFolder, project_name, drug_dict_file)
poblacio_outfile = os.path.join(u.tempFolder, project_name, poblacio_file)
covid_outfile = os.path.join(u.tempFolder, project_name, covid_file)
problemes_outfile = os.path.join(u.tempFolder, project_name, problemes_file)
variables_outfile = os.path.join(u.tempFolder, project_name, variables_file)
tractaments_outfile = os.path.join(u.tempFolder, project_name, tractaments_file)  # noqa
dispensacions_outfile = os.path.join(u.tempFolder, project_name, dispensacions_file)  # noqa
altes_outfile = os.path.join(u.tempFolder, project_name, altes_file)

# [dx for dx in dx_cohort]
# dx_cohort['agr_dm']

"""
for cat in [cat for cat in u.getDatabaseTables('import')]:
    print(cat)
    [col for col in u.getTableColumns(cat, 'import')]
"""

dx_set = []
for k, v in dx_cohort.items():
    if 'agr_' in k:
        for v in v:
            dx_set.append(v.replace('C01-', ''))
    else:
        # print(dx_cohort[k])
        # dx_cohort["dx_cv"]
        # print(v.keys())
        for k1 in v.keys():
            for v in dx_cohort[k][k1]:
                dx_set.append(v.replace('C01-', ''))
                # print(v)

dx_set2 = []
for k, v in dx_definition.items():
    for v in v:
        dx_set2.append(v.replace('C01-', ''))

dx_map = {}
for k, v in dx_cohort.items():
    if 'agr_' in k:
        for v in v:
            dx_map[v] = k
    else:
        for k1, v1 in dx_cohort[k].items():
            for v1 in v1:
                dx_map[v1] = k1

for k, v in dx_cohort_parents.items():
    desc = dx_map[k]
    for v in v:
        dx_map[v] = desc

dx_map_altres = {}
for k, v in dx_definition.items():
    for v in v:
        dx_map_altres[v] = k

len(dx_map_altres)
for k, v in dx_altres_parents.items():
    desc = dx_map_altres[k]
    for v in v:
        dx_map_altres[v] = desc
len(dx_map_altres)

drug_map = {}
for k, v in drugs_definition.items():
    for v in v:
        drug_map[v] = k

len(drug_map)
for k, v in drug_parents.items():
    desc = drug_map[k]
    for v in v:
        drug_map[v] = desc
len(drug_map)

unit_map = {
    "003": "MCG",
    "004": "MG",
    "011": "UI",
    "092": "U.D.ORAL",
    "098": "U.D.INHAL",
}

pf_ddd, pf_not_ddd = {}, {}


def get_ddd(sql):
    for pf, pf_desc, envas_units, quantitat, quantitat_unit, forma, atc, via, \
            ddd, ddd_unit, pos, freq in u.getAll(sql, "import"):
        if atc in drug_map:
            quantitat_unit_desc = unit_map[quantitat_unit] \
                if quantitat_unit in unit_map else quantitat_unit
            if ddd != 0.0:
                ddd_unit_desc = unit_map[ddd_unit] \
                    if ddd_unit in unit_map else ddd_unit
                pf_ddd[pf] = {'desc': pf_desc,
                              'envas_units': envas_units,
                              'quantitat': quantitat,
                              'quantitat_unit': quantitat_unit_desc,
                              'forma': forma, 'atc': atc, 'via': via,
                              'ddd': ddd, 'ddd_unit': ddd_unit_desc,
                              'pos': pos, 'freq': freq
                              }
            else:
                if freq != 0.0 and pos != 0.0 and quantitat != 0.0:
                    ddd = (pos*quantitat)*(24.0/freq)
                    ddd_unit_desc = unit_map[quantitat_unit] \
                        if quantitat_unit in unit_map else quantitat_unit
                    pf_ddd[pf] = {'desc': pf_desc,
                                  'envas_units': envas_units,
                                  'quantitat': quantitat,
                                  'quantitat_unit': quantitat_unit_desc,
                                  'forma': forma, 'atc': atc, 'via': via,
                                  'ddd': ddd, 'ddd_unit': ddd_unit_desc,
                                  'pos': pos, 'freq': freq
                                  }
                else:
                    pf_not_ddd[pf] = {'desc': pf_desc,
                                      'envas_units': envas_units,
                                      'quantitat': quantitat,
                                      'quantitat_unit': quantitat_unit_desc,
                                      'forma': forma, 'atc': atc, 'via': via,
                                      'ddd': None, 'ddd_unit': None,
                                      'pos': pos, 'freq': freq
                                      }


sql = """
    SELECT
        pf_codi,
        pf_desc,
        pf_unitats,
        ce_quantitat,
        ce_uni_mesu,
        pf_ff_codi,
        pf_cod_atc,
        pf_via_adm,
        pf_val_ddd,
        pf_cod_um_ddd, pf_pos_uni, pf_pos_freq
    FROM
        cat_cpftb006 c6 inner join
        cat_cpftb001 c1
            on c6.pf_codi = c1.ce_pf_codi
    WHERE
        pf_cod_atc <> '' and ce_ind_pa_pri = 'S'
        -- and pf_situacio='A'
    """
get_ddd(sql)
len(pf_ddd)     # 3889 (situacio = 'A') 8950 (situacio 'A','B') 10794 recuperando por pos i freq  # noqa
len(pf_not_ddd)  # 99  (situacio = 'A') 2328 (situacio 'A','B') 425

# los ATC de los PFC que que no tienen ddd (pf_not_ddd),
# que tampoco estan en pf_ddd (por ejemplo con otra ruta)
set([pf_not_ddd[i]['atc'] for i in pf_not_ddd.keys()]) - set([pf_ddd[i]['atc'] for i in pf_ddd.keys()])  # noqa

# los que son farmacos de intervención sin ddd:
set([i for i in pf_not_ddd.keys() if pf_not_ddd[i]['atc'][:3] == 'C09'])
# solo este pfc: 724745 RENITEC 5 mg COMPRIMIDOS (Enalapril)


infl_valle = ["00490", "00448", "00489", "00488", "00352", "07505", "07504",
              "00452", "00453", "00450", "00451", "05091", "00497", "00494",
              "05090", "00449"]

up_cohort_dict = {}
for i in up_cohort:
    if i in infl_valle:
        up_cohort_dict[i] = "infl_valle"
    else:
        up_cohort_dict[i] = "infl_altres"


""" omop@postgres: obara omop 1
    select c.concept_code, c2.concept_code from
    concept c inner join
    concept_relationship r
    on c.concept_id  = r.concept_id_1
    inner join concept c2
    on  r.concept_id_2 = c2.concept_id
    and c.domain_id = 'Condition' and c.vocabulary_id = 'ICD10CM'  and relationship_id = 'Subsumes'
    and c.concept_code  in ('E08','E09','E10','E11','E12','E13','O24.4','I61.0','I61.1','I61.2','I61.3',
    'I61.4','I61.5','I61.6','I61.8','I61.9','I70.201','I70.203','I70.209','I70.211',
    'I70.212','I70.218','I70.219','I70.228','I70.233','I70.234','I70.235','I70.245',
    'I70.249','I70.25','I70.261','I70.262','I70.263','I70.269','I70.292','I70.293',
    'I70.299','I73.9','I24.8','I24.9','I25','I25.1','I25.82','I25.83','I25.84',
    'I25.89','I25.9','I21','I21.9','I21.A1','I21.A9','I23.0','I23.1','I23.2','I23.3',
    'I23.4','I23.5','I23.6','I23.7','I23.8','I24.0','I24.1','I25.2','I20','I20.0',
    'I20.1','I20.8','I20.9','I25.10','I25.110','I25.111','I25.118','I25.119',
    'I25.718','I21.0','I21.01','I21.02','I21.09','I21.1','I21.11','I21.19','I21.2',
    'I21.21','I21.29','I21.3','I22.0','I22.1','I22.8','I22.9','I21.4','I22.2',
    'I50.84','I63','I63.00','I63.10','I63.113','I63.12','I63.131','I63.133','I63.20',
    'I63.213','I63.233','I63.239','I63.29','I63.30','I63.312','I63.319','I63.322',
    'I63.333','I63.39','I63.40','I63.412','I63.419','I63.422','I63.431','I63.439',
    'I63.441','I63.49','I63.50','I63.511','I63.532','I63.541','I63.543','I63.59',
    'I63.6','I63.8','I63.81','I63.89','I63.9','I50','I50.1','I50.20','I50.21',
    'I50.22','I50.23','I50.30','I50.31','I50.32','I50.33','I50.40','I50.41','I50.42',
    'I50.43','I50.810','I50.812','I50.813','I50.814','I50.82','I50.9','I10','I11',
    'I12','I13','I14','I15','I16','H35.03','I67.4','O10.1','O10.2','O10.3','O10.4',
    'O19')
    order by c.concept_code, c2.concept_code
    ;
    """  # noqa

""" icd10
    select c.concept_code, c2.concept_code from
    concept c inner join
    concept_relationship r
    on c.concept_id  = r.concept_id_1
    inner join concept c2
    on  r.concept_id_2 = c2.concept_id
    and c.domain_id = 'Condition' and c.vocabulary_id = 'ICD10CM'  and relationship_id = 'Subsumes'
    and c.concept_code  in
    ('C79', 'C79.00', 'C79.01', 'C79.10', 'C79.11', 'C79.2', 'C79.3', 'C79.31', 'C79.32',
     'C79.40', 'C79.49', 'C79.51', 'C79.52', 'C79.60', 'C79.70', 'C79.71', 'C79.81', 'C79.82',
     'C79.89', 'C79.9', 'D09.0', 'D09.10', 'D09.19', 'D09.20', 'D09.3', 'D09.8', 'D09.9', 'D36.0',
     'D36.10', 'D36.11', 'D36.12', 'D36.13', 'D36.15', 'D36.17', 'D36.7', 'D36.9', 'D48', 'D48.0',
     'D48.1', 'D48.2', 'D48.3', 'D48.4', 'D48.5', 'D48.60', 'D48.61', 'D48.62', 'D48.7', 'D48.9',
     'D49.0', 'D49.1', 'D49.2', 'D49.3', 'D49.4', 'D49.511', 'D49.512', 'D49.519', 'D49.6', 'D49.89',
     'D49.9', 'F17', 'F17.2', 'F17.200', 'F17.201', 'F17.203', 'F17.208', 'F17.209', 'F17.210', 'F17.211',
     'F17.213', 'F17.218', 'F17.219', 'F17.220', 'F17.221', 'F17.223', 'F17.228', 'F17.229', 'F17.290',
     'F17.291', 'F17.293', 'F17.298', 'F17.299', 'T65.2', 'T65.224A', 'T65.291A', 'T65.294A', 'Z71.6',
     'Z72.0', 'B20', 'B21', 'E78.0', 'E78.00', 'E78.01', 'E78.1', 'E78.2', 'E78.3', 'E78.4', 'E78.41',
     'E78.49', 'E78.5', 'F01.50', 'F01.51', 'F02.80', 'F02.81', 'F03', 'F03.90', 'F03.91', 'F04', 'F05',
     'F06.0', 'F06.1', 'F06.2', 'F06.30', 'F06.31', 'F06.32', 'F06.33', 'F06.34', 'F06.4', 'F06.8', 'F07.0',
     'F07.81', 'F07.89', 'F07.9', 'F09', 'D80', 'D81', 'D82', 'D83', 'D84', 'D85', 'D86', 'D87', 'D88',
     'D89', 'G47.3', 'J40', 'J41', 'J42', 'J43', 'J44', 'J45', 'J46', 'J47', 'J60', 'J66', 'J67.2', 'J67.8',
     'J67.9', 'J68.4', 'J84', 'D63.1', 'E08.22', 'E10.22', 'E11.22', 'E13.22', 'I12', 'N03', 'N18', 'O10.3',
     'Z49', 'Z91.15', 'Z99.2', 'F20', 'F21', 'F22', 'F23', 'F24', 'F25.0', 'F25.1', 'F25.8', 'F25.9', 'F28',
     'F29', 'I48', 'I48.0', 'I48.1', 'I48.2', 'I48.3', 'I48.4', 'I48.91', 'I48.92', 'E66.01', 'E66.09',
     'E66.1', 'E66.2', 'E66.8', 'E66.9')
    order by c.concept_code, c2.concept_code
    ;
    """  # noqa

""" drugs: esta no, ire por ancestros
    select c.concept_code, c2.concept_code from
    concept c inner join
    concept_relationship r
    on c.concept_id  = r.concept_id_1
    inner join concept c2
    on  r.concept_id_2 = c2.concept_id
    and c.domain_id = 'Drug' and c.vocabulary_id = 'ATC'  and relationship_id = 'Subsumes'
    and c.concept_code  in (
    'R03BC03', 'R03BC01', 'R03BB04', 'R03BB02', 'R03BB01', 'R03AK06', 'R03AK07', 'M01A', 'L01BA01', 'R03AK08', 'C10', 'C01DA', 'C03DA', 'B01AC06',
    'C03DB', 'B01AE', 'R03AC03', 'B01AF', 'B01AA', 'B01AC', 'C09B', 'C09C', 'C09A', 'A10B', 'C09D', 'A10A', 'H02AB', 'N01BA01', 'R03AL02', 'R03CC02',
    'R03DC01', 'R03DC03','A01AD05', 'R03AL0', 'L04AD', 'R03DA04', 'C03A', 'C03C', 'C08', 'R03CC04', 'R03AK11', 'R03AK10', 'R03AL05', 'R03AL04',
    'R03AL06', 'R03AL01', 'L04AA10', 'R03AL03', 'C07', 'R03AC19', 'R03AC18', 'L04AX03', 'C01AA', 'R03AC13', 'R03AC12', 'R03BA05', 'R03AC02',
    'R03BA01', 'R03BA03', 'R03BA02', 'R03AC04', 'L01XC', 'R03BA08')
    order by c.concept_code, c2.concept_code
    ;
    """  # noqa

""" drugs: def
    select c.concept_code as c1, c2.concept_code as c2 -- , c2.concept_name, c.concept_name
    from
        concept_ancestor ca inner join
        concept c
            on  ca.ancestor_concept_id = c.concept_id
            and c.vocabulary_id = 'ATC'
            and c.concept_code in ('R03BC03', 'R03BC01', 'R03BB04', 'R03BB02', 'R03BB01', 'R03AK06', 'R03AK07', 'M01A', 'L01BA01', 'R03AK08', 'C10', 'C01DA', 'C03DA', 'B01AC06',
    'C03DB', 'B01AE', 'R03AC03', 'B01AF', 'B01AA', 'B01AC', 'C09B', 'C09C', 'C09A', 'A10B', 'C09D', 'A10A', 'H02AB', 'N01BA01', 'R03AL02', 'R03CC02',
    'R03DC01', 'R03DC03','A01AD05', 'L04AD', 'R03DA04', 'C03A', 'C03C', 'C08', 'R03CC04', 'R03AK11', 'R03AK10', 'R03AL05', 'R03AL04',
    'R03AL06', 'R03AL01', 'L04AA10', 'R03AL03', 'C07', 'R03AC19', 'R03AC18', 'L04AX03', 'C01AA', 'R03AC13', 'R03AC12', 'R03BA05', 'R03AC02',
    'R03BA01', 'R03BA03', 'R03BA02', 'R03AC04', 'L01XC', 'R03BA08', 'R03AL07', 'R03AL08', 'R03AL09', 'R03AL10', 'R03AL11', 'R03AL12')
        inner join
        concept c2
            on ca.descendant_concept_id = c2.concept_id
            and c2.vocabulary_id = 'ATC'
    order by c2.concept_code
    ;
    """  # noqa


"""
a.) cohort valldehbron: tot >18 anys a data index, de eaps de l’area d’influencia de vallhebron [estimat uns 350k]
"""  # noqa

medeas = {}


def get_medeas():
    sql = "select scs_codi, medea from cat_centres"
    for up, medea in u.getAll(sql, "nodrizas"):
        medeas[up] = medea


get_medeas()
len(medeas)

dx_cohort_patients = set()


def get_dxCohort():
    sql = "select id_cip, pr_cod_ps from problemes"
    for id_cip, ps in u.getAll(sql, "import"):
        if ps in dx_map:
            dx_cohort_patients.add(id_cip)


len(dx_cohort_patients)
get_dxCohort()
len(dx_cohort_patients)


def birthday(date, when=d.utcnow().date()):
    # Get the current date
    # when = d.utcnow().date()
    # Get the difference between the current date and the birthday
    age = rd.relativedelta(when, date)
    age = age.years
    return age


birthday(d(2018, 1, 1).date(), d(2020, 1, 1).date())

rips = {}


def get_ripUp():
    sql = "select id_cip_sec, up from rip_assignada"
    for id_cip, up in u.getAll(sql, "nodrizas"):
        rips[id_cip] = up


get_ripUp()
len(rips)

poblacio = []
poblacio_valle = []
poblacio_altres = []


def get_poblacio():
    sql = """
        SELECT
            id_cip,
            usua_sexe,
            usua_data_naixement,
            usua_up_rca,
            usua_situacio,
            usua_data_situacio
        FROM assignada
    """
    index_date = d(2020, 3, 1).date()
    for row in u.getAll(sql, "import"):
        poblacio.append(row)
    print("got_it!")
    for id_cip, sexe, naix, up, sit, data_sit in poblacio:
        age = birthday(naix, index_date)
        up = rips[id_cip] if up == "" and id_cip in rips else up
        medea = medeas[up] if up in medeas else None
        if up in up_cohort_dict \
                and age >= 18 \
                and ((sit == "D" and data_sit > index_date) or sit == "A"):
            data_sit_d = data_sit if sit == "D" else None
            if up_cohort_dict[up] == "infl_valle":
                poblacio_valle.append((id_cip, sexe, age, up,
                                       medea, data_sit_d))
            if up_cohort_dict[up] == "infl_altres":
                if id_cip in dx_cohort_patients:
                    poblacio_altres.append((id_cip, sexe, age, up,
                                            medea, data_sit_d))
    return set([row[0] for row in poblacio_valle] +
               [row[0] for row in poblacio_altres])


cohort_ids = get_poblacio()
len(poblacio)  # 17.081.012
len(poblacio_valle)  # 264.477
len(poblacio_altres)  # 299.622
len(cohort_ids)  # 563.672

cohort_hash_map = {}  # amb_id_cip_sec: 565.991, amb id_cip: 563669
cohort_cip_map = {}  # para sacar ficheros en hash_ics


def get_cohort_hash_map():
    sql = "select id_cip, hash_a, hash_d from u11_all"
    for id_cip, hash_a, hash_d in u.getAll(sql, "import"):
        if id_cip in cohort_ids:
            cohort_hash_map[hash_a] = id_cip
            cohort_cip_map[id_cip] = hash_d


get_cohort_hash_map()
len(cohort_hash_map)
len(cohort_cip_map)

poblacio_valle.extend(poblacio_altres)
len(poblacio_valle)
poblacio_sortida = []
for row in poblacio_valle:
    fila = list(row)
    id_cip = fila[0]
    hash_d = cohort_cip_map[id_cip]
    fila[0] = hash_d
    poblacio_sortida.append(tuple(fila))
len(poblacio_sortida)

u.writeCSV(poblacio_outfile, poblacio_sortida)

""" # obara:3306.sidiap_forwar_data -> P2262.permanent.tabac (by segments)
    select hash_d,
        sector,
        tab,
        dalta,
        dbaixa,
        dlast,
        last as is_last
    from sidiap_forward_data.tabac t
        inner join sidiap_forward_data.u11_sector us on t.id_cip = us.id
        and date '2020-03-01' between dalta and dbaixa;
    """

tabac = {}


def get_tabac():
    sql = "select hash_d, tab from tabac"
    for hash_d, tab in u.getAll(sql, "permanent"):
        if hash_d in cohort_hash_map:
            id_cip = cohort_hash_map[hash_d]
            tabac[id_cip] = tab


get_tabac()
len(tabac)  # 514682
# cohort_hash_map[('9576279964F66534B9DFA556DE291B5BB899E485', '6102')]['id_cip_sec']  # noqa
# tabac[845869]
cohort_hash_map['9576279964F66534B9DFA556DE291B5BB899E485']
tabac[847913]

tractaments = []  # id_cip_sec: 499058, id_cip: 512344
buuu_presc = set()


def get_tractaments():
    sql = """
        select
            id_cip,
            ppfmc_pf_codi,
            ppfmc_atccodi,
            ppfmc_pmc_data_ini,
            ppfmc_data_fi,
            -- ppfmc_durada,
            ppfmc_freq,
            ppfmc_posologia
        from
            tractaments
        where
            ppfmc_data_fi >= date '2019-01-01' and
            left(ppfmc_atccodi, 3) = 'C09'
        """
    for id_cip, pf, atc, ini, fi, freq, pos in u.getAll(sql, 'import'):
        if id_cip in cohort_ids and atc in drug_map:
            if pf in pf_ddd:
                ddd, quantitat = pf_ddd[pf]['ddd'], pf_ddd[pf]['quantitat']
                ddp = quantitat * pos * (24.0/freq)
                dddpr = ddp/ddd
                tractaments.append((id_cip, atc, ini, fi, dddpr))
            else:
                buuu_presc.add(pf)


get_tractaments()
print(len(tractaments), len(buuu_presc))

sql = """
    select
        rec_cip,
        rec_pf_codi,
        pf_cod_atc,
        date(date_format(concat(rec_any_mes,'01'),'%Y-%m-%d')) as ini,
        rec_envasos
    from
        fartb201
    partition ({})
    where
        left(pf_cod_atc, 3) in ('C08', 'C10', 'C07') or
        left(pf_cod_atc, 4) in ('L04A', 'R03D', 'R03C', 'R03B', 'R03A', 'C09B',
                                'C09C', 'C09A', 'A10B', 'C09D', 'A10A', 'N01B',
                                'C03D', 'C01A', 'C03A', 'C01D', 'C03C', 'M01A',
                                'H02A', 'L01B', 'L01X', 'B01A', 'A01A')
    """
dispensacions = []
buuu_disp = set()


def get_dispensacions(sql):
    partitions = """
        t201901, t201902, t201903, t201904,
        t201905, t201906, t201907, t201908,
                 t201910, t201911, t201912,
        t202001, t202002, t202003, t202004,
        t202005
        """
    # partitions = "t202004"
    sql = sql.format(partitions)
    for hash_a, pf, atc, ini, envasos in u.getAll(sql, SIDICS_DB):
        if hash_a in cohort_hash_map and atc in drug_map:
            if pf in pf_ddd:
                id_cip = cohort_hash_map[hash_a]
                ddd = pf_ddd[pf]['ddd']
                quantitat = pf_ddd[pf]['quantitat']
                envas_units = pf_ddd[pf]['envas_units']
                quantitat_disp = quantitat * envas_units * envasos
                ddd_disp = float(quantitat_disp)/ddd
                fi = ini + timedelta(days=int(ddd_disp))
                dispensacions.append((id_cip, atc, ini, fi, ddd_disp))
            else:
                buuu_disp.add(pf)


get_dispensacions(sql)
print(len(dispensacions), len(buuu_disp))

covid = []
# [col for col in u.getTableColumns('sisap_covid_pac_master','redics')]


def get_covid():
    sql = """
        SELECT
            i.hash_ICS,
            estat,
            cas_data,
            ing_alta,
            ing_desti,
            exitus
        FROM
            sisap_covid_pac_master c INNER JOIN
            preduffa.sisap_covid_pac_id i
            ON c.hash = i.HASH
        WHERE
            estat IN ('Possible', 'Confirmat')
        """
    for hash_a, estat, data, ing_alta, ing_desti, exitus \
            in u.getAll(sql, 'redics'):
        if hash_a in cohort_hash_map:
            id_cip = cohort_hash_map[hash_a]
            data = data.date().isoformat() if data else data
            ing_alta = ing_alta.date().isoformat() if ing_alta else ing_alta
            exitus = exitus.date().isoformat() if exitus else exitus
            covid.append((id_cip, estat, data, ing_alta, ing_desti, exitus))


get_covid()
len(covid)

t = d.now()
problemes = []  # paso1: 652.686, paso 2: +834.195 = 1.486.881
# [col for col in u.getTableColumns('problemes','import')]


def get_problemes():
    sql = """
    select
        id_cip
        ,pr_cod_ps
        ,pr_dde
        -- ,if(pr_dba = 0 or pr_dba > data_ext, 0, 1) as tancat
    from
        problemes,
        nodrizas.dextraccio
    where
        pr_cod_o_ps = 'C'
        and pr_hist = 1
        and pr_dde <= data_ext
    """
    for id_cip, ps, dde in u.getAll(sql, 'import'):
        if id_cip in cohort_ids:
            if ps in dx_map:
                dx = dx_map[ps]
                problemes.append((id_cip, dx, dde))
            if ps in dx_map_altres:
                dx = dx_map_altres[ps]
                problemes.append((id_cip, dx, dde))


get_problemes()
len(problemes)
print(d.now() - t)

# 1.255.382 agrupando a min(dde) del dx_grup,
# 1.101.081 sin agr_fumador (que lo sacamos de tabac)
problemes_dict = {}
for id_cip, dx, dde in problemes:
    if dx != 'agr_fumador':
        if (id_cip, dx) not in problemes_dict:
            problemes_dict[(id_cip, dx)] = dde
        elif dde < problemes_dict[(id_cip, dx)]:
            problemes_dict[(id_cip, dx)] = dde
len(problemes_dict)

# test de poblacio con dx de filtro de cohorte valle
"""
id_test = 1491834 # valle
id_test = 1022991 # altres
for x in [(id_test, k) for k in [k for k in dx_cohort.keys() if 'agr_' in k] + [k for k in dx_cohort['dx_cv'].keys()]]:
    if x in problemes_dict:
        print(x, problemes_dict[x])
"""  # noqa

# test sortida
"""
for k in [k for k in problemes_dict.keys()[:3]]:
    print(k[0], k[1], problemes_dict[k])
"""

problemes_sortida = []  # id, dx, data
for k in problemes_dict:
    problemes_sortida.append((k[0], k[1], problemes_dict[k].isoformat()))

# [col for col in u.getTableColumns('variables', 'import')]
variables = []


def get_variables():
    sql = "SELECT id_cip, vu_cod_vs, vu_dat_act, vu_val FROM variables2 {}"
    sql = sql.format("""
                     WHERE vu_dat_act <= date '2020-03-01'
                     AND vu_cod_vs in {}
                     """.format(tuple(var_map.keys()))
                     )
    for id_cip, cod, dat, val in u.getAll(sql, 'import'):
        if id_cip in cohort_ids:
            cod = var_map[cod]
            variables.append((id_cip, cod, dat, val))


get_variables()
len(variables)

# nos quedamos con la fecha de ultima variable
variables_last = {}
for id_cip, cod, data, val in variables:
    if (id_cip, cod) not in variables_last:
        variables_last[(id_cip, cod)] = data
    elif data > variables_last[(id_cip, cod)]:
        variables_last[(id_cip, cod)] = data
len(variables_last)

# nos quedamos con el valor de la ultima variable
variables_max = {}
for id_cip, cod, data, val in variables:
    max_data = variables_last[(id_cip, cod)]
    if max_data == data:
        if (id_cip, cod, data) not in variables_max:
            variables_max[(id_cip, cod, data)] = val
        elif val > variables_max[(id_cip, cod, data)]:
            variables_max[(id_cip, cod, data)] = val
len(variables_max)

labs = []
# [col for col in u.getTableColumns('laboratori', 'import')]


def get_labs():
    sql = """
        SELECT id_cip, cr_codi_prova_ics, cr_data_reg, cr_res_lab
        FROM laboratori2 {}
        """
    sql = sql.format("""
                     WHERE cr_data_reg <= date '2020-03-01'
                     AND cr_codi_prova_ics IN {}
                     """.format(tuple(lab_map.keys()))
                     )
    for id_cip, cod, dat, val in u.getAll(sql, 'import'):
        if id_cip in cohort_ids:
            try:
                val = float(val.replace(',', '.'))
            except ValueError:
                pass
            cod = lab_map[cod]
            labs.append((id_cip, cod, dat, val))


get_labs()
len(labs)

# nos quedamos con la FECHA de ultimo lab
labs_last = {}
for id_cip, cod, data, val in labs:
    if (id_cip, cod) not in labs_last:
        labs_last[(id_cip, cod)] = data
    elif data > labs_last[(id_cip, cod)]:
        labs_last[(id_cip, cod)] = data
len(labs_last)

# nos quedamos con el max(VALOR) de ultimo lab
for id_cip, cod, data, val in labs:
    max_data = labs_last[(id_cip, cod)]
    if max_data == data:
        if (id_cip, cod, data) not in variables_max:
            variables_max[(id_cip, cod, data)] = val
        elif val > variables_max[(id_cip, cod, data)]:
            variables_max[(id_cip, cod, data)] = val
len(variables_max)

# metemos a variables el valor de tabaco (cod='tabac') en variables_max
# con (data = fecha index):
data = d(2020, 3, 1).date()
cod = 'tabac'
for id_cip, val in tabac.items():
    if (id_cip, val, data) not in variables_max:
        variables_max[(id_cip, cod, data)] = val
    elif val > variables_max[(id_cip, cod, data)]:
        variables_max[(id_cip, cod, data)] = val

variables_sortida = []  # (id, cod, data) = val
for k in variables_max:
    variables_sortida.append((k[0], k[1], k[2].isoformat(), variables_max[k]))

len(variables_sortida)


"""
con nodriza.assignada_tot
    >>> get_poblacio()
    got_it!
    >>> len(poblacio)
    6848809
    >>> len(poblacio_valle)
    264407 # >=18 en ups de influencia
    >>> len(poblacio_altres)
    1064754 # idem, pero sin pasar por problemas de filtrado
    295066 con problemas de filtrado.

con import.assignada:
    >>> len(poblacio)
    17081012
    >>> len(poblacio_valle)
    264630
    >>> len(poblacio_altres)
    295260
    >>>
con import.assignada (recuperando up desde rip_assignada)
>>> get_poblacio()
    >>> len(poblacio)
    17081012
    >>> len(poblacio_valle)
    266333
    >>> len(poblacio_altres)
    299658
"""

# sortides

u.writeCSV(up_cohort_outfile, up_cohort_dict.items())
u.writeCSV(drug_dict_outfile, drug_map.items())

hash_hashcovid_map, hashcovid_hash_map = {}, {}


def get_covid_map():
    sql = """
        SELECT distinct hash_ics, c.hash
        FROM sisap_covid_pac_master c INNER JOIN
            preduffa.sisap_covid_pac_id i
                ON c.hash = i.HASH
        WHERE
            estat IN ('Possible', 'Confirmat')
        """
    for hash_d, hashcovid in u.getAll(sql, 'redics'):
        if hash_d in cohort_hash_map:
            hash_hashcovid_map[hash_d] = hashcovid
            hashcovid_hash_map[hashcovid] = hash_d


get_covid_map()
print(len(hash_hashcovid_map), len(hashcovid_hash_map))

altes = []


def get_altes():
    sql = """
        SELECT
            substr(cip,1,13), up, t_act, d_ingres, c_ingres, pr_ingres, d_alta,
            c_alta, up_desti, dp, dp, ds1, ds2, ds3, ds4, ds5, ds6, ds7, ds8,
            ds9, ds10, ds11, ds12, ds13, ds14, pp, ps1, ps2, ps3, ps4, ps5,
            ps6, ps7, ps8, ps9, ps10, ps11, ps12, ps13, ps14, ps15, ps16,
            ps17, ps18, ps19, uci, d_ini_uci, d_fi_uci, dies_uci
        FROM DWFACTICS.CMBD_ICS
        WHERE d_ingres >= DATE '2020-03-01' and cip is not null
    """
    for cip, up, t_act, d_ingres, c_ingres, pr_ingres, d_alta, c_alta, \
            up_desti, dp, dp, ds1, ds2, ds3, ds4, ds5, ds6, ds7, ds8, ds9, \
            ds10, ds11, ds12, ds13, ds14, pp, ps1, ps2, ps3, ps4, ps5, ps6, \
            ps7, ps8, ps9, ps10, ps11, ps12, ps13, ps14, ps15, ps16, ps17, \
            ps18, ps19, uci, d_ini_uci, d_fi_uci, dies_uci \
            in u.getAll(sql, 'exadata'):
        hashcovid = h.sha1(cip).hexdigest().upper()
        if hashcovid in hashcovid_hash_map:
            hash_d = hashcovid_hash_map[hashcovid]
            altes.append((hash_d, up, t_act, d_ingres, c_ingres, pr_ingres,
                          d_alta, c_alta, up_desti, dp, dp, ds1, ds2, ds3, ds4,
                          ds5, ds6, ds7, ds8, ds9, ds10, ds11, ds12, ds13,
                          ds14, pp, ps1, ps2, ps3, ps4, ps5, ps6, ps7, ps8,
                          ps9, ps10, ps11, ps12, ps13, ps14, ps15, ps16, ps17,
                          ps18, ps19, uci, d_ini_uci, d_fi_uci, dies_uci))


get_altes()
len(altes)
u.writeCSV(altes_outfile, altes)

covid_sortida = []
for row in covid:
    fila = list(row)
    id_cip = fila[0]
    hash_d = cohort_cip_map[id_cip]
    fila[0] = hash_d
    covid_sortida.append(tuple(fila))
len(covid_sortida)
u.writeCSV(covid_outfile, covid_sortida)


problemes_sortida_hash = []
for row in problemes_sortida:
    fila = list(row)
    id_cip = fila[0]
    hash_d = cohort_cip_map[id_cip]
    fila[0] = hash_d
    problemes_sortida_hash.append(tuple(fila))
len(problemes_sortida_hash)
u.writeCSV(problemes_outfile, problemes_sortida_hash)


variables_sortida_hash = []
for row in variables_sortida:
    fila = list(row)
    id_cip = fila[0]
    hash_d = cohort_cip_map[id_cip]
    fila[0] = hash_d
    variables_sortida_hash.append(tuple(fila))
len(variables_sortida_hash)
u.writeCSV(variables_outfile, variables_sortida_hash)

tractaments_sortida = []
for row in tractaments:
    fila = list(row)
    id_cip = fila[0]
    hash_d = cohort_cip_map[id_cip]
    fila[0] = hash_d
    tractaments_sortida.append(tuple(fila))
len(tractaments_sortida)
u.writeCSV(tractaments_outfile, tractaments_sortida)

dispensacions_sortida = []
for row in dispensacions:
    fila = list(row)
    id_cip = fila[0]
    hash_d = cohort_cip_map[id_cip]
    fila[0] = hash_d
    dispensacions_sortida.append(tuple(fila))
len(dispensacions_sortida)
u.writeCSV(dispensacions_outfile, dispensacions_sortida)
