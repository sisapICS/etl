from os import path
import hashlib as h

import sisapUtils as u


project_name = 'P630_S0'
pob_file = 'poblacio.txt'
pob_fsn = path.join(u.tempFolder, project_name, pob_file)
pcr_file = 'pcr.txt'
pcr_outfile = path.join(u.tempFolder, project_name, pcr_file)

hashes = set()
for row in u.readCSV(pob_fsn, sep='@'):
    hashes.add(row[0])


def get_hash(cip):
    """."""
    return h.sha1(cip).hexdigest().upper()


hashcovid_hash_map = {}


def getHashToHashCovid():
    sql = 'select usua_cip, usua_cip_cod from pdptb101'
    for cip, hash in u.getAll(sql, 'pdp'):
        if hash in hashes:
            hashcovid = get_hash(cip)
            # hash_hashcovid_map[hash] = hashcovid
            hashcovid_hash_map[hashcovid] = hash


# hash_hashcovid_map = {}
getHashToHashCovid()

proves_sql = """
    SELECT
    --    i.cip,
        p.hash, {}
    FROM
        preduffa.sisap_covid_pac_prv_aux p
    --        INNER JOIN
    --    preduffa.sisap_covid_pac_id i
    --        ON i.hash = p.hash
    WHERE prova = 'PCR' and origen <> 'IA' and estat_cod <> 9
    """
columns = ["data", "estat_cod"]
proves_sql = proves_sql.format(", ".join("p." + col for col in columns))
db = "redics"

pcr_dict = {0: 'Positiu',
            1: 'No concloent',
            3: 'Negatiu',
            4: 'No valorable',
            9: 'Descartar'}

# GET: proves
proves = []
for hashcovid, data, estat_cod in u.getAll(proves_sql, db):
    if hashcovid in hashcovid_hash_map:
        hash = hashcovid_hash_map[hashcovid]
        estat_des = pcr_dict[estat_cod]
        proves.append((hash, data.date().strftime("%Y-%m-%d"), estat_des))

u.writeCSV(pcr_outfile, proves)


# ######### SCRATCH ####

proves = [(hashcovid_hash_map[row[0]],)
          + row[1:]
          for row in u.getAll(proves_sql, db) if row[0] in hashcovid_hash_map]


def get_covid_map():
    sql = """
        SELECT distinct hash_ics, hash
        FROM
        --    sisap_covid_pac_master c INNER JOIN
            sisap_covid_pac_id i
        --        ON c.hash = i.HASH
        -- WHERE
        --     estat IN ('Possible', 'Confirmat')
        """
    for hash_d, hashcovid in u.getAll(sql, 'redics'):
        if hash_d in hashes:
            # hash_hashcovid_map[hash_d] = hashcovid
            hashcovid_hash_map[hashcovid] = hash_d
