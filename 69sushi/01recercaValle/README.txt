filas		fichero
----------+-----------------
      5148 altes.txt
     22629 covid.txt
  10810692 dispensacions.txt
    564099 poblacio.txt
    512344 prescripcions.txt
   1101081 problemes.txt
   2136751 variables.txt
        90 up_cohort_dict.txt
       695 drug_dict.txt

Diccionario de tablas:
----------------------
Los ficheros constan de registos sin encabezado, a continuación se describen los campos y se mencionan aclaraciones a tener en cuenta:
* Los campos estan separados por el simbolo '@'
* Todas las fechas estan en formato ISO 'YYYY-MM-DD', excepto en altes.txt donde podrian llevar la hora:minuto:segundo
* La fecha de defunción puede venir en los ficheros població.txt, covid.txt y altes.txt (en función de la fuente del dato):
* Cada registro de prescripción corresponde a una prescripción de una presentación farmaceutica (medicamento comercializado), que he homologado al ATC y cuantitativamente a la ddd_prescrita
* Cada registro de dispensación correponde al acumulado de una presentación farmaceutica (medicamento comercializado) facturada en el mes, que he homologado al ATC y cuantitativamente a la ddd_dispensada

up_cohort_dict.txt: (up, cohort)
		(up: unitat productiva EAP, cohort=['infl_valle', 'infl_altres'])
drug_dict.txt: (atc, grup_drug)
		(atc: codi ATC, grup_drug: grup de l'estudi [ej: 'drug_araii_sacubitril_valsartan'])
poblacio.txt: (id_cip, sexe, age, up, medea, data_sit_d)
		(age: edad en años en fecha index, data_sit_d: fecha de defunción segun registro de ECAP)
covid.txt: (id_cip, estat, data, ing_alta, ing_desti, exitus)
		(estat: ['Possible', 'Confirmat'], ing_alta: fecha de ingreso hx en caso de ingreso, exitus: fecha de defunción según Registro RSA-Covid19)
problemes.txt: (id_cip, cod, data)
		(cod: descripción del agrupador del problema de salud)
variables.txt: (id_cip, cod, data, val)
		(cod: ['PES', 'TALLA', 'IMC', 'PAS', 'PAD', 'hba1c','tabac'], val: el valor de la variable, todas son continuas excepto 'tabac': 0=No, 1=Fumador, 2=Exfumador)
prescripcions.txt: (id_cip, atc, data_ini, data_fi, ddd_prescrites)
		(cod: codigo ATC, ddd_prescrites: numero de DosisDiariasDefinidas que se prescribe por dia, para conseguir las totales habria que multiplicarlas por el numero de dias entre data_ini y data_fi)
dispensacions.txt: (id_cip, atc, data_ini, data_fi, ddd_dispensades)
		(cod: codigo ATC ddd_dispensades: numero de DosisDiariasDefinidas TOTALES que se lleva de la farmacia, data_ini: dia 01 del AnyMes de facturación, data_fi= data_ini + ddd_disp)
altes.txt: (id_cip, up, t_act, d_ingres, c_ingres, pr_ingres, d_alta, c_alta, up_desti, dp,
            dp, ds1, ds2, ds3, ds4, ds5, ds6, ds7, ds8, ds9, ds10, ds11, ds12, ds13, ds14,
            pp, ps1, ps2, ps3, ps4, ps5, ps6, ps7, ps8, ps9, ps10, ps11, ps12, ps13, ps14,
            ps15, ps16, ps17, ps18, ps19, uci, d_ini_uci, d_fi_uci, dies_uci)
		(Notas: las altas hospitalarias utilizan codificación cim10mc, c_alta= 6 es circunstancia de alta 'defunción', dp: diagnostico principal, ds1..ds14 diagnosticos secundarios, pp: procedimiento principal, ps1..ps19: procedimientos secundarios)