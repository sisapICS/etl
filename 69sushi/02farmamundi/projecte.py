# embarasos tancats al 2019
import sisapUtils as u
import collections as c
import datetime as d

# estem en Post-entrega, pert tant deixo comentada creació de taula de embaras
# el motiu es que la població ja esta bé i els canvis son a d'altres taules

DB_DESTI = "redics"
WATCHERS = ("PREDULMB", "PREDUTFP",)  # Javier L. Sevilla Merino (MN)
CNT_SQL = "select count(*) from {}"


ps_cods = [
    'C01-O9A.413', 'C01-O14.9', 'C01-O14.23', 'C01-N90.810', 'C01-N90.813',
    'C01-N90.812', 'C01-N90.811', 'C01-O24.41', 'C01-O24.414', 'C01-O24.410',
    'C01-N87', 'C01-N87.9', 'C01-N87.1', 'C01-Y07.01', 'C01-O9A.41',
    'C01-O14.20', 'C01-O9A.412', 'C01-O9A.411', 'C01-O9A.43', 'C01-O9A.42',
    'C01-O9A.419', 'C01-O9A.5', 'C01-O9A.512', 'C01-O9A.511', 'C01-O9A.51',
    'C01-O9A.52', 'C01-O9A.519', 'C01-O9A.513', 'C01-N87.0', 'C01-N80.9',
    'C01-F32.3', 'C01-F32.9', 'C01-D06', 'C01-D06.7', 'C01-D06.1', 'C01-D06.0',
    'C01-D06.9', 'C01-N80.0', 'C01-N80', 'C01-O25.3', 'C01-N80.4', 'C01-N80.3',
    'C01-O14.13', 'C01-N80.1', 'C01-O14.2', 'C01-N80.8', 'C01-N80.6',
    'C01-N80.5', 'C01-O14.02', 'C01-O14.00', 'C01-O14.0', 'C01-O14',
    'C01-O14.12', 'C01-O14.10', 'C01-O14.1', 'C01-O14.03', 'C01-O14.22',
    'C01-O14.90', 'C01-N80.2', 'C01-Z87.51', 'C01-O9A.53', 'C01-T74.21XA',
    'C01-T74.21', 'C01-T74.2', 'C01-T74.21XS', 'C01-T74.21XD', 'C01-T74.31',
    'C01-T74.3', 'C01-Z91.410', 'C01-Z91.41', 'C01-Z91.419', 'C01-Z91.412',
    'C01-T74.11XA', 'C01-Z37.1', 'C01-T74.11XD', 'C01-Z64.0', 'C01-Z85.41',
    'C01-Z86.32', 'C01-O00.90', 'C01-O14.04', 'C01-O14.15', 'C01-O14.14',
    'C01-O14.05', 'C01-O14.94', 'C01-O14.25', 'C01-O14.24', 'C01-O14.95',
    'C01-O24.415', 'C01-Z91.411', 'C01-O9A.319', 'C01-A54.81', 'C01-O14.93',
    'C01-O14.92', 'C01-N71.1', 'C01-N71.0', 'C01-N73', 'C01-N96', 'C01-N73.9',
    'C01-O25.10', 'C01-O9A.3', 'C01-O9A.312', 'C01-O9A.311', 'C01-T74.11XS',
    'C01-O9A.32', 'C01-O24.419', 'C01-O9A.313', 'C01-O9A.33', 'C01-O9A.4',
    'C01-T74.31XS', 'C01-T74.31XD', 'C01-T74.31XA', 'C01-T74.9',
    'C01-T74.91XA', 'C01-T74.91', 'C01-T74.91XD', 'C01-T74.91XS', 'C01-T74.11',
    'C01-T74.1', 'C01-O9A.31', 'C01-A52.1', 'C01-A52.3', 'C01-A56',
    'C01-A54.9', 'C01-A56.00', 'C01-A56.0', 'C01-A56.09', 'C01-A56.02',
    'C01-A56.01', 'C01-A56.19', 'C01-A56.11', 'C01-A56.1', 'C01-A56.3',
    'C01-A54.85', 'C01-D50.9', 'C01-A54.86', 'C01-A52.09', 'C01-A52.06',
    'C01-A52.14', 'C01-A52.13', 'C01-A52.12', 'C01-A52.11', 'C01-A52.10',
    'C01-A52.2', 'C01-A52.19', 'C01-A52.17', 'C01-A52.16', 'C01-A52.15',
    'C01-F32.2', 'C01-A56.2', 'A52.0', 'A54.2', 'A54.9', 'A54.0', 'A54',
    'A54.8', 'A54.6', 'A54.5', 'A54.3', 'A54.1', 'A52.8', 'A52.7', 'A52.3',
    'C01-A54.89', 'A52.1', 'C01-A63.0', 'A52', 'N87.1', 'N87.2', 'N87.0',
    'N87.9', 'N87', 'D06.9', 'D06.7', 'D06.1', 'D06', 'D06.0', 'A54.4',
    'C01-A54.84', 'A52.2', 'C01-A54.4', 'C01-A52.7', 'C01-A54.09',
    'C01-A54.03', 'C01-A54.2', 'C01-A54.1', 'C01-A54.22', 'C01-A54.21',
    'C01-A54.29', 'C01-A54.24', 'C01-A54.23', 'C01-A54.32', 'C01-A54.31',
    'C01-A54.01', 'C01-A54.3', 'C01-A54.02', 'C01-A54.39', 'C01-A54.33',
    'C01-A54.43', 'C01-A54.42', 'C01-A54.41', 'C01-A54.40', 'C01-A54.8',
    'C01-A54.6', 'C01-A54.5', 'C01-A54.49', 'C01-A54.83', 'C01-A54.82',
    'A52.9', 'C01-A54.30', 'C01-A52.0', 'C01-A52.71', 'C01-A52.75',
    'C01-A52.74', 'C01-A52.73', 'C01-A52.72', 'C01-A52.78', 'C01-A52.77',
    'C01-A52.76', 'C01-A64', 'C01-A52.8', 'C01-A52.79', 'C01-F53',
    'C01-A54.00', 'C01-A52.00', 'C01-F43.20', 'C01-A52', 'C01-A56.4',
    'C01-A56.8', 'C01-A59.00', 'C01-A52.03', 'C01-A52.02', 'C01-A52.01',
    'C01-A52.05', 'C01-A52.04', 'C01-A60.00', 'C01-A52.9', 'C01-A54.0',
    'C01-A54', 'C01-F54',
]

usu_cols = [
    'USUA_DATA_NAIXEMENT', 'USUA_SITUACIO', 'USUA_UAB_UP', 'USUA_ABS_CODI_ABS',
    'USUA_NACIONALITAT', 'USUA_UP_RCA',
    # 'USUA_UAB_SAP', 'USUA_UAB_EP',
    'USUA_LOCALITAT',
]

vars_cols = [
    'VU_COD_VS', 'VU_DAT_ACT', 'VU_UP', 'VU_VAL',
    # 'VU_DUM',
    # 'VU_SAP', 'VU_EP', 'VU_DATA_ALTA',
    # 'VU_USU',
    # 'VU_V_CEN', 'VU_V_CLA',
    'VU_V_DAT',
    # 'VU_V_MOD', 'VU_V_NUM',
]

emb_cols = [
    # 'EMB_COD',
    'EMB_N_ORDRE', 'EMB_D_INI',
    # 'EMB_ANT', 'EMB_EDAT',
    'EMB_N_FETUS', 'EMB_DUR',
    # 'EMB_PES_INI', 'EMB_TALLA',
    'EMB_IMC', 'EMB_TEPAL',
    # 'EMB_G_SANG', 'EMB_RH',
    'EMB_DATA_BAIXA', 'EMB_D_TANCA', 'EMB_C_TANCA', 'EMB_DURADA',
    # 'EMB_DUR_QUAL',
    'EMB_RISC', 'EMB_D_FI', 'EMB_DESC_RISC',
    # 'EMB_TANC_PUER',
    'EMB_DURCORR',
    # 'EMB_PUER_TIP', 'EMB_RH_PARE', 'EMB_PLANIF', 'EMB_POSITIV',
    # 'EMB_COD_O_PS',
    'EMB_COD_PS',
    # 'EMB_ANTID', 'EMB_D_ANTID',
    'EMB_UP',
    # 'EMB_SAP', 'EMB_EP', 'CODI_AMBIT', 'PAVI_COD_CEN', 'PAVI_CLA_CEN',
    ]

seg_cols = [  # 'PAVI_EMB_COD',
    'PAVI_COD_CEN', 'PAVI_CLA_CEN', 'PAVI_SERVEI', 'PAVI_MODUL', 'PAVI_DATA_V',
    'PAVI_NUM_V', 'PAVI_SEG_TIPUS', 'PAVI_N_COL',
    'PAVI_DATA_A',  # 'PAVI_DATA_M', 'PAVI_DATA_B',
    'PAVI_UP',
    # 'PAVI_SAP', 'PAVI_EP', 'PAVI_ID', 'CODI_AMBIT',
    ]

ps_cols = [
    'PR_COD_PS', 'PR_DDE', 'PR_DBA', 'PR_UP',
    # 'CODI_SAP',
    ]

grups_cols = [
    'codi_sector', 'GRUP_NUM',
    'GRUP_COD_PS', 'GRUP_CODI_UP', 'GRUP_CODI_CENTRE',
    'GRUP_CLASSE_CENTRE', 'GRUP_DATA_INI', 'GRUP_DATA_INI',
    'GRUP_DATA_FI', 'GRUP_DIAGNOSTIC', 'GRUP_OBJECTIU', 'GRUP_JUSTIFICACIO',
    ]

pac_grups_cols = [
    'PAGR_CIP', 'CODI_SECTOR', 'PAGR_NUM_GRUP',
    ]

visi_cols = [
    # 'VISI_SAP',
    'VISI_UP',
    # 'VISI_EP',
    'VISI_CENTRE_CODI_CENTRE',
    'VISI_CENTRE_CLASSE_CENTRE', 'VISI_SERVEI_CODI_SERVEI',
    'VISI_MODUL_CODI_MODUL', 'VISI_TIPUS_CITACIO', 'VISI_TIPUS_VISITA',
    'VISI_DATA_VISITA',
    ]


def get_embaras():  # DONE
    db = "import"
    tb = "embaras"
    tb_desti = 'fmundi_{}'.format(tb)
    cols = emb_cols
    coldef = ['codi_sector varchar2(4)',
              'id_cip_sec number(10)',
              'emb_cod number(10)']

    dades = []
    emb_cods = set()
    cips = set()
    cip_embs = c.defaultdict(lambda: c.defaultdict(set))

    sql = """
        SELECT
            codi_sector, id_cip_sec, emb_cod, {cols}
            , CASE WHEN emb_d_fi = 0 AND emb_d_tanca = 0 THEN
                    date_add(emb_d_ini, interval 9 month)
                 WHEN emb_d_fi = 0 THEN
                    emb_d_tanca
                 ELSE emb_d_fi END as data_fi_seguiment
        FROM
            embaras emb
        WHERE
            extract(YEAR FROM emb_d_ini) IN (2018, 2019) AND
            CASE WHEN emb_d_fi = 0 AND emb_d_tanca = 0 THEN
                    extract(YEAR FROM date_add(emb_d_ini, interval 9 month))
                 WHEN emb_d_fi = 0 THEN
                    extract(YEAR FROM emb_d_tanca)
                 ELSE extract(YEAR FROM emb_d_fi) END = 2019
    """.format(cols=", ".join(cols))

    for row in u.getAll(sql, db):
        sec, cip, emb_cod = row[0], row[1], row[2]
        emb_d_ini, emb_d_fi = row[4], row[-1]
        cips.add(cip)
        emb_cods.add((sec, emb_cod))
        dades.append(row)
        cip_embs[cip]['ini'].add(emb_d_ini)
        cip_embs[cip]['fi'].add(emb_d_fi)

    for col in cols:
        coldef.append(
            u.getColumnInformation(col, tb, db, desti=DB_DESTI)['create'])
    coldef.append('data_fi_seg date')

    # u.createTable(
    #     tb_desti,
    #     "({})".format(", ".join(coldef)),
    #     "redics",
    #     rm=True)
    # u.listToTable(dades, tb_desti, DB_DESTI)
    # u.grantSelect(tb_desti, WATCHERS, DB_DESTI)

    nd = [row for row, in u.getAll(CNT_SQL.format(tb_desti), DB_DESTI)][0]
    print("{}: ori ({}), dest ({})".format(tb_desti, len(dades), nd))

    return emb_cods, cips, cip_embs


def get_seguiment():
    dades = []
    coldef = [
        'codi_sector varchar2(4)',
        'id_cip_sec number(10)',
        'pavi_emb_cod number(10)']
    db = "import"
    tb = "assir221"
    tb_desti = 'fmundi_{}'.format("seguiment")
    cols = seg_cols
    for col in cols:
        coldef.append(
            u.getColumnInformation(col, tb, db, desti=DB_DESTI)['create'])
    u.createTable(
        tb_desti,
        "({})".format(", ".join(coldef)),
        "redics",
        rm=True)
    u.grantSelect(tb_desti, WATCHERS, DB_DESTI)
    sql = """
        SELECT
            codi_sector, id_cip_sec, PAVI_EMB_COD, {cols}
        FROM
            {tb}
    """.format(cols=", ".join(cols), tb=tb)
    for row in u.getAll(sql, db):
        sec, emb_cod = row[0], row[2]
        if (sec, emb_cod) in emb_cods:
            dades.append(row)
    print("{}: ".format(tb), len(dades), coldef)
    return dades, tb_desti


def get_usuaris():
    dades = []
    coldef = []
    db = "import"
    tb = "assignada"
    tb_desti = 'fmundi_{}'.format("usuaris")
    cols = usu_cols
    for col in ['codi_sector', 'id_cip_sec'] + cols:
        coldef.append(
            u.getColumnInformation(col, tb, db, desti=DB_DESTI)['create'])
    u.createTable(
        tb_desti,
        "({})".format(", ".join(coldef)),
        "redics",
        rm=True)
    u.grantSelect(tb_desti, WATCHERS, DB_DESTI)
    sql = """
        SELECT
            codi_sector, id_cip_sec, {cols}
        FROM
            assignada
    """.format(cols=", ".join(cols))
    # cips_u40 = set()
    for row in u.getAll(sql, db):
        cip = row[1]
        if cip in cips:
            dades.append(row)
            # cips_u40.add(cip)
    print("{}: ".format(tb), len(dades), coldef)
    return dades, tb_desti


def get_partition(db, tb, col, since):
    """."""
    partitions = {}
    dexta, = u.getOne(
        """
        select
            date_add(data_ext, interval - {since})
        from
            dextraccio
        """.format(since=since),
        "nodrizas")
    sql = "select {col} from {part} limit 1"
    for partition in u.getTablePartitions(tb, db):
        try:
            d_col, = u.getOne(sql.format(col=col, part=partition), db)
        except TypeError:
            continue
        if d_col.year >= dexta.year:
            partitions[partition] = True
    return partitions


def get_variables():
    dades = []
    coldef = []
    db = "import"
    tb = "variables"
    tb_desti = 'fmundi_{}'.format(tb)
    cols = vars_cols
    for col in ['codi_sector', 'id_cip_sec'] + cols:
        coldef.append(
            u.getColumnInformation(col, tb, db, desti=DB_DESTI)['create'])
    u.createTable(
        tb_desti,
        "({})".format(", ".join(coldef)),
        "redics",
        rm=True)
    u.grantSelect(tb_desti, WATCHERS, DB_DESTI)
    cods = [
        'EP1001', 'EP1002', 'VW3004',
        'VP3001', 'EP3001', 'VW3002',
        'VW3003', 'VW1001', 'VX5001']
    parts = get_partition(db, tb, 'vu_dat_act', '5 year')
    for part in parts:
        sql = """
            SELECT
                codi_sector, id_cip_sec, {cols}
            FROM
                {tb}
            where
                vu_cod_vs in {cods}
        """.format(cols=", ".join(cols), tb=part, cods=tuple(cods))
        for row in u.getAll(sql, db):
            cip = row[1]
            if cip in cips:
                dades.append(row)
    print("{}: ".format(tb), len(dades), coldef)
    return dades, tb_desti


def get_problemes(ps_cods):
    LIMIT = False
    limit = "LIMIT 1000"
    dades = []
    coldef = []
    db = "import"
    tb = "problemes"
    tb_desti = 'fmundi_{}'.format(tb)
    cols = ps_cols
    for col in ['codi_sector', 'id_cip_sec'] + cols:
        coldef.append(
            u.getColumnInformation(col, tb, db, desti=DB_DESTI)['create'])
    u.createTable(
        tb_desti,
        "({})".format(", ".join(coldef)),
        "redics", rm=True)
    u.grantSelect(tb_desti, WATCHERS, DB_DESTI)
    sql = """
        SELECT
            codi_sector, id_cip_sec, {cols}
        FROM
            {tb}
        where
            pr_cod_ps in {cods}
        {limit}
        """.format(
                cols=", ".join(cols),
                tb=tb,
                cods=tuple(ps_cods),
                limit=limit if LIMIT else '')
    for row in u.getAll(sql, db):
        cip = row[1]
        if cip in cips:
            dades.append(row)
    print("{}: ".format(tb), len(dades), coldef)
    return dades, tb_desti


def get_visites():
    LIMIT = False
    limit = "LIMIT 1000"
    dades = []
    coldef = []
    db, tb = "import", "visites"
    tb_desti = 'fmundi_{}'.format(tb)
    cols = visi_cols
    for col in ['codi_sector', 'id_cip_sec'] + cols:
        coldef.append(
            u.getColumnInformation(col, tb, db, desti=DB_DESTI)['create'])
    u.createTable(
        tb_desti,
        "({})".format(", ".join(coldef)),
        "redics",
        rm=True)
    u.grantSelect(tb_desti, WATCHERS, DB_DESTI)
    since = u.getOne("select data_ext from dextraccio", "nodrizas")[0].year - 2018  # noqa
    parts = get_partition(db, tb, "visi_data_visita", "{} year".format(since))
    up_assir = [row for row, in u.getAll("select up from cat_centres","assir")]  # noqa
    for part in parts:
        sql = """
            SELECT
                codi_sector, id_cip_sec, {cols}
            FROM
                {tb}
            where
                visi_situacio_visita = 'R'
                AND visi_up in {cods}
            {limit}
            """.format(
                    cols=", ".join(cols),
                    tb=part,
                    cods=tuple(up_assir),
                    limit=limit if LIMIT else '')
        for row in u.getAll(sql, db):
            cip = row[1]
            # up = row[2]
            dat = row[-1]
            if cip in cips:
                ini = min(cip_embs.get(cip).get('ini'))
                fi = max(cip_embs.get(cip).get('fi')) + d.timedelta(days=60)
                if ini <= dat <= fi:
                    dades.append(row)
    print("{}: ".format(tb), len(dades), coldef)
    return dades, tb_desti


def get_xml():
    dades = []
    coldef = []
    db = "import"
    tb = "xml"
    tb_desti = 'fmundi_{}'.format("ecos")
    cods = ["ECO_OBS_T1", "ECO_OBS_T2", "ECO_OBS_T3"]
    cols = ["xml_tipus_orig", "xml_data_alta"]
    for col in ['codi_sector', 'id_cip_sec'] + cols:
        coldef.append(
            u.getColumnInformation(col, tb, db, desti=DB_DESTI)['create'])
    u.createTable(
        tb_desti,
        "({})".format(", ".join(coldef)),
        "redics",
        rm=True)
    u.grantSelect(tb_desti, WATCHERS, DB_DESTI)
    sql = """
        SELECT
            codi_sector, id_cip_sec, {cols}
        FROM
            {tb}
        where
            xml_tipus_orig in {cods}
    """.format(cols=", ".join(cols), tb=tb, cods=tuple(cods))
    for row in u.getAll(sql, db):
        cip = row[1]
        dat = row[-1]
        if cip in cips:
            ini = min(cip_embs.get(cip).get('ini'))
            fi = max(cip_embs.get(cip).get('fi'))
            if ini <= dat <= fi:
                dades.append(row)
    print("{}: ".format(tb), len(dades), coldef)
    return dades, tb_desti


def get_grupal():
    db = "import"
    tb = "grupal4"
    tb_desti = 'fmundi_{}'.format("grups")
    cols = [
        'grup_codi_up',
        'grup_data_ini',
        'grup_data_fi',
        'grup_titol',
        'grup_cod_o_ps',
        'grup_cod_ps',
        ]
    coldef = ['codi_sector varchar2(4)', 'id_cip_sec number(10)']

    dades = []

    sql_g4 = """
        SELECT
            codi_sector, grup_num,
            {cols}
        FROM
            {tb}
        WHERE
            extract(year from grup_data_ini) >= 2018
    """.format(cols=", ".join(cols), tb=tb)
    sql_g3 = "select id_cip_sec, codi_sector, pagr_num_grup from grupal3"
    grups = {}
    for row in u.getAll(sql_g4, db):
        grups[(row[0], row[1])] = row[2:]
    for cip, sec, grup in u.getAll(sql_g3, db):
        if cip in cips:
            if (sec, grup) in grups:
                up, dini, dfi, titol, cod_o, cod_ps = grups[(sec, grup)]
                dades.append((sec, cip, up, dini, dfi, titol, cod_o, cod_ps))

    for col in cols:
        coldef.append(
            u.getColumnInformation(col, tb, db, desti=DB_DESTI)['create'])

    u.createTable(
        tb_desti,
        "({})".format(", ".join(coldef)),
        "redics",
        rm=True)
    u.listToTable(dades, tb_desti, DB_DESTI)
    u.grantSelect(tb_desti, WATCHERS, DB_DESTI)

    nd = [row for row, in u.getAll(CNT_SQL.format(tb_desti), DB_DESTI)][0]
    print("{}: ori ({}), dest ({})".format(tb_desti, len(dades), nd))


def get_fdg():
    db = "import"
    tb = "nen11"
    tb_desti = 'fmundi_{}'.format("fdg")
    cols = ["val_var", "val_val", "val_data"]

    dades = []
    coldef = []

    sql = """
        SELECT
            codi_sector, id_cip_sec, {cols}
        FROM
            {tb}
        where
            val_var = '{cod}' AND
            val_hist = 1
    """.format(cols=", ".join(cols), tb=tb, cod='FDGDC001')

    for row in u.getAll(sql, db):
        cip = row[1]
        if cip in cips:
            dades.append(row)

    for col in ['codi_sector', 'id_cip_sec'] + cols:
        coldef.append(
            u.getColumnInformation(col, tb, db, desti=DB_DESTI)['create'])

    u.createTable(
        tb_desti,
        "({})".format(", ".join(coldef)),
        "redics",
        rm=True)
    u.listToTable(dades, tb_desti, DB_DESTI)
    u.grantSelect(tb_desti, WATCHERS, DB_DESTI)

    nd = [row for row, in u.getAll(CNT_SQL.format(tb_desti), DB_DESTI)][0]
    print("{}: ori ({}), dest ({})".format(tb_desti, len(dades), nd))


def get_vassir():  # 5k segundos, OJO!!!
    LIMIT = False
    limit = "limit 1000"

    db = "import"
    tb = "assir216"
    tb_desti = 'fmundi_{}'.format("vassir")
    cols = ["val_cod", "val_var", "val_val", "val_data"]

    coldef = []
    dades = []

    for part in u.sectors:
        sql = """
            SELECT
                codi_sector, id_cip_sec, {cols}
            FROM {tb} {limit}
        """.format(
                cols=", ".join(cols),
                tb="_s".join((tb, part)),
                limit=limit if LIMIT else ''
                )
        for row in u.getAll(sql, db):
            cip = row[1]
            dat = row[-1]
            if cip in cips and isinstance(dat, d.date):
                ini = min(cip_embs.get(cip).get('ini'))
                fi = max(cip_embs.get(cip).get('fi')) + d.timedelta(days=60)
                if ini <= dat <= fi:
                    dades.append(row)

    for col in ['codi_sector', 'id_cip_sec'] + cols:
        coldef.append(
            u.getColumnInformation(col, tb, db, desti=DB_DESTI)['create'])

    u.createTable(
        tb_desti,
        "({})".format(", ".join(coldef)),
        "redics",
        rm=True)
    u.listToTable(dades, tb_desti, DB_DESTI)
    u.grantSelect(tb_desti, WATCHERS, DB_DESTI)

    nd = [row for row, in u.getAll(CNT_SQL.format(tb_desti), DB_DESTI)][0]
    print("{}: ori ({}), dest ({})".format(tb_desti, len(dades), nd))


# fmundi_embaras
emb_cods, cips, cip_embs = get_embaras()

# fmundi_seguiment
seg_dades, seg_desti = get_seguiment()
u.listToTable(seg_dades, seg_desti, "redics")
[row for row, in u.getAll(CNT_SQL.format(seg_desti), 'redics')]

# fmundi_usuaris
usu_dades, usu_desti = get_usuaris()
u.listToTable(usu_dades, usu_desti, "redics")
[row for row, in u.getAll(CNT_SQL.format(usu_desti), 'redics')]

# fmundi_variables
var_dades, var_desti = get_variables()
u.listToTable(var_dades, var_desti, "redics")
[row for row, in u.getAll(CNT_SQL.format(var_desti), 'redics')]

# fmundi_problemes
ps_dades, ps_desti = get_problemes(ps_cods)
u.listToTable(ps_dades, ps_desti, "redics")
[row for row, in u.getAll(CNT_SQL.format(ps_desti), 'redics')]

# fmundi_visites
visi_dades, visi_desti = get_visites()
u.listToTable(visi_dades, visi_desti, "redics")
[row for row, in u.getAll(CNT_SQL.format(visi_desti), 'redics')]

# fmundi_ecos
xml_dades, xml_desti = get_xml()
u.listToTable(xml_dades, xml_desti, "redics")
[row for row, in u.getAll(CNT_SQL.format(xml_desti), 'redics')]

# fmundi_grups
get_grupal()

# fmundi_fdg
get_fdg()

# fmundi_vassir
vassir_dades, vassir_desti = get_vassir()
u.listToTable(vassir_dades, vassir_desti, "redics")
[row for row, in u.getAll(CNT_SQL.format(vassir_desti), 'redics')]
