with
    dext as (select date_add(data_ext, interval -1 year) as dini, data_ext as dfi from nodrizas.dextraccio),
    darrera_multi as (
        select
            id_cip_sec cip, xml_data_alta, xml_tipus_orig as tipus_orig
        from
            import.xml
        where
            xml_tipus_orig in (
                'VAL_SPS_AD',
                'VAL_SPS_NE',
                'VAL_SPS_TN'
            )
         group by id_cip_sec, xml_data_alta, tipus_orig
         ),
    darrera_dat as (
        select cip, max(xml_data_alta) as max_data
        from
            darrera_multi, dext
        where
            xml_data_alta between dext.dini and dext.dfi
        group by
            cip
        ),
    darrera as (
        select d.cip, d.max_data, m.tipus_orig
        from
            darrera_dat d inner JOIN
            darrera_multi m ON
                d.cip = m.cip AND
                d.max_data = m.xml_data_alta
    )
select distinct darrera.cip, max_data
from
    darrera
where
    exists (
        select 1
        from import.xml_detall detall
        where 
            darrera.cip = detall.id_cip_sec	AND
            darrera.max_data = detall.xml_data_alta	AND
            darrera.tipus_orig = detall.xml_tipus_orig AND
            camp_codi in (
                'Xarxa_Result',
                'Habitatge_Result',
                'Economia_Result',
                'Capacitat_Result'
            ) and
            substr(camp_valor, 1, 15) = 'Valorat amb alt'
    )