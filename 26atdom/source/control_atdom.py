# coding: utf8

"""
Afegim indicadors a la taula de control. En aquest cas els indicadors de la carpeta atdom
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u
import sisaptools as t

db = ('shiny', 'ladybug')
tb = 'mst_control_eqa'
# tb = 'prova_ladybug'
tb_cat = 'cat_control_indicadors'
db_origen = "atdom"
csv = 'cataleg_atdom.csv'
"""Taules origen"""
# POBACTUAL A DIM6SET
# exp_khalix_tests_atdom - ind, entity, analysys, valor (agafar POBACTUAL)
# exp_khalix_nafres_resi_atdom - ind, entity, analysys, valor (agafar POBACTUAL)
# exp_khalix_gida_atdom - ind, ent, analisi, valor
# exp_khalix_CONT_atdom - ind, entity, analysys, valor (agafar POBACTUAL)
# exp_khalix_EQA_atdom - ind, entity, analysys, valor (agafar POBACTUAL)
# exp_khalix_LONG_atdom - ind, entity, analysys, valor (agafar POBACTUAL)

cols = "(periode int, sector varchar(4), ambit varchar(50), indicador varchar(20), resolts int, n int)"
u.createTable('prova_ladybug', cols, 'atdom', rm=True)

tables = {
    'exp_khalix_tests_atdom': 'ind, entity, analysys, valor',
    'exp_khalix_nafres_resi_atdom': 'ind, entity, analysys, valor',
    'exp_khalix_gida_atdom': 'ind, ent, analisi, valor',
    'exp_khalix_CONT_atdom': 'ind, entity, analysys, valor',
    'exp_khalix_EQA_atdom': 'ind, entity, analysys, valor',
    'exp_khalix_LONG_atdom': 'ind, entity, analysys, valor',
    'hospitalitzacions_atdom': 'indi, up, analisi, val'
}


periode,= u.getOne("select date_format(data_ext,'%Y%m') from dextraccio", 'nodrizas')

class LadyBug_atdom(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """."""
        self.indicadors, self.resultats, self.cataleg = {}, Counter(), {}
        self.get_territoris()
        self.get_ind()
        self.upload_cat()
        self.export_data()
        
    def upload_cat(self):
        cataleg = {}
        path = './dades_noesb/'
        print(path+csv)
        for (indicador, desc) in u.readCSV(path + csv, sep=';'):
            cataleg[indicador] = desc
        sql = "delete from {tb} where indicador = '{ind}'"
        for ind in cataleg:
            t.Database(*db).execute(sql.format(tb=tb_cat, ind=ind))
        upload = []
        pare = 'QC ATDOM'
        for ind, desc in cataleg.items():
            upload.append((ind, desc, pare))
        t.Database(*db).list_to_table(upload, tb_cat)

    def get_ind(self):
        """."""
          
        sql = "select {camps} from {table} {cond}"
        for tb in tables:
            if tb in ('exp_khalix_gida_atdom', 'hospitalitzacions_atdom'):
                cond = ''
            else:
                cond = "WHERE dim6set = 'POBACTUAL'"
            print(sql.format(camps=tables[tb], table=tb, cond=cond))
            try:
                for indicador, up, analisi, n in u.getAll(sql.format(camps=tables[tb], table=tb, cond=cond), db_origen):
                    self.indicadors[indicador] = True
                    try:
                        ambit = self.centresSCS[up]['ambit']
                        sector = self.centresSCS[up]['sector']
                    except KeyError:
                        continue
                    if analisi == 'DEN':
                        self.resultats[periode, sector, ambit, indicador, 'den'] += n
                    if analisi == 'NUM':
                        self.resultats[periode, sector, ambit, indicador, 'num'] += n
            except Exception as e:
                print(e)
                continue

    def get_territoris(self):
        """Obtenim ambits i sector"""
        self.centres, self.centresSCS = {}, {}
        sql = 'select ics_codi, scs_codi, amb_desc, sector from cat_centres'
        for br, up, amb, sector in u.getAll(sql, 'nodrizas'):
            self.centres[br] = {'sector': sector, 'ambit': amb}
            self.centresSCS[up] = {'sector': sector, 'ambit': amb}
    
    def export_data(self):
        """."""
        upload = []
        for (period, sector, ambit, ind, tipus), n in self.resultats.items():
            if tipus == 'den':
                resolts = self.resultats[period, sector, ambit, ind, 'num']
                upload.append([int(period), sector, ambit, ind, resolts, n])
        
        indi = []
        for codi, n in self.indicadors.items():
            indi.append(codi)
            
        indicadors = tuple(indi)
        try:
            sql = "delete a.* from {0} a where periode = {1} and indicador in {2}".format(tb, periode, indicadors)
            print(sql)
            # u.execute(sql, 'atdom')
            # u.listToTable(upload, tb, 'atdom')
            t.Database(*db).execute(sql)
        except Exception as e:
            print(e)
            pass
        t.Database(*db).list_to_table(upload, tb)
        
  
if __name__ == '__main__':
    LadyBug_atdom()
    