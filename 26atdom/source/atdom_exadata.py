import sisapUtils as u
import sisaptools as su
import datetime as d
import collections as c
import shared as s
u.printTime('inici')
DEXTD = u.getOne("select data_ext from dextraccio", "nodrizas")[0]
sql = """
    SELECT C_CIP, C_EDAT_ANYS, C_SEXE, C_UP, C_METGE, C_INFERMERA,
    PS_ATDOM_DATA, C_INSTITUCIONALITZAT, PS_DISCAPACITAT_DATA,
    VC_DEPENDENCIA_DATA, VC_DEPENDENCIA_VALOR, VC_GRAU_DISCAPACITAT_DATA,
    VC_GRAU_DISCAPACITAT_VALOR, V_BARTHEL_DATA, V_BARTHEL_VALOR,
    V_PFEIFER_DATA, V_PFEIFER_VALOR, V_TIRS_DATA, V_TIRS_VALOR,
    PR_PCC_DATA, PR_MACA_DATA, PS_NEOPLASIA_M_DATA, PS_CURES_PALIATIVES_DATA
    FROM dwsisap.DBS
"""
pob = {}
for row in u.getAll(sql, 'exadata'):
    pob[row[0]] = row[1:]
u.printTime('get_pob')
inds = ['EQA0401', 'EQA0402', 'EQA0403', 'EQA0404']
eqas = c.defaultdict(set)
sql = """
    SELECT id_cip_sec
    FROM {tb}
    WHERE num = 1
"""
for ind in inds:
    tb = ind + "a_ind2"
    for id, in u.getAll(sql.format(tb=tb), 'eqa_ind'):
        eqas[ind].add(id)
    u.printTime(ind)
u.printTime('get_eqas')
sql = """
select
    id_cip_sec,
    case left(visi_col_prov_resp, 1)
        when 1 then 'D_MED'
        when 3 then 'D_INF' end
from
    visites1 v
where
    visi_situacio_visita = 'R'
    and (visi_tipus_visita = '9D' or visi_lloc_visita = 'D')
    and visi_data_visita between
        adddate(DATE '{DEXTD}', interval -1 year)
        and DATE '{DEXTD}'
    and left(visi_col_prov_resp, 1) in ('1', '3', '4')
""".format(DEXTD=DEXTD)
visites = c.Counter()
for id, resp in u.getAll(sql, 'import'):
    visites[(id, resp)] += 1
u.printTime('get_visites')
sql = "select id_cip_sec, hash_d from u11"
id_to_hash = {}
for id, hash in u.getAll(sql, 'import'):
    id_to_hash[hash] = id
u.printTime('get_u11')
sql = "select hash_redics, hash_covid from pdptb101_relacio where hash_redics like '{}%'"
conversor = {}
for redics, covid in s.get_data(sql, model="redics", redics="pdp"):
    try:
        conversor[covid] = id_to_hash[redics]
    except Exception as e:
        continue
u.printTime('get_pdptb101')
upload = []
for hash in pob:
    edat = pob[hash][0]
    sexe = pob[hash][1]
    up = pob[hash][2]
    metge = pob[hash][3]
    inf = pob[hash][4]
    atdom = pob[hash][5]
    inst = pob[hash][6]
    disc = pob[hash][7]
    dep_data = pob[hash][8]
    dep_val = pob[hash][9]
    grau_data = pob[hash][10]
    grau_val = pob[hash][11]
    bar_data = pob[hash][12]
    bar_val = pob[hash][13]
    pfeif_data = pob[hash][14]
    pfeif_val = pob[hash][15]
    tirs_data = pob[hash][16]
    tirs_val = pob[hash][17]
    pcc = pob[hash][18]
    maca = pob[hash][19]
    neo = pob[hash][20]
    cures = pob[hash][21]
    if hash in conversor:
        id = conversor[hash]
        atd01 = visites[(id, 'D_INF')] if (id, 'D_INF') in visites else 0
        atd05 = visites[(id, 'D_MED')] if (id, 'D_MED') in visites else 0
        eqa0401 = 1 if id in eqas['EQA0401'] else 0
        eqa0402 = 1 if id in eqas['EQA0402'] else 0
        eqa0403 = 1 if id in eqas['EQA0403'] else 0
        eqa0404 = 1 if id in eqas['EQA0404'] else 0
        upload.append((hash, edat, sexe, up, metge, inf, atdom, inst, disc, dep_data, dep_val, grau_data, grau_val, bar_data, bar_val, pfeif_data, pfeif_val, tirs_data, tirs_val, pcc, maca, neo, cures, atd01, atd05, eqa0401, eqa0402, eqa0403, eqa0404))
    else:
        upload.append((hash, edat, sexe, up, metge, inf, atdom, inst, disc, dep_data, dep_val, grau_data, grau_val, bar_data, bar_val, pfeif_data, pfeif_val, tirs_data, tirs_val, pcc, maca, neo, cures, 0, 0, 0, 0, 0, 0))
u.printTime('upload')
cols = """(C_CIP varchar2(40), C_EDAT_ANY number, C_SEXE varchar2(1), C_UP varchar2(5), C_METGE varchar2(5), C_INFERMERA varchar2(5),
            PS_ATDOM_DATA date, C_INSTITUCIONALITZAT varchar2(13), PS_DISCAPACITAT_DATA date, VC_DEPENDENCIA_DATA date,
            VC_DEPENDENCIA_VALOR varchar2(255), VC_GRAU_DISCAPACITAT_DATA date, VC_GRAU_DISCAPACITAT_VALOR varchar2(255),
            V_BARTHEL_DATA date, V_BARTHEL_VALOR number, V_PFEIFER_DATA date, V_PFEIFER_VALOR number,
            V_TIRS_DATA date, V_TIRS_VALOR number, PR_PCC_DATA date, PR_MACA_DATA date,
            PS_NEOPLASIA_M_DATA date, PS_CURES_PALIATIVES_DATA date, ATD001AD number, ATD005AD number,
            EQA0401 number, EQA0402 number, EQA0403 number, EQA0404 number)"""
u.createTable('DBS_ATDOM', cols, 'exadata', rm=True)
u.listToTable(upload, 'DBS_ATDOM', 'exadata')
u.grantSelect('DBS_ATDOM', ("DWSISAP_ROL"), 'exadata')
u.printTime('fi')

