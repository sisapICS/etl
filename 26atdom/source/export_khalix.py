# -*- coding: utf-8 -*-
#fitxer per exportar totes les dades nom�s en 2 exportkhalix, depenent de si agrupa per UP o per UBA

import datetime as d
import collections as c

import sisaptools as u  # t
import sisapUtils as su  # u
import shared as s

TEST = False
RECOVER = False
DATABASE = 'atdom'
DEXTD = d.date(2021, 8, 31) if RECOVER else u.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]  # noqa
DEXT = str(DEXTD)
MENSUAL = su.IS_MENSUAL

sql_up = """
SELECT ind, 'Aperiodo' as periode, b.ics_codi as resi, analysys, 'NOCAT', 'NOIMP', dim6set, 'N', valor FROM atdom.exp_khalix_tests_atdom a, nodrizas.cat_centres b WHERE a.entity = b.scs_codi and a.valor <> 0
UNION
SELECT ind, 'Aperiodo', b.ics_codi as resi, analysys, 'NOCAT', detail, dim6set, 'N', valor FROM atdom.exp_khalix_nafres_resi_atdom a, nodrizas.cat_centres b WHERE a.entity = b.scs_codi and a.valor <> 0
UNION
select ind, 'Aperiodo', ent, analisi, motiu, 'NOIMP', 'DIM6SET', 'N', valor FROM atdom.exp_khalix_gida_atdom where length(ent) = 5 and valor > 0 and motiu not in ('PRAOI', 'PRAOPED', 'PRAOAP', 'PACEAP', 'PACEI')
UNION 
select ind, 'Aperiodo', ent, analisi, 'PRAOPED' motiu, 'NOIMP', 'DIM6SET', 'N', valor FROM atdom.exp_khalix_gida_atdom where length(ent) = 5 and valor > 0 and motiu in ('PRAOI', 'PRAOPED', 'PRAOAP') group by ent, ind, analisi
UNION
select ind, 'Aperiodo', ent, analisi, 'PACEI' motiu, 'NOIMP', 'DIM6SET', 'N', valor FROM atdom.exp_khalix_gida_atdom where length(ent) = 5 and valor > 0 and motiu in ('PACEAP', 'PACEI') group by ent, ind, analisi
UNION
SELECT ind, 'Aperiodo', b.ics_codi as resi, analysys, 'NOCAT', 'NOIMP', dim6set, 'N', valor FROM atdom.exp_khalix_EQA_atdom a, nodrizas.cat_centres b WHERE a.entity = b.scs_codi and a.valor <> 0 and length(entity) = 5
UNION
SELECT indi, 'Aperiodo', b.ics_codi as resi, analisi, 'NOCAT', 'NOIMP', 'POBACTUAL', 'N', val FROM atdom.hospitalitzacions_atdom a, nodrizas.cat_centres b WHERE a.up = b.scs_codi and a.val <> 0
"""

if MENSUAL:
    sql_up += """
        UNION
        SELECT ind, 'Aperiodo', b.ics_codi as resi, analysys, 'NOCAT', detail, dim6set, 'N', valor FROM atdom.exp_khalix_CONT_atdom a, nodrizas.cat_centres b WHERE a.entity = b.scs_codi and a.valor <> 0 and length(entity) = 5
    """
sql_uba = """
select a.indicador, concat('A','periodo') as periode, concat(concat(c.ics_codi, a.tipus), a.uba) as entity, a.analisi, 'NOCAT', 'NOIMP', a.dim6set, 'N', a.n from atdom.EXP_KHALIX_QC_ATDOM_UBA a inner join nodrizas.cat_centres c on a.up = c.scs_codi
UNION
select ind, 'Aperiodo', ent, analisi, motiu, 'NOIMP', 'DIM6SET', 'N', valor FROM atdom.exp_khalix_gida_atdom where length(ent) > 5 and valor > 0 and motiu not in ('PRAOI', 'PRAOPED', 'PRAOAP', 'PACEAP', 'PACEI')
UNION 
select ind, 'Aperiodo', ent, analisi, 'PRAOPED' motiu, 'NOIMP', 'DIM6SET', 'N', valor FROM atdom.exp_khalix_gida_atdom where length(ent) > 5 and valor > 0 and motiu in ('PRAOI', 'PRAOPED', 'PRAOAP') group by ent, ind, analisi
UNION
select ind, 'Aperiodo', ent, analisi, 'PACEI' motiu, 'NOIMP', 'DIM6SET', 'N', valor FROM atdom.exp_khalix_gida_atdom where length(ent) > 5 and valor > 0 and motiu in ('PACEAP', 'PACEI') group by ent, ind, analisi
UNION 
SELECT ind, 'Aperiodo', entity, analysys, 'NOCAT', 'NOIMP', dim6set, 'N', valor FROM atdom.exp_khalix_EQA_atdom WHERE valor <> 0 and length(entity) > 5
UNION
SELECT indi, 'Aperiodo', concat(concat(b.ics_codi, a.tipus), a.uba), a.analisi, 'NOCAT', 'NOIMP', 'POBACTUAL', 'N', val FROM atdom.hospitalitzacions_atdom_uba a, nodrizas.cat_centres b WHERE a.up = b.scs_codi and a.val <> 0
"""

if MENSUAL:
    sql_uba += """
        UNION 
        SELECT ind, 'Aperiodo', entity, analysys, 'NOCAT', detail, dim6set, 'N', valor FROM atdom.exp_khalix_CONT_atdom WHERE valor <> 0 and length(entity) > 5
    """

sql_long_up = """
SELECT ind, period, b.ics_codi as resi, analysys, 'NOCAT', detail, dim6set, 'N', valor FROM atdom.exp_khalix_LONG_atdom a, nodrizas.cat_centres b WHERE a.entity = b.scs_codi and a.valor <> 0 and length(entity) = 5
"""
sql_long_uba = """
SELECT ind, period, entity, analysys, 'NOCAT', detail, dim6set, 'N', valor FROM atdom.exp_khalix_LONG_atdom  WHERE valor <> 0 and length(entity) > 5
"""
file_up = "QCATD"
file_uba = "QCATD_UBA"

long_up = 'QCATD_LONG'
long_uba = 'QCATD_LONG_UBA'

su.exportKhalix(sql_up, file_up)
su.exportKhalix(sql_uba, file_uba)
su.exportKhalix(sql_long_up, long_up)
su.exportKhalix(sql_long_uba, long_uba)