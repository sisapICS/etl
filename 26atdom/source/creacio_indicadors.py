# -*- coding: utf-8 -*-

import hashlib as h
import datetime as d
import collections as c
from dateutil.relativedelta import relativedelta

import sisaptools as t
import sisapUtils as u
import shared as s

DEXTD = t.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]  # noqa
DEXT = str(DEXTD)
now_1m = DEXTD - relativedelta(months=1)
now_1y = DEXTD - relativedelta(years=1)
menys30 = DEXTD - relativedelta(days=30)

u.createTable(
    'exp_ecap_uba_hist',
    "(up varchar(5), \
      uba varchar(10), \
      tipus varchar(1), \
      indicador varchar(8), \
      numerador int, \
      denominador int, \
      resultat double, \
      noresolts int)",
    'atdom',
    rm=True
)

u.createTable(
    'pacients_atdom',
    "(id_cip_sec int, \
    indicador varchar(15), \
    compleix int)",
    'atdom',
    rm=True
)

ATDOM = [
    'ATD026A',
    'ATD039A',
    'ATD037A',
    'ATD025A',
    'EQA0402ATD',
    'EQA0403ATD',
    'EQA0404ATD',
    'ATD073A',
    'ATD047A',
    'ATD043A2',
    'ATD051A',
    'ATD049A',
    'ATD050A',
    'ATD052A',
    'ATD046A',
    'ATD053A',
    'ATD043A1',
    'ATD048A',
    'ATD023A',
    'ATD024A'
]

def sub_get_master(sql):
    # partition, sql = params
    dades = c.defaultdict(dict)
    for id, antipsico, benzo, iace_memantina, antidepressius, ansio_hipnotics, estatines, aines, pcc, maca, piic, urinaria, maltracte, osteoporosi, demencia, depre, ci, avc, claud_int, irc, insuf_hepat, antiosteop, DMO, gds_fast, npi_q, gds7b, edat, registre_caig, braden, mna, barthel, imc, pes, max_pes, pfeifer, caigudes, polif, psicof, MEDyear, MEDmonth, INFyear, INFmonth, insomni, pcc_maca, servei_mg, servei_inf, parla, TSyear, TSmonth, D_MEDyear, D_MEDmonth, D_INFyear, D_INFmonth, D_TSyear, D_TSmonth, val_soc_alterada, placures, socialrisk, fragilitat, antidepressius_2, ansio_hipnotics_2, barthel_frag, pfeifer_frag, mna_frag, caigudes_frag, emocional, social in u.getAll(sql, 'resis'):
        if antipsico:
            dades['antipsico'][id] = antipsico
        if benzo:
            dades['benzo'][id] = benzo
        if iace_memantina:
            dades['iace_memantina'][id] = iace_memantina
        if antidepressius:
            dades['antidepressius'][id] = antidepressius
        if ansio_hipnotics:
            dades['ansio_hipnotics'][id] = ansio_hipnotics
        if estatines:
            dades['estatines'][id] = estatines
        if aines:
            dades['aines'][id] = aines
        if edat:
            dades['edat'][id] = edat
        if barthel != -1:
            dades['barthel'][id] = barthel
        if imc:
            dades['imc'][id] = imc
        if pes:
            dades['pes'][id] = pes
        if max_pes:
            dades['max_pes'][id] = max_pes
        if pfeifer != -1:
            dades['pfeifer'][id] = pfeifer
        if caigudes != -1:
            dades['caigudes'][id] = caigudes
        if polif:
            dades['polif'][id] = polif
        if psicof:
            dades['psicof'][id] = psicof
        if MEDyear:
            dades['MEDyear'][id] = MEDyear
        if MEDmonth:
            dades['MEDmonth'][id] = MEDmonth
        if INFyear:
            dades['INFyear'][id] = INFyear
        if INFmonth:
            dades['INFmonth'][id] = INFmonth
        if TSyear:
            dades['TSyear'][id] = TSyear
        if TSmonth:
            dades['TSmonth'][id] = TSmonth
        if D_MEDyear:
            dades['D_MEDyear'][id] = D_MEDyear
        if D_MEDmonth:
            dades['D_MEDmonth'][id] = D_MEDmonth
        if D_INFyear:
            dades['D_INFyear'][id] = D_INFyear
        if D_INFmonth:
            dades['D_INFmonth'][id] = D_INFmonth
        if D_TSyear:
            dades['D_TSyear'][id] = D_TSyear
        if D_TSmonth:
            dades['D_TSmonth'][id] = D_TSmonth
        if servei_mg != 0:
            dades['MG'][id] = servei_mg
        if servei_inf != 0:
            dades['INF'][id] = servei_inf
        if pcc:
            dades['pcc'][id] = pcc
        if maca:
            dades['maca'][id] = maca
        if piic:
            dades['piic'][id] = piic
        if urinaria:
            dades['urinaria'][id] = urinaria
        if maltracte:
            dades['maltracte'][id] = maltracte
        if osteoporosi:
            dades['osteoporosi'][id] = osteoporosi
        if demencia:
            dades['demencia'][id] = demencia
        if depre:
            dades['depre'][id] = depre
        if ci:
            dades['ci'][id] = ci
        if avc:
            dades['avc'][id] = avc
        if claud_int:
            dades['claud_int'][id] = claud_int
        if irc:
            dades['irc'][id] = irc
        if insuf_hepat:
            dades['insuf_hepat'][id] = insuf_hepat
        if antiosteop:
            dades['antiosteop'][id] = antiosteop
        if DMO:
            dades['DMO'][id] = DMO
        if gds_fast:
            dades['gds_fast'][id] = gds_fast
        if npi_q:
            dades['npi_q'][id] = npi_q
        if gds7b:
            dades['gds7b'][id] = gds7b
        if registre_caig:
            dades['registre_caig'][id] = registre_caig
        if braden:
            dades['braden'][id] = braden
        if mna:
            dades['mna'][id] = mna
        if insomni:
            dades['insomni'][id] = insomni
        if pcc_maca:
            dades['pcc_maca'][id] = pcc_maca
        if parla == 0: #exclusio de ATD027A
            dades['parla'][id] = parla
        if val_soc_alterada:
            dades['val_soc_alterada'][id] = val_soc_alterada
        if placures:
            dades['placures'][id] = placures
        if socialrisk:
            dades['socialrisk'][id] = 1
        if fragilitat:
            dades['fragilitat'][id] = 1
        if antidepressius_2 > 1:
            dades['antidepressius_2'][id] = antidepressius_2
        if ansio_hipnotics_2 > 1:
            dades['ansio_hipnotics_2'][id] = ansio_hipnotics_2
        if barthel_frag != -1 and (pfeifer_frag or demencia or parla) and mna_frag and caigudes_frag and (emocional or demencia) and social and edat and edat >= 15:
            dades['valoracio'][id] = 1
        if fragilitat and edat and edat >= 15:
            dades['valoracio'][id] = 1
        if barthel_frag != -1 and social and edat and edat < 15:
            dades['valoracio'][id] = 1
    return dades

class Master():
    def __init__(self):
        u.printTime('inici')
        self.dades = c.defaultdict(dict)
        self.upload_cataleg()
        u.printTime('upload_cataleg')
        self.get_conversor()
        u.printTime('get_conversor')
        self.get_atdom()
        u.printTime('get_atdom')
        self.get_u11()
        u.printTime('get_u11')
        self.get_atdom_ass()
        u.printTime('get_atdom_ass')
        self.get_centres()
        u.printTime('get_centres')
        self.get_ubas()
        u.printTime('get_ubas')
        self.get_master()
        u.printTime('get_master')
        self.treat_data()
        u.printTime('treat_data')
        self.get_indicadors()
        u.printTime('get_indicadors')
        self.get_longit_dades()
        u.printTime('get_longit_dades')
        if u.IS_MENSUAL:
            self.get_cont()
            u.printTime('get_cont')
        self.get_eqa()
        u.printTime('get_eqa')
    
    def get_eqa(self):
        """
        pilla indicadores de EQA que ya se calculan para ATDOMs
        """
        db = "import"
        sch = "eqa_ind"
        tbs = ["eqa0402a_ind2", "eqa0403a_ind2", "eqa0404a_ind2"]
        dim6set = 'POBACTUAL'
        pobs = {'EQA_atdom': self.atdom_ass}

        sql_ = """
            select
                id_cip_sec,
                concat(ind_codi, 'TD') as ind,
                up, uba,
                upinf, ubainf,
                num
            from
                {sch}.{tb}
            where
                ates = 1 and
                institucionalitzat = 0 and
                excl = 0 and
                ci = 0 and
                clin = 0
        """

        sql = "UNION ".join(sql_.format(sch=sch, tb=tb) for tb in tbs)
        llistats = []
        pacients = []
        ll_indicadors = c.defaultdict(c.Counter)
        for file_name, pob_d in pobs.items():
            self.eqa_dades = c.Counter()
            for id, ind, up, uba, upinf, ubainf, num in u.getAll(sql, db):
                if id in pob_d:
                    resi = pob_d[id]['UP']
                    br = self.centres[up]
                    entities = [resi]
                    entities.extend([br + entity[2] + entity[1]
                                    for entity
                                    in ((up, uba, "M"),
                                        (upinf, ubainf, "I"))
                                    if entity in self.ubas])
                    for entity in entities:
                        pacients.append((id, ind, 1))
                        self.eqa_dades[(ind, entity, "NUM", dim6set)] += num  # noqa
                        self.eqa_dades[(ind, entity, "DEN", dim6set)] += 1  # noqa
                    if id in self.ids:
                        idpac = self.ids[id][0]
                        sector = self.ids[id][1]
                        if (up, uba, 'M') in self.ubas:
                            llistats.append((up, uba, 'M', ind, idpac, sector, 0, None))
                            ll_indicadors[(DEXTD.year, DEXTD.month, up, uba, 'M', ind)]['num'] += num
                            ll_indicadors[(DEXTD.year, DEXTD.month, up, uba, 'M', ind)]['den'] += 1
                        if (upinf, ubainf, 'I') in self.ubas:
                            llistats.append((upinf, ubainf, 'I', ind, idpac, sector, 0, None))
                            ll_indicadors[(DEXTD.year, DEXTD.month, upinf, ubainf, 'I', ind)]['num'] += num
                            ll_indicadors[(DEXTD.year, DEXTD.month, upinf, ubainf, 'I', ind)]['den'] += 1
                    

            eqa_dades_ls = []
            for k, v in self.eqa_dades.items():
                kl = list(k)
                kl.append(v)
                eqa_dades_ls.append(tuple(kl))
            cols = "(ind varchar(15), entity varchar(13), analysys varchar(3), dim6set varchar(10), valor int)"
            u.createTable('exp_khalix_eqa_atdom', cols, 'atdom', rm=True)
            u.listToTable(eqa_dades_ls, 'exp_khalix_eqa_atdom', 'atdom')
            u.listToTable(pacients, 'pacients_atdom', 'atdom')
            
            u.listToTable(llistats, 'ATDOMLLISTATS', 'pdp')
            upload = []
            for (year, month, up, uba, tipus, ind) in ll_indicadors:
                num = ll_indicadors[(year, month, up, uba, tipus, ind)]['num'] if 'num' in ll_indicadors[(year, month, up, uba, tipus, ind)] else 0
                den = ll_indicadors[(year, month, up, uba, tipus, ind)]['den'] if 'den' in ll_indicadors[(year, month, up, uba, tipus, ind)] else 0
                upload.append((year, month, up, uba, tipus, ind, num, den, float(num)/float(den), None))
            u.listToTable(upload, 'ATDOMINDICADORS', 'pdp')

    def upload_cataleg(self):
        cataleg_pare = [('ATD', 'Indicadors ATDOM', 10),]
        cols = '(indicador varchar(20), descripcio varchar(50), ordre int)'
        u.createTable('exp_ecap_qc_atdom_catpare', cols, 'altres', rm=True)
        u.listToTable(cataleg_pare, 'exp_ecap_qc_atdom_catpare', 'altres')
        # query = 'select * from altres.exp_ecap_qc_atdom_catpare'
        # table = 'atdomcatalegpare'
        # u.exportPDP(query=query,table=table,datAny=True)
        cataleg = [
                ('EQA0403ATD','Assolir un ambient segur a la llar en persones ATDOM',1533,'ATD',1,0,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/indicador/4370/ver/','PCT'),
                ('ATD073A','Poblaci� ATDOM amb valoraci� del risc social',1531,'ATD',1,0,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/indicador/4372/ver/','PCT'),
                ('EQA0402ATD','Valoraci� del risc d\'�lceres per pressi� en persones ATDOM',1532,'ATD',1,0,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/indicador/4369/ver/','PCT'),
                ('ATD023A','PCC amb PIIC',1230,'ATD',1,0,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD023A','PCT'),
                ('ATD023A1','Pacients PCC amb PIIC amb registre de recomanacions',1231,'ATD',1,0,0,0,0,0,'http://sisap-umi.eines.portalics/indicador/codi/ATD023A1','PCT'),
                ('ATD023A2','Pacients PCC amb PIIC amb decisions anticipades',1232,'ATD',1,0,0,0,0,0,'http://sisap-umi.eines.portalics/indicador/codi/ATD023A2','PCT'),
                ('ATD024A','Pacients MACA amb PIIC.',1240,'ATD',1,0,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD024A','PCT'),
                ('ATD024A1','Pacients MACA amb PIIC i registre de recomanacions en cas de crisi o descompensaci�.',1241,'ATD',1,0,0,0,0,0,'http://sisap-umi.eines.portalics/indicador/codi/ATD024A1','PCT'),
                ('ATD024A2','Pacients MACA amb PIIC i registre del pla de decisions anticipades',1242,'ATD',1,0,0,0,0,0,'http://sisap-umi.eines.portalics/indicador/codi/ATD024A2','PCT'),
                ('ATD025A','Valoraci� Integral en ATDOM',1250,'ATD',1,0,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD025A','PCT'),
                ('ATD026A','Percentatge pacients amb Barthel',1260,'ATD',1,0,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD026A','PCT'),
                ('EQA0404ATD','Sobrec�rrega del cuidador dels pacients en ATDOM',1534,'ATD',1,0,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/indicador/4371/ver/','PCT'),
                ('ATD037A','Percentatge de pacients amb el registre de caigudes',1370,'ATD',1,0,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD037A','PCT'),
                ('ATD039A','Percentatge de pacients amb el test MNA realitzat',1390,'ATD',1,0,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD039A','PCT'),
                ('ATD043A1','Polifarm�cia >10 f�rmacs (excloent bolquers, coliris...) durant m�s de 6 mesos',1431,'ATD',1,1,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD043A1','PCT'),
                ('ATD043A2','Polifarm�cia >8 f�rmacs (excloent bolquers, coliris...)',1432,'ATD',1,1,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD043A2','PCT'),
                ('ATD046A','Percentatge de pacients diagnosticats de dem�ncia en tractament amb antipsic�tics > 3 mesos',1460,'ATD',1,1,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD046A','PCT'),
                ('ATD047A','Duplicitat d\'antidepressius',1470,'ATD',1,1,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD047A','PCT'),
                ('ATD048A','Duplicitat ansiol�tics o hipn�tics',1480,'ATD',1,1,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD048A','PCT'),
                ('ATD049A','Tractament amb estatines',1490,'ATD',1,1,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD049A','PCT'),
                ('ATD050A','Prescripci� estatines en MACA',1500,'ATD',1,1,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD050A','PCT'),
                ('ATD051A','Tractament mal indicat amb antidepressius pel episodi depressiu major',1510,'ATD',1,1,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD051A','PCT'),
                ('ATD052A','Tractament inadequat amb benzodiazepines per insomni en gent gran',1520,'ATD',1,1,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD052A','PCT'),
                ('ATD053A','AINE en malaltia cardiovascular, renal cr�nica o insufici�ncia hep�tica',1530,'ATD',1,1,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/ATD053A','PCT'),
            ]
        cols = '(indicador varchar(20), literal varchar(300), ordre int, pare varchar(20), llistat int, invers int, mmin int, mmint int, mmax int, toshow int, wiki varchar(500), tipusvalor varchar(8))'
        u.createTable('exp_ecap_qc_atdom_cat', cols, 'altres', rm=True)
        u.listToTable(cataleg, 'exp_ecap_qc_atdom_cat', 'altres')
        # query = 'select * from altres.exp_ecap_qc_atdom_cat'
        # table = 'atdomcataleg'
        # u.exportPDP(query=query,table=table,datAny=True)

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, 'nodrizas')}

    def get_ubas(self):
        """."""
        sql = "select up, uba, tipus from mst_ubas"
        self.ubas = set([row for row in u.getAll(sql, "eqa_ind")])

    def get_u11(self):
        self.ids = {}
        self.u11 = {}
        db = "p2262"
        sch = "import"
        tb = "u11_all"
        cols = ["id_cip_sec", "hash_a", "hash_d", "codi_sector"]
        cols = ", ".join(cols)
        sql = "select {} from {}".format(cols, tb)
        with t.Database(db, sch) as conn:
            for id_cip_sec, hash_a, hash_d, sector in conn.get_all(sql):
                if id_cip_sec in self.atdom:
                    self.ids[id_cip_sec] = (hash_d, sector)
                    self.u11[hash_a] = {'id_cip_sec': id_cip_sec,
                                        'hash_d': hash_d,
                                        'sector': sector,
                                        }

    def get_atdom(self):
        """ atdoms """
        sql = """
            SELECT id_cip_sec, case when dde <= DATE '{menys30}' then 1 else 0 end as atdom30
            FROM eqa_problemes
            WHERE
                ps = 45 AND
                dde <= DATE '{DEXTD}'
        """.format(menys30=menys30, DEXTD=DEXTD)
        atdom = set()
        atdom30 = set()
        for id, dies30 in u.getAll(sql, 'nodrizas'):
            atdom.add(id)
            if dies30:
                atdom30.add(id)
        # atdom = set([id for id, in u.getAll(sql, 'nodrizas')])
        residents = set([id for id, in u.getAll("""
            SELECT id_cip_sec FROM assignada_tot WHERE up_residencia <> ''
            """,
            "nodrizas")])
        self.atdom = atdom - residents
        self.atdom30 = atdom30 - residents

    def get_atdom_ass(self):
        self.atdom_ass = c.defaultdict(dict)
        self.atdom_ass30 = c.defaultdict(dict)
        self.pob_ass = c.defaultdict(dict)
        sql = """
            SELECT
                id_cip_sec,
                up, uba,
                upinf, ubainf,
                edat, institucionalitzat
            FROM assignada_tot
        """
        for id, up, uba, upinf, ubainf, edat, inst in u.getAll(sql, "nodrizas"):
            if inst == 0:
                self.pob_ass[id]['M'] = (up, uba)
                self.pob_ass[id]['I'] = (upinf, ubainf)
                self.pob_ass[id]['UP'] = up
                self.pob_ass[id]['edat'] = edat
            ubas = []
            if id in self.atdom:
                self.atdom_ass[id]['UP'] = up
                if id in self.atdom30:
                    self.atdom_ass30[id]['UP'] = up
                if up and uba:
                    ubas.append((up, uba, 'M'))
                if upinf and ubainf:
                    ubas.append((upinf, ubainf, 'I'))
                self.atdom_ass[id]['ubas'] = ubas
                if id in self.atdom30:
                    self.atdom_ass30[id]['ubas'] = ubas

    def get_cont(self):
        self.cont_dades = c.Counter()
        DETALLES = {'MG': 'TIPPROF1',
                    'PED': 'TIPPROF2',
                    'ODN': 'TIPPROF3',
                    'INF': 'TIPPROF4'}
        # cont = c.Counter()
        opts = {
            'atdom': self.atdom_ass
        }
        pacients = []
        for file_name, pob_d in opts.items():
            dim6set = 'POBTOTAL' if file_name == 'total' else 'POBACTUAL'
            ATD = 'ATD' if file_name.endswith('atdom') else ''
            for servei in ('MG', 'INF'):
                for id in self.varss[servei]:
                    if self.varss[servei][id] > 0:
                        if id in pob_d:
                            up = pob_d[id]['UP']
                            ubas_ass = pob_d[id]['ubas']
                            entities = [up]
                            br = self.centres[up]
                            entities.extend([br + entity[2] + entity[1]
                                            for entity
                                            in ubas_ass
                                            if entity in self.ubas])
                            # servei = self.varss['servei'][id]
                            r2 = self.varss[servei][id]
                            for entity in entities:
                                ind = "CONT0002{}".format(ATD)
                                pacients.append((id, ind, 1))
                                self.cont_dades[(ind, entity, "NUM", DETALLES[servei], dim6set)] += r2  # noqa
                                self.cont_dades[(ind, entity, "DEN", DETALLES[servei], dim6set)] += 1  # noqa
                                if id in self.varss['pcc_maca']:
                                    ind = 'CONT0002P{}'.format(ATD)
                                    pacients.append((id, ind, 1))
                                    self.cont_dades[(ind, entity, "NUM", DETALLES[servei], dim6set)] += r2  # noqa
                                    self.cont_dades[(ind, entity, "DEN", DETALLES[servei], dim6set)] += 1  # noqa
        upload = []
        for (ind, up, a, det, dim6set), v in self.cont_dades.items():
            upload.append((ind, up, a, det, dim6set, v))
        cols = "(ind varchar(15), entity varchar(13), analysys varchar(3), detail varchar(10), dim6set varchar(10), valor int)"
        u.createTable('exp_khalix_cont_atdom', cols, 'atdom', rm=True)
        u.listToTable(upload, 'exp_khalix_cont_atdom', 'atdom')
        u.listToTable(pacients, 'pacients_atdom', 'atdom')


    def get_partition(self, db, tb, by):
        """."""
        PERIODE = "DATE '{DEXTD}'".format(DEXTD=DEXTD)
        self.partitions = {}
        dext, = u.getOne("select {data_ext} from dextraccio".format(data_ext=PERIODE), "nodrizas")
        dexta, = u.getOne("""
                          select date_add({data_ext}, interval - 1 month)
                          from dextraccio
                          """.format(data_ext=PERIODE), "nodrizas")
        sql = "select {} from {} partition({}) limit 1"
        for partition in u.getTablePartitions(tb, db):
            try:
                dvis, = u.getOne(sql.format(by, tb, partition), db)
            except TypeError:
                continue
            if (dvis.year == dext.year and dvis.month == dext.month) or \
                    (dvis.year == dexta.year and dvis.month == dexta.month):
                self.partitions[partition] = True

    def get_longit_dades(self):

        # file_name = "LONG_resi"

        pobs = {
            'long_atdom': self.atdom_ass
            # 'LONG_resi_atdom': self.atdom_ass,
            }
        pacients = []
        self.get_partition(db='nodrizas', tb='ag_longitudinalitat_new', by='vis_data')
        self.longit_dades = c.Counter()
        for file_name, pob_d in pobs.items():
            dim6set = 'POBTOTAL' if file_name == 'long_total' else 'POBACTUAL'
            ATD = 'ATD' if file_name.endswith('_atdom') else ''  # noqa
            for parti in self.partitions:
                sql = """
                    SELECT
                        pac_id, pac_up, pac_uba, pac_ubainf,
                        pac_pcc, pac_maca, pac_atdom,
                        modul_serv_homol, modul_eap,
                        prof_responsable, prof_delegat, modul_relacio,
                        prof_rol_gestor, modul_serv_gestor,
                        right(year(vis_data),2),date_format(vis_data,'%m')
                    FROM
                        {} partition({})
                    WHERE
                        prof_numcol <> '' AND
                        vis_laborable = 1
                """.format('ag_longitudinalitat_new', parti)  # Error al filtrar por pac_instit = 0
                for (id, _up, _uba, _ubainf, pcc, maca, _atdom, serv,
                        eap, resp, deleg, _rel, gc_rol, gc_serv,
                        anys, mes) in u.getAll(sql, 'nodrizas'):
                    if id in pob_d:
                        # pob = "POBACTUAL"
                        up = pob_d[id]['UP']
                        ubas_ass = pob_d[id]['ubas']
                        gc = (gc_rol == 1 or gc_serv == 1)
                        periodo = "A" + str(anys) + str(mes)
                        if (eap and serv in ("MG", "PED", "INF", "INFP")) or gc:
                            br = self.centres[up]
                            tip = ["2"]
                            if pcc or maca:
                                tip.append("1")
                            # tip = "1" if (pcc or maca) else "2"  # quito atdom
                            tipprof = 1 if serv == "MG" else 2 if serv == "PED" else 4  # noqa
                            detalle = "TIPPROF{}".format(tipprof)
                            entities = [up]
                            entities.extend([br + entity[2] + entity[1]
                                             for entity
                                             in ubas_ass
                                             if entity in self.ubas])
                            for entity in entities:
                                for t in tip:
                                    compleix = 0
                                    cuenta = "LONG000{}{}".format(t, ATD)
                                    self.longit_dades[(cuenta, periodo, entity,
                                                    detalle, dim6set,
                                                    "DEN")] += 1
                                    if resp == 1 or gc == 1 or deleg == 1:
                                        compleix = 1
                                        self.longit_dades[
                                            ("LONG000{}{}".format(t, ATD),
                                            periodo, entity, detalle, dim6set,
                                            "NUM")] += 1
                                    pacients.append((id, cuenta, compleix))

        longit_dades_ls = []
        for (ind, period, entity, detail, pob, analysys), v in self.longit_dades.items():  # noqa
            longit_dades_ls.append((ind, period, entity, analysys, detail, pob, v))

        cols = "(ind varchar(15), period varchar(5), entity varchar(13), analysys varchar(3), detail varchar(10), dim6set varchar(10), valor int)"
        u.createTable('exp_khalix_long_atdom', cols, 'atdom', rm=True)
        u.listToTable(longit_dades_ls, 'exp_khalix_long_atdom', 'atdom')
        u.listToTable(pacients, 'pacients_atdom', 'atdom')

    def treat_data(self):
        for id in self.atdom30:
            self.varss['atdom30'][id] = 1
        # perdua pes: imc <= 18.5 or peso <= 0.90 * max_pes
        for id in self.varss['imc']:
            imc = self.varss['imc'][id]
            if imc <= 18.5:
                self.varss['perdua_pes'][id] = 1
        for id in self.varss['pes']:
            peso = self.varss['pes'][id]
            if id in self.varss['max_pes']:
                if peso <= 0.9 * self.varss['max_pes'][id]:
                    self.varss['perdua_pes'][id] = 1
        
        # polif10: polif > 10
        for id in self.varss['polif']:
            polif = self.varss['polif'][id]
            if polif > 10:
                self.varss['polif10'][id] = 1
            if polif > 8:
                self.varss['polif8'][id] = 1
        
        # barthel: dividir en < 20, <= 40, entre 20 i 60, 60-100, < 60
        # ((self.grups['barthel'] & (self.grups['pfeifer'] | self.grups['lobo'] | self.grups['demencia']) & self.grups['socialrisk']) | self.grups['fragilitat']) & self.grups['parla'],
        for id in self.varss['barthel']:
            barthel = self.varss['barthel'][id]
            if barthel < 20:
                self.varss['barthel_lt20'][id] = 1
            if barthel <= 40:
                self.varss['barthel_lt40'][id] = 1
            if 20 <= barthel < 60:
                self.varss['barthel_20-59'][id] = 1
            if 60 <= barthel < 100:
                self.varss['barthel_60-99'][id] = 1
            if barthel == 100:
                self.varss['barthel_100'][id] = 1
            if barthel < 60:
                self.varss['barthel_lt60'][id] = 1
        #     if id in (set(self.varss['pfeifer'].keys()) | set(self.varss['lobo'].keys()) | set(self.varss['demencia'].keys())) and id in self.varss['socialrisk'] and id in self.varss['edat'] and self.varss['edat'][id] >= 15:
        #         self.varss['valoracio'][id] = 1
        #     elif id in self.varss['socialrisk'] and id in self.varss['edat'] and self.varss['edat'][id] < 15:
        #         self.varss['valoracio'][id] = 1
        # for id in self.varss['fragilitat']:
        #     if id in self.varss['edat'] and self.varss['edat'][id] >= 15:
        #         self.varss['valoracio'][id] = 1
        
        # pfeifer
        for id in self.varss['pfeifer']:
            pfeifer = self.varss['pfeifer'][id]
            if pfeifer <= 2:
                self.varss['pfeifer_le2'][id] = 1
            if 3 <= pfeifer <= 4:
                self.varss['pfeifer_3-4'][id] = 1
            if 5 <= pfeifer <= 7:
                self.varss['pfeifer_5-7'][id] = 1
            if 8 <= pfeifer <= 10:
                self.varss['pfeifer_ge8'][id] = 1
            if pfeifer >= 5:
                self.varss['pfeifer_ge5'][id] = 1
        # antipsico >= 90 dies
        for id in self.varss['antipsico']:
            data = self.varss['antipsico'][id]
            if u.daysBetween(data, DEXTD) >= 90:
                self.varss['antipsico_90'][id] = 1
        
        for id in self.varss['edat']:
            if self.varss['edat'][id] > 65:
                self.varss['>65 anys'][id] = 1

    def get_llistat(self, codi, pob_d, num, pac, den=None):
        indicador_uba = c.defaultdict(c.Counter)
        pre_llistat = c.defaultdict(lambda: c.defaultdict(set))
        for id in pob_d:
            for uba in pob_d[id]['ubas']:
                # uba te estructura (up, uba, 'M' o 'I')
                up = uba[0]
                tipus = uba[2]
                uba = uba[1]
                denominador = id in den if den else True
                if denominador:
                    number_den = 1 if pac else 1 if id in den else 0
                    indicador_uba[(up,uba,tipus)]["DEN"] += number_den
                    pre_llistat[(up,uba,tipus)]["DEN"].add(den[id] if den else id)   # noqa
                    if id in num:
                        indicador_uba[(up,uba,tipus)]["NUM"] += 1  # noqa
                        pre_llistat[(up,uba,tipus)]["NUM"].add(id)
        try:
            invers = [row for row, in u.getAll("""select invers from atdomcataleg
                                                where indicador = '{}'
                                                """.format(codi), 'pdp')][0]
        except IndexError:
            invers = 0
        llistats = []
        for cosa in pre_llistat.keys():
            up, uba, tipus = cosa
            if invers == 1:
                no_resolts = pre_llistat[(up, uba, tipus)]['NUM']
                resolts = pre_llistat[(up, uba, tipus)]['DEN'] - no_resolts
            else:
                resolts = pre_llistat[(up, uba, tipus)]['NUM']
                no_resolts = pre_llistat[(up, uba, tipus)]['DEN'] - resolts
            indicador_uba[(up, uba, tipus)]['noresolts'] = len(no_resolts)
            # for e9 in resolts:
            #     if e9 in self.ids:
            #         llistats.append((up, uba, tipus, codi, self.ids[e9][0], self.ids[e9][1], 9, None))  # noqa
            for e0 in no_resolts:
                if e0 in self.ids:
                    llistats.append((up, uba, tipus, codi, self.ids[e0][0], self.ids[e0][1], 0, None))  # noqa
        # print("llistat rows: {}".format(llistats[:3]))

        indicador_out = [(up, uba, tipus, codi,
                              indicador_uba[(up, uba, tipus)]["NUM"],
                              indicador_uba[(up, uba, tipus)]["DEN"],
                              indicador_uba[(up, uba, tipus)]["NUM"] * 1.0
                              / indicador_uba[(up, uba, tipus)]["DEN"],
                              indicador_uba[(up, uba, tipus)]["noresolts"])
                             for up, uba, tipus in indicador_uba]
        indicador_out_alt = [(DEXTD.year, DEXTD.month,
                            up, uba, tipus, codi,
                            indicador_uba[(up, uba, tipus)]["NUM"],
                            indicador_uba[(up, uba, tipus)]["DEN"],
                            indicador_uba[(up, uba, tipus)]["NUM"] * 1.0
                            / indicador_uba[(up, uba, tipus)]["DEN"],
                            indicador_uba[(up, uba, tipus)]["noresolts"], 0, 0)
                            for up, uba, tipus in indicador_uba]
        print(len(indicador_out))
        print(len(llistats))
        return (indicador_out, indicador_out_alt, indicador_uba, list(set(llistats)))

    def get_indicadors(self):
        self.grups = c.defaultdict(set)
        for var in self.varss:
            for id in self.varss[var]:
                self.grups[var].add(id)
        u.printTime('despres grups')
        ind_opts = {
            "ATD001A": {'num': self.grups['INFyear'],
                        'type': 'MTJ',
                        'val': self.varss['INFyear']
                        },
            "ATD001S": {'num': self.grups['TSyear'],
                        'type': 'MTJ',
                        'val': self.varss['TSyear']
                        },
            "ATD001AD": {'num': self.grups['D_INFyear'],
                        'type': 'MTJ',
                        'val': self.varss['D_INFyear']
                        },
            "ATD001SD": {'num': self.grups['D_TSyear'],
                        'type': 'MTJ',
                        'val': self.varss['D_TSyear']
                        },
            "ATD002A": {'num': self.grups['INFmonth'],
                        'type': 'MTJ',
                        'val': self.varss['INFmonth']
                        },
            "ATD002S": {'num': self.grups['TSmonth'],
                        'type': 'MTJ',
                        'val': self.varss['TSmonth']
                        },
            "ATD002AD": {'num': self.grups['D_INFmonth'],
                        'type': 'MTJ',
                        'val': self.varss['D_INFmonth']
                        },
            "ATD002SD": {'num': self.grups['D_TSmonth'],
                        'type': 'MTJ',
                        'val': self.varss['D_TSmonth']
                        },
            'ATD003A': {'num': self.grups['INFyear'],
                        'type': 'PCT',
                        },
            'ATD003S': {'num': self.grups['TSyear'],
                        'type': 'PCT',
                        },
            'ATD003AD': {'num': self.grups['D_INFyear'],
                        'type': 'PCT',
                        },
            'ATD003SD': {'num': self.grups['D_TSyear'],
                        'type': 'PCT',
                        },
            'ATD004A': {'num': self.grups['INFmonth'],
                        'type': 'PCT',
                        },
            'ATD004S': {'num': self.grups['TSmonth'],
                        'type': 'PCT',
                        },
            'ATD004AD': {'num': self.grups['D_INFmonth'],
                        'type': 'PCT',
                        },
            'ATD004SD': {'num': self.grups['D_TSmonth'],
                        'type': 'PCT',
                        },
            "ATD005A": {'num': self.grups['MEDyear'],
                        'type': 'MTJ',
                        'val': self.varss['MEDyear']
                        },
            "ATD005AD": {'num': self.grups['D_MEDyear'],
                        'type': 'MTJ',
                        'val': self.varss['D_MEDyear']
                        },
            "ATD006A": {'num': self.grups['MEDmonth'],
                        'type': 'MTJ',
                        'val': self.varss['MEDmonth']
                        },
            "ATD006AD": {'num': self.grups['D_MEDmonth'],
                        'type': 'MTJ',
                        'val': self.varss['D_MEDmonth']
                        },
            'ATD007A': {'num': self.grups['MEDyear'],
                        'type': 'PCT',
                        },
            'ATD007AD': {'num': self.grups['D_MEDyear'],
                        'type': 'PCT',
                        },
            'ATD008A': {'num': self.grups['MEDmonth'],
                        'type': 'PCT',
                        },
            'ATD008AD': {'num': self.grups['D_MEDmonth'],
                        'type': 'PCT',
                        },
            "ATD011A": {'num': self.grups['edat'],
                        'type': 'MTJ',
                        'val': self.varss['edat']
                        },
            "ATD013A": {'num': self.grups['barthel'],
                        'den': self.grups['barthel'],
                        'type': 'MTJ',
                        'val': self.varss['barthel']
                        },
            "ATD014A": {'num': self.grups['pfeifer'],
                        'den': self.grups['pfeifer'],
                        'type': 'MTJ',
                        'val': self.varss['pfeifer']
                        },
            "ATD014B": {'num': self.grups['val_soc_alterada'],
                        'type': 'PCT',
                        },
            'ATD015A': {'num': self.grups['barthel_lt60'],
                        'type': 'PCT',
                        },
            'ATD016A': {'num': self.grups['pfeifer_ge5'],
                        'type': 'PCT',
                        },
            'ATD018A': {'num': self.grups['pcc'],
                        'type': 'PCT',
                        },
            'ATD019A': {'num': self.grups['maca'],
                        'type': 'PCT',
                        },
            'ATD020A': {'num': self.grups['demencia'],
                        'type': 'PCT',
                        },
            'ATD021A': {'num': self.grups['npi_q'],
                        'type': 'PCT',
                        },
            'ATD022A': {'num': self.grups['urinaria'],
                        'type': 'PCT',
                        },
            'ATD023A': {'num': self.grups['piic'] & self.grups['pcc'],
                        'den': self.grups['pcc'],
                        'type': 'PCT',
                        },
            'ATD024A': {'num': self.grups['piic'] & self.grups['maca'],
                        'den': self.grups['maca'],
                        'type': 'PCT',
                        },
            'ATD025A': {'num': self.grups['valoracio'],
                        'type': 'PCT',
                        },
            'ATD026A': {'num': self.grups['barthel'],
                        'type': 'PCT',
                        },
            'ATD026A1': {'num': self.grups['barthel_lt20'],
                        'type': 'PCT',
                        },
            'ATD026A2': {'num': self.grups['barthel_20-59'],
                        'type': 'PCT',
                        },
            'ATD026A3': {'num': self.grups['barthel_60-99'],
                        'type': 'PCT',
                        },
            'ATD026A4': {'num': self.grups['barthel_100'],
                        'type': 'PCT',
                        },
            'ATD027A': {'num': self.grups['pfeifer'],
                        'den': self.grups['parla'] - self.grups['demencia'],
                        'type': 'PCT',
                        },
            'ATD027A1': {'num': self.grups['pfeifer_ge8'],
                        'den': self.grups['parla'] - self.grups['demencia'],
                        'type': 'PCT',
                        },
            'ATD027A2': {'num': self.grups['pfeifer_5-7'],
                        'den': self.grups['parla'] - self.grups['demencia'],
                        'type': 'PCT',
                        },
            'ATD027A3': {'num': self.grups['pfeifer_3-4'],
                        'den': self.grups['parla'] - self.grups['demencia'],
                        'type': 'PCT',
                        },
            'ATD027A4': {'num': self.grups['pfeifer_le2'],
                        'den': self.grups['parla'] - self.grups['demencia'],
                        'type': 'PCT',
                        },
            'ATD035A': {'num': self.grups['gds_fast'] & self.grups['demencia'],
                        'den': self.grups['demencia'],
                        'type': 'PCT',
                        },
            'ATD036A': {'num': self.grups['perdua_pes'],
                        'type': 'PCT',
                        },
            'ATD037A': {'num': self.grups['caigudes'] | self.grups['registre_caig'],
                        'type': 'PCT',
                        },
            'ATD039A': {'num': self.grups['mna'],
                        'type': 'PCT',
                        },
            'ATD042A': {'num': self.grups['psicof'],
                        'type': 'MTJ',
                        'val': self.varss['psicof']
                        },
            'ATD043A1': {'num': self.grups['polif10'],
                        'type': 'PCT',
                        },
            'ATD043A2': {'num': self.grups['polif8'],
                        'type': 'PCT',
                        },
            'ATD044A': {'num': self.grups['demencia'] & self.grups['iace_memantina'],
                        'den': self.grups['demencia'],
                        'type': 'PCT',
                        },
            'ATD045A': {'num': self.grups['gds7b'] & self.grups['iace_memantina'],
                        'type': 'PCT',
                        },
            "ATD046A": {"num": self.grups['demencia'] & self.grups['antipsico_90'],
                        "den": self.grups['demencia'] | self.grups['iace_memantina'],
                        'type': 'PCT',
                        },
            'ATD047A': {'num': self.grups['antidepressius_2'],
                        'type': 'PCT',
                        },
            'ATD048A': {'num': self.grups['ansio_hipnotics_2'],
                        'type': 'PCT',
                        },
            'ATD049A': {'num': self.grups['estatines'],
                        'type': 'PCT',
                        },
            "ATD050A": {"num": self.grups['maca'] & self.grups['estatines'],
                        "den": self.grups['maca'],
                        'type': 'PCT',
                        },
            "ATD051A": {"num": self.grups['antidepressius'] & self.grups['depre'],
                        "den": self.grups['depre'],
                        'type': 'PCT',
                        },
            "ATD052A": {"num": self.grups['insomni'] & self.grups['benzo'],
                        "den": self.grups['insomni'] & self.grups['>65 anys'],
                        'type': 'PCT',
                        },
            "ATD053A": {"num": self.grups['aines'] & (self.grups['ci'] | self.grups['avc'] | self.grups['claud_int'] | self.grups['irc'] | self.grups['insuf_hepat']),
                        "den": self.grups['ci'] | self.grups['avc'] | self.grups['claud_int'] | self.grups['irc'] | self.grups['insuf_hepat'],
                        'type': 'PCT',
                        },
            'ATD054A': {'num': self.grups['maltracte'],
                        'type': 'PCT',
                        },
            "ATD056A": {'num': self.grups['DMO'] & self.grups['osteoporosi'] - self.grups['antiosteop'],
                        'den': self.grups['osteoporosi'] - self.grups['antiosteop'],
                        'type': 'PCT',
                        },
            'ATD057A': {'num': self.grups['caigudes'],
                        'type': 'MTJ',
                        'val': self.varss['caigudes']
                        },
            "ATD058A": {
                        "num": self.grups['placures'],
                        'type': 'PCT',
                        },
            "ATD073A": {
                        "num": self.grups['socialrisk'],
                        'type': 'PCT',
                        },
        }
        ecap_ind = []
        ecap_llistat = []
        pacients = []
        landing = []
        upload = c.Counter()
        sql = '(indicador varchar(20), up varchar(5), uba varchar(5), tipus varchar(1), analisi varchar(3), dim6set varchar(10), n int)'
        u.createTable("EXP_KHALIX_QC_ATDOM_UBA", sql, "atdom", rm=True)
        pobs = {'actual': self.atdom_ass30}
        for file_name, pob_d in pobs.items():
            dim6set = 'POBTOTAL' if file_name == 'total' else 'POBACTUAL'  # noqa
            for ind in ind_opts:
                num = ind_opts[ind]['num']
                den = ind_opts[ind]['den'] if 'den' in ind_opts[ind] else pob_d
                typ = ind_opts[ind]['type']
                val = ind_opts[ind]['val'] if 'val' in ind_opts[ind] else None
                if file_name != 'total' and typ == 'PCT':
                    val_ind, val_ind_alt, val_khalix, val_llist = self.get_llistat(ind,
                        pob_d,
                        num,
                        1 if 'den' not in ind_opts[ind] else None,  # noqa
                        {k: k
                        for k in pob_d.keys()
                        if k in den}
                    )
                    upload_khalix = []
                    for (up, uba, tipus), dic_analisi in val_khalix.items():
                        for analisi, n in dic_analisi.items():
                            if analisi != 'noresolts':
                                upload_khalix.append((ind, up, uba, tipus, analisi, dim6set, n))
                    u.listToTable(upload_khalix, "EXP_KHALIX_QC_ATDOM_UBA", "atdom")
                    try:
                        if ind in ATDOM:
                            ecap_ind += val_ind
                        # sql = "delete from ATDOMINDICADORS where dataany = {any} and datames = {mes} and indicador = '{indicador}'".format(any=DEXTD.year, mes=DEXTD.month, indicador=ind)
                        # u.execute(sql, 'pdp')
                        # u.listToTable(val_ind, "ATDOMINDICADORS", "pdp")
                    except:
                        raise
                    try:
                        if ind in ATDOM:
                            ecap_llistat += val_llist
                        # sql = "delete from ATDOMLLISTATS where indicador = '{indicador}'".format(indicador=ind)
                        # u.execute(sql, 'pdp')
                        # u.listToTable(val_llist, "ATDOMLLISTATS", "pdp")
                    except:
                        raise
                    try:
                        u.listToTable(val_ind_alt, "ALTINDICADORS", "pdp")
                        u.printTime("Done Insert val_indicadors: {}".format(ind))  # noqa
                    except Exception as e:
                        text = str(e)
                    try:
                        u.listToTable(val_llist, "ALTLLISTATS", "pdp")
                    except Exception as e:
                        text = str(e)
                for id in den:
                    if id in pob_d:
                        compleix = 0
                        resi = pob_d[id]['UP']
                        upload[(ind, resi, 'DEN', dim6set)] += 1
                        if id in num:
                            compleix = 1
                            if typ == 'PCT':
                                upload[(ind, resi, 'NUM', dim6set)] += 1
                            if typ == 'MTJ':
                                upload[(ind, resi, 'NUM', dim6set)] += val[id]
                        pacients.append((id, ind, compleix))
                        if id in self.pob_ass:
                            up, uba = self.pob_ass[id]['M']
                            upinf, ubainf = self.pob_ass[id]['I']
                            landing.append((id, ind, up, uba, ubainf, compleix))
            
            indicador = {
                'ATD009A1': (65, 120),
                'ATD009A2': (15, 120),
                'ATD009A3': (0, 15)
                }
            upload_uba = c.Counter()
            for indicador, tall in indicador.items():
                for id in self.pob_ass:
                    edat = self.pob_ass[id]['edat']
                    resi = self.pob_ass[id]['UP']
                    up, uba = self.pob_ass[id]['M']
                    upinf, ubainf = self.pob_ass[id]['I']
                    if edat and tall[0] <= edat < tall[1]:
                        compleix = 0
                        upload[(indicador, resi, 'DEN', dim6set)] += 1
                        upload_uba[(indicador, up, uba, 'M', 'DEN', dim6set)] += 1
                        upload_uba[(indicador, upinf, ubainf, 'I', 'DEN', dim6set)] += 1
                        if id in self.atdom_ass:
                            compleix = 1
                            upload[(indicador, resi, 'NUM', dim6set)] += 1
                            upload_uba[(indicador, up, uba, 'M', 'NUM', dim6set)] += 1
                            upload_uba[(indicador, upinf, ubainf, 'I', 'NUM', dim6set)] += 1
                        pacients.append((id, indicador, compleix))
                            # s'agrupen les dades per UBA
            upload_uba_khalix = [tuple(list(k) + [v]) for k, v in upload_uba.items()]
            u.listToTable(upload_uba_khalix, "EXP_KHALIX_QC_ATDOM_UBA", "atdom")
        upload = [tuple(list(k) + [v]) for k, v in upload.items()]
        cols = "(ind varchar(10), entity varchar(10), analysys varchar(3), dim6set varchar(10), valor int)"
        u.createTable('exp_khalix_tests_atdom', cols, 'atdom', rm=True)
        u.listToTable(upload, 'exp_khalix_tests_atdom', 'atdom')
        cols = "(up varchar(5), uab varchar(20), tipus varchar(1), indicador varchar(10), numerador int, denominador int, resultat double, noresolts int)"
        u.createTable('exp_ecap_atdom_ind', cols, 'atdom', rm=True)
        cols = "(up varchar(5), uab varchar(20), tipus varchar(1), indicador varchar(10), idpac varchar(40), sector varchar(4), exclos int, comentari varchar(500))"
        u.createTable('exp_ecap_atdom_llis', cols, 'atdom', rm=True)
        u.listToTable(ecap_ind, 'exp_ecap_atdom_ind', 'atdom')
        query = "select * from atdom.exp_ecap_atdom_ind"
        u.exportPDP(query=query, table = 'ATDOMINDICADORS', dat=True, fmo=False)
        u.listToTable(ecap_llistat, 'exp_ecap_atdom_llis', 'atdom')
        query = "select * from atdom.exp_ecap_atdom_llis"
        u.exportPDP(query=query, table='ATDOMLLISTATS', truncate=True,pkOut=True,pkIn=True,fmo=False)
        u.listToTable(pacients, 'pacients_atdom', 'atdom')
        cols = "(id_cip_sec int, ind varchar(10), up varchar(10), uba varchar(20), ubainf varchar(20), num int)"
        u.createTable('landing_atdom', cols, 'atdom', rm=True)
        u.listToTable(landing, 'landing_atdom', 'atdom')

    def get_master(self):
        sql = """
            SELECT id_cip_sec, antipsico, benzo, iace_memantina,
            antidepressius, ansio_hipnotics, estatines, aines,
            pcc, maca, piic, urinaria,
            maltracte, osteoporosi, demencia, depre, ci, avc,
            claud_int, irc, insuf_hepat, antiosteop, DMO, gds_fast, npi_q,
            gds7b, edat, registre_caig, braden, mna, barthel,
            imc, pes, max_pes, pfeifer, caigudes, polif, psicof,
            MEDyear, MEDmonth, INFyear, INFmonth, insomni, pcc_maca,
            servei_mg, servei_inf, parla, TSyear, TSmonth,
            D_MEDyear, D_MEDmonth, D_INFyear, D_INFmonth,
            D_TSyear, D_TSmonth, val_soc_alterada, placures, socialrisk,
            fragilitat, antidepressius_2, ansio_hipnotics_2,
            barthel_frag, pfeifer_frag, mna_frag, caigudes_frag,
            emocional, social
            FROM master_residencies
        """
        
        # partitions = ['resis_1', 'resis_2', 'resis_3', 'resis_4', 'resis_5']
        # jobs = [(partition, sql) for partition in partitions]
        # self.varss = c.defaultdict(dict)
        self.varss = sub_get_master(sql)
        # for dades in u.multiprocess(sub_get_master, jobs, 5, close=True):
        # self.varss.update(dades)
        # vars_last �s [var][cip] = val
        # grups �s [var].add(id)
        # u.printTime('multiprocess')
        # for id, antipsico, benzo, iace_memantina, antidepressius, ansio_hipnotics, estatines, aines, pcc, maca, piic, piir, pirda, piirV, piirT, urinaria, maltracte, osteoporosi, demencia, depre, ci, avc, claud_int, irc, insuf_hepat, antiosteop, DMO, gds_fast, gds7b, edat, registre_caig, braden, mna, barthel, imc, pes, max_pes, pfeifer, caigudes, polif, psicof, MEDyear, MEDmonth, INFyear, INFmonth, P_MEDyear, P_INFyear, T_MEDyear, T_INFyear, insomni, pcc_maca, servei, r2 in u.getAll(sql, 'resis'):
        #     self.varss
        # falta afegir antidepressius_2 i ansio_hipnotics_2

    def get_conversor(self):
        """."""
        self.conversor = {}
        sql = "select usua_cip, usua_cip_cod from pdptb101 \
               where usua_cip_cod like '{}%'"
        for cip_a, hash_a in s.get_data(sql, model="redics", redics="pdp"):
            self.conversor[cip_a] = hash_a
        # print(str(len(self.conversor)))

if __name__=='__main__':
    try:
        Master()
        mail = t.Mail()
        mail.to.append("alejandrorivera@gencat.cat")
        mail.subject = "indicadors atdom b� :)"
        text = ''
        mail.text = text
        mail.send()
    except Exception as e:
        text = str(e)
        mail = t.Mail()
        mail.to.append("alejandrorivera@gencat.cat")
        mail.subject = "indicadors atdom malament :("
        mail.text = text
        mail.send()
        raise