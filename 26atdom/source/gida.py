# -*- coding: latin1 -*-

"""
copiado de 08alt/source/gida.py
"""
# import hashlib as h
# from datetime import datetime
import collections as c

# import sisaptools as t
import sisapUtils as u

import dateutil.relativedelta as r

SUFIX = "_atdom"

MASTER_TB = "mst_gida{}".format(SUFIX)
KHALIX_TB = "exp_khalix_gida{}".format(SUFIX)
KHALIX_UP = "GIDA{}".format(SUFIX)
KHALIX_UBA = "GIDA_UBA{}".format(SUFIX)
CORTES_TB = "exp_khalix_gida_mensual{}".format(SUFIX)
CORTES_UP = "GIDA_MENSUAL{}".format(SUFIX)
QC_TB = "exp_qc_gida{}".format(SUFIX)
# DATABASE = "altres"
DATABASE = "atdom"
DEXTD = u.getOne("select data_ext from dextraccio", "nodrizas")[0]
menys30 = DEXTD - r.relativedelta(days=30)
TAXES = {
    "PABCESA": 5.2577444780838,
    "PAFTAA": 0.64353537675087,
    "PANTIC": 0.314891408767176,
    "PANSIE": 3.71921925701736,
    "PAPZB": 0.425027223520727,
    "PCEFALA": 5.16472350344691,
    "PCERVI": 2.52356334348391,
    "PCOLIC": 0.0411098732994345,
    "PCONTD": 1.21704997599514,
    "PCONTU": 5.78656077591262,
    "PCOSEN": 0.0350197187929564,
    "PCOSEO": 0.0877768743695683,
    "PCREM": 2.31795604290189,
    "PCREPE": 0.329983737596058,
    "PDIAGN": 1.03730557538692,
    "PDIARR": 11.4234445032924,
    "PDML": 8.57241327519725,
    "PEPIS": 0.410744946003007,
    "PEPISN": 0.0460311485676763,
    "PFEBAN": 4.27140333603846,
    "PFEBSF": 5.13751961771815,
    "PFERID": 19.1768056475241,
    "PFERNE": 4.16971002682668,
    "PHERPLA": 0.166687886578287,
    "PINSOMA": 0.0939083809556885,
    "PLDERM": 0.98219608971855,
    "PLESC": 0.633488070053698,
    "PMAREI": 10.8921688274182,
    "PMOLOR": 13.5785540195654,
    "PMUCO": 1.3594578348242,
    "PMUGN": 0.044018906711633,
    "PMURI": 33.7683944007229,
    "PMVAGA": 1.31619769359147,
    "PODINO": 25.8706516172878,
    "PODON": 1.35240896560876,
    "PPICIN": 0.672004294395738,
    "PPICPE": 2.754989077508,
    "PPLOIN": 0.0394714334870186,
    "PPOLLS": 0.0467511548140302,
    "PPRESA": 4.36583663891906,
    "PREGUR": 0.060582542618725,
    "PRESTN": 0.159984697721928,
    "PRESTREA": 0.603810144826813,
    "PRESVA": 49.8269922191717,
    "PREVACA": 0.162048520067087,
    "PREVACAP": 0.0453135247117959,
    "PSCOVA": 1.39566644032561,
    "PSCOVAP": 0.0302052523143464,
    "PSOSVA": 0.0473657986723003,
    "PTOS": 2.66359958714152,
    "PTURME": 1.17070731198391,
    "PULL": 5.7918438680642,
    "PUNGA": 0.788700028620055,
    "PURTIA": 2.24985018751402,
    "PVOLTA": 0.744654244982946,
    "PVOMIT": 11.1681124722259,
    "PVOMNE": 1.39029301590878,
    "PRAOADU": 0.257873147546163,
    "PRAOPED": 0.0527365935237381,
    "PACEI": 0.0401676783290664,
    "PVOLTAP": 0.128464121310962,
    "PUNGAP": 0.114838209198685,
    "PHERPAP": 0.0483585401145677,
    "PAFTAAP": 0.181361219737247,
    "PODINAP": 1.88137686510272,
    "PODONTAP": 0.0544801665116972,
    "PMELICAP": 0.0393639903497557,
    "PDOLMA": 0,
    "PGRIPA": 0
    }

CONVERSIO = {"ACE_A": "ANTIC", "CERVI_A": "CERVI", "CONT_A": "CONTU",
             "CREM_A": "CREM", "C_ANSI_A": "ANSIE", "DIARR_A": "DIARR",
             "LUMB_A": "DML", "EPA_A": "PRESA", "ENTOR_A": "TURME",
             "EPIST_A": "EPIS", "FEBRE_A": "FEBSF", "FERIDA_A": "FERID",
             "PLEC_A": "LDERM", "MAREIG_A": "MAREI", "M_OID_A": "MOLOR",
             "M_ULL_A": "ULL", "M_URI_A": "MURI", "ODIN_A": "ODINO",
             "ODONT_A": "ODON", "PICAD_A": "PICPE", "SRVA_A": "RESVA",
             "VOMIT_A": "VOMIT", "ALT_A": "A", "CEFAL": "CEFAL_A",
             "HERP": "HERPL_A", "ABCES": "ABCES_A", "UNG": "UNG_A",
             "VOLT": "VOLT_A", "RESTR": "RESTRE_A", "MVAG": "MVAG_A",
             "REVAC": "REVAC_A", "AFTA": "AFTA_A", "INSOM": "INSOM_A",
             "URTI": "URTI_A", "LESFC_A": "LESC",
             "COLIC_AP": "COLIC", "CON_D_AP": "CONTD", "CEORE_AP": "COSEO",
             "CENAS_AP": "COSEN", "CREMA_AP": "CREPE", "DER_B_AP": "APZB",
             "DIARR_AP": "DIAGN", "EPIST_AP": "EPISN", "FEBRE_AP": "FEBAN",
             "FERID_AP": "FERNE", "MOCS_AP": "MUCO", "PICAD_AP": "PICIN",
             "PLORS_AP": "PLOIN", "POLLS_AP": "POLLS", "REGUR_AP": "REGUR",
             "RESTR_AP": "RESTN", "MUGUE_AP": "MUGN", "VARIC_AP": "SOSVA",
             "TOS_AP": "TOS", "VOMIT_AP": "VOMNE"}


class GIDA(object):
    """."""

    def __init__(self):
        """."""
        self.get_pob()
        self.recomptes = c.defaultdict(lambda: c.defaultdict(c.Counter))
        # 5 y 6 *ubainf (taxa poblacional), 3 y 4 *Colegiado (% visitas)
        self.mensual = c.Counter()  # get_aguda: 154 y export_mensual: 296
        self.get_edats()  # categories: adu, ped
        self.get_poblacio()  # {id: (up, ubainf)}, fitro por id_atdom
        self.get_centres()
        self.get_visites()
        self.get_usu_to_col()
        self.get_motius()
        self.get_aguda()
        self.get_master()
        self.get_derivats()
        self.export_master()
        self.get_col_to_uba()
        self.get_ubas()
        self.get_khalix()
        self.get_taxes()
        self.export_khalix()
        # self.export_mensual()
        # self.export_qc()

    def get_pob(self):  # QcATDOM

        # get ATDOM de eqa_problemes
        sql = """
                select id_cip_sec
                from eqa_problemes where ps = 45 and dde <= DATE '{menys30}'
              """.format(menys30=menys30)
        id_atdom = set([id for id, in u.getAll(sql, 'nodrizas')])
        id_residents = set([id for id, in u.getAll("""
            SELECT id_cip_sec FROM assignada_tot WHERE up_residencia <> ''
            """,
            "nodrizas")])
        self.id_atdom = id_atdom - id_residents
        self.id_atdom_ass = c.defaultdict(dict)
        sql = """
                select id_cip_sec, up, uba, upinf, ubainf, pcc, maca
                from assignada_tot
              """
        for id, up, uba, upinf, ubainf, _pcc, _maca in u.getAll(sql, "nodrizas"):  # noqa
            if id in self.id_atdom:
                self.id_atdom_ass[id]['UP'] = up
                self.id_atdom_ass[id]['dep'] = ''
                if uba:
                    self.id_atdom_ass[id]['M'] = (up, uba)
                if ubainf:
                    self.id_atdom_ass[id]['I'] = (upinf, ubainf)

    def get_edats(self):
        """."""
        sql = """
            SELECT
                id_cip_sec,
                (YEAR(data_ext) -
                 YEAR(usua_data_naixement)
                 ) -
                 (DATE_FORMAT(data_ext, '%m%d') <
                  DATE_FORMAT(usua_data_naixement, '%m%d')
                  )
            FROM
                assignada,
                nodrizas.dextraccio
            """
        self.edats = {id: "ADU" if edat > 14 else "PED"
                      for (id, edat) in u.getAll(sql, "import")
                      if id in self.id_atdom
                      }

    def get_poblacio(self):
        """."""
        self.poblacio = {}
        sql = "SELECT id_cip_sec, up, ubainf FROM assignada_tot WHERE ates = 1"
        for id, up, uba in u.getAll(sql, "nodrizas"):
            if id in self.id_atdom:
                grup = self.edats[id]
                for ind in ("GESTINF05ATD", "GESTINF06ATD"):
                    self.recomptes[ind][(up, uba, "UBA", grup)]["DEN"] += 1  # noqa
                self.poblacio[id] = (up, uba)

    def get_centres(self):
        """."""
        sql = "SELECT scs_codi, tip_eap, ics_codi FROM cat_centres"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_visites(self):
        """."""
        sql = """
            SELECT id_cip_sec, visi_up, visi_col_prov_resp
            FROM visites1, nodrizas.dextraccio
            WHERE
                visi_col_prov_resp like '3%' AND
                s_espe_codi_especialitat not in ('EXTRA', '10102') AND
                visi_tipus_visita not in ('9E', 'EXTRA', 'EXT') AND
                visi_situacio_visita = 'R' AND
                visi_data_visita BETWEEN
                    ADDDATE(data_ext, interval -1 year) AND
                    data_ext
            """
        for id, up, col in u.getAll(sql, "import"):
            if id in self.id_atdom:
                if up in self.centres:
                    if id in self.edats:
                        grup = self.edats[id]
                        tipus = self.centres[up][0]
                        go = (tipus == "M",
                              (grup == "ADU" and tipus == "A"),
                              (grup == "PED" and tipus == "N")
                              )
                        if any(go):  # QcResi
                            for ind in ("GESTINF03ATD", "GESTINF04ATD"):
                                self.recomptes[ind][(up, col, "COL", grup)]["DEN"] += 1  # noqa

    def get_usu_to_col(self):
        """."""
        sql = "SELECT codi_sector, ide_usuari, ide_numcol \
               FROM cat_pritb992 \
               WHERE ide_numcol <> ''"
        self.usu_to_col = {row[:2]: row[2] for row in u.getAll(sql, "import")}

    def get_motius(self):
        """."""
        sql = """
                SELECT sym_name
                FROM icsap.klx_master_symbol
                WHERE
                    dim_index = 4 AND (
                    sym_index IN (
                        SELECT sym_index
                        FROM icsap.klx_parent_child
                        WHERE
                            dim_index = 4 AND
                            parent_index IN (
                                SELECT sym_index
                                FROM icsap.klx_master_symbol
                                WHERE
                                    dim_index = 4 AND
                                    sym_name IN ('PMOTIUPRPE', 'PMOTIUTEL')
                                )
                        ) OR
                    sym_name = 'PALPED')
            """
        self.motius = {motiu: "PED" for motiu, in u.getAll(sql, "khalix")}

    def _get_motiu(self, motiu, grup):
        """."""
        motiu = CONVERSIO.get(motiu, motiu)
        if motiu == "A":
            motiu = "PAL{}".format(grup)
        elif motiu == "RAO_A":
            motiu = "PRAO{}".format(grup)
        else:
            motiu = "P{}".format(motiu.replace("_", ""))
        if motiu == 'PACEAP': motiu = 'PACEI'
        elif motiu == 'PRAOAP': motiu = 'PRAOPED'
        elif motiu == 'PMULLAP': motiu = 'PMULLP'
        if motiu not in self.motius:
            self.motius[motiu] = "ADU"
        return motiu

    def get_aguda(self):
        """."""
        dext, = u.getOne("SELECT data_ext FROM dextraccio", "nodrizas")
        first = (dext - r.relativedelta(months=1)).replace(day=1)
        sql = """
            SELECT
                id_cip_sec, codi_sector, cesp_up, cesp_usu_alta,
                cesp_cod_mce,
                cesp_estat,
                cesp_data_alta
            FROM
                aguda, nodrizas.dextraccio
            WHERE
                cesp_data_alta between
                    adddate(data_ext, interval -1 year) and data_ext
            UNION
            SELECT
                id_cip_sec, codi_sector, pi_up, pi_usu_alta,
                upper(replace(pi_anagrama, ' ', '')),
                if(pi_ser_derivat = '', 'I', 'M'),
                pi_data_alta
            FROM
                ares, nodrizas.dextraccio
            WHERE
                pi_anagrama not in ('', 'TAP_A') and
                pi_data_alta between
                    adddate(data_ext, interval -1 year) and data_ext
            """
        for id, sec, up, usu, motiu, estat, dat in u.getAll(sql, "import"):
            if id in self.edats:
                grup = self.edats[id]
                motiu = self._get_motiu(motiu, grup)
                motiu_tip = self.motius[motiu]
                if motiu == 'PACEAP': motiu = 'PACEI'
                elif motiu == 'PRAOAP': motiu = 'PRAOPED'
                elif motiu == 'PMULLAP': motiu = 'PMULLP'
                if grup == motiu_tip:
                    if id in self.poblacio:
                        up_pac, uba = self.poblacio[id]
                        if up == up_pac:
                            key = (up_pac, uba, "UBA", grup)
                            self.recomptes["GESTINF05ATD"][key][motiu] += 1
                            if estat == "I":
                                self.recomptes["GESTINF06ATD"][key][motiu] += 1  # noqa
                            if dat >= first:
                                key = ("B" + dat.strftime("%y%m"), motiu)  # noqa
                                self.mensual[("GESTINF05ATD",) + key] += 1
                                if estat == "I":
                                    self.mensual[("GESTINF06ATD",) + key] += 1
                    col = self.usu_to_col.get((sec, usu))
                    key = (up, col, "COL", grup)
                    if key in self.recomptes["GESTINF03ATD"]:
                        self.recomptes["GESTINF03ATD"][key][motiu] += 1
                        if estat == "I":
                            self.recomptes["GESTINF04ATD"][key][motiu] += 1
                        if dat >= first:
                            key = ("B" + dat.strftime("%y%m"), motiu)
                            self.mensual[("GESTINF03ATD",) + key] += 1
                            if estat == "I":
                                self.mensual[("GESTINF04ATD",) + key] += 1

    def get_master(self):
        """."""
        self.master = c.defaultdict(c.Counter)
        for ind, dades in self.recomptes.items():
            for (up, prof, tip, grup), minidades in dades.items():
                den = minidades["DEN"]
                for motiu, motiu_tip in self.motius.items():
                    if motiu_tip == grup:
                        key = (ind, up, prof, tip, grup, motiu)
                        self.master[key]["DEN"] = den
                        self.master[key]["NUM"] = minidades[motiu]

    def get_derivats(self):
        """."""
        for (ind, up, prof, tip, grup, motiu), dades in self.master.items():  # noqa
            if ind == "GESTINF03ATD":
                key = ("GESTINF02ATD", up, prof, tip, "MIX", "TIPMOTIU")
                self.master[key]["DEN"] += dades["NUM"]
                if motiu not in ("PALADU", "PALPED"):
                    self.master[key]["NUM"] += dades["NUM"]
            if ind in ("GESTINF03ATD", "GESTINF04ATD"):
                key = ("GESTINF07ATD", up, prof, tip, grup, motiu)
                analysis = "DEN" if ind == "GESTINF03ATD" else "NUM"
                self.master[key][analysis] += dades["NUM"]

    def export_master(self):
        """."""
        u.createTable(MASTER_TB, "(ind varchar(12), up varchar(5), \
                                   prof varchar(10), tip varchar(3), \
                                   grup varchar(3), motiu varchar(10), \
                                   num int, den int)",
                      DATABASE, rm=True)
        upload = [k + (v["NUM"], v["DEN"]) for (k, v) in self.master.items()]
        u.listToTable(upload, MASTER_TB, DATABASE)

    def get_col_to_uba(self):
        """Per passar dades individuals a khalix dels indicadors de NUMCOL."""
        self.col_to_uba = c.defaultdict(set)
        sql = "SELECT ide_numcol, up, uab \
               FROM cat_professionals \
               WHERE tipus = 'I'"
        for col, up, uba in u.getAll(sql, "import"):
            self.col_to_uba[(up, col)].add(uba)

    def get_ubas(self):
        """Nom�s pujarem UBAs amb EQA."""
        self.ubas = c.defaultdict(set)
        for db, grup in (("eqa_ind", "ADU"), ("pedia", "PED")):
            sql = "SELECT up, uba FROM mst_ubas WHERE tipus = 'I'"
            for key in u.getAll(sql, db):
                self.ubas[grup].add(key)
                self.ubas["MIX"].add(key)

    def get_khalix(self):
        """."""
        self.khalix = c.Counter()
        for (ind, up, prof, tip, grup, motiu), dades in self.master.items():
            br = self.centres[up][1]
            for analisi in ("NUM", "DEN"):
                self.khalix[(br, ind, motiu, analisi)] += dades[analisi]
            if tip == "COL":
                ubas = self.col_to_uba[(up, prof)]
            else:
                ubas = (prof,)
            for uba in ubas:
                if (up, uba) in self.ubas[grup]:
                    ent = "{}I{}".format(br, uba)
                    for analisi in ("NUM", "DEN"):
                        self.khalix[(ent, ind, motiu, analisi)] += dades[analisi]  # noqa

    def get_taxes(self):
        """Hace GESTINF01ATD a partir de GESTINF05ATD"""
        for (ent, ind, motiu, analisi), n in self.khalix.items():
            if ind == "GESTINF05ATD" and analisi == "DEN" and motiu in TAXES:
                taxa = 1000 * self.khalix[(ent, ind, motiu, "NUM")] / float(n)
                meta = TAXES[motiu]
                good = taxa >= meta
                self.khalix[(ent, "GESTINF01ATD", motiu, "DEN")] = 1
                self.khalix[(ent, "GESTINF01ATD", motiu, "NUM")] = 1 * good

    def export_khalix(self):
        """."""
        cols = "(ent varchar(11), ind varchar(12), motiu varchar(10), \
                 analisi varchar(3), valor int)"
        u.createTable(KHALIX_TB, cols, DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in self.khalix.items()]
        u.listToTable(upload, KHALIX_TB, DATABASE)
        '''for param, file in (("=", KHALIX_UP), (">", KHALIX_UBA)):
            sql = "select ind, 'Aperiodo', ent, analisi, motiu, \
                          'NOIMP', 'DIM6SET', 'N', valor \
                   from {}.{} \
                   where length(ent) {} 5 and \
                         valor > 0".format(DATABASE, KHALIX_TB, param)
            u.exportKhalix(sql, file)'''

    # def export_khalix(self):
    #     cols = "(
    #             ent varchar(11),
    #             ind varchar(12),
    #             motiu varchar(10),
    #             analisi varchar(3),
    #             valor int
    #             )
    #         "
    #     u.createTable(KHALIX_TB, cols, DATABASE, rm=True)
    #     upload = [k + (v,) for (k, v) in self.khalix.items()]
    #     u.listToTable(upload,     # ("GESTINF01ATD", motiu, "DEN", v)
    #                    KHALIX_TB,  # "exp_khalix_gida_atdom"
    #                    DATABASE)
    #     file = KHALIX_UP          # GIDA_atdom
    #     # for param, file in (("=", KHALIX_UP), (">", KHALIX_UBA)):
    #     cat_cond_den = "analisi = 'DEN' and (motiu = 'PALADU' or ind in ('GESTINF02ATD','GESTINF01ATD','GESTINF07ATD'))"  # noqa
    #     cat_cond_num = "analisi = 'NUM'"

    #     sql = """
    #         SELECT
    #             ind, 'Aperiodo', ent,
    #             analisi, 'NOCAT', 'NOIMP', 'POBACTUAL', 'N',
    #             sum(valor) as val
    #         FROM
    #             {db}.{tb} a
    #         WHERE {cat_cond_den}
    #         GROUP BY
    #             ind, ent, analisi
    #         HAVING
    #             sum(valor) > 0
    #         union
    #         SELECT
    #             ind, 'Aperiodo', ent,
    #             analisi, 'NOCAT', 'NOIMP', 'POBACTUAL', 'N',
    #             sum(valor) as val
    #         FROM
    #             {db}.{tb} a
    #         WHERE {cat_cond_num}
    #         GROUP BY
    #             ind, ent, analisi
    #         HAVING
    #             sum(valor) > 0
    #     """.format(db=DATABASE,
    #                tb=KHALIX_TB,
    #                cat_cond_den=cat_cond_den,
    #                cat_cond_num=cat_cond_num)
    #     u.exportKhalix(sql, file)

    def export_mensual(self):
        """."""
        cols = """(
                ind varchar(12),
                periode varchar(10),
                motiu varchar(10),
                valor int
                )
            """
        u.createTable(CORTES_TB, cols, DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in self.mensual.items()]
        u.listToTable(upload, CORTES_TB, DATABASE)

        sql = """
                SELECT
                    ind, periode, 'NUM', motiu,
                    'NOIMP', 'DIM6SET', 'N', valor
                FROM
                    {}.{} -- INNER JOIN
                    -- nodrizas.cat_centres
                    --    ON up = scs_codi
              """.format(DATABASE, CORTES_TB)
        u.exportKhalix(sql, CORTES_UP)

    def export_qc(self):
        """."""
        indicadors = ("GESTINF05ATD", "GESTINF06ATD")
        dext, = u.getOne("SELECT data_ext FROM dextraccio", "nodrizas")
        fa1m = dext - r.relativedelta(months=1)
        pactual = "A{}".format(dext.strftime("%y%m"))
        pprevi = "A{}".format(fa1m.strftime("%y%m"))
        sqls = []
        #  en la sql, cuando 'ent' es una residencia sera mas larga que 5
        #  el codigo original lo usaba para distinguir las UBAS de khalix
        for x, y in ((pactual, 'ANUAL'), (pactual, 'ACTUAL'), (pprevi, 'PREVI')):  # noqa
            sqls.append("""
                        SELECT
                            ind, '{}',
                            ent, 'DEN', '{}',
                            'TIPPROF4', sum(valor)
                        FROM {}
                        WHERE
                            ind in {} AND
                            -- length(ent) = 5 AND
                            analisi = 'DEN' AND
                            motiu in ('PALADU', 'PALPED')
                        GROUP BY
                            ind, ent
                        """.format(x, y, KHALIX_TB, indicadors))
        sqls.append("""
                    SELECT
                        ind, '{}',
                        ent, 'NUM', 'ANUAL',
                        'TIPPROF4', sum(valor)
                    FROM {}
                    WHERE
                        ind in {} AND
                        -- length(ent) = 5 AND
                        analisi = 'NUM'
                    GROUP BY
                        ind, ent
                    """.format(pactual, KHALIX_TB, indicadors))
        # aqui cambio ics_codi (br) por resi
        # !! habra que transformar a codi kalix de la residencia "-": "_"
        sqls.append("""
                    SELECT
                        ind, replace(periode, 'B', 'A'),
                        'NUM', if(replace(periode, 'B', 'A') = '{}',
                                            'ACTUAL', 'PREVI'),
                        'TIPPROF4', sum(valor)
                    FROM
                        {} a -- INNER JOIN
                        -- nodrizas.cat_centres b
                        --    on a.up = b.scs_codi
                    WHERE
                        ind in {}
                    GROUP BY
                        ind, periode
                    """.format(pactual, CORTES_TB, indicadors))
        dades = []
        for sql in sqls:
            dades.extend([("Q" + row[0],) + row[1:]
                          for row in u.getAll(sql, DATABASE)])
        cols = ["k{} varchar(12)".format(i) for i in range(6)] + ["v int"]
        u.createTable(QC_TB, "({})".format(", ".join(cols)), DATABASE, rm=True)
        u.listToTable(dades, QC_TB, DATABASE)


if __name__ == "__main__":
    GIDA()
