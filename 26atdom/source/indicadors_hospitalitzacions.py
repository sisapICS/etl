# -*- coding: utf-8 -*-
# Indicadors resid�ncies i hospitalitzacions
import collections as c
import sisapUtils as u
import sisaptools as t
import datetime as d
from dateutil.relativedelta import relativedelta
DEXTD = t.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]
# data de fa exactament 1 any
# DEXTD = d.date(2023,5,31) 
LAST12 = DEXTD - relativedelta(years=1)
menys30 = DEXTD - relativedelta(days=30)
class Indicadors():
    def __init__(self):
        u.printTime('inici')
        self.get_converters()
        u.printTime('converters')
        self.get_atdom()
        u.printTime('get_atdom')
        self.get_atdom_ass()
        u.printTime('get_atdom_ass')
        self.get_hospitalitzats_exitus()
        u.printTime('get_hospitalitzats_exitus')
        self.get_ingres_intermedia()
        u.printTime('get_ingres_intermedia')
        self.get_visites_urgencies()
        u.printTime('get_visites_urgencies')
        self.get_indicadors()
        u.printTime('get_indicadors')
        self.uploading()
        u.printTime('done')
    
    def get_ingres_intermedia(self):
        sql = """
            SELECT nia, dies_est
            FROM dwcatsalut.cmbd_ss
            WHERE d_ingres >= DATE '{LAST12}'
        """.format(LAST12=LAST12)
        dies_ingres = c.Counter()
        self.intermedia = c.Counter()
        self.ingres_intermedia = c.Counter()
        for nia, dies in u.getAll(sql, 'exadata'):
            if nia is not None and int(nia) in self.nia_id:
                cip = self.nia_id[int(nia)]
                dies_ingres[cip] += dies
                self.ingres_intermedia[cip] += 1
        for cip in self.dies_hospitalitzats:
            self.intermedia[cip] += self.dies_hospitalitzats[cip]
        
        for cip in dies_ingres:
            self.intermedia[cip] += dies_ingres[cip]
        for cip in self.intermedia:
            self.intermedia[cip] = 365 - self.intermedia[cip]
    
    def get_converters(self):
        sql = """
            SELECT id_cip, id_cip_sec, hash_d
            FROM u11
        """
        hash_id = {}
        self.id_cips = {}
        # self.conversor = c.defaultdict(set)
        self.exitus_generals = set()
        for id_cip, id, hash_r in u.getAll(sql, 'import'):
            hash_id[hash_r] = id_cip
            self.id_cips[id] = id_cip
            # self.conversor[id_cip].add(id)
        u.printTime('u11')
        sql = """
            SELECT hash_redics, hash_covid, nia
            FROM dwsisap.pdptb101_relacio_nia
        """
        self.covid_id = {}
        self.nia_id = {}
        for hash_r, hash_c, nia in u.getAll(sql, 'exadata'):
            try:
                self.covid_id[hash_c] = hash_id[hash_r]
                self.nia_id[nia] = hash_id[hash_r]
            except Exception as e:
                continue
        u.printTime('pdptb101')

        sql = """SELECT nia
                FROM dwsisap.RCA_CIP_NIA 
                WHERE situacio = 'D'
                AND DATA_DEFUNCIO > date '{LAST12}'
            """.format(LAST12=LAST12)
        for nia, in u.getAll(sql, 'exadata'):
            try:
                id = self.nia_id[nia]
                self.exitus_generals.add(id)
            except KeyError:
                continue
    
    def get_atdom_ass(self):
        # atdom_ubas = set()  # {id: {'M':(up, uba), 'I':(up, uba)},}
        self.atdom_ass = c.defaultdict(dict)
        self.pob_ass = c.defaultdict(dict)
        sql = """
            SELECT
                id_cip,
                id_cip_sec,
                up, uba,
                upinf, ubainf,
                edat, institucionalitzat
            FROM assignada_tot
        """
        # filia = c.defaultdict(lambda: c.defaultdict())
        for id_cip, id, up, uba, upinf, ubainf, edat, inst in u.getAll(sql, "nodrizas"):
            if inst == 0:
                self.pob_ass[id]['M'] = (up, uba)
                self.pob_ass[id]['I'] = (upinf, ubainf)
                self.pob_ass[id]['UP'] = up
                self.pob_ass[id]['edat'] = edat
            ubas = []
            if id_cip in self.atdom:
                self.atdom_ass[id_cip]['UP'] = up
                if up and uba:
                    ubas.append((up, uba, 'M'))  # filia[id]['M'] = (up, uba)
                if upinf and ubainf:
                    ubas.append((upinf, ubainf, 'I'))  # filia[id]['I'] = (upinf, ubainf)  # noqa
                self.atdom_ass[id_cip]['ubas'] = ubas
    def get_atdom(self):
        """ atdoms """
        sql = """
            SELECT id_cip_sec
            FROM eqa_problemes
            WHERE
                ps = 45 AND
                dde <= DATE '{menys30}'
        """.format(menys30=menys30)
        atdom = set()
        for id, in u.getAll(sql, 'nodrizas'):
            try:
                id_cip = self.id_cips[id]
                atdom.add(id_cip)
            except Exception as e:
                continue
        # atdom = set([id for id, in u.getAll(sql, 'nodrizas')])
        residents = set([id for id, in u.getAll("""
            SELECT id_cip FROM assignada_tot WHERE up_residencia <> ''
            """,
            "nodrizas")])
        self.atdom = atdom - residents
    def get_hospitalitzats_exitus(self):
        self.visitats_urgencies_hospital = set()
        # agafo els pacients que han estat hospitalitzats
        # els dies que han estat ingressat i si han sigut �xitus o no
        sql = """SELECT nia,
                data_alta - data_ingres AS dies_hosp,
                CASE WHEN C_ALTA = '6' THEN 1 ELSE 0 END exitus,
                data_ingres,
                CASE WHEN t_act = '134' AND c_alta = '1' THEN 1 ELSE 0 END urg
                FROM dwcatsalut.tf_cmbdha
                WHERE nia IS NOT NULL
                AND data_alta > DATE '{LAST12}'
                AND ser_alta <> '40101'
                AND pr_ingres <> '7'""".format(LAST12=LAST12)
                # AND t_act = '11'""".format(LAST12=LAST12)
        self.dies_hospitalitzats = c.Counter()
        self.hosp = set()
        ingressos = c.defaultdict(set)
        self.exitus_hosp = set()
        self.ingressos_hosp = c.Counter()
        for nia, dies_hosp, exitus, ingres, urg in u.getAll(sql, 'exadata'):
            if nia in self.nia_id:
                cip = self.nia_id[nia]
                if cip in self.atdom_ass:
                    if urg:
                        self.visitats_urgencies_hospital.add(cip)
                    else:
                        self.dies_hospitalitzats[cip] += dies_hosp
                        ingressos[cip].add(ingres)
                        self.ingressos_hosp[cip] += 1
                        if exitus == 1:
                            self.exitus_hosp.add(cip)
                            self.exitus_generals.add(cip)

        # u.printTime('ingressos_hosp ' + str(len(self.ingressos_hosp)))
        u.printTime('tf_cmbdha')
        for id in ingressos:
            dates = sorted(list(ingressos[id]))
            for i in range(0, len(dates)-1):
                if (dates[i+1] - dates[i]).days <= 30:
                    self.hosp.add(id)
    def get_visites_urgencies(self):
        #self.visitats_urgencies = set()
        
        self.visitats_urgencies_altres = set()
        # prenc els que s'han visitat a un CUAP en els �ltims 12 mesos
        sql = """SELECT pacient FROM dwsisap.sisap_master_visites
                WHERE data > DATE '{LAST12}'
                AND up IN (SELECT up_cod FROM dwsisap.dbc_rup 
                WHERE subtip_cod = '24')""".format(LAST12=LAST12)
        for hash_c, in u.getAll(sql, 'exadata'):
            if hash_c in self.covid_id:
                cip = self.covid_id[hash_c]
                if cip in self.atdom_ass:
                    self.visitats_urgencies_altres.add(cip)
        u.printTime('master_visites')
        # prenc els que s'han visitat a urg�ncies d'un hospital en els �ltims 12 mesos
        sql = """SELECT c_nia FROM dwsisap.cmbd_urg
                WHERE c_nia != 99999999
                AND c_data_entr > DATE '{LAST12}'""".format(LAST12=LAST12)
        for nia, in u.getAll(sql, 'exadata'):
            if nia in self.nia_id:
                cip = self.nia_id[nia]
                if cip in self.atdom_ass:
                    self.visitats_urgencies_hospital.add(cip)
    
    def get_indicadors(self):
        self.upload = c.Counter()
        self.pacients = c.defaultdict()
        self.ubas = c.Counter()
        # indicador 1
        # Mitjana de dies d�ingres en un centre hospitalari o sociosanitari 
        # de les persones que viuen en una resid�ncia de gent gran
        # indicador 2
        # Percentatge d'�xitus durant l�ingr�s hospitalari o sociosanitari 
        # de la poblaci� institucionalitzada en resid�ncia gent gran
        # indicador 3
        # Percentatge de mortalitat de les persones que viuen en una 
        # resid�ncia de gent gran (grups usuaris, institucionalitzats).
        # indicador 4
        # Taxa d�urg�ncies hospital�ries o CUAP o PAC de les persones que 
        # viuen en una resid�ncia de gent gran (grups usuaris, institucionalitzats).
        self.visitats_urgencies = self.visitats_urgencies_altres.union(self.visitats_urgencies_hospital)
        inds = {
            'ATD078A': [self.visitats_urgencies, None, None],
            'ATD078AA': [self.visitats_urgencies_hospital, None, None],
            'ATD078AB': [self.visitats_urgencies_altres, None, None],
            'ATD079A': [self.dies_hospitalitzats, None, None],
            'ATD080A': [self.hosp, None, None],
            'ATD062A': [self.ingressos_hosp, None, 1],
            'ATD081A': [self.intermedia, None, 1],
            'ATD082A': [self.dies_hospitalitzats, self.dies_hospitalitzats, 1],
            'ATD083A': [self.exitus_hosp, self.dies_hospitalitzats, None],
            'ATD084A': [self.ingres_intermedia, None, None],
            'ATD085A': [self.ingres_intermedia, None, 1],
            'ATD086A': [self.exitus_generals, None, None]
        }

        for ind in inds:
            num, den, val = inds[ind]
            for id in self.atdom_ass:
                # for cip in self.conversor[id]:
                in_num = 0
                pob_den = den if den is not None else self.atdom_ass
                up = self.atdom_ass[id]['UP']
                ubas = self.atdom_ass[id]['ubas']
                if id in pob_den:
                    self.upload[(ind, up, 'DEN')] += 1
                    if id in num:
                        in_num = 1
                        if val:
                            self.upload[(ind, up, 'NUM')] += num[id]
                        else:
                            self.upload[(ind, up, 'NUM')] += 1
                    elif ind == 'ATD081A':
                        self.upload[(ind, up, 'NUM')] += 365
                for (upinf, ubainf, tipus) in ubas:
                    if id in pob_den:
                        self.ubas[(ind, up, ubainf, tipus, 'DEN')] += 1
                        if id in num:
                            in_num = 1
                            if val:
                                self.ubas[(ind, up, ubainf, tipus, 'NUM')] += num[id]
                            else:
                                self.ubas[(ind, up, ubainf, tipus, 'NUM')] += 1
                        elif ind == 'ATD081A':
                            self.ubas[(ind, up, ubainf, tipus, 'NUM')] += 365
                        # self.pacients[(cip, ind)] = in_num

    
    def uploading(self):
        up = [(key[0], key[1], key[2], val) for (key, val) in self.upload.items()]
        cols = '(indi varchar(10), up varchar(10), analisi varchar(3), val int)'
        u.createTable('hospitalitzacions_atdom', cols, 'atdom', rm = True)
        u.listToTable(up, 'hospitalitzacions_atdom', 'atdom')
        # self.ubas[(ind, up, ubainf, tipus, 'DEN')] += 1
        up = [(key[0], key[1], key[2], key[3], key[4], val) for (key, val) in self.ubas.items()]
        cols = '(indi varchar(10), up varchar(10), uba varchar(10), tipus varchar(2), analisi varchar(3), val int)'
        u.createTable('hospitalitzacions_atdom_uba', cols, 'atdom', rm = True)
        u.listToTable(up, 'hospitalitzacions_atdom_uba', 'atdom')
        # pac = []
        # for (id, ind) in self.pacients:
        #     pac.append((id, ind, self.pacients[(id, ind)]))
        # u.listToTable(pac, 'pacients_atdom', 'atdom')
        # cols = '(id_cip_sec int, indi varchar(10), up varchar(10), numerador int)'
        # u.createTable('hospitalitzacions_atdom_llistat', cols, 'atdom', rm = True)
        # u.listToTable(pac, 'hospitalitzacions_atdom_llistat', 'atdom')
        u.execute("""ALTER TABLE atdom.hospitalitzacions_atdom MODIFY up VARCHAR(10)
                        CHARACTER SET latin1 COLLATE latin1_general_ci""", 'atdom')
        # sql = """SELECT DISTINCT i.indi, 'Aperiodo', concat('R', replace(i.up_resi,'-','_')), i.analisi, 
        #             'NOCAT', 'NOIMP', 'POBTOTAL', 'N', i.val
        #             FROM resis.nous_indicadors_resis i, import.cat_sisap_map_residencies c
        #             where i.up_resi = c.resi_cod and c.sisap_class = 1
        #         UNION
        #         SELECT DISTINCT i.indi, 'Aperiodo', concat('R', replace(i.up_resi,'-','_')), i.analisi, 
        #             'NOCAT', 'NOIMP', 'POBACTUAL', 'N', i.val
        #             FROM resis.nous_indicadors_resis i, import.cat_sisap_map_residencies c
        #             where i.up_resi = c.resi_cod and c.sisap_class = 1"""
        # u.exportKhalix(sql, 'RESHOSP')
if __name__ == "__main__":
    Indicadors()