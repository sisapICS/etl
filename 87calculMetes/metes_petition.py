import socket
import json


def send_petition(d):
    d = [d, 'fi_de_la_transmissio']
    HOST = '10.80.217.68'
    PORT = 50009
    packet_length = 4096
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    data_json = json.dumps(d)
    s.sendall(data_json)

    received = ''
    while True:
        packet = s.recv(packet_length)
        if not packet:
            break
        received += packet
        if '"fi_de_la_transmissio"]' in received:
            break

    received_json = json.loads(received)
    s.close()
    return received_json


go = False
while not go:
    file = raw_input("nom del fitxer d'entrada (sense .TXT): ")
    try:
        open('{}.txt'.format(file))
        go = True
    except:
        print '{}.TXT no existeix'.format(file)

print send_petition({'fx': 'metes', 'file': file})
