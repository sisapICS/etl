# coding: utf8

"""
Procés d'obtenció de les dades necessàries per a l'estudi de les variables
relacionades amb absentisme a nivell de pacient
"""

import urllib as w
import os
import sys, datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

db = "test"


class Absentisme(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_hash()
        self.get_poblacio()
        self.get_gma()
        self.get_medea()
        self.get_institucionalitzats()
        self.get_atdom()
        self.get_maca()
        self.get_visites()
        self.get_dades()
        self.export_dades_pacient()
        
    def get_hash(self):
        """Conversió ID-HASH."""
        sql = ("select id_cip_sec, hash_d, codi_sector from u11", "import")
        self.hash = {(hashs, sector):(id) for id, hashs, sector 
                    in u.getAll(*sql)}
         
    def get_poblacio(self):
        """Obtenim edat i sexe pacients."""
        sql = ("select id_cip_sec, usua_data_naixement, usua_sexe, usua_nacionalitat\
                from assignada","import")
        self.pob = {(id): (naix, sexe, nac) for (id, naix, sexe, nac) 
                    in u.getAll(*sql)}
                    
    def get_gma(self):
        """Obté els tres valors de GMA."""
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        self.gma = {(id): (cod, cmplx, num) for (id, cod, cmplx, num) 
                    in  u.getAll(*sql)}
 
    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        sql = ("select sector, valor \
                from sisap_medea", "redics")
        valors = {sector: str(valor) for (sector, valor)
                  in u.getAll(*sql)}
        self.Imedea = {}                  
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if str(sector) in valors:
                self.Imedea[(id)] = valors[sector] 

    def get_institucionalitzats(self):
        """
        Agafem els institucionalitzats de grups de tractaments.
        """
        sql = ( """
                select distinct
                    id_cip_sec, b.resi_cod, b.sisap_class
                from
                    import.institucionalitzats a inner join import.cat_sisap_map_residencies b
                        on a.codi_sector = b.sector and a.ug_codi_grup = b.grup
                where
                    b.sisap_class = 1
                """,
                "import")
        
        self.instis = {(id): institucionalitzat for (id, resi, institucionalitzat) 
                    in u.getAll(*sql)}

    def get_atdom(self):
        """
        Agafem els ATDOM de problemes like Z74
        """
        sql = ("select id_cip_sec, pr_dde from problemes  where pr_cod_o_ps = 'C' and pr_hist = 1 and year(pr_dde) <= '2017' and \
                (pr_data_baixa = 0 or year(pr_data_baixa) > '2017') and (pr_dba = 0 or year(pr_dba) > '2017') and \
                pr_cod_ps like 'Z74%'", "import")
        self.atdom = {(id): dde for id, dde in u.getAll(*sql)}
    
    def get_maca(self):
        """
        Agafem els maca tal com els comptem per EQA
        """
        sql = ("select id_cip_sec from estats where es_cod='ER0002' and year(es_dde)<= '2017' \
                union select id_cip_sec from problemes where pr_cod_ps='Z51.5' and \
                 pr_cod_o_ps = 'C' and pr_hist = 1 and year(pr_dde) <= '2017' and \
                (pr_data_baixa = 0 or year(pr_data_baixa) > '2017') and (pr_dba = 0 or year(pr_dba) > '2017')", 'import')
        self.maca = {(id): True for id,  in u.getAll(*sql)}
    
    def get_visites(self):
        """
        Agafem visites dels pacients i fetes i no fetes de sisap_absentisme
        """
        self.visites = {}
        self.data_visi = {}
        sql = "select sector, hash, visita_feta, data_visita from sisap_absentisme_visita where inclos = 1"
        for sec, hashs, feta, dvis in u.getAll(sql, db):
            self.data_visi[(sec, hashs)] = dvis
            if (sec, hashs) in self.visites:
                self.visites[(sec, hashs)]['visita'] += 1
                self.visites[(sec, hashs)]['feta'] += feta
            else:
                self.visites[(sec, hashs)] = {'visita': 1, 'feta': feta}
    
    def get_dades(self):
        """
        Agafem els pacients de la taula sisap_absentisme_visita i posem les seves característiques
        """
        self.dades = []
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        for (sector, hashs), d in self.visites.items():
            try:
                id = self.hash[(hashs, sector)]
            except KeyError:
                id = ''
            if (id) in self.pob:
                nac = self.pob[(id)][2]
            else:
                nac = ''
            if nac in renta:
                renta_baixa = 1
            else:
                renta_baixa = 0
            datav = self.data_visi[(sector, hashs)]
            try:
                naix = self.pob[(id)][0]
                edat = u.yearsBetween(naix, datav)
            except:
                edat = ''
            try:
                insti = self.instis[(id)]
            except KeyError:
                insti = 0
            atd, mac = 0, 0
            if (id) in self.atdom:
                atd = 1
            if (id) in self.maca:
                mac = 1
            sexe = self.pob[(id)][1] if (id) in self.pob else None
            cod = self.gma[(id)][0] if (id) in self.gma else None
            complex = self.gma[(id)][1] if (id) in self.gma else None
            ncronic = self.gma[(id)][2] if (id) in self.gma else None
            medea =  self.Imedea[(id)] if (id) in self.Imedea else None
            self.dades.append([id, sector, hashs, sexe, edat, renta_baixa, insti, atd, mac, cod, complex, ncronic, medea, d['visita'], d['feta']])
    
    def export_dades_pacient(self):
        """Exporta les dades de nivell PACIENT."""
        columns = ('id_cip_sec int', 'sector varchar(4)', 'hash varchar(40)', 'sexe varchar(1)','edat int', 
                    'immigracio_baixa_renta int','institucionalitzat int', 'atdom int', 'maca int', 'gma_codi varchar(3)', 'gma_complexitat double','gma_num_croniques int', 
                   'medea_seccio double' , 'visites int', 'fetes int')
        table = 'SISAP_ABSENTISME_PACIENT'
        columns_str = "({})".format(', '.join(columns))
        u.createTable(table, columns_str, db , rm=True)
        u.listToTable(self.dades, table,  db)
           
if __name__ == '__main__':
    Absentisme()
