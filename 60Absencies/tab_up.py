# coding: utf8

"""
Procés d'obtenció de les dades necessàries per a l'estudi de les variables
relacionades amb absentisme a nivell d'UP.
Les dades es refereixen a 2017, i s'obtenen de la BD de tancament de P2262.
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

class absentismeUP(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.export_dades_up()
        
    def get_centres(self):
        """EAP ICS."""
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}
    
    def export_dades_up(self):
            """
            Exporta les dades de nivell UP a partir d'un Excel de l'Albert
            de Trello (https://goo.gl/U7AJQN) que formatejo manualment i
            torno a penjar a Trello (https://goo.gl/GWx3b5).
            També afegeix l'índex ISC de l'ACQUAS a partir d'un Excel del
            Manolo que formatejo a mà i penjo a Trello (https://goo.gl/frQ3TN).
            """
            file = 'dklfj9sw8e75fpsdoj43257cfnfiwup34985nf902.txt'
            w.urlretrieve("https://goo.gl/frQ3TN", file)
            acquas = {up: isc for (up, isc) in u.readCSV(file)}
            os.remove(file)
            file = 'jdjeux1nmc423nrr8widkfosjifsjh432rehc83cv.txt'
            w.urlretrieve("https://goo.gl/GWx3b5", file)
            dades = [(up, acquas.get(up, None), atesa, assignada,
                      dispersio, docencia, medea)
                     for (up, dispersio, docencia, medea, assignada, atesa)
                     in u.readCSV(file) if up in self.centres]
            os.remove(file)
            columns = ('codi varchar(5)', 'isc double', 'atesa int',
                       'assignada int', 'dispersio varchar(2)', 'docencia int',
                       'medea varchar(2)')
            
            table = 'SISAP_ABSENTISME_UP'
            columns_str = "({})".format(', '.join(columns))
            db = "test"
            u.createTable(table, columns_str, db, rm=True)
            u.listToTable(dades, table, db)

if __name__ == '__main__':
    absentismeUP()