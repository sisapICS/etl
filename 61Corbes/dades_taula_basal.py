# coding: latin1


import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u
import sisaptools as t


class corbes_basal(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """ExecuciÃ³ seqÃ¼encial."""
        self.get_hash()
        self.get_assignada()
        self.get_historica()     
        self.export_data()
        
    def get_hash(self):
        """hash to id_cip_sec"""
        u.printTime("id_cip")
        self.id_to_hash = {}
        
        sql = """select 
                    id_cip, id_cip_sec, hash_d, codi_sector 
                from 
                    u11"""
        for id, id_sec, hash, sec in u.getAll(sql, 'import'):
            self.id_to_hash[id] = hash
       
    def get_assignada(self):
        """aconseguim """
        u.printTime("import assignada")
        self.pob_total = []

        sql = "select id_cip, usua_nacionalitat from assignada"
        for id, nac in u.getAll(sql, 'import'):
            hash = self.id_to_hash[id] if id in self.id_to_hash else None
            self.pob_total.append([hash, nac])
    
    def get_historica(self):
        """agafem població segons criteris edat"""
        u.printTime("assignada històrica")
        
        self.historica = []
        
        sql =  "select id_cip, dataany, up, ates from assignadahistorica where dataany>2012 and dataany <2020"
        for id, dany, up, ates in u.getAll(sql, 'import'):
            hash = self.id_to_hash[id] if id in self.id_to_hash else None
            self.historica.append([dany, hash, up, ates])    
    
                            
    def export_data(self):
        """."""
        
        db = "corbes"
        tb = "mst_assignada_anual"
        cols = ("dataany int", "hash varchar(40)", "up varchar(10)","ates int")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.historica, tb, db)
        
        tb = "assignada"
        cols = ("hash varchar(40)", "nacionalitat varchar(100)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pob_total, tb, db)
    

   
if __name__ == '__main__':
    u.printTime("Inici")
        
    corbes_basal()
    
    u.printTime("Fi")