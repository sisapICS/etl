# -*- coding: latin1 -*-

"""
Pes al neixer
"""

import collections as c
import psutil as p

import sisapUtils as u


tb = "mst_corbes_pesneixer"
db = "permanent"
   

class pes_neixer(object):
    """."""

    def __init__(self):
        """."""
        self.get_hash()
        self.get_pes()
        self.export()
    
    def get_hash(self):
        """id_cip  to hash"""
        u.printTime("hash")
        self.id_to_hash = {}
        
        sql = """select 
                    id_cip, codi_sector,  hash_d 
                from 
                    u11"""
        for id, sec, hash in u.getAll(sql, 'import'):
            self.id_to_hash[(id)] = hash
        
    def get_pes(self):
        """."""
        self.pesos = []
        sql = """select id_cip, codi_sector, val_val, val_data
               from NEN11 
               where val_var='PEPNA06'"""
        for id, sec, valor, data in u.getAll(sql, 'import'):
            hash = self.id_to_hash[id] if id in self.id_to_hash else None
            self.pesos.append([sec, hash, data, valor])
            

    def export(self):
        """."""
        u.printTime("export")
 
        cols = ("sector varchar(10)", "hash varchar(40)", "data date", "valor varchar(40)")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.pesos, tb, db)

   
if __name__ == '__main__':
    u.printTime("Inici")
    
    pes_neixer()

    u.printTime("Fi")