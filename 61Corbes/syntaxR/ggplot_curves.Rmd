 
```{r}
dades <- fread("../dades/corbes_sense_implausibles.txt", sep = "{", header = T)
dades <- dades[edat_m %in% c(1, 2, 4, 6, 12, 18, c(2, 4, 6, 8, 10, 12, 14)*12)]
dades <- dades[exclusio == 0]
```


```{r}
orbegozo <- readRDS("../dades/orbegozo.rds")
names(orbegozo)[4] <- "edat_m"
orbegozo[, val := as.numeric(gsub(",", ".", gsub("\\.", "", TM_VAL)))]
orbegozo <- orbegozo[TM_TVAR == "L"]
orbegozo[, sexe := TM_SEXE]
orbegozo[, sexe_f := factor(sexe, levels = c("D", "H"), labels = c("F", "M"))]
```

```{r}
library(xlsx)
who_charts_lhfa <- rbind(
  as.data.table(read.xlsx("../dades/who_stadards/tab_lhfa_girls_p_0_2.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "F"],
  as.data.table(read.xlsx("../dades/who_stadards/tab_lhfa_boys_p_0_2.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "M"],
  as.data.table(read.xlsx("../dades/who_stadards/tab_lhfa_girls_p_2_5.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "F"],
  as.data.table(read.xlsx("../dades/who_stadards/tab_lhfa_boys_p_2_5.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "M"],
  as.data.table(read.xlsx("../dades/who_stadards/hfa-girls-perc-who2007-exp.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "F"],
  as.data.table(read.xlsx("../dades/who_stadards/hfa-boys-perc-who2007-exp.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "M"]

)


who_charts_wfa <- rbind(
  as.data.table(read.xlsx("../dades/who_stadards/tab_wfa_girls_p_0_5.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "F"],
  as.data.table(read.xlsx("../dades/who_stadards/tab_wfa_boys_p_0_5.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "M"],
  as.data.table(read.xlsx("../dades/who_stadards/hfa-girls-perc-who2007-exp_6040a43e-81da-48fa-a2d4-5c856fe4fe71.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "F"],
  as.data.table(read.xlsx("../dades/who_stadards/hfa-boys-perc-who2007-exp_07eb5053-9a09-4910-aa6b-c7fb28012ce6.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "M"]

)

who_charts_hcfa <- rbind(
  as.data.table(read.xlsx("../dades/who_stadards/tab_hcfa_girls_p_0_5.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "F"],
  as.data.table(read.xlsx("../dades/who_stadards/tab_hcfa_boys_p_0_5.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "M"]

)

who_charts_bmi <- rbind(
  as.data.table(read.xlsx("../dades/who_stadards/bmi-girls-perc-who2007-exp.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "F"],
  as.data.table(read.xlsx("../dades/who_stadards/bmi-boys-perc-who2007-exp.xlsx", sheetIndex = 1))[, .(edat_m = Month, P3, P50, P97)][, sexe_f := "M"]

)
```

```{r}
carrascosa <- fread("../dades/carrascosa/tabula-S1695403308702054-0_talla_H.csv", sep = ",", header = T)
carrascosa[, sexe_f := "M"]
carrascosa[, agrupador_desc := "Talla"]
aux <- fread("../dades/carrascosa/tabula-S1695403308702054-1_pes_H.csv", sep = ",", header = T)
aux[, sexe_f := "M"]
aux[, agrupador_desc := "Pes"]

carrascosa <- rbind(
  carrascosa,
  aux
)
aux <- fread("../dades/carrascosa/tabula-S1695403308702054-2_imc_H.csv", sep = ",", header = T)
aux[, sexe_f := "M"]
aux[, agrupador_desc := "IMC"]
carrascosa <- rbind(
  carrascosa,
  aux
)
aux <- fread("../dades/carrascosa/tabula-S1695403308702054-3_talla_D.csv", sep = ",", header = T)
aux[, sexe_f := "F"]
aux[, agrupador_desc := "Talla"]
carrascosa <- rbind(
  carrascosa,
  aux
)
aux <- fread("../dades/carrascosa/tabula-S1695403308702054-4_pes_D.csv", sep = ",", header = T)
aux[, sexe_f := "F"]
aux[, agrupador_desc := "Pes"]
carrascosa <- rbind(
  carrascosa,
  aux
)
aux <- fread("../dades/carrascosa/tabula-S1695403308702054-5_imc_D.csv", sep = ",", header = T)
aux[, sexe_f := "F"]
aux[, agrupador_desc := "IMC"]
carrascosa <- rbind(
  carrascosa,
  aux
)

carrascosa <- carrascosa[, lapply(.SD, function(x){
  as.numeric(gsub(",", ".", gsub("\\.", "", x)))
}), .SDcols = 1:14, by = .(sexe_f, agrupador_desc)]
carrascosa[, edat_m := edat_a*12]
```


# Pes

```{r}
load("corbes_definitives/m_mes_D-PesD.RData")
mes_d <- corba
load("corbes_definitives/m_mes_H-PesD.RData")
mes_h <- corba
load("corbes_definitives/m_anys_D-Pes_mes.RData")
anys_d <- corba
load("corbes_definitives/m_anys_H-Pes_mes.RData")
anys_h <- corba

aux <- as.data.table(rbind(
  centiles.pred(mes_d, xvalues = 0:24, xname = "edat_m", cent = c(1, 3, 15, 50, 85, 97, 99)),
  centiles.pred(mes_h, xvalues = 0:24, xname = "edat_m", cent = c(1, 3, 15, 50, 85, 97, 99)),
  centiles.pred(anys_d, xvalues = 4:15, xname = "edat_a", cent = c(1, 3, 15, 50, 85, 97, 99)),
  centiles.pred(anys_h, xvalues = 4:15, xname = "edat_a", cent = c(1, 3, 15, 50, 85, 97, 99))
  ))

names(aux) <- c("edat", paste0("C", c(1, 3, 15, 50, 85, 97, 99)))
aux$sexe_f <- c(rep("F", 25), rep("M", 25),
              rep("F", 12), rep("M", 12))
aux[, model := c(rep("mes", 50),
                 rep("any", 24))]
aux[, edat_m := ifelse(model == "mes", edat, (edat*12))]
```

```{r}
aux_obs <- rbind(
  dades[agrupador_desc == "Pes"][, .(
    C1 = quantile(val, .01),
    C3 = quantile(val, .03),
    C15 = quantile(val, .15),
    C50 = quantile(val, .5),
    C85 = quantile(val, .85),
    C97 = quantile(val, .97),
    C99 = quantile(val, .99)
    ), .(any, edat_m, sexe_f)]
  )
aux_obs[, sexe_f := factor(sexe_f, levels = c("Femení", "Masculí"), labels = c("F", "M"))]
```


```{r fig.height=6, fig.width=10}
# tiff("", units="in", width=5, height=5, res=300)

ggplot(aux_obs[any != 2013], aes(x = edat_m, group = "edat_m")) +
# ggplot(dades[agrupador_desc == "Pes"][edat_a < 15], aes(x = edat_m, group = "edat_m")) +
  # geom_point(aes(y = val), shape = 1, size = .5, color = "grey70") +
  geom_point(aes(y = C3, color = "P3", shape = factor(any)), size = 3) +
  geom_point(aes(y = C50, color = "P50", shape = factor(any)), size = 3) +
  geom_point(aes(y = C97, color = "P97", shape = factor(any)), size = 3) +
  geom_line(data = aux, aes(y = C3, color = "P3", linetype = "ECAP")) +
  geom_line(data = aux, aes(y = C50, color = "P50", linetype = "ECAP")) +
  geom_line(data = aux, aes(y = C97, color = "P97", linetype = "ECAP")) +
  geom_line(data = who_charts_wfa, aes(y = P3, color = "P3", linetype = "WHO")) +
  geom_line(data = who_charts_wfa, aes(y = P50, color = "P50", linetype = "WHO")) +
  geom_line(data = who_charts_wfa, aes(y = P97, color = "P97", linetype = "WHO")) +
  geom_line(data = orbegozo[TM_TIPUS == "P"][TM_VAR == "P3"][edat_m<=15*12], aes(y = val, color = "P3", linetype = "Orbegozo")) +
  geom_line(data = orbegozo[TM_TIPUS == "P"][TM_VAR == "P50"][edat_m<=15*12], aes(y = val, color = "P50", linetype = "Orbegozo")) +
  geom_line(data = orbegozo[TM_TIPUS == "P"][TM_VAR == "P97"][edat_m<=15*12], aes(y = val, color = "P97", linetype = "Orbegozo")) +
  geom_smooth(data = carrascosa[agrupador_desc == "Pes"][edat_a <=15], aes(y = P3, color = "P3", linetype = "Estudio 2010"), se = F, linewidth = .5) +
  geom_smooth(data = carrascosa[agrupador_desc == "Pes"][edat_a <=15], aes(y = P50, color = "P50", linetype = "Estudio 2010"), se = F, linewidth = .5) +
  geom_smooth(data = carrascosa[agrupador_desc == "Pes"][edat_a <=15], aes(y = P97, color = "P97", linetype = "Estudio 2010"), se = F, linewidth = .5) +
  scale_x_continuous(breaks = seq(0, 12*15, 12), labels = seq(0, 15, 1)) +
  scale_y_continuous(breaks = seq(0, 110, 10)) +
  scale_color_manual(values = c("P3" = "#CED4DA", "P50" = "#6C757D", "P97" = "#212529")) +
  scale_linetype_manual(values = c("ECAP" = 1, "Estudio 2010" = 2, "WHO" = 3, "Orbegozo" = 5)) +
  theme_minimal() +
  theme(
    legend.position = "bottom",
    legend.title.position = "top",
    panel.grid.minor = element_blank(),
    # panel.background = element_rect(fill = "#F8FAF0"),
    # plot.background = element_rect(fill = "#F8FAF0")
  ) +
  guides(color = guide_legend(nrow = 1),
         shape = guide_legend(nrow = 3),
         linetype = guide_legend(ncol = 1, override.aes = list(color = "black", linewidth = .5))) +
  facet_wrap(~ sexe_f, nrow = 1) +
  labs(x = "Age (years)", y = "Wight (kg)", color = "Centiles", shape = "Year", linetype = "Curve")

# dev.off()
```

# Talla

```{r}
load("corbes_definitives/m_mes_D-TallaD.RData")
mes_d <- corba
load("corbes_definitives/m_mes_H-TallaD.RData")
mes_h <- corba
load("corbes_definitives/m_anys_D-Talla_mes.RData")
anys_d <- corba
load("corbes_definitives/m_anys_H-Talla_mes.RData")
anys_h <- corba

aux <- as.data.table(rbind(
  centiles.pred(mes_d, xvalues = 0:24, xname = "edat_m", cent = c(1, 3, 15, 50, 85, 97, 99)),
  centiles.pred(mes_h, xvalues = 0:24, xname = "edat_m", cent = c(1, 3, 15, 50, 85, 97, 99)),
  centiles.pred(anys_d, xvalues = 4:15, xname = "edat_a", cent = c(1, 3, 15, 50, 85, 97, 99)),
  centiles.pred(anys_h, xvalues = 4:15, xname = "edat_a", cent = c(1, 3, 15, 50, 85, 97, 99))
  ))

names(aux) <- c("edat", paste0("C", c(1, 3, 15, 50, 85, 97, 99)))
aux$sexe <- c(rep("D", 25), rep("H", 25),
              rep("D", 12), rep("H", 12))
aux[, model := c(rep("mes", 50),
                 rep("any", 24))]
aux[, edat_m := ifelse(model == "mes", edat, (edat*12))]
```

```{r}
aux_obs <- rbind(
  dades[agrupador_desc == "Talla"][, .(
    C1 = quantile(val, .01),
    C3 = quantile(val, .03),
    C15 = quantile(val, .15),
    C50 = quantile(val, .5),
    C85 = quantile(val, .85),
    C97 = quantile(val, .97),
    C99 = quantile(val, .99)
    ), .(any, edat_m, sexe)]
  # dades[agrupador_desc == "Talla"][edat_a>3 & edat_a <=15][, .(
  #   edat_m = edat_a*12,
  #   C1 = quantile(val, .01),
  #   C3 = quantile(val, .03),
  #   C15 = quantile(val, .15),
  #   C50 = quantile(val, .5),
  #   C85 = quantile(val, .85),
  #   C97 = quantile(val, .97),
  #   C99 = quantile(val, .99)
  #   ), .(any, edat_a, sexe)][, .(any, edat_m, sexe, C1, C3, C15, C50, C85, C97, C99)]
)

```


```{r fig.height=10, fig.width=8}
ggplot(aux_obs[any != 2013], aes(x = edat_m, group = "edat_m"), shape = 1) +
# ggplot(dades[agrupador_desc == "Talla"][edat_a < 15], aes(x = edat_m, group = "edat_m"), shape = 1) +
  # geom_point(aes(y = val), shape = 1, size = .5, color = "grey70") +
  # geom_point(aes(y = C3, color = "P3", shape = factor(any)), size = 3) +
  # geom_point(aes(y = C50, color = "P50", shape = factor(any)), size = 3) +
  # geom_point(aes(y = C97, color = "P97", shape = factor(any)), size = 3) +
  geom_line(data = aux, aes(y = C3, color = "P3", linetype = "ECAP")) +
  geom_line(data = aux, aes(y = C50, color = "P50", linetype = "ECAP")) +
  geom_line(data = aux, aes(y = C97, color = "P97", linetype = "ECAP")) +
  # geom_line(data = who_charts_lhfa[edat_m <= 15*12], aes(y = P3, color = "P3", linetype = "WHO")) +
  # geom_line(data = who_charts_lhfa[edat_m <= 15*12], aes(y = P50, color = "P50", linetype = "WHO")) +
  # geom_line(data = who_charts_lhfa[edat_m <= 15*12], aes(y = P97, color = "P97", linetype = "WHO")) +
  # geom_line(data = orbegozo[TM_TIPUS %in% "T" | TM_TIPUS %in% "L"][TM_VAR == "P3"][edat_m<=15*12], aes(y = val, color = "P3", linetype = "Orbegozo")) +
  # geom_line(data = orbegozo[TM_TIPUS %in% "T" | TM_TIPUS %in% "L"][TM_VAR == "P50"][edat_m<=15*12], aes(y = val, color = "P50", linetype = "Orbegozo")) +
  # geom_line(data = orbegozo[TM_TIPUS %in% "T" | TM_TIPUS %in% "L"][TM_VAR == "P97"][edat_m<=15*12], aes(y = val, color = "P97", linetype = "Orbegozo")) +
  geom_smooth(data = carrascosa[agrupador_desc == "Talla"][edat_a <=15], aes(y = P3, color = "P3", linetype = "Estudio 2010"), se = F) +
  geom_smooth(data = carrascosa[agrupador_desc == "Talla"][edat_a <=15], aes(y = P50, color = "P50", linetype = "Estudio 2010"), se = F) +
  geom_smooth(data = carrascosa[agrupador_desc == "Talla"][edat_a <=15], aes(y = P97, color = "P97", linetype = "Estudio 2010"), se = F) +
  scale_x_continuous(breaks = seq(0, 12*15, 12), labels = seq(0, 15, 1)) +
  scale_y_continuous(breaks = seq(0, 180, 10)) +
  scale_linetype_manual(values = c("ECAP" = 1, "Estudio 2010" = 2)) +
  theme_minimal() +
  theme(
    legend.position = "bottom",
    panel.grid.minor = element_blank(),
    # panel.background = element_rect(fill = "#F8FAF0"),
    # plot.background = element_rect(fill = "#F8FAF0")
  ) +
  guides(color = guide_legend(ncol = 1),
         shape = guide_legend(nrow = 3),
         linetype = guide_legend(ncol = 1, override.aes = list(color = "black", linewidth = .5))) +
  facet_wrap(~ sexe, nrow = 2) +
  labs(x = "Anys", y = "Talla", color = "Percentils", shape = "Any", linetype = "Corba")
```

# IMC

```{r}
load("corbes_definitives/m_anys_D-IMC_mes.RData")
anys_d <- corba
load("corbes_definitives/m_anys_H-IMC_mes.RData")
anys_h <- corba

aux <- as.data.table(rbind(
  centiles.pred(anys_d, xvalues = 4:15, xname = "edat_a", cent = c(1, 3, 15, 50, 85, 97, 99)),
  centiles.pred(anys_h, xvalues = 4:15, xname = "edat_a", cent = c(1, 3, 15, 50, 85, 97, 99))
  ))

names(aux) <- c("edat", paste0("C", c(1, 3, 15, 50, 85, 97, 99)))
aux$sexe <- c(rep("D", 12), rep("H", 12))
aux[, model := rep("any", 24)]
aux[, edat_m := ifelse(model == "mes", edat, (edat*12))]
```

```{r}
aux_obs <- rbind(
  dades[agrupador_desc == "IMC"][edat_a>3 & edat_a <=15][, .(
    edat_m,
    C1 = quantile(val, .01),
    C3 = quantile(val, .03),
    C15 = quantile(val, .15),
    C50 = quantile(val, .5),
    C85 = quantile(val, .85),
    C97 = quantile(val, .97),
    C99 = quantile(val, .99)
    ), .(any, edat_a, sexe)][, .(any, edat_m, sexe, C1, C3, C15, C50, C85, C97, C99)]
)

```


```{r fig.height=10, fig.width=8}
ggplot(aux_obs[any != 2013], aes(x = edat_m, group = "edat_m")) +
  # geom_point(aes(y = C3, color = "P3", shape = factor(any)), size = 3) +
  # geom_point(aes(y = C50, color = "P50", shape = factor(any)), size = 3) +
  # geom_point(aes(y = C97, color = "P97", shape = factor(any)), size = 3) +
  geom_line(data = aux, aes(y = C3, color = "P3", linetype = "ECAP")) +
  geom_line(data = aux, aes(y = C50, color = "P50", linetype = "ECAP")) +
  geom_line(data = aux, aes(y = C97, color = "P97", linetype = "ECAP")) +
  # geom_line(data = who_charts_bmi[edat_m <= 15*12], aes(y = P3, color = "P3", linetype = "WHO")) +
  # geom_line(data = who_charts_bmi[edat_m <= 15*12], aes(y = P50, color = "P50", linetype = "WHO")) +
  # geom_line(data = who_charts_bmi[edat_m <= 15*12], aes(y = P97, color = "P97", linetype = "WHO")) +
  # geom_line(data = orbegozo[TM_TIPUS %in% "IMC"][TM_VAR == "P3"][edat_m<=15*12], aes(y = val, color = "P3", linetype = "Orbegozo")) +
  # geom_line(data = orbegozo[TM_TIPUS %in% "IMC"][TM_VAR == "P50"][edat_m<=15*12], aes(y = val, color = "P50", linetype = "Orbegozo")) +
  # geom_line(data = orbegozo[TM_TIPUS %in% "IMC"][TM_VAR == "P97"][edat_m<=15*12], aes(y = val, color = "P97", linetype = "Orbegozo")) +
  geom_smooth(data = carrascosa[agrupador_desc == "IMC"][edat_a <=15], aes(y = P3, color = "P3", linetype = "Estudio 2010"), se = F) +
  geom_smooth(data = carrascosa[agrupador_desc == "IMC"][edat_a <=15], aes(y = P50, color = "P50", linetype = "Estudio 2010"), se = F) +
  geom_smooth(data = carrascosa[agrupador_desc == "IMC"][edat_a <=15], aes(y = P97, color = "P97", linetype = "Estudio 2010"), se = F) +
  scale_x_continuous(breaks = seq(0, 12*15, 12), labels = seq(0, 15, 1)) +
  scale_y_continuous(breaks = seq(0, 50, 2)) +
  scale_linetype_manual(values = c("ECAP" = 1, "Estudio 2010" = 2)) +
  theme_minimal() +
  theme(
    legend.position = "bottom",
    panel.grid.minor = element_blank(),
    # panel.background = element_rect(fill = "#F8FAF0"),
    # plot.background = element_rect(fill = "#F8FAF0")
  ) +
  guides(
    color = guide_legend(ncol = 1),
    shape = guide_legend(ncol = 2),
    linetype = guide_legend(ncol = 1, override.aes = list(color = "black", linewidth = .5))) +
  facet_wrap(~ sexe, nrow = 2) +
  labs(x = "Anys", y = "IMC", color = "Percentils", shape = "Any", linetype = "Corba") 
```
# Perímetre cranial

```{r}
load("corbes_definitives/m_mes_D-Perímetre cranialD.RData")
mes_d <- corba
load("corbes_definitives/m_mes_H-Perímetre cranialD.RData")
mes_h <- corba


aux <- as.data.table(rbind(
  centiles.pred(mes_d, xvalues = 0:24, xname = "edat_m", cent = c(1, 3, 15, 50, 85, 97, 99)),
  centiles.pred(mes_h, xvalues = 0:24, xname = "edat_m", cent = c(1, 3, 15, 50, 85, 97, 99))
  ))

names(aux) <- c("edat", paste0("C", c(1, 3, 15, 50, 85, 97, 99)))
aux$sexe <- c(rep("D", 25), rep("H", 25))
aux[, model := c(rep("mes", 50))]
aux[, edat_m := ifelse(model == "mes", edat, (edat*12))]
```

```{r}
aux_obs <- rbind(
  dades[agrupador_desc == "Perímetre cranial"][edat_m <=24][, .(
    C1 = quantile(val, .01),
    C3 = quantile(val, .03),
    C15 = quantile(val, .15),
    C50 = quantile(val, .5),
    C85 = quantile(val, .85),
    C97 = quantile(val, .97),
    C99 = quantile(val, .99)
    ), .(any, edat_m, sexe)]
)

```


```{r fig.height=10, fig.width=8}
ggplot(aux_obs[any != 2013], aes(x = edat_m, group = "edat_m"), shape = 1) +
  # geom_point(aes(y = C3, color = "P3", shape = factor(any)), size = 3) +
  # geom_point(aes(y = C50, color = "P50", shape = factor(any)), size = 3) +
  # geom_point(aes(y = C97, color = "P97", shape = factor(any)), size = 3) +
  geom_line(data = aux, aes(y = C3, color = "P3", linetype = "ECAP")) +
  geom_line(data = aux, aes(y = C50, color = "P50", linetype = "ECAP")) +
  geom_line(data = aux, aes(y = C97, color = "P97", linetype = "ECAP")) +
  geom_line(data = who_charts_hcfa[edat_m <= 24], aes(y = P3, color = "P3", linetype = "WHO")) +
  geom_line(data = who_charts_hcfa[edat_m <= 24], aes(y = P50, color = "P50", linetype = "WHO")) +
  geom_line(data = who_charts_hcfa[edat_m <= 24], aes(y = P97, color = "P97", linetype = "WHO")) +
  # geom_line(data = orbegozo[TM_TIPUS %in% "PC"][TM_VAR == "P3"][edat_m<=15*12], aes(y = val, color = "P3", linetype = "Orbegozo")) +
  # geom_line(data = orbegozo[TM_TIPUS %in% "PC"][TM_VAR == "P50"][edat_m<=24], aes(y = val, color = "P50", linetype = "Orbegozo")) +
  # geom_line(data = orbegozo[TM_TIPUS %in% "PC"][TM_VAR == "P97"][edat_m<=24], aes(y = val, color = "P97", linetype = "Orbegozo")) +
  scale_x_continuous(breaks = seq(0, 12*15, 1)) +
  theme_minimal() +
  theme(
    legend.position = "bottom",
    panel.grid.minor = element_blank(),
    # panel.background = element_rect(fill = "#F8FAF0"),
    # plot.background = element_rect(fill = "#F8FAF0")
  ) +
    guides(color = guide_legend(ncol = 1),
         shape = guide_legend(nrow = 3),
         linetype = guide_legend(ncol = 1)) +
  facet_wrap(~ sexe, nrow = 2) +
  labs(x = "Mesos", y = "Perímetre cranial", color = "Percentils", shape = "Any", linetype = "Corba")
```

