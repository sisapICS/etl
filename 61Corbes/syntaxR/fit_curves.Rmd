
<!-- ```{r} -->
<!-- options(knitr.kable.NA = '--') -->


<!-- ``` -->


```{r}
dades <- fread("../dades/corbes_per_fit_ics.txt", sep = "{", header = T)
```

## Corbes 0-2 anys

```{r}
sex <- "D"
mesura <- "Pes"

aux <- dades[edat_m <=24][agrupador_desc == mesura][sexe == sex][, .(edat_m, val)]
corba <- lms(val, edat_m, data = aux, trans.x = T, n.cyc = 45)
save(corba, file = paste0("m_mes_", sex, "-", mesura, "D_ICS.RData"))
```

```{r}
wp(corba, ylim.all = 1, xlim.all = 6)
wp(corba, xvar = edat_m, ylim.worm = 1, xlim.worm = 6, xcut.points = c(2, 4, 6, 12, 18), cex = .7)
```



```{r}
sex <- "D"
mesura <- "Talla"

aux <- dades[edat_m <=24][agrupador_desc == mesura][sexe == sex][, .(edat_m, val)]
corba <- lms(val, edat_m, data = aux, trans.x = T, n.cyc = 45)
save(corba, file = paste0("m_mes_", sex, "-", mesura, "D_ICS.RData"))
```

```{r}
# wp(corba, ylim.all = 1)
wp(corba, xvar = edat_m, ylim.worm = 1, xlim.worm = 6, line = TRUE)
```

```{r}
sex <- "D"
mesura <- "Perímetre cranial"

aux <- dades[edat_m <=24][agrupador_desc == mesura][sexe == sex][, .(edat_m, val)]
corba <- lms(val, edat_m, data = aux, trans.x = T, n.cyc = 45)
save(corba, file = paste0("m_mes_", sex, "-", mesura, "D_ICS.RData"))
```

```{r}
# wp(corba, ylim.all = 1)
wp(corba, xvar = edat_m, ylim.worm = 1, xlim.worm = 6, line = TRUE)
```

```{r}
sex <- "H"
mesura <- "Pes"

aux <- dades[edat_m <=24][agrupador_desc == mesura][sexe == sex][, .(edat_m, val)]
corba <- lms(val, edat_m, data = aux, trans.x = T, n.cyc = 45)
save(corba, file = paste0("m_mes_", sex, "-", mesura, "D_ICS.RData"))
```

```{r}
# wp(corba, ylim.all = 1)
wp(corba, xvar = edat_m, ylim.worm = 1, xlim.worm = 6, line = TRUE)
```

```{r}
sex <- "H"
mesura <- "Talla"

aux <- dades[edat_m <=24][agrupador_desc == mesura][sexe == sex][, .(edat_m, val)]
corba <- lms(val, edat_m, data = aux, trans.x = T, n.cyc = 45)
save(corba, file = paste0("m_mes_", sex, "-", mesura, "D_ICS.RData"))
```

```{r}
# wp(corba, ylim.all = 1)
wp(corba, xvar = edat_m, ylim.worm = 1, xlim.worm = 6, line = TRUE)
```

```{r}
sex <- "H"
mesura <- "Perímetre cranial"

aux <- dades[edat_m <=24][agrupador_desc == mesura][sexe == sex][, .(edat_m, val)]
corba <- lms(val, edat_m, data = aux, trans.x = T, n.cyc = 45)
save(corba, file = paste0("m_mes_", sex, "-", mesura, "D_ICS.RData"))
```

```{r}
# wp(corba, ylim.all = 1)
wp(corba, xvar = edat_m, ylim.worm = 1, xlim.worm = 6, line = TRUE)
```


## Corbes 4 - 14 anys

```{r}
sex <- "D"
mesura <- "Pes"

aux <- dades[edat_a > 2][agrupador_desc == mesura][sexe == sex][, .(edat_a, val)]
corba <- lms(val, edat_a, data = aux, trans.x = T, n.cyc = 45)
save(corba, file = paste0("m_anys_", sex, "-", mesura, "_mes_ICS.RData"))
```

```{r}
# wp(corba, ylim.all = 3, xlim.all = 6, cex = 0)
wp(corba, xvar = edat_a, ylim.worm = 1, xlim.worm = 6, line = TRUE)
```

```{r}
sex <- "D"
mesura <- "Talla"

aux <- dades[edat_a > 2][edat_m != 12*12][agrupador_desc == mesura][sexe == sex][, .(edat_a, val)]
corba <- lms(val, edat_a, data = aux, trans.x = T, n.cyc = 45)
save(corba, file = paste0("m_anys_", sex, "-", mesura, "_mes_ICS_2.RData"))
```

```{r}
# wp(corba, ylim.all = 1)
wp(corba, xvar = edat_a, ylim.worm = 1, xlim.worm = 6, line = TRUE)
```

```{r}
sex <- "D"
mesura <- "IMC"

aux <- dades[edat_a > 2][agrupador_desc == mesura][sexe == sex][, .(edat_a, val)]
corba <- lms(val, edat_a, data = aux, trans.x = T, n.cyc = 45)
save(corba, file = paste0("m_anys_", sex, "-", mesura, "_mes_ICS.RData"))
```

```{r}
# wp(corba, ylim.all = 1)
wp(corba, xvar = edat_a, ylim.worm = 1, xlim.worm = 6, line = TRUE)
```

```{r}
sex <- "H"
mesura <- "Pes"

aux <- dades[edat_a > 2][agrupador_desc == mesura][sexe == sex][, .(edat_a, val)]
corba <- lms(val, edat_a, data = aux, trans.x = T, n.cyc = 45)
save(corba, file = paste0("m_anys_", sex, "-", mesura, "_mes_ICS.RData"))
```

```{r}
# wp(corba, ylim.all = 1)
wp(corba, xvar = edat_a, ylim.worm = 1, xlim.worm = 6, line = TRUE)
```

```{r}
sex <- "H"
mesura <- "Talla"

aux <- dades[edat_a >2][agrupador_desc == mesura][sexe == sex][, .(edat_a, val)]
corba <- lms(val, edat_a, data = aux, trans.x = T, n.cyc = 45)
save(corba, file = paste0("m_anys_", sex, "-", mesura, "_mes_ICS.RData"))
```

```{r}
# wp(corba, ylim.all = 1)
wp(corba, xvar = edat_a, ylim.worm = 1, xlim.worm = 6, line = TRUE)
```


```{r}
sex <- "H"
mesura <- "IMC"

aux <- dades[edat_a > 2][agrupador_desc == mesura][sexe == sex][, .(edat_a, val)]
corba <- lms(val, edat_a, data = aux, trans.x = T, n.cyc = 45)
save(corba, file = paste0("m_anys_", sex, "-", mesura, "_mes_ICS.RData"))
```

```{r}
# wp(corba, ylim.all = 1)
wp(corba, xvar = edat_a, ylim.worm = 1, xlim.worm = 6, line = TRUE)
```

