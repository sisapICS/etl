source(file = "../corbes/syntax/99_funcions_corbes.R", encoding = "UTF-8")
load(file = "dt_pes.RData")

dtPes <- dt.pes.i[val>=1.5 & val <=140]

con <- gamlss.control(c.crit = 0.005, n.cyc = 40)
dtPes[,.N, sexe]
d_m <- dtPes[sexe == "H"]
model_setm_H_TOT <- lms(val, edat_s, data = d_m, trans.x = T, control = con)
d_m <- dtPes[sexe == "D"]
model_setm_D_TOT <- lms(val, edat_s, data = d_m, trans.x = T, control = con)
