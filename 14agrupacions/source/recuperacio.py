# -*- coding: utf-8 -*-

"""
.
"""

import collections as c

import sisapUtils as u
import sisaptools as t


FER_NOMES = 2022
TABLE = "exp_khalix_recvisap"
DATABASE = "agrupacions"
FILE = "RECVISAP"


class Recuperacio(object):
    """."""

    def __init__(self):
        """."""
        self.year, _y, self.month = map(int, u.getKhalixDates())
        if self.year == FER_NOMES or (self.year == (FER_NOMES + 1) and self.month < 3):  # noqa
            self.get_centres()
            self.get_dades()
            self.get_indicador()
            self.upload_table()
            self.export_lv()

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for up, br
                        in t.Database("p2262", "nodrizas").get_all(sql)}

    def get_dades(self):
        """."""
        sql = "select max(mes_contacte) from dwcatsalut.recu_dx_cmbdap \
               where any_contacte = {}".format(FER_NOMES)
        ultim = t.Database("exadata", "data").get_one(sql)[0]
        sql = "select any_contacte, mes_contacte, up, \
                      substr(c_tipus_activitat, 0, 2), n_ponderada \
               from dwcatsalut.recu_dx_cmbdap \
               where any_contacte between {} and {}".format(FER_NOMES - 1, FER_NOMES)  # noqa
        self.dades = c.Counter()
        for y, _m, up, tip, n in t.Database("exadata", "data").get_all(sql):
            if up in self.centres:
                for m in range(_m, ultim + 1):
                    self.dades[(y, m, self.centres[up], "RECVISAP" + tip)] += n

    def get_indicador(self):
        """."""
        self.upload = []
        for (y, m, br, ind), n in self.dades.items():
            if y == FER_NOMES:
                basal = self.dades.get((y - 1, m, br, ind))
                if basal:
                    periode = "A{}{}".format(str(y)[2:], str(m).zfill(2))
                    self.upload.extend([(ind, periode, br, "NUM", "NOCAT", "NOIMP", "DIM6SET", "N", n),  # noqa
                                        (ind, periode, br, "DEN", "NOCAT", "NOIMP", "DIM6SET", "N", basal)])  # noqa

    def upload_table(self):
        """."""
        cols = ["dim_{} varchar(10)".format(i) for i in range(8)]
        cols.append("v double")
        with t.Database("p2262", DATABASE) as p2262:
            p2262.create_table(TABLE, cols, remove=True)
            p2262.list_to_table(self.upload, TABLE)

    def export_lv(self):
        """."""
        u.exportKhalix("select * from {}.{}".format(DATABASE, TABLE), FILE)


if __name__ == "__main__":
    Recuperacio()
