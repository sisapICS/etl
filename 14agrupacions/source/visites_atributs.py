# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


TABLE = "exp_visites_atributs"
DATABASE = "agrupacions"
FILE = "VISITES_ATRIBUTS"

ACCOUNT = "VISAPATR"
TIPUS = {"9C": "VIS9C", "9T": "VIS9T", "9R": "VIS9R", "9D": "VIS9D",
         "9E": "VIS9E", "default": "VISALT"}
ESPECIALITATS = {"10999": "AGDMET", "30999": "AGDINF", "10888": "AGDPED",
                 "10777": "AGDODO", "10117": "AGDMET", "10116": "AGDMIR",
                 "05999": "AGDASS", "10106": "AGDODO", "30888": "AGDAIN",
                 "30085": "AGDLLE", "": "AGNOCAT", "default": "AGDALT"}
ATR_DEFAULT = "AT0000"


class Atributs(object):
    """."""

    def __init__(self):
        """."""
        self.get_cataleg()
        self.get_atributs()
        self.get_centres()
        self.get_visites()

    def get_cataleg(self):
        """."""
        self.cataleg = set()
        sql = "select rv_high_value, rv_low_value, rv_meaning \
               from cat_pritb000 \
               where codi_sector = '6734' and \
                     rv_table = 'VISTB027' and \
                     rv_column = 'ATRIBUT_{}'"
        pares = sorted([(row[0].zfill(2), row[2])
                        for row in u.getAll(sql.format(1), "import")])
        fills = sorted([(row[0].zfill(2), row[1].zfill(2), row[2])
                        for row in u.getAll(sql.format(2), "import")])
        for cod_p, des_p in pares:
            print "AT{}".format(cod_p), des_p
            has_children = False
            for cod_pp, cod_f, des_f in fills:
                if cod_p == cod_pp:
                    codi = "AT{}{}".format(cod_p, cod_f)
                    print codi, des_f
                    self.cataleg.add(codi)
                    has_children = True
            if not has_children:
                codi = "AT{}00".format(cod_p)
                print codi, "---"
                self.cataleg.add(codi)
        print ATR_DEFAULT[:4], "Sense classificar"
        print ATR_DEFAULT, "---"

    def get_atributs(self):
        """."""
        self.moduls = []
        sql = "select modu_centre_codi_centre, modu_centre_classe_centre, \
                      modu_servei_codi_servei, modu_codi_modul, \
                      ifnull(modu_act_agenda, ''), \
                      ifnull(modu_subact_agenda, '') \
               from cat_vistb027"
        self.atributs = {row[:4]: row[4:] for row in u.getAll(sql, "import")}

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}

    def get_visites(self):
        """."""
        cols = ["k{} varchar(10)".format(i) for i in range(8)] + ["v int"]
        u.createTable(TABLE, "({})".format(", ".join(cols)), DATABASE, rm=True)
        sql = "show create table visites1"
        tables = u.getOne(sql, "import")[1].split("UNION=(")[1][:-1].split(",")
        u.multiprocess(self._worker, tables)
        u.exportKhalix("select * from {}.{}".format(DATABASE, TABLE), FILE)

    def _worker(self, table):
        """."""
        visites = c.Counter()
        go = u.getOne("select year(visi_data_visita) = year(data_ext) \
                       from {}, nodrizas.dextraccio limit 1".format(table),
                      "import")[0]
        if go:
            y = u.getOne("select date_format(data_ext, '%y') from dextraccio",
                         "nodrizas")[0]
            sql = "select visi_centre_codi_centre, visi_centre_classe_centre, \
                          visi_servei_codi_servei, visi_modul_codi_modul, \
                          month(visi_data_visita), visi_up, \
                          s_espe_codi_especialitat, visi_tipus_visita \
                   from {} \
                   where visi_situacio_visita = 'R'".format(table)
            for row in u.getAll(sql, "import"):
                modul = row[:4]
                mes, up, espe_o, tipus_o = row[4:]
                if modul in self.atributs and up in self.centres:
                    br = self.centres[up]
                    atr1, atr2 = self.atributs[modul]
                    atr = "AT{}{}".format(atr1.zfill(2), atr2.zfill(2))
                    if atr not in self.cataleg:
                        atr = ATR_DEFAULT
                    period = "A{}{}".format(y, str(mes).zfill(2))
                    espe = ESPECIALITATS.get(espe_o, ESPECIALITATS["default"])
                    tipus = TIPUS.get(tipus_o, TIPUS["default"])
                    key = (ACCOUNT, period, br, espe, tipus, "NOIMP", atr, "N")
                    visites[key] += 1
            upload = [k + (v,) for (k, v) in visites.items()]
            u.listToTable(upload, TABLE, DATABASE)


if __name__ == "__main__":
    Atributs()
