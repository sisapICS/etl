# -*- coding: utf-8 -*-

"""
.
"""

import collections as c
import sisapUtils as u
import sisaptools as t

FER_NOMES = 2022
TABLE = "exp_khalix_recdemvisap"
TABLE_metes = "exp_khalix_recdemvisap_METES"
DATABASE = "agrupacions"
FILE_ind = "RECDEMORA"
FILE_metes = "RECUAP2203_meta_2022"

class RecuperacioDemora(object):
    """."""

    def __init__(self):
        """."""
        self.year, _y, self.month = map(int, u.getKhalixDates())
        if self.year == FER_NOMES or (self.year == (FER_NOMES + 1) and self.month < 3):  # noqa
            self.get_centres()
            self.indicadors()
            self.get_metes()
            self.upload_data()
            self.export_lv()  

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for up, br
                        in t.Database("p2262", "nodrizas").get_all(sql)}

    def indicadors(self):
        sql = """SELECT max(ANYMES_ANALISI)
                    FROM DWCATSALUT.RECU_DX_LLEE_ORD"""
        for m, in u.getAll(sql, 'exadata'):
            max_date = m
        self.upload = []
        sql = """SELECT EAP_CODI, sum(N_EN_LLISTA), 
                sum(N_EN_LLISTA)+sum(ES_FORA_TERMINI) 
                FROM DWCATSALUT.RECU_DX_LLEE_PREF
                WHERE ANYMES_ANALISI = {}
                GROUP BY EAP_CODI""".format(max_date)
        for up, num, den in u.getAll(sql, 'exadata'):
            self.upload.append((up, 'RECUAP2202', num, den))
        
        sql = """SELECT EAP_CODI, sum(dies_demora), 
                sum(N_EN_LLISTA)
                FROM DWCATSALUT.RECU_DX_LLEE_ORD
                WHERE ANYMES_ANALISI = {}
                GROUP BY EAP_CODI""".format(max_date)
        for up, num, den in u.getAll(sql, 'exadata'):
            self.upload.append((up, 'RECUAP2203', num, den))
        
        self.to_table = []
        for up, ind, num, den in self.upload:
            if up in self.centres:
                br = self.centres[up]
                self.to_table.append((ind, br, "DEN", "NOCAT", "NOIMP", "DIM6SET", "N", den))
                self.to_table.append((ind, br, "NUM", "NOCAT", "NOIMP", "DIM6SET", "N", num))

    def get_metes(self):
        self.metes = []
        sql = """SELECT min(ANYMES_ANALISI)
                    FROM DWCATSALUT.RECU_DX_LLEE_ORD"""
        for min, in u.getAll(sql, 'exadata'):
            min_date = min
        
        sql = """SELECT EAP_CODI, sum(dies_demora), 
                sum(N_EN_LLISTA)
                FROM DWCATSALUT.RECU_DX_LLEE_ORD
                WHERE ANYMES_ANALISI = {}
                GROUP BY EAP_CODI""".format(min_date)
        for up, num, den in u.getAll(sql, 'exadata'):
            if up in self.centres:
                br = self.centres[up]
                self.metes.append(('RECUAP2203', 'DEF2022', br, 'AGMMAX', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', num/float(den)))

    def upload_data(self):
        cols = """(indicador varchar(20), up varchar(5), analisi varchar(3),
                    nocat varchar(5), noimp varchar(5), dim6set varchar(7),
                    n varchar(1), val int)"""
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.to_table, TABLE, DATABASE)
        cols = """(indicador varchar(20), periode varchar(10), up varchar(5), analisi varchar(6),
                    nocat varchar(5), noimp varchar(5), dim6set varchar(7),
                    n varchar(1), val double)"""
        u.createTable(TABLE_metes, cols, DATABASE, rm=True)
        u.listToTable(self.metes, TABLE_metes, DATABASE)

    def export_lv(self):
        """."""
        u.exportKhalix("""select indicador, 'Aperiodo', up, analisi,
                    nocat, noimp, dim6set, n, val from {}.{}""".format(DATABASE, TABLE), FILE_ind)
        # u.exportKhalix("""select * from {}.{}""".format(DATABASE, TABLE_metes), FILE_metes)


if __name__ == "__main__":
    RecuperacioDemora()
