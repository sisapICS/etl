from sisapUtils import (getAll, multiprocess, sectors, 
                        yearsBetween, getOne, createTable, 
                        listToTable, ageConverter, exportKhalix)
import datetime as d
from collections import defaultdict

tb = 'exp_visites_exadata'
db = 'agrupacions'
file = 'VISITES_CMBDAP_exadata'


def get_data():
    centres = {up: br for (up, br) in getAll('select scs_codi, ics_codi from cat_centres', 'nodrizas')}
    linies = {uporigen: up for (up, uporigen) in getAll('select distinct up, uporigen from cat_linies', 'nodrizas')}
    dext, = getOne('select data_ext from dextraccio', 'nodrizas')
    ini = str(d.date(dext.year, 1, 1))
    fi = str(dext)
    dades = defaultdict(int)
    sql = "select floor((c_data_contacte - c_data_naixement)/365.25), c_sexe, \
            to_char(c_data_contacte, 'YYMM'), c_up_assignada, trim(c_tipus_activitat), count(1) \
            from dwcatsalut.cmbd_ap where c_data_contacte between \
            date '{}' and date '{}' \
            GROUP BY floor((c_data_contacte - c_data_naixement)/365.25), \
            to_char(c_data_contacte, 'YYMM'), c_sexe, c_up_assignada, \
            trim(c_tipus_activitat)".format(ini, fi)
    for edat, sex, dvis, up, tip, n in getAll(sql, 'exadata'):
        if up in centres and 0 <= edat <= 120:
            periode = 'A'+dvis
            # Els de pediatria poden anar a un altre cap
            if edat < 15:
                if up in linies.keys():
                    up = linies[up]
            key = ('VISAP{}'.format(tip), periode, centres[up], 'NOCLI', \
                ageConverter(edat, 5), 'NOIMP', 'DONA' \
                if sex == '1' else 'HOME', 'N')
            dades[key] += n
    listToTable([key + (n,) for (key, n) in dades.items()], tb, db)


if __name__ == '__main__':
    createTable(tb, '(a varchar(10), b varchar(10), c varchar(10), d varchar(10), e varchar(10), f varchar(10), g varchar(10), h varchar(10), n int)', db, rm=True)
    get_data()
    exportKhalix('select * from {}.{}'.format(db, tb), file)
