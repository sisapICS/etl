import collections as c

import sisapUtils as u


if u.IS_MENSUAL:
    y4, y2, m2 = u.getKhalixDates()
    if int(m2) % 3 == 0:
        t = int(m2) / 3
        periode = "A{}T{}".format(y2, t)
        casos = {}
        grups = set()
        sql = "select upper(ciap), br, edat, sexe, total \
               from exp_problemes_general where tipus = 'DIAGACTIU'"
        for grup, eap, edat, sexe, n in u.getAll(sql, "agrupacions"):
            casos[(grup, eap, edat, sexe)] = n
            grups.add(grup)
        sql = "select scs_codi, ics_codi from cat_centres"
        up_to_br = {up: br for up, br in u.getAll(sql, "nodrizas")}
        sql = "select up, edat, sexe from assignada_tot"
        poblacio = c.Counter()
        for up, edat, sexe in u.getAll(sql, "nodrizas"):
            key = (up_to_br[up], u.ageConverter(edat), u.sexConverter(sexe))
            poblacio[key] += 1
        upload = []
        for (eap, edat, sexe), pob in poblacio.items():
            for grup in grups:
                n = casos.get((grup, eap, edat, sexe), 0)
                this = (grup, periode, eap, edat, sexe, n, pob)
                upload.append(this)
        u.execute("delete epidades where periode = '{}'".format(periode), "exadata")  # noqa
        u.listToTable(upload, "epidades", "exadata")
