# coding: latin1

import sisapUtils as u


LLISTATS = 'CDSLLISTATS'
CATALEG = 'CDSCATALEG'

u.exportPDP(query="select *, '' from essencial.exp_khalix_pac where k1 != ''", table=LLISTATS, truncate=True, pkOut=True, pkIn=True)

pare = ''
hiper = 'http://10.80.217.201/sisap-umi/indicador/codi/'
upload = [("ES11", "Tractament mal indicat amb antidepressius pel episodi depressiu major lleu", 1, pare, 1, 1, 0, 0, 0, 1, hiper + "ES11"),
          ("ES14", "Tractament inadequat amb benzodiazepines per l'insomni en gent gran", 2, pare, 1, 1, 0, 0, 0, 1, hiper + "ES14"),
          ("ES15", "Densitometria mineral �ssia sense tractament farmacol�gic per prevenir fractures per fragilitat", 3, pare, 1, 1, 0, 0, 0, 1, hiper + "ES15"),
          ("ES16", "Vitamina D en persones grans en la comunitat", 4, pare, 1, 1, 0, 0, 0, 1, hiper + "ES16"),
          ("ES17", "AINE en malaltia cardiovascular, renal cr�nica o insufici�ncia hep�tica", 5, pare, 1, 1, 0, 0, 0, 1, hiper + "ES17"),
          ("ES20", "Pacients diagnosticats de vertigen inespec�fic (no recomanat)", 6, pare, 1, 1, 0, 0, 0, 1, hiper + "ES20"),
          ("ES21", "Pacients amb prescripci� de sedants vestibulars (no recomanat)", 7, pare, 1, 1, 0, 0, 0, 1, hiper + "ES21"),
          ("ES22", "Percentatge de pacients als que se li ha prescrit betahistina, sulpirida, dimenhidrinato", 8, pare, 1, 1, 0, 0, 0, 1, hiper + "ES22"),
          ("ES23", "VPPB tractats amb sedants vestibulars (no recomanat)", 9, pare, 1, 1, 0, 0, 0, 1, hiper + "ES23"),
          ("ES28", "Antibi�tics en infeccions del tracte respiratori inferior en adults (no recomanat)", 10, pare, 1, 1, 0, 0, 0, 1, hiper + "ES28"),
          ("ES29", "Antibi�tics en faringitis en adults", 11, pare, 1, 1, 0, 0, 0, 1, hiper + "ES29")
        ]
cols = """(
    INDICADOR VARCHAR(8),
    LITERAL VARCHAR(300),
    ORDRE INT,
    PARE VARCHAR(8),
    LLISTAT INT,
    INVERS INT,
    MMIN INT,
    MINT INT,
    MMAX INT,
    TOSHOW INT,
    WIKI VARCHAR(300)
    )"""
u.createTable('exp_ecap_ess_cataleg', cols, 'essencial', rm=True)
u.listToTable(upload, 'exp_ecap_ess_cataleg', 'essencial')
u.exportPDP(query='select * from essencial.exp_ecap_ess_cataleg',table=CATALEG,datAny=True,fmo=False)


