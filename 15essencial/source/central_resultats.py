
import collections as c

import sisaptools as t
import sisapUtils as u


TABLE = "central_resultats_dm"


sqls = (
      """
      select if(c.grup_codi = 'EQA0228', 'ES01',
             if(c.grup_codi = 'EQA0222', 'ES02',
             if(c.grup_codi = 'EQA0216', 'ES03', 
             if(c.grup_codi = 'EQA0313', 'ES05', c.grup_codi)))), '{0}',
             b.ics_codi, a.conc, a.edat, a.comb, a.sexe, sum(n)
      from eqa_ind.exp_khalix_up_ind a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      inner join eqa_ind.eqa_ind c on a.indicador = c.ind_codi
      where grup_codi in ('EQA0213', 'EQA0209', 'EQA0203', 'EQA0401',
                          'EQA0207', 'EQA0501', 'EQA0227', 'EQA0222',
                          'EQA0216', 'EQA0301', 'EQA0201', 'EQA0202',
                          'EQA0210', 'EQA0204', 'EQA0205', 'EQA0239',
                          'EQA0316', 'EQA0315', 'EQA0206', 'EQA0228',
                          'EQA0313') and
            comb = 'NOINSAT'
      group by if(c.grup_codi = 'EQA0228', 'ES01',
               if(c.grup_codi = 'EQA0222', 'ES02',
               if(c.grup_codi = 'EQA0216', 'ES03',
               if(c.grup_codi = 'EQA0313', 'ES05', c.grup_codi)))), '{0}',
               b.ics_codi, a.conc, a.edat, a.comb, a.sexe
      """,
      """
      select left(indicador, 7), '{0}', b.ics_codi, left(analisis, 3),
              'NOCAT', 'NOIMP', 'DIM6SET', n
      from eqa_ind.exp_khalix_up_ind_proc a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      where indicador = 'EQA3109A'
      """,
      """
      select if(indicador = 'EQA1004', 'ES06', a.indicador), '{0}', \
             b.ics_codi, a.conc, a.edat, a.comb, a.sexe, n
      from pedia.exp_khalix_up_ind a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      where indicador in ('EQA0701', 'EQA0702', 'EQA1004') and comb = 'NOINSAT'
      """,
      """
      select indicador, '{}', ics_codi, tipus, edat, comb, sexe, recompte
      from depart.exp_dep_vacunes a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      where indicador in ('DVAC004', 'DVAC006', 'DVAC035') and
            comb = 'NOINSAT'
      """,
      """
      select indicador, '{}', ics_codi, tipus, edat, comb, sexe, recompte
      from depart.exp_dep_vacunes_embarassades a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      where indicador = 'DVAC095' and comb = 'NOINSAT'
      """,
      """
      select k0, replace('{}', 'A', 'B'), k1, k5, k2, k4, k3, v
      from essencial.exp_khalix a
      where k0 in ('ES04', 'ES08', 'ES09', 'ES10', 'ES11', 'ES12', 'ES13', 
                   'ES14', 'ES15', 'ES17', 'ES18', 'ES23', 'ES24', 'ES25',
                   'ES26', 'ES27', 'ES28', 'ES29', 'ES31') and k4 = 'NOINSAT'
      """,
      """
      select indicador, '{}', ics_codi, analisi, 'NOCAT', 'NOIMP', 'DIM6SET',
             sum(valor)
      from esiap.exp_khalix a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      where indicador = 'ESIAP0401'
      group by indicador, ics_codi, analisi
      """,
      """
      select left(indicador, 9), '{}', br, analisis, 'NOCAT', 'NOIMP',
             'DIM6SET', sum(n)
      from altres.exp_khalix_up_econsulta
      where indicador in ('ECONS0000', 'ECONS0001M', 'ECONS0001I') and
            periode = 'ANUAL'
      group by left(indicador, 9), br, analisis
      """,
      """
      select 'POBATDOM', '{}', b.ics_codi, 'NUM', a.edat, 'NOIMP',
              a.sexe, sum(n)
      from eqa_ind.exp_khalix_up_ind a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      where indicador = 'EQA0401A' and conc = 'DEN' and
            comb in ('NOINSASS', 'INSASS')
      group by b.ics_codi, a.edat, a.sexe
      """,
      """
      select 'POBATDOM', '{0}', b.ics_codi, 'DEN', a.edat, 'NOIMP', \
              a.sexe, sum(ass)
      from altres.exp_khalix_pobgeneral a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      group by b.ics_codi, a.edat, a.sexe
      """,
      """
      select if(ciap in ('K86', 'K87'), 'K86K87', ciap), '{0}', br, 'NUM',
             edat, 'NOIMP', sexe, sum(total)
      from agrupacions.exp_problemes_general
      where ciap in ('T90', 'T93', 'K86', 'K87') and tipus = 'DIAGACTIU'
      group by if(ciap in ('K86', 'K87'), 'K86K87', ciap), '{0}', br, 'NUM',
               edat, 'NOIMP', sexe
      """,
      """
      select 'T90', '{0}', b.ics_codi, 'DEN', a.edat, 'NOIMP', a.sexe, sum(n)
      from eqa_ind.exp_khalix_up_pob a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      where comb like '%ASS%'
      group by 'T90', '{0}', b.ics_codi, 'DEN', a.edat, 'NOIMP', a.sexe
      """,
      """
      select 'T90', '{0}', b.ics_codi, 'DEN', a.edat, 'NOIMP', a.sexe, sum(n)
      from pedia.exp_khalix_up_pob a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      where comb like '%ASS%'
      group by 'T90', '{0}', b.ics_codi, 'DEN', a.edat, 'NOIMP', a.sexe
      """,
      """
      select 'T93', '{0}', b.ics_codi, 'DEN', a.edat, 'NOIMP', a.sexe, sum(n)
      from eqa_ind.exp_khalix_up_pob a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      where comb like '%ASS%'
      group by 'T93', '{0}', b.ics_codi, 'DEN', a.edat, 'NOIMP', a.sexe
      """,
      """
      select 'T93', '{0}', b.ics_codi, 'DEN', a.edat, 'NOIMP', a.sexe, sum(n)
      from pedia.exp_khalix_up_pob a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      where comb like '%ASS%'
      group by 'T93', '{0}', b.ics_codi, 'DEN', a.edat, 'NOIMP', a.sexe
      """,
      """
      select 'K86K87', '{0}', b.ics_codi, 'DEN', a.edat, 'NOIMP', a.sexe, sum(n)
      from eqa_ind.exp_khalix_up_pob a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      where comb like '%ASS%'
      group by 'K86K87', '{0}', b.ics_codi, 'DEN', a.edat, 'NOIMP', a.sexe
      """,
      """
      select 'K86K87', '{0}', b.ics_codi, 'DEN', a.edat, 'NOIMP', a.sexe, sum(n)
      from pedia.exp_khalix_up_pob a
      inner join nodrizas.cat_centres b on a.up = b.scs_codi
      where comb like '%ASS%'
      group by 'K86K87', '{0}', b.ics_codi, 'DEN', a.edat, 'NOIMP', a.sexe
      """,
      """
      select left(indicador, 5), '{0}', entity, analisi, edat, 'NOINSAT', sexe,
             if(analisi = 'DEN', min(valor), sum(valor))
      from altres.exp_khalix_it a
      where indicador like 'IT002%' and length(entity) = 5 and
            analisi in ('NUM', 'DEN')
      group by left(indicador, 5), '{0}', entity, analisi, edat, 'NOINSAT',
               sexe
      """,
      """
      select ind, '{0}', br, analisis, edat, poblatip, sexe e, n
      from altres.exp_khalix_benestar_emocional
      where ind in ('BEN01', 'BEN03')
      """,
      """
      select ind, '{0}', br, analisis, edat, poblatip, sexe, n
      from altres.exp_khalix_benestar_emocional_pob
      where ind = 'BENPOB04'
      """,
      """
      select ind, '{0}', br, analisis, edat, poblatip, sexe, n
      from altres.exp_khalix_nutricionistes
      where ind in ('NUT01', 'NUT03')
      """,
      """
      select ind, '{0}', br, analisis, edat, poblatip, sexe, n
      from altres.exp_khalix_nutricionistes_pob
      where ind = 'NUTPOB04'
      """,
      """
      select ind, '{0}', br, analisis, edat, poblatip, sexe, n
      from altres.exp_khalix_nutricionistes_res
      """,
      """
      select ind, '{0}', br, analisis, edat, poblatip, sexe, n
      from altres.exp_khalix_fisioterapeutes
      where ind in ('FIS01', 'FIS03')
      """,
      """
      select ind, '{0}', br, analisis, edat, poblatip, sexe, n
      from altres.exp_khalix_fisioterapeutes_pob
      where ind = 'FISPOB04'
      """,
      """
      select ind, '{0}', br, analisis, edat, poblatip, sexe, n
      from altres.exp_khalix_fisioterapeutes_res
      where ind in ('FISRES01', 'FISRES05')
      """,
      """
      select k0, k1, k2, k3, 'NOCAT', k5, 'DIM6SET', v
      from altres.exp_qc_forats
      where k0 in ('QACC10DF', 'QACC5DF') and length(k2) = 5 and k4 = 'ACTUAL'
      """,
      """
      select ind, '{0}', ent, analisi, 'NOCAT', 'NOIMP', 'DIM6SET',
             if(analisi = 'DEN',
                  sum(if(motiu in ('PALADU', 'PALPED'), valor, 0)),
                  sum(valor)) n
      from  altres.exp_khalix_gida where ind = 'GESTINF06' and length(ent) = 5
      group by ind, ent, analisi
      """,
      """
      select indicador, periode, up, analisi, nc, detalle, d1, resultat
      from altres.exp_long_cont_up where indicador = 'CONT0002'
      """,
      """
      select indicador, periode, up, analisi, nc, detalle, d1, resultat
      from altres.exp_long_cont_up_a where indicador = 'CONT0002A'
      """,
      """
      select indicador, '{0}', ics_codi, analisi, 'NOCAT', 'NOIMP', 'DIM6SET',
             sum(n)
      from altres.qc_its a
      inner join NODRIZAS.cat_centres b on a.up = scs_codi
      where indicador = 'QCITPRO' and periode = 'A'
      group by indicador, ics_codi, analisi
      """
)

periode = "A" + "".join(u.getKhalixDates()[1:])

if u.IS_MENSUAL:
    with t.Database("exadata", "data") as exadata:
        sql = "delete {0} where periode in ('{1}', replace('{1}', 'A', 'B'))".format(TABLE, periode)  # noqa
        exadata.execute(sql)
        with t.Database("p2262", "nodrizas") as p2262:
            for sql in sqls:
                dades = list(p2262.get_all(sql.format(periode)))
                if dades[0][4][:2] == "ED":
                    new = c.Counter()
                    for row in dades:
                        edat = 96 if row[4] == "ED95M" else int(row[4][2:])
                        grup = u.ageConverter(edat)
                        this = row[:4] + (grup,) + row[5:-1]
                        new[this] += row[-1]
                    dades = [k + (v,) for (k, v) in new.items()]
                exadata.list_to_table(dades, TABLE)
