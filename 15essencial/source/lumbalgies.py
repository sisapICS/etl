import sisapUtils as u
import datetime as d
import collections as c
from dateutil import relativedelta as rd

DEXTD = u.getOne("select data_ext from dextraccio", "nodrizas")[0]
menys1 = DEXTD - rd.relativedelta(years=1)
menys15 = DEXTD - rd.relativedelta(months=15)

class Bateria():
    def __init__(self):
        self.get_pob()
        self.get_lumbalgia()
        self.get_prescripcions()
        self.get_proves()
        self.get_redflags()
        self.get_antecedents()
        self.treat_dades()
        self.get_indicadors()
        self.export_khalix()

    def export_khalix(self):
        sql = """
            SELECT ind, 'Aperiodo', b.ics_codi, analisi, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', val
            FROM essencial.lumbalgies_khlx a
            INNER JOIN nodrizas.cat_centres b
            ON a.up = b.scs_codi
            WHERE up <> ''
        """
        u.exportKhalix(sql, 'PADRAQUIS')

        sql = """
            SELECT ind, 'Aperiodo', concat(b.ics_codi, tipus, uba), analisi, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', val
            FROM essencial.lumbalgies_khlx_uba a
            INNER JOIN nodrizas.cat_centres b
            ON a.up = b.scs_codi
            WHERE up <> ''
            AND uba <> ''
        """
        u.exportKhalix(sql, 'PADRAQUIS_UBA')

    def get_indicadors(self):
        res = c.Counter()
        res_uba = c.Counter()
        # pacients = []
        for id in self.pob:
            (up, uba, upinf, ubainf) = self.pob[id]
            # canviar per un dict(dict) x poder fer despres num/den*100
            res['PADRAQUIS01', up, 'DEN'] += 1
            res_uba['PADRAQUIS01', up, uba, 'M', 'DEN'] += 1
            res_uba['PADRAQUIS01', upinf, ubainf, 'I', 'DEN'] += 1
            res['PADRAQUIS03', up, 'DEN'] += 1
            res_uba['PADRAQUIS03', up, uba, 'M', 'DEN'] += 1
            res_uba['PADRAQUIS03', upinf, ubainf, 'I', 'DEN'] += 1
            # res['lumbalgia', up, up_desc, sap, sap_desc, amb, amb_desc]['DEN'] += 1
            num_i = 0
            num_r = 0
            num_l = 0
            if id in self.dades['lumb+imatge'] and id not in self.excl_final:
                num_i = 1
                res['PADRAQUIS01', up, 'NUM'] += 1
                res_uba['PADRAQUIS01', up, uba, 'M', 'NUM'] += 1
                res_uba['PADRAQUIS01', upinf, ubainf, 'I', 'NUM'] += 1
                res['PADRAQUIS02', up, 'NUM'] += 1
                res_uba['PADRAQUIS02', up, uba, 'M', 'NUM'] += 1
                res_uba['PADRAQUIS02', upinf, ubainf, 'I', 'NUM'] += 1
            if id in self.dades['lumb+relaxant'] and id not in self.excl_final:
                num_r = 1
                res['PADRAQUIS03', up, 'NUM'] += 1
                res_uba['PADRAQUIS03', up, uba, 'M', 'NUM'] += 1
                res_uba['PADRAQUIS03', upinf, ubainf, 'I', 'NUM'] += 1
                res['PADRAQUIS04', up, 'NUM'] += 1
                res_uba['PADRAQUIS04', up, uba, 'M', 'NUM'] += 1
                res_uba['PADRAQUIS04', upinf, ubainf, 'I', 'NUM'] += 1
            if id in self.lumbalgia and id not in self.excl_final:
                num_l = 1
                res['PADRAQUIS02', up, 'DEN'] += 1
                res_uba['PADRAQUIS02', up, uba, 'M', 'DEN'] += 1
                res_uba['PADRAQUIS02', upinf, ubainf, 'I', 'DEN'] += 1
                res['PADRAQUIS04', up, 'DEN'] += 1
                res_uba['PADRAQUIS04', up, uba, 'M', 'DEN'] += 1
                res_uba['PADRAQUIS04', upinf, ubainf, 'I', 'DEN'] += 1
            # pacients.append((id, up, up_desc, sap, sap_desc, amb, amb_desc, num_i, num_r, num_l))
        cols = "(ind varchar(12), up varchar(10), analisi varchar(3), val int)"
        u.createTable('lumbalgies_khlx', cols, 'essencial', rm=True)
        upload = []
        for (ind, up, analisi), val in res.items():
            upload.append((ind, up, analisi, val))
        u.listToTable(upload, 'lumbalgies_khlx', 'essencial')
        cols = "(ind varchar(12), up varchar(10), uba varchar(10), tipus varchar(1), analisi varchar(3), val int)"
        u.createTable('lumbalgies_khlx_uba', cols, 'essencial', rm=True)
        upload = []
        for (ind, up, uba, tipus, analisi), val in res_uba.items():
            upload.append((ind, up, uba, tipus, analisi, val))
        u.listToTable(upload, 'lumbalgies_khlx_uba', 'essencial')
        # cols = "(id int, up varchar(10), up_desc varchar(40), sap varchar(2), sap_desc varchar(40), ambit varchar(2), ambit_desc varchar(40), num_imatge int, num_relaxants int, num_lumbalgia int)"
        # u.createTable('lumbalgies_pac', cols, 'essencial', rm=True)
        # u.listToTable(pacients, 'lumbalgies_pac', 'essencial')

    def get_antecedents(self):
        self.antecedents = set()
        sql = """
            SELECT id_cip_sec
            FROM eqa_problemes
            WHERE ps in (415, 486, 922, 911, 425, 1046)
        """
        for id, in u.getAll(sql, 'nodrizas'):
            if id in self.pob:
                self.antecedents.add(id)

    def treat_dades(self):
        self.dades = c.defaultdict(set)
        for id in self.lumbalgia:
            for data_lumb in self.lumbalgia[id]:
                datal_mes6 = data_lumb + rd.relativedelta(weeks=6)
                if id in self.proves:
                    for dprova in self.proves[id]:
                        if data_lumb <= dprova <= datal_mes6:
                            self.dades['lumb+imatge'].add(id)
                if id in self.prescripcions:
                    for codi, dpresc in self.prescripcions[id]:
                        if data_lumb <= dpresc <= datal_mes6 and ('N05BA' not in codi or ('N05BA' in codi and id not in self.antecedents)):
                            self.dades['lumb+relaxant'].add(id)


    def get_redflags(self):
        # exclusions del numerador
        exclusions = c.defaultdict(set)
        eqa_prob = (77, 666, 667, 40, 722)

        sql = """
            SELECT id_cip_sec
            FROM eqa_problemes
            WHERE ps in {probs}
        """.format(probs=eqa_prob)
        for id, in u.getAll(sql, 'nodrizas'):
            if id in self.lumbalgia:
                self.excl_final.add(id)
        
        sql = """
            SELECT id_cip_sec
            FROM eqa_tractaments
            WHERE farmac = 41
            AND tancat = 0
        """
        for id, in u.getAll(sql, 'nodrizas'):
            if id in self.lumbalgia:
                self.excl_final.add(id)

        sql = """
            SELECT id_cip_sec, pr_cod_ps, pr_dde
            FROM {tb}
            WHERE (pr_cod_ps like '%T14.90%' -- traumatisme
            OR pr_cod_ps like '%W99.XXXA%' -- accident
            OR pr_cod_ps like '%V29.99XA%' -- acc moto
            OR pr_cod_ps like '%V99.XXXA%' -- acc transit
            OR pr_cod_ps like '%R50.9' -- febre
            OR pr_cod_ps like '%R63.4') -- pes
            AND pr_dde >= DATE '{menys1}'
        """

        for tb in u.getSubTables('problemes'):
            for id, codi, data in u.getAll(sql.format(tb=tb, menys1=menys1), 'import'):
                if id in self.lumbalgia:
                    var = ''
                    if 'T14.90' in codi or 'W99.XXXA' in codi or 'V29.99XA' in codi or 'V99.XXXA' in codi:
                        var = 'accident'
                    elif 'R50.9' in codi:
                        var = 'febre'
                    elif 'R63.4' in codi:
                        self.excl_final.add(id)
                    exclusions[id].add((var, data))
            u.printTime(tb)

        sql = """
            SELECT id_cip_sec, vu_dat_act
            FROM variables1
            WHERE vu_cod_vs = 'ET1000'
            AND vu_val > 37.9
        """
        for id, data in u.getAll(sql, 'import'):
            if id in self.lumbalgia:
                exclusions[id].add(('febre', data))
        
        for id in exclusions:
            if id in self.lumbalgia:
                excl = self.treat_excl(exclusions[id], self.lumbalgia[id])
                if excl:
                    self.excl_final.add(id)

    def treat_excl(self, set_excl, lumb):
        for (codi, data) in set_excl:
            if codi == 'accident':
                for dlumb in lumb:
                    dlumb_menys30 = dlumb - rd.relativedelta(days=30)
                    dlumb_mes30 = dlumb + rd.relativedelta(days=30)
                    if dlumb_menys30 <= data <= dlumb_mes30:
                        return 1
            if codi == 'febre':
                for dlumb in lumb:
                    dlumb_menys15 = dlumb - rd.relativedelta(days=15)
                    dlumb_mes15 = dlumb + rd.relativedelta(days=15)
                    if dlumb_menys15 <= data <= dlumb_menys15:
                        return 1
        return 0

    def get_proves(self):
        self.proves = c.defaultdict(set)
        sql = """
            SELECT id_cip_sec, oc_data
            FROM proves
            WHERE inf_codi_prova in ('RA00043', 'RA00044', 'RA00047', 'RA00048', 'RA00051', 'RA00237', 'RA00319', 'RA00321', 'RA00593')
            AND oc_data >= DATE '{menys1}'
        """.format(menys1=menys1)
        for id, data in u.getAll(sql, 'import'):
            if id in self.lumbalgia:
                self.proves[id].add(data)


    def get_prescripcions(self):
        self.prescripcions = c.defaultdict(set)
        self.excl_final = set()
        corticoides = ('H02AB01', 'H02AB02', 'H02AB04', 'H02AB05', 'H02AB06', 'H02AB07', 'H02AB08', 'H02AB09', 'H02AB10', 'H02AB13', 'H02BX91', 'H02BX92', 'H02BX93')
        sql = """
            SELECT id_cip_sec, ppfmc_atccodi, ppfmc_pmc_data_ini
            FROM {tb}
            WHERE ((ppfmc_atccodi like 'M03BA%'
            OR ppfmc_atccodi like 'M03BX%'
            OR ppfmc_atccodi like 'N05BA%')
            AND ppfmc_pmc_data_ini >= DATE '{menys1}')
            OR (ppfmc_atccodi in {corticoides}
            AND ppfmc_pmc_data_ini >= DATE '{menys15}'
            AND ppfmc_data_fi - ppfmc_pmc_data_ini > 90)
        """

        for tb in u.getSubTables('tractaments'):
            for id, codi, data in u.getAll(sql.format(tb=tb, menys1=menys1, corticoides=corticoides, menys15=menys15), 'import'):
                if id in self.pob:
                    if codi in corticoides:
                        self.excl_final.add(id)
                    else:
                        self.prescripcions[id].add((codi, data))
            u.printTime(tb)

    def get_lumbalgia(self):
        self.lumbalgia = c.defaultdict(set)
        probs = ('C01-M54.1', 'C01-M54.10', 'C01-M54.15', 'C01-M54.16', 'C01-M54.17', 'C01-M54.18', 'C01-M54.3',
                'C01-M54.30', 'C01-M54.31', 'C01-M54.32', 'C01-M54.4', 'C01-M54.40', 'C01-M54.41', 'C01-M54.42',
                'C01-M54.5', 'C01-M54.50', 'C01-M54.51', 'M54.1', 'M54.3', 'M54.4'
                )
        thes_cim10 = ('0', '1537', '1040', '1142', '1', '2')
        thes = ('400', '1420', '1421', '1422', '1423', '5370')
        sql = """
            SELECT id_cip_sec, pr_dde
            FROM {tb}
            WHERE (pr_cod_ps in {probs}
            OR (pr_cod_ps = 'C01-M54.59' AND pr_th in {thes_cim10})
            OR (pr_cod_ps = 'M54.5' AND pr_th in {thes}))
            AND pr_dde >= DATE '{menys1}'
        """

        for tb in u.getSubTables('problemes'):
            for id, data in u.getAll(sql.format(tb=tb, probs=probs, thes_cim10=thes_cim10, thes=thes, menys1=menys1), 'import'):
                if id in self.pob:
                    self.lumbalgia[id].add(data)
            u.printTime(tb)

    def get_pob(self):
        sql = "select scs_codi from cat_centres"
        centres = [up for up, in u.getAll(sql, 'nodrizas')]
        self.pob = {}
        sql = """select id_cip_sec, up, uba, upinf, ubainf from nodrizas.assignada_tot
                    where edat > 14 and institucionalitzat = 0"""
        for id, up, uba, upinf, ubainf in u.getAll(sql, 'nodrizas'):
            if up in centres:
                self.pob[id] = (up, uba, upinf, ubainf)


Bateria()