# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import os
import os.path as p
import zipfile as z

import sisapUtils as u


VALIDADORS = {("00441", "4T", "M"): "GIRIGUET",
              ("00116", "UAB-N", "M"): "LLOBERA",
              ("00276", "UAB15", "M"): "PEREZ",
              ("00167", "39752", "M"): "LLANO",
              ("00197", "PED01", "M"): "CRESPO",
              ("00281", "UAB08", "M"): "SOLER",
              ("00008", "3704", "M"): "FLORENSA",
              ("00483", "29", "M"): "MONTORO",
              ("00397", "JPM", "M"): "SOLA",
              ("04376", "MG007", "M"): "BARBERA",
              ("00397", "MCV", "M"): "RIERA",
              ("00361", "7PN03", "M"): "GOMEZ_IPARRAGUIRRE",
              ("00474", "MG11", "M"): "GOMEZ_ROIG"}
EXCLOURE = ("ES12", "ES13")


class Validacio(object):
    """."""

    def __init__(self):
        """."""
        self.get_master()
        self.export_files()

    def get_master(self):
        """."""
        self.master = c.defaultdict(set)
        sql = "select hash, up, uba, ubainf, ind, num = den from mst_pacients"
        for hash, up, uba, ubainf, ind, good in u.getAll(sql, "essencial"):
            if ind not in EXCLOURE:
                for key in ((up, uba, "M"), (up, ubainf, "I")):
                    if key in VALIDADORS:
                        prof = VALIDADORS[key]
                        self.master[(prof, ind, 1 * good)].add((hash, up, uba, ubainf))  # noqa

    def export_files(self):
        """."""
        with z.ZipFile(u.tempFolder + 'validacio.zip', 'w', allowZip64=True) as zip:  # noqa
            for key, patients in self.master.items():
                file = u.tempFolder + "{}_{}_{}.TXT".format(*key)
                u.writeCSV(file, patients, sep="@")
                zip.write(file, p.basename(file))
                os.remove(file)


if __name__ == "__main__":
    Validacio()
