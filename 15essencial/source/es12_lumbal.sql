
/*##########################################################################################################################################
ES-12  Proves d'imatge lumbalgia
INDICADOR NO CLINICO (TASA)
/*##########################################################################################################################################*/



/*CREAR DENOMINADOR*/

/*Seleccionar pacientes mayores de 14 a�os para empezar a construi el denominador*/

DROP TABLE IF EXISTS lumbal;

CREATE TABLE lumbal
select 
id_cip_sec
,up
,uba
,data_naix
,edat
,sexe
,ates
,institucionalitzat
from 
nodrizas.assignada_tot
where
edat > 14
;


/*CREAR NUMERADOR*/
set @dataext=(select data_ext from nodrizas.dextraccio);

DROP TABLE IF EXISTS rx_lumbal;

CREATE TABLE rx_lumbal

select
id_cip_sec
,oc_data
,oc_servei_ori
,inf_codi_prova
,inf_radiologia 
from nodrizas.nod_proves
where
inf_codi_prova in ('RA00043','RA00044','RA00047','RA00048','RA00593','RA00051','RA00237','RA00319','RA00321')
and (oc_data between date_add(@dataext, interval -1 year) and @dataext)
;


/*Agregar 2 columnas a lumbal*/


alter table lumbal
ADD (den DOUBLE,num DOUBLE);

/*Llenar el denominador*/
update lumbal
set den=1
;

/*Antes de actualizar hacer �ndices*/

ALTER TABLE lumbal
ADD INDEX (id_cip_sec)
;

ALTER TABLE rx_lumbal
ADD INDEX (id_cip_sec)
;

/*Uniendo directamente la tabla de rx de torax con el denominador*/
update lumbal
set num=0
;
update lumbal a 
inner join rx_lumbal b
set a.num=1 
where
a.id_cip_sec=b.id_cip_sec
;


/*A PREPARAR ARCHIVO PARA ENRIQUE PARA KHALIX*/

/*Crear columna de Cuentas que es el nombre del indicador*/
alter table lumbal add (
cuentas char(5) default 'ES12');

/*Crear columna de Periodos que es el periodo*/

alter table lumbal add (
periodos varchar(10));


DROP TABLE IF EXISTS any_lumbal;

CREATE TABLE any_lumbal
select
YEAR(data_ext) as any
,MONTH(data_ext) as mes
from nodrizas.dextraccio;

select*
from any_lumbal;

DROP TABLE IF EXISTS periodos_lumbal1;

CREATE TABLE periodos_lumbal1 (
 tst varchar(10)
)
select
if(mes<10,concat('B',SUBSTRING(any,3),'0',mes),concat('B',SUBSTRING(any,3),mes)) AS tst
from any_lumbal;

select*
from periodos_lumbal1;

UPDATE lumbal a
inner join periodos_lumbal1 b
SET a.periodos = b.tst;

select*
from lumbal;

/*Crear columna de Entidades que es la UP*/

alter table lumbal add (
entidades char(5));

UPDATE lumbal
SET entidades = up;


/*Crear columna de Tipo*/

alter table lumbal add (
tipo char(10));

DROP TABLE IF EXISTS edad_lumbal;

CREATE TABLE edad_lumbal(
 edat5 varchar(10)
)
select
id_cip_sec
,edat
,IF(edat<2,'EC01',IF(edat BETWEEN 2 AND 4,'EC24',IF(edat>=95, 'EC95M',concat('EC',TRUNCATE((edat/5),0)*5,TRUNCATE((edat/5),0)*5+4)))) AS edat5
from 
lumbal;

ALTER TABLE edad_lumbal
ADD INDEX (id_cip_sec)
;

UPDATE lumbal a
INNER JOIN edad_lumbal b
ON a.id_cip_sec=b.id_cip_sec
SET a.tipo = b.edat5 ;

/*CREAR DETALLES ESTA MAS ABAJO*/

/*Crear Controles*/

alter table lumbal add (
controls varchar(10));

DROP TABLE IF EXISTS sexe_lumbal;

CREATE TABLE sexe_lumbal(
 sexecon varchar(10)
)

select
id_cip_sec
,sexe
,IF(sexe='H','HOME',IF (sexe='M','HOME',IF(sexe='D','DONA','0')))as sexecon
from 
lumbal;

UPDATE lumbal a
INNER JOIN sexe_lumbal b
ON a.id_cip_sec=b.id_cip_sec
SET a.controls = b.sexecon;


/*N*/
alter table lumbal add (N char(1));

update lumbal
set N='N';

/*DETALLES NOTA: Aqui es importante saber que se hara una tabla para asignados y otra para atesos porque para clasificarlos como se quiere no se podria porque cada grupo no es excluyente*/

/*Para la tabla de asignados. Aqui solo hare institucionalizados y no institucionalizados*/

alter table lumbal add (
detalles varchar(10));


DROP TABLE IF EXISTS institu_lumbal;

CREATE TABLE institu_lumbal(
 detalles1 varchar(10)
)

select
id_cip_sec
,institucionalitzat
,ates
,IF(institucionalitzat=1,'INSASS',IF(institucionalitzat=0,'NOINSASS','0'))as detalles1  
from
lumbal;

select*
from institu_lumbal;

UPDATE lumbal a
INNER JOIN institu_lumbal b
ON a.id_cip_sec=b.id_cip_sec
SET a.detalles = b.detalles1;

drop table if exists pac_es12;
create table pac_es12 as select id_cip_sec id, num, den from lumbal;

