
/*##########################################################################################################################################

ES15-Densitometria mineral �ssia en persones sense tractament farmacol�gic per prevenir fractures per fragilitat
INDICADOR CL�NICO
ENFERMEDAD CR�NICA CADA FILA ES UN PACIENTE

##########################################################################################################################################*/

/*CREAR DENOMINADOR*/

/*Edad mayor de 14 a�os*/

drop table if exists edad_osteo;
create table edad_osteo
select
  id_cip_sec
  ,up
  ,uba
  ,edat
  ,sexe
  ,ates
  ,institucionalitzat
from 
  nodrizas.assignada_tot
where
  edat>14
;



/* Seleccionar pacientes con osteoporosis activos- c�digos CIE-10 wiki*/

set @dataext=(select data_ext from nodrizas.dextraccio)
;




/* Creaci�n tabla c�digos CIE-10 de diagn�stico de OSTEO..*/
drop table if exists ps_osteo;
create table ps_osteo
select
	id_cip_sec
	,pr_cod_ps
	,pr_th
	,pr_dde
	,pr_dba
from
	import.problemes
where
	pr_cod_ps in (select criteri_codi from nodrizas.eqa_criteris where agrupador = 77)
	and ((pr_dde<@dataext and pr_dba>@dataext) or (pr_dde<@dataext and pr_dba = 0)) 
;

/* Ahora vamos a hacer que cada paciente se quede con un cip y seleccionamos el primer diagn�stico*/
drop table if exists ps_osteo_ddemin;
create table ps_osteo_ddemin
select
	id_cip_sec
	, min(pr_dde) as pr_dde
from
	ps_osteo
group by
	id_cip_sec
;

/* Indice de la tabla m�s peque�a*/
alter table ps_osteo_ddemin
add index (id_cip_sec)
;

/* Unir mayores de 14 a�os con osteo*/
drop table if exists den_osteo;
create table den_osteo
select
	a.id_cip_sec
	, up
	, uba
	, edat
	, sexe
	, ates
	, institucionalitzat
	, if(b.id_cip_sec is not null,1,0) as osteo
	, pr_dde
from
	edad_osteo a
left join
	ps_osteo_ddemin b
on 
  a.id_cip_sec=b.id_cip_sec
;



/* Tabla m�s peque�ita solo con pacientes con osteoporosis*/

drop table if exists osteoporosis;
create table osteoporosis
select
	id_cip_sec
	, up
	, uba
	, edat
	, sexe
	, ates
	, institucionalitzat
	, osteo
	, pr_dde
from
	den_osteo
where
	osteo=1
;

/* Antes de hacer el indice, indexamos la tabla m�s peque�a*/

/* Indice de la tabla para m�s el inner join con prescripci�n*/
alter table osteoporosis
add index (id_cip_sec)
;

/* Ahora vamos a descargar los antiosteopor�ticos de la tabla de prescripci�n*/
drop table if exists presc_osteo;
create table presc_osteo
select
	id_cip_sec
	, ppfmc_atccodi
	, ppfmc_pmc_data_ini
	, ppfmc_data_fi
from
	import.tractaments
where
  ppfmc_atccodi in(
	  'G03XC01'
	,'G03XC02'
	,'H05AA02'
	,'H05AA03'
	,'H05BA01'
	,'H05BA04'
	,'M05BA01'
	,'M05BA02' 	
	,'M05BA04'
	,'M05BA05'
	,'M05BA06' 	
	,'M05BA07'
	,'M05BB03' 	
	,'M05BX03' 	
	,'M05BX04'
	)
;

/* Selecci�n de las prescripciones entre el DX y la Fecha de extracci�n*/
drop table if exists presc_osteo1;
create table presc_osteo1
select
	a.id_cip_sec
	, a.ppfmc_atccodi
	, a.ppfmc_pmc_data_ini
	, a.ppfmc_data_fi
from
	presc_osteo a
inner join
  osteoporosis b
on
  a.id_cip_sec=b.id_cip_sec
where
  ppfmc_pmc_data_ini between pr_dde and @dataext
;


/* Ahora vamos a hacer una columna en osteoporosis con los pacientes que tienen dx activo de osteoporosis que no han recibido antiosteoporoticos.*/

alter table osteoporosis
add (presc_osteo double)
;


update osteoporosis
set presc_osteo=0
;

update osteoporosis a 
inner join presc_osteo1 b
set a.presc_osteo=1
where
  a.id_cip_sec=b.id_cip_sec
;


drop table if exists osteoporosis_sinpresc;
create table osteoporosis_sinpresc
select
	id_cip_sec
	, up
	, uba
	, edat
	, sexe
	, ates
	, institucionalitzat
	, pr_dde
  , presc_osteo
from 
  osteoporosis
where
  presc_osteo=0
;

alter table osteoporosis_sinpresc
ADD (den DOUBLE);

update osteoporosis_sinpresc a 
set a.den=1
;

/* CREAR NUMERADOR*/
/* Coger� los c�digos de densitometria donde hay registros de acuerdo a la exploraci�n de Edu que escribo al principio de este script*/

drop table if exists densito;
create table densito
select
  id_cip_sec
  , oc_data
  , oc_servei_ori
  , inf_codi_prova
  , inf_radiologia 
from
  nodrizas.nod_proves
where
  inf_codi_prova in ('RA00097','RA00098','RA00101','RA01126')
;

/* Selecci�n de las densitometr�as entre el DX y la Fecha de extracci�n*/
/* DMO tiene que ser en los dos a�os previos a Fecha de extracci�n*/

drop table if exists densito1aux;
create table densito1aux
select
  a.id_cip_sec
  , oc_data
  , oc_servei_ori
  , inf_codi_prova
  , inf_radiologia 
from
	densito a
inner join
  osteoporosis b
on
  a.id_cip_sec=b.id_cip_sec
where
  oc_data between pr_dde and @dataext
;

drop table if exists densito1;
create table densito1
select
  id_cip_sec
  , oc_data
  , oc_servei_ori
  , inf_codi_prova
  , inf_radiologia 
from
  densito1aux
where
  oc_data between date_add(@dataext, interval -2 year) and @dataext
;


/* Indice de la tabla para m�s el inner join de densito*/
alter table densito1
add index (id_cip_sec)
;

/* Para cruzar con el denominador. De los pacientes con dx de osteoporosis que no han recibido tto con antiosteopor�ticos a cuantos se les ha realizado una densitometria*/


/* Indice de la tabla para m�s el inner join de densito*/
alter table densito1
add index (id_cip_sec)
;


alter table osteoporosis_sinpresc
add(num double)
;

update osteoporosis_sinpresc a 
set a.num=0
;


update osteoporosis_sinpresc a
inner join densito1 b
set a.num=1
where 
  a.id_cip_sec=b.id_cip_sec
;

select * from osteoporosis_sinpresc;


select
  sum(den) as denomi
  , sum(num) as numera
  , (sum(num)/sum(den))*100 as resul
from 
  osteoporosis_sinpresc
;


/*A PREPARAR ARCHIVO PARA ENRIQUE PARA KHALIX*/

/*Crear columna de Cuentas que es el nombre del indicador*/
alter table osteoporosis_sinpresc add (
cuentas char(5) default 'ES15');

/*Crear columna de Periodos que es el periodo*/

alter table osteoporosis_sinpresc add (
periodos varchar(10));


DROP TABLE IF EXISTS any_osteoporosis_sinpresc;

CREATE TABLE any_osteoporosis_sinpresc
select
YEAR(data_ext) as any
,MONTH(data_ext) as mes
from nodrizas.dextraccio;

select*
from any_osteoporosis_sinpresc;

DROP TABLE IF EXISTS periodos_osteoporosis_sinpresc1;

CREATE TABLE periodos_osteoporosis_sinpresc1 (
 tst varchar(10)
)
select
if(mes<10,concat('B',SUBSTRING(any,3),'0',mes),concat('B',SUBSTRING(any,3),mes)) AS tst
from any_osteoporosis_sinpresc;

select*
from periodos_osteoporosis_sinpresc1;

UPDATE osteoporosis_sinpresc a
inner join periodos_osteoporosis_sinpresc1 b
SET a.periodos = b.tst;

select*
from osteoporosis_sinpresc;

/*Crear columna de Entidades que es la UP*/

alter table osteoporosis_sinpresc add (
entidades char(5));

UPDATE osteoporosis_sinpresc
SET entidades = up;


/*Crear columna de Tipo*/


alter table osteoporosis_sinpresc add (
tipo char(10));

DROP TABLE IF EXISTS edad_osteoporosis_sinpresc;

CREATE TABLE edad_osteoporosis_sinpresc(
 edat5 varchar(10)
)
select
id_cip_sec
,edat
,IF(edat<2,'EC01',IF(edat BETWEEN 2 AND 4,'EC24',IF(edat>=95, 'EC95M',concat('EC',TRUNCATE((edat/5),0)*5,TRUNCATE((edat/5),0)*5+4)))) AS edat5
from 
osteoporosis_sinpresc;

ALTER TABLE edad_osteoporosis_sinpresc
ADD INDEX (id_cip_sec)
;

UPDATE osteoporosis_sinpresc a
INNER JOIN edad_osteoporosis_sinpresc b
ON a.id_cip_sec=b.id_cip_sec
SET a.tipo = b.edat5 ;

/*CREAR DETALLES ESTA MAS ABAJO*/

/*Crear Controles*/

alter table osteoporosis_sinpresc add (
controls varchar(10));

DROP TABLE IF EXISTS sexe_osteoporosis_sinpresc;

CREATE TABLE sexe_osteoporosis_sinpresc(
 sexecon varchar(10)
)

select
id_cip_sec
,sexe
,IF(sexe='H','HOME',IF(sexe='D','DONA','0'))as sexecon
from 
osteoporosis_sinpresc;

alter table sexe_osteoporosis_sinpresc
add index (id_cip_sec)
;

UPDATE osteoporosis_sinpresc a
INNER JOIN sexe_osteoporosis_sinpresc b
ON a.id_cip_sec=b.id_cip_sec
SET a.controls = b.sexecon;

/*N*/
alter table osteoporosis_sinpresc add (N char(1));

update osteoporosis_sinpresc
set N='N';

/*DETALLES NOTA: Aqui es importante saber que se hara una tabla para asignados y otra para atesos porque para clasificarlos como se quiere no se podria porque cada grupo no es excluyente*/

/*Para la tabla de asignados. Aqui solo hare institucionalizados y no institucionalizados*/

alter table osteoporosis_sinpresc add (
detalles varchar(10));


DROP TABLE IF EXISTS institu_osteoporosis_sinpresc;

CREATE TABLE institu_osteoporosis_sinpresc(
 detalles1 varchar(10)
)

select
id_cip_sec
,institucionalitzat
,ates
,IF(institucionalitzat=1,'INSASS',IF(institucionalitzat=0,'NOINSASS','0'))as detalles1  
from
osteoporosis_sinpresc;

select*
from institu_osteoporosis_sinpresc;


alter table institu_osteoporosis_sinpresc
add index (id_cip_sec)
;

UPDATE osteoporosis_sinpresc a
INNER JOIN institu_osteoporosis_sinpresc b
ON a.id_cip_sec=b.id_cip_sec
SET a.detalles = b.detalles1;

drop table if exists pac_es15;
create table pac_es15 as select id_cip_sec id, num, den from osteoporosis_sinpresc;
