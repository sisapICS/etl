# coding: latin1

import sisapUtils as u
import sisaptools as t
import datetime as d
import collections as c
from dateutil import relativedelta as rd

DEXTD = u.getOne("select data_ext from dextraccio", 'nodrizas')[0]

class DepreLleu():
    def __init__(self):
        u.printTime('inici')
        self.get_pob()
        u.printTime('get_pob')
        self.get_depre_lleu()
        u.printTime('get_depre_lleu')
        self.get_antidepre()
        u.printTime('get_antidepre')
        self.get_exclosos()
        u.printTime('get_exclosos')
        self.get_indicador()
        u.printTime('fi')

    def get_exclosos(self):
        self.exclosos = set()
        tables = u.getSubTables('problemes', 'import')
        sql = """
            SELECT id_cip_sec
            FROM {tb}
            WHERE pr_cod_ps in {data}
        """
        exclusions = [
            'C01-F20.0',
            'C01-F20.1',
            'C01-F20.2',
            'C01-F20.3',
            'C01-F20.5',
            'C01-F20.89',
            'C01-F20.9',
            'C01-F21',
            'C01-F22',
            'C01-F23',
            'C01-F24',
            'C01-F25.0',
            'C01-F25.1',
            'C01-F25.8',
            'C01-F25.9',
            'C01-F28',
            'C01-F29',
            'C01-F31.0',
            'C01-F31.10',
            'C01-F31.2',
            'C01-F31.30',
            'C01-F31.4',
            'C01-F31.5',
            'C01-F31.60',
            'C01-F31.70',
            'C01-F31.89',
            'C01-F31.9',
            'C01-F32.89',
            'C01-F39',
            'C01-F53',
            'F20',
            'F20.0',
            'F20.1',
            'F20.2',
            'F20.3',
            'F20.4',
            'F20.5',
            'F20.6',
            'F20.8',
            'F20.9',
            'F21',
            'F22',
            'F22.0',
            'F22.8',
            'F22.9',
            'F23',
            'F23.0',
            'F23.1',
            'F23.2',
            'F23.3',
            'F23.8',
            'F23.9',
            'F24',
            'F25',
            'F25.0',
            'F25.1',
            'F25.2',
            'F25.8',
            'F25.9',
            'F28',
            'F29',
            'F31',
            'F31.0',
            'F31.1',
            'F31.2',
            'F31.3',
            'F31.4',
            'F31.5',
            'F31.6',
            'F31.7',
            'F31.8',
            'F31.9',
            'F39',
            'C01-F31',
            'C01-F31.0',
            'C01-F31.10',
            'C01-F31.2',
            'C01-F31.30',
            'C01-F31.4',
            'C01-F31.5',
            'C01-F31.60',
            'C01-F31.70',
            'C01-F31.81',
            'C01-F31.89',
            'C01-F31.9',
            'F31',
            'F31.9',
            'C01-F10.10',
            'C01-F10.19',
            'C01-F10.20',
            'C01-F10.231',
            'C01-F10.239',
            'C01-F10.929',
            'C01-F10.959',
            'C01-F11.1',
            'C01-F11.11',
            'C01-F11.120',
            'C01-F11.121',
            'C01-F11.122',
            'C01-F11.129',
            'C01-F11.13',
            'C01-F11.14',
            'C01-F11.151',
            'C01-F11.188',
            'C01-F11.19',
            'C01-F11.2',
            'C01-F11.20',
            'C01-F11.23',
            'C01-F11.29',
            'C01-F12.1',
            'C01-F12.11',
            'C01-F12.120',
            'C01-F12.121',
            'C01-F12.122',
            'C01-F12.129',
            'C01-F12.13',
            'C01-F12.150',
            'C01-F12.151',
            'C01-F12.159',
            'C01-F12.180',
            'C01-F12.188',
            'C01-F12.19',
            'C01-F12.2',
            'C01-F12.20',
            'C01-F12.23',
            'C01-F12.288',
            'C01-F12.29',
            'C01-F12.90',
            'C01-F13.1',
            'C01-F13.11',
            'C01-F13.12',
            'C01-F13.120',
            'C01-F13.121',
            'C01-F13.129',
            'C01-F13.13',
            'C01-F13.130',
            'C01-F13.139',
            'C01-F13.14',
            'C01-F13.180',
            'C01-F13.181',
            'C01-F13.188',
            'C01-F13.19',
            'C01-F13.239',
            'C01-F13.29',
            'C01-F13.929',
            'C01-F14.1',
            'C01-F14.11',
            'C01-F14.120',
            'C01-F14.121',
            'C01-F14.122',
            'C01-F14.129',
            'C01-F14.13',
            'C01-F14.14',
            'C01-F14.150',
            'C01-F14.151',
            'C01-F14.159',
            'C01-F14.18',
            'C01-F14.180',
            'C01-F14.181',
            'C01-F14.182',
            'C01-F14.188',
            'C01-F14.19',
            'C01-F14.20',
            'C01-F14.229',
            'C01-F14.23',
            'C01-F14.29',
            'C01-F14.90',
            'C01-F15.19',
            'C01-F15.20',
            'C01-F15.23',
            'C01-F15.29',
            'C01-F15.929',
            'C01-F16.20',
            'C01-F16.288',
            'C01-F16.29',
            'C01-F16.929',
            'C01-F17.200',
            'C01-F18.19',
            'C01-F18.20',
            'C01-F19.11',
            'C01-F19.159',
            'C01-F19.180',
            'C01-F19.182',
            'C01-F19.188',
            'C01-F19.20',
            'C01-F19.239',
            'C01-F19.29',
            'C01-F19.90',
            'C01-F19.929',
            'C01-F19.98',
            'C01-F19.99',
            'F10',
            'F10.1',
            'F10.2',
            'F11',
            'F11.0',
            'F11.1',
            'F11.2',
            'F11.3',
            'F12',
            'F12.0',
            'F12.1',
            'F12.2',
            'F12.3',
            'F13.0',
            'F13.1',
            'F13.2',
            'F13.3',
            'F14',
            'F14.0',
            'F14.1',
            'F14.2',
            'F14.3',
            'F15.0',
            'F15.1',
            'F15.2',
            'F15.3',
            'F16.1',
            'F16.2',
            'F16.3',
            'F17',
            'F18.0',
            'F18.1',
            'F18.2',
            'F18.3',
            'F19.0',
            'F19.1',
            'F19.2',
            'F19.3',
            'C01-Z74.9',
            'Z74.9',
            'R62.0',
            'F79',
            'F79.0',
            'F79.1',
            'F79.8',
            'F79.9',
            'F54'
        ]
        jobs = [(table, sql, exclusions) for table in tables]
        for dades in u.multiprocess(sub_get_exclusions, jobs, 6, close=True):
            for id in dades:
                self.exclosos.add(id)
        u.printTime('despres multiproces problemes')
        # for id, in u.getAll(sql, 'nodrizas'):
        #     self.exclosos.add(id)
        sql = """
            SELECT id_cip_sec, agrupador, val
            FROM ts_variables
            WHERE agrupador in (862,863,864,865)
        """
        dades = c.defaultdict(set)
        for id, ps, val in u.getAll(sql, 'nodrizas'):
            dades[id].add((ps, val))
        for id in dades:
            cond11 = 0
            cond12 = 0
            cond13 = 0
            cond21 = 0
            cond22 = 0
            cond23 = 0
            cond24 = 0
            for ps, val in dades[id]:
                if ps == 862 and val >= 13 and val <= 23:
                    cond21 = 1
                elif ps == 863 and val >= 15 and val <= 19:
                    cond22 = 1
                elif ps == 864 and val >= 57:
                    cond23 = 1
                elif ps == 865 and val >= 5 and val <= 7:
                    cond24 = 1
                if ps == 862 and val >= 9 and val <= 12:
                    cond11 = 1
                elif ps == 863 and val <= 14:
                    cond12 = 1
                elif ps == 865 and val >= 8 and val <= 10:
                    cond13 = 1
            if cond11 == 1 and cond12 == 1 and cond13 == 1:
                self.exclosos.add(id)
            elif cond21 == 1 and cond22 == 1 and cond23 == 1 and cond24 == 1:
                self.exclosos.add(id)
        # 862 MMSE
        # 863 LOBO
        # 864 informador
        # 865 Pfeiffer


    def get_indicador(self):
        upload = []
        for id in self.depre:
            if id in self.pob:
                num = 0
                den = 1
                excl = 0
                up = self.pob[id]['up']
                uba = self.pob[id]['uba']
                upinf = self.pob[id]['upinf']
                ubainf = self.pob[id]['ubainf']
                if id in self.antidepre:
                    num = 1
                if id in self.exclosos:
                    den = 0
                    num = 0
                    excl = 1
                upload.append((id, up, uba, upinf, ubainf, num, den, excl))
        cols = "(id_cip_sec int, up varchar(10), uba varchar(10), upinf varchar(10), ubainf varchar(10), num int, den int, excl int)"
        u.createTable('es33_8s', cols, 'essencial', rm=True)
        u.listToTable(upload, 'es33_8s', 'essencial')

    def get_antidepre(self):
        self.antidepre = set()
        tables = u.getSubTables('tractaments', 'import')
        antidepressius = [
            'N06AA02','N06AA04','N06AA06','N06AA07','N06AA09',
            'N06AA10','N06AA12','N06AA16','N06AA17','N06AA19',
            'N06AA21','N06AB03','N06AB04','N06AB05','N06AB06',
            'N06AB08','N06AB10','N06AF04','N06AG02','N06AX01',
            'N06AX03','N06AX05','N06AX06','N06AX07','N06AX11',
            'N06AX12','N06AX14','N06AX16','N06AX18','N06AX21',
            'N06AX22','N06AX23','N06AX26','N06AA01','N06AA03',
            'N06AA05','N06AA08','N06AA11','N06AA13','N06AA14',
            'N06AA15','N06AA18','N06AA23','N06AA52','N06AB02',
            'N06AB07','N06AB09','N06AF01','N06AF02','N06AF03',
            'N06AF05','N06AF06','N06AG03','N06AX02','N06AX04',
            'N06AX08','N06AX09','N06AX10','N06AX13','N06AX15',
            'N06AX17','N06AX19','N06AX24','N06AX25','N06AX91'
        ]

        sql = """
            SELECT id_cip_sec, ppfmc_pmc_data_ini
            FROM {tb}
            WHERE ppfmc_atccodi in {data}
        """
        jobs = [(table, sql, antidepressius) for table in tables]
        for dades in u.multiprocess(sub_get_antidepre, jobs, 6, close=True):
            for id in dades:
                if id in self.depre:
                    valid = self.comparacio(self.depre[id], dades[id])
                    if valid:
                        self.antidepre.add(id)

    def comparacio(self, dde, dpresc):
        for data in dde:
            data_mes8 = data + rd.relativedelta(days=56)
            for data2 in dpresc:
                if data2 >= data and data <= data_mes8:
                    return 1
        return 0

    def get_depre_lleu(self):
        self.depre = c.defaultdict(set)
        sql = """
            SELECT id_cip_sec, dde, ps
            FROM eqa_problemes
            WHERE (ps = 792
            AND dde between adddate(DATE '{DEXTD}', interval -1 year)
            AND DATE '{DEXTD}')
            OR ps = 798
        """.format(DEXTD=DEXTD)
        depressio = c.defaultdict(set)
        for id, dde, agrupador in u.getAll(sql, 'nodrizas'):
            if agrupador == 792:
                self.depre[id].add(dde)
            else:
                depressio[id].add(dde)
        
        # sql = """
        #     SELECT id_cip_sec, data_var
        #     FROM eqa_variables
        #     WHERE agrupador = 798
        #     AND valor < 15
        # """
        # for id, data in u.getAll(sql, 'nodrizas'):
        #     if id in depressio:
        #         for dde in depressio[id]:
        #             data_menys30 = dde - rd.relativedelta(days=30)
        #             data_mes30 = dde + rd.relativedelta(days=30)
        #             if data >= data_menys30 and data <= data_mes30:
        #                 self.depre[id].add(dde)

    def get_pob(self):
        self.pob = c.defaultdict()
        sql = """
            SELECT id_cip_sec, up, uba, upinf, ubainf
            FROM assignada_tot
            WHERE edat > 15
        """
        for id, up, uba, upinf, ubainf in u.getAll(sql, 'nodrizas'):
            self.pob[id] = {
                'up': up,
                'uba': uba,
                'upinf': upinf,
                'ubainf': ubainf
            }


def sub_get_antidepre(params):
    tb, sql, antidepre = params
    dades = c.defaultdict(set)
    print(sql.format(tb=tb,data=tuple(antidepre)))
    for id, data_ini in u.getAll(sql.format(tb=tb,data=tuple(antidepre)), 'import'):
        dades[id].add(data_ini)
    print(tb)
    return dades

def sub_get_exclusions(params):
    tb, sql, exclusions = params
    dades = set()
    for id, in u.getAll(sql.format(tb=tb, data=tuple(exclusions)), 'import'):
        dades.add(id)
    print(tb)
    return dades

DepreLleu()