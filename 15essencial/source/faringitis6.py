
# coding: utf8

import sisapUtils as u
import datetime
from datetime import date
import time
from collections import defaultdict,Counter
print time.ctime()
start_time = time.time()

valida = False

profvalida = {1: {'tip' : 'M', 'up' : '00441', 'uba' : '4T'}}

any,any2,mes = u.getKhalixDates()
periodo = 'B' + str(any2) + str(mes)

centres = {}
sql = "select scs_codi, ics_codi from cat_centres"
for up, br in u.getAll(sql, 'nodrizas'):
    centres[up] = br


class atbfaringitis(object):
    
    def __init__(self):
        self.get_assig()
        self.get_faring()
        self.get_atb()
        self.get_proves()
        self.get_dadesp()
        
    def get_assig(self):
        sql = ("select id_cip_sec from eqa_problemes where ps = 40", "nodrizas")
        self.inmu = set(id for id, in u.getAll(*sql))

        sql = ("select id_cip_sec, up, uba, upinf, ubainf, ates, edat, sexe, institucionalitzat from assignada_tot where edat > 14 and ates = 1", "nodrizas")
        self.assig = {id : {'up' : up, 'uba' : uba, 'upinf' : upinf, 'ubainf' : ubainf, 'ates': ates, 'edat':edat, 'sexe':sexe, 'insti': insti} 
                        for id, up, uba, upinf, ubainf, ates, edat, sexe, insti in u.getAll(*sql) if id not in self.inmu}
        
        print "assig ", len(self.assig)
        print "--- %s seconds ---" % (time.time() - start_time)
        
    def get_faring(self):
        sql = ("select id_cip_sec, pr_dde from problemes where pr_dde > current_date - interval 1 year \
                    and pr_cod_ps in ('J02.9', 'J02.8', 'J03', 'J02', 'J03.8', 'J03.9',\
                                      'C01-J02.9','C01-J02.8','C01-J02.9','C01-J03.90','C01-J03.80','C01-J03.90')", "import")
        self.idfaring = set(id for id, datadx in u.getAll(*sql) if id in self.assig)
        self.faring = [[id, datadx] for id, datadx in u.getAll(*sql) if id in self.assig]
        
        print len(self.idfaring), len(self.faring)
        print "faring ", "--- %s seconds ---" % (time.time() - start_time)

    def get_atb(self):
        self.datb = {}
        self.idatb = set()
        sql = ("select id_cip_sec, pres_orig from eqa_tractaments where farmac = 139 or farmac = 140", "nodrizas")
        for id, fechapres in u.getAll(*sql):
            if id in self.idfaring:
                if id in self.datb:
                    self.datb[id].append(fechapres)
                else:
                    self.datb[id] = [fechapres]
                    self.idatb.add(id)
        print len(self.datb)
        print "datb ", "--- %s seconds ---" % (time.time() - start_time)

    def get_proves(self):
        self.pruebas = {}
        self.pruebas2 = {}
        self.idpruebas2 = set()
        
        #gestió d'excepció per taules buides
        try:
            for taula in u.getSubTables("variables", "import"):
                #print(taula)
                dat, = u.getOne("select vu_dat_act from {0} limit 2".format(taula), 'import')
                if date.today() - dat < datetime.timedelta(395):
                    num = 0
                    sql = ("select id_cip_sec, vu_dat_act from {0} where vu_cod_vs = 'TA5001' and vu_val = 1".format(taula), "import")
                    for id, fecha in u.getAll(*sql):
                        if id in self.datb:
                            for e in self.datb[id]:
                                if fecha - e < datetime.timedelta(7):
                                    if id in self.pruebas:
                                        self.pruebas[id].append(fecha)
                                        num +=1
                                    else:
                                        self.pruebas[id] = [fecha]
                                        num+=1
                    print "TA5001: ", num
                    print len(self.pruebas)
                    num2 = 0
                    sql = ("select id_cip_sec, vu_dat_act from {0} where vu_cod_vs = 'VA5001' \
                            and vu_val >= 2 and vu_val <= 5".format(taula), "import")
                    for id, fecha in u.getAll(*sql):
                        if id in self.pruebas:
                            for e in self.pruebas[id]:
                                if fecha - e < datetime.timedelta(7):
                                    self.pruebas2[str(id) + '@' + str(e)] = 2
                                    self.idpruebas2.add(id)
                                    num2 += 2
                    print "+VA5001: ", num2
                    print len(self.pruebas2)
        except:
            print "taula" +taula + " buida" 
        
        print len(self.pruebas), len(self.pruebas2), len(self.idpruebas2)
        print "pruebas ", "--- %s seconds ---" % (time.time() - start_time)

    def get_dadesp(self):
        cnoatb = 0
        cprueb = 0
        cnum = 0
        dadesp = []
        for f in self.faring:
            if f[0] not in self.idatb:
                dadesp.append([f[0], f[1], self.assig[f[0]]['up'], self.assig[f[0]]['uba'],
                                self.assig[f[0]]['upinf'], self.assig[f[0]]['ubainf'],
                                self.assig[f[0]]['ates'], self.assig[f[0]]['edat'], self.assig[f[0]]['sexe'],self.assig[f[0]]['insti'], 'ES29',0])
                cnoatb += 1
            else:
                countnum = 0
                countatb = 0
                for a in self.datb[f[0]]:
                    if a >= f[1] and a - f[1] < datetime.timedelta(7):
                        countatb = 1
                        if f[0] in self.idpruebas2:
                            for e in self.pruebas2:
                                idfecha = e.split('@')
                                id = int(idfecha[0])
                                if id == f[0]:
                                    fecha = datetime.datetime.strptime(idfecha[1], '%Y-%m-%d').date()
                                    if fecha >= f[1] and fecha - f[1] < datetime.timedelta(7):
                                        dadesp.append([f[0], f[1], self.assig[f[0]]['up'], self.assig[f[0]]['uba'],
                                                        self.assig[f[0]]['upinf'], self.assig[f[0]]['ubainf'],
                                                         self.assig[f[0]]['ates'],  self.assig[f[0]]['edat'], self.assig[f[0]]['sexe'],self.assig[f[0]]['insti'], 'ES29',0])
                                        countnum = 1
                                        cprueb += 1
                        if countnum == 0:
                            dadesp.append([f[0], f[1], self.assig[f[0]]['up'], self.assig[f[0]]['uba'],
                                            self.assig[f[0]]['upinf'], self.assig[f[0]]['ubainf'],
                                            self.assig[f[0]]['ates'], self.assig[f[0]]['edat'], self.assig[f[0]]['sexe'],self.assig[f[0]]['insti'], 'ES29',1])
                            cnum += 1

                if countatb == 0:
                    dadesp.append([f[0], f[1], self.assig[f[0]]['up'], self.assig[f[0]]['uba'],
                                    self.assig[f[0]]['upinf'], self.assig[f[0]]['ubainf'],
                                    self.assig[f[0]]['ates'], self.assig[f[0]]['edat'], self.assig[f[0]]['sexe'],self.assig[f[0]]['insti'], 'ES29',0])
                                
        print "dadesp: ", len(dadesp), "cnoatb: ", cnoatb, "cprueb: ", cprueb, "cnum: ", cnum
        
        tablep = "es29faringitis"
        u.createTable(tablep, "(id_cip_sec int, data date, up varchar(6), uba varchar(6), upinf varchar(6), ubainf varchar(6), \
                               ates int, edat int, sexe varchar(1), insti varchar(10), indicador varchar(7), num int)", "essencial", rm=True)
        u.listToTable(dadesp, tablep, "essencial")
        u.execute("drop table if exists pac_es29", "essencial")
        u.execute("create table pac_es29 as select id_cip_sec id, sum(num) num, count(1) den from es29faringitis group by id_cip_sec", "essencial")
        print "--- %s seconds ---" % (time.time() - start_time)
        
        
if __name__=='__main__':
    atbfaringitis()
