# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import os
import os.path as p
import zipfile as z

import sisapUtils as u

KLX_TB = "exp_khalix_uba"
KLX_TB_LLISTATS = "exp_khalix_pac"
DB = "essencial"

EXCLOURE = ("ESxx",)


class Main(object):
    """."""

    def __init__(self):
        """."""
        self.create_structure()
        self.get_poblacio()
        self.get_centres()
        for table in u.getDatabaseTables(DB):
            if table[:4] == "pac_":
                Indicador(table, self.poblacio, self.centres, self.cip_2_hash)

    def create_structure(self):
        """."""
        tables = {KLX_TB: "({})".format(", ".join(["k{} varchar(20)".format(i)
                                                   for i in range(6)] + ["v int"])),
                KLX_TB_LLISTATS: "({})".format(", ".join(["k{} varchar(40)".format(i)
                                                   for i in range(6)] + ["exclos int"]))}  # noqa
        for table, cols in tables.items():
            u.createTable(table, cols, DB, rm=True)

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, up, uba, ubainf, edat, sexe, codi_sector, \
                      ates, institucionalitzat \
               from assignada_tot"
        self.poblacio = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

        sql = "select id_cip_sec, hash_d from import.u11"
        self.cip_2_hash = {row[0]: row[1] for row in u.getAll(sql, "import")}
    
    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}


class Indicador(object):
    """."""

    def __init__(self, table, poblacio, centres, cip_2_hash):
        """."""
        self.table = table
        self.indicador = table[-4:].upper()
        if self.indicador not in EXCLOURE:
            self.poblacio = poblacio
            self.centres = centres
            self.cip_2_hash = cip_2_hash
            self.get_data()
            self.set_data()

    def get_data(self):
        """."""
        self.khalix = c.Counter()
        self.llistats = set()
        sql = "select id, num, den from {}".format(self.table)
        for id, num, den in u.getAll(sql, DB):
            if id in self.poblacio:
                keys = []
                up, uba, ubainf, edat, sexe, sector, ates, instit = self.poblacio[id]
                br_m = self.centres[up] + 'M' + uba
                br_i = self.centres[up] + 'I' + ubainf
                instit_k = "{}INS".format("" if instit else "NO")
                keys.append((self.indicador, br_i, 'NOCAT', instit_k + "ASS", 'DIM6SET'))
                keys.append((self.indicador, br_m, 'NOCAT', instit_k + "ASS", 'DIM6SET'))
                if ates:
                    keys.append((self.indicador, br_i, 'NOCAT', instit_k + "AT", 'DIM6SET'))  # noqa
                    keys.append((self.indicador, br_m, 'NOCAT', instit_k + "AT", 'DIM6SET'))  # noqa
                for key in keys:
                    self.khalix[key + ("DEN",)] += den
                    if num:
                        self.khalix[key + ("NUM",)] += num
                # llistats, tots inversos
                if id in self.cip_2_hash:
                    hash = self.cip_2_hash[id]
                    if self.indicador in ('ES11','ES14','ES15','ES16','ES17','ES20','ES21','ES22','ES23','ES28','ES29'):
                        if num == 0 and den == 1:
                            self.llistats.add((up, uba, 'M', self.indicador, hash, sector, 0))
                            self.llistats.add((up, ubainf, 'I', self.indicador, hash, sector, 0))

    def set_data(self):
        """."""
        khalix = [k + (v,) for (k, v) in self.khalix.items()]
        u.listToTable(khalix, KLX_TB, DB)
        self.llistats = list(self.llistats)
        u.listToTable(self.llistats, KLX_TB_LLISTATS, DB)

    
if __name__ == "__main__":
    Main()
