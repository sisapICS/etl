# -*- coding: utf-8 -*-

import sisapUtils as u 
import collections as c

"""
## % de pacients amb prescripció de benzodiazepines major a 50 dies
*Descripció*
% de pacients amb prescripció de benzodiazepines major a 50 dies
*Definició*
Percentatge de pacients amb prescripció de benzodiazepines de durada superior a 50 dies 
*Numerador*
Població assignada atesa major o igual a 18 anys amb un diagnòstic incident 
d ansietat (data el nou diagnòstic en els darrers 12 mesos) 
i una prescripció de benzodiazepines (amb una durada major a 30 dies. 
*Denominador*
Població assignada atesa major o igual a 18 anys.
"""
# població assignada atesa major o igual a 18 anys
print('poblacions')
users = set()
denominador = set()
sql = """select id_cip_sec, edat
            from nodrizas.assignada_tot"""
for id, edat in u.getAll(sql, 'nodrizas'):
    users.add(id)
    if edat >= 18:
        denominador.add(id)

# diagòstics ansietat
print('ansietat')
ansietat = set()
sql = """select id_cip_sec
        from import.problemes
        where pr_cod_ps in
        ('C01-F40.00','C01-F40.10','C01-F40.298',
        'C01-F40.8','C01-F40.9','C01-F41','C01-F41.0',
        'C01-F41.1','C01-F41.3','C01-F41.8','C01-F41.9',
        'F40','F40.0','F40.1','F40.2','F40.8','F40.9',
        'F41','F41.0','F41.1' ,'F41.2' ,'F41.3' ,'F41.8' ,
        'F41.9','F40.3', 'F40.4', 'F40.5', 'F40.6', 'F40.7', 
        'F41.4', 'F41.5', 'F41.6', 'F41.7') and 
        pr_data_baixa = '' and 
        datediff(current_date(),pr_dde) <= 365"""
for cip, in u.getAll(sql, 'nodrizas'):
    if cip in denominador:
        ansietat.add(cip)

# tractaments
num = set()
print('tractaments')
cips_fets = set()
sql = """select id_cip_sec from import.tractaments
            where PPFMC_atccodi like 'N05BA%'
            and (datediff(ppfmc_data_fi,ppfmc_pmc_data_ini) > 50
            or ppfmc_data_fi is null)
            and datediff(current_date(),ppfmc_pmc_data_ini) <= 365"""
for cip, in u.getAll(sql, 'import'):
    if cip in ansietat:
        if cip not in cips_fets:
            num.add(cip)

upload = []
for id in users:
    n = 1 if id in num else 0
    d = 1 if id in denominador else 0
    upload.append((id, n, d))

u.createTable('pac_es31', 
        "(id int, num int, den int)", 'essencial', rm=True)
u.listToTable(upload, 'pac_es31', 'essencial')

