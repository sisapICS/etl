# coding: latin1

"""
Export a khalix.
"""

import shared as s
import sisapUtils as u


SQLS = {"LAB_PET": "select account, 'Aperiodo', entity, analysis, \
                           edat, 'NOINSAT', sexe, 'N', n \
                    from {}.{} \
                    where length(entity) = 5",
        "LAB_PET_UBA": "select account, 'Aperiodo', entity, analysis, \
                               'NOCAT', 'NOINSAT', 'DIM6SET', 'N', sum(n) \
                        from {}.{} \
                        where length(entity) > 5 \
                        group by account, entity, analysis"}


for file, sql in SQLS.items():
    u.exportKhalix(sql.format(s.DATABASE, s.TABLE_KHALIX), file)
