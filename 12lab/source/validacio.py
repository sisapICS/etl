# coding: latin1

"""
Validació de pacients.
"""

import shared as s
import sisapUtils as u


INDICADOR = "LABGLI"
VALIDADORS = [("00441", "4T", "M")]
FILE = u.tempFolder + "VALIDACIO_{}.CSV".format(INDICADOR)


class Validacio(object):
    """."""

    def __init__(self):
        """."""
        self.get_pacients()
        self.get_id_to_hash()
        self.get_hash_to_cip()
        self.get_export()
        self.export_data()

    def get_pacients(self):
        """."""
        self.pacients = {}
        sql = "select id, up, uba, tipus, inadequats, total \
               from {} \
               where indicador = '{}'".format(s.TABLE_MASTER, INDICADOR)
        for id, up, uba, tipus, inadequats, total in u.getAll(sql, s.DATABASE):
            if (up, uba, tipus) in VALIDADORS:
                self.pacients[int(id)] = (up, uba, tipus, inadequats == 0)

    def get_id_to_hash(self):
        """."""
        sql = "select id_cip_sec, hash_d from u11 \
               where id_cip_sec in {}".format(tuple(self.pacients.keys()))
        self.id_to_hash = {id: hash for (id, hash) in u.getAll(sql, "import")}

    def get_hash_to_cip(self):
        """."""
        sql = "select usua_cip_cod, usua_cip from pdptb101 \
               where usua_cip_cod in {}".format(tuple(self.id_to_hash.values()))  # noqa
        self.hash_to_cip = {hash: cip for (hash, cip) in u.getAll(sql, "pdp")}

    def get_export(self):
        """."""
        self.export = []
        for id, (up, uba, tipus, complidor) in self.pacients.items():
            hash = self.id_to_hash[id]
            cip = self.hash_to_cip[hash]
            self.export.append((up, uba, tipus, cip, 1 * complidor))

    def export_data(self):
        """."""
        header = [("up", "uba", "tipus", "cip", "compleix")]
        u.writeCSV(FILE, header + sorted(self.export), sep=";")


if __name__ == "__main__":
    Validacio()
