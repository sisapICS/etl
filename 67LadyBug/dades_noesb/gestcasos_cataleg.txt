GC0000@Pacients inclosos en gesti� de casos@Gesti� de casos
GC0001@Pacients en gesti� de casos amb pla de cures@Gesti� de casos
GC0001A@Pacients en gesti� de casos amb pla de cures cr�nic@Gesti� de casos
GC0001B@Pacients en gesti� de casos amb pla de cures agut@Gesti� de casos
GC0002@Pacients en gesti� de casos amb valoraci� integral@Gesti� de casos
GC0003@Pacients en gesti� de casos amb registre d'adher�ncia al tractament@Gesti� de casos
GC0004@Pacients en gesti� de casos amb cribratge de caigudes@Gesti� de casos
GC0005@Pacients en gesti� de casos PCC o MACA amb PIIC@Gesti� de casos
GC0006@Pacients en gesti� de casos MACA amb pla de decisions anticipades@Gesti� de casos
GC0007@Pacients en gesti� de casos amb registre de l'estat nutricional@Gesti� de casos
GC0008@Freq�entaci� dels pacients en gesti� de casos@Gesti� de casos
GC0009@Pacients en gesti� de casos inclosos en el programa ATDOM@Gesti� de casos
GC0010@Pacients en gesti� de casos amb c�lcul del grau de complexitat@Gesti� de casos
GC0010A@Pacients en gesti� de casos amb complexitat alta@Gesti� de casos
GC0010B@Pacients en gesti� de casos amb complexitat mitja@Gesti� de casos
GC0010C@Pacients en gesti� de casos amb complexitat baixa@Gesti� de casos
GC0011@Pacients en gesti� de casos amb cuidador principal identificat@Gesti� de casos
GC0011A@Pacients en gesti� de casos amb valoraci� de l'entorn del cuidador@Gesti� de casos
GC0012@Pacients en gesti� de casos amb valoraci� de la qualitat de vida@Gesti� de casos
GC0013@Seguiment de pacients amb MPOC en gesti� de casos@Gesti� de casos
GC0014@Seguiment de pacients amb Insufici�ncia Card�aca en gesti� de casos@Gesti� de casos
GC0015@Visites totals dels pacients en gesti� de casos@Gesti� de casos
GC0015A@Percentatge de visites presencials dels pacients en gesti� de casos@Gesti� de casos
GC0015B@Percentatge de visites presencials dels pacients en gesti� de casos@Gesti� de casos
GC0015C@Percentatge de visites domicili�ries dels pacients en gesti� de casos@Gesti� de casos
GC0015D@Percentatge de visites telef�niques dels pacients en gesti� de casos@Gesti� de casos
GC0016@Percentatge de pacients en gesti� de casos amb derivaci�@Gesti� de casos
GC0017@Percentatge de pacients amb derivaci� que no estan en gesti� de casos@Gesti� de casos
GC0018@Percentatge de pacients en gesti� de casos amb coordinaci�@Gesti� de casos
GC0019@Percentatge de pacients amb coordinaci� que no estan en gesti� de casos@Gesti� de casos
GC0020@Pacients en gesti� de casos PCC amb ingr�s a l'hospital@Gesti� de casos
GC0021@Pacients en gesti� de casos MACA amb ingr�s a l'hospital@Gesti� de casos
GC0022@Pacients en gesti� de casos PCC que reingressa a l'hospital@Gesti� de casos
GC0023@Pacients en gesti� de casos MACA que reingressa a l'hospital@Gesti� de casos