# coding: utf8

"""
Afegim indicadors a la taula de control. En aquest cas els històrics de econsulta que els hem tret de khalix
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

db = 'permanent'
tb = 'mst_control_eqa'

years = ['2017', '2018', '2019']

class hist_econsulta(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.periodes, self.indicadors, self.resultats = {},[],Counter()
        self.get_territoris()
        self.get_cataleg()
        for self.anys in years:
            self.get_indicador()
        self.export_data()
                
    def get_territoris(self):
        """Obtenim àmbits i sector"""
        
        self.centres = {}
        sql = 'select ics_codi, amb_desc, sector from cat_centres'
        for br, amb, sector in u.getAll(sql, 'nodrizas'):
            self.centres[br] = {'sector': sector, 'ambit': amb}
            
    def get_cataleg(self):
        """Obtenim el catàleg de khalix. Si hi ha nous indicadors, afegir-los"""
        self.cataleg = {}
        file = u.baseFolder + ["67LadyBug\dades_noesb", "econsulta_cataleg.txt"]
        file = "\\".join(file)
        for codi, desc, pare in u.readCSV(file, sep="@"):
            self.cataleg[codi] = {'d': desc, 'p': pare}
    
    def get_indicador(self):
        """Les dades de num i den dels indicadors econsulta dels històrics que figurin al ppi del fitxer. Noms fitxes han de ser econsultaYYYY.csv"""
        nom = "econsulta" + str(self.anys) + ".csv"
        file = u.baseFolder + ["67LadyBug\dades_noesb", nom]
        file = "\\".join(file)
        for indicador, periode, br, tip, n in u.readCSV(file, sep=";"):
            n = int(n)
            self.indicadors.append(indicador)
            periode = periode.replace('A', '20')
            self.periodes[(periode)] = True
            try:
                ambit = self.centres[br]['ambit']
                sector = self.centres[br]['sector']
            except KeyError:
                continue
            if tip == 'DEN':
                self.resultats[periode, sector, ambit, indicador, 'den'] += n
            if tip == 'NUM':
                self.resultats[periode, sector, ambit, indicador, 'num'] += n
                
       
    def export_data(self):
        """."""
        upload = []
        for (period, sector, ambit, ind, tipus), n in self.resultats.items():
            if tipus == 'den':
                desc_ind = self.cataleg[ind]['d'] if ind in self.cataleg else "Sense descripció"
                pare = self.cataleg[ind]['p'] if ind in self.cataleg else "Sense descripció"
                resolts = self.resultats[period, sector, ambit, ind, 'num']
                upload.append([int(period), sector, ambit, ind, resolts, n])
        
        indicadors = tuple(self.indicadors)
        for (periode),i in self.periodes.items():
            sql = "delete a.* from {0} a where periode = '{1}' and indicador in {2}".format(tb, periode, indicadors)
            u.execute(sql, db)
        u.listToTable(upload, tb, 'permanent')
      
if __name__ == '__main__':
    hist_econsulta()
    