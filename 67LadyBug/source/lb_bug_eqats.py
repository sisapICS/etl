# coding: utf8

"""
Afegim resultats EQA treball social del mes de gener del 2020 que no es van carregar
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u

db = 'permanent'
tb = 'mst_control_eqa'

class hist_econsulta(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.periodes, self.indicadors, self.resultats = {},[],Counter()
        self.get_territoris()
        self.get_indicador()
        self.export_data()
                
    def get_territoris(self):
        """Obtenim àmbits i sector"""
        
        self.centres = {}
        sql = 'select ics_codi, amb_desc, sector from cat_centres'
        for br, amb, sector in u.getAll(sql, 'nodrizas'):
            self.centres[br] = {'sector': sector, 'ambit': amb}
          
            
    def get_indicador(self):
        """Les dades de num i den dels indicadors històrics que figurin al ppi del fitxer."""
        nom = "LB_eqats_012020.txt"
        baseFold = ['D:', 'temp']
        file = baseFold + [nom]
        file = u.SLASH.join(file)
        for indicador, periode, br, tip, n in u.readCSV(file, sep="@"):
            n = float(n)
            self.indicadors.append(indicador)
            periode = periode.replace('B', '20')
            periode = periode.replace('A', '20')
            self.periodes[(periode)] = True
            try:
                ambit = self.centres[br]['ambit']
                sector = self.centres[br]['sector']
            except KeyError:
                continue
            if tip == 'DEN':
                self.resultats[periode, sector, ambit, indicador, 'den'] += n
            if tip == 'NUM':
                self.resultats[periode, sector, ambit, indicador, 'num'] += n
                
       
    def export_data(self):
        """."""
        upload = []
        for (period, sector, ambit, ind, tipus), n in self.resultats.items():
            if tipus == 'den':
                resolts = self.resultats[period, sector, ambit, ind, 'num']
                upload.append([int(period), sector, ambit, ind, resolts, n])
        
        indicadors = tuple(self.indicadors)
        for (periode),i in self.periodes.items():
            sql = "delete a.* from {0} a where periode = '{1}' and indicador in {2}".format(tb, periode, indicadors)
            u.execute(sql, db)
        u.listToTable(upload, tb, db)
        
      
if __name__ == '__main__':
    hist_econsulta()
    