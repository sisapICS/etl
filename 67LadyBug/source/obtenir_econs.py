# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

econs = exemple(c, 'ECONSULTA###;A2204###;AMBITOS###;NUM,DEN;NOCAT;NOINSAT;DIM6SET')
econsA = exemple(c, 'ECONSULTA###;A2206###;AMBITOS###;NUM,DEN;MENSUAL;NOINSAT;DIM6SET')

c.close()

print('export')

file = u.tempFolder + "ECONS_ANUAL.txt"
with open(file, 'w') as f:
   f.write(econs)

file = u.tempFolder + "ECONS_MENSUAL.txt"
with open(file, 'w') as f:
   f.write(econsA)
