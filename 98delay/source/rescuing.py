from sisapUtils import *
from subprocess import Popen,PIPE
import MySQLdb

me= 'sisap_ctl'
tab= 'delay'
py= 'C:/Python27/python'

sql= 'drop table if exists {}'.format(tab)
execute(sql,me)
sql= "create table {} (wd varchar(50),file varchar(50) not null default '',ext varchar(50) not null default '',param varchar(250) not null default '',db varchar(50) not null default '')".format(tab)
execute(sql,me)

for nom,wd,_source,db,_call in readCSV(componentsFile,sep=';'):
    if nom<> 'down':
        sql= 'select file,extension,param from ctl_{} where status=4'.format(db)
        for file,ext,param in getAll(sql,db):
            sql= "insert into {} values ('{}','{}','{}','{}','{}')".format(tab,wd,file,ext,param,db)
            execute(sql,me)
            sql= "update ctl_{} set ts_start=current_timestamp,ts_finish=0,status=9,error='' where file='{}' and extension='{}' and param='{}'".format(db,file,ext,param)
            execute(sql,db)
            pth= baseFolder + [wd]
            pth= '/'.join(pth)
            f= (py,'./source/'+file+'.'+ext,param)
            process= Popen(f, stdout=PIPE, stdin=PIPE, stderr=PIPE, cwd=pth)
            output,errors= process.communicate()
            exit= process.returncode
            error= str(MySQLdb.escape_string(errors))
            sql= "update ctl_{} set ts_finish=current_timestamp,status={},error='{}' where file='{}' and extension='{}' and param='{}'".format(db,exit,error,file,ext,param)
            execute(sql,db)
