
use eqa_ind;

#Pirmer el de cessacions de tabac!
#M�s endavant hi ha el d abstinents en poblaci� de risc!

drop table if exists eqa_tabac;
create table eqa_tabac (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
ates double,
trasplantat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ind_codi varchar(10) not null default'',
fuma_1a double,
nofuma_ara double,
num double,
den double,
excl double,
ci double,
clin double
)
select
	a.id_cip_sec
	,a.up
	,a.uba
	,a.upinf
	,a.ubainf
	,a.edat
	,ates
    ,trasplantat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ind_codi
   ,0 as fuma_1a
   ,0 as nofuma_ara
   ,0 as num
   ,0 as den
   ,0 as excl
   ,0 as ci
   ,0 as clin
from
	assignada_tot a
   ,eqa_ind c
where
	 c.tipus=6
;

#creo taula de fumadors de fa un any!
drop table if exists fuma_1a;
create table fuma_1a(
id_cip_sec double null,
index(id_cip_sec)
)
select
   id_cip_sec
from
   nodrizas.eqa_tabac
   ,nodrizas.dextraccio
where
   tab=1 and (date_add(data_ext,interval - 12 month) between dalta and dbaixa)
;

#Agafo dels que fumen, quants no fumen ara!
drop table if exists nofuma_ara;
create table nofuma_ara(
id_cip_sec double null,
index(id_cip_sec)
)
select
   a.id_cip_sec
from
   fuma_1a a
inner join
   nodrizas.eqa_tabac b
on
   a.id_cip_sec=b.id_cip_sec
   ,nodrizas.dextraccio
where
   tab in ('2') and (data_ext between dalta and dbaixa)
;


update eqa_tabac a inner join fuma_1a b on a.id_cip_sec=b.id_cip_sec
set fuma_1a=1
;

update eqa_tabac a inner join nofuma_ara b on a.id_cip_sec=b.id_cip_sec
set nofuma_ara=1
;

update eqa_tabac
set den=1 where fuma_1a=1
;

update eqa_tabac
set num=1 where nofuma_ara=1
;

set @eqa='ind';

set @maxt=(select count(distinct ind_codi) from  eqa_ind where tipus=6);

drop table if exists agrupadorsi;
create table agrupadorsi (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct ind_codi, 0 usar from eqa_ind where tipus=6
;

drop procedure if exists eqa_g;

delimiter //
CREATE PROCEDURE eqa_g()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num=p;


update agrupadorsi set usar=0;
update agrupadorsi set usar=1 where id=@num;


set @ind=(select ind_codi from agrupadorsi where usar=1);

set @sql1 = concat("
drop table if exists ",@ind,"_",@eqa,"2
");

set @sql2 = concat("
create table ",@ind,"_",@eqa,"2 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
index(id_cip_sec)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,ind_codi
	,num
	,excl
   ,ci
   ,clin
from
	eqa_tabac
where
	den=1 
");


PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;


SET p=p+1;
IF p > @maxt
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_g();


set @maxg=(select distinct ind_codi from  eqa_ind where tipus=6);

delete a.* from eqa_relacio a where ind_codi=@maxg
;

insert into eqa_relacio (ind_codi,baixa)
values (@maxg,0)
;

drop table if exists nofuma_ara;
drop table if exists fuma_1a;
drop table eqa_tabac;



#Ara el d abstinents
drop table if exists eqa_abstabac;
create table eqa_abstabac (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
ates double,
trasplantat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ind_codi varchar(10) not null default'',
hafumat double,
sit_ara double,
dalta_ara date,
num double,
den double,
excl double,
ci double,
clin double
)
select
	a.id_cip_sec
	,a.up
	,a.uba
	,a.upinf
	,a.ubainf
	,a.edat
	,ates
    ,trasplantat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ind_codi
   ,0 as hafumat
   ,0 as sit_ara
   ,0 as dalta_ara
   ,0 as num
   ,0 as den
   ,0 as excl
   ,0 as ci
   ,0 as clin
from
	assignada_tot a
   ,eqa_ind c
where
	 c.tipus=7
;

#Haur�em de parametritzar, per� hi ha poc temps aix� que m�s endavant...

drop table if exists ps_tab;
create table ps_tab(
id_cip_sec double,
index(id_cip_sec)
)
select id_cip_sec from nodrizas.eqa_problemes where ps in ('1','18','58','62','21','55','254','100')
;
update eqa_abstabac a inner join ps_tab b on a.id_cip_sec=b.id_cip_sec
set den=1 
;

drop table if exists far_tab;
create table far_tab(
id_cip_sec double,
index(id_cip_sec)
)
select id_cip_sec from nodrizas.eqa_tractaments  where farmac=255
;

update eqa_abstabac a inner join far_tab b on a.id_cip_sec=b.id_cip_sec
set den=1 
;

#creo taula dels que han fumat algun cop
drop table if exists hafumat;
create table hafumat(
id_cip_sec double null,
index(id_cip_sec)
)
select 
   id_cip_sec
from
   nodrizas.eqa_tabac
where
   tab=1 
group by id_cip_sec
;
#Agafo situacio actual!
#Agafo dels que fumen, quants no fumen ara!

drop table if exists sit_ara;
create table sit_ara(
id_cip_sec double null,
tab double,
dlast date,
index(id_cip_sec,tab,dlast)
)
select
   a.id_cip_sec
   ,tab
   ,dlast
from
   nodrizas.eqa_tabac a
   ,nodrizas.dextraccio
where
    (data_ext between dalta and dbaixa)
;

update eqa_abstabac a inner join hafumat b on a.id_cip_sec=b.id_cip_sec
set a.hafumat=1
;

update eqa_abstabac a inner join sit_ara b on a.id_cip_sec=b.id_cip_sec
set a.sit_ara=tab
,a.dalta_ara=b.dlast
;

#Compliment:
#�ltim registre d habit tab�quic amb const�ncia abstin�ncia (no fumador o exfumador), amb periodicitat de cribratge adequada:
#Darrers 24 mesos, si t� <=25 anys o consta que en algun moment hagi estat fumador
#Nom�s 1 sol registre, si t� >25 anys i tots els registres indiquen que mai ha sigut fumador

update eqa_abstabac,nodrizas.dextraccio
set num=1 where sit_ara in ('0','2') and dalta_ara >=date_add(data_ext,interval - 24 month) and (edat <=25 or hafumat=1) 
;

update eqa_abstabac,nodrizas.dextraccio
set num=1 where sit_ara in ('0','2')  and edat >25 and hafumat=0 
;



set @eqa='ind';

set @maxt=(select count(distinct ind_codi) from  eqa_ind where tipus=7);

drop table if exists agrupadorsi;
create table agrupadorsi (
 id MEDIUMINT NOT NULL AUTO_INCREMENT,
 ind_codi VARCHAR(10) NOT NULL DEFAULT'',
 usar double null,
  PRIMARY KEY (id)
)
select distinct ind_codi, 0 usar from eqa_ind where tipus=7
;

drop procedure if exists eqa_g;

delimiter //
CREATE PROCEDURE eqa_g()
BEGIN
DECLARE p INT;
SET p=1;
lbl: LOOP

set @num=p;


update agrupadorsi set usar=0;
update agrupadorsi set usar=1 where id=@num;


set @ind=(select ind_codi from agrupadorsi where usar=1);

set @sql1 = concat("
drop table if exists ",@ind,"_",@eqa,"2
");

set @sql2 = concat("
create table ",@ind,"_",@eqa,"2 (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(7) not null default'',
upinf varchar(5) not null default'',
ubainf varchar(7) not null default'',
edat double,
sexe varchar(7) not null default'',
seccio_censal varchar(10) not null default'',
up_residencia varchar(13) not null default'',
institucionalitzat double,
maca double,
ates double,
trasplantat double,
ind_codi varchar(10) not null default'',
num double,
excl double,
ci double,
clin double,
index(id_cip_sec)
)
select
	id_cip_sec
	,up
	,uba
	,upinf
	,ubainf
	,edat
	,sexe
    ,seccio_censal
    ,up_residencia
	,institucionalitzat
	,maca
	,ates
    ,trasplantat
	,ind_codi
	,num
	,excl
   ,ci
   ,clin
from
	eqa_abstabac
where
	den=1 
");


PREPARE s1 FROM @sql1;
EXECUTE s1;

PREPARE s2 FROM @sql2;
EXECUTE s2;



SET p=p+1;
IF p > @maxt
THEN LEAVE lbl;
END IF;
END LOOP;
END
//
delimiter ;

call eqa_g();


set @maxg=(select distinct ind_codi from  eqa_ind where tipus=7);

delete a.* from eqa_relacio a where ind_codi=@maxg
;

insert into eqa_relacio (ind_codi,baixa)
values (@maxg,0)
;

drop table if exists sit_ara;
drop table if exists hafumat;
drop table if exists eqa_abstabac;
drop table if exists ps_tab;
drop table if exists far_tab;
drop table if exists assir_tab;