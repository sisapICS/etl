# -*- coding: utf-8 -*-

"""Bolet per l'indicador EQD023901"""


import sisapUtils as u
import collections as c
import datetime

print('pacients')
pacients = {}
sql = """select
            id_cip_sec,
            up,
            uba,
            upinf,
            ubainf,
            edat,
            sexe,
            seccio_censal,
            up_residencia,
            institucionalitzat,
            maca,
            ates,
            trasplantat
        from
            nodrizas.assignada_tot_with_jail
        where 
            edat between 15 and 89"""
for id_cip_sec, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat in u.getAll(sql, 'nodrizas'):
    pacients[id_cip_sec] = [up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat]
print(len(pacients))

print('problemes')
problemes = c.defaultdict(set)
sql = """select
            id_cip_sec,
            ps
        from
            nodrizas.eqa_problemes
        where 
            ps in (277, 18, 534, 24, 953, 21, 954, 239, 53)"""
for id_cip, ps in u.getAll(sql, 'nodrizas'):
    problemes[ps].add(id_cip)

print('tractaments')
tractaments = set()
empaglifozina_dates = {}
sql = """select
            id_cip_sec, farmac, pres_orig
        from
            nodrizas.eqa_tractaments
        where
            (farmac = 445 and tancat = 0)
            or (farmac = 1005 and tancat = 0)
            or (farmac = 1006 and tancat = 0)
            or farmac in (953, 954, 445)"""
for id_cip, farmac, pres_orig in u.getAll(sql, 'nodrizas'):
    if farmac in (953, 954, 1005, 1006, 445):
        problemes[farmac].add(id_cip)
    if farmac == 445:
        tractaments.add(id_cip)
    if farmac == 1006: empaglifozina_dates[id_cip] = pres_orig

print('variables')
variables = c.defaultdict(set)
quac = c.defaultdict(set)
sql = """select
            id_cip_sec,
            agrupador,
            valor,
            data_var,
            usar
        from
            nodrizas.eqa_variables
        where
            (agrupador in (191, 19, 30)
            and usar = 1) or (agrupador = 513)"""
for id_cip, agrupador, valor, data_var, usar in u.getAll(sql, 'nodrizas'):
    if agrupador == 19 and valor >= 30: variables[agrupador].add(id_cip) # IMC
    elif agrupador == 191 and valor < 40: variables[agrupador].add(id_cip) # Fracci� ejecci�
    elif agrupador == 30 and valor <= 75 and valor >= 25: variables[agrupador].add(id_cip) # Filtrat glomerular 25-75
    elif agrupador == 30 and valor <= 90 and valor >= 20 and usar == 1: variables['30_a'].add(id_cip) # �ltim filtrat glomerular 20-90
    elif agrupador == 513 and valor > 200: quac[id_cip].add((data_var, usar))

print('cooking')
# DEN = PACIENTS 445 = tractaments
den = tractaments
print(len(den))

# NUM = PACIENTS 18
num = set()
for id in problemes[18]:
    if id in den: num.add(id)

# Dues determinacions de QAC> 200 separades entre elles de 3 mesos (513) abans del tractament
condicions_qac = set()
abans_tractament_200 = set()
for id_cip, data_ini_tractament in empaglifozina_dates.items():
    pac_qac = {}
    counting_2 = set()
    abans_tractament = {}
    if id_cip in quac:
        for (data_var, usar) in quac[id_cip]:
            abans_tractament[usar] = data_var < data_ini_tractament
            if data_var < data_ini_tractament:
                pac_qac[usar] = data_var
    for usar in sorted(pac_qac.keys()):
        data_var = pac_qac[usar]
        if usar+1 in pac_qac:
            if (data_var - pac_qac[usar+1]).days > 90:
                condicions_qac.add(id_cip)
    for usar in sorted(abans_tractament.keys()):
        abans = abans_tractament[usar]
        if usar == 1 and abans == True:
            abans_tractament_200.add(id_cip)
        elif abans == False:
            if usar+1 in abans_tractament:
                if abans_tractament[usar+1] == True:
                    abans_tractament_200.add(id_cip)
    

print('exclusions')
# EXCLUSIONS
# EXCLUSI� 1: 445 + 277 + NO 18
# EXCLUSI� 2: 445 + 534 + NO 18
# EXCLUSI� 3: 24
# EXCLUSI� 4: 953 + (21 O 191 < 40) + NO 18
# EXCLUSI� 5: 954 + (239 O 19 >= 30) + NO 18
excl = set()
for id in den:
    if id in tractaments and id in problemes[277] and id not in problemes[18]: excl.add(id)
    if id in tractaments and id in problemes[534] and id not in problemes[18]: excl.add(id)
    if id in problemes[24]: excl.add(id)
    if id in problemes[953] and (id in problemes[21] and id in variables[191]) and id not in problemes[18]: excl.add(id)
    if id in problemes[954] and (id in problemes[239] or id in variables[19]) and id not in problemes[18]: excl.add(id)
    # Pacient no diab�tics que estan en tractament amb Dapaglifozina amb diagn�stica d'Insufici�ncia card�aca
    if id not in problemes[18] and id in problemes[1005] and id in problemes[21]: excl.add(id)
    # Pacient no diab�tics que estan en tractament amb Dapaglifozina amb diagn�stic de Malaltia renal cr�nica
    # + �ltim filtrat glomerular entre 25 - 75 ml/min + dues determinacions de QAC> 200 separades entre elles de 3 mesos i que la �ltima entre les dues sigui QAC> 200
    if id not in problemes[18] and id in problemes[1005] and id in problemes[53] and id in variables[30] and id in condicions_qac: excl.add(id) 
    # Pacient no diab�tics que estan en tractament amb Empaglifozina (A10BK03) i diagn�stic d'Insufici�ncia card�aca
    if id not in problemes[18] and id in problemes[1006] and id in problemes[21]: excl.add(id)
    # Pacient no diab�tics en tractament amb qualsevol f�rmac ILGP1 (A10BJ%) amb diagn�stic d'Obesitat o IMC >=30
    if id not in problemes[18] and id in problemes[445] and (id in problemes[239] or id in problemes[19]): excl.add(id)
    # Pacient no diab�tics que estan en tractament amb Empaglifozina amb diagn�stic de Malaltia renal cr�nica amb l'�ltim filtrat glomerular entre 20 - 90 ml/min i la �ltima determinaci� sigui > 200  abans de l'inici del tractament.
    if id not in problemes[18] and id in problemes[1006] and id in problemes[53] and id in abans_tractament_200 and id in problemes['30_a']: excl.add(id)



print('uploading')
# estructura
# id_cip_sec, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat, ind_codi = EQD023901, num, excl, ci = 0, clin = 0
upload = []
for id in den:
    n = 1 if id in num else 0
    e = 1 if id in excl else 0
    if id in pacients:
        up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat = pacients[id]
        upload.append((id, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat, 'EQD023901', n, e, 0, 0))

print('table stuff')
cols = """(id_cip_sec double,
            up varchar(5),
            uba varchar(7),
            upinf varchar(5),
            ubainf varchar(7),
            edat double,
            sexe varchar(7),
            seccio_censal varchar(10),
            up_residencia varchar(13),
            institucionalitzat double,
            maca double,
            ates double,
            trasplantat double,
            ind_codi varchar(10),
            num double,
            excl double,
            ci double,
            clin double)"""
u.createTable('EQD023901_ind2', cols, 'eqa_ind', rm=True)
u.listToTable(upload, 'EQD023901_ind2', 'eqa_ind')