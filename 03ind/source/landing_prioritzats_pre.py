
# -*- coding: utf8 -*-

import sisapUtils as u
import collections as c
from datetime import date, datetime
import sisaptools as t

CODIS = {
    "FAC01":
    {
        "etiqueta": 'FA-ACO',
        "indicadors": 
        {
            'EQA0201A': {
                'descripcio': u'anticoagulació FA', 
                'punts': 1
            },
            'EQA0202A': {
                'descripcio': u'anticoagulació', 
                'punts': 1
            }
        },
        "detall": u'\"Incidència\": \"Indicació {txt} mal controlada\"',
        "max_punts": 1,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "AGR01":
    {
        "etiqueta": 'AAGR',
        "indicadors": 
        {
            'EQA0203A': {
                'descripcio': u'Antiagregació en cardiopatia isquèmica o AVC', 
                'punts': 1
            },
            'EQA0203B': {
                'descripcio': u'Antiagregació en cardiopatia isquèmica o AVC', 
                'punts': 1
            },
            'EQA0203C': {
                'descripcio': u'Antiagregació en cardiopatia isquèmica o AVC', 
                'punts': 1
            }
        },
        "detall": u'\"Incidència\": \"{txt}\"',
        "max_punts": 1,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "BBL01":
    {
        "etiqueta": 'BBL0Q',
        "indicadors": 
        {
            'EQA0206A': {
                'descripcio': u'Tractament amb betablocadors de la CI O IC', 
                'punts': 1
            },
            'EQA0206B': {
                'descripcio': u'Tractament amb betablocadors de la CI O IC', 
                'punts': 1
            },
            'EQA0206C': {
                'descripcio': u'Tractament amb betablocadors de la CI O IC', 
                'punts': 1
            }
        },
        "detall": u'\"Incidència\": \"{txt}\"',
        "max_punts": 1,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "IEC01":
    {
        "etiqueta": 'IECA',
        "indicadors": 
        {
            'EQA0236A': {
                'descripcio': u'insuficiència cardíaca', 
                'punts': 1
            },
            'EQA0237A': {
                'descripcio': u'malaltia renal crònica amb HTA o DM', 
                'punts': 1
            },
            'EQA0237B': {
                'descripcio': u'malaltia renal crònica amb HTA o DM', 
                'punts': 1
            }
        },
        "detall": u'\"Incidència\": \"Tractament amb IECA en {txt}\"',
        "max_punts": 1,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "LDL01":
    {
        "etiqueta": 'LDL',
        "indicadors": 
        {
            'EQA0204A': {
                'descripcio': u'', 
                'punts': 1
            },
             'EQA0204B': {
                'descripcio': u'', 
                'punts': 1
            },
             'EQA0204C': {
                'descripcio': u'', 
                'punts': 1
            },
             'EQA0204D': {
                'descripcio': u'', 
                'punts': 1
            }
        },
        "detall": u'\"Incidència\": \"Atenció en control de LDL en prevenció secundària.{txt}\", \"LDL\": \"{valor}\", \"Data\": \"{data}\"',
        "max_punts": 1,
        "valor": True,
        "data": True,
        "situacio": ''
    },
    "DLP01":
    {
        "etiqueta": 'DLP',
        "indicadors": 
        {
            'EQA0214A': {
                'descripcio': u'Mal control LDL', 
                'punts': 0.5
            },
            'EQA0215A': {
                'descripcio': u'Sense calcul RCV', 
                'punts': 0.5
            }
        },
        "detall": u'\"Incidència\": \"Atenció en control de Risc Cardiovascular\", \"Motiu\": \"{txt}\"',
        "max_punts": 1,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "TA01":
    {
        "etiqueta": 'TA',
        "indicadors": 
        {
            'EQA0205A': {
                'descripcio': u'Mal control de la pressió arterial', 
                'punts': 1
            },
            'EQA0205B': {
                'descripcio': u'Mal control de la pressió arterial', 
                'punts': 1
            },
            'EQA0205C': {
                'descripcio': u'Mal control de la pressió arterial', 
                'punts': 1
            },
            'EQA0212A': {
                'descripcio': u'Mal control de la pressió arterial', 
                'punts': 1
            },
             'EQA0235A': {
                'descripcio': u'Mal control de la pressió arterial', 
                'punts': 1
            },
            'EQA0213A': {
                'descripcio': u'Mal control de la pressió arterial', 
                'punts': 0.5
            },
            'EQA0240A': {
                'descripcio': u'Inedequacio diagnòstica de la HTA', 
                'punts': 0.5
            }
        },
        "detall": u'\"Incidència\": \"Atenció en el control de HTA\", \"Motiu\": \"{txt}\", \"TA\": \"{valor}\", \"Data\": \"{data}\"',
        "max_punts": 2,
        "valor": True,
        "data": True,
        "situacio": ''
    },
    "GLI01":
    {
        "etiqueta": 'HbA1c',
        "indicadors": 
        {
            # 'EQA3102A': {
            #     'descripcio': u'Mal control de l\'hemoglobina glicosilada', 
            #     'punts': 1
            # },
            'EQA0209A': {
                'descripcio': u'Mal control de l\'hemoglobina glicosilada', 
                'punts': 2
            },
            # 'EQA3108B': {
            #     'descripcio': u'Mal control de l\'hemoglobina glicosilada', 
            #     'punts': 1
            # },
            #  'EQA3108C': {
            #     'descripcio': u'Mal control de l\'hemoglobina glicosilada', 
            #     'punts': 2
            # },
            # 'EQA3108D': {
            #     'descripcio': u'Mal control de l\'hemoglobina glicosilada', 
            #     'punts': 2
            # },
            'EQA0315A': {
                'descripcio': u'Control excessiu de la DM2', 
                'punts': 1
            }
        },
        "detall": u'\"Incidència\": "Atenció en el control de l\'hemoglobina glicosilada\", \"Motiu\": \"{txt}\"',
        "max_punts": 2,
        "valor": False,
        "data": False,
        "situacio": ('EQA3108C', 'EQA3108D')
    },
    "CRT01":
    {
        "etiqueta": 'CROC',
        "indicadors": 
        {
            'EQA0210A': {
                'descripcio': u'', 
                'punts': 1
            }
        },
        "detall": u'\"Incidència\": \"Atenció en cribatge retinopatia{txt}\"',
        "max_punts": 1,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "HTR01":
    {
        "etiqueta": 'Tiroides',
        "indicadors": 
        {
            'EQA0227A': {
                'descripcio': u'Mal control de l\'hipotiroidisme', 
                'punts': 1
            }
        },
        "detall": u'\"Incidència\": \"Atenció en control de l\'hipotiroidisme\", \"Motiu\": \"{txt}\"',
        "max_punts": 1,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "TBC01":
    {
        "etiqueta": 'Tabac',
        "indicadors": 
        {
            'EQA0305A': {
                'descripcio': u'', 
                'punts': 0.5
            },
            'EQA0304A': {
                'descripcio': u'', 
                'punts': 1
            }
        },
        "detall": u'\"Incidència\": \"Atenció amb dependència al tabac\", \"Motiu\": \"Tabaquisme{txt}\"',
        "max_punts": 1,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "ALC01":
    {
        "etiqueta": 'Alcohol',
        "indicadors": 
        {
            'EQA0302A': {
                'descripcio': u'', 
                'punts': 1
            }
        },
        "detall": u'\"Incidència\": \"Atenció amb consum d\'alcohol{txt}\"',
        "max_punts": 1,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "EQD01":
    {
        "etiqueta": 'EQD',
        "indicadors": 
        {
            'EQD023801': {
                'descripcio': u'Inadequació diagnòstica de la ICC', 
                'punts': 1
            },
            'EQD023802': {
                'descripcio': u'Falta de registre del grau fucional de la ICC', 
                'punts': 1
            },
             'EQD023803': {
                'descripcio': u'Falta de l\'especificitat diagnòstica de la ICC', 
                'punts': 1
            },
            'EQD023901': {
                'descripcio': u'Antidiabètics sense diagnostic de DM2', 
                'punts': 1
            },
            'EQD023902': {
                'descripcio': u'Inadequació diagnòstica de DM2', 
                'punts': 1
            },
            'EQD023903': {
                'descripcio': u'Compleix criteris de DM2', 
                'punts': 1
            },
            'EQD024202': {
                'descripcio': u'Inadequació diagnòstica d\'MPOC', 
                'punts': 1
            },
            'EQD024203': {
                'descripcio': u'Inadequació diagnòstica d\'asma', 
                'punts': 1
            },
            'EQD024205': {
                'descripcio': u'Falta el grau de severitat de l\'asma', 
                'punts': 1
            },
            'EQD024206': {
                'descripcio': u'Falta el grau de severitat de l\'MPOC', 
                'punts': 1
            },
            'EQD024001': {
                'descripcio': u'Inadequació diagnòstica de la HTA', 
                'punts': 1
            },
            'EQD024101': {
                'descripcio': u'Inadequació diagnòstica de la hipercolesterolèmia', 
                'punts': 0.5
            },
            'EQD024201': {
                'descripcio': u'Ús inadequat de broncodilatadors', 
                'punts': 0.5
            },
            'EQD031301': {
                'descripcio': u'Inadequació diagnòstica d\'obesitat', 
                'punts': 0.5
            },
            'EQD031302': {
                'descripcio': u'Compleix criteris d\'obesitat', 
                'punts': 0.5
            },
            'EQD024704': {
                'descripcio': u'Inadequació diagnòstica de l\'hipotiroidisme primari', 
                'punts': 0.5
            },
            'EQD024701': {
                'descripcio': u'Inadequació diagnòstica de l\'anèmia', 
                'punts': 0.5
            },
            'EQD024702': {
                'descripcio': u'Ús inadequat d\'antiosteoporòtics', 
                'punts': 0.5
            },
            'EQD024703': {
                'descripcio': u'Inadequació diagnòstica d\'osteoporosi', 
                'punts': 0.5
            },
            'EQD024705': {
                'descripcio': u'Ús inadequat d\'antimigranyosos', 
                'punts': 0.5
            },
            'EQD024706': {
                'descripcio': u'Ús inadequat de bolquers', 
                'punts': 0.5
            },
            'EQD024707': {
                'descripcio': u'Inadequació diagnòstica de demència', 
                'punts': 0.5
            },
            'EQD024708': {
                'descripcio': u'Compleix criteris de malaltia renal crònica', 
                'punts': 0.5
            }
        },
        "detall": u'\"Incidència\": \"Atenció amb adequació diagnòstica\", \"Motiu\": \"{txt}\"',
        "max_punts": 2,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "PQT01":
    {
        "etiqueta": 'Prev 4',
        "indicadors": 
        {
            'EQA0216A': {
                'descripcio': u'Ús inadequat d\'hipolipemiants', 
                'punts': 0.5
            },
            'EQA0238A': {
                'descripcio': u'Ús inadequat de noves estatines', 
                'punts': 0.5
            },
            'EQA0222A': {
                'descripcio': u'Ús inadequat d\'inhibidors de bomba de protons', 
                'punts': 0.5
            },
            'EQA0224A': {
                'descripcio': u'Tractament inadequat de la hiperuricemia assimptomatica', 
                'punts': 0.5
            },
            'EQA0228A': {
                'descripcio': u'Tractament inadequat en osteoporosi', 
                'punts': 0.5
            },
            'EQA0239A': {
                'descripcio': u'Ús incorrecte del PSA', 
                'punts': 0.5
            }
        },
        "detall": u'\"Incidència\": \"Atenció amb prevenció quaternària\", \"Motiu\": \"{txt}\"',
        "max_punts": 2,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "VAC01":
    {
        "etiqueta": 'Vacunes',
        "indicadors": 
        {
            'EQA0508A': {
                'descripcio': u'Cobertura vacunal sistemàtica fins els 18a', 
                'punts': 1
            },
            'EQA0509A': {
                'descripcio': u'Cobertura vacunal sistemàtica entre 19 i 64 a', 
                'punts': 1
            },
            'EQA0510A': {
                'descripcio': u'Cobertura vacunal sistemàtica en >65a', 
                'punts': 1
            },
            'EQA0248A': {
                'descripcio': u'Cobertura vacunal antipneumocòccica en persones amb Diabetis Mellitus', 
                'punts': 1
            },
            'EQA0249A': {
                'descripcio': u'Cobertura vacunal antipneumocòccica en persones amb MPOC', 
                'punts': 1
            },
        },
        "detall": u'\"Motiu\": \"Cobertura vacunal incomplerta\"',
        "max_punts": 1,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "PDM01":
    {
        "etiqueta": u'Peu DM',
        "indicadors": 
        {
            'EQA0208A': {
                'descripcio': u'Cribratge del peu diabètic', 
                'punts': 1
            }
        },
        "detall": u'\"Incidència\": \"Atenció en el control neurovascular del peu\", \"Motiu\": \"Sense avaluació en els darrers 12m\"', # , \"Data\": \"{data}\"',
        "max_punts": 1,
        "valor": False,
        "data": True,
        "situacio": ''
    },
    "OBS01":
    {
        "etiqueta": u'Obesitat',
        "indicadors": 
        {
            'EQD031302': {
                'descripcio': u'Criteris diagnòstics obesitat', 
                'punts': 0.5
            },
            'EQD031301': {
                'descripcio': u'Adequació diagnòstica obesitat', 
                'punts': 0.5
            }
        },
        "detall": u'\"Motiu\": \"{txt}\"', # , \"Data\": \"{data}\"',
        "max_punts": 0.5,
        "valor": False,
        "data": True,
        "situacio": ''
    },
    "PRESC01":
    {
        "etiqueta": u'Prescripció',
        "indicadors": 
        {
            'IMP043': {
                'descripcio': u'Pacients amb més de 5 bolquers al dia', 
                'punts': 1
            },
            'IMP044': {
                'descripcio': u'Pacients amb prescripció de bolquer rectangular', 
                'punts': 1
            },
            'IMP043': {
                'descripcio': u'Pacients amb més de 2 bolquers nit/super nit al dia', 
                'punts': 1
            }
        },
        "detall": u'\"Incidència\": \"Incidència en la prescripció de bolquers{txt}\", \"Motiu\": \"Prescripció de bolquers incorrecta\"',
        "max_punts": 1,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "ATD01":
    {
        "etiqueta": u'Val. ATDOM',
        "indicadors": 
        {
            'EQA0402A': {
                'descripcio': u'Valoració del risc UPP en ATDOM incompleta', 
                'punts': 0.5
            },
            'EQA0401A': {
                'descripcio': u'Valoració integral en ATDOM incompleta', 
                'punts': 0.5
            },
            'EQA0403A': {
                'descripcio': u'Valoració ambient segur ATDOM incompleta', 
                'punts': 0.5
            },
            'EQA0404A': {
                'descripcio': u'Valoració sobrecàrrega cuidador incompleta', 
                'punts': 0.5
            }
        },
        "detall": u'\"Motiu\": \"{txt}\"', #, \"Data\": \"{data}\"',
        "max_punts": 1,
        "valor": False,
        "data": True,
        "situacio": ''
    },
    "GRIP_COVID":
    {
        "etiqueta": u'Grip/Covid',
        "indicadors": 
        {
            'EQA0501A': {
                'descripcio': u'Vacunació de la grip en majors de 59 anys', 
                'punts': 1
            },
            'EQA0502A': {
                'descripcio': u'Vacunació de la grip en població de risc', 
                'punts': 1
            },
            'EQA0503A': {
                'descripcio': u'Vacunació de COVID en majors de 59 anys', 
                'punts': 1
            },
            'EQA0504A': {
                'descripcio': u'Vacunació de COVID en població de risc', 
                'punts': 1
            },
            'EQA0506A': {
                'descripcio': u'Cobertura vacunal de grip en persones que viuen en la residència de gent gran', 
                'punts': 1
            },
            'EQA0507A': {
                'descripcio': u'Cobertura vacunal de COVID en persones que viuen en la residència de gent gran', 
                'punts': 1
            }
        },
        "detall": u'\"Motiu\": \"Cobertura vacunal campanya grip/covid incomplerta\"',
        "max_punts": 1,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "POLIF01":
    {
        "etiqueta": u'Polifarmàcia',
        "indicadors": 
        {
            'ATD043A1': {
                'descripcio': u'Polifarmàcia > 10 fàrmacs', 
                'punts': 0.5
            }
        },
        "detall": u'\"Incidència\": \"Atenció a les persones cròniques amb polifarmàcia\", \"Motiu\": \"Polimedicació > 10 fàrmacs\"',
        "max_punts": 0.5,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "VPCC01": # unificar amb VMACA01
    {
        "etiqueta": u'Val. Integral',
        "indicadors": 
        {
            'QCCRO023': {
                'descripcio': u'Valoració integral en PCC incomplerta', 
                'punts': 1
            },
            'QCCRO024': {
                'descripcio': u'Valoració integral en MACA incomplerta', 
                'punts': 1
            }
        },
        "detall": u'\"Motiu\": \"{txt}\"', #, \"Data\": \"{data}\"',
        "max_punts": 1,
        "valor": False,
        "data": True,
        "situacio": ''
    },
    "VACRES01":
    {
        "etiqueta": u'Vacunes RES',
        "indicadors": 
        {
            'EQA0505': {
                'descripcio': u'Cobertura vacunal sistemàtica en persones que viuen en una residència de gent gran', 
                'punts': 1
            },
            'EQA0505A': {
                'descripcio': u'Cobertura vacunal Td en persones que viuen en una residència de gent gran', 
                'punts': 1
            },
            'EQA0505B': {
                'descripcio': u'Cobertura vacunal Pneumocòccica en persones que viuen en una residència de gent gran', 
                'punts': 1
            },
            'EQA0505C': {
                'descripcio': u'Cobertura vacunal Herpes Zòster en persones que viuen en una residència de gent gran', 
                'punts': 1
            },
        },
        "detall": u'\"Motiu\": \"Cobertura vacunal incomplerta\"',
        "max_punts": 1,
        "valor": False,
        "data": False,
        "situacio": ''
    },
    "VIRES01":
    {
        "etiqueta": u'Val. Integral RES',
        "indicadors": 
        {
            'RES025A': {
                'descripcio': u'Valoració integral en residències', 
                'punts': 0.5
            }
        },
        "detall": u'\"Incidència\": \"Incidència en la valoració integral\", \"Motiu\": \"Pendent valoració integral\"',
        "max_punts": 0.5,
        "valor": False,
        "data": False,
        "situacio": ''
    }
}
class Landing():
    def __init__(self):
        for db in ('exadata_pre',):
            sql = """DELETE FROM dwlanding.INDICADORS_PACIENTS_tr WHERE INDICADOR IN
                    (SELECT INDICADOR FROM DWLANDING.indicadors
                    WHERE RESPONSABLE = 'SISAP')"""
            u.execute(sql, db)
        cols = """(
            cip varchar(14),
            etiqueta varchar(100),
            indicador varchar(100),
            up varchar(10),
            uba varchar(10), 
            ubainf varchar(10),
            punts int,
            valor varchar(200),
            detall varchar(2000),
            situacio varchar(200),
            dextraccio date,
            avui date
        )"""
        u.createTable('proves_landing', cols, 'eqa_ind', rm=True)
        sql = """select data_ext from dextraccio"""
        for dext, in u.getAll(sql, 'nodrizas'):
            self.data_ext = dext
        self.today = datetime.now()
        print('inici')
        self.get_data()
        print('get_data')
        self.transform_data()
        print('transform_data')
        self.uploading()
        print('fi')

    def get_data(self):
        # inversos
        print('inversos')
        cat_inversos = {}
        sql = """select indicador, invers from eqa_ind.exp_ecap_cataleg
                union
                select indicador, invers from eqa_ind.exp_ecap_cataleg_proc
                union
                select indicador, invers from eqa_ind.exp_ecap_resis_cataleg
                union
                select indicador, invers from altres.exp_ecap_qc_atdom_cat
                union
                select indicador, invers from resis.exp_ecap_resi_cataleg
                union
                select indicador, invers from altres.exp_ecap_imp_cataleg"""
        for indicador, invers in u.getAll(sql, 'eqa_ind'):
            cat_inversos[indicador] = invers

        print('indicadors')
        self.conversio_indicadors_2_lletra = {}
        self.data = c.defaultdict(set)
        sql = """select id_cip_sec, ind_codi, grup_codi,
                up, uba, ubainf, num
                from eqa_ind.mst_indicadors_pacient
                where den = 1 and excl = 0 and ci = 0 
                and clin = 0 and excl_edat = 0
                and ates = 1
                and id_cip_sec > 0""" # int
        for id_cip_sec, ind_codi, grup_codi, up, uba, ubainf, num in u.getAll(sql, 'eqa_ind'):
            self.conversio_indicadors_2_lletra[grup_codi] = ind_codi
            if grup_codi in cat_inversos:
                invers = cat_inversos[grup_codi]
                if invers == 1 and num == 1:
                    self.data[(id_cip_sec, up, uba, ubainf)].add(ind_codi)
                if invers == 0 and num == 0:
                    self.data[(id_cip_sec, up, uba, ubainf)].add(ind_codi)
        
        # bolet per procés diabetis
        sql = """select id_cip_sec, ind_codi, 'EQA3102' ind,
                up, uba, ubainf, num
                from eqa_ind.mst_indicadors_pacient_dm2
                where den = 1 and excl = 0 
                and ates = 1
                and id_cip_sec > 0
                and ind_codi in ('EQA3102A')"""
        for id_cip_sec, ind_codi, grup_codi, up, uba, ubainf, num in u.getAll(sql, 'eqa_ind'):
            self.conversio_indicadors_2_lletra[grup_codi] = ind_codi
            if grup_codi in cat_inversos:
                invers = cat_inversos[grup_codi]
                if invers == 1 and num == 1:
                    self.data[(id_cip_sec, up, uba, ubainf)].add(ind_codi)
                if invers == 0 and num == 0:
                    self.data[(id_cip_sec, up, uba, ubainf)].add(ind_codi)

        # bolet per procés atdom
        sql = """
            SELECT id_cip_sec, ind, 'ATD043A1', up, uba, ubainf, num
            FROM atdom.landing_atdom
            WHERE ind = 'ATD043A1'
        """
        for id_cip_sec, ind_codi, grup_codi, up, uba, ubainf, num in u.getAll(sql, 'atdom'):
            self.conversio_indicadors_2_lletra[grup_codi] = ind_codi
            if grup_codi in cat_inversos:
                invers = cat_inversos[grup_codi]
                if invers == 1 and num == 1:
                    self.data[(id_cip_sec, up, uba, ubainf)].add(ind_codi)
                if invers == 0 and num == 0:
                    self.data[(id_cip_sec, up, uba, ubainf)].add(ind_codi)

        # bolet per procés qccro
        sql = """
            SELECT id_cip_sec, ind, ind, up, uba, ubainf, num
            FROM altres.landing_qccro
            WHERE poblacio in ('INSASSCRO', 'NOINSASSCRO')
        """
        for id_cip_sec, ind_codi, grup_codi, up, uba, ubainf, num in u.getAll(sql, 'altres'):
            self.conversio_indicadors_2_lletra[grup_codi] = ind_codi
            # if grup_codi in cat_inversos:
            #     invers = cat_inversos[grup_codi]
                # if invers == 1 and num == 1:
                #     self.data[(id_cip_sec, up, uba, ubainf)].add(ind_codi)
            if num == 0:
                self.data[(id_cip_sec, up, uba, ubainf)].add(ind_codi)
        
        # bolet per procés resis
        sql = """
            SELECT id_cip_sec, ind, 'RES025A', up, uba, ubainf, num
            FROM resis.landing_resis
            WHERE ind = 'RES025A'
        """
        for id_cip_sec, ind_codi, grup_codi, up, uba, ubainf, num in u.getAll(sql, 'resis'):
            self.conversio_indicadors_2_lletra[grup_codi] = ind_codi
            if grup_codi in cat_inversos:
                invers = cat_inversos[grup_codi]
                if invers == 1 and num == 1:
                    self.data[(id_cip_sec, up, uba, ubainf)].add(ind_codi)
                if invers == 0 and num == 0:
                    self.data[(id_cip_sec, up, uba, ubainf)].add(ind_codi)

        # bolet per procés imp
        sql = """
            SELECT id_cip_sec, ind, ind, up, uba, ubainf, num
            FROM catsalut.landing_imp
        """
        for id_cip_sec, ind_codi, grup_codi, up, uba, ubainf, num in u.getAll(sql, 'catsalut'):
            self.conversio_indicadors_2_lletra[grup_codi] = ind_codi
            if grup_codi in cat_inversos:
                invers = cat_inversos[grup_codi]
                if invers == 1 and num == 1:
                    self.data[(id_cip_sec, up, uba, ubainf)].add(ind_codi)
                if invers == 0 and num == 0:
                    self.data[(id_cip_sec, up, uba, ubainf)].add(ind_codi)

        print('valors')
        self.valors = c.defaultdict(dict)
        sql = """select id_cip_sec, grup_codi, valor
                from eqa_ind.exp_ecap_pacient_new
                where valor != ''""" # int
        for id, codi, valor in u.getAll(sql, 'eqa_ind'):
            if codi in self.conversio_indicadors_2_lletra:
                codi_lletra = self.conversio_indicadors_2_lletra[codi]
                valors = valor.split('(')
                valor = valors[0].strip() 
                if len(valors) == 2:
                    data = valors[1].replace(")", "")
                else: data = ''
                self.valors[id][codi_lletra] = (valor, data)
        print(len(self.valors))
        for k, v in self.valors.items():
            print(k, v)
            break

        print("getting the pool party of the hash conversion")
        print('u11')
        hash_2_id_cip = {}
        sql = """select id_cip_sec, hash_d, codi_sector from import.u11""" # int
        for id_cip, hash, sector in u.getAll(sql, 'import'):
            hash_2_id_cip[(hash, sector)] = id_cip
        
        print('hash relation')
        hash_covid_2_redics = {}
        sql = """SELECT hash_redics, hash_covid FROM dwsisap.pdptb101_relacio"""
        for hash_r, hash_c in u.getAll(sql, 'exadata'):
            hash_covid_2_redics[hash_c] = hash_r

        print('real cip 14')
        # volem cip de 14
        self.id_cip_2_cip = {}
        sql = """SELECT hash, rca_cip, ecap_sector FROM dwsisap.dbc_poblacio WHERE situacio = 'A'"""
        for hash_c, cip, sector in u.getAll(sql, 'exadata'):
            if hash_c in hash_covid_2_redics:
                hash_r = hash_covid_2_redics[hash_c]
                if (hash_r, sector) in hash_2_id_cip:
                    id_cip = hash_2_id_cip[(hash_r, sector)]
                    self.id_cip_2_cip[id_cip] = cip

    
    def transform_data(self):
        print('transform data')
        self.upload = []
        for (id_cip_sec, up, uba, ubainf), indicadors in self.data.items():
            if id_cip_sec in self.id_cip_2_cip:
                cip = self.id_cip_2_cip[id_cip_sec]
                if cip:
                    for indi in CODIS:
                        punts = 0
                        valor = ''
                        detall = ''
                        situacio = '0'
                        txt = set()
                        detall = ''
                        v = u'Cap determinació en el període'
                        data = ''
                        afegir = False
                        indi_in = CODIS[indi]["indicadors"].keys()
                        j = 0
                        for i in indi_in:
                            if i in indicadors:
                                j += 1
                                afegir = True
                                punts += CODIS[indi]["indicadors"][i]["punts"]
                                if CODIS[indi]["indicadors"][i]["descripcio"] != '':
                                    txt.add(CODIS[indi]["indicadors"][i]["descripcio"])
                                if i in self.valors[id_cip_sec]:
                                    v, data = self.valors[id_cip_sec][i]

                        # situacio
                        if CODIS[indi]["situacio"] != '':
                            for i in CODIS[indi]["situacio"]:
                                if i in indi_in: situacio = '1'
                        # fita superior maxim de punts
                        max_punts = CODIS[indi]["max_punts"]
                        if punts > max_punts: 
                            punts = max_punts
                        # detall
                        detalls = set()
                        for t in txt:
                            if CODIS[indi]["valor"] == True and CODIS[indi]["data"] == True: 
                                detalls.add(CODIS[indi]["detall"].format(txt=t, data=data, valor=v))
                            else: 
                                detalls.add(CODIS[indi]["detall"].format(txt=t))
                        # inici
                        detall = u'{\"detall\": ['
                        max_len = len(detalls)
                        if max_len > 0:
                            for i, d in enumerate(detalls):
                                detall = detall + u'{' + d + u'}'
                                # posem la coma si hi ha més d'un item
                                if i != max_len - 1:
                                    detall = detall + ','
                        else:
                            if CODIS[indi]["valor"] == True and CODIS[indi]["data"] == True: 
                                detall = detall + '{' + CODIS[indi]["detall"].format(txt='', data=data, valor=v) + '}'
                            else:
                                # print(indi)
                                # print(detall)
                                detall = detall + '{' + CODIS[indi]["detall"].format(txt='') + '}'
                                
                        # tanquem json
                        detall = detall + u']}'
                        if afegir:
                            if CODIS[indi]["etiqueta"] == 'EQD':
                                etiqueta = CODIS[indi]["etiqueta"] + u'(' + str(j) + u')'
                            elif CODIS[indi]["etiqueta"] in ('LDL', 'DLP'):
                                if v != u'Cap determinació en el període':
                                    etiqueta = CODIS[indi]["etiqueta"] + u'(' + str(v) + u')'
                                else:
                                    etiqueta = CODIS[indi]["etiqueta"] 
                            else: 
                                etiqueta = CODIS[indi]["etiqueta"] 
                            self.upload.append((cip, etiqueta, indi, up, uba, ubainf, punts, valor, detall, situacio, self.data_ext, self.today))

    def uploading(self):
        print('uploading data')
        # u.listToTable(self.upload, 'proves_landing', 'eqa_ind')
        print(self.upload[0])
        batch = list()
        count=0
        for db in ('exadata_pre',):
            for row in self.upload:
                batch.append(row)
                count+=1
                if count%100000 == 0:
                    u.listToTable(batch, 'dwlanding.INDICADORS_PACIENTS_tr', db)
                    batch = list()
            u.listToTable(batch, 'dwlanding.INDICADORS_PACIENTS_tr', db)
    # ups @Nuria afegeix la resta no múltiple

if __name__ == "__main__":
    Landing() 
    for db in ('exadata_pre',):
        u.execute("""begin
                        delete from dwlanding.INDICADORS_PACIENTS WHERE INDICADOR IN
                            (SELECT INDICADOR FROM DWLANDING.indicadors WHERE RESPONSABLE = 'SISAP');
                        insert into dwlanding.INDICADORS_PACIENTS select * from dwlanding.INDICADORS_PACIENTS_tr
                                WHERE INDICADOR IN (SELECT INDICADOR FROM DWLANDING.indicadors
                                WHERE RESPONSABLE = 'SISAP');
                        commit;
                        end;""", db)
    #Developing()
    # try:
    #     Landing()  
    # except Exception as e:
    #     mail = t.Mail()
    #     mail.to.append("roser.cantenys@catsalut.cat")
    #     mail.subject = "LANDNGGG!!!"
    #     mail.text = str(e)
    #     mail.send()    
    



