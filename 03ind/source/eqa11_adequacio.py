# coding: latin1

import collections as c
import multiprocessing as m

import sisaptools as u


DEBUG = False
FMO = False


class Main(object):
    """."""

    def get_criteris(self):
        """."""
        sql = "select taula, agrupador, data_min, data_max, \
                      date_add(data_ext, interval - data_min month), \
                      date_add(data_ext, interval - data_max month), \
                      valor_min, valor_max, var_num, a.ind_codi, tipus, \
                      numero_criteri \
               from eqa_relacio a, nodrizas.dextraccio \
               where baixa = 0 and \
                     a.ind_codi in \
                        (select b.ind_codi from eqa_ind b \
                         where tipus = 11 and baixa_ = 0)"
        self.criteris = c.defaultdict(set)
        self.agrupadors = c.defaultdict(set)
        self.recomptes = c.Counter()
        self.n_criteris = c.defaultdict(set)
        for i, row in enumerate(u.Database("p2262", "eqa_ind").get_all(sql)):
            self.criteris[row[:2]].add((i,) + row[2:])
            self.agrupadors[(row[0], row[10] == "den")].add(int(row[1]))
            self.recomptes[(row[9], row[10], row[11], i)] = row[8] if row[8] else 1  # noqa
            self.n_criteris[(row[9], row[10])].add(row[11])

    def create_structure(self):
        """."""
        sql = "select ind_codi from eqa_ind \
               where tipus = 11 and baixa_ = 0"
        cols = ("id_cip_sec int", "up varchar(5)", "uba varchar(5)",
                "upinf varchar(5)", "ubainf varchar(5)", "edat int",
                "sexe varchar(1)", "seccio_censal varchar(10)",
                "up_residencia varchar(13)", "institucionalitzat int",
                "maca int", "ates int", "trasplantat int",
                "ind_codi varchar(10)", "num int", "excl int", "ci int",
                "clin int")
        with u.Database("p2262", "eqa_ind") as eqa:
            codis = [cod for cod, in eqa.get_all(sql)]
            for cod in codis:
                taula = "{}_ind2{}".format(cod, "_fmo" if FMO else "")
                eqa.create_table(taula, cols, remove=True,
                                 partition_type="hash",
                                 partition_id="id_cip_sec", partitions=16)

    def get_eqa(self):
        """."""
        if DEBUG:
            Particio("p3", self.criteris, self.agrupadors, self.recomptes,
                     self.n_criteris)
        else:
            pool = m.Pool(16)
            results = []
            for i in range(16):
                particio = "p{}".format(i)
                args = (particio, self.criteris, self.agrupadors,
                        self.recomptes, self.n_criteris)
                results.append(pool.apply_async(Particio, args=args))
            pool.close()
            pool.join()
            for result in results:
                try:
                    result.get()
                except Exception:
                    raise

    def get_control(self):
        """."""
        upload = []
        sql = "select ind_codi from eqa_ind \
               where tipus = 11 and baixa_ = 0"
        for ind, in u.Database("p2262", "eqa_ind").get_all(sql):
            for sufix in ("", "_fmo"):
                taula = "{}_ind2{}".format(ind, sufix)
                this = "select count(1), sum(num), sum(excl), sum(ci), \
                               sum(clin) \
                        from {}".format(taula)
                nums = u.Database("p2262", "eqa_ind").get_one(this)
                if not nums:
                    nums = (0,) * 5
                that = (ind, sufix) + nums
                upload.append(that)
        cols = ("ind varchar(10)", "suffix varchar(5)", "cnt int", "num int",
                "excl int", "ci int", "clin int")
        with u.Database("p2262", "eqa_ind") as eqa:
            eqa.create_table("eqa11_fmo", cols, remove=True)
            eqa.list_to_table(upload, "eqa11_fmo")


class Particio(object):
    """."""

    def __init__(self, particio, criteris, agrupadors, recomptes, n_criteris):
        """."""
        self.particio = particio
        self.criteris = criteris
        self.agrupadors = agrupadors
        self.recomptes = recomptes
        self.n_criteris = n_criteris
        self.get_denominadors()
        self.get_atributs()
        self.get_indicadors()
        self.upload_data()
        self.denominadors = []
        self.upload = []

    def get_denominadors(self):
        """."""
        agrs = tuple(self.agrupadors[("problemes", True)])
        sql = "select id_cip_sec, ps, dde \
               from prt_problemes partition({}) \
               where ps in {} and incident = 1".format(self.particio, agrs)
        self.denominadors = {}
        for id, cod, ini in u.Database("p2262", "nodrizas").get_all(sql):
            for i, t_min, t_max, d_min, d_max, v_min, v_max, v_num, i_ind, i_tipus, n_crit in self.criteris[("problemes", cod)]:  # noqa
                if i_tipus == "den":
                    self.denominadors[(i_ind, id)] = [ini, c.Counter()]

    def get_atributs(self):
        """."""
        taules = (
            ["variables", "prt_variables", ("agrupador", "data_var", "valor")],
            ["problemes", "prt_problemes", ("ps", "dde", "pr_gra")],
            ["proves", "prt_proves", ("agrupador", "data", "null")]
        )
        mitjanes = c.defaultdict(list)
        for key, origen, columnes in taules:
            args = (", ".join(columnes), origen, self.particio, columnes[0],
                    tuple(self.agrupadors[(key, False)]))
            sql = "select id_cip_sec, {} \
                   from {} partition({}) \
                   where {} in {}".format(*args)
            for id, cod, dat, val in u.Database("p2262", "nodrizas").get_all(sql):  # noqa
                for i, t_min, t_max, d_min, d_max, v_min, v_max, v_num, i_ind, i_tipus, n_crit in self.criteris[(key, cod)]:  # noqa
                    if (i_ind, id) in self.denominadors:
                        if i_tipus != "den":
                            if key in ("variables", "proves"):
                                if cod in (16, 17) and (i_ind != 'EQD024001' or v_min not in (180, 110)):
                                    dde = self.denominadors[(i_ind, id)][0]
                                    if dat <= dde:
                                        diff = u.months_between(dde, dat)
                                        if diff >= -12:
                                            mitjanes[id].append((cod, dat, val))  # noqa
                                if not v_max or v_min <= val <= v_max:
                                    dde = self.denominadors[(i_ind, id)][0]
                                    diff = u.months_between(dde, dat)
                                    enrera = -12
                                    if i_ind == "EQD024001":
                                        endavant = 3
                                        if cod in (16, 17) and v_min in (180, 110):
                                            if dat <= dde and diff >= -12:
                                                self.denominadors[(i_ind, id)][1][(i_tipus, n_crit, i)] += 1 # noqa
                                        elif cod in (16, 17):
                                            enrera = 1
                                            endavant = 90
                                            diff = (dat - dde).days
                                    else:
                                        endavant = t_max if t_max else 6
                                    if enrera <= diff <= endavant and (cod not in (16, 17) or v_min not in (180, 110)):
                                        self.denominadors[(i_ind, id)][1][(i_tipus, n_crit, i)] += 1  # noqa
                            elif key == "problemes":
                                self.denominadors[(i_ind, id)][1][(i_tipus, n_crit, i)] += 1  # noqa
        for id, dades in mitjanes.items():
            this = c.defaultdict(list)
            for cod, dat, val in sorted(dades, reverse=True):
                if len(this[cod]) < 3:
                    this[cod].append(val)
            if (sum(this[16]) / 3.0) >= 140 or (sum(this[17]) / 3.0) >= 90:
                self.denominadors[("EQD024001", id)][1][("num", 1, 999)] += 1
        self.recomptes[("EQD024001", "num", 1, 999)] = 1

    def get_indicadors(self):
        """."""
        sql = "select id_cip_sec, up, uba, upinf, ubainf, edat, sexe, \
                      seccio_censal, up_residencia, institucionalitzat, \
                      maca, ates, trasplantat \
               from assignada_tot partition({})".format(self.particio)
        poblacio = {row[0]: row for row
                    in u.Database("p2262", "eqa_ind").get_all(sql)}
        self.upload = c.defaultdict(list)
        for (ind, id), dades in self.denominadors.items():
            if id in poblacio:
                crits = c.defaultdict(set)
                for (tipus, n_crit, i), n in dades[1].items():
                    if n >= self.recomptes[(ind, tipus, n_crit, i)]:
                        crits[tipus].add(n_crit)
                num = 1 * (crits["num"] == self.n_criteris[(ind, "num")])
                excl = 1 * (len(crits["excl"]) > 0 and crits["excl"] == self.n_criteris[(ind, "excl")])  # noqa
                this = poblacio[id] + (ind, num, excl, 0, 0)
                self.upload[ind].append(this)

    def upload_data(self):
        """."""
        for key, rows in self.upload.items():
            taula = "{}_ind2{}".format(key, "_fmo" if FMO else "")
            u.Database("p2262", "eqa_ind").list_to_table(rows, taula, partition=self.particio)  # noqa


if __name__ == "__main__":
    main = Main()
    main.get_criteris()
    main.create_structure()
    main.get_eqa()
    if FMO:
        main.get_control()
