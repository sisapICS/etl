# -*- coding: utf-8 -*-

import sisapUtils as u
import sisaptools as su
import sys
import collections as c
import dateutil.relativedelta
from datetime import timedelta
import datetime as d

# EQD023803@problemes@den@1@21@@@@@@@@@
# EQD023803@problemes@num@1@275@@@@@@@@@
# EQD023803@problemes@num@1@593@@@@@@@@@

RECOVER = False
# calculem l'indicador EQD023803 (bolet)
DEXTD = d.date(2021, 8, 31) if RECOVER else su.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]  # noqa
sql = """
    SELECT id_cip_sec, ps
    FROM eqa_problemes
    WHERE ps in (21, 45)
    and dde <= DATE '{DEXTD}'
""".format(DEXTD=DEXTD)
den = set()
excl = set()
for id, ps in u.getAll(sql, 'nodrizas'):
    if ps == 21:
        den.add(id)
    else:
        excl.add(id)
for id in list(den):
    if id in excl:
        den.remove(id)
# agafem valors de FE
sql = """
    SELECT id_cip_sec, valor
    from eqa_variables
    where agrupador = 1054
"""
fe = c.defaultdict(list)
for id, valor in u.getAll(sql, 'nodrizas'):
    fe[id].append(valor)
# agafem codis de diagnostic + thesaurus
sql = """
    SELECT id_cip_sec, pr_cod_ps, pr_th
    FROM {tb}
    WHERE (pr_cod_ps = 'C01-I50.30'
    AND pr_th in (16569, 16572,16575))
    OR (pr_cod_ps = 'C01-I50.20'
    AND pr_th in (16567,16573,16570,16568,16571,16574))
    AND (pr_data_baixa = 0 OR pr_data_baixa > DATE '{DEXTD}')
"""

dx = set()
for table in u.getSubTables('problemes'):
    for id, codi, th in u.getAll(sql.format(tb=table,DEXTD=DEXTD), 'import'):
        if id in den:
            dx.add((id, codi, th))
            # if codi == 'C01-50.30':
            #     if id in fe and fe[id] >= 50:
            #         dx.add(id)
            # else:
            #     if th in (16567,16573,16570):
            #         if id in fe and fe[id] < 40:
            #             dx.add(id)
            #     elif id in fe and fe[id] >= 40 and fe[id] <= 49:
            #         dx.add(id)
nums = set()
for id,codi,th in dx:
    if id in fe and id not in nums:
        for val in fe[id]:
            if codi == 'C01-I50.30' and val >= 50:
                nums.add(id)
            elif codi == 'C01-I50.20':
                if th in (16567,16573,16570) and val < 40:
                    nums.add(id)
                elif th in (16568,16571,16574) and val >= 40 and val <= 49:
                    nums.add(id)
pob = c.defaultdict()
sql = """
    SELECT id_cip_sec, up, uba, upinf, ubainf, edat, sexe, seccio_censal,
    up_residencia, institucionalitzat, maca, ates, trasplantat
    FROM assignada_tot
"""
for id, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat in u.getAll(sql, 'nodrizas'):
    pob[id] = (up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat)
# agrupem indicador per up i uba
upload = []
for id in den:
    if id in pob:
        up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat = pob[id]
        num = 0
        if id in nums:
            num = 1
        upload.append((id, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat, 'EQD023803', num, 0,0,0))
cols = """(id_cip_sec double, up varchar(5), uba varchar(7), upinf varchar(5),
        ubainf varchar(7), edat double, sexe varchar(7), seccio_censal varchar(10),
        up_residencia varchar(13), institucionalitzat double, maca double,
        ates double, trasplantat double, ind_codi varchar(10), num double,
        excl double, ci double, clin double)"""
u.createTable('eqd023803_ind2', cols, 'eqa_ind', rm=True)
u.listToTable(upload, 'eqd023803_ind2', 'eqa_ind')