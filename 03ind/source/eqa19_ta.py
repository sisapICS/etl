# coding: latin1

"""
.
"""

import collections as c
import sisapUtils as u
from datetime import timedelta


DB = 'eqa_ind'
VAR_TB = 'eqa_variables'
TIPUS = 19
MAPAS = 482  # 'EK2081'
MAPAD = 483  # 'EK2082'
AMPAS = 906  # 'EK2091'
AMPAD = 907  # 'EK2092'
TAS = 674  # Mitjana 3 ultimes TAS
TAD = 675  # Mitjan 3 ultimes TAD

NOD = "nodrizas"

OBERTS = {
    "dm1": 24,
    "dm2": 18,
    "hta": 55,
    "irc": 53,
    "ci": 1,
    "avc": 7,
}

TANCATS = {
    "ci_t": 211,
    "avc_t": 212
}


def get_past_date(x_anys):
    "returns the datetime.date (x_anys) in the past since dextraccio.data_ext"
    n_days = x_anys * 365
    for data, in u.getAll("SELECT data_ext FROM dextraccio", NOD):
        past_date = data - timedelta(days=n_days)
    return past_date


probs = c.defaultdict(set)
for ps, cod in OBERTS.items():
    sql = """
        select
            id_cip_sec
        from
            eqa_problemes
        where
            ps = {cod}
        """.format(cod=cod)
    for cip, in u.getAll(sql, NOD):
        probs[ps].add(cip)
for ps, cod in TANCATS.items():
    sql = """
        select
            id_cip_sec
        from
            eqa_problemes
        where
            ps = {cod} and
            dde >= date '{fa_anys}'
        """.format(cod=cod, fa_anys=get_past_date(5))
    for cip, in u.getAll(sql, NOD):
        probs[ps].add(cip)

sql = """
    with data_pac as (
        select
            id_cip_sec as id_c, max(data_var) as max_data
        from
            nodrizas.eqa_variables
        where
            agrupador in (674, 675)
            and usar = 1
            -- and id_cip_sec = 1462835
        group by
            id_cip_sec)
    select
        id_cip_sec,
        data_var,
        max(case when agrupador = 674 then valor end) as tas,
        max(case when agrupador = 675 then valor end) as tad
    from
        eqa_variables,
        data_pac
    where
        id_cip_sec = data_pac.id_c
        and data_var = data_pac.max_data
        and agrupador in (674, 675)
    group by
        id_cip_sec,
        data_var
    """
data_pac = [row for row in u.getAll(sql, NOD)]

sql = """
    with data_pac as (
        select
            id_cip_sec as id_c, max(data_var) as max_data
        from
            nodrizas.eqa_variables
        where
            agrupador in (906, 907)
            and usar = 1
            -- and id_cip_sec = 1462835
        group by
            id_cip_sec)
    select
        id_cip_sec,
        data_var,
        max(case when agrupador = 906 then valor end) as tas,
        max(case when agrupador = 907 then valor end) as tad
    from
        eqa_variables,
        data_pac
    where
        id_cip_sec = data_pac.id_c
        and data_var = data_pac.max_data
        and agrupador in (906, 907)
        and data_var >= DATE '{fa_anys}'
    group by
        id_cip_sec,
        data_var
    """.format(fa_anys=get_past_date(1))
data_ampa = [row for row in u.getAll(sql, NOD)]

sql = """
    with data_pac as (
        select
            id_cip_sec as id_c, max(data_var) as max_data
        from
            nodrizas.eqa_variables
        where
            agrupador in (482, 483)
            and usar = 1
            -- and id_cip_sec = 1462835
        group by
            id_cip_sec)
    select
        id_cip_sec,
        data_var,
        max(case when agrupador = 482 then valor end) as tas,
        max(case when agrupador = 483 then valor end) as tad
    from
        eqa_variables,
        data_pac
    where
        id_cip_sec = data_pac.id_c
        and data_var = data_pac.max_data
        and agrupador in (482, 483)
        and data_var >= DATE '{fa_anys}'
    group by
        id_cip_sec,
        data_var
    """.format(fa_anys=get_past_date(1))
data_mapa = [row for row in u.getAll(sql, NOD)]

# sql = """
#     SELECT id_cip_sec
#     FROM u11
#     WHERE (hash_d = '373455772B79E37A7C9DAC8DF599E0EC1483078F'
#     AND codi_sector = '6731')
#     OR (hash_d = 'FD7176513B13D6D17B7AB9F89A8F1897DBE320B9'
#     AND codi_sector = '6416')
# """
# exclusio = [id for id, in u.getAll(sql, 'import')]

INDICADOR = {
    "EQA0212A": {
        "prob": probs['dm2'],
        "edats": (15, 80),
        "mapa": (140, 85),
        "ampa": (145, 90),
        "pac": (150, 95)},
    "EQA0213A": {
        "prob": probs['hta'],
        "edats": (15, 60),
        "mapa": (140, 85),
        "ampa": (145, 90),
        "pac": (150, 95)},
    "EQA0213B": {
        "prob": probs['hta'],
        "edats": (61, 80),
        "mapa": (145, 90),
        "ampa": (145, 90),
        "pac": (160, 95)},
    "EQA0235A": {
        "prob": probs['hta'] & probs['irc'],
        "edats": (15, 80),
        "mapa": (140, 85),
        "ampa": (145, 90),
        "pac": (150, 95)},
    "EQA0205A": {
        "prob": ((probs['ci'] | probs['ci_t']) - probs['avc']),
        "edats": (15, 80),
        "mapa": (130, 80),
        "ampa": (135, 85),
        "pac": (140, 90)},
    "EQA0205B": {
        "prob": ((probs['avc'] | probs['avc_t']) - probs['ci']),
        "edats": (15, 80),
        "mapa": (130, 80),
        "ampa": (135, 85),
        "pac": (140, 90)},
    "EQA0205C": {
        "prob": ((probs['ci'] | probs['ci_t']) &
                 (probs['avc'] | probs['avc_t'])),
        "edats": (15, 80),
        "mapa": (130, 80),
        "ampa": (135, 85),
        "pac": (140, 90)},
    "PS0001": {
        "prob": probs['hta'],
        "edats": (15, 80),
        "mapa": (140, 85),
        "ampa": (145, 90),
        "pac": (150, 95)},
    }

sql = """
    SELECT id_cip_sec
    FROM u11
    WHERE (hash_d = '373455772B79E37A7C9DAC8DF599E0EC1483078F'
    AND codi_sector = '6731')
    OR (hash_d = 'FD7176513B13D6D17B7AB9F89A8F1897DBE320B9'
    AND codi_sector = '6416')
    OR (hash_d = '1DE5343DC5218FCCFCFB943E7F86487AF04BE2C2'
    AND codi_sector = '6734')
    OR (hash_d = '0009A9BC42BDAF48BF7847766BB4441303EC33E8'
    AND codi_sector = '6734')
"""
exclusio = [id for id, in u.getAll(sql, 'import')]

class HTA(object):
    """."""

    def __init__(self, indicador, prob, edats, mapa, ampa, pac):
        """."""
        self.indicador = indicador
        self.prob = prob
        self.edat_min, self.edat_max = edats[0], edats[1]
        self.mapa_tas, self.mapa_tad = mapa[0], mapa[1]
        self.ampa_tas, self.ampa_tad = ampa[0], ampa[1]
        self.pac_tas, self.pac_tad = pac[0], pac[1]

        self.get_poblacio()
        self.get_controls()
        self.get_resolts()
        self.get_master()
        self.upload_data()

    def get_poblacio(self):
        """."""
        sql = """
            select
                id_cip_sec, up, uba, upinf, ubainf, edat, ates_novc,
                trasplantat, sexe, seccio_censal, up_residencia,
                institucionalitzat, maca
            from
                assignada_tot_with_jail
            where
                edat between {emin} and {emax}"""
        self.poblacio = {
            row[0]: row
            for row in u.getAll(
                sql.format(
                    emin=self.edat_min,
                    emax=self.edat_max), NOD) if row[0] in self.prob
            }
        print(len(self.poblacio))

    def get_controls(self):
        """."""
        self.pac = []
        for row in data_pac:
            cip, data, tas, tad = row
            if cip in self.poblacio:
                self.pac.append((cip, data,
                                 tas <= self.pac_tas and
                                 tad <= self.pac_tad))
        print(self.pac[:2])
        self.ampa = []
        for row in data_ampa:
            cip, data, tas, tad = row
            if cip in self.poblacio:
                self.pac.append((cip, data,
                                 tas <= self.ampa_tas and
                                 tad <= self.ampa_tad))
        print(self.ampa[:2])
        self.mapa = []
        for row in data_mapa:
            cip, data, tas, tad = row
            if cip in self.poblacio:
                self.pac.append((cip, data,
                                 tas <= self.mapa_tas and
                                 tad <= self.mapa_tad))
        print(self.mapa[:2])

    def get_resolts(self):
        self.resolts = c.defaultdict()
        for font in [self.pac, self.ampa, self.mapa]:
            for cip, dat, r in font:
                if cip not in self.resolts:
                    self.resolts[cip] = (r, dat)
                else:
                    if r:
                        (r_old, dat_old) = self.resolts[cip]
                        if (r_old and dat > dat_old) or not r_old:
                            self.resolts[cip] = (r, dat)
                    else:
                        if dat <= self.resolts[cip][1]:
                            continue
                        else:
                            self.resolts[cip] = (r, dat)
        # print(self.resolts)

    def get_master(self):
        """."""
        self.master = []
        for id, dades in self.poblacio.items():
            if id in self.resolts:
                resolt = self.resolts.get(id)[0]
            else:
                resolt = False
            self.master.append(dades + (self.indicador, 1 * resolt, 0, 0, 0))

    def upload_data(self):
        """."""
        table = "{}_ind2".format(self.indicador)
        cols = "(id_cip_sec int, up varchar(5), uba varchar(5), \
                 upinf varchar(5), ubainf varchar(5), edat int, ates int, \
                 trasplantat int, sexe varchar(1), seccio_censal varchar(10), \
                 up_residencia varchar(13), institucionalitzat int, maca int, \
                 ind_codi varchar(10), num int, excl int, ci int, clin int)"
        db = "eqa_ind"
        u.createTable(table, cols, db, rm=True)
        u.listToTable(self.master, table, db)


if __name__ == "__main__":
    JUNTAR = {'EQA0213A': ('EQA0213B',)}
    EXCLUSIONS = {
        'EQA0213A': (1, 7, 18, 24, 53),
        'EQA0213B': (1, 7, 18, 24, 53),
        'EQA0212A': (1, 7),
        'EQA0235A': (1, 7)
    
    }

    # Main proc
    for indicador, params in INDICADOR.items():
        print(indicador)
        prob = params["prob"]
        edats = params["edats"]
        mapa = params["mapa"]
        ampa = params["ampa"]
        pac = params["pac"]
        HTA(indicador, prob, edats, mapa, ampa, pac)

    # Ara juntem indicadors, si cal.
    for target, others in JUNTAR.items():
        check = []
        for indicador in others + (target,):
            check.append(indicador in INDICADOR)
        if len(check) > 0 and all(check):  # All checks OK!
            for other in others:
                u.execute(
                    "insert into {target} select * from {other}".format(
                        target="{}_ind2".format(target),
                        other="{}_ind2".format(other)
                        ),
                    DB)

    # ara marquem exclusions
    for target, exclusio in EXCLUSIONS.items():
        u.execute(
            """
            update {target} inner join nodrizas.eqa_problemes b
                on {target}.id_cip_sec = b.id_cip_sec
            set excl = 1
            where b.ps in {excl}
            """.format(
                target="{}_ind2".format(target),
                excl=exclusio
                ),
            DB)
