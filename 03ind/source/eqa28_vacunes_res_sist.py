import sisapUtils as u
import sisaptools as su
import collections as c
import datetime as d

class EQA0248_0249():
    def __init__(self):
        self.get_dext()
        u.printTime('get_dext')
        self.get_den()
        u.printTime('get_den')
        self.get_num()
        u.printTime('get_num')
        self.get_indicadors()
        u.printTime('get_indicadors')
    def get_dext(self):
        sql = """
                SELECT
                    data_ext
                FROM
                    dextraccio
             """
        self.dext, = u.getOne(sql, "nodrizas")
        self.periode = "A{}".format(self.dext.strftime("%y%m"))

    def get_indicadors(self):
        # EQA0248
        inds = c.defaultdict(list)
        for id in self.dens['DM2']:
            if id in self.pob:
                up = self.pob[id]['up']
                uba = self.pob[id]['uba']
                upinf = self.pob[id]['upinf']
                ubainf = self.pob[id]['ubainf']
                edat = self.pob[id]['edat']
                sexe = self.pob[id]['sexe']
                seccio_censal = self.pob[id]['seccio']
                up_residencia = self.pob[id]['resi']
                institucionalitzat = self.pob[id]['inst']
                maca = self.pob[id]['maca']
                ates = self.pob[id]['ates']
                trasplantat = self.pob[id]['trasplantat']
                ind = 'EQA0248A'
                in_num = 0
                in_excl = 0
                if id in self.dens['EXCL']:
                    in_excl = 1
                elif id in self.grups:
                    in_num = 1
                inds[ind].append((id, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat, ind, in_num, in_excl, 0, 0))
        for id in self.dens['POCS']:
            if id in self.pob:
                up = self.pob[id]['up']
                uba = self.pob[id]['uba']
                upinf = self.pob[id]['upinf']
                ubainf = self.pob[id]['ubainf']
                edat = self.pob[id]['edat']
                sexe = self.pob[id]['sexe']
                seccio_censal = self.pob[id]['seccio']
                up_residencia = self.pob[id]['resi']
                institucionalitzat = self.pob[id]['inst']
                maca = self.pob[id]['maca']
                ates = self.pob[id]['ates']
                trasplantat = self.pob[id]['trasplantat']
                ind = 'EQA0249A'
                in_num = 0
                in_excl = 0
                if id in self.grups:
                    in_num = 1
                inds[ind].append((id, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat, ind, in_num, in_excl, 0, 0))
        cols = """(id_cip_sec int, up varchar(5),
                uba varchar(5), upinf varchar(5),
                ubainf varchar(5), edat int,
                sexe varchar(1), seccio_censal varchar(10),
                up_residencia varchar(13), institucionalitzat int,
                maca int, ates int, trasplantat int,
                ind_codi varchar(10), num int, excl int,
                ci int, clin int)"""
        
        for ind in inds:
            table = ind + "_ind2"
            u.createTable(table, cols, 'eqa_ind', rm=True)
            u.listToTable(inds[ind], table, 'eqa_ind')
            

    def get_num(self):
        sql = """
            SELECT id_cip_sec, edat, pn13, pn20, pn23, pn23_1
            FROM mst_vacunes
            WHERE edat >= 15
        """
        self.grups = set()
        for id, edat, pn13, pn20, pn23, pn23_65 in u.getAll(sql,'nodrizas'):
            if pn20 == 1:
                self.grups.add(id)
            elif pn23 == 1 and edat >= 15 and edat <= 17:
                self.grups.add(id)
            elif pn13 == 1 and pn23 == 1 and edat < 65:
                self.grups.add(id)
            elif pn13 == 1 and pn23 == 1 and pn23_65 == 1:
                self.grups.add(id)

    def get_den(self):

        sql = """
            SELECT id_cip_sec, ps
            FROM eqa_problemes
            WHERE ps in (18, 62, 487)
            AND dde <= DATE '{DEXTD}'
        """.format(DEXTD=self.dext)
        self.dens = c.defaultdict(set)
        probs = {
            18: 'DM2',
            62: 'POCS',
            487: 'EXCL'
        }
        for id, ps in u.getAll(sql, 'nodrizas'):
            self.dens[probs[ps]].add(id)

        sql = """
            SELECT id_cip_sec, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat
            FROM mst_vacunes
            WHERE edat >= 15
        """
        self.pob = c.defaultdict()
        for id, up, uba, upinf, ubainf, edat, sexe, seccio, resi, inst, maca, ates, trasp in u.getAll(sql, 'nodrizas'):
            self.pob[id] = {
                'up':up,
                'uba':uba,
                'upinf':upinf,
                'ubainf':ubainf,
                'edat':edat,
                'sexe': sexe,
                'seccio': seccio,
                'resi': resi,
                'inst': inst,
                'maca': maca,
                'ates': ates,
                'trasplantat': trasp
            }
        

class IndicadorsVacunes():
    def __init__(self):
        self.get_dext()
        u.printTime('get_dext')
        self.get_den()
        u.printTime('get_den')
        self.get_immunitzacions()
        u.printTime('get_immunitzacions')
        self.get_num()
        u.printTime('get_num')
        self.get_indicadors()
        u.printTime('get_indicadors')

    def get_immunitzacions(self):
        self.id_vrc = set()
        self.id_xrp = set()
        self.id_rub = set()
        self.id_par = set()
        sql = """
            SELECT id_cip_sec, agrupador
            FROM eqa_immunitzats
            WHERE agrupador in (467, 516, 517, 518)
        """
        for id, agr in u.getAll(sql, 'nodrizas'):
            if agr == 467:
                self.id_vrc.add(id)
            elif agr == 516:
                self.id_xrp.add(id)
            elif agr == 517:
                self.id_rub.add(id)
            else:
                self.id_par.add(id)

    def get_dext(self):
        sql = """
                SELECT
                    data_ext
                FROM
                    dextraccio
             """
        self.dext, = u.getOne(sql, "nodrizas")
        self.periode = "A{}".format(self.dext.strftime("%y%m"))

    def get_indicadors(self):
        indicadors = {
            'EQA0508A': {
                'NUM': self.grups['vph'] & self.grups['polio'] & self.grups['vhb'] & self.grups['vha'] & self.grups['td'] & self.grups['macwy'] and self.grups['xrp'] & self.grups['v'],
                'DEN': self.pob['15-18'],
                'excl': self.grups['emb'] | self.grups['m_hz'],
                'vacs': ['vph', 'polio', 'vhb', 'vha', 'td', 'macwy', 'xrp', 'v']
            },
            'EQA0509A': {
                'NUM': self.grups['vph'] & self.grups['td'] & self.grups['xrp'],
                'DEN': self.pob['19-64'],
                'excl': self.grups['emb'],
                'vacs': ['vph', 'td', 'xrp']
            },
            'EQA0510A': {
                'NUM': self.grups['pn'] & self.grups['td'] & self.grups['hz'],
                'DEN': self.pob['>65'],
                'excl':  self.grups['m_hz'] | self.grups['excl_transpl'],
                'vacs': ['pn', 'td', 'hz']
            },
            'EQA0511A': {
                'NUM': self.grups['pn'],
                'DEN': self.pob['>65'],
                'excl': self.grups['excl_transpl'],
                'vacs': ['pn']
            },
            'EQA0512A': {
                'NUM': self.grups['hz_vac'],
                'DEN': self.pob['cohorts'],
                'excl':  self.grups['m_hz'] | self.grups['excl_transpl'],
                'vacs': ['hz']
            },
            'EQA0513A': {
                'NUM': self.grups['td'],
                'DEN': self.pob['>65'],
                'excl': self.grups['excl_transpl'],
                'vacs': ['td']
            }
        }
        inds = c.defaultdict(list)
        for ind, params in indicadors.items():
            num = params['NUM']
            den = params['DEN']
            excl = params['excl'] if 'excl' in params else None
            missings = params['vacs']
            for id in den:
                up = den[id]['up']
                uba = den[id]['uba']
                upinf = den[id]['upinf']
                ubainf = den[id]['ubainf']
                edat = den[id]['edat']
                sexe = den[id]['sexe']
                seccio_censal = den[id]['seccio']
                up_residencia = den[id]['resi']
                institucionalitzat = den[id]['inst']
                maca = den[id]['maca']
                ates = den[id]['ates']
                trasplantat = den[id]['trasplantat']
                in_num = 0
                in_excl = 0
                txt = []
                if excl and id in excl:
                    in_excl = 1
                elif id in num:
                    in_num = 1
                else:
                    for vac in missings:
                        if id not in self.grups[vac]:
                            txt.append(vac)
                txt = ','.join(txt)
                inds[ind].append((id, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat, ind, in_num, in_excl, 0, 0, txt))
        cols = """(id_cip_sec int, up varchar(5),
                uba varchar(5), upinf varchar(5),
                ubainf varchar(5), edat int,
                sexe varchar(1), seccio_censal varchar(10),
                up_residencia varchar(13), institucionalitzat int,
                maca int, ates int, trasplantat int,
                ind_codi varchar(10), num int, excl int,
                ci int, clin int, text varchar(255))"""
        
        for ind in inds:
            table = ind + "_ind2"
            u.createTable(table, cols, 'eqa_ind', rm=True)
            u.listToTable(inds[ind], table, 'eqa_ind')

    def get_num(self):
        sql = """
            SELECT id_cip_sec, vph, polio, vhb, vha, td, macwy, xarampio, rubeola, parotiditis, vrc, pn20, pn23_2, hz, hz_vac, m_hz, emb, excl_xrp_vrc
            FROM mst_vacunes
        """
        self.grups = c.defaultdict(set)
        for id, vph, polio, vhb, vha, td, macwy, xrp, rub, par, vrc, pn20, pn23_2, hz, hz_vac, m_hz, emb, excl_xrp_vrc in u.getAll(sql,'nodrizas'):
            if vph == 1:
                self.grups['vph'].add(id)
            if polio == 1:
                self.grups['polio'].add(id)
            if vhb == 1:
                self.grups['vhb'].add(id)
            if vha == 1:
                self.grups['vha'].add(id)
            if td == 1:
                self.grups['td'].add(id)
            if macwy == 1:
                self.grups['macwy'].add(id)
            if (xrp == 1 or id in self.id_xrp) and excl_xrp_vrc == 0 and (rub == 1 or id in self.id_rub) and (par == 1 or id in self.id_par):
                self.grups['xrp'].add(id)
            if (excl_xrp_vrc == 0 and (id in self.id_vrc or m_hz == 1)) or vrc == 1:
                self.grups['v'].add(id)
            if excl_xrp_vrc == 1:
                self.grups['excl_transpl'].add(id)
            if emb == 1:
                self.grups['emb'].add(id)
            if hz == 1:
                self.grups['hz'].add(id)
            if hz_vac == 1:
                self.grups['hz_vac'].add(id)
            if m_hz == 1:
                self.grups['m_hz'].add(id)
            if pn23_2 == 1:
                self.grups['pn'].add(id)
    
    def get_den(self):
        sql = """
            SELECT id_cip_sec, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat
            FROM mst_vacunes
        """
        self.pob = c.defaultdict(dict)
        for id, up, uba, upinf, ubainf, edat, sexe, seccio, resi, inst, maca, ates, trasp in u.getAll(sql, 'nodrizas'):
            grup = ''
            if edat >= 15 and edat <= 18:
                grup = '15-18'
            elif edat >= 19 and edat <= 64:
                grup = '19-64'
            elif edat >= 65:
                grup = '>65'
            if grup:
                self.pob[grup][id] = {
                    'up':up,
                    'uba':uba,
                    'upinf':upinf,
                    'ubainf':ubainf,
                    'edat':edat,
                    'sexe': sexe,
                    'seccio': seccio,
                    'resi': resi,
                    'inst': inst,
                    'maca': maca,
                    'ates': ates,
                    'trasplantat': trasp
                }
        sql = """
            SELECT id_cip_sec, year(data_naix), edat
            FROM assignada_tot_with_jail
            WHERE edat >= 65
        """
        for id, dataany, edat in u.getAll(sql, 'nodrizas'):
            if id in self.pob['>65']:
                if dataany <= 1933 or (1934 <= dataany <= 1942 and edat >= 90) or (dataany >= 1942 and edat >= 80) or (dataany >= 1957 and edat >= 65):
                    self.pob['cohorts'][id] = self.pob['>65'][id]

class IndicadorsVacunesResis():
    def __init__(self):
        self.get_dext()
        u.printTime('get_dext')
        self.get_den()
        u.printTime('get_den')
        self.get_num()
        u.printTime('get_num')
        self.get_indicadors()
        u.printTime('get_indicadors')

    def get_dext(self):
        sql = """
                SELECT
                    data_ext
                FROM
                    dextraccio
             """
        self.dext, = u.getOne(sql, "nodrizas")
        self.periode = "A{}".format(self.dext.strftime("%y%m"))

    def get_indicadors(self):
        indicadors = {
            # 'EQA0505': self.grups['ag'],
            'EQA0505A': self.grups['td'],
            'EQA0505B': self.grups['pn'],
            'EQA0505C': self.grups['hz'],
            'EQA0506A': self.grups['grip'],
            'EQA0507A': self.grups['covid']
        }
        inds = c.defaultdict(list)
        for ind, num in indicadors.items():
            for id in self.pob:
                up = self.pob[id]['up']
                uba = self.pob[id]['uba']
                upinf = self.pob[id]['upinf']
                ubainf = self.pob[id]['ubainf']
                edat = self.pob[id]['edat']
                sexe = self.pob[id]['sexe']
                seccio_censal = self.pob[id]['seccio']
                up_residencia = self.pob[id]['resi']
                institucionalitzat = self.pob[id]['inst']
                maca = self.pob[id]['maca']
                ates = self.pob[id]['ates']
                trasplantat = self.pob[id]['trasplantat']
                in_num = 0
                if id in num:
                    in_num = 1
                inds[ind].append((id, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat, ind, in_num, 0, 0, 0))
        
        cols = """(id_cip_sec int, up varchar(5),
                uba varchar(5), upinf varchar(5),
                ubainf varchar(5), edat int,
                sexe varchar(1), seccio_censal varchar(10),
                up_residencia varchar(13), institucionalitzat int,
                maca int, ates int, trasplantat int,
                ind_codi varchar(10), num int, excl int,
                ci int, clin int)"""
        
        for ind in inds:
            table = ind + "_ind2"
            u.createTable(table, cols, 'eqa_ind', rm=True)
            u.listToTable(inds[ind], table, 'eqa_ind')

    def get_num(self):
        sql = """
            SELECT id_cip_sec, td, pn20, pn13, pn23_1,pn23_2,hz, m_hz,grip,covid
            FROM mst_vacunes
        """
        self.grups = c.defaultdict(set)
        for id, td, pn20, pn13, pn23_1,pn23_2,hz,m_hz,grip,covid in u.getAll(sql,'nodrizas'):
            if td == 1:
                self.grups['td'].add(id)
            if pn20 == 1:
                self.grups['pn'].add(id)
            if pn13 == 1 and pn23_1 == 1:
                self.grups['pn'].add(id)
            if hz == 1:
                self.grups['hz'].add(id)
            if grip == 1:
                self.grups['grip'].add(id)
            if covid == 1:
                self.grups['covid'].add(id)
        self.grups['ag'] = self.grups['td'].union(self.grups['pn'], self.grups['hz'])
        print(str(len(self.grups['td'])))
    def get_den(self):
        sql = """
            SELECT id_cip_sec, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat
            FROM mst_vacunes
            WHERE institucionalitzat = 1
        """
        self.pob = c.defaultdict()
        for id, up, uba, upinf, ubainf, edat, sexe, seccio, resi, inst, maca, ates, trasp in u.getAll(sql, 'nodrizas'):
            self.pob[id] = {
                'up':up,
                'uba':uba,
                'upinf':upinf,
                'ubainf':ubainf,
                'edat':edat,
                'sexe': sexe,
                'seccio': seccio,
                'resi': resi,
                'inst': inst,
                'maca': maca,
                'ates': ates,
                'trasplantat': trasp
            }

if __name__ == '__main__':
    try:
        IndicadorsVacunes()
        IndicadorsVacunesResis()
        EQA0248_0249()
    except Exception as e:
        raise
