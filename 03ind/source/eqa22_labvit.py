import sisaptools as u


INDICADOR = "EQA0316A"


sql = "select id, inadequats \
       from mst_pacients where indicador  ='LABVIT'"
num = {row[0]: row[1] for row
       in u.Database("p2262", "laboratori").get_all(sql)}

sql = "select id_cip_sec, up, uba, upinf, ubainf, edat, sexe, seccio_censal, \
              up_residencia, institucionalitzat, maca, ates, trasplantat \
       from assignada_tot"
table = INDICADOR.lower() + "_ind2"
with u.Database("p2262", "eqa_ind") as eqa:
    upload = [row + (INDICADOR, num.get(row[0], 0), 0, 0, 0)
              for row in eqa.get_all(sql)]
    eqa.execute("create or replace table {} like eqa0201a_ind2".format(table))
    eqa.list_to_table(upload, table)
