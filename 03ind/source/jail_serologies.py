# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict, Counter
import csv
import os
import sys
from datetime import datetime

printTime('Inici')

OnlyJail = True

db = 'eqa_ind'
nod = 'nodrizas'

eqa_serologies = {'EQA2001A': {
                                'calcular': True,
                                'serologies': "('007000','007032','D05485','S21085','S23085','S38485')",
                                'ps': '(101)',
                                'tip': 1,
                             },
                 'EQA2002A': {
                                'calcular': True,
                                'serologies': "('D01785','S10185')",
                                'ps': '(13, 14)',
                                'tip': 1,
                             },
                 'EQA2003A': {
                                'calcular': True,
                                'serologies': "('R23185','S10385', 'S10485')",
                                'ps': '(550, 553)',
                                'tip': 1,
                             },
                 'EQD2003A': {
                                'calcular': True,
                                'serologies': "('007000','007032','D05485','S21085','S23085','S38485')",
                                'ps': '(101)',
                                'tip': 2,
                             },
                 'EQA2000A': {
                                'calcular': True,
                                'den': '(101)',
                                'criteris': [
                                                [('serologies', "('V03966', 'V04466', '006783')", 12, None)],
                                                [('serologies', "('I89272', 'I04672','I04400','I04672','I89072','I89250','I89272','I89372','I90072','I90272','I90372','I92072','I94872','I97272','IC082','IC149','IC151')", 12, None)]
                                            ],
                                'tip': 3,
                             },
                }

def get_dextraccio():
    dext = {}
    sql = 'select data_ext from dextraccio'
    for data, in getAll(sql, nod):
        dext = data
    return dext
    
def get_poblacio():
    assig, assig_12m = {}, {}
    sql = 'select id_cip_sec,up,uba,upinf,ubainf,edat,ates,trasplantat,sexe,seccio_censal,up_residencia,institucionalitzat,maca,last_visit from assignada_tot{}'.format('_with_jail where id_cip_sec<0' if OnlyJail else '')
    for id_cip_sec, up, uba, upinf, ubainf, edat, ates, trasplantat,sexe, seccio_censal, up_residencia, institucionalitzat, maca, ingres in getAll(sql, nod):
        assig[(id_cip_sec)] = {'up': up, 'uba': uba, 'upinf': upinf, 'ubainf': ubainf, 'edat': edat, 'ates': ates, 'trasplantat': trasplantat, 'sexe': sexe, 'seccio_censal': seccio_censal, 'up_residencia': up_residencia, 'institucionalitzat': institucionalitzat, 'maca': maca}
        if ingres and 0 <= monthsBetween(ingres, dext) <= 11:
            assig_12m[id_cip_sec] = assig[id_cip_sec]
    return assig, assig_12m
 
def get_problemes(agr):
    problemes = {}
    sql = 'select id_cip_sec, dde from eqa_problemes where ps in {0}{1}'.format(agr, ' and id_cip_sec<0' if OnlyJail else '')
    for id, dde in getAll(sql, nod):
        problemes[id] = dde
    return problemes
    
def get_serologies(agr, dext, binari=True):
    serologies = {}
    sql = "select id_cip_sec, dat, val from jail_serologies where cod in {0}{1}".format(agr, ' and id_cip_sec<0' if OnlyJail else '')
    for id, date, val in getAll(sql, nod):
        dat = datetime.strptime(date, '%Y%m%d')
        if val in (0, 1) or not binari:
            fa = monthsBetween(dat, dext)
            if 0 <= fa <= 11:
                serologies[id] = True
    return serologies 
    
def get_serologiesPosit(agr, dext):
    serologiesP = {}
    sql = "select id_cip_sec, dat, val from jail_serologies where cod in {0}{1}".format(agr, ' and id_cip_sec<0' if OnlyJail else '')
    for id, date, val in getAll(sql, nod):
        dat = datetime.strptime(date, '%Y%m%d')
        if val == 1:
            serologiesP[id] = True
    return serologiesP 
 
def get_EQAserol(serologies, ps, pob, dext):
    EQAserol = {}
    serol = get_serologies(serologies, dext)
    hepat = get_problemes(ps)
    for id, rows in pob.items():
        den = 1
        num = 0
        excl = 0
        if id in serol:
            num = 1
        if id in hepat:
            num = 1
        EQAserol[id] = {'num': num, 'den': den, 'excl': excl}
    return EQAserol

def get_EQDserol(serologies, ps, pob, dext):
    EQDserol = {}
    serol = get_serologiesPosit(serologies, dext)
    hepat = get_problemes(ps)
    for id, rows in pob.items():
        den = 0
        if id in serol:
            den = 1
            excl = 0
            num = 0
        if id in hepat:
            num = 1
        if den == 1:
            EQDserol[id] = {'num': num, 'den': den, 'excl': excl}
    return EQDserol
    
def get_variables(codis, temps, valors):
    sql_temps = 'and data_var between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext' if temps else ''
    sql_valors = 'and valor {}'.format(valors) if valors else ''
    sql = 'select id_cip_sec from eqa_variables, dextraccio where id_cip_sec < 0 and agrupador in {} {} {}'.format(codis, sql_temps, sql_valors)
    pacients = [id for id, in getAll(sql, nod)]
    return pacients
    
dext = get_dextraccio()
pob, pob_12m = get_poblacio()

for indicador in eqa_serologies:
    upload = []
    create = "(id_cip_sec int, up varchar(5) not null default'', uba varchar(7) not null default'', upinf varchar(5) not null default'', ubainf varchar(7) not null default'',\
        edat double, ates double, trasplantat double, sexe varchar(7) not null default'', seccio_censal varchar(10) not null default'', up_residencia varchar(13) not null default'', institucionalitzat double, \
        maca double, ind_codi varchar(10) not null default'', num double, excl double, ci double, clin double, index(id_cip_sec))"
    serologies = eqa_serologies[indicador]['serologies'] if 'serologies' in eqa_serologies[indicador] else None
    ps = eqa_serologies[indicador]['ps'] if 'ps' in eqa_serologies[indicador] else None
    calcular = eqa_serologies[indicador]['calcular']
    tip = eqa_serologies[indicador]['tip']
    if calcular:
        table = indicador + '_ind2'
        if tip == 1:
            resultats = get_EQAserol(serologies, ps, pob, dext)
        elif tip == 2:
            resultats = get_EQDserol(serologies, ps, pob, dext)
        elif tip == 3:
            denominador = set(pob_12m) & set(get_problemes(eqa_serologies[indicador]['den']))
            numerador = set([id for id in denominador])
            for criteri in eqa_serologies[indicador]['criteris']:
                pacients = set()
                for taula, codis, temps, valors in criteri:
                    if taula == 'serologies':
                        aquests = set(get_serologies(codis, dext, binari=valors))
                    elif taula == 'problemes':
                        aquests = set(get_problemes(codis))
                    elif taula == 'variables':
                        aquests = set(get_variables(codis, temps, valors))
                    pacients |= aquests
                numerador &= pacients
            resultats = {id: {'num': 1 * (id in numerador), 'den': 1, 'excl': 0} for id in denominador}
        for id, rows in resultats.items():
            upload.append([id, pob[id]['up'], pob[id]['uba'], pob[id]['upinf'], pob[id]['ubainf'], pob[id]['edat'], pob[id]['ates'], pob[id]['trasplantat'],pob[id]['sexe'], pob[id]['seccio_censal'], pob[id]['up_residencia'], pob[id]['institucionalitzat'], pob[id]['maca'], indicador, rows['num'], rows['excl'], 0, 0])  
        createTable(table,create,db, rm=True)
        listToTable(upload, table, db)
    