# -*- coding: utf8 -*-


import collections as c

import sisapUtils as u


debug = False

class eqa_pacient(object):
    """."""

    def __init__(self):
        """."""
        
        self.data_ext()
        self.get_id_to_hash()
        self.get_eqa()
        self.export()
        
        
    
    def data_ext(self):
        """."""
        u.printTime("data_ext")
        self.periode, = u.getOne("select extract(year_month from data_ext) from dextraccio", 'nodrizas')
    
    
    def get_id_to_hash(self):
        """."""
        u.printTime("Hashos")
        self.hashos = []
        sql = "select id_cip_sec, hash_d, codi_sector from u11 {}".format("limit 100" if debug else '')
        for id, hash, sector in u.getAll(sql, "import"):
            self.hashos.append([self.periode,id, hash, sector])


    def get_eqa(self):
        """agafem indicadors adults eqa"""
        u.printTime("indicadors eqa")
        self.upload = []
        sql="select id_cip_sec,ind_codi,grup_codi,up,uba,upinf,ubainf,ates,trasplantat,excl_edat,institucionalitzat,maca,num,excl,ci,clin,llistat from eqa_ind.mst_indicadors_pacient {}".format("limit 100" if debug else '')
        for id_cip_sec,ind_codi,grup_codi,up,uba,upinf,ubainf,ates,trasplantat,excl_edat,institucionalitzat,maca,num,excl,ci,clin,llistat in u.getAll(sql, "eqa_ind"):
            self.upload.append([self.periode,id_cip_sec, ind_codi,grup_codi,up,uba,upinf,ubainf,ates,trasplantat,excl_edat,institucionalitzat,maca,num,excl,ci,clin,llistat])
        
    def export(self):
        """."""
        u.printTime("export")
        tb = "planificat_eqa"
        db = 'exadata2'
        cols = ("periode varchar2(10)", "id_cip_sec int","ind_codi varchar2(10)",
                "grup_codi varchar2(10)", "up varchar2(5)", "uba varchar2(5)",
                "upinf varchar2(5)", "ubainf varchar2(5)", "ates int", "trasplantat int",
                "excl_edat int",  "institucionalitzat int",
                "maca int", "num int", "excl int", "ci int",
                "clin int", "llistat int")
        #u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True) 
        sql = "delete from {0} a where periode = '{1}'".format(tb, self.periode)
        u.execute(sql, db)        
        u.listToTable(self.upload, tb, db)

        tb11 = "planificat_u11"
        cols = ("periode varchar2(10)", "id_cip_sec int", "hash varchar2(40)", "sector varchar2(5)")
        u.createTable(tb11, "({})".format(", ".join(cols)), db, rm=True) 
        sql = "delete from {0} a where periode = {1}".format(tb11, self.periode)
        #u.execute(sql, db)        
        u.listToTable(self.hashos, tb11, db)         

        users= ['DWSISAP_ROL', ]
        for user in users:
            u.execute("grant select on {} to {}".format(tb,user),db) 
            u.execute("grant select on {} to {}".format(tb11,user),db) 
        
if __name__ == '__main__':
    u.printTime("Inici")
     
    eqa_pacient()
    
    u.printTime("Final")                 
