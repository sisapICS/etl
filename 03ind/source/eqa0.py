# coding: latin1

import collections as c
import csv
import math
import multiprocessing as m
import os

import sisaptools as u


DEBUG = False
FMO = False


class Main(object):
    """."""

    def get_criteris(self):
        """."""
        sql = "update eqa_ind set baixa_ = 0 \
               where ind_codi in (select ind_codi_o from eqa_conjunts)"
        u.Database("p2262", "eqa_ind").execute(sql)
        sql = "select taula, agrupador, \
                      date_add(date_add(data_ext, interval - data_min month), \
                               interval +1 day), \
                      date_add(data_ext, interval - data_max month), \
                      valor_min, valor_max, var_ult, var_num, ind_codi, \
                      tipus, numero_criteri, sense \
               from eqa_relacio, nodrizas.dextraccio \
               where baixa = 0 and \
                     ind_codi in \
                        (select ind_codi from eqa_ind \
                         where tipus in (0, 14, 26) and baixa_ = 0)"
        self.criteris = c.defaultdict(set)
        for row in u.Database("p2262", "eqa_ind").get_all(sql):
            self.criteris[row[:2]].add(row[2:])

    def get_avisos(self):
        """."""
        self.avisos = set()
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        file = "../../08alt/dades_noesb/avisos_indicadors.txt"
        for row in csv.reader(open(file), delimiter="@"):
            if row[-1]:
                self.avisos.add(int(row[-1]))

    def create_structure(self):
        """."""
        sql = "select ind_codi from eqa_ind \
               where tipus in (0, 14, 26) and baixa_ = 0"
        cols = ("id_cip_sec int", "up varchar(5)", "uba varchar(5)",
                "upinf varchar(5)", "ubainf varchar(5)", "edat int",
                "sexe varchar(1)", "seccio_censal varchar(10)",
                "up_residencia varchar(13)", "institucionalitzat int",
                "maca int", "ates int", "trasplantat int",
                "ind_codi varchar(10)", "num int", "excl int", "ci int",
                "clin int")
        with u.Database("p2262", "eqa_ind") as eqa:
            codis = [cod for cod, in eqa.get_all(sql)]
            for cod in codis:
                taula = "{}_ind2{}".format(cod, "_fmo" if FMO else "")
                eqa.create_table(taula, cols, remove=True,
                                 partition_type="hash",
                                 partition_id="id_cip_sec", partitions=16)
        cols = ("id_cip_sec int", "agrupador int", "data date", "es_data int",
                "necessita_valor int", "valor double", "es_valor int")
        u.Database("p2262", "eqa_ind").create_table("mst_avisos_pre", cols,
                                                    remove=True,
                                                    partition_type="hash",
                                                    partition_id="id_cip_sec",
                                                    partitions=16)

    def get_eqa(self):
        """."""
        if DEBUG:
            Particio("p3", self.criteris, self.avisos)
        else:
            pool = m.Pool(16)
            results = []
            for i in range(16):
                particio = "p{}".format(i)
                args = (particio, self.criteris, self.avisos)
                results.append(pool.apply_async(Particio, args=args))
            pool.close()
            pool.join()
            for result in results:
                try:
                    result.get()
                except Exception:
                    raise

    def get_conjunts(self):
        """."""
        dades = c.defaultdict(dict)
        sql = "select ind_codi_o, ind_codi from eqa_conjunts"
        for ori, des in u.Database("p2262", "eqa_ind").get_all(sql):
            ori_tb = ori + "_ind2"
            if FMO:
                ori_tb += "_fmo"
            des_tb = des + "_ind2"
            if FMO:
                des_tb += "_fmo"
            sql = "select * from {}".format(ori_tb)
            for row in u.Database("p2262", "eqa_ind").get_all(sql):
                id = row[0]
                if id not in dades[des_tb]:
                    dades[des_tb][id] = list(row)
                else:
                    for i in range(-4, 0):
                        if row[i] and not dades[des_tb][id][i]:
                            dades[des_tb][id][i] = row[i]
        for table, files in dades.items():
            upload = list(files.values())
            with u.Database("p2262", "eqa_ind") as eqa:
                eqa.execute("truncate table {}".format(table))
                eqa.list_to_table(upload, table)

        sql = "update eqa_ind set baixa_ = 1 \
               where ind_codi in (select ind_codi_o \
                                  from eqa_conjunts \
                                  where ind_codi_o <> ind_codi)"
        u.Database("p2262", "eqa_ind").execute(sql)

    def get_control(self):
        """."""
        upload = []
        sql = "select ind_codi from eqa_ind \
               where tipus in (0, 14, 26) and baixa_ = 0"
        for ind, in u.Database("p2262", "eqa_ind").get_all(sql):
            for sufix in ("", "_fmo"):
                taula = "{}_ind2{}".format(ind, sufix)
                this = "select count(1), sum(num), sum(excl), sum(ci), \
                               sum(clin) \
                        from {}".format(taula)
                nums = u.Database("p2262", "eqa_ind").get_one(this)
                if not nums:
                    nums = (0,) * 5
                that = (ind, sufix) + nums
                upload.append(that)
        cols = ("ind varchar(10)", "suffix varchar(5)", "cnt int", "num int",
                "excl int", "ci int", "clin int")
        with u.Database("p2262", "eqa_ind") as eqa:
            eqa.create_table("eqa_fmo", cols, remove=True)
            eqa.list_to_table(upload, "eqa_fmo")


class Particio(object):
    """."""

    def __init__(self, particio, criteris, avisos):
        """."""
        self.particio = particio
        self.criteris = criteris
        self.avisos = avisos
        self.get_data()
        self.get_indicadors()
        self.upload_data()
        self.dades = []
        self.upload = []

    def get_data(self):
        """."""
        taules = (
            ["problemes", "prt_problemes", ("ps", "dde", "null", "pr_gra", "null"), "problemes"],  # noqa
            ["prescripcions", "prt_tractaments", ("farmac", "pres_orig", "data_fi", "null", "null"), "tractaments"],  # noqa
            ["variables", "prt_variables", ("agrupador", "data_var", "null", "valor", "usar"), "variables"],  # noqa
            ["vacunes", "prt_vacunes", ("agrupador", "datamax", "null", "dosis", "null"), "vacunes"],  # noqa
            ["inatural", "prt_immunitzats", ("agrupador", "null", "null", "null", "null"), "raw"],  # noqa
            ["proves", "prt_proves", ("agrupador", "data", "null", "null", "null"), "problemes"],  # noqa
            ["ram", "prt_ram", ("agr", "null", "null", "null", "null"), "raw"],  # noqa
            ["QAC", "prt_microalb", ("agrupador", "null", "null", "null", "null"), "raw"],  # noqa
            ["familiars", "prt_familiars", ("agrupador", "null", "null", "edat", "null"), "raw_val"],  # noqa
            ["edat", "eqa_ind.assignada_tot", ("8", "null", "null", "edat", "null"), "poblacio"],  # noqa
            ["sexe", "eqa_ind.assignada_tot", ("35", "null", "null", "if(sexe='H', 0, if(sexe='D', 1, 9999))", "null"), "poblacio"],  # noqa
            ["instit", "eqa_ind.assignada_tot", ("523", "null", "null", "institucionalitzat", "null"), "poblacio"]  # noqa
        )
        self.dades = c.defaultdict(set)
        self.dades_avisos = {}
        variables = c.Counter()
        for key, origen, columnes, metode in taules:
            if metode == "poblacio":
                key = "assignacio"
            sql = "select id_cip_sec, {} from {} partition({})".format(", ".join(columnes), origen, self.particio)  # noqa
            for id, cod, ini, fi, val, n in u.Database("p2262", "nodrizas").get_all(sql):  # noqa
                for d_min, d_max, v_min, v_max, v_ult, v_num, i_ind, i_tipus, i_num, i_sense in self.criteris[(key, cod)]:  # noqa
                    go = False
                    var = False
                    if metode == "raw":
                        go = True
                    elif metode == "problemes":
                        if d_min <= ini <= d_max:
                            if v_min is None or v_max is None or v_min <= val <= v_max:  # noqa
                                go = True
                    elif metode == "tractaments":
                        if ini <= d_max and fi >= d_min:
                            go = True
                    elif metode == "variables":
                        if d_min <= ini <= d_max:
                            if v_min is None or v_max is None or v_min <= val <= v_max:  # noqa
                                if not v_ult or n <= v_num:
                                    var = True
                        if cod in self.avisos and n == 1 and (id, cod) not in self.dades_avisos:  # noqa
                            es_dat = d_min <= ini <= d_max
                            need_val = cod != 46
                            val = val if isinstance(val, int) else round(val, 1)  # noqa
                            es_val = need_val and v_min <= val <= v_max
                            this = (ini, 1 * es_dat, 1 * need_val, val, 1 * es_val)  # noqa
                            self.dades_avisos[(id, cod)] = this
                    elif metode == "vacunes":
                        if d_min <= ini <= d_max and val >= v_num:
                            go = True
                    elif metode in ("poblacio", "raw_val"):
                        if v_min <= val <= v_max:
                            go = True
                    if go:
                        self.dades[id].add((i_ind, i_tipus, i_num))
                    elif var:
                        variables[(id, cod, v_num, i_ind, i_tipus, i_num)] += 1
        for (id, cod, v_num, i_ind, i_tipus, i_num), n in variables.items():
            if n >= v_num:
                self.dades[id].add((i_ind, i_tipus, i_num))

    def get_indicadors(self):
        """."""
        criteris_diff = c.defaultdict(set)
        sense = set()  # hauria de ser a nivell d'agrupador, no de criteri
        for values in self.criteris.values():
            for d_min, d_max, v_min, v_max, v_ult, v_num, i_ind, i_tipus, i_num, i_sense in values:  # noqa
                criteris_diff[(i_ind, i_tipus)].add(i_num)
                if i_sense:
                    sense.add((i_ind, i_tipus, i_num))
        sql = "select id_cip_sec, up, uba, upinf, ubainf, edat, sexe, \
                      seccio_censal, up_residencia, institucionalitzat, \
                      maca, ates, trasplantat \
               from assignada_tot partition({})".format(self.particio)
        self.upload = c.defaultdict(list)
        for row in u.Database("p2262", "eqa_ind").get_all(sql):
            id = row[0]
            dades = self.dades[id]
            for (i_ind, i_tipus), nums in criteris_diff.items():
                if i_tipus == "den":
                    entra = all([(i_ind, i_tipus, num) in dades
                                 for num in nums])
                    if entra:
                        this = list(row) + [i_ind]
                        for tipus in ("num", "excl", "ci", "clin"):
                            es = set()
                            for num in criteris_diff[(i_ind, tipus)]:
                                that = (i_ind, tipus, num)
                                if that not in sense:
                                    es.add(that in dades)
                                else:
                                    es.add(that not in dades)
                            this.append(1 * (all(es) if es else False))
                        self.upload[i_ind].append(this)

    def upload_data(self):
        """."""
        for key, rows in self.upload.items():
            taula = "{}_ind2{}".format(key, "_fmo" if FMO else "")
            u.Database("p2262", "eqa_ind").list_to_table(rows, taula, partition=self.particio)  # noqa
        upload = [k + v for (k, v) in self.dades_avisos.items()]
        u.Database("p2262", "eqa_ind").list_to_table(upload, "mst_avisos_pre",
                                                     partition=self.particio)


if __name__ == "__main__":
    main = Main()
    main.get_criteris()
    main.get_avisos()
    main.create_structure()
    main.get_eqa()
    main.get_conjunts()
    if FMO:
        main.get_control()
