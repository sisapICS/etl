# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u
import sisaptools as t
from dateutil.relativedelta import relativedelta
import datetime as d

DEXT = t.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]
# DEXT = d.date(2023, 9, 30)
DEXTD=str(DEXT)
now24 = DEXT - relativedelta(years=2)


class Indicadors():
    def __init__(self):
        self.indicadors = c.defaultdict(set)
        self.get_pob()
        self.get_variables()
        self.get_tabac()
        self.get_problemes()
        self.get_resultat()

    def get_resultat(self):
        # self.pob[id] = (up, uba, edat, ubainf, sector)
        llistats = c.defaultdict(list)
        for id, (up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat) in self.pob.items():
            for ind in self.indicadors:
                num = 0
                if id in self.indicadors[ind]:
                    num = 1
                llistats[ind].append((id, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat, ind + 'A', num, 0, 0, 0))
        cols = """(id_cip_sec double, up varchar(5), uba varchar(7), upinf varchar(5),
                ubainf varchar(7), edat double, sexe varchar(7), seccio_censal varchar(10),
                up_residencia varchar(13), institucionalitzat double, maca double,
                ates double, trasplantat double, ind_codi varchar(10), num double,
                excl double, ci double, clin double)"""
        u.createTable('eqa0307a_ind2', cols, 'eqa_ind', rm=True)
        u.createTable('eqa0317a_ind2', cols, 'eqa_ind', rm=True)
        u.listToTable(llistats['EQA0307'], 'eqa0307a_ind2', 'eqa_ind')
        u.listToTable(llistats['EQA0317'], 'eqa0317a_ind2', 'eqa_ind')

    def get_problemes(self):
        """."""
        sql = "select id_cip_sec, ps \
               from eqa_problemes \
               where ps = 84"
        for id, ps in u.getAll(sql, "nodrizas"):
            if ps == 84:
                self.indicadors["EQA0317"].add(id)

    def get_variables(self):
        """."""
        sql = "select id_cip_sec, agrupador \
               from eqa_variables \
               where agrupador = 84 and \
                    usar = 1 and \
                    data_var between DATE '{now24}' and \
                    DATE '{DEXTD}'".format(DEXTD=DEXTD, now24=now24)
        for id, agr in u.getAll(sql, "nodrizas"):
            self.indicadors['EQA0317'].add(id)

    def get_tabac(self):
        """."""
        # EQA0307 Darrers 24 mesos, si t� ? 25 anys o consta que en algun moment hagi estat fumador
        # Nom�s 1 sol registre, si t� >25 anys i tots els registres indiquen que mai ha sigut fumador.
        fumador_ex_fumador = set()
        no_fum = set()
        sql = """select id_cip_sec, tab
                    from nodrizas.eqa_tabac"""
        for id, tab in u.getAll(sql, 'nodrizas'):
            if tab in (1,2):
                fumador_ex_fumador.add(id)
            else:
                no_fum.add(id)
        for id in no_fum:
            if id in self.pob:
                edat = self.pob[id][4]
                if edat > 25 and id not in fumador_ex_fumador:
                    self.indicadors["EQA0307"].add(id)
        sql = "select id_cip_sec, tab \
               from eqa_tabac \
               where last = 1 and \
                     dlast between DATE '{now24}' and \
                     DATE '{DEXTD}'".format(now24=now24, DEXTD=DEXTD)
        for id, tab in u.getAll(sql, "nodrizas"):
            if id in self.pob:
                edat = self.pob[id][4]
                if edat <= 25 or id in fumador_ex_fumador:
                    self.indicadors["EQA0307"].add(id)

    def get_pob(self):
        self.pob = c.defaultdict(list)
        sql = """
            select id_cip_sec, up, uba, upinf, ubainf, edat,
            sexe, seccio_censal, up_residencia, institucionalitzat,
            maca, ates, trasplantat
            from assignada_tot_with_jail
            where edat between 15 and 60
            and ates = 1"""
        for id, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat in u.getAll(sql, 'nodrizas'):
            self.pob[id] = [up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat]

if __name__ == "__main__":
    Indicadors()