# coding: latin1

"""
.
"""

import collections as c
import csv
import math
import multiprocessing as m
import os

import sisaptools as u


ES_BASAL = False  # activar per recalcular prevalences

CONNEXIO = ("p2262", "eqa_ind")
MASTER = "mst_indicadors_pacient"
LV_UP = "exp_khalix_up_ind"
LV_POB = "exp_khalix_up_pob"
LV_DET = "exp_khalix_up_det"
ECAP_ENTREN = "mst_ubas"
ECAP_UBA = "exp_ecap_uba"
LV_UBA = "exp_khalix_uba_ind"
LV_EQD = "exp_khalix_uba_indEQD"
U11 = "mst_u11"
ECAP_PACIENT = "exp_ecap_pacient"
ECAP_CATALEG = "exp_ecap_cataleg"

DETECCIO = {"EQD0913": ("MCV", ("EQD023802", "EQD0204A", "EQA0204B", "EQA0204C", "EQA0201A")),  # noqa
            "EQD0914": ("FA", ("EQA0201A",)),
            "EQD0915": ("DM2", ("EQA0209A",)),
            "EQD0901": ("HTA", ("EQA0213A", "EQA0213B")),
            "EQD0916": ("MPOC", ("EQD024206",)),
            "EQD0917": ("Asma", ("EQD024205",)),
            "EQD0902": ("Aguda ORL", ("EQA0226A", "EQA0226B", "EQA0231A")),
            "EQD0903": ("Aguda respirat�ria", ("EQA0232A",)),
            "EQD0904": ("Aguda digestiva", ("EQA0229A",)),
            "EQD0905": ("Aguda urin�ria", ("EQA0230A", "EQA0230B", "EQA0230C")),  # noqa
            "EQD0906": ("Altres", ("EQA0219A", "EQA0227A", "EQA0235A")),
            "EQD0907": ("Obesitat", ("EQA0303A",)),
            "EQD0908": ("ATDOM", ("EQA0401A",)),
            "EQD0909": ("IC", ("EQD023802",))}


class EQA(object):
    """."""

    def __init__(self):
        """."""
        # maca
        sql = "select withMaca_ind from nodrizas.dextraccio"
        self.with_maca = u.Database(*CONNEXIO).get_one(sql)[0]
        # grip
        sql = "select grip, grip_ind from nodrizas.dextraccio"
        self.grip_flag, self.grip_ind = u.Database(*CONNEXIO).get_one(sql)
        # detecci�
        self.deteccio_inv = c.defaultdict(list)
        self.deteccio_lit = []
        for nou, (literal, antics) in DETECCIO.items():
            for antic in antics:
                self.deteccio_inv[antic].append(nou)
            self.deteccio_lit.append((nou, literal))
        this = os.path.dirname(os.path.realpath(__file__))
        that = "/prevalences.txt"
        self.deteccio_file = this.replace("source", "dades_noesb") + that

    def _get_master(self, sql):
        """."""
        print(sql)
        u.Database(*CONNEXIO).execute(sql)

    def get_master(self):
        """."""
        cols = ("id_cip_sec int", "ind_codi varchar(10)",
                "grup_codi varchar(10)", "up varchar(5)", "uba varchar(5)",
                "upinf varchar(5)", "ubainf varchar(5)", "edat int",
                "sexe varchar(1)", "ates int", "trasplantat int",
                "excl_edat int", "seccio_censal varchar(20)",
                "up_residencia varchar(13)", "institucionalitzat int",
                "maca int", "num int", "den int", "excl int", "ci int",
                "clin int", "llistat int", "valor varchar(255)", "text varchar(255)")
        sql = "select distinct ind_codi from eqa_ind where baixa_ = 0"
        partitions = ["partition {0} values in ('{0}')".format(ind)
                      for ind, in u.Database(*CONNEXIO).get_all(sql)]
        u.Database(*CONNEXIO).create_table(MASTER, cols, remove=True,
                                           partition_type="list columns",
                                           partition_id="ind_codi",
                                           partitions=partitions)
        base = "insert into {} partition ({}) \
                select id_cip_sec, '{}', '{}', up, uba, upinf, ubainf, edat, \
                       sexe, ates, trasplantat, \
                       if(edat between {} and {}, 0, 1), seccio_censal, \
                       up_residencia, institucionalitzat, maca, \
                       {}, {}, {}, ci, clin, {}, '', {} \
                from {}"
        sql = "select distinct a.ind_codi, grup_codi, grup2_codi, llistats, \
                      invers, minim, maxim, tipus, girar \
               from eqa_ind a \
               inner join eqa_edats b on a.ind_codi = b.ind_codi \
               where baixa_ = 0"
        jobs = []
        for ind, grp, grp2, te_llis, inv, emin, emax, tip, girar in u.Database(*CONNEXIO).get_all(sql):  # noqa
            es_aguda = tip == 3 or ind == 'EQA0316A'
            num = "1 - num" if girar else "num"
            den = "den" if (es_aguda and ind != 'EQA0316A') else 1
            exclos = "excl"
            txt = 'text' if grp in ('EQA0508', 'EQA0509', 'EQA0510') else "'' as txt"
            if grp2 not in ("EQAG08", "EQAG09", "EQAG10", "EQAG11", "EQAG19"):
                if "EQD" not in ind:
                    exclos = "greatest(excl, trasplantat)"
            if te_llis:
                if es_aguda:
                    llistat = "if(num{}, 1, 0)".format(" > 0" if inv else " < den")  # noqa
                else:
                    llistat = "if({} = {}, 1, 0)".format(num, inv)
            else:
                llistat = "0"
            taula = ind + "_ind2"
            this = base.format(MASTER, ind, ind, grp, int(emin), int(emax),
                               num, den, exclos, llistat, txt, taula)
            jobs.append(this)
        pool = m.Pool(8)
        pool.map(self._get_master, jobs, chunksize=1)
        pool.close()

    def _get_lv_up(self, sql):
        """."""
        dades = c.Counter()
        with u.Database(*CONNEXIO) as conn:
            for up, edat, sexe, ates, instit, maca, ind, num, den, ind_maca in conn.get_all(sql):  # noqa
                if maca and not instit and not ind_maca:
                    continue
                ins = "INS" if instit else "NOINS"
                if num:
                    dades[(up, edat, sexe, "{}ASS".format(ins), ind, "NUM")] += num  # noqa
                    if ates:
                        dades[(up, edat, sexe, "{}AT".format(ins), ind, "NUM")] += num  # noqa
                if den:
                    dades[(up, edat, sexe, "{}ASS".format(ins), ind, "DEN")] += den  # noqa
                    if ates:
                        dades[(up, edat, sexe, "{}AT".format(ins), ind, "DEN")] += den  # noqa
            upload = [k + (v,) for (k, v) in dades.items()]
            conn.list_to_table(upload, LV_UP)

    def get_lv_up(self):
        """."""
        cols = ("up varchar(5)", "edat varchar(10)", "sexe varchar(4)",
                "comb varchar(10)", "indicador varchar(10)",
                "conc varchar(10)", "n int")
        u.Database(*CONNEXIO).create_table(LV_UP, cols, remove=True)
        sql = "select up, khalix edat, if(sexe='H','HOME','DONA'), ates, \
                      institucionalitzat, maca, ind_codi, num, den, \
                      if(grup_codi in {}, 1, 0) \
                 from {} partition({}) a \
                 inner join nodrizas.khx_edats5a b on a.edat = b.edat \
                 where excl_edat = 0 and excl = 0 and ci = 0 and clin = 0"
        if not self.grip_flag:
            sql += " and left(ind_codi, 7) not in {}".format(self.grip_ind)
        parts = u.Database(*CONNEXIO).get_table_partitions(MASTER)
        jobs = []
        for k, v in sorted(parts.items(), key=lambda x: x[0], reverse=True):
            jobs.append(sql.format(self.with_maca, MASTER, k))
        pool = m.Pool(8)
        results = []
        for job in jobs:
            results.append(pool.apply_async(self._get_lv_up, args=(job,)))
        pool.close()
        pool.join()
        for result in results:
            a = result.get()  # noqa
        sql = "alter table {} add index up (up, edat, comb, sexe)"
        u.Database(*CONNEXIO).execute(sql.format(LV_UP))

    def get_lv_pob(self):
        """."""
        cols = ("up varchar(5)", "edat varchar(10)", "sexe varchar(4)",
                "comb varchar(10)", "n int")
        sql = "select up, khalix, if(sexe='H', 'HOME', 'DONA'), \
                      institucionalitzat, ates \
               from assignada_tot a \
               inner join nodrizas.khx_edats5a b on a.edat = b.edat \
               where institucionalitzat=1 or \
                     (institucionalitzat = 0 and maca = 0)"
        dades = c.Counter()
        with u.Database(*CONNEXIO) as conn:
            for up, edat, sexe, instit, ates in conn.get_all(sql):
                ins = "INS" if instit else "NOINS"
                dades[(up, edat, sexe, "{}ASS".format(ins))] += 1
                if ates:
                    dades[(up, edat, sexe, "{}AT".format(ins))] += 1
            upload = [k + (v,) for (k, v) in dades.items()]
            conn.create_table(LV_POB, cols, remove=True)
            conn.list_to_table(upload, LV_POB)
            sql = "alter table {} add index up (up, edat, comb, sexe)"
            conn.execute(sql.format(LV_POB))

    def get_prev_anual(self):
        """."""
        sql = """select nvl(b.medea, '2U'), a.edat, a.comb, a.sexe, sum(n)
                 from {} a
                 inner join nodrizas.cat_centres b \
                       on a.up = b.scs_codi
                 group by nvl(b.medea, '2U'), a.edat, \
                          a.comb, a.sexe""".format(LV_POB)
        poblacio = {row[:-1]: row[-1]
                    for row in u.Database(*CONNEXIO).get_all(sql)}
        ind = tuple(self.deteccio_inv.keys())
        sql = """select a.indicador, nvl(b.medea, '2U'), a.edat, a.comb,
                        a.sexe, a.n
                 from {} a
                 inner join nodrizas.cat_centres b on a.up = b.scs_codi
                 where indicador in {} and
                       conc = 'DEN'""".format(LV_UP, ind)
        dades = c.defaultdict(c.Counter)
        fets = set()
        for row in u.Database(*CONNEXIO).get_all(sql):
            for nou in self.deteccio_inv[row[0]]:
                key = (nou,) + row[1:-1]
                if key not in fets:
                    dades[key]["den"] += poblacio[key[1:]]
                    fets.add(key)
                dades[key]["num"] += row[-1]
        upload = [key + (vals["num"] / float(vals["den"]),)
                  for key, vals in dades.items()]
        u.TextFile(self.deteccio_file).write_iterable(upload, delimiter="{",
                                                      endline="\r\n")

    def get_lv_deteccio(self):
        """."""
        # detectats
        dades = c.Counter()
        ind = tuple(self.deteccio_inv.keys())
        sql = """select a.up, a.indicador, nvl(b.medea, '2U'), a.edat, a.comb,
                        a.sexe, a.n
                    from {} a
                    inner join nodrizas.cat_centres_with_jail b
                               on a.up = b.scs_codi
                    where indicador in {} and
                          conc = 'DEN'""".format(LV_UP, ind)
        for row in u.Database(*CONNEXIO).get_all(sql):
            for nou in self.deteccio_inv[row[1]]:
                key = (row[0], row[3], row[5], row[4], nou, "NUM")
                dades[key] += row[-1]
        upload = [k + (v,) for (k, v) in dades.items()]
        # esperats
        poblacio = c.defaultdict(dict)
        sql = """select a.up, nvl(b.medea, '2U'), a.edat, a.comb, a.sexe, a.n
                 from {} a
                 inner join nodrizas.cat_centres_with_jail b
                            on a.up = b.scs_codi""".format(LV_POB)  # noqa
        for up, medea, edat, pob, sex, n in u.Database(*CONNEXIO).get_all(sql):
            poblacio[(medea, edat, pob, sex)][up] = n
        for ind, medea, edat, pob, sex, prev in csv.reader(open(self.deteccio_file), delimiter="{"):  # noqa
            ups = poblacio[(medea, edat, pob, sex)]
            for up, n in ups.items():
                upload.append((up, edat, sex, pob, ind, "DEN", n * float(prev)))  # noqa
        # upload
        cols = ("up varchar(5)", "edat varchar(10)", "sexe varchar(4)",
                "comb varchar(10)", "indicador varchar(10)",
                "conc varchar(10)", "n double")
        with u.Database(*CONNEXIO) as conn:
            conn.create_table(LV_DET, cols, remove=True)
            conn.list_to_table(upload, LV_DET)

    def _get_ecap_uba(self, sql):
        """."""
        return(list(u.Database(*CONNEXIO).get_all(sql)))

    def get_ecap_uba(self):
        """."""
        # ubas
        sql = """select up, uba, ubainf, a.edat, khalix,
                        if(sexe='H', 'HOME', 'DONA'), nvl(medea, '2U'), maca
                 from assignada_tot a
                 inner join nodrizas.cat_centres_with_jail b
                            on a.up = b.scs_codi
                 inner join nodrizas.khx_edats5a c on a.edat = c.edat \
                 where ates = 1 and institucionalitzat = 0"""
        ubas = c.Counter()
        poblacio = c.defaultdict(c.Counter)
        for up, uba, inf, edat, klx, sex, med, maca in u.Database(*CONNEXIO).get_all(sql):  # noqa
            for key in ((up, uba, "M"), (up, inf, "I")):
                if key[1]:
                    poblacio[(klx, sex, med)][key] += (1 - maca)
                    if edat >= 20:
                        ubas[key] += 1
        entren = set([k for k, v in ubas.items() if v >= 50])
        cols = ("up varchar(5)", "uba varchar(5)", "tipus varchar(1)")
        with u.Database(*CONNEXIO) as conn:
            conn.create_table(ECAP_ENTREN, cols, remove=True)
            conn.list_to_table(entren, ECAP_ENTREN)
            conn.execute("alter table {} add unique (up, uba, tipus)".format(ECAP_ENTREN))  # noqa
        # par�metres
        sql = "select distinct grup_codi from eqa_ind where invers = 1 \
               union all select distinct ind_codi from eqa_ind \
                         where nets = 1 and invers = 1"
        inversos = set([cod for cod, in u.Database(*CONNEXIO).get_all(sql)])
        sql = "select indicador, substr(meta, 4, 3), valor / 100 \
               from eqa_metesres_subind_anual \
               where z4 = 'NOINSAT' and meta not like '%INT%'"
        metes = {row[:2]: row[2] for row in u.Database(*CONNEXIO).get_all(sql)}
        sql = "select indicador, left(tipus, 1), valor \
               from eqa_ponderacio_anual where valor > 0"
        ponderacio = {row[:2]: row[2] for row
                      in u.Database(*CONNEXIO).get_all(sql)}
        # captura
        sql = """select a.up, a.uba, a.ubainf, a.ind_codi, a.grup_codi, nets,
                        sum(den), sum(num)
                 from {} partition({}) a
                 inner join eqa_ind b on a.ind_codi = b.ind_codi
                 where ates = 1 and excl_edat = 0 and
                       institucionalitzat = 0 and excl = 0 and ci = 0 and
                       clin = 0 and (maca = 0 or a.grup_codi in {})
                       and a.ind_codi != 'EQD024204'
                 group by a.up, a.uba, a.ubainf, a.ind_codi, a.grup_codi,
                          nets"""
        parts = u.Database(*CONNEXIO).get_table_partitions(MASTER)
        jobs = []
        for k, v in sorted(parts.items(), key=lambda x: x[0], reverse=True):
            this = sql.format(MASTER, k, self.with_maca)
            jobs.append(this)
        pool = m.Pool(8)
        results = pool.map(self._get_ecap_uba, jobs)
        pool.close()
        denominador = c.Counter()
        numerador = c.Counter()
        indicadors = set()
        deteccio = set()
        for chunk in results:
            for up, uba, inf, ind, grp, net, den, num in chunk:
                for key in ((up, uba, "M"), (up, inf, "I")):
                    if key in entren:
                        for codi in (ind, grp) if net else (grp,):
                            indicadors.add(codi)
                            denominador[key + (codi,)] += int(den)
                            numerador[key + (codi,)] += int(num)
                        for nou in self.deteccio_inv[ind]:
                            indicadors.add(nou)
                            deteccio.add(nou)
                            numerador[key + (nou,)] += int(den)
        # esperats
        for ind, medea, edat, pob, sex, prev in csv.reader(open(self.deteccio_file), delimiter="{"):  # noqa
            if pob == "NOINSAT":
                ubas = poblacio[(edat, sex, medea)]
                for key, n in ubas.items():
                    if key in entren:
                        denominador[key + (ind,)] += n * float(prev)
        # c�lcul
        upload = []
        for up, uba, tipus in entren:
            for ind in indicadors:
                key = (up, uba, tipus, ind)
                es_invers = ind in inversos
                es_deteccio = ind in deteccio
                mmax = metes.get((ind, "MAX"), 0)
                mmin = min(metes.get((ind, "MIN"), 0), mmax * 0.9)
                pond = ponderacio.get((ind, tipus), 0)
                detectats = denominador[key]
                resolts = numerador[key]
                resultat = (0 if not detectats
                            else 1 if resolts > detectats
                            else resolts / float(detectats))
                llistat = (0 if es_deteccio
                           else resolts if es_invers
                           else detectats - resolts)
                if mmin:
                    if es_invers:
                        mmin_p = math.ceil(mmin * detectats)
                        mmax_p = math.ceil(mmax * detectats)
                        if mmin_p == mmax_p:
                            mmax_p += 1
                    else:
                        mmin_p = math.floor(mmin * detectats)
                        mmax_p = math.floor(mmax * detectats)
                        if mmin_p == mmax_p and mmax_p:
                            mmin_p -= 1
                    assol = (1 if resolts >= mmax_p
                             else 0 if resolts <= mmin_p
                             else (resolts - mmin_p) / (mmax_p - mmin_p))
                    if es_invers:
                        assol = 1 - assol
                    punts = assol * pond
                    if detectats:
                        mmin_perc = mmin_p / detectats
                        mmax_perc = mmax_p / detectats
                    else:
                        mmin_perc = 0
                        mmax_perc = 0
                else:
                    mmin_p, mmax_p, assol, punts, mmin_perc, mmax_perc = (0, 0, 1, 0, 0, 0)  # noqa
                this = (up, uba, tipus, ind, 0, detectats, resolts, 0, 0,
                        resultat, resultat, assol, punts, llistat, assol,
                        1 * es_invers, mmin_perc, mmax_perc)
                upload.append(this)
        cols = ("up varchar(5)", "uba varchar(5)", "tipus varchar(1)",
                "indicador varchar(10)", "esperats int", "detectats double",
                "resolts int", "deteccio int", "deteccioPerResultat int",
                "resolucio double", "resultat double",
                "resultatPerPunts double", "punts double", "llistat int",
                "resolucioPerPunts double", "invers int", "mmin_p double",
                "mmax_p double")
        with u.Database(*CONNEXIO) as conn:
            conn.create_table(ECAP_UBA, cols, remove=True)
            conn.list_to_table(upload, ECAP_UBA)

    def get_lv_uba(self):
        """."""
        sql = "select ind_codi from eqa_ind where nets = 1"
        nets = set([ind for ind, in u.Database(*CONNEXIO).get_all(sql)])
        sql = "select ind_codi, grup_codi, nets from eqa_ind \
               where ind_codi like 'EQD%'"
        eqd = {ind if nets else grp: ind for (ind, grp, nets)
               in u.Database(*CONNEXIO).get_all(sql)}
        dimensions = [("resolts", "NUM"),
                      ("detectats", "DEN"),
                      ("llistat", "AGNORESOL"),
                      ("resultat * 100", "AGRESOLTR"),
                      ("resolucioPerPunts * 100", "AGRESULT"),
                      ("resultatPerPunts * 100", "AGASSOLP"),
                      ("punts", "AGASSOL"),
                      ("mmin_p * 100", "AGMMINRES"),
                      ("mmax_p * 100", "AGMMAXRES")]
        keys = [row[0] for row in dimensions]
        vals = [row[1] for row in dimensions]
        sql = "select up, uba, tipus, indicador, {} \
               from {}".format(", ".join(keys), ECAP_UBA)
        if not self.grip_flag:
            sql += " where left(indicador, 7) not in {}".format(self.grip_ind)
        upload = {LV_UBA: [], LV_EQD: []}
        for row in u.Database(*CONNEXIO).get_all(sql):
            up, uba, tipus, ind = row[:4]
            for i, analisi in enumerate(vals):
                val = round(row[4 + i], 4)
                if ind not in nets:
                    mod = ind[:3] + "U" + ind[3:]
                    this = (up, uba, tipus, mod, analisi, "NOINSAT", val)
                    upload[LV_UBA].append(this)
                if ind in eqd:
                    mod = eqd[ind][:3] + "U" + eqd[ind][3:]
                    this = (up, uba, tipus, mod, analisi, "NOINSAT", val)
                    upload[LV_EQD].append(this)
        cols = ("up varchar(5)", "uba varchar(5)", "tipus varchar(5)",
                "indicador varchar(10)", "analisis varchar(10)",
                "detalle varchar(10)", "valor double")
        with u.Database(*CONNEXIO) as conn:
            for table, dades in upload.items():
                conn.create_table(table, cols, remove=True)
                conn.list_to_table(dades, table)

    def get_ecap_pacients(self):
        """."""
        sql = "select id_cip_sec, hash_d, codi_sector \
               from nodrizas.eqa_u11_with_jail"
        u11 = {row[0]: row[1:] for row in u.Database(*CONNEXIO).get_all(sql)}
        marca = "if(institucionalitzat = 1, 2, if(a.excl_edat= 1, 3, \
                 if(clin = 1, 4, if(ates = 0, 5, if(ci = 1, 6, \
                 if(maca = 1 and a.grup_codi not in {}, 1, 0))))))".format(self.with_maca)  # noqa
        condicio = "llistat = 1 and a.excl = 0 and ci = 0 and \
                    a.excl_edat <= nvl(c.excl_edat, 1)"
        sql = "select id_cip_sec, up, uba, upinf, ubainf, \
                      if(nets = 1, a.ind_codi, a.grup_codi) grup_codi, \
                      {} exclos, valor, text \
               from {} a \
               inner join eqa_ind b on a.ind_codi = b.ind_codi \
               left join eqa_exclusions_hide c on a.grup_codi = c.indicador \
               where {}".format(marca, MASTER, condicio)
        cols = ("id_cip_sec int", "up varchar(5)", "uba varchar(5)",
                "upinf varchar(5)", "ubainf varchar(5)",
                "grup_codi varchar(10)", "exclos int", "hash_d varchar(40)",
                "sector varchar(4)", "valor varchar(255)", "text varchar(255)")
        with u.Database(*CONNEXIO) as conn:
            dades = set()
            for id, up, uba, upinf, ubainf, grup, exclos, val, text in conn.get_all(sql):
                hash, sector = u11[id]
                dades.add((id, up, uba, upinf, ubainf, grup, exclos, hash, sector, val, text))
            dades = list(dades)
            conn.create_table(ECAP_PACIENT, cols, remove=True)
            conn.list_to_table(dades, ECAP_PACIENT)

    def get_ecap_catalegs(self):
        """."""
        # indicadors
        table = ECAP_CATALEG
        base = "http://10.80.217.201/sisap-umi/indicador/codi/"
        sql = "select distinct indicador from {}".format(ECAP_UBA)
        existeix = tuple([cod for cod, in u.Database(*CONNEXIO).get_all(sql)])
        ocult = ("EQA0217", "EQA0301", "EQA0308", "EQA0309", "EQA0312",
                 "EQA0313", "EQD024204")
        if not self.grip_flag:
            ind = self.grip_ind.replace("(", "").replace(")", "").replace("'", "").split(",")  # noqa
            ocult += tuple(ind)
        sqls = ("create or replace table {} as \
                 select distinct a.grup_codi indicador, grup_desc literal, \
                        ordre_grup, grup2_codi pare, \
                        if(nets = 1, 0, llistats) llistat, invers, \
                        0 mdet, nvl(least(mmin, mmax * 0.9), 0) mmin, \
                        nvl((least(mmin, mmax * 0.9) + mmax) / 2, 0) mint, \
                        nvl(mmax, 0) mmax, if(grup_codi in {}, 0, 1) toShow, \
                        concat('{}', grup_codi) wiki, '' curt, \
                        nvl(mmin, 0) rsomin, nvl(mmax, 0) rsomax, \
                        'GENERAL' pantalla \
                 from eqa_ind a \
                 left join \
                  (select indicador, \
                          avg(if(meta = 'AGMMINRES', valor / 100, null)) mmin,\
                          avg(if(meta = 'AGMMAXRES', valor / 100, null)) mmax \
                   from eqa_metesres_subind_anual \
                   where z4 = 'NOINSAT' \
                   group by indicador) b on a.grup_codi = b.indicador \
                 where grup_codi in {}".format(table, ocult, base, existeix),
                "alter table {} modify indicador varchar(10)".format(table),
                "insert ignore into {} \
                 select distinct ind_codi, ind_desc, ordre_grup, \
                        grup_codi, llistats, invers, 0, 0, 0, 0, \
                        if(ind_codi in {}, 0, 1), concat('{}', grup_codi), \
                        '', '', '', 'GENERAL' \
                 from eqa_ind \
                 where ind_codi in {}".format(table, ocult, base, existeix))
        deteccio = "insert into {0} \
                    select '{1}', '{2}', {3}, 'EQADET', 0, 0, 0, 0, \
                            0, 0, 1, concat('{4}', '{1}'), '', 0, 0, \
                            'GENERAL' \
                    from dual"
        with u.Database(*CONNEXIO) as conn:
            for sql in sqls:
                conn.execute(sql)
            for i, (cod, lit) in enumerate(sorted(self.deteccio_lit)):
                this = ("Detecci�: " + lit)
                conn.execute(deteccio.format(table, cod, this, i + 1, base))
        # curts
        sqls = ("alter table {} modify curt varchar(80)".format(table),
                "update {} a inner join {} b on a.indicador = b.indicador \
                 set a.curt = b.curt where a.indicador not in {}",
                "update {} a inner join {} b on a.pare = b.indicador \
                 set a.curt = b.curt where a.pare not in {}")
        with u.Database(*CONNEXIO) as conn:
            for sql in sqls:
                this = sql.format(table, "mst_eqa_literals_curts", tuple(DETECCIO.keys())) # noqa
                conn.execute(this)
        # pares
        table = ECAP_CATALEG + "Pare"
        sql = "create or replace table {0} as \
               select distinct grup2_codi pare, grup2_desc literal, \
                      ordre_grup2 ordre, curt, '                ' proces \
               from eqa_ind a \
               left join mst_eqa_literals_curts b \
                     on a.grup2_codi = b.indicador \
               where ind_codi in {1} or \
                     grup_codi in {1} \
               union \
               select 'EQADET', 'Detecci�', 20, 'Detecci�', '' \
               from dual".format(table, existeix)
        u.Database(*CONNEXIO).execute(sql)
        # punts
        informatius = ("EQA0202", "EQA0206", "EQA0228", "EQA0248", "EQA0249",
                       "EQA0302", "EQA0303", "EQA0305", "EQA0306", "EQA0307",
                       "EQA0503", "EQA0504", "EQA0508", "EQA0509", "EQA0510")
        table = ECAP_CATALEG + "Punts"
        sql = "select indicador, left(tipus, 1), valor \
               from eqa_ponderacio_anual where valor > 0"
        punts = {row[:2]: row[2] for row in u.Database(*CONNEXIO).get_all(sql)}
        dades = []
        for ind in existeix:
            for tip in ("M", "I"):
                ponderacio = punts.get((ind, tip), 0)
                if ponderacio:
                    categoria = "P"
                elif ind in informatius:
                    categoria = "I"
                else:
                    categoria = "E"
                dades.append([ind, tip, ponderacio, categoria])
        cols = ("indicador varchar(10)", "tipus varchar(10)", "valor double",
                "categoria varchar(1)")
        with u.Database(*CONNEXIO) as conn:
            conn.create_table(table, cols, remove=True)
            conn.list_to_table(dades, table)
        # exclosos
        table = ECAP_CATALEG + "Exclosos"
        dades = [[0, "Pacients que formen part de l&#39;indicador", 0, 0],
                 [1, "Pacients MACA", 2, 0],
                 [2, "Pacients institucionalitzats", 3, 0],
                 [3, "Pacients exclosos per edat", 4, 0],
                 [4, "Pacients exclosos per motius cl&iacute;nics", 5, 0],
                 [5, "Pacients no atesos el darrer any", 1, 0],
                 [6, "Pacients exclosos per contraindicacions", 6, 0]]
        cols = ("codi int", "descripcio varchar(150)", "ordre int", "valor int")  # noqa
        with u.Database(*CONNEXIO) as conn:
            conn.create_table(table, cols, remove=True)
            conn.list_to_table(dades, table)
        # rev
        sql = "create or replace table exp_khalix_cataleg as \
               select ind_codi, ind_desc, grup_codi, grup_desc, grup2_codi, \
                      grup2_desc \
               from eqa_ind where baixa_=0"
        u.Database(*CONNEXIO).execute(sql)


if __name__ == "__main__":
    eqa = EQA()
    eqa.get_master()
    eqa.get_lv_up()
    eqa.get_lv_pob()
    if ES_BASAL:
        eqa.get_prev_anual()
    eqa.get_lv_deteccio()
    eqa.get_ecap_uba()
    eqa.get_lv_uba()
    eqa.get_ecap_pacients()
    eqa.get_ecap_catalegs()
