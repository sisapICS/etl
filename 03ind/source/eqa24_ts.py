# -*- coding: utf-8 -*-

"""
.
"""

import collections as c
import dateutil
import dateutil.relativedelta

import sisapUtils as u


agrupador_diagnostic_viu_sol = (107,)
agrupadors_diagnostics_dependencia = (98,)
agrupador_escala_suport_social = 825
codis_escala_gijon = "('VSEVS', 'VZ3005')"
codis_actuacions_treball_social = "('AZ1001','AZ1002','AZ1003','VZ1003','VZ101')"


class EQATreballSocial():
    """."""

    def __init__(self):
        """."""

        self.get_dextraccio();
        self.get_centres();                                                     print("get_centres(): executat")
        self.get_poblacio();                                                    print("get_poblacio(): executat")
        self.get_diagnostics();                                                 print("get_diagnostics(): executat")
        self.get_valoracions_socials();                                         print("get_valoracions_socials(): executat")
        self.get_variables();                                                   print("get_variables(): executat")
        self.get_escala_suport_social();                                        print("get_escala_suport_social(): executat")
        self.get_viu_sol_valoracio_social();                                    print("get_viu_sol_valoracio_social(): executat")
        self.get_indicadors();                                                  print("get_indicadors(): executat")
        self.upload_tables();                                                   print("upload_tables(): executat")

    
    def get_dextraccio(self):
        """ . """

        sql = """
                SELECT
                    data_ext
                FROM
                    dextraccio
	          """
        self.dextraccio, = u.getOne(sql, "nodrizas")

        print("Data d'extraccio: {}".format(self.dextraccio))


    def get_centres(self):
        """."""

        self.centres = dict()

        sql = """
                SELECT
                    scs_codi,
                    ics_codi
                FROM
                    cat_centres
              """
        for (up, br) in u.getAll(sql, "nodrizas"):
            self.centres[up] = br


    def get_poblacio(self):
        """
        entitats: (br, br + 'M' + uba, br + 'I' + ubainf
        """

        self.poblacio = dict()

        sql = """
                SELECT
                    id_cip_sec,
                    up,
                    uba,
                    upinf,
                    ubainf,
                    edat,
                    sexe,
                    ates,
                    seccio_censal,
                    institucionalitzat,
                    up_residencia,
                    maca,
                    trasplantat
                FROM
                    assignada_tot
                WHERE
                    institucionalitzat = 0
              """
        for id_cip_sec, up, uba, upinf, ubainf, edat, sexe, ates, seccio_censal, institucionalitzat, up_residencia, maca, trasplantat in u.getAll(sql, "nodrizas"):
            if up in self.centres:
                br = self.centres[up]
                self.poblacio[id_cip_sec] = {"up": up, "br": br, "edat": edat, "uba": uba, "upinf": upinf, "ubainf": ubainf, 
                                             "sexe": sexe, "ates": ates, "seccio_censal": seccio_censal, "maca": maca, "trasplantat": trasplantat,
                                             "institucionalitzat": institucionalitzat, "up_residencia": up_residencia}


    def get_diagnostics(self):
        """ Ens quedem amb la primera de les dates d'un dx lligat amb discapacitats """

        self.dates_diagnostics_discapacitat, self.dates_diagnostics_demencia, self.dates_diagnostics_ATDOM = c.defaultdict(set), c.defaultdict(set), c.defaultdict(set)
        self.viu_sol, self.infants_adolescents_amb_risc, self.dependencia = set(), set(), set()

        agrupadors_diagnostics = agrupador_diagnostic_viu_sol
        agrupadors_diagnostics += agrupadors_diagnostics_dependencia

        sql = """
                SELECT
                    id_cip_sec,
                    ps,
                    dde
                FROM
                    eqa_problemes
                WHERE
                    ps IN {}
              """.format(agrupadors_diagnostics)
        for id_cip_sec, agrupador, data_diagnostic in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                if agrupador in agrupador_diagnostic_viu_sol:
                    self.viu_sol.add(id_cip_sec)
                elif agrupador in agrupadors_diagnostics_dependencia:
                    self.dependencia.add(id_cip_sec)


    def get_valoracions_socials(self):
        """ Ens quedem amb la �ltima de les dates a la qual s'ha realitzar una valoraci� social """
        
        self.valoracions_socials = c.defaultdict(lambda: c.defaultdict(set))

        sql = """
                SELECT
                    id_cip_sec,
                    xml_data_alta
                FROM
                    xml_detall
                WHERE
                    camp_codi IN ('Identitat_Result', 'Xarxa_Result', 'Habitatge_Result',
                                  'Economia_Result', 'Laboral_Result', 'Formacio_Result',
                                  'Nivell_Result', 'Juridica_Result', 'Capacitat_Result',
                                  'Rif_Result', 'Recursos_Result', 'Prestacions_Result')
                    AND camp_valor <> 'Sense Valorar'   
              """
        for id_cip_sec, data_valoracio_social in u.getAll(sql, "import"):
            if id_cip_sec in self.poblacio:
                self.valoracions_socials[id_cip_sec]["data_valoracio_social"].add(data_valoracio_social)


    def get_variables(self):
        """ . """

        self.dates_escala_gijon = c.defaultdict(set)   

        for particio in u.multiprocess(get_variables_single, u.getSubTables("variables"), 8):
            for id_cip_sec, vu_cod_vs, data_vs in particio:
                if id_cip_sec in self.poblacio:
                    if vu_cod_vs in codis_escala_gijon:
                        self.dates_escala_gijon[id_cip_sec].add(data_vs)


    def get_escala_suport_social(self):
        """ . """
        
        self.dates_escala_suport_social =  c.defaultdict(set)
        sql = """
                SELECT
                    id_cip_sec,
	                data_var
                FROM
                    eqa_variables
                WHERE
                    agrupador = {}
              """.format(agrupador_escala_suport_social)
        for id_cip_sec, data_var in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                self.dates_escala_suport_social[id_cip_sec].add(data_var)

    
    def get_viu_sol_valoracio_social(self):
        """ . """
        
        sql = """
            SELECT
                id_cip_sec, xml_data_alta, camp_valor 
            FROM
                xml_detall
            WHERE
                xml_origen = 'GABINETS'
                AND XML_TIPUS_ORIG IN ('VAL_SPS_NE', 'VAL_SPS_TN', 'VAL_SPS_AD')
                AND CAMP_CODI = 'IiC_Convivencia'
                AND xml_data_alta <= '{}'
              """.format(self.dextraccio)
        dates = {}
        for id_cip_sec, data_alta, valor in u.getAll(sql, "import"):
            if id_cip_sec not in dates:
                dates[id_cip_sec] = (data_alta,valor)
            elif data_alta > dates[id_cip_sec][0]:
                dates[id_cip_sec] = (data_alta, valor)
        for id_cip_sec, (data_alta, valor) in dates.items():
            if id_cip_sec in self.poblacio and valor == '0':
                self.viu_sol.add(id_cip_sec)


    def get_indicadors(self):
        """ . """

        self.indicador_0408A = list()
        self.indicador_0409A = list()

        dextraccio_menys_36_messos = self.dextraccio - dateutil.relativedelta.relativedelta(months=36)

        # 0408A

        for id_cip_sec in self.viu_sol:
            if self.poblacio[id_cip_sec]["edat"] >= 80:
                num = 0
                up = self.poblacio[id_cip_sec]["up"]
                uba = self.poblacio[id_cip_sec]["uba"]
                upinf = self.poblacio[id_cip_sec]["upinf"]
                ubainf = self.poblacio[id_cip_sec]["ubainf"]
                edat = self.poblacio[id_cip_sec]["edat"]
                sexe = self.poblacio[id_cip_sec]["sexe"]
                seccio_censal = self.poblacio[id_cip_sec]["seccio_censal"]
                up_residencia = self.poblacio[id_cip_sec]["up_residencia"]
                institucionalitzat = self.poblacio[id_cip_sec]["institucionalitzat"]
                maca = self.poblacio[id_cip_sec]["maca"]
                trasplantat = self.poblacio[id_cip_sec]["trasplantat"]
                ates = self.poblacio[id_cip_sec]["ates"]
                if id_cip_sec in self.valoracions_socials and id_cip_sec in self.dates_escala_suport_social and id_cip_sec in self.dates_escala_gijon:
                    data_ultima_valoracio_social = max(self.valoracions_socials[id_cip_sec]["data_valoracio_social"])
                    data_ultima_escala_suport_social = max(self.dates_escala_suport_social[id_cip_sec])
                    data_ultima_escala_gijon = max(self.dates_escala_gijon[id_cip_sec])
                    if dextraccio_menys_36_messos <= min(data_ultima_valoracio_social, data_ultima_escala_suport_social, data_ultima_escala_gijon):
                        num = 1
                self.indicador_0408A.append((id_cip_sec, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat, 'EQA0408A', num, 0, 0, 0))

        # 0409A

        for id_cip_sec in self.dependencia:
            num = 0
            up = self.poblacio[id_cip_sec]["up"]
            uba = self.poblacio[id_cip_sec]["uba"]
            upinf = self.poblacio[id_cip_sec]["upinf"]
            ubainf = self.poblacio[id_cip_sec]["ubainf"]
            edat = self.poblacio[id_cip_sec]["edat"]
            sexe = self.poblacio[id_cip_sec]["sexe"]
            seccio_censal = self.poblacio[id_cip_sec]["seccio_censal"]
            up_residencia = self.poblacio[id_cip_sec]["up_residencia"]
            institucionalitzat = self.poblacio[id_cip_sec]["institucionalitzat"]
            maca = self.poblacio[id_cip_sec]["maca"]
            trasplantat = self.poblacio[id_cip_sec]["trasplantat"]
            ates = self.poblacio[id_cip_sec]["ates"]
            if edat >= 18:
                if id_cip_sec in self.valoracions_socials and id_cip_sec in self.dates_escala_suport_social:
                    data_ultima_valoracio_social = max(self.valoracions_socials[id_cip_sec]["data_valoracio_social"])
                    data_ultima_escala_suport_social = max(self.dates_escala_suport_social[id_cip_sec])
                    if dextraccio_menys_36_messos <= min(data_ultima_valoracio_social, data_ultima_escala_suport_social):
                        num = 1
            else:
                if id_cip_sec in self.valoracions_socials:
                    data_ultima_valoracio_social = max(self.valoracions_socials[id_cip_sec]["data_valoracio_social"])
                    if dextraccio_menys_36_messos <= data_ultima_valoracio_social:
                        num = 1                    
            self.indicador_0409A.append((id_cip_sec, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat, 'EQA0409A', num, 0, 0, 0))


    def upload_tables(self):
        """."""
        cols = "(id_cip_sec int, up varchar(5), uba varchar(7), upinf varchar(5), ubainf varchar(7), edat int, sexe varchar(1), \
            seccio_censal varchar(10), up_residencia varchar(13), institucionalitzat int, maca int, ates int, trasplantat int, ind_codi varchar(10), \
            num int, excl int, ci int, clin int)"
        u.createTable("eqa0408a_ind2", cols, "eqa_ind", rm=True)
        u.listToTable(self.indicador_0408A, "eqa0408a_ind2", "eqa_ind")
        u.createTable("eqa0409a_ind2", cols, "eqa_ind", rm=True)
        u.listToTable(self.indicador_0409A, "eqa0409a_ind2", "eqa_ind")

def get_variables_single(taula):
    """ . """
    
    print(taula)
    sql = """
            SELECT
                id_cip_sec,
                vu_cod_vs,
                vu_dat_act
            FROM
                {}
            WHERE
                (vu_cod_vs IN {}
                OR vu_cod_vs IN {})
            """.format(taula, codis_actuacions_treball_social, codis_escala_gijon)
    return list(u.getAll(sql, "import"))

if __name__ == "__main__":
    EQATreballSocial()