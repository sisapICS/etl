# coding: latin1

"""
.
"""

import collections as c
import os

import sisapUtils as u


TABLE = "exp_ecap_pacient_new"
DATABASE = "eqa_ind"


class Avisos(object):
    """."""

    def __init__(self):
        """."""
        self.get_cli_to_ind()
        self.get_avisos()
        self.get_vacunes()
        self.get_llistats()
        self.upload_data()
        self.set_cataleg()
        self.set_avstb201()

    def get_cli_to_ind(self):
        """."""
        self.cli_to_ind = c.defaultdict(set)
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        file = "../../08alt/dades_noesb/avisos_indicadors.txt"
        for row in u.readCSV(file, sep="@"):
            self.cli_to_ind[row[1]].add(row[0][:7])

    def get_avisos(self):
        """."""
        self.avisos = {}
        self.codis = set()
        sql = "select id_cip_sec, cli, text4, text5 from mst_rec_avisos"
        for id, cli, text4, text5 in u.getAll(sql, "eqa_ind"):
            if not text4 or cli == "CLI1":
                status = 0
                valor = None
            elif text4[:3] == "Cap":
                status = -2
                valor = None
            else:
                status = -1
                if text4[:4] == "Data":
                    valor = text5
                else:
                    valor = "{} ({})".format(text5, text4)
            for ind in self.cli_to_ind[cli]:
                if cli != "CLI9":
                    self.avisos[(id, ind)] = (status, valor)
                self.codis.add(ind)

    def get_vacunes(self):
        """."""
        dext = u.getOne("select data_ext from dextraccio", "nodrizas")[0]
        y = dext.year if dext.month > 8 else (dext.year) - 1
        relacio = {"A-GRIP": ("EQA0501", "EQA0502"),
                   "A00043": ("EQA0503", "EQA0504")}
        self.vacunes = set(relacio["A-GRIP"] + relacio["A00043"])
        sql = "select a.id_cip_sec, b.antigen \
               from import.exclusions2withjail a \
               inner join import.cat_prstb040_new b on a.ief_cod_v = b.vacuna \
               where b.antigen in ('A-GRIP', 'A00043') and \
                     ief_inc_exc = 'E' and \
                     ief_data_alta > '{}0901' and \
                     ief_data_baixa = 0".format(y)
        for id, antigen in u.getAll(sql, "import"):
            for ind in relacio[antigen]:
                self.avisos[(id, ind)] = (-3, None)

    def get_llistats(self):
        """."""
        self.llistats = []
        sql = "select id_cip_sec, up, uba, upinf, ubainf, \
                      grup_codi, exclos, hash_d, sector, valor, text \
               from exp_ecap_pacient"
        for id, up, uba, upinf, ubainf, ind, exclos, hash, sector, val, txt in u.getAll(sql, "eqa_ind"):  # noqa
            if exclos == 0 and (id, ind) in self.avisos:
                status, valor = self.avisos[(id, ind)]
            elif exclos == 0 and ind in self.vacunes:
                status = -4
                valor = None
            else:
                status = exclos
                valor = None
            this = (id, up, uba, upinf, ubainf, ind, exclos,
                    hash, sector, status, valor, txt)
            self.llistats.append(this)

    def upload_data(self):
        """."""
        cols = "(id_cip_sec int, up varchar(5), uba varchar(5), \
                 upinf varchar(5), ubainf varchar(5), grup_codi varchar(10), \
                 exclos int, hash_d varchar(40), sector varchar(4), \
                 status int, valor varchar(255), text varchar(255))"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.llistats, TABLE, DATABASE)

    def set_cataleg(self):
        """."""
        table = "exp_ecap_catalegexclosos"
        upload = [(-2, "Cap determinaci&oacute; en el periode", -2, 0),
                  (-1, "Darrera determinaci&oacute; alterada", -1, 1),
                  (-4, "Immunitzaci&oacute; pendent", -4, 0),
                  (-3, "Immunitzaci&oacute; rebutjada", -3, 0)]
        eliminar = tuple([row[0] for row in upload])
        u.execute("delete from {} \
                   where codi in {}".format(table, eliminar), "eqa_ind")
        u.listToTable(upload, table, "eqa_ind")

    def set_avstb201(self):
        """."""
        table = "avstb201"
        upload = [(codi, 1) for codi in self.codis]
        u.execute("truncate table {}".format(table), "pdp")
        u.listToTable(upload, table, "pdp")


if __name__ == "__main__":
    Avisos()
