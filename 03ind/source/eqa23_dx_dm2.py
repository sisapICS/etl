
# -*- coding: utf-8 -*-

import sisapUtils as u
import collections as c
import sisaptools as st
import datetime

INDICADOR = 'EQD023903'


class Indicador():
    def __init__(self):
        self.get_dextraccio()
        print('dextraccio done')
        self.create_table()
        print('table')
        self.get_denominador()
        print('den')
        self.get_numerador()
        print('num')
        self.get_exclusions()
        print('excl')
        self.get_patient_info()
        print('patient')
        self.resultat()

    def get_dextraccio(self):
        """Get current extraction date. Returns date in datetime.date format"""
        sql="select data_ext from dextraccio"
        self.current_date = u.getOne(sql, "nodrizas")[0]
        self.two_years_ago = self.current_date - datetime.timedelta(days=730)
        print(self.current_date, self.two_years_ago)

    def create_table(self):
        self.table, self.db = 'EQD023903_ind2', 'eqa_ind'
        with st.Database("p2262", "eqa_ind") as eqa:
            eqa.execute("create or replace table {} like eqa0201a_ind2".format(self.table))
        
    def get_denominador(self):
        denominador = {}
        glicemies_deju_consecutives = c.defaultdict(dict)
        glicada = c.defaultdict(dict)
        glicemies = c.defaultdict(dict)
        glicemies_deju = c.defaultdict(set)
        sql = """select id_cip_sec, agrupador, valor, usar, data_var from nodrizas.eqa_variables
                where data_var between date('{}') and date('{}')
                and agrupador in (20, 458, 459, 460, 461)""".format(self.two_years_ago, self.current_date)
        for id, agrupador, valor, usar, data in u.getAll(sql, 'nodrizas'):
            # mirem si t� 2 glic�mies consecutives
            denominador[id] = (agrupador, usar, valor, data)
            if agrupador == 460:
                if valor >= 126:
                    glicemies_deju[id].add(data)
                    glicemies_deju_consecutives[id][usar] = data
            if agrupador == 20 and valor >= 6.5:
                glicada[id][usar] = data
            if agrupador in (458, 459, 460, 461) and usar == 1:
                glicemies[id][agrupador] = (data, valor)
        
        self.den = set()
        # El denominador est� compost per 5 criteris en format or:
        # CRITERI 1: 2 glic�mies en dej� ? 126 consecutives o 2 hba1c consecutives >= 6.5
        for id, valors in glicemies_deju_consecutives.items():
            if 1 in valors and 2 in valors:
                if (valors[1]-valors[2]).days >= 90:
                    self.den.add(id)
        for id, valors in glicada.items():
            if 1 in valors and 2 in valors:
                if (valors[1]-valors[2]).days >= 90:
                    self.den.add(id)
        # CRITERI 2: �ltima glic�mia en qualsevol moment 
        # (glic�mia en dej�, capilar o venosa en qualsevol moment) ? 200 
        # sense cap altra glic�mia en dej� de control posterior
        valor_ultima_glicemia_200 = set()
        for id, gli in glicemies.items():
            # busco la data de l'�ltima glic�mia de qualsevol agrupador i el seu valor
            # i ho guardo en 
            max_data = None
            valor_max_data = 0
            posterior = False
            for agrupador, (data, valor) in gli.items():
                if max_data == None:
                    max_data = data
                    valor_max_data = valor
                if max_data < data:
                    max_data = data
                    valor_max_data = valor
            # miro si el valor de l'�ltima lic�mia �s >= 200
            if valor_max_data >= 200:
                valor_ultima_glicemia_200.add((id, max_data))
                if id in glicemies_deju:
                    for d in glicemies_deju[id]:
                        if d > max_data: posterior = True
                if posterior == False:
                    self.den.add(id)
        # CRITERI 3: �ltima glic�mia en qualsevol moment 
        # (glic�mia en dej�, capilar o venosa en qualsevol moment) ? 200 
        # + glic�mia posterior en dej� ?126
        for (id, data) in valor_ultima_glicemia_200:
            if id in glicemies_deju:
                for d in glicemies_deju[id]:
                    if d > data:
                        self.den.add(id)
        # CRITERI 4: una glic�mia en dej� >= 126 + una glicada >= 6.5 (en qualsevol moment)
        for id in glicemies_deju:
            if id in glicada:
                self.den.add(id)
        # CRITERI 5: �ltima HbA1C ? 6.5% + �ltima glic�mia venosa en dej� >126.
        for id, valors in glicemies_deju_consecutives.items():
            if 1 in valors and id in glicada:
                if 1 in glicada[id]:
                    self.den.add(id)

    def get_numerador(self):
        self.num = set()
        sql = """select id_cip_sec from nodrizas.eqa_problemes where ps = 18"""
        for id, in u.getAll(sql, 'nodrizas'):
            if id in self.den:
                self.num.add(id)
    
    def get_exclusions(self):
        """Calculem les exclusions"""
        # Si t� PS 24 o 487 --> excl�s
        self.excl = set()
        sql = """select id_cip_sec from nodrizas.eqa_problemes where ps in (24, 487)"""
        for id, in u.getAll(sql, 'nodrizas'):
            if id in self.den:
                self.excl.add(id)
        candidats = c.defaultdict(set)
        # Si t� alguna de les seg�ents variables 458, 459, 461 amb valor >= 200
        # i t� variables 460 amb valor <126 --> excloem
        sql = """select id_cip_sec, agrupador, valor from nodrizas.eqa_variables
                where data_var between date('{}') and date('{}')
                and agrupador in (458, 459, 460, 461)""".format(self.two_years_ago, self.current_date)
        for id, agrupador, valor in u.getAll(sql, 'nodrizas'):
            if agrupador == 460 and valor < 126:
                candidats['C1'].add(id)
            elif agrupador in (458, 459, 461) and valor >= 200:
                candidats['C2'].add(id)
        for cip in candidats['C1']:
            if cip in candidats['C2']:
                self.excl.add(cip)

    def get_patient_info(self):
        """Gets the patients of interest"""
        print('pop')
        self.poblacio = {}
        sql = """select id_cip_sec, up, uba, upinf, ubainf, edat, sexe, seccio_censal,
              up_residencia, institucionalitzat, maca, ates, trasplantat
            from nodrizas.assignada_tot where edat >= 15 and edat <= 89"""
        for id, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat in u.getAll(sql, 'nodrizas'):
            self.poblacio[id] = (up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat)

    def resultat(self):
        """Computes the final table"""
        upload = []
        for id in self.poblacio:
            if id in self.den:
                up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat = self.poblacio[id]
                num = 1 if id in self.num else 0
                excl = 1 if id in self.excl else 0
                upload.append((id, up, uba, upinf, ubainf, edat, sexe, seccio_censal, up_residencia, institucionalitzat, maca, ates, trasplantat, INDICADOR, num, excl, 0, 0))
        u.listToTable(upload, self.table, self.db)
        

if __name__ == '__main__':
    Indicador()