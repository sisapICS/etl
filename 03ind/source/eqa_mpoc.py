# coding: iso-8859-1

import sisapUtils as u
# import sys
# import datetime
# from time import strftime
from collections import Counter

CalculProvisional = True
nod = "nodrizas"
db = "eqa_ind"
agr_mpoc = 62
codi_indicador = "EQA3301A"
proces = 'MPOC'
dimensio = 'PREVSEC'

"""
Aquest indicador que ha proposat Elisabet
    per pacte territorial Girona formar� part EQA proc�s MPOC
De moment, doncs el calculem com a bolet
    per carregar a khalix en fitxer separat
Quan tinguem eqa proc�s mpoc haurem afegir al c�lcul eqa per processos.
Els passem a khalix com a NUM i DEN.
    Quan integrem en proc�s ja ser� NUMZ i DENZ
"""


def get_data_fa1any():
    """
    Data de fa un any i actual
    """
    sql = """
    select
        date_add(date_add(data_ext, interval - 12 month), interval + 1 day),
        data_ext
    from
        dextraccio
    """
    for past, current in u.getAll(sql, nod):
        return past, current


def get_fumadors(past_date):
    """
    Aconsegueix fumadors a inici del per�ode
    """
    sql = "select id_cip_sec, dalta,dbaixa from eqa_tabac where tab=1"
    return {id for (id, dalta, dbaixa) in u.getAll(sql, nod)
            if dalta < past_date < dbaixa}


def get_exfumadors(past_date, current_date):
    """Aconsegueix els exfumadors del per�ode
    """
    sql = "select id_cip_sec, dalta from eqa_tabac where tab=2 and last=1"
    return {
        id for (id, dalta) in u.getAll(sql, nod)
        if past_date <= dalta <= current_date}


def get_mpocs(agr_mpoc):
    """
    Aconsegueix pacient amb mpoc
    """
    sql = """
    select
        id_cip_sec,
        dde
    from
        eqa_problemes where ps = {}
    """.format(agr_mpoc)
    return {
        id for (id, dalta) in u.getAll(sql, nod)
        }


def get_indicador(fumadors, exfumadors, mpoc):
    """
    Construeix indicador a partir del denominador (fumadors + mpoc)
        i numerador(exfumadors).
    """
    sql = """
        select
            id_cip_sec,
            up,
            uba,
            upinf,
            ubainf,
            edat,
            sexe,
            ates_novc,
            institucionalitzat,
            maca
        from
            assignada_tot where edat >= 15
    """
    rows = []
    for (id, up, uba, upinf, ubainf,
         edat, sexe, ates, institucionalitzat, maca) in u.getAll(sql, nod):
        if id in fumadors and id in mpoc:
            num = 0
            if id in exfumadors:
                num = 1
            this = (
                id, codi_indicador,
                up, uba,
                upinf, ubainf,
                edat, sexe,
                ates,
                institucionalitzat,
                maca,
                num,
                1, 0
                )
            rows.append(this)
    return rows


def export_table(table, columns, db, rows):
    u.createTable(table, columns, db, rm=True)
    u.listToTable(rows, table, db)


def get_tb_khalix(tb, dim, pro):
    exp_khalixUP, exp_khalixUBA = Counter(), Counter()
    sql = """
    select
        id_cip_sec,
        indicador,
        up,
        uba,
        upinf,
        ubainf,
        num,
        den
    from {}
    where
        ates = 1 and institucionalitzat = 0 and maca = 0
    """.format(tb)
    for id, ind, up, uba, upinf, ubainf, num, den in u.getAll(sql, db):
        exp_khalixUP[(ind, up, 'NUM', dim, pro)] += num
        exp_khalixUP[(ind, up, 'DEN', dim, pro)] += den
        exp_khalixUBA[(ind, up, uba, 'M', 'NUM', dim, pro)] += num
        exp_khalixUBA[(ind, up, uba, 'M', 'DEN', dim, pro)] += den
        exp_khalixUBA[(ind, upinf, ubainf, 'I', 'NUM', dim, pro)] += num
        exp_khalixUBA[(ind, upinf, ubainf, 'I', 'DEN', dim, pro)] += den
    rows, rows2 = [], []
    for (ind, up, analisi, dim, pro), d in exp_khalixUP.items():
        rows.append((ind, up, analisi, dim, pro, d))
    for (ind, up, uba, tipus, analisi, dim, pro), d in exp_khalixUBA.items():
        rows2.append((ind, up, uba, tipus, analisi, dim, pro, d))
    return rows, rows2


if __name__ == '__main__':
    u.printTime("Inici")
    past_date, current_date = get_data_fa1any()
    print(current_date)
    fumadors = get_fumadors(past_date)
    u.printTime("Fumadors")
    exfumadors = get_exfumadors(past_date, current_date)
    u.printTime("Exfumadors")
    mpoc = get_mpocs(agr_mpoc)
    u.printTime("mpoc")
    rows = get_indicador(fumadors, exfumadors, mpoc)
    taula_name = "mst_eqa_mpoc_provisional"
    columns = "(id_cip_sec int, indicador varchar(10),up varchar(8), uba varchar(8), upinf varchar(8), ubainf varchar(8), edat int, sexe varchar(5),ates int, institucionalitzat int, maca int,num int, den int, excl int)"  # noqa
    export_table(taula_name, columns, db, rows)

    """
    Exporto a khalix. Aix� haurem de canviar quan integrem en processos
    """
    if CalculProvisional:
        centres = "nodrizas.cat_centres"
        tb_klx_up = "exp_khalix_up_ind_proc_mpoc_prov"
        tb_klx_uba = "exp_khalix_uba_ind_proc_mpoc_prov"
        columns_up = "(indicador varchar(10), up varchar(5), analisi varchar(10), dimensio varchar(10), proces varchar(10), n int)"  # noqa
        columns_uba = "(indicador varchar(10), up varchar(5), uba varchar(5), tipus varchar(1), analisi varchar(10), dimensio varchar(10), proces varchar(10), n int)"  # noqa
        rows_up, rows_uba = get_tb_khalix(taula_name, dimensio, proces)
        u.printTime("Taula khalix")
        export_table(tb_klx_up, columns_up, db, rows_up)
        export_table(tb_klx_uba, columns_uba, db, rows_uba)

        sql = """
            select
                indicador,
                concat('A','periodo'),
                ics_codi,
                analisi,
                dimensio,
                proces,
                'DIM6SET',
                'N',
                n
            from {}.{} a inner join {} b
                on a.up=scs_codi
            """.format(db, tb_klx_up, centres)
        file = 'EQA_PROC_MPOC_PROVISIONAL'
        u.exportKhalix(sql, file)

        sql = """
            select
                indicador,
                concat('A','periodo'),
                concat(ics_codi,tipus,uba),
                analisi,
                dimensio,
                proces,
                'DIM6SET',
                'N',
                n
            from {}.{} a inner join {} b
                on a.up=scs_codi
            """.format(db, tb_klx_uba, centres)
        file = "EQA_PROC_MPOC_PROVISIONAL_UBA"
        u.exportKhalix(sql, file)
