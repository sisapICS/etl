
import sisapUtils as u
import collections as c

"""
Aquest proces agafa la taula creada al proces general (eqa_ind.eqa0216a_ind2) i la modifica afegint els seguents canvis:
1) filtrar el RCV al darrer abans d'iniciar l'estatina tal i com es fa per EQA0238 (ja sabem que hi
ha registres posteriors i en teoria baixara un cop iniciada estatina i modificacions estils de vida).
2) A banda, tot i sera excepcional, tambe donaria com a resolts aquells pacients que tot i tenir un RCV
a l'inici estatina inferior en tenen algun posterior de superior al que marca l'indicador (>5%)
"""

# estatines
estatines = {}
sql = """select id_cip_sec, pres_orig from nodrizas.eqa_tractaments where farmac = 82"""
for id_cip, data_o in u.getAll(sql, 'nodrizas'):
    estatines[id_cip] = data_o

# RCV
rcv = c.defaultdict(dict)
ok = set()
sql = """select id_cip_sec, data_var, valor from nodrizas.eqa_variables where agrupador = 10"""
for id_cip, data_var, valor in u.getAll(sql, 'nodrizas'):
    if id_cip in estatines:
        data_estatines = estatines[id_cip]
        if data_var > data_estatines and valor > 5: 
            ok.add(id_cip)
        elif data_var < data_estatines:
            rcv[id_cip][data_var] = valor

for id_cip in rcv:
    max_d = max(rcv[id_cip].keys())
    if rcv[id_cip][max_d] <= 5:
        ok.add(id_cip)


u.createTable('eqa216_canvis', '(id_cip_sec int, prova varchar(2))', 'eqa_ind', rm=True)
u.listToTable([(id_cip, 'a') for id_cip in ok], 'eqa216_canvis', 'eqa_ind')
u.execute('create index ind_cip on eqa216_canvis (id_cip_sec)', 'eqa_ind')


sql = """update eqa0216a_ind2 a inner join eqa216_canvis b on a.id_cip_sec=b.id_cip_sec set a.num=0"""
u.execute(sql, 'eqa_ind')

