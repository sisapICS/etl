# coding: latin1

import collections as c
import datetime as d
import dateutil.relativedelta as du
import multiprocessing as m

import sisaptools as u


DEBUG = False
FMO = False


class Main(object):
    """."""

    def get_criteris(self):
        """."""
        sql = "select taula, agrupador, data_min, data_max, \
                      date_add(data_ext, interval - data_min month), \
                      date_add(data_ext, interval - data_max month), \
                      valor_min, a.ind_codi, tipus, sense, \
                      if(unitat = 'week', 7, 1) \
               from eqa_relacio a \
               inner join eqa_temps_ag b on a.ind_codi = b.ind_codi, \
               nodrizas.dextraccio \
               where baixa = 0 and \
                     a.ind_codi in \
                        (select c.ind_codi from eqa_ind c \
                         where tipus = 3 and baixa_ = 0)"
        self.criteris = c.defaultdict(set)
        self.agrupadors = c.defaultdict(set)
        self.sense = set()
        for row in u.Database("p2262", "eqa_ind").get_all(sql):
            self.criteris[row[:2]].add(row[2:])
            self.agrupadors[row[0]].add(int(row[1]))
            if row[8] == "num" and row[9]:
                self.sense.add(row[7])

    def create_structure(self):
        """."""
        sql = "select ind_codi from eqa_ind \
               where tipus = 3 and baixa_ = 0"
        cols = ("id_cip_sec int", "up varchar(5)", "uba varchar(5)",
                "upinf varchar(5)", "ubainf varchar(5)", "edat int",
                "sexe varchar(1)", "seccio_censal varchar(10)",
                "up_residencia varchar(13)", "institucionalitzat int",
                "maca int", "ates int", "trasplantat int",
                "ind_codi varchar(10)", "num int", "excl int", "ci int",
                "clin int", "den int")
        with u.Database("p2262", "eqa_ind") as eqa:
            codis = [cod for cod, in eqa.get_all(sql)]
            for cod in codis:
                taula = "{}_ind2{}".format(cod, "_fmo" if FMO else "")
                eqa.create_table(taula, cols, remove=True,
                                 partition_type="hash",
                                 partition_id="id_cip_sec", partitions=16)

    def get_eqa(self):
        """."""
        if DEBUG:
            Particio("p3", self.criteris, self.agrupadors, self.sense)
        else:
            pool = m.Pool(16)
            results = []
            for i in range(16):
                particio = "p{}".format(i)
                args = (particio, self.criteris, self.agrupadors, self.sense)
                results.append(pool.apply_async(Particio, args=args))
            pool.close()
            pool.join()
            for result in results:
                try:
                    result.get()
                except Exception:
                    raise

    def get_control(self):
        """."""
        upload = []
        sql = "select ind_codi from eqa_ind \
               where tipus = 3 and baixa_ = 0"
        for ind, in u.Database("p2262", "eqa_ind").get_all(sql):
            for sufix in ("", "_fmo"):
                taula = "{}_ind2{}".format(ind, sufix)
                this = "select count(1), sum(den), sum(num), sum(excl), \
                               sum(ci), sum(clin) \
                        from {}".format(taula)
                nums = u.Database("p2262", "eqa_ind").get_one(this)
                if not nums:
                    nums = (0,) * 6
                that = (ind, sufix) + nums
                upload.append(that)
        cols = ("ind varchar(10)", "suffix varchar(5)", "cnt int", "den int",
                "num int", "excl int", "ci int", "clin int")
        with u.Database("p2262", "eqa_ind") as eqa:
            eqa.create_table("eqa3_fmo", cols, remove=True)
            eqa.list_to_table(upload, "eqa3_fmo")


class Particio(object):
    """."""

    def __init__(self, particio, criteris, agrupadors, sense):
        """."""
        self.particio = particio
        self.criteris = criteris
        self.agrupadors = agrupadors
        self.sense = sense
        self.get_denominadors()
        self.get_atributs()
        self.get_indicadors()
        self.upload_data()
        self.denominadors = []
        self.upload = []

    def get_denominadors(self):
        """."""
        taules = (
            ["assignacio", "eqa_ind.assignada_tot", ("35", "null", "if(sexe='H', 0, if(sexe='D', 1, 9999))"), False],  # noqa
            ["problemes", "prt_problemes_incid", ("ps", "dde", "pr_gra"), True]
        )
        out = set()
        self.denominadors = c.defaultdict(list)
        for key, origen, columnes, filtre in taules:
            sql = "select id_cip_sec, {} \
                   from {} partition({})".format(", ".join(columnes), origen, self.particio)  # noqa
            if filtre:
                sql += " where {} in {}".format(columnes[0], tuple(self.agrupadors[key]))  # noqa
            for id, cod, ini, val in u.Database("p2262", "nodrizas").get_all(sql):  # noqa
                for t_min, t_max, d_min, d_max, v_min, i_ind, i_tipus, i_sense, factor in self.criteris[(key, cod)]:  # noqa
                    if key == "assignacio":
                        if val != v_min:
                            out.add((i_ind, id))
                    elif i_tipus == "den":
                        if d_min <= ini <= d_max and (i_ind, id) not in out:
                            # pot haver-hi duplicats de dat, igual a cmo
                            self.denominadors[(i_ind, id)].append((ini, set()))

    def get_atributs(self):
        """
        Hi ha diverses coses que no tenen sentit, per exemple:
        - l'�s dels temps (a vegades s'agafen d'eqa_temps_ag, altres es fixen
           manualment a dies o mesos, etc.)
        - hi ha par�metres d'eqa_relacio que no es tenen en compte, per exemple
          les exclusions per variables o els valors de variables i problemes
        L'objectiu era replicar exactament els resultats que s'obtenien amb
        el proc�s antic, no arreglar-lo.
        """
        sqls = ("select id_cip_sec, agr \
                 from prt_ram partition({})".format(self.particio),
                "select id_cip_sec, 505 \
                 from prt_problemes partition({}) \
                 where ps = 163".format(self.particio))
        ram = c.defaultdict(set)
        for sql in sqls:
            for id, agr in u.Database("p2262", "nodrizas").get_all(sql):
                ram[id].add(agr)
        taules = (
            ["problemes", "excl", "prt_problemes_incid", ("ps", "dde")],
            ["problemes", "ci", "prt_problemes", ("ps", "dde")],
            ["prescripcions", None, "prt_tractaments", ("farmac", "pres_orig")],  # noqa
            ["variables", None, "prt_variables", ("agrupador", "data_var")]
        )
        for key, tipus, origen, columnes in taules:
            args = (", ".join(columnes), origen, self.particio, columnes[0],
                    tuple(self.agrupadors[key]))
            sql = "select id_cip_sec, {} \
                   from {} partition({}) \
                   where {} in {}".format(*args)
            for id, cod, ini in u.Database("p2262", "nodrizas").get_all(sql):  # noqa
                for t_min, t_max, d_min, d_max, v_min, i_ind, i_tipus, i_sense, factor in self.criteris[(key, cod)]:  # noqa
                    if not tipus or tipus == i_tipus:
                        if (i_ind, id) in self.denominadors:
                            if not v_min or v_min in ram[id] or i_tipus != "num":  # noqa
                                for dat, obj in self.denominadors[(i_ind, id)]:
                                    if key == "variables":
                                        first = dat + d.timedelta(days=t_min*factor)  # noqa
                                        last = dat + d.timedelta(days=t_max*factor)  # noqa
                                    elif i_tipus == "ci":
                                        first = dat - du.relativedelta(months=t_min)  # noqa
                                        last = dat + du.relativedelta(months=t_max)  # noqa
                                    elif i_tipus == "excl" and key == "problemes":  # noqa
                                        first = dat - d.timedelta(days=t_min)
                                        last = dat + d.timedelta(days=t_max)
                                    else:
                                        first = dat - d.timedelta(days=t_min*factor)  # noqa
                                        last = dat + d.timedelta(days=t_max*factor)  # noqa
                                    if first <= ini <= last:
                                        if i_tipus == "num":
                                            value = "sense" if i_sense else "amb"  # noqa
                                        else:
                                            value = i_tipus
                                        obj.add(value)

    def get_indicadors(self):
        """."""
        sql = "select id_cip_sec, up, uba, upinf, ubainf, edat, sexe, \
                      seccio_censal, up_residencia, institucionalitzat, \
                      maca, ates, trasplantat \
               from assignada_tot partition({})".format(self.particio)
        poblacio = {row[0]: row for row
                    in u.Database("p2262", "eqa_ind").get_all(sql)}
        self.upload = c.defaultdict(list)
        for (ind, id), episodis in self.denominadors.items():
            if id in poblacio:
                den = len(episodis)
                num = sum(["amb" in flags or
                           (ind in self.sense and "sense" not in flags)
                           for dat, flags in episodis])
                # a cmo, excl i ci es fa per episodi i s'aplica a pacient
                excl = 1 * any(["excl" in flags for dat, flags in episodis])
                ci = 1 * any(["ci" in flags for dat, flags in episodis])
                this = poblacio[id] + (ind, num, excl, ci, 0, den)
                self.upload[ind].append(this)

    def upload_data(self):
        """."""
        for key, rows in self.upload.items():
            taula = "{}_ind2{}".format(key, "_fmo" if FMO else "")
            u.Database("p2262", "eqa_ind").list_to_table(rows, taula, partition=self.particio)  # noqa


if __name__ == "__main__":
    main = Main()
    main.get_criteris()
    main.create_structure()
    main.get_eqa()
    if FMO:
        main.get_control()
