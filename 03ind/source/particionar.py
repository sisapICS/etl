# coding: latin1

import sisaptools as u


taules = [("assignada_tot_with_jail", "eqa_ind.assignada_tot", "edat > 14"),
          ("eqa_problemes", None, None), ("eqa_tractaments", None, None),
          ("eqa_variables", None, None), ("eqa_vacunes", None, None),
          ("eqa_immunitzats", None, None), ("eqa_proves", None, None),
          ("eqa_ram", None, None), ("eqa_microalb", None, None),
          ("eqa_familiars", None, None), ("eqa_problemes_incid", None, None),
          ("eqa_u11_with_jail", "prt_u11", None)]

for origen, renamed, where in taules:
    desti = renamed if renamed else origen.replace("eqa", "prt")
    with u.Database("p2262", "nodrizas") as nodrizas:
        columns = nodrizas.get_table_columns(origen)
        create = [nodrizas.get_column_information(col, origen)["create"]
                  for col in columns]
        nodrizas.create_table(desti, create, remove=True,
                              partition_type="hash", partition_id="id_cip_sec",
                              partitions=16)
        sql = "insert into {} select * from {}".format(desti, origen)
        if where:
            sql += " where {}".format(where)
        nodrizas.execute(sql)
