# coding: latin1

import csv
import multiprocessing as m
import os

import sisaptools as u


TABLE = "mst_rec_avisos"


def worker(sql):
    return (list(u.Database("p2262", "eqa_ind").get_all(sql)))


class Avisos(object):
    """."""

    def __init__(self):
        """."""
        sql = "select data_ext, grip_avis from dextraccio"
        self.dextraccio, self.avis_grip = u.Database("p2262", "nodrizas").get_one(sql)  # noqa
        self.get_parametres()
        self.get_agrupadors()
        self.get_hash()
        self.get_avisos()
        self.upload_data()

    def get_parametres(self):
        """."""
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        file = "../../08alt/dades_noesb/avisos_indicadors.txt"
        cols = ("cli", "text1", "text2", "text3", "text6", "text8", "grup", "agr")  # noqa
        self.ind_to_cli = {row[0]: {col: row[i] for i, col
                                    in enumerate(cols, 1)}
                           for row in csv.reader(open(file), delimiter="@")
                           if self.avis_grip or row[0] != "CLI9"}

    def get_agrupadors(self):
        """."""
        sql = "select id_cip_sec, agrupador, data, valor \
               from mst_avisos_pre partition(p{}) \
               where es_data = 1"
        jobs = [sql.format(i) for i in range(16)]
        pool = m.Pool(16)
        dades = pool.map(worker, jobs, chunksize=1)
        pool.close()
        self.agrupadors = {}
        for chunk in dades:
            for row in chunk:
                self.agrupadors[row[:2]] = row[2:]

    def get_hash(self):
        """."""
        sql = "select id_cip_sec, hash_d, codi_sector \
               from nodrizas.prt_u11 partition(p{})"
        jobs = [sql.format(i) for i in range(16)]
        pool = m.Pool(16)
        dades = pool.map(worker, jobs, chunksize=1)
        pool.close()
        self.id_to_hash = {}
        for chunk in dades:
            for row in chunk:
                self.id_to_hash[row[0]] = row[1:]

    def get_avisos(self):
        """."""
        self.upload = []
        for indicador, parametres in self.ind_to_cli.items():
            cli = parametres["cli"]
            agr = int(parametres["agr"]) if parametres["agr"] else None
            sql = "select id_cip_sec, up \
                   from mst_indicadors_pacient partition({}) \
                   where llistat = 1 and excl = 0 and ci = 0 and \
                         clin = 0 and excl_edat = 0".format(indicador)
            for id, up in u.Database("p2262", "eqa_ind").get_all(sql):
                hash, sector = self.id_to_hash[id]
                if parametres["grup"] == "Variable" and agr:
                    if cli == "CLI1":
                        text4 = "Cap en els �ltims 2 anys"
                    else:
                        text4 = "Cap en l'�ltim any"
                    text5 = None
                    if (id, agr) in self.agrupadors:
                        text4, val = self.agrupadors[(id, agr)]
                        text5 = round(val, 1) if cli == "CLI2" else int(val)
                else:
                    text4, text5 = (None, None)
                this = [sector, id, hash, up, cli]
                this += [parametres[key] for key in ("text1", "text2", "text3")]  # noqa
                this += [text4, text5]
                this += [parametres[key] for key in ("text6", "text8", "grup")]
                self.upload.append(this)

    def upload_data(self):
        """."""
        cols = ("sector varchar(4)", "id_cip_sec int", "hash varchar(40)",
                "up varchar(5)", "cli varchar(5)", "text1 varchar(200)",
                "text2 int", "text3 varchar(200)", "text4 varchar(100)",
                "text5 varchar(50)", "text6 varchar(20)", "text8 varchar(20)",
                "grup varchar(20)")
        with u.Database("p2262", "eqa_ind") as eqa:
            eqa.create_table(TABLE, cols, remove=True)
            eqa.list_to_table(self.upload, TABLE)


Avisos()
