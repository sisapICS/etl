# coding: utf-8

import sisapUtils as u
import datetime
from datetime import date

valida = False

profvalida = {
           1: {'tip': 'I', 'up': '00488', 'uba': '25888'},
           2: {'tip': 'I', 'up': '00474', 'uba': 'INF25'},
           3: {'tip': 'I', 'up': '00475', 'uba': 'INF25'}
           }

DB = "eqa_ind"

class Disneampoc(object):

    def __init__(self):
        self.get_poblacio()
        self.get_dades()
        self.get_hash()
        self.get_escala()
        self.get_databmrc()
        self.get_tdesde()
        self.get_Indicador()
        self.export_dades_pacient()
        self.export_dades_pacient_no()

    def get_poblacio(self):
        sql = (
            """
            SELECT
                id_cip_sec,
                pr_gra
            FROM eqa_problemes
            WHERE
                ps = 62 and
                pr_gra != 0
            """,
            "nodrizas"
        )
        self.poblacio = {id: gra for id, gra in u.getAll(*sql)}

    def get_dades(self):
        self.dades = {}
        sql = (
            """
            SELECT
                id_cip_sec,
                up, uba,
                upinf, ubainf
            FROM
                assignada_tot
            WHERE
                edat > 14 and
                ates = 1 and
                institucionalitzat = 0
            """,
            "nodrizas"
        )
        for id, up, uba, upinf, ubainf in u.getAll(*sql):
            if id in self.poblacio:
                self.dades[id] = {
                    'id': id,
                    'up': up,
                    'uba': uba,
                    'upinf': upinf,
                    'ubainf': ubainf,
                    'grav': self.poblacio[id],
                    'BMRC': 0,
                    'databmrc': datetime.date(1900, 1, 1),
                    'tdesde': datetime.timedelta(0),
                    'Imepoc': None,
                    'Indicador': 'IAD0006',
                    'hash': None,
                    'sector': None
                }

    def get_hash(self):
        sql = ("SELECT id_cip_sec, codi_sector, hash_d FROM u11", "import")
        for id, sector, hash in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['hash'] = hash
                self.dades[id]['sector'] = sector

    def get_escala(self):
        sql = (
            """
            SELECT id_cip_sec
            FROM eqa_variables
            WHERE agrupador = 795
            """,
            "nodrizas"
        )
        for id, in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['BMRC'] = 1

    def get_databmrc(self):
        sql = (
            """
            SELECT
                id_cip_sec,
                data_var
            FROM eqa_variables
            WHERE
                agrupador = 795
            """,
            "nodrizas"
        )
        for id, time in u.getAll(*sql):
            if id in self.dades and time > self.dades[id]['databmrc']:
                self.dades[id]['databmrc'] = time

    def get_tdesde(self):
        for id, value in self.dades.iteritems():
            if 'databmrc' in value:
                self.dades[id]['tdesde'] = date.today() - self.dades[id]['databmrc']  # noqa

    def get_Indicador(self):
        for id, value in self.dades.iteritems():
            if value['grav'] == 1 and value['BMRC'] == 1:
                self.dades[id]['Imepoc'] = 1
            elif (value['grav'] == 2 and
                  value['BMRC'] == 1 and
                  value['tdesde'] < datetime.timedelta(365)):
                self.dades[id]['Imepoc'] = 1
            elif (value['grav'] == 3 and
                  value['BMRC'] == 1 and
                  value['tdesde'] < datetime.timedelta(180)):
                self.dades[id]['Imepoc'] = 1
            elif (value['grav'] == 4 and
                  value['BMRC'] == 1 and
                  value['tdesde'] < datetime.timedelta(180)):
                self.dades[id]['Imepoc'] = 1
            else:
                self.dades[id]['Imepoc'] = 0

    @staticmethod
    def export_dades(tb, columns, dades):
        table = 'mst_esc_disnea_mpoc'
        columns_str = "({})".format(', '.join(columns))
        db = DB
        u.createTable(table, columns_str, db, rm=True)
        u.listToTable(dades, table, db)

    def export_dades_pacient(self):
        dades = [
            (d['id'],
             d['up'],
             d['uba'],
             d['upinf'],
             d['ubainf'],
             d['grav'],
             d['BMRC'],
             d['databmrc'],
             d['tdesde'],
             d['Imepoc'],
             d['Indicador'],
             d['hash'],
             d['sector'])
            for id, d in self.dades.items()]
        columns = (
            'id varchar(40)',
            'up varchar(6)',
            'uba varchar(5)',
            'upinf varchar(6)',
            'ubainf varchar(6)',
            'grav int',
            'BMRC int',
            'databmrc varchar(16)',
            'tdesde int',
            'Imepoc int',
            'Indicador varchar(7)',
            'Hash varchar(40)',
            'Sector varchar(8)',
            )
        Disneampoc.export_dades("pacient", columns, dades)

    def export_dades_pacient_no(self):
        dadesp_no = []
        for id, d in self.dades.iteritems():
            if d['Imepoc'] == 0:
                row = (d['id'], d['up'], d['uba'], d['upinf'], d['ubainf'], 0)
                dadesp_no.append(row)

        tablep = 'exp_ecap_IAD0006_pacient'
        columns = (
            "id_cip_sec int",
            "up varchar(6)",
            "uba varchar(5)",
            "upinf varchar(6)",
            "ubainf varchar(6)",
            "exclos int",
            )
        columns_str = "({})".format(', '.join(columns))

        u.createTable(
            tablep,
            columns_str,
            source=DB,
            rm=True)

        u.listToTable(dadesp_no, tablep, DB)
