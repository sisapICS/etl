use nodrizas;
# suposarem que el format es com ped_vacunes
set @td=49;
set @pn20=978;
set @pn13=977;
set @pn23=48;
set @hz=920;
set @excl_hz1=319;
set @excl_hz2=320;
set @grip=99;
set @covid=1085;
set @vph = 782;
set @polio = 311;
set @vhb = 15;
set @vha = 34;
set @macwy_1 = 313;
set @macwy_2 = 883;
set @xrp = 36;
set @rub = 37;
set @par = 38;
set @vrc = 314;
set @emb_1 = 39;
set @emb_2 = 874;
set @quimio = 557;
set @tp_1 = 529;
set @tp_2 = 677;
set @tp_3 = 787;
set @ims_1 = 40;
set @ims_2 = 41;

DROP TABLE IF EXISTS mst_vacunes;
CREATE TABLE mst_vacunes (
  id_cip_sec int,
  up VARCHAR(5) NOT NULL DEFAULT '',
  uba VARCHAR(5) NOT NULL DEFAULT '',
  upinf VARCHAR(5) NOT NULL DEFAULT '',
  ubainf VARCHAR(5) NOT NULL DEFAULT '',
  data_naix DATE NOT NULL DEFAULT 0,
  edat double NULL,
  sexe VARCHAR(1) NOT NULL DEFAULT '',
  ates double null,
  institucionalitzat double null,
  maca double null,
  seccio_censal VARCHAR(10) NOT NULL DEFAULT '',
  up_residencia VARCHAR(13) NOT NULL DEFAULT '',
  trasplantat INT NOT NULL DEFAULT 0,
  TD INT NOT NULL DEFAULT 0,
  PN20 INT NOT NULL DEFAULT 0,
  PN13 INT NOT NULL DEFAULT 0,
  PN23 INT NOT NULL DEFAULT 0,
  PN23_1 INT NOT NULL DEFAULT 0,
  PN23_2 INT NOT NULL DEFAULT 0,
  HZ INT NOT NULL DEFAULT 0,
  HZ_VAC INT NOT NULL DEFAULT 0,
  M_HZ INT NOT NULL DEFAULT 0,
  GRIP INT NOT NULL DEFAULT 0,
  COVID INT NOT NULL DEFAULT 0,
  VPH INT NOT NULL DEFAULT 0,
  POLIO INT NOT NULL DEFAULT 0,
  VHB INT NOT NULL DEFAULT 0,
  VHA INT NOT NULL DEFAULT 0,
  MACWY INT NOT NULL DEFAULT 0,
  XARAMPIO INT NOT NULL DEFAULT 0,
  RUBEOLA INT NOT NULL DEFAULT 0,
  PAROTIDITIS INT NOT NULL DEFAULT 0,
  VRC INT NOT NULL DEFAULT 0,
  EXCL_XRP_VRC INT NOT NULL DEFAULT 0,
  EMB INT NOT NULL DEFAULT 0,
  unique (id_cip_sec)
)

SELECT
	id_cip_sec
   ,up
   ,uba
   ,upinf
   ,ubainf
   ,data_naix
   ,edat
   ,sexe
   ,ates
   ,institucionalitzat
   ,maca
   ,seccio_censal
   ,up_residencia
   ,trasplantat
	,0 AS TD
	,0 AS PN20
	,0 AS PN13
    ,0 AS PN23
	,0 AS PN23_1
	,0 AS PN23_2
	,0 AS HZ
    ,0 AS HZ_VAC
	,0 AS M_HZ
    ,0 AS GRIP
	,0 AS COVID
    ,0 AS VPH
    ,0 AS POLIO
    ,0 AS VHB
    ,0 AS VHA
    ,0 AS MACWY
    ,0 AS XARAMPIO
    ,0 AS RUBEOLA
    ,0 AS PAROTIDITIS
    ,0 AS VRC
    ,0 AS EXCL_XRP_VRC
    ,0 AS EMB
FROM
	nodrizas.assignada_tot_with_jail
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.td = 1
WHERE
    edat >= 14 and agrupador = @td and
    ((dosis>=1 and temps<=2)
    or (dosis>=2 and temps<=7)
    or (dosis >= 3 and maxedat >= 84 and edat >= 14 and edat < 40 and temps <= 120)
    or (dosis >= 3 and maxedat >= 168 and edat >= 40 and edat < 65 and temps <= 120)
    or (dosis >= 3 and maxedat >= 480 and edat >= 65 and temps <= 120))
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.pn20 = 1
WHERE
    dosis >= 1 and agrupador = @pn20 and edat >= 18
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.pn13 = 1
WHERE
    dosis >= 1 and agrupador = @pn13
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.pn23 = 1
WHERE
    dosis >= 1 and agrupador = @pn23
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.pn23_1 = 1
WHERE
    dosis >= 1 and agrupador = @pn23 and timestampdiff(year, data_naix, datamax) >= 65
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.pn23_2 = 1
WHERE
    (dosis >= 1 and agrupador = @pn23 and timestampdiff(year, data_naix, datamax) >= 65) or
    (dosis >= 1 and agrupador = @pn20 and timestampdiff(year, data_naix, datamax) >= 65)
;

-- UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b  ON a.id_cip_sec=b.id_cip_sec
-- LEFT JOIN nodrizas.assignada_tot c ON b.id_cip_sec = c.id_cip_sec
-- SET a.hz = 1
-- WHERE
--     agrupador = @hz and
--     ((dosis >= 2 and edat >= 90)
--     or (dosis >= 2 and edat in (80,81,82))
--     or (dosis >= 2 and edat in (65,66,67))
--     or (dosis >= 1 and temps <= 3))
--     or (edat >= 68 and edat < 80)
--     or (edat >= 83 and edat < 90)
-- ;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b  ON a.id_cip_sec=b.id_cip_sec
LEFT JOIN nodrizas.assignada_tot c ON b.id_cip_sec = c.id_cip_sec
SET a.hz = 1
WHERE
    (agrupador = @hz and (
    ((dosis >= 2 or (dosis = 1 and temps <= 3)) and YEAR(a.data_naix) <= 1933)
    or ((dosis >= 2 or (dosis = 1 and temps <= 3)) and YEAR(a.data_naix) between 1934 and 1942 and a.edat >= 90)
    or ((dosis >= 2 or (dosis = 1 and temps <= 3)) and YEAR(a.data_naix) >= 1942 and a.edat >= 80)
    or ((dosis >= 2 or (dosis = 1 and temps <= 3)) and YEAR(a.data_naix) >= 1957 and a.edat >= 65))
    )
    or (YEAR(a.data_naix) between 1934 and 1942 and a.edat < 90)
    or (YEAR(a.data_naix) between 1942 and 1957 and a.edat < 80)
    or (YEAR(a.data_naix) >= 1957 and a.edat < 65)
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b  ON a.id_cip_sec=b.id_cip_sec
LEFT JOIN nodrizas.assignada_tot c ON b.id_cip_sec = c.id_cip_sec
SET a.hz_vac = 1
WHERE
    agrupador = @hz and (
    ((dosis >= 2 or (dosis = 1 and temps <= 3)) and YEAR(a.data_naix) <= 1933)
    or ((dosis >= 2 or (dosis = 1 and temps <= 3)) and YEAR(a.data_naix) between 1934 and 1942 and a.edat >= 90)
    or ((dosis >= 2 or (dosis = 1 and temps <= 3)) and YEAR(a.data_naix) >= 1942 and a.edat >= 80)
    or ((dosis >= 2 or (dosis = 1 and temps <= 3)) and YEAR(a.data_naix) >= 1957 and a.edat >= 65))
;

UPDATE mst_vacunes a INNER JOIN nodrizas.dextraccio d LEFT JOIN nodrizas.eqa_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.grip = 1
WHERE
    agrupador = @grip and year(datamax) = year(d.data_ext) and month(datamax) >= 9 and month(datamax) <= 12
;

UPDATE mst_vacunes a INNER JOIN nodrizas.dextraccio d LEFT JOIN nodrizas.eqa_vacunes b  ON a.id_cip_sec=b.id_cip_sec
SET a.covid = 1
WHERE
    agrupador = @covid and year(datamax) = year(d.data_ext) and month(datamax) >= 9 and month(datamax) <= 12
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_problemes b ON a.id_cip_sec = b.id_cip_sec
SET a.m_hz = 1
WHERE
    ps in (@excl_hz1,@excl_hz2) and timestampdiff(month, dde, curdate()) <= 12
;

-- UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b on a.id_cip_sec=b.id_cip_sec
-- set a.vph = 1
-- WHERE
--     agrupador = @vph and
--     (
--     -- ((edat <= 26 and dosis >= 2 and sexe = 'D') OR
--     (edat <= 26 and dosis >= 1 and sexe = 'D' and temps <= 7) OR
--     -- (edat <= 13 and sexe = 'H' and dosis >= 2) OR
--     -- (edat <= 13 and sexe = 'H' and dosis >= 1 and temps <= 7) OR
--     (edat > 26 and sexe = 'D') OR
--     (edat > 13 and sexe = 'H') OR
--     (edat <= 26 and dosis >= 2 and sexe = 'D' and timestampdiff(month, datamin, datamax) >= 5)
--     )
-- ;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b on a.id_cip_sec=b.id_cip_sec
LEFT JOIN nodrizas.assignada_tot c on b.id_cip_sec = c.id_cip_sec
set a.vph = 1
WHERE
    (agrupador = @vph and (
    (a.edat < 26 and dosis >= 1 and a.sexe = 'D') OR
    (dosis >= 1 and a.sexe = 'H' and YEAR(a.data_naix) >= 2011 and a.edat >= 11)
    ))
    OR (a.edat >= 26 and a.sexe = 'D')
    OR (a.sexe = 'H' and YEAR(a.data_naix) >= 2011 and a.edat < 11)
    OR (a.sexe = 'H' and YEAR(a.data_naix) < 2011)
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b ON a.id_cip_sec=b.id_cip_sec
set a.polio = 1
WHERE
    agrupador = @polio and
    ((maxedat >= 48 and dosis >= 3) OR
    (maxedat < 48 and dosis >= 4) OR
    (dosis >= 1 and temps <= 3) OR
    (dosis >= 2 and temps <= 7))
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b ON a.id_cip_sec=b.id_cip_sec
set a.vhb = 1
WHERE
    agrupador = @vhb and
    ((dosis >= 3) OR
    (dosis >= 1 and temps <= 3) OR
    (dosis >= 2 and temps <= 7))
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_problemes b ON a.id_cip_sec=b.id_cip_sec
set a.vhb = 1
WHERE
    ps in (13,14,994)
;
UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_problemes b ON a.id_cip_sec=b.id_cip_sec
set a.vha = 1
WHERE
    ps in (32,33)
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b ON a.id_cip_sec=b.id_cip_sec
set a.vha = 1
WHERE
    agrupador = @vhb and
    ((dosis >= 2) OR
    (dosis >= 1 and temps <= 7))
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b ON a.id_cip_sec=b.id_cip_sec
set a.macwy = 1
WHERE
    agrupador in (@macwy_1, @macwy_2) and dosis >= 1 and edat >= 10
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b ON a.id_cip_sec=b.id_cip_sec
set a.xarampio = 1
WHERE
    agrupador = @xrp and(
    (dosis >= 2) OR
    (dosis >= 1 and temps <= 2))
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_problemes b ON a.id_cip_sec=b.id_cip_sec
set a.xarampio = 1
WHERE
    ps in (108,109,516)
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b ON a.id_cip_sec=b.id_cip_sec
set a.rubeola = 1
WHERE
    agrupador = @rub and(
    (dosis >= 2) OR
    (dosis >= 1 and temps <= 2))
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_problemes b ON a.id_cip_sec=b.id_cip_sec
set a.rubeola = 1
WHERE
    ps in (110,111,517)
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b ON a.id_cip_sec=b.id_cip_sec
set a.parotiditis = 1
WHERE
    agrupador = @par and(
    (dosis >= 2) OR
    (dosis >= 1 and temps <= 2))
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_problemes b ON a.id_cip_sec=b.id_cip_sec
set a.parotiditis = 1
WHERE
    ps in (112,113,518)
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_vacunes b ON a.id_cip_sec=b.id_cip_sec
set a.vrc = 1
WHERE
    agrupador = @vrc and (dosis >= 2 or (dosis >=1 and temps <= 2))
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_problemes b ON a.id_cip_sec=b.id_cip_sec
set a.vrc = 1
WHERE
    ps in (317,318,467) or m_hz = 1
;

-- UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_immunitzats b ON a.id_cip_sec=b.id_cip_sec
-- set a.vrc = 1
-- WHERE
--     agrupador = 467 or m_hz = 1
-- ;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_problemes b ON a.id_cip_sec=b.id_cip_sec
set a.EXCL_XRP_VRC = 1
WHERE
    ps in (@quimio, @tp_1, @tp_2, @tp_3, @ims_1, @ims_2)
;

UPDATE mst_vacunes a LEFT JOIN nodrizas.eqa_problemes b ON a.id_cip_sec=b.id_cip_sec
set a.emb = 1
WHERE
    ps in (@emb_1, @emb_2)
;

