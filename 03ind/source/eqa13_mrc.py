# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict, Counter
import csv
import os
import sys
from time import strftime
import datetime as d
from dateutil import relativedelta as rd

DEXTD = getOne('select data_ext from dextraccio', 'nodrizas')[0]
menys1 = DEXTD - rd.relativedelta(years=1)

printTime('Inici')

debug = False

db = 'eqa_ind'
nod = 'nodrizas'
FG = 30
QAC = 513999  # eliminar 999 si realment es vol introduir QAC
variables = 'eqa_variables'
tipusInd = 13

OutFile = tempFolder + 'adequacioMRC.txt'


assig = {}
sql = 'select id_cip_sec,up,uba,upinf,ubainf,edat,ates,trasplantat,sexe,seccio_censal,up_residencia,institucionalitzat,maca from assignada_tot'
for id_cip_sec, up, uba, upinf, ubainf, edat, ates, trasplantat,sexe, seccio_censal, up_residencia, institucionalitzat, maca in getAll(sql, db):
    assig[(id_cip_sec)] = {'up': up, 'uba': uba, 'upinf': upinf, 'ubainf': ubainf, 'edat': edat, 'ates': ates, 'trasplantat': trasplantat, 'sexe': sexe, 'seccio_censal': seccio_censal, 'up_residencia': up_residencia, 'institucionalitzat': institucionalitzat, 'maca': maca}

# EQD024708
MDRDp = {}
sql = "select id_cip_sec, agrupador, data_var, valor, usar from {0} where agrupador in ({1}, {2}) and usar < 3 {3}".format(variables, FG, QAC, ' limit 10' if debug else '')
for id, agr, data_var, valor, usar in getAll(sql, nod):
    if (agr == FG and 0 < valor < 59.99) or (agr == QAC and valor > 30):
        key = (id, agr)
        if key in MDRDp:
            data = MDRDp[key]['data']
            dies = daysBetween(data_var, data)
            if (agr == FG and abs(dies) > 90) or (agr == QAC and abs(dies) > 180):
                usar1 = MDRDp[key]['usar']
                difusar = usar - usar1
                if abs(difusar) == 1:
                    MDRDp[key]['num'] += 1
                    MDRDp[key]['data'] = data_var
                    MDRDp[key]['usar'] = usar
                else:
                    num = MDRDp[key]['num']
                    if num > 1:
                        continue
                    else:
                        MDRDp[key]['data'] = data_var
                        MDRDp[key]['usar'] = usar
        else:
            MDRDp[key] = {'data': data_var, 'num': 1, 'usar': usar, 'numerador': 0}

MDRD = {}
for (id, agr), dades in MDRDp.items():
    if agr == FG:
        MDRD[id] = {'num': max([dades['num'], MDRDp[(id, QAC)]['num'] if (id, QAC) in MDRDp else 0]), 'numerador': 0}

eqd09 = defaultdict(set)
sql = """
    SELECT id_cip_sec
    FROM eqa_problemes
    WHERE ps = 53
"""
for id, in getAll(sql, 'nodrizas'):
        eqd09['mrc'].add(id)

estadis =  {
    'C01-N18.31': '3a',
    'C01-N18.32': '3b',
    'C01-N18.4': '4',
    'C01-N18.5': '5'
}
sql = """
    SELECT id_cip_sec, pr_cod_ps
    FROM {tb}
    WHERE pr_dde >= DATE '{menys1}'
    AND pr_cod_ps in {probs}
    AND (pr_dba = '' OR pr_dba is null)
"""
for tb in getSubTables('problemes'):
    for id, ps in getAll(sql.format(tb=tb, menys1=menys1, probs=tuple(estadis.keys())), 'import'):
        eqd09[estadis[ps]].add(id)
    printTime(tb)

estadis_var = {
    4: '4',
    5: '5',
    6: '3a',
    7: '3b'
}
sql = """
    SELECT id_cip_sec, vu_val
    FROM variables1
    WHERE vu_cod_vs = 'VU2000'
"""
for id, val in getAll(sql, 'import'):
    if val in estadis_var:
        eqd09[estadis_var[val]].add(id)

sql = """
    SELECT id_cip_sec, valor
    FROM eqa_variables
    WHERE agrupador = 30
    AND usar = 1
    AND data_var >= DATE '{menys1}'
""".format(menys1=menys1)
fge = {}
for id, valor in getAll(sql, 'nodrizas'):
    fge[id] = valor

indicadors = []
sql = 'select ind_codi from eqa_ind where tipus = {}'.format(tipusInd)
for ind_codi, in getAll(sql, db):
    indicadors.append(ind_codi)

eqa0250 = []
for indica in indicadors:
    tableMy = indica + '_ind2'
    if indica == 'EQD024708':
        sql = "select taula, agrupador, data_min, data_max from eqa_relacio where ind_codi in ('{}')".format(indica)
        for taula, agr, dmin, dmax in getAll(sql, nod):
            sql = "select date_add(date_add(data_ext,interval - {0} month),interval + 1 day),date_add(data_ext,interval - {1} month) from dextraccio".format(dmin, dmax)
            for fat, ar in getAll(sql, nod):
                fatemps = fat
                ara = ar
            if taula == 'problemes':
                sql = "select id_cip_sec from eqa_problemes where ps={0} and dde between '{1}' and '{2}' {3}".format(agr, fatemps, ara, ' limit 10' if debug else '')
                for id, in getAll(sql, nod):
                    if (id) in MDRD:
                        MDRD[(id)]['numerador'] = 1


        with openCSV(OutFile) as c:
            for (id), d in MDRD.items():
                n = d['num']
                numerador = d['numerador']
                if n > 1:
                    try:
                        up = assig[(id)]['up']
                    except KeyError:
                        continue
                    c.writerow([id, assig[id]['up'], assig[id]['uba'], assig[id]['upinf'], assig[id]['ubainf'], assig[id]['edat'], assig[id]['ates'], assig[id]['trasplantat'],assig[id]['sexe'], assig[id]['seccio_censal'], assig[id]['up_residencia'], assig[id]['institucionalitzat'], assig[id]['maca'], indica, numerador, 0, 0, 0])
    elif indica == 'EQA0252A':
        for id in eqd09['mrc']:
            if id in assig:
                num = 0
                if id in eqd09['3a'] and id in fge and 45 <= fge[id] <= 59:
                    num = 1
                elif id in eqd09['3b'] and id in fge and 30 <= fge[id] <= 44:
                    num = 1
                elif id in eqd09['4'] and id in fge and 15 <= fge[id] <= 29:
                    num = 1
                elif id in eqd09['5'] and id in fge and fge[id] < 15:
                    num = 1
                try:
                    up = assig[(id)]['up']
                except KeyError:
                    continue
                eqa0250.append(([id, assig[id]['up'], assig[id]['uba'], assig[id]['upinf'], assig[id]['ubainf'], assig[id]['edat'], assig[id]['ates'], assig[id]['trasplantat'],assig[id]['sexe'], assig[id]['seccio_censal'], assig[id]['up_residencia'], assig[id]['institucionalitzat'], assig[id]['maca'], indica, num, 0, 0, 0]))
                
    execute("drop table if exists {}".format(tableMy), db)
    execute("create table {} (id_cip_sec int, up varchar(5) not null default'', uba varchar(7) not null default'', upinf varchar(5) not null default'', ubainf varchar(7) not null default'',\
        edat double, ates double, trasplantat double, sexe varchar(7) not null default'', seccio_censal varchar(10) not null default'', up_residencia varchar(13) not null default'', institucionalitzat double, \
        maca double, ind_codi varchar(10) not null default'', num double, excl double, ci double, clin double, index(id_cip_sec))".format(tableMy), db)
    if indica == 'EQD024708':
        loadData(OutFile, tableMy, db)
    elif indica == 'EQA0252A':
        listToTable(eqa0250, tableMy, db)

printTime('Fi')

# EQA0250 i EQA0251
sql = """
    SELECT id_cip_sec
    FROM eqa_problemes
    WHERE ps = 1093
"""
mrc = set()
for id, in getAll(sql, 'nodrizas'):
    if id in assig:
        mrc.add(id)

sql = """
    SELECT id_cip_sec, data_var
    FROM eqa_variables
    WHERE ((agrupador = 30 and valor < 60)
    OR (agrupador = 513 and valor > 30))
    AND data_var >= DATE '{menys1}'
""".format(menys1=menys1)
registres = defaultdict(set)
for id, data in getAll(sql, 'nodrizas'):
    if id in assig:
        registres[id].add(data)

entren = set()
for id, dates in registres.items():
    matching = {data for data in dates for data2 in dates if abs((data - data2).days) > 90}
    if len(matching) > 0:
        entren.add(id)

inds = {
    'EQA0250A': {
        'NUM': entren,
        'DEN': mrc
    },
    'EQA0251A': {
        'NUM': mrc,
        'DEN': entren
    }
}
for ind in inds:
    upload = []
    num = inds[ind]['NUM']
    den = inds[ind]['DEN']
    for id in den:
        if id in assig:
            numerador = 0
            if id in num:
                numerador = 1
            upload.append((id, assig[id]['up'], assig[id]['uba'], assig[id]['upinf'], assig[id]['ubainf'], assig[id]['edat'], assig[id]['ates'], assig[id]['trasplantat'],assig[id]['sexe'], assig[id]['seccio_censal'], assig[id]['up_residencia'], assig[id]['institucionalitzat'], assig[id]['maca'], ind, numerador, 0, 0, 0))
    tableMy = ind + '_ind2'
    execute("drop table if exists {}".format(tableMy), db)
    execute("create table {} (id_cip_sec int, up varchar(5) not null default'', uba varchar(7) not null default'', upinf varchar(5) not null default'', ubainf varchar(7) not null default'',\
        edat double, ates double, trasplantat double, sexe varchar(7) not null default'', seccio_censal varchar(10) not null default'', up_residencia varchar(13) not null default'', institucionalitzat double, \
        maca double, ind_codi varchar(10) not null default'', num double, excl double, ci double, clin double, index(id_cip_sec))".format(tableMy), db)
    listToTable(upload, tableMy, db)