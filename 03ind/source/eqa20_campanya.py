import collections as c

import sisaptools as u

sql = "select id_cip_sec, if(motiu = 3, 'EQA0503A', 'EQA0504A'), vacunat, \
              0, 0, if(vacunat + vacunable = 0, 1, 0) \
       from covid \
       union all \
       select id_cip_sec, if(motiu = 3, 'EQA0503A', 'EQA0504A'), vacunat, \
              0, 0, if(vacunat + vacunable = 0, 1, 0) \
       from import_jail.covid \
       union all \
       select id_cip_sec, if(edat = 3, 'EQA0501A', 'EQA0502A'), vacunat, \
              0, 0, 0 \
       from vag \
       union all \
       select id_cip_sec, if(edat = 3, 'EQA0501A', 'EQA0502A'), vacunat, \
              0, 0, 0 \
       from import_jail.vag"
indicadors = c.defaultdict(list)
for row in u.Database("p2262", "import").get_all(sql):
    indicadors[row[0]].append(row[1:])

dades = c.defaultdict(list)
sql = "select id_cip_sec, up, uba, upinf, ubainf, edat, sexe, seccio_censal, \
              up_residencia, institucionalitzat, maca, ates, trasplantat \
       from eqa_ind.assignada_tot"
for row in u.Database("p2262", "import").get_all(sql):
    if row[0] in indicadors:
       for fila in indicadors[row[0]]:
           dades[fila[0]].append(row + fila)

cols = ("id_cip_sec int", "up varchar(5)", "uba varchar(5)",
        "upinf varchar(5)", "ubainf varchar(5)", "edat int", "sexe varchar(1)",
        "seccio_censal varchar(10)", "up_residencia varchar(13)",
        "institucionalitzat int", "maca int", "ates int", "trasplantat int",
        "ind_codi varchar(10)", "num int", "excl int", "ci int", "clin int")
with u.Database("p2262", "eqa_ind") as p2262:
    for indicador, upload in dades.items():
        taula = indicador + "_ind2"
        p2262.create_table(taula, cols, remove=True)
        p2262.list_to_table(upload, taula)
