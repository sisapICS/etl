# -*- coding: utf-8 -*-

"""
.
"""

import multiprocessing as m
import collections as c
import sisaptools as u
import sisapUtils as uu
import math
import csv
import os

ES_BASAL = False  # activar per recalcular prevalences

CONNEXIO = ("p2262", "eqa_ind")
CONNEXIO_exadata = ('exadata', 'data')
MASTER = "mst_indicadors_pacient"
ECAP_ENTREN = "mst_ups_jail"
ECAP_UP = "exp_ecap_up_jail"
ECAP_CATALEG = "exp_ecap_cataleg_jail"
ECAP_CATALEG_PARE = "exp_ecap_cataleg_pare_jail"

DETECCIO = {"EQD0913": ("MCV", ("EQD023802", "EQD0204A", "EQA0204B", "EQA0204C", "EQA0201A")),  # noqa
            "EQD0914": ("FA", ("EQA0201A",)),
            "EQD0915": ("DM2", ("EQA0209A",)),
            "EQD0901": ("HTA", ("EQA0213A", "EQA0213B")),
            "EQD0916": ("MPOC", ("EQD024206",)),
            "EQD0917": ("Asma", ("EQD024205",)),
            "EQD0902": ("Aguda ORL", ("EQA0226A", "EQA0226B", "EQA0231A")),
            "EQD0903": ("Aguda respirat�ria", ("EQA0232A",)),
            "EQD0904": ("Aguda digestiva", ("EQA0229A",)),
            "EQD0905": ("Aguda urin�ria", ("EQA0230A", "EQA0230B", "EQA0230C")),  # noqa
            "EQD0906": ("Altres", ("EQA0219A", "EQA0227A", "EQA0235A")),
            "EQD0907": ("Obesitat", ("EQA0303A",)),
            "EQD0908": ("ATDOM", ("EQA0401A",))}

this = os.path.dirname(os.path.realpath(__file__))
that = "/prevalences.txt"
deteccio_file = this.replace("source", "dades_noesb") + that
that = "/eqaeapp_metesres_subind_anual.txt"
metes_file = this.replace("source", "dades_noesb") + that
that = "/eqaeapp_ponderacio_anual.txt"
ponderacio_file = this.replace("source", "dades_noesb") + that


def create_metes_ponderacions():
    cols = """(indicador varchar(8), 
                z1 varchar(10), 
                z2 varchar(10), 
                meta varchar(10), 
                z3 varchar(10), 
                z4 varchar(10), 
                z5 varchar(10), 
                valor double)"""
    uu.createTable('eqa_preso_metes_subind_a', cols, 'eqa_ind', rm=True)
    upload = []
    for ind, z1, z2, meta, z3, z4, z5, valor in csv.reader(open(metes_file), delimiter="{"):  # noqa
        if z4 == "NOINSAT":
            upload.append((ind, z1, z2, meta, z3, z4, z5, valor))
    uu.listToTable(upload, 'eqa_preso_metes_subind_a', 'eqa_ind')

    cols = """(indicador varchar(10), 
                z1 varchar(10), 
                z2 varchar(10), 
                z3 varchar(10),
                tipus varchar(10), 
                z4 varchar(10), 
                z5 varchar(10), 
                valor double)"""
    uu.createTable('eqa_preso_ponderacio_anual', cols, 'eqa_ind', rm=True)
    upload = []
    for ind, z1, z2, z3, tipus, z4, z5, valor in csv.reader(open(ponderacio_file), delimiter="{"):  # noqa
        if valor < 0: valor = 0
        upload.append((ind, z1, z2, z3, tipus, z4, z5, valor))
    uu.listToTable(upload, 'eqa_preso_ponderacio_anual', 'eqa_ind')


def _get_ecap_up(sql):
        """."""
        return(list(u.Database(*CONNEXIO).get_all(sql)))

def get_ecap_up():
    """."""
    sql = "select withMaca_ind from nodrizas.dextraccio"
    with_maca = u.Database(*CONNEXIO).get_one(sql)[0]
    # detecci�
    deteccio_inv = c.defaultdict(list)
    for nou, (literal, antics) in DETECCIO.items():
        for antic in antics:
            deteccio_inv[antic].append(nou)
    # ups
    sql = """select up, a.edat, khalix,
            if(sexe='H', 'HOME', 'DONA'), '2U'
            from nodrizas.jail_assignada a
            inner join nodrizas.jail_centres b
            on a.up = b.scs_codi
            inner join nodrizas.khx_edats5a c on a.edat = c.edat
            where ates = 1 and institucionalitzat = 0 and maca = 0"""
    ups = c.Counter()
    poblacio = c.defaultdict(c.Counter)
    for up, edat, klx, sex, med in u.Database(*CONNEXIO).get_all(sql):  # noqa
        poblacio[(klx, sex, med)][(up, 'M')] += 1
        if edat >= 20:
            ups[(up, 'M')] += 1
    entren = set([k for k, v in ups.items() if v >= 50])
    cols = ("up varchar(5)", "tipus varchar(1)")
    with u.Database(*CONNEXIO) as conn:
        conn.create_table(ECAP_ENTREN, cols, remove=True)
        conn.list_to_table(entren, ECAP_ENTREN)
        conn.execute("alter table {} add unique (up, tipus)".format(ECAP_ENTREN))  # noqa
    # par�metres
    sql = "select distinct grup_codi from eqa_ind where invers = 1 \
            union all select distinct ind_codi from eqa_ind \
                        where nets = 1 and invers = 1"
    inversos = set([cod for cod, in u.Database(*CONNEXIO).get_all(sql)])
    sql = "select indicador, substr(meta, 4, 3), valor / 100 \
            from eqa_preso_metes_subind_a \
            where z4 = 'NOINSAT' and meta not like '%INT%'"
    metes = {row[:2]: row[2] for row in u.Database(*CONNEXIO).get_all(sql)}
    sql = "select indicador, 'M', z2, valor \
            from eqa_preso_ponderacio_anual where valor > 0"
    ponderacio = {row[:3]: row[3] for row
                    in u.Database(*CONNEXIO).get_all(sql)}
    # conversi� ups brs
    sql = """SELECT UP_COD, BR FROM DWSISAP.DBC_CENTRES_TOTS"""
    br_up = {row[0]: row[1] for row
                    in u.Database(*CONNEXIO_exadata).get_all(sql)}

    # captura
    sql = """select a.up, a.ind_codi, a.grup_codi, nets,
                    sum(den), sum(num)
                from {} partition({}) a
                inner join eqa_ind b on a.ind_codi = b.ind_codi
                where ates = 1 and excl_edat = 0 and
                    id_cip_sec < 0 and
                    institucionalitzat = 0 and excl = 0 and ci = 0 and
                    clin = 0 and (maca = 0 or a.grup_codi in {})
                group by a.up, a.ind_codi, a.grup_codi,
                        nets"""
    parts = u.Database(*CONNEXIO).get_table_partitions(MASTER)
    jobs = []
    for k, v in sorted(parts.items(), key=lambda x: x[0], reverse=True):
        this = sql.format(MASTER, k, with_maca)
        jobs.append(this)
    sql_extra = """select A.up, A.indicador, substr(A.indicador,1,7) as grp, NULL as net, B.DEN, A.NUM
                    FROM(
                    select up, indicador, sum(n) as NUM
                    from eqa_ind.exp_khalix_up_ind
                    where indicador in ('EQD2001A', 'EQD2002A', 'EQA2000A')
                    and comb = 'NOINSAT'
                    and conc = 'NUM'
                    group by indicador, up) A
                    inner JOIN
                    (select up, indicador, sum(n) as DEN
                    from eqa_ind.exp_khalix_up_ind
                    where indicador in ('EQD2001A', 'EQD2002A', 'EQA2000A')
                    and comb = 'NOINSAT'
                    and conc = 'DEN'
                    group by indicador, up) B
                    ON A.UP = B.UP and A.INDICADOR = B.INDICADOR
                    where a.up in (select up from eqa_ind.mst_ups_jail)"""
    jobs.append(sql_extra)
    pool = m.Pool(8)
    results = pool.map(_get_ecap_up, jobs)
    pool.close()
    denominador = c.Counter()
    numerador = c.Counter()
    indicadors = set()
    deteccio = set()
    for chunk in results:
        for up, ind, grp, net, den, num in chunk:
            key = (up, 'M')
            if key in entren:
                for codi in (ind, grp) if net else (grp,):
                    indicadors.add(codi)
                    denominador[key + (codi,)] += int(den)
                    numerador[key + (codi,)] += int(num)
                for nou in deteccio_inv[ind]:
                    indicadors.add(nou)
                    deteccio.add(nou)
                    numerador[key + (nou,)] += int(den)
    # esperats
    for ind, medea, edat, pob, sex, prev in csv.reader(open(deteccio_file), delimiter="{"):  # noqa
        if pob == "NOINSAT":
            ups = poblacio[(edat, sex, medea)]
            for key, n in ups.items():
                if key in entren:
                    denominador[key + (ind,)] += n * float(prev)
    # c�lcul
    upload = []
    for up, tipus in entren:
        for ind in indicadors:
            key = (up, tipus, ind)
            es_invers = ind in inversos
            es_deteccio = ind in deteccio
            mmax = metes.get((ind, "MAX"), 0)
            mmin = min(metes.get((ind, "MIN"), 0), mmax * 0.9)
            br = br_up[up] if up in br_up else ''
            if (ind, tipus, br) in ponderacio:
                pond = ponderacio[(ind, tipus, br)]
            else:
                pond = 0
            detectats = denominador[key]
            resolts = numerador[key]
            resultat = (0 if not detectats
                        else 1 if resolts > detectats
                        else resolts / float(detectats))
            llistat = (0 if es_deteccio
                        else resolts if es_invers
                        else detectats - resolts)
            if mmin:
                if es_invers:
                    mmin_p = math.ceil(mmin * detectats)
                    mmax_p = math.ceil(mmax * detectats)
                    if mmin_p == mmax_p:
                        mmax_p += 1
                else:
                    mmin_p = math.floor(mmin * detectats)
                    mmax_p = math.floor(mmax * detectats)
                    if mmin_p == mmax_p and mmax_p:
                        mmin_p -= 1
                assol = (1 if resolts >= mmax_p
                            else 0 if resolts <= mmin_p
                            else (resolts - mmin_p) / (mmax_p - mmin_p))
                if es_invers:
                    assol = 1 - assol
                punts = assol * pond
                if detectats:
                    mmin_perc = mmin_p / detectats
                    mmax_perc = mmax_p / detectats
                else:
                    mmin_perc = 0
                    mmax_perc = 0
            else:
                mmin_p, mmax_p, assol, punts, mmin_perc, mmax_perc = (0, 0, 1, 0, 0, 0)  # noqa
            this = (up, tipus, ind, 0, detectats, resolts, 0, 0,
                    resultat, resultat, assol, punts, llistat, assol,
                    1 * es_invers, mmin_perc, mmax_perc)
            upload.append(this)
    print('len upload:', len(upload))
    cols = ("up varchar(5)", "tipus varchar(1)",
            "indicador varchar(10)", "esperats int", "detectats double",
            "resolts int", "deteccio int", "deteccioPerResultat int",
            "resolucio double", "resultat double",
            "resultatPerPunts double", "punts double", "llistat int",
            "resolucioPerPunts double", "invers int", "mmin_p double",
            "mmax_p double")
    with u.Database(*CONNEXIO) as conn:
        conn.create_table(ECAP_UP, cols, remove=True)
        conn.list_to_table(upload, ECAP_UP)

def get_catalegs():
    upload_pare = []
    sql = """SELECT pare, literal, ordre, lcurt, proces FROM eqacatalegPare WHERE dataany = (SELECT max(dataany) FROM eqacatalegPare)"""
    for row in uu.getAll(sql, 'pdp'):
        upload_pare.append(row)
    upload_pare.append(('EQAG20', 'Malalties virals', 14, 'No EQA'))
    cols = """(PARE varchar(80), literal VARCHAR(80), ordre int, lcurt VARCHAR(80), proces VARCHAR(50))"""
    uu.createTable(ECAP_CATALEG_PARE, cols, 'eqa_ind', rm=True)
    uu.listToTable(upload_pare, ECAP_CATALEG_PARE, 'eqa_ind')

    upload = []
    sql = """SELECT indicador, literal, ordre, pare, llistat, invers, mdet, mmin, mint, mmax, toshow, wiki, lcurt, rsomin, rsomax, pantalla FROM eqacataleg WHERE dataany = (SELECT max(dataany) FROM eqacataleg)"""
    for row in uu.getAll(sql, 'pdp'):
        upload.append(row)
    upload.append(('EQA2000','Maneig inicial del pacient VIH+',1,'EQAG20',1,0,0,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/EQA2000','',0,0,'GENERAL'))
    upload.append(('EQD2001','Especificitat diagn�stica de les hepatitis v�riques',2,'EQAG20',1,0,0,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/EQD2001','',0,0,'GENERAL'))
    upload.append(('EQD2002','Concordan�a del tractament antiviral',3,'EQAG20',1,0,0,0,0,0,1,'http://sisap-umi.eines.portalics/indicador/codi/EQD2002','',0,0,'GENERAL'))
    cols = """(INDICADOR varchar(10), literal VARCHAR(80), ordre int, pare VARCHAR(80), llistat int,
                invers int, mdet double, mmin double, mint double, mmax double, toshow int,
                wiki VARCHAR(250), lcurt VARCHAR(80), rsomin double, rsomax double, pantalla VARCHAR(50))"""
    uu.createTable(ECAP_CATALEG, cols, 'eqa_ind', rm=True)
    uu.listToTable(upload, ECAP_CATALEG, 'eqa_ind')


if __name__ == "__main__":
    create_metes_ponderacions()
    get_ecap_up()
    get_catalegs()