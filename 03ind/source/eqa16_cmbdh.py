# coding: iso-8859-1
import sisapUtils as u
from collections import defaultdict,Counter
from datetime import datetime, timedelta

nod = 'nodrizas'

eqa = 'eqa_ind'

criteris_khalix = 'ates = 1 and institucionalitzat = 0 and excl = 0 and den = 1'

def get_data_ext():
    sql = 'select data_ext from dextraccio'
    dext,= u.getOne(sql, nod)
    return dext

def get_problemes():
    print('problems')
    """
    EQA3008: ICC : 21  // 625-626-627 ingres_icc 
    EQA3005: FA : 709 180 //  619, 620, 622 trombo
    EQA3006: FA : 709 180 // 621, 624 hemorragia
    EQA3213: FA : 709 180 // 625-626-627 ingres_icc
    EQA3003: CI/AVC : 1, 7 // 1,7 -> hospital 622, 939
    EQA3004: sense CI/AVC : 1, 7  // 619, 620, 621, 622, 623 :: hospital 622, 939
    EQAXXX: DM2 : 18
    """
    problems = (1,7,709,180,21,18)
    ci_avc = {}
    icc = {}
    fa = {}
    dm2 = {}
    sql = 'select id_cip_sec, ps, dde from eqa_problemes where ps in {}'.format(problems)
    print(sql)
    for id, ps, dde in u.getAll(sql, nod):
        if ps == 21:
            icc[id] = dde
        if ps in (1, 7):
            ci_avc[id] = dde
        if ps in (709, 180):
            fa[id] = dde
        if ps == 18:
            dm2[id] = dde
    return icc, ci_avc, fa, dm2


def get_cmbdh():
    print('cmbdh')
    ingres_icc = {}
    ingres_trombo = {}
    ingres_hemorragia = {}
    ingres_ci_avc = {}
    ingres_amputacio = {}
    ingres_descompensacio_diabetica = {}
    agrupadors = (619, 620, 621, 622, 623, 624, 625, 626, 627, 932, 935, 936)
    sql = """select id_cip_sec, agr, dat from eqa_ingressos_EX, dextraccio 
                where agr in {} and 
                dat between date_add(date_add(data_ext,interval - 12 month),interval + 1 day) 
                and data_ext order by dat asc""".format(agrupadors)
    print(sql)
    for id, ps, dat in u.getAll(sql, nod):
        if ps in (625,626,627):
            ingres_icc[id] = dat
        elif ps in (619, 620, 622):
            ingres_trombo[id] = dat
        elif ps in (621, 624):
            ingres_hemorragia[id] = dat
        elif ps in (622, 939):
            ingres_ci_avc[id] = dat
        elif ps == 936:
            ingres_amputacio[id] = dat
        elif ps in (932, 935):
            ingres_descompensacio_diabetica[id] = dat

    return ingres_icc, ingres_trombo, ingres_hemorragia, ingres_ci_avc, ingres_amputacio, ingres_descompensacio_diabetica

dext = get_data_ext()
icc, ci_avc, fa, dm2 = get_problemes()
ingres_icc, ingres_trombo, ingres_hemorragia, ingres_ci_avc, ingres_amputacio, ingres_descompensacio_diabetica = get_cmbdh()

print('assigned population')
population = {}
sql = """select id_cip_sec, up, uba, upinf, ubainf, edat, sexe, 
            ates_novc, trasplantat, seccio_censal, up_residencia, 
            institucionalitzat, maca from assignada_tot_with_jail 
            where edat>14"""
for id, up, uba, upinf, ubainf, edat, sexe, ates, trasplantat, seccio_censal, up_residencia, insti, maca in u.getAll(sql, nod):
    population[id] = [up, uba, upinf, ubainf, edat, sexe, ates, trasplantat, seccio_censal, up_residencia, insti, maca]

print('calculating w/ assigned population')
upload_3008A, upload_3005A, upload_3003A, upload_3004A, upload_3006A = [], [], [], [], []
upload_3213 = []
upload_3008pob = []
upload_3005pob = []
upload_3006pob = []
upload_3213pob = []
upload_3003pob = []
upload_3004pob = []
upload_31xx_1 = []
upload_31xx_2 = []
upload_31xx_3 = []
upload_31XX1 = []
upload_31XX2 = []
upload_31XX3 = []
for id, value in population.items():
    up, uba, upinf, ubainf, edat, sexe, ates, trasplantat, seccio_censal, up_residencia, insti, maca = value
    den = 1
    num = 0
    excl = 0
    datp, dati = dext, dext
    # INDICADOR 3008A EQA3008: ICC : 21  // 625-626-627 ingres_icc 
    if id in icc:
        den = 1
        dicc = icc[id]
        if id in ingres_icc:
            num = 1
        if dicc > dext - timedelta(days=365):
            excl = 1
        upload_3008A.append([id, 'EQA3008A', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR 3008A EQA3008_POB
    den = 1
    num = 0
    if id in icc:
        dicc = icc[id]
        if id in ingres_icc:
            if dicc < ingres_icc[id]:
                num = 1
    upload_3008pob.append([id, 'EQA3008pob', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR EQA3005: FA : 709 180 //  619, 620, 622 trombo
    if id in fa:
        den = 1
        if id in ingres_trombo:
            num = 1
        upload_3005A.append([id, 'EQA3005A', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR 3005A EQA3005_pob
    den = 1
    num = 0
    if id in fa:
        dfa = fa[id]
        if id in ingres_trombo:
            if dfa < ingres_trombo[id]:
                num = 1
    upload_3005pob.append([id, 'EQA3005pob', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR EQA3006: FA : 709 180 // 621, 624 hemorragia
    if id in fa:
        den = 1
        dfa = fa[id]
        if id in ingres_hemorragia:
            num = 1
        if dfa > dext - timedelta(days=365):
            excl = 1
        upload_3006A.append([id, 'EQA3006A', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR 3006A EQA3006_pob
    den = 1
    num = 0
    if id in fa:
        dfa = fa[id]
        if id in ingres_hemorragia:
            if dfa < ingres_hemorragia[id]:
                num = 1
    upload_3006pob.append([id, 'EQA3006pob', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR EQA3213: FA : 709 180 // 625-626-627 ingres_icc
    if id in fa:
        den = 1
        dfa = fa[id]
        if id in ingres_icc:
            num = 1
        if dfa > dext - timedelta(days=365):
            excl = 1
        upload_3213.append([id, 'EQA3213', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR EQA3213pob:
    den = 1
    num = 0
    if id in fa:
        dfa = fa[id]
        if id in ingres_icc:
            if dfa < ingres_icc[id]:
                num = 1
    upload_3213pob.append([id, 'EQA3213pob', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR EQA3003 CI/AVC : 1, 7 // 1,7 -> hospital 622, 939
    if id in ci_avc:
        den = 1
        dci_avc = ci_avc[id]
        if id in ingres_ci_avc:
            num = 1
        if dci_avc > dext - timedelta(days=365):
            excl = 1
        upload_3003A.append([id, 'EQA3003A', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR EQA3003pob:
    den = 1
    num = 0
    if id in ci_avc:
        dci_avc = ci_avc[id]
        if id in ingres_ci_avc:
            if dci_avc < ingres_ci_avc[id]:
                num = 1
    upload_3003pob.append([id, 'EQA3003pob', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR EQA3004 sense CI/AVC : 1, 7  // 619, 620, 621, 622, 623 :: hospital 622, 939
    if id not in ci_avc:
        den = 1
        if id in ingres_ci_avc:
            num = 1
        upload_3004A.append([id, 'EQA3004A', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    elif id in ci_avc:
        den = 1
        dci_avc = ci_avc[id]
        if id in ingres_ci_avc:
            if ingres_ci_avc[id] < dci_avc:
                num = 1
        upload_3004A.append([id, 'EQA3004A', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR EQA3004pob:
    den = 1
    num = 0
    if id in ingres_ci_avc:
        if id not in ci_avc:
            num = 1
        elif id in ci_avc:
            dci_avc = ci_avc[id]
            if dci_avc > ingres_ci_avc[id]:
                num = 1
    upload_3004pob.append([id, 'EQA3004pob', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR EQA31XX_1
    den = 1
    num = 0
    if id in ingres_ci_avc:
        if id in dm2:
            ddm2 = dm2[id]
            if ddm2 < ingres_ci_avc[id]:
                num = 1
    upload_31XX1.append([id, 'EQA31XX_1', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR EQA31XX_2 ingres_amputacio
    den = 1
    num = 0
    if id in ingres_amputacio:
        if id in dm2:
            ddm2 = dm2[id]
            if ddm2 < ingres_amputacio[id]:
                num = 1
    upload_31XX2.append([id, 'EQA31XX_2', up, uba, upinf, ubainf, ates, insti, den, num, excl])
    # INDICADOR EQA31XX_3 ingres_descompensacio_diabetica
    den = 1
    num = 0
    if id in ingres_descompensacio_diabetica:
        if id in dm2:
            ddm2 = dm2[id]
            if ddm2 < ingres_descompensacio_diabetica[id]:
                num = 1
    upload_31XX3.append([id, 'EQA31XX_3', up, uba, upinf, ubainf, ates, insti, den, num, excl])


print('upload')
tables = {'EQA3004A_ind2': upload_3004A,
            'EQA3008A_ind2': upload_3008A,
            'EQA3005A_ind2': upload_3005A,
            'EQA3003A_ind2': upload_3003A,
            'EQA3006A_ind2': upload_3006A,
            'EQA3213_ind2': upload_3213,
            'EQA3004pob_ind2': upload_3004pob,
            'EQA3008pob_ind2': upload_3008pob,
            'EQA3005pob_ind2': upload_3005pob,
            'EQA3003pob_ind2': upload_3003pob,
            'EQA3006pob_ind2': upload_3006pob,
            'EQA3213pob_ind2': upload_3213pob,
            'EQA31XX_1_ind2':  upload_31XX1,
            'EQA31XX_2_ind2':  upload_31XX2,
            'EQA31XX_3_ind2':  upload_31XX3,
            }
create = """(id_cip_sec double null,ind_codi varchar(10) not null default'',
            up varchar(5) not null default'',uba varchar(7) not null default'',
            upinf varchar(5) not null default'',ubainf varchar(7) not null default'',
            ates double, institucionalitzat double, den double,num double,excl double)"""
for t in tables:
    u.createTable(t,create,eqa, rm=True)
    u.listToTable(tables[t], t, eqa)
    