# coding: iso-8859-1

import sisapUtils as u
import collections as c
import simplejson as j
from dateutil import relativedelta as rd

import prof_esc_disnea_mpoc as disnea


TB_PAC = 'mst_indicadors_pacient_mpocasm'
TB_KHALIX = 'mst_khalix_mpocasm'
TB_CAT = 'mst_cataleg_mpocasm'

NOD = "nodrizas"
DB = "eqa_ind"


class EQA_MPOC_ASMA():

    def __init__(self):
        self.get_dextraccio()
        self.create_table()
        self.get_cataleg()
        self.insert_cataleg()
        self.get_fets()
        # self.get_deteccions()  # solo queda aqui EQA3335 que es EQD0916
        self.get_seg()

    def get_dextraccio(self):
        sql = """select
                    day(data_ext), month(data_ext), year(data_ext),
                    date_add(date_add(data_ext, interval - 12 month), interval + 1 day) as past_dat,
                    data_ext
                from nodrizas.dextraccio"""
        for d, m, y, past_dat, dext in u.getAll(sql, 'nodrizas'):
            self.day = d
            self.month = m
            self.year = y
            self.dext = dext
            self.past_date = past_dat

    def create_table(self):
        for tb in (TB_PAC, TB_CAT):
            desde, hasta = '_dm2', '_mpocasm'
            orig = tb.replace(hasta, desde)
            if tb == TB_PAC:  # hago catalogo de 0 en get_cataleg()
                u.createTable(tb, 'like {}'.format(orig), DB, rm=True)

    def get_cataleg(self):
        """
        estamos en calculo sistematico, mientras comento todo con # sencillo y creo aqui la tabla de catalogo
        en create_table() se copia de _dm2
        y aqui 'get_cataleg()' le defino las columnas
        """
        file = 'dades_noesb/eqa_mpocasm.json'
        try:
            stream = open(file)
        except IOError:
            stream = open('../' + file)
        self.indicadors = j.load(stream)

    def insert_cataleg(self):
        """."""
        columns = [
            "indicador varchar(10)",
            "literal varchar(255)",
            "dimensio varchar(10)",
            "is_eqa int",
            "to_show int",
            "is_invers int",
            "has_llistat int"]
        columns = "({})".format(", ".join(columns))
        u.createTable(TB_CAT, columns, DB, rm=True)
        no_llistat = [
            "EQA3316",
            "EQA3335",
            "EQA3303",
            "EQA3304",
            "EQA3309",
            "EQA3317",
            "EQD0242",
            ]
        cataleg = []
        for codi, especificacions in self.indicadors.items():
            row = (
                codi,
                especificacions.get('literal', '').encode('latin1', 'replace'),
                especificacions['dimensio'],
                especificacions['is_eqa'],
                especificacions.get('to_show', False),
                especificacions.get('is_invers', False),
                0 if codi in no_llistat else 1 * especificacions.get('has_llistat', True))
            cataleg.append(row)
        u.listToTable(cataleg, TB_CAT, DB)
        """ # PLANTILLA
        is_eqa = 1 * ('eqa' in self.especificacions)
        cataleg = (self.codi,
                   self.especificacions.get('literal', '').encode('latin1'),
                   self.especificacions['dimensio'],
                   is_eqa,
                   9 if is_eqa else 1 - self.especificacions.get('hide', False),
                   9 if is_eqa else 1 * self.especificacions.get('invers', False),
                   9 if is_eqa else 0 if self.codi in no_llistat else 1 * self.especificacions.get('llistat', True))
        u.listToTable([cataleg], tb_cat, db)
        """

    def get_indicador(self, ind, clase, pacients, dens, nums):
        """
        recibe sets de ids de dens i nums
        busca atributos del paciente: up, uba
        prepara salida para upload a tabla de pacientes
        """
        upload_pac = []
        # contador_uba = c.defaultdict(c.Counter)
        for id, ubas in pacients.items():
            if id in dens:
                ubam, ubai = ubas
                up, uba = ubam[:2]
                upinf, ubainf = ubai[:2]
                num = 1 if id in nums else 0
                row = (id, ind, clase, up, uba, upinf, ubainf, 1, 0, 1, num, 0)
                upload_pac.append(row)
                # for ubat in ubas:
                #     contador_uba[ubat]['DEN'] += 1
                #     if id in nums:
                #         contador_uba[ubat]['NUM'] += 1
        u.listToTable(upload_pac, TB_PAC, DB)

    def get_pacients(self, years=15, years_final=100):
        """diccionari dels pacients amb id[edat] i ubas"""
        print("entro en get_pacients()")
        pacients = c.defaultdict(dict)
        sql = """
            select
                id_cip_sec,
                up, uba,
                upinf, ubainf
            from nodrizas.assignada_tot
            where
                edat between {ini} and {fi}
                and ates = 1
                and maca = 0
                and institucionalitzat = 0
            """.format(ini=years, fi=years_final)
        for id, up, uba, upinf, ubainf in u.getAll(sql, 'nodrizas'):  # noqa
            pacients[id] = ((up, uba, 'M'), (upinf, ubainf, 'I'))
        return pacients

    def get_mpoc(self):
        """Consigue los id_cip_sec de los MPOC prevalentes e incidentes"""
        sql = """
        SELECT id_cip_sec, incident FROM eqa_problemes where ps = 62
        """
        prevalents = set()
        incidents = set()
        for id, incident in u.getAll(sql, 'nodrizas'):
            prevalents.add(id)
            if incident == 1:
                incidents.add(id)
        return prevalents, incidents

    def get_exitus(self):
        """."""
        mpoc_incidents, mpoc_prevalents = self.get_mpoc()
        pacients = self.get_pacients()
        exitus = {}
        for k in mpoc_prevalents | mpoc_incidents:
            if k in pacients:
                exitus[k] = pacients[k]
        sql = "select id_cip_sec from rip_problemes where ps = 62"
        mpoc_rip = set([id for id, in u.getAll(sql, 'nodrizas')])
        for id, up, uba, ubainf in u.getAll("select id_cip_sec, up, uba, ubainf from rip_assignada", 'nodrizas'):
            if id in mpoc_rip:
                exitus[id] = ((up, uba, 'M'), (up, ubainf, 'I'))
        return exitus

    def get_eqd_fets(self):
        """
        Trae los indicadores del EQD:
        clase QDIAG, llamado desde get_fets()
        """
        # ROSER va dir: 'mirar que exactament sigui aix�'
        # LEO interpreta: preguntar si queria decir %pacientes vs %diagnostico?

        taules_eqd = [
            'eqa_ind.eqd024201_ind2',
            'eqa_ind.eqd024202_ind2',
            'eqa_ind.eqd024203_ind2',
            'eqa_ind.eqd024204_ind2',
            'eqa_ind.eqd024205_ind2',
            'eqa_ind.eqd024206_ind2',
            ]

        for taula_eqd in taules_eqd:
            upload = []
            sql = """
                select
                    id_cip_sec,
                    ind_codi, 'QDIAG',
                    up, uba,
                    upinf, ubainf,
                    ates, institucionalitzat,
                    1 as den, num, excl
                from {}""".format(taula_eqd)
            for row in u.getAll(sql, 'eqa_ind'):
                upload.append(row)

            u.listToTable(upload, TB_PAC, DB)

    def get_altres_fets(self):
        sqls = [
            """
            SELECT
                id_cip_sec,
                'EQA3301', 'RESTRAC',
                up, uba,
                up upinf, ubainf,
                ates, institucionalitzat,
                den, num, excl
            FROM mst_eqa_mpoc_provisional
            WHERE ates = 1 and institucionalitzat = 0 and excl = 0
            """,
            """
            SELECT
                id_cip_sec,
                'EQA3306', 'PREVSEC',
                up, uba,
                upinf, ubainf,
                ates, institucionalitzat,
                1 as den, num, excl
            FROM eqa0304a_ind2
            WHERE
                ates = 1 and institucionalitzat = 0 and excl = 0 and
                id_cip_sec in (
                    SELECT id_cip_sec
                    FROM nodrizas.eqa_problemes
                    WHERE ps in (62, 58, 100)
                    )
            """,
            """
            SELECT
                id,
                'EQA3322', 'SEGUIMENT',
                up, uba,
                upinf, ubainf,
                1 as ates, 0 as institucionalitzat,
                1 as den, Imepoc as num, 0 as excl
            FROM
                mst_esc_disnea_mpoc
            """,
            # """
            # SELECT
            #     id_cip_sec,
            #     'EQA3308', 'PREVPRIM',
            #     up, uba,
            #     up, ubainf,
            #     ates, institucionalitzat,
            #     den, num, excl
            # FROM catsalut.indicador_iap11bis
            # """,
            ]
        for sql in sqls:
            upload = []
            for row in u.getAll(sql, 'eqa_ind'):
                upload.append(row)
            u.listToTable(upload, TB_PAC, DB)

    def get_fets(self):
        self.get_eqd_fets()  # 6/6: EQD0242[01] �� EQD0242[06]
        self.get_altres_fets()  # 4/4: EQA3301, EQA3306<-EQA0304f, EQA3308<-IAP11BIS, EQA3322  # noqa

    def get_deteccions(self):  # 3/3 (0/2)
        # EQA3335, sale de deteccion, que salio del EQA, no necesito listado
        # resolver nivel UP para todos los indicadores y a�adir este alli!
        upload = []
        sql = """
        select
            up,
            uba,
            tipus,
            case
                when indicador = 'EQD0916' then 'EQA3335'
                else indicador
            end as indicador,
            'QDIAG' as clase,
            resolts as num,
            detectats as den
        from eqa_ind.exp_ecap_uba
        where indicador like 'EQD0916'
        """
        for row in u.getAll(sql, 'eqa_ind'):
            upload.append(row)
        # u.listToTable(upload, TB_PAC, DB)  # NO VA PARA LISTADO

        # falta documentar estos otros de cualitat diagnostica:
        #  EQA3303	Incid�ncia MPOC en >= 40 anys
        #  EQA3304	Prevalen�a MPOC en >= 40 anys
        upload = []
        clase = 'QDIAG'
        contador_prev = c.defaultdict(c.Counter)
        contador_inc = c.defaultdict(c.Counter)
        pacients = self.get_pacients(40)
        mpoc_incidents, mpoc_prevalents = self.get_mpoc()
        for id, ubas in pacients.items():
            for uba in ubas:
                contador_prev[uba]['DEN'] += 1
                contador_inc[uba]['DEN'] += 1
                if id in mpoc_prevalents:
                    contador_prev[uba]['NUM'] += 1
                if id in mpoc_incidents:
                    contador_inc[uba]['NUM'] += 1
        for k, v in contador_prev.items():
            upload.append(
                tuple(list(k) + list(('EQA3304', clase, v['NUM'], v['DEN'])))
                )
        for k, v in contador_inc.items():
            upload.append(
                tuple(list(k) + list(('EQA3303', clase, v['NUM'], v['DEN'])))
                )
        # u.listToTable(upload, TB_PAC, DB)  # NO VA PARA LISTADO

        # Pendent de definicions:
        #  EQA3302
        #   Espirometria amb prova broncodilatadora
        #   en pacients amb sospita de MPOC
        #  EQA3309
        #   Espirometries realitzades
        #   amb registre de la qualitat de la t�cnica

    def get_seg(self):  # 5/5
        # asma, mpoc, mpoc_last_year, fumadors_last_year, cessacio_tabac
        # EQA3307, EQA3321, EQA3322, EQA3324, EQA3330
        # Encara no fer: EQA3312, EQA3313, EQA3319, EQA3320

        # EQA3307: OK
        #   NUM: MPOC (62) o asma (58) i fumadors q s'ha registrat
        #        consell de cessacio tabac (827) ultims 12 mesos
        #   DEN: Poblaci� atesa assignada, major de 14 anys, amb el diagn�stic
        #        de MPOC (62) o asma (58) i fumadors (eqa_tabac)
        #        en el darrer any
        # EQA3321: OK
        #   NUM : Poblaci� atesa assignada, major de 35 anys,
        #         amb el diagn�stic de MPOC,
        #         en qu� s'ha realitzat una espirometria (v agr: 447, 448) en els �ltims 24 mesos
        #   DEN : Poblaci� atesa assignada, major de 35 anys
        #         amb el diagn�stic de MPOC en el darrer any.
        # EQA3322: OK FETS()
        #   NUM : Que se'ls ha passat l'escala de la dispnea segons la guia ICS
        #         Estadi I: algun cop en la vida del pacient.
        #         Estadi II: en els �ltims 12 mesos.
        #         Estadi III i IV: en els �ltims 6 mesos.
        #         Estadi desconegut: surten en els llitats tot i tenir
        #                            l'escala feta ja que cal d'enregistrar
        #                            l'estadi en els pacients amb MPOC.
        #   DEN : Pacients adults diagnosticats de MPOC.
        # EQA3324: OK
        #   NUM: Poblaci� atesa assignada de 15-90 amb diagn�stic de MPOC amb
        #        registre nombre exacerbacions (v: ER3101) en el darrer any
        #   DEN: Poblaci� atesa assignada de 15-90 amb diagn�stic de MPOC
        # EQA3330: OK
        #   NUM: Pacients amb MPOC i FEV1 < 50%
        #        que tenen registre IMC (agr: 19) en el darrer any
        #   DEN: Pacients amb MPOC i FEV1 < 50%
        """ els que encara no s'ha de fer """
        # EQA3312 (NO FER ENCARA! mal descrit, en realitat va de verificaci� de inahladors)
        #   NUM:
        #   DEN:
        # EQA3313 (PENDENT CAROL)
        #   NUM: Poblaci� atesa assignada de 15-90 anys
        #        amb diagn�stic de MPOC (62) en qu� consta alguna SatO2 basal.
        #   DEN: Poblaci� atesa assignada, 15-90 anys,
        #        amb el diagn�stic de MPOC (62)
        # EQA3319 (PENDENT CAROL)
        #   NUM: Poblaci� atesa assignada de 15-90 anys
        #        amb diagn�stic MPOC i prescripci� d'OCD
        #        en qu� en els �ltims 12 mesos s'han registrat les variables
        #        del full de seguiment relacionades amb l'OCD
        #   DEN: Poblaci� atesa assignada de 15-90 anys
        #        amb diagn�stic MPOC i prescripci� d'OCD
        # EQA3320 (PENDENT CAROL)
        #   NUM: Poblaci� atesa assignada de 15-90 anys
        #        amb daign�stic actiu de asma i/o MPOC en qu�
        #        en els �ltims 12 mesos s'ha registrat el
        #        grau d'activitat f�sica
        #   DEN: Poblaci� atesa assignada de 15-90 anys
        #        amb daign�stic actiu de asma i/o MPOC

        # Denominadors
        majors_35 = set()
        pob_15_90 = set()
        pacients = {}
        sql = """select id_cip_sec, up, uba, upinf, ubainf,
                    institucionalitzat, edat
                from nodrizas.assignada_tot
                where edat > 14
                    and ates = 1
                    and maca = 0"""
        for id, up, uba, upinf, ubainf, inst, edat in u.getAll(sql, 'nodrizas'):  # noqa
            pacients[id] = [up, uba, upinf, ubainf, inst, edat]
            if edat > 35:
                majors_35.add(id)
            if edat < 90:
                pob_15_90.add(id)

        asma = set()
        mpoc = set()
        diabetis = set()
        excl_diabetis = set()
        # mpoc_last_year = set()
        sql = """
            SELECT
                id_cip_sec
                ,ps
            FROM
                nodrizas.eqa_problemes
            WHERE
                edat > 14
        """
        for id, ps in u.getAll(sql, 'nodrizas'):
            if ps == 62:
                mpoc.add(id)
                # if dde > self.dext - rd.relativedelta(years=1):
                #     mpoc_last_year.add(id)
            if ps == 58:
                asma.add(id)
            if ps == 24:
                diabetis.add(id)
            if ps == 563:
                excl_diabetis.add(id)
        for id in list(diabetis):
            if id in excl_diabetis:
                diabetis.remove(id)

        fumadors_last_year = set()
        sql = """
            SELECT
                id_cip_sec, dalta, dbaixa
            FROM
                nodrizas.eqa_tabac
            WHERE
                tab = 1
            """
        for (id, dalta, dbaixa) in u.getAll(sql, 'nodrizas'):
            if dalta < self.past_date < dbaixa:
                fumadors_last_year.add(id)

        cessacio_tabac = set()
        sql = """
            SELECT
                id_cip_sec
            FROM
                eqa_variables
            WHERE
                agrupador = {agr} and
                datediff(str_to_date('{d}-{m}-{y}','%d-%m-%Y'), data_var) < 365
        """.format(agr=827, d=self.day, m=self.month, y=self.year)
        for id, in u.getAll(sql, 'nodrizas'):
            cessacio_tabac.add(id)

        self.get_indicador(
            ind='EQA3307', clase='SEGUIMENT',
            pacients=self.get_pacients(15),
            dens=(mpoc | asma) & fumadors_last_year,
            nums=cessacio_tabac)

        mpoc_incidents, mpoc_prevalents = self.get_mpoc()
        self.get_indicador(
            ind='EQA3303', clase='QDIAG',
            pacients=self.get_pacients(40),
            dens=self.get_pacients(40),
            nums=mpoc_incidents)

        self.get_indicador(
            ind='EQA3304', clase='QDIAG',
            pacients=self.get_pacients(40),
            dens=self.get_pacients(40),
            nums=mpoc_prevalents)

        exacerb_mpoc_last_year = set()
        sql = """
            SELECT id_cip_sec
            from variables1 v
            where vu_cod_vs = '{v}'
        """.format(v='ER3101')
        for id, in u.getAll(sql, 'import'):
            exacerb_mpoc_last_year.add(id)

        self.get_indicador(
            ind='EQA3324', clase='SEGUIMENT',
            pacients=self.get_pacients(15, 90),
            dens=mpoc,
            nums=exacerb_mpoc_last_year)

        espiro_2anys = set()
        espiro_LT_50 = set()
        sql = """
            SELECT
                id_cip_sec, agrupador, valor
            FROM
                eqa_variables
            WHERE
                agrupador in ({agr}) and
                usar = 1 and
                datediff(str_to_date('{d}-{m}-{y}','%d-%m-%Y'), data_var) < 365 * 2
        """.format(agr='447, 448', d=self.day, m=self.month, y=self.year)
        for id, agr, val in u.getAll(sql, 'nodrizas'):
            espiro_2anys.add(id)
            if agr == 447 and val < 50.0:
                espiro_LT_50.add(id)

        self.get_indicador(
            ind='EQA3321', clase='SEGUIMENT',
            pacients=self.get_pacients(35),
            dens=mpoc,
            nums=espiro_2anys)

        imc_last_year = set()
        sql = """
            SELECT
                id_cip_sec, agrupador, valor
            FROM
                eqa_variables
            WHERE
                agrupador = {agr} and
                usar = 1 and
                datediff(str_to_date('{d}-{m}-{y}','%d-%m-%Y'), data_var) < 365
        """.format(agr=19, d=self.day, m=self.month, y=self.year)
        for id, agr, val in u.getAll(sql, 'nodrizas'):
            imc_last_year.add(id)

        self.get_indicador(
            ind='EQA3330', clase='SEGUIMENT',
            pacients=self.get_pacients(15),
            dens=mpoc & espiro_LT_50,
            nums=imc_last_year)

        var_spo2 = set()
        sql = """
            SELECT id_cip_sec
            from variables1 v
            where vu_cod_vs IN {v}
        """.format(v="""('TR101', 'TR101A', 'TR101B')""")
        for id, in u.getAll(sql, 'import'):
            var_spo2.add(id)

        self.get_indicador(
            ind='EQA3313', clase='SEGUIMENT',
            pacients=self.get_pacients(15, 90),
            dens=mpoc,
            nums=var_spo2)

        var_activitat_fisica = set()
        sql = """
            SELECT
                id_cip_sec
            FROM
                eqa_variables
            WHERE
                agrupador in ({agr}) and
                usar = 1 and
                datediff(str_to_date('{d}-{m}-{y}','%d-%m-%Y'), data_var) < 365
        """.format(agr='242, 859', d=self.day, m=self.month, y=self.year)
        for id, in u.getAll(sql, 'nodrizas'):
            var_activitat_fisica.add(id)

        self.get_indicador(
            ind='EQA3320', clase='SEGUIMENT',
            pacients=self.get_pacients(15, 90),
            dens=mpoc or asma,
            nums=var_activitat_fisica)

        # RESTRAC
        asma_persistent = set()
        vac_grip = set()
        vac_pneumococ = set()
        sql = """
            SELECT
                id_cip_sec
            FROM
                eqa_variables
            WHERE
                agrupador = {agr} and
                usar = 1 and
                valor in ({val}) and
                datediff(str_to_date('{d}-{m}-{y}','%d-%m-%Y'), data_var) < 365
        """.format(agr=278, val='3, 4', d=self.day, m=self.month, y=self.year)
        for id, in u.getAll(sql, 'nodrizas'):
            asma_persistent.add(id)

        tto_cortis_inhalats = set()
        sql = """
            SELECT id_cip_sec
            FROM
                eqa_tractaments
            WHERE
                farmac = {agr} and
                tancat = 0
        """.format(agr=642)
        for id, in u.getAll(sql, 'nodrizas'):
            tto_cortis_inhalats.add(id)

        # excluir: paliatius (agr: 276) i (maca: ya excluidos en pobl)
        self.get_indicador(
            ind='EQA3314', clase='RESTRAC',
            pacients=self.get_pacients(15),
            dens=asma or asma_persistent,
            nums=tto_cortis_inhalats)

        sql = """
            SELECT
                id_cip_sec, agrupador, datamax
            from
                eqa_vacunes
            where
                agrupador in ({agr})
        """.format(agr='99')
        for id, agr, dat in u.getAll(sql, 'nodrizas'):
            if agr == 99:
                if dat > self.dext - rd.relativedelta(years=1):
                    vac_grip.add(id)

        # sql = """
        #     SELECT id_cip_sec
        #     FROM mst_vacunes
        #     WHERE (pn20 = 1 and edat >= 18) or
        #     (pn23_1 = 1 and edat >= 15 and edat <= 17) or
        #     (pn13 = 1 and pn23_1 = 1) or
        #     (pn13 = 1 and pn23_1 = 1 and pn23_2 = 1 and edat >= 65)
        # """
        # for id, in u.getAll(sql, 'nodrizas'):
        #     vac_pneumococ.add(id)

        self.get_indicador(
            ind='EQA3310', clase='RESTRAC',
            pacients=self.get_pacients(15, 90),
            dens=asma or mpoc,
            nums=vac_grip)

        # self.get_indicador(
        #     ind='EQA3311', clase='RESTRAC',
        #     pacients=self.get_pacients(15),
        #     dens=mpoc,
        #     nums=vac_pneumococ)

        # self.get_indicador(
        #     ind='EQA0311', clase='RESTRAC',
        #     pacients=self.get_pacients(15),
        #     dens=diabetis,
        #     nums=vac_pneumococ
        # )
        # PREVQUAT
        tto_laba = set()
        sql = """
            SELECT id_cip_sec
            FROM
                eqa_tractaments
            WHERE
                farmac = {agr} and
                tancat = 0
        """.format(agr=641)

        for id, in u.getAll(sql, 'nodrizas'):
            tto_laba.add(id)

        self.get_indicador(
            ind='EQA3333', clase='PREVQUAT',
            pacients=self.get_pacients(15, 90),
            dens=asma,
            nums=tto_laba)

        self.get_indicador(
            ind='EQA3334', clase='PREVQUAT',
            pacients=self.get_pacients(15),
            dens=mpoc,
            nums=tto_cortis_inhalats)

        # PREVPRIM
        tabac_cribrats = set()
        sql = "select id_cip_sec \
               from eqa_tabac, dextraccio \
               where last = 1 and \
                     dlast between adddate(data_ext, interval -2 year) and \
                                   data_ext"
        for id, in u.getAll(sql, "nodrizas"):
            tabac_cribrats.add(id)

        self.get_indicador(
            ind='EQA3305', clase='PREVPRIM',
            pacients=self.get_pacients(15),
            dens=self.get_pacients(15),
            nums=tabac_cribrats)

        self.get_indicador(
            ind='EQA3308', clase='PREVPRIM',
            pacients=self.get_pacients(15),
            dens=fumadors_last_year,
            nums=cessacio_tabac)

        rip_mpoc = set()
        sql = "select id_cip_sec from rip_problemes where ps = 62"
        for id, in u.getAll(sql, "nodrizas"):
            rip_mpoc.add(id)
        exitus = self.get_exitus()
        self.get_indicador(
            ind='EQA3317', clase='RESEVEN',
            pacients=exitus,
            dens=exitus,
            nums=rip_mpoc)
    def RESEVEN(self):  # 0/2
        # EQA3316
        #   NUM: Poblaci� assignada de >14 anys amb diagn�stic de MPOC
        #        que han ingressat per descompensaci� MPOC el darrer any
        #   DEN: Poblaci� assignada de >14 anys amb diagn�stic de MPOC

        # EQA3317: OK
        #   NUM: Poblaci� atesa assignada amb diagn�stic actiu de  MPOC que
        #        han mort en els darrers 12 mesos.
        #   DEN: Poblaci� atesa assignada amb diagn�stic actiu de MPOC
        #        en els darrers 12 mesos.
        pass

    def restgcmpoc(self):  # 0/3
        # EQA3310
        #   NUM: Poblaci� atesa assignada 15-90
        #        amb diagn�stic actiu asma i/o MPOC que han estat
        #        vacunats de la grip (agr: 99)
        #        (http://10.80.217.201/sisap-umi/indicador/concepte/3909/ver)
        #        en el periode d'avaluaci� (campanya VAG).
        #   DEN: Poblaci� atesa assignada 15-90 a
        #        amb diagn�stic actiu d'asma i/o MPOC.

        # EQA3311
        #   NUM: Poblaci� atesa assignada de 15-90 anys
        #        amb diagn�stic actiu de asma i/o MPOC
        #        que han rebut alguna dosi de vacunaci� antipneumoc�ccica (agr: 48).
        #        (http://10.80.217.201/sisap-umi/indicador/concepte/3910/ver)
        #   DEN: http://10.80.217.201/sisap-umi/indicador/concepte/3910/ver

        # EQA3314
        #   NUM: Poblaci� atesa assignada major de 14 anys
        #        amb diagn�stic d'asma persistent
        #        amb tractament controlador amb corticoides inhalats (agr: 642)
        #       (http://10.80.217.201/sisap-umi/indicador/concepte/4195/ver/)
        #   DEN: Poblaci� atesa assignada major de 14 anys
        #        amb diagn�stic d'asma persistent.
        #        ASMA PERSISTENT: Concepte asma persistent inclou:
        #         Valor "3" i "4" a l'apartat de "gravetat"
        #          dels estadis de la malaltia que es troba en
        #          la finestra de detall del diagn�stic asma (J45.909)
        #         Valors "2", "3" o "4" de la variable VR3001
        #          a intel�lig�ncia activa
        #   EXCLUSIONS: Atenci� pal�liativa, MACA

        """ els que tenen algun problema """
        # EQA3331 (IF282P filtrat PER < 90, nivell up, resolver como PRE_MAPA)
        #   NUM: Poblaci� assignada / atesa 15-90 a
        #        amb dx actiu d'asma i/o MPOC amb prescripci� activa de
        #        f�rmacs inhalats que tinguin prescripci� activa
        #        exclusivament d'un o m�s f�rmacs per l'asma i la
        #        malaltia pulmonar obstructiva cr�nica recomanats.(PHF-APC)
        #       (http://10.80.217.201/sisap-umi/indicador/concepte/4456/ver/)
        #   DEN: Poblaci� atesa assignada 15-90 a
        #        amb dx actiu d'asma i/o MPOC
        #        amb prescripci� activa de f�rmacs inhalats.
        #   Exclusi�
        #    Numerador:
        #     S'exclouen els pacients que tinguin prescripci� activa
        #     d'algun f�rmac que no estigui a la llista de recomanats,
        #     encara que tamb� en tinguin de recomanats.
        #    Denominador:
        #     R03AL02 | Salbutamol i Ipatropi Bromur

        # EQA3315 (PENDENT DEFINICIO)
        #   NUM: Pobl assignada / atesa 15-90 a
        #        amb dx MPOC amb un tractament adequat
        #        segons l'estratificaci� del risc:
        #         - Risc baix: broncodilatadors d'acci� llarga en monoter�pia
        #         - Risc alt: doble broncodilataci� (LABA + LAMA)
        #   DEN: Pobl atesa assignada de 15-90 anys amb diagn�stic de MPOC
        pass

    def prevprim(self):  # 0/1
        # EQA3305
        #   NUM: Pacients atesos > 14 anys a qui s' ha realitzat el cribratge
        #        tabaquisme amb periodicitat de cribratge adequada:
        #          - En els darrers 24 mesos, si t� <= 25 anys
        #            o consta que en algun moment hagi estat fumador.
        #          - Si hi ha nom�s 1 sol registre:
        #            si t� >25 anys i mai ha sigut fumador.
        #   DEN: Poblaci� atesa assignada > 14 anys
        pass

    def prevquat(self):  # 0/2
        # EQA3333
        #   NUM: Poblaci� atesa/assignada, 15-90 anys amb dx asma en
        #        tractament amb LABA (agr: 641)
        #        (http://10.80.217.201/sisap-umi/indicador/concepte/3586/ver)
        #        en monoter�pia
        #   DEN: Poblaci� atesa assignada, 15-90 anys amb dx asma

        # EQA3334
        #   NUM: Poblaci� atesa/assignada, 15-90 anys amb dx MPOC
        #        en tractament amb corticoides inhalats (agr 642)
        #        (http://10.80.217.201/sisap-umi/indicador/concepte/4195/ver)
        #        en monoter�pia en el periode d'avaluaci�
        #   DEN: Poblaci� atesa assignada, 15-90 anys amb dx  MPOC
        pass


class EQA_MPOC_ASMA_KHALIX():
    def __init__(self):
        self.create_table()
        self.get_eqd()

    def create_table(self):
        cols = (
            "up varchar(5)",
            "edat varchar(10)",
            "sexe varchar(4)",
            "comb varchar(10)",
            "indicador varchar(15)",
            "conc varchar(5)",
            "n int",
            )
        u.createTable(TB_KHALIX, "({})".format(", ".join(cols)), DB)

    def get_eqd(self):  # ja hi eren a exadata.dwsisap.sisap_master_indicadors_up  # noqa
        upload = []
        # sql = """
        #     SELECT
        #         up,
        #         edat,
        #         sexe,
        #         comb,
        #         indicador,
        #         conc,
        #         n
        #     FROM eqa_ind.exp_khalix_up_ind
        #     WHERE
        #         indicador IN (
        #             'EQD024201',
        #             'EQD024202',
        #             'EQD024203',
        #             'EQD024204',
        #             'EQD024205',
        #             'EQD024206')
        # """
        # Minimizo a 4 dimensiones para Khalix
        # 'ind varchar(15)',
        # 'period varchar(5)',  # constante 'Aperiodo'
        # 'entity varchar(13)',
        # 'analysys varchar(10)',
        # 'valor int',
        sql = """
            SELECT
                indicador,
                'Aperiodo',
                up,
                conc,
                sum(n)
            FROM
                eqa_ind.exp_khalix_up_ind
            WHERE
                indicador IN (
                    'EQD024201',
                    'EQD024202',
                    'EQD024203',
                    'EQD024204',
                    'EQD024205',
                    'EQD024206')
                AND comb = 'NOINSAT'
            GROUP BY
                indicador,
                up,
                conc
        """
        for row in u.getAll(sql, 'eqa_ind'):
            upload.append(row)
        u.listToTable(upload, TB_KHALIX, DB)

    # EQA3331 --> RESTRAC
    # sql = """select * from altres.exp_khalix_eqpf
    #             where k3 not in ('EC9094','EC95M')
    #             and K0 = 'IF282P'"""

    # eqa3301
    # "exp_khalix_up_ind_proc_mpoc_prov"
    # "exp_khalix_uba_ind_proc_mpoc_prov"


if __name__ == "__main__":
    disnea.Disneampoc()
    EQA_MPOC_ASMA()
    # EQA_MPOC_ASMA_KHALIX()
