# -*- coding: latin1 -*-

"""
C�lcul assoliment Catsalut.
"""

import collections as c

import shared as s


class Catsalut(object):
    """."""

    def __init__(self):
        """."""
        self.codi_indicador = "AGCATSALUT"
        self.month, self.year = s.Period("CATSALUT_NOU").period
        self.get_metes()
        self.get_punts()
        self.get_indicadors()
        self.get_sintetic()
        self.upload_data()

    def get_metes(self):
        """."""
        self.metes = {}
        self.indicadors = set()
        filename = "catsalut_metes_{}.txt".format(self.year)
        for row in s.File(filename):
            keys = ("cuenta", "centro", "analisis")
            self.metes[tuple([row[key] for key in keys])] = row["valor"]
            self.indicadors.add(row["cuenta"])

    def get_punts(self):
        """."""
        self.punts = {}
        filename = "catsalut_puntsmax_{}.txt".format(self.year)
        for row in s.File(filename):
            keys = ("cuenta", "centro")
            self.punts[tuple([row[key] for key in keys])] = row["valor"]

    def _calculate_row(self, row, indicador=None):
        """."""
        if indicador:
            cuenta = indicador
        else:
            cuenta = self.convert.get(row["cuenta"], row["cuenta"])
        centro = row["centro"]
        resultat = 100 * row["res"]
        mmin = self.metes.get((cuenta, centro, "AGMMIN"))
        mmax = self.metes.get((cuenta, centro, "AGMMAX"))
        pmax = self.punts.get((cuenta, centro))
        if mmin is not None and mmax is not None and pmax is not None:
            if resultat >= mmax:
                assol = 1
            elif resultat <= mmin:
                assol = 0
            else:
                assol = (resultat - mmin) / (mmax - mmin)
            punts = assol * pmax
            self.sintetic[centro] += punts
            this = (self.year, self.month, centro, self.codi_indicador,
                    cuenta, resultat, mmin, mmax, 100 * assol, pmax, punts)
            self.detall.append(this)

    def get_indicadors(self):
        """."""
        self.sintetic = c.Counter()
        self.detall = []
        files = ("CATSALUT_NOU", "EQA_NOU", "EQAPED_NOU", "VISMACAPCC_CATSALUT")  # noqa
        self.convert = {"EQA0306A": "IAP016", "EQA0708": "IAP04BIS",
                        "LMS01APA": "LMS01APBIS"}
        filter = {"cuenta": list(self.indicadors) + self.convert.keys(),
                  "detalles": ("NOINSAT", "INSAT", "NOIMP")}
        aggregate = ("tipo", "controles", "detalles")
        calculate = True
        for file in files:
            filename = "{}_{}_{}.TXT".format(file, self.month, self.year)
            for row in s.File(filename, filter, aggregate, calculate):
                self._calculate_row(row)
            if file == "CATSALUT_NOU":
                this_filter = {"cuenta": ("ICAM01AP05", "ICAM01AP13", "ICAM01AP17"),  # noqa
                               "detalles": ("NOINSAT", "INSAT")}
                this_aggregate = ("cuenta", "tipo", "controles", "detalles")
                for row in s.File(filename, this_filter, this_aggregate, calculate):  # noqa
                    self._calculate_row(row, "ICAM01AP")

    def get_sintetic(self):
        """."""
        self.upload = []
        filter = {"cuenta": self.codi_indicador}
        mmax = {row["centro"]: row["valor"]
                for row in s.File("ag_metes_{}.txt".format(self.year), filter)}
        pmax = {row["centro"]: row["valor"]
                for row in s.File("ag_punts_{}.txt".format(self.year), filter)}
        for centro, resultat in self.sintetic.items():
            meta_min = 0
            meta_max = mmax.get(centro)
            punts_max = pmax.get(centro)
            if meta_max and punts_max:
                assoliment = min([1, resultat / meta_max])
                punts = punts_max * assoliment
                this = (self.year, self.month, centro, self.codi_indicador,
                        resultat, meta_min, meta_max, 100 * assoliment,
                        punts_max, punts)
                self.upload.append(this)

    def upload_data(self):
        """."""
        s.Upload(self.upload, "pares")
        s.Upload(self.detall, "fills")
