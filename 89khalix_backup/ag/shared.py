# -*- coding: latin1 -*-

"""
Utilitats compartides.
"""


import collections as c
import os
import random as rd
import requests as r

import sisapUtils as u


TB_DESTI = {"pares": "SISAP_AG",
            "fills": "SISAP_AG_DETALL"}
DB_DESTI = "redics"
COL_DESTI = {"pares": {"key": ("data_any", "data_mes", "br", "indicador"),
                       "value": ("resultat", "meta_min", "meta_max",
                                 "assoliment", "punts_max", "punts")},
             "fills": {"key": ("data_any", "data_mes", "br",
                               "indicador", "fill"),
                       "value": ("resultat", "meta_min", "meta_max",
                                 "assoliment", "punts_max", "punts")}}


class Period(object):
    """."""

    def __init__(self, pattern):
        """."""
        content = r.get("http://10.80.217.68/khalix").content
        files = []
        for row in content.split("<tr>"):
            inici = row.find(pattern)
            if inici > 0:
                final = row.find(">", inici) - 1
                files.append(row[inici:final])
        actual = max(files).split(".")[0].split("_")
        self.period = actual[-2:]


class Inspect(object):
    """."""

    def __init__(self, *args, **kwargs):
        """."""
        self.cardinality = c.defaultdict(set)
        for self.length, row in enumerate(File(*args, **kwargs), 1):
            for k, v in row.items():
                if k not in ("valor", "num", "den", "res"):
                    self.cardinality[k].add(v)
        print "rows: {}".format(self.length)
        for k, v in self.cardinality.items():
            print "{}: {} > {}".format(k, len(v), tuple(v)[:10])


class File(object):
    """."""

    def __init__(self, file, filter={}, aggregate=[], calculate=False):
        """."""
        self.file = file
        self.filter_dimensions = filter
        self.aggregate_dimensions = aggregate
        self.calculate = calculate
        me = os.path.dirname(os.path.abspath(__file__))
        self.path = "{}\\files\\".format(me)
        if not os.path.isfile(self.path + self.file):
            self.download_file()

    def download_file(self):
        """."""
        print "downloading {}".format(self.file)
        url = "http://10.80.217.68/khalix/{}".format(self.file)
        open(self.path + self.file, "wb").write(r.get(url).content)

    def __iter__(self):
        """."""
        first = self.iterate_data
        second = self.aggregate_data if self.aggregate_dimensions else self.do_nothing  # noqa
        third = self.calculate_result if self.calculate else self.do_nothing  # noqa
        for row in third(second(first())):
            yield row

    def iterate_data(self):
        """."""
        dimensions = ("cuenta", "periodo", "centro", "analisis", "tipo",
                      "detalles", "controles", "tipus_valor", "valor")
        for line in open(self.path + self.file):
            splitted = line.replace("\r", "").replace("\n", "").split("{")
            if len(splitted) > 1:
                row = {}
                go = True
                for i in range(len(dimensions)):
                    row[dimensions[i]] = splitted[i]
                    if dimensions[i] in self.filter_dimensions and \
                       splitted[i] not in self.filter_dimensions[dimensions[i]]:  # noqa
                        go = False
                tipus_valor = row.pop("tipus_valor")
                if tipus_valor == "N":
                    row["valor"] = float(row["valor"])
                if go:
                    yield row

    def aggregate_data(self, func):
        """Agrega per les dimensions especificades."""
        data = c.Counter()
        for row in func:
            valor = row.pop("valor")
            for dim in self.aggregate_dimensions:
                row.pop(dim)
            this = tuple(sorted(row.items()))
            data[this] += valor
        for key, valor in data.items():
            row = {"valor": valor}
            for dimension, value in key:
                row[dimension] = value
            yield row

    def calculate_result(self, func):
        """."""
        dades = c.defaultdict(dict)
        for row in func:
            analisis = row.pop("analisis")
            valor = row.pop("valor")
            key = tuple([(k, v) for (k, v) in row.items()])
            dades[analisis][key] = valor
        for key, valor in dades["DEN"].items():
            this = {k: v for (k, v) in key}
            this["den"] = valor
            this["num"] = dades["NUM"].get(key, 0)
            this["res"] = this["num"] / this["den"]
            yield this

    def do_nothing(self, func):
        """."""
        for row in func:
            yield row


class Upload(object):
    """."""

    def __init__(self, dades, tipus):
        """."""
        self.dades = dades
        self.tipus = tipus
        self.upload_to_temp()
        self.merge_to_prd()

    def upload_to_temp(self):
        """."""
        temp_rd = str(rd.randrange(0, 2**16)).zfill(5)
        self.table = "{}_{}".format(TB_DESTI[self.tipus], temp_rd)
        u.execute("create table {} as \
                   select * from  {} \
                   where 1 = 0".format(self.table, TB_DESTI[self.tipus]),
                  DB_DESTI)
        u.listToTable(self.dades, self.table, DB_DESTI)

    def merge_to_prd(self):
        """."""
        keys = COL_DESTI[self.tipus]["key"]
        vals = COL_DESTI[self.tipus]["value"]
        cols = keys + vals
        sql = "MERGE INTO {} a USING {} b \
               ON ({}) \
               WHEN MATCHED THEN \
                   UPDATE SET {} \
               WHEN NOT MATCHED THEN \
                   INSERT ({}) \
                   VALUES ({})".format(TB_DESTI[self.tipus],
                                       self.table,
                                       " and ".join(["a.{0} = b.{0}".format(key) for key in keys]),  # noqa
                                       ", ".join(["a.{0} = b.{0}".format(val) for val in vals]),  # noqa
                                       ", ".join(["a.{}".format(col) for col in cols]),  # noqa
                                       ", ".join(["b.{}".format(col) for col in cols]))  # noqa
        u.execute(sql, DB_DESTI)
        u.execute("drop table {}".format(self.table), DB_DESTI)
