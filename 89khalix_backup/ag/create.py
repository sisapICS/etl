# -*- coding: latin1 -*-

"""
Crea estructura a REDICS.
"""


import shared as s
import sisapUtils as u


REMOVE = False


def create():
    """."""
    for tipus in s.TB_DESTI:
        columnes = [col + " varchar2(10)" for col
                    in s.COL_DESTI[tipus]["key"]] + \
                   [col + " number" for col in s.COL_DESTI[tipus]["value"]]
        u.createTable(s.TB_DESTI[tipus], "({})".format(", ".join(columnes)),
                      s.DB_DESTI, rm=REMOVE)
        users = ("PREDUECR", "PREDUEDT", "PREDUMMP", "PREDULMB")
        u.grantSelect(s.TB_DESTI[tipus], users, s.DB_DESTI)
        try:
            u.execute("create index {0}_idx on {0} ({1})".format(
                                        s.TB_DESTI[tipus],
                                        ", ".join(s.COL_DESTI[tipus]["key"])),
                      s.DB_DESTI)
        except Exception:
            pass
