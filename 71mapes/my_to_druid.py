from sisapUtils import multiprocess, sectors, getAll, tempFolder, writeCSV, printTime, sshExecuteCommand, openCSV, getDataExt
from subprocess import Popen
from collections import defaultdict
import json
import urllib2


db = 'import'
folder = '{}druid/mapes/'.format(tempFolder)
folder_unix = '/home/sisap/druid/mapes'
spec = {"type": "index_hadoop",
        "spec": {
            "ioConfig": {
                "type": "hadoop",
                "inputSpec": {"type": "static", "paths": "{}/*.txt".format(folder_unix)}},
            "dataSchema": {
                "dataSource": "mapes",
                "granularitySpec": {
                    "type": "uniform",
                    "segmentGranularity": "year",
                    "queryGranularity": "day",
                    "intervals": ["2015-01-01/2049-12-31"]},
                "parser": {
                    "type": "string",
                    "parseSpec": {
                        "format": "csv",
                        "columns": ["DIA", "COMARCA", "MUNICIPI", "DISTRICTE", "SECCIO", "PATOLOGIA", "NUMERADOR", "DENOMINADOR"],
                        "dimensionsSpec": {"dimensions": ["COMARCA", "MUNICIPI", "DISTRICTE", "SECCIO", "PATOLOGIA"]},
                        "timestampSpec": {
                            "format": "auto",
                            "column": "DIA"}}},
                "metricsSpec": [
                    {"name": "NUMERADOR", "type": "longSum", "fieldName": "NUMERADOR"},
                    {"name": "DENOMINADOR", "type": "longSum", "fieldName": "DENOMINADOR"}]},
            "tuningConfig": {
                "type": "hadoop",
                "partitionsSpec": {"type": "hashed", "targetPartitionSize": 5000000000, "assumeGrouped": True},
                "ignoreInvalidRows": True,
                "jobProperties": {}}}}


def get_poblacio():
    poblacio = defaultdict(dict)
    for sector, id, seccio in getAll("select codi_sector, id_cip_sec, seccio_censal from nodrizas.assignada_tot where ates = 1 and edat > 59 and seccio_censal > 0", db):
        poblacio[sector][id] = seccio
    return poblacio


def get_comarques():
    url = 'http://www.idescat.cat/codis/?id=50&n=10&f=ssv'
    req = urllib2.Request(url, headers={'User-Agent': "Mozilla/5.0"}) 
    response = urllib2.urlopen(req)
    csv = response.read()
    comarques = {}
    for row in csv.split('\n'):
        cols = row.split(';')
        if len(cols) == 4:
            tip, cod, des, _r = cols
            if tip == 'Comarca':
                comarca = cod
            elif tip == 'Municipi':
                comarques[cod[:5]] = comarca
    return comarques


def get_data(params):
    sector, poblacio, comarques = params
    codis = {cod: agr for (cod, agr) in getAll("select distinct trim(codi_cim10), substr(agrupador, 4, 10000) from \
                                                (select * from cat_dbscat where taula='problemes' and ps_tancat = 0) a \
                                                inner join (select * from cat_md_ct_cim10_ciap) b on a.codi = codi_ciap_m", db)}

    data = defaultdict(lambda: defaultdict(set))
    sql = 'select id_cip_sec, pr_cod_ps from problemes_s{}, nodrizas.dextraccio where pr_cod_ps in {} and pr_dde <= data_ext'.format(sector, tuple(codis))
    for id, cod in getAll(sql, db):
        try:
            seccio = poblacio[id]
        except KeyError:
            continue
        agr = codis[cod]
        data[seccio][agr].add(id)

    seccions = defaultdict(int)
    for id, seccio in poblacio.items():
        seccions[seccio] += 1
    agrupadors = set(codis.values())
    dext = getDataExt(convert=False)

    with openCSV('{}{}.txt'.format(folder, sector), sep=',') as f:
        for seccio, count in seccions.items():
            for agr in agrupadors:
                f.writerow(['{}T12:00:00.000Z'.format(dext), comarques[seccio[:5]], seccio[:5], seccio[:7], seccio, agr, len(data[seccio][agr]), count])


if __name__ == '__main__':
    printTime('pob')
    poblacio = get_poblacio()
    comarques = get_comarques()
    printTime('my to txt')
    t = Popen('rm {}*.txt'.format(folder), shell=False)
    jobs = [(sector, poblacio[sector], comarques) for sector in sectors]
    multiprocess(get_data, jobs, 12)
    printTime('txt to druid')
    with open(folder + 'spec.json', 'wb') as f:
        json.dump(spec, f)
    sshExecuteCommand('docker-compose exec druid bin/post-index-task --file {}/spec.json'.format(folder_unix), 'druid', 'x0002')
    printTime('the end')
