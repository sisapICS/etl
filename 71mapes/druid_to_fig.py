# -*- coding: utf8 -*-

"""
Procés per mostrar informació de druid en mapes.
Pensat per jupyter amb pydb.
"""

import pydruid.client as dr_c
import pydruid.utils.aggregators as dr_a
import pydruid.utils.filters as dr_f
import pydruid.utils.postaggregator as dr_p
import matplotlib as mpl
import mpl_toolkits.basemap as mpl_bm
import numpy as np
import pandas as pd
import urllib
import urllib2
import zipfile
import os
import warnings


warnings.filterwarnings("ignore")
%matplotlib inline


def get_seccions():
    url = 'https://www.dropbox.com/sh/qd9lru5t0cxxw5e/AADHDqyMWcFoRbvmUlo5p0a8a?dl=1'
    file = 'seccions.zip'
    a = urllib.urlretrieve(url, file)
    with zipfile.ZipFile(file, 'r') as zip_ref:
        zip_ref.extractall('seccions')
    os.remove(file)


def get_comarques():
    url = 'http://www.idescat.cat/codis/?id=50&n=10&f=ssv'
    req = urllib2.Request(url, headers={'User-Agent': "Mozilla/5.0"})
    response = urllib2.urlopen(req)
    csv = response.read()
    comarques = {}
    for row in csv.split('\n'):
        cols = row.split(';')
        if len(cols) == 4:
            tip, cod, des, _r = cols
            if tip == 'Comarca':
                comarca = cod
            elif tip == 'Municipi':
                comarques[cod] = comarca
    return comarques


class PlotMap(object):

    def __init__(self, mapa, nivell, patologia, limit, colors, num_colors):
        self.mapa = mapa
        self.nivell = nivell
        self.patologia = patologia
        self.limit = limit
        self.colors = colors
        self.num_colors = num_colors
        self.get_druid_data()
        self.categorize()
        self.plot()

    def get_druid_data(self):
        query = dr_c.PyDruid('http://10.80.217.84:8082', 'druid/v2/')
        group = query.groupby(
                    datasource='mapes',
                    granularity='all',
                    intervals='2010-01-01/2029-12-31',
                    dimensions=[self.nivell],
                    filter=(dr_f.Dimension('PATOLOGIA') == self.patologia),
                    aggregations={'num': dr_a.doublesum('NUMERADOR'),
                                  'den': dr_a.doublesum('DENOMINADOR')},
                    post_aggregations={
                        'prev': ((dr_p.Field('num') / dr_p.Field('den')) *
                                 dr_p.Const(100))}
            )
        self.df = query.export_pandas()
        self.df = self.df[self.df.den >= self.limit]
        self.df.set_index(self.nivell, inplace=True)
        if self.mapa == 'BARCELONA':
            self.df = self.df.loc['08019':'0801999999']

    def categorize(self):
        values = self.df['prev']
        cm = mpl.pyplot.get_cmap(self.colors)
        self.scheme = [cm(float(i) / self.num_colors)
                       for i in range(self.num_colors)]
        self.bins = np.linspace(values.min(), values.max(), self.num_colors)
        self.df['bin'] = np.digitize(values, self.bins) - 1

    def plot(self):
        fig = mpl.pyplot.figure(figsize=(40, 20))
        ax = fig.add_subplot(111, facecolor='w', frame_on=False)
        fig.suptitle(
            'Prevalenca de {} en pacients atesos >59a ({})'.format(self.patologia,
                                                                   self.nivell),
            fontsize=30, y=.88)
        if self.mapa == 'CATALUNYA':
            map = mpl_bm.Basemap(llcrnrlon=-0.5, llcrnrlat=39.8, urcrnrlon=4.,
                                 urcrnrlat=43., resolution='i',
                                 projection='tmerc', lat_0=39.5, lon_0=1)
        elif self.mapa == 'BARCELONA':
            map = mpl_bm.Basemap(resolution='i', projection='tmerc',
                                 lat_0=41.3946, lon_0=2.14, width=15000,
                                 height=20000)
        map.drawmapboundary(color='w')
        map.fillcontinents(color='#ffffff')
        map.drawcoastlines()
        map.readshapefile('./seccions/seccions', 'seccions', drawbounds=False)
        for info, shape in zip(map.seccions_info, map.seccions):
            if self.mapa == 'BARCELONA' and info['MUNICIPI'] != '080193':
                continue
            if self.nivell == 'COMARCA':
                regio = comarques[info['MUNICIPI']]
            elif self.nivell == 'MUNICIPI':
                regio = info['MUNICIPI'][:5]
            elif self.nivell == 'DISTRICTE':
                regio = info['MUNICIPI'][:5] + info['DISTRICTE']
            elif self.nivell == 'SECCIO':
                regio = (info['MUNICIPI'][:5] + info['DISTRICTE'] +
                         info['SECCIO'])
            if regio not in self.df.index:
                color = '#dddddd'
            else:
                color = self.scheme[self.df.ix[regio]['bin']]
            patches = [mpl.patches.Polygon(np.array(shape), True)]
            pc = mpl.collections.PatchCollection(patches)
            pc.set_facecolor(color)
            pc.set_linewidth(0.)
            pc.set_zorder(9)
            ax.add_collection(pc)
        ax_legend = fig.add_axes([0.4, 0.14, 0.2, 0.01], zorder=3)
        cmap = mpl.colors.ListedColormap(self.scheme)
        cb = mpl.colorbar.ColorbarBase(ax_legend, cmap=cmap, ticks=self.bins,
                                       boundaries=self.bins,
                                       orientation='horizontal')
        cb.ax.set_xticklabels([str(round(i, 1)) for i in self.bins])
        mpl.pyplot.show()


if __name__ == '__main__':
    get_seccions()
    comarques = get_comarques()
    PlotMap('CATALUNYA', 'COMARCA', 'DIABETIS2', 999, 'YlOrBr', 5)
    PlotMap('CATALUNYA', 'MUNICIPI', 'DIABETIS2', 99, 'YlOrBr', 9)
    PlotMap('BARCELONA', 'DISTRICTE', 'DIABETIS2', 99, 'YlOrBr', 6)
    PlotMap('BARCELONA', 'SECCIO', 'DIABETIS2', 99, 'YlOrBr', 9)
