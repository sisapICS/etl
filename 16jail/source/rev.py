import sisaptools as u


TABLE = "mst_sisap_rev"
DIM = "mst_dim_sisap_rev"


COMPONENTS = ["select id_cip_sec, indicador, num, den, excl from mst_iep_indicadors_pacient",
              "select id, ind, num, den, excl from mst_indicadors_pacient_ie_eapp",
              "select id, ind, num, den, exc_den from mst_indicadors_pacient_irs_eapp",
              "select id, ind, num, den, excl from mst_indicadors_fetgegras_eapp",
              "select id_cip_sec, 'EQA2000', num, 1, excl from eqa_ind.eqa2000a_ind2",
              "select id_cip_sec, 'EQD2001', num, 1, excl from eqa_ind.eqd2001a_ind2",
              "select id_cip_sec, 'EQD2002', num, 1, excl from eqa_ind.eqd2002a_ind2"]


with u.Database("p2262", "jail") as conn:
    cols = ("id_cip_sec int", "indicador varchar(16)", "num int", "den int", "excl int")
    conn.create_table(TABLE, cols, remove=True)
    for sql in COMPONENTS:
        conn.execute("insert into {} {}".format(TABLE, sql))
    cols = ("codi varchar(16)", "descripcio varchar(255)")
    conn.create_table(DIM, cols, remove=True)
    with u.Database("x0001", "umi") as umi:
        cataleg = []
        for indicador, in conn.get_all("select distinct indicador from {}".format(TABLE)):
            this = "select nom, descripcio from indicador_indicador where nom = '{}'".format(indicador)
            cataleg.append(umi.get_one(this))
        conn.list_to_table(cataleg, DIM)
