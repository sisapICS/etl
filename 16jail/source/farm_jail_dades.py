import sisapUtils as u



class Dades_farm():
    def __init__(self):
        print('converters')
        self.get_converters()
        print('get data')
        self.get_data()
    
    def get_converters(self):
        sql = """select id_cip_sec, hash_d from import_jail.u11"""
        self.converters = {}
        for id_sec, hash in u.getAll(sql, 'import_jail'):
            self.converters[id_sec] = hash
    
    def get_data(self):
        cols = """(hash varchar(40), huab_up_codi varchar(5), 
                    huab_uab_codi varchar(5), huab_data_ass date, 
                    huab_data_final date, huab_motiu_des varchar(400))"""
        u.createTable('moviments', cols, ('farm_jail', 'analisi'), rm=True)
        sql = """select id_cip_sec, huab_up_codi, huab_uab_codi, huab_data_ass, 
                    huab_data_final, huab_motiu_des from import_jail.moviments"""
        upload = []
        for id_cip_sec, huab_up_codi, huab_uab_codi, huab_data_ass, huab_data_final, huab_motiu_des in u.getAll(sql, 'import_jail'):
            if id_cip_sec in self.converters:
                hash = self.converters[id_cip_sec]
                upload.append((hash, huab_up_codi, huab_uab_codi, huab_data_ass, huab_data_final, huab_motiu_des))
        u.listToTable(upload, 'moviments', ('farm_jail', 'analisi'))
        
        cols = """(hash varchar(40), huab_up_codi varchar(5), 
                    ingres date, 
                    sortida date, durada int, sortit int)"""
        u.createTable('jail_atesa_avui', cols, ('farm_jail', 'analisi'), rm=True)
        sql = """select id_cip_sec, up, ingres, sortida, durada, sortit from nodrizas.jail_atesa_avui"""
        upload = []
        for id_cip_sec, up, ingres, sortida, durada, sortit in u.getAll(sql, 'import_jail'):
            if id_cip_sec in self.converters:
                hash = self.converters[id_cip_sec]
                upload.append((hash, up, ingres, sortida, durada, sortit))
        u.listToTable(upload, 'jail_atesa_avui', ('farm_jail', 'analisi'))
        
        cols = """(hash varchar(40), huab_up_codi varchar(5), 
                    ingres date, 
                    sortida date, durada int, sortit int)"""
        u.createTable('jail_atesa_avui_per_up', cols, ('farm_jail', 'analisi'), rm=True)
        sql = """select id_cip_sec, up, ingres, sortida, durada, sortit from nodrizas.jail_atesa_avui_per_up"""
        upload = []
        for id_cip_sec, up, ingres, sortida, durada, sortit in u.getAll(sql, 'import_jail'):
            if id_cip_sec in self.converters:
                hash = self.converters[id_cip_sec]
                upload.append((hash, up, ingres, sortida, durada, sortit))
        u.listToTable(upload, 'jail_atesa_avui_per_up', ('farm_jail', 'analisi'))
        
        cols = """(hash varchar(40), huab_up_codi varchar(5), 
                    data_naix date, 
                    sexe varchar(1))"""
        u.createTable('assignada', cols, ('farm_jail', 'analisi'), rm=True)
        sql = """select id_cip_sec, usua_uab_up, usua_data_naixement, usua_sexe from import_jail.assignada"""
        upload = []
        for id_cip_sec, usua_uab_up, usua_data_naixement, usua_sexe in u.getAll(sql, 'import_jail'):
            if id_cip_sec in self.converters:
                hash = self.converters[id_cip_sec]
                upload.append((hash, usua_uab_up, usua_data_naixement, usua_sexe))
        u.listToTable(upload, 'assignada', ('farm_jail', 'analisi'))
        
        cols = """(hash varchar(40), codi_ps varchar(50), 
                    dde date, 
                    dba date)"""
        u.createTable('problemes', cols, ('farm_jail', 'analisi'), rm=True)
        sql = """select id_cip_sec, pr_cod_ps, pr_dde, pr_dba from import_jail.problemes"""
        upload = []
        for id_cip_sec, pr_cod_ps, pr_dde, pr_dba in u.getAll(sql, 'import_jail'):
            if id_cip_sec in self.converters:
                hash = self.converters[id_cip_sec]
                upload.append((hash, pr_cod_ps, pr_dde, pr_dba))
        u.listToTable(upload, 'problemes', ('farm_jail', 'analisi'))
        
        cols = """(hash varchar(40), pf_codi int, atc_codi varchar(7),
                    data_inici date, 
                    data_fi date, ppfmc_pmc_codi int, ppfmc_num_prod int,
                    ppfmc_durada int,
                    ppfmc_freq int,
                    ppfmc_posologia int
                    )"""
        u.createTable('tractaments', cols, ('farm_jail', 'analisi'), rm=True)
        sql = """select id_cip_sec, ppfmc_pf_codi, ppfmc_atccodi, ppfmc_pmc_data_ini, ppfmc_data_fi, ppfmc_pmc_codi, ppfmc_num_prod, ppfmc_durada,
                    ppfmc_freq,
                    ppfmc_posologia from import_jail.tractaments"""
        upload = []
        for id_cip_sec, ppfmc_pf_codi, ppfmc_atccodi, ppfmc_pmc_data_ini, ppfmc_data_fi, ppfmc_pmc_codi, ppfmc_num_prod, ppfmc_durada, ppfmc_freq, ppfmc_posologia in u.getAll(sql, 'import_jail'):
            if id_cip_sec in self.converters:
                hash = self.converters[id_cip_sec]
                upload.append((hash, ppfmc_pf_codi, ppfmc_atccodi, ppfmc_pmc_data_ini, ppfmc_data_fi, ppfmc_pmc_codi, ppfmc_num_prod,  ppfmc_durada, ppfmc_freq, ppfmc_posologia))
        u.listToTable(upload, 'tractaments', ('farm_jail', 'analisi'))
        
        cols = """(scs_codi varchar(5), ics_codi varchar(5), 
                    ics_desc varchar(40))"""
        u.createTable('cat_centres_with_jail', cols, ('farm_jail', 'analisi'), rm=True)
        sql = """select scs_codi, ics_codi, ics_desc from nodrizas.cat_centres_with_jail"""
        upload = [row for row in u.getAll(sql, 'nodrizas')]
        u.listToTable(upload, 'cat_centres_with_jail', ('farm_jail', 'analisi'))
        
        cols = """(ps_cod varchar(50), ps_des varchar(60))"""
        u.createTable('cat_prstb001', cols, ('farm_jail', 'analisi'), rm=True)
        sql = """select ps_cod, ps_des from import.cat_prstb001"""
        upload = [row for row in u.getAll(sql, 'import')]
        u.listToTable(upload, 'cat_prstb001', ('farm_jail', 'analisi'))
        
        cols = """(atc_cod varchar(7), atc_desc varchar(120))"""
        u.createTable('cat_cpftb010', cols, ('farm_jail', 'analisi'), rm=True)
        sql = """select atc_codi, atc_desc from import.cat_cpftb010"""
        upload = [row for row in u.getAll(sql, 'import')]
        u.listToTable(upload, 'cat_cpftb010', ('farm_jail', 'analisi'))
        
        cols = """(pf_codi int, desc_gen varchar(30), 
                    desc_par varchar(70), 
                    sexe varchar(7))"""
        u.createTable('cat_cpftb006', cols, ('farm_jail', 'analisi'), rm=True)
        sql = """select PF_CODI, PF_DESC_GEN, PF_DESC_PAR, PF_COD_ATC from import.cat_cpftb006"""
        upload = [row for row in u.getAll(sql, 'import')]
        u.listToTable(upload, 'cat_cpftb006', ('farm_jail', 'analisi'))

        cols = """(dgppf_pmc_codi int, dgppf_num_prod int, dgppf_ps_cod varchar(50), dgppf_versio int, dgppf_data_alta date)"""
        u.createTable('dx_prescripcio', cols, ('farm_jail', 'analisi'), rm=True)
        sql = """select dgppf_pmc_codi, dgppf_num_prod, dgppf_ps_cod, dgppf_versio, dgppf_data_alta from import.dx_prescripcio_s6951"""
        upload = [row for row in u.getAll(sql, 'import')]
        u.listToTable(upload, 'dx_prescripcio', ('farm_jail', 'analisi'))

        cols = """(dgppf_pmc_codi varchar(50), dgppf_num_prod varchar(100), dgppf_ps_cod varchar(10), dgppf_versio varchar(100), dgppf_data_alta varchar(200))"""
        u.createTable('cat_cim10mc_ciap_dbs', cols, ('farm_jail', 'analisi'), rm=True)
        sql = """
                SELECT
                CODI_CIM10 AS CIM10MC,
                PS_DES AS DESC_CIM10MC,
                CODI_CIAP_M AS CIAP_M,
                DESC_CIAP_M AS DESC_CIAP_M,
                LITERAL_CURT AS DESC_DBS
                FROM MD_CT_CIM10_CIAP A
                LEFT JOIN PRSTB001 B ON CODI_CIM10=PS_COD
                LEFT JOIN DBSCAT C ON CODI_CIAP_M=CODI
                LEFT JOIN DBSCATDESC D ON C.AGRUPADOR=D.AGRUPADOR
                WHERE CODI_CIM10 LIKE 'C01-%'
                GROUP BY
                CODI_CIM10,
                PS_DES,
                CODI_CIAP_M,
                DESC_CIAP_M,
                C.AGRUPADOR,
                LITERAL_CURT
                ORDER BY CODI_CIM10,
                CODI_CIAP_M,
                DESC_DBS
        """
        upload = [row for row in u.getAll(sql, 'redics')]
        u.listToTable(upload, 'cat_cim10mc_ciap_dbs', ('farm_jail', 'analisi'))


if __name__ == "__main__":
    Dades_farm()
    