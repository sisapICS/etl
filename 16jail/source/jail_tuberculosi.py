# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime

def get_dextraccio():
    """."""

    global data_ext, data_ext_menys1any, data_ext_menys2anys

    sql = """
            SELECT
                data_ext,
                date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
                date_add(date_add(data_ext, INTERVAL -2 YEAR), INTERVAL + 1 DAY)
            FROM
                dextraccio
            """
    data_ext, data_ext_menys1any, data_ext_menys2anys = u.getOne(sql, "nodrizas")

def get_centres():

    global centres

    sql = """
            SELECT
                scs_codi,
                ics_codi
            FROM
                jail_centres
          """
    centres = {up: br for up, br in u.getAll(sql, "nodrizas")}

def get_poblacio():

    global poblacio, conversor_cip2idcip, poblacio_ingres_ultim_any
    poblacio_ultim_any = {}
    poblacio_ingres_ultim_any = set()

    sql = """
            SELECT
                id_cip_sec,
                ingres,
                up,
                uba
            FROM
                jail_atesa_avui_60
            WHERE
                (sortida = '0000-00-00' OR sortida BETWEEN '{}' AND '{}')
                -- AND durada >= 60
          """.format(data_ext_menys1any, data_ext)
    for id_cip_sec, ingres, up, uba in u.getAll(sql, "nodrizas"):
        if up in centres:
            poblacio_ultim_any[id_cip_sec] = {"up": up, "br": centres[up], "uba": uba, "ingres": ingres}
            if ingres >= data_ext_menys1any:
                poblacio_ingres_ultim_any.add(id_cip_sec)

    sql = """
            SELECT
                id_cip,
                id_cip_sec,
                usua_data_naixement,
                usua_sexe
            FROM
                assignada
          """
    poblacio = {id_cip: {"id_cip_sec": id_cip_sec, "up": poblacio_ultim_any[id_cip_sec]["up"], "br": poblacio_ultim_any[id_cip_sec]["br"], "uba": poblacio_ultim_any[id_cip_sec]["uba"], 
                         "ingres": poblacio_ultim_any[id_cip]["ingres"], "upinf": None, "ubainf": None, "edat": u.yearsBetween(data_naix, data_ext), "sexe": sexe} 
                for id_cip, id_cip_sec, data_naix, sexe in u.getAll(sql, "import_jail") if id_cip_sec in poblacio_ultim_any}

    sql = """
            SELECT
                id_cip,
                hash_d
            FROM
                u11
          """
    conversor_hash2idcip = {hash: id_cip for id_cip, hash in u.getAll(sql, "import_jail")}

    sql = """
            SELECT
                usua_cip AS cip,
                usua_cip_cod AS hash
            FROM
                pdptb101
          """
    conversor_cip2idcip = {cip: conversor_hash2idcip.get(hash) for cip, hash in u.getAll(sql, "pdp") if hash in conversor_hash2idcip}

def get_problemes():

    global problemes;           problemes = c.defaultdict(set)

    agrupadors = {101: "VIH - Immunodepresi�",
                  529: "Trasplantats",
                  639: "Tuberculosi",
                  665: "Di�lisi",
                  666: "VIH - Immunodepresi�",
                  681: "VIH - Immunodepresi�",
                  1072: "Silicosi"  
                }

    codis_problemes = {"C01-Z22.39": "Infecci� Tuberculosa Latent"}

    sql = """
            SELECT
                criteri_codi,
                agrupador
            FROM
                eqa_criteris
            WHERE
                agrupador IN {_agrupadors}
          """.format(_agrupadors=tuple(agrupadors.keys()))
    codis_problemes.update({criteri_codi: agrupadors[agrupador] for criteri_codi, agrupador in u.getAll(sql, "nodrizas")})

    sql = """
            SELECT
                id_cip,
                pr_cod_ps,
                pr_dde
            FROM
                problemes
            WHERE
                pr_cod_ps IN {_codis_problemes}
          """.format(_codis_problemes=tuple(codis_problemes.keys()))
    for id_cip, criteri_codi, data in u.getAll(sql, "import_jail"):
        if id_cip in poblacio:
            agrupador_desc = codis_problemes[criteri_codi]
            problemes[agrupador_desc].add(id_cip)

def get_visites_domi_inf_post_dx_tuberculosi():

    global visites_domi_inf_post_dx_tuberculosi;    visites_domi_inf_post_dx_tuberculosi = set()

    sql = """
            SELECT
                sc_cip,
                sc_datseg
            FROM
                prstb318
            WHERE
                sc_coddiag LIKE '%C01-Z22.39%'
                AND sc_numcol LIKE '3%'
                AND sc_datseg BETWEEN TO_DATE('{_data_ext_menys1any}','YYYY-MM-DD') AND TO_DATE('{_data_ext}','YYYY-MM-DD')
          """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    for cip, data in u.getAll(sql, "6951"):
        id_cip = conversor_cip2idcip.get(cip)
        if id_cip in tractaments["Tractament de la infecci� tuberculosa latent"]:
            if id_cip in poblacio and any(0 <= u.daysBetween(data_inici_tract, data.date()) <= 30 for data_inici_tract in tractaments["Tractament de la infecci� tuberculosa latent"][id_cip]):
                visites_domi_inf_post_dx_tuberculosi.add(id_cip)

def get_variables():

    global variables;           variables = c.defaultdict(lambda: c.defaultdict(dict))

    agrupadors = {637: "PPD",
                  1: "inventat"
                }

    sql = """
            SELECT
                criteri_codi,
                agrupador
            FROM
                eqa_criteris
            WHERE
                agrupador IN {_agrupadors}
          """.format(_agrupadors=tuple(agrupadors.keys()))
    codis_variables = {criteri_codi: agrupadors[agrupador] for criteri_codi, agrupador in u.getAll(sql, "nodrizas")}

    sql = """
            SELECT
                id_cip,
                vu_cod_vs,
                vu_dat_act,
                vu_val,
                CASE WHEN vu_val >= 10 THEN 1
                     ELSE 0
                   END AS ppd_positiu
            FROM
                variables
            WHERE
                vu_cod_vs IN {_codis_variables}
          """.format(_codis_variables=tuple(codis_variables.keys()), _data_ext=data_ext)
    for id_cip, criteri_codi, data, valor, ppd_positiu in u.getAll(sql, "import_jail"):
        if id_cip in poblacio:
            agrupador_desc = codis_variables[criteri_codi]
            variables[agrupador_desc][id_cip][data] = valor
            if agrupador_desc == "PPD":
                if ppd_positiu and data_ext_menys1any < data <= data_ext:
                    variables[agrupador_desc + " positiu ultim any"][id_cip][data] = valor
                else:
                    variables[agrupador_desc + " negatiu"][id_cip][data] = valor
                    if 0 <= u.daysBetween(poblacio[id_cip]["ingres"], data) <= 30:
                        variables[agrupador_desc + " negatiu 30 dies ingres"][id_cip][data] = valor

    for id_cip in variables["PPD"]:
        algun_valor_negatiu = any(valor < 10 for data, valor in variables["PPD"][id_cip].items() if data >= data_ext_menys2anys)
        ultima_data = max(variables["PPD"][id_cip])
        ultim_valor_es_positiu = 1 if variables["PPD"][id_cip][ultima_data] >= 10 else 0
        if ultim_valor_es_positiu and algun_valor_negatiu:
            variables["Convertor recent"][id_cip][ultima_data] = valor

def get_tractaments():

    global tractaments;                                 tractaments = c.defaultdict(lambda: c.defaultdict(set))
    global adherencia_tractament_tuberculosi;           adherencia_tractament_tuberculosi = set()
    quant_tratament_tuberculosi = c.defaultdict(c.Counter)

    agrupadors = {41: "Immunosupressi� farmacol�gica",
                  1070: "F�rmacs biol�gics",
                  1073: "Tractament de la infecci� tuberculosa latent"
                }

    sql = """
            SELECT
                criteri_codi,
                agrupador                
            FROM
                eqa_criteris
            WHERE
                agrupador IN {_agrupadors}
          """.format(_agrupadors=tuple(agrupadors.keys()))
    codis_tractaments = {criteri_codi: agrupadors[agrupador] for criteri_codi, agrupador in u.getAll(sql, "nodrizas")}

    sql = """
            SELECT
                dgppf_ps_cod,
                dgppf_pmc_codi,
                dgppf_num_prod
            FROM
                dx_prescripcio
            WHERE
                dgppf_ps_cod LIKE '%Z22.39%'
          """
    tractaments_associats_tuberculosi = {(pf_codi, num_prod) for ps_cod, pf_codi, num_prod in u.getAll(sql, "import_jail")}

    sql = """
            SELECT
                id_cip_sec,
                dis_quant,
                dis_quant_dispens
            FROM
                import_jail.eapp_ppftb072
            WHERE
                dis_pf_codi IN ('J04AB02', 'J04AC01', 'J04AM02')
                AND dis_data_fi >= '{}'
                AND dis_data_ini <= '{}'
          """.format(data_ext_menys1any, data_ext)
    for id_cip_sec, quant, quant_dispens in u.getAll(sql, "import_jail"):
        quant_tratament_tuberculosi[id_cip_sec]["quant"] += quant
        quant_tratament_tuberculosi[id_cip_sec]["quant_dispens"] += quant_dispens
    
    for id_cip_sec, dades in quant_tratament_tuberculosi.items():
        if 0.9 <= float(dades["quant_dispens"]) / float(dades["quant"]):
            adherencia_tractament_tuberculosi.add(id_cip_sec)

    sql = """
            SELECT
                id_cip,
                ppfmc_atccodi,
                ppfmc_pmc_data_ini,
                ppfmc_pmc_codi,
                ppfmc_num_prod
            FROM
                tractaments
            WHERE
                ppfmc_atccodi IN {_codis_tractaments}
                AND (ppfmc_data_fi >'{_data_ext}' OR ppfmc_data_fi = '0000-00-00')
          """.format(_codis_tractaments=tuple(codis_tractaments.keys()), _data_ext=data_ext)
    for id_cip, criteri_codi, data_inici, pmc_codi, num_prod in u.getAll(sql, "import_jail"):
        if id_cip in poblacio:
            agrupador_desc = codis_tractaments[criteri_codi]
            tractaments[agrupador_desc][id_cip].add(data_inici)
            if (pmc_codi, num_prod) in tractaments_associats_tuberculosi and agrupador_desc == "Tractament de la infecci� tuberculosa latent":
                tractaments["Tractament associat infecci� tuberculosa latent"][id_cip].add(data_inici)

def get_laboratori():

    global laboratori;           laboratori = c.defaultdict(set)

    agrupadors = {1071: "IGRA",
                  1: "inventat"
                }

    codis_laboratori = {"L04AB01": "IGRA",
                }

    sql = """
            SELECT
                criteri_codi,
                agrupador                
            FROM
                eqa_criteris
            WHERE
                agrupador IN {_agrupadors}
          """.format(_agrupadors=tuple(agrupadors.keys()))
    codis_laboratori = {criteri_codi: agrupadors[agrupador] for criteri_codi, agrupador in u.getAll(sql, "nodrizas")}

    sql = """
            SELECT
                id_cip,
                cr_codi_prova_ics,
                CASE WHEN cr_res_lab LIKE '%posit%' THEN 1
                     ELSE 0
                   END AS positiu,
                IF(cr_data_reg BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}', 1, 0) ultim_any
            FROM
                laboratori
            WHERE
                cr_codi_prova_ics IN {_codis_laboratori}
          """.format(_codis_laboratori=tuple(codis_laboratori.keys()), _data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    for id_cip, criteri_codi, positiu, ultim_any in u.getAll(sql, "import_jail"):
        if id_cip in poblacio:
            agrupador_desc = codis_laboratori[criteri_codi]
            if ultim_any:
                laboratori[agrupador_desc + " ultim any"].add(id_cip)
                if positiu:
                    laboratori[agrupador_desc + " positiu" + " ultim any"].add(id_cip)
            laboratori[agrupador_desc + " qualsevol moment"].add(id_cip)
            if positiu:
                laboratori[agrupador_desc + " positiu qualsevol moment"].add(id_cip)
    
def get_indicadors():

    global indicador;           indicador = c.defaultdict(c.Counter)
    global set_registres;       set_registres = c.defaultdict(lambda: c.defaultdict(c.Counter))
    global mst_registres;       mst_registres = []
    denominador, numerador, exclusio = dict(), dict(), dict()

    # ITL1

    denominador["ITL1"] = (set(problemes["VIH - Immunodepresi�"]) | set(tractaments["F�rmacs biol�gics"]) | set(tractaments["Immunosupressi� farmacol�gica"]) | set(problemes["Di�lisi"])) & set(variables["PPD negatiu 30 dies ingres"]) & poblacio_ingres_ultim_any
    numerador["ITL1"] = set(laboratori["IGRA ultim any"])
    exclusio["ITL1"] = set(problemes["Tuberculosi"])

    # ITL2

    denominador["ITL2"] = (set(problemes["VIH - Immunodepresi�"]) | set(problemes["Trasplantats"]) | set(tractaments["F�rmacs biol�gics"]) | set(tractaments["Immunosupressi� farmacol�gica"]) | set(problemes["Di�lisi"]) | set(problemes["Silicosi"])) & (set(laboratori["IGRA positiu ultim any"]) | set(variables["PPD positiu ultim any"]))
    numerador["ITL2"] = set(tractaments["Tractament de la infecci� tuberculosa latent"])
    exclusio["ITL2"] = set(problemes["Tuberculosi"])

    # ITL2B

    denominador["ITL2B"] = (set(problemes["VIH - Immunodepresi�"]) | set(problemes["Trasplantats"]) | set(tractaments["F�rmacs biol�gics"]) | set(tractaments["Immunosupressi� farmacol�gica"]) | set(problemes["Di�lisi"]) | set(problemes["Silicosi"])) & (set(laboratori["IGRA positiu ultim any"]) | set(variables["PPD positiu ultim any"]))
    numerador["ITL2B"] = set(problemes["Infecci� Tuberculosa Latent"])
    exclusio["ITL2B"] = set(problemes["Tuberculosi"])

    # # ITL3

    denominador["ITL3"] = (set(laboratori["IGRA positiu qualsevol moment"]) | set(variables["PPD positiu qualsevol moment"])) & set(tractaments["Tractament de la infecci� tuberculosa latent"])
    numerador["ITL3"] = adherencia_tractament_tuberculosi
    exclusio["ITL3"] = set(problemes["Tuberculosi"])

    # ITL4

    denominador["ITL4"] = set(tractaments["Tractament associat infecci� tuberculosa latent"])
    numerador["ITL4"] = visites_domi_inf_post_dx_tuberculosi
    exclusio["ITL4"] = problemes["Tuberculosi"]

    # ITL5

    denominador["ITL5"] = set(variables["Convertor recent"])
    numerador["ITL5"] = set(tractaments["Tractament de la infecci� tuberculosa latent"])
    exclusio["ITL5"] = ((set(problemes["VIH - Immunodepresi�"]) | set(problemes["Trasplantats"]) | set(tractaments["F�rmacs biol�gics"]) | set(tractaments["Immunosupressi� farmacol�gica"]) | set(problemes["Di�lisi"]) | set(problemes["Silicosi"]))) | set(problemes["Tuberculosi"])

    for indicador in denominador:
        for id_cip in denominador[indicador]:
            den, num = 1, 0
            excl = 0 if id_cip not in exclusio[indicador] else 1
            id_cip_sec, up, uba, upinf, ubainf, edat, sexe = (poblacio[id_cip][k] for k in ("id_cip_sec", "up", "uba", "upinf", "ubainf", "edat", "sexe"))
            br = poblacio[id_cip]["br"]
            set_registres[indicador][br]["DEN"] += 1
            if id_cip in numerador[indicador]:
                set_registres[indicador][br]["NUM"] += 1
                num = 1
            mst_registres.append([id_cip_sec, indicador, up, uba, upinf, ubainf, edat, sexe, num, den, excl])

def export_khalix():

    tb_name = "jail_tuberculosi"
    db_name = "jail"
    tb_cols = "(indicador varchar(10), br varchar(5), analisis varchar(3), n int)"
    file_name = "ITL"

    registres = list()
    for indicador in set_registres:
        for br in set_registres[indicador]:
            den, num = set_registres[indicador][br]["DEN"], set_registres[indicador][br]["NUM"] if "NUM" in set_registres[indicador][br] else 0
            registres.append((indicador, br, "DEN", den))
            registres.append((indicador, br, "NUM", num))

    u.createTable(tb_name, tb_cols, db_name, rm=True)
    u.listToTable(registres, tb_name, db_name)

    sql = """
            SELECT
                indicador,
                "Aperiodo",
                br,
                analisis,
                "NOCAT",
                "NOIMP",
                "DIM6SET",
                "N",
                n
            FROM
                {}.{} a
            WHERE indicador != 'ITL3'
            """.format(db_name, tb_name)
    u.exportKhalix(sql, file_name)

    mst_name = "mst_tuberculosi_pacient"
    mst_cols = "(id_cip_sec int, indicador varchar(10), up varchar(5) not null default'', uba varchar(5) not null default'',upinf varchar(5) not null default'', ubainf varchar(5) not null default'', \
                edat int, sexe varchar(1) not null default'',num double, den double, excl double)"
    u.createTable(mst_name, mst_cols, db_name, rm=True)
    u.listToTable(mst_registres, mst_name, db_name)

if __name__ == '__main__':

    get_dextraccio();                               print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_centres();                                  print("get_centres(): {}".format(datetime.datetime.now()))
    get_poblacio();                                 print("get_poblacio(): {}".format(datetime.datetime.now()))
    get_problemes();                                print("get_problemes(): {}".format(datetime.datetime.now()))
    get_tractaments();                              print("get_tractaments(): {}".format(datetime.datetime.now()))
    get_visites_domi_inf_post_dx_tuberculosi();     print("get_visites_domi_inf_post_dx_tuberculosi(): {}".format(datetime.datetime.now()))
    get_variables();                                print("get_variables(): {}".format(datetime.datetime.now()))
    get_laboratori();                               print("get_laboratori(): {}".format(datetime.datetime.now()))
    get_indicadors();                               print("get_indicadors(): {}".format(datetime.datetime.now()))
    export_khalix();                                print("export_khalix(): {}".format(datetime.datetime.now()))