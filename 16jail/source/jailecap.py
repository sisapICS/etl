# coding: iso-8859-1
from sisapUtils import *

INDICADORS_VALIDACIO = ('ITL3',)

# Per INDICADORS_VALIDACIO: 
#       Si no hi ha res a validar, deixar una tupla buida perquè el script funcioni. 
#       Si només hi ha un indicador a validar, posar tupla d'un element (amb coma després del nom de l'indicador).
PARES_VALIDACIO = ('IEPAGTA',)

MMIN, MINT, MMAX = 500, 615, 700

db = "jail"

uba = "%s.exp_ecap_IEP_uba" % db
pac = "%s.exp_ecap_IEP_pacient" % db
dext = "nodrizas.dextraccio"  # compte
paceqa = "eqa_ind.exp_ecap_pacient"

cat = [['%s.exp_ecap_IEP_catalegPare' % db,'prsCatalegPare'],
       ['%s.exp_ecap_IEP_catalegMetes' % db,'prsCatalegMetes']]

no_indicadors_validacio = " NOT IN {}".format(INDICADORS_VALIDACIO) if len(INDICADORS_VALIDACIO) > 1 else " <> '{}'".format(INDICADORS_VALIDACIO[0])
si_indicadors_validacio = " IN {}".format(INDICADORS_VALIDACIO) if len(INDICADORS_VALIDACIO) > 1 else " = '{}'".format(INDICADORS_VALIDACIO[0])

if INDICADORS_VALIDACIO:

    # Primer de tot, borrem els registres d'indicadors de validació de les taules altIndicadors,

    sql = "DELETE FROM ALTINDICADORS WHERE indicador {}".format(si_indicadors_validacio)
    execute(sql, 'pdp')
    sql = "DELETE FROM ALTLLISTATS WHERE indicador {}".format(si_indicadors_validacio)
    execute(sql, 'pdp')
    sql = "DELETE FROM ALTCATALEG WHERE indicador {}".format(si_indicadors_validacio)
    execute(sql, 'pdp')

    if PARES_VALIDACIO:
        for pare in PARES_VALIDACIO:
            sql = "DELETE FROM ALTCATALEGPARE WHERE pare = '{}'".format(pare)
            try:
                execute(sql, 'pdp')
            except:
                continue

    query = """
                SELECT
                    *
                FROM
                    {0}
                WHERE
                    up <> ''
                    AND indicador {1}
            UNION
                SELECT
                    up,
                    'PRS',
                    indicador,
                    sum(numerador),
                    sum(denominador),
                    round(sum(numerador) / sum(denominador), 4)
                FROM
                    {0}
                WHERE
                    up <> ''
                    AND indicador {1}
                GROUP BY
                    up,
                    indicador
            """.format(uba, no_indicadors_validacio)
    table = "prsIndicadors"
    exportPDP(query=query, table=table, dat=True)

    query = """
                SELECT DISTINCT
                    year(data_ext),
                    date_format(data_ext,'%c'),
                    up,
                    'sense',
                    'M',
                    indicador,
                    sum(numerador),
                    sum(denominador),
                    round(sum(numerador) / sum(denominador), 4),
                    NULL,
                    0,
                    0
                FROM
                    {0},
                    nodrizas.dextraccio
                WHERE
                    up <> ''
                    AND indicador {1}
                GROUP BY
                    up,
                    indicador
            UNION
                SELECT DISTINCT
                    year(data_ext),
                    date_format(data_ext,'%c'),
                    up,
                    'sense',
                    'I',
                    indicador,
                    sum(numerador),
                    sum(denominador),
                    round(sum(numerador) / sum(denominador), 4),
                    NULL,
                    0,
                    0
                FROM
                    {0},
                    nodrizas.dextraccio
                WHERE
                    up <> ''
                    AND indicador {1}
                GROUP BY
                    up,
                    indicador
            """.format(uba, si_indicadors_validacio)
    table = "altIndicadors"
    exportPDP(query=query, table=table)

    query = """
                SELECT DISTINCT
                    up,
                    uba,
                    'M',
                    grup_codi,
                    hash_d,
                    sector,
                    exclos
                FROM
                    {0}
                WHERE
                    uba <> ''
                    AND grup_codi {1}
            UNION
                SELECT DISTINCT
                    upinf,
                    ubainf,
                    'I',
                    grup_codi,
                    hash_d,
                    sector,
                    exclos
                FROM
                    {0}
                WHERE
                    ubainf <> ''
                    AND grup_codi {1}
            """.format(pac, no_indicadors_validacio)
    table = "prsLlistats"
    exportPDP(query=query, table=table, truncate=True, pkOut=True)

    query = """
                SELECT DISTINCT
                    up,
                    'sense',
                    'M',
                    grup_codi,
                    hash_d,
                    sector,
                    exclos,
                    NULL
                FROM
                    {0}
                WHERE
                    uba <> ''
                    AND grup_codi {1}
            UNION
                SELECT DISTINCT
                    up,
                    'sense',
                    'I',
                    grup_codi,
                    hash_d,
                    sector,
                    exclos,
                    NULL
                FROM
                    {0}
                WHERE
                    uba <> ''
                    AND grup_codi {1}
            """.format(pac, si_indicadors_validacio)
    table = "altLlistats"
    exportPDP(query=query, table=table, pkOut=True)

    query = """
                SELECT DISTINCT
                    indicador, literal, ROW_NUMBER() OVER (ORDER BY pare, indicador) AS ordre, pare, llistat, invers, mmin, mint, mmax, toShow, wiki, tipus, ncol
                FROM
                    {0}.exp_ecap_IEP_cataleg
                WHERE
                    indicador {1}
                ORDER BY pare, indicador
            """.format(db, no_indicadors_validacio)
    table = "prsCataleg"
    exportPDP(query=query, table=table, truncate=True, datAny=True)


    query_pares = """
                    SELECT DISTINCT
                        pare
                    FROM
                        {0}.exp_ecap_IEP_cataleg
                    WHERE
                        indicador {1}
                """.format(db, no_indicadors_validacio)
    pares = tuple([pare[0] for pare in getAll(query_pares, "import")])

    si_pares = " IN {}".format(pares) if len(pares) > 1 else " = '{}'".format(pares[0])

    query = """
                SELECT DISTINCT
                    *
                FROM
                    {0}.exp_ecap_IEP_catalegPare
                WHERE
                    pare {1}
            """.format(db, si_pares)
    table = 'prsCatalegPare'
    exportPDP(query=query, table=table, datAny=True)

    query = """
                SELECT
                    *
                FROM
                    {0}.exp_ecap_IEP_catalegMetes
                WHERE
                    indicador {1}
            """.format(db, no_indicadors_validacio)
    table = 'prsCatalegMetes'
    exportPDP(query=query, table=table, datAny=True)

    query = """
                SELECT
                    year(data_ext),
                    indicador,
                    literal,
                    ordre,
                    pare,
                    llistat,
                    invers,
                    mmin,
                    mint,
                    mmax,
                    toShow,
                    wiki,
                    'PCT'
                FROM
                    {0}.exp_ecap_IEP_cataleg,
                    nodrizas.dextraccio
                WHERE
                    indicador {1}
            """.format(db, si_indicadors_validacio)
    table = 'altCataleg'
    exportPDP(query=query, table=table)

    query_pares = """
                    SELECT DISTINCT
                        pare
                    FROM
                        {0}.exp_ecap_IEP_cataleg
                    WHERE
                        indicador {1}
                """.format(db, si_indicadors_validacio)
    pares = tuple([pare[0] for pare in getAll(query_pares, "import")])
    if PARES_VALIDACIO:
        pares += PARES_VALIDACIO

    query = """
                SELECT DISTINCT
                    year(data_ext),
                    pare,
                    literal,
                    ROW_NUMBER() OVER (ORDER BY pare) AS ordre,
                    'VALIDACIO'
                FROM
                    {0}.exp_ecap_IEP_catalegPare,
                    nodrizas.dextraccio
                WHERE
                    pare {1}
            """.format(db, si_pares)
    try:
        exportPDP(query=query, table='altCatalegPare')
    except:
        pass

    query = """
                SELECT
                    up,
                    uba,
                    'M',
                    grup_codi,
                    hash_d,
                    a.sector,
                    exclos
                FROM
                    {0} a
                INNER JOIN nodrizas.jail_centres ON
                    up = scs_codi
                INNER JOIN jail.mst_jail_eqaindicadors c ON
                    a.grup_codi = c.indicador
                WHERE
                    id_cip_sec <0
                    AND grup_codi {1}
                UNION
                SELECT
                    upinf,
                    ubainf,
                    'I',
                    grup_codi,
                    hash_d,
                    a.sector,
                    exclos
                FROM
                    {0} a
                INNER JOIN nodrizas.jail_centres ON
                    up = scs_codi
                INNER JOIN jail.mst_jail_eqaindicadors c ON
                    a.grup_codi = c.indicador
                WHERE
                    id_cip_sec <0
                    AND grup_codi {1}
            """.format(paceqa, no_indicadors_validacio)
    table = "prseqaLlistats"
    try:
        exportPDP(query=query, table=table, truncate=True, pkOut=True, fmo=False)
    except:
        pass

else:

    query = """
                SELECT
                    *
                FROM
                    {0}
                WHERE
                    up <> ''
              UNION
                SELECT
                    up,
                    'PRS',
                    indicador,
                    sum(numerador),
                    sum(denominador),
                    round(sum(numerador) / sum(denominador), 4)
                FROM
                    {0}
                WHERE
                    up <> ''
                GROUP BY
                    up,
                    indicador
            """.format(uba)
    table = "prsIndicadors"
    exportPDP(query=query,table=table,dat=True)

    query = """
                SELECT DISTINCT
                    up,
                    uba,
                    'M',
                    grup_codi,
                    hash_d,
                    sector,
                    exclos
                FROM
                    {0}
                WHERE
                    uba <> ''
              UNION
                SELECT DISTINCT
                    upinf,
                    ubainf,
                    'I',
                    grup_codi,
                    hash_d,
                    sector,
                    exclos
                FROM
                    {0}
                WHERE
                    ubainf <> ''
            """.format(pac)
    table = "prsLlistats"
    exportPDP(query=query, table=table, truncate=True, pkOut=True)

    query = """
                SELECT DISTINCT
                    indicador, literal, ROW_NUMBER() OVER (ORDER BY pare, indicador) AS ordre, pare, llistat, invers, mmin, mint, mmax, toShow, wiki, tipus, ncol
                FROM
                    {0}.exp_ecap_IEP_cataleg
                ORDER BY pare, indicador
            """.format(db)
    table = "prsCataleg"
    exportPDP(query=query, table=table, truncate=True, datAny=True)

    query_pares = """
                    SELECT DISTINCT
                        pare
                    FROM
                        {0}.exp_ecap_IEP_cataleg
                  """.format(db)
    pares = tuple([pare[0] for pare in getAll(query_pares, "import")])

    si_pares = " IN {}".format(pares) if len(pares) > 1 else " = '{}'".format(pares[0])

    query = """
                SELECT DISTINCT
                    *
                FROM
                    {0}.exp_ecap_IEP_catalegPare
                WHERE
                    pare {1}
            """.format(db, si_pares)
    table = 'prsCatalegPare'
    exportPDP(query=query, table=table, datAny=True)

    query = """
                SELECT
                    *
                FROM
                    {0}.exp_ecap_IEP_catalegMetes
            """.format(db)
    table = 'prsCatalegMetes'
    exportPDP(query=query, table=table, datAny=True)
    
'''
query= "select data_ext from %s limit 1" % dext
table= "prsLlistatsData"
exportPDP(query=query,table=table,truncate=True)
'''

for r in cat:
    my, ora = r[0], r[1]
    query = "select * from %s" % my
    exportPDP(query=query, table=ora, datAny=True)

query = "select up,uba,'M',grup_codi,hash_d,a.sector,exclos from %s a inner join nodrizas.jail_centres on up=scs_codi inner join jail.mst_jail_eqaindicadors c on a.grup_codi=c.indicador where id_cip_sec <0 " % (paceqa)
table = "prseqaLlistats"
exportPDP(query=query, table=table, truncate=True, pkOut=True, fmo=False)

query = "select upinf,ubainf,'I',grup_codi,hash_d,a.sector,exclos from %s a inner join nodrizas.jail_centres on upinf=scs_codi inner join jail.mst_jail_eqaindicadors c on a.grup_codi=c.indicador where id_cip_sec <0 " % (paceqa)
table = "prseqaLlistats"
exportPDP(query=query, table=table, truncate=False, pkOut=True, fmo=False)

query= "select data_ext from %s limit 1" % dext
table= "prsLlistatsData"
exportPDP(query=query,table=table,truncate=True)

query = """select up, 'UAB', 'M', indicador, esperats, detectats,
        resolts, 0, 0,
        resolucio, resolucio, resultatperpunts,
        punts, 
        case when invers = 1 then resolts 
            when indicador like 'EQD09%' then 0
            else detectats-resolts end noresolts,
        resolucioperpunts, mmin_p, mmax_p
        from eqa_ind.exp_ecap_up_jail
        where indicador not in ('EQA0401', 'EQA0402', 'EQA0403')"""
table = "prseqaindicadors"
exportPDP(query=query, table=table, dat=True, fmo=False)


query = """select 'G', UP, 'UAB', 'M', 
            SUM(case when invers = 1 then resolts 
            when indicador like 'EQD09%' then 0
            else detectats-resolts end) as res, 
            SUM(PUNTS) as punts, 1000, 
            case when sum(punts) < {MMIN} then 0
                when sum(punts) >= {MMAX} then 1
                else (sum(punts)-{MMIN})/({MMAX}-{MMIN})
            end punts_assol
            from eqa_ind.exp_ecap_up_jail
            group by up"""
table = "prseqasintetic"
exportPDP(query=query.format(MMIN=MMIN, MMAX=MMAX), table=table, dat=True, fmo=False)

sql = """select scs_codi, ics_codi from nodrizas.jail_centres"""
convert_jail = {}
for br, ics in getAll(sql, 'nodrizas'):
    convert_jail[ics] = br

indicadors_presents = set()
sql = """select z2, indicador, 'M', valor
        from eqa_ind.eqa_preso_ponderacio_anual"""
upload = set()
for preso, indi, m, v in getAll(sql, 'eqa_ind'):
    if preso in convert_jail:
        up = convert_jail[preso]
        indicadors_presents.add((indi, up))
        if v < 0: v = 0
        upload.add((up, indi, m, v, ''))

ups = set()
sql = """SELECT up, indicador FROM PRSEQAINDICADORS p WHERE dataany = 2024"""
for up, indi in getAll(sql, 'pdp'):
    ups.add(up)
    if (indi, up) not in indicadors_presents:
        upload.add((up, indi, 'M', 0, ''))
cols = "(up varchar(20), indicador varchar(20), tipus varchar(20), punts double, noimp varchar(1))"
createTable('prseqapunts', cols, 'eqa_ind', rm=True)
upload = list(upload)
listToTable(upload, 'prseqapunts', 'eqa_ind')
table = "prseqacatalegpunts"
query = "select * from eqa_ind.prseqapunts"
exportPDP(query=query, table=table, datAny=True, fmo=False)

upload = []
for up in ups:
    upload.append(('G', up, 'UAB', 'M', None, None, None, None, None, None, None, None, MMIN, MINT, MMAX))
cols = """(eqa varchar(1), up varchar(20), uab varchar(3), tipus varchar(1), basalany int, basalmes int, 
        basalvalororig int, mminroig int, mintorig int, mmaxorig int,
        datamodi date, basalvalor int, mmin int, mint int, mmax int)"""
createTable('prseqasinteticmetes', cols, 'eqa_ind', rm=True)
upload = list(upload)
listToTable(upload, 'prseqasinteticmetes', 'eqa_ind')
table = "prseqasinteticmetes"
query = "select * from eqa_ind.prseqasinteticmetes"
exportPDP(query=query, table=table, datAny=True, fmo=False)


query = "select * from eqa_ind.exp_ecap_cataleg_jail"
exportPDP(query=query, table='prseqacataleg', datAny=True, fmo=False)
query = "select * from eqa_ind.exp_ecap_cataleg_pare_jail"
exportPDP(query=query, table='prseqacatalegpare', datAny=True, fmo=False)