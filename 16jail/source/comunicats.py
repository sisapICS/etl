# coding: latin1

"""
Des de la creaci� de l'indicador les dades s'obtenien directament de 6951.
El 2022 migrem codi per� mantenim la font de dades (potser �s un tema
particularment confidencial?)
"""

import sisaptools as u
import sisapUtils as s


PACIENTS = "mst_comunicats_pacient"
RESULTATS = "mst_comunicats_up"
INDICADOR = "COMJUTJAT"
FILE = "COMUNICATS_JAIL"


class Comunicats(object):
    """."""

    def __init__(self):
        """."""
        sql = "select adddate(adddate(data_ext, interval - 1 year), \
                              interval + 1 day), data_ext \
               from dextraccio"
        dates = u.Database("p2262", "nodrizas").get_one(sql)
        self.dates = [dat.strftime("%Y-%m-%d") for dat in dates]

    def get_dades(self):
        """."""
        sql = "select usua_cip, usua_cip_cod from pdptb101"
        cip_to_hash = {cip: hash for (cip, hash)
                       in u.Database("6951", "pdp").get_all(sql)}
        sql = "select cip_usuari_cip, com_data, com_up, com_assist_per_fop, \
                      com_assist_per_altres, com_relat_pacient, \
                      com_desc_lesio, com_recom_observ, com_pronostic \
               from vistb018 a \
               inner join usutb011 b on a.com_cip = b.cip_cip_anterior \
               where com_data between date '{}' and \
                                      date '{}'".format(*self.dates)
        dades = {}        
        for cip, dat, up, fop, alt, pac, desc, obs, pron in u.Database("6951", "data").get_all(sql):  # noqa
            if cip in cip_to_hash:
                compleix = False
                if fop or alt:
                    if pac and desc and obs and pron:
                        if len(pac) > 10 and len(desc) > 10:
                            compleix = True
                key = (cip_to_hash[cip], INDICADOR, dat, up)
                if key not in dades or compleix > dades[key]:
                    dades[key] = compleix
        self.dades = [k + (1 * v,) for (k, v) in dades.items()]

    def set_masters(self):
        """."""
        # pacients
        cols = ("hash varchar(40)", "indicador varchar(10)", "data date",
                "up varchar(5)", "compleix int")
        with u.Database("p2262", "jail") as jail:
            jail.create_table(PACIENTS, cols, remove=True)
            jail.list_to_table(self.dades, PACIENTS)
        # resultats
        sql = "select up, ics_codi br, '{}' indicador, count(1) denominador, \
                      sum(compleix) numerador, avg(compleix) resultat \
               from {} a \
               inner join nodrizas.jail_centres b on a.up = b.scs_codi \
               group by up, ics_codi".format(INDICADOR, PACIENTS) 
        with u.Database("p2262", "jail") as jail:
            jail.drop_table(RESULTATS)
            jail.execute("create table {} as {}".format(RESULTATS, sql))

    def export_khalix(self):
        """."""
        sql = "select indicador, 'Aperiodo', br, 'NUM', 'NOCAT', 'NOIMP', \
                      'DIM6SET', 'N', numerador \
                from jail.{0} \
                union \
                select indicador, 'Aperiodo', br, 'DEN', 'NOCAT', 'NOIMP', \
                       'DIM6SET', 'N', denominador \
                from jail.{0}".format(RESULTATS)
        s.exportKhalix(sql, FILE)

    def export_ecap(self):
        """."""
        literal = "Comunicat de lesions al jutjat"
        pare = "Comunicats al jutjat"
        umi = "http://10.80.217.201/sisap-umi/indicador/codi/{}"
        params = [("uba", "indicador", INDICADOR,
                   "select up, 'UAB', indicador, numerador, denominador, \
                           resultat\
                    from {}".format(RESULTATS)),
                  ("pacient", "grup_codi", INDICADOR,
                   "select distinct 0, up, 'UAB', '', '', indicador, 0, hash, \
                                    '6951' \
                    from {} where compleix = 0".format(PACIENTS)),
                  ("cataleg", "indicador", INDICADOR,
                   "select '{0}', '{1}', 1, '{0}', 1, 0, 0, 0, 0, 1, \
                           concat('{2}', '{0}'), 'MI', 0 \
                    from dual".format(INDICADOR, literal, umi)),
                  ("catalegPare", "pare", INDICADOR,
                   "select '{}', '{}', 11, 0 \
                    from dual".format(INDICADOR, pare))]
        delete = "delete from exp_ecap_iep_{} where {} like '{}%'"
        insert = "insert into exp_ecap_iep_{} {}"
        with u.Database("p2262", "jail") as jail:
            for taula, columna, valor, sql in params:
                jail.execute(delete.format(taula, columna, valor))
                jail.execute(insert.format(taula, sql))


if __name__ == "__main__":
    comunicats = Comunicats()
    comunicats.get_dades()
    comunicats.set_masters()
    comunicats.export_khalix()
    comunicats.export_ecap()