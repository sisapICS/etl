#  coding: latin1

"""
Indicadors RS EAPP.
"""

import sisapUtils as u
import collections as c
import datetime as d
import dateutil.relativedelta

TABLE = "mst_indicadors_pacient_irs_eapp"
DATABASE = "jail"
FILE = "JAILRISCSUI"

LITERALS = (
    ("RS001", "Criteris d'inclusi� de risc de suicidi"),
    ("RS001B", "Criteris d'inclusi� B de risc de suicidi"),
    ("RS002", "Derivaci� a Salut Mental en risc elevat"),
    ("RS003", "Seguiment de risc elevat"),
    ("RS004", "Derivaci� a Salut Mental en risc moderat"),
    ("RS005", "Seguiment de risc moderat"),
    )


class IRS_EAPP(object):
    """."""

    def __init__(self):
        """."""

        self.get_dates()
        self.get_poblacio()
        self.get_xml()
        self.get_variables()
        self.get_rs001()
        self.get_data_derivacio()
        self.get_rs002()
        self.get_visites_psi()
        self.get_rs003()
        self.get_rs004()
        self.set_master()
        self.export_khalix()
        # self.delete_ecap()
        self.export_ecap_uba()
        self.export_ecap_pacient()
        self.export_ecap_cataleg()

    def get_dates(self):
        """ Obtenci� de les dates de refer�ncia per la desc�rrega de dades """

        sql = """
                SELECT
                    data_ext
                FROM
                    dextraccio
              """
        self.data_ext = u.getOne(sql, "nodrizas")[0]
        self.data_ext_menys1any = self.data_ext - dateutil.relativedelta.relativedelta(years=1) + dateutil.relativedelta.relativedelta(days=1)

    def get_poblacio(self):
        """ Selecci� de pacients que ingressen al centre els darrers 12 mesos """

        self.poblacio_ingressada_darrers_12_mesos_i_que_continua_o_ha_marxat = {}
        self.poblacio_que_continua_o_ha_marxat_darrers_12_mesos = {}
        self.poblacio_actualment_ingressada = {}

        sql = """
                SELECT
                    id_cip_sec,
                    ingres,
                    sortida,
                    up,
                    uba,
                    durada,
                    sortit
                FROM
                    jail_atesa_avui
              """

        # A cada individu li assignem nom�s la seva darrera data d'ingr�s
        for id_cip_sec, ini, fi, up, uba, durada, finalitzat in u.getAll(sql, "nodrizas"):
            if self.data_ext_menys1any <= ini <= self.data_ext:
                self.poblacio_ingressada_darrers_12_mesos_i_que_continua_o_ha_marxat[id_cip_sec] = (ini, up, uba, durada)
            if (finalitzat and self.data_ext_menys1any <= fi) or not finalitzat:
                self.poblacio_que_continua_o_ha_marxat_darrers_12_mesos[id_cip_sec] = (ini, up, uba, durada)
            if not finalitzat:
                self.poblacio_actualment_ingressada[id_cip_sec] = (ini, up, uba, durada)

    def get_xml(self):
        """ Dels individus de self.poblacio_que_continua_o_ha_marxat_darrers_12_mesos seleccionem nom�s els que tenen
            RiscSeleccionat a la variable XML0000083 de xml_detall
        """
        self.xml = c.defaultdict(dict)
        sql = """
                SELECT
                    id_cip_sec,
                    xml_data_alta,
                    camp_valor
                FROM
                    xml_detall,
                    nodrizas.dextraccio
                WHERE
                    xml_tipus_orig = 'XML0000083'
                    AND camp_codi = 'RiscSeleccionat'
                    AND xml_data_alta BETWEEN DATE_ADD(data_ext, INTERVAL -1 YEAR) AND data_ext
                ORDER BY
                    id_cip_sec,
                    xml_data_alta ASC
              """

        # A cada individu li assignem nom�s el primer valor despr�s
        # de la data d'ingr�s
        # S'obvien els que tenen data de RiscSeleccionat anterior a data ingr�s
        actual = 0
        for id_cip_sec, data, val in u.getAll(sql, "import_jail"):
            if id_cip_sec in self.poblacio_que_continua_o_ha_marxat_darrers_12_mesos:
                if data >= self.poblacio_que_continua_o_ha_marxat_darrers_12_mesos[id_cip_sec][0] and id_cip_sec != actual:
                    actual = id_cip_sec
                    self.xml[id_cip_sec] = (data, val)

        self.xml.default_factory = None

    def get_variables(self):
        """ Dels individus de self.poblacio_que_continua_o_ha_marxat_darrers_12_mesos seleccionem nom�s els que tenen
            RiscSeleccionat a la variable XML0000083 de xml_detall
        """
        self.variables = c.defaultdict(dict)
        sql = """
                SELECT
                    id_cip_sec,
                    vu_dat_act 
                FROM
                    variables
                WHERE
                    vu_cod_vs = 'VP5406'
                    AND vu_dat_act <= DATE'{}'
                ORDER BY 
                    1,
                    2 ASC
              """.format(self.data_ext)

        # A cada individu li assignem nom�s el primer valor despr�s
        # de la data d'ingr�s
        actual = 0
        for id_cip_sec, data in u.getAll(sql, "import_jail"):
            if id_cip_sec in self.poblacio_que_continua_o_ha_marxat_darrers_12_mesos:
                if data >= self.poblacio_que_continua_o_ha_marxat_darrers_12_mesos[id_cip_sec][0] and id_cip_sec != actual:
                    actual = id_cip_sec
                    self.variables[id_cip_sec] = (data)

        self.variables.default_factory = None

    def get_rs001(self):
        """
        Calcula indicador RS001
        Den: Individus amb RiscSeleccionat
        Num: Individus amb registre de prioritat els 7 primers dies d'ingr�s
        i RS001B
        Den: Individus amb RiscSeleccionat
        Num: Individus amb registre de prioritat fins els 6 mesos d'ingr�s
        """
        self.master = []
        val_risc = {'1', '2', '3'}
        cod_a = "RS001"
        cod_b = cod_a + "B"
        for id_cip_sec, (ini, up, uba, durada) in self.poblacio_que_continua_o_ha_marxat_darrers_12_mesos.items():
            if durada >= 7:

                num_a, num_b = 0, 0
                deadline = ini + d.timedelta(days=7)

                if id_cip_sec in self.variables:
                    data = self.variables[id_cip_sec]
                    if id_cip_sec in self.poblacio_ingressada_darrers_12_mesos_i_que_continua_o_ha_marxat:
                        num_a = 1 if ini <= data <= deadline else 0
                        self.master.append((id_cip_sec, ini, up, uba, cod_a, num_a, 1, 0))

                    num_b = 1 if ini <= data else 0
                    self.master.append((id_cip_sec, ini, up, uba, cod_b, num_b, 1, 0))
                else:
                    for cod in [cod_a, cod_b]:
                        self.master.append((id_cip_sec, ini, up, uba, cod, 0, 1, 0))

    def get_data_derivacio(self):
        """ Obtenim taula amb data derivaci� i servei de derivaci� per a cada
            pacient amb RiscSeleccionat
        """

        self.id_data = c.defaultdict(set)
        self.servei = {}
        self.data = {}
        self.dates_psi = c.defaultdict(set)

        # A 11 de juliol de 2019 nom�s hi ha 148 individus amb DataDerivacio
        # i 90 d'ells compleixen criteris d'inclusi�
        sql = "select id_cip_sec, camp_codi, camp_valor \
               from xml_detall, nodrizas.dextraccio \
               where xml_tipus_orig = 'XML0000083' \
               and camp_codi = 'DataDerivacio' \
               and xml_data_alta <= data_ext"
        for id_cip_sec, cod, val in u.getAll(sql, "import_jail"):
            if id_cip_sec in self.poblacio_actualment_ingressada:
                self.id_data[id_cip_sec].add(int(val))

        sql = "select inf_numid, inf_servei_d_codi from oc2"
        for id, servei in u.getAll(sql, "import_jail"):
            # '50115' �s el camp de text del servei de psiquiatria
            if servei == '50115':
                servei = 'PSI'
            self.servei[id] = (servei)

        sql = "select oc_numid, oc_data from oc1"
        for id, data in u.getAll(sql, "import_jail"):
            if id in self.servei:
                self.data[id] = (data, self.servei[id])

        for id_cip_sec, camps_valors in self.id_data.items():
            for camp_valor in camps_valors:
                if camp_valor in self.data:
                    data, val = self.data[camp_valor]
                    if val == 'PSI':
                        self.dates_psi[id_cip_sec].add(data)

    def get_rs002(self):
        """
        Calcula indicador RS002
        Den:
            Individus amb RiscSeleccionat '3', prioritat URGENT
        Num:
            Individus amb derivaci� a psiquiatria el mateix dia de la prioritat
        """
        RiscSeleccionat = {'3'}

        # sql = "select data_ext from {}.dextraccio;".format("nodrizas")
        # data_ext = u.getOne(sql, "nodrizas")[0]

        for id_cip_sec in self.poblacio_actualment_ingressada:
            data_ingres = self.poblacio_actualment_ingressada[id_cip_sec][0]
            if id_cip_sec in self.xml:
                prior = self.xml[id_cip_sec][1]
                if prior in RiscSeleccionat:
                    den = 1
                    exc_den = 0
                    data_registre = self.xml[id_cip_sec][0]
                    # Hi ha individus a self.xml que no tenen data de derivaci�
                    dates_psi = self.dates_psi.get(id_cip_sec, set())
                    num = 0
                    if any(data_psi == data_registre for data_psi in dates_psi):
                        num = 1
                    this = (id_cip_sec,
                            data_ingres,
                            self.poblacio_actualment_ingressada[id_cip_sec][1],
                            self.poblacio_actualment_ingressada[id_cip_sec][2],
                            "RS002",
                            num,
                            den,
                            exc_den)
                    self.master.append(this)

    def get_visites_psi(self):
        """ Obtenim taula amb dates de visita a psiquiatria posteriors a OC
        """

        self.data_visita7_psi = set()
        self.data_visita1_psi = set()

        sql = """
            SELECT id_cip_sec,
                visi_data_visita
            FROM visites,
                 nodrizas.dextraccio
            WHERE s_espe_codi_especialitat  = '10147'
                and visi_situacio_visita = 'R'
                and visi_data_visita <= data_ext
            ORDER BY 1,
                2 asc
        """
        # �s condici� que l'ordre cl�nica es faci en les 24 hores posteriors a l'ingr�s
        for id_cip_sec, data in u.getAll(sql, "import_jail"):
            if id_cip_sec in self.dates_psi:
                data_ingres = self.poblacio_que_continua_o_ha_marxat_darrers_12_mesos[id_cip_sec][0]
                for data_derivacio in self.dates_psi[id_cip_sec]:
                    if 0 <= u.daysBetween(data_ingres, data_derivacio) <= 1:
                        if 0 <= u.daysBetween(data_derivacio, data) <= 7:
                            self.data_visita7_psi.add(id_cip_sec)
                            if 0 <= u.daysBetween(data_derivacio, data) <= 1:
                                self.data_visita1_psi.add(id_cip_sec)

    def get_rs003(self):
        """
        Calcula indicadors RS003 i RS005

        RS003
        Den:
            Individus amb RiscSeleccionat '3', prioritat URGENT i data OC darrers 6 mesos
        Num:
            Individus amb visita m�dul psiquiatria el mateix dia de la prioritat

        RS005
        Den:
            Individus amb RiscSeleccionat '2', prioritat PREFERENT i data OC darrers 6 mesos
        Num:
            Individus amb visita m�dul psiquiatria m�xim 7 dies despr�s d'OC
        """
        RiscSeleccionat = {'2', '3'}
        # rs005 RiscSeleccionat 2 i 7 dies

        sql = "select data_ext from {}.dextraccio;".format("nodrizas")
        data_ext = u.getOne(sql, "nodrizas")[0]

        for id_cip_sec in self.poblacio_actualment_ingressada:
            den = 1
            exc_den = 0
            data_ingres = self.poblacio_actualment_ingressada[id_cip_sec][0]
            if id_cip_sec in self.xml:
                prior = self.xml[id_cip_sec][1]
                if prior in RiscSeleccionat:
                    # data_registre �s la data de la prioritat
                    # data_registre = self.xml[id_cip_sec][0]

                    # Hi ha individus a self.xml que no tenen data de derivaci�
                    dates_der = self.dates_psi.get(id_cip_sec, set())
                    data_registre = self.xml[id_cip_sec][0]

                    # data OC en els darrers 12 mesos
                    if any(data_der != 0 and 0 <= u.daysBetween(data_registre, data_der) <= 1 for data_der in dates_der):
                        num1 = 0
                        num2 = 0
                        # data_visita1_psi dataset
                        # amb individus amb visita m�dul psiquiatria
                        # el mateix dia o posterior a ordre cl�nica

                        # data_visita7_psi dataset
                        # amb individus amb visita m�dul psiquiatria
                        # fins als 7 dies posteriors a ordre cl�nica

                        if prior == '3':
                            if id_cip_sec in self.data_visita1_psi:
                                num1 = 1
                            self.master.append(
                                (id_cip_sec,
                                 data_ingres,
                                 self.poblacio_actualment_ingressada[id_cip_sec][1],
                                 self.poblacio_actualment_ingressada[id_cip_sec][2],
                                 "RS003",
                                 num1,
                                 den,
                                 exc_den)
                                )

                        if prior == '2':
                            if id_cip_sec in self.data_visita7_psi:
                                num2 = 1
                            self.master.append(
                                (id_cip_sec,
                                 data_ingres,
                                 self.poblacio_actualment_ingressada[id_cip_sec][1],
                                 self.poblacio_actualment_ingressada[id_cip_sec][2],
                                 "RS005",
                                 num2,
                                 den,
                                 exc_den)
                                )

    def get_rs004(self):
        """
        Calcula indicador RS004
        Den:
            Individus amb RiscSeleccionat '2',
            prioritat PREFERENT darrers 6 mesos
        Num:
            Individus amb derivaci� a psiquiatria el mateix dia de la prioritat
        """
        RiscSeleccionat = {'2'}
        for id_cip_sec in self.poblacio_actualment_ingressada:
            if id_cip_sec in self.xml.keys() and self.xml[id_cip_sec][1] in RiscSeleccionat:

                data_ingres = self.poblacio_actualment_ingressada[id_cip_sec][0]
                data_registre = self.xml[id_cip_sec][0]
                dates_psi = self.dates_psi.get(id_cip_sec, set())

                exc_den = 0
                den = 1    
                num = 1 if any(data_psi == data_registre for data_psi in dates_psi) else 0

                self.master.append(
                    (id_cip_sec,
                    data_ingres,
                    self.poblacio_actualment_ingressada[id_cip_sec][1],
                    self.poblacio_actualment_ingressada[id_cip_sec][2],
                    "RS004",
                    num,
                    den,
                    exc_den)
                    )

    def set_master(self):
        """."""
        cols = "(id int, dat date, up varchar(5), uba varchar(5), \
                 ind varchar(10), num int, den int, exc_den int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.master, TABLE, DATABASE)

    def export_khalix(self):
        """."""
        base = """
                SELECT
                    ind,
                    'Aperiodo',
                    ics_codi,
                    '{0}',
                    'NOCAT',
                    'NOIMP',
                    'DIM6SET',
                    'N',
                    sum({0})
                FROM
                    {1}.{2} a
                INNER JOIN nodrizas.jail_centres b ON
                    a.up = b.scs_codi
                WHERE
                    exc_den = 0
                GROUP BY
                    ind,
                    ics_codi
               """
        sql = " union ".join([base.format(analysis, DATABASE, TABLE)
                              for analysis in ("NUM", "DEN")])
        u.exportKhalix(sql, FILE)

    def delete_ecap(self):
        """."""
        params = [("uba", "indicador", "RS0"),
                  ("pacient", "grup_codi", "RS0"),
                  ("cataleg", "indicador", "RS0"),
                  ("catalegPare", "pare", "RISCSUI")]
        delete = "delete from exp_ecap_iep_{} where {} like '{}%'"
        for param in params:
            sql = delete.format(*param)
            u.execute(sql, DATABASE)

    def export_ecap_uba(self):
        """."""
        sql = """
                SELECT
                    up,
                    uba,
                    ind,
                    sum(num),
                    sum(den),
                    sum(num) / sum(den)
                FROM
                    {}
                WHERE
                    exc_den = 0
                GROUP BY
                    up,
                    uba,
                    ind
              """.format(TABLE)
        upload = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload, "exp_ecap_iep_uba", DATABASE)

    def export_ecap_pacient(self):
        """."""
        sql = """
                SELECT DISTINCT
                    id,
                    up,
                    uba,
                    '',
                    '',
                    ind,
                    0,
                    hash_d,
                    codi_sector
                FROM
                    {} a
                INNER JOIN import_jail.u11 b ON
                    id = id_cip_sec
                WHERE
                    num = 0
                    AND exc_den = 0
              """.format(TABLE)
        upload2 = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload2, "exp_ecap_iep_pacient", DATABASE)

    def export_ecap_cataleg(self):
        """."""
        umi = "http://10.80.217.201/sisap-umi/indicador/codi/{}"
        upload = [
            (cod,
             des,
             i + 1,
             "RISCSUI",
             1, 0, 0, 0, 0, 1,
             umi.format(cod),
             "MI",
             0)
            for i, (cod, des) in enumerate(LITERALS)
            ]
        u.listToTable(upload, "exp_ecap_iep_cataleg", DATABASE)
        pare = [("RISCSUI", "Risc Suicidi", 10, 0)]
        u.listToTable(pare, "exp_ecap_iep_catalegPare", DATABASE)


if __name__ == "__main__":
    IRS_EAPP()