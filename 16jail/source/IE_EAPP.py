# coding: latin1

"""
Indicadors IE EAPP.
"""

import collections as c
import datetime as d

import sisapUtils as u

TABLE = "mst_indicadors_pacient_ie_eapp"
DATABASE = "jail"
FILE = "JAILINFENLL"

INDICADORS = {
    "IE002": {
        "den": ["PVVisHosp"],
        "num": ["PVVisHospD", "PVVisHospD2", "PVVisHospD3"],
        "poblacio_out": False},
    "IE003A": {
        "den": ["PVVisCSM"],
        "num": ["PVConPsiqD", "PV_CtePsiquiatre"],
        "poblacio_out": False},
    "IE003B": {
        "den": ["PV_CtePsiquiatre"],
        "num": ["PVVisSMD"],
        "poblacio_out": False},
    "IE004": {
        "den": ["PVVisCas"],
        "num": ["PVServMetadD"],
        "poblacio_out": False},
    "IE005": {
        "den": ["PVVisInfermerAP", "PVVisMetgeAP"],
        "num": ["PVVisMetgeD", "PVVisInfermD"],
        "poblacio_out": False},
    "IE006": {
        "num": ["PVVisHospD", "PVVisHospD2", "PVVisHospD3"],
        "ass": ["SVAsistHospit", "SVAsistHospit2", "SVAsistHospit3"],
        "poblacio_out": True},
    "IE007": {
        "num": ["PVVisSMD"],
        "ass": ["SVAsistSM"],
        "poblacio_out": True},
    "IE008": {
        "num": ["PVServMetadD"],
        "ass": ["SVAsistCASS"],
        "poblacio_out": True},
    }

INDICADOR_009 = {"num": ["PVVisMetgeD", "PVVisInfermD"]}
EXCLUSIONS = ("NOforaCAT",
              "NOExpulsio",
              "NORecollit",
              "NODemorada",
              "NOAnticipada",
              )
LITERALS = (
    ("IE001", "Criteris d'inclusi� d'infermera d'enlla�"),
    ("IE002", "Programaci� de visita de seguiment hospital�ria"),
    ("IE003A", "Contactats i valorats pel psiquiatre de l'EAPP"),
    ("IE003B", "Programats a Salut Mental"),
    ("IE004", "Programaci� de visita de seguiment al CAS"),
    ("IE005", "Programaci� de visita de seguiment a l'atenci� prim�ria"),
    ("IE006", "Assistencia a hospital"),
    ("IE007", "Assistencia a XSM"),
    ("IE008", "Assistencia a CAS"),
    ("IE009", "Assistencia a AP"),
    )


class EAPP(object):
    """."""

    def __init__(self):
        """."""
        self.get_poblacio()
        self.get_xml()
        self.get_exclosos()
        self.get_consentits()
        self.get_ie001()
        self.get_ie009()
        self.get_components()
        self.set_master()
        self.export_khalix()
        self.delete_ecap()
        self.export_ecap_uba()
        self.export_ecap_pacient()
        self.export_ecap_cataleg()

    def get_poblacio(self):
        """Assignem a cada pacient a l'�ltim EAPP on ha estat durant l'any."""
        self.poblacio = {}
        sql = """
            select
                id_cip_sec,
                huab_data_ass,
                huab_data_final,
                huab_up_codi,
                huab_uab_codi
            from
                moviments,
                nodrizas.dextraccio
            where
                huab_data_ass <= data_ext and
                (huab_data_final = 0 or
                 huab_data_final > adddate(data_ext, interval -1 year)
                )
            order by
                1,
                2 desc,
                3 desc
        """
        actual = 0
        for id, ini, fi, up, uba in u.getAll(sql, "import_jail"):
            if id != actual:
                self.poblacio[id] = (up, uba, fi)
                actual = id

        self.poblacio_out = self.poblacio.copy()
        for id, ini, fi, up, uba in u.getAll(sql, "import_jail"):
            if fi is None and id in self.poblacio_out:
                del(self.poblacio_out[id])

        # Afegim el id_cip de fora de press� a self.poblacio_out

        for id, _ in self.poblacio_out.items():
            self.poblacio_out[id] += (None,)

        id_hash = {}
        sql = """
            select id_cip_sec, hash_d from u11_all where id_cip_sec in {}
            """.format(str(tuple(self.poblacio_out.keys())).replace("L,", ",").replace("L)", ")"))  # noqa
        for id, hash_d in u.getAll(sql, "import_jail"):
            id_hash[hash_d] = id

        actual = 0
        sql = """
            select id_cip, hash_d from u11_all where hash_d in {} order by 1
            """.format(tuple(id_hash.keys()))
        for id, hash_d in u.getAll(sql, "import"):
            if id != actual:
                dades_llista = list(self.poblacio_out[id_hash[hash_d]])
                dades_llista[3] = id
                self.poblacio_out[id_hash[hash_d]] = tuple(dades_llista)
                actual = id

    def get_xml(self):
        """."""
        self.xml = c.defaultdict(dict)
        sql = "select id_cip_sec, xml_data_alta, camp_codi, camp_valor \
               from xml_detall, nodrizas.dextraccio \
               where xml_tipus_orig = 'ENLL_PRES' and \
                     xml_data_alta between adddate(data_ext, interval -1 year)\
                                           and data_ext"
        for id, dat, cod, val in u.getAll(sql, "import_jail"):
            if id in self.poblacio:
                self.xml[(id, dat)][cod] = val

    def get_exclosos(self):
        """."""
        self.exclosos = set()
        for key, dades in self.xml.items():
            if any([dades.get(camp) == "S" for camp in EXCLUSIONS]):
                self.exclosos.add(key)

    def get_consentits(self):
        """."""
        self.consentits = {}
        for key, dades in self.xml.items():
            if dades.get("PVInfSigCI") == "S":
                self.consentits[key] = dades.get("PVCIEcap")

    def get_ie001(self):
        """."""
        self.master = []
        rca = c.defaultdict(set)
        sql = "select id_cip_sec, com22_data_loc from eapp_usutb196a, nodrizas.dextraccio where com22_data_loc <= data_ext"
        for id, dat in u.getAll(sql, "import_jail"):
            rca[id].add(dat)
        for key, ci in self.consentits.items():
            if ci:
                dci = d.datetime.strptime(ci, "%d/%m/%Y").date()
                dates = rca[key[0]]
                up, uba, _ = self.poblacio[key[0]]
                den = 1
                num = 1 * any([(drca - dci).days < 15 for drca in dates])
                exc = 1 * (key in self.exclosos)
                self.master.append((key[0], key[1], up, uba, "IE001", den, num, exc))  # noqa

    def get_ie009(self):
        """."""
        # Visites a l'exterior
        id_out_visites = {
            id_out: set()
            for id_in, (up, uba, fi, id_out) in self.poblacio_out.items()
            if id_out is not None
        }
        sql = """
            select id_cip,
                visi_data_visita
            from visites2, nodrizas.dextraccio
            where id_cip in {}
            and visi_data_visita between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext
            """.format(str(tuple(id_out_visites.keys())).replace("L,", ",").replace("L)", ")"))  # noqa
        for id_out, dat in u.getAll(sql, "import"):
            id_out_visites[id_out].add(dat)

        for key in self.consentits:
            if key[0] in self.poblacio_out:
                dades = self.xml[key]
                up, uba, fi, id_out = self.poblacio_out[key[0]]
                num = 1 * any([dades.get(codi) for codi in INDICADOR_009["num"]])  # noqa
                visites = set()
                if id_out is not None:
                    visites = id_out_visites[id_out]
                ass = 1 * any([u.monthsBetween(fi, dat) == 0 for dat in visites])  # noqa
                exc = 1 * (key in self.exclosos)
                if num:
                    this = (key[0], key[1], up, uba, "IE009", num, ass, exc)
                    self.master.append(this)

    def get_components(self):
        """."""
        for key in self.consentits:
            dades = self.xml[key]
            up, uba, fi = self.poblacio[key[0]]
            for indicador, components in INDICADORS.items():
                poblacio_out = components["poblacio_out"]
                num = 1 * any([dades.get(codi) for codi in components["num"]])
                exc = 1 * (key in self.exclosos)
                if poblacio_out:
                    ass = 1 * any([dades.get(codi) == "S" for codi in components["ass"]])  # noqa
                    if num and key[0] in self.poblacio_out:
                        self.master.append((key[0], key[1], up, uba, indicador, num, ass, exc))  # noqa
                else:
                    den = 1 * any([dades.get(codi) == "S" for codi in components["den"]])  # noqa
                    if den:
                        self.master.append((key[0], key[1], up, uba, indicador, den, num, exc))  # noqa

    def set_master(self):
        """."""
        cols = "(id int, dat date, up varchar(5), uba varchar(5), \
                 ind varchar(6), den int, num int, excl int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.master, TABLE, DATABASE)

    def export_khalix(self):
        """."""
        base = "select ind, 'Aperiodo', ics_codi, '{0}', \
                       'NOCAT', 'NOIMP', 'DIM6SET', 'N', sum({0}) \
                from {1}.{2} a \
                inner join nodrizas.jail_centres b on a.up = b.scs_codi \
                where excl = 0 \
                group by ind, ics_codi"
        sql = " union ".join([base.format(analysis, DATABASE, TABLE)
                              for analysis in ("NUM", "DEN")])
        u.exportKhalix(sql, FILE)

    def delete_ecap(self):
        """."""
        params = [("uba", "indicador", "IE0"),
                  ("pacient", "grup_codi", "IE0"),
                  ("cataleg", "indicador", "IE0"),
                  ("catalegPare", "pare", "AGENLLAC")]
        delete = "delete from exp_ecap_iep_{} where {} like '{}%'"
        for param in params:
            sql = delete.format(*param)
            u.execute(sql, DATABASE)

    def export_ecap_uba(self):
        """."""
        sql = "select up, uba, ind, sum(num), sum(den), sum(num) / sum(den) \
               from {} \
               where excl = 0 \
               group by up, uba, ind".format(TABLE)
        upload = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload, "exp_ecap_iep_uba", DATABASE)

    def export_ecap_pacient(self):
        """."""
        sql = "select distinct id, up, uba, '', '', ind, 0, \
                               hash_d, codi_sector \
               from {} a \
               inner join import_jail.u11 b on id = id_cip_sec \
               where num = 0 and \
                     excl = 0".format(TABLE)
        upload = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload, "exp_ecap_iep_pacient", DATABASE)

    def export_ecap_cataleg(self):
        """."""
        umi = "http://10.80.217.201/sisap-umi/indicador/codi/{}"
        upload = [(cod,
                   des,
                   i + 1,
                   "AGENLLAC",
                   1, 0, 0, 0, 0, 1,
                   umi.format(cod),
                   "I",
                   0)
                  for i, (cod, des) in enumerate(LITERALS)]
        u.listToTable(upload, "exp_ecap_iep_cataleg", DATABASE)
        pare = [("AGENLLAC", "Infermera enlla�", 8, 0)]
        u.listToTable(pare, "exp_ecap_iep_catalegPare", DATABASE)


if __name__ == "__main__":
    EAPP()
