# coding: iso-8859-1
import sisapUtils as u
from collections import *

import sys, os.path
path = os.path.realpath("./") + "/source"
sys.path.append(path)
from jail_utils import *


class JAIL_TABAC():

    def __init__(self):
        """."""

        self.dextraccio();                              print("self.dextraccio()")
        self.get_centres();                             print("self.get_centres()")
        self.get_poblacio();                            print("self.get_poblacio()")
        self.get_problemes();                           print("self.get_problemes()")
        self.get_variables();                           print("self.get_variables()")
        self.get_tractaments();                         print("self.get_tractaments()")
        self.get_placures_alta_anual_deixarfumar();     print("self.get_placures_alta_anual_deixarfumar()")
        self.get_indicadors();                          print("self.get_indicadors()")
        self.export_jail();                             print("self.export_jail()")

    def dextraccio(self):
        """."""

        sql = """
                SELECT
                    data_ext,
                    date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY)
                FROM
                    dextraccio
              """
        self.dext, self.dext_menys1any = u.getOne(sql, "nodrizas")

    def get_centres(self):

        sql = """
                SELECT
                    scs_codi,
                    ics_codi
                FROM
                    jail_centres
            """
        self.centres = {up: br for up, br in u.getAll(sql, "nodrizas")}

    def get_poblacio(self):
        """Calcula nombre de dies a pres� darrer any"""

        sql = """
                SELECT
                    id_cip_sec,
                    ingres,
                    up,
                    uba
                FROM
                    jail_atesa_avui
                WHERE 
                    durada >= 7
                    AND (sortida = '0000-00-00' OR sortida BETWEEN '{}' AND '{}')
            """.format(self.dext_menys1any, self.dext)
        poblacio_ultim_any = {id_cip_sec: {"up": up, "br": self.centres[up], "uba": uba, "ingres": ingres} for id_cip_sec, ingres, up, uba in u.getAll(sql, "nodrizas") if up in self.centres}

        sql = """
                SELECT
                    id_cip_sec,
                    usua_data_naixement,
                    usua_sexe
                FROM
                    assignada
            """
        self.poblacio = {id_cip_sec: (poblacio_ultim_any[id_cip_sec]["up"], poblacio_ultim_any[id_cip_sec]["uba"], '', '', u.yearsBetween(data_naix, self.dext), sexe)
                    for id_cip_sec, data_naix, sexe in u.getAll(sql, "import_jail") if id_cip_sec in poblacio_ultim_any}

    def get_problemes(self):
        """."""

        self.problemes = defaultdict(set)

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    problemes,
                    nodrizas.dextraccio
                WHERE
                    pr_cod_ps IN (SELECT criteri_codi FROM nodrizas.eqa_criteris WHERE agrupador IN (998))
                    AND (pr_dba > '{}' OR pr_dba = '0000-00-00')
              """.format(self.dext)
        for id_cip_sec, in u.getAll(sql, "import_jail"):
            if id_cip_sec in self.poblacio:
                self.problemes["Dx Tabaquisme"].add(id_cip_sec)              

    def get_variables(self):
        """."""

        self.variables = defaultdict(lambda: defaultdict(dict))
        self.variables["Ara Fuma: S�"] = set()
        self.variables["Intervenci� breu de tabac"] = set()

        sql = """
                SELECT
                    id_cip_sec,
                    vu_cod_vs,
                    vu_dat_act,
                    vu_val
                FROM
                    variables
                WHERE
                    vu_cod_vs IN ('EP2700', 'CP271')
                    and vu_dat_act <= '{}'
              """.format(self.dext)
        for id_cip_sec, codi_var, data, valor in u.getAll(sql, "import_jail"):
            if id_cip_sec in self.poblacio:
                self.variables[codi_var][id_cip_sec][data] = valor

        for id_cip_sec in self.variables["EP2700"]: 
            data_max = max(self.variables["EP2700"][id_cip_sec].keys())
            if self.variables["EP2700"][id_cip_sec][data_max] == 1:
                self.variables["Ara Fuma: S�"].add(id_cip_sec)
        for id_cip_sec in self.variables["CP271"]:
            for data in self.variables["CP271"][id_cip_sec]:
                if data >= self.dext_menys1any and self.variables["CP271"][id_cip_sec][data] == 6:
                    self.variables["Intervenci� breu de tabac"].add(id_cip_sec)

    def get_tractaments(self):
        """."""

        self.tractaments = defaultdict(set)

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    import_jail.tractaments
                WHERE 	
                    ppfmc_atccodi IN ('N07BA01', 'N07BA04', 'N07BA03')
                    AND ppfmc_pmc_data_ini BETWEEN DATE'{}' AND DATE'{}'
              """.format(self.dext_menys1any, self.dext)
        for id_cip_sec, in u.getAll(sql, "import_jail"):
            if id_cip_sec in self.poblacio:
                self.tractaments["Deshabituaci� farmacol�gica amb nicotina i/o citisina"].add(id_cip_sec)

    def get_placures_alta_anual_deixarfumar(self):
        """."""

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    master_placures_jail
                WHERE
                    flag_pc_alta_anual_deixarfumar = 1
              """
        self.alta_anual_deixarfumar = set([id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")])

    def get_indicadors(self):
        """."""

        # Inicialitzaci� de variables
        self.master = set()

        cond_ind = {"IEP0801": {"dens": self.variables["Ara Fuma: S�"],
                                "nums": self.problemes["Dx Tabaquisme"],
                                "excls": set()},
                    "IEP0802": {"dens": self.variables["Ara Fuma: S�"],
                                "nums": self.variables["Intervenci� breu de tabac"],
                                "excls": set()},
                    "IEP0803": {"dens": self.variables["Ara Fuma: S�"],
                                "nums": self.alta_anual_deixarfumar,
                                "excls": set()},
                    "IEP0804": {"dens": self.problemes["Dx Tabaquisme"],
                                "nums": self.tractaments["Deshabituaci� farmacol�gica amb nicotina i/o citisina"],
                                "excls": set()},
                    }

        # Iteraci� sobre els indicadors i c�lcul dels resultats
        for ind in cond_ind:
            # Per a cada pacient que compleixi les condicions del denominador de l'indicador
            for id_cip_sec in cond_ind[ind]["dens"]:
                if id_cip_sec in self.poblacio:
                    # Recuperar informaci� b�sica del pacient
                    up, uba, upinf, ubainf, edat, sexe = self.poblacio[id_cip_sec]
                    # Assignar 1 si el pacient compleix les condicions del numerador, 0 en cas contrari
                    num = 1 if id_cip_sec in cond_ind[ind]["nums"] else 0
                    # Assignar 1 si el pacient compleix les condicions d'exclusi� del denominador, 0 en cas contrari
                    excl = 1 if id_cip_sec in cond_ind[ind]["excls"] else 0
                    # Emmagatzemar les dades dels pacients en el conjunt 'self.master'
                    self.master.add((id_cip_sec, ind, up, uba, upinf, ubainf, edat, sexe, num, 1, excl))

    def export_jail(self):
        """."""

        cols = "(id_cip_sec int,  indicador varchar(10), up varchar(5) not null default'', uba varchar(5) not null default'',upinf varchar(5) not null default'', ubainf varchar(5) not null default'' \
                    ,edat int, sexe varchar(1) not null default'',num double, den double, excl double)"
        createTable(tabletabac, cols, db, rm=True)
        u.listToTable(self.master, tabletabac, db)

if __name__ == "__main__":
    JAIL_TABAC()