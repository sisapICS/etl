# coding: latin1

import collections as c
import sisapUtils as u
import itertools as it
import dateutil.relativedelta

MASTER_TABLE = "mst_indicadors_fetgegras_eapp"
DATABASE = "jail"
FILE = "FETGEGRASEAPP"

LITERALS = (
    ("FGNA1", "Utilitzaci� del FLI (fatty liver index) en pacients que presenten algun criteri de s�ndr. metab�lica"),
    ("FGNA2", "Utilitzaci� del FIB4 en pacients que presenten algun criteri de sindr. metab�lica i un �ndex FLI>=60"),
    ("FGNA3", "Realitzar elastografia pacients amb signes ecogr�fics esteatosi hep�tica i/o valors risc FLI i FIB4"),
)

# Consulta SQL per obtenir la data d'extracci�
sql = """
        SELECT
            data_ext
        FROM
            dextraccio
      """
# Obtenir la data d'extracci�
data_ext, = u.getOne(sql, "nodrizas")

# Calcular dates relatives a la data d'extracci�
data_ext_menys1any = data_ext - dateutil.relativedelta.relativedelta(years=1)
data_ext_menys13mesos = data_ext - dateutil.relativedelta.relativedelta(months=13)
data_ext_menys2anys = data_ext - dateutil.relativedelta.relativedelta(years=2)

class FGNA(object):
    """Classe per gestionar els indicadors de fetge gras en EAPP."""

    def __init__(self):
        """Inicialitza la classe FGNA i executa totes les funcions necess�ries."""

        self.resultat, self.pacients, self.literals = list(), dict(), dict()

        self.get_poblacio();                        print("self.get_poblacio()")
        self.get_problemes();                       print("self.get_problemes()")
        self.get_variables();                       print("self.get_variables()")
        self.get_indicadors();                      print("self.get_indicadors()")
        self.export_dades();                        print("self.export_dades()")

        self.set_master();                          print("self.set_master()")
        self.export_khalix();                       print("self.export_khalix()")
        self.delete_ecap();                         print("self.delete_ecap()")
        self.export_ecap_uba();                     print("self.export_ecap_uba()")
        self.export_ecap_pacient();                 print("self.export_ecap_pacient()")
        self.export_ecap_cataleg();                 print("self.export_ecap_cataleg()")

    def get_poblacio(self):
        """Obt� informaci� dels pacients."""

        dates = c.defaultdict(list)
        self.poblacio = dict()

        def get_nombre_dies_consecutius_preso(llista_dates):
            """Calcula el nombre de dies consecutius a la pres� a partir d'una llista de dates d'ingr�s i sortida."""
            
            llista_dates.sort(key=lambda x: x[1])
            ultima_data_final = llista_dates[-1][1]
            primera_data_inicial = next((llista_dates[i][0] for i in range(len(llista_dates) - 1, 0, -1) if llista_dates[i][0] > llista_dates[i - 1][1]), llista_dates[0][0])
            duracio = u.daysBetween(primera_data_inicial, ultima_data_final) + 1
            return duracio

        self.poblacio = {}

        # Consulta SQL per obtenir informaci� dels pacients
        sql = """
                SELECT
                    a.id_cip_sec,
                    up,
                    uba,
                    upinf,
                    ubainf,
                    CASE WHEN huab_data_ass < DATE'{_data_ext_menys1any}' THEN DATE'{_data_ext_menys1any}'
                         ELSE huab_data_ass
                      END AS huab_data_ass,
                    CASE WHEN huab_data_final = '0000-00-00' THEN DATE'{_data_ext}'
                         ELSE huab_data_final
                      END AS huab_data_final,
                    edat
                FROM
                    moviments a,
                    nodrizas.jail_assignada b
                WHERE
                    a.id_cip_sec = b.id_cip_sec
                    AND huab_data_ass <= DATE'{_data_ext}'
                    AND (huab_data_final = '0000-00-00' OR huab_data_final > DATE'{_data_ext_menys1any}')
              """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
        
        # Processar els resultats de la consulta SQL
        for id_cip_sec, up, uba, up_inf, uba_inf, data_ingres, data_sortida, edat in u.getAll(sql, 'import_jail'):
            dates[id_cip_sec].append((data_ingres, data_sortida))
            self.poblacio[id_cip_sec] = {"up": up, "uba": uba, "up_inf": up_inf, "uba_inf": uba_inf, "edat": edat}

        # Calcular la duraci� de la pres� per cada pacient
        for id_cip_sec in dates:
            self.poblacio[id_cip_sec]["duracio"] = get_nombre_dies_consecutius_preso(dates[id_cip_sec])

    def get_problemes(self):
        """Obt� els problemes de salut dels pacients."""

        self.problemes = c.defaultdict(set)

        problemes_agrupadors = {18: "DM2",
                                47: "Hipercolesterolemia",
                                55: "HTA",
                                168: "Cirrosi hep�tica",
                                239: "Obesitat"}

        codis_ps = tuple()
        codis_agrupadors_ps = dict()
        
        # Consulta SQL per obtenir els codis de problemes
        sql = """
                SELECT
                    agrupador,
                    criteri_codi 
                FROM
                    eqa_criteris
                WHERE
                    agrupador IN {_problemes_agrupadors}
              """.format(_problemes_agrupadors=tuple(problemes_agrupadors.keys()))
        for agrupador, codi_ps in u.getAll(sql, "nodrizas"):
            codi_ps = codi_ps.replace("C01-", "")
            codis_ps += (codi_ps,)
            codis_agrupadors_ps[codi_ps] = problemes_agrupadors[agrupador]

        # Codis diagn�stics sense agrupador
        codis_sense_agrupador_ps = {"K75.81": "Esteatosi hep�tica"}

        for codi_ps, desc in codis_sense_agrupador_ps.items():
            codis_ps += (codi_ps,)
            codis_agrupadors_ps[codi_ps] = desc

        # Crear una cadena de consulta amb codis ps
        codis_ps_codi_in = "("

        for ps_codi in codis_ps:
            codis_ps_codi_in += "'{_ps_codi}', ".format(_ps_codi=ps_codi)
            if "C01" not in ps_codi:
                codis_ps_codi_in += "'C01-{_ps_codi}', ".format(_ps_codi=ps_codi)

        codis_ps_codi_in = codis_ps_codi_in[:-2] + ")"

        # Consulta SQL per obtenir els problemes dels pacients
        sql = """
                SELECT
                    id_cip_sec,
                    pr_cod_ps
                FROM
                    problemes
                WHERE
                    pr_cod_ps IN {_codis_ps_codi_in}
                    AND (pr_dba > '{_data_ext}' OR pr_dba = '0000-00-00')
              """.format(_codis_ps_codi_in=codis_ps_codi_in, _data_ext=data_ext)
        for id_cip_sec, ps_codi in u.getAll(sql, "import_jail"):
            # Comprovar si el pacient est� dins la poblaci�
            if id_cip_sec in self.poblacio:
                # Afegir al conjunt de problemes
                ps_codi = ps_codi.replace("C01-", "")
                agrupador = codis_agrupadors_ps[ps_codi]
                self.problemes[agrupador].add(id_cip_sec)

    def get_variables(self):
        """Obt� les variables cl�niques dels pacients."""

        self.variables = c.defaultdict(dict)

        codis_sense_agrupador_vs = {"VD5004": "Index FLI",
                                    "VD5005": "FIB-4",
                                    "YC0001": "Fibroscan"}
        
        codis_vs = tuple()
        codis_agrupadors_vs = dict()
        for codi_vs, desc in codis_sense_agrupador_vs.items():
            codis_vs += (codi_vs,)
            codis_agrupadors_vs[codi_vs] = desc

        # Consulta SQL per obtenir les variables cl�niques dels pacients
        sql = """
                SELECT
                    id_cip_sec,
                    vu_cod_vs,
                    vu_val,
                    vu_dat_act
                FROM
                    variables
                WHERE
                    vu_cod_vs IN {_codis_vs}
              """.format(_codis_vs=str(tuple(codis_vs)))

        # Processar els resultats de la consulta SQL
        for id_cip_sec, codi_vs, valor, data in u.getAll(sql, "import_jail"):
            # Comprovar si el pacient est� dins la poblaci�
            if id_cip_sec in self.poblacio:
                # Afegir al conjunt de variables
                agrupador = codis_agrupadors_vs[codi_vs]
                if id_cip_sec not in self.variables[agrupador]:
                    self.variables[agrupador][id_cip_sec] = {"data": data, "valor": valor}
                elif self.variables[agrupador][id_cip_sec]["data"] < data:
                    self.variables[agrupador][id_cip_sec] = {"data": data, "valor": valor}

    def get_indicadors(self):
        """
        Genera els indicadors basats en els criteris definits i les dades disponibles.

        Aquesta funci� processa els pacients, excloent aquells amb cirrosi hep�tica, 
        i calcula diversos indicadors basats en problemes de salut i valors de variables.
        """

        self.indicadors = c.defaultdict(set)
    
        # Itera per cada pacient que no tingui cirrosi hep�tica
        for id_cip_sec in (set(self.poblacio) - self.problemes["Cirrosi hep�tica"]):

            up, uba, edat, duracio = (self.poblacio[id_cip_sec][key] for key in ("up", "uba", "edat", "duracio"))

            # Pacients amb duraci� de pres� superior o igual a 30 dies
            if duracio >= 30:
                if id_cip_sec in (self.problemes["DM2"] | self.problemes["Hipercolesterolemia"] | self.problemes["HTA"] | self.problemes["Obesitat"]):
                    self.indicadors[("FGNA1", "DEN", up, uba)].add(id_cip_sec)
                    if id_cip_sec in self.variables["Index FLI"]:
                        self.indicadors[("FGNA1", "NUM", up, uba)].add(id_cip_sec)
                        if self.variables["Index FLI"][id_cip_sec]["valor"] >= 60:
                            self.indicadors[("FGNA2", "DEN", up, uba)].add(id_cip_sec)
                            if id_cip_sec in self.variables["FIB-4"]:
                                self.indicadors[("FGNA2", "NUM", up, uba)].add(id_cip_sec)

            # Pacients amb duraci� de pres� superior o igual a 120 dies
            if duracio >= 120:
                if id_cip_sec in self.problemes["Esteatosi hep�tica"]:
                    self.indicadors[("FGNA3", "DEN", up, uba)].add(id_cip_sec)
                    if id_cip_sec in self.variables["Fibroscan"]:
                        self.indicadors[("FGNA3", "NUM", up, uba)].add(id_cip_sec)
                elif id_cip_sec in self.variables["FIB-4"]:
                    if edat < 65:
                        if self.variables["FIB-4"][id_cip_sec]["valor"] >= 1.3:
                            self.indicadors[("FGNA3", "DEN", up, uba)].add(id_cip_sec)
                            if id_cip_sec in self.variables["Fibroscan"]:
                                self.indicadors[("FGNA3", "NUM", up, uba)].add(id_cip_sec)
                    else:
                        if self.variables["FIB-4"][id_cip_sec]["valor"] >= 2:
                            self.indicadors[("FGNA3", "DEN", up, uba)].add(id_cip_sec)
                            if id_cip_sec in self.variables["Fibroscan"]:
                                self.indicadors[("FGNA3", "NUM", up, uba)].add(id_cip_sec)

    def export_dades(self):
        """
        Exporta les dades dels indicadors a una taula de la base de dades.

        Crea una taula i hi insereix les dades dels indicadors calculats.
        """

        tb = "jail_fgna"
        db = "jail"
        u.createTable(tb, "(ind varchar(10), up varchar(5), uba varchar(20), analisis varchar(3), n double)", db, rm=True)
        upload = [(ind, up, uba, analisis, len(pacients)) for (ind, analisis, up, uba), pacients in self.indicadors.items()]
        u.listToTable(upload, tb, db)

    def set_master(self):
        """
        Estableix la taula mestre amb les dades dels indicadors calculats.

        Processa les dades dels indicadors per generar una taula mestre i la insereix a la base de dades.
        """

        self.master = list()

        for (ind, analisis, up, uba), pacients in self.indicadors.items():
            for id_cip_sec in pacients:
                if analisis == 'DEN':
                    num = 1 if id_cip_sec in self.indicadors[(ind, "NUM", up, uba)] else 0
                    self.master.append((id_cip_sec, up, uba, self.poblacio[id_cip_sec]["up_inf"], self.poblacio[id_cip_sec]["uba_inf"], ind, num, 1, 0))

        cols = "(id int, up varchar(5), uba varchar(5), upinf varchar(5), ubainf varchar(5), ind varchar(8), num int, den int, excl int)"

        u.createTable(MASTER_TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.master, MASTER_TABLE, DATABASE)

    def export_khalix(self):
        """
        Exporta les dades dels indicadors a Khalix.

        Genera una consulta SQL per agrupar les dades i les exporta a Khalix.
        """

        base = """
                SELECT
                    ind,
                    'Aperiodo',
                    ics_codi,
                    '{_analisi}',
                    'NOCAT',
                    'NOIMP',
                    'DIM6SET',
                    'N',
                    sum({_analisi})
                FROM
                    {_DATABASE}.{_MASTER_TABLE} a
                INNER JOIN nodrizas.jail_centres b ON
                    a.up = b.scs_codi
                WHERE
                    excl = 0
                GROUP BY
                    ind,
                    ics_codi
            """
        sql = " union ".join([base.format(_analisi=analisi, _DATABASE=DATABASE, _MASTER_TABLE=MASTER_TABLE) for analisi in ("NUM", "DEN")])
        u.exportKhalix(sql, FILE)

    def delete_ecap(self):
        """
        Elimina les dades anteriors d'ECAP.

        Executa una s�rie de consultes SQL per eliminar les dades relacionades amb FGNA a la taula que anir� a ECAP.
        """

        params = [("uba", "indicador", "FGNA"),
                  ("pacient", "grup_codi", "FGNA"),
                  ("cataleg", "indicador", "FGNA"),
                  ("catalegPare", "pare", "FGNA")]
        delete = "delete from exp_ecap_iep_{} where {} like '{}%'"
        for param in params:
            sql = delete.format(*param)
            u.execute(sql, DATABASE)

    def export_ecap_uba(self):
        """
        Exporta les dades dels indicadors a la taula que anir� a ECAP per UBA.

        Agrupa les dades dels indicadors per UBA i les insereix a la taula que anir� a ECAP.
        """

        sql = """
                SELECT
                    up,
                    uba,
                    ind,
                    sum(num),
                    sum(den),
                    sum(num) / sum(den)
                FROM
                    {_MASTER_TABLE}
                WHERE
                    excl = 0
                GROUP BY
                    up,
                    uba,
                    ind
            """.format(_MASTER_TABLE=MASTER_TABLE)
        upload = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload, "exp_ecap_iep_uba", DATABASE)

    def export_ecap_pacient(self):
        """
        Exporta les dades dels pacients a la taula que anir� a ECAP.

        Insereix les dades dels pacients que no compleixen els indicadors a la taula que anir� a ECAP.
        """

        sql = """
                SELECT
                    DISTINCT id,
                    up,
                    uba,
                    upinf,
                    ubainf,
                    ind,
                    0,
                    hash_d,
                    codi_sector
                FROM
                    {_MASTER_TABLE} a
                INNER JOIN import_jail.u11 b ON
                    id = id_cip_sec
                WHERE
                    num = 0
                    AND excl = 0
            """.format(_MASTER_TABLE=MASTER_TABLE)
        upload = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload, "exp_ecap_iep_pacient", DATABASE)

    def export_ecap_cataleg(self):
        """
        Exporta el cat�leg dels indicadors que anir� a ECAP.

        Crea una entrada al cat�leg per cada indicador i la insereix a la taula corresponent que anir� a ECAP.
        """
        
        umi = "http://10.80.217.201/sisap-umi/indicador/codi/{}"
        upload = [(cod,
                des,
                i + 1,
                "FGNA",
                1, 0, 0, 0, 0, 1,
                umi.format(cod),
                "MI",
                0)
                for i, (cod, des) in enumerate(LITERALS)]
        u.listToTable(upload, "exp_ecap_iep_cataleg", DATABASE)
        pare = [("FGNA", "Fetge Gras", 12, 0)]
        u.listToTable(pare, "exp_ecap_iep_catalegPare", DATABASE)

if __name__ == "__main__":
    FGNA()