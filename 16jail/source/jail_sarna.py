# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime

def get_dextraccio():
    """."""

    global data_ext, data_ext_menys1any

    sql = """
            SELECT
                data_ext,
                date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY)
            FROM
                dextraccio
            """
    data_ext, data_ext_menys1any = u.getOne(sql, "nodrizas")

def get_centres():

    global centres

    sql = """
            SELECT
                scs_codi,
                ics_codi
            FROM
                jail_centres
          """
    centres = {up: br for up, br in u.getAll(sql, "nodrizas")}

def get_poblacio():

    global poblacio

    sql = """
            SELECT
                id_cip_sec,
                up,
                uba
            FROM
                jail_atesa_avui
            WHERE 
                sortida = '0000-00-00'
                OR sortida BETWEEN '{}' AND '{}'
          """.format(data_ext_menys1any, data_ext)
    poblacio_ultim_any = {id_cip_sec: {"up": up, "br": centres[up], "uba": uba} for id_cip_sec, up, uba in u.getAll(sql, "nodrizas") if up in centres}

    sql = """
            SELECT
                id_cip_sec,
                usua_data_naixement,
                usua_sexe
            FROM
                assignada
          """
    poblacio = {id_cip_sec: {"up": poblacio_ultim_any[id_cip_sec]["up"], "br": poblacio_ultim_any[id_cip_sec]["br"], "uba": poblacio_ultim_any[id_cip_sec]["uba"], 
                             "upinf": None, "ubainf": None, "edat": u.yearsBetween(data_naix, data_ext), "sexe": sexe} 
                for id_cip_sec, data_naix, sexe in u.getAll(sql, "import_jail") if id_cip_sec in poblacio_ultim_any}

def get_sarna():

    global sarna

    sql = """
            SELECT
                id_cip_sec 
            FROM
                eqa_problemes
            WHERE
                id_cip_sec < 0
                AND ps IN (1064, 1065)
                AND dde BETWEEN '{}' AND '{}'
          """.format(data_ext_menys1any, data_ext)
    sarna = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in poblacio}

def get_ivermectina():
    
    global ivermectina

    sql = """
            SELECT
                id_cip_sec
            FROM
                tractaments
            WHERE
                ppfmc_atccodi = 'P02CF01'
                AND ppfmc_pmc_data_ini BETWEEN '{}' AND '{}'
          """.format(data_ext_menys1any, data_ext)
    ivermectina = {id_cip_sec for id_cip_sec, in u.getAll(sql, "import_jail") if id_cip_sec in poblacio}

def get_indicador():

    global indicador;           indicador = c.defaultdict(c.Counter)
    global mst_registres;       mst_registres = []

    for id_cip_sec in sarna:
        excl, den, num = 0, 1, 0
        up, br, uba, upinf, ubainf, edat, sexe = (poblacio[id_cip_sec][k] for k in ("up", "br", "uba", "upinf", "ubainf", "edat", "sexe"))
        indicador[br]["DEN"] += 1
        if id_cip_sec in ivermectina:
            indicador[br]["NUM"] += 1
            num = 1
        mst_registres.append([id_cip_sec, "SARNA1", up, uba, upinf, ubainf, edat, sexe, num, den, excl])

def export_khalix():

    tb_name = "jail_sarna"
    db_name = "jail"
    tb_cols = "(indicador varchar(10), br varchar(5), analisis varchar(3), n int)"
    file_name = "SARNA"

    registres = list()
    for br in indicador:
        den, num = indicador[br]["DEN"], indicador[br].get("NUM", 0)
        registres.append(("SARNA1", br, "DEN", den))
        registres.append(("SARNA1", br, "NUM", num))

    u.createTable(tb_name, tb_cols, db_name, rm=True)
    u.listToTable(registres, tb_name, db_name)

    sql = """
            SELECT
                indicador,
                "Aperiodo",
                br,
                analisis,
                "NOCAT",
                "NOIMP",
                "DIM6SET",
                "N",
                n
            FROM
                {}.{} a
            """.format(db_name, tb_name)
    u.exportKhalix(sql, file_name)

    mst_name = "mst_sarna_pacient"
    mst_cols = "(id_cip_sec int, indicador varchar(10), up varchar(5) not null default'', uba varchar(5) not null default'',upinf varchar(5) not null default'', ubainf varchar(5) not null default'', \
                edat int, sexe varchar(1) not null default'',num double, den double, excl double)"
    u.createTable(mst_name, mst_cols, db_name, rm=True)
    u.listToTable(mst_registres, mst_name, db_name)

if __name__ == '__main__':

    get_dextraccio();                               print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_centres();                                  print("get_centres(): {}".format(datetime.datetime.now()))
    get_poblacio();                                 print("get_poblacio(): {}".format(datetime.datetime.now()))
    get_sarna();                                    print("get_sarna(): {}".format(datetime.datetime.now()))
    get_ivermectina();                              print("get_ivermectina(): {}".format(datetime.datetime.now()))
    get_indicador();                                print("get_indicador(): {}".format(datetime.datetime.now()))
    export_khalix();                                print("export_khalix(): {}".format(datetime.datetime.now()))