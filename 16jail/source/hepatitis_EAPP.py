# coding: latin1

"""
Indicadors Hepatitis EAPP.
"""

import time as t
import sisapUtils as u
import sisaptools as su
# import csv as csv
# import collections as c
from collections import defaultdict
import datetime as d
from dateutil.relativedelta import relativedelta

TABLE = "mst_indicadors_hepatitis_eapp"
DATABASE = "jail"
FILE = "HEPATITISEAPP"

DEXTD = su.Database("p2262", 'nodrizas').get_one("select data_ext from dextraccio")[0]
menys7 = DEXTD - relativedelta(months=7)
menys3 = DEXTD - relativedelta(months=3)

LITERALS = (
    ("EQA3401", "Cribratge Hepatitis A"),
    ("EQA3402", "Cribratge Hepatitis B"),
    ("EQA3403", "Cribratge Hepatitis C"),
    ("EQA3405", "Continuitat assistencial hepatitis B"),
    # ("EQA3406", "Continuitat assistencial hepatitis C"),
    ("EQA3407", "Correcta vacunaci� (A+B) en pacients amb Hepatitis C"),
    ("EQA3408", "Cobertura vacunal Hepatitis B"),
    # ("EQA3409", "Tractaments espec�fic Hepatitis C"),
    )

# Codis vacunes hepatitis A i hepatitis B per exclusions
agr_vac_dict = {15: 'vacuna_hepatitis_b', 34: 'vacuna_hepatitis_a'}

# Agrupadors d'eco i fibroscan
agr_table = {"variables": ("data_var", (848, 849))}

# Llistes d'immunitzats
inm_A = set(['B15', 'B15.9', 'B15.0', 'C01-B15.9', 'C01-B15.0', 'C01-B15'])
inm_B = set(['B16', 'B16.0', 'B16.1', 'B16.2', 'B16.9', 'B18.0', 'B18.1',
             'C01-B16.9', 'C01-B16.0', 'C01-B16.1', 'C01-B16.2', 'C01-B18.0',
             'C01-B18.1', 'C01-B16', 'C01-B18.0' ,'C01-B18.1'])

# Llista de serologies per c�lcul de numeradors
serologies = {
    "vha": set(['S09785', '013354', '011444', 'S09885', '003847',' 016126']),
    "vhb": set(['D01785', 'D01885', '011194', '003738', '011315',
                '003739', '003740', 'S10085', '011557', '011556']),
    "vhc": set(['S10385', 'S10485', 'S10685', 'R23185', 'S23185',
                '003849', '011448', '011449', '007144', 'S10585',
                'D02085', '003848']),
    # "vhc_pos": set(['S10385','S10485','S10685','V01585','R23185','S23185','003849','011448','011449','006808','006383','007144','S10585','001996','003848']),  # noqa
    # "vhc_anticos_pos": set(['S10385', 'S10485', 'S10685', 'R23185', 'S23185', '003849', '011448', '011449', '007144', 'S10585'])  # noqa
    }

# Llista de codis que corresponen a rna demanada
rna_codis = set(['V01385', 'V01485', '006807', '006382', '001995'])

# Llista d'ant�gens Hbs (Extret de llista Trello)
antigens_HB_codis = set(
    ['D01785', 'D01885', '011194', '003738', '011315', '003739', '003740'])


def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql = "SELECT data_ext, date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY) FROM dextraccio"
    return u.getOne(sql, 'nodrizas')


def get_labels_codis(labels, codis_list):
    """
    Crea un diccionario que relaciona cada codigo de codis_list
        con un label en labels_list.
    Devuelve {codi:label}
    """
    return {
        codi: name for name, set_codis in
        zip(labels, codis_list) for codi in set_codis}


def getCipToHash():
    # cip_ant=getCipAnt()
    sql = 'select usua_cip, usua_cip_cod from pdptb101'
    return {cip: hash for cip, hash in u.getAll(sql, 'pdp')}


def getHashToId():
    hashId = {}
    sql = "select id_cip_sec, hash_d from import_jail.u11"
    for id, hash_d in u.getAll(sql, "import_jail"):
        hashId[hash_d] = id
    return hashId


def get_visites_modul(current_date):
    """
    Obtiene los id_cip_sec que han tenido una visita 
        en el modul INFEC en el servei MG independientemente de la fecha
    Devuelve un set de ids.
    """
    cipToHash = getCipToHash()
    HashToId = getHashToId()
    sql = """
        SELECT
            visi_data_alta,
            visi_usuari_cip
        FROM 
            vistb043
        WHERE
            visi_modul_codi_modul = 'INFEC'
            AND visi_data_alta <= TO_DATE('{}', 'YYYY-MM-DD')
    """.format(current_date)
    return {
        HashToId[cipToHash[cip]] for data, cip in u.getAll(sql, '6951')
        if cip in cipToHash and cipToHash[cip] in HashToId}  # noqa


def get_eco_fibro(current_date):
    """
    Obtiene los pacientes con las variables eco y fibroscan en eqa_variables,
    que tienen la fecha mas reciente de la prueba hace un a�o como muy pronto.
    Primero se consigue los pacientes que tienen alguno de los agrupadores
        correspondientes con una fecha de hace al menos un a�o
    y luego se filtra por los que
        tienen los dos agrupadores.
    Devuelve un set de ids
    """
    id_agrs = defaultdict(lambda: defaultdict(set))
    table = 'variables'
    data_field = agr_table[table][0]
    agrs = agr_table[table][1]
    sql = """
        select
            id_cip_sec,
            {},
            agrupador
        from {}.eqa_{},
        nodrizas.dextraccio
        where
            agrupador in {}
            and id_cip_sec < 0 
            and {} between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext
    """.format(data_field, "nodrizas", table, agrs, data_field)
    for id, data, agr in u.getAll(sql, "nodrizas"):
        # if u.monthsBetween(data, current_date) > 2:
        if u.monthsBetween(data, current_date) < 12:
            id_agrs[id][agr].add(data)
    return {id for id in id_agrs if all([agr in id_agrs[id] for agr in agrs])}


def get_immunitzats():
    """
    Obtiene los pacientes que presentan inmunizacion segun los codigos
        en las variables inm_A e inmB y se agrupan en un diccionario a nivel
        del virus para el que estan inmunizados.
    Esto se consigue utilizando un diccionario, inmmunizats_codis,
        que a cada codigo de inmunizacion le asigna el nombre de virus que
        corresponde.
    Devuelve un diccionario de sets {virus: set(ids inmunizados)}
    """
    inmmunizats_codis = get_labels_codis(['vha', 'vhb'], [inm_A, inm_B])
    inmmunizats_by_virus = defaultdict(set)
    sql = "select id_cip_sec,imu_cod_ps from import_jail.immunitzats, nodrizas.dextraccio where \
        imu_cod_ps in {} and imu_data_alta <= data_ext;".format(tuple(inm_A | inm_B))
    for id, cod in u.getAll(sql, 'import_jail'):
        inmmunizats_by_virus[inmmunizats_codis[cod]].add(id)
    return inmmunizats_by_virus


def get_pac_tract(current_date):
    """
    Obtiene los pacientes que en el ultimo any
    (fecha de inicio o de fin de hace menos de 12 meses)
    han estado medicados con los farmacos cuyo codi atc
    esta en la variable global codis_atc.
    Devuelve un set de ids
    """
    sql = "select id_cip_sec,ppfmc_pmc_data_ini,ppfmc_data_fi from \
        import_jail.tractaments, nodrizas.dextraccio where ppfmc_atccodi in {} \
        and ppfmc_pmc_data_ini <= data_ext".format(codis_atc)
    return {
        id for id, ini, fi in
        u.getAll(sql, 'import_jail')
        if u.monthsBetween(ini, current_date) < 12 or u.monthsBetween(fi, current_date) < 12}  # noqa


# Codis ATC dels retrovirals per EQA3409
codis_atc = ('J05AP08', 'J05AP51', 'J05AP54', 'J05AP55', 'J05AP56', 'J05AP57')


class HEP_EAPP(object):
    """."""
    def __init__(self):
        """."""
        self.get_ups_ubas_inf()
        self.get_denom()
        # self.get_denom_EQA3409()
        self.get_vacunats()
        self.get_serologies()
        self.get_problemes()
        self.get_indicadors()
        self.set_master()
        self.export_khalix()
        self.delete_ecap()
        self.export_ecap_uba()
        self.export_ecap_pacient()
        self.export_ecap_cataleg()
        self.export_taula()

    def get_ups_ubas_inf(self):
        sql = "SELECT DISTINCT usua_uab_up up, usua_uab_codi_uab uba,ass_codi_up upinf, ass_codi_unitat ubainf \
               FROM import_jail.assignada \
               ORDER BY usua_data_alta"
        self.ups_ubas_inf = {(up, uba): (upinf, ubainf) for up, uba, upinf, ubainf in u.getAll(sql, "import_jail")}

    def get_denom(self):
        """Calcula nombre de dies a pres� darrer any"""
        self.poblacio = {}
        self.actualment_intern = {}
        data_ext, data_ext_menys1any = get_date_dextraccio()
        data_ini = data_ext-relativedelta(years=1)
        print("get_denom")
        start = t.time()
        sql = "select id_cip_sec, up, uba, ingres, sortida, durada, sortit \
               from jail_atesa_avui"
        for id, up, uba, data_ingres, data_sortida, durada, sortit in u.getAll(sql, 'nodrizas'):
            if durada > 60:
                if not sortit:
                    self.actualment_intern[id] = id
                    self.poblacio[id] = (durada, up, uba)
                else:
                    if u.daysBetween(data_ext_menys1any, data_sortida) >= 60:
                        self.poblacio[id] = (durada, up, uba)

        finish = t.time()
        print(finish-start)

    def get_denom_EQA3409(self):
        """Obt� presos amb m�nim 1 dia a pres� darrer any"""
        self.poblacio_EQA3409 = {}
        data_ext = get_date_dextraccio()[0]
        data_ini = data_ext - relativedelta(years=1)
        sql = "select id_cip_sec, huab_up_codi, huab_uab_codi, huab_data_ingres, \
            huab_data_sortida from moviments, nodrizas.dextraccio  \
            where huab_data_ingres <= data_ext order by 1,4 desc"
        actual = 0
        for id, up, uba, data_ingres, data_sortida in u.getAll(sql, 'import_jail'):
            if data_sortida is None or data_ingres >= data_ini:
                if data_sortida is None:
                    durada = (data_ext-data_ingres).days
                else:
                    durada = (data_sortida-data_ingres).days
                if id != actual:
                    self.poblacio_EQA3409[id] = (durada, up, uba)
                    actual = id
                else:
                    days = self.poblacio_EQA3409[id][0]
                    days += durada
                    self.poblacio_EQA3409[id] = (days, up, uba)

        self.poblacio_EQA3409 = {
            key: value for key, value in
            self.poblacio_EQA3409.iteritems() if value[0] > 0}

    def get_vacunats(self):
        """ Obtenim pacients vacunats amb una dosi o m�s d'hep. A o hep. B
            Agrupa fent servir agr de la vacuna
            Retorna {agr_vacuna: set(ids)}
        """
        print("get_vacunats")
        start = t.time()
        self.vacunats = defaultdict(set)
        # sql="select id_cip_sec, agrupador, datamax from {}.eqa_vacunes \
        #     where agrupador in {} and dosis >= 1 and \
        #     id_cip_sec < 0".format('nodrizas',tuple(agr_vac_dict.keys()))
        # for id, agrupador, data in u.getAll(sql,'nodrizas'):
        #     self.vacunats[agr_vac_dict[agrupador]].add(id)
        sql = """select id_cip_sec, agrupador, dosis, datamax from eqa_vacunes
             where agrupador in {} and dosis >= 1 and
             id_cip_sec < 0""".format(tuple(agr_vac_dict.keys()))
        # codis_vac = tuple({codi_vac for codi_vac, _ in u.getAll(sql, "nodrizas")})
        # agrs_vac_codis = {codi_vac: agrupador for codi_vac, agrupador in u.getAll(sql, "nodrizas")}
        # sql = "select id_cip_sec, va_u_cod, va_u_dosi, va_u_data_vac from vacunes, nodrizas.dextraccio \
        #     where va_u_cod in {} and va_u_data_vac <= data_ext".format(tuple(agrs_vac_codis.keys()))
        for id, agrupador, dosis, data in u.getAll(sql,'nodrizas'):
            if agrupador == 15:
                if dosis >= 3:
                    self.vacunats['vacuna_hepatitis_b'].add(id)
                elif dosis >= 2 and data >= menys7:
                    self.vacunats['vacuna_hepatitis_b'].add(id)
                elif dosis >= 1 and data >= menys3:
                    self.vacunats['vacuna_hepatitis_b'].add(id)
            else:
                self.vacunats['vacuna_hepatitis_a'].add(id)
        
        #     self.vacunats[agr_vac_dict[agrupador]].add(id)
        # for id, codi_vac, dosis, data in u.getAll(sql, 'import_jail'):
        #     agrupador = agrs_vac_codis[codi_vac]
        #     # revisar dosis per dates en cas de vhb
        #     if codi_vac in ('A00020', 'A00032'):
        #         if dosis >= 3:
        #             self.vacunats['vacuna_hepatitis_b'].add(id)
        #         elif dosis >= 2 and data >= menys7:
        #             self.vacunats['vacuna_hepatitis_b'].add(id)
        #         elif dosis >= 1 and data >= menys3:
        #             self.vacunats['vacuna_hepatitis_b'].add(id)
        #     else:
        #         self.vacunats[agr_vac_dict[agrupador]].add(id)
        finish = t.time()
        print(finish - start)

    def get_serologies(self):
        """ Obtenim pacients amb serologies, rna demanada i ant�gensB
        """
        self.serologiesA = {}
        self.serologiesB = {}
        self.serologiesC = {}  # EQA3403
        self.serologiesB_pos = {}
        self.serologiesC_pos = {}  # EQA3406
        # self.serologiesC_anticos_pos = {}  # EQA3407
        self.rna_demanada = {}
        self.antigensB = {}  # EQA3405

        print("get_serologies")
        start = t.time()

        sql = "select id_cip_sec, cod, val from jail_serologies, dextraccio where dat <= DATE_FORMAT(data_ext, '%Y%m%d')"
        for id, codi, val in u.getAll(sql, 'nodrizas'):
            if id in self.poblacio:
                if codi in serologies["vha"]:
                    self.serologiesA[id] = (codi)
                if codi in serologies["vhb"]:
                    self.serologiesB[id] = (codi)
                    if val == 1:
                        self.serologiesB_pos[id] = (codi)
                if codi in serologies["vhc"]:
                    self.serologiesC[id] = (codi)
                    if val == 1:
                        self.serologiesC_pos[id] = (codi)
                if codi in rna_codis:
                    self.rna_demanada[id] = (codi)
                if codi in antigens_HB_codis and val == 1:
                    self.antigensB[id] = (codi)

        # self.rna_demanada_EQA3409 = {}
        # sql = "select id_cip_sec, cod, dat, val from jail_serologies \
        #     where cod in {}".format(tuple(rna_codis))
        # for id, codi, data, val in u.getAll(sql, 'nodrizas'):
        #     data = d.datetime.strptime(data, "%Y%m%d")
        #     # if id in self.poblacio_EQA3409:
        #     if id in self.poblacio:
        #         if (
        #           u.monthsBetween(data, get_date_dextraccio()) <= 12 and
        #           u.monthsBetween(data, get_date_dextraccio()) >= 6 and
        #           codi in rna_codis and
        #           val > 0):
        #             self.rna_demanada_EQA3409[id] = (codi)
        finish = t.time()
        print(finish-start)

    def get_problemes(self):
        """."""        

        self.diagnosticB = set()

        print("get_problemes")
        sql = """
                SELECT
                    id_cip_sec 
                FROM
                    problemes,
                    nodrizas.dextraccio
                WHERE
                    pr_cod_ps IN ('C01-B18.0', 'C01-B18.1')     
                    and pr_dde < data_ext
              """
        for id, in u.getAll(sql, "import_jail"):
            self.diagnosticB.add(id)

    def get_indicadors(self):
        """."""
        self.master = []

        print("get_indicadors")
        start = t.time()
        # Indicador EQA3401
        for id in self.poblacio:
            den = 1
            num = 0
            exc_den = 0
            up, uba = self.poblacio[id][1], self.poblacio[id][2]
            upinf, ubainf = self.ups_ubas_inf.get((up, uba), (up, 'I'+uba))
            if id in self.vacunats['vacuna_hepatitis_a']:
                exc_den = 1
            elif id in self.serologiesA:
                num = 1
            this = (id,
                    up, uba, upinf, ubainf,
                    "EQA3401",
                    num, den, exc_den)
            self.master.append(this)

        # Indicador EQA3402
        for id in self.poblacio:
            den = 1
            num = 0
            exc_den = 0
            up, uba = self.poblacio[id][1], self.poblacio[id][2]
            upinf, ubainf = self.ups_ubas_inf.get((up, uba), (up, 'I'+uba))
            if id in self.vacunats['vacuna_hepatitis_b']:
                exc_den = 1
            elif id in self.serologiesB:
                num = 1
            this = (id,
                    up, uba, upinf, ubainf,
                    "EQA3402",
                    num, den, exc_den)
            self.master.append(this)

        # Indicador EQA3403
        for id in self.poblacio:
            den = 1
            num = 0
            exc_den = 0
            up, uba = self.poblacio[id][1], self.poblacio[id][2]
            upinf, ubainf = self.ups_ubas_inf.get((up, uba), (up, 'I'+uba))
            if id in self.rna_demanada:
                exc_den = 1
            elif id in self.serologiesC:
                num = 1
            this = (id,
                    up, uba, upinf, ubainf,
                    "EQA3403",
                    num, den, exc_den)
            self.master.append(this)

        # Indicador EQA3405
        # id's de pacients
        llista_visitants_moduls = get_visites_modul(get_date_dextraccio()[0])
        for id in self.poblacio:
            # Definir self.diagnosticB com el conjunt de persones amb diagnostic d'HB
            if id in self.diagnosticB:
                up, uba = self.poblacio[id][1], self.poblacio[id][2]
                upinf, ubainf = self.ups_ubas_inf.get((up, uba), (up, 'I'+uba))
                den = 1
                num = 0
                exc_den = 0
                # Definir self.derivats com els pacients derivats a MG o INFEC
                if id in llista_visitants_moduls:
                    num = 1
                this = (id,
                        up, uba, upinf, ubainf,
                        "EQA3405",
                        num, den, exc_den)
                self.master.append(this)

        # Indicador EQA3406
        # Definir eco_fibroscan com pacients amb eco i fibroscan el darrer any
        """
        eco_fibroscan = get_eco_fibro(get_date_dextraccio())
        for id in self.poblacio:
            if (
             self.poblacio[id][0]<=365 and
             self.poblacio[id][0] >=180 and
             id in self.actualment_intern):
                # denom: Persones amb serologies C+ i rna+
                if id in self.serologiesC_pos and id in self.rna_demanada:
                    den = 1
                    num = 0
                    exc_den = 0
                    if id in eco_fibroscan:
                        num = 1
                    this = (id,
                            self.poblacio[id][1],
                            self.poblacio[id][2],
                            "EQA3406",
                            num, den, exc_den
                            )
                    self.master.append(this)
        """

        # Indicador EQA3408
        llista_immunitzats = get_immunitzats()
        for id in self.poblacio:
            if id in self.actualment_intern:
                up, uba = self.poblacio[id][1], self.poblacio[id][2]
                upinf, ubainf = self.ups_ubas_inf.get((up, uba), (up, 'I'+uba))
                den = 1
                num = 0
                exc_den = 0
                if id in self.antigensB:
                    # exc_den = 1
                    num = 1
                    # Si immunitzats o vacunats amb dosi >=1 de vhb
                elif (id in self.vacunats['vacuna_hepatitis_b'] or
                      id in llista_immunitzats['vhb']):
                    num = 1
                this = (id,
                        up, uba, upinf, ubainf,
                        "EQA3408", num, den, exc_den)
                self.master.append(this)

        sql = """
            SELECT id_cip_sec, up, uba, num, excl
            FROM eqa0310a_ind2
            WHERE id_cip_sec < 0
            AND ates = 1
            AND institucionalitzat = 0
        """
        for id, up, uba, num, excl in u.getAll(sql, 'eqa_ind'):
            # if id in self.poblacio:
            upinf, ubainf = self.ups_ubas_inf.get((up, uba), (up, 'I'+uba))
            self.master.append((id, up, uba, upinf, ubainf, 'EQA3410', num, 1, excl))

        # Indicador EQA3407
        # llista_immunitzats = get_immunitzats()
        # for id in self.poblacio:
        #     # Amb anticosos d'hepatitis C positiu
        #     if id in self.serologiesC_pos and id in self.actualment_intern:
        #         den = 1
        #         num = 0
        #         exc_den = 0
        #         # Si immunitzats d'A i B o vacunats amb dosi >=1 per A i B
        #         if (
        #          id in self.vacunats['vacuna_hepatitis_a'] and
        #          id in self.vacunats['vacuna_hepatitis_b']):
        #             num = 1
        #         elif (
        #          id in llista_immunitzats['vha'] and
        #          id in llista_immunitzats['vhb']):
        #             num = 1
        #         elif (
        #             id in self.vacunats['vacuna_hepatitis_a'] and
        #             id in llista_immunitzats['vhb']):
        #             num = 1
        #         elif (
        #             id in self.vacunats['vacuna_hepatitis_b'] and
        #             id in llista_immunitzats['vha']):
        #             num = 1
        #         this = (id,
        #                 self.poblacio[id][1],
        #                 self.poblacio[id][2],
        #                 "EQA3407",
        #                 num, den, exc_den)
        #         self.master.append(this)

        # Indicador EQA3409
        # pacients_retrovirals = get_pac_tract(get_date_dextraccio())
        # for id in self.rna_demanada_EQA3409:
        #     den = 1
        #     num = 0
        #     exc_den = 0
        #     # Si tenen els codis ATC dels retrovirals definits al set codis_ATC
        #     if id in pacients_retrovirals:
        #         num = 1
        #     this = (id,
        #             self.poblacio[id][1],
        #             self.poblacio[id][2],
        #             "EQA3409",
        #             num, den, exc_den)
        #     self.master.append(this)
        finish = t.time()
        print(finish-start)

    def set_master(self):
        """."""
        cols = "(id int, up varchar(5), uba varchar(5), upinf varchar(5), ubainf varchar(5), ind varchar(8), \
                 num int, den int, exc_den int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.master, TABLE, DATABASE)

    def export_khalix(self):
        """."""
        base = "select ind, 'Aperiodo', ics_codi, '{0}', \
                       'NOCAT', 'NOIMP', 'DIM6SET', 'N', sum({0}) \
                from {1}.{2} a \
                inner join nodrizas.jail_centres b on a.up = b.scs_codi \
                where exc_den = 0 \
                group by ind, ics_codi"
        sql = " union ".join([base.format(analysis, DATABASE, TABLE)
                              for analysis in ("NUM", "DEN")])
        u.exportKhalix(sql, FILE)

    def delete_ecap(self):
        """."""
        params = [("uba", "indicador", "EQA34"),
                  ("pacient", "grup_codi", "EQA34"),
                  ("cataleg", "indicador", "EQA34"),
                  ("catalegPare", "pare", "PROCHEP")]
        delete = "delete from exp_ecap_iep_{} where {} like '{}%'"
        for param in params:
            sql = delete.format(*param)
            u.execute(sql, DATABASE)

    def export_ecap_uba(self):
        """."""
        sql = "select up, uba, ind, sum(num), sum(den), sum(num) / sum(den) \
               from {} \
               where exc_den = 0 \
               group by up, uba, ind".format(TABLE)
        upload = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload, "exp_ecap_iep_uba", DATABASE)

    def export_ecap_pacient(self):
        """."""
        sql = "select distinct id, up, uba, upinf, ubainf, ind, 0, \
                               hash_d, codi_sector \
               from {} a \
               inner join import_jail.u11 b on id = id_cip_sec \
               where num = 0 and \
                     exc_den = 0".format(TABLE)
        upload2 = [row for row in u.getAll(sql, DATABASE)]
        u.listToTable(upload2, "exp_ecap_iep_pacient", DATABASE)

    def export_ecap_cataleg(self):
        """."""
        umi = "http://10.80.217.201/sisap-umi/indicador/codi/{}"
        upload = [(cod,
                   des,
                   i + 1,
                   "PROCHEP",
                   1, 0, 0, 0, 0, 1,
                   umi.format(cod),
                   "MI", 0)
                  for i, (cod, des) in enumerate(LITERALS)]
        u.listToTable(upload, "exp_ecap_iep_cataleg", DATABASE)
        pare = [("PROCHEP", "EQA del proc�s de l'Hepatitis", 9, 0)]
        u.listToTable(pare, "exp_ecap_iep_catalegPare", DATABASE)

    def export_taula(self):
        upload = self.poblacio.items()
        u.writeCSV(u.tempFolder + "pob_sum_2.csv", upload, sep=";")
        upload = self.vacunats.items()
        u.writeCSV(u.tempFolder + "vacunats.csv", upload, sep=";")
        upload = self.serologiesA.items()
        u.writeCSV(u.tempFolder + "serologiesA.csv", upload, sep=";")
        upload = self.serologiesB.items()
        u.writeCSV(u.tempFolder + "serologiesB.csv", upload, sep=";")
        upload = self.serologiesC.items()
        u.writeCSV(u.tempFolder + "serologiesC.csv", upload, sep=";")
        # upload = self.serologiesC_pos.items()
        # u.writeCSV(u.tempFolder + "serologiesC_pos.csv", upload, sep=";")
        upload = self.rna_demanada.items()
        u.writeCSV(u.tempFolder + "rna_demanada.csv", upload, sep=";")
        upload = self.actualment_intern.items()
        u.writeCSV(u.tempFolder + "interns.csv", upload, sep=";")
        upload = self.antigensB.items()
        u.writeCSV(u.tempFolder + "antigensB.csv", upload, sep=";")
        upload = {}
        eco = get_eco_fibro(get_date_dextraccio()[0])
        for id in eco:
            upload[id] = id
        u.writeCSV(u.tempFolder + "eco_fibro.csv", upload.items(), sep=";")
        pac = list(get_pac_tract(get_date_dextraccio()[0]))
        upload = {}
        for id in pac:
            upload[id] = id
        u.writeCSV(u.tempFolder + "retrovirals.csv", upload.items(), sep=";")
        immu = get_immunitzats()
        u.writeCSV(u.tempFolder + "immunitzats.csv", immu.items(), sep=";")
        upload = self.master
        u.writeCSV(u.tempFolder + "master.csv", upload, sep=";")


if __name__ == "__main__":
    HEP_EAPP()
