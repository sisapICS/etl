# coding: latin1

"""

Trello: https://trello.com/c/bEZxhRZK

Drive: https://docs.google.com/document/d/1XZWU2ZLi6X52XQXXoKXxrxWEFzrOCDYpbHAzTX_hNrg/edit?usp=sharing


Time Execution: DEBUG 1m 30s / NOT DEBUG 2h 
DEBUG se puede ejecutar en local, pero NOT DEBUG se ha de hacer en el SERVIDOR, el proceso llena la RAM (32GB) en mi pc

"""

import collections as c
import sisapUtils as u
from os import remove
import datetime as d
from datetime import datetime

NODEBUG = True # Poner False para ejecutar en DEBUG / Local
SECTOR = '6837'

DATABASE = 'Gis'

SERVEIS = ("MG", "PED", "INF", "ENF", "INFPD", "INFP", "INFG", "URGEN", "ESP")
USUARIS = ("04104", "04416", "04302")
ATRIBUTS = ("Assistencial - UBA", "Assistencial No - UBA")

INDICADORS = {"GIS001": {"literal": "Percentatge de visites programades amb motiu de consulta",
                         "pare": "PROGMOT"},
              "GIS002": {"literal": "Visites amb motiu recollit per la pantala pxm",
                         "pare": "PROGMOT"},
              "GIS003": {"literal": "Visites amb motiu registrat que no son de pxm",
                         "pare": "PROGMOT"}}

TABLE = 'mst_gis_pacient' # o 'mst_indicadors_pacient' (si es crea carpeta pròpia)
#TABLE = 'mst_gis_u11' o 'mst_u11' (si es crea carpeta pròpia)
#TABLE = 'mst_gis_ubas' o 'mst_ubas' (si es crea carpeta pròpia)

ECAP_IND = "exp_ecap_uba_gis"
ECAP_PAC = "exp_ecap_pacient_gis"

CAT_IND = "exp_ecap_cataleg_gis"
CAT_PARE = "exp_ecap_catalegpare_gis"

class GIS(object):
    """."""

    def __init__(self):
        """."""

        # self.get_upusu()
        
        self.get_centres()
        self.get_usuaris()
        self.get_atribut()        
        # # # self.get_poblacio()
        self.get_tasques()
        self.get_numerador()
        self.get_pim()
        self.get_indicador()
        # # # self.export_data()              

        # MST
        # MST_XXX_PACIENT
        self.create_table()
        self.get_master()
        self.upload_master()

        # MST_UBAS

        # MST_U11

        # SISAP-ECAP
        # EXP_ECAP_PACIENT_GIS

        # EXP_ECAP_UBA_GIS
        self.set_ecap_indicadors()
        # # # self.get_professionals()

        # EXP_CATALEG_GIS / EXP_CATALEGPARE_GIS
        self.get_cataleg()

        # KHALIX
        # EXP_KHALIX_UP_GIS/EXP_KHALIX_UBA_GIS
        self.export_khalix()


    def get_upusu(self):
        """ Aconseguir l'UP de l'USUARI: UP amb més assignació de visites.
            Crea l'arxiu csv GIS_AssignacioUP.csv
            Time execution: DEBUG / NOT DEBUG 7m """
        ts = datetime.now()
        self.cont = c.Counter()
        self.upusu = c.defaultdict(set)
        self.upusu_usu = {}
        sql = "select codi_sector, visi_assign_visita, visi_up, count(1) as n \
               from {{}}, nodrizas.dextraccio \
               where visi_data_visita between adddate(data_ext, interval -1 year) and data_ext and \
                     visi_up in {} and \
                     visi_servei_codi_servei in {} and \
                     visi_tipus_visita not in ('9E') and \
                     visi_forcada_s_n = 'N' and \
                     visi_internet <> 'S' \
                group by codi_sector, visi_assign_visita, visi_up".format(tuple(self.centres), SERVEIS)
        crt = "show create table visites1"
        tables = u.getOne(crt, "import")[1].split("UNION=(")[1][:-1].split(",")
        jobs = [(sql.format(table), "import") for table in tables] if NODEBUG else [(sql.format('visites_sys_p7161'), "import")]  # noqa
        for mes in u.multiprocess(_worker, jobs):
            for sec, usu, up, n in mes:
                if (sec, usu) in self.usuaris:
                    self.cont[(sec, usu, up)] += n
        for (sec, usu, up), n in self.cont.items():
            self.upusu[(sec, usu)].add((n, up))
        for (sec, usu) in self.upusu:
            maxtupla = max(self.upusu[(sec, usu)])
            self.upusu_usu[(sec, usu)] = {'up': maxtupla[1], 'n': maxtupla[0]}
        self.uploadcsv = []
        for (sec, usu, up), n in self.cont.items():
            #if n > 5:
            self.uploadcsv.append((usu, sec, up, n))
        u.writeCSV(u.tempFolder + "GIS_AssignacioUP.csv", 
        [("usuario", "sector", "up", "n")] + self.uploadcsv)
        #print(len(self.upusu_usu))
        print('Time execution {} / nrow {}'.format(datetime.now() - ts,
                                                   len(self.cont)))
    
    def get_centres(self):
        """."""
        self.centres = {}
        sql = "select scs_codi, ics_codi \
               from cat_centres"
        for up, br in u.getAll(sql, "nodrizas"):
            self.centres[up] = {'br': br} 
        print("CENTRES: ", len(self.centres))

    def get_usuaris(self):
        """."""
        self.usuaris = set()
        self.dni = {}
        sql = "select codi_sector, ide_usuari, left(ide_dni, 8) from cat_pritb992 \
               where \
                    ide_categ_prof_c in {} and \
                    (substring(ide_usuari,1,1) = 'P' or \
                     substring(ide_usuari,1,4) = 'OPS$')".format(USUARIS)
        for sector, usu, dni in u.getAll(sql, 'import'):
            self.usuaris.add((sector, usu))
            self.dni[(sector, usu)] = {'dni': dni}
        print("USUARIS: ", len(self.usuaris))
        print("DNI: ", len(self.dni))

    def get_atribut(self):
        """ . """
        self.atribut = {}
        sql = "select eap, \
                    servei, \
                    modul, \
                    atribut_1 \
                from \
                    sisap_agendes_atributs \
                where \
                    atribut_1 in {}".format(ATRIBUTS)
        for br, servei, modul, atribut in u.getAll(sql, 'redics'):
            self.atribut[(br, servei, modul)] = {'atribut': atribut}
        print("ATRIBUTS: ", len(self.atribut))

    def get_poblacio(self):
        """ . """
        self.poblacio = {}
        sql = "select id_cip_sec, up, edat, sexe, \
                      ates, institucionalitzat \
                from assignada_tot"
        for id_cip_sec, up, edat, sexe, ates, ins in u.getAll(sql, 'nodrizas'):
            self.poblacio[id_cip_sec] = {'up': up,
                                         'edat': edat,
                                         'sexe': sexe,
                                         'ates': ates,
                                         'ins': ins}
        print(len(self.poblacio))

    def get_tasques(self):
        """."""
        print("-------------------------------------------------- get_tasques")
        self.tasques = set()
        sql = "select id_cip_sec, alv_data_al \
               from alertes_s{}, nodrizas.dextraccio \
               where alv_tipus = 'TASCAD' and \
                     alv_data_al between adddate(data_ext, interval -1 year) and data_ext"
        jobs = [(sql.format(sector), "import") for sector in u.sectors] if NODEBUG else [(sql.format(SECTOR), "import")]  # noqa
        for sector in u.multiprocess(_worker, jobs, 8):
            self.tasques |= set(sector)
        print("TASQUES", len(self.tasques))
        print('Time execution {}'.format(datetime.now() - ts))

    def get_numerador(self):
        """."""
        print("------------------------------------------------ get_numerador")
        ts = datetime.now()
        self.numerador = set()
        sql = "select visi_id \
               from motius_s{}, nodrizas.dextraccio \
               where visi_data_visita between adddate(data_ext, interval -1 year) and data_ext and \
                    (visi_motiu_prior not in ('', 'CAP') or \
                     visi_usuari_text <> '' or \
                     visi_motiu_lliure <> '')"
        # SELECT PARA LOS HIJOS
        # sql = "select visi_id \
        #        from motius_s{}, nodrizas.dextraccio \
        #        where visi_data_visita between adddate(data_ext, interval -1 year) and data_ext and \
        #             (visi_motiu_prior not in ('', 'CAP') or \
        #             (visi_motiu_prior in ('CAP') and \
        #                 (visi_usuari_text <> '' or \
        #                  visi_motiu_lliure <> '')))"

        jobs = [(sql.format(sector), "import") for sector in u.sectors] if NODEBUG else [(sql.format(SECTOR), "import")]  # noqa
        for sector in u.multiprocess(_worker, jobs, 8):
            pacients = set([id for id, in sector])
            self.numerador |= pacients
        print("MOTIUS (NUMERADOR):", len(self.numerador))
        print('Time execution {}'.format(datetime.now() - ts))

    def get_pim(self):
        """."""
        print("------------------------------------------------------ get_pim")
        ts = datetime.now()
        self.pim = set()
        sql = "select visi_id \
               from motius_s{}, nodrizas.dextraccio \
               where visi_data_visita between adddate(data_ext, interval -1 year) and data_ext and \
                     visi_ticket_teseo <> ''"
        jobs = [(sql.format(sector), "import") for sector in u.sectors] if NODEBUG else [(sql.format(SECTOR), "import")]  # noqa
        for sector in u.multiprocess(_worker, jobs, 8):
            pacients = set([id for id, in sector])
            self.pim |= pacients
        print("PIM", len(self.pim))
        print('Time execution {}'.format(datetime.now() - ts))

    def get_indicador(self):
        """ Time Execution: DEBUG 1m / NOT DEBUG: """
        print("------------------------------------------------ get_indicador")
        ts = datetime.now()
        # self.gorg1 = c.Counter()
        # self.gorg2 = c.Counter()
        # self.gorg3 = c.Counter()
        # self.gorg4 = c.Counter()
        # self.gorgupload = []
        self.tomaster = c.Counter()
        self.indicador_up = c.Counter()
        self.indicador_uba = c.Counter()
        self.pacients = c.defaultdict(lambda: c.defaultdict(set))
        sql = "select visi_id, id_cip_sec, visi_data_visita, visi_up, \
                      codi_sector, visi_assign_visita, \
                        visi_servei_codi_servei, \
                        visi_modul_codi_modul \
               from {{}}, nodrizas.dextraccio \
               where visi_data_visita between adddate(data_ext, interval -1 year) and data_ext and \
                     visi_up in {} and \
                     visi_servei_codi_servei in {} and \
                     visi_tipus_visita not in ('9E') and \
                     visi_forcada_s_n = 'N' and \
                     visi_internet <> 'S'".format(tuple(self.centres), SERVEIS)
        crt = "show create table visites1"
        tables = u.getOne(crt, "import")[1].split("UNION=(")[1][:-1].split(",")
        jobs = [(sql.format(table), "import") for table in tables] if NODEBUG else [(sql.format('visites_sys_p6441'), "import")]  # noqa
        for mes in u.multiprocess(_worker, jobs):
            for id, pac, dat, up_vis, sec, usu, servei, modul in mes:
                # self.gorg1[up_vis, usu, servei, modul] += 1
                if (sec, usu) in self.usuaris:
                    # self.gorg2[up_vis, usu, servei, modul] += 1
                    br_vis = self.centres[up_vis]['br']
                    if (br_vis, servei, modul) in self.atribut:
                        # self.gorg3[up_vis, usu, servei, modul] += 1
                        # up = self.upusu_usu[(sec, usu)]['up']
                        dni = self.dni[(sec, usu)]['dni']                     
                        if (pac, dat) not in self.tasques and id not in self.pim:                            
                            # self.gorg4[up_vis, usu, servei, modul] += 1
                            self.tomaster[(pac, up_vis, "DEN")] += 1
                            self.tomaster[(pac, up_vis, "NUM")] += id in self.numerador
                            self.indicador_up[(up_vis, 'NOIMP', "DEN")] += 1
                            self.indicador_up[(up_vis, 'NOIMP', "NUM")] += id in self.numerador
                            self.indicador_uba[(up_vis, dni, 'NOIMP', "DEN")] += 1
                            self.indicador_uba[(up_vis, dni, 'NOIMP', "NUM")] += id in self.numerador

                            self.pacients['GIS001']["denominador"].add(pac)
                            if id in self.numerador:
                                self.pacients['GIS001']["numerador"].add(pac)

        print("MASTER:", len(self.tomaster))

        print("INDICADOR UP:", len(self.indicador_up))
        print("INDICADOR UBA:", len(self.indicador_uba))

        print(len(self.pacients['GIS001']["denominador"]))
        print(len(self.pacients['GIS001']["numerador"]))
        print('Time execution {}'.format(datetime.now() - ts))

        # for (up_vis, usu, servei, modul), n in self.gorg1.items():
        #     if up_vis in ('00274','00276','00108','00441'):
        #         self.gorgupload.append((up_vis, usu, servei, modul,'0 - VISITES1',n))
        # for (up_vis, usu, servei, modul), n in self.gorg2.items():
        #     if up_vis in ('00274','00276','00108','00441'):
        #         self.gorgupload.append((up_vis, usu, servei, modul,'1 - VISITES1 flt USUARI',n))
        # for (up_vis, usu, servei, modul), n in self.gorg3.items():
        #     if up_vis in ('00274','00276','00108','00441'):
        #         self.gorgupload.append((up_vis, usu, servei, modul,'2 - VISITES1 flt ATRIBUT',n))
        # for (up_vis, usu, servei, modul), n in self.gorg4.items():
        #     if up_vis in ('00274','00276','00108','00441'):
        #         self.gorgupload.append((up_vis, usu, servei, modul,'3 - VISITES1 flt EXCLUSIONS',n))
        # file = u.tempFolder + "gis_validacion.csv"
        # u.writeCSV(file, self.gorgupload, sep=";")
        # subject = 'Gestió de la demanda (validació)'
        # text = 'Adjuntem arxiu amb les dades'
        # u.sendGeneral('SISAP <sisap@gencat.cat>',
        #               'mbustos.bnm.ics@gencat.cat ',
        #               'ehermosilla@idiapjgol.info',
        #               subject,
        #               text,
        #               file)
        # remove(file)

    # def export_data(self):
    #     """."""
    #     self.export = []
    #     for (up, comb, ana), n in self.indicador_up.items():
    #         num = dades["NUM"]
    #         den = dades["DEN"]
    #         ind = str(round(100 * num / float(den), 2)).replace(".", ",")
    #         this = self.centres[up] + (num, den, ind)
    #         self.export.append(this)
    #     u.writeCSV(u.tempFolder + "gis_up.csv", self.export, sep=";")

 


    # Creació Taules MST

    # MST_INDICADORS_PACIENT
    def create_table(self):
        """ Creació tabla MST_INDICADORS_PACIENT """
        cols = "(id_cip_sec int, up varchar(5), edat int, sexe varchar(1), \
                 ates int, instit int, indicador varchar(10), compleix int, \
                 exclos int, num int, den int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)

    def get_master(self):
        """."""
        print("--------------------------------------------------- get_master")
        ts = datetime.now()
        self.totomaster = {}
        for (id_cip_sec, up, analisi), n in self.tomaster.items():
            if analisi == "DEN":
                den = n
                num = self.tomaster[(id_cip_sec, up, "NUM")]
                self.totomaster[(id_cip_sec, up)] = {'num': num, 'den': den}
        print('Time execution {}'.format(datetime.now() - ts))                
        self.master = []
        # edats = {cod: spec["edat"] for (cod, spec) in self.indicadors.items()}
        sql = "select id_cip_sec, uporigen, edat, sexe, \
                      ates, institucionalitzat \
               from assignada_tot"
        for id_cip_sec, up, edat, sexe, ates, instit in u.getAll(sql, "nodrizas"):
            #for ind, pacients in self.pacients.items():
            #    if edats[ind][0] <= edat <= edats[ind][1]:
            ind = 'GIS001'            
            if id_cip_sec in self.pacients[ind]["denominador"]:
                num = self.totomaster[(id_cip_sec, up)]['num'] if (id_cip_sec, up) in self.totomaster else 0
                den = self.totomaster[(id_cip_sec, up)]['den'] if (id_cip_sec, up) in self.totomaster else 0
                # good = 1 * (id_cip_sec in self.pacients[ind]["numerador"])
                if num == den:
                    good = 1
                else:
                    good = 0
                # # #            excl = 1 * (id in pacients["exclusio"])             
                excl = 0
                this = (id_cip_sec, up, edat, sexe, ates, instit, ind, good, excl, num, den)  # noqa
                self.master.append(this)
        print('Time execution {}'.format(datetime.now() - ts))

    def upload_master(self):
        """."""
        print("------------------------------------------------ upload_master")
        ts = datetime.now()
        u.listToTable(self.master, TABLE, DATABASE)
        print('Time execution {}'.format(datetime.now() - ts))

    # MST_PROFESSIONALS
    # Nota: No está Mercé en ninguna de las 2 tablas (cat_profvisual i cat_professionals). Utilizar cat_pritb992
    def get_professionals(self):
        """ time execution: DEBUG: 12s / NO DEBUG: 2m 45s """
        print("-------------------------------------------- get_professionals")
        ts = datetime.now()
        self.professionals = c.defaultdict(set)
        upload = set()
        # Select de ODONTO
        # sql = "select distinct a.up, left(a.ide_dni, 8) \
        #       from cat_profvisual a \
        #       where a.visualitzacio = 1 \
        #       and exists (select 1 from cat_professionals b \
        #                   where a.ide_usuari = b.ide_usuari and b.tipus = 'T')"
        sql = "select codi_sector, visi_assign_visita, visi_up, count(1) as n \
               from {{}}, nodrizas.dextraccio \
               where visi_data_visita between adddate(data_ext, interval -1 year) and data_ext and \
                     visi_up in {} and \
                     visi_servei_codi_servei in {} and \
                     visi_tipus_visita not in ('9E') and \
                     visi_forcada_s_n = 'N' and \
                     visi_internet <> 'S' \
                group by codi_sector, visi_assign_visita, visi_up".format(tuple(self.centres), SERVEIS)
        crt = "show create table visites1"
        tables = u.getOne(crt, "import")[1].split("UNION=(")[1][:-1].split(",")
        jobs = [(sql.format(table), "import") for table in tables] if NODEBUG else [(sql.format('visites_sys_p7161'), "import")]  # noqa
        for mes in u.multiprocess(_worker, jobs):
            for sec, usu, up, n in mes:
                if (sec, usu) in self.dni:
                    dni = self.dni[(sec, usu)]['dni']
                    self.professionals[up].add(dni)
                    upload.add((up, dni))
        print('Time execution {} / centres {} / up-professionals {}'.format(datetime.now() - ts,
                                                  len(self.professionals),
                                                  len(upload)))
        # print(upload)
        # u.createTable(PROFESSIONALS, '(up varchar(5), dni varchar(20))',
        #               DATABASE, rm=True)
        # u.listToTable(upload, PROFESSIONALS, DATABASE)

    # MST_U11




    # Creació Taules per SISAP-ECAP
    # ECAP_IND = "exp_ecap_uba_gis"
    # ECAP_PAC = "exp_ecap_pacient_gis"

    # EXP_ECAP_UBA_GIS
    def set_ecap_indicadors(self):
        """."""
        print("------------------------------------------ set_ecap_indicadors")
        ts = datetime.now()        
        dades = []
        ubas = set()
        # sql = "select up, '0', indicador, sum(compleix), count(1), \
        #               sum(compleix) / count(1) \
        #        from {} \
        #        where instit = 0 and \
        #              ates = 1 and \
        #              exclos = 0 \
        #        group by up, indicador".format(TABLE)        
        # sql = "select up, '0', indicador, sum(num), sum(den), \
        #               sum(num) / sum(den) \
        #        from {} \
        #        where instit = 0 and \
        #              ates = 1 and \
        #              exclos = 0 \
        #        group by up, indicador".format(TABLE)
        # sql += " union select 'UP', dni, indicador, sum(compleix), count(1), \
        #               sum(compleix) / count(1) \
        #        from {} a \
        #        inner join mst_professionals b on a.up = b.up \
        #        where instit = 0 and \
        #              ates = 1 and \
        #              exclos = 0 \
        #        group by dni, indicador".format(BASE)
        #for up, uba, ind, num, den, res in u.getAll(sql, DATABASE):
        for (up, comb, analisis), n in self.indicador_up.items():
            uba = '0'
            ind = 'GIS001'
            if analisis == 'DEN':
                den = n
                num = self.indicador_up[(up, comb, 'NUM')]
                res = num / den
            # mmin = self.metes.get((ind, "AGMMINRES"), 0)
            # mmax = self.metes.get((ind, "AGMMAXRES"), 0)
            # pond = self.ponderacio[ind]
            # punts = pond if res >= mmax else 0 if res < mmin else pond * ((float(res) - mmin) / (mmax - mmin))  # noqa
            # resolucioPerPunts = 1 if res >= mmax else 0 if res < mmin else (float(res) - mmin) / (mmax - mmin)  # noqa
                mmin = 0
                mmax = 0
                pond = 0
                punts = 0
                resolucioPerPunts = 0
                this = (up, uba, "A", ind, 0, den, num, 1, 1, res, res,
                    res, punts, den - num, resolucioPerPunts, 0, mmin, mmax)
                dades.append(this)
            #ubas.add((up, uba, "A"))
        for (up, dni, comb, analisis), n in self.indicador_uba.items():
            ind = 'GIS001'
            if analisis == 'DEN':
                den = n
                num = self.indicador_uba[(up, dni, comb, 'NUM')]
                res = num / den
            # mmin = self.metes.get((ind, "AGMMINRES"), 0)
            # mmax = self.metes.get((ind, "AGMMAXRES"), 0)
            # pond = self.ponderacio[ind]
            # punts = pond if res >= mmax else 0 if res < mmin else pond * ((float(res) - mmin) / (mmax - mmin))  # noqa
            # resolucioPerPunts = 1 if res >= mmax else 0 if res < mmin else (float(res) - mmin) / (mmax - mmin)  # noqa
                mmin = 0
                mmax = 0
                pond = 0
                punts = 0
                resolucioPerPunts = 0
                this = (up, dni, "A", ind, 0, den, num, 1, 1, res, res,
                    res, punts, den - num, resolucioPerPunts, 0, mmin, mmax)
                dades.append(this)            
        cols = "(up varchar(5), uba varchar(10), tipus varchar(1), \
                 indicador varchar(8), esperats double, detectats int, \
                 resolts int, deteccio double, deteccioPerResultat double, \
                 resolucio double, resultat double, resultatPerPunts double, \
                 punts double, llistat int, resolucioPerPunts double, \
                 invers int, mmin_p double, mmax_p double)"
        u.createTable(ECAP_IND, cols, DATABASE, rm=True)
        u.listToTable(dades, ECAP_IND, DATABASE)
        # cols = "(up varchar(5), uba varchar(10), tipus varchar(1))"
        # u.createTable(UBAS, cols, DATABASE, rm=True)
        # u.listToTable(list(ubas), UBAS, DATABASE)

    # EXP_ECAP_PACIENTS_GIS
    def set_ecap_pacients(self):
        """."""
        sql = "select id_cip_sec, up, indicador, \
                      if(instit = 1, 2, \
                         if(ates = 0, 5, \
                            if(exclos = 1, 4, 0))) \
               from {} \
               where compleix = 0".format(BASE)
        dades = [row + self.id_to_hash[row[0]] for row
                 in u.getAll(sql, DATABASE)]
        cols = "(id_cip_sec int, up varchar(5), indicador varchar(10), \
                 exclos int, hash_d varchar(40), sector varchar(4))"
        u.createTable(ECAP_PAC, cols, DATABASE, rm=True)
        u.listToTable(dades, ECAP_PAC, DATABASE)
        cataleg = [(0, "Pacients que formen part de l'indicador", 0),
                   (2, "Pacients institucionalitzats", 1),
                   (4, "Pacients exclosos per motius clínics", 2),
                   (5, "Pacients no atesos el darrer any", 3)]
        cols = "(codi int, descripcio varchar(255), ordre int)"
        u.createTable(CAT_EXC, cols, DATABASE, rm=True)
        u.listToTable(cataleg, CAT_EXC, DATABASE)

    # CATÀLEGS
    def get_cataleg(self):
        """."""

        self.uploadcat = []
        # khalix = []
        # file = 'dades_noesb/indicadors.json'
        # cataleg = j.load(open(file), encoding="latin1")
        umi = "http://10.80.217.201/sisap-umi/indicador/indicador/"
        
        for codi, dades in INDICADORS.items():
            this = (codi, 
                    dades["literal"].encode("latin1"),
                    int(codi[-2:]),
                    dades["pare"], 
                    1, 
                    0, 
                    0, 
                    0, # self.metes.get((codi, "AGMMINRES"), 0),
                    0, # self.metes.get((codi, "AGMINTRES"), 0),
                    0, # self.metes.get((codi, "AGMMAXRES"), 0),
                    1, 
                    umi + codi,
                    "",
                    0, # rsomin ¿qué es?
                    0, # rsomax ¿qué es?
                    "GENERAL")
            self.uploadcat.append(this)

        # Creació taula EXP_ECAP_CATALEG_GIS
        cols = "(indicador varchar(8), \
                 literal varchar(255), \
                 ordre int, \
                 pare varchar(8), \
                 llistat int, \
                 invers int, \
                 mdet double, \
                 mmin double, \
                 mint double, \
                 mmax double, \
                 toShow int, \
                 wiki varchar(255), \
                 curt varchar(80),  \
                 rsomin double, \
                 rsomax double, \
                 pantalla varchar(50))"
        u.createTable(CAT_IND, cols, DATABASE, rm=True)
        u.listToTable(self.uploadcat, CAT_IND, DATABASE)

        # Creació taula EXP_ECAP_CATALEGPARE_GIS
        cols = "(pare varchar(10), \
                 literal varchar(255), \
                 ordre int, \
                 curt varchar(80), \
                 proces varchar(50))"
        u.createTable(CAT_PARE, cols, DATABASE, rm=True)
        pare = 'PROGMOT'
        pare_lit = 'Indicadors Programacio per Motius'
        dades = [(pare, pare_lit, 1, "P. Motius", "")]
        u.listToTable(dades, CAT_PARE, DATABASE)

        # CATÀLEG KHALIX
        # cols = "(indicador varchar(10), desc_ind varchar(255), \
        #          grup varchar(10), grup_desc varchar(255))"
        # u.createTable(CAT_KLX, cols, DATABASE, rm=True)
        # u.listToTable(khalix, CAT_KLX, DATABASE)

    # Creació de Taules per KHALIX
    def export_khalix(self):
        """."""
        print("------------------------------------------------ export_khalix")
        ts = datetime.now()
        self.upload = []
        u.createTable('exp_khalix_up_gis', \
                     '(indicador varchar(12), br varchar(5), \
                       comb varchar(10), \
                       analisis varchar(10), n int)', DATABASE, rm=True)
        for (up, comb, analisis), n in self.indicador_up.items():
            ind = 'GIS001'
            br = self.centres[up]['br']
            self.upload.append((ind, br, comb, analisis, n))
        u.listToTable([row for row in self.upload],
                      'exp_khalix_up_gis', DATABASE)

        self.upload = []
        u.createTable('exp_khalix_uba_gis', \
                     '(indicador varchar(12), entity varchar(25), \
                       comb varchar(10), \
                       analisis varchar(10), n int)', DATABASE, rm=True)
        for (up, dni, comb, analisis), n in self.indicador_uba.items():
            ind = 'GIS001'
            br = self.centres[up]['br']
            ent = br + dni
            self.upload.append((ind, ent, comb, analisis, n))
        u.listToTable([row for row in self.upload],
                      'exp_khalix_uba_gis', DATABASE)

        # Exportació
        sql = "select indicador, 'Aperiodo', br, analisis, 'NOCAT', \
               'NOIMP', 'DIM6SET', 'N', n from {}.{}".format(DATABASE, 'exp_khalix_up_gis')
        file = 'GIS_UP'
        u.exportKhalix(sql, file)
        print('Time execution {}'.format(datetime.now() - ts))

        sql = "select indicador, 'Aperiodo', entity, analisis, 'NOCAT', \
               'NOIMP', 'DIM6SET', 'N', n from {}.{}".format(DATABASE, 'exp_khalix_uba_gis')
        file = 'GIS_UBA'
        u.exportKhalix(sql, file)
        print('Time execution {}'.format(datetime.now() - ts))

def _worker(params):
    return list(u.getAll(*params))


if __name__ == "__main__":
    ts = datetime.now()
    GIS()
    print('Time execution {}'.format(datetime.now() - ts))
