# coding: latin1

"""

Trello: https://trello.com/c/bEZxhRZK

Drive: https://docs.google.com/document/d/1XZWU2ZLi6X52XQXXoKXxrxWEFzrOCDYpbHAzTX_hNrg/edit?usp=sharing

"""

# from _typeshed import Self
import collections as c
import sisapUtils as u
from os import remove
import datetime as d
from datetime import datetime

NODEBUG = True # Poner False para ejecutar en DEBUG / Local
SECTOR = '6837'

DATABASE = 'Gis'

SERVEIS = ("MG", "PED", "INF", "ENF", "INFPD", "INFP", "INFG", "URGEN", "ESP") # Serveis especialitats agendes
USUARIS = ("04104", "04416", "04302") # Usuaris administratius
ATRIBUTS = ("Assistencial - UBA", "Assistencial No - UBA")

TABLE = 'mst_gis_pacient' # o 'mst_indicadors_pacient' (si es crea carpeta pròpia)

ECAP_IND = "exp_ecap_uba_gis"
ECAP_PAC = "exp_ecap_pacient_gis"

CAT_IND = "exp_ecap_cataleg_gis"
CAT_PARE = "exp_ecap_catalegpare_gis"


class CATALEGS_TAULES():
    def __init__(self):
        """."""
        self.INDICADORS = {"GIS001": {"literal": "Percentatge de visites programades amb motiu de consulta",
                            "pare": "PROGMOT"},
                            "GIS002": {"literal": "Visites amb motiu recollit per la pantala pxm",
                            "pare": "PROGMOT"},
                            "GIS003": {"literal": "Visites amb motiu registrat que no son de pxm",
                            "pare": "PROGMOT"},
                            "GIS005": {"literal": "Percentatge de visites amb motiu recollit per la pantalla PxM i ateses pel servei MG",
                            "pare": "PROGMOT"}}
        self.pare = 'PROGMOT'
        self.pare_lit = 'Indicadors Programacio per Motius'
        self.dades = [(self.pare, self.pare_lit, 1, "P. Motius", "")]
        self.get_cataleg()
        self.get_cataleg_pare()
        self.create_tables()
        self.create_khalix()

    def get_cataleg(self):
        """Creació taula EXP_ECAP_CATALEG_GIS"""
        self.uploadcat = []
        umi = "http://10.80.217.201/sisap-umi/indicador/indicador/"
        
        for codi, dades in self.INDICADORS.items():
            this = (codi, dades["literal"].encode("latin1"), int(codi[-2:]), dades["pare"], 
                    1, 0, 0, 
                    0, # self.metes.get((codi, "AGMMINRES"), 0),
                    0, # self.metes.get((codi, "AGMINTRES"), 0),
                    0, # self.metes.get((codi, "AGMMAXRES"), 0),
                    1, umi + codi, "",
                    0, # rsomin ¿qué es?
                    0, # rsomax ¿qué es?
                    "GENERAL")
            self.uploadcat.append(this)

        cols = "(indicador varchar(8), literal varchar(255), ordre int, pare varchar(8), \
                 llistat int, invers int, mdet double, mmin double,  mint double, mmax double, \
                 toShow int, wiki varchar(255), curt varchar(85), rsomin double, rsomax double, \
                 pantalla varchar(50))"

        u.createTable(CAT_IND, cols, DATABASE, rm=True)
        u.listToTable(self.uploadcat, CAT_IND, DATABASE)

    def get_cataleg_pare(self):
        """Creació taula EXP_ECAP_CATALEGPARE_GIS"""
        cols = "(pare varchar(10), literal varchar(255), ordre int, \
                 curt varchar(80), proces varchar(50))"
        
        u.createTable(CAT_PARE, cols, DATABASE, rm=True)
        u.listToTable(self.dades, CAT_PARE, DATABASE)
    
    def create_tables(self):
        """ Creació taula MST_INDICADORS_PACIENT i EXP_ECAP_UBA_GIS"""
        cols = "(id_cip_sec int, up varchar(5), edat int, sexe varchar(1), \
                 ates int, instit int, indicador varchar(10), compleix int, \
                 exclos int, num int, den int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)

        cols = "(up varchar(5), uba varchar(10), tipus varchar(1), \
                 indicador varchar(8), esperats double, detectats int, \
                 resolts int, deteccio double, deteccioPerResultat double, \
                 resolucio double, resultat double, resultatPerPunts double, \
                 punts double, llistat int, resolucioPerPunts double, \
                 invers int, mmin_p double, mmax_p double)"
        u.createTable(ECAP_IND, cols, DATABASE, rm=True)
    
    def create_khalix(self):
        u.createTable('exp_khalix_up_gis', \
                        '(indicador varchar(12), br varchar(5), \
                        comb varchar(10), analisis varchar(10), n int)',
                        DATABASE, rm=True)
        u.createTable('exp_khalix_uba_gis', \
                     '(indicador varchar(12), entity varchar(25), \
                       comb varchar(10), analisis varchar(10), n int)', 
                       DATABASE, rm=True)


def get_centres():
    """."""
    centres = {}
    sql = """select 
                scs_codi, ics_codi 
            from 
                cat_centres"""
    for up, br in u.getAll(sql, "nodrizas"):
        centres[up] = {'br': br} 
    print("CENTRES: ", len(centres))
    return centres

def get_usuaris():
    """."""
    usuaris = set()
    dnis = {}
    sql = """select 
                codi_sector, ide_usuari, left(ide_dni, 8) 
            from 
                cat_pritb992 
            where 
                ide_categ_prof_c in {} and 
                (substring(ide_usuari,1,1) = 'P' or 
                substring(ide_usuari,1,4) = 'OPS$')""".format(USUARIS)
    for sector, usu, dni in u.getAll(sql, 'import'):
        usuaris.add((sector, usu))
        dnis[(sector, usu)] = dni
    print("USUARIS: ", len(usuaris))
    print("DNI: ", len(dnis))
    return usuaris, dnis

# class GIS001(object):
#     """Creació indicador GIS001"""

#     def __init__(self, centres, usuaris, dni):
#         """."""
#         self.centres = centres
#         self.usuaris = usuaris
#         self.dni = dni
#         self.get_atribut()        
#         self.get_tasques()
#         self.get_numerador()
#         self.get_pim()
#         self.get_indicador()             
#         self.get_master()
#         self.upload_master()
#         self.set_ecap_indicadors()
#         self.export_khalix()

class GIS001(object):
    """Creació indicador GIS001"""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_usuaris()
        self.get_atribut()        
        self.get_tasques()
        self.get_numerador()
        self.get_pim()
        self.get_indicador()             
        self.get_master()
        self.upload_master()
        self.set_ecap_indicadors()
        self.export_khalix()

    def get_centres(self):
         """."""
         self.centres = {}
         sql = """select 
                     scs_codi, ics_codi 
                 from 
                     cat_centres"""
         for up, br in u.getAll(sql, "nodrizas"):
             self.centres[up] = {'br': br} 
         print("CENTRES: ", len(self.centres))

    def get_usuaris(self):
         """."""
         self.usuaris = set()
         self.dni = {}
         sql = """select 
                     codi_sector, ide_usuari, left(ide_dni, 8) 
                from 
                     cat_pritb992 
                where 
                     ide_categ_prof_c in {} and 
                     (substring(ide_usuari,1,1) = 'P' or 
                      substring(ide_usuari,1,4) = 'OPS$')""".format(USUARIS)
         for sector, usu, dni in u.getAll(sql, 'import'):
             self.usuaris.add((sector, usu))
             self.dni[(sector, usu)] = {'dni': dni}
         print("USUARIS: ", len(self.usuaris))
         print("DNI: ", len(self.dni))

    def get_atribut(self):
         """ . """
         self.atribut = {}
         sql = """select 
                     eap, servei, modul, atribut_1 
                 from 
                     sisap_agendes_atributs 
                 where 
                     atribut_1 in {}""".format(ATRIBUTS)
         for br, servei, modul, atribut in u.getAll(sql, 'redics'):
             self.atribut[(br, servei, modul)] = {'atribut': atribut}
         print("ATRIBUTS: ", len(self.atribut))

    def get_tasques(self):
        """."""
        print("-------------------------------------------------- get_tasques")
        self.tasques = set()
        sql = """select 
                    id_cip_sec, alv_data_al 
               from 
                    alertes_s{}, nodrizas.dextraccio
               where 
                    alv_tipus = 'TASCAD' and
                    alv_data_al between adddate(data_ext, interval -1 year) 
                    and data_ext"""
        jobs = [(sql.format(sector), "import") for sector in u.sectors] if NODEBUG else [(sql.format(SECTOR), "import")] 
        for sector in u.multiprocess(_worker, jobs, 8):
            self.tasques |= set(sector)
        print("TASQUES", len(self.tasques))
        print('Time execution {}'.format(datetime.now() - ts))

    def get_numerador(self):
        """."""
        print("------------------------------------------------ get_numerador")
        ts = datetime.now()
        self.numerador = set()
        sql = """select 
                    visi_id 
               from 
                    motius_s{}, nodrizas.dextraccio 
               where 
                    visi_data_visita between adddate(data_ext, interval -1 year) and 
                    data_ext and 
                    (visi_motiu_prior not in ('', 'CAP') or 
                    visi_usuari_text <> '' or 
                    visi_motiu_lliure <> '')"""

        jobs = [(sql.format(sector), "import") for sector in u.sectors] if NODEBUG else [(sql.format(SECTOR), "import")] 

        for sector in u.multiprocess(_worker, jobs, 8):
            pacients = set([id for id, in sector])
            self.numerador |= pacients

        print("MOTIUS (NUMERADOR):", len(self.numerador))
        print('Time execution {}'.format(datetime.now() - ts))

    def get_pim(self):
        """."""
        print("------------------------------------------------------ get_pim")
        ts = datetime.now()
        self.pim = set()
        sql = """select 
                    visi_id 
               from 
                    motius_s{}, nodrizas.dextraccio 
               where 
                    visi_data_visita between adddate(data_ext, interval -1 year) and 
                    data_ext and 
                    visi_ticket_teseo <> ''"""

        jobs = [(sql.format(sector), "import") for sector in u.sectors] if NODEBUG else [(sql.format(SECTOR), "import")]

        for sector in u.multiprocess(_worker, jobs, 8):
            pacients = set([id for id, in sector])
            self.pim |= pacients

        print("PIM", len(self.pim))
        print('Time execution {}'.format(datetime.now() - ts))

    def get_indicador(self):
        """ Time Execution: DEBUG 1m / NOT DEBUG: """
        print("------------------------------------------------ get_indicador")
        ts = datetime.now()
        self.tomaster = c.Counter()
        self.indicador_up = c.Counter()
        self.indicador_uba = c.Counter()
        self.pacients = c.defaultdict(lambda: c.defaultdict(set))
        sql = """select 
                    visi_id, id_cip_sec, visi_data_visita, visi_up, codi_sector, visi_assign_visita, 
                    visi_servei_codi_servei, visi_modul_codi_modul 
               from 
                    {{}}, nodrizas.dextraccio 
               where 
                    visi_data_visita between adddate(data_ext, interval -1 year) and 
                    data_ext and 
                    visi_up in {} and 
                    visi_servei_codi_servei in {} and 
                    visi_tipus_visita not in ('9E') and 
                    visi_forcada_s_n = 'N' and 
                    visi_internet <> 'S'""".format(tuple(self.centres), SERVEIS)

        crt = "show create table visites1"
        tables = u.getOne(crt, "import")[1].split("UNION=(")[1][:-1].split(",")

        jobs = [(sql.format(table), "import") for table in tables] if NODEBUG else [(sql.format('visites_sys_p6441'), "import")]

        for mes in u.multiprocess(_worker, jobs):
            for id, pac, dat, up_vis, sec, usu, servei, modul in mes:
                if (sec, usu) in self.usuaris:
                    br_vis = self.centres[up_vis]['br']
                    if (br_vis, servei, modul) in self.atribut:
                        dni = self.dni[(sec, usu)]['dni']                     
                        if (pac, dat) not in self.tasques and id not in self.pim:                            
                            self.tomaster[(pac, up_vis, "DEN")] += 1
                            self.tomaster[(pac, up_vis, "NUM")] += id in self.numerador
                            self.indicador_up[(up_vis, 'NOIMP', "DEN")] += 1
                            self.indicador_up[(up_vis, 'NOIMP', "NUM")] += id in self.numerador
                            self.indicador_uba[(up_vis, dni, 'NOIMP', "DEN")] += 1
                            self.indicador_uba[(up_vis, dni, 'NOIMP', "NUM")] += id in self.numerador
                            self.pacients['GIS001']["denominador"].add(pac)
                            if id in self.numerador:
                                self.pacients['GIS001']["numerador"].add(pac)

        print("MASTER:", len(self.tomaster))
        print("INDICADOR UP:", len(self.indicador_up))
        print("INDICADOR UBA:", len(self.indicador_uba))
        print(len(self.pacients['GIS001']["denominador"]))
        print(len(self.pacients['GIS001']["numerador"]))
        print('Time execution {}'.format(datetime.now() - ts))

    def get_master(self):
        """."""
        print("--------------------------------------------------- get_master")
        ts = datetime.now()
        self.totomaster = {}
        for (id_cip_sec, up, analisi), n in self.tomaster.items():
            if analisi == "DEN":
                den = n
                num = self.tomaster[(id_cip_sec, up, "NUM")]
                self.totomaster[(id_cip_sec, up)] = {'num': num, 'den': den}
        print('Time execution {}'.format(datetime.now() - ts))                
        self.master = []
        sql = """select 
                    id_cip_sec, uporigen, edat, sexe, ates, institucionalitzat 
               from 
                    assignada_tot"""
        for id_cip_sec, up, edat, sexe, ates, instit in u.getAll(sql, "nodrizas"):
            ind = 'GIS001'            
            if id_cip_sec in self.pacients[ind]["denominador"]:
                num = self.totomaster[(id_cip_sec, up)]['num'] if (id_cip_sec, up) in self.totomaster else 0
                den = self.totomaster[(id_cip_sec, up)]['den'] if (id_cip_sec, up) in self.totomaster else 0
                if num == den:
                    good = 1
                else:
                    good = 0           
                excl = 0
                this = (id_cip_sec, up, edat, sexe, ates, instit, ind, good, excl, num, den)  # noqa
                self.master.append(this)
        print('Time execution {}'.format(datetime.now() - ts))

    def upload_master(self):
        """."""
        print("------------------------------------------------ upload_master")
        ts = datetime.now()
        u.listToTable(self.master, TABLE, DATABASE)
        print('Time execution {}'.format(datetime.now() - ts))

    def set_ecap_indicadors(self):
        """."""
        print("------------------------------------------ set_ecap_indicadors")
        ts = datetime.now()        
        dades = []
        ubas = set()
        for (up, comb, analisis), n in self.indicador_up.items():
            uba = '0'
            ind = 'GIS001'
            if analisis == 'DEN':
                den = n
                num = self.indicador_up[(up, comb, 'NUM')]
                res = num / den
                mmin, mmax, pond, punts, resolucioPerPunts = 0, 0, 0, 0, 0
                this = (up, uba, "A", ind, 0, den, num, 1, 1, res, res,
                    res, punts, den - num, resolucioPerPunts, 0, mmin, mmax)
                dades.append(this)

        for (up, dni, comb, analisis), n in self.indicador_uba.items():
            ind = 'GIS001'
            if analisis == 'DEN':
                den = n
                num = self.indicador_uba[(up, dni, comb, 'NUM')]
                res = num / den
                mmin, mmax, pond, punts, resolucioPerPunts = 0, 0, 0, 0, 0
                this = (up, dni, "A", ind, 0, den, num, 1, 1, res, res,
                    res, punts, den - num, resolucioPerPunts, 0, mmin, mmax)
                dades.append(this)

        u.listToTable(dades, ECAP_IND, DATABASE)

    def export_khalix(self):
        """Creació de Taules per KHALIX"""
        print("------------------------------------------------ export_khalix")
        ts = datetime.now()
        self.upload = []
        for (up, comb, analisis), n in self.indicador_up.items():
            ind = 'GIS001'
            br = self.centres[up]['br']
            self.upload.append((ind, br, comb, analisis, n))

        u.listToTable([row for row in self.upload], 'exp_khalix_up_gis', DATABASE)

        self.upload = []
        for (up, dni, comb, analisis), n in self.indicador_uba.items():
            ind = 'GIS001'
            br = self.centres[up]['br']
            ent = br + dni
            self.upload.append((ind, ent, comb, analisis, n))

        u.listToTable([row for row in self.upload], 'exp_khalix_uba_gis', DATABASE)

class GIS004(object):
    """Creació indicador GIS004"""
    def __init__(self, centres, usuaris, dni, v_motius_den, v_motius_num, agendes):
        """."""
        print('GIS004')
        self.centres = centres
        self.usuaris = usuaris
        self.dni = dni
        self.visites_motius_den = v_motius_den
        self.visites_motius_num = v_motius_num
        self.agendes = agendes
        self.get_indicador()
        self.get_master()
        self.upload_master()
        self.set_ecap_indicadors()
        self.export_khalix()

    def get_indicador(self):
        print('--------------------------------------get_indicador')
        ts = datetime.now()
        print('select visites')
        # Prenem les visites d'aquestes agendes
        visites_agendes = {}
        visi_inf = set()
        sql_visites = """select
                        visi_id, visi_modul_codi_modul, visi_centre_classe_centre, 
                        visi_servei_codi_servei, codi_sector , visi_up, visi_assign_visita
                    from
                        import.visites1, nodrizas.dextraccio
                    where 
                        visi_data_visita between adddate(data_ext, interval -1 year) and 
                        data_ext and 
                        visi_servei_codi_servei in {} and 
                        visi_tipus_visita not in ('9E') and 
                        visi_forcada_s_n = 'N' and 
                        visi_internet <> 'S' and
                        s_espe_codi_especialitat not in {} and
                        (substr(visi_assign_visita,1,1) = 'P' or
                        substr(visi_assign_visita,1,4) = 'OPS$')""".format(SERVEIS, USUARIS)

        for visita, modul, centre, servei, sector, up, usu in u.getAll(sql_visites, "import"):
            key = (modul, servei, centre, sector)
            if key in visites_agendes:
                visites_agendes[key].append((visita, up, usu, sector))
            else:
                visites_agendes[key] = [(visita, up, usu, sector)]
            if servei == 'INF': 
                visi_inf.add(visita)
        
        print('visites_agendes', len(visites_agendes))
        print('visites_infermeria num', len(visi_inf))
        print('calcul denominador')

        # Anem a prendre les visites del denominador
        self.denominador = c.defaultdict(int)
        self.visites_den = set()
        self.ups_den = c.defaultdict(int)
        self.ups_usu_den = c.defaultdict(int)
        visi_up = dict()
        for e in visites_agendes.keys():
            if e in self.agendes: 
                for (visita, up, usu, sector) in visites_agendes[e]:
                    if (sector, usu) in self.usuaris:
                        try:
                            # Nombre de vegades que els pacients compleixen denominador
                            self.denominador[self.visites_motius_den[visita]]+= 1
                            # Visites que comepleixen els requisits
                            self.visites_den.add(visita)
                            # El mateix per ub
                            self.ups_den[up] += 1 # Denominador per up
                            visi_up[visita] = (up, usu, sector)
                            self.ups_usu_den[(up, usu, sector)] += 1
                        except:
                            pass
        print('calcul numerador')
        visites_num = set(self.visites_motius_num.keys()) & visi_inf
        print('visites_num', len(visites_num))

        self.numerador = c.defaultdict(int)
        self.ups_num = c.defaultdict(int)
        self.ups_usu_num = c.defaultdict(int)
        for v in visites_num:
            if v in self.visites_den:
                up, usu, sector = visi_up[v]
                self.ups_num[up] += 1
                self.numerador[self.visites_motius_num[v]] += 1
                self.ups_usu_num[(up, usu, sector)] += 1
        
        print('denominador', len(self.denominador))
        print('numerador', len(self.numerador))
        print('Time execution {}'.format(datetime.now() - ts))

    def get_master(self):
        """Creem dades amb format a les que necessitem a la taula"""
        print("--------------------------------------------------- get_master")
        ts = datetime.now()            
        self.master = []
        sql = """select 
                    id_cip_sec, uporigen, edat, sexe, ates, institucionalitzat 
               from 
                    assignada_tot"""
        assignada_tot = dict()   

        for id_cip_sec, up, edat, sexe, ates, instit in u.getAll(sql, "nodrizas"):
            assignada_tot[id_cip_sec] = (up, edat, sexe, ates, instit)
        print("tinc la info de l'assignada :)))), anem a crear")
        long_assign = len(assignada_tot)
        print(long_assign)

        ts1 = datetime.now() 
        for id, dades in assignada_tot.items():        
            den = self.denominador.get(id)
            if den:
                num = self.numerador.get(id, 0)
                good = num == den     
                this = (id,) + dades + ('GIS004', good, 0, num, den)  # noqa
                self.master.append(this)

        print('Time execution {}'.format(datetime.now() - ts))

    def upload_master(self):
        """."""
        cols = "(id_cip_sec int, up varchar(5), edat int, sexe varchar(1), \
                 ates int, instit int, indicador varchar(10), compleix int, \
                 exclos int, num int, den int)"
        u.createTable('mst_gis_pacient_proves', cols, DATABASE, rm=True)
        print("------------------------------------------------ upload_master")
        ts = datetime.now()
        u.listToTable(self.master, 'mst_gis_pacient_proves', DATABASE)
        print('Time execution {}'.format(datetime.now() - ts))

    def set_ecap_indicadors(self):
        """."""
        print("------------------------------------------ set_ecap_indicadors")
        ts = datetime.now()        
        dades = []
        for up, den in self.ups_den.items():
            num = self.ups_num[up] if up in self.ups_num.keys() else 0
            res = num / den
            # up, uba, "A", ind, 0, den, num, 1, 1, res, res, res, punts, den - num, resolucioPerPunts, 0, mmin, mmax
            this = (up, '0', 'A', 'GIS004', 0, den, num,
                    1, 1, res, res, res, 0, den-num, 0, 0, 0, 0)
            dades.append(this)

        for (up, usu, sector), den in self.ups_usu_den.items():
            num = self.ups_usu_num[(up, usu, sector)]
            res = num / den
            dni = self.dni[(sector, usu)]
            this = (up, dni, "A", 'GIS004', 0, den, num, 
                    1, 1, res, res, res, 0, den - num, 0, 0, 0, 0)
            dades.append(this)

        # u.listToTable(dades, ECAP_IND, DATABASE)
        cols = "(up varchar(5), uba varchar(10), tipus varchar(1), \
                 indicador varchar(8), esperats double, detectats int, \
                 resolts int, deteccio double, deteccioPerResultat double, \
                 resolucio double, resultat double, resultatPerPunts double, \
                 punts double, llistat int, resolucioPerPunts double, \
                 invers int, mmin_p double, mmax_p double)"
        u.createTable('exp_ecap_uba_gis_prova', cols, DATABASE, rm=True)
        u.listToTable(dades, 'exp_ecap_uba_gis_prova', DATABASE)

        print('Time execution {}'.format(datetime.now() - ts))
        

    def export_khalix(self):
        u.createTable('exp_khalix_up_gis_prova', \
                '(indicador varchar(12), br varchar(5), \
                comb varchar(10), analisis varchar(10), n int)',
                DATABASE, rm=True)
        u.createTable('exp_khalix_uba_gis_prova', \
                     '(indicador varchar(12), entity varchar(25), \
                       comb varchar(10), analisis varchar(10), n int)', 
                       DATABASE, rm=True)

        """Creació de Taules per KHALIX"""
        print("------------------------------------------------ export_khalix")
        ts = datetime.now()
        self.upload = []
        for up, den in self.ups_den.items():
            try:
                br = self.centres[up]['br']
                self.upload.append(('GIS004', br, 'NOIMP', 'DEN', den))
            except:
                pass

        for up, num in self.ups_num.items():
            try:
                br = self.centres[up]['br']
                self.upload.append(('GIS004', br, 'NOIMP', 'NUM', num))
            except:
                pass

        u.listToTable([row for row in self.upload], 'exp_khalix_up_gis_prova', DATABASE)

        self.upload = []
        for (up, usu, sector), den in self.ups_usu_den.items():
            try:
                br = self.centres[up]['br']
                dni = self.dni[(sector, usu)]
                ent = br + dni
                self.upload.append(('GIS004', ent, 'NOIMP', 'DEN', den))
            except:
                pass
        
        for (up, usu, sector), num in self.ups_usu_num.items():
            try:
                br = self.centres[up]['br']
                dni = self.dni[(sector, usu)]
                ent = br + dni
                self.upload.append(('GIS004', ent, 'NOIMP', 'NUM', num))
            except:
                pass

        u.listToTable([row for row in self.upload], 'exp_khalix_uba_gis_prova', DATABASE)
        print('Time execution {}'.format(datetime.now() - ts))


def get_motius():
    print('motius')
    visites_motius_den = {}
    # Numerador
    visites_motius_num = {}
    # Comprovem que tingui motiu
    sql_motius = """select
                        id_cip_sec, visi_id, visi_motiu_prior 
                    from 
                        import.motius_s{}, nodrizas.dextraccio 
                    where 
                        visi_data_visita between adddate(data_ext, interval -1 year) and data_ext
                        and (visi_usuari_text is not null or visi_motiu_lliure is not null or visi_motiu_prior not in ('', 'CAP'))
                        and visi_ticket_teseo is not null"""

    jobs = [(sql_motius.format(sector), "import") for sector in u.sectors]

    for sector in u.multiprocess(_worker, jobs, 8):
        for id, visita, prior in sector:
            visites_motius_den[visita] = id
            # Pertany al numerador
            if prior != '': 
                visites_motius_num[visita] = id
    
    print('visi_motius_den', len(visites_motius_den))
    print('visites_motiu_num', len(visites_motius_num))
    return visites_motius_den, visites_motius_num

def get_agendes():
    print('agendes')
    # Prenem agendes que ens interessen
    agendes = set()
    sql_agendes = """select
                        MODU_CODI_MODUL, MODU_SERVEI_CODI_SERVEI, MODU_CENTRE_CLASSE_CENTRE, CODI_SECTOR 
                    from
                        import.cat_vistb027
                    where
                        MODU_ACT_AGENDA in ('1', '2')
                """
    for modul, servei, centre, sector in u.getAll(sql_agendes, "import"):
        agendes.add((modul, servei, centre, sector))

    print('agendes: ', len(agendes))
    return agendes

class GIS005(object):
    """Creació indicador GIS005"""
    def __init__(self, centres, usuaris, dni, v_motius_den, v_motius_num, agendes):
        """."""
        print('GIS005')
        self.centres = centres
        self.usuaris = usuaris
        self.dni = dni
        self.visites_motius_den = v_motius_den
        self.visites_motius_num = v_motius_num
        self.agendes = agendes
        self.get_indicador()
        self.get_master()
        self.upload_master()
        self.set_ecap_indicadors()
        self.export_khalix()

    def get_indicador(self):
        print('--------------------------------------get_indicador')
        ts = datetime.now()
        print('select visites')
        # Prenem les visites d'aquestes agendes
        visites_agendes = {}
        visi_mg = set()
        sql_visites = """select
                        visi_id, visi_modul_codi_modul, visi_centre_classe_centre, 
                        visi_servei_codi_servei, codi_sector , visi_up, visi_assign_visita
                    from
                        import.visites1, nodrizas.dextraccio
                    where 
                        visi_data_visita between adddate(data_ext, interval -1 year) and 
                        data_ext and 
                        visi_servei_codi_servei in {} and 
                        visi_tipus_visita not in ('9E') and 
                        visi_forcada_s_n = 'N' and 
                        visi_internet <> 'S' and
                        s_espe_codi_especialitat not in {} and
                        (substr(visi_assign_visita,1,1) = 'P' or
                        substr(visi_assign_visita,1,4) = 'OPS$')""".format(SERVEIS, USUARIS)

        for visita, modul, centre, servei, sector, up, usu in u.getAll(sql_visites, "import"):
            key = (modul, servei, centre, sector)
            if key in visites_agendes:
                visites_agendes[key].append((visita, up, usu, sector))
            else:
                visites_agendes[key] = [(visita, up, usu, sector)]
            if servei == 'MG': 
                visi_mg.add(visita)
        
        print('visites_agendes', len(visites_agendes))
        print('visites_infermeria num', len(visi_mg))
        print('calcul denominador')

        # Anem a prendre les visites del denominador
        self.denominador = c.defaultdict(int)
        self.visites_den = set()
        self.ups_den = c.defaultdict(int)
        self.ups_usu_den = c.defaultdict(int)
        visi_up = dict()
        for e in visites_agendes.keys():
            if e in self.agendes: 
                for (visita, up, usu, sector) in visites_agendes[e]:
                    if (sector, usu) in self.usuaris:
                        try:
                            # Nombre de vegades que els pacients compleixen denominador
                            self.denominador[self.visites_motius_den[visita]]+= 1
                            # Visites que comepleixen els requisits
                            self.visites_den.add(visita)
                            # El mateix per ub
                            self.ups_den[up] += 1 # Denominador per up
                            visi_up[visita] = (up, usu, sector)
                            self.ups_usu_den[(up, usu, sector)] += 1
                        except:
                            pass
        print('calcul numerador')
        visites_num = set(self.visites_motius_num.keys()) & visi_mg
        print('visites_num', len(visites_num))

        self.numerador = c.defaultdict(int)
        self.ups_num = c.defaultdict(int)
        self.ups_usu_num = c.defaultdict(int)
        for v in visites_num:
            if v in self.visites_den:
                up, usu, sector = visi_up[v]
                self.ups_num[up] += 1
                self.numerador[self.visites_motius_num[v]] += 1
                self.ups_usu_num[(up, usu, sector)] += 1
        
        print('denominador', len(self.denominador))
        print('numerador', len(self.numerador))
        print('Time execution {}'.format(datetime.now() - ts))

    def get_master(self):
        """Creem dades amb format a les que necessitem a la taula"""
        print("--------------------------------------------------- get_master")
        ts = datetime.now()            
        self.master = []
        sql = """select 
                    id_cip_sec, uporigen, edat, sexe, ates, institucionalitzat 
               from 
                    assignada_tot"""
        assignada_tot = dict()   

        for id_cip_sec, up, edat, sexe, ates, instit in u.getAll(sql, "nodrizas"):
            assignada_tot[id_cip_sec] = (up, edat, sexe, ates, instit)
        print("tinc la info de l'assignada :)))), anem a crear")
        long_assign = len(assignada_tot)
        print(long_assign)

        ts1 = datetime.now() 
        for id, dades in assignada_tot.items():        
            den = self.denominador.get(id)
            if den:
                num = self.numerador.get(id, 0)
                good = num == den     
                this = (id,) + dades + ('GIS005', good, 0, num, den)  # noqa
                self.master.append(this)

        print('Time execution {}'.format(datetime.now() - ts))

    def upload_master(self):
        """."""
        cols = "(id_cip_sec int, up varchar(5), edat int, sexe varchar(1), \
                 ates int, instit int, indicador varchar(10), compleix int, \
                 exclos int, num int, den int)"
        u.createTable('mst_gis_pacient_proves_05', cols, DATABASE, rm=True)
        print("------------------------------------------------ upload_master")
        ts = datetime.now()
        u.listToTable(self.master, 'mst_gis_pacient_proves_05', DATABASE)
        print('Time execution {}'.format(datetime.now() - ts))

    def set_ecap_indicadors(self):
        """."""
        print("------------------------------------------ set_ecap_indicadors")
        ts = datetime.now()        
        dades = []
        for up, den in self.ups_den.items():
            num = self.ups_num[up] if up in self.ups_num.keys() else 0
            res = num / den
            # up, uba, "A", ind, 0, den, num, 1, 1, res, res, res, punts, den - num, resolucioPerPunts, 0, mmin, mmax
            this = (up, '0', 'A', 'GIS005', 0, den, num,
                    1, 1, res, res, res, 0, den-num, 0, 0, 0, 0)
            dades.append(this)

        for (up, usu, sector), den in self.ups_usu_den.items():
            num = self.ups_usu_num[(up, usu, sector)]
            res = num / den
            dni = self.dni[(sector, usu)] #això s'ha d'arreglar!!!!
            this = (up, dni, "A", 'GIS005', 0, den, num, 
                    1, 1, res, res, res, 0, den - num, 0, 0, 0, 0)
            dades.append(this)

        # u.listToTable(dades, ECAP_IND, DATABASE)
        cols = "(up varchar(5), uba varchar(10), tipus varchar(1), \
                 indicador varchar(8), esperats double, detectats int, \
                 resolts int, deteccio double, deteccioPerResultat double, \
                 resolucio double, resultat double, resultatPerPunts double, \
                 punts double, llistat int, resolucioPerPunts double, \
                 invers int, mmin_p double, mmax_p double)"
        u.createTable('exp_ecap_uba_gis_prova_05', cols, DATABASE, rm=True)
        u.listToTable(dades, 'exp_ecap_uba_gis_prova_05', DATABASE)

        print('Time execution {}'.format(datetime.now() - ts))
        

    def export_khalix(self):
        u.createTable('exp_khalix_up_gis_prova_05', \
                '(indicador varchar(12), br varchar(5), \
                comb varchar(10), analisis varchar(10), n int)',
                DATABASE, rm=True)
        u.createTable('exp_khalix_uba_gis_prova_05', \
                     '(indicador varchar(12), entity varchar(25), \
                       comb varchar(10), analisis varchar(10), n int)', 
                       DATABASE, rm=True)

        """Creació de Taules per KHALIX"""
        print("------------------------------------------------ export_khalix")
        ts = datetime.now()
        self.upload = []
        for up, den in self.ups_den.items():
            try:
                br = self.centres[up]['br']
                self.upload.append(('GIS005', br, 'NOIMP', 'DEN', den))
            except:
                pass

        for up, num in self.ups_num.items():
            try:
                br = self.centres[up]['br']
                self.upload.append(('GIS005', br, 'NOIMP', 'NUM', num))
            except:
                pass

        u.listToTable([row for row in self.upload], 'exp_khalix_up_gis_prova_05', DATABASE)

        self.upload = []
        for (up, usu, sector), den in self.ups_usu_den.items():
            try:
                br = self.centres[up]['br']
                dni = self.dni[(sector, usu)]
                ent = br + dni
                self.upload.append(('GIS005', ent, 'NOIMP', 'DEN', den))
            except:
                pass
        
        for (up, usu, sector), num in self.ups_usu_num.items():
            try:
                br = self.centres[up]['br']
                dni = self.dni[(sector, usu)]
                ent = br + dni
                self.upload.append(('GIS005', ent, 'NOIMP', 'NUM', num))
            except:
                pass

        u.listToTable([row for row in self.upload], 'exp_khalix_uba_gis_prova_05', DATABASE)
        print('Time execution {}'.format(datetime.now() - ts))


def _worker(params):
    return list(u.getAll(*params))


def export_khalix_tots():
    """Exportar tots indicadors GIS a Khalix"""
    sql = """select 
                indicador, 'Aperiodo', br, analisis, 'NOCAT', 
                'NOIMP', 'DIM6SET', 'N', n 
            from 
                {}.{}""".format(DATABASE, 'exp_khalix_up_gis')

    file = 'GIS_UP'
    u.exportKhalix(sql, file)
    print('Time execution {}'.format(datetime.now() - ts))

    sql = """select 
                indicador, 'Aperiodo', entity, analisis, 'NOCAT', 
                'NOIMP', 'DIM6SET', 'N', n 
            from 
                {}.{}""".format(DATABASE, 'exp_khalix_uba_gis')

    file = 'GIS_UBA'
    u.exportKhalix(sql, file)
    print('Time execution {}'.format(datetime.now() - ts))


if __name__ == "__main__":
    CATALEGS_TAULES()
    # centres = get_centres()
    # usuaris, dni = get_usuaris()
    ts = datetime.now()
    #GIS001(centres, usuaris, dni)
    GIS001()
    print('Time execution GIS001 {}'.format(datetime.now() - ts))
    # motius_den, motius_num = get_motius()
    # agendes = get_agendes()
    # ts = datetime.now()
    # GIS004(centres, usuaris, dni, motius_den, motius_num, agendes)
    # print('Time execution GIS004 {}'.format(datetime.now() - ts))
    # ts = datetime.now()
    # GIS005(centres, usuaris, dni, motius_den, motius_num, agendes)
    # print('Time execution GIS005 {}'.format(datetime.now() - ts))
    export_khalix_tots()
