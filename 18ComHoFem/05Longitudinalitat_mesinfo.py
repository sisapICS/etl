# coding: utf8

"""
extra del comhofem d elongitudinalitat, necessiten diagnostics
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u


ORIG_TB = "comhofem_long_poblacio"
ORIG_DB = "test"

tb_ps = "comhofem_long_problemes"

db = "test"

agrupadors = "(62, 18, 21)"



class long_pacient(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_hash()
        self.get_pacients()
        self.get_problemes()
        self.export_dades()
    
    def get_hash(self):
        """obtenim els hashos per desencriptar"""
        u.printTime("hashos")
        self.id_to_hash = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        for id, sector, hash in u.getAll(sql, "import"):
            self.id_to_hash[id] = {'sec': sector, 'hash': hash}
    
    def get_pacients(self):
        """Treu els pacients de la taula que ja havíem quedat, ens quedem amb aquells hashos"""
        u.printTime("Pacients del comhofem de longitudinalitat")
        self.pacients = {}
        sql = "select id, sector, hash from {}".format(ORIG_TB)
        for id, sec, hash in u.getAll(sql, ORIG_DB):
            self.pacients[(sec, hash)] = id
    
    def get_problemes(self):
        """agafem problemes de salu definits"""
        u.printTime("Problemes de salut")
        self.dades = {}
        sql = "select id_cip_sec, ps from eqa_problemes where ps in {}".format(agrupadors)
        for id, agr in u.getAll(sql, 'nodrizas'):
            if id in self.id_to_hash:
                sector = self.id_to_hash[id]['sec']
                hash = self.id_to_hash[id]['hash']
                if (sector, hash) in self.pacients:
                    id_antic = self.pacients[(sector, hash)]
                    self.dades[(id_antic, agr)] = 1
                
    def export_dades(self):
        """."""
        u.printTime("export")
        upload = [(id, agr)
                 for (id, agr), d in self.dades.items()]

        columns = ["id int",  "agrupador int",]
        u.createTable(tb_ps, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb_ps, db)
      
        
   
if __name__ == '__main__':
    long_pacient()