## Carregar BD ## 
# MySQL de la BD test les taules 
# - comhofem_its_poblacio
# - comhofem_its_serologies
# > str(poblacio)
# Classes ‘data.table’ and 'data.frame':	2691775 obs. of  17 variables:
#   $ id            : int  12582912 4194306 7 4194312 9346297 4194314 11 2094970 11184813 12582928 ...
# $ sector        : chr  "6838" "6519" "6102" "6519" ...
# $ hash          : chr  "674BD1B1945A59CFE63C15A704D5553294C5C49E" "B602A85F399FC2455612E3A9D653E467EDD5C675" "DFEC948B46BB9F783D095B980FBCC6EE78841226" "BD623ADFECEB3AC80E5364025854A684156682F6" ...
# $ edat          : int  26 17 32 27 55 23 34 50 56 25 ...
# $ sexe          : chr  "D" "D" "H" "D" ...
# $ up            : chr  "00469" "00184" "00018" "00184" ...
# $ medea         : chr  "3U" "4U" "1R" "4U" ...
# $ comorbilitat  : int  0 0 0 0 0 0 0 0 0 0 ...
# $ dx_vih        : chr  "" "" "" "" ...
# $ dx_vhb        : chr  "" "" "" "" ...
# $ dx_vhc        : chr  "" "" "" "" ...
# $ conducta_risc : int  0 0 0 0 0 0 0 0 0 0 ...
# $ contacte_its  : int  0 0 0 0 0 0 0 0 0 0 ...
# $ contacte_vih  : int  0 0 0 0 0 0 0 0 0 0 ...
# $ cribratge_fet : int  0 0 0 0 0 0 1 0 0 0 ...
# $ cribratge_risc: int  9 9 9 9 9 9 0 9 9 9 ...
# $ cribratge_qui : chr  "" "" "" "" ...
# - attr(*, ".internal.selfref")=<externalptr> 
# > str(serologies)
# Classes ‘data.table’ and 'data.frame':	1210235 obs. of  5 variables:
#   $ id   : int  14739641 13979543 12534602 13669862 11964378 11875845 11882574 11906790 14054576 12853252 ...
# $ dat  : chr  "20181016" "20181016" "20181016" "20181016" ...
# $ ps   : chr  "VIH" "VIH" "VIH" "VIH" ...
# $ prova: chr  "SEROLOGIA" "SEROLOGIA" "SEROLOGIA" "SEROLOGIA" ...
# $ val  : int  0 0 0 0 0 0 0 0 0 0 ...
# - attr(*, ".internal.selfref")=<externalptr> 
#   > 

# --- libraries ----
library(RMySQL)
library(data.table)
library(lubridate)
library(splitstackshape)

# ----- MYSQL ------
drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password="",host = "10.80.217.68", port=3307, dbname="test")
nfetch <- -1
dbListTables(con)
dbListFields(con, "comhofem_its_poblacio")
query <- dbSendQuery(con,
                     statement = "select *
                     from comhofem_its_poblacio")
poblacio <- data.table(fetch(query, n = nfetch))
save(poblacio, file = "Dades/poblacio.RData")

dbListFields(con, "comhofem_its_serologies")
query <- dbSendQuery(con,
                     statement = "select *
                     from comhofem_its_serologies")
serologies <- data.table(fetch(query, n = nfetch))
save(serologies, file = "Dades/serologies.RData")
dbDisconnect(con)

# ------- Formatejar BD ----------
# ----- POBLACIÓ ------
poblacio <- poblacio[sexe != "M"]
poblacio[, ":=" (edat_f = cut(edat, breaks = c(15, 19, 24, 29, 34, 39, 44, 49, 54, 59, 64, 69, 74, 79, 84, 89, 94, 200), include.lowest = T, right = T, 
                              labels = c("De 15 a 19 anys", "De 20 a 24 anys", "De 25 a 29 anys", "De 30 a 34 anys", "De 35 a 39 anys", "De 40 a 44 anys",
                                         "De 45 a 49 anys", "De 50 a 54 anys", "De 55 a 59 anys", "De 60 a 64 anys", "De 65 a 69 anys", "De 70 a 74 anys",
                                         "De 75 a 79 anys", "De 80 a 84 anys", "De 85 a 89 anys", "De 90 a 94 anys", "Més de 95 anys")),
                 sexe_f = factor(sexe, levels = c("D", "H"), labels = c("Home", "Dona")),
                 medea_f = factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U")),
                 rural = factor(medea, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), 
                                labels = c("Rural", "Rural", "Rural", "Urbà", "Urbà", "Urbà", "Urbà")))]
poblacio[, paste0(c("comorbilitat", "conducta_risc", "contacte_its", "contacte_vih", "cribratge_fet", "cribratge_risc"), "_f") := 
           lapply(.SD, function(x) factor(x, levels = c(0, 1), labels = c("No", "Sí"))), .SDcols = c("comorbilitat", "conducta_risc", "contacte_its", "contacte_vih", "cribratge_fet", "cribratge_risc")]
poblacio[, paste0(c("dx_vih", "dx_vhb", "dx_vhc"), "_f") := 
           lapply(.SD, function(x) as.factor(ifelse(x == "", "No", "Sí"))), .SDcols = c("dx_vih", "dx_vhb", "dx_vhc")]
poblacio[, c("sexe", "medea", "comorbilitat", "conducta_risc", "contacte_its", "contacte_vih", "cribratge_fet", "cribratge_risc") := NULL]
summary(poblacio)

# ----- CRIBRATGE ------
cribratge <- cSplit(poblacio[, c("id", "cribratge_qui")], splitCols = "cribratge_qui", sep = ",", direction = "long", drop = FALSE)

# ----- SEROLOGIES ------
serologies[, PsProva := paste(ps, prova, sep = "-")]
opt <- unique(serologies$PsProva)
# l <- lapply(opt, function(x) {
#   print(x)
#   serologies[PsProva == x, .N, "VAL"]
#   }
# )
# l
serologies <- serologies[, VAL := ifelse(PsProva %in% opt[c(1:6, 11, 12, 14:17)] & val == 9, NA, val)]
serologies[, ":=" (ps_f = factor(ps, levels = serologies[, .N, ps]$ps),
                   prova_noBuit = ifelse(prova == "", NA, prova))]
serologies[, prova_f := factor(prova_noBuit, levels = serologies[, .N, prova_noBuit]$prova_noBuit)]

save(poblacio, file  = "Dades/poblacio_depurada.RData")
save(serologies, file  = "Dades/serologies_depurada.RData")
save(cribratge, file  = "Dades/cribratge.RData")
