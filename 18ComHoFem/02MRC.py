# -*- coding: utf-8 -*-

from typing import Any
from sisapUtils import *
from collections import defaultdict, Counter
import datetime

# Variables globals
db = 'test'
patologia = '18'

nod = 'nodrizas'
imp = 'import'

def get_hashos():
    hashos = {}
    sql = 'select id_cip_sec, codi_sector, hash_d from u11'
    for id, sector, hash in getAll(sql, imp):
        hashos[id] = {'sector': sector, 'hash': hash}
    return hashos

def get_centres():
    centres = {}
    sql = "select scs_codi,ics_codi, ics_desc, medea from cat_centres where ep='0208'"
    for up,br, ics_desc, medea in getAll(sql,nod):
        centres[(up)] = medea
    return centres
    
def get_poblacio(centres, actual, any=0):
    poblacio = {}
    if actual:
        sql = 'select id_cip_sec, up, edat from assignada_tot where edat > 17 and ates=1'
        for id, up, edat in getAll(sql, nod):
            if up in centres:
                medea = centres[(up)]
                poblacio[id] = {'edat': edat, 'medea':medea}
    else:
        print('cips')
        hash_id = {}
        sql = 'select id_cip_sec, hash_d from import.u11'
        for id, hash in getAll(sql, 'import'):
            hash_id[hash] = id
        print('edats')
        edats = {}
        sql = 'select c_cip, C_EDAT_ANYS from dbs.dbs_2019 d'
        for hash, edat in getAll(sql, ("sidics", "x0002")):
            if edat > 17:
                try:
                    edats[hash_id[hash]] = edat
                except:
                    pass
        print('all')
        sql = 'select id_cip_sec, up from import.assignadahistorica_s{} where ates=1'.format(any) # edat > 17 NO PODEM FILTRAR
        for id, up in getAll(sql, 'import'):
            try:
                medea = centres[(up)]
                poblacio[id] = {'edat': edats[id], 'medea':medea}
            except:
                pass
        print('LONGITUD POBLACIO:', len(poblacio))
    return poblacio

class Antecedents():
    def __init__(self, poblacio, hashos, dext, any, poliquistosi):
        self.poliquistosi = poliquistosi
        self.dext = dext
        self.any = any
        self.hashos = hashos
        self.poblacio = poblacio
        self.TableMy = 'ComHoFem_MRC_antecedents_{}'.format(self.any)
        self.file_cribratge = 'ComHoFem_MRC_antecedents_{}'.format(self.any)
        self.create_table()
        self.get_cribatge()
        self.get_antecedents()
        self.uploading()
    
    def create_table(self):
        create = '(codi_sector varchar(4), hash_d varchar(40), id_cip_sec int, edat int, medea varchar(10), fg int, qac int, microalb int, m_val double, dm2 int, hta int)'
        createTable(self.TableMy, create, db, rm=True)
    
    def get_cribatge(self):
        print('cribatge')
        self.cribFG, self.cribQAC = {}, {}
        sql = 'select id_cip_sec, data_var, agrupador from eqa_variables where agrupador in (30, 513)'
        for id, data, agrupador in getAll(sql, nod):
            mesos = monthsBetween(data, self.dext)
            if 0 <= mesos <=11:
                if agrupador == 30:
                    self.cribFG[id] = True
                else:
                    self.cribQAC[id] = True
        self.albu = {}
        sql = """select id_cip_sec, data_var, valor
                from nodrizas.eqa_variables
                where agrupador=513 and year(data_var) = 2019"""
        for id, data, valor in getAll(sql, 'nodrizas'):
            if id in self.poblacio:
                if id in self.albu:
                    if data > self.albu[id]['data']: self.albu[id]['val'] = valor
                else:
                    self.albu[id] = {'data': data, 'val':valor}
    
    def get_antecedents(self):
        print('antecedents')
        self.antecedents = {}
        self.diabetis = set()
        self.hta = set()
        sql = 'select id_cip_sec, ps from eqa_problemes where ps in (55, 18, 1, 211, 7, 212, 50, 51, 11, 213, 21)'
        for id, ps in getAll(sql, nod):
            if id in self.poblacio:
                edat = self.poblacio[id]['edat']
                medea = self.poblacio[id]['edat']
                self.antecedents[id] = {'edat': edat, 'medea': medea}
                if int(ps) == 55:
                    self.hta.add(id)
                elif int(ps) == 18:
                    self.diabetis.add(id) 
        sql = 'select id_cip_sec from familiars where af_cod_ps in {}'.format(self.poliquistosi)
        for id, in getAll(sql, imp):
            if id in self.poblacio:
                edat = self.poblacio[id]['edat']
                medea = self.poblacio[id]['edat']
                self.antecedents[id] = {'edat': edat, 'medea': medea}
    
    def uploading(self):
        print('anem a carregar les dades:)))')
        upload = []
        for (id), valors in self.antecedents.items():
            hash = self.hashos[id]['hash']
            sector = self.hashos[id]['sector']
            FG, QAC, ALBU, VAL, DM2, HTA = 0, 0, 0, 0, 0, 0
            if id in self.cribFG:
                FG = 1
            if id in self.cribQAC:
                QAC = 1
            if id in self.albu:
                ALBU = 1
                VAL = self.albu[id]['val']
            if id in self.diabetis:
                DM2 = 1
            if id in self.hta:
                HTA = 1
            upload.append([sector, hash, id, valors['edat'], valors['medea'],FG, QAC, ALBU, VAL, DM2, HTA])
        listToTable(upload, self.TableMy, db)
        writeCSV(self.file_cribratge, upload, sep=';') 

# 2 Pacients amb MRC incident
class Incidents():
    def __init__(self, dext, any):
        self.dext = dext
        self.any = any
        self.TableMy = 'ComHoFem_MRC_incidents_{}'.format(self.any)
        self.file = 'ComHoFem_MRC_incidents_{}'.format(self.any)
        self.create_table()
        self.get_FGs()
        self.get_CAC()
        self.incidents()
        self.uploading()
    
    def create_table(self):
        create = '(codi_sector varchar(4), hash_d varchar(40), id_cip_sec int, edat int, medea varchar(10), fg int, qac int, microalb int, fg1 int)'
        createTable(self.TableMy, create, db, rm=True)

    def get_FGs(self):   
        print('FGs') 
        self.FG_x2 = {}
        self.FG_x1={}
        sql = """select id_cip_sec, data_var, valor, usar 
                from eqa_variables where agrupador=30"""
        for id, data_var, valor, usar in getAll(sql, nod):
            mesos = monthsBetween(data_var, self.dext)
            if 0 <= mesos <= 23:
                if 0 < valor < 59.99:
                    if (id) in self.FG_x2:
                        data = self.FG_x2[(id)]['data']
                        dies = daysBetween(data_var, data)
                        if abs(dies) > 90:
                            usar1 = self.FG_x2[(id)]['usar']
                            difusar = usar - usar1
                            if abs(difusar) == 1:
                                self.FG_x2[(id)]['num'] += 1
                                self.FG_x2[(id)]['data'] = data_var
                                self.FG_x2[(id)]['usar'] = usar
                            else:
                                num = self.FG_x2[(id)]['num']
                                if num > 1:
                                    continue
                                else:
                                    self.FG_x2[(id)]['data'] = data_var
                                    self.FG_x2[(id)]['usar'] = usar
                    else:
                        self.FG_x2[(id)] = {'data': data_var, 'num': 1, 'usar': usar}
                    if (id) in self.FG_x1:
                        self.FG_x1[(id)]['num'] += 1
                    else:
                        self.FG_x1[(id)] = {'num': 1}

    def get_CAC(self):   
        print('CAC') 
        self.CAC_x2 = {}
        sql = "select id_cip_sec, data_var, valor, usar from eqa_variables where agrupador=513"
        for id, data_var, valor, usar in getAll(sql, nod):
            mesos = monthsBetween(data_var, self.dext)
            if 0 <= mesos <= 23:
                if  valor >30:
                    if (id) in self.CAC_x2:
                        self.CAC_x2[(id)]['num'] += 1
                    else:
                        self.CAC_x2[(id)] = {'num': 1}
        self.microalb_= {}
        sql = 'select id_cip_sec from eqa_microalb'
        for id, in getAll(sql, nod):
            self.microalb_[(id)] = True
    
    def incidents(self):
        print('incidents')
        self.incidents = {}
        sql = 'select id_cip_sec, dde from eqa_problemes_incid where ps in (52,53,54)'
        for id, dde in getAll(sql, nod):
            mesos = monthsBetween(dde, self.dext)
            if 0 <= mesos <=11:
                if id in poblacio:
                    self.incidents[id] = {'edat':poblacio[id]['edat'],'medea':poblacio[id]['medea'],'data':dde}
    
    def uploading(self):
        print('carreguem dades:)))')
        upload = []
        for (id), valors in self.incidents.items():
            hash = hashos[id]['hash']
            sector = hashos[id]['sector']
            numfg = 0
            numfg1 = 0
            numcac = 0
            numalb = 0
            if id in self.FG_x2:
                nfg = self.FG_x2[id]['num']
                if nfg >1:
                    numfg = 1
            if id in self.CAC_x2:
                ncac = self.CAC_x2[id]['num']
                if ncac >1:
                    numcac = 1
            if id in self.microalb_:
                numalb = 1
            if id in self.FG_x1:
                nfg1 = self.FG_x1[id]['num']
                if nfg1 == 1:
                    numfg1 = 1
            upload.append([sector, hash, id, valors['edat'], valors['medea'], numfg, numcac, numalb, numfg1])
            
        listToTable(upload, self.TableMy, db)
        writeCSV(self.file, upload, sep=';')   


class Actius():
    def __init__(self, dext, any):
        self.dext = dext
        self.any = any
        self.TableMy = 'ComHoFem_MRC_actius_{}'.format(self.any)
        self.file = 'ComHoFem_MRC_actius_{}'.format(self.any)
        self.create_table()
        self.estadiatge()
        self.filtrat()
        self.qac()
        self.complicprob()
        self.tractaments()
        self.tabac()
        self.variables()
        self.malalties()
        self.pre_upload()
        self.hipo()
        self.upload()
    
    def create_table(self):
        create = '(codi_sector varchar(4), hash_d varchar(40), id_cip_sec int, edat int, medea varchar(10),  estadiatge double, hta double, dm2 double, dislip double, psobesitat double, hiperuricemia double, \
                ci double, avc double, icc double,claud double, anemia double, hiperpara double, fg double, qac double, ieca double, araII double, aines double, metformina double, aas double, ado double,\
                    tabac double, glicada double, tas double, tad double, obesitat double, hemoglobina double, ldl double, tetanus int, grip int, vhb int, pneumococ int, dialisi double, estamines int)'
        createTable(self.TableMy,create,db, rm=True)

    def estadiatge(self):
        print('estadiatge')
        self.estadiatge = {}
        sql = "select id_cip_sec, vu_val, vu_dat_act from variables where vu_cod_vs = 'VU2000' and year(vu_dat_act) <= {}".format(any)
        for id, val, data in getAll(sql, imp):
            if id in self.estadiatge:
                dat1 = self.estadiatge[id]['data']
                if dat1 < data:
                    self.estadiatge[id]['data'] = data
                    self.estadiatge[id]['nivell'] = val
            else:
                self.estadiatge[id] = {'nivell': val, 'data': data}

    def filtrat(self):     
        print('filtrat')   
        self.filtrat = {}
        sql = 'select id_cip_sec, valor from eqa_variables where agrupador=30 and usar=1'
        for id, valor in getAll(sql, nod):
            self.filtrat[id] = valor

    def qac(self):
        print('qac')
        self.qac = {}
        sql = 'select id_cip_sec, data_var from eqa_variables where agrupador=513 and valor>30 and usar <3'
        for id, data in getAll(sql, nod):
            if id in self.qac:
                dat1 = self.qac[id]['data']
                mes = monthsBetween(dat1,data)
                if -3 <= mes <= 3:
                    self.qac[id]['num'] += 1
            else:
                self.qac[id] = {'data':data, 'num': 1}

    def complicprob(self): 
        print('complicacions de problemes')       
        self.complicprob = defaultdict(list)
        sql = 'select id_cip_sec, ps from eqa_problemes where ps in (55,18,47,239,73,1,211,7,212,50,51,11,213, 376, 208, 645, 21, 646)'
        for id, ps in getAll(sql, nod):
            self.complicprob[id].append(ps)

    def tractaments(self):    
        print('tractaments')
        self.tractaments = defaultdict(list)
        sql = 'select id_cip_sec, farmac, TIMESTAMPDIFF(month, pres_orig, data_fi) from eqa_tractaments where farmac in (56,72,118,173,5,22)'
        for id, farmac, durada in getAll(sql, nod):
            self.tractaments[id].append(farmac)
            if farmac == 118:
                if durada > 1:
                    self.tractaments[id].append('118_d')

    def tabac(self):   
        print('tabac') 
        self.tabac = {}
        sql = 'select id_cip_sec from eqa_tabac where tab=1 and last=1'
        for id, in getAll(sql, nod):
            self.tabac[id] = True

    def variables(self):   
        print('variables') 
        self.glicada, self.tas, self.tad, self.ldl, self.obesitat, self.hemoglobina = {}, {}, {}, {}, {}, {}
        sql = 'select id_cip_sec, agrupador, data_var, valor from nodrizas.eqa_variables where usar=1 and agrupador in (19, 20, 16, 17, 9, 260)'
        for id, agr, data, valor in getAll(sql, nod):
            if agr == 19:
                self.obesitat[id] = valor
            else:
                mesos = monthsBetween(data, self.dext)
                if 0<=mesos<= 11:
                    if agr == 20:
                        self.glicada[id] = valor
                    elif agr == 16:
                        self.tas[id] = valor
                    elif agr == 17:
                        self.tad[id] = valor
                    elif agr == 9:
                        self.ldl[id] = valor
                    elif agr == 260:
                        self.hemoglobina[id] = valor

    def malalties(self):    
        print('malalties')            
        self.tetanus, self.grip, self.vhb, self.pneumococ = {},{},{},{}
        sql = 'select id_cip_sec, agrupador, datamax, dosis from eqa_vacunes where agrupador in (49,99,15,48)'
        for id, agr, data, dosis in getAll(sql, nod):
            if agr==49:
                m = monthsBetween(data, self.dext)
                if 0<= m <= 300:
                    if id in self.tetanus:
                        if dosis > self.tetanus[id]:
                            self.tetanus[id] = dosis
                    else:
                        self.tetanus[id] = dosis
            if agr == 99:
                m = monthsBetween(data, self.dext)
                if 0<= m <= 18:
                    self.grip[id] = True
            if agr == 15:
                if id in self.vhb:
                    if dosis > self.vhb[id]:
                        self.vhb[id] = dosis
                else:
                    self.vhb[id] = dosis
            if agr == 48:
                if id in self.pneumococ:
                    if dosis > self.pneumococ[id]:
                        self.pneumococ[id] = dosis
                else:
                    self.pneumococ[id] = dosis
    def hipo(self):
        hash_cip = {}
        sql = 'select id_cip_sec, hash_d from u11'
        for id, hash in getAll(sql, 'import'):
            hash_cip[hash] = id
        print('estatines')
        self.estamines = set()
        sql = """SELECT c_cip FROM dbs
                WHERE F_HIPOLIPEMIANTS IS NOT null"""
        for hash in getAll(sql, 'redics'):
            try:
                self.estamines.append(hash_cip(hash))
            except:
                pass
        
               
    def pre_upload(self):
        print('pre')
        self.pre_upload = {}   
        sql = 'select id_cip_sec from eqa_problemes where ps in (52,53,54)'
        for id, in getAll(sql, nod):
            self.pre_upload[id] = True
            
        sql = 'select id_cip_sec from eqa_microALB'
        for id, in getAll(sql, nod):
            self.pre_upload[id] = True 


    def upload(self):
        print('anem a penjar les dades :)')
        upload = []    
        for id, cosetes in self.pre_upload.items():
            if id in poblacio:
                edat = poblacio[id]['edat']
                medea = poblacio[id]['medea']
                hash = hashos[id]['hash']
                sector = hashos[id]['sector']
                mrcestadiatge = 99999
                if id in self.estadiatge:
                    mrcestadiatge = self.estadiatge[id]['nivell']
                hta, dm2, dislip, psobesitat, hiperuri, ci, avc, claud, anemia,hiperpara, icc, dialisi = 0,0,0,0,0,0,0,0,0,0,0,0
                if id in self.complicprob:
                    complis = self.complicprob[id]
                    for problem in complis:
                        if problem == 55:
                            hta = 1
                        elif problem == 18:
                            dm2 = 1
                        elif problem == 47:
                            dislip = 1
                        elif problem == 239:
                            psobesitat = 1
                        elif problem == 73:
                            hiperuri = 1
                        elif problem == 1 or problem == 211:
                            ci = 1
                        elif problem == 7 or problem == 212:
                            avc = 1
                        elif problem == 50 or problem == 51:
                            avc = 1
                        elif problem == 11 or problem == 213:
                            claud = 1
                        elif problem == 376:
                            hiperpara = 1
                        elif problem == 208 or problem == 645:
                            anemia = 1
                        elif problem == 21:
                            icc = 1
                        elif problem == 646:
                            dialisi = 1
                mrcfg, mrcqac = None, 0
                if id in self.filtrat:
                    mrcfg = self.filtrat[id]
                if id in self.qac:
                    mrcqac = self.qac[id]['num']
                ieca, araII, aines, metformina,aas, ado = 0,0,0,0,0,0
                if id in self.tractaments:
                    farmacs = self.tractaments[id]
                    for far in farmacs:
                        if far == 56:
                            ieca = 1
                        elif far == 72:
                            araII = 1
                        elif far == 118:
                            aines = 1
                        elif far == 173:
                            metformina = 1
                        elif far == 5:
                            aas = 1
                        elif far == 22:
                            ado = 1
                mrctabac, mrcglicada, mrctas, mrctad, mrcobesitat, mrchemoglo, mrcldl = 0, 0, 0, 0, 0, 0,0
                if id in self.tabac:
                    mrctabac = 1
                if id in self.glicada:
                    mrcglicada = self.glicada[id]
                if id in self.tas:
                    mrctas = self.tas[id]
                if id in self.tad:
                    mrctad = self.tad[id]
                if id in self.obesitat:
                    mrcobesitat = self.obesitat[id]
                if id in self.hemoglobina:
                    mrchemoglo = self.hemoglobina[id]
                if id in self.ldl:
                    mrcldl = self.ldl[id]
                mrctetanus, mrcgrip,mrcvhb,mrcpneumococ = 0,0,0,0
                if id in self.tetanus:
                    mrctetanus = self.tetanus[id]
                if id in self.grip:
                    mrcgrip = 1
                if id in self.vhb:
                    mrcvhb = self.vhb[id]
                if id in self.pneumococ:
                    mrcpneumococ= self.pneumococ[id]
                estamines = 0
                if id in self.estamines:
                    estamines = 1
                upload.append([sector, hash, id, edat, medea, mrcestadiatge, hta, dm2, dislip, psobesitat, hiperuri, ci, avc, icc,claud, anemia,hiperpara, mrcfg, mrcqac, ieca, araII, aines, metformina,aas, ado,mrctabac, mrcglicada, mrctas, mrctad, mrcobesitat, mrchemoglo,mrcldl,mrctetanus,mrcgrip,mrcvhb,mrcpneumococ, dialisi, estamines])      
        listToTable(upload, self.TableMy, db)
        writeCSV(self.file, upload, sep=';')   

def mrc_i_altres(poblacio):
    MRC = {}
    sql = "select id_cip_sec, dde from nodrizas.eqa_problemes where ps in (52,53,54)"
    for id, data in getAll(sql, 'nodrizas'):
        if id in poblacio:
            if id in MRC:
                if MRC[id] < data: MRC[id] = data
        else:
            MRC[id] = data
    
    CI_primer = {}
    CI_ultim = {}
    IC = {}
    ICTUS_primer = {}
    ICTUS_ultim = {}
    sql = """select id_cip_sec, ps, dde
                from nodrizas.eqa_problemes ep 
                where ps in (1, 211, 726, 727,
                        7, 50, 51, 212, 728, 729,
                        21, 275, 593)"""
    for id, ps, data in getAll(sql, 'nodrizas'):
        if id in MRC:
            if ps in (1, 211, 726, 727):
                if id in CI_primer:
                    if CI_primer[id] > data: CI_primer[id] = data 
                else:
                    CI_primer[id] = data 
                if id in CI_ultim:
                    if CI_ultim[id] < data: CI_ultim[id] = data 
                else:
                    CI_ultim[id] = data 
            if ps in (7, 50, 51, 212, 728, 729):
                if id in ICTUS_primer:
                    if ICTUS_primer[id] > data: ICTUS_primer[id] = data 
                else:
                    ICTUS_primer[id] = data 
                if id in ICTUS_ultim:
                    if ICTUS_ultim[id] < data: ICTUS_ultim[id] = data 
                else:
                    ICTUS_ultim[id] = data 
            if ps in (21, 275, 593):
                if id in IC:
                    if IC[id] > data: IC[id] = data 
                else:
                    IC[id] = data 
    upload = []
    print('long MRC', len(MRC))
    print('long ci primer', len(CI_primer))
    print('long ci ultim', len(CI_ultim))
    print('long ICTUS PRIMER', len(ICTUS_primer))
    print('long ICTUS ultim', len(ICTUS_ultim))
    print('long IC', len(IC))
    for id in MRC:
        if id in CI_primer:
            CI_P = CI_primer[id]
        else: CI_P = None
        if id in CI_ultim:
            CI_U = CI_ultim[id]
        else: CI_U = None
        if id in ICTUS_ultim:
            ICTUS_U = ICTUS_ultim[id]
        else: ICTUS_U = None
        if id in ICTUS_primer:
            ICTUS_P = ICTUS_primer[id]
        else: ICTUS_P = None
        if id in IC:
            ic_p = IC[id]
        else: ic_p = None
        upload.append((id, MRC[id], CI_P, CI_U, ICTUS_P, ICTUS_U, ic_p))

    cols = '(id int, MRC date, CI_primer date, CI_ultim date, ICTUS_PRIMER date, ICTUS_ULTIM date, IC date)'    
    createTable('comhofem_MRC_2021_extres', cols, 'test', rm=True)   
    listToTable(upload, 'comhofem_MRC_2021_extres', 'test')     



def farmacs_sectors(info):
    print('multiprocess jejejeje')
    sector, hash_cip = info[0], info[1]
    no_ado = set()
    estatina = set()
    sql = """SELECT ppfmc_pmc_usuari_cip, PF_COD_ATC FROM  ppftb016 
                WHERE extract(YEAR FROM PPFMC_PMC_DATA_INI) = 2021"""
    for id, farmac in getAll(sql, sector):
        if farmac in ('A10BH05', 'A10BX02', 'A10BX03'):
            try:
                no_ado.add(hash_cip[id])
            except:
                pass
        elif farmac in ('C10AA03', 'C10AA05', 'C10AA05', 'C10AA07', 'C10AA07', 'C10AA08', 'C10AA04', 
                        'C10AA04', 'C10AA02', 'C10AA05', 'C10AA01', 'C10AA08', 'C10AA06', 'C10AA05'):
            try:
                estatina.add(hash_cip[id])
            except:
                pass
    return no_ado, estatina


if __name__ == "__main__":
    # variable 1 si és última data d'extracció, altrament 0; si és 0 modificar data a else
    actual = 1
    poliquistosi = "('Q61', 'Q61.1', 'Q61.2', 'Q61.3', 'Q61.5', 'Q61.8', 'Q61.9')"

    if actual:
        sql = """select date '2024-12-31' from dextraccio"""
        dext,=getOne(sql, nod)
        any = str(dext.year)
    else:
        dext = datetime.date(2019,12,31) # imputar a mà la data d'extracció desitjada
        any = str(dext.year)
        
    print('Es farà servir la següent data dext: ', dext, 'amb any: ', any)

    print('HASHOS')
    hashos = get_hashos()
    print('CENTRES')
    centres = get_centres()
    print('POBLACIÓ')
    if actual:
        poblacio = get_poblacio(centres, actual)  
    else:
        poblacio = get_poblacio(centres, actual, dext.year)  
    print('ANTECEDENTS')
    Antecedents(poblacio, hashos, dext, any, poliquistosi)
    print('INCIDENTS')
    Incidents(dext, any)
    print('ACTIUS')
    Actius(dext, any)
    mrc_i_altres(poblacio)

