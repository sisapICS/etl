# coding: utf8

"""
COMHOFEM de longitudinalitat
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u


ORIG_TB = "mst_long_cont_pacient"
ORIG_DB = "altres"

tb_pac = "comhofem_long_poblacio"
tb_ind = "comhofem_long_indicadors"

db = "test"

ind_eqa = "('EQA0236A', 'EQA0206A', 'EQA0206C', 'EQA0201A', 'EQA0213A', 'EQA0213B', 'EQA0209A', 'EQA0501A', 'EQA0502A', 'EQA0304A', 'EQA0305A')"

dext, = u.getOne("select data_ext from dextraccio","nodrizas")

class long_pacient(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.dades_indicadors = {}
        self.get_centres()
        self.get_hash()
        self.get_longitudinalitat()
        self.get_visites()
        self.get_pob()
        self.get_gma()
        self.get_medea()
        self.get_indicadors_IAD()
        self.get_indicadors_eqa()
        self.get_indicador_mpoc()
        self.export_dades()
                
    def get_centres(self):
        """EAP ICS"""
        u.printTime("Centres")
        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi \
                from cat_centres where ep='0208'", "nodrizas")
        self.centres = {up: (br, amb, sap) for (up, br, amb, sap)
                        in u.getAll(*sql)}
                        
    def get_hash(self):
        """obtenim els hashos per a si en un futur cal carregar més dades"""
        u.printTime("hashos")
        self.id_to_hash = {}
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        for id, sector, hash in u.getAll(sql, "import"):
            self.id_to_hash[id] = {'sec': sector, 'hash': hash}
            
    def get_longitudinalitat(self):
        """Obtenim els indicadors de longitudinalitat per pacient"""
        u.printTime("Longitudinalitat")
        self.long = {}
        sql = "select id_cip_sec, nprof, totals, r1, r2, r3, r4 from {} where servei = 'MG'".format(ORIG_TB)
        for id, nprof, vtot, r1, r2, r3, r4 in u.getAll(sql, ORIG_DB):
            self.long[(id, 'CONT0001')] = r1
            self.long[(id, 'CONT0002')] = r2
            self.long[(id, 'CONT0003')] = r3
            self.long[(id, 'CONT0004')] = r4
            self.long[(id, 'nprof')] = nprof
            self.long[(id, 'nvisites')] = vtot
        
    def get_visites(self):
        """Obté dades de visites2 pels pacients a qui no se'ls calcula la longitudinalitat"""
        u.printTime("Visites")
        self.visitespacients = Counter()
        self.data = defaultdict(lambda: defaultdict(list))
        sql = "select id_cip_sec, visi_up \
                    from visites2 \
                    where visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_col_prov_resp <> 0 and visi_servei_codi_servei='MG'"
        for id, up in u.getAll(sql, "import"):
            if up in self.centres:
                self.visitespacients[(id)] += 1
        
    def get_pob(self):
        """Partint de la població assignada crea taula per a anàlisi (falten les variables típiques d'anànlisi)"""
        u.printTime("Pob")
        self.dades = {}
        self.mes35 = {}
        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = "select id_cip_sec, codi_sector, up, sexe, edat, nacionalitat, institucionalitzat from assignada_tot where edat>14"
        for  id1, sec, up, sexe, edat, nac, insti in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                nac1 = 1 if nac in renta else 0
                cont001 = self.long[(id1, 'CONT0001')] if (id1, 'CONT0001') in self.long else None
                cont002 = self.long[(id1, 'CONT0002')] if (id1, 'CONT0002') in self.long else None
                cont003 = self.long[(id1, 'CONT0003')] if (id1, 'CONT0003') in self.long else None
                cont004 = self.long[(id1, 'CONT0004')] if (id1, 'CONT0004') in self.long else None
                nvisites = self.long[(id1, 'nvisites')] if (id1, 'nvisites') in self.long else None
                nprof = self.long[(id1, 'nprof')] if (id1, 'nprof') in self.long else None
                nvis = 0
                if (id1) in self.visitespacients:
                    nvis = self.visitespacients[(id1)]
                if nvisites == None:
                    nvisites=nvis
                sector, hash =  self.id_to_hash[id1]['sec'], self.id_to_hash[id1]['hash']
                self.dades[id1] = {
                              'sector': sec, 'hash': hash, 'up': up, 'sex': sexe,'edat': edat,
                              'renta': nac1, 'gma_cod': None, 'gma_cmplx': None,'gma_num': None, 'medea': None, 'insti': insti,  
                              'C1': cont001, 'C2': cont002, 'C3': cont003, 'C4': cont004, 'nvisites': nvisites, 'nprof': nprof}
                if edat> 35:
                    self.mes35[id1] = True

    def get_gma(self):
        """Obté els tres valors de GMA."""
        u.printTime("GMA")
        sql = ("select id_cip_sec, gma_cod, gma_ind_cmplx, gma_num_cron \
                from gma", "import")
        for id, cod, cmplx, num in u.getAll(*sql):
            if id in self.dades:
                self.dades[id]['gma_cod'] = cod
                self.dades[id]['gma_cmplx'] = cmplx
                self.dades[id]['gma_num'] = num

    def get_medea(self):
        """Índex MEDEA del pacient segons el seu codi de secció censal."""
        u.printTime("Medea")
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
           if id in self.dades and sector in valors:
                self.dades[id]['medea'] = valors[sector]
    
    def get_indicadors_IAD(self):
        """indicadors IAD que necessitem per a longitudinalitat"""
        u.printTime("IADs")
        sql = "select id_cip_sec, up, num from mst_prof_nyha where ates=1"
        for id, up, num in u.getAll(sql, "altres"):
            if up in self.centres:
                self.dades_indicadors[(id, 'IAD0005')] = {'num': num, 'den': 1}
        
        sql = "select id, up, imepoc, indicador from mst_esc_disnea_mpoc"
        for id, up, num, indicador in u.getAll(sql, "altres"):
            if up in self.centres:
                self.dades_indicadors[(id, indicador)] = {'num': num, 'den': 1}
            
    
    def get_indicadors_eqa(self):
        """Obté el compliment o no dels indicadors de l'eqa dels pacients que tenen longitudinalitat calculada"""
        u.printTime("EQA")
        sql = "select \
                    id_cip_sec, grup_codi, up, num \
            from \
                    mst_indicadors_pacient \
        where \
                ind_codi in {} and ates=1 and institucionalitzat=0 and maca=0 and excl=0 and ci=0 and clin=0 and excl_edat=0".format(ind_eqa)
        for id, indicador, up, num in u.getAll(sql, 'eqa_ind'):
            if up in self.centres:
                self.dades_indicadors[(id, indicador)] = {'num': num, 'den': 1}
        
    def get_indicador_mpoc(self):
        """Un indicador que en teoria està a EQA processos mpoc però encara no el tenim creat: percentatge de mpoc amb espiro feta els 2 últims anys"""
        u.printTime("indicador MPOC")
        ind = 'EQA3321'
        espiro = {}
        sql = "select id_cip_sec, data_var from eqa_variables where agrupador in (464,447,448,449,462)"
        for id, dat in u.getAll(sql, 'nodrizas'):
            m = u.monthsBetween(dat, dext)
            if m < 24:
                espiro[id] = True
        sql = "select id_cip_sec from eqa_problemes where ps=62"
        for id, in u.getAll(sql, 'nodrizas'):
            if id in self.mes35:
                num = 0
                if id in espiro:
                    num = 1
                self.dades_indicadors[(id, ind)] = {'num': num, 'den': 1}
                
    
    def export_dades(self):
        """."""
        u.printTime("export")
        upload = [(id, d['sector'], d['hash'], d['up'], d['sex'], d['edat'],d['renta'], d['gma_cod'], d['gma_cmplx'],d['gma_num'], d['medea'], d['insti'], 
                    d['C1'], d['C2'], d['C3'], d['C4'], d['nvisites'], d['nprof'] )
                 for id, d in self.dades.items()]

        columns = ["id int",  "sector varchar(4)", "hash varchar(40)", "up varchar(5)", "sexe varchar(1)",
                    "edat int", "nac1 int",  'gma_codi varchar(3)', 'gma_complexitat double',
                   'gma_num_croniques int', 'medea_seccio double',
                   'institucionalitzat int',  "cont001 double", "cont002 double", "cont003 double", "cont004 double",
                     "nvisites int", "nprof int"]
        u.createTable(tb_pac, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb_pac, db)
        
        upload = [(id, indicador, d['num'], d['den'] )
                 for (id, indicador), d in self.dades_indicadors.items()]
        columns = ["id int", "indicador varchar(20)", "num int", "den int"]
        u.createTable(tb_ind, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(upload, tb_ind, db)
        
        
   
if __name__ == '__main__':
    long_pacient()