from sisapUtils import *

file = 'Set_Period.lvpro'
command = 'Set_Period.bat'

any,any2,mes= getKhalixDates()
with open(tempFolder + 'Set_Period.lvpro','wb') as f:
    f.write('MAINTENANCE ON\r\n')
    f.write('SET ATTRIBUTE SYSTEM ISAGPERPR VALUE DBDEFAULT "A{}{}"\r\n'.format(any2,mes))
    f.write('SET ATTRIBUTE SYSTEM ISAGPERFL VALUE DBDEFAULT "_{}_{}"\r\n'.format(mes,any))
    f.write('MAINTENANCE OFF\r\n')
sshPutFile(file,'procesos','khalix')
status,stdout,stderr = sshExecuteCommand(command,'procesos','khalix')
