# coding: iso-8859-1

"""
Estudio ISMI-HEALTH (coordinado por ISGLOBAL)
anteriormente fue el Estudio Cribmi (cribado de inmigrantes)

En esta versi�n (ISMI-HEALTH) la diferencia principal es que ahora dan los
criterios por pais para cada cribado (ej: VHB cuando la prevalencia > 0,1)
mientras que antes se hacia deterministico (ej: Camerun -> cribar VHB y HIV)

    Esta parte se implement� en Jupyter 999jupyernotebooks/cribmi/cribmi.pynb
    (backup en ./source)
    que captura el excel de criterios que provee ISGLOBAL:
        u.SFTP("sisap").ls("cribmi")
        'Recomendaciones de Catalu�a_Prevalencias-Incidencias-Endemicidad_19012022_modificado.xlsx'
    
    y genera el fichero ISMI_flags.csv (en ./dades_noesb)
    que se captura en este! script

Este script:
    1. Genera la tabla de p2262: recerca_av.mst_avisos_cribmi
    2. rellena redics.rec_avisos
"""

import os
import collections as c
import sisapUtils as u
import sisaptools as t
import pandas as pd


ISMI_FLAGS = "dades_noesb/ISMI_flags.csv"

validacio = set(["CRIB001", "CRIB002", "CRIB003", "CRIB004",
                 "CRIB005", "CRIB006", "CRIB008",
                 "CRIB007", "CRIB009",
                 ])

# retomamos en 2023 con nuevos validadores, veran avisos
validadors = set([
    # ("00166", "40679"),  # modul MG13: YOLANDA RANDO
    ("00167", "3219"),   # modul MG08: ANNA M AGUILAR
    # ("00462", "MED20"),  # modul MED20: C-SEQUEIRA AYMAR, ETHEL
    # ("00462", "MED11"),  # modul MED11: DRA. MARTINEZ PEREZ
    # ("00441", "4T"),     # Mencia Benitez (consultable por Carol Guiriguet)
    ("00016", "5000"),   # Ponts: Dra EVA SERRA LLAVAL
    ("00065", "S"),      # Salou: DRA. ARANZAZU LLANO SANCHEZ
    ("00050", "M"),      # Tarragona 1: DRA. ROXANA CATALIN
    ("00088", "2"),      # ABS AMETLLA DE MAR-EL PERELL�: MAESTRO IBA�EZ, MANUEL VICENTE
    ("00089", "30"),     # ABS AMPOSTA: CURTO ROMEU, CLAUDIA
    ("00020", "13113"),  # EAP Alcarr�s: MF
    ])

validadors_inf = set([
    ("00176", "INF02"),     # ABS ST. BOI-3 (Camps Blancs): MARIA ROSA GARCIA CERDAN
    ("00020", "113"),       # AP Alcarr�s: DUI
])

# rellenar eqa_ind.mst_rec_avisos (que la pilla directamente avisos.py para volcarla a redics
# o alguna otra mas aislada mst_rec_avisos y volcarla a redics
# TaulaMy = "mst_rec_avisos"

# Equipos intervenci�n:
upint = set([
    # Terres de l_Ebre:
    "00053",  # EAP Terra Alta (Gandesa)
    "00089",  # EAP Amposta
    "00091",  # EAP St. Carles de la R�pita
    "00088",  # EAP L_Ametlla de Mar - El Perell�
    # Lleida:
    "00016",  # EAP Ponts
    "00020",  # EAP Alcarr�s
    "00003",  # EAP Alfarr�s - Almenar
    # Tarragona:
    "00050",  # EAP Tarragona- 1 (La Canonja/Bonavista)
    "00068",  # EAP Tarragona- 6 (T�rraco)
    "00065",  # EAP Salou
    "00051",  # EAP Tarragona- 2 (Torreforta)
    "00041",  # EAP Constant�
    # Costa de Ponent:
    "00155",  # EAP Esparreguera
    "00175",  # EAP St. Boi de Llobregat- 2 (Mol� Nou)
    "00159",  # EAP Gav�- 2
    "00176",  # EAP St. Boi de Llobregat- 3 (Camps Blancs)
    "00156",  # EAP Esplugues de LLobregat- 1 (Can Vidalet)
    "00167",  # EAP L_Hospitalet de Llobregat- 8 (Florida sud)
    ])

class CRIB(object):

    """."""

    def __init__(self):
        """."""
        self.get_nacionalitats()
        self.get_id_to_hash()
        self.get_denominadors()
        self.get_exclusions()
        self.get_numeradors()
        self.get_pacients()
        self.y, self.m = u.getOne("select year(data_ext), month(data_ext) from dextraccio", "nodrizas")
        self.get_output()
        self.upload_sisap()
        self.get_indicadors()
        self.send_correu()

    def get_nacionalitats(self):
        """."""
        self.nacionalitats = c.defaultdict(set)
        # file = "dades_noesb/CRIB-MI_Nacionalitats.csv"
        # nuevos criterios: generados con 999jupyernotebooks/cribmi/cribmi.pynb
        columnes = ("", "", "", "",
                    "CRIB001", "CRIB002", "CRIB003", "CRIB005", "CRIB006",
                    "CRIB008", "CRIB004", "CRIB007", "CRIB009")
        for row in u.readCSV(ISMI_FLAGS, sep=","):
            # print(type(row), row)
            pais = row[0].zfill(3)
            for i in range(4, 13):
                if columnes[i] and int(row[i]):
                    self.nacionalitats[pais].add(columnes[i])
        # print('nacionalitats done')
        # print('180:', self.nacionalitats['180'])

    def get_id_to_hash(self):
        """."""
        self.id_to_hash = {id: hash for (id, hash)
                           in u.getAll("select id_cip_sec, hash_d \
                                        from eqa_u11", "nodrizas")}
        # print('get_id_to_hash done')

    def get_denominadors(self):
        """."""
        self.poblacio = {}
        self.denominadors = c.defaultdict(set)
        sql = """
              select id_cip_sec, codi_sector, up, uba, ubainf, nacionalitat, edat, sexe, ates
              from assignada_tot
              where edat > 14 and nacionalitat not in ('', '724')
              """
        for id, sector, up, uba, ubainf, pais, edat, sexe, ates in u.getAll(sql, "nodrizas"):
            self.poblacio[id] = (self.id_to_hash[id], sector, up, uba, ubainf, edat, sexe, ates)
            for indicador in self.nacionalitats[pais]:
                # if indicador not in validacio or up in upint:   # (up, uba) in validadors: up in upint:
                if indicador not in validacio or (up, uba) in validadors or (up, ubainf) in validadors_inf or up in upint:   # (up, uba) in validadors: up in upint:
                    if indicador == 'CRIB009' and edat <= 49 and sexe == 'D':
                        self.denominadors[indicador].add(id)
                    elif indicador != 'CRIB009':
                        self.denominadors[indicador].add(id)
        # print('get_denominadors done')
        # print(indicador, self.denominadors[indicador])
        # print(self.denominadors.items())
        # print(len(self.denominadors['CRIB001']))

    def get_exclusions(self):
        """."""
        codis = {"CRIB001": (101,),
                 "CRIB002": (13, 14),
                 "CRIB003": (12, 29, 550, 553),
                 "CRIB004": (639, 640),
                 "CRIB005": (771, 772),
                 "CRIB006": (773, 774),
                 "CRIB008": (775, 776),
                 "CRIB007": (797, 415, 85, 425, 486)}
        exc_crib009 = ('N90.81',
                        'C01-N90.81', 'C01-N90.810', 'C01-N90.811',
                        'C01-N90.812', 'C01-N90.813', 'C01-N90.818',
                       'Z60.81',
                       'N90.91',
                       'S38.2',
                        'C01-S38.2', 'C01-S38.21'
                        'C01-S38.211','C01-S38.211A','C01-S38.211D',
                        'C01-S38.211S','C01-S38.212','C01-S38.212A',
                        'C01-S38.212D','C01-S38.212S')
        self.exclusions = c.defaultdict(set)
        codis_inv = c.defaultdict(set)
        codis_sql = set()
        for indicador, agrupadors in codis.items():
            for agrupador in agrupadors:
                codis_inv[agrupador].add(indicador)
                codis_sql.add(agrupador)
        sql = """
              select id_cip_sec, ps
              from eqa_problemes
              where ps in {}""".format(tuple(codis_sql))
        for id, ps in u.getAll(sql, "nodrizas"):
            for indicador in codis_inv[ps]:
                if id in self.denominadors[indicador]:
                    self.exclusions[indicador].add(id)
        sql = "select id_cip_sec, 'CRIB009' \
               from problemes \
               where pr_cod_ps in {}".format(tuple(exc_crib009))
        for id, indicador in u.getAll(sql, "import"):
            if id in self.denominadors[indicador]:
                self.exclusions[indicador].add(id)

        # agrupdor-837 (OLD no vale para ISMI):
        #   'XP0027': 'Salut mental immigrants (CRIBMI)',
        #   'XX0001': 'Risc mutilacio genital femenina (CRIBMI)'
        # sql = "select id_cip_sec from eqa_variables where agrupador=837"
        # for id, in u.getAll(sql, "nodrizas"):
        #      for indicador, agrupadors in codis.items():
        #         # print indicador, id
        #         self.exclusions[indicador].add(id)

    def get_numeradors(self):
        """."""
        self.numeradors = c.defaultdict(set)
        self.__get_serologies()
        self.__get_variables()
        self.__get_vacunes()
        self.__get_rx()
        # print(len(self.numeradors['CRIB001']))
        # print('get_numeradors_done')

    def __get_serologies(self):
        """."""
        equivalencia = {"VIH": "CRIB001", "VHB": "CRIB002", "VHC": "CRIB003"}
        codis = {codi: equivalencia[agrupador[2:5]]
                 for (codi, agrupador)
                 in u.getAll("select codi, agrupador from cat_dbscat \
                              where taula = 'serologies'", "import")
                 if agrupador[2:5] in equivalencia}
        sql = "select id_cip_sec, cod from nod_serologies"
        for id, codi in u.getAll(sql, "nodrizas"):
            if codi in codis:
                indicador = codis[codi]
                if id in self.denominadors[indicador]:
                    self.numeradors[indicador].add(id)

    def __get_variables(self):
        """."""
        codis = {777: "CRIB004",
                 778: "CRIB005",
                 779: "CRIB006",
                 780: "CRIB008"}
        sql = "select id_cip_sec, agrupador \
               from eqa_variables \
               where agrupador in {}".format(tuple(codis))
        for id, agr in u.getAll(sql, "nodrizas"):
            indicador = codis[agr]
            if id in self.denominadors[indicador]:
                self.numeradors[indicador].add(id)

        codis = {
            'YX0001': {
                "indicador": "CRIB009",
                "valors": (0, 1)}
            }

        for codi, v in codis.items():
            indicador, valors = v["indicador"], v["valors"]
            sql = """
                select
                    id_cip_sec,
                    '{}'
                from
                    variables2
                where
                    vu_cod_vs = '{}' and vu_val in {}
                """.format(indicador, codi, valors)
            for id, agr in u.getAll(sql, "import"):
                if id in self.denominadors[indicador]:
                    self.numeradors[indicador].add(id)

    def __get_vacunes(self):
        """."""
        indicador = "CRIB002"
        sql = """
              select
                id_cip_sec
              from
                eqa_vacunes
              where
                agrupador = 15
              """
        for id, in u.getAll(sql, "nodrizas"):
            if id in self.denominadors[indicador]:
                self.numeradors[indicador].add(id)

    def __get_rx(self):
        """."""
        indicador = "CRIB004"
        codis = ("RA00001", "RA00002", "RA00007",
                 "RA00590", "RA00591", "RA01055")
        sql = """
              select
                id_cip_sec
              from
                nod_proves, dextraccio
              where
                inf_codi_prova in {}
                and oc_data <= data_ext
              """.format(codis)
        for id, in u.getAll(sql, "nodrizas"):
            if id in self.denominadors[indicador]:
                self.numeradors[indicador].add(id)

    def get_pacients(self):
        """."""
        self.pacients = set()
        for indicador, pacients in self.denominadors.items():
            for id in pacients:
                hash, sector, up, uba, ubainf, dummy_edat, dummy_sexe, ates = self.poblacio[id]
                this = (hash, sector, up, uba, ubainf, indicador, ates,
                        1 * (id in self.numeradors[indicador]),
                        4 * (id in self.exclusions[indicador]))
                self.pacients.add(this)
        # print('get_pacients done')
        # print(len(self.pacients))

        # Avisos
        # Malaties copiado de upload_sisap
        malalties = ("VIH", "Hepatitis B", "Hepatitis C", "Tuberculosi",
                    "Strongiloidiasis", "Schistosomiasis", "Salut mental",
                    "Chagas", "Mutilacio Genital")
        # malalties = ("VIH", "HB", "HC", "Tbc", "Str", "Sch", "Str", "Ment", "Chg", "MGF")

        self.upload_avisos_pre = c.defaultdict(list)
        for hash, sector, up, uba, ubainf, indicador, ates, num, excl in self.pacients:
            if excl == 0 and num == 0:
                ordre = int(indicador[-1])
                avisos = malalties[ordre - 1]
                self.upload_avisos_pre[(sector, hash, up)].append(avisos)

        self.upload_avisos = []
        for hashes, aviso in self.upload_avisos_pre.items():
            sector, hash, up = list(hashes)
            avisos = sorted(aviso)
            avisos_text = ''.join('Cribrar: ' + ', '.join(avisos))
            avisos_text2 = '1'
            avisos_text3 = 'Immigrant'
            avisos_text4 = None
            avisos_text5 = None
            avisos_text6 = 'PROCES'
            avisos_text7 = None
            avisos_text8 = 'migra'
            avisos_grup = 'Variable'
            this = (sector, hash, up, 'CLI15', avisos_text, avisos_text2, avisos_text3, avisos_text4, avisos_text5, avisos_text6, avisos_text7, avisos_text8, avisos_grup)
            self.upload_avisos.append(this)

        # print(self.upload_avisos_pre[('D9E38F9054B40A1AA420BB9E16E853816E18E6A4', '6310', '00094')])  # hash 29AC56D1927990BC96EA12FE90321D71B3527129	sector 6102 up 00030
        # print(self.upload_avisos[:5])
        db = 'recerca_av'
        tb = 'mst_avisos_cribmi'
        tb_redics_avisos = 'rec_avisos'
        try:
            u.execute('drop table {}'.format(tb), db)
        except:
            pass

        sql = """
              create table {} (
              codi_sector varchar(4),
              usua_cip varchar(40),
              usua_uab_up varchar(5),
              talv_tipus varchar(5),
              talv_text1 varchar(200),
              talv_text2 varchar(1),
              talv_text3 varchar(100),
              talv_text4 varchar(100),
              talv_text5 varchar(50),
              talv_text6 varchar(100),
              talv_text7 varchar(100),
              talv_text8 varchar(50),
              talv_grup varchar(15))
              """.format(tb)

        u.execute(sql, db)

        u.listToTable(self.upload_avisos, tb, db)

        # TODO: DEScomentar para CARGAR a avisos redics, comentar para prototipar seguimiento
        u.listToTable(self.upload_avisos, tb_redics_avisos, 'redics')

        print('escrito redics.rec_avisos <- con avisos de CRIBMI;   !!! Avisale a Marcos Lopez para que informe el catalogo')

    def get_output(self):
        """."""
        self.llistats = []
        self.resultats = []
        indicadors = c.Counter()
        immigrants_up = c.defaultdict(list)
        for hash, sec, up, uba, ubainf, ind, ates, num, excl in self.pacients:
            sit = excl if excl else 9 if num else 0
            if up and uba:
                self.llistats.append((up, uba, "M", ind, hash, sec, sit))
            if up and ubainf:
                self.llistats.append((up, ubainf, "I", ind, hash, sec, sit))
            if not excl:
                indicadors[(up, uba, "M", ind, ates, "DEN")] += 1
                indicadors[(up, ubainf, "I", ind, ates, "DEN")] += 1
                immigrants_up[(up, hash, ates)].append(ind)
                if num:
                    indicadors[(up, uba, "M", ind, ates, "NUM")] += 1
                    indicadors[(up, ubainf, "I", ind, ates, "NUM")] += 1
        for hashes, aviso in immigrants_up.items():
            up, dummy_hash, ates = list(hashes)
            n_aviso = len(aviso)
            indicadors[(up, "dummy", "M", "Qualsevol", ates, "DEN")] +=1
            indicadors[(up, "dummy", "M", "Qualsevol", ates, "NUM")] += n_aviso
        for (up, uba, tipus, ind, ates, concepte), n in indicadors.items():
            if concepte == "DEN" and up and uba:
                num = indicadors[(up, uba, tipus, ind, ates, "NUM")]
                res = float(num) / n
                norslt = n - num if ind != "Qualsevol" else 0
                this = (self.y, self.m, up, uba, tipus, ind, ates, num, n, res, norslt, 0, 0)
                self.resultats.append(this)
        print('get_output done')
        print(self.resultats[:3])

    def upload_sisap(self):
        """."""
        db = "recerca_av"
        # resultats
        DB_ORIG = ("redics", "pdp")
        DB_DESTI = ("p2262", "recerca_av")
        TB_ORIG = "altindicadors"
        TB_DESTI = "_".join(['exp', TB_ORIG[3:]])
        # u.execute("delete from {} \
        #            where dataany = {} and \
        #                  datames = {} and \
        #                  indicador like 'CRIB%'".format(tb, self.y, self.m), db)
        columns = []
        with t.Database(*DB_ORIG) as db:
            for column in db.get_table_columns(TB_ORIG):
                    columns.append(db.get_column_information(column, TB_ORIG)['create'])
        columns[1] = 'DATAMES int'  # erroneamente lo pilla como tipo DATE
        columns[8] = 'RESULTAT double'  # erroneamente lo pilla como entero.
        columns.insert(6, 'ATES int')  # quieren seguimiento estratificado por atesos
        with t.Database(*DB_DESTI) as db:
            db.create_table(TB_DESTI, columns, remove=False)
            db.list_to_table(self.resultats, TB_DESTI)
        # llistats
        TB_ORIG = "altllistats"
        TB_DESTI = "_".join(['exp', TB_ORIG[3:]])
        columns = []
        # u.execute("delete from {} \
        #            where indicador like 'CRIB%'".format(tb), db)
        with t.Database(*DB_ORIG) as db:
            for column in db.get_table_columns(TB_ORIG):
                    columns.append(db.get_column_information(column, TB_ORIG)['create'])
        with t.Database(*DB_DESTI) as db:
            db.create_table(TB_DESTI, columns, remove=True)
            db.list_to_table(self.llistats, TB_DESTI)
        # cataleg
        TB_ORIG = "altcataleg"
        TB_DESTI = "_".join(['exp', TB_ORIG[3:]])
        pare = "CRIB-MI"
        malalties = ("VIH", "Hepatitis B", "Hepatitis C", "Tuberculosi",
                     "Strongiloidiasis", "Schistosomiasis", "Salut mental",
                     "Chagas", "MGF")
        cataleg = []
        for indicador in self.denominadors:
            ordre = int(indicador[-1])
            literal = "Cribratge " + malalties[ordre - 1]
            this = (self.y, indicador, literal, ordre, pare,
                    1, 0, 0, 0, 0, 1, "", "PCT")
            cataleg.append(this)
        columns = []
        with t.Database(*DB_ORIG) as db:
            for column in db.get_table_columns(TB_ORIG):
                    columns.append(db.get_column_information(column, TB_ORIG)['create'])
        with t.Database(*DB_DESTI) as db:
            db.create_table(TB_DESTI, columns, remove=True)
            db.list_to_table(cataleg, TB_DESTI)
        # u.execute("delete from {} \
        #            where dataany = {} and \
        #                  pare = '{}'".format(tb, self.y, pare), db)
        # u.listToTable(cataleg, tb, db)
        """
        # cataleg pare
        tb = "altcatalegpare"
        upload = [(self.y, pare, "Estudi de recerca CRIB-MI", 6, "VALIDACIO")]
        u.execute("delete from {} \
                   where dataany = {} and \
                         pare = '{}'".format(tb, self.y, pare), db)
        u.listToTable(upload, tb, db)
        """
        print('upload done')

    def get_indicadors(self):
        sql = """
            WITH
                cataleg as (
                    SELECT indicador, literal
                    FROM recerca_av.exp_cataleg
                    UNION
                    SELECT
                        'Qualsevol' as indicador,
                        'Qualsevol cribratge (taxa)' as literal
                    FROM dual
                )
            SELECT
                up,
                ei.indicador,
                ec.LITERAL,
                ates,
                sum(numerador),
                sum(denominador),
                sum(noresolts),
                count(distinct uab)
            FROM
                exp_indicadors ei
            INNER JOIN cataleg ec on
                ei.INDICADOR = ec.INDICADOR
            WHERE
                tipus = 'M'
            GROUP BY
                up,
                ei.indicador,
                ates,
                ec.LITERAL
        """
        columns = ('UP', 'CODI', 'DESC', 'ATES', 'NUMERADOR', 'DENOMINADOR', 'NO_RESOLTS', 'N_UBAS')
        with t.Database("p2262","recerca_av") as db:
            df = pd.DataFrame(db.get_all(sql), columns=columns)
        df.to_excel(os.path.join(t.constants.TMP_FOLDER, "ISMI_seguiment.xls"), index=False)

    def send_correu(self):
        """."""
        mail = t.Mail()
        mail.to.append("angeline.cruz@isglobal.org")
        mail.cc.extend((
            "lmendezboo@gencat.cat",
            "caguilar.ebre.ics@gencat.cat",
            "aqueiroga.ebre.ics@gencat.cat",))
        mail.subject = "SISAP: Indicadors de seguiment ISMI-HEALT"
        mail.text = "Enviem indicadors de seguiment a data {}-{}".format(self.y, self.m)
        mail.attachments.append(os.path.join(t.constants.TMP_FOLDER, "ISMI_seguiment.xls"))
        mail.send()


if __name__ == "__main__":
    CRIB()
