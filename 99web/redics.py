from sisapUtils import getOne, getAll
from tabulate import tabulate
from datetime import datetime


def duration(i, f):
    if i:
        if not f:
            f = datetime.now()
        t = f - i
        seconds = t.total_seconds()
        h = int(seconds // 3600)
        m = int((seconds % 3600) // 60)
        s = int(seconds % 60)
        return '{}h {}m {}s'.format(h, m, s)
    else:
        return ''


def status(exti, extf, cari, carf):
    if (exti and not extf) or (cari and not carf):
        return 'd'
    else:
        return ''


db = 'redics'
dat, = getOne("select to_char(max(d_df_pinc), 'YYYYMMDD') from redics.directora", db)
resul = sorted([(taula, duration(ext_ini, ext_fi), duration(car_ini, car_fi), status(ext_ini, ext_fi, car_ini, car_fi)) for taula, ext_ini, ext_fi, car_ini, car_fi in getAll(
        "select dd_taula, dd_dhi_ext_inc, dd_dhf_ext_inc, dd_dhi_car_inc, dd_dhf_car_inc from redics.directora_det where dd_df_pinc = to_date('{0}', 'YYYYMMDD')".format(dat), db)])
headers = [(dat, duration(ext_ini, ext_fi), duration(car_ini, car_fi), car_fi if car_fi else '') for taula, ext_ini, ext_fi, car_ini, car_fi in getAll(
        "select 'total', d_dhi_ext_inc, d_dhf_ext_inc, d_dhi_car_inc, d_dhf_car_inc from redics.directora where d_df_pinc = to_date('{0}', 'YYYYMMDD')".format(dat), db)]
taula = tabulate(resul, headers[0], 'html')
print taula
