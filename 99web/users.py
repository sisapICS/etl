import cx_Oracle

import sisaptools as u


header = ("EXP_FF2", "EXP_LM2", "PRO_FF2", "PRO_LM2", "PDP_WEB", "PDP_FFA", "PDP_LMB")
taula = "<table style='font-family:calibri;'><thead><tr><td></td>"
taula += "".join(["<td>{}</td>".format(item) for item in header])
taula += "</tr></thead><tbody>"

for sector in u.constants.SECTORS_ECAP:
    taula += "<tr><td>{}</td>".format(sector)
    for user in ("data", "data_l", "prod", "prod_l", "pdp"):
        try:
            a = u.Database(sector, user, retry=False, do_fallback=False)
        except Exception as e:
            taula += "<td style='background:lightcoral; align=centrer;' title='{}'></td>".format(str(e))
        else:
            taula += "<td style='background:lightgreen; align=centrer;'></td>"
    for _user in ("P{}XFFAN", "P{}XLMBN"):
        user = _user.format(sector[1:])
        string = u.services.DB_INSTANCES[sector]["string"]
        try:
            a = cx_Oracle.connect(user, "temp01", string)
        except Exception as e:
            taula += "<td style='background:lightcoral; align=centrer;' title='{}'></td>".format(str(e))
        else:
            taula += "<td style='background:lightgreen; align=centrer;'></td>"
    taula += "</tr>"

taula += "</tbody></table>"
print(taula)
