drop table cslIndicadors;
create table cslIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(20)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,numerador number
 ,denominador number
 ,resultat number
);

commit;

ALTER TABLE cslIndicadors ADD (
  CONSTRAINT cslIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));

drop table cslLlistats;
create table cslLlistats (
  up varchar2(5)
  ,uab varchar2(5)
  ,tipus varchar2(1)
  ,indicador varchar2(10)
  ,idpac varchar2(40)
  ,sector varchar2(4)
  ,exclos int
);

commit;

ALTER TABLE cslLlistats ADD (
  CONSTRAINT cslLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,idpac,sector,exclos));
  
drop table cslCataleg;
create table cslCataleg (
  dataAny number
  ,indicador varchar2(8)
  ,literal varchar2(300)
  ,ordre number
  ,llistat number
  ,wiki varchar2(1000)
  ,oficial varchar(15)
);

commit;

ALTER TABLE cslCataleg ADD (
  CONSTRAINT cslCatalegPK PRIMARY KEY (dataAny,indicador));

drop table cslLlistatsData;
create table cslLlistatsData (
  dat date
);