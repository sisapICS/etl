drop table itindicadors;
create table itindicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(20)
 ,indicador varchar2(15)
 ,numerador number
 ,denominador number
 ,resultat number
);

commit;

ALTER TABLE itindicadors ADD (
  CONSTRAINT itindicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,indicador));
  
drop table itcataleg;
create table itcataleg (
  dataAny number
  ,indicador varchar2(15)
  ,literal varchar2(300)
  ,ordre number
  ,pare varchar2(15)
  ,llistat number
  ,invers number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
);

commit;

ALTER TABLE itcataleg ADD (
  CONSTRAINT itcatalegPK PRIMARY KEY (dataAny,indicador));


commit;

drop table itCatalegPare;
create table itCatalegPare (
  dataAny number
  ,pare varchar2(8)
  ,literal varchar2(300)
  ,ordre number
);

commit;

ALTER TABLE itcatalegpare ADD (
  CONSTRAINT itcatalegparePK PRIMARY KEY (dataAny,pare));
  
commit;


ALTER TABLE itindicadorsllistat ADD (
  CONSTRAINT itindicadorsllistatPK PRIMARY KEY (up,uab,tipus,indicador,exclos,idpac));

 COMMIT;