drop table ct_der_serveid;
create table ct_der_serveid (
 servei varchar2(5)
 ,desc_servei varchar2(40)
 ,servei_agrupat varchar2(5)
 ,desc_agrupat varchar2(40)
 ,codi_khalix varchar2(10)
);

commit;

drop table ct_der_serveio;
create table ct_der_serveio (
 sector varchar2(4)
 ,servei varchar2(10)
 ,desc_servei varchar2(40)
 ,desc_codi varchar2(40)
 ,codi_khalix varchar2(10)
);

commit;


insert into ct_der_serveid values ('21211','TRAUMATOLOGIA','21211','TRAUMATOLOGIA','DTRAUMA');
insert into ct_der_serveid values ('20209','OFTALMOLOGIA','20209','OFTALMOLOGIA','DOFTALM');
insert into ct_der_serveid values ('10103','DERMATOLOGIA','10103','DERMATOLOGIA','DDERMAT');
insert into ct_der_serveid values ('NNNNN','PROVES','','','');
insert into ct_der_serveid values ('20210','OTORRINOLARINGOLOGIA','20210','OTORRINOLARINGOLOGIA','DOTORRI');
insert into ct_der_serveid values ('70121','REHABILITACIO','70121','REHABILITACIO','DREHABI');
insert into ct_der_serveid values ('40217','GINECOLOGIA','40217','GINECOLOGIA','DGINECO');
insert into ct_der_serveid values ('20201','CIRUGIA GENERAL','FUSIO','CIRUGIA GENERAL','DCIRGEN');
insert into ct_der_serveid values ('10114','SERVEIS MEDICS:ALTRES','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('20212','UROLOGIA','20212','UROLOGIA','DUROLOG');
insert into ct_der_serveid values ('10105','GASTROENTEROLOGIA','10105','GASTROENTEROLOGIA','DGASTRO');
insert into ct_der_serveid values ('10131','CARDIOLOGIA','10131','CARDIOLOGIA','DCARDIO');
insert into ct_der_serveid values ('10115','MEDICINA INTERNA: URGENCIES','10115','MEDICINA INTERNA: URGENCIES','DMIURGE');
insert into ct_der_serveid values ('10118','ANATOMIA PATOLOGICA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('10108','NEUROLOGIA','10108','NEUROLOGIA','DNEUROL');
insert into ct_der_serveid values ('50115','PSIQUIATRIA','50115','PSIQUIATRIA','DPSIQUI');
insert into ct_der_serveid values ('10111','REUMATOLOGIA','10111','REUMATOLOGIA','DREUMAT');
insert into ct_der_serveid values ('LLLLL','OPTOMETRIA','LLLLL','OPTOMETRIA','DOPTOME');
insert into ct_der_serveid values ('10155','RADIOLOGIA','','','');
insert into ct_der_serveid values ('10110','PNEUMOLOGIA','10110','PNEUMOLOGIA','DPNEUMO');
insert into ct_der_serveid values ('20204','CIRURGIA MAXIL?LO FACIAL','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('10104','ENDOCRINOLOGIA','10104','ENDOCRINOLOGIA','DENDOCR');
insert into ct_der_serveid values ('02004','PSICOLOGIA','02004','PSICOLOGIA','DPSICOL');
insert into ct_der_serveid values ('40101','OBSTETRICIA','40101','OBSTETRICIA','DOBSTET');
insert into ct_der_serveid values ('OOOOO','PODOLOGIA','OOOOO','PODOLOGIA','DPODOLO');
insert into ct_der_serveid values ('20207','CIRURGIA VASCULAR','FUSIO','CIRURGIA VASCULAR','DCIRVAS');
insert into ct_der_serveid values ('30319','PEDIATRIA : URGENCIES','30319','PEDIATRIA : URGENCIES','DPURGEN');
insert into ct_der_serveid values ('10102','AL?LERGIA','10102','AL?LERGIA','DALLERG');
insert into ct_der_serveid values ('10106','ODONTOLOGIA','10106','ODONTOLOGIA','DODONTO');
insert into ct_der_serveid values ('VVVVV','CONSULTA DATENCI? IMMEDIATA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('10101','MEDICINA INTERNA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('10107','HEMATOLOGIA CLINICA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30215','PEDIATRIA: CIRURGIA PEDIATRICA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('04231','LOGOPEDIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30085','LLEVADORA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('10109','NEFROLOGIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('JJJJJ','NEUROELECTROFISIOLOGIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30305','PEDIATRIA : PAIDOPSIQUIATRIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('20203','CIRURGIA DIGESTIVA','FUSIO','CIRURGIA DIGESTIVA','DCIRDIG');
insert into ct_der_serveid values ('30999','INFERMERA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30317','PEDIATRIA: NEUROLOGIA','10108','NEUROLOGIA','DNEUROL');
insert into ct_der_serveid values ('CRO','CRO','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30315','PEDIATRIA: CARDIOLOGIA','10131','CARDIOLOGIA','DCARDIO');
insert into ct_der_serveid values ('30306','PEDIATRIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('10150','UNITAT CLINICA DOLOR','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30314','PEDIATRIA: ENDOCRINO','10104','ENDOCRINOLOGIA','DENDOCR');
insert into ct_der_serveid values ('30316','PEDIATRIA: GASTROENTEROLOGIA','10105','GASTROENTEROLOGIA','DGASTRO');
insert into ct_der_serveid values ('80123','MEDICINA NUCLEAR','','','');
insert into ct_der_serveid values ('20208','NEUROCIRURGIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('SSSSS','LABORATORI HOSPITAL','','','');
insert into ct_der_serveid values ('30313','PEDIATRIA : INMUNOLOGIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30318','PEDIATRIA: PNEUMOLOGICA','10110','PNEUMOLOGIA','DPNEUMO');
insert into ct_der_serveid values ('20205','CIRURGIA PLASTICA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('50116','DROGODEPENDENCIES','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('10112','ONCOLOGIA MEDICA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('05999','TREBALL SOCIAL','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30320','PEDIATRIA : ESTIMULACI? PRECO?','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('10149','UNITAT GERIATRICS AGUDITZATS','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30310','PEDIATRIA : NEFROLOGIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('UUUUU','HEMOSTASIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30321','PEDIATRIA GENERAL','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('10125','INFECCIOSES','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('BBBBB','GENETICA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('PSGE','PSICOGERIATRIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30302','PEDIATRIA : LACTANTS','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('80130','ANESTESIA I REANIMACIO','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('IIIII','MALALTIES DE TRANSMISSI? SEXUAL','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('80125','UNITAT DE CURES PAL?LIATIVES','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30303','PEDIATRIA : ESCOLARS','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('10119','MEDICINA DEL TREBALL','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('GGGGG','MALATIES TROPICALS IMPORTADES','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30301','PEDIATRIA : UNITAT DE NEONATS','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30308','PEDIATRIA : HEMATOLOGIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('20206','CIRURGIA TORACICA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('QQQQQ','TUBERCOLOSI','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('HHHHH','ATENCI? AL VIATGER','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('10137','IMMUNOLOGIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('TTTTT','LABORTORI MICROBIOLOGIA','','','');
insert into ct_der_serveid values ('20202','CIRURGIA CARDIACA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30304','PEDIATRIA : ADOLESCENTS','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('10152','RADIOTERAPIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('90000','FARMACIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('60118','UNITAT DE CREMATS','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('EEEEE','MEDICACI? CULTURAL','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('30312','PEDIATRIA : ONCOLOGIA','99999','ALTRES','DALTRES');
insert into ct_der_serveid values ('10116','UNITAT DE DIALISI','99999','ALTRES','DALTRES');


