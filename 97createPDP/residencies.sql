drop table resIndicadors;
create table resIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(5)
 ,residencia varchar2(13)
 ,tipus varchar2(1)
 ,indicador varchar2(10)
 ,esperats number
 ,detectats number
 ,resolts number
 ,deteccio number
 ,assolDeteccio number
 ,resolucio number
 ,resultat number
 ,assolResultat number
 ,punts number 
 ,noResolts number
);

commit;

ALTER TABLE resIndicadors ADD (
  CONSTRAINT resIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));
  
commit;

drop table resCataleg;
create table resCataleg (
  dataAny number
  ,indicador varchar2(10)
  ,literal varchar2(80)
  ,ordre number
  ,pare varchar2(8)
  ,llistat number
  ,invers number
  ,mdet number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
  ,lcurt varchar2(80)
  ,rsomin number
  ,rsomax number
);

commit;

ALTER TABLE resCataleg ADD (
  CONSTRAINT resCatalegPK PRIMARY KEY (dataAny,indicador));
  
commit;

drop table resCatalegPare;
create table resCatalegPare (
  dataAny number
  ,pare varchar2(8)
  ,literal varchar2(80)
  ,ordre number
  ,lcurt varchar2(80)
);

commit;

ALTER TABLE resCatalegPare ADD (
  CONSTRAINT resCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;


drop table resLlistats;
create table resLlistats (
  up varchar2(5)
  ,uab varchar2(5)
  ,tipus varchar2(1)
  ,indicador varchar2(10)
  ,idpac varchar2(40)
  ,sector varchar2(4)
  ,exclos int
  ,residencia varchar2(13)
);

commit;

ALTER TABLE resLlistats ADD (
  CONSTRAINT resLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,exclos,idpac,residencia,sector));
 
commit;

drop table resLlistatsData;
create table resLlistatsData (
  dat date
);

commit;