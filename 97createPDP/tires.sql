drop table Tirdetall;
create table Tirdetall (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(5)
 ,tipus varchar2(1)
 ,dm varchar2(8)
 ,total number
 ,c1 number
 ,c2 number
 ,c3 number
 ,c4 number
 ,c5 number
 ,c6 number
);

commit;

ALTER TABLE Tirdetall ADD (
  CONSTRAINT TirdetallPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,dm));

commit;

drop table Tirdetallcataleg;
create table Tirdetallcataleg (
  codi varchar2(10)
  ,literal varchar2(300)
  ,ordre number
);

commit;

ALTER TABLE Tirdetallcataleg ADD (
  CONSTRAINT TirdetallcatalegPK PRIMARY KEY (codi));
  
commit;


drop table TirdetallcatalegColumna;
create table TirdetallcatalegColumna (
  columna varchar2(10)
  ,literal varchar2(300)
);

commit;

ALTER TABLE TirdetallcatalegColumna ADD (
  CONSTRAINT TirdetallcatalegColumnaPK PRIMARY KEY (columna));
  
commit;