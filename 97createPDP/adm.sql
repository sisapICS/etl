drop table admIndicadors;
create table admIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,adm varchar2(20)
 ,uas varchar2(20)
 ,uba varchar2(20)
 ,indicador varchar2(10)
 ,edat varchar2(15)
 ,numerador number
 ,denominador number
 ,resultat number
 ,nivell number
);

commit;

ALTER TABLE admIndicadors ADD (
  CONSTRAINT admIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,adm,uas,uba,indicador,edat,nivell));
  
drop table admCataleg;
create table admCataleg (
  dataAny number
  ,indicador varchar2(8)
  ,literal varchar2(300)
  ,ordre number
  ,pare varchar2(8)
  ,llistat number
  ,invers number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
  ,tipusvalor varchar2(25)
);

commit;

ALTER TABLE admCataleg ADD (
  CONSTRAINT admCatalegPK PRIMARY KEY (dataAny,indicador));


commit;

drop table admCatalegPare;
create table admCatalegPare (
  dataAny number
  ,pare varchar2(8)
  ,literal varchar2(300)
  ,ordre number
  ,PANTALLA VARCHAR2(20)
);

commit;

ALTER TABLE admCatalegPare ADD (
  CONSTRAINT admCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;
