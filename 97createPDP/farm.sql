drop table farmIndicadors;
create table farmIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(20)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,numerador number
 ,denominador number
 ,resultat number
);

commit;

ALTER TABLE farmIndicadors ADD (
  CONSTRAINT farmIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));

drop table farmLlistats;
create table farmLlistats (
  up varchar2(5)
  ,uab varchar2(5)
  ,tipus varchar2(1)
  ,indicador varchar2(10)
  ,idpac varchar2(40)
  ,sector varchar2(4)
  ,exclos int
);

commit;

ALTER TABLE farmLlistats ADD (
  CONSTRAINT farmLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,idpac,sector,exclos));
  
drop table farmCataleg;
create table farmCataleg (
  dataAny number
  ,indicador varchar2(8)
  ,literal varchar2(300)
  ,ordre number
  ,llistat number
  ,wiki varchar2(1000)
  ,oficial varchar(15)
);

commit;

ALTER TABLE farmCataleg ADD (
  CONSTRAINT farmCatalegPK PRIMARY KEY (dataAny,indicador));

commit;

drop table farmLlistatsData;
create table farmLlistatsData (
  dat date
);

commit;


drop table farmCatalegPare;
create table farmCatalegPare (
  dataAny number
  ,pare varchar2(10)
  ,literal varchar2(300)
  ,ordre number
  ,PANTALLA VARCHAR2(20)
);

commit;

ALTER TABLE farmCatalegPare ADD (
  CONSTRAINT farmCatalegPare PRIMARY KEY (dataAny,pare));
  
commit;