drop table helenaindicadors;
create table helenaindicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(20)
 ,tipus varchar2(1)
 ,indicador varchar2(15)
 ,numerador number
 ,denominador number
 ,resultat number
);

commit;

ALTER TABLE helenaindicadors ADD (
  CONSTRAINT helenaindicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));

drop table helenaLlistats;
create table helenaLlistats (
  up varchar2(5)
  ,uab varchar2(5)
  ,tipus varchar2(1)
  ,indicador varchar2(10)
  ,idpac varchar2(40)
  ,sector varchar2(4)
  ,exclos int
);

commit;

ALTER TABLE helenaLlistats ADD (
  CONSTRAINT helenaLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,idpac,sector,exclos));
  
drop table helenacataleg;
create table helenacataleg (
  dataAny number
  ,indicador varchar2(15)
  ,literal varchar2(300)
  ,ordre number
  ,pare varchar2(15)
  ,llistat number
  ,invers number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
);

commit;

ALTER TABLE helenacataleg ADD (
  CONSTRAINT helenacatalegPK PRIMARY KEY (dataAny,indicador));


commit;

drop table helenaCatalegPare;
create table helenaCatalegPare (
  dataAny number
  ,pare varchar2(8)
  ,literal varchar2(300)
  ,ordre number
);

commit;

ALTER TABLE helenaCatalegPare ADD (
  CONSTRAINT helenaCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;


drop table helenaLlistatsData;
create table helenaLlistatsData (
  dat date
);