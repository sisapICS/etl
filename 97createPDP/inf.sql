drop table infIndicadors;
create table infIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(20)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,motiu varchar2(20)
 ,numerador number
 ,denominador number
);

commit;

ALTER TABLE infIndicadors ADD (
  CONSTRAINT infIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));

commit;

drop table infindicadorsData;
create table infindicadorsData (
  dat date
);
