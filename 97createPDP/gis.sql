drop table gisIndicadors;
create table gisIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(8)
 ,tipus varchar2(1)
 ,indicador varchar2(10)
 ,esperats number
 ,detectats number
 ,resolts number
 ,deteccio number
 ,assolDeteccio number
 ,resolucio number
 ,resultat number
 ,assolResultat number
 ,punts number
 ,noResolts number
 ,assolResolucio number
 ,invers number
 ,mmin number
 ,mmax number
);

commit;

ALTER TABLE gisIndicadors ADD (
  CONSTRAINT gisIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));
  
commit;

drop table gisCataleg;
create table gisCataleg (
  dataAny number
  ,indicador varchar2(10)
  ,literal varchar2(85)
  ,ordre number
  ,pare varchar2(8)
  ,llistat number
  ,invers number
  ,mdet number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
  ,lcurt varchar2(80)
  ,rsomin number
  ,rsomax number
  ,pantalla varchar2(50)
);

commit;

ALTER TABLE gisCataleg ADD (
  CONSTRAINT gisCatalegPK PRIMARY KEY (dataAny,indicador));
  
commit;

drop table gisCatalegPare;
create table gisCatalegPare (
  dataAny number
  ,pare varchar2(8)
  ,literal varchar2(80)
  ,ordre number
  ,lcurt varchar2(80)
  ,proces varchar2(50)
);

commit;

ALTER TABLE gisCatalegPare ADD (
  CONSTRAINT gisCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;


-- drop table gisLlistats;
-- create table gisLlistats (
--   up varchar2(5)
--   ,uab varchar2(5)
--   ,tipus varchar2(1)
--   ,indicador varchar2(10)
--   ,idpac varchar2(40)
--   ,sector varchar2(4)
--   ,exclos int
--   ,residencia varchar2(13)
-- );

-- commit;

-- ALTER TABLE gisLlistats ADD (
--   CONSTRAINT gisLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,exclos,idpac,residencia,sector));
 
-- commit;

-- drop table gisLlistatsData;
-- create table gisLlistatsData (
--   dat date
-- );

commit;