drop table sisap_khx_dades;
create table sisap_khx_dades (
 compte varchar2(25)
 ,periode varchar2(25)
 ,entitat varchar2(25)
 ,analisi varchar2(25)
 ,tipus varchar2(25)
 ,detalls varchar2(25)
 ,controls varchar2(25)
 ,valor number
 ,fitxer varchar2(100)
 ,tipus_dada varchar2(100)
 ,inKhalix number
);

commit;

ALTER TABLE sisap_khx_dades ADD (
  CONSTRAINT sisap_khxFarmacia PRIMARY KEY (compte,periode,entitat,analisi,tipus,detalls,controls,fitxer,tipus_dada));
  
commit;
