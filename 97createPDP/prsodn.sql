drop table prsodnIndicadors;
create table prsodnIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(10)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,esperats number
 ,detectats number
 ,resolts number
 ,deteccio number
 ,assolDeteccio number
 ,resolucio number
 ,resultat number
 ,assolResultat number
 ,punts number 
 ,noResolts number
 ,assolResolucio number
 ,invers number
 ,mmin number
 ,mmax number
 ,tipus_up varchar2(1)
);

commit;

ALTER TABLE prsodnIndicadors ADD (
  CONSTRAINT prsodnIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));
  
commit;

drop table prsodnCataleg;
create table prsodnCataleg (
  dataAny number  
  ,indicador varchar2(8)
  ,literal varchar2(80)
  ,ordre number
  ,pare varchar2(10)
  ,llistat number
  ,invers number
  ,mdet number
  ,mmin number
  ,mint number
  ,mmax number
  ,toShow number
  ,wiki varchar2(250)
  ,lcurt varchar2(80)
  ,pantalla varchar2(50)
);

commit;

ALTER TABLE prsodnCataleg ADD (
  CONSTRAINT prsodnCatalegPK PRIMARY KEY (dataAny,indicador,pantalla));
  
commit;

drop table prsodnCatalegPare;
create table prsodnCatalegPare (
   dataAny number  
  ,pare varchar2(10)
  ,literal varchar2(80)
  ,ordre number
  ,lcurt varchar2(80)
  ,proces varchar2(50)
);

commit;

ALTER TABLE prsodnCatalegPare ADD (
  CONSTRAINT prsodnCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;

drop table prsodnCatalegPunts;
create table prsodnCatalegPunts (
   dataAny number  
  ,indicador varchar2(8)
  ,tipus varchar2(1)
  ,punts number
  ,tipus_up varchar2(1)
);

commit;

ALTER TABLE prsodnCatalegPunts ADD (
  CONSTRAINT prsodnCatalegPuntsPK PRIMARY KEY (dataAny,indicador,tipus));
  
commit;

drop table prsodnLlistats;
create table prsodnLlistats (
  up varchar2(5)
  ,uab varchar2(5)
  ,tipus varchar2(1)
  ,indicador varchar2(8)
  ,idpac varchar2(40)
  ,sector varchar2(4)
  ,exclos int
);

commit;


ALTER TABLE prsodnLlistats ADD (
  CONSTRAINT prsodnLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,exclos,idpac));
 
commit;

drop table prsodnLlistatsData;
create table prsodnLlistatsData (
  dat date
);

commit;

drop table prsodnCatalegExclosos;
create table prsodnCatalegExclosos (
  codi int
  ,descripcio varchar2(150)
  ,ordre int
);

commit;

ALTER TABLE prsodnCatalegExclosos ADD (
  CONSTRAINT prsodnCatalegExclososPK PRIMARY KEY (codi));
  
commit;

drop table prsodnIndicadorsICS;
create table prsodnIndicadorsICS (
 dataAny number
 ,dataMes number
 ,indicador varchar2(8)
 ,resolucio number
);

commit;

ALTER TABLE prsodnIndicadorsICS ADD (
  CONSTRAINT prsodnIndicadorsICSPK PRIMARY KEY (dataAny,dataMes,indicador));
  
commit;

drop table prsodnCatalegDefinicions;
create table prsodnCatalegDefinicions (
  indicador varchar2(8)
  ,denominador varchar2(4000)
  ,numerador varchar2(4000)
  ,exclusions varchar2(4000)
  ,observacions varchar2(4000)
);

commit;

ALTER TABLE prsodnCatalegDefinicions ADD (
  CONSTRAINT prsodnCatalegDefinicionsPK PRIMARY KEY (indicador));
  
commit;

drop table prsodnIndicadorsAgregats;
create table prsodnIndicadorsAgregats (
 up varchar2(5)
 ,indicador varchar2(8)
 ,eap number
 ,sap number
 ,ambit number
 ,ics number
 ,medea number
);

commit;

ALTER TABLE prsodnIndicadorsAgregats ADD (
  CONSTRAINT prsodnIndicadorsAgregatsPK PRIMARY KEY (up,indicador));
  
commit;


drop table prsodnSinteticMetes;
create table prsodnSinteticMetes (
 dataAny number
 ,eqa varchar2(1)
 ,up varchar2(5)
 ,uab varchar2(20)
 ,tipus varchar2(1)
 ,basalAny number
 ,basalMes number
 ,basalValorOrig number
 ,mminOrig number
 ,mintOrig number
 ,mmaxOrig number
 ,dataModi date
 ,basalValor number
 ,mmin number
 ,mint number
 ,mmax number
);

commit;

ALTER TABLE prsodnSinteticMetes ADD (
  CONSTRAINT prsodnSinteticMetesPK PRIMARY KEY (dataAny,eqa,up,uab,tipus));
  
commit;
 
drop table prsodnSintetic;
create table prsodnSintetic (
 dataAny number
 ,dataMes number
 ,eqa varchar2(1)
 ,up varchar2(5)
 ,uab varchar2(20)
 ,tipus varchar2(1)
 ,noResolts number
 ,punts number
 ,puntsMax number
 ,assoliment number
);

commit;

ALTER TABLE prsodnSintetic ADD (
  CONSTRAINT prsodnSinteticPK PRIMARY KEY (dataAny,dataMes,eqa,up,uab,tipus));
  
commit;


drop table prsodnIndividualitzat;
create table prsodnIndividualitzat (
 dataAny number
 ,up varchar2(5)
 ,uab varchar2(10)
 ,tipus varchar2(1)
 ,indicador varchar2(8)
 ,punts number
);

commit;

ALTER TABLE prsodnIndividualitzat ADD (
  CONSTRAINT prsodnIndividualitzatPK PRIMARY KEY (dataAny,up,uab,tipus,indicador));
  
commit;


drop table prsodnProfessionals;
create table prsodnProfessionals (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(10)
);

commit;

ALTER TABLE prsodnProfessionals ADD (
  CONSTRAINT prsodnProfessionalsPK PRIMARY KEY (dataAny,dataMes,up, uab));
  
commit;