drop table iqfIndicadors;
create table iqfIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(20)
 ,tipus varchar2(1)
 ,indicador varchar2(32)
 ,punts_max number
 ,punt number
 ,resultat number
 ,situacio number
);

commit;

ALTER TABLE iqfIndicadors ADD (
  CONSTRAINT iqfIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));

drop table iqfLlistats;
create table iqfLlistats (
  up varchar2(5)
  ,uab varchar2(5)
  ,tipus varchar2(1)
  ,indicador varchar2(32)
  ,idpac varchar2(40)
  ,sector varchar2(4)
);

commit;

ALTER TABLE iqfLlistats ADD (
  CONSTRAINT iqfLlistatsPK PRIMARY KEY (up,uab,tipus,indicador,idpac,sector));
  
drop table iqfCataleg;
create table iqfCataleg (
  dataAny number
  ,indicador varchar2(32)
  ,literal varchar2(300)
  ,ordre number
  ,pare varchar(15) 
  ,llistat number
  ,toshow number
  ,wiki varchar2(1000)
);

commit;

ALTER TABLE iqfCataleg ADD (
  CONSTRAINT iqfCatalegPK PRIMARY KEY (dataAny,indicador));

commit;

drop table iqfLlistatsData;
create table iqfLlistatsData (
  dat date
);

commit;


drop table iqfCatalegPare;
create table iqfCatalegPare (
  dataAny number
  ,pare varchar2(10)
  ,literal varchar2(300)
  ,ordre number
);

commit;

ALTER TABLE iqfCatalegPare ADD (
  CONSTRAINT iqfCatalegParePK PRIMARY KEY (dataAny,pare));
  
commit;