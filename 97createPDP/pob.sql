drop table pobIndicadors;
create table pobIndicadors (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(5)
 ,tipus varchar2(1)
 ,indicador varchar2(10)
 ,assignada number
 ,assignada_up number
 ,atesa number
 ,atesa_up number
);

commit;

ALTER TABLE pobIndicadors ADD (
  CONSTRAINT pobIndicadorsPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));
  
commit;

drop table pobIndicadorsResum;
create table pobIndicadorsResum (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,uab varchar2(5)
 ,tipus varchar2(1)
 ,indicador varchar2(10)
 ,num number
 ,den number
 ,ind number
);

commit;

ALTER TABLE pobIndicadorsResum ADD (
  CONSTRAINT pobIndicadorsResumPK PRIMARY KEY (dataAny,dataMes,up,uab,tipus,indicador));
  
commit;

drop table PobCataleg;
create table PobCataleg (
  indicador varchar2(10)
  ,literal varchar2(300)
  ,ordre number
);

commit;

ALTER TABLE PobCataleg ADD (
  CONSTRAINT PobCatalegPK PRIMARY KEY (indicador));
  
commit;


drop table pobIndicadorsResumEAP;
create table pobIndicadorsResumEAP (
 dataAny number
 ,dataMes number
 ,up varchar2(5)
 ,indicador varchar2(10)
 ,num number
 ,den number
 ,ind number
);

commit;

ALTER TABLE pobIndicadorsResumEAP ADD (
  CONSTRAINT pobIndicadorsResumEAPPK PRIMARY KEY (dataAny,dataMes,up,indicador));
  
commit;


drop table pobIndicadorsResumICS;
create table pobIndicadorsResumICS (
 dataAny number
 ,dataMes number
 ,indicador varchar2(10)
 ,num number
 ,den number
 ,ind number
);

commit;

ALTER TABLE pobIndicadorsResumICS ADD (
  CONSTRAINT pobIndicadorsResumICSPK PRIMARY KEY (dataAny,dataMes,indicador));
  
commit;