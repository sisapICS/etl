from sisapUtils import printTime, multiprocess, getSubTables, getTableCount, getAll, getTableColumns, writeCSV, getOne
from shared import folder, dominis, first_year
import collections
from subprocess import Popen


def get_data(params):
    domini, taula, n = params
    model, db, dat, cod = dominis[domini]['model'], dominis[domini]['db'], dominis[domini]['dat'], dominis[domini]['cod']
    if model == 't':
        do = getOne('select year({}) from {} limit 1'.format(dat, taula), db)[0] >= first_year
    elif model == 's':
        do = True
    if do:
        sql = "select codi_sector, concat(date_format({0}, '%Y-%m-%d'), 'T12:00:00.000Z') from {1}, nodrizas.dextraccio where {0} between '{2}0101' and data_ext".format(dat, taula, first_year)
        data = collections.Counter()
        for row in getAll(sql, db):
            data[row] += 1
        if data:
            data_file = (id + (domini, count) for id, count in data.items())
            writeCSV('{}\\{}.txt'.format(folder, taula), data_file, sep=',')


if __name__ == '__main__':
    t = Popen('rm {}\\*'.format(folder), shell=False)
    jobs = []
    for domini, attrs in dominis.items():
        this_jobs = getSubTables(attrs['tb'], attrs['db'])
        if this_jobs:
            jobs.extend([(domini, taula, n) for (taula, n) in this_jobs.items()])
        else:
            jobs.append((domini, attrs['tb'], getTableCount(attrs['tb'], attrs['db'])))
    multiprocess(get_data, sorted(jobs, key=lambda x: x[2], reverse=True), 12)
