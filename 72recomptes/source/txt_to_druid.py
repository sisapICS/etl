from sisapUtils import sshExecuteCommand, sendSISAP
from shared import folder, folder_unix, spec
import json


with open(folder + '\\spec.json', 'wb') as f:
    json.dump(spec, f)
sshExecuteCommand('docker-compose exec druid bin/post-index-task --file {}/spec.json'.format(folder_unix), 'druid', 'x0002')
sendSISAP(['ehermosilla@idiapjgol.info', 'lmendezboo@gencat.cat'], 'Recomptes REDICS', 'all', 'Recomptes actualitzats a http://10.80.217.68/control/valid.php')
