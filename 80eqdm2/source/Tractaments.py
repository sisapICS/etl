from sisapUtils import *
import csv, os
from operator import itemgetter
from time import strftime
db= 'eqdm2'

path="./dades_noesb/eqdm2_relacio.txt"
pathi="./dades_noesb/eqdm2_indicadors.txt"

camps="a.id_cip_sec,a.up,a.edat,a.sexe,a.ates,a.institucionalitzat,a.maca"

tractaments="dm2_tractaments"
A10="eqdm2_ttA10"
C10="eqdm2_ttC10"
C02="eqdm2_ttC02"
B01="eqdm2_ttB01"
dm2="mst_diabetics"


arxiu1= tempFolder+'eqdm2_ADO.txt'
arxiu2= tempFolder+'eqdm2_Insulina.txt'
arxiu3= tempFolder+'eqdm2_Hpl.txt'
arxiu4= tempFolder+'eqdm2_aHTA.txt'
arxiu5= tempFolder+'eqdm2_aagtxt'


ADO = {}
Ins = {}
Hpl ={}
aHTA = {}
aag = {}


destins = [['ADO'],['Ins'],['Hpl'],['aHTA'],['aag']]
destitto = {'ADO':'eqdm2_tt_ADO','Ins':'eqdm2_tt_Ins','Hpl':'eqdm2_tt_Hpl','aHTA':'eqdm2_tt_aHTA','aag':'eqdm2_tt_aag'}
arxiustto = {'ADO':arxiu1,'Ins':arxiu2,'Hpl':arxiu3,'aHTA':arxiu4,'aag':arxiu5}
Agr = {'ADO':'(173,174,1001,1002,1003,175,1004)','Ins':'(412,413)','Hpl':'(82,1005,1006,1007,1008)','aHTA':'(1009,57,1010,72,56,1011)','aag':'(189,1012,5,1013,1014,379)'}
dict = {'ADO':ADO,'Ins':Ins,'Hpl':Hpl,'aHTA':aHTA,'aag':aag}

conn = connect(db)
c = conn.cursor()

try:
    os.remove(arxiu1)
except:
    pass
try:
    os.remove(arxiu2)
except:
    pass
try:
    os.remove(arxiu3)
except:
    pass
try:
    os.remove(arxiu4)
except:
    pass
try:
    os.remove(arxiu5)
except:
    pass

print 'inici ado: ' + strftime("%Y-%m-%d %H:%M:%S")

for d in destins:
   desti = d[0]
   query = "select id_cip_sec,1 from %s where agrupador in %s" % (tractaments,Agr[desti])
   c.execute(query)
   for pac in c:
      pacient=int(pac[0])
      valor=pac[1]
      try:
         if pacient in dict[desti]:  
            dict[desti][pacient]=valor + dict[desti][pacient]
         else:
            dict[desti][pacient]=valor
      except:
         pass
      
    
   with open('%s' % arxiustto[desti], 'wb') as fp:
      a = csv.writer(fp, delimiter='@')
      for key,value in dict[desti].iteritems():
         a.writerow([key,value])

   c.execute("drop table if exists %s" % destitto[desti])
   c.execute("create table %s (id_cip_sec int,recompte double)" % destitto[desti])
   c.execute("LOAD DATA LOCAL INFILE '%s' into table %s  FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (arxiustto[desti],destitto[desti]))
   c.execute("alter table %s add index(id_cip_sec,recompte)" % destitto[desti])
   
   dict[desti].clear()

print 'fi ado: ' + strftime("%Y-%m-%d %H:%M:%S")
print 'inici taules: ' + strftime("%Y-%m-%d %H:%M:%S")

c.execute("drop table if exists %s" % A10)
c.execute("create table %s (id_cip_sec int,up varchar(5), dde date, data_naix date, edat double, sexe varchar(1), ates double, institucionalitzat double, maca double, ADO double,Ins double,dieta double,valor varchar(3))" % A10)
c.execute("insert into %s select a.*,0 ADO,0 Ins, 0 dieta,'res' valor from %s a" % (A10,dm2))
c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set ADO=if(recompte>3,3,recompte)" % (A10,destitto['ADO']))
c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set Ins=1" % (A10,destitto['Ins']))
c.execute("update %s a set dieta=1 where ADO=0 and Ins=0" % (A10))
c.execute("update %s a set valor=concat(ADO,Ins,dieta)" % (A10))

c.execute("drop table if exists %s" % C10)
c.execute("create table %s (id_cip_sec int,up varchar(5), dde date, data_naix date, edat double, sexe varchar(1), ates double, institucionalitzat double, maca double, valor varchar(3))" % C10)
c.execute("insert into %s select a.*,'res' valor from %s a" % (C10,dm2))
c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set valor=if(recompte>3,3,recompte)" % (C10,destitto['Hpl']))

c.execute("drop table if exists %s" % C02)
c.execute("create table %s (id_cip_sec int,up varchar(5), dde date, data_naix date, edat double, sexe varchar(1), ates double, institucionalitzat double, maca double, valor varchar(3))" % C02)
c.execute("insert into %s select a.*,'res' valor from %s a" % (C02,dm2))
c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set valor=if(recompte>4,4,recompte)" % (C02,destitto['aHTA']))

c.execute("drop table if exists %s" % B01)
c.execute("create table %s (id_cip_sec int,up varchar(5), dde date, data_naix date, edat double, sexe varchar(1), ates double, institucionalitzat double, maca double, valor varchar(3))" % B01)
c.execute("insert into %s select a.*,'res' valor from %s a" % (B01,dm2))
c.execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set valor=if(recompte>3,3,recompte)" % (B01,destitto['aag']))

print 'fi taules: ' + strftime("%Y-%m-%d %H:%M:%S")
print 'inici indicadors: ' + strftime("%Y-%m-%d %H:%M:%S")
with open(pathi, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      i,desc,tip = cat[0],cat[1],cat[2]
      if tip=="F":
         taula="%s" % i
         c.execute("drop table if exists %s" % taula)
         c.execute("create table %s (id_cip_sec int,up varchar(5), edat double, sexe varchar(1), ates double, institucionalitzat double, maca double,ctl int )" % taula)
with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,emin,emax,sexe,agrupa,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11]
      if tipus=="F":
         taula="%s" % ind
         if vmin:
            origen="eqdm2_tt%s" % agrupa
            c.execute("insert into %s select %s,%s from %s a where valor='%s'" % (taula,camps,f,origen,vmin))
         else:
            c.execute("insert into %s select %s,%s from %s a where agrupador=%s" % (taula,camps,f,tractaments,agrupa))
            
print 'fi indicadors: ' + strftime("%Y-%m-%d %H:%M:%S")

         
c.close()
conn.close()
os.remove(arxiu1)
os.remove(arxiu2)
os.remove(arxiu3)
os.remove(arxiu4)
os.remove(arxiu5)