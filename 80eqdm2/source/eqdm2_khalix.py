from sisapUtils import getAll, createTable, listToTable, exportKhalix, readCSV
from collections import defaultdict
import sys


db = "eqdm2"
error = []
tb_o = 'exp_khalix_up_ind'
tb_d = tb_o + '_def'


centres = {up: br for (up, br) in getAll('select scs_codi, ics_codi from cat_centres', 'nodrizas')}
cuentas = defaultdict(list)
for cuenta, num, den in readCSV('dades_noesb/conversio_cuentas.csv', sep=';'):
    cuentas[num].append([cuenta, 'NUM'])
    cuentas[den].append([cuenta, 'DEN'])

createTable(tb_d, 'as select * from {} where 1 = 0'.format(tb_o), db, rm=True)
upload = []
nf = set()
for periode, up, edat, sexe, comb, indicador, conc, n in getAll('select * from {}'.format(tb_o), db):
    if indicador.upper() in cuentas:
        for cuenta, conc in cuentas[indicador.upper()]:
            upload.append([periode, centres[up], edat, sexe, comb, cuenta, conc, n])
    else:
        nf.add(indicador)
listToTable(upload, tb_d, db)

query = "select indicador, concat('A', 'periodo'), up, conc, edat, comb, sexe, 'N', n from {}.{}".format(db, tb_d)
file = 'EQDM2_NOU'
error.append(exportKhalix(query, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
