from sisapUtils import *
import csv, os
from operator import itemgetter
from time import strftime

db= 'eqdm2'

path="./dades_noesb/eqdm2_relacio.txt"
pathi="./dades_noesb/eqdm2_indicadors.txt"

camps="a.id_cip_sec,a.up,a.edat,a.sexe,a.ates,a.institucionalitzat,a.maca"

pob="mst_diabetics"
problemes="dm2_problemes"
variables="dm2_variables"
PrevCriteris= "ps in ('1','7','431')"

arxiu1= tempFolder+'eqdm2_prev1.txt'
arxiu2= tempFolder+'eqdm2_prev2.txt'
arxiu3= tempFolder+'eqdm2_prev1bc.txt'
arxiu4= tempFolder+'eqdm2_prev2bc.txt'


destins = [['Prev1'],['Prev2']]
arxius = {'Prev1':arxiu1,'Prev2':arxiu2}

prev1 = {}
prev2 = {}
prev1bc = {}
prev2bc = {}

controls = [['HbA1c','20'],['TAS','16'],['TAD','17'],['LDL','9']]
bonControl = {'Prev1_HbA1c':'between 0 and 7','Prev2_HbA1c':'between 0 and 7','Prev1_TAS':'between 75 and 130','Prev2_TAS':'between 75 and 130','Prev1_TAD':'between 45 and 80','Prev2_TAD':'between 45 and 80','Prev1_LDL':'between 20 and 129.99','Prev2_LDL':'between 20 and 99.99'}
filebc = {'Prev1':prev1bc,'Prev2':prev2bc}
arxiusbc = {'Prev1':arxiu3,'Prev2':arxiu4}
destibc = {'Prev1':'Prev1bc','Prev2':'Prev2bc'}

conn=connect(db)
c = conn.cursor()

try:
    os.remove(arxiu1)
except:
    pass
try:
    os.remove(arxiu)
except:
    pass
try:
    os.remove(arxiu3)
except:
    pass
try:
    os.remove(arxiu4)
except:
    pass



print 'inici problemes: ' + strftime("%Y-%m-%d %H:%M:%S")
pato= {}
query = "select id_cip_sec from %s where %s" % (problemes,PrevCriteris)
c.execute(query)
for pac in c:
   pacient=int(pac[0])
   pato[pacient]=pacient
print 'fi problemes: ' + strftime("%Y-%m-%d %H:%M:%S")


print 'inici prevencio: ' + strftime("%Y-%m-%d %H:%M:%S")
query = "select id_cip_sec from %s" % pob
c.execute(query)
for pac in c:
   pacient=int(pac[0])
   try:
      pato[pacient]
      prev2[pacient]=pacient
   except:
      prev1[pacient]=pacient
      
with open('%s' % arxiu1, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   for key,value in prev1.iteritems():
      a.writerow([value])
      
with open('%s' % arxiu2, 'wb') as fp:
   a = csv.writer(fp, delimiter='@')
   for key,value in prev2.iteritems():
      a.writerow([value])
print 'fi prevencio: ' + strftime("%Y-%m-%d %H:%M:%S")

print 'inici control: ' + strftime("%Y-%m-%d %H:%M:%S")

for d in destins:
   desti = d[0]
   c.execute("drop table if exists %s" % desti)
   c.execute("create table %s (id_cip_sec int)" % desti)
   c.execute("LOAD DATA LOCAL INFILE '%s' into table %s  FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (arxius[desti],desti))
   c.execute("alter table %s add unique (id_cip_sec)" % desti)
   cHbA1c = {}
   cTAS = {}
   cTAD = {}
   cLDL = {} 
   for l in controls:
      crit,agr = l[0],l[1]
      valor = "%s_%s" % (desti,crit)
      query = "select id_cip_sec from %s where agrupador=%s and valor %s" % (variables,agr,bonControl[valor])
      c.execute(query)
      for pac in c:
         pacient=int(pac[0])
         if crit=="HbA1c":
            cHbA1c[pacient]=pacient
         elif crit=="TAS":
            cTAS[pacient]=pacient
         elif crit=="TAD":
            cTAD[pacient]=pacient
         elif crit=="LDL":
            cLDL[pacient]=pacient
         else:
            ko=1
   query= "select id_cip_sec from %s" % desti
   c.execute(query)
   for pac in c:
      pacient=int(pac[0])  
      try:
         cHbA1c[pacient]
         cTAS[pacient]
         cTAD[pacient]
         cLDL[pacient]
         if desti=="Prev1":
            prev1bc[pacient]=pacient
         else:
            prev2bc[pacient]=pacient
      except:
         pass
   with open('%s' % arxiusbc[desti], 'wb') as fp:
      a = csv.writer(fp, delimiter='@')
      for key,value in filebc[desti].iteritems():
         a.writerow([value])
   c.execute("drop table if exists %s" % destibc[desti])
   c.execute("create table %s (id_cip_sec int)" % destibc[desti])
   c.execute("LOAD DATA LOCAL INFILE '%s' into table %s  FIELDS TERMINATED BY '@' LINES TERMINATED BY '\r\n'" % (arxiusbc[desti],destibc[desti]))
   c.execute("alter table %s add unique (id_cip_sec)" % destibc[desti])      
print 'fi control: ' + strftime("%Y-%m-%d %H:%M:%S")      

print 'inici indicadors: ' + strftime("%Y-%m-%d %H:%M:%S")
with open(pathi, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      i,desc,tip = cat[0],cat[1],cat[2]
      if tip=="P":
         taula="%s" % i
         c.execute("drop table if exists %s" % taula)
         c.execute("create table %s (id_cip_sec int,up varchar(5), edat double, sexe varchar(1), ates double, institucionalitzat double, maca double,ctl int )" % taula)
with open(path, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11]
      if tipus=="P":
         taula="%s" % ind
         c.execute("insert into %s select %s,%s from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" % (taula,camps,f,pob,agr))
         
print 'fi indicadors: ' + strftime("%Y-%m-%d %H:%M:%S")
         
c.close()
conn.close()
os.remove(arxiu1)
os.remove(arxiu2)
os.remove(arxiu3)
os.remove(arxiu4)