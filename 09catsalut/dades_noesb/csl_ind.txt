14@ICMBDAP01@Percentatge de visites amb almenys un motiu de consulta informat@CSLA01@Indicadors Catsalut AP@@@0@@@0@@0
15@IT14@Percentatge de pacients amb diagn�stic de depressi�@CSLA01@Indicadors Catsalut AP@@@0@@@0@@0
1@IT06@Cribratge de factors de risc de sdme metab�lica en psicosi@CSLA01@Indicadors Catsalut AP@@@0@11@@0@IAP15@0
2@IT15@Pacients amb depressi� i avaluaci� risc suicidi@CSLA01@Indicadors Catsalut AP@@@0@@@0@@0
3@IAP01@Poblaci� de 5 anys correctament vacunats amb la TV@CSLA01@Indicadors Catsalut AP@@@1@4@@1@IAP01@1
4@IAP03A@Control de la pressi� arterial i diabetis@CSLA01@Indicadors Catsalut AP@@@0@@@0@@0
5@IAP03B@Control de la pressi� arterial i diabetis (DM)@CSLA01@Indicadors Catsalut AP@@@0@@@0@@0
6@IAP03C@Control de la pressi� arterial i diabetis (HTA)@CSLA01@Indicadors Catsalut AP@@@0@@@0@@0
8@IAP06@Pacients de 15 a 69 anys que realitzen activitats f�siques saludables@CSLA01@Indicadors Catsalut AP@@@0@@@0@@0
9@IASSIR01@Embarassades alt risc amb 4 visites a ASSIR@CSLA02@Indicadors Catsalut ASSIR@@@0@@@0@@0
10@IASSIR02@Embarassades que han deixat de fumar durant embar�s@CSLA02@Indicadors Catsalut ASSIR@@@0@@@0@@0
11@IASSIR04@Embarassades amb una primera visita abans de la desena setmana embar�s@CSLA02@Indicadors Catsalut ASSIR@@@0@@@0@@0
12@IASSIR05@Atenci� puerperal durant els primers 15 dies posteriors al part@CSLA02@Indicadors Catsalut ASSIR@@@0@@@0@@0
13@IASSIR03@Cobertura del cribratge adequat de c�ncer de coll uter� en dones de 25 a 65 anys@CSLA02@Indicadors Catsalut ASSIR@@@0@@@0@@0
16@IAP09@Control �ptim de la hipertensi� arterial@CSLA01@Indicadors Catsalut AP@@@0@@@0@@0
17@IAP08@Realitzar un determinat percentatge m�nim de visites no presencials@CSLA01@Indicadors Catsalut AP@@@0@12@@0@IAP08@0
19@IT23@Pacients entre 40 i 80 anys amb diagn�stic de MPOC amb espirometria publicada en l'HC3@CSLA01@Indicadors Catsalut AP@@@0@1@@0@IT23@0
20@IASSIR07@Cribratge del consum d'alcohol en dones embarassades@CSLA02@Indicadors Catsalut ASSIR@@@0@@@0@@0
21@IASSIR06@Embarassades vacunades de dTpa@CSLA02@Indicadors Catsalut ASSIR@@@0@@@0@@0
22@CMBDAP02@CMBD-AP dels pacients residents a altres CCAA amb dades d'identificaci� incorrectes@CSLA03@Indicadors Catsalut AP@@@1@9@@0@CMBDAP02@1
23@IASSIR13@Tancament del proc�s en IVE farmacol�gic@CSLA02@Indicadors Catsalut ASSIR@@@0@@@0@@0
24@ICAM01AP@Durada dels episodis de IT@CSLA04@Indicadors Catsalut AP@@@1@8@@1@ICAM01-AP@1
25@LMS01AP@Utilitzaci� (sol�licituds) de La Meva Salut (LMS)@CSLA05@Indicadors Catsalut AP@@@0@10@@1@LMS01-AP@0
26@T02BISA@Hospitalitzacions potencialment evitables relacionades amb ICC@CSL06@Indicadors Catsalut AP@@@0@2@@0@@0
27@T02BISB@Hospitalitzacions potencialment evitables relacionades amb MPOC@CSL06@Indicadors Catsalut AP@@@0@3@@0@@0
28@IASSIR12@Embarassades que han deixat de fumar durant embar�s que continuen abstinents en la visita de postpart@CSLA02@Indicadors Catsalut ASSIR@@@0@@@0@@0
29@IAP25A@Pacients institucionalitzats inclosos en ATDOM@CSL06@Indicadors Catsalut AP@@@0@4@@0@@0
30@IAP25B@Pacients institucionalitzats (assistides) inclosos en ATDOM@CSL06@Indicadors Catsalut AP@@@1@5@@1@IAP25B@0
31@IAP24@Pacients ATDOM amb criteris m�nims de qualitat@CSL06@Indicadors Catsalut AP@@@0@6@@0@@0
32@IGFM04AP@Pacients amb combinaci� de benzodiazepines i opioides forts@CSLA01@Indicadors Catsalut AP@@@1@11@@1@IGFM04AP@1
32@IGFM05AP@Pacients amb combinaci� de pregabalina o gabapentina i opioides forts@CSLA01@Indicadors Catsalut AP@@@1@12@@1@IGFM05AP@1
33@IAP11BIS@Pacients que han rebut consell antitabac@CSLA01@Indicadors Catsalut AP@@@1@13@@1@IAP11BIS@0
34@IAP18@Percentatge de poblaci� assignada de 65 anys o m�s correctament vacunada contra la grip estacional@CSLA01@Indicadors Catsalut AP@@@1@14@@1@IAP18@1
35@AP35@Avaluaci� de risc de su�cidi@CSLA01@Indicadors Catsalut AP@@@1@@@1@AP35@0
36@SGAM02AP@IT amb durada igual o inferior a l'�ptima@CSLA03@Indicadors Catsalut AP@@@1@9@@0@SGAM02AP@1
37@ESIAP0401@Cribratge del consum d'alcohol entre 15 i 60 anys@CSLA03@Indicadors Catsalut AP@@@1@9@@1@ESIAP0401@1
38@IESIAP0408@Intervencions en fumadors@CSLA03@Indicadors Catsalut AP@@@0@9@@1@IESIAP0408@1
