# -*- coding: utf-8 -*-

import sisapUtils as u

if u.IS_MENSUAL:
  sql = """
          SELECT * FROM catsalut.IMP_FASE1
          UNION
          SELECT * FROM catsalut.IMP_FASE2
          UNION
          SELECT * FROM catsalut.IMP_INF_VAL
        """
  file = "IMPRE"
  u.exportKhalix(sql,file)

  sql = """
          SELECT indicador, periode, concat(up, tipus, uba), analisi, nocat, noimp, dim6set, n, valor  FROM catsalut.IMP_UBA_FASE1
          UNION
          SELECT indicador, periode, concat(up, tipus, uba), analisi, nocat, noimp, dim6set, n, valor  FROM catsalut.IMP_UBA_FASE2
          UNION
          SELECT indicador, periode, concat(up, tipus, uba), analisi, nocat, noimp, dim6set, n, valor  FROM catsalut.IMP_UBA_INF_VAL
        """
  file = "IMPRE_UBA"
  u.exportKhalix(sql,file)

