from sisapUtils import *
from collections import defaultdict
import csv, os

debug = False

db="catsalut"
nod="nodrizas"
ped="pedia"

path0 = os.path.realpath("../")
path = path0.replace("\\","/") + "/09catsalut/dades_noesb/csl_relacio_main.txt" 
pathe = path0.replace("\\","/") + "/09catsalut/dades_noesb/csl_edats_main.txt"
path30 = path0.replace("\\","/") + "/09catsalut/dades_noesb/csl_IMC30infants.txt"

OutFile= tempFolder + 'csl_main.txt'

crit="eqa_criteris"
critped="ped_criteris_var"
dext="dextraccio"

tableMain="mst_csl_main"
sql= 'drop table if exists {}'.format(tableMain)
execute(sql,db)
sql= "create table {} (up varchar(5) not null default'',ates double,indicador varchar(20) not null default'',valor double)".format(tableMain)
execute(sql,db)
sql="alter table {} add primary key(up,ates,indicador)".format(tableMain)
execute(sql,db)

indicadors,edats= {},{}
with open(pathe, 'rb') as file:
    p=csv.reader(file, delimiter='@', quotechar='|')
    for ind in p:
        i,i2,s,emin,emax = ind[0],ind[1],ind[2],ind[3],ind[4]
        indicadors[i]=True
        edats[(i,s)]={'subi':i2,'emin':int(emin),'emax':int(emax)}


sql='select data_ext from {}'.format(dext)
for d in getOne(sql, nod):
    dara=d
    
i30ped={}
with open(path30, 'rb') as file:
    p=csv.reader(file, delimiter=';', quotechar='|')
    for ind in p:
        s,mesos,val=ind[0],ind[1],ind[2]
        mesos=int(mesos)
        i30ped[(s,mesos)]={'valimc': val}

#FEM ULTIM IMC PQ PED_VARIABLES NO HO CONTEMPLA
maxDatIMC={}
sql="select id_cip_sec,val,dat,edat_m from ped_variables where agrupador=19"
for id_cip_sec,val,dat,edat in getAll(sql,nod):
    id_cip_sec=int(id_cip_sec)
    edat=int(edat)
    if id_cip_sec in maxDatIMC:
        if dat>maxDatIMC[id_cip_sec]['data']:
            maxDatIMC[id_cip_sec]={'data':dat,'valor':val,'edat_m':edat}
        else:
            ok=1
    else:
        maxDatIMC[id_cip_sec]={'data':dat,'valor':val,'edat_m':edat}

printTime('inici pob') 
            
def getPacients():

    assig= {}
    sql= 'select id_cip_sec,up,edat,sexe,ates from assignada_tot_with_jail'
    for id_cip_sec,up,edat,sex,ates in getAll(sql,nod):
         assig[int(id_cip_sec)]= {'up':up,'edat':int(edat),'sex':sex,'ates':int(ates)}
    return assig 
    
ultvar = {1:"select id_cip_sec,data_var,valor from eqa_variables where agrupador={0} and usar=1 {1}",0:"select id_cip_sec,data_var,valor from eqa_variables where agrupador={0} {1}"}

ass = getPacients()

printTime('inici calcul') 

consell=488

conspafes={}
sql="select id_cip_sec, valor from eqa_variables where agrupador='{0}' {1}".format(consell,' limit 10' if debug else '')
for id_cip_sec,valor in getAll(sql,nod):
    id_cip_sec=int(id_cip_sec)
    valor=int(valor)
    if 1<=valor<=6:
        if id_cip_sec in conspafes:
            if valor > conspafes[id_cip_sec]['v']:
                conspafes[id_cip_sec]['v']=valor
        else:
            conspafes[id_cip_sec]={'v':valor}

     
for ind in indicadors:
    csl_ind ={}
    nmax=0    
    with open(path, 'rb') as file:
        p=csv.reader(file, delimiter='@', quotechar='|')
        for indi in p:
            i,n,agr,dmin,dmax,vmin,vmax,vult = indi[0],indi[1],indi[2],indi[3],indi[4],indi[5],indi[6],indi[7]
            if i==ind:
                n=int(n)
                if nmax<n:
                    nmax=n
                try:
                    dmin=int(dmin)
                    dmax=int(dmax)
                    vult=int(vult)
                except KeyError:
                    ok=1  
                except ValueError:
                    ok=1
                if vult==1:
                    ok=0
                else:
                    vult=0           
                if agr=='TABAC':
                    sql="select id_cip_sec,tab, dalta,dbaixa,dlast, last from eqa_tabac {}".format(' limit 10' if debug else '')
                    for id_cip_sec,tab,dalta,dbaixa,dlast,last in getAll(sql,nod):
                        id_cip_sec=int(id_cip_sec)
                        if int(tab)==int(vmin):
                            Calcular=False
                            if vult==1:
                                if int(last)==1:
                                    Calcular=True
                            else:
                                b=monthsBetween(dalta,dara)  
                                if dmax<=b<=dmin:
                                    Calcular=True
                            if Calcular:
                                if id_cip_sec in csl_ind:
                                    if csl_ind[id_cip_sec]['n']==n:
                                        ok=1
                                    else:   
                                        csl_ind[id_cip_sec]['num']+=1
                                        csl_ind[id_cip_sec]['n']=n
                                else:
                                    try:
                                        csl_ind[id_cip_sec]={'up':ass[id_cip_sec]['up'],'edat':ass[id_cip_sec]['edat'],'sex':ass[id_cip_sec]['sex'],'ates':ass[id_cip_sec]['ates'],'n':n,'num':1}
                                    except KeyError:
                                        ok=1
                elif agr=='VACSIST':
                    if vmin:
                        sql="select id_cip_sec,{0} from mst_vacsist {1}".format(vmin,' limit 10' if debug else '')
                    else:
                        sql="select id_cip_sec,num from mst_vacsist {}".format(' limit 10' if debug else '')
                    for id_cip_sec,num in getAll(sql,ped):
                        id_cip_sec=int(id_cip_sec)
                        num=int(num)
                        if num==1:
                            if id_cip_sec in csl_ind:
                                if csl_ind[id_cip_sec]['n']==n:
                                    ok=1
                                else:   
                                    csl_ind[id_cip_sec]['num']+=1
                                    csl_ind[id_cip_sec]['n']=n
                            else:
                                try:
                                    csl_ind[id_cip_sec]={'up':ass[id_cip_sec]['up'],'edat':ass[id_cip_sec]['edat'],'sex':ass[id_cip_sec]['sex'],'ates':ass[id_cip_sec]['ates'],'n':n,'num':1}
                                except KeyError:
                                    ok=1
                elif agr=='RES':
                    sql="select id_cip_sec from assignada_tot_with_jail where institucionalitzat=1  {}".format(' limit 10' if debug else '')
                    for id_cip_sec, in getAll(sql,nod):
                        id_cip_sec=int(id_cip_sec)
                        if id_cip_sec in csl_ind:
                            if csl_ind[id_cip_sec]['n']==n:
                                ok=1
                            else:   
                                csl_ind[id_cip_sec]['num']+=1
                                csl_ind[id_cip_sec]['n']=n
                        else:
                            try:
                                csl_ind[id_cip_sec]={'up':ass[id_cip_sec]['up'],'edat':ass[id_cip_sec]['edat'],'sex':ass[id_cip_sec]['sex'],'ates':ass[id_cip_sec]['ates'],'n':n,'num':1}
                            except KeyError:
                                ok=1
                elif agr=='PCC':
                    sql="select id_cip_sec from assignada_tot_with_jail where pcc=1  {}".format(' limit 10' if debug else '')
                    for id_cip_sec, in getAll(sql,nod):
                        id_cip_sec=int(id_cip_sec)
                        if id_cip_sec in csl_ind:
                            if csl_ind[id_cip_sec]['n']==n:
                                ok=1
                            else:   
                                csl_ind[id_cip_sec]['num']+=1
                                csl_ind[id_cip_sec]['n']=n
                        else:
                            try:
                                csl_ind[id_cip_sec]={'up':ass[id_cip_sec]['up'],'edat':ass[id_cip_sec]['edat'],'sex':ass[id_cip_sec]['sex'],'ates':ass[id_cip_sec]['ates'],'n':n,'num':1}
                            except KeyError:
                                ok=1
                elif agr=='CONSELL':
                    sql="select id_cip_sec from eqa_variables where agrupador='{0}' {1}".format(consell,' limit 10' if debug else '')
                    for id_cip_sec, in getAll(sql,nod):
                        id_cip_sec=int(id_cip_sec)
                        try:
                            cons=conspafes[id_cip_sec]['v']
                            cons=int(cons)
                        except KeyError:
                            continue
                        if int(vmin) <= cons <= int(vmax):
                            if id_cip_sec in csl_ind:
                                if csl_ind[id_cip_sec]['n']==n:
                                    ok=1
                                else:   
                                    csl_ind[id_cip_sec]['num']+=1
                                    csl_ind[id_cip_sec]['n']=n
                            else:
                                try:
                                    csl_ind[id_cip_sec]={'up':ass[id_cip_sec]['up'],'edat':ass[id_cip_sec]['edat'],'sex':ass[id_cip_sec]['sex'],'ates':ass[id_cip_sec]['ates'],'n':n,'num':1}
                                except KeyError:
                                    ok=1
                elif agr=='IMC30':
                    for id_cip_sec,d in maxDatIMC.items():
                        dat=d['data']
                        val=d['valor']
                        edat_m=d['edat_m']
                        b=monthsBetween(dat,dara)   
                        if dmax<b<=dmin:
                            Calcular=False
                            try:
                                sexeimc=ass[id_cip_sec]['sex']
                                vali30ped=i30ped[(sexeimc,edat_m)]['valimc']
                            except KeyError:
                                continue
                            if float(vali30ped)<float(val)<=float(vmax):
                                Calcular=True
                            if Calcular:
                                if id_cip_sec in csl_ind:
                                    if csl_ind[id_cip_sec]['n']==n:
                                        ok=1
                                    else:
                                        csl_ind[id_cip_sec]['num']+=1
                                        csl_ind[id_cip_sec]['n']=n
                                else:
                                    try:
                                        csl_ind[id_cip_sec]={'up':ass[id_cip_sec]['up'],'edat':ass[id_cip_sec]['edat'],'sex':ass[id_cip_sec]['sex'],'ates':ass[id_cip_sec]['ates'],'n':n,'num':1}
                                    except KeyError:
                                        ok=1     
                elif agr=='45':
                    sql="select id_cip_sec from csl_visites_atdom"
                    for id_cip_sec, in getAll(sql,nod):
                        id_cip_sec=int(id_cip_sec)
                        if id_cip_sec in csl_ind:
                            if csl_ind[id_cip_sec]['n']==n:
                                ok=1
                            else:   
                                csl_ind[id_cip_sec]['num']+=1
                                csl_ind[id_cip_sec]['n']=n
                        else:
                            try:
                                csl_ind[id_cip_sec]={'up':ass[id_cip_sec]['up'],'edat':ass[id_cip_sec]['edat'],'sex':ass[id_cip_sec]['sex'],'ates':ass[id_cip_sec]['ates'],'n':n,'num':1}
                            except KeyError:
                                ok=1       
                else:
                    agr=int(agr)
                    sql="select distinct if(taula='actuacions','variables',if(taula='laboratori','variables',if(taula='activitats','variables',taula))), agrupador,'A' from {0} where agrupador={1} \
                        union  select distinct if(taula='actuacions','variables',if(taula='laboratori','variables',if(taula='activitats','variables',taula))), agrupador,'P' from {2} where agrupador={1} ".format(crit,agr,critped)
                    for t,a,ap in getAll(sql,nod):
                        if t=="problemes":
                            sql="select id_cip_sec,dde from eqa_problemes,{0} where ps={1} {2}".format(dext,agr,' limit 10' if debug else '')
                            for id_cip_sec,dat in getAll(sql,nod):
                                id_cip_sec=int(id_cip_sec)
                                b=monthsBetween(dat,dara)   
                                if dmax<=b<=dmin:
                                    if id_cip_sec in csl_ind:
                                        if csl_ind[id_cip_sec]['n']==n:
                                            ok=1
                                        else:   
                                            csl_ind[id_cip_sec]['num']+=1
                                            csl_ind[id_cip_sec]['n']=n
                                    else:
                                        try:
                                            csl_ind[id_cip_sec]={'up':ass[id_cip_sec]['up'],'edat':ass[id_cip_sec]['edat'],'sex':ass[id_cip_sec]['sex'],'ates':ass[id_cip_sec]['ates'],'n':n,'num':1}
                                        except KeyError:
                                            ok=1
                        elif t=="variables":
                            if ap=='A':
                                sql=ultvar[vult].format(agr,' limit 10' if debug else '')
                            elif ap=='P':
                                sql="select id_cip_sec,dat,val from ped_variables where agrupador={0} {1}".format(agr,' limit 10' if debug else '')
                            #sql="select id_cip_sec,data_var,valor,usar from eqa_variables where agrupador={0} {1}".format(agr,' limit 10' if debug else '')
                            for id_cip_sec,dat,val in getAll(sql,nod):
                                b=monthsBetween(dat,dara)
                                if dmax<=b<=dmin:
                                    if vmin and vmax:
                                        Calcular=False
                                        if float(vmin)<=float(val)<=float(vmax):
                                            Calcular=True
                                    else:
                                        Calcular=True
                                    if Calcular:
                                        if id_cip_sec in csl_ind:
                                            if csl_ind[id_cip_sec]['n']==n:
                                                ok=1
                                            else:
                                                csl_ind[id_cip_sec]['num']+=1
                                                csl_ind[id_cip_sec]['n']=n
                                        else:
                                            try:
                                                csl_ind[id_cip_sec]={'up':ass[id_cip_sec]['up'],'edat':ass[id_cip_sec]['edat'],'sex':ass[id_cip_sec]['sex'],'ates':ass[id_cip_sec]['ates'],'n':n,'num':1}
                                            except KeyError:
                                                ok=1
                        elif t=="vacunes":
                            sql="select id_cip_sec,datamax,dosis from eqa_vacunes where agrupador={0} {1}".format(agr,' limit 10' if debug else '')
                            for id_cip_sec,dat,dosi in getAll(sql,nod):
                                if id_cip_sec in csl_ind:
                                    if csl_ind[id_cip_sec]['n']==n:         
                                        ok=1
                                    else:
                                        csl_ind[id_cip_sec]['num']+=1
                                        csl_ind[id_cip_sec]['n']=n
                                else:
                                    try:
                                        csl_ind[id_cip_sec]={'up':ass[id_cip_sec]['up'],'edat':ass[id_cip_sec]['edat'],'sex':ass[id_cip_sec]['sex'],'ates':ass[id_cip_sec]['ates'],'n':n,'num':1}
                                    except KeyError:
                                        ok=1
    csl={}
    for (id),d in csl_ind.items():   
        if csl_ind[id]['num']>=nmax and (ind,csl_ind[id]['sex']) in edats and (edats[(ind,csl_ind[id]['sex'])]['emin'] <=csl_ind[id]['edat'] <= edats[(ind,csl_ind[id]['sex'])]['emax']):
            if (edats[(ind,csl_ind[id]['sex'])]['subi'],csl_ind[id]['up'],csl_ind[id]['ates']) in csl:
                csl[edats[(ind,csl_ind[id]['sex'])]['subi'],csl_ind[id]['up'],csl_ind[id]['ates']]['num']+=1
            else:
                csl[edats[(ind,csl_ind[id]['sex'])]['subi'],csl_ind[id]['up'],csl_ind[id]['ates']]={'num':1}
    with open(OutFile,'wb') as file:
        w= csv.writer(file, delimiter='@', quotechar='|')
        for (si,id,a),d in csl.items():
            try:
                w.writerow([id,a,si,d['num']])
            except KeyError:
                continue
    table="csl_%s" % ind
    sql= 'drop table if exists {}'.format(table)
    execute(sql,db)
    sql= "create table {} (up varchar(5) not null default'',ates double,indicador varchar(20) not null default'',valor double)".format(table)
    execute(sql,db)
    loadData(OutFile,table,db)
    sql="insert ignore into {} select * from {}".format(tableMain,table)
    execute(sql,db)
    csl_ind.clear()
    csl.clear()
 
printTime('fi calcul') 