from sisapUtils import getAll, execute, listToTable, ageConverter, sexConverter
from collections import Counter


tb_pac = 'mst_indicadors_pacient'
tb_klx = 'exp_khalix_up_ind'
db = 'catsalut'
ind = 'ICAM01AP'
grups = ('05', '13', '17')


ps = {cod: grup for (cod, grup) in getAll("select b.eq_cim10, a.dg_ggd from cat_prstb601 a inner join cat_prstb602 b on a.dg_cod = b.eq_cim9 where a.dg_tcd = 'CIM9MC' and a.dg_tdg = 'D'", 'import')}

pob = {id: (ind, up, uba, ubainf, edat, sexe, ates, instit, maca) for (id, up, uba, ubainf, edat, sexe, ates, instit, maca) in getAll(
      "select id_cip_sec, up, uba, ubainf, edat, sexe, ates, institucionalitzat, maca from assignada_tot_with_jail where edat > 14", 'nodrizas')}

dades = {}
sql = "select id_cip_sec, ilt_prob_salut, datediff(ilt_data_alta, ilt_data_baixa), ilt_durada_opt from baixes, nodrizas.dextraccio \
       where ilt_data_alta between date_add(date_add(data_ext, interval -1 year), interval +1 day) and data_ext and ilt_cau_codi_baixa in ('MC', 'ANL')"
for id, cim, dur, opt in getAll(sql, 'import'):
    if id in pob and opt:
        grup = ps[cim] if cim in ps and ps[cim] else '99'
        compleix = dur <= opt
        key = (id, grup)
        if key not in dades:
            dades[key] = [0, 0]
        dades[key][1] += 1
        if compleix:
            dades[key][0] += 1

pacients = ((id,) + pob[id] + (num, den, 0) for (id, grup), (num, den) in dades.items() if grup in grups)
# execute("delete a.* from {} a where indicador = ''".format(tb_pac, ind), db)
listToTable(pacients, tb_pac, db)

khalix = Counter()
for (id, grup), (num, den) in dades.items():
    indicador = '{}{}'.format(ind, grup)
    _ind, up, uba, ubainf, edat, sexe, ates, instit, maca = pob[id]
    edat, sexe, ates, instit = ageConverter(edat, 5), sexConverter(sexe), bool(ates), 'NOINS' if instit == 0 else 'INS'
    khalix[(up, edat, sexe, '{}ASS'.format(instit), indicador, 'NUM')] += num
    khalix[(up, edat, sexe, '{}ASS'.format(instit), indicador, 'DEN')] += den
    if ates:
        khalix[(up, edat, sexe, '{}AT'.format(instit), indicador, 'NUM')] += num
        khalix[(up, edat, sexe, '{}AT'.format(instit), indicador, 'DEN')] += den

# execute("delete a.* from {} a where indicador like '{}%'".format(tb_klx, ind), db)
listToTable((key + (n,) for (key, n) in khalix.items()), tb_klx, db)
