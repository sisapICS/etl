from sisapUtils import *
import sys
import collections as c

IS_TEST = 0


def Dext():
    sql = "select month(data_ext), year(data_ext) from nodrizas.dextraccio d "
    for mes, year in getAll(sql, 'nodrizas'):
        print(mes, str(year)[2:4])
        if len(str(mes)) == 1:
            return '0'+str(mes), str(year)[2:4]
        else: return str(mes), str(year)[2:4]

class INDICADORS():
    def __init__(self, mes, any):
        self.periode = 'A' + any + mes
        self.mes = mes
        self.any = '20'+ any
        self.CreateTable()
        self.UP_ICS()
        self.EQAS()
        self.FARMA()
        self.ALERTES()
        if IS_TEST:
            self.to_Khalix()

    def CreateTable(self):
        cols = '(indicador varchar(10), periode varchar(5), up varchar(5), analisi varchar(3),\
                nocat varchar(5), noimp varchar(5), dim6set varchar(7), n varchar(1), valor int)'
        createTable('IMP_FASE1', cols, 'catsalut', rm=True)
    
    def UP_ICS(self):
        self.ups = {}
        sql = """select scs_codi, ics_codi from nodrizas.cat_centres"""
        for up, ics in getAll(sql, 'nodrizas'):
            self.ups[up] = ics

    def EQAS(self):
        eqas = [('EQA0222A', 'IMP001'),
                ('EQA0228A', 'IMP002'),
                ('EQA0226%', 'IMP003'),
                ('EQA0230B', 'IMP004'),
                ('EQA0216A', 'IMP005'),
                ('EQA0238A', 'IMP006'),
                ]
        for e in eqas:
            upload = []
            sql = """select up, conc, sum(n) from eqa_ind.exp_khalix_up_ind ekui
                    where indicador like '{}' and comb = 'NOINSAT'
                    group by up, conc""".format(e[0])
            print(sql)
            for up, conc, n in getAll(sql, 'eqa_ind'):
                if up in self.ups:
                     upload.append((e[1], self.periode, self.ups[up], conc, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', n))
            listToTable(upload, 'IMP_FASE1', 'catsalut')

        

    def FARMA(self):
        translate = {
            'IF299P': 'IMP016',
            'FARM0007': 'IMP017',
            'FARM0008': 'IMP018',
            'FARM0009': 'IMP019',
            'FARM0005': 'IMP012',
            'FARM0006': 'IMP013',
            'IF292P': 'IMP020',
            'IF301P': 'IMP010',
            'IF280P': 'IMP021',
            'IF269P': 'IMP011',
            'IF249P': 'IMP007',
            'IF254P': 'IMP008',
            'IF254PA': 'IMP008A',
            'IF254PB': 'IMP008B',
            'IF263P': 'IMP009'
        }
        upload = []
        sql = """SELECT k0, k1, k2, SUM(v) 
                    FROM altres.exp_khalix_eqpf 
                    WHERE k0 IN {}
                    and k4 = 'NOINSAT' and length(k1) = 5
                    GROUP BY k0, k1, k2""".format(tuple(translate.keys()))
        print(sql)
        for indi, up, tipu, val in getAll(sql, 'altres'):
            if tipu == 'DEN':
                upload.append((translate[str(indi)], self.periode, up, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', val))
            elif tipu == 'NUM':
                upload.append((translate[str(indi)], self.periode, up, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', val))
        listToTable(upload, 'IMP_FASE1', 'catsalut')

        upload = []
        sql = """select indicador, up, sum(numerador), sum(denominador) 
                    from altres.exp_ecap_alertes_uba eeau 
                    where indicador in {}
                    and tipus = 'M'
                    group by indicador, up""".format(tuple(translate.keys()))
        for indi, up, num, den in getAll(sql, 'altres'):
            upload.append((translate[str(indi)], self.periode, self.ups[up], 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', den))
            upload.append((translate[str(indi)], self.periode, self.ups[up], 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', num))
        listToTable(upload, 'IMP_FASE1', 'catsalut')

    def ALERTES(self):
        translate = {'IGFM04AP': 'IMP014', 'IGFM05AP': 'IMP015'}
        upload = []
        sql = """select indicador, up, conc, sum(n) from catsalut.exp_khalix_up_ind_def
            where indicador in ('IGFM04AP','IGFM05AP')
            group by indicador, up, conc"""
        print(sql)
        for indi, up, con, val in getAll(sql, 'catsalut'):
            upload.append((translate[str(indi)], self.periode, self.ups[up], con, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', val))
        listToTable(upload, 'IMP_FASE1', 'catsalut')

    def to_Khalix(self):
        sql = "select * from catsalut.IMP_FASE1"
        file = "IMPRE"
        exportKhalix(sql,file)

class INDICADORS_UBA():
    def __init__(self, mes, any):
        self.periode = 'A' + any + mes
        self.mes = mes
        self.any = '20'+ any
        self.CreateTable()
        self.UP_ICS()
        self.EQAS()
        self.FARMA()
        self.ALERTES()
        if IS_TEST:
            self.to_Khalix()

    def CreateTable(self):
        cols = '(indicador varchar(10), periode varchar(5), up varchar(5), uba varchar(7), tipus varchar(1), analisi varchar(3),\
                nocat varchar(5), noimp varchar(5), dim6set varchar(7), n varchar(1), valor int)'
        createTable('IMP_UBA_FASE1', cols, 'catsalut', rm=True)
    
    def UP_ICS(self):
        self.ups = {}
        sql = """select scs_codi, ics_codi from nodrizas.cat_centres"""
        for up, ics in getAll(sql, 'nodrizas'):
            self.ups[up] = ics

    def EQAS(self):
        eqas = [('EQAU0222', 'IMP001'),
                ('EQAU0228', 'IMP002'),
                ('EQAU0226', 'IMP003'),
                ('EQAU0230', 'IMP004'),
                ('EQAU0216', 'IMP005'),
                ('EQAU0238', 'IMP006')
                ]
        for e in eqas:
            upload = []
            sql = """select up, uba, tipus, analisis, sum(valor) from eqa_ind.exp_khalix_uba_ind ekui 
                    where indicador = '{}' and analisis in ('DEN', 'NUM')
                    group by up, uba, tipus, analisis""".format(e[0])
            print(sql)
            for up ,uba, tipus, conc, n in getAll(sql, 'eqa_ind'):
                if up in self.ups:
                    upload.append((e[1], self.periode, self.ups[up], uba, tipus, conc, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', n))
            listToTable(upload, 'IMP_UBA_FASE1', 'catsalut')
        

    def FARMA(self):
        translate = {
            'IF299P': 'IMP016',
            'FARM0007': 'IMP017',
            'FARM0008': 'IMP018',
            'FARM0009': 'IMP019',
            'FARM0005': 'IMP012',
            'FARM0006': 'IMP013',
            'IF292P': 'IMP020',
            'IF301P': 'IMP010',
            'IF280P': 'IMP021',
            'IF269P': 'IMP011',
            'IF249P': 'IMP007',
            'IF254P': 'IMP008',
            'IF254PA': 'IMP008A',
            'IF254PB': 'IMP008B',
            'IF263P': 'IMP009'
        }
        upload = []
        sql = """SELECT k0, k1, k2, SUM(v) 
                    FROM altres.exp_khalix_eqpf 
                    WHERE k0 IN {}
                    and k4 = 'NOINSAT' and length(k1) != 5
                    GROUP BY k0, k1, k2""".format(tuple(translate.keys()))
        print(sql)
        for indi, up_uba, tipu, val in getAll(sql, 'altres'):
            up = up_uba[0:5]
            tipus = up_uba[5:6]
            uba = up_uba[6:]
            if tipu == 'DEN':
                upload.append((translate[str(indi)], self.periode, up, uba, tipus, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', val))
            elif tipu == 'NUM':
                upload.append((translate[str(indi)], self.periode, up, uba, tipus, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', val))
        listToTable(upload, 'IMP_UBA_FASE1', 'catsalut')

        upload = []
        sql = """select indicador, up, uba, tipus, numerador, denominador 
                    from altres.exp_ecap_alertes_uba eeau 
                    where indicador in {}""".format(tuple(translate.keys()))
        for indi, up, uba, tipus, num, den in getAll(sql, 'altres'):
            upload.append((translate[str(indi)], self.periode, self.ups[up], uba, tipus, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', den))
            upload.append((translate[str(indi)], self.periode, self.ups[up], uba, tipus, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', num))
        listToTable(upload, 'IMP_UBA_FASE1', 'catsalut')


    def ALERTES(self):
        translate = {'IGFM04AP': 'IMP014', 'IGFM05AP': 'IMP015'}
        upload = []
        sql = """select indicador, up, uba, tipus, analisis, sum(valor) from catsalut.exp_khalix_uba_ind
                where indicador in ('IGFM04AP','IGFM05AP')
                group by up, uba, tipus, analisis """
        print(sql)
        for indi, up, uba, tipus, con, val in getAll(sql, 'catsalut'):
            upload.append((translate[str(indi)], self.periode, self.ups[up], uba, tipus, con, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', val))
        listToTable(upload, 'IMP_UBA_FASE1', 'catsalut')

    def to_Khalix(self):
        sql = "select indicador, periode, concat(up, tipus, uba), analisi, nocat, noimp, dim6set, n, valor from catsalut.IMP_UBA_FASE1"
        file = "IMPRE_UBA"
        exportKhalix(sql,file)

if __name__ == "__main__":
    test = False
    mes, any = Dext()
    INDICADORS(mes, any)
    INDICADORS_UBA(mes, any)
