# coding: latin1

"""
.
"""

import collections as c
import datetime as d
import sisapUtils as u

ind_2022 = {"AP19", "AP21", "AP32", "AP34", "AP36", "EQA0301", "EQA0239", "EQA0204",
            "CONT0002A", "ICAMP01AP", "GFM01-AP", "AP28", "AP33", "AP35", "LABVIT04",
            "ESIAP0402", "RS_AP22", "RS_AP23", "RS_AP24"}
# antics: "SGAM02-AP", "AP29", "AP25bis", "AP30"

ind_ped = {"CAT0702": {"Percentils": {"p25": 89.55,
                                      "p50": 91.43,
                                      "p75": 93.17},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 46.22,
                                  "p50-p75": 69.33,
                                  ">=p75": 92.94}},
           "CAT0703": {"Percentils": {"p25": 88.05,
                                      "p50": 91.76,
                                      "p75": 94.79},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 38.73,
                                  "p50-p75": 58.09,
                                  ">=p75": 77.45}},
           "CAT0704": {"Percentils": {"p25": 86.28,
                                      "p50": 89.61,
                                      "p75": 92.83},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 36.15,
                                  "p50-p75": 54.22,
                                  ">=p75": 72.29}},
           "CAT0709": {"Percentils": {"p25": 96.55,
                                      "p50": 97.72,
                                      "p75": 98.87},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 37.01,
                                  "p50-p75": 55.51,
                                  ">=p75": 74.01}},
           "CAT0710": {"Percentils": {"p25": 88.80,
                                      "p50": 91.51,
                                      "p75": 93.88},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 37.01,
                                  "p50-p75": 55.51,
                                  ">=p75": 74.01}},
           "CAT1103": {"Percentils": {"p25": 79.38,
                                      "p50": 86.93,
                                      "p75": 92.81},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 37.87,
                                  "p50-p75": 56.80,
                                  ">=p75": 75.73}},
           }

FILE = "SIAC_ECAP_{}{}.txt"


class CatSalut(object):
    """."""

    def __init__(self):
        """."""
        self.dext, self.grip = u.getOne("select data_ext, grip from dextraccio", "nodrizas")
        # self.dext = d.datetime.strptime('30/06/22', '%d/%m/%y')
        self.dades = c.defaultdict(c.Counter)
        self.get_centres()
        self.get_linies()
        self.get_catsalut()
        self.get_ap35()
        self.get_eqa()
        self.get_lab()
        self.get_esiap()
        self.get_pedia()
        self.get_ap34()
        self.get_icam()
        self.get_econsulta()
        self.get_accessibilitat()
        self.get_longitudinalitat()
        self.get_gida()
        self.get_upload()
        self.export_data()

    def get_centres(self):
        """."""
        self.conversio = {}
        sql = "select ics_codi, scs_codi from cat_centres"
        for br, up in u.getAll(sql, "nodrizas"):
            self.conversio[br] = up

    def get_linies(self):
        """."""
        self.linies = c.defaultdict(set)
        sql = "select distinct up, uporigen from cat_linies"
        for lp, up in u.getAll(sql, "nodrizas"):
            if lp == "04957":
                up = "04957"
            self.linies[lp].add(up)
    
    def get_catsalut(self):
        """."""
        codis = {"IAP01": "AP01", "IAP11BIS": "AP011bis", "IAP18": "AP18"}
        pobs = {"IAP01": ("NOINSATRC", "INSATRC"),
                "IAP11BIS": ("NOINSAT", "INSAT"),
                "IAP18": ("NOINSASS", "INSASS")}
        sql = "select indicador, comb, up, conc, n \
               from exp_khalix_up_ind_def \
               where indicador in {}".format(tuple(codis))
        for ind, pob, up, analisi, n in u.getAll(sql, "catsalut"):
            if pob in pobs[ind]:
                self.dades[(codis[ind], up, up)][analisi] += n

    def get_ap35(self):
        # OK
        """ INDI003 """
        sql = """select indicador, up, conc, n
               from exp_khalix_up_ind
               where indicador = 'AP35' and comb='NOINSAT'"""
        for ind, up, analisi, n in u.getAll(sql, "catsalut"):
            self.dades[(ind, up, up)][analisi] += n

    def get_eqa(self):
        """."""
        codis = {"EQA0208": "AP19",
                "EQA0306": "AP16",
                 "EQA0210": "AP36", 
                 "EQA0303": "RS_AP22",
                 "EQA0312": "AP22",
                 "EQA0209": "AP21", 
                 # "EQA0501": "AP32", # grip oct a des altres mesos no
                 "EQA0210": "AP36",
                 "EQA0301": "EQA0301",
                 "EQA0239": "EQA0239",
                 "EQA0204": "EQA0204",
                 "EQA0235": "AP28",
                 "EQA0201": "AP30"
                 }
        if self.grip != 1: codis["EQA0501"] = "AP32"
        sql = "select left(indicador, 7), up, conc, edat, n from exp_khalix_up_ind \
               where left(indicador, 7) in {} and \
                     comb in ('NOINSAT', 'INSAT')".format(tuple(codis))
        for ind, up, analisi, edat, n in u.getAll(sql, "eqa_ind"):
            if ind == 'EQA0303':
                if edat in ('EC5559', 'EC6064'):
                    self.dades[(codis[ind], up, up)][analisi] += n
            else:
                self.dades[(codis[ind], up, up)][analisi] += n
        
        mes = self.dext.strftime("%m")
        if mes in ('09', '10', '11', '12'):
            codis = {"EQA0501": "AP32"}
            sql = "select left(indicador, 7), up, conc, n from exp_khalix_up_ind \
                   where left(indicador, 7) = '{}' and \
                         comb in ('NOINSAT', 'INSAT')".format(tuple(codis)[0])
            for ind, up, analisi, n in u.getAll(sql, "eqa_ind"):
                self.dades[(codis[ind], up, up)][analisi] += n
        
        codis = {"EQA1106": "RS_AP23"} # pedia.exp_khalix_up_ind
        sql = "select left(indicador, 7), up, conc, n from pedia.exp_khalix_up_ind \
               where left(indicador, 7) = 'EQA1106' and \
                     comb in ('NOINSAT', 'INSAT')"
        for ind, up, analisi, n in u.getAll(sql, "pedia"):
            self.dades[(codis[ind], up, up)][analisi] += n
    
    def get_lab(self):
        sql = """select account, scs_codi, analysis, n
                from LABORATORI.EXP_KHALIX, nodrizas.cat_centres
                where ACCOUNT = 'LABVIT04'
                and length(entity) = 5
                and entity = ics_codi"""
        for ind, up, analisi, n in u.getAll(sql, "laboratori"):
            self.dades[(ind, up, up)][analisi] += n
    
    def get_esiap(self):
        sql = """select indicador, up, analisi, valor 
                    from ESIAP.EXP_KHALIX 
                    where INDICADOR = 'ESIAP0402'"""
        for ind, up, analisi, n in u.getAll(sql, "esiap"):
            self.dades[(ind, up, up)][analisi] += n

    def get_pedia(self):
        """."""
        sql = "select id_cip_sec, up, num, den \
               from mst_indicadors_pacient \
               where indicador = 'EQA0708' and \
                     ates = 1 and maca = 0 and excl = 0"
        for _id, up, num, den in u.getAll(sql, "pedia"):
            if up in self.linies:
                for uplp in self.linies[up]:
                    self.dades[('AP04', up, uplp)]["NUM"] += num
                    self.dades[('AP04', up, uplp)]["DEN"] += den
            else:
                self.dades[('AP04', up, up)]["NUM"] += num
                self.dades[('AP04', up, up)]["DEN"] += den
        self.dadesped = c.defaultdict(c.Counter)
        sql = """
        select
            id_cip_sec, indicador, up, num, den
        from
            mst_indicadors_pacient
        where
            indicador in ('EQA0703', 'EQA0704',
                          'EQA0709', 'EQA0710', 'EQA1103') and
            ates = 1 and
            maca = 0 and
            excl = 0
        """
        self.dades_khalix = c.defaultdict(c.Counter)
        for _id, indicador, up, num, den in u.getAll(sql, "pedia"):
            indicador_new = 'CAT' + indicador[3:]
            if up in self.linies:
                for uplp in self.linies[up]:
                    self.dadesped[(indicador_new, up, uplp)]["NUM"] += num
                    self.dadesped[(indicador_new, up, uplp)]["DEN"] += den
                # self.dades_khalix[(indicador_new, up, up)]["NUM"] += num
                # self.dades_khalix[(indicador_new, up, up)]["DEN"] += den
            else:
                self.dadesped[(indicador_new, up, up)]["NUM"] += num
                self.dadesped[(indicador_new, up, up)]["DEN"] += den
                # self.dades_khalix[(indicador_new, up, up)]["NUM"] += num
                # self.dades_khalix[(indicador_new, up, up)]["DEN"] += den
        # indicador CAT0702
        sql = """
                select
                    id_cip_sec, indicador, up, num
                from
                    mst_vacsist
                where
                    indicador = 'CAT0702' and
                    ates = 1 and
                    maca = 0 and
                    excl = 0
        """
        for _id, indicador, up, num in u.getAll(sql, "pedia"):
            if up in self.linies:
                for uplp in self.linies[up]:
                    self.dadesped[(indicador, up, uplp)]["NUM"] += num
                    self.dadesped[(indicador, up, uplp)]["DEN"] += 1
            else:
                self.dadesped[(indicador, up, up)]["NUM"] += num
                self.dadesped[(indicador, up, up)]["DEN"] += 1

        for (indicador, up, uplp), dades in self.dadesped.items():
            if indicador in ind_ped:
                num = dades["NUM"]
                den = dades["DEN"]
                if num and den:
                    res = round(num / float(den), 4)
                else:
                    res = 0
                self.dades[(indicador, up, uplp)]["DEN"] = 1
                if res < ind_ped[indicador]["Percentils"]["p25"]/100:
                    punts = ind_ped[indicador]["Punts"]["<p25"]
                elif (res >= ind_ped[indicador]["Percentils"]["p25"]/100 and
                      res < ind_ped[indicador]["Percentils"]["p50"]/100):
                    punts = ind_ped[indicador]["Punts"]["p25-p50"]
                elif (res >= ind_ped[indicador]["Percentils"]["p50"]/100 and
                      res < ind_ped[indicador]["Percentils"]["p75"]/100):
                    punts = ind_ped[indicador]["Punts"]["p50-p75"]
                elif res >= ind_ped[indicador]["Percentils"]["p75"]/100:
                    punts = ind_ped[indicador]["Punts"][">=p75"]
                self.dades[(indicador, up, uplp)]["NUM"] = punts

    def get_ap34(self):
        """ . """
        self.dadesap34 = c.defaultdict(c.Counter)
        for (indicador, up, uplp), dades in self.dadesped.items():
            if indicador in ind_ped:
                num = dades["NUM"]
                den = dades["DEN"]
                if num and den:
                    res = round(num / float(den), 4)
                else:
                    res = 0
                self.dadesap34[('AP34', up, uplp)]["DEN"] = 1
                if res < ind_ped[indicador]["Percentils"]["p25"]/100:
                    punts = ind_ped[indicador]["Punts"]["<p25"]
                elif (res >= ind_ped[indicador]["Percentils"]["p25"]/100 and
                      res < ind_ped[indicador]["Percentils"]["p50"]/100):
                    punts = ind_ped[indicador]["Punts"]["p25-p50"]
                elif (res >= ind_ped[indicador]["Percentils"]["p50"]/100 and
                      res < ind_ped[indicador]["Percentils"]["p75"]/100):
                    punts = ind_ped[indicador]["Punts"]["p50-p75"]
                elif res >= ind_ped[indicador]["Percentils"]["p75"]/100:
                    punts = ind_ped[indicador]["Punts"][">=p75"]
                self.dadesap34[('AP34', up, uplp)]["NUM"] += punts

        for (indicador, up, uplp), dades in self.dadesap34.items():
            num = dades["NUM"]
            den = dades["DEN"]
            self.dades[(indicador, up, uplp)]["NUM"] = num
            self.dades[(indicador, up, uplp)]["DEN"] = 1

    def get_icam(self):
        """."""
        codis = ("IT003OST", "IT003MEN", "IT003TRA", "IT003SIG")
        sql = "select entity, analisi, valor from exp_khalix_it \
               where indicador in {} and \
                     length(entity) = 5".format(codis)
        for br, analisi, n in u.getAll(sql, "altres"):
            self.dades[(
                "SGAM02-AP",
                self.conversio[br],
                self.conversio[br]
                )][analisi] += n

    def get_econsulta(self):
        """."""
        sql = "select br, analisis, n from exp_khalix_up_econsulta \
               where indicador like 'ECONS0001%' and PERIODE = 'ANUAL'"
        for br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[("RS_AP24", up, up)][analisi] += n
    
    def get_accessibilitat(self):
        """."""
        codis = {"QACC5D": "AP23", "QACC10D": "AP24"}
        sql = "select k0, k2, k3, v from exp_qc_forats \
               where k4 = 'ANUAL' and \
                     length(k2) = 5 and \
                     k0 in {}".format(tuple(codis))
        for ind, br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[(codis[ind], up, up)][analisi] += n

    def get_longitudinalitat(self):
        """."""
        sql = "select up, analisi, resultat from exp_long_cont_up_a \
               where indicador ='CONT0002A'"
        for br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[("CONT0002A", up, up)][analisi] += n
            else: self.dades_khalix[("CONT0002A", up, up)][analisi] += n
        sql = "select up, analisi, resultat from exp_long_cont_up \
               where indicador ='CONT0002'"
        for br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[("AP25", up, up)][analisi] += n
        sql = "select up, analisi, resultat from exp_long_cont_up_a \
               where indicador ='CONT0002A' and detalle = 'TIPPROF1'"
        for br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[("AP25bis", up, up)][analisi] += n

    def get_gida(self):
        """."""
        sql = "select ent, analisi, valor \
               from exp_khalix_gida \
               where ind = 'GESTINF05' and length(ent) = 5 and analisi = 'NUM'"
        for br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[("AP33", up, up)][analisi] += n
            else: self.dades_khalix[("AP33", up, up)][analisi] += n
        sql = "select distinct ent, analisi, valor \
               from exp_khalix_gida \
               where ind = 'GESTINF05' and length(ent) = 5 and analisi = 'DEN'"
        for br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[("AP33", up, up)][analisi] += n
            else: self.dades_khalix[("AP33", up, up)][analisi] += n

    def get_upload(self):
        """."""
        self.upload_table = []
        self.upload = []
        self.uploadvells = []
        especials = {"AP18": 1}
        convert = {'AP19': 'IAP19', 'AP21': 'IAP021', 'ESIAP0402': 'IESIAP0402',
                    'CONT0002A': 'ICONT0002A', 'EQA0301': 'CAT0301',
                    'EQA0239': 'CAT0239', 'EQA0204': 'CAT0204',
                    'SGAM02-AP': 'ICAM01AP'}
        for (ind, up_ics, up_rca), dades in self.dades_khalix.items():
            # nomes hi ha el CONT0002A x linies pediitriques q nomes van a khalix aqui dintre
            if ind in convert:
                num = dades["NUM"]
                den = dades["DEN"]
                self.upload_table.append((convert[ind], 'Aperiodo', up_ics, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(num)))
                self.upload_table.append((convert[ind], 'Aperiodo', up_ics, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(den)))
        for (ind, up_ics, up_rca), dades in self.dades.items():
            if especials.get(ind, 0) >= self.dext.month:
                periode = self.dext.year - 1
            else:
                periode = self.dext.year
            estat = "D" if especials.get(ind, 12) == self.dext.month else "P"
            num = dades["NUM"]
            den = dades["DEN"]
            if num and den:
                if ind not in ('CAT0702', 'CAT0703', 'CAT0704', 'CAT0709',
                               'CAT0710', 'CAT1103', 'AP34'):
                    res = str(round(float(num) / float(den), 4)).replace(".", ",")
                    res_t = round(float(num) / float(den), 4)
                else:
                    res = str(num).replace(".", ",")
                    res_t = num
            else:
                res = 0
            this = (periode, ind, up_ics, up_rca, res, int(num), int(den),
                    self.dext.strftime("%d/%m/%Y"), "", estat,
                    d.date.today().strftime("%d/%m/%Y"))
            self.upload.append(this)
            # CAT0704 surt duplicat, revisar. en export a khalix
            if ind not in convert and ind not in ('CAT0702', 'CAT0703', 'CAT0704', 'CAT0709',
                               'CAT0710', 'CAT1103', 'AP34'):
                self.upload_table.append((ind, 'Aperiodo', up_ics, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(num)))
                self.upload_table.append((ind, 'Aperiodo', up_ics, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(den)))
            if ind in convert:
                self.upload_table.append((convert[ind], 'Aperiodo', up_ics, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(num)))
                self.upload_table.append((convert[ind], 'Aperiodo', up_ics, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(den)))
            else:
                self.upload_table.append((ind, 'Aperiodo', up_ics, 'AGASSOL', 'NOCAT', 'NOIMP', 'DIM6SET','N', num))
        # per afegir el num i den dels subindicadors del AP34
        indicadors_set = set()
        for (indicador, up, uplp), dades in self.dadesped.items():
            if indicador in ind_ped:
                num = dades["NUM"]
                den = dades["DEN"]
                self.upload_table.append((indicador, 'Aperiodo', up, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(num)))
                self.upload_table.append((indicador, 'Aperiodo', up, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(den)))
                indicadors_set.add(indicador)
        print('indicadors pedia', indicadors_set)

        cols = '(indicador varchar(20), periode varchar(10), up varchar(5), analisi varchar(10), nocat varchar(5), \
                    noimp varchar(5), dim6set varchar(7), n varchar(2), val double)'
        u.createTable('contracte_catsalut', cols, 'catsalut', rm=True)
        u.listToTable(self.upload_table, 'contracte_catsalut', 'catsalut')
        sql = """select distinct indicador, concat('A', 'periodo'), ics_codi,
                    analisi, nocat ,noimp, dim6set, n, round(val, 2) 
                    from catsalut.contracte_catsalut, nodrizas.cat_centres
                    where up = scs_codi 
                    and indicador not in ('RS_AP22','RS_AP23','RS_AP24', 'AP01',
                        'AP011bis', 'AP22', 'AP29', 'AP30', 'AP04', 'AP16',
                        'AP23', 'AP24', 'AP25', 'AP25bis')"""
        u.exportKhalix(sql, 'CATIND_2022')

    def export_data(self):
        """."""
        mes = self.dext.strftime("%m")
        file = FILE.format(self.dext.year, mes)
        # u.writeCSV(u.tempFolder + file,
        #            [('Periode', 'Indicador', 'UP_ICS', 'UP', 'Resultat',
        #              'Numerador', 'Denominador', 'Data Extraccio', 'Nulo',
        #              'Estat', 'Data Execucio')] + self.upload,
        #            sep=";")
        # NO VOLEN CAPÇALERA, si hi ha la capçalera els falla aplicatiu
        u.writeCSV(file, self.upload, sep=";")
        # u.writeCSV(u.tempFolder + file, self.upload, sep=";")
        # text = """Adjuntem arxiu SIAC_ECAP SISAP corresponent a {}/{}.
        # """.format(mes, self.dext.year)
        # text = """Benvolguts,\r\n\r\n{}\r\n\r\nSalutacions cordials,
        # \r\n\r\nSISAP""".format(text)
        
        # # u.sendGeneral(
        # #     'SISAP <sisap@gencat.cat>',
        # #     'roser.cantenys@catsalut.cat',
        # #     'roser.cantenys@catsalut.cat',
        # #     'Dades SIAC_ECAP SISAP',
        # #     text,
        # #     u.tempFolder + file)
        # u.sendGeneral(
        #     'SISAP <sisap@gencat.cat>',
        #     'ecros@catsalut.cat',
        #     'sisap@gencat.cat',
        #     'Dades SIAC_ECAP SISAP',
        #     text,
        #     u.tempFolder + file)
        # u.sshPutFile(file, "export", "catsalut", subfolder="ICS/")
        # u.sshChmod(file, "export", "catsalut", 0666, subfolder="ICS/")
        print("Export data")


if __name__ == "__main__":
    if u.IS_MENSUAL:
        CatSalut()