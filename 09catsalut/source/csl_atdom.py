# coding: utf8
import collections as c

import sisapUtils as u


tb_pac = 'mst_indicadors_pacient'
tb_klx = 'exp_khalix_up_ind'
db = 'catsalut'
ind = 'IAP24'
eqa = 'eqa_ind'
alt = 'altres'
nod = 'nodrizas'


# població i valoració integral
pob = {}
val = set()
sql = "select id_cip_sec, up, uba, ubainf, edat, sexe, ates, \
              institucionalitzat, maca, num \
       from mst_indicadors_pacient \
       where ind_codi = 'EQA0401A' and edat > 74"
for (id, up, uba, ubainf, edat, sexe, ates,
     instit, maca, num) in u.getAll(sql, eqa):
    pob[id] = (up, uba, ubainf, int(edat), sexe, ates, instit, maca)
    if num:
        val.add(id)
# pla de cures
pla = set([id for id, in u.getAll('select id_cip_sec from mst_pla_tots', alt)
           if id in pob])
# recomanacions en cas de crisi
piir = set([id for id, in u.getAll('select id_cip_sec from mst_piir', alt)
            if id in pob])
# visita referents
vis = c.defaultdict(set)
sql = 'select id_cip_sec, left(especialitat, 1) from ag_long_visites \
       where relacio = 1 or numcol = 1 or delegat = 1 or gcasos = 1'
for id, espe in u.getAll(sql, nod):
    if id in pob:
        vis[espe].add(id)

# compleixen
compleixen = vis['1'] & vis['3'] & (val | pla | piir)

# pacients
pacients = [(id, ind) + dades + (int(id in compleixen), 1, 0)
            for (id, dades) in pob.items()]
sql = "select id_cip_sec, up, uba, ubainf, edat, sexe, ates, \
              institucionalitzat, maca from assignada_tot_with_jail where edat > 74"
pacients += [(row[0], ind) + row[1:] + (0, 1, 0) for row
             in u.getAll(sql, 'nodrizas') if row[0] not in pob]
u.execute("delete from {} where indicador = '{}'".format(tb_pac, ind), db)
u.listToTable(pacients, tb_pac, db)

# khalix
khalix = c.Counter()
for (id, ind, up, uba, ubainf, edat, sexe, ates, instit, maca,
     num, den, excl) in pacients:
    edat = u.ageConverter(edat)
    sexe = u.sexConverter(sexe)
    instit = '{}INS'.format('NO' if instit == 0 else '')
    grups = ['{}ASS'.format(instit)]
    if ates == 1:
        grups.append('{}AT'.format(instit))
    for grup in grups:
        khalix[(up, edat, sexe, grup, ind, 'DEN')] += 1
        if num:
            khalix[(up, edat, sexe, grup, ind, 'NUM')] += 1
u.execute("delete from {} where indicador = '{}'".format(tb_klx, ind), db)
u.listToTable([key + (n,) for key, n in khalix.items()], tb_klx, db)
