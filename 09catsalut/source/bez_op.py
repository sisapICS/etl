from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter


nod="nodrizas"
alt = "altres"
db = "catsalut"

agr_op=(828,829)
agr_benz=(830,608)
agr_ep=(872, 873)
agr_neoplasias=(721,722,831,276,411, 731, 730, 511, 732, 733, 708, 521,522,734,258,560,506)

codi_indicador="IGFM04AP"
codi_indicador2="IGFM05AP"



def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from nodrizas.dextraccio;"
    return getOne(sql,nod)[0]

def get_id_farmaco_agr(agr,meses,current_date):
    """ Consigue los pacientes que estan actualmente en tratamiento de los farmacos en agr con una duracion de mas de x meses y la fecha de inicio del tratamiento de dicho farmaco.
        Agrupa por farmaco (agr) cada uno de los pacientes prescritos con dicho farmaco y con la fecha de inicio de tratamiento del farmaco (se crea un tuple (id,ddp) y se anyade a un set
        que es el valor al que senyala el agr en un diccionario)
        Devuelve un diccionario con la estructura {agr: {(id,ini)}
    """

    sql="select id_cip_sec,farmac,pres_orig from nodrizas.eqa_tractaments where farmac in {} and tancat=0".format(agr)
    agr_id=defaultdict(set)
    for id,agr,ini in getAll(sql,nod):
        if monthsBetween(ini,current_date) >= meses:
            agr_id[agr].add((id,ini))

    return agr_id

def get_exclusions(agr):
    """Consigue los pacientes con la patologia en agr.
        Devuelve un set de ids de pacientes.
    """
    sql="select id_cip_sec from nodrizas.eqa_problemes where ps in {}".format(agr)
    return  {id for id in getAll(sql,nod)}

def get_rows(denominador,numerador,exclusions,ind):
    """ Construye las rows de la tabla final a nivel de paciente. Solo entran los ids en el denominador que no esten en exclusions y la columna num
        se cambia a 1 si esta en numerador.
        Devuelve una lista de tuples (rows).
    """
    sql="select id_cip_sec, up, uba, ubainf, edat, sexe, ates, institucionalitzat, maca from {}.assignada_tot where edat >= 15 and ates=1".format(nod)
    rows=[]
    for id, up, uba, ubainf, edat, sexe, ates, institucionalitzat, maca  in getAll(sql,nod):
        if id in denominador and id not in exclusions:
            num=0
            if id in numerador:
                num=1
            rows.append((id, ind, up, uba, ubainf, edat, sexe, ates, institucionalitzat, maca, num,1, 0))
    return rows

def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)
	
	
if __name__ == '__main__':

    printTime("Inicio")
    current_date=get_date_dextraccio()
    print(current_date)
	
    op_id_ddp=get_id_farmaco_agr(agr_op,1,current_date)
    printTime("Opiodes")

    denominador={id for id, ddp in op_id_ddp[828]-op_id_ddp[829]}
    printTime("Denominador")

    bz_id_ddp=get_id_farmaco_agr(agr_benz,3,current_date)
    printTime("Benzodiacepinas")
    ep_id_ddp=get_id_farmaco_agr(agr_ep,3,current_date)
    printTime("Antiepilepticos")

    numerador={id for agr in bz_id_ddp for (id,ddp) in bz_id_ddp[agr]}
    numerador2={id for agr in ep_id_ddp for (id,ddp) in ep_id_ddp[agr]}
    printTime("Numeradores")
    
    exclusions=get_exclusions(agr_neoplasias)

    for obj, ind in ((numerador, codi_indicador), (numerador2, codi_indicador2)):
        rows=get_rows(denominador,obj,exclusions,ind)
        printTime("Rows OK")

        taula_name="indicador_{}".format(ind)
        columns="(id_cip_sec int, indicador varchar(10),up varchar(8), uba varchar(8),ubainf varchar(8),edat int, sexe varchar(5),ates int, institucionalitzat int, maca int,num int, den int, excl int)"
        export_table(taula_name, columns, db, rows)
