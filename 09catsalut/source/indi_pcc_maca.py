#coding: utf8

import sisapUtils as u
import sisaptools as t
import hashlib as h
import collections as c

'''
Titol indicador: PCC/MACA ingressats Hospital/Urgencies 
Agregacio: equip, professional (MF, INF), pacient per edat i sexe
Periode: ultims 12 mesos
Indicador = % pacients PCC/MACA que ingressen  a un dispositiu hospitalari (ingres i/o urgencies)  
Numerador:
        A-Persones que ingressen a un dispositiu hospitalari en el ultims 12 mesos 
        que tinguin una visita en el periode a la variable import.cmbdh de la taula d_ingres 

        B- Persones que ingressen a urgencies hospitalaries en el ultims 12 mesos: 
        que tinguin una visita en el periode a la variable C_DATA_ENTR_F de la taula DWAQUAS.COVID19_URGENCIES_HOSP 

        C- Persones que ingressen a ACUT en el ultims 12 mesos: que tinguin una visita 
        en el periode a la variable C_DATA_ENTR_F de la taula DWAQUAS.COVID19_URGENCIES_CUAP 
Denominador: Poblacio assignada PCC/MACA  
Grup edat > 14 anys
Exclusio: Pacients institucionalitzats en una residencia geriatrica 
'''

def get_hash(cip):
    """."""
    try:
        if t.constants.IS_PYTHON_3:
            cip = cip.encode("latin1")
        return h.sha1(cip).hexdigest().upper()
    except:
        return cip

class INDI_PCC_MACA():
    def __init__(self):
        self.get_extraccio()
        self.get_denominador()
        self.get_numerador()
        self.get_indicador()
        self.export_taula()

    def get_extraccio(self):
        print('extraccio')
        sql = 'select data_ext from dextraccio'
        self.dext, = u.getOne(sql, 'nodrizas')
        print(self.dext, type(self.dext))

    def get_denominador(self):
        print('denominador')
        self.den = dict()
        sql = """SELECT c_cip, c_up, c_metge, C_INFERMERA, c_edat_anys, c_sexe
                    FROM DWSISAP.dbs 
                    WHERE C_EDAT_ANYS > 14 AND 
                    (PR_MACA_DATA IS NOT NULL OR PR_PCC_DATA IS NOT NULL)"""

        for id, up, metge, infermer, edat, sexe in u.getAll(sql, "exadata"):
            self.den[id] = (up, metge, infermer, edat, sexe)
        print(len(self.den))

    def get_numerador(self):
        print('numerador A')
        # part A
        self.numA = set()
        self.numB = set()
        self.numC = set()
        sql = """SELECT SUBSTR(b.cip, 0, 13) 
                    FROM dwcatsalut.tf_cmbdha a, DWSISAP.rca_cip_nia b
                    WHERE DATA_INGRES > add_months(SYSDATE,-2)
                    AND a.nia = b.nia
                    AND edat > 14"""    #.format(self.dext)
        for cip, in u.getAll(sql, "exadata"):
            self.numA.add(get_hash(cip))

        centres_C = set()
        sql = """SELECT up_cod FROM dwsisap.SISAP_CORONAVIRUS_CAT_UP 
                    where up_tip = 'Atenció continuada'"""
        for cip, in u.getAll(sql, "exadata"):
            centres_C.add(cip)

        # part B + C
        print('numerador B+C')
        sql = """SELECT SUBSTR(b.cip, 0, 13),  lpad(c_up, 5, 0) 
                    FROM dwsisap.cmbd_urg a, DWSISAP.rca_cip_nia b 
                    WHERE c_data_entr > add_months(SYSDATE,-1)
                    AND a.c_nia = b.nia
                    AND c_edat > 14"""    #.format(self.dext)
        for cip, up in u.getAll(sql, "exadata"):
            if up in centres_C:
                self.numC.add(get_hash(cip))
            else:
                self.numB.add(get_hash(cip))
        print(len(self.numA), len(self.numB), len(self.numC))

    def get_indicador(self):
        print('indicador')
        self.num_expA = c.defaultdict(int) # per sex / age / up / inf
        self.num_expB = c.defaultdict(int) # per sex / age / up / inf
        self.num_expC = c.defaultdict(int) # per sex / age / up / inf
        self.den_exp = c.defaultdict(int) # per sex / age / up / med
        for id, (up, metge, infermer, edat, sexe) in self.den.items():
            self.den_exp[(up, 'I', infermer, edat, sexe)] += 1
            self.den_exp[(up, 'M', metge, edat, sexe)] += 1
            # print(id)
            if id in self.numA:
                self.num_expA[(up, 'I', infermer, edat, sexe)] += 1
                self.num_expA[(up, 'M', metge, edat, sexe)] += 1
            if id in self.numB:
                self.num_expB[(up, 'I', infermer, edat, sexe)] += 1
                self.num_expB[(up, 'M', metge, edat, sexe)] += 1
            if id in self.numC:
                self.num_expC[(up, 'I', infermer, edat, sexe)] += 1
                self.num_expC[(up, 'M', metge, edat, sexe)] += 1
        print(len(self.num_expA), len(self.num_expB), len(self.num_expC), len(self.den_exp))
    
    def export_taula(self):
        print('crear taules')
        cols = "(indicador varchar(9), up varchar(5), tipus varchar(2), professional varchar(10), edat int, sexe varchar(1), \
                 num int, den int, result double)"
        u.createTable('indi_pcc_maca', cols, 'altres', rm=True)

        print('crear taules A')
        upload_inf = []
        for (up, tipus, professional, edat, sexe), den in self.den_exp.items():
            try:
                num = self.num_expA[(up, tipus, professional, edat, sexe)]
            except:
                num = 0
            res = num/float(den)
            upload_inf.append(['A', up, tipus, professional, edat, sexe, num, den, res])
        u.listToTable(upload_inf, 'indi_pcc_maca', "altres")

        print('crear taules B')
        upload_inf = []
        for (up, tipus, professional, edat, sexe), den in self.den_exp.items():
            try:
                num = self.num_expB[(up, tipus, professional, edat, sexe)]
            except:
                num = 0
            res = num/float(den)
            upload_inf.append(['B', up, tipus, professional, edat, sexe, num, den, res])
        u.listToTable(upload_inf, 'indi_pcc_maca', "altres")

        print('crear taules C')
        upload_inf = []
        for (up, tipus, professional, edat, sexe), den in self.den_exp.items():
            try:
                num = self.num_expC[(up, tipus, professional, edat, sexe)]
            except:
                num = 0
            res = num/float(den)
            upload_inf.append(['C', up, tipus, professional, edat, sexe, num, den, res])
        u.listToTable(upload_inf, 'indi_pcc_maca', "altres")

        conn = u.connect('nodrizas')
        c = conn.cursor()
        c.execute("""create table altres.indi_pcc_maca_agregat as (
            select indicador, up, aga, rs, sum(num) num, sum(den) den, sum(num)/sum(den) res
            from altres.indi_pcc_maca, nodrizas.cat_centres
            where tipus = 'M'
            and up = scs_codi
            group by indicador, up, aga, rs)""")
        conn.close()

if __name__ == "__main__":
    INDI_PCC_MACA()