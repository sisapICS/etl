# coding: iso-8859-1
from sisapUtils import *
import sys, csv, os
from time import strftime
from collections import defaultdict,Counter

debug = False

nod = 'nodrizas'
db = 'catsalut'
imp = 'import'

indicador = 'CMBDAP02'

pob = {}
sql = "select id_cip_sec, (year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')), usua_sexe from assignada, nodrizas.dextraccio"
for id, edat, sexe in getAll(sql, imp):
    pob[id] = {'edat': edat, 'sexe':sexe}
    
id_to_hash = {}
sql = 'select id_cip_sec, codi_sector, hash_d from u11'
for id, sector, hash in getAll(sql, imp):
    id_to_hash[id] = {'sector': sector, 'hash': hash}

recompte = Counter()
llistats = {}
sql = "select id_cip_sec, cp_up_ecap, cp_tcis, cp_muni from cmbdap, nodrizas.dextraccio where (cp_ecap_dvis between date_add(date_add(data_ext,interval - 12 month),interval + 1 day) and data_ext) and cp_finan='02'"
for id, up, tcis, muni in getAll(sql, imp):
    if not muni or muni[:2] not in ('08', '17', '25', '43'):
        den, num = 1, 0
        if tcis in ('02', '05') and muni:
            num = 1
        try:
            edat = pob[id]['edat']
            sexe = pob[id]['sexe']
        except KeyError:
            continue
        recompte[up, ageConverter(edat,5), sexConverter(sexe),'DEN'] += den
        recompte[up, ageConverter(edat,5), sexConverter(sexe),'NUM'] += num
        if num == 0:
            llistats[id, up] = True
upload = []
for (up, edat, sexe, tip), rec in recompte.items():
    upload.append([up, edat, sexe, tip, rec])

table = 'csl_cmbdap02'
create = '(up varchar(5), edat varchar(10), sexe varchar(10), tipus varchar(10), recompte int)'    
createTable(table,create,db, rm=True)
listToTable(upload, table, db)

uploadll = []
for (id, up), rec in llistats.items():
    try:
        sector = id_to_hash[id]['sector']
        hash = id_to_hash[id]['hash']
    except KeyError:
        continue
    uploadll.append([up, 'rup', 'M', indicador, hash, sector, 0])
    
tkhx = 'exp_khalix_up_ind'
sql = "delete a.* from {0} a where indicador = '{1}'".format(tkhx, indicador)
execute(sql, db)

sql = "insert into {0} select up, edat, sexe, 'NOINSAT', '{1}', tipus, recompte from {2}".format(tkhx, indicador, table)
execute(sql, db)

table = 'cmbdap02_llistat_rup'
create = '(up varchar(10), uba varchar(10), tipus varchar(1), indicador varchar(20), hash varchar(40), sector varchar(4), exclos int)'    
createTable(table,create,db, rm=True)
listToTable(uploadll, table, db)
