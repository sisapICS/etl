from sisapUtils import *
from collections import defaultdict
import csv, os

OutFile= tempFolder + 'csl_visites_atdom.txt'
table= 'csl_visites_atdom'
nod= 'nodrizas'
db='catsalut'

tableMain="mst_csl_main"

printTime('inici calcul') 

def csl_espe():

    espe={}
    espe['VIMFATDOMC']={'espe':"('10999','10117')",'lloc':'C','grup':'VISMFATDOM','ped':False}
    espe['VIPDATDOMC']={'espe':"('10999','10888')",'lloc':'C','grup':'VISPDATDOM','ped':True}
    espe['VIODATDOMC']={'espe':"('10777','10106')",'lloc':'C','grup':'VISODATDOM','ped':False}
    espe['VIINATDOMC']={'espe':"('30999')",'lloc':'C','grup':'VISINATDOM','ped':False}
    espe['VIASATDOMC']={'espe':"('05999')",'lloc':'C','grup':'VISASATDOM','ped':False}
    espe['VIMFATDOMD']={'espe':"('10999','10117')",'lloc':'D','grup':'VISMFATDOM','ped':False}
    espe['VIPDATDOMD']={'espe':"('10999','10888')",'lloc':'D','grup':'VISPDATDOM','ped':True}
    espe['VIODATDOMD']={'espe':"('10777','10106')",'lloc':'D','grup':'VISODATDOM','ped':False}
    espe['VIINATDOMD']={'espe':"('30999')",'lloc':'D','grup':'VISINATDOM','ped':False}
    espe['VIASATDOMD']={'espe':"('05999')",'lloc':'D','grup':'VISASATDOM','ped':False}
    return espe
    
a=csl_espe()
visT={}
for b,d in a.items():
    vis={}
    prof=d['espe']
    lloc=d['lloc']
    grup=d['grup']
    pedia=d['ped']
    sql="select id_cip_sec,up_pacient,edat from {0} where espe in {1} and lloc='{2}'".format(table,prof,lloc)
    for id_cip_sec,up,edat in getAll(sql,nod):
        Calcular=False
        if pedia:
            if 0<=int(edat)<=14:
                Calcular=True
        else:
           Calcular=True
        if Calcular:
            if up in vis:
                vis[up]+=1
            else:
                vis[up]=1
            if (up,grup) in visT:
                visT[(up,grup)]+=1
            else:
                visT[(up,grup)]=1
    with open(OutFile,'wb') as file:
        w= csv.writer(file, delimiter='@', quotechar='|')
        for up,d in vis.items():
            dada=int(d)
            try:
                w.writerow([up,dada])
            except KeyError:
                continue
    tableMy="csl_%s" % b
    execute('drop table if exists {}'.format(tableMy),db)
    execute('create table {} (up varchar(5),recompte int)'.format(tableMy),db)
    loadData(OutFile,tableMy,db)
    sql= "create table if not exists {} (up varchar(5) not null default'',ates double,indicador varchar(20) not null default'',valor double)".format(tableMain)
    execute(sql,db)
    sql="delete a.* from {0} a where indicador='{1}'".format(tableMain,b)
    execute(sql,db)
    sql= "insert into {0} select up,1,'{1}',recompte from {2}".format(tableMain,b,tableMy)
    execute(sql,db)
with open(OutFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for (up,grup),d in visT.items():
        dada=int(d)
        try:
            w.writerow([up,grup,dada])
        except KeyError:
            continue
tableMy="csl_vistes_atdom_grup" 
execute('drop table if exists {}'.format(tableMy),db)
execute('create table {} (up varchar(5),indicador varchar(10),recompte int)'.format(tableMy),db)
loadData(OutFile,tableMy,db)
sql= "create table if not exists {} (up varchar(5) not null default'',ates double,indicador varchar(20) not null default'',valor double)".format(tableMain)
execute(sql,db)
sql="delete a.* from {} a where indicador in  ('VISASATDOM','VISINATDOM','VISMFATDOM','VISODATDOM','VISPDATDOM')".format(tableMain)
execute(sql,db)
sql= "insert into {0} select up,1,indicador,recompte from {1}".format(tableMain,tableMy)
execute(sql,db)
vis.clear()
visT.clear()
printTime('fi calcul') 