# coding: latin1

"""
.
"""

import collections as c
import datetime as d
import sisaptools as u
import sisapUtils as t
from dateutil.relativedelta import relativedelta

ind_2022 = {"AP19", "AP21", "AP32", "AP34", "AP36", "EQA0239", "EQA0204",
            "CONT0002A", "ICAMP01AP", "GFM01-AP", "AP28", "AP33", "AP35", "LABVIT04",
            "ESIAP0402", "RS_AP22", "RS_AP23", "RS_AP24", "AP33B", "IACC5DF", "RS_AP26", 
            "IAGC0201", 'ESIAP0401'}

CONVERSIONS = {
    'IACC5DF': 'ACC5DF',
    'IAGC0201': 'AGC0201',
    'ESIAP0401': 'AP38',
    'EQA0239': 'CAT0239'
}

ind_ped = {"CAT0702": {"Percentils": {"p25": 89.55,
                                      "p50": 91.43,
                                      "p75": 93.17},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 46.22,
                                  "p50-p75": 69.33,
                                  ">=p75": 92.94}},
           "CAT0703": {"Percentils": {"p25": 88.05,
                                      "p50": 91.76,
                                      "p75": 94.79},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 38.73,
                                  "p50-p75": 58.09,
                                  ">=p75": 77.45}},
           "CAT0704": {"Percentils": {"p25": 86.28,
                                      "p50": 89.61,
                                      "p75": 92.83},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 36.15,
                                  "p50-p75": 54.22,
                                  ">=p75": 72.29}},
           "CAT0709": {"Percentils": {"p25": 96.55,
                                      "p50": 97.72,
                                      "p75": 98.87},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 37.01,
                                  "p50-p75": 55.51,
                                  ">=p75": 74.01}},
           "CAT0710": {"Percentils": {"p25": 88.80,
                                      "p50": 91.51,
                                      "p75": 93.88},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 37.01,
                                  "p50-p75": 55.51,
                                  ">=p75": 74.01}},
           "CAT1103": {"Percentils": {"p25": 79.38,
                                      "p50": 86.93,
                                      "p75": 92.81},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 37.87,
                                  "p50-p75": 56.80,
                                  ">=p75": 75.73}},
           }

FILE = "SIAC_ECAP_AP_{}{}.txt"


class CatSalut(object):
    """."""

    def __init__(self):
        """."""
        sql = "select data_ext, grip from dextraccio"
        for dext, grip in u.Database("p2262", "nodrizas").get_all(sql):
            self.dext = dext 
            self.grip = grip
        self.dades = c.defaultdict(c.Counter)
        self.get_centres()                          
        print("self.get_centres()")
        self.get_linies()                           
        print("self.get_linies()")
        self.get_catsalut()                         
        print("self.get_catsalut()")
        self.get_ap35()                             
        print("self.get_ap35()")
        self.get_eqa()                              
        print("self.get_eqa()")
        self.get_lab()                              
        print("self.get_lab()")
        self.get_esiap()                            
        print("self.get_esiap()")
        self.get_pedia()                            
        print("self.get_pedia()")
        self.get_ap34()                             
        print("self.get_ap34()")
        self.get_icam()                             
        print("self.get_icam()")
        self.get_econsulta()                        
        print("self.get_econsulta()")
        self.get_accessibilitat()                   
        print("self.get_accessibilitat()")
        self.get_longitudinalitat()                 
        print("self.get_longitudinalitat()")
        self.get_gida()                             
        print("self.get_gida()")
        # self.get_benestar()                         
        # print("self.get_benestar()")
        self.get_grupal_comunitaria()               
        print("self.get_grupal_comunitaria()")
        self.get_resis_global()                     
        print("self.get_resis_global()")
        self.get_SGAM03_AP()
        print("self.get_SGAM03_AP()")
        # self.get_tabac()
        # print("get_tabac()")
        self.get_alcohol()
        print("get_alcohol()")
        self.get_upload()                           
        print("self.get_upload()")
        self.export_data()                        
        print("self.export_data()")

    def get_centres(self):
        """."""
        self.conversio = {}
        sql = "select ics_codi, scs_codi from cat_centres"
        for br, up in u.Database("p2262", "nodrizas").get_all(sql):
            self.conversio[br] = up

    def get_linies(self):
        """."""
        self.linies = c.defaultdict(set)
        sql = "select distinct up, uporigen from cat_linies"
        for lp, up in u.Database("p2262", "nodrizas").get_all(sql):
            if lp == "04957":
                up = "04957"
            self.linies[lp].add(up)
    
    def get_catsalut(self):
        """."""
        codis = {"IAP01": "AP01", "IAP11BIS": "AP011bis", "IAP18": "AP18"}
        pobs = {"IAP01": ("NOINSAT",),
                "IAP11BIS": ("NOINSAT", "INSAT"),
                "IAP18": ("NOINSASS", "INSASS")}
        sql = "select indicador, comb, up, conc, n \
               from exp_khalix_up_ind_def \
               where indicador in {}".format(tuple(codis))
        for ind, pob, up, analisi, n in u.Database("p2262", "catsalut").get_all(sql):
            if pob in pobs[ind]:
                if up in self.linies:
                    for uplp in self.linies[up]:
                        self.dades[(codis[ind], up, uplp)][analisi] += n
                else:
                    self.dades[(codis[ind], up, up)][analisi] += n

    def get_ap35(self):
        # OK
        """ INDI003 """
        sql = """select indicador, up, conc, n
               from exp_khalix_up_ind
               where indicador = 'AP35' and comb='NOINSAT'"""
        for ind, up, analisi, n in u.Database("p2262", "catsalut").get_all(sql):
            self.dades[(ind, up, up)][analisi] += n

    def get_eqa(self):
        """."""
        codis = {"EQA0208": "AP19",
                "EQA0306": "AP16",
                 "EQA0210": "AP36", 
                 "EQA0303": "RS_AP22",
                 "EQA0302": "CAT0302",
                 "EQA0312": "AP22",
                 "EQA0209": "AP21", 
                 # "EQA0501": "AP32", # grip oct a des altres mesos no
                 "EQA0210": "AP36",
                 "EQA0239": "EQA0239",
                 "EQA0204": "EQA0204",
                 "EQA0235": "AP28",
                 "EQA0201": "AP30"
                 }
        if self.grip != 1: codis["EQA0501"] = "AP32"
        sql = "select left(indicador, 7), up, conc, edat, n from exp_khalix_up_ind \
               where left(indicador, 7) in {} and \
                     comb in ('NOINSAT', 'INSAT')".format(tuple(codis))
        for ind, up, analisi, edat, n in u.Database("p2262", "eqa_ind").get_all(sql):
            if ind == 'EQA0303':
                if edat in ('EC5559', 'EC6064'):
                    self.dades[(codis[ind], up, up)][analisi] += n
            else:
                self.dades[(codis[ind], up, up)][analisi] += n
        
        mes = self.dext.strftime("%m")
        if mes in ('09', '10', '11', '12'):
            codis = {"EQA0501": "AP32"}
            sql = "select left(indicador, 7), up, conc, n from exp_khalix_up_ind \
                   where left(indicador, 7) = '{}' and \
                         comb in ('NOINSAT', 'INSAT')".format(tuple(codis)[0])
            for ind, up, analisi, n in u.Database("p2262", "eqa_ind").get_all(sql):
                self.dades[(codis[ind], up, up)][analisi] += n
        
        codis = {"EQA1106": "RS_AP23"} # pedia.exp_khalix_up_ind
        sql = "select left(indicador, 7), up, conc, n from pedia.exp_khalix_up_ind \
               where left(indicador, 7) = 'EQA1106' and \
                     comb in ('NOINSAT', 'INSAT')"
        for ind, up, analisi, n in u.Database("p2262", "pedia").get_all(sql):
            self.dades[(codis[ind], up, up)][analisi] += n
    
    def get_lab(self):
        sql = """select account, scs_codi, analysis, n
                from LABORATORI.EXP_KHALIX, nodrizas.cat_centres
                where ACCOUNT = 'LABVIT04'
                and length(entity) = 5
                and entity = ics_codi"""
        for ind, up, analisi, n in u.Database("p2262", "laboratori").get_all(sql):
            self.dades[(ind, up, up)][analisi] += n
    
    def get_esiap(self):
        sql = """select indicador, up, analisi, valor 
                    from ESIAP.EXP_KHALIX 
                    where INDICADOR in ('ESIAP0402', 'ESIAP0401', 'ESIAP0408')"""
        for ind, up, analisi, n in u.Database("p2262", "esiap").get_all(sql):
            self.dades[(ind, up, up)][analisi] += n

    def get_pedia(self):
        """."""
        sql = "select id_cip_sec, up, num, den \
               from mst_indicadors_pacient \
               where indicador = 'EQA0708' and \
                     ates = 1 and maca = 0 and excl = 0"
        for _id, up, num, den in u.Database("p2262", "pedia").get_all(sql):
            if up in self.linies:
                for uplp in self.linies[up]:
                    self.dades[('AP04', up, uplp)]["NUM"] += num
                    self.dades[('AP04', up, uplp)]["DEN"] += den
            else:
                self.dades[('AP04', up, up)]["NUM"] += num
                self.dades[('AP04', up, up)]["DEN"] += den
        self.dadesped = c.defaultdict(c.Counter)
        sql = """
        select
            id_cip_sec, indicador, up, num, den
        from
            mst_indicadors_pacient
        where
            indicador in ('EQA0703', 'EQA0704',
                          'EQA0709', 'EQA0710', 'EQA1103') and
            ates = 1 and
            maca = 0 and
            excl = 0
        """
        self.dades_khalix = c.defaultdict(c.Counter)
        for _id, indicador, up, num, den in u.Database("p2262", "pedia").get_all(sql):
            indicador_new = 'CAT' + indicador[3:]
            if up in self.linies:
                for uplp in self.linies[up]:
                    self.dadesped[(indicador_new, up, uplp)]["NUM"] += num
                    self.dadesped[(indicador_new, up, uplp)]["DEN"] += den
                # self.dades_khalix[(indicador_new, up, up)]["NUM"] += num
                # self.dades_khalix[(indicador_new, up, up)]["DEN"] += den
            else:
                self.dadesped[(indicador_new, up, up)]["NUM"] += num
                self.dadesped[(indicador_new, up, up)]["DEN"] += den
                # self.dades_khalix[(indicador_new, up, up)]["NUM"] += num
                # self.dades_khalix[(indicador_new, up, up)]["DEN"] += den
        # indicador CAT0702
        sql = """
                select
                    id_cip_sec, indicador, up, num
                from
                    mst_vacsist
                where
                    indicador = 'CAT0702' and
                    ates = 1 and
                    maca = 0 and
                    excl = 0
        """
        for _id, indicador, up, num in u.Database("p2262", "pedia").get_all(sql):
            if up in self.linies:
                for uplp in self.linies[up]:
                    self.dadesped[(indicador, up, uplp)]["NUM"] += num
                    self.dadesped[(indicador, up, uplp)]["DEN"] += 1
            else:
                self.dadesped[(indicador, up, up)]["NUM"] += num
                self.dadesped[(indicador, up, up)]["DEN"] += 1

        for (indicador, up, uplp), dades in self.dadesped.items():
            if indicador in ind_ped:
                num = dades["NUM"]
                den = dades["DEN"]
                if num and den:
                    res = round(num / float(den), 4)
                else:
                    res = 0
                self.dades[(indicador, up, uplp)]["DEN"] = 1
                if res < ind_ped[indicador]["Percentils"]["p25"]/100:
                    punts = ind_ped[indicador]["Punts"]["<p25"]
                elif (res >= ind_ped[indicador]["Percentils"]["p25"]/100 and
                      res < ind_ped[indicador]["Percentils"]["p50"]/100):
                    punts = ind_ped[indicador]["Punts"]["p25-p50"]
                elif (res >= ind_ped[indicador]["Percentils"]["p50"]/100 and
                      res < ind_ped[indicador]["Percentils"]["p75"]/100):
                    punts = ind_ped[indicador]["Punts"]["p50-p75"]
                elif res >= ind_ped[indicador]["Percentils"]["p75"]/100:
                    punts = ind_ped[indicador]["Punts"][">=p75"]
                self.dades[(indicador, up, uplp)]["NUM"] = punts

    def get_ap34(self):
        """ . """
        self.dadesap34 = c.defaultdict(c.Counter)
        for (indicador, up, uplp), dades in self.dadesped.items():
            if indicador in ind_ped:
                num = dades["NUM"]
                den = dades["DEN"]
                if num and den:
                    res = round(num / float(den), 4)
                else:
                    res = 0
                self.dadesap34[('AP34', up, uplp)]["DEN"] = 1
                if res < ind_ped[indicador]["Percentils"]["p25"]/100:
                    punts = ind_ped[indicador]["Punts"]["<p25"]
                elif (res >= ind_ped[indicador]["Percentils"]["p25"]/100 and
                      res < ind_ped[indicador]["Percentils"]["p50"]/100):
                    punts = ind_ped[indicador]["Punts"]["p25-p50"]
                elif (res >= ind_ped[indicador]["Percentils"]["p50"]/100 and
                      res < ind_ped[indicador]["Percentils"]["p75"]/100):
                    punts = ind_ped[indicador]["Punts"]["p50-p75"]
                elif res >= ind_ped[indicador]["Percentils"]["p75"]/100:
                    punts = ind_ped[indicador]["Punts"][">=p75"]
                self.dadesap34[('AP34', up, uplp)]["NUM"] += punts

        for (indicador, up, uplp), dades in self.dadesap34.items():
            num = dades["NUM"]
            den = dades["DEN"]
            self.dades[(indicador, up, uplp)]["NUM"] = num
            self.dades[(indicador, up, uplp)]["DEN"] = 1

    def get_icam(self):
        """."""
        codis = ("IT003OST", "IT003MEN", "IT003TRA", "IT003SIG")
        sql = "select entity, analisi, valor from exp_khalix_it \
               where indicador in {} and \
                     length(entity) = 5".format(codis)
        for br, analisi, n in u.Database("p2262", "altres").get_all(sql):
            self.dades[(
                "SGAM02-AP",
                self.conversio[br],
                self.conversio[br]
                )][analisi] += n

    def get_econsulta(self):
        """."""
        sql = "select br, analisis, n from exp_khalix_up_econsulta \
               where indicador like 'ECONS0001%' and PERIODE = 'ANUAL'"
        for br, analisi, n in u.Database("p2262", "altres").get_all(sql):
            up = self.conversio[br]
            if up not in self.linies or up == '00478':
                self.dades[("RS_AP24", up, up)][analisi] += n
    
    def get_accessibilitat(self):
        """."""
        codis = {"QACC5D": "AP23", "QACC10D": "AP24", "QACC5DF": "IACC5DF"}
        sql = "select k0, k2, k3, sum(v) from exp_qc_forats \
               where k4 = 'ANUAL' and \
                     length(k2) = 5 and \
                     k0 in {} \
               group by k0, k2, k3".format(tuple(codis))
        for ind, br, analisi, n in u.Database("p2262", "altres").get_all(sql):
            up = self.conversio[br]
            if up not in self.linies or up == '00478':
                self.dades[(codis[ind], up, up)][analisi] += n
            if codis[ind] == 'IACC5DF' and up in self.linies:
                self.dades_khalix[("IACC5DF", up, up)][analisi] += n

    def get_longitudinalitat(self):
        """."""
        sql = "select up, analisi, resultat from exp_long_cont_up_a \
               where indicador ='CONT0002A'"
        for br, analisi, n in u.Database("p2262", "altres").get_all(sql):
            up = self.conversio[br]
            if up not in self.linies or up == '00478':
                self.dades[("CONT0002A", up, up)][analisi] += n
            else:
                self.dades_khalix[("CONT0002A", up, up)][analisi] += n
        sql = "select up, analisi, resultat from exp_long_cont_up \
               where indicador ='CONT0002'"
        for br, analisi, n in u.Database("p2262", "altres").get_all(sql):
            up = self.conversio[br]
            if up not in self.linies or up == '00478':
                self.dades[("AP25", up, up)][analisi] += n
        sql = "select up, analisi, resultat from exp_long_cont_up_a \
               where indicador ='CONT0002A' and detalle = 'TIPPROF1'"
        for br, analisi, n in u.Database("p2262", "altres").get_all(sql):
            up = self.conversio[br]
            if up not in self.linies or up == '00478':
                self.dades[("AP25bis", up, up)][analisi] += n

    def get_gida(self):
        """."""
        sql = "select ent, analisi, valor \
               from exp_khalix_gida \
               where ind = 'GESTINF05' and length(ent) = 5 and analisi = 'NUM'"
        for br, analisi, n in u.Database("p2262", "altres").get_all(sql):
            up = self.conversio[br]
            if up not in self.linies or up == '00478':
                self.dades[("AP33", up, up)][analisi] += n
            else: self.dades_khalix[("AP33", up, up)][analisi] += n
        sql = "select distinct ent, analisi, valor \
               from exp_khalix_gida \
               where ind = 'GESTINF05' and length(ent) = 5 and analisi = 'DEN'"
        for br, analisi, n in u.Database("p2262", "altres").get_all(sql):
            up = self.conversio[br]
            if up not in self.linies or up == '00478':
                self.dades[("AP33", up, up)][analisi] += n
            else: self.dades_khalix[("AP33", up, up)][analisi] += n
        sql = "select ent, analisi, valor \
               from exp_khalix_gida \
               where ind = 'GESTINF06' and length(ent) = 5 and analisi = 'NUM'"
        for br, analisi, n in u.Database("p2262", "altres").get_all(sql):
            up = self.conversio[br]
            if up not in self.linies or up == '00478':
                self.dades[("AP33B", up, up)][analisi] += n
            else: self.dades_khalix[("AP33B", up, up)][analisi] += n
        sql = "select distinct ent, analisi, valor \
               from exp_khalix_gida \
               where ind = 'GESTINF06' and length(ent) = 5 and analisi = 'DEN'"
        for br, analisi, n in u.Database("p2262", "altres").get_all(sql):
            up = self.conversio[br]
            if up not in self.linies or up == '00478':
                self.dades[("AP33B", up, up)][analisi] += n
            else: self.dades_khalix[("AP33B", up, up)][analisi] += n
    
    def get_benestar(self):
        sql = """select br, analisis, sum(n)
                    from altres.exp_khalix_benestar_emocional_res
                    where ind = 'BENRES01'
                    group by br, analisis"""
        for br, analisi, n in u.Database("p2262", "altres").get_all(sql):
            up = self.conversio[br]
            if up not in self.linies or up == '00478':
                self.dades[("RS_AP26", up, up)][analisi] += n
    
    def get_grupal_comunitaria(self):
        sql = """select br, 'NUM', N 
                from altres.exp_khalix_grupal_comunitaria
                where ind = 'AGC0201'"""
        for br, analisi, n in u.Database("p2262", "altres").get_all(sql):
            up = self.conversio[br]
            if up not in self.linies or up == '00478':
                self.dades[("IAGC0201", up, up)][analisi] += n
        sql = """select  ics_codi, 'DEN', sum(at) 
                from altres.exp_khalix_pobgeneral a inner join nodrizas.cat_centres b 
                on a.up = scs_codi
                group by ics_codi"""
        for br, analisi, n in u.Database("p2262", "altres").get_all(sql):
            up = self.conversio[br]
            if up not in self.linies or up == '00478':
                self.dades[("IAGC0201", up, up)][analisi] += n
    
    def get_resis_global(self):
        # creem simulaci� indicador CRONICITAT_RES
        sql = """select src_code, target_code FROM preduffa.SISAP_MAP_GRUP_RUP"""
        resi_2_eap = {}
        for resi, eap in u.Database("redics", "data").get_all(sql):
            resi_2_eap[resi] = eap
        sql = """select up, ANALisi, sum(val) from resis.exp_khalix_resis
                where ((indicador in ('RES023A', 'RES024A') and ANALisi = 'NUM')
                or (INDICADOR = 'RES018A' and ANALisi = 'DEN'))
                and DIM6SET = 'POBACTUAL'
                group by up, ANALisi"""
        for resi, analisi, n in u.Database("p2262", "resis").get_all(sql):
            if resi in resi_2_eap:
                up = resi_2_eap[resi]
                self.dades[("RS_AP25", up, up)][analisi] += n
    
    def get_SGAM03_AP(self):
        sql = """SELECT MAX(any_alta) FROM DWCATSALUT.TR_IT_SGAM03_AP"""
        for y, in u.Database("exadata", "data").get_all(sql):
            max_y = y
        sql = """SELECT UP, TOTAL_IT DEN, IT_ASSOLIDES NUM 
                FROM DWCATSALUT.TR_IT_SGAM03_AP
                WHERE any_alta = {}""".format(max_y)
        for up, den, num in u.Database("exadata", "data").get_all(sql):
            self.dades[("SGAM03-AP", up, up)]['DEN'] += den
            self.dades[("SGAM03-AP", up, up)]['NUM'] += num
    
    def get_alcohol(self):
        sql = """
            SELECT id_cip_sec, up
            FROM assignada_tot
            WHERE edat >=15 
            AND edat < 60
            AND ates = 1
        """

        den_cat0303 = c.defaultdict()
        for id, up in t.getAll(sql, 'nodrizas'):
            den_cat0303[id] = up
            self.dades[("CAT0303", up, up)]['DEN'] += 1

        num_cat0303 = set()
        sql = """select id_cip_sec
                from eqa_problemes
                where ps = 85"""
        for id, in t.getAll(sql, "nodrizas"):
            if id in den_cat0303:
                num_cat0303.add(id)

        sql = """
            select id_cip_sec, agrupador
            from eqa_variables
            where agrupador = 1020 and
            usar = 1 and
            data_var between adddate(DATE '{dextd}', interval -2 year) and
            DATE '{dextd}'
        """.format(dextd=self.dext)
        for id, agr in t.getAll(sql, "nodrizas"):
            if id in den_cat0303:
                num_cat0303.add(id)
        for id in num_cat0303:
            self.dades[("CAT0303",den_cat0303[id],den_cat0303[id])]['NUM'] += 1

    # def get_tabac(self):
    #     menys1 = self.dext - relativedelta(years=1)
    #     sql = """
    #         select distinct a.id_cip_sec, b.up
    #         from eqa_tabac a
    #         inner join assignada_tot b
    #         on a.id_cip_sec = b.id_cip_sec
    #         where b.edat > 14
    #         and DATE '{DEXTD}' between dalta and dbaixa
    #         and a.last = 1
    #         and (a.tab  = 1
    #         or (a.tab = 2
    #         and a.dlast >= DATE '{menys1}'))
    #     """.format(DEXTD=self.dext, menys1 = menys1)
    #     den_iesiap0408 = c.defaultdict()
    #     for id,up in t.getAll(sql, 'nodrizas'):
    #         den_iesiap0408[id] = up
    #         self.dades[("ESIAP0408", up, up)]['DEN'] += 1
        
    #     sql = """
    #         SELECT distinct id_cip_sec
    #         FROM eqa_variables
    #         WHERE agrupador = 827
    #         AND data_var >= DATE '{menys1}'
    #     """.format(menys1=menys1)
    #     for id, in t.getAll(sql, 'nodrizas'):
    #         if id in den_iesiap0408:
    #             self.dades[("ESIAP0408",den_iesiap0408[id],den_iesiap0408[id])]['NUM'] += 1

    def get_upload(self):
        """."""
        self.upload_table = []
        self.upload = []
        self.uploadvells = []
        especials = {"AP18": 1}
        convert = {'AP19': 'IAP19', 'AP21': 'IAP021', 'ESIAP0402': 'IESIAP0402',
                    'ESIAP0401': 'IESIAP0401',
                    'ESIAP0401A': 'CAT0303',
                    'CONT0002A': 'ICONT0002A',
                    'EQA0239': 'CAT0239', 'EQA0204': 'CAT0204',
                    'SGAM02-AP': 'ICAM01AP',
                    'AP01': 'IAP01',
                    'SGAM03-AP': 'SGAM03_AP',
                    'ESIAP0408': 'IESIAP0408',
                    'AP33': 'AP33'}
        for (ind, up_ics, up_rca), dades in self.dades_khalix.items():
            # nomes hi ha el CONT0002A x linies pediitriques q nomes van a khalix aqui dintre
            if ind in convert:
                num = dades["NUM"]
                den = dades["DEN"]
                self.upload_table.append((convert[ind], 'Aperiodo', up_ics, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(num)))
                self.upload_table.append((convert[ind], 'Aperiodo', up_ics, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(den)))
            elif ind == 'IACC5DF':
                num = dades["NUM"]
                den = dades["DEN"]
                self.upload_table.append(('IACC5DF', 'Aperiodo', up_ics, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(num)))
                self.upload_table.append(('IACC5DF', 'Aperiodo', up_ics, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(den)))
        for (ind, up_ics, up_rca), dades in self.dades.items():
            if especials.get(ind, 0) >= self.dext.month:
                periode = self.dext.year - 1
            else:
                periode = self.dext.year
            estat = "D" if especials.get(ind, 12) == self.dext.month else "P"
            num = dades["NUM"]
            den = dades["DEN"]
            if num and den:
                if ind not in ('CAT0702', 'CAT0703', 'CAT0704', 'CAT0709',
                               'CAT0710', 'CAT1103', 'AP34'):
                    res = str(round(float(num) / float(den), 4)).replace(".", ",")
                    res_t = round(float(num) / float(den), 4)
                else:
                    res = str(num).replace(".", ",")
                    res_t = num
            else:
                res = 0
            if ind in CONVERSIONS:
                ind_catsalut = CONVERSIONS[ind]
            else:
                ind_catsalut = ind
            this = (periode, ind_catsalut, up_ics, up_rca, res, int(num), int(den),
                    self.dext.strftime("%d/%m/%Y"), "", estat,
                    d.date.today().strftime("%d/%m/%Y"), 'AP')
            self.upload.append(this)
            # CAT0704 surt duplicat, revisar. en export a khalix
            if ind not in convert and ind not in ('CAT0702', 'CAT0703', 'CAT0704', 'CAT0709',
                               'CAT0710', 'CAT1103', 'AP34'):
                self.upload_table.append((ind, 'Aperiodo', up_ics, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(num)))
                self.upload_table.append((ind, 'Aperiodo', up_ics, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(den)))
            if ind in convert or ind == 'SGAM03-AP':
                self.upload_table.append((convert[ind], 'Aperiodo', up_ics, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(num)))
                self.upload_table.append((convert[ind], 'Aperiodo', up_ics, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(den)))
            else:
                self.upload_table.append((ind, 'Aperiodo', up_ics, 'AGASSOL', 'NOCAT', 'NOIMP', 'DIM6SET','N', num))
        # per afegir el num i den dels subindicadors del AP34
        indicadors_set = set()
        for (indicador, up, uplp), dades in self.dadesped.items():
            if indicador in ind_ped:
                num = dades["NUM"]
                den = dades["DEN"]
                self.upload_table.append((indicador, 'Aperiodo', up, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(num)))
                self.upload_table.append((indicador, 'Aperiodo', up, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET','N', int(den)))
                indicadors_set.add(indicador)
        print('indicadors pedia', indicadors_set)
        cols = ("indicador varchar(20)", "periode varchar(10)", "up varchar(5)",
                "analisi varchar(10)", "nocat varchar(5)", "noimp varchar(5)",
                "dim6set varchar(7)", "n varchar(2)", "val double")
        with u.Database("p2262", "catsalut") as exadata:
            exadata.create_table('contracte_catsalut', cols, remove=True)
            exadata.list_to_table(self.upload_table, "contracte_catsalut")

        sql = """select distinct indicador, concat('A', 'periodo'), ics_codi,
                    analisi, nocat ,noimp, dim6set, n, round(val, 2) 
                    from catsalut.contracte_catsalut, nodrizas.cat_centres
                    where up = scs_codi 
                    and indicador in ('IAP01', 'IAP19', 'AP28', 'AP33',
                                        'IESIAP0402', 'CAT0239', 
                                        'ICONT0002A', 'IAGC0201', 'IACC5DF',
                                        'IAP021', 'RS_AP26', 'RS_AP25',
                                        'AP33B', 'CAT0302', 'SGAM03_AP',
                                        'CAT0303', 'IESIAP0403', 'IESIAP0401',
                                        'IESIAP0408')"""
        t.exportKhalix(sql, 'CATIND')

    def export_data(self):
        """."""
        mes = self.dext.strftime("%m")
        file = FILE.format(self.dext.year, mes)
        # u.writeCSV(u.tempFolder + file,
        #            [('Periode', 'Indicador', 'UP_ICS', 'UP', 'Resultat',
        #              'Numerador', 'Denominador', 'Data Extraccio', 'Nulo',
        #              'Estat', 'Data Execucio')] + self.upload,
        #            sep=";")
        # NO VOLEN CAPÇALERA, si hi ha la capçalera els falla aplicatiu
        t.writeCSV(t.tempFolder + file, self.upload, sep=";")
        text = """Adjuntem arxiu SIAC_ECAP SISAP corresponent a {}/{} amb nova columna i nom per validar.
        """.format(mes, self.dext.year)
        text = """Benvolguts,\r\n\r\n{}\r\n\r\nSalutacions cordials,
        \r\n\r\nSISAP""".format(text)
        
        # u.sendGeneral(
        #     'SISAP <sisap@gencat.cat>',
        #     'roser.cantenys@catsalut.cat',
        #     'roser.cantenys@catsalut.cat',
        #     'Dades SIAC_ECAP SISAP',
        #     text,
        #     u.tempFolder + file)
        t.sendGeneral(
            'SISAP <sisap@gencat.cat>',
            'ecros@catsalut.cat',
            'sisap@gencat.cat',
            'Dades SIAC_ECAP SISAP',
            text,
            t.tempFolder + file)
        t.sshPutFile(file, "export", "catsalut", subfolder="ICS/")
        t.sshChmod(file, "export", "catsalut", 0666, subfolder="ICS/")
        print("Export data")


if __name__ == "__main__":
    if t.IS_MENSUAL:
        CatSalut()
