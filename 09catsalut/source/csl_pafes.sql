use catsalut
;

set @fa2anys = (select date_add(date_add(data_ext, interval -2 year), interval +1 day) from nodrizas.dextraccio);
set @fa3mesos = (select date_add(date_add(data_ext, interval -3 month), interval +1 day) from nodrizas.dextraccio);
set @avui = (select data_ext from nodrizas.dextraccio);

#ACTIVITAT FÍSICA
drop table if exists eqa_actfisica;
create table eqa_actfisica (
id_cip_sec double null,
up varchar(5) not null default'',
uba varchar(5) not null default'',
ubainf varchar(5) not null default'',
edat double,
ates double,
sexe varchar(7) not null default'',
institucionalitzat double,
maca double,
fr double,
vP1101_1a double,
VA3030_1a double,
CA331_1a double,
EP4012_1a double,
vP1101_ara double,
VA3030_ara double,
CA331_ara double,
EP4012_ara double,
num double,
den double,
excl double
)
select
	a.id_cip_sec
	,a.up
    ,a.uba
    ,a.ubainf
	,a.edat
	,ates
	,sexe
	,institucionalitzat
	,maca
   ,0 as fr
   ,null as VP1101_1a
   ,null as VA3030_1a
   ,null as CA331_1a
   ,null as EP4012_1a
   ,null as VP1101_ara
   ,null as VA3030_ara
   ,null as CA331_ara
   ,null as EP4012_ara
   ,0 as num
   ,0 as den
   ,0 as excl
from
	nodrizas.assignada_tot_with_jail a
where   
    edat between 15 and 69
;

#Hauríem de parametritzar, però hi ha poc temps així que més endavant...

drop table if exists ps_actf;
create table ps_actf(
id_cip_sec double,
index(id_cip_sec)
)
select id_cip_sec from nodrizas.eqa_problemes where ps in ('18','24','55','239','47','267') group by id_cip_sec
;
update eqa_actfisica a inner join ps_actf b on a.id_cip_sec=b.id_cip_sec
set fr=1 
;
drop table if exists ob_actf;
create table ob_actf(
id_cip_sec double,
index(id_cip_sec)
)
select id_cip_sec from nodrizas.eqa_variables where agrupador=19 and valor>30 and usar=1 group by id_cip_Sec
;
update eqa_actfisica a inner join ob_actf b on a.id_cip_sec=b.id_cip_sec
set fr=1 
;

drop table if exists tabac_actf;
create table tabac_actf(
id_cip_sec double,
index(id_cip_sec)
)
select
   id_cip_sec
from
   nodrizas.eqa_tabac
where
   tab=1 and last=1
;

update eqa_actfisica a inner join tabac_actf b on a.id_cip_sec=b.id_cip_sec
set fr=1 
;

drop table if exists atdom_actf;
create table atdom_actf(
id_cip_sec double,
index(id_cip_sec)
)
select id_cip_sec from nodrizas.eqa_problemes where ps in ('45','86','428','301','429') group by id_cip_sec
;
update eqa_actfisica a inner join atdom_actf b on a.id_cip_sec=b.id_cip_sec
set excl=1 
;
#Mirem que hagi estat sedentari en els últims 2 anys i fins fa 3m

drop table if exists VP1101_1a;
create table VP1101_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,max(valor) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador =240 and data_var between @fa2anys and @fa3mesos group by id_cip_sec
;

update eqa_actfisica a inner join VP1101_1a b on a.id_cip_sec=b.id_cip_sec
set VP1101_1a=valor; 

drop table if exists VA3030_1a;
create table VA3030_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,min(valor) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador =241 and data_var between @fa2anys and @fa3mesos group by id_cip_sec
;

update eqa_actfisica a inner join VA3030_1a b on a.id_cip_sec=b.id_cip_sec
set VA3030_1a=valor;


drop table if exists CA331_1a;
create table CA331_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,max(if(valor = 2 and data_var > 20190131, 3, valor)) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador =242 and data_var between @fa2anys and @fa3mesos group by id_cip_sec
;

update eqa_actfisica a inner join CA331_1a b on a.id_cip_sec=b.id_cip_sec
set CA331_1a=valor;

drop table if exists EP4012_1a;
create table EP4012_1a(
id_cip_sec double,
valor double,
index(id_cip_sec,valor)
)
select id_cip_sec,min(if(valor=0,9999,if(valor=5,0,valor))) as valor from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador =243 and data_var between @fa2anys and @fa3mesos group by id_cip_sec
;

update eqa_actfisica a inner join EP4012_1a b on a.id_cip_sec=b.id_cip_sec
set EP4012_1a=valor;

drop table if exists fisica_ara;
create table fisica_ara(
id_cip_sec double,
agrupador double,
valor double,
usar double,
index(id_cip_sec)
)
select id_cip_sec,agrupador,if(agrupador = 242 and valor = 2 and data_var > 20190131, 3, valor) valor,usar from nodrizas.eqa_variables,nodrizas.dextraccio where agrupador in ('240','241','242','243') and usar=1 and data_var between @fa2anys and @avui
;

update eqa_actfisica a inner join fisica_ara b on a.id_cip_sec=b.id_cip_sec
set VP1101_ara=valor where agrupador=240
;

update eqa_actfisica a inner join fisica_ara b on a.id_cip_sec=b.id_cip_sec
set VA3030_ara=valor where agrupador=241
;

update eqa_actfisica a inner join fisica_ara b on a.id_cip_sec=b.id_cip_sec
set CA331_ara=valor where agrupador=242
;

update eqa_actfisica a inner join fisica_ara b on a.id_cip_sec=b.id_cip_sec
set EP4012_ara=if(valor=0,9999,if(valor=5,0,valor)) where agrupador=243
;

update eqa_actfisica
set den=1 where fr=1 
;
update eqa_actfisica
set num=1 where VP1101_ara in ('1','2') or  VA3030_ara in ('3','2','4') or CA331_ara=1 or EP4012_ara in ('3','4')
;
update eqa_actfisica
set num=1 where (VP1101_1a >VP1101_ara) or (CA331_1a>CA331_ara) or (VA3030_1a < VA3030_ara) or (EP4012_1a < EP4012_ara)
;


drop table if exists csl_iap016;
create table csl_iap016 (
id_cip_sec double null,
indicador varchar(10) not null default'', 
up varchar(5) not null default'',
uba varchar(5) not null default'',
ubainf varchar(5) not null default'',
edat double,
sexe varchar(1) not null default'',
ates double,
institucionalitzat double,
maca double,
num double,
den double,
excl double)
select  
    id_cip_sec,'IAP016' indicador,up,uba,ubainf,edat,sexe,ates,institucionalitzat,maca,num,den,excl from eqa_actfisica where den=1
;

drop table if exists ps_actf;
drop table if exists ob_actf;
drop table if exists VP1101_1a;
drop table if exists VA3030_1a;
drop table if exists CA331_1a;
drop table if exists EP4012_1a;
drop table if exists fisica_ara;
drop table if exists atdom_actf;
drop table if exists eqa_actfisica;

