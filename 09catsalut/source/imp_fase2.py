# -*- coding: utf-8 -*-

import sisapUtils as u
import sys
import collections as c
from dateutil.relativedelta import relativedelta
from datetime import *

IS_TEST = False

# Definir les taules de la base de dades
taula_cataleg = "exp_ecap_IMP_cataleg"
taula_cataleg_pare = "exp_ecap_IMP_catalegPare"
taula_pacients = "exp_ecap_IMP_pacient"

INDICADORS_INVERSOS = {"IMP022": 1,
                       "IMP023": 1,
                       "IMP024": 1,
                       "IMP025": 1,
                       "IMP026": 1,
                       "IMP027": 1,
                       "IMP028": 1,
                    #    "IMP029": 1,
                    #    "IMP030": 0,
                    #    "IMP031": 0,
                       "IMP032": 1,
                       "IMP033": 1,
                       "IMP034": 1,
                       "IMP035": 1,
                       "IMP036": 1,
                       "IMP037": 1,
                       "IMP038": 0, # hauria de ser 1, pero per validar fem 0
                       "IMP039": 1,
                       "IMP040": 0,
                    #    "IMP041": 1,
                    #    "IMP042": 1
                      }

indicadors_validacio = ('IMP024',
                        'IMP035',
                        'IMP037',
                        'IMP038',
                        'IMP039',
                        'IMP040') # si no hi han indicadors per validar, deixar una tupla buida tal que aix�: indicadors_validacio = tuple()

ups_validacio = ('00356',
                 '00106',
                 '08208',
                 '00172') 

# Consulta SQL per obtenir la data de c�lcul
sql = """
        SELECT
            data_ext
        FROM
            dextraccio
        """
data_ext, = u.getOne(sql, "nodrizas")
data_ext_menys1any = data_ext - relativedelta(years=1)
data_ext_menys_15_mesos = data_ext - relativedelta(months=15)
periode_anymes = "A{}".format(data_ext.strftime("%y%m"))                 

class INDICADORS():

    def __init__(self):
        """."""         

        # M�todes necessaris per calcular els indicadors
        self.get_centres();                                 print("self.get_centres()")
        self.get_poblacio();                                print("self.get_poblacio()")
        self.get_problemes();                               print("self.get_problemes()")
        self.get_variables();                               print("self.get_variables()")
        self.get_tractaments();                             print("self.get_tractaments()")
        self.get_envasos_disponibles();                     print("self.get_envasos_disponibles()")
        self.get_episodis_pneumonia_considerats();          print("self.get_episodis_pneumonia_considerats()")
        self.get_tractaments_correctes_pneumonia();         print("self.get_tractaments_correctes_pneumonia()")
        self.get_RAM();                                     print("self.get_RAM()")
        self.get_indicadors();                              print("self.get_indicadors()")

        self.export_cataleg();                              print("self.export_cataleg()")
        self.export_cataleg_pare();                         print("self.export_cataleg_pare()")
        self.export_taula_indicadors();                     print("self.export_taula_indicadors()")
        self.export_taula_pacients();                       print("self.export_taula_pacients()")

        if u.IS_MENSUAL or IS_TEST:
            self.create_tables_Khalix();                    print("self.create_tables_Khalix()")
        if IS_TEST:
            self.export_khalix();                           print("self.export_khalix()")        

    def get_centres(self):
        """Obtenir la informaci� dels Centres d'Atenci� Prim�ria."""

        sql = """
                SELECT
                    scs_codi,
                    ics_codi
                FROM
                    cat_centres
              """
        self.centres = {up: br for up, br in u.getAll(sql, "nodrizas")}

    def get_poblacio(self):
        """Obtenir la informaci� de la poblaci�."""

        # Inicialitzar diccionaris per emmagatzemar la poblaci� assignada adulta i un diccionari conversor id_cip_sec a hash
        self.poblacio = dict()
        self.idcipsec2hash = dict()

        # Consulta SQL per obtenir la informaci� de la poblaci� amb edat igual o superior a 15 anys
        sql = """
                SELECT
                    id_cip_sec,
                    codi_sector,
                    up,
                    upinf,
                    uba,
                    ubainf,
                    data_naix,
                    sexe,
                    ates,
                    maca,
                    institucionalitzat
                FROM
                    assignada_tot
            """
        for id_cip_sec, sector, up, upinf, uba, ubainf, data_naix, sexe, ates, maca, institucionalitzat in u.getAll(sql, "nodrizas"):
            if up in self.centres:
                edat = u.yearsBetween(data_naix, data_ext)
                self.poblacio[id_cip_sec] = {"up": up,
                                            "upinf": upinf,
                                            "uba": uba,
                                            "ubainf": ubainf,
                                            "edat": edat,
                                            "sexe": sexe,
                                            "ates": ates,
                                            "maca": maca,
                                            "institucionalitzat": institucionalitzat,
                                            "sector": sector}

        # Consulta SQL per obtenir els parells id_cip_sec i hash_d, per emmagatzemar les dades en idcipsec2hash
        sql = """
                SELECT
                    id_cip_sec,
                    hash_d
                FROM
                    u11
            """
        for id_cip_sec, hash_d in u.getAll(sql, "import"):
            self.idcipsec2hash[id_cip_sec] = hash_d

    def get_problemes(self):
        """Aquesta funci� recupera els problemes de salut dels pacients i els emmagatzema en un diccionari."""

        # Crear un diccionari per emmagatzemar els problemes de salut dels pacients
        self.problemes = c.defaultdict(set)
        self.problemes["Pneum�nia"] = c.defaultdict(set)

        # Crear un diccionari amb la informaci� dels agrupadors de problemes de salut, amb el nombre de messos contemplats
        # des de la data d'extracci� per ser considerats al c�lcul dels indicadors
        # Clau: agrupador, Valor: descripci� del agrupador i per�ode en mesos
        ps_dict = {1: {"desc": "CI", "periode": 1000},
                   7: {"desc": "AVC", "periode": 1000},
                   11: {"desc": "Claud_int", "periode": 1000},
                   18: {"desc": "DM2", "periode": 1000},
                   21: {"desc": "ICC", "periode": 1000},
                   26: {"desc": "Malaltia Hep�tica Greu", "periode": 1000},
                   53: {"desc": "IRC", "periode": 1000},
                   55: {"desc": "HTA", "periode": 1000},
                   58: {"desc": "Asma", "periode": 1000},
                   62: {"desc": "MPOC", "periode": 1000},
                   158: {"desc": "Pneum�nia", "periode": 15},
                   159: {"desc": "Pneum�nia", "periode": 15},
                   160: {"desc": "Insufici�ncia hep�tica greu", "periode": 1000},
                   163: {"desc": "Al�l�rgia penicil�lina", "periode": 1000},
                   276: {"desc": "Atenci� pal�liativa", "periode": 1000},
                   383: {"desc": "Malaltia de Paget", "periode": 1000},
                   415: {"desc": "Psicosi", "periode": 1000},
                #    425: {"desc": "Depressi�", "periode": 12},
                   710: {"desc": "Fractura per fragilitat", "periode": 1000},
                   722: {"desc": "Neopl�sia maligna", "periode": 1000},
                #    922: {"desc": "Insomni", "periode": 12},                   
                   956: {"desc": "Depressi� recurrent", "periode": 1000},
                   957: {"desc": "Incident: Otitis mitjana aguda serosa", "periode": 12},
                   959: {"desc": "Esteatosi hep�tica", "periode": 1000},
                   961: {"desc": "Incident: Otitis mitjana aguda serosa", "periode": 12},
                   1045: {"desc": "Fibromi�lgia", "periode": 1000},
                   1046: {"desc": "Trastorn bipolar", "periode": 1000},
                   }

        # Consulta SQL per obtenir les dades dels problemes de salut, mitjan�ant agrupadors
        sql = """
                SELECT
                    id_cip_sec,
                    ps,
                    dde
                FROM
                    eqa_problemes
                WHERE
                    ps IN {}
                    AND dde <= '{}'
              """.format(str(tuple(ps_dict.keys())), data_ext)
        for id_cip_sec, ps, data_episodi in u.getAll(sql, "nodrizas"):
            # Comprovar si el pacient est� dins la poblaci� i si el problema est� dins del rang de temps definit a ps_dict
            if id_cip_sec in self.poblacio and data_ext - relativedelta(months=ps_dict[ps]["periode"]) < data_episodi:
                # Si el problema no �s pneum�nia, afegir al conjunt de problemes
                if ps_dict[ps]["desc"] != "Pneum�nia":
                    self.problemes[ps_dict[ps]["desc"]].add(id_cip_sec)
                # Si el problema �s pneum�nia, afegir a un subdiccionari amb la data de l'episodi
                else:
                    self.problemes["Pneum�nia"][id_cip_sec].add(data_episodi)

        # Consulta SQL per obtenir les dades dels problemes d'ansietat, mitjan�ant codis_ps

        codis_ps = {"C01-F40.00": "Ansietat",
                    "C01-F40.10": "Ansietat",
                    "C01-F40.298": "Ansietat",
                    "C01-F40.8": "Ansietat",
                    "C01-F40.9": "Ansietat",
                    "C01-F41": "Ansietat",
                    "C01-F41.0": "Ansietat",
                    "C01-F41.1": "Ansietat",
                    "C01-F41.3": "Ansietat",
                    "C01-F41.8": "Ansietat",
                    "C01-F41.9": "Ansietat",
                    "F40": "Ansietat",
                    "F40.0": "Ansietat",
                    "F40.1": "Ansietat",
                    "F40.2": "Ansietat",
                    "F40.8": "Ansietat",
                    "F40.9": "Ansietat",
                    "F41": "Ansietat",
                    "F41.0": "Ansietat",
                    "F41.1": "Ansietat",
                    "F41.2": "Ansietat",
                    "F41.3": "Ansietat",
                    "F41.8": "Ansietat",
                    "F41.9": "Ansietat",
                    "F40.3": "Ansietat",
                    "F40.4": "Ansietat",
                    "F40.5": "Ansietat",
                    "F40.6": "Ansietat",
                    "F40.7": "Ansietat",
                    "F41.4": "Ansietat",
                    "F41.5": "Ansietat",
                    "F41.6": "Ansietat",
                    "F41.7": "Ansietat",
                    "C01-J45.30": "ASMA persistent",
                    "C01-J45.40": "ASMA persistent",
                    "C01-J45.50": "ASMA persistent",
                    "J45.30": "ASMA persistent",
                    "J45.40": "ASMA persistent",
                    "J45.50": "ASMA persistent",
                }

        sql = """
                SELECT
                    id_cip_sec,
                    pr_cod_ps,
                    pr_dde
                FROM
                    {}
                WHERE 
                    (pr_cod_ps IN {})
                    AND pr_dde <= STR_TO_DATE('{}', '%Y-%m-%d')
            """

        jobs = [(sql.format(table, tuple(codis_ps.keys()), data_ext), codis_ps, self.poblacio) for table in u.getSubTables("problemes") if table[-6:] != '_s6951']
        count = 0
        for sub_problemes in u.multiprocess(sub_get_problemes, jobs, 4):
            for (id_cip_sec, agrupador_ps) in sub_problemes:
                self.problemes[agrupador_ps].add(id_cip_sec)
            count+=1
            print("    probs: {}/{}. {}".format(count, len(jobs), datetime.now()))

        for problema in self.problemes:
            print("Nombre de resultats problema '{}': {}".format(problema, len(self.problemes[problema])))

    def get_variables(self):
        """Aquesta funci� recupera les variables de salut dels pacients i les emmagatzema en un diccionari."""
        
        # Crear un diccionari per defecte per les variables
        self.variables, self.variables_detall, variables = c.defaultdict(lambda: c.defaultdict(dict)), c.defaultdict(set), c.defaultdict(lambda: c.defaultdict(dict))

        # Crear dos comptadors per controlar els pacients que poden tenir valors excessius de pressi� arterial
        excessiu_1, excessiu_2, broncodilatacions = c.Counter(), c.Counter(), c.defaultdict(dict)

        # Crear un diccionari amb els agrupadors de les variables i els seus llindars
        # Clau: agrupador, Valor: descripci� del agrupador i valors llindars segons el concepte de l'agrupador
        vars_dict = {
                     20: {"desc": "HbA1c", "desc_detall": "HbA1c Alta"," valor_alt": 8, "valor_alt_>=60": 8},
                     278: {"desc": "ASMA", "desc_detall": "ASMA persistent", "valor_baix": 2, "valor_alt": 4},
                     482: {"desc": "TAS MAPA", "desc_detall": "TAS MAPA Alta", "valor_alt": 140, "valor_alt_>=60": 145},
                     483: {"desc": "TAD MAPA", "desc_detall": "TAD MAPA Alta", "valor_alt": 85, "valor_alt_>=60": 90},
                     674: {"desc": "TAS", "desc_detall": "TAS Alta", "valor_alt": 150, "valor_alt_>=60": 160, "excessiu_1": 120, "excessiu_2": 135},
                     675: {"desc": "TAD", "desc_detall": "TAD Alta", "valor_alt": 95, "valor_alt_>=60": 95, "excessiu_1": 80, "excessiu_2": 65},
                     906: {"desc": "TAS AMPA", "desc_detall": "TAS AMPA Alta", "valor_alt": 145, "valor_alt_>=60": 145},
                     907: {"desc": "TAD AMPA", "desc_detall": "TAD AMPA Alta", "valor_alt": 90, "valor_alt_>=60": 90},
                     1024: {"desc": "Fenotip MPOC", "desc_detall": "Exacerbaci� MPOC o Fenotip mixt de superposici� asma-MPOC", "valor_baix": 2, "valor_alt": 4},
                     1035: {"desc": "FEV1 sense broncodilataci� (%)"},
                     1036: {"desc": "FEV1 post-broncodilataci� (%)", "desc_detall": "FEV1 post-broncodilataci� (%) >= 50%", "valor_baix": 50},
                     1037: {"desc": "FEV1 percentatge de variaci� broncodilataci� (%)"},
                     1038: {"desc": "FEV1 sense broncodilataci� (L)"},
                     1039: {"desc": "Millor FEV1 sense broncodilataci� (L)"},
                     1040: {"desc": "FEV1 post-broncodilataci� (L)"},
                     1041: {"desc": "Millor FEV1 post-broncodilataci� (L)"},
                     1042: {"desc": "Avaluaci� broncodilataci� (% difer�ncia)"},
                }

        # Crear la consulta SQL per obtenir les dades de les variables
        sql = """
                SELECT
                    id_cip_sec,
                    agrupador,
                    data_var,
                    valor
                FROM
                    eqa_variables
                WHERE
                    agrupador IN {}
                    -- AND data_var BETWEEN '{}' AND '{}' 
              """.format(tuple(vars_dict.keys()), data_ext_menys1any, data_ext)
        for id_cip_sec, agrupador, data_var, valor in u.getAll(sql, "nodrizas"):
            # Comprovar si el id_cip_sec est� en la poblaci�
            if id_cip_sec in self.poblacio:
                if agrupador in (674, 675) or (data_var >= data_ext_menys1any and data_var <= data_ext):
                    self.variables[vars_dict[agrupador]["desc"]][id_cip_sec][data_var] = valor
                    variables[agrupador][id_cip_sec][data_var] = valor
        for agrupador in variables:
            # Comprovar si el valor �s excessiu, segons l'edat
            if "valor_alt" in vars_dict[agrupador] and "valor_alt_>=60" in vars_dict[agrupador]:
                for id_cip_sec in variables[agrupador]:
                    ultima_data = max(variables[agrupador][id_cip_sec])
                    ultim_valor = variables[agrupador][id_cip_sec][ultima_data]
                    edat = self.poblacio[id_cip_sec]["edat"]
                    if (edat < 60 and vars_dict[agrupador]["valor_alt"] < ultim_valor) or (60 <= edat and vars_dict[agrupador]["valor_alt_>=60"] < ultim_valor):
                        self.variables_detall[vars_dict[agrupador]["desc_detall"]].add(id_cip_sec)
            # Comprovar si hi ha un valor de TAS o TAD que excedeixi el llindar "excessiu_1" a vars_dict[agrupador]
            if "excessiu_1" in vars_dict[agrupador]:
                for id_cip_sec in variables[agrupador]:
                    ultima_data = max(variables[agrupador][id_cip_sec])
                    ultim_valor = variables[agrupador][id_cip_sec][ultima_data]
                    if ultim_valor < vars_dict[agrupador]["excessiu_1"]:
                        excessiu_1[id_cip_sec] += 1
            # Comprovar si hi ha un valor de TAS o TAD que excedeixi el llindar "excessiu_2" a vars_dict[agrupador]
            if "excessiu_2" in vars_dict[agrupador]:
                for id_cip_sec in variables[agrupador]:
                    ultima_data = max(variables[agrupador][id_cip_sec])
                    ultim_valor = variables[agrupador][id_cip_sec][ultima_data]
                    if ultim_valor < vars_dict[agrupador]["excessiu_2"]:
                        excessiu_2[id_cip_sec] += 1
            # Comprovar si el valor supera el valor m�nim a considerar
            if "valor_baix" in vars_dict[agrupador] and not "valor_alt" in vars_dict[agrupador]:
                for id_cip_sec in variables[agrupador]:
                    if all([valor >= vars_dict[agrupador]["valor_baix"] for valor in variables[agrupador][id_cip_sec].values()]):
                        self.variables_detall[vars_dict[agrupador]["desc_detall"]].add(id_cip_sec)
            # Comprovar si el valor es troba entre un valor m�nim i un valor m�xim a considerar
            if "valor_baix" in vars_dict[agrupador] and "valor_alt" in vars_dict[agrupador]:
                for id_cip_sec in variables[agrupador]:
                    ultima_data = max(variables[agrupador][id_cip_sec])
                    ultim_valor = variables[agrupador][id_cip_sec][ultima_data]
                    if vars_dict[agrupador]["valor_baix"] <= ultim_valor <= vars_dict[agrupador]["valor_alt"]:
                        self.variables_detall[vars_dict[agrupador]["desc_detall"]].add(id_cip_sec)
            # Agafar els �ltims valors referents a broncodilatacions
            if "broncodilataci�" in vars_dict[agrupador]["desc"]:
                for id_cip_sec in variables[agrupador]:
                    ultima_data = max(variables[agrupador][id_cip_sec])
                    ultim_valor = variables[agrupador][id_cip_sec][ultima_data]
                    broncodilatacions[id_cip_sec][vars_dict[agrupador]["desc"]] = {"ultima_data": ultima_data, "ultim_valor": ultim_valor}
        # Comprovar els comptadors de control excessiu per identificar si el control de PA en un pacient �s excessiu
        for id_cip_sec in excessiu_1:
            if excessiu_1[id_cip_sec] == 2:
                self.variables_detall["Control excessiu PA"].add(id_cip_sec)
        for id_cip_sec in excessiu_2:
            if excessiu_2[id_cip_sec] == 2:
                self.variables_detall["Control excessiu PA"].add(id_cip_sec)      

        # Comprovar si es compleixen condicions de variaci� de FEV1 amb broncodilataci�
        for id_cip_sec in broncodilatacions:
            if "FEV1 percentatge de variaci� broncodilataci� (%)" in broncodilatacions[id_cip_sec]:
                if broncodilatacions[id_cip_sec]["FEV1 percentatge de variaci� broncodilataci� (%)"]["ultim_valor"] >= 12:
                    self.variables_detall["Variaci� del FEV1 amb broncodilataci� >= 12%"].add(id_cip_sec)
            if "FEV1 sense broncodilataci� (%)" in broncodilatacions[id_cip_sec] and "FEV1 post-broncodilataci� (%)" in broncodilatacions[id_cip_sec]:
                if broncodilatacions[id_cip_sec]["FEV1 post-broncodilataci� (%)"]["ultim_valor"] - broncodilatacions[id_cip_sec]["FEV1 sense broncodilataci� (%)"]["ultim_valor"] >= 12 and \
                   broncodilatacions[id_cip_sec]["FEV1 post-broncodilataci� (%)"]["ultima_data"] == broncodilatacions[id_cip_sec]["FEV1 sense broncodilataci� (%)"]["ultima_data"]:
                    self.variables_detall["Variaci� del FEV1 amb broncodilataci� >= 12%"].add(id_cip_sec)
            if "Avaluaci� broncodilataci� (% difer�ncia)" in broncodilatacions[id_cip_sec]:
                if broncodilatacions[id_cip_sec]["Avaluaci� broncodilataci� (% difer�ncia)"]["ultim_valor"] >= 12:
                    self.variables_detall["Variaci� del FEV1 amb broncodilataci� >= 12 % (Per VR1011)"].add(id_cip_sec)
            if "Millor FEV1 post-broncodilataci� (L)" in broncodilatacions[id_cip_sec] and "Millor FEV1 sense broncodilataci� (L)" in broncodilatacions[id_cip_sec]:
                if broncodilatacions[id_cip_sec]["Millor FEV1 post-broncodilataci� (L)"]["ultim_valor"] - broncodilatacions[id_cip_sec]["Millor FEV1 sense broncodilataci� (L)"]["ultim_valor"] >= 200 and \
                   broncodilatacions[id_cip_sec]["Millor FEV1 post-broncodilataci� (L)"]["ultima_data"] == broncodilatacions[id_cip_sec]["Millor FEV1 sense broncodilataci� (L)"]["ultima_data"]:
                    self.variables_detall["Variaci� del FEV1 amb broncodilataci� >= 200 mL"].add(id_cip_sec)
            if "FEV1 post-broncodilataci� (L)" in broncodilatacions[id_cip_sec] and "FEV1 sense broncodilataci� (L)" in broncodilatacions[id_cip_sec]:
                if broncodilatacions[id_cip_sec]["FEV1 post-broncodilataci� (L)"]["ultim_valor"] - broncodilatacions[id_cip_sec]["FEV1 sense broncodilataci� (L)"]["ultim_valor"] >= 200 and \
                   broncodilatacions[id_cip_sec]["FEV1 post-broncodilataci� (L)"]["ultima_data"] == broncodilatacions[id_cip_sec]["FEV1 sense broncodilataci� (L)"]["ultima_data"]:
                    self.variables_detall["Variaci� del FEV1 amb broncodilataci� >= 200 mL"].add(id_cip_sec)

        self.variables_detall["Prova broncodilatadora positiva"] = (self.variables_detall["Variaci� del FEV1 amb broncodilataci� >= 200 mL"] | self.variables_detall["Variaci� del FEV1 amb broncodilataci� >= 12%"]) | self.variables_detall["Variaci� del FEV1 amb broncodilataci� >= 12 % (Per VR1011)"]

        # Calcular el mal control de la pressi� arterial
        TA_cons = set(self.variables["TAS"]) | set(self.variables["TAD"])
        TA_alta_cons = set(self.variables_detall["TAS Alta"]) | set(self.variables_detall["TAD Alta"])
        TA_ampa = set(self.variables["TAS AMPA"]) | set(self.variables["TAD AMPA"])
        TA_alta_ampa = set(self.variables_detall["TAS AMPA Alta"]) | set(self.variables_detall["TAD AMPA Alta"])
        TA_mapa = set(self.variables["TAS MAPA"]) | set(self.variables["TAD MAPA"])
        TA_alta_mapa = set(self.variables_detall["TAS MAPA Alta"]) | set(self.variables_detall["TAD MAPA Alta"])

        control_PA = c.defaultdict(dict)
        for id_cip_sec in TA_cons:
            control_PA[id_cip_sec]["TA_cons"] = 0 if id_cip_sec not in TA_alta_cons else 1
        for id_cip_sec in TA_ampa:
            control_PA[id_cip_sec]["TA_ampa"] = 0 if id_cip_sec not in TA_alta_ampa else 1
        for id_cip_sec in TA_mapa:
            control_PA[id_cip_sec]["TA_mapa"] = 0 if id_cip_sec not in TA_alta_mapa else 1

        self.variables_detall["Mal control pressi� arterial"] = {
            id_cip_sec
            for id_cip_sec, control_PA_tipus in control_PA.items()
            if len(control_PA_tipus.keys()) == sum(control_PA_tipus.values())
        }
        
        for variable in self.variables:
            print("Nombre de resultats variable '{}': {}".format(variable, len(self.variables[variable])))
        for variable in self.variables_detall:
            print("Nombre de resultats variables_detall '{}': {}".format(variable, len(self.variables_detall[variable])))

    def get_tractaments(self):

        # Defineix un diccionari predeterminat per emmagatzemar els diferents tipus de tractaments
        # i els pacients als quals se'ls ha recetat
        self.tractaments = c.defaultdict(set)
        self.tractaments["Set d'hipertensius"] = c.defaultdict(set)
        self.tractaments["Amoxicilina"] = c.defaultdict(set)
        self.tractaments["Levofloxacina"] = c.defaultdict(set)
        self.tractaments["Set d'ansiol�tics/hipn�tics"] = c.defaultdict(set)
        self.tractaments["Set d'antiespasm�dics urinaris"] = c.defaultdict(set)
        # caldr� afegir:
        # self.tractaments["Absorbents d'orina rectangular"]
        # self.tractaments["Absorbents d'orina nit"]
        # Defineix un diccionari predeterminat per emmagatzemar les prescripcions de pacients amb pneum�nia
        # OR (pf_gt_cod = '{}' AND ((ppfmc_posologia*ce_quantitat) / ppfmc_freq*24) > {}).format(pf_cod_atc, dosi_maxima) \
        #                        for pf_cod_atc, dosi_maxima in benzo_dosi_maxima.items())[3:]
        # benzo_dosi_maxima semblant a: {'23C01': 5}
        # millor es fa l'extracci� del codi amb la freq, poso i quantitat calculat, i despr�s s'agrupa i es filtra
        self.prescripcions_lligades_pneumonia = c.defaultdict(set)

        # Defineix un conjunt per emmagatzemar l'historial d'antidepressius recomanats
        self.historic_antidepressius_recomanats = set()

        # Defineix un conjunt per emmagatzemar el historial de tractament amb glucocorticoides sist�mics de com a m�nim 3 mesos de durada
        self.glucocorticoides_sistemics_minim_3_mesos = set()

        # Defineix un diccionari de d'agrupadors de tractaments 
        tractaments_dict = {56: "IECA",
                            65: "Salbutamol",
                            66: "Terbutalina",
                            72: "ARA II",
                            81: "Aromatasa",
                            118: "AINES",
                            155: "Amoxicilina",
                            156: "Amoxicilina",
                            717: "Bisfofonats",
                            735: "Sacubitril/Valsartan",
                            898: "Antidepressius tric�clics",
                            955: "Olmesartan",
                            967: "IECA Tancat",
                            973: "Levofloxacina",
                            995: "Corticoides inhalats"}

        # Obtenir els tractaments corresponents a cada pacient
        # Afegit de la query del 967 �s perqu� el IECA ha d'estar tancat, independentment de quan el port�s
        sql = """
                SELECT
                    id_cip_sec,
                    farmac,
                    pres_orig,
                    data_fi,
                    tancat
                FROM
                    eqa_tractaments
                WHERE
                    (farmac IN {}
                    AND '{}' BETWEEN pres_orig AND data_fi)
                    or (farmac = 967)
              """.format(str(tuple(tractaments_dict.keys())), data_ext)
        for id_cip_sec, farmac, data_ini, data_fi, tancat in u.getAll(sql, "nodrizas"):
            # Comprovar si el id_cip_sec est� en la poblaci�
            if id_cip_sec in self.poblacio:
                # Afegir pacients amb tractaments d'amoxicilina o levofloxacina i les dates del tractament
                if tractaments_dict[farmac] == "Amoxicilina" or tractaments_dict[farmac] == "Levofloxacina":
                    self.tractaments[tractaments_dict[farmac]][id_cip_sec].add((data_ini, data_fi))
                # Afegir pacients amb tractaments diferents a bisfosfonats, amoxicilina o levofloxacina
                else:
                    if tancat:
                        if tractaments_dict[farmac] == "IECA Tancat":
                            self.tractaments[tractaments_dict[farmac]].add(id_cip_sec)
                    elif not tancat:
                        # Afegir pacients en tractament de bisfosfonats m�s llargs de 5 anys
                        if tractaments_dict[farmac] == "Bisfofonats" and u.yearsBetween(data_ini, data_ext) >= 5: 
                            self.tractaments["Bisfofonats m�s de 5 anys"].add(id_cip_sec)                        
                        self.tractaments[tractaments_dict[farmac]].add(id_cip_sec)

        # Consulta SQL per obtenir els codis_pf de productes farmac�utics de Zolpidem 10mg
        sql = """
                SELECT
                    pf_codi
                FROM
                    cat_cpftb006
                WHERE
                    pf_cod_atc = 'N05CF02'
                    AND pf_desc_gen LIKE '%10%'
              """
        codis_pf_zolpidem_10mg = set([pf_codi for pf_codi, in u.getAll(sql, "import")])

        # Consulta SQL per obtenir els codis_pf de productes farmac�utics absorbents
        sql = """
                SELECT
                    pf_codi
                FROM
                    cat_cpftb006
                WHERE
                    pf_gt_codi IN ('23C', '23C00', '23C01', '23C02', '23C03', '23C04', '23C05', '23C06')
              """
        codis_pf_absorbents = set([pf_codi for pf_codi, in u.getAll(sql, "import")])

        # Unir els codis_pf de productes farmac�utics de Zolpidem 10mg i absorbents
        codis_pf = codis_pf_zolpidem_10mg | codis_pf_absorbents

        # Diccionari que relaciona codis ATC amb descripcions de tractaments
        codis_atc_dict = {"C02%": "Hipertensius",
                          "C03%": "Hipertensius",
                          "C07%": "Hipertensius",
                          "C08%": "Hipertensius",
                          "C09%": "Hipertensius",
                          "N03AE01": "Ansiol�tics/Hipn�tics",
                          "N05B%": "Ansiol�tics/Hipn�tics",
                          "N05BA%": "Benzodiacepines",
                          "N05C%": "Ansiol�tics/Hipn�tics",
                          "N05BB01": "Hidroxizina",
                          "N05CF02": "Zolpidem",
                          "N06AB03": "Antidepressius recomanats",
                          "N06AB04": "Antidepressius recomanats",
                          "N06AB05": "Antidepressius recomanats",
                          "N06AB06": "Antidepressius recomanats",
                          "N06AB08": "Antidepressius no recomanats",
                          "N06AB10": "Antidepressius no recomanats",
                          "N06AX%": "Antidepressius no recomanats",
                          "N06AX05": "Trazodona",
                          "N06AG02": "Moclobemida",
                          "N06AX16": "Venlafaxina",
                          "N06AX21": "Duloxetina",
                          "N06AX23": "Desvenlafaxina",
                          "A10BA%": "Hipoglucemiants no insul�nics",
                          "A10BB%": "Hipoglucemiants no insul�nics",
                          "A10BC%": "Hipoglucemiants no insul�nics",
                          "A10BD%": "Hipoglucemiants no insul�nics",
                          "A10BF%": "Hipoglucemiants no insul�nics",
                          "A10BG%": "Hipoglucemiants no insul�nics",
                          "A10BH%": "Hipoglucemiants no insul�nics",
                          "A10BJ%": "Hipoglucemiants no insul�nics",
                          "A10BK%": "Hipoglucemiants no insul�nics",
                          "A10BX%": "Hipoglucemiants no insul�nics",
                          "A10BA02": "Hipoglucemiants no insul�nics recomanats",
                          "A10BB07": "Hipoglucemiants no insul�nics recomanats",
                          "A10BB09": "Hipoglucemiants no insul�nics recomanats",
                          "A10BB12": "Hipoglucemiants no insul�nics recomanats",
                          "A10BD05": "Hipoglucemiants no insul�nics recomanats",
                          "A10BD06": "Hipoglucemiants no insul�nics recomanats",
                          "A10BD07": "Hipoglucemiants no insul�nics recomanats",
                          "A10BD15": "Hipoglucemiants no insul�nics recomanats",
                          "A10BD20": "Hipoglucemiants no insul�nics recomanats",
                          "A10BG03": "Hipoglucemiants no insul�nics recomanats",
                          "A10BH01": "Hipoglucemiants no insul�nics recomanats",
                          "A10BK01": "Hipoglucemiants no insul�nics recomanats",
                          "A10BK03": "Hipoglucemiants no insul�nics recomanats",
                          "A10BX02": "Hipoglucemiants no insul�nics recomanats",
                          "J01%": "Actibacterians",
                          "G04AC53": "Antiespasm�dics urinaris",
                          "G04BD02": "Antiespasm�dics urinaris",
                          "G04BD05": "Antiespasm�dics urinaris",
                          "G04BD06": "Antiespasm�dics urinaris",
                          "G04BD07": "Antiespasm�dics urinaris",
                          "G04BD08": "Antiespasm�dics urinaris",
                          "G04BD09": "Antiespasm�dics urinaris",
                          "G04BD11": "Antiespasm�dics urinaris",
                          "G04BD12": "Antiespasm�dics urinaris",
                          "G04BD13": "Antiespasm�dics urinaris",
                          "G04CB%": "Inhibidors de la testosterona 5?-reductasa",
                          "G04CA51": "Inhibidors de la testosterona 5?-reductasa",
                          "G04CA51": "Inhibidors de la testosterona 5?-reductasa",
                          "H02AA%": "Glucocordicoides",
                          "H02AB%": "Glucocordicoides",
                          "R03AC02": "Salbutamol",
                          "R03AC03": "Terbutalina"
                         }
        # Detecci� dels codis ATC preparats per fer 'LIKE' a la query SQL i guardar-los eliminant el car�cter '%'
        codis_atc_dict_retallats = {atc_codi.replace("%", ""): desc for atc_codi, desc in codis_atc_dict.items() if "%" in atc_codi}

        # Obtenir particions de les taules
        particions_taules = u.getTablePartitions("tractaments", "import")
        
        # Llistar codis d'antidepressius recomanats i glucocorticoides sistemics
        codis_antidepressius_recomanats = tuple(codi for codi in codis_atc_dict if codis_atc_dict[codi] == "Antidepressius recomanats")
        codis_glucocorticoides_sistemics = tuple(codi.replace("%", "") for codi in codis_atc_dict if codis_atc_dict[codi] == "Glucocordicoides")
        
        # Crear llista dels inputs de la funci� a realitzar en multiprocess
        jobs = [(codis_atc_dict.keys(), codis_pf, codis_antidepressius_recomanats, codis_glucocorticoides_sistemics, data_ext, particio_taula) for particio_taula in particions_taules]
        
        counter = 0

        # for job in jobs:
        #     sub_tractaments, dates_prescripcions_lligades_pneumonia, historic_antidepressius_recomanats, glucocorticoides_sistemics_minim_3_mesos = sub_get_tractaments(job)        
        # Executar en paral�lel el subm�tode sub_get_tractaments
        for sub_tractaments, dates_prescripcions_lligades_pneumonia, historic_antidepressius_recomanats, glucocorticoides_sistemics_minim_3_mesos in u.multiprocess(sub_get_tractaments, jobs, 8):
            # Afegir resultats de sub_tractaments al conjunt de tractaments
            for id_cip_sec in sub_tractaments:
                for codi_tractament in sub_tractaments[id_cip_sec]:
                    # D'aquells tractaments que tenen el codi_atc complet a l'objecte codis_atc_dict
                    if codi_tractament in codis_atc_dict:
                        self.tractaments[codis_atc_dict[codi_tractament]].add(id_cip_sec)
                        # D'aquells tractaments que tenen el codi_atc complet a l'objecte codis_atc_dict dels que volem obtenir el set de diferents prescripcions
                        if codis_atc_dict[codi_tractament] == "Hipertensius":
                            self.tractaments["Set d'hipertensius"][id_cip_sec].add(codi_tractament)
                        elif codis_atc_dict[codi_tractament] == "Ansiol�tics/Hipn�tics":
                            self.tractaments["Set d'ansiol�tics/hipn�tics"][id_cip_sec].add(codi_tractament)
                        elif codis_atc_dict[codi_tractament] == "Antiespasm�dics urinaris":
                            self.tractaments["Set d'antiespasm�dics urinaris"][id_cip_sec].add(codi_tractament)
                    # D'aquells tractaments que tenen el codi_pf complet a l'objecte codis_pf
                    elif codi_tractament in codis_pf_zolpidem_10mg:
                        self.tractaments["Zolpidem 10 mg"].add(id_cip_sec)
                    elif codi_tractament in codis_pf_absorbents:
                        self.tractaments["Absorbents d'orina"].add(id_cip_sec)
                    # Comprobem que el codi_tractament �s string, ja que treballarem amb els codis_atc retallats
                    if isinstance(codi_tractament, str):
                        for codi_atc_dict_retallat in codis_atc_dict_retallats:
                            if codi_atc_dict_retallat in codi_tractament:
                                # Afegir resultats de sub_tractaments aquells tractaments que tenen el codi_atc retallat
                                self.tractaments[codis_atc_dict_retallats[codi_atc_dict_retallat]].add(id_cip_sec)
                                # I aquells tractaments que tenen el codi_atc retallat a l'objecte codis_atc_dict dels que volem obtenir el set de diferents prescripcions
                                if codis_atc_dict_retallats[codi_atc_dict_retallat] == "Hipertensius":
                                    self.tractaments["Set d'hipertensius"][id_cip_sec].add(codi_tractament)
                                elif codis_atc_dict_retallats[codi_atc_dict_retallat] == "Ansiol�tics/Hipn�tics":
                                    self.tractaments["Set d'ansiol�tics/hipn�tics"][id_cip_sec].add(codi_tractament)
                                elif codis_atc_dict_retallats[codi_atc_dict_retallat] == "Antiespasm�dics urinaris":
                                    self.tractaments["Set d'antiespasm�dics urinaris"][id_cip_sec].add(codi_tractament)                                    

            # Actualitzar conjunts amb les dades obtingudes
            self.prescripcions_lligades_pneumonia.update(dates_prescripcions_lligades_pneumonia)
            self.historic_antidepressius_recomanats.update(historic_antidepressius_recomanats)
            self.glucocorticoides_sistemics_minim_3_mesos.update(glucocorticoides_sistemics_minim_3_mesos)

            # Imprimir l'estat de la tasca
            counter += 1
            print("     sub_get_tractaments({}/{})".format(str(counter).zfill(2), len(particions_taules)))
        
        # Comprovar si un pacient t� prescrits 3 o m�s hipertensius i actualitzar el conjunt corresponent
        for id_cip_sec in self.tractaments["Set d'hipertensius"]:
            if len(self.tractaments["Set d'hipertensius"][id_cip_sec]) >= 3:
                self.tractaments["3 o + hipertensius"].add(id_cip_sec)
        # Comprovar si un pacient t� prescrits 2 o m�s ansiol�tics/hipn�tics i actualitzar el conjunt corresponent
        for id_cip_sec in self.tractaments["Set d'ansiol�tics/hipn�tics"]:
            if len(self.tractaments["Set d'ansiol�tics/hipn�tics"][id_cip_sec]) >= 2:
                self.tractaments["2 o + ansiol�tics/hipn�tics"].add(id_cip_sec)
        # Comprovar si un pacient t� prescrits 2 o m�s antiespasm�dics urinaris i actualitzar el conjunt corresponent
        for id_cip_sec in self.tractaments["Set d'antiespasm�dics urinaris"]:
            if len(self.tractaments["Set d'antiespasm�dics urinaris"][id_cip_sec]) >= 2:
                self.tractaments["2 o + antiespasm�dics urinaris"].add(id_cip_sec)                

        # Consulta SQL per trobar pacients amb tractataments amb benzodiazepines amb durada superior a 90 dies i iniciats l'�ltim any
        sql = """
                SELECT
                    id_cip_sec
                FROM
                    import.tractaments
                WHERE
                    PPFMC_atccodi LIKE 'N05BA%'
                    AND (datediff(ppfmc_data_fi, ppfmc_pmc_data_ini) > 90 OR ppfmc_data_fi IS NULL)
                    AND datediff('{}', ppfmc_pmc_data_ini) <= 365
                    AND ppfmc_pmc_data_ini <= '{}'
                    AND ('{}' <= ppfmc_data_fi OR ppfmc_data_fi IS NULL)
              """.format(data_ext, data_ext, data_ext)
        for id_cip_sec, in u.getAll(sql, "import"):
            self.tractaments["Benzo amb durada > 90 dies"].add(id_cip_sec)

        for tractament in self.tractaments:
            print("Nombre de resultats tractament '{}': {}".format(tractament, len(self.tractaments[tractament])))

    def get_envasos_disponibles(self):
        """ La funci� obt� els envasos facturats dels �ltims 12 mesos carregats a la taula "facturacio" d'import. 
            Despr�s filtra els id_cip_sec amb m�s de 6 envasos faturats i els emmagatzema en un conjunt.
        """

        envasos_disponibles = c.Counter()

        sql = """
                SELECT
                    max(rec_any_mes)
                FROM
                    facturacio
            """
        max_any_mes, = u.getOne(sql, "import")
        max_data = datetime.strptime(max_any_mes, '%Y%m')

        ultims_any_mes = [max_any_mes]
        
        for i in range(1, 12):
            data = max_data - relativedelta(months=i)  # Restar d�as para obtener meses anteriores
            ultims_any_mes.append(data.strftime('%Y%m'))
        
        sql = """
                SELECT
                    id_cip_sec,
                    rec_envasos
                FROM
                    facturacio
                WHERE
                    rec_any_mes IN {}
            """.format(tuple(ultims_any_mes))
        for id_cip_sec, n_envasos in u.getAll(sql, "import"):
            envasos_disponibles[id_cip_sec] += n_envasos

        self.mes_6_envasos_disponibles_pla_med_agonistes_beta2_adrenergics_accio_curta = {id_cip_sec for id_cip_sec in envasos_disponibles if envasos_disponibles[id_cip_sec] > 6}

    def get_episodis_pneumonia_considerats(self):
        """ Obt� els episodis de pneum�nia considerats per a l'an�lisi, que s�n:
             - Els succe�ts l'�ltim any
             - Els que han succe�t, com a m�xim, 90 dies abans d'una prescripci� lligada a pneum�nia, si el tractament ha iniciat l'�ltim any
        """

        self.episodis_pneumonia_considerats = set()

        # Itera sobre els problemes de pneum�nia
        for id_cip_sec in self.problemes["Pneum�nia"]:
            # Si la la �ltima data de registre del problema ha sigut durant l'�ltim any, s'afegeix a la llista d'episodis considerats
            if max(self.problemes["Pneum�nia"][id_cip_sec]) > data_ext_menys1any:
                self.episodis_pneumonia_considerats.add(id_cip_sec)
            else:
                # Si hi ha prescripcions relacionades amb la pneum�nia l'�ltim any
                if id_cip_sec in self.prescripcions_lligades_pneumonia:
                    # Si la difer�ncia de dies entre la prescripci� m�s antiga i el l'�ltim diagn�stic �s menor a 90, s'afegeix a la llista d'episodis considerats
                    if (min(self.prescripcions_lligades_pneumonia[id_cip_sec]) - max(self.problemes["Pneum�nia"][id_cip_sec])).days < 90:
                        self.episodis_pneumonia_considerats.add(id_cip_sec)
 
    def get_tractaments_correctes_pneumonia(self):
        """Obt� els tractaments correctes per als casos de pneum�nia."""

        self.tractament_correcte_pneumonia = set()

        # Itera els pacients que han tingut diagn�stic de pneum�nia l'�ltim any
        for id_cip_sec in self.problemes["Pneum�nia"]:
            incloure, excloure = 0, 0
            # Si el pacient ha sigut tractat amb Amoxicilina
            if id_cip_sec in self.tractaments["Amoxicilina"]:
                # Itera sobre les dates dels episodis de pneum�nia
                for data_episodi in self.problemes["Pneum�nia"][id_cip_sec]:
                    # Itera sobre les dates d'inici i fi dels tractaments d'Amoxicilina
                    for data_ini_tractament, data_fi_tractament in self.tractaments["Amoxicilina"][id_cip_sec]:
                        # Si la data del tractament est� dins del rang de 7 dies abans o despr�s de l'episodi, s'inclou
                        if data_episodi - timedelta(days=7) <= data_fi_tractament or data_ini_tractament <= data_episodi + timedelta(days=7):
                            incloure = 1
                        else:
                            excloure = 1
            # O si el pacient t� al�l�rgia a la penicil�lina per� ha sigut tractat amb Levofloxacina
            elif id_cip_sec in self.problemes["Al�l�rgia penicil�lina"] and id_cip_sec in self.tractaments["Levofloxacina"]:
                # Itera sobre les dates dels episodis de pneum�nia
                for data_episodi in self.problemes["Pneum�nia"][id_cip_sec]:
                    # Itera sobre les dates d'inici i fi dels tractaments de Levofloxacina
                    for data_ini_tractament, data_fi_tractament in self.tractaments["Levofloxacina"][id_cip_sec]:
                        # Si la data del tractament est� dins del rang de 7 dies abans o despr�s de l'episodi, s'inclou
                        if data_episodi - timedelta(days=7) <= data_fi_tractament or data_ini_tractament <= data_episodi + timedelta(days=7):
                            incloure = 1
                        else:
                            excloure = 1
            
            # Si el pacient s'ha d'incloure i no compleix criteri d'exclusi�
            if incloure == 1 and excloure == 0:
                self.tractament_correcte_pneumonia.add(id_cip_sec)

        print("Nombre de persones amb tractament correcte de pneumonia: {}".format(len(self.tractament_correcte_pneumonia)))
                      
    def get_RAM(self):
        """Obt� les contraindicacions per als tractaments."""

        self.contraindicacions = c.defaultdict(set)

        # Diccionari de contraindicacions
        # Clau: agrupador, Valor: descripci� del agrupador
        contraindicacions_dict = {503: "IECA"}

        # Consulta SQL per obtenir les contraindicacions
        sql = """
                SELECT
                    id_cip_sec,
                    agr
                FROM
                    eqa_ram
                WHERE
                    agr = {}
              """.format(503)      #(str(tuple(contraindicacions_dict.keys())))  Canviar linia anterior si s'afegeixen m�s valors a contraindicacions_dict
        for id_cip_sec, ram_atc in u.getAll(sql, "nodrizas"):
            # Si l'id_cip_sec est� dins de la poblaci�, s'afegeix a la llista de contraindicacions
            if id_cip_sec in self.poblacio:
                self.contraindicacions[contraindicacions_dict[ram_atc]].add(id_cip_sec)

        for contraindicacio in self.contraindicacions:
            print("Nombre de resultats contraindicacio '{}': {}".format(contraindicacio, len(self.contraindicacions[contraindicacio])))

    def get_indicadors(self):
        """Obt� els indicadors IMP i els emmagatzema en la classe."""

        # Inicialitzaci� de variables
        self.dades = set()
        resultats_up = c.Counter()
        self.resultats_up = list()
        resultats_uba = c.Counter()
        self.resultats_uba = list()
        self.complidors_numerador_llistat = set()
        self.resultats_altindicadors = c.defaultdict(c.Counter)

        # Definici� de les condicions per a cada indicador IMP
        # Cada clau del diccionari representa un indicador IMP, i el seu valor �s un altre diccionari
        # amb les claus "dens", "nums" i "excls", que contenen els conjunts de pacients que compleixen
        # les condicions del denominador, numerador i exclusions del denominador, respectivament.
        cond_ind = {"IMP022": {"dens": self.problemes["HTA"] & (self.tractaments["IECA"] | self.tractaments["ARA II"]),
                               "nums": self.tractaments["Olmesartan"],
                               "excls": self.tractaments["Sacubitril/Valsartan"]},
                    "IMP023": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["ates"]]) & self.problemes["HTA"],
                               "nums": self.tractaments["ARA II"] & (self.variables_detall["TAS Alta"] | self.variables_detall["TAD Alta"]),
                               "excls": self.contraindicacions["IECA"] | self.tractaments["IECA Tancat"] | self.tractaments["Sacubitril/Valsartan"]},
                    "IMP024": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["edat"]>=80]) & self.tractaments["3 o + hipertensius"],
                               "nums": self.variables_detall["Control excessiu PA"],
                               "excls": self.problemes["ICC"]},
                    "IMP025": {"dens": (set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["edat"]>75]) | self.problemes["Malaltia Hep�tica Greu"]) & self.tractaments["Zolpidem"],
                               "nums": self.tractaments["Zolpidem 10 mg"],
                               "excls": self.problemes["Esteatosi hep�tica"]},
                    "IMP026": {"dens": self.tractaments["Ansiol�tics/Hipn�tics"],
                               "nums": self.tractaments["2 o + ansiol�tics/hipn�tics"],
                               "excls": self.tractaments["Hidroxizina"] | set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["maca"]]) | self.problemes["Psicosi"] | self.problemes["Neopl�sia maligna"] | self.problemes["Fibromi�lgia"] | self.problemes["Trastorn bipolar"] | self.problemes["Depressi� recurrent"]},
                    "IMP027": {"dens": self.tractaments["Antidepressius recomanats"] | self.tractaments["Antidepressius no recomanats"],
                               "nums": self.tractaments["Antidepressius no recomanats"] - self.historic_antidepressius_recomanats,
                               "excls": self.tractaments["Trazodona"] | self.tractaments["Moclobemida"] | self.tractaments["Antidepressius tric�clics"] | self.problemes["Depressi� recurrent"] | self.problemes["Psicosi"] | self.problemes["Fibromi�lgia"] | self.problemes["Trastorn bipolar"]},
                    "IMP028": {"dens": self.tractaments["Duloxetina"] | self.tractaments["Venlafaxina"] | self.tractaments["Desvenlafaxina"],
                               "nums": self.variables_detall["Mal control pressi� arterial"],
                               "excls": set()},
                    # "IMP029": {"dens": self.problemes["DM2"] & self.variables_detall["HbA1c Alta"] & self.tractaments["Hipoglucemiants no insul�nics"],
                    #            "nums": set(self.poblacio) - self.tractaments["Hipoglucemiants no insul�nics recomanats"],
                    #            "excls": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["maca"]]) | (set(self.poblacio) - self.variables_detall["HbA1c"])},
                    # "IMP030": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if 14 < self.poblacio[id_cip_sec]["edat"] <= 75]) & self.episodis_pneumonia_considerats,
                    #            "nums": self.tractament_correcte_pneumonia,
                    #            "excls": self.problemes["MPOC"] | set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["institucionalitzat"]])},
                    # "IMP031": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if 14 < self.poblacio[id_cip_sec]["edat"] <= 90]) & self.problemes["Incident: Otitis mitjana aguda serosa"],
                    #            "nums": set(self.poblacio) - self.tractaments["Actibacterians"],
                    #            "excls": set()},
                    "IMP032": {"dens": self.tractaments["Antiespasm�dics urinaris"],
                               "nums": self.tractaments["2 o + antiespasm�dics urinaris"],
                               "excls": set()},
                    "IMP033": {"dens": self.tractaments["Antiespasm�dics urinaris"],
                               "nums": self.tractaments["Absorbents d'orina"],
                               "excls": set()},
                    "IMP034": {"dens": self.tractaments["Inhibidors de la testosterona 5?-reductasa"],
                               "nums": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["sexe"] == "D"]),
                               "excls": set()},
                    "IMP035": {"dens": self.tractaments["Bisfofonats"],
                               "nums": self.tractaments["Bisfofonats m�s de 5 anys"],
                               "excls": self.tractaments["Fractura per fragilitat"] | self.glucocorticoides_sistemics_minim_3_mesos | self.tractaments["Aromatasa"] | self.problemes["Malaltia de Paget"]},
                    "IMP036": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if 14 < self.poblacio[id_cip_sec]["edat"]]) & (self.problemes["CI"] | self.problemes["AVC"] | self.problemes["Claud_int"] | self.problemes["IRC"] | self.problemes["Insufici�ncia hep�tica greu"]),
                               "nums": self.tractaments["AINES"],
                               "excls": set()},
                    "IMP037": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["edat"]>=18]),
                               "nums": self.problemes["Ansietat"] & self.tractaments["Benzo amb durada > 90 dies"],
                               "excls": self.problemes["Psicosi"] | self.problemes["Fibromi�lgia"] | self.problemes["Trastorn bipolar"] | self.problemes["Depressi� recurrent"]},
                    "IMP038": {"dens": self.problemes["MPOC"] & self.tractaments["Corticoides inhalats"],
                               "nums": self.variables_detall["FEV1 post-broncodilataci� (%) >= 50%"] | set([id_cip_sec for id_cip_sec in self.poblacio if id_cip_sec not in self.variables["FEV1 post-broncodilataci� (%)"]]), # 
                               "excls": self.problemes["Asma"] | set(self.variables["Exacerbaci� MPOC o Fenotip mixt de superposici� asma-MPOC"]) | self.variables_detall["Prova broncodilatadora positiva"]},
                    "IMP039": {"dens": self.tractaments["Salbutamol"] | self.tractaments["Terbutalina"],
                               "nums": self.mes_6_envasos_disponibles_pla_med_agonistes_beta2_adrenergics_accio_curta,
                               "excls": set()},
                    "IMP040": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if 14 < self.poblacio[id_cip_sec]["edat"]]) & (self.variables_detall["ASMA persistent"] | self.problemes["ASMA persistent"]),
                               "nums": self.tractaments["Corticoides inhalats"],
                               "excls": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["maca"]]) | self.problemes["Atenci� pal�liativa"]},                               
                    # "IMP041": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["edat"]>=18]),
                    #            "nums": self.problemes["Insomni"] & self.tractaments["Benzo amb durada > 90 dies"],
                    #            "excls": set()},
                    # "IMP042": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["edat"]>=18]),
                    #            "nums": self.problemes["Depressi�"] & self.tractaments["Benzo amb durada > 90 dies"],
                    #            "excls": set()}                               
                    }
        dades_carol=list()
        # Iteraci� sobre els indicadors i c�lcul dels resultats
        for ind in cond_ind:
            # Per a cada pacient que compleixi les condicions del denominador de l'indicador
            for id_cip_sec in cond_ind[ind]["dens"]:
                if id_cip_sec in self.poblacio:
                    # Assignar 1 si el pacient compleix les condicions del numerador, 0 en cas contrari
                    num = 1 if id_cip_sec in cond_ind[ind]["nums"] else 0
                    # Assignar 1 si el pacient compleix les condicions d'exclusi� del denominador, 0 en cas contrari
                    excl = 1 if id_cip_sec in cond_ind[ind]["excls"] else 0
                    dades_carol.append((ind, id_cip_sec, 1, num, excl))
                    up = self.poblacio[id_cip_sec]["up"]
                    uba = self.poblacio[id_cip_sec]["uba"]
                    ubainf = self.poblacio[id_cip_sec]["ubainf"]
                    edat, sexe = self.poblacio[id_cip_sec]["edat"], self.poblacio[id_cip_sec]["sexe"]
                    edat_agr, sexe_agr = u.ageConverter(edat), u.sexConverter(sexe)

                    # Emmagatzemar les dades dels pacients en el conjunt 'self.dades'
                    self.dades.add((id_cip_sec, ind, up, uba, edat_agr, sexe_agr, 1, num, excl))
                    # Incrementar els comptadors de denominador i numerador dels resultats
                    if ind not in indicadors_validacio:
                        resultats_up[(ind, up, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += 1 if not excl else 0
                        resultats_up[(ind, up, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += num if not excl else 0
                        resultats_uba[(ind, up, uba, 'M', 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += 1 if not excl else 0
                        resultats_uba[(ind, up, uba, 'M', 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += num if not excl else 0
                        resultats_uba[(ind, up, ubainf, 'I', 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += 1 if not excl else 0
                        resultats_uba[(ind, up, ubainf, 'I', 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += num if not excl else 0

                    # Si el pacient no est� excl�s i compleix les condicions del numerador sent el indicador invers,
                    # o no compleix les condicions del numerador sent el indicador no invers, l'afegim al conjunt 
                    # 'self.complidors_numerador_llistat'
                    noresolt = 0
                    if not excl:
                        invers = INDICADORS_INVERSOS[ind]
                        if (invers and num) or (not invers and not num):
                            self.complidors_numerador_llistat.add((id_cip_sec, ind, excl))
                            noresolt = 1
                    
                        # Incrementar els comptadors de denominador i numerador dels resultats per FARMINDICADORS de pdp
                        self.resultats_altindicadors[(up, uba, "M", ind)]["DEN"] += 1 
                        self.resultats_altindicadors[(up, uba, "M", ind)]["NUM"] += num
                        self.resultats_altindicadors[(up, uba, "M", ind)]["NORESOLTS"] += noresolt
                        if ind == "IMP033":
                            self.resultats_altindicadors[(up, ubainf, "I", ind)]["DEN"] += 1 
                            self.resultats_altindicadors[(up, ubainf, "I", ind)]["NUM"] += num
                            self.resultats_altindicadors[(up, ubainf, "I", ind)]["NORESOLTS"] += noresolt

        u.createTable("dades_carol", "(id_cip_sec varchar(14), ind varchar(15), den int, num int, excl int)", "catsalut", rm=1)
        u.listToTable(dades_carol,"dades_carol","catsalut")

        for (ind, up, analisi, NOCAT, NOIMP, DIM6SET, N), n in resultats_up.items():
            self.resultats_up.append((ind, periode_anymes, self.centres[up], analisi, NOCAT, NOIMP, DIM6SET, N, n))
        for (ind, up, uba, tipus, analisi, NOCAT, NOIMP, DIM6SET, N), n in resultats_uba.items():
            self.resultats_uba.append((ind, periode_anymes, self.centres[up], uba, tipus, analisi, NOCAT, NOIMP, DIM6SET, N, n))

    def create_tables_Khalix(self):
        """ Crea les taules a partir de les quals s'exportar� la informaci� a Khalix."""

        cols_up = '(indicador varchar(10), periode varchar(5), up varchar(5), analisi varchar(3),\
        nocat varchar(5), noimp varchar(5), dim6set varchar(7), n varchar(1), valor int)'
        u.createTable('IMP_FASE2', cols_up, 'catsalut', rm=True)
        cols_uba = '(indicador varchar(10), periode varchar(5), up varchar(5), uba varchar(7), tipus varchar(1), analisi varchar(3),\
                nocat varchar(5), noimp varchar(5), dim6set varchar(7), n varchar(1), valor int)'
        u.createTable('IMP_UBA_FASE2', cols_uba, 'catsalut', rm=True)

        u.listToTable(self.resultats_up, 'IMP_FASE2', 'catsalut')
        u.listToTable(self.resultats_uba, 'IMP_UBA_FASE2', 'catsalut')

    def export_khalix(self):
        """Exporta les dades obtingudes a la taula 'imp_fase2'."""

        sql_up = "select * from catsalut.IMP_FASE2"
        file_up = "IMPRE"
        # u.exportKhalix(sql_up,file_up,force=1)

        sql_uba = "select indicador, periode, concat(up, tipus, uba), analisi, nocat, noimp, dim6set, n, valor from catsalut.IMP_UBA_FASE2"
        file_uba = "IMPRE_UBA"
        # u.exportKhalix(sql_uba,file_uba,force=1)

    def export_cataleg(self):
        """Crear el cat�leg d'indicadors, per exportar a PDP."""

        # Definir les columnes del cat�leg
        columnes_cataleg = '(indicador varchar(8), literal varchar(300), ordre int, pare varchar(8), llistat int, invers int, \
                             mmin double, mint double,mmax double,toShow int,wiki varchar(250),tipusvalor varchar(5))'
        # Crear la taula del cat�leg
        u.createTable(taula_cataleg, columnes_cataleg, "altres", rm=True)

        # Llista de valors a carregar al cat�leg. Cada element de la llista cont� la informaci� d'un indicador
        upload = [
                ("IMP022", "Pacients amb HTA i tractament amb olmesartan", 22, "IMP01" if "IMP022" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP022"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5182/ver/", "PCT"),
                ("IMP023", "Pacients amb HTA no controlada i ARA II sense IECA previ", 23, "IMP01" if "IMP023" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP023"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5183/ver/", "PCT"),
                ("IMP024", "Control excessiu de la pressi� arterial en poblaci� d'edat avan�ada", 3, "IMP01" if "IMP024" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP024"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                ("IMP025", "Inadequaci� dosi de zolpidem en edat avan�ada o malaltia hep�tica greu", 25, "IMP01" if "IMP025" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP025"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5557/ver/", "PCT"),
                ("IMP026", "Duplicitat de tractament ansiol�tic i hipn�tic", 5, "IMP01" if "IMP026" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP026"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5558/ver/", "PCT"),
                ("IMP027", "Antidepressius de 2 l�nia o no recomanats sense un previ de primera l�nia", 27, "IMP01" if "IMP027" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP027"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5559/ver/", "PCT"),
                ("IMP028", "Adequaci� del tractament amb antidepressius en pacients amb hipertensi� arterial no controlada", 28, "IMP01" if "IMP028" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP028"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5199/ver/", "PCT"),
                # ("IMP029", "Pacients amb DM2 i mal control sense tractament amb hipoglucemiants no insul�nics recomanats", 8, "IMP01" if "IMP029" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP029"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                # ("IMP030", "Adequaci� del tractament antibi�tic en pneum�nia comunit�ria en adults", 9, "IMP01" if "IMP030" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP030"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                # ("IMP031", "Adequaci� del tractament antibi�tic en otitis serosa", 10, "IMP01" if "IMP031" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP031"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                ("IMP032", "Pacients en tractament amb dos o m�s antiespasm�dics urinaris (anticolin�rgics o mirabegr�)", 32, "IMP01" if "IMP032" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP032"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5184/ver/", "PCT"),
                ("IMP033", "Pacients en tractament amb antiespasm�dics urinaris (anticolin�rgics o mirabegr�) i prescripci� d'absorbents d'orina", 33, "IMP01" if "IMP033" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP033"], 0, 0, 0, 1, "http://10.80.217.201/sisap-umi/indicador/indicador/5186/ver/", "PCT"),
                ("IMP034", "Dones amb prescripci� de f�rmacs per a la hiperpl�sia prost�tica benigna (HBP)", 34, "IMP01" if "IMP034" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP034"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5197/ver/", "PCT"),
                ("IMP035", "Percentatge de pacients amb prescripci� de bifosfonats orals amb una durada superior a 5 anys sobre el total de pacients tractats amb bifosfonats.", 14, "IMP01" if "IMP035" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP035"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                ("IMP036", "AINE en malaltia cardiovascular, renal cr�nica o insufici�ncia hep�tica", 36, "IMP01" if "IMP036" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP036"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5560/ver/", "PCT"),
                ("IMP037", "Benzodiacepines de >3 mesos durada en nous diagn�stics d'ansietat", 16, "IMP01" if "IMP037" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP037"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),           
                ("IMP038", "Percentatge de pacients amb MPOC i bona funci� pulmonar en tractament corticoides inhalats", 16, "IMP01" if "IMP038" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP038"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),           
                ("IMP039", "Percentatge de pacients amb exc�s d'utilitzaci� d'agonistes beta-2-adren�rgics d'acci� curta", 16, "IMP01" if "IMP039" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP039"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),           
                ("IMP040", "Percentatge de pacients amb asma persistent  amb tractament controlador amb corticoides inhalats", 16, "IMP01" if "IMP040" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP040"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),           
                # ("IMP041", "Tractament inadequat amb benzodiazepines en insomni", 16, "IMP01" if "IMP041" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP041"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),           
                # ("IMP042", "Tractament inadequat amb benzodiazepines en depressi�", 16, "IMP01" if "IMP042" in indicadors_validacio else "IMP", 1, INDICADORS_INVERSOS["IMP042"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),           
                ]
        # Carregar els valors a la taula del cat�leg
        u.listToTable(upload, taula_cataleg, "altres")


    def export_cataleg_pare(self):
        """Crear el cat�leg pare."""

        # Definir les columnes del cat�leg pare
        columnes_catalegPare = '(pare varchar(8), literal varchar(300), ordre int, pantalla varchar(20))'
        # Crear la taula del cat�leg pare
        u.createTable(taula_cataleg_pare, columnes_catalegPare, "altres", rm=True)
        
        # Llista de valors a carregar al cat�leg pare
        upload = [('IMP', 'Indicadors de prescripci� activa', 10, 'EQPFIMP'),
                  ('IMP01', 'Validaci� indicadors farma', 11, 'FARMAVAL')] 
        
        # Carregar els valors a la taula del cat�leg pare
        u.listToTable(upload, taula_cataleg_pare, "altres")

    def export_taula_indicadors(self):
        """Exportar les dades a FARMINDICADORS."""

        # Creem la llista buides on emmagatzemar les dades dels pacients
        upload_pdp = list() 

        # Afegim a la llista els valors a carregar a FARMINDICADORS
        for (up, uba, tipus_prof, ind) in self.resultats_altindicadors:
            if ind not in indicadors_validacio or (ind in indicadors_validacio and up in ups_validacio):
                den = self.resultats_altindicadors[(up, uba, tipus_prof, ind)]["DEN"]
                num = self.resultats_altindicadors[(up, uba, tipus_prof, ind)]["NUM"]
                noresolts = self.resultats_altindicadors[(up, uba, tipus_prof, ind)]["NORESOLTS"]
                resultat = float(num)/float(den)
                upload_pdp.append((up, uba, tipus_prof, ind, num, den, resultat, noresolts))

        cols = """(up varchar(5), uba varchar(5), tipus varchar(1), ind varchar(10),
                    num double, den double, res double, 
                    noresolts int)"""
        u.createTable("exp_ecap_IMP_uba", cols, 'catsalut', rm = True)
        u.listToTable(upload_pdp, "exp_ecap_IMP_uba", "catsalut")


    def export_taula_pacients(self):
        """Crea un llistat de pacients amb les seves dades d'imputaci� i els indicadors IMP relacionats."""

        # Creem dues llistes buides on emmagatzemar les dades dels pacients
        upload = list() 
        upload_pdp = list()

        # Iterem a trav�s de la llista de pacients que es troben en el numerador dels indicadors IMP
        for (id_cip_sec, ind, excl_num) in self.complidors_numerador_llistat:
            try:
                # Obtenim el hash associat a l'identificador del pacient
                hash_d = self.idcipsec2hash[id_cip_sec]

                # Obtenim les dades d'assignaci� del pacient
                up = self.poblacio[id_cip_sec]["up"]
                uba = self.poblacio[id_cip_sec]["uba"]
                upinf = self.poblacio[id_cip_sec]["upinf"]
                ubainf = self.poblacio[id_cip_sec]["ubainf"]
                sector = self.poblacio[id_cip_sec]["sector"]

                # Afegim aquestes dades a la llista 'upload'
                if ind not in indicadors_validacio or (ind in indicadors_validacio and up in ups_validacio):
                    upload.append((id_cip_sec, up, uba, upinf, ubainf, ind, excl_num, hash_d, sector))
            except:
                pass

        # Definim les columnes de la taula taula_pacients
        columnes_llistats = '(id_cip_sec double, up varchar(5), uba varchar(7), upinf varchar(5), ubainf varchar(7),\
                            grup_codi varchar(10), exclos int, hash_d varchar(40), sector varchar(4))'
        # Crear la taula taula_pacients
        u.createTable(taula_pacients, columnes_llistats, "catsalut", rm=True)

        # Carregar els valors a la taula taula_pacients
        u.listToTable(upload, taula_pacients, "catsalut")
            
def sub_get_problemes(input):
    
    sql, codis_ps, poblacio = input

    sub_problemes = set()

    for id_cip_sec, codi_ps, data_ps in u.getAll(sql, "import"):
        
        if id_cip_sec in poblacio and codi_ps in codis_ps:
            agrupador_ps = codis_ps[codi_ps]
            if agrupador_ps == "Ansietat":
                if data_ext_menys1any < data_ps:
                    sub_problemes.add((id_cip_sec, agrupador_ps))
            elif agrupador_ps == "ASMA persistent":
                sub_problemes.add((id_cip_sec, agrupador_ps))
                
    return sub_problemes

def sub_get_tractaments((codis_atc, codis_pf, codis_antidepressius_recomanats, codis_glucocorticoides_sistemics, dext, particio_taula)):

    # Crear un diccionari per emmagatzemar els tractaments de cada pacient
    sub_tractaments = c.defaultdict(set)
    # Crear un diccionari per emmagatzemar les dates de prescripcions relacionades amb pneum�nia per cada pacient
    dates_prescripcions_lligades_pneumonia = c.defaultdict(set)
    # Crear un conjunt per emmagatzemar els pacients amb historial d'antidepressius recomanats
    historic_antidepressius_recomanats = set()
    # Crear un conjunt per emmagatzemar els pacients amb tractament amb glucocorticoides sist�mics durant com a m�nim 3 mesos
    glucocorticoides_sistemics_minim_3_mesos = set()

    # Crear una cadena de consulta amb codis ATC
    atc_codis_codi_like = "("
    for atc_codi in codis_atc:
        atc_codis_codi_like += "ppfmc_atccodi LIKE '{}' OR ".format(atc_codi)
    atc_codis_codi_like = atc_codis_codi_like[:-5] + "')"

    # Crear una cadena de consulta amb codis PF
    pf_codis_in = "ppfmc_pf_codi in {}".format(tuple(codis_pf))

    # Definir la consulta SQL per obtenir les dades dels tractaments
    sql = """
            SELECT
                id_cip_sec,
                ppfmc_atccodi,
                ppfmc_pf_codi,
                ppfmc_pmc_data_ini,
                ppfmc_data_fi,
                CASE WHEN ppfmc_data_fi >= '{}' OR ppfmc_data_fi = '0000-00-00' THEN 0
                	 ELSE 1
               	END AS tractament_finalitzat
            FROM
                {}
            WHERE
                ({} OR {})
                AND ((ppfmc_data_fi >= '{}' OR ppfmc_data_fi = '0000-00-00') OR ppfmc_atccodi IN {})
            """.format(dext, particio_taula, atc_codis_codi_like, pf_codis_in, dext, codis_antidepressius_recomanats)
    for id_cip_sec, atc_codi, pf_codi, data_ini, data_fi, tractament_finalitzat in u.getAll(sql, "import"):
        # Comprovar si la data de finalitzaci� del tractament �s posterior a la data de c�lcul o si �s nul�la
        if not tractament_finalitzat:
            # Afegir el codi ATC al conjunt de tractaments del pacient
            sub_tractaments[id_cip_sec].add(atc_codi)
            # Comprovar si el codi PF est� dins dels codis PF i afegir-lo al conjunt de tractaments del pacient
            if pf_codi in codis_pf:
                sub_tractaments[id_cip_sec].add(pf_codi)
            # Comprovar si el codi ATC comen�a per "J01" (prescripci� relacionada amb pneum�nia) i afegir la data d'inici del tractament al conjunt de dates per pacient
            if atc_codi[:3] == "J01":
                dates_prescripcions_lligades_pneumonia[id_cip_sec].add(data_ini)
            # Afegir pacients amb tractament de bisfosfonats m�s llargs de 5 anys
            if atc_codi[:5] in codis_glucocorticoides_sistemics and relativedelta(data_fi, data_ini).months >= 3: 
                glucocorticoides_sistemics_minim_3_mesos.add(id_cip_sec)                
        # Comprovar si el codi ATC est� dins dels codis antidepressius recomanats i afegir el pacient al conjunt historic_antidepressius_recomanats
        if atc_codi in codis_antidepressius_recomanats:
            historic_antidepressius_recomanats.add(id_cip_sec)

    return sub_tractaments, dates_prescripcions_lligades_pneumonia, historic_antidepressius_recomanats, glucocorticoides_sistemics_minim_3_mesos

if __name__ == "__main__":
    INDICADORS()