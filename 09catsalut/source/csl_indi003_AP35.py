# -*- coding: latin1 -*-

import collections as c
import sisapUtils as u

validacio = True

db = "nodrizas"
db_khalix = 'catsalut'

tb = "csl_indi"
tb_klx = 'exp_khalix_up_ind'

OutFile = u.tempFolder + 'ap35.txt'

class INDI(object):
    """."""

    def __init__(self):
        """."""

        self.get_dextraccio()
        self.get_centres()
        self.get_poblacio()
        self.get_diagnostics_depressio()
        self.get_tractaments_antidepressius()
        self.get_questionaris_suicidi()
        self.get_den_num()
        self.get_exclosos()
        self.get_resultats()


    def get_dextraccio(self):
        """ . """

        sql = """
                SELECT
                    data_ext
                FROM
                    dextraccio
             """
        self.dext, = u.getOne(sql, "nodrizas")
        self.year, self.month = self.dext.year, self.dext.month

        print("Data d'extraccio: {}".format(self.dext))


    def get_centres(self):
        """."""

        sql = """
                SELECT
                    scs_codi
                FROM
                    cat_centres
              """
        self.centres = set([up for up, in u.getAll(sql, db)])

        print("Nombre centres EAP: {}".format(len(self.centres)))


    def get_poblacio(self):
        """."""

        self.poblacio = dict()

        sql = """
                SELECT
                    id_cip_sec,
                    up,
                    uba,
                    upinf,
                    ubainf,
                    edat,
                    ates,
                    sexe,
                    institucionalitzat,
                    up_residencia,
                    maca
                FROM
                    assignada_tot
                WHERE
                    edat > 14
                    AND maca = 0
              """
        for id_cip_sec, up, uba, upinf, ubainf, edat, ates, sexe, institucionalitzat, resi, maca in u.getAll(sql, db):
            if up in self.centres:
                self.poblacio[id_cip_sec] = {'up': up, 'uba': uba, 'upinf': upinf, 'ubainf': ubainf, 'edat': edat, 'ates': ates, 
                                             'sexe': sexe, 'insti': institucionalitzat, 'residencia': resi, 'maca': maca}


    def get_diagnostics_depressio(self):
        """."""

        agr_diag = (791, 792, 793, 892)
        self.diagnostics_depressio = {}

        sql = """
                SELECT
                    id_cip_sec,
                    dde
                FROM
                    eqa_problemes,
                    dextraccio
                WHERE
                    ps IN {}
                    AND dde <= data_ext
              """.format(agr_diag)
        for id_cip_sec, data_diagnostic in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                if id_cip_sec not in self.diagnostics_depressio:
                    self.diagnostics_depressio[id_cip_sec] = data_diagnostic
                else:
                    if data_diagnostic > self.diagnostics_depressio[id_cip_sec]:   # Ens quedem amb la data més recent
                        self.diagnostics_depressio[id_cip_sec] = data_diagnostic


    def get_tractaments_antidepressius(self):
        """."""

        agr_trac = (473, 474, 898)
        self.tractaments_antidepressius = c.defaultdict(list)

        sql = """
                SELECT
                    id_cip_sec,
                    pres_orig,
                    data_fi
                FROM
                    eqa_tractaments_ep,
                    dextraccio
                WHERE
                    farmac IN {}
                    AND pres_orig <= data_ext
                    AND data_fi > adddate(data_ext, INTERVAL -15 MONTH)
                    AND data_fi >= pres_orig
              """.format(agr_trac)
        for id_cip_sec, data_ini_tractament, data_fi_tractament in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                self.tractaments_antidepressius[id_cip_sec].append((data_ini_tractament, data_fi_tractament))


    def get_questionaris_suicidi(self):
        """."""

        agr_var = {800: "suicidi"}
        self.questionaris_suicidi = c.defaultdict(set)

        sql = """
                SELECT
                    id_cip_sec,
                    data_var
                FROM
                    eqa_variables
                WHERE
                    agrupador = {}
              """.format(agr_var.keys()[0])
        for id_cip_sec, data_questionari in u.getAll(sql, db):
            if id_cip_sec in self.diagnostics_depressio:
                self.questionaris_suicidi[id_cip_sec].add(data_questionari)


    def get_den_num(self):
        """."""

        self.numerador = c.defaultdict(set)
        self.denominador = c.defaultdict(set)
        ind, agr, minim, maxim = ("AP35", "suicidi", -3, 6)

        for id_cip_sec, data_diagnostic in self.diagnostics_depressio.items():
            # Episodis a l'ultim any --> Denominador
            if u.yearsBetween(data_diagnostic, self.dext) == 0:
                self.denominador[ind].add(id_cip_sec)
                # Pel numerador, es comprova que s'hagi fet una avaluació de risc de suicidi entre 3 mesos abans i 6 mesos desprès d'un episodi de depressió
                if any([minim <= u.monthsBetween(data_diagnostic, data_questionari) < maxim for data_questionari in self.questionaris_suicidi[id_cip_sec]]):
                    self.numerador[ind].add(id_cip_sec)
            # Usuaris amb tractament a l'ultim any, que tinguin diagnostic previ i que no tingui un tractament
            # finalitzat en els tres mesos previs a l'inici del nou tractament
            else:
                if id_cip_sec in self.tractaments_antidepressius:
                    for i in range(len(sorted(self.tractaments_antidepressius[id_cip_sec]))):
                        data_ini_tractament = self.tractaments_antidepressius[id_cip_sec][i][0]
                        if u.yearsBetween(data_ini_tractament, self.dext) == 0:
                            # Si el tractament més antic (o l'únic tractament) és de l'últim any, no cal comprovar si ha finalitzat tractament previament. Compta al denominador
                            if i == 0:
                                self.denominador[ind].add(id_cip_sec)
                                # Pel numerador, es comprova que s'hagi fet una avaluació de risc de suicidi entre 3 mesos abans o 6 mesos després d'un episodi de depressió
                                if any([minim <= u.monthsBetween(data_ini_tractament, data_questionari) < maxim for data_questionari in self.questionaris_suicidi[id_cip_sec]]):
                                    self.numerador[ind].add(id_cip_sec)
                            else:
                                # Si l'usuari ha tingut dos o més tractaments, es compara que la data d'inici de cada nou tractament sigui, com a mínim, tres mesos més tard 
                                # que qualsevol de les dates de fi dels tractaments previs
                                if all(u.monthsBetween(data_fi_tractament_anterior, data_ini_tractament) >= 3 for _, data_fi_tractament_anterior in sorted(self.tractaments_antidepressius[id_cip_sec])[:i]):
                                    self.denominador[ind].add(id_cip_sec)
                                    # Pel numerador, es comprova que s'hagi fet una avaluació de risc de suicidi entre 3  mesos abans o 6 mesos després d'un episodi de depressió
                                    if any([minim <= u.monthsBetween(data_ini_tractament, data_questionari) < maxim for data_questionari in self.questionaris_suicidi[id_cip_sec]]):
                                        self.numerador[ind].add(id_cip_sec)


    def get_exclosos(self):
        """."""

        self.exclosos = c.defaultdict(set)
        exclosos_ind = {"AP35": 86}

        for ind in exclosos_ind.keys():
            sql = """
                    SELECT
                        id_cip_sec
                    FROM
                        eqa_problemes
                    WHERE
                        ps = {}
                  """.format(exclosos_ind[ind])
            self.exclosos[ind] = set([id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in self.denominador[ind]])

            for id_cip_sec, _ in self.poblacio.items():
                if self.poblacio[id_cip_sec]["insti"]:
                    self.exclosos[ind].add(id_cip_sec)


    def get_resultats(self):
        """."""

        self.master = {}
        self.llistats = []
        self.khalix = c.Counter()

        for ind, pacients in self.denominador.items():
            for id_cip_sec in pacients:
                self.master[id_cip_sec] = {"ind": ind,
                                           "up": self.poblacio[id_cip_sec]["up"],
                                           "uba": self.poblacio[id_cip_sec]["uba"],
                                           "upinf": self.poblacio[id_cip_sec]["upinf"],
                                           "ubainf": self.poblacio[id_cip_sec]["ubainf"],
                                           "edat": self.poblacio[id_cip_sec]["edat"],
                                           "sexe": self.poblacio[id_cip_sec]["sexe"],
                                           "ates": self.poblacio[id_cip_sec]["ates"],
                                           "maca": self.poblacio[id_cip_sec]["maca"],
                                           "insti": self.poblacio[id_cip_sec]["insti"],
                                           "den": 1 if id_cip_sec in self.denominador[ind] else 0,
                                           "num": 1 if id_cip_sec in self.numerador[ind] else 0,
                                           "excl": 1 if id_cip_sec in self.exclosos[ind] else 0
                }

        with u.openCSV(OutFile) as csv_file:
            for id_cip_sec, d in self.master.items():
                csv_file.writerow([id_cip_sec, d["ind"], d['up'], d['uba'], d['ubainf'], d['edat'],
                            d['sexe'], d['ates'], d['insti'], d['maca'], d['num'], d['den'], d['excl']])

        u.execute('drop table if exists {}'.format(tb), "catsalut")
        u.execute("create table {} (id_cip_sec double null, indicador varchar(10) not null default'', up varchar(5) not null default'',uba varchar(5) not null default '',ubainf varchar(5) not null default '',edat double,sexe varchar(1) not null default'',ates double,institucionalitzat double,maca double,num double,den double,excl double)".format(tb), "catsalut")
        u.loadData(OutFile, tb, "catsalut")


if __name__ == "__main__":
    INDI()