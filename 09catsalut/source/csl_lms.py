from sisapUtils import getAll, ageConverter, sexConverter, execute, listToTable, getOne, yearsBetween
from collections import defaultdict


tb_pac = 'mst_indicadors_pacient'
tb_klx = 'exp_khalix_up_ind'
db = 'catsalut'
ind = 'LMS01AP'
ind_anual = 'LMS02AP'


data_ext, = getOne('select data_ext from dextraccio', 'nodrizas')
resultat = defaultdict(int)
pacients = []
sql = 'select id_cip_sec, up, uba, ubainf, edat, sexe, ates, institucionalitzat, maca, \
       lms_peticio, lms_connexio \
       from assignada_tot_with_jail where edat >= 16'
for id, up, uba, ubainf, edat, sexe, ates, instit, maca, peticio, connexio in getAll(sql, 'nodrizas'):
    pacients.append((id, ind, up, uba, ubainf, edat, sexe, ates, instit, maca, 1 if peticio or connexio else 0, 1, 0))
    edat = ageConverter(edat)
    sexe = sexConverter(sexe)
    peticio_anual = peticio and yearsBetween(peticio, data_ext) == 0
    connexio_anual = connexio and yearsBetween(connexio, data_ext) == 0
    instit = '{}INS'.format('NO' if instit == 0 else '')
    grups = ['{}ASS'.format(instit)]
    if ates == 1:
        grups.append('{}AT'.format(instit))
    for grup in grups:
        resultat[(up, edat, sexe, grup, '{}A'.format(ind), 'DEN')] += 1
        resultat[(up, edat, sexe, grup, '{}B'.format(ind), 'DEN')] += 1
        resultat[(up, edat, sexe, grup, '{}A'.format(ind_anual), 'DEN')] += 1
        resultat[(up, edat, sexe, grup, '{}B'.format(ind_anual), 'DEN')] += 1
        if peticio or connexio:
            resultat[(up, edat, sexe, grup, '{}A'.format(ind), 'NUM')] += 1
            resultat[(up, edat, sexe, grup, '{}C'.format(ind), 'DEN')] += 1
        if peticio_anual or connexio_anual:
            resultat[(up, edat, sexe, grup, '{}A'.format(ind_anual), 'NUM')] += 1
            resultat[(up, edat, sexe, grup, '{}C'.format(ind_anual), 'DEN')] += 1
        if connexio:
            resultat[(up, edat, sexe, grup, '{}B'.format(ind), 'NUM')] += 1
            resultat[(up, edat, sexe, grup, '{}C'.format(ind), 'NUM')] += 1
        if connexio_anual:
            resultat[(up, edat, sexe, grup, '{}B'.format(ind_anual), 'NUM')] += 1
            resultat[(up, edat, sexe, grup, '{}C'.format(ind_anual), 'NUM')] += 1


# execute("delete a.* from {} where indicador = ''".format(tb_pac, ind), db)
listToTable(pacients, tb_pac, db)
            
# execute("delete a.* from {} a where indicador like '{}%'".format(tb_klx, ind), db)
listToTable([key + (n,) for key, n in resultat.items()], tb_klx, db)
