# -*- coding: utf-8 -*-

from sisapUtils import *
import csv,os
from time import strftime

db="pdp"
conn = connect(db)
c=conn.cursor()

c.execute("""
            INSERT INTO
                ALTCATALEGPARE
            VALUES 
                (2023, 'EQPFP_V', 'Indicadors de prescripció activa validació', 9, 'FARMAVAL')
          """)
c.execute("""
            INSERT INTO
                ALTCATALEG
            SELECT
                DATAANY,
                CONCAT(indicador, '_V'),
                literal,
                ORDRE,
                'EQPFP_V',
                llistat,
                invers,
                mmin,
                mint,
                mmax,
                toshow,
                wiki,
                tipusvalor
            FROM
                ALTCATALEG
            WHERE
                indicador IN ('IMP022', 'IMP023', 'IMP024', 'IMP025', 'IMP026', 'IMP027', 'IMP028', 'IMP029', 'IMP030', 'IMP031', 'IMP032', 'IMP033', 'IMP034', 'IMP035', 'IMP036', 'IMP037')
          """)
c.execute("""
            DELETE FROM
                ALTLLISTATS
            WHERE
                INDICADOR IN ('IMP022_V', 'IMP023_V', 'IMP024_V', 'IMP025_V', 'IMP026_V', 'IMP027_V', 'IMP028_V', 'IMP029_V', 'IMP030_V', 'IMP031_V', 'IMP032_V', 'IMP033_V', 'IMP034_V', 'IMP035_V', 'IMP036_V', 'IMP037_V')
          """)
c.execute("""
            INSERT INTO
                ALTLLISTATS
            SELECT
                UP,
                UAB,
                TIPUS,
                CONCAT(INDICADOR, '_V'),
                IDPAC,
                SECTOR,
                EXCLOS
            FROM
                ALTLLISTATS
            WHERE
                INDICADOR IN ('IMP022', 'IMP023', 'IMP024', 'IMP025', 'IMP026', 'IMP027', 'IMP028', 'IMP029', 'IMP030', 'IMP031', 'IMP032', 'IMP033', 'IMP034', 'IMP035', 'IMP036', 'IMP037')
                AND ((up = '00195' AND UAB = 'MG004')
                  OR (up = '00195' AND UAB = 'MG004')
                  OR (UP = '00371' AND UAB = 'UAB06')
                  OR (up = '00106' AND UAB = 'UAB-D')
                  OR (up = '08208' AND UAB = 'MG11'))
          """)
c.execute("""
            INSERT INTO
                ALTINDICADORS
            SELECT
                DATAANY,
                DATAMES,
                UP,
                UAB,
                TIPUS,
                CONCAT(INDICADOR, '_V'),
                NUMERADOR,
                DENOMINADOR,
                RESULTAT,
                NORESOLTS,
                MMIN,
                MMAX
            FROM
                ALTINDICADORS a
            WHERE
                INDICADOR IN ('IMP022', 'IMP023', 'IMP024', 'IMP025', 'IMP026', 'IMP027', 'IMP028', 'IMP029', 'IMP030', 'IMP031', 'IMP032', 'IMP033', 'IMP034', 'IMP035', 'IMP036', 'IMP037')
                AND ((up = '00195' AND UAB = 'MG004' AND TIPUS = 'M')
                  OR (up = '00195' AND UAB = 'MG004' AND TIPUS = 'M')
                  OR (UP = '00371' AND UAB = 'UAB06' AND TIPUS = 'M')
                  OR (up = '00106' AND UAB = 'UAB-D' AND TIPUS = 'M')
                  OR (up = '08208' AND UAB = 'MG11'  AND TIPUS = 'M'))
                AND DATAANY = 2023
                AND DATAMES = 2
          """)