from sisapUtils import *
import csv,os
from time import strftime
from collections import defaultdict
print strftime("%Y-%m-%d %H:%M:%S")

db="catsalut"

path="./dades_noesb/csl_ind.txt"
pathr="./dades_noesb/csl_relacio.txt"

nod ='nodrizas'
ass_emb="nodrizas.ass_embaras"
visites="nodrizas.ass_visites"
dext="nodrizas.dextraccio"
tabac="nodrizas.eqa_tabac"
ass_tabac="ass_tabac"
variables="nodrizas.ass_variables"
agrupador_puerperi=389
poblacio="nodrizas.ass_poblacio"

puerperi="ass_puerperi"
embaras="csl_embaras"
visembaras1="csl_visites_embarassades1"
visembaras="csl_visites_embarassades"
den="indicador"
indicadors_3omesvisites = set()

with open(pathr, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,tpob,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11],cat[12]
      if tipus in ("ASS", "ASS_3OMES"):
         t="%s_%s" % (den,ind)
         t2="%s_%s_1" % (den,ind)
         execute("drop table if exists %s" % t, db)
         execute("create table %s (id_cip_sec int,num double,den double,excl double)" % t, db)
         execute("drop table if exists %s" % t2, db)
         execute("create table %s (id_cip_sec int)" % t2, db)
         if tipus == "ASS_3OMES":
            indicadors_3omesvisites.add(ind)
         
sql = "select date_add(data_ext,interval - 12 month),date_add(data_ext,interval - 15 day),date_add(data_ext, interval + 6 month),date_add(data_ext,interval - 3 year),data_ext,date_add(data_ext,interval - 75 day) from %s" % (dext)
fa1any,fa15dies,en6mesos,fa3anys,avui,fa75dies,=getOne(sql, db)

execute("drop table if exists %s" % embaras, db)
execute("create table %s like %s" % (embaras,ass_emb), db)
execute("insert into %s select * from %s where inici>'%s' or fi >'%s'"  % (embaras,ass_emb,fa1any,fa1any), db)
execute("alter table %s add index(id_cip_sec,inici,fi,risc)" % embaras, db)

execute("drop table if exists %s" % puerperi, db)
execute("create table %s (id_cip_sec int, dat date)" % puerperi, db)
execute("insert into %s select id_cip_sec,dat from %s where agrupador=%s " % (puerperi,variables,agrupador_puerperi), db)
execute("alter table %s add index (id_cip_sec,dat)" % puerperi, db)

#creo taula visites de dones assir

execute("drop table if exists %s" % visembaras1, db)
execute("create table %s (id_cip_sec int, risc varchar(15),inici date, fi date,visi_data_visita date,sg double)" % visembaras1, db)
execute("insert into %s select a.id_cip_sec,risc,inici,fi,visi_data_visita,(datediff(fi,visi_data_visita))/7 as sg from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where visi_data_visita between inici and fi" % (visembaras1,embaras,visites), db)
execute("drop table if exists %s" % visembaras, db)
execute("create table %s (id_cip_sec int, risc varchar(15),recompte double)" % visembaras, db)
execute("insert into %s select id_cip_sec,risc,count(*) recompte from %s group  by id_cip_sec,risc" % (visembaras,visembaras1), db)

execute("drop table if exists %s" % ass_tabac, db)
execute("create table %s (id_cip_sec int, agrupador int,val_txt varchar(10) not null default'', embaras int, puerperi int)" % ass_tabac, db)
execute("insert into %s select a.id_cip_sec,agrupador,val_txt,if(dat between inici and fi, 1, 0) embaras, if(dat between date_add(fi, interval 35 day) and date_add(fi, interval 75 day), 1, 0) puerperi from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec" % (ass_tabac,variables,embaras), db)

#algunes definicions

sql = 'select data_ext from dextraccio'
for dat, in getAll(sql, nod):
    ddext = dat

def get_consumOH():
    ConsumOH = {}
    sql = 'select id_cip_sec, data_var from eqa_variables where agrupador=84'
    for id, data in getAll(sql, nod):
        mesos = monthsBetween(data, ddext)
        if 0 <= mesos <= 23:
            ConsumOH[id] = True
    sql = 'select id_cip_sec, dat from {} where agrupador=391'.format(variables)
    for id, data in getAll(sql, nod):
        mesos = monthsBetween(data, ddext)
        if 0 <= mesos <= 23:
            ConsumOH[id] = True
    sql = 'select id_cip_sec from eqa_problemes where ps = 85'
    for id, in getAll(sql, nod):
        ConsumOH[id] = True
    return ConsumOH


def get_TosFerina():
    tos_ferina = {}
    sql = 'select id_cip_sec, datamax from eqa_vacunes where agrupador=631'
    for id, data in getAll(sql, nod):
        tos_ferina[id] = {'data': data}
    return tos_ferina
    
    
with open(pathr, 'rb') as file:
   d=csv.reader(file, delimiter='@', quotechar='|')
   for cat in d:
      ind,tipus,tpob,emin,emax,sexe,agr,f,tmin,tmax,vmin,vmax,uvalor = cat[0],cat[1],cat[2],cat[3],cat[4],cat[5],cat[6],cat[7],cat[8],cat[9],cat[10],cat[11],cat[12]
      t="%s_%s" % (den,ind)
      t2="%s_%s_1" % (den,ind)
      if tipus=="ASSRISC":
         execute("insert into %s select id_cip_sec from %s where recompte>=%s" % (t2,visembaras,vmax), db)
         execute("alter table %s add index(id_cip_sec)" % t2, db)
         execute("insert into %s select id_cip_sec, 0 num,1 den, 0 excl from %s where risc like ('%%alt%%') and fi<'%s'" % (t,embaras,avui), db)
         execute("alter table %s add index(id_cip_sec)" % t, db)
         execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set den=den+1 where sg<=12" % (t,visembaras1), db)
         execute("update %s a set den=0 where den<>2" % (t), db)
         execute("update %s a set den=1 where den=2" % (t), db)        
         execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1" % (t,t2), db)
      elif tipus=="ASSTABAC":
         execute("insert into %s select a.id_cip_sec from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where tab=%s and dalta between inici and fi " % (t2,embaras,tabac,vmax), db)
         execute("alter table %s add index(id_cip_sec)" % t2, db)
         execute("insert into %s select a.id_cip_sec, 0 num,1 den, 0 excl from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where tab=1 and dalta<inici and dbaixa>inici group by id_cip_sec" % (t,embaras,tabac), db)
         execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1" % (t,t2), db)
         execute("alter table %s add index(id_cip_sec)" % t, db)
      elif tipus=="ASSTABACA":
         execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=%s where %s=1 and agrupador=%s and val_txt='%s'" % (t,ass_tabac,vmax,tmin,agr,vmin), db)
      elif tipus=="ASS1VIS": 
         execute("insert into %s select id_cip_sec from %s a where visi_data_visita between inici and date_add(inici,interval + %s week)" % (t2,visembaras1,tmax), db)
         execute("alter table %s add index(id_cip_sec)" % t2, db)
         execute("insert into %s select id_cip_sec, 0 num,1 den, 0 excl from %s where inici > date_add('%s',interval - 10 week)" % (t,embaras,fa1any), db)
         execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1" % (t,t2), db)
      elif tipus=="ASSPUERP": 
         execute("insert into %s select distinct a.id_cip_sec from %s a inner join %s b on a.id_cip_sec=b.id_cip_sec where dat between fi and date_add(fi,interval + %s day) and emb_c_tanca != 'Tr'" % (t2,embaras,puerperi,tmax), db)
         execute("alter table %s add index(id_cip_sec)" % t2, db)
         execute("insert into %s select id_cip_sec, 0 num,1 den, 0 excl from %s where fi <= '%s' and emb_c_tanca != 'Tr'" % (t,embaras,fa15dies), db)
         execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1" % (t,t2), db)
      elif tipus=="ASSCITO": 
         execute("insert into %s select id_cip_sec, 0 num,1 den, 0 excl from %s where edat between %s and %s" % (t,poblacio,emin,emax), db)
         execute("alter table %s add index(id_cip_sec)" % t, db)
         execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1 where agrupador=203 and  STR_TO_DATE(replace(replace(replace(replace(val_txt,'ABR','APR'),'DIC','DEC'),'ENE','JAN'),'AGO','AUG'),'%%d-%%M-%%Y') between '%s' and '%s'" % (t,variables,fa3anys,avui), db)
         execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1 where agrupador=204 and val_num=1 and dat between '%s' and '%s'" % (t,variables,fa3anys,avui), db)
         execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1 where agrupador in ('203','400')  and dat between '%s' and '%s'" % (t,variables,fa3anys,avui), db)
         execute("update %s a inner join %s b on a.id_cip_sec=b.id_cip_sec set num=1 where agrupador=205  and STR_TO_DATE(replace(replace(replace(replace(val_txt,'ABR','APR'),'DIC','DEC'),'ENE','JAN'),'AGO','AUG'),'%%d-%%M-%%Y') between '%s' and '%s'" % (t,variables,avui,en6mesos), db)
      elif tipus=="OH":
         upload = []
         ConsumOH = get_consumOH()
         sql = 'select id_cip_sec from {}'.format(embaras)
         for id, in getAll(sql, db):
            denom, num, excl = 1, 0, 0
            if id in ConsumOH:
                num = 1
            upload.append([id, num, denom, excl])
         listToTable(upload, t, db)
      elif tipus == "DTPA":
        upload = []
        tos_ferina = get_TosFerina()
        sql = 'select id_cip_sec, inici, fi from {} where temps >265'.format(embaras)
        for id, inici, final in getAll(sql, db):
            denom, num, excl = 1, 0, 0
            if id in tos_ferina:
                dtos = tos_ferina[id]['data']
                if inici <= dtos <= final:
                    num =1
            upload.append([id, num, denom, excl])
        listToTable(upload, t, db)
      elif tipus == 'IVE':
        denom = set()
        num = set()
        visites = defaultdict(set)
        sql = 'select id_cip_sec, visi_data_visita from ass_visites'
        for id, dat in getAll(sql, nod):
            visites[id].add(dat)
        sql = "select id_cip_sec, dat from ass_variables where agrupador = 636 and val_num = 0"
        for id, dat in getAll(sql, nod):
            denom.add(id)
            if any([0 < (vis - dat).days < 30 for vis in visites[id]]):
                num.add(id)
        resul = [(id, 1 * (id in num), 1, 0) for id in denom]
        listToTable(resul, t, db)
      elif tipus == "copiar":
        if vmax == 'den':
            execute('insert into {} select id_cip_sec, 0 num, 1 den, 0 excl from {}_{} {}'.format(t, den, agr, 'where num = 1' if vmin == 'num' else ''), db)
            execute('alter table {} add index (id_cip_sec)'.format(t), db)
            if ind == 'IASSIR12':
                execute("update {} set num = 1".format(t), db)
                execute("update {} a inner join {} b on a.id_cip_sec = b.id_cip_sec set excl = 1 where b.fi > '{}'".format(t, embaras, fa75dies), db)
        else:
            raise AttributeError('vmax {} not implemented'.format(vmax))
      
      excloure_menys_3visites = 1 if ind in indicadors_3omesvisites else 0
      if excloure_menys_3visites:
        execute("DELETE FROM {} WHERE id_cip_sec NOT IN (SELECT id_cip_sec FROM nodrizas.ass_poblacio_3omes)".format(t), db)
        execute("DELETE FROM {} WHERE id_cip_sec NOT IN (SELECT id_cip_sec FROM nodrizas.ass_poblacio_3omes)".format(t2), db)


print strftime("%Y-%m-%d %H:%M:%S")
