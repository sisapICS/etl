# coding: latin1


import sisapUtils as u


# FARMINDICADORS
# NOTA, SI DEN = 0 NO ES VEU INDICADOR!!!
sqls = [
    """drop table if exists catsalut.exp_ecap_farm_ind""",
    """create table catsalut.exp_ecap_farm_ind as (
            select
                up,
                uba,
                tipus,
                indicador,
                resolts,
                detectats,
                resultat,
                llistat
            from
                eqa_ind.exp_ecap_uba a
            inner join nodrizas.cat_centres b
            on
                a.up = b.scs_codi
            where 
            tipus = 'M' and 
            INDICADOR in (
            'EQA0228',
            'EQA0230',
            'EQA0238',
            'EQA0222',
            'EQA0226',
            'EQA0216'))""",
    """insert into catsalut.exp_ecap_farm_ind
        select
            up,
            uba,
            tipus,
            indicador,
            numerador,
            denominador,
            resultat,
            noresolts
        from
            altres.exp_ecap_alt_uba
        where TIPUS = 'M' AND 
            indicador in (
            'IF249P',
            'IF301P',
            'IF292P',
            'IF254P',
            'FARM0007',
            'IF254PA',
            'IF269P',
            'IF280P',
            'IF254PB',
            'FARM0005',
            'IF263P',
            'IF299P',
            'FARM0009',
            'FARM0006',
            'FARM0008')""",
    """insert into catsalut.exp_ecap_farm_ind
        select
            up,
            uba,
            tipus,
            ind,
            num,
            den,
            res,
            noresolts
        from
            catsalut.exp_ecap_IMP_uba""",
    """insert into catsalut.exp_ecap_farm_ind
        select
            up,
            uba,
            tipus,
            indicador,
            numerador,
            denominador,
            resultat,
            denominador-numerador
        from
            catsalut.exp_ecap_uba
        where  TIPUS = 'M' AND 
        indicador in ('IGFM04AP', 'IGFM05AP')"""
        ,
    """insert into catsalut.exp_ecap_farm_ind
        select
            up,
            uab,
            tipus,
            indicador,
            numerador,
            denominador,
            resultat,
            noresolts
        from catsalut.impindicadors
    """,
    """insert into catsalut.exp_ecap_farm_ind
        select
            up,
            uba,
            tipus,
            indicador,
            numerador,
            denominador,
            resultat,
            numerador
        from altres.exp_ecap_alertes_uba
        WHERE indicador in ('FARM0001', 'FARM0002', 'FARM0004')
    """
]
for sql in sqls:
    u.execute(sql, 'catsalut')

# FARMLLISTATS
sqls = [
    """drop table if exists catsalut.exp_ecap_farm_llis""",
    """create table catsalut.exp_ecap_farm_llis as (select
            up,
            uba,
            'M',
            grup_codi,
            hash_d,
            a.sector,
            status,
            valor
        from
            EQA_IND.exp_ecap_pacient_new A
        inner join nodrizas.cat_centres b on
            a.up = b.scs_codi
        where
            exists (
            select
                1
            from
                eqa_ind.mst_ubas b
            where
                a.up = b.up
                and a.uba = b.uba
                and tipus = 'M')
            and grup_codi in (
                    'EQA0228',
                    'EQA0230',
                    'EQA0238',
                    'EQA0222',
                    'EQA0226',
            'EQA0216'))""",

    """insert
            into
            catsalut.exp_ecap_farm_llis
        select
            upinf,
            ubainf,
            'I',
            grup_codi,
            hash_d,
            a.sector,
            status,
            valor
        from
            EQA_IND.exp_ecap_pacient_new a
        inner join nodrizas.cat_centres b on
            a.up = b.scs_codi
        where
            exists (
            select
                1
            from
                eqa_ind.mst_ubas b
            where
                a.upinf = b.up
                and a.ubainf = b.uba
                and tipus = 'I')
            and grup_codi in 
        (
                    'EQA0228',
                    'EQA0230',
                    'EQA0238',
                    'EQA0222',
                    'EQA0226',
            'EQA0216')""",
        
        """insert
            into
            catsalut.exp_ecap_farm_llis 
                select
                    up,
                    uba,
                    'M' as tipus,
                    grup_codi,
                    hash_d,
                    sector,
                    exclos,
                    comentari
                from
                    altres.exp_ecap_alt_pacient
                where
                    uba <> ''
                    and grup_codi in (
                            'IF249P',
                            'IF301P',
                            'IF292P',
                            'IF254P',
                            'FARM0007',
                            'IF254PA',
                            'IF269P',
                            'IF280P',
                            'IF254PB',
                            'FARM0005',
                            'IF263P',
                            'IF299P',
                            'FARM0009',
                            'FARM0006',
                            'FARM0008')""",
        
        """insert
            into
            catsalut.exp_ecap_farm_llis 
                select
                    up,
                    ubainf,
                    'I' as tipus,
                    grup_codi,
                    hash_d,
                    sector,
                    exclos,
                    comentari
                from
                    altres.exp_ecap_alt_pacient
                where
                    ubainf <> ''
                    and grup_codi in (
                            'IF249P',
                            'IF301P',
                            'IF292P',
                            'IF254P',
                            'FARM0007',
                            'IF254PA',
                            'IF269P',
                            'IF280P',
                            'IF254PB',
                            'FARM0005',
                            'IF263P',
                            'IF299P',
                            'FARM0009',
                            'FARM0006',
                            'FARM0008')""",   
        """insert
            into
            catsalut.exp_ecap_farm_llis 
                select
                    up,
                    uba,
                    'M' as tipus,
                    grup_codi,
                    hash_d,
                    sector,
                    exclos,
                    ''
                from
                    catsalut.exp_ecap_IMP_pacient
                where
                    uba != ''""",   
        """insert
            into
            catsalut.exp_ecap_farm_llis 
                select
                    upinf,
                    ubainf,
                    'I' as tipus,
                    grup_codi,
                    hash_d,
                    sector,
                    exclos,
                    ''
                from
                    catsalut.exp_ecap_IMP_pacient
                where
                    grup_codi in (
                            'IMP033')
                    and ubainf != ''""",
        """insert
            into
            catsalut.exp_ecap_farm_llis 
                select
                    up,
                    uba,
                    tipus,
                    indicador,
                    hash,
                    sector,
                    exclos,
                    ''
                from
                    catsalut.exp_ecap_pacient
                where indicador in ('IGFM04AP', 'IGFM05AP')"""
        ,"""insert into catsalut.exp_ecap_farm_llis
                select
                    up,
                    uab,
                    tipus,
                    indicador,
                    idpac,
                    sector,
                    exclos,
                    comentari
                from
                    catsalut.impllistats"""   
        ,"""insert into catsalut.exp_ecap_farm_llis
                select
                    up,
                    uba,
                    'M',
                    grup_codi,
                    hash_d,
                    sector,
                    exclos,
                    null
                from
                    altres.exp_ecap_alertes_pacient
                where grup_codi in ('FARM0001', 'FARM0002', 'FARM0004')"""    
        ,"""insert into catsalut.exp_ecap_farm_llis
                select
                    upinf,
                    ubainf,
                    'I',
                    grup_codi,
                    hash_d,
                    sector,
                    exclos,
                    null
                from
                    altres.exp_ecap_alertes_pacient
                where grup_codi in ('FARM0001', 'FARM0002', 'FARM0004')"""    
]

for sql in sqls:
    u.execute(sql, 'catsalut')

# FARMCATALEG
sqls = [
    """drop table if exists catsalut.exp_ecap_farm_cat""",
    """create table catsalut.exp_ecap_farm_cat as (
        select * from EQA_IND.EXP_ECAP_CATALEG
        where indicador in (
                    'EQA0228',
                    'EQA0230',
                    'EQA0238',
                    'EQA0222',
                    'EQA0226',
                    'EQA0216'))""",
    """alter table catsalut.exp_ecap_farm_cat modify wiki varchar(300)""",
    """insert into catsalut.exp_ecap_farm_cat
            select
                indicador, literal, ordre, pare, llistat, invers, 0, mmin, mint, mmax, toshow, wiki, '', 0, 0, ''
            from
                ALTRES.exp_ecap_alt_cataleg
            where
                INDICADOR in (
                        'IF249P',
                        'IF301P',
                        'IF292P',
                        'IF254P',
                        'FARM0007',
                        'IF254PA',
                        'IF269P',
                        'IF280P',
                        'IF254PB',
                        'FARM0005',
                        'IF263P',
                        'IF299P',
                        'FARM0009',
                        'FARM0006',
                        'FARM0008')""",
    """insert into catsalut.exp_ecap_farm_cat
            select
	            indicador, literal, ordre, pare, llistat, invers, 0, mmin, mint, mmax, toshow, wiki,  '', 0, 0, ''
            from
                altres.exp_ecap_IMP_cataleg""",
    """insert into catsalut.exp_ecap_farm_cat
            select 'IGFM04AP', 'Pacients amb combinació de benzodiazepines i opioides forts', 1, 'IMP', 1, '', 0, '', '', '', 1, 'http://10.80.217.201/sisap-umi/indicador/codi/IGFM04AP', '', 0, 0, '' from dual""",
    """insert into catsalut.exp_ecap_farm_cat
            select 'IGFM05AP', 'Pacients amb combinació de pregabalina o gabapentina i opioides forts', 2, 'IMP', 1, '', 0, '', '', '', 1, 'http://10.80.217.201/sisap-umi/indicador/codi/IGFM05AP', '', 0, 0, '' from dual"""
]
for sql in sqls:
    u.execute(sql, 'catsalut')


# FARMCATALEGPARE
sqls = [
    """drop table if exists catsalut.exp_ecap_farm_catpare""",
    """create table catsalut.exp_ecap_farm_catpare as ( 
        select * from EQA_IND.EXP_ECAP_CATALEGPARE
        where pare in (select pare from EQA_IND.EXP_ECAP_CATALEG
            where indicador in (
                        'EQA0228',
                        'EQA0230',
                        'EQA0238',
                        'EQA0222',
                        'EQA0226',
                        'EQA0216')))""",
    """insert into catsalut.exp_ecap_farm_catpare 
        select
                pare, literal, ordre, '', pantalla
            from
                ALTRES.exp_ecap_alt_catalegPare
            where
                pare in (select pare from ALTRES.exp_ecap_alt_cataleg
                    where 
                    INDICADOR in (
                            'IF249P',
                            'IF301P',
                            'IF292P',
                            'IF254P',
                            'FARM0007',
                            'IF254PA',
                            'IF269P',
                            'IF280P',
                            'IF254PB',
                            'FARM0005',
                            'IF263P',
                            'IF299P',
                            'FARM0009',
                            'FARM0006',
                            'FARM0008'))""",
        """insert into catsalut.exp_ecap_farm_catpare
                select
                    pare,
                    literal,
                    ordre,
                    '',
                    pantalla
                from
                    altres.exp_ecap_IMP_catalegPare"""]

for sql in sqls:
    u.execute(sql, 'catsalut')


#### EXPORT PDP #####
table = """farmindicadors"""
query = """select * from catsalut.exp_ecap_farm_ind where uba != ''"""
u.exportPDP(query=query, table=table, dat=True,fmo=False)  # indicadors
table = """farmllistats"""
query = """select * from catsalut.exp_ecap_farm_llis"""
u.exportPDP(query=query,table=table,truncate=True,pkOut=True,pkIn=True,fmo=False)
table = """farmcataleg"""
query = """select indicador, literal, ordre_grup, pare, llistat, invers, mmin, mint, mmax, toshow, wiki, '' from catsalut.exp_ecap_farm_cat"""
u.exportPDP(query=query, table=table, datAny=True,fmo=False)  # cataleg
table = """farmcatalegpare"""
query = """select pare, literal, ordre, proces from catsalut.exp_ecap_farm_catpare"""
u.execute("""UPDATE farmcatalegpare SET pantalla = 'EQA' WHERE dataany = 2024 AND pare IN ('EQAG06', 'EQAG12')""", 'pdp')
u.exportPDP(query=query, table=table, datAny=True,fmo=False)  # catalegpare
table = """farmLlistatsData"""
query = """select data_ext from nodrizas.dextraccio"""
u.exportPDP(query=query,table=table,truncate=True,fmo=False)
table = """farmcatalegexclosos"""
query = """select * from altres.exp_ecap_alt_catalegExclosos"""
u.exportPDP(query=query,table=table,truncate=True,fmo=False)
# table = """altllistats"""
# query = """select * from catsalut.impllistats where indicador like 'IMP04%'"""
# u.exportPDP(query=query,table=table,truncate=False,fmo=False)
# table = """altindicadors"""
# query = """select * from catsalut.impindicadors where indicador like 'IMP04%' and uab is not null and uab <> ''"""
# u.exportPDP(query=query,table=table,truncate=False,fmo=False)
