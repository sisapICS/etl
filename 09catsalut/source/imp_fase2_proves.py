# -*- coding: utf-8 -*-

import sisapUtils as u
import sys
import collections as c
import dateutil.relativedelta
from datetime import timedelta

IS_TEST = False

# Definir les taules de la base de dades
taula_cataleg = "exp_ecap_IMP_cataleg"
taula_cataleg_pare = "exp_ecap_IMP_catalegPare"
taula_pacients = "exp_ecap_IMP_pacient"

INDICADORS_INVERSOS = {
                       "IMP033": 1
                      }


ubas_validacio = [("00195", "MG004"),
                  ("00371", "UAB06"),
                  ("00106", "UAB-D"),
                  ("08208", "MG11")]                       
                  
ubas_validacio = [("00195", "MG004"),
                  ("00371", "UAB06"),
                  ("00106", "UAB-D"),
                  ("08208", "MG11")]                       

class INDICADORS():

    def __init__(self):
        """."""         

        # M�todes necessaris per calcular els indicadors
        self.get_dextraccio();                              print("self.get_dextraccio()")
        self.get_centres();                                 print("self.get_centres()")
        self.get_poblacio();                                print("self.get_poblacio()")
        #self.get_problemes();                               print("self.get_problemes()")
        #self.get_variables();                               print("self.get_variables()")
        self.get_tractaments();                             print("self.get_tractaments()")
        #self.get_episodis_pneumonia_considerats();          print("self.get_episodis_pneumonia_considerats()")
        #self.get_tractaments_correctes_pneumonia();         print("self.get_tractaments_correctes_pneumonia()")
        self.get_RAM();                                     print("self.get_RAM()")
        self.get_indicadors();                              print("self.get_indicadors()")

        self.create_tables_Khalix();                        print("self.create_tables_Khalix()")

        self.export_cataleg();                              print("self.export_cataleg()")
        self.export_cataleg_pare();                         print("self.export_cataleg_pare()")
        self.export_taula_indicadors();                     print("self.export_taula_indicadors()")
        self.export_taula_pacients();                       print("self.export_taula_pacients()")

        if IS_TEST:
            self.export_khalix();                           print("self.export_khalix()")        

    def get_dextraccio(self):
        """Obtenir la data d'extracci�."""

        # Consulta SQL per obtenir la data de c�lcul
        sql = """
                SELECT
                    data_ext
                FROM
                    dextraccio
             """
        self.dext, = u.getOne(sql, "nodrizas")
        self.dext_menys1any = self.dext - dateutil.relativedelta.relativedelta(years=1)
        self.dext_menys_15_mesos = self.dext - dateutil.relativedelta.relativedelta(months=15)
        self.periode = "A{}".format(self.dext.strftime("%y%m"))

    def get_centres(self):
        """Obtenir la informaci� dels Centres d'Atenci� Prim�ria."""

        sql = """
                SELECT
                    scs_codi,
                    ics_codi
                FROM
                    cat_centres
              """
        self.centres = {up: br for up, br in u.getAll(sql, "nodrizas")}

    def get_poblacio(self):
        """Obtenir la informaci� de la poblaci�."""

        # Inicialitzar diccionaris per emmagatzemar la poblaci� assignada adulta i un diccionari conversor id_cip_sec a hash
        self.poblacio = dict()
        self.idcipsec2hash = dict()

        # Consulta SQL per obtenir la informaci� de la poblaci� amb edat igual o superior a 15 anys
        sql = """
                SELECT
                    id_cip_sec,
                    codi_sector,
                    up,
                    upinf,
                    uba,
                    ubainf,
                    edat,
                    sexe,
                    ates,
                    maca,
                    institucionalitzat
                FROM
                    assignada_tot
                WHERE
                    edat >= 15
            """
        for id_cip_sec, sector, up, upinf, uba, ubainf, edat, sexe, ates, maca, institucionalitzat in u.getAll(sql, "nodrizas"):
            if up in self.centres:
                self.poblacio[id_cip_sec] = {"up": up,
                                            "upinf": upinf,
                                            "uba": uba,
                                            "ubainf": ubainf,
                                            "edat": edat,
                                            "sexe": sexe,
                                            "ates": ates,
                                            "maca": maca,
                                            "institucionalitzat": institucionalitzat,
                                            "sector": sector}

        # Consulta SQL per obtenir els parells id_cip_sec i hash_d, per emmagatzemar les dades en idcipsec2hash
        sql = """
                SELECT
                    id_cip_sec,
                    hash_d
                FROM
                    u11
            """
        for id_cip_sec, hash_d in u.getAll(sql, "import"):
            self.idcipsec2hash[id_cip_sec] = hash_d

    '''def get_problemes(self):
        """Aquesta funci� recupera els problemes de salut dels pacients i els emmagatzema en un diccionari."""

        # Crear un diccionari per emmagatzemar els problemes de salut dels pacients
        self.problemes = c.defaultdict(set)
        self.problemes["Pneum�nia"] = c.defaultdict(set)

        # Crear un diccionari amb la informaci� dels agrupadors de problemes de salut, amb el nombre de messos contemplats
        # des de la data d'extracci� per ser considerats al c�lcul dels indicadors
        # Clau: agrupador, Valor: descripci� del agrupador i per�ode en mesos
        ps_dict = {1: {"desc": "CI", "periode": 1000},
                   7: {"desc": "AVC", "periode": 1000},
                   11: {"desc": "Claud_int", "periode": 1000},
                   18: {"desc": "DM2", "periode": 1000},
                   26: {"desc": "Malaltia Hep�tica Greu", "periode": 1000},
                   53: {"desc": "IRC", "periode": 1000},
                   55: {"desc": "HTA", "periode": 1000},
                   58: {"desc": "Asma", "periode": 1000},
                   62: {"desc": "MPOC", "periode": 1000},
                   158: {"desc": "Pneum�nia", "periode": 15},
                   159: {"desc": "Pneum�nia", "periode": 15},
                   160: {"desc": "Insufici�ncia hep�tica greu", "periode": 1000},
                   163: {"desc": "Al�l�rgia penicil�lina", "periode": 1000},
                   383: {"desc": "Malaltia de Paget", "periode": 1000},
                   415: {"desc": "Psicosi", "periode": 1000},
                #    425: {"desc": "Depressi�", "periode": 12},
                   710: {"desc": "Fractura per fragilitat", "periode": 1000},
                   722: {"desc": "Neopl�sia maligna", "periode": 1000},
                #    922: {"desc": "Insomni", "periode": 12},                   
                   956: {"desc": "Depressi� recurrent", "periode": 1000},
                   957: {"desc": "Incident: Otitis mitjana aguda serosa", "periode": 12},
                   961: {"desc": "Incident: Otitis mitjana aguda serosa", "periode": 12}}

        # Consulta SQL per obtenir les dades dels problemes de salut, mitjan�ant agrupadors
        sql = """
                SELECT
                    id_cip_sec,
                    ps,
                    dde
                FROM
                    eqa_problemes
                WHERE
                    ps IN {}
                    AND dde <= '{}'
              """.format(str(tuple(ps_dict.keys())), self.dext)
        for id_cip_sec, ps, data_episodi in u.getAll(sql, "nodrizas"):
            # Comprovar si el pacient est� dins la poblaci� i si el problema est� dins del rang de temps definit a ps_dict
            if id_cip_sec in self.poblacio and self.dext - dateutil.relativedelta.relativedelta(months=ps_dict[ps]["periode"]) < data_episodi:
                # Si el problema no �s pneum�nia, afegir al conjunt de problemes
                if ps_dict[ps]["desc"] != "Pneum�nia":
                    self.problemes[ps_dict[ps]["desc"]].add(id_cip_sec)
                # Si el problema �s pneum�nia, afegir a un subdiccionari amb la data de l'episodi
                else:
                    self.problemes["Pneum�nia"][id_cip_sec].add(data_episodi)

        # Consulta SQL per obtenir les dades dels problemes d'ansietat, mitjan�ant codis_ps
        codis_ansietat = ('C01-F40.00', 'C01-F40.10', 'C01-F40.298', 'C01-F40.8', 'C01-F40.9', 'C01-F41', 'C01-F41.0', 'C01-F41.1', 'C01-F41.3', 'C01-F41.8', 'C01-F41.9', 'F40', 'F40.0', 'F40.1', 'F40.2', 'F40.8', 'F40.9', 'F41', 'F41.0', 'F41.1' , 'F41.2' , 'F41.3' , 'F41.8' , 'F41.9', 'F40.3', 'F40.4', 'F40.5', 'F40.6', 'F40.7', 'F41.4', 'F41.5', 'F41.6', 'F41.7')
        sql_2 = """
                    SELECT
                        id_cip_sec
                    FROM
                        problemes
                    WHERE
                        pr_cod_ps IN {}
                        AND pr_data_baixa = ''
                        AND datediff('{}', pr_dde) <= 365
                """.format(codis_ansietat, self.dext)
        for id_cip_sec, in u.getAll(sql_2, "import"):
            # Afegir els pacients amb ansietat al conjunt de problemes
            self.problemes["Ansietat"].add(id_cip_sec)'''


    '''def get_variables(self):
        """Aquesta funci� recupera les variables de salut dels pacients i les emmagatzema en un diccionari."""
        
        # Crear un diccionari per defecte per les variables
        self.variables = c.defaultdict(set)

        # Crear dos comptadors per controlar els pacients que poden tenir valors excessius de pressi� arterial
        excessiu_1, excessiu_2 = c.Counter(), c.Counter()

        # Crear un diccionari amb els agrupadors de les variables i els seus llindars
        # Clau: agrupador, Valor: descripci� del agrupador i valors llindars segons el concepte de l'agrupador
        vars_dict = {20: {"desc": "HbA1c", "valor_alt": 8, "valor_alt_>=60": 8},
                     464: {"desc": "FEV1/FVC", "valor_baix": 50},
                     674: {"desc": "TAS", "valor_alt": 150, "valor_alt_>=60": 160, "excessiu_1": 120, "excessiu_2": 160},
                     675: {"desc": "TAD", "valor_alt": 95, "valor_alt_>=60": 95, "excessiu_1": 90, "excessiu_2": 70}}

        # Crear la consulta SQL per obtenir les dades de les variables
        sql = """
                SELECT
                    id_cip_sec,
                    agrupador,
                    valor
                FROM
                    eqa_variables
                WHERE
                    agrupador IN {}
                    AND '{}' <= data_var <= '{}' 
                    AND usar = 1
              """.format(tuple(vars_dict.keys()), self.dext_menys1any, self.dext)
        for id_cip_sec, agrupador, valor in u.getAll(sql, "nodrizas"):
            # Comprovar si el id_cip_sec est� en la poblaci�
            if id_cip_sec in self.poblacio:
                edat = self.poblacio[id_cip_sec]["edat"]
                # Afegir el id_cip_sec a les variables
                self.variables[vars_dict[agrupador]["desc"]].add(id_cip_sec)                
                # Comprovar si el valor �s excessiu, segons l'edat
                if "valor_alt" in vars_dict[agrupador] and "valor_alt_>=60" in vars_dict[agrupador]:
                    if (edat < 60 and vars_dict[agrupador]["valor_alt"] < valor) or (60 <= edat and vars_dict[agrupador]["valor_alt_>=60"] < valor):
                        self.variables[vars_dict[agrupador]["desc"] + " Alta"].add(id_cip_sec)
                # Comprovar si hi ha un valor de TAS o TAD que excedeixi el llindar "excessiu_1" a vars_dict[agrupador]
                if "excessiu_1" in vars_dict[agrupador]:
                    if valor < vars_dict[agrupador]["excessiu_1"]:
                        excessiu_1[id_cip_sec] += 1
                # Comprovar si hi ha un valor de TAS o TAD que excedeixi el llindar "excessiu_2" a vars_dict[agrupador]
                if "excessiu_2" in vars_dict[agrupador]:
                    if valor < vars_dict[agrupador]["excessiu_2"]:
                        excessiu_2[id_cip_sec] += 1
                # Comprovar si el valor supera el valor m�nim a considerar
                if "valor_baix" in vars_dict[agrupador]:
                    if valor >= vars_dict[agrupador]["valor_baix"]:
                        self.variables["FEV1/FVC >= 50%"].add(id_cip_sec)
        
        # Comprovar els comptadors de control excessiu per identificar si el control de PA en un pacient �s excessiu
        for id_cip_sec in excessiu_1:
            if excessiu_1[id_cip_sec] == 2:
                self.variables["Control excessiu PA"].add(id_cip_sec)
        for id_cip_sec in excessiu_2:
            if excessiu_2[id_cip_sec] == 2:
                self.variables["Control excessiu PA"].add(id_cip_sec)       '''         

    def get_tractaments(self):

        # Defineix un diccionari predeterminat per emmagatzemar els diferents tipus de tractaments
        # i els pacients als quals se'ls ha recetat
        self.tractaments = c.defaultdict(set)
        '''self.tractaments["Set d'hipertensius"] = c.defaultdict(set)
        self.tractaments["Amoxicilina"] = c.defaultdict(set)
        self.tractaments["Levofloxacina"] = c.defaultdict(set)
        self.tractaments["Set d'ansiol�tics/hipn�tics"] = c.defaultdict(set)'''
        self.tractaments["Set d'antiespasm�dics urinaris"] = c.defaultdict(set)

        # Defineix un diccionari predeterminat per emmagatzemar les prescripcions de pacients amb pneum�nia
        self.prescripcions_lligades_pneumonia = c.defaultdict(set)

        # Defineix un conjunt per emmagatzemar l'historial d'antidepressius recomanats
        self.historic_antidepressius_recomanats = set()

        # Defineix un conjunt per emmagatzemar el historial de tractament amb glucocorticoides sist�mics de com a m�nim 3 mesos de durada
        self.glucocorticoides_sistemics_minim_3_mesos = set()

        # Defineix un diccionari de d'agrupadors de tractaments 
        '''tractaments_dict = {56: "IECA",
                            65: "Salbutamol",
                            66: "Terbutalina",
                            72: "ARA II",
                            81: "Aromatasa",
                            118: "AINES",
                            155: "Amoxicilina",
                            156: "Amoxicilina",
                            717: "Bisfofonats",
                            735: "Sacubitril/Valsartan",
                            898: "Antidepressius tric�clics",
                            955: "Olmesartan",
                            967: "IECA Tancat",
                            973: "Levofloxacina",
                            995: "Corticoides inhalats"}

        # Obtenir els tractaments corresponents a cada pacient
        sql = """
                SELECT
                    id_cip_sec,
                    farmac,
                    pres_orig,
                    data_fi,
                    tancat
                FROM
                    eqa_tractaments
                WHERE
                    farmac IN {}
                    AND pres_orig <= '{}'
              """.format(str(tuple(tractaments_dict.keys())), self.dext)
        for id_cip_sec, farmac, data_ini, data_fi, tancat in u.getAll(sql, "nodrizas"):
            # Comprovar si el id_cip_sec est� en la poblaci�
            if id_cip_sec in self.poblacio:
                # Afegir pacients amb tractaments d'amoxicilina o levofloxacina i les dates del tractament
                if tractaments_dict[farmac] == "Amoxicilina" or tractaments_dict[farmac] == "Levofloxacina":
                    self.tractaments[tractaments_dict[farmac]][id_cip_sec].add((data_ini, data_fi))
                # Afegir pacients amb tractaments diferents a bisfosfonats, amoxicilina o levofloxacina
                else:
                    if tancat:
                        if tractaments_dict[farmac] == "IECA Tancat":
                            self.tractaments[tractaments_dict[farmac]].add(id_cip_sec)
                    elif not tancat:
                        # Afegir pacients en tractament de bisfosfonats m�s llargs de 5 anys
                        if tractaments_dict[farmac] == "Bisfofonats" and u.yearsBetween(data_ini, self.dext) >= 5: 
                            self.tractaments["Bisfofonats m�s de 5 anys"].add(id_cip_sec)                        
                        self.tractaments[tractaments_dict[farmac]].add(id_cip_sec)

        # Consulta SQL per obtenir els codis_pf de productes farmac�utics de Zolpidem 10mg
        sql = """
                SELECT
                    pf_codi
                FROM
                    cat_cpftb006
                WHERE
                    pf_cod_atc = 'N05CF02'
                    AND pf_desc_gen LIKE '%10%'
              """
        codis_pf_zolpidem_10mg = set([pf_codi for pf_codi, in u.getAll(sql, "import")])'''

        # Consulta SQL per obtenir els codis_pf de productes farmac�utics absorbents
        sql = """
                SELECT
                    pf_codi
                FROM
                    cat_cpftb006
                WHERE
                    pf_gt_codi IN ('23C', '23C00', '23C01', '23C02', '23C03', '23C04', '23C05', '23C06')
              """
        codis_pf_absorbents = set([pf_codi for pf_codi, in u.getAll(sql, "import")])

        # Unir els codis_pf de productes farmac�utics de Zolpidem 10mg i absorbents
        codis_pf = codis_pf_absorbents

        # Diccionari que relaciona codis ATC amb descripcions de tractaments
        '''codis_atc_dict = {"C02%": "Hipertensius",
                          "C03%": "Hipertensius",
                          "C07%": "Hipertensius",
                          "C08%": "Hipertensius",
                          "C09%": "Hipertensius",
                          "N03AE01": "Ansiol�tics/Hipn�tics",
                          "N05B%": "Ansiol�tics/Hipn�tics",
                          "N05BA%": "Benzodiacepines",
                          "N05C%": "Ansiol�tics/Hipn�tics",
                          "N05BB01": "Hidroxizina",
                          "N05CF02": "Zolpidem",
                          "N06AB03": "Antidepressius recomanats",
                          "N06AB04": "Antidepressius recomanats",
                          "N06AB05": "Antidepressius recomanats",
                          "N06AB06": "Antidepressius recomanats",
                          "N06AB08": "Antidepressius no recomanats",
                          "N06AB10": "Antidepressius no recomanats",
                          "N06AX%": "Antidepressius no recomanats",
                          "N06AX05": "Trazodona",
                          "N06AG02": "Moclobemida",
                          "N06AX16": "Venlafaxina",
                          "N06AX21": "Duloxetina",
                          "N06AX23": "Desvenlafaxina",
                          "A10BA%": "Hipoglucemiants no insul�nics",
                          "A10BB%": "Hipoglucemiants no insul�nics",
                          "A10BC%": "Hipoglucemiants no insul�nics",
                          "A10BD%": "Hipoglucemiants no insul�nics",
                          "A10BF%": "Hipoglucemiants no insul�nics",
                          "A10BG%": "Hipoglucemiants no insul�nics",
                          "A10BH%": "Hipoglucemiants no insul�nics",
                          "A10BJ%": "Hipoglucemiants no insul�nics",
                          "A10BK%": "Hipoglucemiants no insul�nics",
                          "A10BX%": "Hipoglucemiants no insul�nics",
                          "A10BA02": "Hipoglucemiants no insul�nics recomanats",
                          "A10BB07": "Hipoglucemiants no insul�nics recomanats",
                          "A10BB09": "Hipoglucemiants no insul�nics recomanats",
                          "A10BB12": "Hipoglucemiants no insul�nics recomanats",
                          "A10BD05": "Hipoglucemiants no insul�nics recomanats",
                          "A10BD06": "Hipoglucemiants no insul�nics recomanats",
                          "A10BD07": "Hipoglucemiants no insul�nics recomanats",
                          "A10BD15": "Hipoglucemiants no insul�nics recomanats",
                          "A10BD20": "Hipoglucemiants no insul�nics recomanats",
                          "A10BG03": "Hipoglucemiants no insul�nics recomanats",
                          "A10BH01": "Hipoglucemiants no insul�nics recomanats",
                          "A10BK01": "Hipoglucemiants no insul�nics recomanats",
                          "A10BK03": "Hipoglucemiants no insul�nics recomanats",
                          "A10BX02": "Hipoglucemiants no insul�nics recomanats",
                          "J01%": "Actibacterians",
                          "G04AC53": "Antiespasm�dics urinaris",
                          "G04BD02": "Antiespasm�dics urinaris",
                          "G04BD05": "Antiespasm�dics urinaris",
                          "G04BD06": "Antiespasm�dics urinaris",
                          "G04BD07": "Antiespasm�dics urinaris",
                          "G04BD08": "Antiespasm�dics urinaris",
                          "G04BD09": "Antiespasm�dics urinaris",
                          "G04BD11": "Antiespasm�dics urinaris",
                          "G04BD12": "Antiespasm�dics urinaris",
                          "G04BD13": "Antiespasm�dics urinaris",
                          "G04CB%": "Inhibidors de la testosterona 5?-reductasa",
                          "G04CA51": "Inhibidors de la testosterona 5?-reductasa",
                          "G04CA51": "Inhibidors de la testosterona 5?-reductasa",
                          "H02AA%": "Glucocordicoides",
                          "H02AB%": "Glucocordicoides"
                         }'''
        codis_atc_dict = {"G04AC53": "Antiespasm�dics urinaris",
                          "G04BD02": "Antiespasm�dics urinaris",
                          "G04BD05": "Antiespasm�dics urinaris",
                          "G04BD06": "Antiespasm�dics urinaris",
                          "G04BD07": "Antiespasm�dics urinaris",
                          "G04BD08": "Antiespasm�dics urinaris",
                          "G04BD09": "Antiespasm�dics urinaris",
                          "G04BD11": "Antiespasm�dics urinaris",
                          "G04BD12": "Antiespasm�dics urinaris",
                          "G04BD13": "Antiespasm�dics urinaris"}
        # Detecci� dels codis ATC preparats per fer 'LIKE' a la query SQL i guardar-los eliminant el car�cter '%'
        codis_atc_dict_retallats = {atc_codi.replace("%", ""): desc for atc_codi, desc in codis_atc_dict.items() if "%" in atc_codi}

        # Obtenir particions de les taules
        particions_taules = u.getTablePartitions("tractaments", "import")
        
        # Llistar codis d'antidepressius recomanats i glucocorticoides sistemics
        codis_antidepressius_recomanats = tuple(codi for codi in codis_atc_dict if codis_atc_dict[codi] == "Antidepressius recomanats")
        codis_glucocorticoides_sistemics = tuple(codi.replace("%", "") for codi in codis_atc_dict if codis_atc_dict[codi] == "Glucocordicoides")
        
        # Crear llista dels inputs de la funci� a realitzar en multiprocess
        jobs = [(codis_atc_dict.keys(), codis_pf, codis_antidepressius_recomanats, codis_glucocorticoides_sistemics, self.dext, particio_taula) for particio_taula in particions_taules]
        
        counter = 0

        # for job in jobs:
        #     sub_tractaments, dates_prescripcions_lligades_pneumonia, historic_antidepressius_recomanats, glucocorticoides_sistemics_minim_3_mesos = sub_get_tractaments(job)        
        # Executar en paral�lel el subm�tode sub_get_tractaments
        for sub_tractaments, dates_prescripcions_lligades_pneumonia, historic_antidepressius_recomanats, glucocorticoides_sistemics_minim_3_mesos in u.multiprocess(sub_get_tractaments, jobs, 8):
            # Afegir resultats de sub_tractaments al conjunt de tractaments
            for id_cip_sec in sub_tractaments:
                for codi_tractament in sub_tractaments[id_cip_sec]:
                    # D'aquells tractaments que tenen el codi_atc complet a l'objecte codis_atc_dict
                    if codi_tractament in codis_atc_dict:
                        self.tractaments[codis_atc_dict[codi_tractament]].add(id_cip_sec)
                        if codis_atc_dict[codi_tractament] == "Antiespasm�dics urinaris":
                            self.tractaments["Set d'antiespasm�dics urinaris"][id_cip_sec].add(codi_tractament)
                    if codi_tractament in codis_pf_absorbents:
                        self.tractaments["Absorbents d'orina"].add(id_cip_sec)
                    # Comprobem que el codi_tractament �s string, ja que treballarem amb els codis_atc retallats
                    if isinstance(codi_tractament, str):
                        for codi_atc_dict_retallat in codis_atc_dict_retallats:
                            if codi_atc_dict_retallat in codi_tractament:
                                # Afegir resultats de sub_tractaments aquells tractaments que tenen el codi_atc retallat
                                self.tractaments[codis_atc_dict_retallats[codi_atc_dict_retallat]].add(id_cip_sec)
                                if codis_atc_dict_retallats[codi_atc_dict_retallat] == "Antiespasm�dics urinaris":
                                    self.tractaments["Set d'antiespasm�dics urinaris"][id_cip_sec].add(codi_tractament)                                    

            # Actualitzar conjunts amb les dades obtingudes
            '''self.prescripcions_lligades_pneumonia.update(dates_prescripcions_lligades_pneumonia)
            self.historic_antidepressius_recomanats.update(historic_antidepressius_recomanats)
            self.glucocorticoides_sistemics_minim_3_mesos.update(glucocorticoides_sistemics_minim_3_mesos)'''

            # Imprimir l'estat de la tasca
            counter += 1
            print("     sub_get_tractaments({}/{})".format(str(counter).zfill(2), len(particions_taules)))
        
        # Comprovar si un pacient t� prescrits 3 o m�s hipertensius i actualitzar el conjunt corresponent
        # Comprovar si un pacient t� prescrits 2 o m�s antiespasm�dics urinaris i actualitzar el conjunt corresponent
        for id_cip_sec in self.tractaments["Set d'antiespasm�dics urinaris"]:
            if len(self.tractaments["Set d'antiespasm�dics urinaris"][id_cip_sec]) >= 2:
                self.tractaments["2 o + antiespasm�dics urinaris"].add(id_cip_sec)                
                      
    def get_RAM(self):
        """Obt� les contraindicacions per als tractaments."""

        self.contraindicacions = c.defaultdict(set)

        # Diccionari de contraindicacions
        # Clau: agrupador, Valor: descripci� del agrupador
        contraindicacions_dict = {503: "IECA"}

        # Consulta SQL per obtenir les contraindicacions
        sql = """
                SELECT
                    id_cip_sec,
                    agr
                FROM
                    eqa_ram
                WHERE
                    agr = {}
              """.format(503)      #(str(tuple(contraindicacions_dict.keys())))  Canviar linia anterior si s'afegeixen m�s valors a contraindicacions_dict
        for id_cip_sec, ram_atc in u.getAll(sql, "nodrizas"):
            # Si l'id_cip_sec est� dins de la poblaci�, s'afegeix a la llista de contraindicacions
            if id_cip_sec in self.poblacio:
                self.contraindicacions[contraindicacions_dict[ram_atc]].add(id_cip_sec)

    def get_indicadors(self):
        """Obt� els indicadors IMP i els emmagatzema en la classe."""

        # Inicialitzaci� de variables
        self.dades = set()
        resultats_up = c.Counter()
        self.resultats_up = list()
        resultats_uba = c.Counter()
        self.resultats_uba = list()
        self.complidors_numerador_llistat = set()
        self.resultats_altcataleg = c.defaultdict(c.Counter)

        # Definici� de les condicions per a cada indicador IMP
        # Cada clau del diccionari representa un indicador IMP, i el seu valor �s un altre diccionari
        # amb les claus "dens", "nums" i "excls", que contenen els conjunts de pacients que compleixen
        # les condicions del denominador, numerador i exclusions del denominador, respectivament.
        cond_ind = {#"IMP022": {"dens": self.problemes["HTA"] & (self.tractaments["IECA"] | self.tractaments["ARA II"]),
                    #           "nums": self.tractaments["Olmesartan"],
                    #           "excls": self.tractaments["Sacubitril/Valsartan"]},
                    #"IMP023": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["ates"]]) & self.problemes["HTA"],
                    #           "nums": self.tractaments["ARA II"] & (self.variables["TAS Alta"] | self.variables["TAD Alta"]),
                    #           "excls": self.contraindicacions["IECA"] | self.tractaments["IECA Tancat"] | self.tractaments["Sacubitril/Valsartan"]},
                    # "IMP024": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["edat"]>=80]) & self.tractaments["3 o + hipertensius"],
                    #            "nums": self.variables["Control excessiu PA"],
                    #            "excls": set()},
                    # "IMP025": {"dens": (set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["edat"]>75]) | self.problemes["Malaltia Hep�tica Greu"]) & self.tractaments["Zolpidem"],
                    #            "nums": self.tractaments["Zolpidem 10 mg"],
                    #            "excls": set()},
                    # "IMP026": {"dens": self.tractaments["Ansiol�tics/Hipn�tics"],
                    #            "nums": self.tractaments["2 o + ansiol�tics/hipn�tics"],
                    #            "excls": self.tractaments["Hidroxizina"] | set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["maca"]]) | self.problemes["Psicosi"] | self.problemes["Neopl�sia maligna"]},
                    # "IMP027": {"dens": self.tractaments["Antidepressius recomanats"] | self.tractaments["Antidepressius no recomanats"],
                    #            "nums": self.tractaments["Antidepressius no recomanats"] - self.historic_antidepressius_recomanats,
                    #            "excls": self.tractaments["Trazodona"] | self.tractaments["Moclobemida"] | self.tractaments["Antidepressius tric�clics"] | self.tractaments["Depressi� recurrent"]},
                    #"IMP028": {"dens": self.tractaments["Duloxetina"] | self.tractaments["Venlafaxina"] | self.tractaments["Desvenlafaxina"],
                    #           "nums": self.variables["TAS Alta"] | self.variables["TAD Alta"],
                    #           "excls": set()},
                    # "IMP029": {"dens": self.problemes["DM2"] & self.variables["HbA1c Alta"] & self.tractaments["Hipoglucemiants no insul�nics"],
                    #            "nums": set(self.poblacio) - self.tractaments["Hipoglucemiants no insul�nics recomanats"],
                    #            "excls": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["maca"]]) | (set(self.poblacio) - self.variables["HbA1c"])},
                    # "IMP030": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if 14 <= self.poblacio[id_cip_sec]["edat"] <= 75]) & self.episodis_pneumonia_considerats,
                    #            "nums": self.tractament_correcte_pneumonia,
                    #            "excls": self.problemes["MPOC"] | set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["institucionalitzat"]])},
                    # "IMP031": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if 14 <= self.poblacio[id_cip_sec]["edat"] <= 90]) & self.problemes["Incident: Otitis mitjana aguda serosa"],
                    #            "nums": set(self.poblacio) - self.tractaments["Actibacterians"],
                    #            "excls": set()},
                    #"IMP032": {"dens": self.tractaments["Antiespasm�dics urinaris"],
                    #           "nums": self.tractaments["2 o + antiespasm�dics urinaris"],
                    #           "excls": set()},
                    "IMP033": {"dens": self.tractaments["Antiespasm�dics urinaris"],
                               "nums": self.tractaments["Absorbents d'orina"],
                               "excls": set()},
                    #"IMP034": {"dens": self.tractaments["Inhibidors de la testosterona 5?-reductasa"],
                    #           "nums": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["sexe"] == "D"]),
                    #           "excls": set()},
                    # "IMP035": {"dens": self.tractaments["Bisfofonats"],
                    #            "nums": self.tractaments["Bisfofonats m�s de 5 anys"],
                    #            "excls": self.tractaments["Fractura per fragilitat"] | self.glucocorticoides_sistemics_minim_3_mesos | self.tractaments["Aromatasa"] | self.problemes["Malaltia de Paget"]},
                    # "IMP036": {"dens": self.problemes["CI"] | self.problemes["AVC"] | self.problemes["Claud_int"] | self.problemes["IRC"] | self.problemes["HTA"] | self.problemes["Insufici�ncia hep�tica greu"],
                    #            "nums": self.tractaments["AINES"],
                    #            "excls": set()},
                    # "IMP037": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["edat"]>=18]),
                    #            "nums": self.problemes["Ansietat"] & self.tractaments["Benzo amb durada > 50 dies"],
                    #            "excls": set()},
                    # "IMP038": {"dens": self.problemes["MPOC"] & self.tractaments["Corticoides inhalats"],
                    #            "nums": self.variables["FEV1/FVC >= 50%"], # 
                    #            "excls": self.problemes["Asma"] | self.variables["Fenotip MPOC"]},
                    # "IMP039": {"dens": self.tractaments["Salbutamol"] | self.tractaments["Terbutalina"],
                    #            "nums": ,
                    #            "excls": set()},
                    # "IMP040": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if 14 <= self.poblacio[id_cip_sec]["edat"]]),
                    #            "nums": ,
                    #            "excls": set()},                               
                    # "IMP041": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["edat"]>=18]),
                    #            "nums": self.problemes["Insomni"] & self.tractaments["Benzo amb durada > 50 dies"],
                    #            "excls": set()},
                    # "IMP042": {"dens": set([id_cip_sec for id_cip_sec in self.poblacio if self.poblacio[id_cip_sec]["edat"]>=18]),
                    #            "nums": self.problemes["Depressi�"] & self.tractaments["Benzo amb durada > 50 dies"],
                    #            "excls": set()}                               
                    }

        # Iteraci� sobre els indicadors i c�lcul dels resultats
        for ind in cond_ind:
            # Per a cada pacient que compleixi les condicions del denominador de l'indicador
            for id_cip_sec in cond_ind[ind]["dens"]:
                if id_cip_sec in self.poblacio:
                    # Assignar 1 si el pacient compleix les condicions del numerador, 0 en cas contrari
                    num = 1 if id_cip_sec in cond_ind[ind]["nums"] else 0
                    # Assignar 1 si el pacient compleix les condicions d'exclusi� del denominador, 0 en cas contrari
                    excl = 1 if id_cip_sec in cond_ind[ind]["excls"] else 0

                    up = self.poblacio[id_cip_sec]["up"]
                    uba = self.poblacio[id_cip_sec]["uba"]
                    ubainf = self.poblacio[id_cip_sec]["ubainf"]
                    edat, sexe = self.poblacio[id_cip_sec]["edat"], self.poblacio[id_cip_sec]["sexe"]
                    edat_agr, sexe_agr = u.ageConverter(edat), u.sexConverter(sexe)

                    # Emmagatzemar les dades dels pacients en el conjunt 'self.dades'
                    self.dades.add((id_cip_sec, ind, up, uba, edat_agr, sexe_agr, 1, num, excl))
                    # Incrementar els comptadors de denominador i numerador dels resultats
                    resultats_up[(ind, self.periode, up, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += 1 if not excl else 0
                    resultats_up[(ind, self.periode, up, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += num if not excl else 0
                    resultats_uba[(ind, self.periode, up, uba, 'M', 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += 1 if not excl else 0
                    resultats_uba[(ind, self.periode, up, uba, 'M', 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += num if not excl else 0
                    resultats_uba[(ind, self.periode, up, ubainf, 'I', 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += 1 if not excl else 0
                    resultats_uba[(ind, self.periode, up, ubainf, 'I', 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += num if not excl else 0

                    # Si el pacient no est� excl�s i compleix les condicions del numerador sent el indicador invers,
                    # o no compleix les condicions del numerador sent el indicador no invers, l'afegim al conjunt 
                    # 'self.complidors_numerador_llistat'
                    noresolt = 0
                    if not excl:
                        invers = INDICADORS_INVERSOS[ind]
                        if (invers and num) or (not invers and not num):
                            self.complidors_numerador_llistat.add((id_cip_sec, ind, excl))
                            noresolt = 1
                    
                        # Incrementar els comptadors de denominador i numerador dels resultats per ALTCATALEG de pdp
                        #self.resultats_altcataleg[(up, uba, "M", ind)]["DEN"] += 1 
                        #self.resultats_altcataleg[(up, uba, "M", ind)]["NUM"] += num
                        #self.resultats_altcataleg[(up, uba, "M", ind)]["NORESOLTS"] += noresolt
                        self.resultats_altcataleg[(up, ubainf, "I", ind)]["DEN"] += 1 
                        self.resultats_altcataleg[(up, ubainf, "I", ind)]["NUM"] += num
                        self.resultats_altcataleg[(up, ubainf, "I", ind)]["NORESOLTS"] += noresolt

        for (ind, self.periode, up, analisi, NOCAT, NOIMP, DIM6SET, N), n in resultats_up.items():
            self.resultats_up.append((ind, self.periode, self.centres[up], analisi, NOCAT, NOIMP, DIM6SET, N, n))
        for (ind, self.periode, up, uba, tipus, analisi, NOCAT, NOIMP, DIM6SET, N), n in resultats_uba.items():
            self.resultats_uba.append((ind, self.periode, self.centres[up], uba, tipus, analisi, NOCAT, NOIMP, DIM6SET, N, n))

    def create_tables_Khalix(self):
        """ Crea les taules a partir de les quals s'exportar� la informaci� a Khalix."""

        cols_up = '(indicador varchar(10), periode varchar(5), up varchar(5), analisi varchar(3),\
        nocat varchar(5), noimp varchar(5), dim6set varchar(7), n varchar(1), valor int)'
        u.createTable('IMP_FASE2_proves', cols_up, 'catsalut', rm=True)
        cols_uba = '(indicador varchar(10), periode varchar(5), up varchar(5), uba varchar(7), tipus varchar(1), analisi varchar(3),\
                nocat varchar(5), noimp varchar(5), dim6set varchar(7), n varchar(1), valor int)'
        u.createTable('IMP_UBA_FASE2_proves', cols_uba, 'catsalut', rm=True)

        u.listToTable(self.resultats_up, 'IMP_FASE2_proves', 'catsalut')
        u.listToTable(self.resultats_uba, 'IMP_UBA_FASE2_proves', 'catsalut')

    def export_khalix(self):
        """Exporta les dades obtingudes a la taula 'imp_fase2'."""

        sql_up = "select * from catsalut.IMP_FASE2_proves"
        file_up = "IMPRE"
        u.exportKhalix(sql_up,file_up,force=1)

        sql_uba = "select indicador, periode, concat(up, tipus, uba), analisi, nocat, noimp, dim6set, n, valor from catsalut.IMP_UBA_FASE2"
        file_uba = "IMPRE_UBA"
        u.exportKhalix(sql_uba,file_uba,force=1)

    def export_cataleg(self):
        """Crear el cat�leg d'indicadors, per exportar a PDP."""

        # Definir les columnes del cat�leg
        columnes_cataleg = '(indicador varchar(8), literal varchar(300), ordre int, pare varchar(8), llistat int, invers int, \
                             mmin double, mint double,mmax double,toShow int,wiki varchar(250),tipusvalor varchar(5))'
        # Crear la taula del cat�leg
        u.createTable(taula_cataleg, columnes_cataleg, "altres", rm=True)

        # Llista de valors a carregar al cat�leg. Cada element de la llista cont� la informaci� d'un indicador
        upload = [
                #("IMP022", "Pacients amb HTA i tractament amb olmesartan", 22, "IMP", 1, INDICADORS_INVERSOS["IMP022"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5182/ver/", "PCT"),
                #("IMP023", "Pacients amb HTA no controlada i tractament amb antagonistes del receptor de l'angiotensina 2 (ARA-II) sense tractament amb un inhibidor de l'enzim conversiu de l'angiotensina (IECA)", 23, "IMP", 1, INDICADORS_INVERSOS["IMP023"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5183/ver/", "PCT"),
                # ("IMP024", "Control excessiu de la pressi� arterial en poblaci� d'edat avan�ada", 3, "IMP", 1, INDICADORS_INVERSOS["IMP024"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                # ("IMP025", "Inadequaci� dosi de zolpidem en pacients d'edat avan�ada o amb malaltia hep�tica greu", 4, "IMP", 1, INDICADORS_INVERSOS["IMP025"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                # ("IMP026", "Duplicitat de tractament ansiol�tic i hipn�tic", 5, "IMP", 1, INDICADORS_INVERSOS["IMP026"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                # ("IMP027", "Pacients amb prescripci� d'antidepressius de segona l�nia o no recomanats sense haver tingut prescrit un f�rmac de primera l�nia", 6, "IMP", 1, INDICADORS_INVERSOS["IMP027"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                #("IMP028", "Adequaci� del tractament amb antidepressius en pacients amb hipertensi� arterial no controlada", 28, "IMP", 1, INDICADORS_INVERSOS["IMP028"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5199/ver/", "PCT"),
                # ("IMP029", "Pacients amb DM2 i mal control sense tractament amb hipoglucemiants no insul�nics recomanats", 8, "IMP", 1, INDICADORS_INVERSOS["IMP029"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                # ("IMP030", "Adequaci� del tractament antibi�tic en pneum�nia comunit�ria en adults", 9, "IMP", 1, INDICADORS_INVERSOS["IMP030"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                # ("IMP031", "Adequaci� del tractament antibi�tic en otitis serosa", 10, "IMP", 1, INDICADORS_INVERSOS["IMP031"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                #("IMP032", "Pacients en tractament amb dos o m�s antiespasm�dics urinaris (anticolin�rgics o mirabegr�)", 32, "IMP", 1, INDICADORS_INVERSOS["IMP032"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5184/ver/", "PCT"),
                #("IMP033", "Pacients en tractament amb antiespasm�dics urinaris (anticolin�rgics o mirabegr�) i prescripci� d'absorbents d'orina", 33, "IMP", 1, INDICADORS_INVERSOS["IMP033"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5186/ver/", "PCT"),
                #("IMP034", "Dones amb prescripci� de f�rmacs per a la hiperpl�sia prost�tica benigna (HBP)", 34, "IMP", 1, INDICADORS_INVERSOS["IMP034"], 0, 0, 0, 1, "http://sisap-umi.eines.portalics/indicador/indicador/5197/ver/", "PCT"),
                # ("IMP035", "Percentatge de pacients amb prescripci� de bifosfonats orals amb una durada superior a 5 anys sobre el total de pacients tractats amb bifosfonats.", 14, "IMP", 1, INDICADORS_INVERSOS["IMP035"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                # ("IMP036", "AINE en malaltia cardiovascular, renal cr�nica o insufici�ncia hep�tica", 15, "IMP", 1, INDICADORS_INVERSOS["IMP036"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),
                # ("IMP037", "Percentatge de pacients amb nou diagn�stic d'ansietat i prescripci� de benzodiazepines major a 50 dies", 16, "IMP", 1, INDICADORS_INVERSOS["IMP037"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),           
                # ("IMP038", "Percentatge de pacients amb MPOC i bona funci� pulmonar en tractament corticoides inhalats", 16, "IMP", 1, INDICADORS_INVERSOS["IMP038"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),           
                # ("IMP039", "Percentatge de pacients amb exc�s d'utilitzaci� d'agonistes beta-2-adren�rgics d'acci� curta", 16, "IMP", 1, INDICADORS_INVERSOS["IMP039"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),           
                # ("IMP040", "Percentatge de pacients amb asma persistent  amb tractament controlador amb corticoides inhalats", 16, "IMP", 1, INDICADORS_INVERSOS["IMP040"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),           
                # ("IMP041", "Tractament inadequat amb benzodiazepines en insomni", 16, "IMP", 1, INDICADORS_INVERSOS["IMP041"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),           
                # ("IMP042", "Tractament inadequat amb benzodiazepines en depressi�", 16, "IMP", 1, INDICADORS_INVERSOS["IMP042"], 0, 0, 0, 1, "https://drive.google.com/drive/u/0/folders/1YLaqO5Vc2Er6nxcdAZphLGnVOyqz1Ict", "PCT"),           
                ]
        # Carregar els valors a la taula del cat�leg
        #u.listToTable(upload, taula_cataleg, "altres")
        # Carregar els valors a la taula del cat�leg a PDP
        sql = """
                DELETE FROM
                    ALTCATALEG
                WHERE
                    PARE = 'IMP'
                    AND DATAANY = {}
              """.format(self.dext.year)
        u.execute(sql, "pdp")          
        upload_pdp = [(self.dext.year, indicador, literal, ordre, pare, llistat, invers, mmin, mint, mmax, toShow, wiki, tipusvalor) for (indicador, literal, ordre, pare, llistat, invers, mmin, mint, mmax, toShow, wiki, tipusvalor) in upload]
        u.listToTable(upload_pdp, "altcataleg", "pdp")

    def export_cataleg_pare(self):
        """Crear el cat�leg pare."""

        # Definir les columnes del cat�leg pare
        columnes_catalegPare = '(pare varchar(8), literal varchar(300), ordre int, pantalla varchar(20))'
        # Crear la taula del cat�leg pare
        u.createTable(taula_cataleg_pare, columnes_catalegPare, "altres", rm=True)
        
        # Llista de valors a carregar al cat�leg pare
        upload = [('IMP', 'Indicadors de prescripci� activa', 10, 'EQPFIMP')] 
        
        # Carregar els valors a la taula del cat�leg pare
        u.listToTable(upload, taula_cataleg_pare, "altres")

        # Carregar els valors a la taula del cat�leg pare a PDP
        sql = """
                DELETE FROM
                    ALTCATALEGPARE
                WHERE
                    PARE LIKE 'IMP%'
                    AND DATAANY = {}
              """.format(self.dext.year)
        u.execute(sql, "pdp")        
        upload_pdp = [(self.dext.year, pare, literal, ordre, pantalla) for (pare, literal, ordre, pantalla) in upload]
        u.listToTable(upload_pdp, "altcatalegpare", "pdp")

    def export_taula_indicadors(self):
        """Exportar les dades a ALTINDICADORS."""

        # Creem la llista buides on emmagatzemar les dades dels pacients
        upload_pdp = list() 

        # Afegim a la llista els valors a carregar a ALTINDICADORS
        for (up, uba, tipus_prof, ind) in self.resultats_altcataleg:
            den = self.resultats_altcataleg[(up, uba, tipus_prof, ind)]["DEN"]
            num = self.resultats_altcataleg[(up, uba, tipus_prof, ind)]["NUM"]
            noresolts = self.resultats_altcataleg[(up, uba, tipus_prof, ind)]["NORESOLTS"]
            resultat = num/den
            upload_pdp.append((self.dext.year, self.dext.month, up, uba, tipus_prof, ind, num, den, resultat, noresolts, 0, 0))

        # Carregar els valors a la taula ALTINDICADORS a PDP
        sql = """
                DELETE FROM
                    ALTINDICADORS
                WHERE
                    indicador LIKE 'IMP0%' 
                    AND DATAANY = {} 
                    AND DATAMES = {}
              """.format(self.dext.year, self.dext.month)
        u.execute(sql, "pdp")
        u.listToTable(upload_pdp, "altindicadors", "pdp")


    def export_taula_pacients(self):
        """Crea un llistat de pacients amb les seves dades d'imputaci� i els indicadors IMP relacionats."""

        # Creem dues llistes buides on emmagatzemar les dades dels pacients
        upload = list() 
        upload_pdp = list()

        # Iterem a trav�s de la llista de pacients que es troben en el numerador dels indicadors IMP
        for (id_cip_sec, ind, excl_num) in self.complidors_numerador_llistat:
            try:
                # Obtenim el hash associat a l'identificador del pacient
                hash_d = self.idcipsec2hash[id_cip_sec]

                # Obtenim les dades d'assignaci� del pacient
                up = self.poblacio[id_cip_sec]["up"]
                uba = self.poblacio[id_cip_sec]["uba"]
                upinf = self.poblacio[id_cip_sec]["upinf"]
                ubainf = self.poblacio[id_cip_sec]["ubainf"]
                sector = self.poblacio[id_cip_sec]["sector"]

                # Afegim aquestes dades a la llista 'upload'
                upload.append((id_cip_sec, up, uba, upinf, ubainf, ind, excl_num, hash_d, sector))
            except:
                pass

        # Definim les columnes de la taula taula_pacients
        columnes_llistats = '(id_cip_sec double, up varchar(5), uba varchar(7), upinf varchar(5), ubainf varchar(7),\
                            grup_codi varchar(10), exclos int, hash_d varchar(40), sector varchar(4))'
        # Crear la taula taula_pacients
        u.createTable(taula_pacients, columnes_llistats, "catsalut", rm=True)

        # Carregar els valors a la taula taula_pacients
        u.listToTable(upload, taula_pacients, "catsalut")

        # Carregar els valors a la taula llistats a PDP
        sql = """
                DELETE FROM
                    ALTLLISTATS
                WHERE
                    indicador LIKE 'IMP0%'
              """
        u.execute(sql, "pdp")
        for (id_cip_sec, up, uba, upinf, ubainf, indicador, exclos, hash_d, sector) in upload:
            upload_pdp.append((up, uba, "M", indicador, hash_d, sector, exclos, None))
            # upload_pdp.append((upinf, ubainf, "I", indicador, hash_d, sector, exclos))
        u.listToTable(upload_pdp, "altllistats", "pdp")

def sub_get_tractaments((codis_atc, codis_pf, codis_antidepressius_recomanats, codis_glucocorticoides_sistemics, dext, particio_taula)):

    # Crear un diccionari per emmagatzemar els tractaments de cada pacient
    sub_tractaments = c.defaultdict(set)
    # Crear un diccionari per emmagatzemar les dates de prescripcions relacionades amb pneum�nia per cada pacient
    dates_prescripcions_lligades_pneumonia = c.defaultdict(set)
    # Crear un conjunt per emmagatzemar els pacients amb historial d'antidepressius recomanats
    historic_antidepressius_recomanats = set()
    # Crear un conjunt per emmagatzemar els pacients amb tractament amb glucocorticoides sist�mics durant com a m�nim 3 mesos
    glucocorticoides_sistemics_minim_3_mesos = set()

    # Crear una cadena de consulta amb codis ATC
    atc_codis_codi_like = "("
    for atc_codi in codis_atc:
        atc_codis_codi_like += "ppfmc_atccodi LIKE '{}' OR ".format(atc_codi)
    atc_codis_codi_like = atc_codis_codi_like[:-5] + "')"

    # Crear una cadena de consulta amb codis PF
    pf_codis_in = "ppfmc_pf_codi in {}".format(tuple(codis_pf))

    # Definir la consulta SQL per obtenir les dades dels tractaments
    sql = """
            SELECT
                id_cip_sec,
                ppfmc_atccodi,
                ppfmc_pf_codi,
                ppfmc_pmc_data_ini,
                ppfmc_data_fi,
                CASE WHEN ppfmc_data_fi >= '{}' OR ppfmc_data_fi = '0000-00-00' THEN 0
                	 ELSE 1
               	END AS tractament_finalitzat
            FROM
                {}
            WHERE
                ({} OR {})
                AND ((ppfmc_data_fi >= '{}' OR ppfmc_data_fi = '0000-00-00') OR ppfmc_atccodi IN {})
            """.format(dext, particio_taula, atc_codis_codi_like, pf_codis_in, dext, codis_antidepressius_recomanats)
    for id_cip_sec, atc_codi, pf_codi, data_ini, data_fi, tractament_finalitzat in u.getAll(sql, "import"):
        # Comprovar si la data de finalitzaci� del tractament �s posterior a la data de c�lcul o si �s nul�la
        if not tractament_finalitzat:
            # Afegir el codi ATC al conjunt de tractaments del pacient
            sub_tractaments[id_cip_sec].add(atc_codi)
            # Comprovar si el codi PF est� dins dels codis PF i afegir-lo al conjunt de tractaments del pacient
            if pf_codi in codis_pf:
                sub_tractaments[id_cip_sec].add(pf_codi)
            # Comprovar si el codi ATC comen�a per "J01" (prescripci� relacionada amb pneum�nia) i afegir la data d'inici del tractament al conjunt de dates per pacient
            if atc_codi[:3] == "J01":
                dates_prescripcions_lligades_pneumonia[id_cip_sec].add(data_ini)
            # Afegir pacients amb tractament de bisfosfonats m�s llargs de 5 anys
            if atc_codi[:5] in codis_glucocorticoides_sistemics and dateutil.relativedelta.relativedelta(data_fi, data_ini).months >= 3: 
                glucocorticoides_sistemics_minim_3_mesos.add(id_cip_sec)                
        # Comprovar si el codi ATC est� dins dels codis antidepressius recomanats i afegir el pacient al conjunt historic_antidepressius_recomanats
        if atc_codi in codis_antidepressius_recomanats:
            historic_antidepressius_recomanats.add(id_cip_sec)

    return sub_tractaments, dates_prescripcions_lligades_pneumonia, historic_antidepressius_recomanats, glucocorticoides_sistemics_minim_3_mesos


if __name__ == "__main__":
    INDICADORS()
