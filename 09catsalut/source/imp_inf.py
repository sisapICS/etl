# -*- coding: utf-8 -*-

import sisapUtils as u
import sys
import collections as c
import dateutil.relativedelta
from datetime import timedelta
import sisaptools as su
import datetime as d

IS_TEST = False

#per obtenir tots els pacients amb prescripci� d'absorbents
#table = import.tractaments

class IND_INF():
    def __init__(self):
        self.get_conversor()
        u.printTime('conversor')
        self.get_centres()
        u.printTime('centres')
        self.get_poblacio()
        u.printTime('poblacio')
        self.get_prescripcions()
        u.printTime('prescripcions')
        self.get_indicadors()
        u.printTime('indicadors')
        self.create_tables_Khalix()
        u.printTime('khalix')
        # self.to_pdp()
        # u.printTime('to_pdp')
    
    # def to_pdp(self):
    #     sql = """
    #         SELECT up,uab,tipus,indicador,idpac,sector,exclos,comentari
    #         FROM impllistats
    #     """
    #     llistat = []
    #     for up,uab,tipus,indicador,idpac,sector,exclos,comentari in u.getAll(sql,'catsalut'):
    #         llistat.append((up,uab,tipus,indicador,idpac,sector,exclos,comentari))
    #     u.listToTable(llistat, 'ALTLLISTATS', 'pdp')
    #     sql = """
    #         SELECT dataany, datames, up, uab, tipus, indicador, numerador, denominador, resultat, noresolts
    #         FROM impindicadors
    #     """
    #     indicadors = []
    #     for dataany, datames, up, uab, tipus, indicador, numerador, denominador, resultat, noresolts in u.getAll(sql, 'catsalut'):
    #         indicadors.append((dataany, datames, up, uab, tipus, indicador, numerador, denominador, resultat, noresolts, 0, 0))
    #     u.listToTable(indicadors, 'ALTINDICADORS', 'pdp')

    def get_conversor(self):
        sql = "select id_cip_sec, codi_sector, hash_d from u11"
        self.ids = c.defaultdict()
        for id, sec, h in u.getAll(sql, 'import'):
            self.ids[id] = (sec,h)

    def get_poblacio(self):
        """Obtenir la informaci� de la poblaci�."""

        # Inicialitzar diccionaris per emmagatzemar la poblaci� assignada adulta i un diccionari conversor id_cip_sec a hash
        self.poblacio = dict()

        # Consulta SQL per obtenir la informaci� de la poblaci� amb edat igual o superior a 15 anys
        sql = """
                SELECT
                    id_cip_sec,
                    codi_sector,
                    up,
                    upinf,
                    uba,
                    ubainf,
                    edat,
                    sexe,
                    ates,
                    maca,
                    institucionalitzat
                FROM
                    assignada_tot
                WHERE
                    edat >= 15
            """
        for id_cip_sec, sector, up, upinf, uba, ubainf, edat, sexe, ates, maca, institucionalitzat in u.getAll(sql, "nodrizas"):
            if up in self.centres:
                self.poblacio[id_cip_sec] = {"up": up,
                                            "upinf": upinf,
                                            "uba": uba,
                                            "ubainf": ubainf,
                                            "edat": edat,
                                            "sexe": sexe,
                                            "ates": ates,
                                            "maca": maca,
                                            "institucionalitzat": institucionalitzat,
                                            "sector": sector}
    def get_centres(self):
        """Obtenir la informaci� dels Centres d'Atenci� Prim�ria."""

        sql = """
                SELECT
                    scs_codi,
                    ics_codi
                FROM
                    cat_centres
              """
        self.centres = {up: br for up, br in u.getAll(sql, "nodrizas")}
    
    def get_prescripcions(self):
        sql = """
                SELECT
                    data_ext
                FROM
                    dextraccio
             """
        self.dext, = u.getOne(sql, "nodrizas")
        # self.dext = d.date(2023,9,27)
        self.periode = "A{}".format(self.dext.strftime("%y%m"))
        absorbents = ['23C01', '23C02', '23C03', '23C04', '23C05', '23C06']
        self.prescripcions = c.defaultdict(set)
        
        particions_taules = u.getTablePartitions("tractaments", "import")
        jobs = [(absorbents, particio_taula, self.poblacio, self.dext) for particio_taula in particions_taules]
        u.printTime('post resultat')
        for prescripcions, calculs in u.multiprocess(sub_get_tractaments, jobs, 6):
            for key in prescripcions:
                for id in prescripcions[key]:
                    self.prescripcions[key].add(id)
            for cip, ind in calculs.items():
                if 'total' in calculs[cip] and calculs[cip]['total'] > 5:
                    self.prescripcions['+5 absorbents'].add(cip)
                if 'nit' in calculs[cip] and calculs[cip]['nit'] > 2:
                    self.prescripcions['+2 absorbents nit'].add(cip)
            u.printTime('despres multiprocess')
        u.printTime('despres sql pf_gt_codi')
        print(len(self.prescripcions['total']))
        print(len(self.prescripcions['rectangulars absorbents']))
        print(len(self.prescripcions['+5 absorbents']))
        print(len(self.prescripcions['+2 absorbents nit']))

    def get_indicadors(self):
        cond_ind = {
            'IMP043': {'NUM': self.prescripcions['+5 absorbents'], 'DEN': self.prescripcions['total'], 'EXCL':set()},
            'IMP044': {'NUM': self.prescripcions['rectangulars absorbents'], 'DEN': self.prescripcions['total'], 'EXCL':set()},
            'IMP045': {'NUM': self.prescripcions['+2 absorbents nit'], 'DEN': self.prescripcions['total'], 'EXCL':set()}
        }
        
        self.dades = set()
        resultats_up = c.Counter()
        self.resultats_up = list()
        resultats_uba = c.Counter()
        self.resultats_uba = list()
        self.resultats_altcataleg = c.defaultdict(c.Counter)
        # idpac = hash_d
        # necessitem altcatalegare (manual)
        # altcataleg (manual)
        # altllistats (up,uba,tipus,indicador,hash_d,sector,exclos,comentari)
        llistats = []
        landing = []
        
        for ind in cond_ind:
            # Per a cada pacient que compleixi les condicions del denominador de l'indicador
            for id_cip_sec in cond_ind[ind]['DEN']:
                if id_cip_sec in self.poblacio:
                    # Assignar 1 si el pacient compleix les condicions del numerador, 0 en cas contrari
                    num = 1 if id_cip_sec in cond_ind[ind]['NUM'] else 0
                    # Assignar 1 si el pacient compleix les condicions d'exclusi� del denominador, 0 en cas contrari
                    excl = 1 if id_cip_sec in cond_ind[ind]['EXCL'] else 0

                    up = self.poblacio[id_cip_sec]["up"]
                    uba = self.poblacio[id_cip_sec]["uba"]
                    ubainf = self.poblacio[id_cip_sec]["ubainf"]

                    if num == 1 and id_cip_sec in self.ids:
                        llistats.append((up,uba, 'M', ind, self.ids[id_cip_sec][1], self.ids[id_cip_sec][0],0,None))
                        llistats.append((up,ubainf, 'I', ind, self.ids[id_cip_sec][1], self.ids[id_cip_sec][0],0,None))
                    landing.append((id_cip_sec, ind, up, uba, ubainf, num))
                    edat, sexe = self.poblacio[id_cip_sec]["edat"], self.poblacio[id_cip_sec]["sexe"]
                    edat_agr, sexe_agr = u.ageConverter(edat), u.sexConverter(sexe)
                    # Emmagatzemar les dades dels pacients en el conjunt 'self.dades'
                    self.dades.add((id_cip_sec, ind, up, uba, edat_agr, sexe_agr, 1, num, excl))
                    # Incrementar els comptadors de denominador i numerador dels resultats
                    resultats_up[(ind, self.periode, up, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += 1 if not excl else 0
                    resultats_up[(ind, self.periode, up, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += num if not excl else 0
                    resultats_uba[(ind, self.periode, up, uba, 'M', 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += 1 if not excl else 0
                    resultats_uba[(ind, self.periode, up, uba, 'M', 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += num if not excl else 0
                    resultats_uba[(ind, self.periode, up, ubainf, 'I', 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += 1 if not excl else 0
                    resultats_uba[(ind, self.periode, up, ubainf, 'I', 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N')] += num if not excl else 0
                    # Incrementar els comptadors de denominador i numerador dels resultats per ALTCATALEG de pdp
                    if not excl:
                        self.resultats_altcataleg[(up, uba, "M", ind)]["DEN"] += 1 
                        self.resultats_altcataleg[(up, uba, "M", ind)]["NUM"] += num

        for (ind, self.periode, up, analisi, NOCAT, NOIMP, DIM6SET, N), n in resultats_up.items():
            self.resultats_up.append((ind, self.periode, self.centres[up], analisi, NOCAT, NOIMP, DIM6SET, N, n))
        for (ind, self.periode, up, uba, tipus, analisi, NOCAT, NOIMP, DIM6SET, N), n in resultats_uba.items():
            self.resultats_uba.append((ind, self.periode, self.centres[up], uba, tipus, analisi, NOCAT, NOIMP, DIM6SET, N, n))

        # VALIDACIO
        # altindicadors (dataany,datames,up,uba,tipus,indicador,numerador,denominador,resultat,noresolts)
        llistat_inds = []
        dataany = self.dext.year
        datames = self.dext.month
        for (ind, p, up, uba, tipus, analisi, NOCAT, NOIMP, DIM6SET, N), n in resultats_uba.items():
            if analisi == 'NUM':
                den = resultats_uba[((ind, p, up, uba, tipus, 'DEN', NOCAT, NOIMP, DIM6SET, N))]
                resultat = float(n)/float(den)
                # els no resolts han de ser iguals al numerador perque son indicadors inversos
                # en el cas contrari haurien de ser els pacients del denominador que no estan al numerador
                llistat_inds.append((dataany,datames,up,uba,tipus,ind,n,den,resultat,n))
        # up,uab,tipus,indicador,idpac,sector,exclos,comentari impllistats
        # dataany, datames, up, uab, tipus, indicador, numerador, denominador, resultat, noresolts impindicadors
        cols_indicadors = """(dataany int, datames int, up varchar(5),
                            uab varchar(20), tipus varchar(1), indicador varchar(10),
                            numerador int, denominador int, resultat double, noresolts int)"""
        cols_llistats = """(up varchar(5), uab varchar(10), tipus  varchar(1),
                            indicador varchar(8), idpac varchar(40), sector varchar(4),
                            exclos int, comentari varchar(500))"""
        u.createTable('impindicadors', cols_indicadors, 'catsalut', rm=True)
        u.createTable('impllistats', cols_llistats, 'catsalut', rm=True)
        u.listToTable(llistat_inds, 'impindicadors','catsalut')
        u.listToTable(llistats, 'impllistats','catsalut')
        cols = "(id_cip_sec int, ind varchar(10), up varchar(10), uba varchar(20), ubainf varchar(20), num int)"
        u.createTable('landing_imp', cols, 'catsalut', rm=True)
        u.listToTable(landing, 'landing_imp', 'catsalut')

    def create_tables_Khalix(self):
            """ Crea les taules a partir de les quals s'exportar� la informaci� a Khalix."""
            cond_ind = {
                'IMP043': self.prescripcions['+5 absorbents'],
                'IMP044': self.prescripcions['rectangulars absorbents'],
                'IMP045': self.prescripcions['+2 absorbents nit']
            }
            cols_up = '(indicador varchar(10), periode varchar(5), up varchar(5), analisi varchar(3),\
            nocat varchar(5), noimp varchar(5), dim6set varchar(7), n varchar(1), valor int)'
            u.createTable('IMP_INF_val', cols_up, 'catsalut', rm=True)
            cols_uba = '(indicador varchar(10), periode varchar(5), up varchar(5), uba varchar(7), tipus varchar(1), analisi varchar(3),\
                    nocat varchar(5), noimp varchar(5), dim6set varchar(7), n varchar(1), valor int)'
            u.createTable('IMP_UBA_INF_val', cols_uba, 'catsalut', rm=True)
            u.listToTable(self.resultats_up, 'IMP_INF_val', 'catsalut')
            u.listToTable(self.resultats_uba, 'IMP_UBA_INF_val', 'catsalut')
            cataleg = 'exp_ecap_IMP_cataleg'
            upload = [
                ('IMP043', 'Pacients amb m�s de 5 bolquers al dia', 43, 'IMP', 1, 1, 0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/5321/ver/', 'PCT'),
                ('IMP044', 'Pacients amb prescripci� de bolquer rectangular', 44, 'IMP', 1, 1, 0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/5322/ver/', 'PCT'),
                ('IMP045', 'Pacients amb m�s de 2 bolquers nit/super nit al dia', 45, 'IMP', 1, 1, 0,0,0,1,'http://10.80.217.201/sisap-umi/indicador/indicador/5323/ver/', 'PCT')
            ]
            u.listToTable(upload, cataleg, 'altres')
            # cataleg_pare = 'exp_ecap_IMP_catalegPare'
            # upload = [('IMP', 'Nous indicadors', 45, 'FARMAVAL')]
            # u.listToTable(upload, cataleg_pare, 'altres')

def sub_get_tractaments((codis_abs, particions, poblacio, dext)):
    prescripcions = c.defaultdict(set)
    calculs = c.defaultdict(dict)
    sql = """
        select id_cip_sec, pf_gt_codi, ppfmc_posologia, ppfmc_freq from
        {}, cat_cpftb006
        where ppfmc_pf_codi = pf_codi and pf_gt_codi in {}
        and (ppfmc_data_fi > '{}' or ppfmc_data_fi = '0000-00-00')""".format(particions, tuple(codis_abs), dext)
    for id_cip_sec, pf_codi, posologia, freq in u.getAll(sql, "import"):
        if id_cip_sec in poblacio:
            prescripcions['total'].add(id_cip_sec)
            if pf_codi in ('23C01', '23C03', '23C05'):
                prescripcions['rectangulars absorbents'].add(id_cip_sec)
            if freq > 0:
                total = round(float(posologia)/freq*24,2)
                if 'total' not in calculs[id_cip_sec].keys():
                    calculs[id_cip_sec]['total'] = total
                else:
                    calculs[id_cip_sec]['total'] += total
                if pf_codi in ('23C04', '23C05', '23C06'):
                    if 'nit' not in calculs[id_cip_sec].keys():
                        calculs[id_cip_sec]['nit'] = total
                    else:
                        calculs[id_cip_sec]['nit'] += total
    u.printTime('despres sql tractaments ' + str(particions))
    return (prescripcions, calculs)

if __name__ == "__main__":
    inici = d.datetime.now()
    try:
        u.printTime("Inici")
        IND_INF()
        u.printTime("Fi")
        fi = d.datetime.now()
        txt = """Hora inici {},
                hora fi {},
                total de {}""".format(str(inici), str(fi), str(fi-inici))
        mail = su.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "imp_inf.py b�!! :)"
        mail.text = txt
        mail.send()
    except Exception as e:
        fi = d.datetime.now()
        txt = """Hora inici {},
                hora fi {},
                total de {},
                error {}""".format(str(inici), str(fi), str(fi-inici), str(e))
        mail = su.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "error imp_inf.py"
        mail.text = txt
        mail.send()
        raise