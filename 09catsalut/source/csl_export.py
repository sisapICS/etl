# coding: utf8

"""
.
"""

import collections as c
import datetime as d
import sisapUtils as u

ind_2021 = {"AP19", "AP21", "SGAM02-AP", "AP36", "AP28", "AP29", "AP25bis",
            "AP30", "AP32", "AP33", "AP35", "AP34",
            "CAT0702", "CAT0703", "CAT0704", "CAT0709", "CAT0710", "CAT1103"}

ind_ped = {"CAT0702": {"Percentils": {"p25": 90.13,
                                      "p50": 92.11,
                                      "p75": 93.62},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 27,
                                  "p50-p75": 40.5,
                                  ">=p75": 54}},
           "CAT0703": {"Percentils": {"p25": 88.12,
                                      "p50": 91.37,
                                      "p75": 94.26},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 22.5,
                                  "p50-p75": 33.75,
                                  ">=p75": 45}},
           "CAT0704": {"Percentils": {"p25": 85.57,
                                      "p50": 89.25,
                                      "p75": 92.52},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 21,
                                  "p50-p75": 31.5,
                                  ">=p75": 42}},
           "CAT0709": {"Percentils": {"p25": 96.04,
                                      "p50": 97.61,
                                      "p75": 98.75},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 21.5,
                                  "p50-p75": 32.25,
                                  ">=p75": 43}},
           "CAT0710": {"Percentils": {"p25": 88.36,
                                      "p50": 91.57,
                                      "p75": 94.33},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 21.5,
                                  "p50-p75": 32.25,
                                  ">=p75": 43}},
           "CAT1103": {"Percentils": {"p25": 75.64,
                                      "p50": 82.81,
                                      "p75": 89.09},
                       "Punts":  {"<p25": 0,
                                  "p25-p50": 22,
                                  "p50-p75": 33,
                                  ">=p75": 44}},
           }

FILE = "SIAC_ECAP_{}{}.txt"


class CatSalut(object):
    """."""

    def __init__(self):
        """."""
        self.dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        self.dades = c.defaultdict(c.Counter)
        self.get_centres()
        self.get_linies()
        self.get_catsalut()
        self.get_ap35()
        self.get_eqa()
        self.get_pedia()
        self.get_ap34()
        self.get_icam()
        self.get_econsulta()
        self.get_accessibilitat()
        self.get_longitudinalitat()
        self.get_gida()
        self.get_upload()
        self.export_data()

    def get_centres(self):
        """."""
        self.conversio = {}
        sql = "select ics_codi, scs_codi from cat_centres"
        for br, up in u.getAll(sql, "nodrizas"):
            self.conversio[br] = up

    def get_linies(self):
        """."""
        self.linies = c.defaultdict(set)
        sql = "select distinct up, uporigen from cat_linies"
        for lp, up in u.getAll(sql, "nodrizas"):
            if lp == "04957":
                up = "04957"
            self.linies[lp].add(up)

    def get_catsalut(self):
        """."""
        codis = {"IAP01": "AP01", "IAP11BIS": "AP011bis", "IAP18": "AP18"}
        pobs = {"IAP01": ("NOINSATRC", "INSATRC"),
                "IAP11BIS": ("NOINSAT", "INSAT"),
                "IAP18": ("NOINSASS", "INSASS")}
        sql = "select indicador, comb, up, conc, n \
               from exp_khalix_up_ind_def \
               where indicador in {}".format(tuple(codis))
        for ind, pob, up, analisi, n in u.getAll(sql, "catsalut"):
            if pob in pobs[ind]:
                self.dades[(codis[ind], up, up)][analisi] += n

    def get_ap35(self):
        """ INDI003 """
        sql = "select indicador, up, conc, n \
               from exp_khalix_up_ind \
               where indicador = 'AP35' and comb='NOINSAT'"
        for ind, up, analisi, n in u.getAll(sql, "catsalut"):
            self.dades[(ind, up, up)][analisi] += n

    def get_eqa(self):
        """."""
        codis = {"EQA0306": "AP16",
                 "EQA0208": "AP19",
                 "EQA0210": "AP36",
                 "EQA0301": "RS_AP02",
                 "EQA0209": "AP21",
                 "EQA0312": "AP22",
                 "EQA0235": "AP28",
                 "EQA0204": "AP29",
                 "EQA0201": "AP30"}
        sql = "select left(indicador, 7), up, conc, n from exp_khalix_up_ind \
               where left(indicador, 7) in {} and \
                     comb in ('NOINSAT', 'INSAT')".format(tuple(codis))
        for ind, up, analisi, n in u.getAll(sql, "eqa_ind"):
            self.dades[(codis[ind], up, up)][analisi] += n
        mes = self.dext.strftime("%m")
        if mes in ('09', '10', '11', '12'):
            codis = {"EQA0501": "AP32"}
            sql = "select left(indicador, 7), up, conc, n from exp_khalix_up_ind \
                   where left(indicador, 7) = '{}' and \
                         comb in ('NOINSAT', 'INSAT')".format(tuple(codis)[0])
            for ind, up, analisi, n in u.getAll(sql, "eqa_ind"):
                self.dades[(codis[ind], up, up)][analisi] += n

    def get_pedia(self):
        """."""
        sql = "select id_cip_sec, up, num, den \
               from mst_indicadors_pacient \
               where indicador = 'EQA0708' and \
                     ates = 1 and maca = 0 and excl = 0"
        for _id, up, num, den in u.getAll(sql, "pedia"):
            if up in self.linies:
                for uplp in self.linies[up]:
                    self.dades[('AP04', up, uplp)]["NUM"] += num
                    self.dades[('AP04', up, uplp)]["DEN"] += den
            else:
                self.dades[('AP04', up, up)]["NUM"] += num
                self.dades[('AP04', up, up)]["DEN"] += den

        self.dadesped = c.defaultdict(c.Counter)
        sql = """
        select
            id_cip_sec, indicador, up, num, den
        from
            mst_indicadors_pacient
        where
            indicador in ('EQA0702', 'EQA0703', 'EQA0704',
                          'EQA0709', 'EQA0710', 'EQA1103') and
            ates = 1 and
            maca = 0 and
            excl = 0
        """
        for _id, indicador, up, num, den in u.getAll(sql, "pedia"):
            indicador_new = 'CAT' + indicador[3:]
            if up in self.linies:
                for uplp in self.linies[up]:
                    self.dadesped[(indicador_new, up, uplp)]["NUM"] += num
                    self.dadesped[(indicador_new, up, uplp)]["DEN"] += den
            else:
                self.dadesped[(indicador_new, up, up)]["NUM"] += num
                self.dadesped[(indicador_new, up, up)]["DEN"] += den

        for (indicador, up, uplp), dades in self.dadesped.items():
            if indicador in ind_ped:
                num = dades["NUM"]
                den = dades["DEN"]
                if num and den:
                    res = round(num / float(den), 4)
                else:
                    res = 0
                self.dades[(indicador, up, uplp)]["DEN"] = 1
                if res < ind_ped[indicador]["Percentils"]["p25"]/100:
                    punts = ind_ped[indicador]["Punts"]["<p25"]
                elif (res >= ind_ped[indicador]["Percentils"]["p25"]/100 and
                      res < ind_ped[indicador]["Percentils"]["p50"]/100):
                    punts = ind_ped[indicador]["Punts"]["p25-p50"]
                elif (res >= ind_ped[indicador]["Percentils"]["p50"]/100 and
                      res < ind_ped[indicador]["Percentils"]["p75"]/100):
                    punts = ind_ped[indicador]["Punts"]["p50-p75"]
                elif res >= ind_ped[indicador]["Percentils"]["p75"]/100:
                    punts = ind_ped[indicador]["Punts"][">=p75"]
                self.dades[(indicador, up, uplp)]["NUM"] = punts

    def get_ap34(self):
        """ . """
        self.dadesap34 = c.defaultdict(c.Counter)
        for (indicador, up, uplp), dades in self.dadesped.items():
            if indicador in ind_ped:
                num = dades["NUM"]
                den = dades["DEN"]
                if num and den:
                    res = round(num / float(den), 4)
                else:
                    res = 0
                self.dadesap34[('AP34', up, uplp)]["DEN"] = 1
                if res < ind_ped[indicador]["Percentils"]["p25"]/100:
                    punts = ind_ped[indicador]["Punts"]["<p25"]
                elif (res >= ind_ped[indicador]["Percentils"]["p25"]/100 and
                      res < ind_ped[indicador]["Percentils"]["p50"]/100):
                    punts = ind_ped[indicador]["Punts"]["p25-p50"]
                elif (res >= ind_ped[indicador]["Percentils"]["p50"]/100 and
                      res < ind_ped[indicador]["Percentils"]["p75"]/100):
                    punts = ind_ped[indicador]["Punts"]["p50-p75"]
                elif res >= ind_ped[indicador]["Percentils"]["p75"]/100:
                    punts = ind_ped[indicador]["Punts"][">=p75"]
                self.dadesap34[('AP34', up, uplp)]["NUM"] += punts

        for (indicador, up, uplp), dades in self.dadesap34.items():
            num = dades["NUM"]
            den = dades["DEN"]
            self.dades[(indicador, up, uplp)]["NUM"] = num
            self.dades[(indicador, up, uplp)]["DEN"] = 1

    def get_icam(self):
        """."""
        codis = ("IT003OST", "IT003MEN", "IT003TRA", "IT003SIG")
        sql = "select entity, analisi, valor from exp_khalix_it \
               where indicador in {} and \
                     length(entity) = 5".format(codis)
        for br, analisi, n in u.getAll(sql, "altres"):
            self.dades[(
                "SGAM02-AP",
                self.conversio[br],
                self.conversio[br]
                )][analisi] += n

    def get_econsulta(self):
        """."""
        sql = "select br, analisis, n from exp_khalix_up_econsulta \
               where indicador like 'ECONS0001%'"
        for br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[("AP26bis", up, up)][analisi] += n

    def get_accessibilitat(self):
        """."""
        codis = {"QACC5D": "AP23", "QACC10D": "AP24"}
        sql = "select k0, k2, k3, v from exp_qc_forats \
               where k4 = 'ANUAL' and \
                     k0 in {}".format(tuple(codis))
        for ind, br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[(codis[ind], up, up)][analisi] += n

    def get_longitudinalitat(self):
        """."""
        sql = "select up, analisi, resultat from exp_long_cont_up \
               where indicador ='CONT0002'"
        for br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[("AP25", up, up)][analisi] += n
        sql = "select up, analisi, resultat from exp_long_cont_up_a \
               where indicador ='CONT0002A' and detalle = 'TIPPROF1'"
        for br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[("AP25bis", up, up)][analisi] += n

    def get_gida(self):
        """."""
        sql = "select ent, analisi, valor \
               from exp_khalix_gida \
               where ind = 'GESTINF05' and length(ent) = 5 and analisi = 'NUM'"
        for br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[("AP33", up, up)][analisi] += n
        sql = "select distinct ent, analisi, valor \
               from exp_khalix_gida \
               where ind = 'GESTINF05' and length(ent) = 5 and analisi = 'DEN'"
        for br, analisi, n in u.getAll(sql, "altres"):
            up = self.conversio[br]
            if up not in self.linies:
                self.dades[("AP33", up, up)][analisi] += n

    def get_upload(self):
        """."""
        self.upload = []
        self.uploadvells = []
        especials = {"AP18": 1}
        for (ind, up_ics, up_rca), dades in self.dades.items():
            if especials.get(ind, 0) >= self.dext.month:
                periode = self.dext.year - 1
            else:
                periode = self.dext.year
            estat = "D" if especials.get(ind, 12) == self.dext.month else "P"
            num = dades["NUM"]
            den = dades["DEN"]
            if num and den:
                if ind not in ('CAT0702', 'CAT0703', 'CAT0704', 'CAT0709',
                               'CAT0710', 'CAT1103', 'AP34'):
                    res = str(round(num / float(den), 4)).replace(".", ",")
                else:
                    res = str(num).replace(".", ",")
            else:
                res = 0
            this = (periode, ind, up_ics, up_rca, res, int(num), int(den),
                    self.dext.strftime("%d/%m/%Y"), "", estat,
                    d.date.today().strftime("%d/%m/%Y"))
            self.upload.append(this)

    def export_data(self):
        """."""
        mes = self.dext.strftime("%m")
        file = FILE.format(self.dext.year, mes)
        u.writeCSV(u.tempFolder + file,
                   [('Periode', 'Indicador', 'UP_ICS', 'UP', 'Resultat',
                     'Numerador', 'Denominador', 'Data Extraccio', 'Nulo',
                     'Estat', 'Data Execucio')] + self.upload,
                   sep=";")
        u.writeCSV(u.tempFolder + file, self.upload, sep=";")
        text = """Adjuntem arxiu SIAC_ECAP SISAP corresponent a {}/{}.
        """.format(mes, self.dext.year)
        text = """Benvolguts,\r\n\r\n{}\r\n\r\nSalutacions cordials,
        \r\n\r\nSISAP""".format(text)
        u.sendGeneral(
            'SISAP <sisap@gencat.cat>',
            'ecros@catsalut.cat',
            'sisap@gencat.cat',
            'Dades SIAC_ECAP SISAP',
            text,
            u.tempFolder + file)
        u.sshPutFile(file, "export", "catsalut", subfolder="ICS/")
        u.sshChmod(file, "export", "catsalut", 0666, subfolder="ICS/")
        print("Export data")


if __name__ == "__main__":
    if u.IS_MENSUAL:
        CatSalut()
