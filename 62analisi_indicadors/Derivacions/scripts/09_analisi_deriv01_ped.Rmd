***

## Pediatria

### Mensual

```{r}
var.ind <- "DERIV01P"
var.ind.text <- "DERIV01P: Número de derivacions per especialitat (Numerador)"
dt.ana <- dt.ind.der.flt[indicador == var.ind & especialitat != "DERESPECIALITAT"]
var.periode <- max(dt.ind.der.flt$periode)
```

A continuació s'analitza el indicador 1: 

- **Número de derivacions per especialitat**

### Denominador

No hi ha denominador.

### Numerador

```{r}
dt.ggplot <- dt.ana[analisis == "NUM"]
gg.agr.oc <- ggplot(dt.ggplot, aes(x = n)) +
  geom_histogram(color = "white", fill = "orange") +
  # scale_y_continuous(labels = scales::percent) +
  labs(title = var.ind.text,
       caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "Numerador",
       y = "Número de registres") +  
  theme_minimal()
  # theme(legend.position = "bottom", legend.title = element_blank(), 
  #       axis.title.x = element_blank(), 
  #       plot.caption = element_text(size = 8, colour = "grey35")) +
gg.agr.oc

tail(dt.ggplot[,.(periode, br, up_origen_desc, especialitat,n)][order(n)])
```

#### Top 10 Especialitats

```{r}

dt.agr  <- dt.ana[, .(Nregistres = .N,
                      Numerador = sum(n[analisis == "NUM"]),
                      Nperiodes = n_distinct(periode),
                      NUPOrigen = n_distinct(br),
                      NUPDesti = n_distinct(br_desti)), especialitat][order(-Numerador)]

datatable(dt.agr,
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.agr)))
```

### Resultat

El resultat és el Numerador.

#### Període

Es calcula el resultat per UP / Periode

```{r}
dt.ana.agr <- dt.ana[, .(resultat = sum(n)),
                         .(up_origen_desc, periode)]
#dt.ana.agr[, resultat := round((num/den)*1000,3)]

gg.agr.oc <- ggplot(dt.ana.agr, aes(x = resultat)) +
  geom_histogram(color = "white", fill = "orange") +
  # scale_y_continuous(labels = scales::percent) +
  labs(title = var.ind.text,
       caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "Resultat",
       y = "Número de registres") +  
  theme_minimal()
  # theme(legend.position = "bottom", legend.title = element_blank(), 
  #       axis.title.x = element_blank(), 
  #       plot.caption = element_text(size = 8, colour = "grey35")) +
gg.agr.oc
```

```{r, fig.height = 16, fig.width = 8}
gg.agr.oc <- ggplot(dt.ana.agr, aes(x = periode, y = resultat)) +
    geom_dotplot(binaxis = "y", stackdir = "center", binwidth = 0.8, fill = "dark red") +
    labs(title = var.ind.text,
         caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
         x = "Periode",
         y = "Resultat") +  
    theme_minimal() +
    coord_flip()
gg.agr.oc
```


#### Especialitat

Es calcula el resultat per UP / Periode / Especialitat

```{r, fig.height = 16, fig.width = 8}

dt.ana.agr <- dt.ana[, .(resultat = sum(n)),
                       .(up_origen_desc, periode, indicador, especialitat)]

ggplot(dt.ana.agr[indicador == var.ind & periode == var.periode,],
       aes(x = especialitat, y = resultat)) +
    geom_dotplot(binaxis = "y", stackdir = "center", binwidth = 0.1, fill = "dark red") +
    labs(title = var.ind.text,
         subtitle = paste0("Periode ", var.periode),
         caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
         x = "Especialitat",
         y = "Resultat") + 
    theme_minimal() +
    theme(text = element_text(size = 12)) +
    coord_flip()
```

```{r}
gc.var <- gc()
```
