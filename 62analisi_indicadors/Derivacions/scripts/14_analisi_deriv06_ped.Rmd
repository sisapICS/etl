
## Pediatria

### Mensual

```{r}
var.ind <- "DERIV06P"
var.ind.text <- "DERIV06P: Percentage de derivacions realitzades"
dt.ana <- dt.ind.der.flt[indicador == var.ind][, c("indicador", "periode", "tip_eap","br", "up_origen_desc", "agrupador_prova", "especialitat", "br_desti", "analisis", "n")]
var.periode <- max(dt.ind.der.flt$periode)
```

A continuació s'analitza el indicador 6: 

- **Percentage de derivacions realitzades**

#### Denominador

```{r}
dt.ggplot <- dt.ana[analisis == "DEN"]
gg.agr.oc <- ggplot(dt.ggplot, aes(x = n)) +
  geom_histogram(color = "white", fill = "orange") +
  # scale_y_continuous(labels = scales::percent) +
  labs(title = var.ind.text,
       caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "Denominador",
       y = "Número de registres") +  
  theme_minimal()
  # theme(legend.position = "bottom", legend.title = element_blank(), 
  #       axis.title.x = element_blank(), 
  #       plot.caption = element_text(size = 8, colour = "grey35")) +
gg.agr.oc

tail(dt.ggplot[,.(periode, br, up_origen_desc, especialitat,n)][order(n)])
```

#### Numerador

```{r}
dt.ggplot <- dt.ana[analisis == "NUM"]
gg.agr.oc <- ggplot(dt.ggplot, aes(x = n)) +
  geom_histogram(color = "white", fill = "orange") +
  # scale_y_continuous(labels = scales::percent) +
  labs(title = var.ind.text,
       caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "Numerador",
       y = "Número de registres") +  
  theme_minimal()
gg.agr.oc

tail(dt.ggplot[,.(periode, br, up_origen_desc, especialitat,n)][order(n)])
```

#### Resultat

```{r}
# Aconseguir el denominador
  dt.ana.den <- dt.ana[analisis == "DEN"]
  dt.ana.den[, analisis := NULL]
  setnames(dt.ana.den, "n", "den")
  
# Aconseguir el numaredor
  dt.ana.num <- dt.ana[analisis == "NUM"]
  dt.ana.num[, analisis := NULL]
  setnames(dt.ana.num, "n", "num")
  
  dt.ana <- merge(dt.ana.den,
                  dt.ana.num,
                  by.x = c("indicador", "periode", "br", "up_origen_desc",
                           "agrupador_prova", "especialitat", "br_desti"),
                  by.y = c("indicador", "periode", "br", "up_origen_desc", 
                           "agrupador_prova", "especialitat", "br_desti"),
                  all = TRUE)
  
# Hi han valor NULL a num, es transformen a 0
  dt.ana[is.na(num), num := 0]
```


```{r}
dt.ana.agr <- dt.ana[, .(num = sum(num),
                         den = sum(den))
                     , .(up_origen_desc, periode)]
dt.ana.agr[, resultat := round((num/den)*100,1)]

gg.agr.oc <- ggplot(dt.ana.agr, aes(x = resultat)) +
  geom_histogram(color = "white", fill = "orange") +
  # scale_y_continuous(labels = scales::percent) +
  labs(title = var.ind.text,
       caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "Resultat",
       y = "Número de registres") +  
  theme_minimal()
gg.agr.oc
```

##### Periode

```{r, fig.height = 16, fig.width = 8}
ggplot(dt.ana.agr, aes(x = periode, y = resultat)) +
    geom_dotplot(binaxis = "y", stackdir = "center", binwidth = 0.5, fill = "dark red") +
    labs(title = var.ind.text,
         caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
         x = "Periode",
         y = "Resultat") +  
    coord_flip() +
    theme_minimal()

ggplot(dt.ana.agr, aes(x = periode, y = resultat)) +
    geom_violin(color = "darkred", fill = "darkred") +
    labs(title = var.ind.text,
         caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
         x = "Periode",
         y = "Resultat") +  
    coord_flip() +
    theme_minimal()
```

##### Especialitat

```{r}
dt.ana.agr <- dt.ana[, .(num = sum(num),
                         den = sum(den)),
                       .(up_origen_desc, periode, indicador, especialitat)]
dt.ana.agr[, resultat := round((num/den)*100,1)]
```


```{r, fig.height = 16, fig.width = 8}
ggplot(dt.ana.agr[indicador == var.ind & periode == var.periode,],       
       aes(x = especialitat, y = resultat)) +
    geom_dotplot(binaxis = "y", stackdir = "center", binwidth = 0.6, fill = "dark red") +
    labs(title = var.ind.text,
         subtitle = paste0("Periode ", var.periode),
         caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
         x = "Especialitat",
         y = "Resultat") + 
    theme_minimal() +
    theme(text = element_text(size = 12)) +
    coord_flip()
```

```{r}
gc.var <- gc()
```


