***

### Anual

```{r}
var.ind <- "DERIV01_12M"
var.ind.text <- "DERIV01_12M: Número de derivacions per especialitat (Numerador) (Anual)"
dt.ana <- dt.ind.der.flt[indicador == var.ind & especialitat != "DERESPECIALITAT"][, c("indicador", "periode", "tip_eap","br", "up_origen_desc", "agrupador_prova", "especialitat", "br_desti", "analisis", "n")]
var.periode <- max(dt.ind.der.flt$periode)
```

A continuació s'analitza el indicador 1 anual: 

- **Número de derivacions per especialitat (Numerador) (Anual)**

#### Denominador

No hi ha denominador.

#### Numerador

```{r}
dt.ggplot <- dt.ana[analisis == "NUM"]
gg.agr.oc <- ggplot(dt.ggplot, aes(x = n)) +
  geom_histogram(color = "white", fill = "orange") +
  # scale_y_continuous(labels = scales::percent) +
  labs(title = var.ind.text,
       caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "Numerador",
       y = "Número de registres") +  
  theme_minimal()
  # theme(legend.position = "bottom", legend.title = element_blank(), 
  #       axis.title.x = element_blank(), 
  #       plot.caption = element_text(size = 8, colour = "grey35")) +
gg.agr.oc

tail(dt.ggplot[,.(periode, br, up_origen_desc, especialitat,n)][order(n)])
```

#### Resultat

El resultat és el Numerador

Es calcula el resultat per UP / Periode

```{r}
dt.ana.agr <- dt.ana[, .(resultat = sum(n)),
                     , .(up_origen_desc, periode)]

gg.agr.oc <- ggplot(dt.ana.agr, aes(x = resultat)) +
  geom_histogram(color = "white", fill = "orange") +
  # scale_y_continuous(labels = scales::percent) +
  labs(title = var.ind.text,
       caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "Resultat",
       y = "Número de registres") +  
  theme_minimal()
gg.agr.oc
```

##### Especialitat

Es calcula el resultat per UP / Periode / Especialitat

```{r}
dt.ana.agr <- dt.ana[, .(resultat = sum(n)),
                       .(up_origen_desc, periode, indicador, especialitat)]
ggplot(dt.ana.agr[indicador == var.ind &
                  periode == var.periode &
                  especialitat %in% c("DERTRA", "DEROFT", "DERDER", "DERREH", "DERORL",
                                      "DERCIR", "DERURO", "DERNRL", "DERDIG", "DERCAR"),],
       aes(x = especialitat, y = resultat)) +
    geom_dotplot(binaxis = "y", stackdir = "center", binwidth = 5, fill = "dark red") +
    labs(title = var.ind.text,
         subtitle = paste0("Periode ", "Anual"),
         caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
         x = "Especialitat",
         y = "Resultat") + 
    theme_minimal() +
    theme(text = element_text(size = 12)) +
    coord_flip()
```


```{r, fig.height = 16, fig.width = 8}
ggplot(dt.ana.agr[indicador == var.ind & periode == var.periode,],       
       aes(x = especialitat, y = resultat)) +
    geom_dotplot(binaxis = "y", stackdir = "center", binwidth = 3, fill = "dark red") +
    labs(title = var.ind.text,
         subtitle = paste0("Periode ", "Anual"),
         caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
         x = "Especialitat",
         y = "Resultat") + 
    theme_minimal() +
    theme(text = element_text(size = 12)) +
    coord_flip()
```

```{r}
gc.var <- gc()
```


