
### Anual

```{r}
var.ind <- "DERIV05P_12M"
var.ind.text <- "DERIV05P_12M: Mitjana de dies de demora de les peticions programades (Anual)"
dt.ana <- dt.ind.der.flt[indicador == var.ind][, c("indicador", "periode", "tip_eap","br", "up_origen_desc", "agrupador_prova", "especialitat", "br_desti", "analisis", "n")]
var.periode <- max(dt.ind.der.flt$periode)
```

A continuació s'analitza el indicador 5 anual:

- **Mitjana de dies de demora de les peticions programades (Anual)**

#### Denominador

```{r}
dt.ggplot <- dt.ana[analisis == "DEN"]
gg.agr.oc <- ggplot(dt.ggplot, aes(x = n)) +
  geom_histogram(color = "white", fill = "orange") +
  # scale_y_continuous(labels = scales::percent) +
  labs(title = var.ind.text,
       caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "Denominador",
       y = "Número de registres") +  
  theme_minimal()
  # theme(legend.position = "bottom", legend.title = element_blank(), 
  #       axis.title.x = element_blank(), 
  #       plot.caption = element_text(size = 8, colour = "grey35")) +
gg.agr.oc

tail(dt.ggplot[,.(periode, br, up_origen_desc, especialitat,n)][order(n)])
```

- LLista de UP amb denominador = 0

```{r}

datatable(unique(dt.ggplot[n == 0, .(tip_eap, up_origen_desc)]),
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(unique(dt.ggplot[n == 0, .(tip_eap, up_origen_desc)]))))
```

#### Numerador

```{r}
dt.ggplot <- dt.ana[analisis == "NUM"]
gg.agr.oc <- ggplot(dt.ggplot, aes(x = n)) +
  geom_histogram(color = "white", fill = "orange") +
  # scale_y_continuous(labels = scales::percent) +
  labs(title = var.ind.text,
       caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "Numerador",
       y = "Número de registres") +  
  theme_minimal()
  # theme(legend.position = "bottom", legend.title = element_blank(), 
  #       axis.title.x = element_blank(), 
  #       plot.caption = element_text(size = 8, colour = "grey35")) +
gg.agr.oc

tail(dt.ggplot[,.(periode, br, up_origen_desc, especialitat,n)][order(n)])
```

#### Resultat

Es calcula el resultat per UP

```{r}
# Aconseguir el denominador
  dt.ana.den <- dt.ana[analisis == "DEN"]
  dt.ana.den[, analisis := NULL]
  setnames(dt.ana.den, "n", "den")
  
# Aconseguir el numaredor
  dt.ana.num <- dt.ana[analisis == "NUM"]
  dt.ana.num[, analisis := NULL]
  setnames(dt.ana.num, "n", "num")
  
  dt.ana <- merge(dt.ana.den,
                  dt.ana.num,
                  by.x = c("indicador", "periode", "tip_eap",
                           "br", "up_origen_desc", "agrupador_prova",
                           "especialitat", "br_desti"),
                  by.y = c("indicador", "periode", "tip_eap",
                           "br", "up_origen_desc", "agrupador_prova",
                           "especialitat", "br_desti"),
                  all = TRUE)
  
# Hi han valor NULL a num, es transformen a 0
  dt.ana[is.na(num), num := 0]  
```


```{r}
dt.ana.agr <- dt.ana[, .(num = sum(num),
                         den = sum(den))
                     , .(up_origen_desc, periode)]
dt.ana.agr[, resultat := round((num/den)*100,1)]

gg.agr.oc <- ggplot(dt.ana.agr, aes(x = resultat)) +
  geom_histogram(color = "white", fill = "orange") +
  # scale_y_continuous(labels = scales::percent) +
  labs(title = var.ind.text,
       caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
       x = "Resultat",
       y = "Número de registres") +  
  theme_minimal()
gg.agr.oc

tail(dt.ana.agr[,.(periode, up_origen_desc, resultat)][order(resultat)])
```

##### Especialitat

Es calcula el resultat per UP / Especialitat

```{r}
dt.ana.agr <- dt.ana[, .(num = sum(num),
                         den = sum(den)),
                       .(up_origen_desc, periode, indicador, especialitat)]
dt.ana.agr[, resultat := round((num/den)*100,1)]
```


```{r, fig.height = 16, fig.width = 8}
ggplot(dt.ana.agr[indicador == var.ind & periode == var.periode,],       
       aes(x = especialitat, y = resultat)) +
    geom_dotplot(binaxis = "y", stackdir = "center", binwidth = 40, fill = "dark red") +
    labs(title = var.ind.text,
         subtitle = paste0("Periode ", "Anual"),
         caption = "Font: Sistema d’Informació dels Serveis d’Atenció Primària (SISAP) [ICS]",
         x = "Especialitat",
         y = "Resultat") + 
    theme_minimal() +
    theme(text = element_text(size = 12)) +
    coord_flip()
```

```{r}
gc.var <- gc()
```


