***
***

# Data Manager

## Korra

```{r}

dt.korra.dm <- dt.korra.raw
```

### Creació noves variables

Creació de les noves variables.

```{r}

dt.korra.dm[edad_c == "EC01", edad_c := "EC0001"]
dt.korra.dm[edad_c == "EC24", edad_c := "EC0204"]
dt.korra.dm[edad_c == "EC59", edad_c := "EC0509"]
#dt.korra.dm[, .N, edad_c][order(edad_c)]
```

### Filtrar Adult

```{r}
dt.korra.dm <- dt.korra.dm[!(edad_c %in% c("EC0001", "EC0204", "EC0509", "EC1014")),]
#dt.korra.dm[, .N, edad_c][order(edad_c)]
```

```{r}
dt.korra.flt <- dt.korra.dm
dt.korra.flt <- dt.korra.flt[, paste0(c("poblacio", 
                                      "sum_dona", "sum_edat", 
                                      "institucionalitzats", "immigrants_renta_baixa", "pensionistes", "pens_menys_65", 
                                      "pcc", "maca", "atdom", 
                                      "den_gma", "num_gma", 
                                      "num_medea", "den_medea", 
                                      "prof_grup9", "prof_grup2",
                                      "den_seccio", 
                                      "ist", 
                                      "pob_ocupada", "treb_baixa_qualitat",
                                      "estudis_baixos", "jove_sense_estudis",
                                      "imm_renda_baixa_idescat", "renda_minima"),
                              "_ates") := lapply(.SD[ates == 1], sum, na.rm = T),
                          .SDcols = c("poblacio", 
                                      "sum_dona", "sum_edat", 
                                      "institucionalitzats", "immigrants_renta_baixa", "pensionistes", "pens_menys_65",
                                      "pcc", "maca", "atdom", 
                                      "den_gma", "num_gma", 
                                      "num_medea", "den_medea", 
                                      "prof_grup9", "prof_grup2",
                                      "den_seccio", 
                                      "ist", 
                                      "pob_ocupada", "treb_baixa_qualitat",
                                      "estudis_baixos", "jove_sense_estudis",
                                      "imm_renda_baixa_idescat", "renda_minima"),
                          by = c('up', "br")]

dt.korra.flt <- dt.korra.flt[, paste0(c("poblacio", 
                                      "sum_dona", "sum_edat", 
                                      "institucionalitzats", "immigrants_renta_baixa", "pensionistes", "pens_menys_65", 
                                      "pcc", "maca", "atdom", 
                                      "den_gma", "num_gma", 
                                      "num_medea", "den_medea", 
                                      "prof_grup9", "prof_grup2",
                                      "den_seccio", 
                                      "ist", 
                                      "pob_ocupada", "treb_baixa_qualitat",
                                      "estudis_baixos", "jove_sense_estudis",
                                      "imm_renda_baixa_idescat", "renda_minima"),
                              "_assignat") := lapply(.SD, sum, na.rm = T),
                          .SDcols = c("poblacio", 
                                      "sum_dona", "sum_edat", 
                                      "institucionalitzats", "immigrants_renta_baixa", "pensionistes", "pens_menys_65",
                                      "pcc", "maca", "atdom", 
                                      "den_gma", "num_gma", 
                                      "num_medea", "den_medea", 
                                      "prof_grup9", "prof_grup2",
                                      "den_seccio", 
                                      "ist", 
                                      "pob_ocupada", "treb_baixa_qualitat",
                                      "estudis_baixos", "jove_sense_estudis",
                                      "imm_renda_baixa_idescat", "renda_minima"),
                          by = c('up', "br")]

dt.korra.flt.agr.up <- unique(dt.korra.flt[, c("up", "br", 
                                       paste0(c("poblacio", 
                                      "sum_dona", "sum_edat", 
                                      "institucionalitzats", "immigrants_renta_baixa", "pensionistes", "pens_menys_65",
                                      "pcc", "maca", "atdom", 
                                      "den_gma", "num_gma", 
                                      "num_medea", "den_medea", 
                                      "prof_grup9", "prof_grup2",
                                      "den_seccio", 
                                      "ist", 
                                      "pob_ocupada", "treb_baixa_qualitat",
                                      "estudis_baixos", "jove_sense_estudis",
                                      "imm_renda_baixa_idescat", "renda_minima"), "_ates"),
                                       paste0(c("poblacio", 
                                      "sum_dona", "sum_edat", 
                                      "institucionalitzats", "immigrants_renta_baixa", "pensionistes", "pens_menys_65",
                                      "pcc", "maca", "atdom", 
                                      "den_gma", "num_gma", 
                                      "num_medea", "den_medea", 
                                      "prof_grup9", "prof_grup2",
                                      "den_seccio", 
                                      "ist", 
                                      "pob_ocupada", "treb_baixa_qualitat",
                                      "estudis_baixos", "jove_sense_estudis",
                                      "imm_renda_baixa_idescat", "renda_minima"),
                                      "_assignat"))])
```

```{r}
dt.korra.flt.agr.up[, ":="(
  dona_ATES = sum_dona_ates/poblacio_ates*100,
  edat_ATES = sum_edat_ates/poblacio_ates,
  institucionalitzats_ATES = institucionalitzats_ates/poblacio_ates*100,
  immigrants_renta_baixa_ATES = immigrants_renta_baixa_ates/poblacio_ates*100,
  pensionistes_ATES = pensionistes_ates/poblacio_ates*100,
  pens_menys_65_ATES = pens_menys_65_ates/poblacio_ates*100,
  pcc_ATES = pcc_ates/poblacio_ates*100,
  maca_ATES = maca_ates/poblacio_ates*100,
  atdom_ATES = atdom_ates/poblacio_ates*100,
  gma_ATES = num_gma_ates/den_gma_ates,
  prof_grup9_ATES = prof_grup9_ates/poblacio_ates*100,
  prof_grup2_ATES = prof_grup2_ates/poblacio_ates*100,
  ratio_prof_grup9grup2_ATES = round(prof_grup9_ates/prof_grup2_ates,2),
  ist_ATES = ist_ates/den_seccio_ates,
  pob_ocupada_ATES = pob_ocupada_ates/den_seccio_ates,
  treb_baixa_qualitat_ATES = treb_baixa_qualitat_ates/den_seccio_ates,
  estudis_baixos_ATES = estudis_baixos_ates/den_seccio_ates,
  jove_sense_estudis_ATES = jove_sense_estudis_ates/den_seccio_ates,
  imm_renda_baixa_idescat_ATES = imm_renda_baixa_idescat_ates/den_seccio_ates,
  renda_minima_ATES = renda_minima_ates/den_seccio_ates,
  
  dona_ASSIGN = sum_dona_assignat/poblacio_assignat*100,
  edat_ASSIGN = sum_edat_assignat/poblacio_assignat,
  institucionalitzats_ASSIGN = institucionalitzats_assignat/poblacio_assignat*100,
  immigrants_renta_baixa_ASSIGN = immigrants_renta_baixa_assignat/poblacio_assignat*100,
  pensionistes_ASSIGN = pensionistes_assignat/poblacio_assignat*100,
  pens_menys_65_ASSIGN = pens_menys_65_assignat/poblacio_assignat*100,
  pcc_ASSIGN = pcc_assignat/poblacio_assignat*100,
  maca_ASSIGN = maca_assignat/poblacio_assignat*100,
  atdom_ASSIGN = atdom_assignat/poblacio_assignat*100,
  gma_ASSIGN = num_gma_assignat/den_gma_assignat,
  prof_grup9_ASSIGN = prof_grup9_assignat/poblacio_assignat*100,
  prof_grup2_ASSIGN = prof_grup2_assignat/poblacio_assignat*100,
  ratio_prof_grup9grup2_ASSIGN = round(prof_grup9_assignat/prof_grup2_assignat,2),
  ist_ASSIGN = ist_assignat/den_seccio_assignat,
  pob_ocupada_ASSIGN = pob_ocupada_assignat/den_seccio_assignat,
  treb_baixa_qualitat_ASSIGN = treb_baixa_qualitat_assignat/den_seccio_assignat,
  estudis_baixos_ASSIGN = estudis_baixos_assignat/den_seccio_assignat,
  jove_sense_estudis_ASSIGN = jove_sense_estudis_assignat/den_seccio_assignat,
  imm_renda_baixa_idescat_ASSIGN = imm_renda_baixa_idescat_assignat/den_seccio_assignat,
  renda_minima_ASSIGN = renda_minima_assignat/den_seccio_assignat
)]
```

***

## Derivacions

```{r}

dt.ind.der.dm <- dt.ind.der.raw
gc.var <- gc()
```

A continuació es detalla el procés de manipulació de les dades per aconseguir la informació necessaria per realitzar l'exploració.

### Afegir informació

S'afegit la informació dels catàlegs.

```{r}
 
dt.ind.der.dm <- merge(dt.ind.der.dm,
                       dt.cat_centres.raw,
                       by.x = "br",
                       by.y = "ics_codi",
                       all.x = TRUE)
# Nota: Se pierden 5 EAP 
#a <- dt.ind.der.dm[, .N, br]
#b <- dt.ind.der.dm[, .N, up_origen_desc]
#c <- dt.ind.der.dm[, .N, tip_eap]
```

S'afegit la informació de les distàncies.

```{r}
dt.ind.der.dm[, up_hosp := str_sub(br_desti, -5, -1)]
dt.ind.der.dm <- merge(dt.ind.der.dm, # N= 18.493.322
                       dt.distancies.raw,
                       by.x = c("scs_codi", "up_hosp"),
                       by.y = c("up_cap", "up_hosp"),
                       all.x = TRUE)
```

S'afegit la informació de korra: NO HO FEM, augmenta molt la RAM

```{r, eval = FALSE}
 
dt.ind.der.dm <- merge(dt.ind.der.dm,
                       dt.korra.flt.agr.up,
                       by.x = "br",
                       by.y = "br",
                       all.x = TRUE)
gc.var <- gc()
```

***

### Creació noves variables

Creació de les noves variables.

#### Flag indicador de Pediatria vs Adulto

```{r}
dt.ind.der.dm[,indicador_tipus_l := str_detect(indicador, "P")]
dt.ind.der.dm[indicador_tipus_l == TRUE, indicador_tipus := "Pediatria"]
dt.ind.der.dm[indicador_tipus_l != TRUE, indicador_tipus := "Adulto"]
dt.ind.der.dm[, .N, indicador_tipus]
dt.ind.der.dm[, .N, .(indicador_tipus, indicador)][order(indicador)]
```

#### Medea

```{r}
# dt.ind.der.dm[, medea_c := factor(medea_c,
#                                   levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"))]
# 
# dt.ind.der.dm[medea_c %in% c("0R", "1R", "2R"), rural := "Rural"]
# dt.ind.der.dm[medea_c %in% c("1U", "2U", "3U", "4U"), rural := "Urbà"]
# dt.ind.der.dm[, rural := factor(rural,
#                                 levels = c("Rural", "Urbà"),
#                                 labels = c("Rural", "Urbà"))]
# 
# dt.ind.der.dm[, medea_c2 := medea_c]
# dt.ind.der.dm[medea_c %in% c("0R", "1R", "2R"), medea_c2 := "Rural"]
# dt.ind.der.dm[, medea_c_2 := factor(medea_c2,
#                                     levels = c("Rural", "1U", "2U", "3U", "4U"),
#                                     labels = c("Rural", "1U", "2U", "3U", "4U"))]
```

```{r orden columnas}
#|

```

```{r data manager gc}
rm(list = c("dt.ind.der.raw", "dt.korra.raw"))
gc.var <- gc()
```

