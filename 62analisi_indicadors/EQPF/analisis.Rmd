---
title: "Factors ajust EQPF"
output:
  html_document:
    toc: true
    toc_depth: 2
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, results = "asis", message = FALSE, warning = FALSE)
```

```{r}
library(data.table)
library(ggplot2)
library(hrbrthemes)
library(pander)
library(kableExtra)
library(dplyr)
```

```{r}
source("0-ImportDB.R", encoding = "UTF-8")
indicadors <- unique(dades$indicador)
vars <- c("sum_edat_mean", "institucionalitzats_mean", "immigrants_renta_baixa_mean", "pensionistes_mean", "pcc_mean", "maca_mean", "atdom_mean", "dona_mean", "medea_mean", "gma_mean", "aquas", "medea_f")
```

# Primeres anàlisis

```{r}
l <- lapply(indicadors, function(ind){
  d0 <- dades[indicador == ind]
  d0[, sample := "Original"]
  outliers <- boxplot(d0$AGRESULT, plot = F)$out
  d <- d0[!AGRESULT %in% outliers]
  d[, sample := "Sense outliers"]
  d <- rbind(d0, d)
  g <- ggplot(d, aes(y = ..density..)) + 
  geom_histogram(aes(x = AGRESULT), alpha = 0.8, color = "white") +
  xlab("Resultats de l'indicador") + ylab("Nombre d'EAP") + 
  theme_ipsum(grid_col = "transparent", axis_title_size = 11) +
  theme(
    legend.position = "bottom" ,
    legend.text = element_text(colour = "grey35"),
    plot.title = element_text(hjust = 0.5, size = 18, colour = "grey35", margin=margin(0, 0, 10, 0)),
    plot.subtitle = element_text(hjust = 0.5, size = 16, colour = "grey35", margin=margin(0, 0, 20, 0)),
    plot.caption = element_text(size = 8, colour = "grey35"),
    strip.text.x = element_text(angle = 0, hjust = 0.5, colour = "grey35"),
    axis.title = element_text(colour = "grey35"),
    axis.text = element_text(colour = "grey35"),
    axis.line = element_line(colour = "grey75")) + 
    facet_grid(. ~ sample, scales = "free_x") + 
  labs(title = "Resultat dels EAP",
       subtitle = paste(ind),
       caption = "FONT: SISAP")
  print(g)
})
```

```{r}
t <- Reduce(function(...) merge(..., all=TRUE, by = "Factor d'ajust"),
lapply(indicadors, function(ind){
  d <- dades[indicador == ind]
  outliers <- boxplot(d$AGRESULT, plot = F)$out
  d <- d[!AGRESULT %in% outliers]
  do.call("rbind", lapply(vars, function(x){
    R2 <- summary(lm(AGRESULT ~ get(x), data = d))$r.squared*100
    t <- data.table(x, R2)
    names(t) <- c("Factor d'ajust", ind)
    t
}))
}))
```

```{r}
t %>%
  mutate(IF246P = ifelse(IF246P >= 10,
                  cell_spec(format(IF246P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF246P, decimal.mark = ",", digits = 2))),
         IF247P = ifelse(IF247P >= 10,
                  cell_spec(format(IF247P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF247P, decimal.mark = ",", digits = 2))),
         IF248P = ifelse(IF248P >= 10,
                  cell_spec(format(IF248P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF248P, decimal.mark = ",", digits = 2))),
         IF249P = ifelse(IF249P >= 10,
                  cell_spec(format(IF249P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF249P, decimal.mark = ",", digits = 2))),
         IF250P = ifelse(IF250P >= 10,
                  cell_spec(format(IF250P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF250P, decimal.mark = ",", digits = 2))),
         IF255P = ifelse(IF255P >= 10,
                  cell_spec(format(IF255P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF255P, decimal.mark = ",", digits = 2))),
         IF257P = ifelse(IF257P >= 10,
                  cell_spec(format(IF257P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF257P, decimal.mark = ",", digits = 2))),
         IF259P = ifelse(IF259P >= 10,
                  cell_spec(format(IF259P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF259P, decimal.mark = ",", digits = 2))),
         IF261P = ifelse(IF261P >= 10,
                  cell_spec(format(IF261P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF261P, decimal.mark = ",", digits = 2))),
         IF263P = ifelse(IF263P >= 10,
                  cell_spec(format(IF263P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF263P, decimal.mark = ",", digits = 2))),
         IF265P = ifelse(IF265P >= 10,
                  cell_spec(format(IF265P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF265P, decimal.mark = ",", digits = 2))),
         IF267P = ifelse(IF267P >= 10,
                  cell_spec(format(IF267P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF267P, decimal.mark = ",", digits = 2))),
         IF269P = ifelse(IF269P >= 10,
                  cell_spec(format(IF269P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF269P, decimal.mark = ",", digits = 2))),
         IF271P = ifelse(IF271P >= 10,
                  cell_spec(format(IF271P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF271P, decimal.mark = ",", digits = 2))),
         IF274P = ifelse(IF274P >= 10,
                  cell_spec(format(IF274P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF274P, decimal.mark = ",", digits = 2))),
         IF276P = ifelse(IF276P >= 10,
                  cell_spec(format(IF276P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF276P, decimal.mark = ",", digits = 2))),
         IF278P = ifelse(IF278P >= 10,
                  cell_spec(format(IF278P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF278P, decimal.mark = ",", digits = 2))),
         IF280P = ifelse(IF280P >= 10,
                  cell_spec(format(IF280P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF280P, decimal.mark = ",", digits = 2))),
         IF282P = ifelse(IF282P >= 10,
                  cell_spec(format(IF282P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF282P, decimal.mark = ",", digits = 2))),
         IF284P = ifelse(IF284P >= 10,
                  cell_spec(format(IF284P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF284P, decimal.mark = ",", digits = 2))),
         IF2862P = ifelse(IF2862P >= 10,
                  cell_spec(format(IF2862P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF2862P, decimal.mark = ",", digits = 2))),
         IF286P = ifelse(IF286P >= 10,
                  cell_spec(format(IF286P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF286P, decimal.mark = ",", digits = 2))),
         IF288P = ifelse(IF288P >= 10,
                  cell_spec(format(IF288P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF288P, decimal.mark = ",", digits = 2))),
         IF290P = ifelse(IF290P >= 10,
                  cell_spec(format(IF290P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF290P, decimal.mark = ",", digits = 2))),
         IF292P = ifelse(IF292P >= 10,
                  cell_spec(format(IF292P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF292P, decimal.mark = ",", digits = 2))),
         IF294P = ifelse(IF294P >= 10,
                  cell_spec(format(IF294P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF294P, decimal.mark = ",", digits = 2))),
         IF296P = ifelse(IF296P >= 10,
                  cell_spec(format(IF296P, decimal.mark = ",", digits = 2), background = "#68B684"),
                  cell_spec(format(IF296P, decimal.mark = ",", digits = 2)))) %>%

  kable(digits = 2, row.names = F, escape = F, caption = "R2 dels models de regressió ajustats amb cadascun dels factors", 
        align = c("l", rep("c", ncol(t)-1)), format.args = list(decimal.mark = ',', big.mark = "."))%>%
  kable_styling(bootstrap_options = c("hover", "condensed", "responsive", "stripped"), position = "float_left") %>%
  row_spec(0, bold = T, align = "c", background = "#0061a9", color = "white") %>%
  column_spec(1, bold = F, background = "#E7F0F7") 
```

# Models d'ajust

```{r}
indicadors <- c("IF261P", "IF265P", "IF269P", "IF271P", "IF276P", "IF284P", "IF288P", "IF294P")
vars <- c("sum_edat_mean", "immigrants_renta_baixa_mean", "pensionistes_mean", "dona_mean", "gma_mean", "medea_f")
```

```{r include=FALSE}
t_model <- Reduce(function(...) merge(..., by = "Factors d'ajust", all = T),
lapply(indicadors, function (ind){
  d <- dades[indicador == ind]
  outliers <- boxplot(d$AGRESULT, plot = F)$out
  d <- d[!AGRESULT %in% outliers, .SD, .SDcols = c("AGRESULT", vars)]
  model <- lm(AGRESULT ~ ., data = d)
  selected_model <- step(model, direction = "backward")
  coeficients <- data.table(cbind(selected_model$coefficients, confint(selected_model)))
  coeficients[, tot_junt := linebreak(paste0(format(round(V1, 2), decimal.mark = ","), "<br>(", format(round(`2.5 %`, 2), decimal.mark = ","), "; ", format(round(`97.5 %`, 2), decimal.mark = ",", digits = 2), ")"))]
  coeficients[, `Factors d'ajust` := names(selected_model$coefficients)]
  t <- coeficients[, c("Factors d'ajust", "tot_junt")]
  names(t)[2] <- ind
  t
}))

names(t_model)[1] <- " "
```

```{r}
options(knitr.kable.NA = '--')
t_model[c(1, 12, 2, 3, 4, 5, 7, 6, 8: 11)] %>%
kable(digits = 2, row.names = F, escape = F, caption = "Coeficients del model ajustat pels factors d'ajust. Estimació puntual i interval de confiança del 95%", align = c("l", rep("c", ncol(t)-1)), format.args = list(decimal.mark = ',', big.mark = ".")) %>%
kable_styling(bootstrap_options = c("hover", "condensed", "responsive"), full_width = T) %>%
row_spec(0, bold = T, align = "c", background = "#0061a9", color = "white") %>%
column_spec(1, bold = F, border_right = F, background = "#E7F0F7")
```



```{r}
indicadors <- c("IF261P", "IF265P", "IF269P", "IF271P", "IF276P", "IF284P", "IF288P", "IF294P")
vars <- c("sum_edat_mean", "immigrants_renta_baixa_mean", "pensionistes_mean", "dona_mean", "medea_f")
```

```{r include=FALSE}
t_model <- Reduce(function(...) merge(..., by = "Factors d'ajust", all = T),
lapply(indicadors, function (ind){
  d <- dades[indicador == ind]
  outliers <- boxplot(d$AGRESULT, plot = F)$out
  d <- d[!AGRESULT %in% outliers, .SD, .SDcols = c("AGRESULT", vars)]
  model <- lm(AGRESULT ~ ., data = d)
  selected_model <- step(model, direction = "backward")
  coeficients <- data.table(cbind(selected_model$coefficients, confint(selected_model)))
  coeficients[, tot_junt := linebreak(paste0(format(round(V1, 2), decimal.mark = ","), "<br>(", format(round(`2.5 %`, 2), decimal.mark = ","), "; ", format(round(`97.5 %`, 2), decimal.mark = ",", digits = 2), ")"))]
  coeficients[, `Factors d'ajust` := names(selected_model$coefficients)]
  t <- coeficients[, c("Factors d'ajust", "tot_junt")]
  names(t)[2] <- ind
  t
}))

names(t_model)[1] <- " "
```

```{r}
options(knitr.kable.NA = '--')
t_model[c(1, 11, 2, 3, 4, 6, 5, 7: 10)] %>%
kable(digits = 2, row.names = F, escape = F, caption = "Coeficients del model ajustat pels factors d'ajust. Estimació puntual i interval de confiança del 95%", align = c("l", rep("c", ncol(t)-1)), format.args = list(decimal.mark = ',', big.mark = ".")) %>%
kable_styling(bootstrap_options = c("hover", "condensed", "responsive"), full_width = T) %>%
row_spec(0, bold = T, align = "c", background = "#0061a9", color = "white") %>%
column_spec(1, bold = F, border_right = F, background = "#E7F0F7")
```

```{r}
indicadors <- c("IF261P", "IF265P", "IF269P", "IF271P", "IF276P", "IF284P", "IF288P", "IF294P")
vars <- c("sum_edat_mean", "pensionistes_mean", "dona_mean", "medea_f")
```

```{r include=FALSE}
t_model <- Reduce(function(...) merge(..., by = "Factors d'ajust", all = T),
lapply(indicadors, function (ind){
  d <- dades[indicador == ind]
  outliers <- boxplot(d$AGRESULT, plot = F)$out
  d <- d[!AGRESULT %in% outliers, .SD, .SDcols = c("AGRESULT", vars)]
  model <- lm(AGRESULT ~ ., data = d)
  selected_model <- step(model, direction = "backward")
  coeficients <- data.table(cbind(selected_model$coefficients, confint(selected_model)))
  coeficients[, tot_junt := linebreak(paste0(format(round(V1, 2), decimal.mark = ","), "<br>(", format(round(`2.5 %`, 2), decimal.mark = ","), "; ", format(round(`97.5 %`, 2), decimal.mark = ",", digits = 2), ")"))]
  coeficients[, `Factors d'ajust` := names(selected_model$coefficients)]
  t <- coeficients[, c("Factors d'ajust", "tot_junt")]
  names(t)[2] <- ind
  t
}))

names(t_model)[1] <- " "
```

```{r}
options(knitr.kable.NA = '--')
t_model[c(1, 10, 2, 3, 5, 4, 6: 9)] %>%
  kable(digits = 2, row.names = F, escape = F, caption = "Coeficients del model ajustat pels factors d'ajust. Estimació puntual i interval de confiança del 95%", align = c("l", rep("c", ncol(t)-1)), format.args = list(decimal.mark = ',', big.mark = ".")) %>%
  kable_styling(bootstrap_options = c("hover", "condensed", "responsive"), full_width = T) %>%
  row_spec(0, bold = T, align = "c", background = "#0061a9", color = "white") %>% 
  column_spec(1, bold = F, border_right = F, background = "#E7F0F7") 
```