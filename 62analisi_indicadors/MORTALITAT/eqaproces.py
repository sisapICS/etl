# coding: latin1

"""
.
"""

import collections as c
import itertools as i

import sisapUtils as u

processos = {"MCV": "EQA3007A",
             "DM2": "EQA3111A",
             "FA": "EQA3214A"}
             

db = "eqa_ind"


class Proces(object):
    """."""

    def __init__(self):
        """."""
        self.get_poblacio()
        self.get_centres()
        self.get_gma()
        self.get_master()
        self.export_data()

    def get_poblacio(self):
        """."""
        self.pob = {}
        sql = "select id_cip_sec, (year(data_ext)-year(usua_data_naixement))-(date_format(data_ext,'%m%d')<date_format(usua_data_naixement,'%m%d')), usua_sexe from assignada, nodrizas.dextraccio"
        for id, edat, sexe in u.getAll(sql, "import"):
            self.pob[id] = {'edat': u.ageConverter(edat, 5), 'sexe': u.sexConverter(sexe)}
    
    def get_centres(self):
        """."""
        self.centres = {}
        sql = "select scs_codi, ics_codi from cat_centres where ep='0208'"
        for up, br in u.getAll(sql, "nodrizas"):
            self.centres[up] = br
    
    def get_gma(self):
        """."""
        self.gma = {}
        sql = "select br, edat, sexe, sum(npacients), sum(complex) from exp_khalix_gma group by br, edat, sexe"
        for br, edat, sexe, np, compl in u.getAll(sql, "altres"):
            self.gma[(br, edat, sexe)] = {'np': np, 'compl': compl}
    
    def get_master(self):
        """."""
        self.dades_up = c.Counter()
        sql = "select * from mst_indicadors_pacient_{} where ind_codi = '{}'"
        for proces in processos:
            indicador = processos[proces]
            for (id, ind, dim, up, uba, upinf, ubainf, ates, instit,
                 den, num, excl) in u.getAll(sql.format(proces, indicador), db):
                if ates and not instit and not excl:
                    if up in self.centres:
                        br = self.centres[up]
                        edat, sexe = self.pob[id]['edat'],self.pob[id]['sexe']
                        self.dades_up[(proces, ind, br, edat, sexe, "DENZ")] += den
                        self.dades_up[(proces, ind, br, edat, sexe, "NUMZ")] += num
                    
    def export_data(self):
        """."""
        dades = []
        for (proces, ind, up, edat, sexe, tip), d in self.dades_up.items():
            if tip == 'DENZ':
                try:
                    np = self.gma[(up, edat, sexe)]['np']
                    compl = self.gma[(up, edat, sexe)]['compl']
                    xgma = float(compl/np)                  
                except KeyError:
                    print up, edat, sexe
                    continue
                xgma = str(xgma).replace('.',',')
                num = self.dades_up[(proces, ind, up, edat, sexe, 'NUMZ')]
                dades.append([proces, ind, up, edat, sexe, xgma, int(num), int(d)])
        u.writeCSV(u.tempFolder + 'indicadors_mortalitat.txt', dades)


if __name__ == "__main__":
    Proces()
