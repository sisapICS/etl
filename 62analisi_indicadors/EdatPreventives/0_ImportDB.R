# --- libraries ----
library(RMySQL)
library(data.table)


# ----- MYSQL ------
# drv <- dbDriver("MySQL")
# con <- dbConnect(drv, user = "sisap", password="",host = "10.80.217.68", port=3307, dbname="test")
# nfetch <- -1
# dbListTables(con)
# dbListFields(con, "preventives_eap")
# query <- dbSendQuery(con,
#                      statement = "select *
#                      from preventives_eap")
# eap <- data.table(fetch(query, n = nfetch))
# 
# query <- dbSendQuery(con,
#                      statement = "select *
#                      from preventives_eap2")
# eap2 <- data.table(fetch(query, n = nfetch))
# query <- dbSendQuery(con,
#                      statement = "select *
#                      from preventives_pac")
# pacients <- data.table(fetch(query, n = nfetch))
# save(pacients, file = "pacients.RData")
# dbDisconnect(con)
drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = "sisap", password="",host = "10.80.217.68", port=3307, dbname="eqa_ind")
nfetch <- -1
query <- dbSendQuery(con,
                     statement = "select * from exp_ecap_cataleg")
nom <- data.table(fetch(query, n = nfetch))[, c("indicador", "literal", "wiki")]
dbDisconnect(con)
nom[, tot := paste0(indicador, " - ", literal)]

# -------- EAPS --------------

# Formatejar 
# # Eliminem NO ICS
# 
# load("Dades/cataleg_centres.RData")
# eap <- merge(eap, cat_centres[, c("ics_codi", "amb_codi", "amb_desc")], by.x = "BR", by.y = "ics_codi", all.x = T)
# eap <- eap[!is.na(amb_desc) & amb_codi != "00"]
# 
# # Eliminem població pediàtrica
# 
# khalix <- fread("Dades/catalegPOBTIP_def2018.csv", sep = ";")
# eap <- merge(eap, khalix, by.x = "BR", by.y = "br", all.x= T)
# eap <- eap[tipus_poblacio != "NENS"]
# 
# str(eap)
# 
# eap[, ":=" (ATASS = (POBASSAT/POBASS)*100,
#             medea_f = factor(EAPTIPUS2, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U")),
#             rural = factor(EAPTIPUS2, levels = c("0R", "1R", "2R", "1U", "2U", "3U", "4U"), 
#                            labels = c("Rural", "Rural", "Rural", "Urbà", "Urbà", "Urbà", "Urbà")),
#             prof_POBASS = POBASS/GRPEQAPOBA,
#             # EAPTIPUS = NULL,
#             EAPTIPUS2 = NULL
#             )]
# 
# # names(eap) <- tolower(names(eap))
# 
# eap2 <- merge(eap2, nom[, c("indicador", "literal", "tot")], by.x = "INDICADOR", by.y = "indicador", all.x = T)
# 
# save(eap, file = "Dades/eap.RData")
# save(eap2, file = "Dades/eap2.RData")

# -------- PACIENTS --------------

# Formatejar

pacients[, br := NULL]
pacients[, amb_desc := NULL]
pacients[, sap_desc := NULL]
pacients[, up_desc := NULL]

pacients <- merge(pacients, cat_centres[, c("scs_codi", "ics_codi", "ics_desc", "amb_codi", "amb_desc")], by.x = "up", by.y = "scs_codi", all.x = T)
pacients <- pacients[amb_desc != "ÀMBIT ALIÉ A L'ICS"]
pacients[, sexe_f := factor(sexe, levels = c("H", "D"), labels = c("Home", "Dona"))]

# Els indicadors s'han d'ajustar per:
# EQA0301: 15 a 80 anys
# EQA0302: 15 a 80 anys
# EQA0303: 35 a 64 anys
# EQA0304: 15 a 80 anys
# EQA0305: 15 a 80 anys
# EQA0306: 15 a 69 anys

pacients[, EQA0301A_f := ifelse(edat <= 15 | edat >= 80, NA, EQA0301A)]
pacients[, EQA0302A_f := ifelse(edat <= 15 | edat >= 80, NA, EQA0302A)]
pacients[, EQA0303A_f := ifelse(edat <= 35 | edat >= 64, NA, EQA0303A)]
pacients[, EQA0304A_f := ifelse(edat <= 15 | edat >= 80, NA, EQA0304A)]
pacients[, EQA0305A_f := ifelse(edat <= 15 | edat >= 69, NA, EQA0305A)]

# Verificar els resultats dels indicadors

num <- pacients[, lapply(.SD, function(x) sum(x, na.rm = T)), .SDcols = c("EQA0301A", "EQA0302A", "EQA0303A", "EQA0304A", "EQA0305A")]
den <- pacients[edat>14, lapply(.SD, function(x) sum(!is.na(x))), .SDcols = c("EQA0301A", "EQA0302A", "EQA0303A", "EQA0304A", "EQA0305A")]
t <- rbind(num, den)
tt <- eap2[, lapply(.SD, function(x) sum(x)), .SDcols = c("NUM", "DEN"), by = "INDICADOR"]

t[1, ]/ t[2, ]*100
tt[, NUM/DEN*100]

pacients <- merge(pacients, nom[, c("indicador", "literal", "tot")], by. x)
save(pacients, file = "pacients.RData")
