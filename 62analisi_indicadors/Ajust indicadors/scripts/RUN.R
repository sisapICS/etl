library('rmarkdown')

setwd("C:/Users/nmora/repositori/etl/62analisi_indicadors/Ajust indicadors/scripts/")
Sys.setenv(RSTUDIO_PANDOC= "C:/Program Files/RStudio/bin/pandoc")

render("it_indicadors.Rmd", 
       output_format = "html_document",
       output_file = paste('it_indicadors_korra_actius_A1912',
                           format(Sys.Date(),'%Y%m%d'),
                           format(Sys.time(),'%H%M'), sep="_"),
       output_dir = "../resultats/")