---
title: "Anàlisi exploratòria dels factors d'ajust de l’indicador de la taxa de peticions al laboratori (LABASS01)"
output: 
  html_document:
    toc: true
    toc_float: true
---

```{r setup, include=FALSE}
# -------------- Packages ---------------
library(data.table)
library(ggplot2)
library(pander)
library(caret)

# ----------- Dades ----------------
load("../dades.RData")
str(dades)
dades <- dades[DESC_AMBIT != "Entitats Proveïdores - Xarxa Concertada", c(1, 2, 5, 6, 7, 11:18)]
summary(dades)
```

# Univariant

```{r uni, echo = F, fig.align='center', fig.height=6, fig.width=10, results="asis"}
g <- ggplot(dades, aes(resposta)) + geom_histogram(fill = "#006687") + 
  ggtitle("Histograma de la taxa de peticions al laboratori\n per població atesa") + xlab("Taxa de peticions") + ylab("Freqüència") + xlim(0, 7.5) +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5)) 
g 
pander(summary(dades$resposta), caption = "Summary de la taxa de peticions al laboratori", digits = 2)
```

# Bivariant

## Edat
```{r bi_edat, echo = F, fig.align='center', fig.height=6, fig.width=10, results="asis"}
g <- ggplot(dades, aes(edat_f, resposta)) +  geom_dotplot(binaxis = "y", stackdir = "center", binwidth = 0.025, fill = "#006687", color = "#006687") +  
  ggtitle("Taxa de peticions al laboratori per grup d'edat i sexe") + xlab("Edat") + ylab("Taxa de peticions") + ylim(0, 7.5) +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5), axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 0.5)) 
g 
g + facet_grid(.~sexe_f)
```

Al gràfics anteriors s'aprecia que existeix correlació entre edat i taxa de peticions al laboratori i que aquesta correlació varia per sexe, on en les dones, s'observa un salt en la taxa en el grup de 15 a 19. 

```{r bi_edat3, echo = F, fig.align='center', fig.height=6, fig.width=10, results="asis"}
g <- ggplot(dades[resposta<7.5], aes(edat_f, eap_tipus_f)) +  geom_tile(aes(fill = resposta)) +  
  ggtitle("Taxa de peticions al laboratori per grup d'edat i tipus d'EAP") + xlab("Edat") + ylab("Tipus EAP") +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5), axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 0.5)) + scale_fill_gradientn("Taxa", 
colours = terrain.colors(4))
g 
g + facet_grid(.~sexe_f)
```

S'observa, tant en dones com en homes, una correlació gradual entre edat i taxa de peticions. No obstant, aquesta correlació varia segons tipus de EAP. 2R i 3U tenen taxes més baixes. 

### Test ANOVA

El test d'anàlisi de la variància per a provar si la correlació entre edat i taxa de peticions és significativa dona un p-valor inferior a 0.05

```{r edat_test, echo = F}
a <- aov(resposta ~ edat_f, data = dades)
pander(a, missing = "")
```

## Sexe
```{r bi_sexe, echo = F, fig.align='center', fig.height=6, fig.width=10, results="asis"}
g <- ggplot(dades, aes(sexe_f, resposta)) + geom_violin(fill = "#006687") + 
  ggtitle("Taxa de peticions de laboratori per sexe") + xlab("Sexe") + ylab("Taxa num laboratori") +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5)) + ylim(0, 7.5)
g
```

També s'observen taxes diferenciades per sexe. Totes dues distribucions son bimodals, tot i que en punts diferents. 

### Test ANOVA

El test d'anàlisi de la variància per a provar si la correlació entre sexe i taxa de peticions és significativa dona un p-valor inferior a 0.05

```{r sexe_test, echo = F}
a <- aov(resposta ~ sexe_f, data = dades)
pander(a, missing = "")
```

## Tipus d' EAP
```{r bi_tipus_eap, echo = F, fig.align='center', fig.height=6, fig.width=10, results="asis"}
g <- ggplot(dades, aes(eap_tipus_f, resposta)) + geom_violin(fill = "#006687") + 
  ggtitle("Taxa de peticions de laboratori per tipus EAP") + xlab("Tipus EAP") + ylab("Taxa num laboratori") +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5)) + ylim(0, 7.5)
g
g + facet_grid(.~sexe_f)
```

S'observa que les distribucions de la taxa varien entre tipus d'EAP

### Test ANOVA

El test d'anàlisi de la variància per a provar si la correlació entre tipus d'EAP i taxa de peticions és significativa dona un p-valor inferior a 0.05

```{r eap_test, echo = F}
a <- aov(resposta ~ eap_tipus_f, data = dades)
pander(a, missing = "")
```

## Índex socioeconòmic (MEDEA i AQuAS)

```{r bi_socioecon, echo = F, fig.hold='hold', out.width = "50%", results= "asis", fig.height=6, fig.width=10}
g <- ggplot(dades[!is.na(medea)], aes(medea, resposta)) + geom_point() + geom_smooth(method = lm) +
  ggtitle("Correlació entre la taxa de peticions de laboratori\n i l'índex MEDEA") + xlab("MEDEA") + ylab("Taxa num laboratori") +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5)) + ylim(0, 7.5)
g
g <- ggplot(dades[!is.na(aquas)], aes(aquas, resposta)) + geom_point(position = "jitter") + geom_smooth(method = lm) +
  ggtitle("Correlació entre la taxa de peticions de laboratori\n i l'índex socoeconòmic de l'AQuAS") + xlab("Índex AQuAS") + ylab("Taxa num laboratori") +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5)) + ylim(0, 7.5)
g
```

```{r socioecon_corr, echo = F, results= "asis", fig.height=6, fig.width=10}
corr1 <- cor.test(dades$resposta, dades$medea, method = "pearson", conf.level = 0.95)
corr2 <- cor.test(dades$resposta, dades$aquas, method = "pearson", conf.level = 0.95)
t <- cbind("Coeficient de Pearson" = c(corr1$estimate, corr2$estimate), 
           "IC 95% - Límit inferior" = c(corr1$conf.int[1], corr2$conf.int[1]),
           "IC 95% - Límit superior" = c(corr1$conf.int[2], corr2$conf.int[2]),
           "P-valor" = c(corr1$p.value, corr2$p.value))
rownames(t) <- c("Índex MEDEA", "Índex AQuAS")
knitr::kable(t, digits = 3, row.names = T)
```

Els gràfics de correlació entre els índex socioeconòmics i la taxa de peticions, no mostren una correlació entre les dues variables. No obstant, el p-valor del test de correlació de pearson si que surt significatiu per la variable AQuAS.

```{r bi_socioecon_mes, echo = F, results= "asis", fig.height=6, fig.width=10}
g <- ggplot(dades[!is.na(medea)], aes(medea, resposta)) + geom_point() + geom_smooth(method = lm) +
  ggtitle("Correlació entre la taxa de peticions de laboratori\n i l'índex MEDEA") + xlab("MEDEA") + ylab("Taxa num laboratori") +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5)) + ylim(0, 7.5)
g + facet_grid(sexe_f~edat_f)
g <- ggplot(dades[!is.na(aquas)], aes(aquas, resposta)) + geom_point(position = "jitter") + geom_smooth(method = lm) +
  ggtitle("Correlació entre la taxa de peticions de laboratori\n i l'índex socoeconòmic de l'AQuAS") + xlab("Índex AQuAS") + ylab("Taxa num laboratori") +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5)) + ylim(0, 7.5)
g + facet_grid(sexe_f~edat_f)
```

Igual que el que passa amb l'edat, la correlació entre l'índex socioeconòmic i la taxa de peticions varia en funció del grup d'edat i el sexe.

## GMA

```{r bi_gma, echo = F, results= "asis", fig.height=6, fig.width=10}
g <- ggplot(dades, aes(gma, resposta)) + geom_point() + geom_smooth(method = lm) +
  ggtitle("Correlació entre la taxa de peticions de laboratori\n i l'índex GMA") + xlab("GMA") + ylab("Taxa num laboratori")+ ylim(0, 7.5) +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5)) 
g
```

```{r gma_corr, echo = F, results= "asis", fig.height=6, fig.width=10}
corr1 <- cor.test(dades$resposta, dades$gma, method = "pearson", conf.level = 0.95)
t <- cbind("Coeficient de Pearson" = c(corr1$estimate), 
           "IC 95% - Límit inferior" = c(corr1$conf.int[1]),
           "IC 95% - Límit superior" = c(corr1$conf.int[2]),
           "P-valor" = c(corr1$p.value))
rownames(t) <- c("Índex GMA")
knitr::kable(t, digits = 3, row.names = T)
```

Tant el gràfic com el test de coeficient de Pearson, mostren una correlació significativa entre GMA i taxa de peticions.

```{r bi_gma_mes, echo = F, results= "asis", fig.height=6, fig.width=10}
g <- ggplot(dades, aes(gma, resposta)) + geom_point() + geom_smooth(method = lm) +
  ggtitle("Correlació entre la taxa de peticions de laboratori\n i l'índex GMA") + xlab("GMA") + ylab("Taxa num laboratori")+ ylim(0, 7.5) +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5)) 
g + facet_grid(sexe_f~edat_f)
```

També el GMA mostra diferents correlacions entre la taxa de peticions per sexe i grup d'edat.

## Conclusió bivariant

* La taxa de peticions està estadísticament correlacionada amb l'edat, el sexe, el tipus d'EAP, l'índex socioeconòmic de l'AQuAS i el GMA
* La correlació entre aquestes variables i la taxa de peticions és diferent segons grup d'edat i sexe.

# GLM 

<!-- Com que tenim dos variables relacionades amb l'índex socioeconòmic, primer mirarem quina de les dues explica millor la taxa de peticions. -->

<!-- ```{r glm_compare_socioecon, echo = TRUE} -->
<!-- dades <- dades[den != 0] -->
<!-- smp_size <- floor(0.75 * nrow(dades)) -->
<!-- train_ind <- sample(seq_len(nrow(dades)), size = smp_size) -->
<!-- train <- dades[train_ind, ] -->
<!-- test <- dades[-train_ind, ] -->

<!-- glm1 <- glm(num ~ medea + offset(log(den)), data = dades, family = "poisson") -->
<!-- summary(glm1) -->
<!-- glm2 <- glm(num ~ aquas + offset(log(den)), data = dades, family = "poisson") -->
<!-- summary(glm2) -->

<!-- aic1 <- glm1$aic -->
<!-- aic2 <- glm2$aic -->
<!-- t <- cbind("AIC amb MEDEA" = aic1, -->
<!--            "AIC amb AQuAS" = aic2) -->

<!-- pander(t, caption = "Comparació d'AIC per variables socieconòmiques") -->
<!-- ``` -->

```{r glm_compare, echo = TRUE}
# dades <- dades[den != 0]
smp_size <- floor(0.75 * nrow(dades))
train_ind <- sample(seq_len(nrow(dades)), size = smp_size)
train <- dades[train_ind, ]
test <- dades[-train_ind, ]

glm0 <- glm(num ~ 1 + offset(log(den)), data = dades, family = "poisson")
pred <- predict(glm0, newdata = test, type = "response")
test$pred0 <- pred
test$resid0 <- test$num-test$pred0

glm1 <- glm(num ~ edat_f + offset(log(den)), data = dades, family = "poisson")
pred <- predict(glm1, newdata = test, type = "response")
test$pred1 <- pred
test$resid1 <- test$num-test$pred1

glm2 <- glm(num ~ edat_f + sexe_f + offset(log(den)), data = dades, family = "poisson")
pred <- predict(glm2, newdata = test, type = "response")
test$pred2 <- pred
test$resid2 <- test$num-test$pred2

glm3 <- glm(num ~ edat_f + sexe_f + eap_tipus_f + offset(log(den)), data = dades, family = "poisson")
pred <- predict(glm3, newdata = test, type = "response")
test$pred3 <- pred
test$resid3 <- test$num-test$pred3

glm4 <- glm(num ~ edat_f + sexe_f + eap_tipus_f + gma + offset(log(den)), data = dades, family = "poisson")
pred <- predict(glm4, newdata = test, type = "response")
test$pred4 <- pred
test$resid4 <- test$num-test$pred4

glm5 <- glm(num ~ edat_f + sexe_f + eap_tipus_f + gma + aquas + offset(log(den)), data = dades, family = "poisson")
pred <- predict(glm5, newdata = test, type = "response")
test$pred5 <- pred
test$resid5 <- test$num-test$pred5

glm6 <- glm(num ~ edat_f*sexe_f*eap_tipus_f + offset(log(den)), data = dades, family = "poisson")
pred <- predict(glm6, newdata = test, type = "response")
test$pred6 <- pred
test$resid6 <- test$num-test$pred6

glm7 <- glm(num ~ edat_f*sexe_f*eap_tipus_f + gma*edat_f*sexe_f + offset(log(den)), data = dades, family = "poisson")
pred <- predict(glm7, newdata = test, type = "response")
test$pred7 <- pred
test$resid7 <- test$num-test$pred7

glm8 <- glm(num ~ edat_f*sexe_f*eap_tipus_f + aquas*edat_f*sexe_f + offset(log(den)), data = dades, family = "poisson")
pred <- predict(glm8, newdata = test, type = "response")
test$pred8 <- pred
test$resid8 <- test$num-test$pred8
```


```{r glm_plot1, echo = F, fig.height=8, fig.width=8,results= "asis"}
dg <-melt(test, id.vars = c("CENTROS", "num"), measure.vars = c("pred0", "pred1", "pred2", "pred3", "pred4", "pred5", "pred6", "pred7", "pred8"), variable.name = "pred", value.name = "value")
levels(dg$pred) <- c("Model Nul", "Model amb Edat", "Model amb Edat i Sexe", "Model amb Edat, Sexe i Tipus EAP", "Model amb Edat, Sexe,\n Tipus EAP i GMA", "Model amb Edat, Sexe,\n Tipus EAP, GMA i AQuAS", "Model amb Edat*Sexe*TipusEAP", "Model amb Edat*Sexe*TipusEAP\n i GMA*Edat*Sexe", "Model amb Edat*Sexe*TipusEAP\n i AQuAS*Edat*Sexe")
g <- ggplot(dg, aes(value, num)) + geom_point() + geom_smooth(method = lm) + facet_wrap(~pred) +
  ggtitle("Observat vs Predit. Comparació de models") + xlab("Predit") + ylab("Observat") +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5)) 
g
```

```{r glm_r, echo = F,results= "asis"}
r_squard0 <- cor(test$num, test$pred0)^2
r_squard1 <- cor(test$num, test$pred1)^2
r_squard2 <- cor(test$num, test$pred2)^2
r_squard3 <- cor(test$num, test$pred3)^2
r_squard4 <- cor(test$num, test$pred4)^2
r_squard5 <- cor(test$num, test$pred5, use = "pairwise.complete.obs")^2
r_squard6 <- cor(test$num, test$pred6)^2
r_squard7 <- cor(test$num, test$pred7)^2
r_squard8 <- cor(test$num, test$pred8, use = "pairwise.complete.obs")^2
t <- rbind("Model Nul" = r_squard0,
           "Model amb Edat" = r_squard1,
           "Model amb Edat i Sexe" = r_squard2,
           "Model amb Edat, Sexe i tipus EAP" = r_squard3,
           "Model amb Edat, Sexe, tipus EAP i GMA" = r_squard4,
           "Model amb Edat, Sexe, tipus EAP, GMA i AQuAS" = r_squard5,
           "Model amb Edat*Sexe*tipus EAP" = r_squard6,
           "Model amb Edat*Sexe*TipusEAP i GMA*Edat*Sexe" = r_squard7,
           "Model amb Edat*Sexe*TipusEAP i AQuAS*Edat*Sexe" = r_squard8)
colnames(t) <- "R quadrat"
 
knitr::kable(t, caption = "R quadrats dels models estimats", digits = 3)
```

```{r glm_plot2, echo = F, fig.height=8, fig.width=8,results= "asis"}
dg <-melt(test, id.vars = c("CENTROS", "num"), measure.vars = c("resid0", "resid1", "resid2", "resid3", "resid4", "resid5", "resid6", "resid7", "resid8"), variable.name = "resid", value.name = "value")
levels(dg$resid) <- c("Model Nul", "Model amb Edat", "Model amb Edat i Sexe", "Model amb Edat, Sexe i Tipus EAP", "Model amb Edat, Sexe,\n Tipus EAP i GMA", "Model amb Edat, Sexe,\n Tipus EAP, GMA i AQuAS", "Model amb Edat*Sexe*TipusEAP", "Model amb Edat*Sexe*TipusEAP\n i GMA*Edat*Sexe", "Model amb Edat*Sexe*TipusEAP\n i AQuAS*Edat*Sexe")
g <- ggplot(dg, aes(value, num)) + geom_point() + facet_wrap(~resid) +
  ggtitle("Observat vs Residus Comparació de models") + xlab("residus") + ylab("Observat") +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5)) 
g + geom_vline(xintercept = 0, color = "blue", size = 1)
```

```{r glm_plot3, echo = F, fig.height=8, fig.width=8,results= "asis"}
g <- ggplot(dg, aes(value)) + geom_histogram() + facet_wrap(~resid) +
  ggtitle("Residus. Comparació de models") + xlab("residus") + ylab("Observat") +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5))
g 
```


```{r glm_plot4, echo = F, fig.height=8, fig.width=8,results= "asis"}
g <- ggplot(dg, aes(value)) + geom_density(aes(color = resid)) +
  ggtitle("Residus. Comparació de models") + xlab("residus") + ylab("Observat") +
  theme_bw() + theme(plot.title = element_text(hjust = 0.5)) 
g 
```

