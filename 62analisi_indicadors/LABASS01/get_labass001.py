# coding: latin1

"""
.
"""

import collections as c
import itertools as i

import sisapUtils as u

indicador = "LABASS01"      

db = "altres"


class Proces(object):
    """."""

    def __init__(self):
        """."""
        self.dades = []
        self.get_centres()
        self.get_gma()
        self.get_master()
        self.export_data()


    def get_centres(self):
        """."""
        self.centres = {}
        sql = "select scs_codi, ics_codi, ics_desc from cat_centres where ep='0208'"
        for up, br, desc in u.getAll(sql, "nodrizas"):
            self.centres[up] = {'br': br, 'desc': desc}
    
    def get_gma(self):
        """."""
        self.gma = {}
        sql = "select br, edat, sexe, sum(npacients), sum(complex) from exp_khalix_gma group by br, edat, sexe"
        for br, edat, sexe, np, compl in u.getAll(sql, "altres"):
            self.gma[(br, edat, sexe)] = {'np': np, 'compl': compl}
    
    def get_master(self):
        """."""
        sql = "select up, edat, sexe, numerador, denominador from exp_khalix_labass01_up where indicador = '{}'".format(indicador)
        for (up, edat, sexe, num, den) in u.getAll(sql, db):
            if up in self.centres:
                br = self.centres[up]['br']
                desc = self.centres[up]['desc']
                try:
                    np = self.gma[(br, edat, sexe)]['np']
                    compl = self.gma[(br, edat, sexe)]['compl']
                    xgma = float(compl/np)                  
                except KeyError:
                    print up, edat, sexe
                    continue
                xgma = str(xgma).replace('.',',')    
                self.dades.append([br, desc, edat, sexe, int(num), int(den), xgma])
                                           
    def export_data(self):
        """."""
        u.writeCSV(u.tempFolder + 'dades_LABASS01.txt', self.dades)


if __name__ == "__main__":
    Proces()
