***
***

# Import

## Dades

S'import la informació descarregada de LV dels indicadors de Comunitaria dels anys 2020-2022.

```{r}

dt.ind.com.2020.raw <- fread("D:/SISAP/sisap/62analisi_indicadors/Comunitaria/data/comunitaria_2020.txt",
                         sep = "{")
dt.ind.com.2021.raw <- fread("D:/SISAP/sisap/62analisi_indicadors/Comunitaria/data/comunitaria_2021.txt",
                         sep = "{")
dt.ind.com.2022.raw <- fread("D:/SISAP/sisap/62analisi_indicadors/Comunitaria/data/comunitaria_2022.txt",
                         sep = "{")

dt.ind.com.raw <- rbind(dt.ind.com.2020.raw,
                        dt.ind.com.2021.raw,
                        dt.ind.com.2022.raw)

```

Informació importada:

- files n = `r nrow(dt.ind.com.raw)`
- columnes n = `r ncol(dt.ind.com.raw)`

```{r}

names(dt.ind.com.raw) <- tolower(names(dt.ind.com.raw))

# S'eliminen columnes que són constants:
dt.ind.com.raw[, v5 := NULL]
dt.ind.com.raw[, v6 := NULL]
dt.ind.com.raw[, v7 := NULL]

setnames(dt.ind.com.raw, c("indicador", "any", "ics_codi", "dimensio", "n"))
```

Quantitat de missing:

```{r}
df <- dt.ind.com.raw
df <- df %>% mutate(taula = "Comunitaria")
res <- compareGroups(taula ~ .
                     , df
                     , max.xlev = 100
                     , method = 4
                     , include.label = FALSE)
export2md(missingTable(res
                       , show.p.overall = FALSE
                       ),
          format = "html",
          caption = "Exploració número de missing")
```

Control de qüalitat Valors:

```{r}
f.cq.table2(dt.ind.com.raw)
```

***

## Catàlegs

### Catèleg de Centres

S'importa el catàleg de centres de nodrizas.

```{r cat_centres}

drv <- dbDriver("MySQL")
con <- dbConnect(drv, user = idsisap,
                 password = "", 
                 host = hostsisap,
                 port = portsisap,
                 dbname = "nodrizas")
query <- dbSendQuery(con,
                     statement = "select ep, scs_codi, ics_codi, ics_desc, tip_eap, medea, aquas from cat_centres")
dt.cat_centres.raw <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)

setnames(dt.cat_centres.raw, "medea", "medea_c")
```

Informació importada:
 
 - files n = `r nrow(dt.cat_centres.raw)`
 - columnes n = `r ncol(dt.cat_centres.raw)`
 - columnes: `r names(dt.cat_centres.raw)`


Quantitat de missing:

```{r}
df <- dt.cat_centres.raw
df <- df %>% mutate(taula = "Catàleg de Centres")
res <- compareGroups(taula ~ .
                     , df
                     , max.xlev = 100
                     , method = 4
                     , include.label = FALSE)
export2md(missingTable(res
                       , show.p.overall = FALSE
                       ),
          format = "html",
          caption = "Exploració número de missing")
```

Control de qüalitat Valors:

```{r}
f.cq.table2(dt.cat_centres.raw)
```

```{r}
gc.var <- gc()
```

