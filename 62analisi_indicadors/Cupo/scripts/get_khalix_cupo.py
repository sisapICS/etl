# -*- coding: utf8 -*-

from lvclient import LvClient
import sisapUtils as u

#import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

POBASS = exemple(c, 'POBASS; A2409; PAMBITOS###; ORIGEN; EDATS1; NOIMP; DIM6SET')

c.close()

print('export')

file = u.tempFolder + "POBASS_2409.txt"
with open(file, 'w') as f:
   f.write(POBASS)
