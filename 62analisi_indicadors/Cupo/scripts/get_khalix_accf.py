# -*- coding: utf8 -*-

from lvclient import LvClient
import sisapUtils as u

#import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

ACCF = exemple(c, 'ACCF###; A2409YTD; PAMBITOS###; AGRESULT; NOCAT; NOIMP; DIM6SET')

c.close()

print('export')

file = u.tempFolder + "ACCF_2409.txt"
with open(file, 'w') as f:
   f.write(ACCF)
