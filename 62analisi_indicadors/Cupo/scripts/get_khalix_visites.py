# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

VISATE = exemple(c, 'VISITESTOT#99;B2409;PAMBITOS###;NOCLI;NOCAT;NOIMP;DIM6SET')

c.close()

print('export')

file = u.tempFolder + "VISATE_2409.txt"
with open(file, 'w') as f:
   f.write(VISATE)