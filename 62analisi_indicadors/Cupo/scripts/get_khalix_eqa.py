# -*- coding: utf8 -*-

# Creuaments UMI: http://sisap-umi.eines.portalics/indicador/indicador/1303/ver/
# Síntètic
# Nivell UBA

from lvclient import LvClient
import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

EQA = exemple(c, 'EQUBADULTS; A2409; PAMBITOS###; AGRESULT; NOCAT; NOINSAT; DIM6SET')

c.close()

print('export')

file = u.tempFolder + "EQA_2409.txt"
with open(file, 'w') as f:
   f.write(EQA)
