---
title: "Untitled"
author: "Edu"
date: "23/10/2024"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r test-main}
# Definir parámetros que quieres pasar

params <- list(eval.tabset = TRUE)
print(params)
new_params <- list(#variable = "pobass", 
                   title = "EQA")
params <- as.list(modifyList(params, new_params))
print(params)

# Llamar al archivo child y pasarle los parámetros
knitr::knit_child('09_prova.Rmd', envir = environment())
```