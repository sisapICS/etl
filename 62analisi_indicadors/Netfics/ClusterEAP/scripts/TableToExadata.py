import sisapUtils as u

upload = []
for row in u.readCSV("D:/SISAP/sisap/62analisi_indicadors/Netfics/ClusterEAP/results/SinteticOrganitzacioEAP.csv", sep = ","):
    upload.append(row)
#print(upload)    

cols = "(ics_codi varchar2(8), entitat_codi varchar2(8), entitat varchar2(100), \
         cluster_ind_3 varchar2(1), \
         accesibilitat_mf number, accesibilitat_inf number, \
         longitudinalitat_mf number, longitudinalitat_inf number, \
         gdemanda number, \
         Dx_QDiagnostica number, \
         Prev1_Habits number, Prev1_Vacunacions number, Prev1_Altres number, \
         Prev2_Cribatge number, Prev2_PrevCompl number, \
         Prev4_InadTract number, Prev4_InadProves number, \
         Seguiment number, \
         Res_AdeqTract number, Res_AdeqControl number, \
         Farm_Exposicio number, Farm_Seguretat number, Farm_Seleccio number, \
         Satisfaccio number)"

u.createTable('sintetic_organitzacio_eap', cols, 'exadata', rm=True)
u.listToTable(upload, 'sintetic_organitzacio_eap', "exadata")
u.grantSelect('sintetic_organitzacio_eap','DWSISAP_ROL','exadata')
