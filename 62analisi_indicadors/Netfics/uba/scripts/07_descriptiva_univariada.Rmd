***
***

# Descriptiva Univariada

Taula resum

```{r, warning=warning.var}

dt.mapa.uba.flt[, taula := params$taules[1]]
df <- dt.mapa.uba.flt[sample(100000)]
res <- compareGroups(taula ~ .
                     , df
                     , method = 1
                     , max.xlev = 100
                     , include.miss = TRUE)
cT <- createTable(res
                  , show.descr = FALSE
                  , show.all = TRUE
                  , show.p.overall = FALSE
                  , show.n = TRUE
                  )
export2md(cT,
          format = "html",
          caption = "")
```

***

## Resultat

```{r, eval=eval.fig.var}
ggplot(dt.mapa.uba.flt, aes(x = resultat)) +
       geom_histogram(fill = "darkgreen", color = "white") +
       theme_minimal()
```

***

## Punts

```{r, eval=eval.fig.var}
ggplot(dt.mapa.uba.flt, aes(x = punts)) +
  geom_histogram(fill = "darkgreen", color = "white") +
  theme_minimal()
```


```{r}
gc.var <- gc()
```


