---
title: "MAPA - Evaluació - Aleatorització"
author: "SISAP"
date: "`r format(Sys.Date(),'%d-%m-%Y')`"
always_allow_html: true
output:
  html_document:
    toc: yes
    toc_float: yes
    toc_depth: 6
  word_document:
    toc: yes
    toc_depth: '6'
---

***
***
***

### Actualitzacions de l'informe

- 2022-09-15: Inici informe

***

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

```{r parametres de conexió}
source("C:/Users/ehermosilla/Documents/Keys.R")
```

```{r parametres markdown}
inici <- Sys.time()

nfetch <- -1

warning.var <- FALSE
message.var <- FALSE

eval.timeexecution.var <- FALSE

var.fig.height <- 4
var.fig.width <- 6
```

```{r library}
library("RMySQL")
library("tibble")
library("data.table")
library("compareGroups")
library("blockrand")
library("DT")
```

```{r import}

drv <- dbDriver("MySQL")
con <- dbConnect(drv,
                 user = idsisap,
                 password = "",
                 host = hostsisap,
                 port = portsisap,
                 dbname = "nodrizas")
query <- dbSendQuery(con,
                     statement = "select ep, amb_desc, ics_codi, ics_desc, medea, aquas, tip_eap from cat_centres")
dt.cat_centres <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)

setnames(dt.cat_centres, "medea", "medea_c")
```

```{r data manager}

#dt.pi.dm[, .N, medea][order(medea)]
dt.cat_centres[, medea_c1 := medea_c]
dt.cat_centres[medea_c == "0R", medea_c1 := "Rural"]
dt.cat_centres[medea_c == "1R", medea_c1 := "Rural"]
dt.cat_centres[medea_c == "2R", medea_c1 := "Rural"]
#dt.cat_centres[, .N, .(medea_c, medea_c1)][order(medea_c)]

dt.cat_centres[medea_c1 == "Rural", rural := "Rural"]
dt.cat_centres[medea_c1 != "Rural", rural := "Urbà"]
dt.cat_centres[medea_c1 == "0", rural := NA]
dt.cat_centres[medea_c1 == "", rural := NA]
#dt.cat_centres[, .N, .(rural,medea_c1)][order(rural, medea_c1)]
```

```{r criteris inclusió}

# ICS
  dt.cat_centres.flt <- dt.cat_centres[ep == "0208",]

# No LP
  dt.cat_centres.flt <- dt.cat_centres.flt[tip_eap != "N",]
```

## Blocked randomization

```{r aleatorització}

# Creació aleatorització
dt.ale <- blockrand(n = 287, 
                    num.levels = 2, # two treatments
                    levels = c("Control", "Mapa"), # arm names
                    #stratum = "Bfail.LowAlb", # stratum name
                    #id.prefix = "Mapa", # stratum abbrev
                    block.sizes = c(1,2,3))
#dt.ale

# Creació variable aleatoria per ordenar les EAP
  set.seed(12345)
  random.var <- runif(287, 0, 1)
  dt.cat_centres.flt[, rand := random.var]
  dt.cat_centres.flt <- dt.cat_centres.flt[order(rand),]
  
# Creació variable per fer merge amb l'aleatorització
  dt.cat_centres.flt <- tibble::rownames_to_column(dt.cat_centres.flt)
  dt.cat_centres.flt[, rowname := as.numeric(rowname)]
  
# Juntar les dues data.table
  dt.cat.centres.ale <- merge(dt.cat_centres.flt,
                              dt.ale,
                              by.x = c("rowname"),
                              by.y = c("id"))
  
# Selecció dels 60 primers:
  dt.cat.centres.ale.60 <- dt.cat.centres.ale[rowname <= 60,]

# Comparaciço
cG <- compareGroups(treatment ~ ep + 
                                amb_desc +
                                medea_c + medea_c1 + rural +
                                aquas,
                    data.frame(dt.cat.centres.ale.60))
cT <- createTable(cG)
export2md(cT,
          format = "html")

cG <- compareGroups(treatment ~ ep + 
                                amb_desc +
                                medea_c + medea_c1 + rural +
                                aquas,
                    data.frame(dt.cat.centres.ale.60[rowname <= 48,]))
cT <- createTable(cG)
export2md(cT,
          format = "html")
```

## Stratificated Blocked randomization

```{r aleatorització estratificada}
# dt.cat_centres.flt[, .N, medea_c1]
# 1U	48			
# 2U	36			
# 3U	51			
# 4U	53			
# Rural	99
seed.var = 1234
set.seed(seed.var)
dt.1u.ale <- blockrand(n = 48, 
                       num.levels = 2, # two treatments
                       levels = c("Control", "Mapa"), # arm names
                       stratum = "1U", # stratum name
                       #id.prefix = "Mapa", # stratum abbrev
                       block.sizes = c(1))
random.var <- runif(48, 0, 1)
dt.cat_centres.1u.flt <- dt.cat_centres.flt[medea_c1 == "1U"][, rand := random.var][, rowname := NULL]
dt.cat_centres.1u.flt <- dt.cat_centres.1u.flt[order(rand),]
dt.cat_centres.1u.flt <- tibble::rownames_to_column(dt.cat_centres.1u.flt)
dt.cat_centres.1u.flt[, rowname := as.numeric(rowname)]
dt.cat.centres.1u.ale <- merge(dt.cat_centres.1u.flt,
                               dt.1u.ale,
                               by.x = c("rowname"),
                               by.y = c("id"))
dt.cat.centres.1u.ale.12 <- dt.cat.centres.1u.ale[rowname <= 12,]

set.seed(seed.var)
dt.2u.ale <- blockrand(n = 36, 
                       num.levels = 2, # two treatments
                       levels = c("Control", "Mapa"), # arm names
                       stratum = "2U", # stratum name
                       #id.prefix = "Mapa", # stratum abbrev
                       block.sizes = c(1))
random.var <- runif(36, 0, 1)
dt.cat_centres.2u.flt <- dt.cat_centres.flt[medea_c1 == "2U"][, rand := random.var][, rowname := NULL]
dt.cat_centres.2u.flt <- dt.cat_centres.2u.flt[order(rand),]
dt.cat_centres.2u.flt <- tibble::rownames_to_column(dt.cat_centres.2u.flt)
dt.cat_centres.2u.flt[, rowname := as.numeric(rowname)]
dt.cat.centres.2u.ale <- merge(dt.cat_centres.2u.flt,
                               dt.2u.ale,
                               by.x = c("rowname"),
                               by.y = c("id"))
dt.cat.centres.2u.ale.12 <- dt.cat.centres.2u.ale[rowname <= 12,]

set.seed(seed.var)
dt.3u.ale <- blockrand(n = 51, 
                       num.levels = 2, # two treatments
                       levels = c("Control", "Mapa"), # arm names
                       stratum = "3U", # stratum name
                       #id.prefix = "Mapa", # stratum abbrev
                       block.sizes = c(1))
random.var <- runif(51, 0, 1)
dt.cat_centres.3u.flt <- dt.cat_centres.flt[medea_c1 == "3U"][, rand := random.var][, rowname := NULL]
dt.cat_centres.3u.flt <- dt.cat_centres.3u.flt[order(rand),]
dt.cat_centres.3u.flt <- tibble::rownames_to_column(dt.cat_centres.3u.flt)
dt.cat_centres.3u.flt[, rowname := as.numeric(rowname)]
dt.cat.centres.3u.ale <- merge(dt.cat_centres.3u.flt,
                               dt.3u.ale,
                               by.x = c("rowname"),
                               by.y = c("id"))
dt.cat.centres.3u.ale.12 <- dt.cat.centres.3u.ale[rowname <= 12,]

set.seed(seed.var)
dt.4u.ale <- blockrand(n = 53, 
                       num.levels = 2, # two treatments
                       levels = c("Control", "Mapa"), # arm names
                       stratum = "4U", # stratum name
                       #id.prefix = "Mapa", # stratum abbrev
                       block.sizes = c(1))
random.var <- runif(53, 0, 1)
dt.cat_centres.4u.flt <- dt.cat_centres.flt[medea_c1 == "4U"][, rand := random.var][, rowname := NULL]
dt.cat_centres.4u.flt <- dt.cat_centres.4u.flt[order(rand),]
dt.cat_centres.4u.flt <- tibble::rownames_to_column(dt.cat_centres.4u.flt)
dt.cat_centres.4u.flt[, rowname := as.numeric(rowname)]
dt.cat.centres.4u.ale <- merge(dt.cat_centres.4u.flt,
                               dt.4u.ale,
                               by.x = c("rowname"),
                               by.y = c("id"))
dt.cat.centres.4u.ale.12 <- dt.cat.centres.4u.ale[rowname <= 12,]

set.seed(seed.var)
dt.r.ale <- blockrand(n = 93, 
                      num.levels = 2, # two treatments
                      levels = c("Control", "Mapa"), # arm names
                      stratum = "Rural", # stratum name
                       #id.prefix = "Mapa", # stratum abbrev
                      block.sizes = c(1))
random.var <- runif(93, 0, 1)
dt.cat_centres.r.flt <- dt.cat_centres.flt[medea_c1 == "Rural" & amb_desc != "ALT PIRINEU - ARAN",][, rand := random.var][, rowname := NULL]
dt.cat_centres.r.flt <- dt.cat_centres.r.flt[order(rand),]
dt.cat_centres.r.flt <- tibble::rownames_to_column(dt.cat_centres.r.flt)
dt.cat_centres.r.flt[, rowname := as.numeric(rowname)]
dt.cat.centres.r.ale <- merge(dt.cat_centres.r.flt,
                              dt.r.ale,
                              by.x = c("rowname"),
                              by.y = c("id"))
dt.cat.centres.r.ale.12 <- dt.cat.centres.r.ale[rowname <= 12,]
  
dt.cat.centres.medea.ale.12 <- rbind(dt.cat.centres.1u.ale.12,
                                     dt.cat.centres.2u.ale.12,
                                     dt.cat.centres.3u.ale.12,
                                     dt.cat.centres.4u.ale.12,
                                     dt.cat.centres.r.ale.12)
  
# Comparació
cG <- compareGroups(treatment ~ ep + 
                                amb_desc +
                                medea_c + medea_c1 + rural +
                                aquas,
                    data.frame(dt.cat.centres.medea.ale.12))
cT <- createTable(cG)
export2md(cT,
          format = "html")

cG <- compareGroups(treatment ~ ep + 
                                amb_desc +
                                medea_c + medea_c1 + rural +
                                aquas,
                    data.frame(dt.cat.centres.medea.ale.12[rowname <= 10,]))
cT <- createTable(cG)
export2md(cT,
          format = "html")
```

### LLista de EAP's aleatoritzades

```{r}
datatable(dt.cat.centres.medea.ale.12[rowname <= 10, .(treatment, ics_codi, ics_desc, amb_desc, medea_c1)],
          rownames = FALSE,
          filter = "top",
          options = list(ordering = T,
                         pageLength = nrow(dt.cat.centres.medea.ale.12[rowname <= 10,])))
```


***
***

```{r time execution, eval=eval.timeexecution.var}

final <- Sys.time()
print(round(final - inici,2))
```
