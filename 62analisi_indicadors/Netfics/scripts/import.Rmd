***
***

# Importació

```{r}
drv <- dbDriver("MySQL")
```

## `r params$taules[1]`

```{r}
con <- dbConnect(drv, user=idnetflics, password=pwnetflics, host=hostnetflics, port=portnetflics, dbname=dbnetflics)
query <- dbSendQuery(con,
                     statement = paste0("select * from ", params$taules[1]))
dt1 <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)

gc.var <- gc()
```

Informació importada:

- files n = `r nrow(dt1)`
- columnes n = `r ncol(dt1)`
- columnes: `r names(dt1)`

```{r}
names(dt1) <- tolower(names(dt1))
```

Quantitat de missing:

```{r}
df <- data.frame(dt1)
df <- df %>% mutate(taula=params$taules[1])
res <- compareGroups(taula ~ .
                     , df
                     , method = 4
                     , include.label = FALSE)
export2md(missingTable(res
                       , show.p.overall=FALSE
                       ),
          caption = "")
```

Control de qüalitat Valors:

```{r}
f.cq.table2(dt1)
```

***

## `r params$taules[2]`

```{r}
con <- dbConnect(drv, user=idnetflics, password=pwnetflics, host=hostnetflics, port=portnetflics, dbname=dbnetflics)
query <- dbSendQuery(con,
                     statement = paste0("select * from ", params$taules[2]))
dt2 <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)

gc.var <- gc()
```

Informació importada:

- files n = `r nrow(dt2)`
- columnes n = `r ncol(dt2)`
- columnes: `r names(dt2)`

```{r}
names(dt2) <- tolower(names(dt2))
```

Quantitat de missing:

```{r}
df <- data.frame(dt2)
df <- df %>% mutate(taula=params$taules[2])
res <- compareGroups(taula ~ .
                     , df
                     , method = 4
                     , include.label = FALSE)
export2md(missingTable(res
                       , show.p.overall=FALSE
                       ),
          caption = "")
```

Control de qüalitat Valors:

```{r}
f.cq.table2(dt2)
```

***

## `r params$taules[3]`

```{r}
con <- dbConnect(drv, user=idnetflics, password=pwnetflics, host=hostnetflics, port=portnetflics, dbname=dbnetflics)
query <- dbSendQuery(con,
                     statement = paste0("select * from ", params$taules[3]))
dt3 <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)

gc.var <- gc()
```

Informació importada:

- files n = `r nrow(dt3)`
- columnes n = `r ncol(dt3)`
- columnes: `r names(dt3)`

```{r}
names(dt3) <- tolower(names(dt3))
setnames(dt3, "codi", "entitat_cod")
setnames(dt3, "nom", "entitat_nom")
```

Quantitat de missing:

```{r}
df <- data.frame(dt3)
df <- df %>% mutate(taula=params$taules[3])
res <- compareGroups(taula ~ .
                     , df
                     , method = 4
                     , include.label = FALSE)
export2md(missingTable(res
                       , show.p.overall=FALSE
                       ),
          caption = "")
```

Control de qüalitat Valors:

```{r}
f.cq.table2(dt3)
```

***

## `r params$taules[4]`

```{r}
con <- dbConnect(drv, user=idnetflics, password=pwnetflics, host=hostnetflics, port=portnetflics, dbname=dbnetflics)
query <- dbSendQuery(con,
                     statement = paste0("select * from ", params$taules[4]))
dt4 <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)

gc.var <- gc()
```

Informació importada:

- files n = `r nrow(dt4)`
- columnes n = `r ncol(dt4)`
- columnes: `r names(dt4)`

```{r}
names(dt4) <- tolower(names(dt4))
```

Quantitat de missing:

```{r}
df <- data.frame(dt4)
df <- df %>% mutate(taula=params$taules[4])
res <- compareGroups(taula ~ .
                     , df
                     , method = 4
                     , include.label = FALSE)
export2md(missingTable(res
                       , show.p.overall=FALSE
                       ),
          caption = "")
```

Control de qüalitat Valors:

```{r}
f.cq.table2(dt4)
```

***

## `r params$taules[5]`

```{r}
con <- dbConnect(drv, user=idnetflics, password=pwnetflics, host=hostnetflics, port=portnetflics, dbname=dbnetflics)
query <- dbSendQuery(con,
                     statement = paste0("select * from ", params$taules[5]))
dt5 <- data.table(fetch(query, n = nfetch))
var.disconnect <- dbDisconnect(con)

gc.var <- gc()
```

Informació importada:

- files n = `r nrow(dt5)`
- columnes n = `r ncol(dt5)`
- columnes: `r names(dt5)`

```{r}
names(dt5) <- tolower(names(dt5))
```

Quantitat de missing:

```{r}
df <- data.frame(dt5)
df <- df %>% mutate(taula=params$taules[5])
res <- compareGroups(taula ~ .
                     , df
                     , method = 4
                     , include.label = FALSE)
export2md(missingTable(res
                       , show.p.overall=FALSE
                       ),
          caption = "")
```


Control de qüalitat Valors:

```{r}
f.cq.table2(dt5)
```

```{r}
gc.var <- gc()
```

