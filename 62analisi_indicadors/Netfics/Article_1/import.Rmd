

```{r}
library(RJDBC)
server<-"excdox-scan.cpd4.intranet.gencat.cat"
port<-"1522"
sid<-"excdox01srv"
# Create connection driver and open connection
jdbcDriver <- JDBC(driverClass="oracle.jdbc.OracleDriver", classPath="C:/Oracle/OJDBC-Full/ojdbc6.jar")
jdbcStr = paste("jdbc:oracle:thin:@", server, ":", port, "/", sid, sep="")
jdbcConnection = dbConnect(jdbcDriver, jdbcStr, "DWECOMA", "ranagustavo2020")



# Query on the Oracle instance name.
MAPA_MASTER_ETIQUETA <- as.data.table(dbGetQuery(jdbcConnection,"SELECT * FROM DWSISAP.MAPA_MASTER_ETIQUETA"))
# MAPA_MASTER_ETIQUETA_INDICADOR <- as.data.table(dbGetQuery(jdbcConnection,"SELECT * FROM DWSISAP.MAPA_MASTER_ETIQUETA_INDICADOR"))

dbDisconnect(jdbcConnection)
```

