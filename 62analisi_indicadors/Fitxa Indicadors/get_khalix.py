# -*- coding: utf8 -*-

from lvclient import LvClient
import sisapUtils as u

#import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    return res

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)


AGENDQC1_22 = exemple(c, 'AGENDQC1;AYR22###; AMBITOS###; NUM, DEN; ANUAL, MENSUAL; TIPPROF###; DIM6SET')
AGENDQC1_21 = exemple(c, 'AGENDQC1;AYR21###; AMBITOS###; NUM, DEN; ANUAL, MENSUAL; TIPPROF###; DIM6SET')
AGENDQC1_20 = exemple(c, 'AGENDQC1;AYR20###; AMBITOS###; NUM, DEN; ANUAL, MENSUAL; TIPPROF###; DIM6SET')
AGENDQC1_19 = exemple(c, 'AGENDQC1;AYR19###; AMBITOS###; NUM, DEN; ANUAL, MENSUAL; TIPPROF###; DIM6SET')





c.close()

print('export')

file = u.tempFolder + "AGENDQC1_22.txt"
with open(file, 'w') as f:
   f.write(AGENDQC1_22)
   
file = u.tempFolder + "AGENDQC1_21.txt"
with open(file, 'w') as f:
   f.write(AGENDQC1_21)
   
file = u.tempFolder + "AGENDQC1_20.txt"
with open(file, 'w') as f:
   f.write(AGENDQC1_20)

file = u.tempFolder + "AGENDQC1_19.txt"
with open(file, 'w') as f:
   f.write(AGENDQC1_19)