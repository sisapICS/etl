# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    print('RESULTAT:')
    print(res[:1000])
    print('')
    return res
    

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

com2022 = exemple(c, 'GRUPALCOM###;AYR22;AMBITOS###;NUM,DEN;NOCAT;NOIMP;DIM6SET')
com2021 = exemple(c, 'GRUPALCOM###;AYR21;AMBITOS###;NUM,DEN;NOCAT;NOIMP;DIM6SET')
com2020 = exemple(c, 'GRUPALCOM###;AYR20;AMBITOS###;NUM,DEN;NOCAT;NOIMP;DIM6SET')


c.close()

print('export')

file = u.tempFolder + "comunitaria_2022.txt"
with open(file, 'w') as f:
   f.write(com2022)

file = u.tempFolder + "comunitaria_2021.txt"
with open(file, 'w') as f:
   f.write(com2021)
   
file = u.tempFolder + "comunitaria_2020.txt"
with open(file, 'w') as f:
   f.write(com2020)