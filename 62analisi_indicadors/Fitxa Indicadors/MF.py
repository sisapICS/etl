# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    print('RESULTAT:')
    print(res[:1000])
    print('')
    return res
    

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

#metges23 = exemple(c, 'MA0012,MA0013,MA0014,MA0015,MA0016,MA0019;AYR23###;AMBITOS;NOCLI;NOCAT;NOIMP;DIM6SET')
#metges22 = exemple(c, 'MA0012,MA0013,MA0014,MA0015,MA0016,MA0019;AYR22###;AMBITOS;NOCLI;NOCAT;NOIMP;DIM6SET')
#metges21 = exemple(c, 'MA0012,MA0013,MA0014,MA0015,MA0016,MA0019;AYR21###;AMBITOS;NOCLI;NOCAT;NOIMP;DIM6SET')
#metges20 = exemple(c, 'MA0012,MA0013,MA0014,MA0015,MA0016,MA0019;AYR20###;AMBITOS;NOCLI;NOCAT;NOIMP;DIM6SET')
#metges19 = exemple(c, 'MA0012,MA0013,MA0014,MA0015,MA0016,MA0019;AYR19###;AMBITOS;NOCLI;NOCAT;NOIMP;DIM6SET')
#metges18 = exemple(c, 'MA0012,MA0013,MA0014,MA0015,MA0016,MA0019;AYR18###;AMBITOS;NOCLI;NOCAT;NOIMP;DIM6SET')

#metges2022 = exemple(c, 'MA0012,MA0015,MA0026, MA0032;B2209;AMBITOS###;NOCLI;NOCAT;NOIMP;DIM6SET')
#metges2023 = exemple(c, 'MA0012,MA0015,MA0026, MA0032;B2303###;AMBITOS###;NOCLI;NOCAT;NOIMP;DIM6SET')

#gma22 = exemple(c, 'MA0006;A2209;AMBITOS###;NOCLI;NOCAT;NOIMP;DIM6SET')

#gma22 = exemple(c, 'MA0006;A2209;AMBITOS;NOCLI;NOCAT;NOIMP;DIM6SET')
#ATDOM = exemple(c, 'MA0009;A2209;AMBITOS;NOCLI;NOCAT;NOIMP;DIM6SET')
#gma22 = exemple(c, 'MA0006;A2209;AMBITOS;NOCLI;NOCAT;NOIMP;DIM6SET')

#eaps = exemple(c, 'MA0012,MA0013,MA0014,MA0015,MA0016,MA0019;A2209;AMBITOS###;NOCLI;NOCAT;NOIMP;DIM6SET')

#eaps = exemple(c, 'MA0012;A2209;AMBITOS###;NOCLI;NOCAT;NOIMP;DIM6SET')

#atdom22 = exemple(c, 'MA0009;A2209;AMBITOS###;NOCLI;NOCAT;NOIMP;DIM6SET')

#mf_eqa23 = exemple(c, 'GRPEQAPOBT;AYR23###;AMBITOS;DEN;TIPOMEDA;NOIMP;DIM6SET')
#mf_eqa22 = exemple(c, 'GRPEQAPOBT;AYR22###;AMBITOS;DEN;TIPOMEDA;NOIMP;DIM6SET')
#mf_eqa21 = exemple(c, 'GRPEQAPOBT;AYR21###;AMBITOS;DEN;TIPOMEDA;NOIMP;DIM6SET')
#mf_eqa20 = exemple(c, 'GRPEQAPOBT;AYR20###;AMBITOS;DEN;TIPOMEDA;NOIMP;DIM6SET')
#mf_eqa19 = exemple(c, 'GRPEQAPOBT;AYR19###;AMBITOS;DEN;TIPOMEDA;NOIMP;DIM6SET')
#mf_eqa18 = exemple(c, 'GRPEQAPOBT;AYR18###;AMBITOS;DEN;TIPOMEDA;NOIMP;DIM6SET')

#mf_eqa23 = exemple(c, 'EQAPROCMPOC###;A1812;AMBITOS###;DEN;EDATS5;NOINSAT;SEXE')

#per_model = exemple(c, 'MA0001,MA0004,MA0005,MA0006,MA0007,MA0008,MA0009,MA0010;A2209;AMBITOS###;NOCLI;NOCAT;NOIMP;DIM6SET')
#consultoris = exemple(c, 'MA0015,MA0016;A2209;AMBITOS###;NOCLI;NOCAT;NOIMP;DIM6SET')

#ModelA = exemple(c, 'MA0012;A2209;AMBITOS###;NOCLI;NOCAT;NOIMP;DIM6SET')
#ModelB = exemple(c, 'MA0012;B2209;AMBITOS###;NOCLI;NOCAT;NOIMP;DIM6SET')

#ncentres = exemple(c, 'MA0010;A2212;AMBITOS###;NOCLI;NOCAT;NOIMP;DIM6SET')

#DVAC = exemple(c, 'DVAC175;A2405;AMBITOS;NUM,DEN;EDATS5;NOINSASS;SEXE')

#carassis = exemple(c, 'CARASSIS08;AYR22;AMBITOS###;NUM, DEN;NOCAT;TIPPROF1,TIPPROF2;DIM6SET')
#carassis23 = exemple(c, 'CARASSIS08;AYR23;AMBITOS###;NUM, DEN;NOCAT;TIPPROF1,TIPPROF2;DIM6SET')
#atnas = exemple(c, 'ATNAS14;A2305;BR212;NOCLI;NOCAT;NOIMP;DIM6SET')
#pobassat = exemple(c, 'POBASSAT;AYR23;AMBITOS###;NACIONS;EDATS1###;USUTIP;SEXE')
#pobass16_H = exemple(c, 'POBASS;A1612;AMBITOS###;NACIONS;EDATS1###;USUTIP;HOME')
#pobass16_D = exemple(c, 'POBASS;A1612;AMBITOS###;NACIONS;EDATS1###;USUTIP;DONA')
#pobass17_H = exemple(c, 'POBASS;A1712;AMBITOS###;NACIONS;EDATS1###;USUTIP;HOME')
#pobass17_D = exemple(c, 'POBASS;A1712;AMBITOS###;NACIONS;EDATS1###;USUTIP;DONA')
#pobass18_H = exemple(c, 'POBASS;A1812;AMBITOS###;NACIONS;EDATS1###;USUTIP;HOME')
#pobass18_D = exemple(c, 'POBASS;A1812;AMBITOS###;NACIONS;EDATS1###;USUTIP;DONA')
#pobass19_H = exemple(c, 'POBASS;A1912;AMBITOS###;NACIONS;EDATS1###;USUTIP;HOME')
#pobass19_D = exemple(c, 'POBASS;A1912;AMBITOS###;NACIONS;EDATS1###;USUTIP;DONA')
#pobass20_H = exemple(c, 'POBASS;A2012;AMBITOS###;NACIONS;EDATS1###;USUTIP;HOME')
#pobass20_D = exemple(c, 'POBASS;A2012;AMBITOS###;NACIONS;EDATS1###;USUTIP;DONA')
#pobass21_H = exemple(c, 'POBASS;A2112;AMBITOS###;NACIONS;EDATS1###;USUTIP;HOME')
#pobass21_D = exemple(c, 'POBASS;A2112;AMBITOS###;NACIONS;EDATS1###;USUTIP;DONA')
#pobass22_H = exemple(c, 'POBASS;A2212;AMBITOS###;NACIONS;EDATS1###;USUTIP;HOME')
#pobass22_D = exemple(c, 'POBASS;A2212;AMBITOS###;NACIONS;EDATS1###;USUTIP;DONA')

#POBASS_ALL = pobass16_H + pobass16_D + pobass17_H + pobass17_D + pobass18_H + pobass18_D + pobass19_H + pobass19_D + pobass20_H + pobass20_D + pobass21_H + pobass21_D + pobass22_H + pobass22_D

#consob = exemple(c, 'CONSOB02;A2306;AMBITOS###;NUM,DEN;CURS;TORIGEN;AGRSEXE')

#IT = exemple(c, 'AGRIT###;A2312;AMBITOS###;AGRESULT, NUM, DEN;EDATS5;NOINSAT;SEXE')
#despeses = exemple(c, 'ACTIVDOC###;AYR23;AMBITOS###;CATPROF###;NOCAT;NOIMP;DIM6SET')
#anpicost = exemple(c, 'ANPICOST,ANPICOSTF;AYR23;AMBITOS###;PIHTOT###;PROVEIDOR###;NOIMP;DIM6SET')
#ACC = exemple(c, 'ACC###;A2311;AMBITOS###;AGRESULT;NOCAT;TIPPROF1;DIM6SET')
#LONG  = exemple(c, 'AGLONGCONT###;A2311;AMBITOS###;NUM, DEN;NOCAT;TIPPROF1;DIM6SET')
#EQA  = exemple(c, 'EQUBADULTS###;A2309;PAMBITOS###;AGRESOLTR;NOCAT;NOINSAT;DIM6SET')
#EQA_SINT  = exemple(c, 'EQUBADULTS;A2309;PAMBITOS###;AGASSOLP;NOCAT;NOINSAT;DIM6SET')
#EQA_SINT2  = exemple(c, 'EQUBADULTS;A2309;PAMBITOS###;AGRESULT;NOCAT;NOINSAT;DIM6SET')
#EQPFUBA  = exemple(c, 'EQPFUBA###;A2309;PAMBITOS###;AGRESULT;NOCAT;NOIMP;DIM6SET')
#EQPF_sint  = exemple(c, 'EQPFUBA;A2309;PAMBITOS###;AGASSOL;NOCAT;NOIMP;DIM6SET')

#cap1  = exemple(c, 'AGPERFV###;AYR23;AMBITOS###;CATPROF###;NOCAT;NOIMP;DIM6SET')

#junt = IT + ACC + LONG 
# + EQA + EQA_SINT + EQPFUBA + EQPF_sint + EQA_SINT2
#junt2 = EQA_SINT + EQA_SINT2 + EQPF_sint



#FREQ  = exemple(c, 'VISMF, VISINF;BYR23###;AMBITOS###;FREQATESA;EDATS5;VISTOT###;SEXE')

#visites23  = exemple(c, 'VISMF, VISINF;AYR23###;AMBITOS###;NOCLI;EDATS5;VISTOT###;SEXE')
#visites22  = exemple(c, 'VISMF, VISINF;AYR22###;AMBITOS###;NOCLI;EDATS5;VISTOT###;SEXE')

#VISURG  = exemple(c, 'VISURG;AYR23###;AMBITOS###;NUM,DEN;MENSUAL,ANUAL;TIPPROF###;DIM6SET')

#ATNASAGA  = exemple(c, 'ATNASAGA014,ATNASAGA14;A2402;AMBITOS###;NOCLI;NOCAT;NOIMP;DIM6SET')

#es11 = exemple(c,'ES11; B2410; AMBITOS###; NUM,DEN; EDATS5; NOINSAT; SEXE###')
#visitesmf  = exemple(c, 'VISTOTMF;A2408;PAMBITOS###;VISEDATS;FREQATES, FREQASSI;NOIMP;DIM6SET')
#print DVAC
#LONG5  = exemple(c, 'CONT0005, CONT0005A;A2412;AMBITOS;NUM, DEN;NOCAT;TIPPROF###;DIM6SET')
#punts = exemple(c, 'EQADULTS#2;DEF2025;NOENT;PUNTSMAX;MF,INF;NOIMP;DIM6SET')

LONG  = exemple(c, 'CONT0001,CONT0002,CONT0003,CONT0004;AYR17###;AMBITOS###;NUM, DEN;NOCAT;TIPPROF1;DIM6SET')
#LONGa  = exemple(c, 'CONT0001A,CONT0002A,CONT0003A,CONT0004A;AYR20###;AMBITOS###;NUM, DEN;NOCAT;TIPPROF1;DIM6SET')
#EQA_adults = exemple(c, 'EQA0213, EQA0212, EQA0235, EQA0205, EQA0204, EQA0209, EQA0214, EQA0231;A2412;AMBITOS###;NUM,DEN;EDATS5;NOINSAT;SEXE')
#polimed = exemple(c, 'POLIMED15, MEDADIAB, MEDESTATINES, MEDIBP;A2412;AMBITOS###;NUM,DEN;NOCAT;NOIMP;DIM6SET')

print('export')

file = u.tempFolder + "LONG_2017.txt"
with open(file, 'w') as f:
   f.write(LONG)

"""  
file = u.tempFolder + "LONGA_2020.txt"
with open(file, 'w') as f:
   f.write(LONGa)
  
file = u.tempFolder + "polimedicats.txt"
with open(file, 'w') as f:
   f.write(polimed)

file = u.tempFolder + "eqa_adults_202412.txt"
with open(file, 'w') as f:
   f.write(EQA_adults)
   

   


file = u.tempFolder + "long_PHY.txt"
with open(file, 'w') as f:
   f.write(LONG5)
 
file = u.tempFolder + "ES11.txt"
with open(file, 'w') as f:
   f.write(es11)
  


file = u.tempFolder + "POBASS_2016_2022.txt"
with open(file, 'w') as f:
   f.write(POBASS_ALL)
  
file = u.tempFolder + "visites_2023.txt"
with open(file, 'w') as f:
   f.write(visites23)

file = u.tempFolder + "visites_2022.txt"
with open(file, 'w') as f:
   f.write(visites22)
   

file = u.tempFolder + "pobassat_2023.txt"
with open(file, 'w') as f:
   f.write(pobassat)

file = u.tempFolder + "pobass_2023.txt"
with open(file, 'w') as f:
   f.write(pobass)
   



file = u.tempFolder + "longA_2023.txt"
with open(file, 'w') as f:
   f.write(LONGa)
   
file = u.tempFolder + "VISURG_2023.txt"
with open(file, 'w') as f:
   f.write(VISURG)
   
file = u.tempFolder + "FREQUENTACIO_2023.txt"
with open(file, 'w') as f:
   f.write(FREQ)


file = u.tempFolder + "ATNASAGA.txt"
with open(file, 'w') as f:
   f.write(ATNASAGA)

file = u.tempFolder + "long_202312.txt"
with open(file, 'w') as f:
   f.write(LONG)
   



file = u.tempFolder + "PROVES_202312.txt"
with open(file, 'w') as f:
   f.write(anpicost)



file = u.tempFolder + "despeses_202312.txt"
with open(file, 'w') as f:
   f.write(despeses)


file = u.tempFolder + "it_202312.txt"
with open(file, 'w') as f:
   f.write(IT)
   


   

file = u.tempFolder + "CAPITOL1_202312.txt"
with open(file, 'w') as f:
   f.write(cap1)


file = u.tempFolder + "pobassat_202312.txt"
with open(file, 'w') as f:
   f.write(pobassat)

file = u.tempFolder + "pobass_202312.txt"
with open(file, 'w') as f:
   f.write(pobass)
   

   
  
file = u.tempFolder + "metges_2022.txt"
with open(file, 'w') as f:
   f.write(metges22)
   
file = u.tempFolder + "metges_2023.txt"
with open(file, 'w') as f:
   f.write(metges23)
   
   
file = u.tempFolder + "metges_2023.txt"
with open(file, 'w') as f:
   f.write(metges2023)
   

file = u.tempFolder + "carassis_2022.txt"
with open(file, 'w') as f:
   f.write(carassis)
   
file = u.tempFolder + "carassis_2023.txt"
with open(file, 'w') as f:
   f.write(carassis23)


file = u.tempFolder + "atnas.txt"
with open(file, 'w') as f:
   f.write(atnas)

file = u.tempFolder + "DVAC.txt"
with open(file, 'w') as f:
   f.write(DVAC)



file = u.tempFolder + "idea_ciscu_up.txt"
with open(file, 
'w') as f:
   f.write(junt)
   

   

file = u.tempFolder + "MODEL_MF22.txt"
with open(file, 'w') as f:
   f.write(metges22)

file = u.tempFolder + "MODEL_MF21.txt"
with open(file, 'w') as f:
   f.write(metges21)
 
file = u.tempFolder + "MODEL_MF20.txt"
with open(file, 'w') as f:
   f.write(metges20)
 
file = u.tempFolder + "MODEL_MF19.txt"
with open(file, 'w') as f:
   f.write(metges19)
 
file = u.tempFolder + "MODEL_MF18.txt"
with open(file, 'w') as f:
   f.write(metges18)

   
file = u.tempFolder + "GMA_22.txt"
with open(file, 'w') as f:
   f.write(gma22)


file = u.tempFolder + "eaps2022_09.txt"
with open(file, 'w') as f:
   f.write(eaps)

file = u.tempFolder + "EQA_MF22.txt"
with open(file, 'w') as f:
   f.write(mf_eqa22)

file = u.tempFolder + "EQA_MF21.txt"
with open(file, 'w') as f:
   f.write(mf_eqa21)
 
file = u.tempFolder + "EQA_MF20.txt"
with open(file, 'w') as f:
   f.write(mf_eqa20)
 
file = u.tempFolder + "EQA_MF19.txt"
with open(file, 'w') as f:
   f.write(mf_eqa19)
 
file = u.tempFolder + "EQA_MF18.txt"
with open(file, 'w') as f:
   f.write(mf_eqa18)

file = u.tempFolder + "model_C_eap.txt"
with open(file, 'w') as f:
   f.write(per_model)


file = u.tempFolder + "model_consultoris.txt"
with open(file, 'w') as f:
   f.write(consultoris)

file = u.tempFolder + "modelA.txt"
with open(file, 'w') as f:
   f.write(ModelA)
   
file = u.tempFolder + "modelB.txt"
with open(file, 'w') as f:
   f.write(ModelB)


file = u.tempFolder + "MF_esperats_202209.txt"
with open(file, 'w') as f:
   f.write(eaps)


file = u.tempFolder + "atdom_22.txt"
with open(file, 'w') as f:
   f.write(atdom22)

file = u.tempFolder + "CONSOB.txt"
with open(file, 'w') as f:
   f.write(consob)


file = u.tempFolder + "EQA_MF23.txt"
with open(file, 'w') as f:
   f.write(mf_eqa23)
   
"""