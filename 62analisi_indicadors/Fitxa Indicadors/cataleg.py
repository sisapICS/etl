# coding: iso-8859-1

from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

"""
Fem procés per crear catàleg de l'any que vulguem les fitxes
"""


upload = []
db = "permanent"

sql = "select indicador, literal, wiki, dataany from eqacataleg where pantalla='GENERAL'"
for indicador, literal, wiki, anys in getAll(sql, 'pdp'):
    upload.append([indicador, literal, wiki, anys])
    
tb = "exp_ecap_cataleg"
columns =  ["indicador varchar(20)", "literal varchar(200)", "wiki varchar(500)", "any int"]     
createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
listToTable(upload, tb, db)    