# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    print('RESULTAT:')
    print(res[:1000])
    print('')
    return res
    

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

EQA2013 = exemple(c, 'QAA08;A1312;AMBITOS###;AGRESOLT;NOCAT;NOIMP;DIM6SET')
EQA2012 = exemple(c, 'QAA08;A1212;AMBITOS###;AGRESOLT;NOCAT;NOIMP;DIM6SET')
EQA2011 = exemple(c, 'QAA08;A1112;AMBITOS###;AGRESOLT;NOCAT;NOIMP;DIM6SET')
EQA2010 = exemple(c, 'QAA08;A1012;AMBITOS###;AGRESOLT;NOCAT;NOIMP;DIM6SET')
EQA2009 = exemple(c, 'QAA08;A0912;AMBITOS###;AGRESOLT;NOCAT;NOIMP;DIM6SET')
EQA2008 = exemple(c, 'QAA08;A0812;AMBITOS###;AGRESOLT;NOCAT;NOIMP;DIM6SET')
EQA2007 = exemple(c, 'QAA08;A0712;AMBITOS###;AGRESOLT;NOCAT;NOIMP;DIM6SET')







c.close()

print('export')

file = u.tempFolder + "EQA2013_08.txt"
with open(file, 'w') as f:
   f.write(EQA2013)
   
file = u.tempFolder + "EQA2012_08.txt"
with open(file, 'w') as f:
   f.write(EQA2012)
   
file = u.tempFolder + "EQA2011_08.txt"
with open(file, 'w') as f:
   f.write(EQA2011)

file = u.tempFolder + "EQA2010_08.txt"
with open(file, 'w') as f:
   f.write(EQA2010)
   
file = u.tempFolder + "EQA2009_08.txt"
with open(file, 'w') as f:
   f.write(EQA2009)

file = u.tempFolder + "EQA2008_08.txt"
with open(file, 'w') as f:
   f.write(EQA2008)

file = u.tempFolder + "EQA2007_08.txt"
with open(file, 'w') as f:
   f.write(EQA2007)

