# -*- coding: utf8 -*-

from lvclient import LvClient

import sisapUtils as u

def exemple(conn, query):
    print("executem: {}".format(query))
    res = conn.query(query)
    print('RESULTAT:')
    print(res[:1000])
    print('')
    return res
    

c = LvClient('ecoma', 'y#8e$uic', '10.52.137.134', 50002)

homes = exemple(c, 'EQADULTS###;A2112;AMBITOS###;NUM,DEN;EDATS5###;NOINSAT;HOME')
dones = exemple(c, 'EQADULTS###;A2112;AMBITOS###;NUM,DEN;EDATS5###;NOINSAT;DONA')



c.close()

print('export')

file = u.tempFolder + "EQA_202112_home.txt"
with open(file, 'w') as f:
   f.write(homes)

file = u.tempFolder + "EQA_202112_dona.txt"
with open(file, 'w') as f:
   f.write(dones)
