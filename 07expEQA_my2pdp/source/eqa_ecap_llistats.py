from sisapUtils import *
import sys

try:
    db= sys.argv[1]
    pf= sys.argv[1][:3]
except:
    sys.exit("no m'has passat arguments")

do= {'eqa_ind':True,'pedia':True,'social':False,'odn':False}
t= {'eqa_ind':'M','pedia':'M','social':'T','odn':'O'}        
tip=t[db]

pac = "%s.exp_ecap_pacient" % db
uba_in = "%s.mst_ubas" % db
dext = "nodrizas.dextraccio"    #compte

fmo=False
fmo_up = "('00441', '00295', '00108')"

if do[db]:
    if db == "eqa_ind":
        if fmo:
            query= "select up,uba,'M',grup_codi,hash_d,a.sector,status,valor,text from {}_new a inner join nodrizas.cat_centres b on a.up=b.scs_codi where up in {}".format(pac,fmo_up)
            query += " union select upinf,ubainf,'I',grup_codi,hash_d,a.sector,status,valor,text from {}_new a inner join nodrizas.cat_centres b on a.up=b.scs_codi where up in {}".format(pac,fmo_up)
            query += " union select a.*, '' valor, '' text from eqa_ind.exp_ecap_pacient_proc a where up in {}".format(fmo_up)
        else:
            query= "select up,uba,'M',grup_codi,hash_d,a.sector,status,valor,text from %s_new a inner join nodrizas.cat_centres b on a.up=b.scs_codi where exists (select 1 from %s b where a.up=b.up and a.uba=b.uba and tipus='M')" % (pac,uba_in)
            query += " union select upinf,ubainf,'I',grup_codi,hash_d,a.sector,status,valor,text from %s_new a inner join nodrizas.cat_centres b on a.up=b.scs_codi where exists (select 1 from %s b where a.upinf=b.up and a.ubainf=b.uba and tipus='I')" % (pac,uba_in)
            query += " union select a.*, '' valor, '' text from eqa_ind.exp_ecap_pacient_proc a"
    elif db == "pedia":
        query= "select up,uba,'M',grup_codi,hash_d,a.sector,exclos,comentari from %s a inner join nodrizas.cat_centres b on a.up=b.scs_codi where exists (select 1 from %s b where a.up=b.up and a.uba=b.uba and tipus='M')" % (pac,uba_in)
        query += " union select upinf,ubainf,'I',grup_codi,hash_d,a.sector,exclos,comentari from %s a inner join nodrizas.cat_centres b on a.up=b.scs_codi where exists (select 1 from %s b where a.upinf=b.up and a.ubainf=b.uba and tipus='I')" % (pac,uba_in)        
    else:
        query= "select up,uba,'M',grup_codi,hash_d,a.sector,exclos from %s a inner join nodrizas.cat_centres b on a.up=b.scs_codi where exists (select 1 from %s b where a.up=b.up and a.uba=b.uba and tipus='M')" % (pac,uba_in)
        query += " union select upinf,ubainf,'I',grup_codi,hash_d,a.sector,exclos from %s a inner join nodrizas.cat_centres b on a.up=b.scs_codi where exists (select 1 from %s b where a.upinf=b.up and a.ubainf=b.uba and tipus='I')" % (pac,uba_in)
        
else:
   query= "select up,'','%s',indicador,hash_d,sector,exclos from %s a where exists (select 1 from %s b where a.up=b.up and tipus='%s')" % (tip,pac,uba_in,tip)
   if tip == "O":
      query_prs= "select up,'','%s',indicador,hash_d,sector,exclos from %s_jail a where exists (select 1 from %s_jail b where a.up=b.up and tipus='%s')" % (tip,pac,uba_in,tip)
   
table= pf + "Llistats"
exportPDP(query=query,table=table,truncate=True,pkOut=True,pkIn=True,fmo=fmo)
if tip == "O":
   table_prs= "prs" + pf + "Llistats"
   exportPDP(query=query_prs,table=table_prs,truncate=True,pkOut=True,pkIn=True,fmo=fmo)

query= "select data_ext from %s limit 1" % dext
table= pf + "LlistatsData"
exportPDP(query=query,table=table,truncate=True,fmo=fmo)
if tip == "O":
   table_prs= "prs" + pf + "LlistatsData"
   exportPDP(query=query,table=table_prs,truncate=True,fmo=fmo)