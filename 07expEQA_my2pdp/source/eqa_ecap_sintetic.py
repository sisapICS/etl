from sisapUtils import *
from collections import Counter
from sys import argv,exit
import os


posem_nosaltres_una_meta_inicial = False
metes_individualitzades = True
metes_via_khalix = {}
pathodnmetessintetic = 'eqaodo_metes_sintetic.txt'
pathsocmetessintetic = 'eqats_metes_2021.txt'

try:
    db = argv[1]
    pf = db[:3]
except:
    exit("no m'has passat arguments")

# db = 'eqa_ind'
# pf = 'eqa'

# db = 'pedia'
# pf = 'ped'

# db = 'ass'
# pf = 'ass'

# db = 'odn'
# pf = 'odn'

# db = 'social'
# pf = 'soc'

values = {
    'eqa': {'minFix':0,'maxFix':0,'p20':550,'p80':700,'esforc':('888'),'mensual':0.05,'tipus':'M', 'minDataMes': 0},
    'ped': {'minFix':0,'maxFix':0,'p20':680,'p80':780,'esforc':('888'),'mensual':0.05,'tipus':'M', 'minDataMes': 0},
    'soc': {'minFix':0,'maxFix':0,'p20':20,'p80':60,'esforc':(),'mensual':0.05,'tipus_up':'AMBTS', 'minDataMes': 0},
    'odn': {'minFix':0,'maxFix':0,'p20':20,'p80':65,'esforc':(),'mensual':0.05,'tipus_up':'M', 'minDataMes': 0},
    'ass': {'minFix':0,'maxFix':0,'p20':55,'p80':75,'esforc':(),'mensual':0.05,'tipus':'G', 'minDataMes': 0}
}

if db in metes_via_khalix:
    centres = {}
    sql = "select scs_codi, ics_codi from cat_centres"
    for up, br in getAll(sql, 'nodrizas'):
        centres[br] = {'up': up}
    if db in ('odn', 'social'):
        professionals = {}
        sql = "select up, dni from mst_professionals"
        for up, dni in getAll(sql, db):
            professionals[(dni,up)] = True
        up_metes = {}
        if db == 'odn':
            path = os.path.join('source', pathodnmetessintetic)
        if db == 'social':
            path = os.path.join('source', pathsocmetessintetic)
        print(path)            
        with open(path, 'rb') as file:
            m=csv.reader(file, delimiter='{', quotechar='|')
            for met in m:
                indicador,z1,br,meta,z2,z3,z4,valor = met[0],met[1],met[2],met[3],met[4],met[5],met[6],met[7]
                if br in centres:
                    up = centres[br]['up']
                    if meta == 'AGMMIN':
                        up_metes[(up, 'mmin')] = float(valor)
                    if meta == 'AGMMAX':
                        up_metes[(up, 'mmax')] = float(valor)
        print(len(up_metes))

ora = 'pdp'
dataAny,dataMes = getOne("select year(data_ext),date_format(data_ext,'%c') from dextraccio",'nodrizas')
if pf in ('odn', 'soc'):
    max, = getOne("select sum(punts) from {}CatalegPunts where dataAny={} and tipus_up='{}'".format(pf,dataAny,values[pf]['tipus_up']),ora)
else:
    max, = getOne("select sum(punts) from {}CatalegPunts where dataAny={} and tipus='{}'".format(pf,dataAny,values[pf]['tipus']),ora)
    
execute("delete from {}Sintetic where dataAny={} and dataMes={}".format(pf,dataAny,dataMes),ora)
execute("delete from {}SinteticMetes where basalAny={} and basalMes={}".format(pf,dataAny,dataMes),ora)

if pf == "eqa" and dataAny == 2025:
    flash_ups = set([up for up, in getAll("select up from flashups", "pdp")])
    flash_ind = set([ind for ind, in getAll("select indicador from flashindicadors", "pdp")])
    flash_top = {tip: punts for tip, punts in getAll("select tipus, 1000 - sum(punts) from flashindicadors a inner join eqacatalegpunts b on a.indicador = b.indicador where dataany = 2025 group by tipus", "pdp")}
    print(flash_top)
    flash_exc = set([row for row in getAll("select up, uab, tipus from flashprofexclosos", "pdp")])
    for row in flash_exc:
        this = "delete from eqaSinteticMetes where dataAny = 2025 and up = '{}' and uab = '{}' and tipus = '{}'".format(*row)
        execute(this, "pdp")

ambit = {}
q = "select scs_codi,amb_codi from cat_centres"
for up,amb in getAll(q,'nodrizas'):
    ambit[up] = amb

showed = {}
q = "select indicador,toShow from {}Cataleg where dataAny={} and pantalla = 'GENERAL'".format(pf,dataAny)
for indicador,toShow in getAll(q,ora):
    showed[indicador] = True if toShow == 1 else False
print("SHOWED:", len(showed))

selected = {}
q = "select up,uab,tipus,indicador,punts from {}Individualitzat where dataAny={}".format(pf,dataAny)
for up,uba,tipus,indicador,punts in getAll(q,ora):
    selected[up,uba,tipus,indicador] = punts
print("SELECTED:", len(selected))

goals = {}
q = "select eqa,up,uab,tipus,mmin,mint,mmax from {}SinteticMetes where dataAny={}".format(pf,dataAny)
for eqa,up,uba,tipus,mmin,mint,mmax in getAll(q,ora):
    goals[eqa,up,uba,tipus] = mmin,mint,mmax
print("GOALS:", len(goals))

firstMonth = {}
firstUpload = []
q = "select up,uab,tipus,min(dataMes) from {}Indicadors where dataAny={} and dataMes>={} group by up,uab,tipus".format(pf,dataAny,values[pf]['minDataMes'])
for up,uba,tipus,mes in getAll(q,ora):
    firstMonth[up,uba,tipus] = mes
    firstUpload.append([dataAny,mes,up,uba,tipus])
try:    
    execute('drop table sisap_first',ora)
except:
    pass
execute('create table sisap_first (dataany int,datames int,up varchar2(5),uab varchar2(20),tipus varchar2(1),constraint pk PRIMARY KEY (dataany,datames,up,uab,tipus))',ora)
uploadOra(firstUpload,'sisap_first',ora)
print("FIRSTUPLOAD:", len(firstUpload))

start = Counter()
q = "select a.up,a.uab,a.tipus,indicador,punts,assolResultat from {0}Indicadors a,sisap_first b where a.dataany=b.dataany and a.dataMes=b.dataMes and a.up=b.up and a.uab=b.uab and a.tipus=b.tipus".format(pf)
for up,uba,tipus,indicador,punts,assolResultat in getAll(q,ora):
    if indicador in showed and not (pf == "eqa" and dataAny == 2025 and up in flash_ups and indicador in flash_ind and (up, uba, tipus) not in flash_exc):
        try:
            start['G',up,uba,tipus] += punts
        except TypeError:
            continue
        try:
            start['I',up,uba,tipus] += assolResultat * selected[up,uba,tipus,indicador]
        except KeyError:
            pass
execute('drop table sisap_first',ora)
print("START:", len(start))

points = Counter()
unresolved = Counter()
q = "select up,uab,tipus,indicador,punts,noResolts,assolResultat from {}Indicadors where dataAny={} and dataMes={}".format(pf,dataAny,dataMes)
print(q)
for up,uba,tipus,indicador,punts,noResolts,assolResultat in getAll(q,ora):
    if indicador in showed and showed[indicador] and not (pf == "eqa" and dataAny == 2025 and up in flash_ups and indicador in flash_ind and (up, uba, tipus) not in flash_exc):
        points['G',up,uba,tipus] += punts
        unresolved['G',up,uba,tipus] += noResolts
        try:
            points['I',up,uba,tipus] += assolResultat * selected[up,uba,tipus,indicador]
            unresolved['I',up,uba,tipus] += noResolts
        except KeyError:
            pass
print("POINTS:", len(points))

newGoals = []
achievement = []
khalix = {'G':[],'I':[]}
for (eqa,up,uba,tipus),total in points.items():
    try:
        mmin,mint,mmax = goals[eqa,up,uba,tipus]
    except KeyError:
        baseMes = firstMonth[up,uba,tipus]
        basal = start[eqa,up,uba,tipus]
        try:
            ambits = ambit[up]
        except KeyError:
            ambits = 'UP'
        if metes_individualitzades:
            if db in metes_via_khalix:
                if up != 'UP' and (up, 'mmin') in up_metes:
                    mmin = up_metes[(up, 'mmin')]
                    mmax = up_metes[(up, 'mmax')]
                    mint = ((mmin + mmax) / 2) if metes_individualitzades else None        
                    newGoals.append([dataAny,eqa,up,uba,tipus,dataAny,baseMes,basal,mmin,mint,mmax,'',basal,mmin,mint,mmax])
                else:
                    mmin = None
                    mmax = None
                    mint = None
                    newGoals.append([dataAny,eqa,up,uba,tipus,dataAny,baseMes,basal,mmin,mint,mmax,'',basal,mmin,mint,mmax])
            else:
                mesos = 12 - baseMes
                esforc = mesos * values[pf]['mensual']
                p20 = values[pf]['p20']
                p80 = values[pf]['p80']
                if pf == "eqa" and dataAny == 2025 and up in flash_ups and (up, uba, tipus) not in flash_exc:
                    p20 = p20 * flash_top[tipus] / 1000
                    p80 = p80 * flash_top[tipus] / 1000
                mmin = p20 if basal >= p20 else basal + (esforc * (p20 - basal))
                mmax = p80 if basal >= p80 else basal + (esforc * (p80 - basal))
                mint = ((mmin + mmax) / 2) if metes_individualitzades else None        
                newGoals.append([dataAny,eqa,up,uba,tipus,dataAny,baseMes,basal,mmin,mint,mmax,'',basal,mmin,mint,mmax])
        else:
            mmin = values[pf]['minFix'] if posem_nosaltres_una_meta_inicial else None
            mmax = values[pf]['maxFix'] if posem_nosaltres_una_meta_inicial else None
            mint = ((mmin + mmax) / 2) if metes_individualitzades else None        
            newGoals.append([dataAny,eqa,up,uba,tipus,dataAny,baseMes,basal,mmin,mint,mmax,'',basal,mmin,mint,mmax])
        # mint = ((mmin + mmax) / 2) if posem_nosaltres_una_meta_inicial else None
    if mmin:
        if total >= mmax:
            assoliment = 1
        elif total <= mmin:
            assoliment = 0
        else:
            assoliment = (total - mmin) / (mmax - mmin)
    else:
        assoliment = None
    if pf == "eqa" and dataAny == 2025 and up in flash_ups and (up, uba, tipus) not in flash_exc:
        maxim = flash_top[tipus]
    else:
        maxim = max
    #print(dataAny,dataMes,eqa,up,uba,tipus,unresolved[eqa,up,uba,tipus],total,maxim,assoliment)
    achievement.append([dataAny,dataMes,eqa,up,uba,tipus,unresolved[eqa,up,uba,tipus],total,int(round(maxim)),assoliment])
    khalix[eqa].append([up,uba,'L' if tipus == 'E' else tipus,'AGRESULT',total])
    khalix[eqa].append([up,uba,'L' if tipus == 'E' else tipus,'AGASSOLP',assoliment])
    khalix[eqa].append([up,uba,'L' if tipus == 'E' else tipus,'AGMMIN',mmin])
    khalix[eqa].append([up,uba,'L' if tipus == 'E' else tipus,'AGMINT',mint])
    khalix[eqa].append([up,uba,'L' if tipus == 'E' else tipus,'AGMMAX',mmax])

#writeCSV(tempFolder + 'newGoals.csv', newGoals) 
#writeCSV(tempFolder + 'achievement.csv', achievement) 

exportPDP(newGoals,'{}SinteticMetes'.format(pf),obj=True,fmo=False)
exportPDP(achievement,'{}Sintetic'.format(pf),obj=True,fmo=False)

for eqa, in ('G','I'):
    file = tempFolder + eqa + '.txt'
    table = 'exp_khalix_uba_sintetic' if eqa == 'G' else 'exp_khalix_uba_carta'
    writeCSV(file,khalix[eqa])
    execute('drop table if exists {}'.format(table),db)
    execute('create table {} (up varchar(5),uba varchar(10),tipus varchar(1),analisis varchar(10),dada double)'.format(table),db)
    loadData(file,table,db)
