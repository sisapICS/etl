from sisapUtils import *
import time, sys

origen="EQALLISTATSINCIDENCIES"
desti="EQALLISTATSINCIDENCIES_COPSEG"
u11="PREDUFFA.SISAP_U11"    #grant donat a creacio u11
w=1

while w==1:
    try:
        conn = connect('pdp')
        conn.close()
        w=0
    except:
        time.sleep(600)

try:
    conn = connect('pdp')
    c=conn.cursor()
    c.execute("drop table %s" % desti)    
    c.execute("create table %s as select * from %s" % (desti,origen))
    c.execute("truncate table %s" % origen)
    c.execute("insert into %s select eqa.sector as sector,hash_d as cip,login,indicador,desactivat,visitavirtual from %s eqa,%s t11 where cip=hash_a and eqa.sector=t11.sector" % (origen,desti,u11))
    conn.commit()
    conn.close()
except:
    sys.exit("algo no ha anat be")
