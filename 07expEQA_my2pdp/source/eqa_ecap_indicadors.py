﻿# coding: utf-8
import datetime as d
import sisapUtils as u
import sys

SUBSCRIBERS = [
    'mfabregase@gencat.cat',
    'eballo.girona.ics@gencat.cat',
    'mbenitez.bcn.ics@gencat.cat',
    'cguiriguet.bnm.ics@gencat.cat',
    'mbustos.bnm.ics@gencat.cat',
    'sflayeh.bnm.ics@gencat.cat',
    'mquintana@ambitcp.catsalut.net',
    'framospe@gencat.cat',
    'arene.girona.ics@gencat.cat'
    ]

try:
    db = sys.argv[1]
    pf = sys.argv[1][:3]
except:
    sys.exit("no m'has passat arguments")

t = {'eqa_ind': 'M', 'pedia': 'M', 'social': 'T', 'odn': 'O'}    
uba = "%s.exp_ecap_uba" % db
cat = [
    ['%s.exp_ecap_cataleg' % db, pf + 'Cataleg'],
    ['%s.exp_ecap_catalegPare' % db, pf + 'CatalegPare'],
    ['%s.exp_ecap_catalegPunts' % db, pf + 'CatalegPunts'],
    ['%s.exp_ecap_catalegExclosos' % db, pf + 'CatalegExclosos']
    ]
carta = "%s.exp_ecap_carta" % db
doCarta = True

tip = t[db]

if db == 'odn' or db == 'social':
    query = "select a.* from %s a" % uba
    if db == 'odn':
        query_prs = "select * from %s" % uba + "_jail"
else:
    query = """
        select a.*
        from
            %s a inner join nodrizas.cat_centres b
                on a.up = b.scs_codi
        """ % uba
    if db == "eqa_ind":
        query += " union select * from eqa_ind.exp_ecap_uba_proc"
table = pf + "Indicadors"
u.exportPDP(query=query, table=table, dat=True,fmo=False)
if tip == "O":
    table_prs = "prs" + pf + "Indicadors"
    u.exportPDP(query=query_prs, table=table_prs, dat=True,fmo=False)

query = """
    select indicador, sum(resolts)/sum(detectats) resolucio
    from %s where tipus = '%s' and up <> 'UP' group by indicador
    """ % (uba, tip)
table = pf + "IndicadorsICS"
u.exportPDP(query=query, table=table, dat=True,fmo=False)
if tip == "O":
    query_prs = """
        select indicador, sum(resolts)/sum(detectats) resolucio
        from %s where tipus = '%s' and up <> 'UP' group by indicador
        """ % (uba + "_jail", tip)
    table_prs = "prs" + pf + "IndicadorsICS"
    u.exportPDP(query=query_prs, table=table_prs, dat=True,fmo=False)

for r in cat:
    my, ora = r[0], r[1]
    query = "select * from %s" % my
    if my.endswith("exp_ecap_catalegExclosos"):
        u.exportPDP(query=query, table=ora, truncate=True,fmo=False)
    else:
        u.exportPDP(query=query, table=ora, datAny=True,fmo=False)
    if tip == "O":
        my, ora = r[0] + "_jail", "prs" + r[1]
        query_jail = "select * from %s" % my
        if my.endswith("exp_ecap_catalegExclosos_jail"):
            u.exportPDP(query=query_jail, table=ora, truncate=True,fmo=False)
        else:
            u.exportPDP(query=query_jail, table=ora, datAny=True,fmo=False)  

if doCarta:
    query = "select up,uba,tipus,indicador,pond_ind from %s" % carta
    table = pf + "Individualitzat"
    u.exportPDP(query=query, table=table, datAny=True,fmo=False)

if db == "eqa_ind":
    for my, ora in [("exp_ecap_cataleg_proc", "eqaCataleg"),
                    ("exp_ecap_catalegpare_proc", "eqaCatalegPare")]:
        sql = """
            select year(data_ext), a.*
            from eqa_ind.{} a, nodrizas.dextraccio b
        """.format(my)
        u.exportPDP(sql, ora,fmo=False)

    u.exportPDP("select * from eqa_ind.exp_ecap_cataleg_processos",
                "eqaCatalegProcessos",
                datAny=True,fmo=False)

if db == 'eqa_ind':
    text = 'S\'ha carregat l\'EQA a l\'ECAP; pots revisar els resultats a eines.portalics/ladybug/'
    u.sendSISAP(SUBSCRIBERS, 'Càrrega EQA', 'Mireia', text)