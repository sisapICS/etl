# coding: iso-8859-1

from sisapUtils import *
import sisaptools as u
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import datetime as d
import pytz


printTime("Inici")

taules = [('eqa_ind.exp_ecap_uba', 'M', 'EQA adults'),
          ('pedia.exp_ecap_uba', 'M', 'EQA pediatria'),
          ('odn.exp_ecap_uba', 'O', 'EQA Odontologia'),
          ('social.exp_ecap_uba', 'T', 'EQA Treball social'),
          ('ass.exp_khalix_up_ind', 'G', 'EQA assir')]

catalegs = ['eqa_ind.exp_ecap_cataleg', 'pedia.exp_ecap_cataleg', 'odn.exp_ecap_cataleg', 'social.exp_ecap_cataleg', 'ass.exp_ecap_cataleg', 'eqa_ind.exp_ecap_cataleg_jail']

periode,= getOne("select date_format(data_ext,'%Y%m') from dextraccio", 'nodrizas')

class Control_EQA(object):
    """."""

    def __init__(self):
        """."""
        self.indicadors = []
        self.get_territoris()
        self.get_indicadors()
        self.get_dades()
        self.export_data()
        self.set_wekan()

    def get_territoris(self):
        """Obtenim àmbits i sector"""
        
        self.centres = {}
        sql = 'select scs_codi, amb_desc, sector from cat_centres_with_jail'
        for up, amb, sector in getAll(sql, 'nodrizas'):
            self.centres[up] = {'sector': sector, 'ambit': amb}
        sql = "select distinct amb_desc, up_assir from ass_centres"
        for amb, up in getAll(sql, 'nodrizas'):
            if up not in self.centres:
                self.centres[up] = {'sector': None, 'ambit': amb}
            
    def get_indicadors(self):
        """agafem indicadors del catàleg"""
        
        self.cataleg = {}
        for cat in catalegs:
            sql = "select indicador, literal from {} where pantalla = 'GENERAL'".format(cat)
            for ind, literal in getAll(sql, 'nodrizas'):
                self.cataleg[ind] = literal
                self.indicadors.append(ind)

    def get_dades(self):
        """Obtenim resultats dels indicadors per territori"""
        
        self.resultats = Counter()
        for (taula, tip, eqa) in taules:
            if tip in ('O', 'T'):
                sql = "select  up, indicador, detectats, resolts from {} where tipus = '{}' and uba = '0'".format(taula, tip)
            elif tip == 'G':
                sql = "select a.up, a.indicador, den, num from \
                        (select up, indicador, sum(n) den \
                        from {0} where conc = 'DEN' group by up, indicador) a\
                        inner join (select up, indicador, sum(n) num \
                                    from {0} where conc = 'NUM' \
                                    group by up, indicador) b \
                        on a.up = b.up and a.indicador = b.indicador".format(taula)
            else:    
                sql = "select   up, indicador, detectats, resolts from {} where tipus = '{}'".format(taula, tip) 
            for  up, ind, det, res in getAll(sql, 'nodrizas'):
                try:
                    ambit = self.centres[up]['ambit']
                    sector = self.centres[up]['sector']
                except KeyError:
                    continue
                try:
                    desc_ind =self.cataleg[ind]
                except KeyError:
                    continue
                self.resultats[periode, sector, ambit, ind, desc_ind, eqa, 'detectats'] += det
                self.resultats[periode, sector, ambit, ind, desc_ind, eqa, 'resolts'] += res
        # bolet per indicadors EQA jail
        sql = """select up, indicador, conc, sum(n) as N
                from eqa_ind.exp_khalix_up_ind
                where indicador in ('EQD2001A', 'EQD2002A', 'EQA2000A')
                and comb = 'NOINSAT'
                group by indicador, up, conc"""
        for up, ind, tipus, n in getAll(sql, 'eqa_ind'):
            ind = ind[:-1]
            try:
                ambit = self.centres[up]['ambit']
                sector = self.centres[up]['sector']
            except KeyError:
                continue
            try:
                desc_ind =self.cataleg[ind]
            except KeyError:
                continue
            if tipus == 'DEN':
                self.resultats[periode, sector, ambit, ind, desc_ind, 'EQA jail', 'detectats'] += n
            elif tipus == 'NUM':
                self.resultats[periode, sector, ambit, ind, desc_ind, 'EQA jail', 'resolts'] += n

    def export_data(self):
        """."""
        upload = []
        catal = {}

        for (period, sector, ambit, ind, desc_ind, eqa, tipus), n in self.resultats.items():
            if tipus == 'detectats':
                resolts = self.resultats[periode, sector, ambit, ind, desc_ind, eqa, 'resolts']
                upload.append([int(period), sector, ambit, ind, resolts, n])
                catal[ind, desc_ind, eqa] = True
        
        table = 'mst_control_eqa'
        
        indicadors = tuple(self.indicadors)
        sql = "delete a.* from {0} a where periode = {1} and indicador in {2}".format(table, periode, indicadors)
        execute(sql, ('ladybug', 'shiny'))
        u.Database("shiny", "ladybug").list_to_table(upload, table)

        tb_cat = 'cat_control_indicadors'
        upload_cat = []
        for (ind, desc_ind, eqa), dadesc in catal.items():
            upload_cat.append([ind, desc_ind, eqa])

        sql = "delete a.* from {0} a where indicador in {1}".format(tb_cat, indicadors)
        execute(sql, ('ladybug', 'shiny'))
        u.Database("shiny", "ladybug").list_to_table(upload_cat, tb_cat)

    def set_wekan(self):
        """."""
        avui = d.datetime.today()
        card = WekanCard("SISAP", "Carril principal", "Pre", 'Revisar ladybug EQA ' + avui.strftime("%d/%m/%Y"))  # noqa
        dext = getOne("select data_ext from dextraccio", "nodrizas")[0].strftime("%d/%m/%Y")  # noqa
        card.add_description("Data de càlcul: " + dext)
        card.add_members("Souhel")
        card.add_labels("Definició")
        dia = avui + d.timedelta(days=2)
        dia_hora = d.datetime(dia.year, dia.month, dia.day, 8, 0, 0, 0)
        amb_tz = pytz.timezone('Europe/Madrid').localize(dia_hora)
        card.add_date(amb_tz)
        checklist = "Tasques"
        card.add_to_checklist(checklist, "Revisar ladybug: http://eines.portalics/ladybug/")
        card.add_to_checklist(checklist, "Comunicar incidencies")
        card.add_to_checklist(checklist, "Tancar targeta")
        card.add_peticionari("ICS")
        card.add_comment("@Souhel targeta nova")


if __name__ == "__main__":
    Control_EQA()  

printTime("Fi")