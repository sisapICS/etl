# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime

# Definici� de l'indicador
INDICADOR = "EQA9212"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula la data fa un any.

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys90dies': Fa un any des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys90dies

    sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
            date_add(data_ext, INTERVAL -90 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys90dies = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� assignada al servei ASSIR en edats fora del programa de cribratge mamogr�fic (menors de 50 anys o majors o igual a 70 anys).

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les dones assignades fora de l'edat de cribratge.
    """
    global poblacio_ass

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_poblacio
        WHERE
            (edat < 50) OR (edat >= 70)
    """
    poblacio_ass = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_denominador():
    """
    Calcula el denominador de l'indicador, que inclou:
    1. Dones fora del programa de cribratge que han fet una mamografia en l'�ltim any.

    Defineix:
        - 'denominador': Diccionari amb identificadors i dates de mamografies fora del programa.
    """
    global denominador
    denominador = dict()

    sql = """
            SELECT
                id_cip_sec,
                dat
            FROM
                ass_proves
            WHERE
                agrupador = 394 -- Agrupador per a mamografies
                AND dat BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
          """.format(_data_ext_menys1any=data_ext_menys1any,_data_ext=data_ext)
    for id_cip_sec, data_mamografia in u.getAll(sql, "nodrizas"):
        if id_cip_sec in poblacio_ass:
            denominador.setdefault(id_cip_sec, data_mamografia)
            if denominador[id_cip_sec] < data_mamografia:
                denominador[id_cip_sec] = data_mamografia

def get_exclusions():
    """
    Calcula les exclusions basades en diagn�stics espec�fics:
    - Dones amb diagn�stic de c�ncer de mama.
    - Dones amb embar�s actiu o puerperi obert en els �ltims 90 dies.

    Defineix:
        - 'exclusions': Conjunt d'identificadors que compleixen criteris d'exclusi�.
    """
    global exclusions
    exclusions = set()

    sqls = (
        """
            SELECT
                id_cip_sec
            FROM
                eqa_problemes
            WHERE
                ps = 560 -- Diagn�stic de c�ncer de mama
        """,
        """
            SELECT
                id_cip_sec
            FROM
                ass_embaras
            WHERE
                fi = DATE'{_data_ext}' 
                OR (puerperi = 1 AND fi >= DATE'{_data_ext_menys90dies}') -- Embar�s actiu o puerperi obert en els �ltims 90 dies
        """.format(_data_ext=data_ext,_data_ext_menys90dies=data_ext_menys90dies)
    )
    for sql in sqls:
        exclusions.update({id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in denominador})

def get_numerador():
    """
    Calcula el numerador de l'indicador, que inclou:
    1. Dones amb un criteri cl�nic justificatiu per a la mamografia fora del programa de cribratge.

    Defineix:
        - 'numerador': Conjunt d'identificadors amb criteri cl�nic justificatiu.
    """
    global numerador
    numerador = set()

    # Verifica criteris cl�nics en les mamografies realitzades
    sql = """
        SELECT
            id_cip_sec,
            dat
        FROM
            ass_variables
        WHERE
            agrupador IN (395, 396) -- Agrupadors d'�ltima mamografia i de risc c�ncer mama
            AND (val_num > 0 OR LEFT(val_txt, 5) <> 'Sense') -- Valor num�ric o text diferent de 'Sense'
    """
    numerador.update({id_cip_sec for id_cip_sec, data_risc_cancer in u.getAll(sql, "nodrizas") if id_cip_sec in denominador and data_risc_cancer <= denominador[id_cip_sec]})

    sql = """
        SELECT
            id_cip_sec,
            dde
        FROM
            nodrizas.eqa_problemes
        WHERE
            ps = 411 -- Agrupador d'antecedent c�ncer mama
    """
    numerador.update({id_cip_sec for id_cip_sec, data_risc_cancer in u.getAll(sql, "nodrizas") if id_cip_sec in denominador and data_risc_cancer <= denominador[id_cip_sec]})

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [(id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 1 if id_cip_sec in exclusions else 0) for id_cip_sec in denominador]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_exclusions();           print("get_exclusions(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))