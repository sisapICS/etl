# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime
from dateutil.relativedelta import relativedelta

# Definici� de l'indicador
INDICADOR = "EQA9209"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants.

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys2anys': Fa dos anys des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys2anys

    sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL - 1 YEAR), INTERVAL + 1 DAY),
            date_add(date_add(data_ext, INTERVAL - 2 YEAR), INTERVAL + 1 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys2anys = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� assignada al servei ASSIR.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les persones assignades al servei ASSIR.
    """
    global poblacio_ass

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_poblacio
    """
    poblacio_ass = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_embarasos():
    """
    Obt� informaci� sobre els embarassos actius durant el per�ode definit.

    Defineix:
        - 'embarasos': Diccionari amb les dates d'inici i fi de cada embar�s per cada 'id_cip_sec'.
    """
    global embarasos

    sql = """
        SELECT
            id_cip_sec,
            inici,
            fi
        FROM
            ass_embaras
        WHERE
            (inici BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
             OR fi BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}')
            AND temps > 154 -- Temps en dies (A partir de les 22 setmanes + 1 dia --> Inici setmana 23)
    """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    embarasos = {id_cip_sec: {"inici": inici, "fi": fi} for id_cip_sec, inici, fi in u.getAll(sql, "nodrizas")}

def get_denominador():
    """
    Calcula el denominador de l'indicador.

    Defineix:
        - 'denominador': Conjunt d'identificadors de dones embarassades assignades al servei ASSIR.
    """
    global denominador

    denominador = {id_cip_sec for id_cip_sec in embarasos if id_cip_sec in poblacio_ass}

def get_numerador():
    """
    Calcula el numerador de l'indicador, que inclou:
    1. Dones amb control de pressi� arterial correcte (<140/90 mmHg) durant el segon trimestre de l'embar�s.

    Defineix:
        - 'numerador': Conjunt d'identificadors de dones amb pressi� arterial controlada en el segon trimestre.
    """
    global numerador
    numerador = set()
    dates_ultima_PA = c.defaultdict(set)
    registres_PA = c.defaultdict(dict)

    sql = """
        SELECT
            id_cip_sec,
            agrupador,
            dat,
            val_num
        FROM
            ass_variables
        WHERE
            ((agrupador = 16 AND val_num BETWEEN 0 AND 140)     -- Pressi� sist�lica
             OR (agrupador = 17 AND val_num BETWEEN 0 AND 90))  -- Pressi� diast�lica
            AND dat BETWEEN DATE'{_data_ext_menys2anys}' AND DATE'{_data_ext}'
    """.format(_data_ext_menys2anys=data_ext_menys2anys, _data_ext=data_ext)

    # Comptabilitzar registres v�lids de pressi� arterial
    for id_cip_sec, agrupador, data_vs, valor in u.getAll(sql, "nodrizas"):
        if id_cip_sec in denominador and embarasos[id_cip_sec]["inici"] + relativedelta(days=22*7+1) <= data_vs <= embarasos[id_cip_sec]["inici"] + relativedelta(days=31*7):
            dates_ultima_PA.setdefault(id_cip_sec, data_vs)
            if dates_ultima_PA[id_cip_sec] == data_vs:
                registres_PA[id_cip_sec][agrupador] = valor
            elif dates_ultima_PA[id_cip_sec] > data_vs:
                dates_ultima_PA[id_cip_sec] = data_vs
                registres_PA[id_cip_sec] = {agrupador: valor}

    # Incloure nom�s aquells que tenen tant pressi� sist�lica com diast�lica controlades
    for id_cip_sec, registre_PA in registres_PA.items():
        if len(registre_PA) == 2:
            if registre_PA[16] < 140 and registre_PA[17] < 90:
                numerador.add(id_cip_sec)

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [(id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 0) for id_cip_sec in denominador]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_embarasos();            print("get_embarasos(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))