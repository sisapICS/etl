# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime
from dateutil.relativedelta import relativedelta

INDICADOR = "EQA9230"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants:

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys2anys': Fa dos anys des de la data d'extracci�.
        - 'data_ext_menys3mesos': Fa 3 mesos des de la data d'extracci�.
    """
    global data_ext, data_ext_menys2anys, data_ext_menys3mesos

    sql = """
            SELECT
                data_ext,
                DATE_ADD(DATE_ADD(data_ext, INTERVAL -2 YEAR), INTERVAL 1 DAY),
                DATE_ADD(data_ext, INTERVAL -3 MONTH)
            FROM
                dextraccio
    """
    data_ext, data_ext_menys2anys, data_ext_menys3mesos = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� assignada al servei ASSIR.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les dones d'entre 25 i 65 anys assignades al servei ASSIR.
    """
    global poblacio_ass

    sql = """
            SELECT
                id_cip_sec
            FROM
                ass_poblacio
            WHERE
                edat BETWEEN 25 AND 65
          """
    poblacio_ass = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def sub_get_problemes(sql):
    """
    Subfunci� per obtenir problemes espec�fics mitjan�ant consultes SQL.

    Retorna:
        - Conjunt d'identificadors amb els problemes trobats.
    """
    sub_problemes = {(id_cip_sec, data) for id_cip_sec, data in u.getAll(sql, "import")}
    return sub_problemes

def get_neoplasies_o_cancer_cervix():
    """
    Obt� informaci� sobre les dones amb diagn�stic de neopl�sia intracervical avan�ada o c�ncer de c�rvix en els darrers dos anys.

    Defineix:
        - 'neoplasies_o_cancer_cervix': Diccionari amb informaci� de diagn�stic per cada 'id_cip_sec'.
    """
    global neoplasies_o_cancer_cervix
    neoplasies_o_cancer_cervix = dict()

    sql = """
            SELECT
                id_cip_sec,
                pr_dde
            FROM
                {_tb_name}
            WHERE
                pr_cod_ps in ('C53.0', 'C53.1', 'C53.8', 'C53.9', 'D06.0', 'D06.1', 'D06.8', 'D06.9', 'N87.1', 'C01-C53.0', 'C01-C53.1', 'C01-C53.8', 'C01-C53.9', 'C01-D06.0', 'C01-D06.1', 'C01-D06.8', 'C01-D06.9', 'C01-N87.1')
                AND pr_dde BETWEEN DATE'{_data_ext_menys2anys}' AND DATE'{_data_ext_menys3mesos}'
          """
    jobs = [sql.format(_tb_name=tb_name, _data_ext_menys2anys=data_ext_menys2anys, _data_ext_menys3mesos=data_ext_menys3mesos) for tb_name in u.getSubTables("problemes") if tb_name[-6:] != '_s6951']
    for sub_problemes in u.multiprocess(sub_get_problemes, jobs, 4):
        for id_cip_sec, data in sub_problemes:
            neoplasies_o_cancer_cervix.setdefault(id_cip_sec, data)
            if neoplasies_o_cancer_cervix[id_cip_sec] < data:
                neoplasies_o_cancer_cervix[id_cip_sec] = data

    sql = """
            SELECT
                id_cip_sec,
                dat
            FROM
                ass_variables
            WHERE
                ((agrupador = 400 AND val_num IN (8, 11, 12, 13, 98))
                OR (agrupador =  897 AND val_num IN (3, 4, 5, 10, 11, 12, 13, 14)))
                AND dat BETWEEN DATE'{_data_ext_menys2anys}' AND DATE'{_data_ext_menys3mesos}'
          """.format(_data_ext_menys2anys=data_ext_menys2anys,_data_ext_menys3mesos=data_ext_menys3mesos)
    for id_cip_sec, data in u.getAll(sql, "nodrizas"):
        neoplasies_o_cancer_cervix.setdefault(id_cip_sec, data)
        if neoplasies_o_cancer_cervix[id_cip_sec] < data:
            neoplasies_o_cancer_cervix[id_cip_sec] = data

def get_denominador():
    """
    Calcula el denominador de l'indicador:

    Defineix:
        - 'denominador': Conjunt d'identificadors de les dones amb diagn�stic de neopl�sia intracervical avan�ada o c�ncer de c�rvix.
    """
    global denominador

    denominador = {id_cip_sec for id_cip_sec in neoplasies_o_cancer_cervix if id_cip_sec in poblacio_ass}

def get_exclusions():
    """
    Calcula les exclusions per a l'indicador:

    Defineix:
        - 'exclusions': Conjunt d'identificadors de dones amb al�l�rgia a la vacuna del papil�loma hum�.
    """
    global exclusions

    sql = """
            SELECT
                id_cip_sec
            FROM
                ram
            WHERE 
                ram_atc LIKE 'J07BM%'
            """
    exclusions = {id_cip_sec for id_cip_sec, in u.getAll(sql, "import") if id_cip_sec in denominador}

def get_numerador():
    """
    Calcula el numerador de l'indicador:

    Defineix:
        - 'numerador': Conjunt d'identificadors ('id_cip_sec') de dones correctament vacunades contra el papil�loma hum� segons el protocol establert.
    """
    global numerador
    numerador = set()
    neoplasies_o_cancer_cervix_vacunades_VPH = c.defaultdict(set)

    sql = """
            SELECT
                id_cip_sec,
                va_u_data_vac
            FROM
                vacunes
            WHERE
                va_u_cod IN ('P00122', 'P00123', 'P00124', 'P00125', 'P00126', 'P00185', 'P00193', 'P00231', 'P00232', 'P00233', 'P00234', 'P00235', 'P00236', 'P00266', 'P00267', 'VPH2', 'VPH2-S', 'VPH4', 'VPH4-S')
                AND va_u_data_vac <= DATE'{_data_ext}'
          """.format(_data_ext=data_ext)
    for id_cip_sec, data_vacunacio in u.getAll(sql, "import"):
        if id_cip_sec in denominador:
            neoplasies_o_cancer_cervix_vacunades_VPH[id_cip_sec].add(data_vacunacio)

    for id_cip_sec, dates_vacunacio in neoplasies_o_cancer_cervix_vacunades_VPH.items():
        dosis = {i + 1: data_vacunacio for i, data_vacunacio in enumerate(sorted(dates_vacunacio))}
        data_primera_vacunacio = dosis.get(1)
        data_segona_vacunacio = dosis.get(2)
        data_diagnostic = neoplasies_o_cancer_cervix[id_cip_sec]

        if 3 in dosis:
            numerador.add(id_cip_sec)
        elif 2 in dosis:
            if 0 <= u.monthsBetween(data_segona_vacunacio, data_ext) < 7 or (5 <= u.monthsBetween(data_primera_vacunacio, data_segona_vacunacio) and data_segona_vacunacio < data_diagnostic):
                numerador.add(id_cip_sec)
        elif 1 in dosis:
            if 0 <= u.monthsBetween(data_primera_vacunacio, data_ext) < 3:
                numerador.add(id_cip_sec)

def create_resultats():
    """
    Genera els resultats finals per a l'indicador:

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, exclusi�).
    """
    global resultats
    resultats = [(id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 1 if id_cip_sec in exclusions else 0) for id_cip_sec in denominador]

def export_resultats():
    """
    Exporta els resultats a la base de dades:

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();                       print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();                     print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_neoplasies_o_cancer_cervix();       print("get_neoplasies_o_cancer_cervix(): {}".format(datetime.datetime.now()))
    get_denominador();                      print("get_denominador(): {}".format(datetime.datetime.now()))
    get_exclusions();                       print("get_exclusions(): {}".format(datetime.datetime.now()))
    get_numerador();                        print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();                     print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();                     print("export_resultats(): {}".format(datetime.datetime.now()))