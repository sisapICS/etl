# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime
from dateutil.relativedelta import relativedelta

INDICADOR = "EQA9220"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants.
    
    Defineix:
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys2anys': Fa dos anys des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys2anys

    sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL +1 DAY),
            date_add(date_add(data_ext, INTERVAL -2 YEAR), INTERVAL +1 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys2anys = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� assignada al servei ASSIR.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les dones assignades.
    """
    global poblacio_ass

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_poblacio
    """
    poblacio_ass = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_embarasos():
    """
    Obt� informaci� sobre els embarassos actius que hagin superat les 38 setmanes de gestaci� completes.

    Defineix:
        - 'embarasos': Diccionari amb les dates d'inici i fi de l'embar�s per a cada 'id_cip_sec'.
    """
    global embarasos

    sql = """
        SELECT
            id_cip_sec,
            inici,
            fi
        FROM
            ass_embaras
        WHERE
            (inici BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
             OR fi BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}')
            AND temps > 266 -- Temps en dies (A partir de les 38 setmanes + 1 dia --> Inici setmana 39)
    """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    embarasos = {id_cip_sec: {"inici": inici, "fi": fi} for id_cip_sec, inici, fi in u.getAll(sql, "nodrizas")}

def get_denominador():
    """
    Calcula el denominador de l'indicador:
    - Dones embarassades que han arribat a la setmana 38 de gestaci� i han tingut una visita a partir de la setmana 27.

    Defineix:
        - 'denominador': Conjunt d'identificadors de les dones que compleixen els criteris per al denominador.
    """
    global denominador

    sql = """
        SELECT
            id_cip_sec
        FROM
            nodrizas.ass_visites_embaras
        WHERE
            dies > 182 -- Temps en dies (A partir de les 26 setmanes + 1 dia --> Inici setmana 27)
    """
    denominador = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in embarasos and id_cip_sec in poblacio_ass}

def get_numerador():
    """
    Calcula el numerador de l'indicador:
    - Dones que han rebut la vacuna dTPa (dift�ria-t�tanus-tosferina acel�lular) durant l'embar�s.

    Defineix:
        - 'numerador': Conjunt d'identificadors de dones que han estat vacunades correctament amb la vacuna dTPa durant l'embar�s.
    """
    global numerador
    numerador = set()

    sql = """
        SELECT
            id_cip_sec,
            datamax
        FROM
            nodrizas.eqa_vacunes
        WHERE
            agrupador = 631 -- Vacuna dTPa
            AND datamax BETWEEN DATE'{_data_ext_menys2anys}' AND DATE'{_data_ext}'
    """.format(_data_ext_menys2anys=data_ext_menys2anys, _data_ext=data_ext)

    numerador = {
        id_cip_sec for id_cip_sec, data_vac in u.getAll(sql, "nodrizas")
        if id_cip_sec in denominador and embarasos[id_cip_sec]["inici"] <= data_vac <= embarasos[id_cip_sec]["fi"]
    }

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [(id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 0) for id_cip_sec in denominador]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Defineix la taula de resultats i l'insereix a la base de dades especificada.
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_embarasos();            print("get_embarasos(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))