# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime
from dateutil.relativedelta import relativedelta

INDICADOR = "EQA9227B"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants:

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys22mesos': Fa 22 mesos des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys22mesos

    sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
            date_add(date_add(data_ext, INTERVAL -22 MONTH), INTERVAL + 1 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys22mesos = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� assignada al servei ASSIR.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les dones assignades al servei ASSIR.
    """
    global poblacio_ass

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_poblacio
    """
    poblacio_ass = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_embarasos():
    """
    Obt� informaci� sobre els embarassos actius en l'�ltim any.

    Defineix:
        - 'embarasos': Diccionari amb informaci� sobre inici, fi i setmanes de l'embar�s per cada 'id_cip_sec'.
    """
    global embarasos

    sql = """
        SELECT
            id_cip_sec,
            inici,
            fi,
            TRUNCATE(temps / 7, 0) + 1 AS setmanes
        FROM
            ass_embaras
        WHERE
            ((inici BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}')
            OR (fi BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'))
            AND temps >= 150
    """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    embarasos = {
        id_cip_sec: {"inici": inici, "fi": fi, "setmanes": setmanes}
        for id_cip_sec, inici, fi, setmanes in u.getAll(sql, "nodrizas")
        if id_cip_sec in poblacio_ass
    }

def get_cribratges():
    """
    Recull els resultats dels q�estionaris Whooley i EPDS.

    Defineix:
        - 'cribatges': Diccionari amb informaci� sobre els q�estionaris realitzats per cada 'id_cip_sec'.
        - Inclou informaci� de Whooley (abans i despr�s de la setmana 27) i EPDS (abans de la setmana 21 i despr�s).
    """
    global cribatges
    cribatges = c.defaultdict(lambda: {"whooley": dict(), "epds": dict(), "whooley_abans_20": dict(), "whooley_despres_27": dict(), "epds_abans_21": dict(), "epds_abans_27": dict(), "epds_despres_27": dict()})

    sql_whooley = """
            SELECT
                id_cip_sec,
                val_num,
                dat
            FROM
                ass_variables
            WHERE
                agrupador = 860 -- Q�estionari Whooley
                AND dat BETWEEN DATE'{_data_ext_menys22mesos}' AND DATE'{_data_ext}'
          """.format(_data_ext_menys22mesos=data_ext_menys22mesos, _data_ext=data_ext)
    for id_cip_sec, valor, data_whooley in u.getAll(sql_whooley, "nodrizas"):
        if id_cip_sec in embarasos:
            data_inici_embaras = embarasos[id_cip_sec]["inici"]
            data_fi_embaras = embarasos[id_cip_sec]["fi"]
            if data_inici_embaras <= data_whooley < data_fi_embaras:
                setmanes = int(u.daysBetween(data_inici_embaras, data_whooley) / 7) + 1
                cribatges[id_cip_sec]["whooley"][data_whooley] = valor
                if setmanes < 20:
                    cribatges[id_cip_sec]["whooley_abans_20"][data_whooley] = valor
                elif setmanes >= 28:
                    cribatges[id_cip_sec]["whooley_despres_27"][data_whooley] = valor

    sql_epds = """
            SELECT
                id_cip_sec,
                val_num,
                dat
            FROM
                ass_variables
            WHERE
                agrupador = 387 -- Q�estionari EPDS
                AND dat BETWEEN DATE'{_data_ext_menys22mesos}' AND DATE'{_data_ext}'
          """.format(_data_ext_menys22mesos=data_ext_menys22mesos, _data_ext=data_ext)
    for id_cip_sec, valor, data_epds in u.getAll(sql_epds, "nodrizas"):
        if id_cip_sec in embarasos:
            data_inici_embaras = embarasos[id_cip_sec]["inici"]
            data_fi_embaras = embarasos[id_cip_sec]["fi"]
            if data_inici_embaras <= data_epds < data_fi_embaras:
                setmanes = int(u.daysBetween(data_inici_embaras, data_epds) / 7) + 1
                cribatges[id_cip_sec]["epds"][data_epds] = valor
                if setmanes < 21:
                    cribatges[id_cip_sec]["epds_abans_21"][data_epds] = valor
                if setmanes < 27:
                    cribatges[id_cip_sec]["epds_abans_27"][data_epds] = valor
                elif setmanes >= 28:
                    cribatges[id_cip_sec]["epds_despres_27"][data_epds] = valor

def get_denominador():
    """
    Calcula el denominador de l'indicador.

    Defineix:
        - 'denominador': Conjunt d'identificadors de les dones embarassades assignades al servei ASSIR amb almenys un test de Whooley positiu.
    """
    global denominador; denominador = set()

    for id_cip_sec in embarasos:
        if cribatges[id_cip_sec]["whooley"]:
            if any(valor_whooley == 1 for _, valor_whooley in cribatges[id_cip_sec]["whooley_abans_20"].items()) \
               or (embarasos[id_cip_sec]["setmanes"] >= 28 and any(valor_whooley == 1 for _, valor_whooley in cribatges[id_cip_sec]["whooley_despres_27"].items())):
                denominador.add(id_cip_sec)

def get_exclusions():
    """
    Calcula les exclusions per a l'indicador. Dones no visitades abans dels 150 dies d'embaras, no visitades durant l'embaras o uan el primer test de Whooley realitzat a l'embar�s sigui negatiu.

    Defineix:
        - 'exclusions': Conjunt d'identificadors de les dones que no compleixen els criteris per ser incloses al numerador.
    """
    global exclusions; exclusions = set()
    visites_embaras = c.defaultdict(set)

    sql = """
        SELECT
            id_cip_sec,
            dies
        FROM
            ass_visites_embaras
    """
    for id_cip_sec, dies in u.getAll(sql, "nodrizas"):
        visites_embaras[id_cip_sec].add(dies)

    exclusions = {id_cip_sec for id_cip_sec in denominador if not visites_embaras[id_cip_sec] or not any(dies < 150 for dies in visites_embaras[id_cip_sec])}

    for id_cip_sec in cribatges:
        if cribatges[id_cip_sec]["whooley"]:
            primera_data_whooley = min(cribatges[id_cip_sec]["whooley"])
            primer_valor_whooley = cribatges[id_cip_sec]["whooley"][primera_data_whooley]
            if primer_valor_whooley == 0:
                exclusions.add(id_cip_sec)

def get_numerador():
    """
    Calcula el numerador de l'indicador.

    Defineix:
        - 'numerador': Conjunt d'identificadors ('id_cip_sec') de dones que han completat tots els q�estionaris requerits despr�s d'un resultat positiu del test de Whooley.
    """
    global numerador; numerador = set()
    
    for id_cip_sec in denominador:
        condicions_num = 0
        if any(valor_whooley == 1 for _, valor_whooley in cribatges[id_cip_sec]["whooley_abans_20"].items()):
            if cribatges[id_cip_sec]["whooley_abans_20"] and cribatges[id_cip_sec]["epds_abans_21"]:
                for data_whooley, valor_whooley in cribatges[id_cip_sec]["whooley_abans_20"].items():                     
                    if valor_whooley == 1:
                        for data_EPDS, valor_EPDS in cribatges[id_cip_sec]["epds_abans_21"].items():
                            if data_whooley <= data_EPDS:
                                condicions_num += 1
        else:
            condicions_num += 1
        if embarasos[id_cip_sec]["setmanes"] >= 28 and any(valor_whooley == 1 for _, valor_whooley in cribatges[id_cip_sec]["whooley_despres_27"].items()):
            if cribatges[id_cip_sec]["whooley_despres_27"] and cribatges[id_cip_sec]["epds_despres_27"]:
                for data_whooley, valor_whooley in cribatges[id_cip_sec]["whooley_despres_27"].items():                     
                    if valor_whooley == 1:
                        for data_EPDS, valor_EPDS in cribatges[id_cip_sec]["epds_despres_27"].items():
                            if data_whooley <= data_EPDS:
                                condicions_num += 1
        else:
            condicions_num += 1

        if condicions_num == 2:
            numerador.add(id_cip_sec)

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [(id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 1 if id_cip_sec in exclusions else 0) for id_cip_sec in denominador]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_embarasos();            print("get_embarasos(): {}".format(datetime.datetime.now()))
    get_cribratges();           print("get_cribratges(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_exclusions();           print("get_exclusions(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))