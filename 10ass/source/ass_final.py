# -*- coding: latin1 -*-

import sisapUtils as u
import collections as col
import datetime
from dateutil.relativedelta import relativedelta
import csv

# Per old_is_the_new_fashion
from sisapUtils import *
import csv,os
from time import strftime
import os
import itertools

indicadors_minim_3omes_visites = ("EQA9201", "EQA9202", "EQA9204", "EQA9205", "EQA9206", "EQA9207", "EQA9207B", "EQA9208", "EQA9209", "EQA9221")

def get_ass_centres():

    global ass_centres

    sql = """
            SELECT
                up
            FROM
                ass_centres
          """
    ass_centres = {up for up, in u.getAll(sql, "nodrizas")}

def get_ass_poblacio():

    global ass_poblacio, ass_poblacio_3omes, conv_u11

    sql = """
            SELECT
                id_cip_sec,
                edat
            FROM
                ass_poblacio
          """
    ass_poblacio = {id_cip_sec: int(edat) for id_cip_sec, edat in u.getAll(sql, "nodrizas")}

    sql = """
            SELECT
                id_cip_sec,
                edat
            FROM
                ass_poblacio_3omes
          """
    ass_poblacio_3omes = {id_cip_sec: int(edat) for id_cip_sec, edat in u.getAll(sql, "nodrizas")}

    sql = """
            SELECT
                id_cip_sec,
                codi_sector,
                hash_d
            FROM
                u11
          """
    conv_u11 = {id_cip_sec: (codi_sector, hash_d) for id_cip_sec, codi_sector, hash_d in u.getAll(sql, "import") if id_cip_sec in ass_poblacio}

def get_ass_poblacio_imputacio_dni():
    global ass_poblacio_imputacio_dni;          ass_poblacio_imputacio_dni = col.defaultdict(set)
    global ass_poblacio_imputacio_dni_3omes;    ass_poblacio_imputacio_dni_3omes = col.defaultdict(set)
    global ass_sense_gine;                      ass_sense_gine = ass_centres.copy()
    counter_ass_amb_gine = col.Counter()

    sql = """
            SELECT
                id_cip_sec,
                visi_up up,
                visi_dni_prov_resp dni,
                tipus
            FROM
                ass_imputacio_dni
          """
    for id_cip_sec, up, dni, tipus in u.getAll(sql, "nodrizas"):
        if up in ass_centres and tipus == "G":
            counter_ass_amb_gine[up] += 1
    for up, n_pacients in counter_ass_amb_gine.items():
        if n_pacients >= 300:
            ass_sense_gine.discard(up)

    for id_cip_sec, up, dni, tipus in u.getAll(sql, "nodrizas"):
        tipus = tipus if not (tipus == "L" and up in ass_sense_gine) else "E"
        ass_poblacio_imputacio_dni[id_cip_sec].add((up, dni, tipus))
        if id_cip_sec in ass_poblacio_3omes:
            ass_poblacio_imputacio_dni_3omes[id_cip_sec].add((up, dni, tipus))

def get_ass_poblacio_imputacio_up_assir():

    global ass_poblacio_imputacio_up;               ass_poblacio_imputacio_up = col.defaultdict(set)
    global ass_poblacio_imputacio_up_3omes;         ass_poblacio_imputacio_up_3omes = col.defaultdict(set)
    global ass_poblacio_imputacio_up_assir;         ass_poblacio_imputacio_up_assir = col.defaultdict(set)
    global ass_poblacio_imputacio_up_assir_3omes;   ass_poblacio_imputacio_up_assir_3omes = col.defaultdict(set)
    
    sql = """
            SELECT
                id_cip_sec,
                visi_up 
            FROM
                ass_imputacio_up
          """
    for id_cip_sec, up in u.getAll(sql, "nodrizas"):
        ass_poblacio_imputacio_up[id_cip_sec].add(up)
        if id_cip_sec in ass_poblacio_3omes:
            ass_poblacio_imputacio_up_3omes[id_cip_sec].add(up)

    sql = """
            SELECT
                id_cip_sec,
                visi_up_assir 
            FROM
                ass_imputacio_up_assir
          """
    for id_cip_sec, up_assir in u.getAll(sql, "nodrizas"):
        ass_poblacio_imputacio_up_assir[id_cip_sec].add(up_assir)
        if id_cip_sec in ass_poblacio_3omes:
            ass_poblacio_imputacio_up_assir_3omes[id_cip_sec].add(up_assir)

def get_txt():

    path = "./dades_noesb/ass_ind.txt"

    global txt;                             txt = dict()
    global indicadors_inversos;             indicadors_inversos = set()
    global indicadors_llistats;             indicadors_llistats = set()

    with open(path, 'rb') as file:
        d=csv.reader(file, delimiter='@', quotechar='|')
        for _, indicador, ind_desc, ind_grup, grup_desc, baixa, dbaixa, llistat, ind_grup_ordre, grup_ordre, invers in d:
            txt[indicador] = (ind_desc, ind_grup, grup_desc, baixa, dbaixa, llistat, ind_grup_ordre, grup_ordre, invers)
            if int(invers):
                indicadors_inversos.add(indicador)
            if int(llistat):
                indicadors_llistats.add(indicador)

def get_mst_indicadors():

    global mst_indicadors_rows;                     mst_indicadors_rows = list()
    global exp_ecap_pacient_rows;                   exp_ecap_pacient_rows = list()
    global exp_khalix_up_ind_counter;               exp_khalix_up_ind_counter = col.Counter()

    sql = """
            SELECT
                id_cip_sec,
                den,
                excl,
                num
            FROM
                {_tb_indicador}
          """
    for indicador in txt:
        try:
            tb_indicador = "ass_indicador_{_indicador}".format(_indicador=indicador)
            indicador_invers = 1 if indicador in indicadors_inversos else 0
            indicador_llistat = 1 if indicador in indicadors_llistats else 0
            for id_cip_sec, den, excl, num in u.getAll(sql.format(_tb_indicador=tb_indicador), "ass"):
                edat = ass_poblacio[id_cip_sec]
                pacient_llistat = 1 if indicador_llistat and (num + indicador_invers) in (0, 2) else 0
                if (indicador not in indicadors_minim_3omes_visites and id_cip_sec in ass_poblacio_imputacio_dni) or (indicador in indicadors_minim_3omes_visites and id_cip_sec in ass_poblacio_imputacio_dni_3omes):
                    for up, dni, tipus in ass_poblacio_imputacio_dni[id_cip_sec]:
                        mst_indicadors_rows.append((id_cip_sec, indicador, up, dni, tipus, edat, "D", num, den, excl, pacient_llistat))
                if (indicador not in indicadors_minim_3omes_visites and id_cip_sec in ass_poblacio_imputacio_up_assir) or (indicador in indicadors_minim_3omes_visites and id_cip_sec in ass_poblacio_imputacio_up_3omes) and not excl:
                    for up_assir in ass_poblacio_imputacio_up_assir[id_cip_sec]:
                        edat_lv = u.ageConverter(edat)
                        exp_khalix_up_ind_counter[(up_assir, edat_lv, "DONA", "NOINSAT", indicador, "DEN")] += den
                        exp_khalix_up_ind_counter[(up_assir, edat_lv, "DONA", "NOINSAT", indicador, "NUM")] += num
                
                if indicador_llistat and pacient_llistat:
                    if (indicador not in indicadors_minim_3omes_visites and id_cip_sec in ass_poblacio_imputacio_up) or (indicador in indicadors_minim_3omes_visites and id_cip_sec in ass_poblacio_imputacio_up_3omes) and not excl:
                         for up, dni, tipus in ass_poblacio_imputacio_dni[id_cip_sec]:
                             codi_sector, hash_d = conv_u11.get(id_cip_sec, (None, None))
                             if hash_d:
                                  exp_ecap_pacient_rows.append((id_cip_sec, up, dni, tipus, indicador, excl, hash_d, codi_sector))
        except:
            next

    exp_khalix_up_ind_rows = [k + (v,) for k, v in exp_khalix_up_ind_counter.items()]

    tb_mst = "mst_indicadors_pacient"
    col_mst = "(id_cip_sec double null, indicador varchar(10) not null default'', up varchar(5) not null default'', dni varchar(10) not null default'', tipus varchar(1) not null default'', edat double, sexe varchar(1) not null default'', num double, den double, excl double, llistat double)"
    u.createTable(tb_mst, col_mst, "ass", rm=1)
    u.listToTable(mst_indicadors_rows, tb_mst, "ass")

    tb_ecap_pacient = "exp_ecap_pacient"
    col_ecap_pacient = "(id_cip_sec double, up varchar(5) not null default'', dni varchar(10) not null default'', tipus varchar(1) not null default'', indicador varchar(10) not null default'', exclos int, hash_d varchar(40) not null default '', sector varchar(4) not null default '')"
    u.createTable(tb_ecap_pacient, col_ecap_pacient, "ass", rm=1)
    u.listToTable(exp_ecap_pacient_rows, tb_ecap_pacient, "ass")

    tb_khalix_pacient = "exp_khalix_up_ind"
    col_ecap_pacient = "(up varchar(5) not null default'', edat varchar(8) not null default '', sexe varchar(4) not null default '', comb varchar(8) not null default '', indicador varchar(10) not null default'', conc varchar(6) not null default '', n double)"
    u.createTable(tb_khalix_pacient, col_ecap_pacient, "ass", rm=1)
    u.listToTable(exp_khalix_up_ind_rows, tb_khalix_pacient, "ass")

def old_is_the_new_fashion():

    print strftime("%Y-%m-%d %H:%M:%S")

    db="ass"
    conn = connect((db,'aux'))
    c_cursor=conn.cursor()

    nod = 'nodrizas'

    path="./dades_noesb/ass_relacio.txt"
    pathc="./dades_noesb/ass_ind.txt"
    pathmetes="./dades_noesb/eqassir_metesres_subind_anual.txt"
    pathpond="./dades_noesb/eqassir_ponderacio_anual.txt"

    OutFile = tempFolder + 'excloureEmb.txt'
    TableMyExc = "ass_exclusions_gine"

    pac1 = "indicadors_pacient1"
    pac = "mst_indicadors_pacient"
    den="indicador"
    embaras="ass_embaras"
    puerperi="ass_puerperi"
    poblacio="nodrizas.ass_imputacio_dni"
    poblacioup="nodrizas.ass_imputacio_up_assir"
    assignada="nodrizas.ass_poblacio"
    edats_k = "nodrizas.khx_edats5a"
    u11 = "mst_u11"
    u11nod = "nodrizas.eqa_u11"
    centres="nodrizas.ass_centres"

    up1 = "eqa_khalix_up_pre1"
    up2 = "eqa_khalix_up_pre2"
    up = "eqa_khalix_up_pre"
    pobk = "eqa_poblacio_khx"
    khalix_pre = "eqa_khalix_up_ind"
    khalix = "exp_khalix_up_ind"
    conceptes=[['NUM','num'],['DEN','den']]
    sexe = "if(sexe='H','HOME','DONA')"
    comb="NOINSAT"
    condicio_khalix = "excl=0" 

    ecap_ind = "exp_ecap_uba"  
    uba5="uba5"
    condicio_ecap_ind = "excl=0"
    uba_in = "mst_ubas"
    minPob = 50

    khalix_uba = 'exp_khalix_uba_ind'
    dimensions = [['NUM','resolts', 1],['DEN','detectats', 1],['AGNORESOL','llistat', 1],['AGRESOLTR','resolucio', 100],['AGRESOLT','resolucioPerPunts', 100],['AGRESULT','resultatPerPunts', 100],['AGASSOLP','resultatPerPunts', 100],['AGASSOL','punts', 1],['AGMMINRES','mmin_p', 100],['AGMMAXRES','mmax_p', 100]]

    ecap_pac_pre = "ass_ecap_pacient_pre"
    ecap_pac = "exp_ecap_pacient"
    ecap_marca_exclosos = "if(excl=1,4,0)"
    condicio_ecap_llistat = "llistat=1 and excl=0"

    metesOrig = "ass_metes_subind_anual"
    metes = "eqa_metes"
    ponderacio = "ass_ponderacio"


    indic = 'ass_indicadors'
    cataleg = "exp_ecap_cataleg"
    catalegPare = "exp_ecap_catalegPare"
    catalegPunts = "exp_ecap_catalegPunts"
    catalegKhx = "exp_khalix_cataleg"

    sql = 'select data_ext from dextraccio'
    for d in getOne(sql, 'nodrizas'):
        dara = d

    c_cursor.execute("drop table if exists %s" % uba_in)
    c_cursor.execute("create table %s (up varchar(5) not null default '',uba varchar(10) not null default '',tipus varchar(1) not null default '',primary key(up,uba,tipus))" % uba_in)
    c_cursor.execute("insert into %s select visi_up up, visi_dni_prov_resp uba, if(tipus = 'L' and visi_up in %s, 'E', tipus) from %s where visi_up <> '' group by 1, 2, 3 having count(1)>=%d" % (uba_in,tuple(ass_sense_gine),poblacio,minPob))
        
    c_cursor.execute("drop table if exists %s" % uba5)
    c_cursor.execute("create table %s (up varchar(5) not null default '',uba varchar(10) not null default '',tipus varchar(1) not null default'',indicador varchar(8) not null default '',detectats int,resolts int)" % uba5)
    c_cursor.execute("insert into %s select up,dni uba,tipus,indicador,sum(den) detectats,sum(num) resolts from %s where %s group by 1,2,3,4" % (uba5,pac,condicio_ecap_ind))

    tb_rd = "lv_conversio_assir"
    # execute("drop table %s" % tb_rd, 'redics')
    # execute("create table %s (codi_lv varchar2(10), up varchar2(10), br varchar2(10), dni varchar2(10))" % tb_rd, 'redics')
    cols = "(codi_lv varchar(10), up varchar(10), br varchar(10), dni varchar(10))"
    createTable(tb_rd, cols, 'ass', rm=True)
    execute("insert into ass.%s select upper(substr(concat('S', sha1(concat(concat(a.up, case when tipus = 'E' then 'L' else tipus end), uba))), 1, 10)), a.up, br, uba from ass.%s a inner join nodrizas.%s b on a.up = b.up" % (tb_rd, 'mst_ubas', 'ass_centres'), "nodrizas")
    sql = "select codi_lv, up, br, dni from lv_conversio_assir"
    upload = []
    for row in getAll(sql, 'ass'):
        upload.append(tuple(row))
    cols = "(codi_lv varchar2(10), up varchar2(10), br varchar2(10), dni varchar2(10))"
    createTable(tb_rd, cols, 'redics', rm=True)
    listToTable(upload, tb_rd, 'redics')
    users = ('PREDUPRP', 'PREDUJVG', 'PREDUMAA', 'PREDUTFP', 'PREDUXMG')
    grantSelect(tb_rd, users, 'redics')

    sql = "select ics_codi, scs_codi from cat_centres"
    centres = {}
    for br, up in getAll(sql, 'nodrizas'):
        centres[up] = br
    sql = "select up, dni, hash, uas from dwsisap.assignada_administratius"
    adm = []
    for up, dni, hash, uas in getAll(sql, 'exadata'):
        if up in centres:
            br = centres[up]
            codi_lv = br + 'A' + hash[:6]
            adm.append((codi_lv, up, br, dni, uas))
    cols = "(codi_lv varchar2(20), up varchar2(10), br varchar2(10), dni varchar2(10), uas varchar2(10))"
    createTable('lv_conversio_adm', cols, 'redics', rm=True)
    listToTable(adm, 'lv_conversio_adm', 'redics')
    users = ('PREDUPRP', 'PREDUJVG', 'PREDUMAA', 'PREDUTFP', 'PREDUXMG')
    grantSelect('lv_conversio_adm', users, 'redics')

    c_cursor.execute("drop table if exists %s" % ecap_ind)
    c_cursor.execute("create table %s (up varchar(5) not null default '',uba varchar(10) not null default '',tipus varchar(1) not null default '', indicador varchar(8) not null default '',esperats double,detectats int,resolts int,deteccio double,deteccioPerResultat double,resolucio double,resultat double,resultatPerPunts double,punts double,llistat int, resolucioPerPunts double, invers int, mmin_p double, mmax_p double)" % ecap_ind)
    c_cursor.execute("insert into %s select a.up, uba,tipus, a.indicador,0 esperats,if(detectats is null,0,detectats),if(resolts is null,0,resolts),0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat, 0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p from %s a" % (ecap_ind,uba5))
    c_cursor.execute("update %s set deteccio=if((detectats/esperats) is null,0,detectats/esperats),resolucio=if(detectats=0,0,resolts/detectats)" % ecap_ind)


    #aqui anirien esperades Com que no hi ha esperades fem resultat igual resolucio. Posem 1 per si de cas!
    c_cursor.execute("update %s set deteccioPerResultat=1" % ecap_ind)

    c_cursor.execute("update %s set resultat=resolucio" % ecap_ind)
    c_cursor.execute("delete from %(ecap_ind)s where not exists (select 1 from %(uba_in)s where %(ecap_ind)s.up=%(uba_in)s.up and %(ecap_ind)s.uba=%(uba_in)s.uba and %(ecap_ind)s.tipus=%(uba_in)s.tipus)" % {'ecap_ind': ecap_ind,'uba_in':uba_in})
    c_cursor.execute("alter table %s add unique (up,uba,tipus,indicador)" % ecap_ind)
    with open(pathc, 'rb') as file:
        d=csv.reader(file, delimiter='@', quotechar='|')
        for id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,invers in d:
            if baixa==1:
                ok=1
            else:
                c_cursor.execute("insert ignore into %s select up,uba,tipus,'%s' indicador,0 esperats,0 detectats,0 resolts,0 deteccio,1 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat, 0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p from (select up,uba,tipus from %s) a" % (ecap_ind,i,uba_in))

    c_cursor.execute("drop table if exists %s" % metes)
    c_cursor.execute("create table %s (indicador varchar(8) not null default '',mmin double,mint double,mmax double,unique (indicador,mmin,mmax))" % metes)
    c_cursor.execute("drop table if exists %s" % metesOrig)
    c_cursor.execute("create table %s(indicador varchar(8) not null default '',z1 varchar(10) not null default '',z2 varchar(10) not null default '',meta varchar(10) not null default '',z3 varchar(10) not null default '',z4 varchar(10) not null default '',z5 varchar(10) not null default '',z6 varchar(10) not null default '',valor double)" % metesOrig)
    with open(pathmetes, 'rb') as file:
        m=csv.reader(file, delimiter='{', quotechar='|')
        for met in m:
            indicador,z1,z2,meta,z3,z4,z5,valor = met[0],met[1],met[2],met[3],met[4],met[5],met[6],met[7]
            c_cursor.execute("insert into %s Values('%s','%s','%s','%s','%s','%s','%s','',%s)" % (metesOrig,indicador,z1,z2,meta,z3,z4,z5,valor))
    c_cursor.execute("insert into %s select a.indicador,a.valor/100,b.valor/100,c.valor/100 from (select indicador,valor from %s where meta='AGMMINRES') a inner join (select indicador,valor from %s where meta='AGMINTRES') b on a.indicador=b.indicador inner join (select indicador,valor from %s where meta='AGMMAXRES') c on a.indicador=c.indicador" % (metes,metesOrig,metesOrig,metesOrig))
    c_cursor.execute('update {} set mmin = mmax * 0.9 where mmin > mmax * 0.9'.format(metes))
    c_cursor.execute('update {} set mint = (mmax + mmin) / 2'.format(metes))
    
    if len(indicadors_inversos) == 1:
        c_cursor.execute("update {} a set invers = 1 where indicador = '{}'".format(ecap_ind, list(indicadors_inversos)[0]))
    elif len(indicadors_inversos) > 1:
        c_cursor.execute("update {} a set invers = 1 where indicador IN {}".format(ecap_ind, tuple(indicadors_inversos)))
    c_cursor.execute("update {} a inner join {} b on a.indicador = b.indicador set mmin_p = if(invers = 0, floor(mmin * detectats), ceil(mmin * detectats)), mmax_p = if(invers = 0, floor(mmax * detectats), ceil(mmax * detectats))".format(ecap_ind, metes))
    c_cursor.execute('update {} set mmin_p = mmin_p - 1 where invers = 0 and mmin_p = mmax_p and mmax_p > 0'.format(ecap_ind))
    c_cursor.execute('update {} set mmax_p = mmax_p + 1 where invers = 1 and mmin_p = mmax_p'.format(ecap_ind))
    c_cursor.execute("update %s set resolucioPerPunts=if(invers = 0, if(resolts>=mmax_p,1,if(resolts<mmin_p,0,(resolts-mmin_p)/(mmax_p-mmin_p))), \
                                                        1 - if(resolts>=mmax_p,1,if(resolts<mmin_p,0,(resolts-mmin_p)/(mmax_p-mmin_p))))" % (ecap_ind))
    c_cursor.execute('update {} set mmin_p = mmin_p / detectats, mmax_p = mmax_p / detectats where detectats > 0'.format(ecap_ind))
    if len(indicadors_inversos) == 1:
        c_cursor.execute("update {} a set llistat=if(indicador = '{}', resolts, detectats-resolts)".format(ecap_ind, list(indicadors_inversos)[0]))
    elif len(indicadors_inversos) > 1:
        c_cursor.execute("update {} a set llistat=if(indicador in {}, resolts, detectats-resolts)".format(ecap_ind, tuple(indicadors_inversos)))

    c_cursor.execute("update %s a set resultatPerPunts=resolucioPerPunts*deteccioPerResultat" % (ecap_ind))


    c_cursor.execute("drop table if exists %s" % ponderacio)
    c_cursor.execute("create table %s (indicador varchar(8) not null default '',tipus varchar(1) not null default '',valor double,unique (indicador,tipus))" % ponderacio)
    conv = {"MF": "G", "INF": "L", "INFSENSEGI": "E"}
    upload = []
    inds = set()
    tips = set()
    for ind, _r, _r, _r, tip, _r, _r, val in csv.reader(open(pathpond, 'rb'), delimiter='{'):
        upload.append((ind, conv[tip], max(float(val), 0)))
        inds.add(ind)
        tips.add(conv[tip])
    listToTable(upload, ponderacio, db)
    upload2 = [(ind, tip, 0) for (ind, tip) in itertools.product(inds, tips)]
    listToTable(upload2, ponderacio, db)
    c_cursor.execute("update %s a inner join %s b on a.indicador=b.indicador and a.tipus=b.tipus set punts=resolucioPerPunts*deteccioPerResultat*valor" % (ecap_ind,ponderacio))

    c_cursor.execute("alter table %s add unique (up,uba,tipus,indicador)" % ecap_ind)


    with open(pathc, 'rb') as file:
        d=csv.reader(file, delimiter='@', quotechar='|')
        for id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,invers in d:
            c_cursor.execute("insert ignore into %s select up,uba,tipus,'%s' indicador,0 esperats,0 detectats,0 resolts,0 deteccio,0 deteccioPerResultat,0 resolucio,0 resultat,0 resultatPerPunts,0 punts,0 llistat, 0 resolucioPerPunts, 0 invers, 0 mmin_p, 0 mmax_p  from (select up,uba,tipus from %s) a" % (ecap_ind,i,uba_in))

    createTable(khalix_uba, '(up varchar(5), uba varchar(10), tipus varchar(1), indicador varchar(10), analisis varchar(12), detalle varchar(12), valor double)', db, rm=True)
    for analisis, variable, coeficient in dimensions:
        dades = (row for row in getAll("select up, uba, if(tipus = 'E', 'L', tipus), indicador, '{}', 'NOINSAT', round({} * {} , 6) from {}".format(analisis, variable, coeficient, ecap_ind), db))
        listToTable(dades, khalix_uba, db)

    c_cursor.execute("drop table if exists %s,%s,%s,%s" % (cataleg,catalegPare,catalegPunts,catalegKhx))
    c_cursor.execute("create table %s (indicador varchar(8),literal varchar(80),ordre int,pare varchar(8),llistat int,invers int,mdet double,mmin double,mint double,mmax double,toShow int,wiki varchar(250),pantalla varchar(50))" % cataleg)
    c_cursor.execute("drop table if exists %s" % indic)
    c_cursor.execute("create table %s (id int, indicador varchar(8), literal varchar(80),grup varchar(8),grup_desc varchar(80),baixa_ int, dbaixa varchar(80),llistats int,invers int,ordre int,grup_ordre int)" % indic)
    with open(pathc, 'rb') as file:
        d=csv.reader(file, delimiter='@', quotechar='|')
        for id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,invers in d:
            if baixa==1:
                ok=1
            else:
                c_cursor.execute("""insert into %s values("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")""" % (indic,id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,invers,ordre,grup_ordre))
    c_cursor.execute("insert into %s select distinct a.indicador indicador,literal,ordre,grup pare,llistats llistat,invers,0 mdet,if(mmin is null,0,mmin),if(mint is null,0,mint),if(mmax is null,0,mmax),1 toShow,concat('http://10.80.217.201/sisap-umi/indicador/codi/',a.indicador) wiki,'GENERAL' pantalla from %s a left join %s b on a.indicador=b.indicador where baixa_=0" % (cataleg,indic,metes))
    c_cursor.execute("create table %s (pare varchar(10) not null default '',literal varchar(80) not null default '',ordre int,proces varchar(50))" % catalegPare)
    c_cursor.execute("insert into %s select distinct grup pare, grup_desc literal,grup_ordre ordre,'' proces from %s where baixa_=0" % (catalegPare,indic))
    c_cursor.execute("create table %s like %s" % (catalegPunts,ponderacio))
    c_cursor.execute("insert into %s select * from %s" % (catalegPunts,ponderacio))
    c_cursor.execute("create table %s (indicador varchar(10) not null default'',desc_ind varchar(300) not null default'',grup varchar(10) not null default'', grup_desc varchar(300) not null default'')" % catalegKhx)
    with open(pathc, 'rb') as file:
        d=csv.reader(file, delimiter='@', quotechar='|')
        for id,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,invers in d:
            if baixa=="1":
                ok=1
            else:
                c_cursor.execute("""insert into %s Values("%s","%s","%s","%s")""" % (catalegKhx,i,desc,grup,grupdesc))     
            
    # c_cursor.execute("update exp_ecap_catalegpunts set valor=0")
    c_cursor.execute("update exp_ecap_cataleg set mdet=0")

    conn.close()

if __name__ == """__main__""":
    get_ass_centres();                              print("Done: get_ass_centres()")
    get_ass_poblacio();                             print("Done: get_ass_poblacio()")
    get_ass_poblacio_imputacio_dni();               print("Done: get_ass_poblacio_imputacio_dni()")
    get_ass_poblacio_imputacio_up_assir();          print("Done: get_ass_poblacio_imputacio_up_assir()")
    get_txt();                                      print("Done: get_txt()")
    get_mst_indicadors();                           print("Done: get_mst_indicadors()")
    old_is_the_new_fashion();                       print("Done: old_is_the_new_fashion()")