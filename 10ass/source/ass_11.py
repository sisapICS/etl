# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime

# Definici� de l'indicador
INDICADOR = "EQA9211"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula la data fa un any.

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys90dies': Fa un any des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys90dies

    sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
            date_add(data_ext, INTERVAL -90 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys90dies = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� assignada al servei ASSIR en edats de 13 a 55 anys.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les persones assignades en edats de 13 a 55 anys.
    """
    global poblacio_ass

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_poblacio
        WHERE
            edat BETWEEN 13 AND 55
    """
    poblacio_ass = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_denominador():
    """
    Calcula el denominador de l'indicador, que inclou:
    1. Dones entre 13 i 55 anys assignades al servei ASSIR.

    Defineix:
        - 'denominador': Conjunt d'identificadors de dones assignades al servei ASSIR.
    """
    global denominador

    denominador = poblacio_ass.copy()

def get_exclusions():
    """
    Calcula les exclusions basades en diversos criteris, incloent:
    - Embarassos actius.
    - Puerperi en els �ltims 90 dies.
    - Dones sense relacions sexuals o en menopausa.
    - Dones amb histerectomia.

    Defineix:
        - 'exclusions': Conjunt d'identificadors de dones que compleixen criteris d'exclusi�.
    """
    global exclusions
    exclusions = set()

    sqls = (
        """
            SELECT
                id_cip_sec
            FROM
                ass_variables 
            WHERE
                (agrupador = 490 AND val_txt LIKE 'Mai%') -- Situaci� sexual: Mai
                OR (agrupador = 818 AND val_num IN (0, 2)) -- Relacions sexuals: 0 (No) o 2 (Mai)
                OR (agrupador = 819) -- Edat menopausa registrada
        """,
        """
            SELECT
                id_cip_sec
            FROM
                eqa_problemes
            WHERE
                ps IN (78, 493, 852, 974) -- Diagn�stics de menopausa o histerectomia
        """,
        """
            SELECT
                id_cip_sec
            FROM
                ass_embaras
            WHERE
                fi = DATE'{_data_ext}' 
                OR (puerperi = 1 AND fi >= DATE'{_data_ext_menys90dies}') -- Embar�s actiu o puerperi obert en els �ltims 90 dies
        """.format(_data_ext=data_ext,_data_ext_menys90dies=data_ext_menys90dies)
    )
    for sql in sqls:
        print(sql)
        exclusions.update({id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in denominador})

def get_numerador():
    """
    Calcula el numerador de l'indicador, que inclou:
    1. Dones amb un registre del m�tode anticonceptiu utilitzat.

    Defineix:
        - 'numerador': Conjunt d'identificadors amb m�tode anticonceptiu registrat.
    """
    global numerador

    sql = """
        SELECT
            id_cip_sec,
            dat
        FROM
            ass_variables
        WHERE
            agrupador = 390 -- Agrupador per registre del m�tode anticonceptiu
    """
    numerador = {id_cip_sec for id_cip_sec, data_anticonceptiu in u.getAll(sql, "nodrizas") if id_cip_sec in denominador}

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [(id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 1 if id_cip_sec in exclusions else 0) for id_cip_sec in denominador]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_exclusions();           print("get_exclusions(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))