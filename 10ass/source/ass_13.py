# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime

# Definici� de l'indicador
INDICADOR = "EQA9213"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula la data fa un any.

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys90dies': Fa un any des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys90dies

    sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
            date_add(data_ext, INTERVAL -90 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys90dies = u.getOne(sql, "nodrizas")

def get_denominador():
    """
    Calcula el denominador de l'indicador, que inclou:
    1. Dones entre 35-49 anys i 70-74 anys assignades al servei ASSIR.

    Defineix:
        - 'denominador': Conjunt d'identificadors de les dones en les franges d'edat especificades.
    """
    global denominador

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_poblacio
        WHERE
            (edat BETWEEN 35 AND 49) OR (edat BETWEEN 70 AND 74)
    """
    denominador = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_exclusions():
    """
    Calcula les exclusions basades en criteris espec�fics:
    - Dones amb diagn�stic actiu d'embar�s o puerperi en els darrers 90 dies.
    - Dones amb diagn�stic de c�ncer de mama.

    Defineix:
        - 'exclusions': Conjunt d'identificadors que compleixen criteris d'exclusi�.
    """
    global exclusions
    exclusions = set()

    # Excloure dones amb c�ncer de mama
    sqls = ["""
                SELECT
                    id_cip_sec
                FROM
                    eqa_problemes
                WHERE
                    ps = 560 -- Codi per diagn�stic de c�ncer de mama
            """,
            """
                SELECT
                    id_cip_sec
                FROM
                    ass_embaras
                WHERE
                    fi = DATE'{_data_ext}' 
                    OR (puerperi = 1 AND fi >= DATE'{_data_ext_menys90dies}') -- Embar�s actiu o puerperi obert en els �ltims 90 dies
            """.format(_data_ext=data_ext,_data_ext_menys90dies=data_ext_menys90dies)
    ]
    for sql in sqls:
        exclusions.update({id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in denominador})

def get_numerador():
    """
    Calcula el numerador de l'indicador, que inclou:
    1. Dones amb factors de risc de c�ncer de mama registrats.

    Defineix:
        - 'numerador': Conjunt d'identificadors amb factors de risc registrats.
    """
    global numerador

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_variables
        WHERE
            agrupador = 396 -- Codi per factors de risc de c�ncer de mama
    """
    numerador = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in denominador}

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [(id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 1 if id_cip_sec in exclusions else 0) for id_cip_sec in denominador]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_exclusions();           print("get_exclusions(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))