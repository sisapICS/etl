# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime
from dateutil.relativedelta import relativedelta

INDICADOR = "EQA9221"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants:
    - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
    - 'data_ext_menys2anys': Fa dos anys des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys2anys

    sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
            date_add(date_add(data_ext, INTERVAL -2 YEAR), INTERVAL + 1 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys2anys = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� atesa pel servei ASSIR.
    Filtra les dones menors de 28 anys assignades al servei, per buscar despr�s
    aquelles amb embar�s actiu i edat de 25 anys o menys al moment de finalitzar-ho.

    Defineix:
        - 'poblacio_ass': Diccionari amb identificadors ('id_cip_sec') i dates de naixement.
    """
    global poblacio_ass

    sql = """
        SELECT
            id_cip_sec,
            data_naix
        FROM
            assignada_tot
        WHERE
            edat < 28
    """
    poblacio_menor_28 = {id_cip_sec: data_naix for id_cip_sec, data_naix in u.getAll(sql, "nodrizas")}

    sql = """
        SELECT 
            id_cip_sec
        FROM
            ass_poblacio
        WHERE
            edat < 28
    """
    poblacio_ass = {
        id_cip_sec: poblacio_menor_28[id_cip_sec]
        for id_cip_sec, in u.getAll(sql, "nodrizas")
        if id_cip_sec in poblacio_menor_28
    }

def get_embarasos():
    """
    Obt� informaci� sobre els embarassos actius en l'�ltim any.
    Filtra les dones embarassades que tenien 25 anys o menys al final de l'embar�s.

    Defineix:
        - 'embarasos': Diccionari amb les dades d'embar�s ('inici', 'fi') per cada 'id_cip_sec'
                       amb 25 anys o menys al final de l'embar�s.
    """
    global embarasos

    sql = """
        SELECT
            id_cip_sec,
            inici,
            fi
        FROM
            ass_embaras
        WHERE
            inici BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
            OR fi BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
    """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    embarasos = {
        id_cip_sec: {"inici": inici, "fi": fi}
        for id_cip_sec, inici, fi in u.getAll(sql, "nodrizas")
        if id_cip_sec in poblacio_ass and u.yearsBetween(poblacio_ass[id_cip_sec], fi) <= 25
    }

def get_denominador():
    """
    Calcula el denominador:
    Dones embarassades menors de 25 anys amb embar�s actiu.
    """
    global denominador
    denominador = {id_cip_sec for id_cip_sec in embarasos if id_cip_sec in poblacio_ass}

def sub_get_problemes(sql):
    """
    Subfunci� per obtenir problemes espec�fics mitjan�ant consultes SQL.
    """
    sub_problemes = {id_cip_sec for id_cip_sec, in u.getAll(sql, "import")}
    return sub_problemes

def get_exclusions():
    """
    Calcula les exclusions basades en diagn�stics espec�fics que impedeixen el cribratge.
    Inclou dones amb diagn�stics relacionats amb complicacions de l'embar�s, com avortaments o embar�s de risc.

    Defineix:
        - 'exclusions': Conjunt d'identificadors de dones excloses.
    """
    global exclusions
    exclusions = set()

    codis_exclusions = ('C01-O02.1', 'C01-O03.9', 'C01-O36.9', 'C01-Z35', 'O02.1', 'O03.9', 'O36.9', 'Z35')
    sql = """
        SELECT
            id_cip_sec
        FROM
            {_tb_name}
        WHERE
            pr_cod_ps IN {_codis_exclusions}
            AND pr_dde BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
    """
    jobs = [sql.format(_tb_name=tb_name, _codis_exclusions=codis_exclusions, _data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
            for tb_name in u.getSubTables("problemes") if tb_name[-6:] != '_s6951']
    for sub_problemes in u.multiprocess(sub_get_problemes, jobs, 4):
        exclusions.update(sub_problemes)

def get_numerador():
    """
    Calcula el numerador:
    Dones embarassades menors de 25 anys que han rebut el cribratge de clam�dia i gonococ abans del part.

    Defineix:
        - 'numerador': Conjunt d'identificadors de les embarassades menors de 25 anys que han rebut el cribratge de clam�dia i gonococ abans del part.
    """
    global numerador
    agrupadors_numerador = (652, 654)

    sql = """
        SELECT
            id_cip_sec,
            dat
        FROM
            ass_variables
        WHERE
            agrupador IN {_agrupadors_numerador}
            AND dat BETWEEN DATE'{_data_ext_menys2anys}' AND DATE'{_data_ext}'
    """.format(_agrupadors_numerador=agrupadors_numerador, _data_ext_menys2anys=data_ext_menys2anys, _data_ext=data_ext)

    numerador = {
        id_cip_sec for id_cip_sec, data in u.getAll(sql, "nodrizas")
        if id_cip_sec in denominador and embarasos[id_cip_sec]["inici"] <= data <= embarasos[id_cip_sec]["fi"]
    }

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [
        (id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 1 if id_cip_sec in exclusions else 0)
        for id_cip_sec in denominador
    ]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_embarasos();            print("get_embarasos(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_exclusions();           print("get_exclusions(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))