# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime

INDICADOR = "EQA9223"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants:
    - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
    - 'data_ext_menys2anys': Fa dos anys des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys2anys

    sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
            date_add(date_add(data_ext, INTERVAL -2 YEAR), INTERVAL + 1 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys2anys = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� atesa pel servei ASSIR menor de 25 anys.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les dones menors de 25 anys.
    """
    global poblacio_ass

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_poblacio
        WHERE
            edat < 25
    """
    poblacio_ass = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_denominador():
    """
    Calcula el denominador de l'indicador:
    - Total de dones menors de 25 anys ateses pel servei ASSIR.

    Defineix:
        - 'denominador': Conjunt d'identificadors de les dones que compleixen els criteris per al denominador.
    """
    global denominador
    denominador = {id_cip_sec for id_cip_sec in poblacio_ass}

def sub_get_problemes(sql):
    """
    Subfunci� per obtenir problemes espec�fics mitjan�ant consultes SQL.

    Retorna:
        - Conjunt d'identificadors amb els problemes trobats.
    """
    sub_problemes = {id_cip_sec for id_cip_sec, in u.getAll(sql, "import")}
    return sub_problemes

def get_exclusions():
    """
    Calcula les exclusions per a l'indicador:
    - Dones amb diagn�stics de displ�sia cervical o HPV.

    Defineix:
        - 'exclusions': Conjunt d'identificadors de dones amb criteris d'exclusi�.
    """
    global exclusions
    exclusions = set()

    codis_exclusions_ps = ('C01-B97.7', 'C01-N87.0', 'C01-N87.1', 'C01-N87.2', 'C01-N87.9', 'C01-N93.0', 'B97.7', 'N87.0', 'N87.1', 'N87.2', 'N87.9', 'N93.0')

    sql = """
        SELECT
            id_cip_sec
        FROM
            {_tb_name}
        WHERE
            pr_cod_ps IN {_codis_exclusions_ps}
            AND pr_data_baixa = 0
    """

    jobs = [sql.format(_tb_name=tb_name, _codis_exclusions_ps=codis_exclusions_ps)
            for tb_name in u.getSubTables("problemes") if tb_name[-6:] != '_s6951']
    for sub_problemes in u.multiprocess(sub_get_problemes, jobs, 4):
        exclusions.update(sub_problemes)

def get_numerador():
    """
    Calcula el numerador de l'indicador:
    - Dones menors de 25 anys que han rebut una citologia en l'�ltim any.

    Defineix:
        - 'numerador': Conjunt d'identificadors de dones que han rebut una citologia en l'�ltim any.
    """
    global numerador
    numerador = set()

    sql = """
        SELECT
            id_cip_sec,
            dat
        FROM
            ass_variables
        WHERE
            agrupador = 400 -- Codi per citologia
            AND dat BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
    """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)

    for id_cip_sec, data_vs in u.getAll(sql, "nodrizas"):
        if id_cip_sec in denominador and id_cip_sec not in exclusions:
            numerador.add(id_cip_sec)

def create_resultats():
    """
    Genera els resultats finals de l'indicador:
    - Crea una llista amb (id_cip_sec, numerador, denominador, exclusi�).

    Defineix:
        - 'resultats': Llista de tuples amb el resultat per cada dona.
    """
    global resultats
    resultats = [
        (id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 1 if id_cip_sec in exclusions else 0)
        for id_cip_sec in denominador
    ]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats de l'indicador i els guarda a la base de dades especificada.
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_exclusions();           print("get_exclusions(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))