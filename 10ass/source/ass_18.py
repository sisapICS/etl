# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime

INDICADOR = "EQA9218"
TABLE = "ass_indicador_{}".format(INDICADOR)
DATABASE = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants:

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys90dies': Fa 90 dies abans de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys90dies

    sql = """
        SELECT
            data_ext,
            date_add(data_ext, INTERVAL -1 YEAR),
            date_add(data_ext, INTERVAL -90 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys90dies = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� assignada al servei ASSIR.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de pacients assignades.
    """
    global poblacio_ass

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_poblacio
    """
    poblacio_ass = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_denominador():
    """
    Calcula el denominador de l'indicador:
    - Dones amb diagn�stic de metrorr�gia postmenop�usica en l'�ltim any.

    Defineix:
        - 'denominador': Diccionari amb identificadors i dates de diagn�stic de metrorr�gia postmenop�usica.
    """
    global denominador
    denominador = c.defaultdict(set)

    sql = """
        SELECT
            id_cip_sec,
            dde
        FROM
            eqa_problemes_incid
        WHERE
            ps IN (409, 410) -- Codis per a metrorr�gia postmenop�usica
            AND dde BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
    """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)

    for id_cip_sec, data_dx in u.getAll(sql, "nodrizas"):
        if id_cip_sec in poblacio_ass:
            denominador[id_cip_sec].add(data_dx)

def get_exclusions():
    """
    Calcula les exclusions per a l'indicador:
    - Exclou dones amb embar�s actiu o en puerperi en els darrers 90 dies.

    Defineix:
        - 'exclusions': Conjunt d'identificadors de dones excloses.
    """
    global exclusions

    sql = """
            SELECT
                id_cip_sec
            FROM
                ass_embaras
            WHERE
                fi = DATE'{_data_ext}' 
                OR (puerperi = 1 AND fi >= DATE'{_data_ext_menys90dies}') -- Embar�s actiu o puerperi obert en els �ltims 90 dies
          """.format(_data_ext=data_ext,_data_ext_menys90dies=data_ext_menys90dies)
    exclusions = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in denominador}

def get_numerador():
    """
    Calcula el numerador de l'indicador:
    - Dones amb diagn�stic de metrorr�gia postmenop�usica que han tingut una ecografia ginecol�gica o derivaci� a ginecologia
      en els 15 dies posteriors al diagn�stic.

    Defineix:
        - 'numerador': Conjunt d'identificadors que compleixen el criteri.
    """
    global numerador
    numerador = set()

    sqls = ["""
                SELECT
                    id_cip_sec,
                    dat,
                    agrupador
                FROM
                    ass_proves
                WHERE
                    agrupador IN (438) -- Codis per a ecografia ginecol�gica o derivaci�
                    AND dat BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
            """,
            """
                SELECT
                    id_cip_sec,
                    dat,
                    agrupador
                FROM
                    ass_variables
                WHERE
                    agrupador IN (403) -- Codis per a ecografia (altres)
                    AND dat BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
            """,
            """
                SELECT
                    id_cip_sec,
                    dat,
                    agrupador
                FROM
                    ass_derivacions
                WHERE
                    agrupador IN (437) -- Codis per a ecografia ginecol�gica o derivaci�
                    AND dat BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
            """,
            """
                SELECT
                    id_cip_sec,
                    ecg_data_alta,
                    0 agrupador
                FROM
                    ass_ecos
                WHERE
                    ecg_data_alta BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
            """,
            """
                SELECT
                    id_cip_sec,
                    dat,
                    agrupador
                FROM
                    eqa_xml
                WHERE
                    agrupador IN (438) -- Codis per a ecografia ginecol�gica
                    AND dat BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
            """,
            """
                SELECT
                    id_cip_sec,
                    dat,
                    agrupador
                FROM
                    ass_variables
                WHERE
                    agrupador IN (820) -- Codis per a biopsia endometri
                    AND dat BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
            """,
            """
                SELECT
                    id_cip_sec,
                    oc_data,
                    "DER"
                FROM
                    import.derivacions
                WHERE
                    inf_servei_d_codi = 40217
                    AND oc_data BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
            """,
            """
                SELECT
                    id_cip_sec,
                    oc_data,
                    "DER"
                FROM
                    import.proves
                WHERE
                    inf_servei_d_codi = 40217
                    AND oc_data BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
            """]
    for sql in sqls:
        for id_cip_sec, data_prova, agrupador in u.getAll(sql.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext), "nodrizas"):
            dates_dx = denominador.get(id_cip_sec)
            if dates_dx:
                for data_dx in dates_dx:
                    if agrupador != 820:
                        if data_dx <= data_prova <= data_dx + datetime.timedelta(days=15):
                            numerador.add(id_cip_sec)
                    else:
                        if data_dx <= data_prova <= data_dx + datetime.timedelta(days=45):
                            numerador.add(id_cip_sec)

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [(id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 1 if id_cip_sec in exclusions else 0) for id_cip_sec in denominador]

def export_resultats():
    """
    Exporta els resultats a la base de dades.
    """
    u.createTable(TABLE, COL_NAMES, DATABASE, rm=True)
    u.listToTable(resultats, TABLE, DATABASE)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_exclusions();           print("get_exclusions(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))