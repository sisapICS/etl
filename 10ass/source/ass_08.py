# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime
from dateutil.relativedelta import relativedelta

# Definici� de l'indicador
INDICADOR = "EQA9208"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants.

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys22mesos': Fa 22 mesos des de la data d'extracci�.
        - 'data_ext_menys24setmanes': Fa 24 setmanes des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys22mesos, data_ext_menys24setmanes

    sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL - 1 YEAR), INTERVAL + 1 DAY),
            date_add(date_add(data_ext, INTERVAL - 22 MONTH), INTERVAL + 1 DAY),
            date_add(date_add(data_ext, INTERVAL - 24 WEEK), INTERVAL + 1 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys22mesos, data_ext_menys24setmanes = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� assignada al servei ASSIR.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les persones assignades al servei ASSIR.
    """
    global poblacio_ass

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_poblacio
    """
    poblacio_ass = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_embarasos():
    """
    Obt� la informaci� sobre els embarassos actius que han arribat a la setmana 24.

    Defineix:
        - 'embarasos': Diccionari amb les dates d'inici i fi de cada embar�s per cada 'id_cip_sec'.
    """
    global embarasos

    sql = """
        SELECT
            id_cip_sec,
            inici,
            fi
        FROM
            ass_embaras
        WHERE
            (inici BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext_menys24setmanes}'
            OR fi BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}')
            AND temps > 161 -- Temps en dies (A partir de les 23 setmanes + 1 dia --> Inici setmana 24)
    """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext_menys24setmanes=data_ext_menys24setmanes, _data_ext=data_ext)
    embarasos = {id_cip_sec: {"inici": inici, "fi": fi} for id_cip_sec, inici, fi in u.getAll(sql, "nodrizas")}

def get_visites_embaras():
    """
    Calcula les visites realitzades a partir de la setmana 24 d'embar�s per a l'indicador.

    Defineix:
        - 'visites_embaras': Conjunt d'identificadors de les dones embarassades que tenen visites 
            realitzades a partir de la setmana 24 d'embar�s.
    """
    global visites_embaras

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_visites_embaras
        WHERE
            dies > 161 -- Temps en dies (A partir de les 23 setmanes + 1 dia --> Inici setmana 24)
    """
    visites_embaras = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_denominador():
    """
    Calcula el denominador de l'indicador.

    Defineix:
        - 'denominador': Conjunt d'identificadors de dones que han arribat a les 24 setmanes de gestaci� i han estat ateses pel servei ASSIR.
    """
    global denominador

    denominador = {id_cip_sec for id_cip_sec in embarasos if id_cip_sec in visites_embaras and id_cip_sec in poblacio_ass}

def get_numerador():
    """
    Calcula el numerador de l'indicador, que inclou:
    1. Dones embarassades amb el pacte del Pla de Naixement registrat a partir de la setmana 24 d'embar�s.

    Defineix:
        - 'numerador': Conjunt d'identificadors de dones amb el Pla de Naixement registrat abans del part,
            a partir de la setmana 24 d'embar�s.
    """
    global numerador

    sql = """
        SELECT
            id_cip_sec,
            dat
        FROM
            ass_variables
        WHERE
            agrupador = 392 -- Agrupador per al Pla de Naixement
            AND dat BETWEEN DATE'{_data_ext_menys22mesos}' AND DATE'{_data_ext}'
    """.format(_data_ext_menys22mesos=data_ext_menys22mesos, _data_ext=data_ext)
    numerador = {id_cip_sec for id_cip_sec, data_vs in u.getAll(sql, "nodrizas") 
                 if id_cip_sec in denominador
                 and embarasos[id_cip_sec]["inici"] + relativedelta(days=7*23+1) <= data_vs <= embarasos[id_cip_sec]["fi"]}

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [(id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 0) for id_cip_sec in denominador]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_embarasos();            print("get_embarasos(): {}".format(datetime.datetime.now()))
    get_visites_embaras();      print("get_visites_embaras(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))