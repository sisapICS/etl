# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime
from dateutil.relativedelta import relativedelta

# Definici� de l'indicador
INDICADOR = "EQA9205"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants.

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys28dies': Fa 28 dies des de la data d'extracci�.
        - 'data_ext_menys1anyi28dies': Fa un any i 28 dies des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys28dies, data_ext_menys1anyi28dies

    sql = """
        SELECT
            data_ext,
            DATE_ADD(DATE_ADD(data_ext, INTERVAL -1 YEAR), INTERVAL 1 DAY),
            DATE_ADD(data_ext, INTERVAL -28 DAY),
            DATE_ADD(DATE_ADD(data_ext, INTERVAL -1 YEAR), INTERVAL -28 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys28dies, data_ext_menys1anyi28dies = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� assignada al servei ASSIR.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les persones assignades al servei ASSIR.
    """
    global poblacio_ass

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_poblacio
    """
    poblacio_ass = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_puerperis():
    """
    Obt� informaci� sobre els puerperis actius durant el per�ode definit.

    Defineix:
        - 'puerperis': Diccionari amb les dates d'inici del puerperi per cada 'id_cip_sec'.
    """
    global puerperis

    sql = """
        SELECT
            id_cip_sec,
            fi as inici_puerperi
        FROM
            ass_embaras
        WHERE
            puerperi = 1
            AND fi BETWEEN DATE'{_data_ext_menys1anyi28dies}' AND DATE'{_data_ext_menys28dies}'
    """.format(_data_ext_menys1anyi28dies=data_ext_menys1anyi28dies, _data_ext_menys28dies=data_ext_menys28dies)
    puerperis = {id_cip_sec: {"inici": inici} for id_cip_sec, inici in u.getAll(sql, "nodrizas")}

def get_denominador():
    """
    Calcula el denominador de l'indicador.

    Defineix:
        - 'denominador': Conjunt d'identificadors de dones en puerperi ateses pel servei ASSIR.
    """
    global denominador

    denominador = {id_cip_sec for id_cip_sec in puerperis if id_cip_sec in poblacio_ass}

def get_numerador():
    """
    Calcula el numerador de l'indicador, que inclou:
    1. Dones en puerperi que han completat el q�estionari EPDS entre els 28 i 90 dies despr�s del part.

    Defineix:
        - 'numerador': Conjunt d'identificadors de dones que han realitzat el q�estionari EPDS en el per�ode definit.
    """
    global numerador

    sql = """
        SELECT
            id_cip_sec,
            dat
        FROM
            ass_variables
        WHERE
            agrupador = 387 -- Agrupador per al q�estionari EPDS
            AND dat BETWEEN DATE'{_data_ext_menys1anyi28dies}' AND DATE'{_data_ext}'
    """.format(_data_ext_menys1anyi28dies=data_ext_menys1anyi28dies, _data_ext=data_ext)

    numerador = {id_cip_sec for id_cip_sec, data_vs in u.getAll(sql, "nodrizas") 
                 if id_cip_sec in denominador 
                 and puerperis[id_cip_sec]["inici"] + relativedelta(days=28) <= data_vs <= puerperis[id_cip_sec]["inici"] + relativedelta(days=90)}

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [(id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 0) for id_cip_sec in denominador]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_puerperis();            print("get_puerperis(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))