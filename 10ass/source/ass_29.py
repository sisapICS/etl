# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime
from dateutil.relativedelta import relativedelta

INDICADOR = "EQA9229"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants:

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_any': Any de la data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys28dies': Fa 28 dies des de la data d'extracci�.
        - 'mesos_calcul_periode_gripal': Mesos de la campanya de vacunaci� antigripal.
        - 'inici_periode_gripal': Inici del per�ode gripal.
        - 'fi_periode_gripal': Fi del per�ode gripal.
    """
    global data_ext, data_ext_any, data_ext_menys1any, data_ext_menys28dies, mesos_calcul_periode_gripal, inici_periode_gripal, fi_periode_gripal

    sql = """
        SELECT
            data_ext,
            YEAR(data_ext),
            DATE_ADD(DATE_ADD(data_ext, INTERVAL -1 YEAR), INTERVAL 1 DAY),
            DATE_ADD(data_ext, INTERVAL -28 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_any, data_ext_menys1any, data_ext_menys28dies = u.getOne(sql, "nodrizas")

    mesos_calcul_periode_gripal = (9, 10, 11, 12)
    inici_periode_gripal = datetime.date(data_ext_any, 9, 1)
    fi_periode_gripal = datetime.date(data_ext_any, 12, 31)

def get_poblacio_ass():
    """
    Obt� la poblaci� assignada al servei ASSIR.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les dones assignades al servei.
    """
    global poblacio_ass

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_poblacio
    """
    poblacio_ass = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_embarasos():
    """
    Obt� la informaci� sobre els embarassos actius durant el per�ode definit.

    Defineix:
        - 'embarasos_periode_gripal': Diccionari amb informaci� sobre els embarassos actius durant el per�ode gripal.
    """
    global embarasos_periode_gripal

    sql = """
        SELECT
            id_cip_sec,
            inici,
            fi
        FROM
            ass_embaras
        WHERE
            inici BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
            OR fi BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
    """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    embarasos_periode_gripal = {
        id_cip_sec: {"inici_embaras": inici_embaras, "fi_embaras": fi_embaras}
        for id_cip_sec, inici_embaras, fi_embaras in u.getAll(sql, "nodrizas")
        if inici_embaras + datetime.timedelta(days=90) <= min(fi_periode_gripal, data_ext) and inici_periode_gripal <= fi_embaras
    }

def get_puerperis():
    """
    Obt� informaci� sobre els puerperis actius durant el per�ode definit.

    Defineix:
        - 'puerperis_periode_gripal': Diccionari amb les dates d'inici del puerperi per cada 'id_cip_sec'.
    """
    global puerperis_periode_gripal

    sql = """
        SELECT
            id_cip_sec,
            fi as inici_puerperi,
            date_add(fi, INTERVAL 120 DAY) AS fi_puerperi
        FROM
            ass_embaras
        WHERE
            puerperi = 1
            AND fi BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext_menys28dies}'
    """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext_menys28dies=data_ext_menys28dies)
    puerperis_periode_gripal = {
        id_cip_sec: {"inici_puerperi": inici_puerperi, "fi_puerperi": fi_puerperi} 
        for id_cip_sec, inici_puerperi, fi_puerperi in u.getAll(sql, "nodrizas")
        if min(fi_periode_gripal, data_ext) < fi_puerperi
    }

def get_denominador():
    """
    Calcula el denominador de l'indicador.

    Defineix:
        - 'denominador': Conjunt d'identificadors de les dones embarassades o en puerperi assignades al servei ASSIR.
    """
    global denominador

    denominador = {id_cip_sec for id_cip_sec in set(embarasos_periode_gripal) | set(puerperis_periode_gripal) if id_cip_sec in poblacio_ass}

def get_exclusions():
    """
    Calcula les exclusions per a l'indicador.

    Defineix:
        - 'exclusions': Conjunt d'identificadors de les dones que tenen contraindicacions per a la vacuna antigripal.
    """
    global exclusions

    sql = """
        SELECT
            id_cip_sec
        FROM
            ram
        WHERE 
            ram_atc LIKE 'J07BB%'
            AND ram_data_alta <= DATE'{_data_ext}'
    """.format(_data_ext=data_ext)
    exclusions = {id_cip_sec for id_cip_sec, in u.getAll(sql, "import") if id_cip_sec in denominador}

def get_numerador():
    """
    Calcula el numerador de l'indicador.

    Defineix:
        - 'numerador': Conjunt d'identificadors ('id_cip_sec') de dones que han rebut la vacuna antigripal durant el per�ode gripal.
    """
    global numerador
    numerador = set()

    sql = """
        SELECT
            id_cip_sec,
            va_u_data_vac
        FROM
            vacunes
        WHERE
            YEAR(va_u_data_vac) = {_data_ext_any}
            AND va_u_cod IN ('P-G-AR', 'P-G-NE', 'P-G-AD', 'P-G-60')
    """.format(_data_ext_any=data_ext_any)
    for id_cip_sec, data_vacunacio in u.getAll(sql, "import"):
        data_fi_embaras = embarasos_periode_gripal.get(id_cip_sec, {}).get("fi_embaras")
        data_fi_puerperi = puerperis_periode_gripal.get(id_cip_sec, {}).get("fi_puerperi")
        if data_fi_embaras and data_vacunacio < data_fi_embaras:
            numerador.add(id_cip_sec)
        if data_fi_puerperi and data_vacunacio < data_fi_puerperi:
            numerador.add(id_cip_sec)

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [(id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 1 if id_cip_sec in exclusions else 0) for id_cip_sec in denominador]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    if data_ext.month in mesos_calcul_periode_gripal:
        get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
        get_embarasos();            print("get_embarasos(): {}".format(datetime.datetime.now()))
        get_puerperis();            print("get_puerperis(): {}".format(datetime.datetime.now()))
        get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
        get_exclusions();           print("get_exclusions(): {}".format(datetime.datetime.now()))
        get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
        create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
        export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))