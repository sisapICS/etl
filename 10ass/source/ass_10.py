# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime
from dateutil.relativedelta import relativedelta

# Definici� de l'indicador
INDICADOR = "EQA9210"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula una data fa un any.

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any

    sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� assignada al servei ASSIR.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les persones assignades al servei ASSIR.
    """
    global poblacio_ass

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_poblacio
    """
    poblacio_ass = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_denominador():
    """
    Calcula el denominador de l'indicador, que inclou:
    1. Dones que han sol�licitat una IVE o anticoncepci� d'emerg�ncia en l'�ltim any.

    Defineix:
        - 'denominador': Diccionari amb identificadors i dates de les IVE/emerg�ncies.
    """
    global denominador
    denominador = c.defaultdict(set)

    sqls = (
        """
        SELECT
            id_cip_sec,
            dde
        FROM
            eqa_problemes_incid
        WHERE
            ps IN (404, 405, 702) -- Codi per IVE o anticoncepci� d'emerg�ncia
            AND dde BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
        """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext),
        """
        SELECT
            id_cip_sec,
            dat
        FROM
            ass_variables
        WHERE
            agrupador IN (393, 836, 1082) -- Agrupadors per IVE (petici� o realitzat) i anticoncepci� d'emerg�ncia
            AND dat BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
        """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    )

    for sql in sqls:
        for id_cip_sec, data_den in u.getAll(sql, "nodrizas"):
            if id_cip_sec in poblacio_ass:
                denominador[id_cip_sec].add(data_den)

def get_numerador():
    """
    Calcula el numerador de l'indicador, que inclou:
    1. Dones que han actualitzat el seu m�tode anticonceptiu dins dels 3 mesos posteriors a la IVE o anticoncepci� d'emerg�ncia.

    Defineix:
        - 'numerador': Conjunt d'identificadors que compleixen amb l'actualitzaci� del m�tode anticonceptiu.
    """
    global numerador

    sql = """
        SELECT
            id_cip_sec,
            dat
        FROM
            ass_variables
        WHERE
            agrupador = 390 -- Agrupador per registre d'anticoncepci�
    """
    numerador = {
        id_cip_sec
        for id_cip_sec, data_anticonceptiu in u.getAll(sql, "nodrizas")
        if id_cip_sec in denominador and any(
            data_den <= data_anticonceptiu <= data_den + relativedelta(months=3)
            for data_den in denominador[id_cip_sec]
        )
    }

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [(id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 0) for id_cip_sec in denominador]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))