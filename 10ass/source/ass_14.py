# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import datetime

# Definici� de l'indicador
INDICADOR = "EQA9214"
TB_NAME = "ass_indicador_{}".format(INDICADOR)
DB_NAME = 'ass'
COL_NAMES = "(id_cip_sec int, num int, den int, excl int)"

def get_dextraccio():
    """
    Obt� la data d'extracci� i calcula dates rellevants.

    Defineix:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys22mesos': Fa 22 mesos des de la data d'extracci�.
    """
    global data_ext, data_ext_menys1any, data_ext_menys22mesos

    sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
            date_add(date_add(data_ext, INTERVAL -22 MONTH), INTERVAL + 1 DAY)
        FROM
            dextraccio
    """
    data_ext, data_ext_menys1any, data_ext_menys22mesos = u.getOne(sql, "nodrizas")

def get_poblacio_ass():
    """
    Obt� la poblaci� assignada al servei ASSIR.

    Defineix:
        - 'poblacio_ass': Conjunt d'identificadors ('id_cip_sec') de les persones assignades.
    """
    global poblacio_ass

    sql = """
        SELECT
            id_cip_sec
        FROM
            ass_poblacio
    """
    poblacio_ass = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas")}

def get_dx_ITS():
    """
    Obt� els diagn�stics d'infeccions de transmissi� sexual (ITS) realitzats en l'�ltim any.

    Defineix:
        - 'dx_ITS': Diccionari amb identificadors i dates de diagn�stic ITS.
    """
    global dx_ITS
    dx_ITS = c.defaultdict(set)

    sql = """
            SELECT
                id_cip_sec,
                dde
            FROM
                eqa_problemes_incid
            WHERE
                ps IN (406, 680) -- Codi per ITS
                AND dde BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
          """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    for id_cip_sec, data_dx in u.getAll(sql, "nodrizas"):
        if id_cip_sec in poblacio_ass:
            dx_ITS[id_cip_sec].add(data_dx)

def get_denominador():
    """
    Calcula el denominador de l'indicador, que inclou:
    - Dones amb un nou diagn�stic d'ITS en l'�ltim any que han estat visitades a l'ASSIR despr�s del diagn�stic.

    Defineix:
        - 'denominador': Conjunt d'identificadors de dones amb diagn�stic ITS i visita a l'ASSIR posterior.
    """
    global denominador
    denominador = set()

    sql = """
            SELECT
                id_cip_sec,
                visi_data_visita
            FROM
                ass_visites
            WHERE
                visi_data_visita BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
          """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    for id_cip_sec, data_visita in u.getAll(sql, "nodrizas"):
        if id_cip_sec in dx_ITS and any(data_dx <= data_visita for data_dx in dx_ITS[id_cip_sec]):
            denominador.add(id_cip_sec)

def get_exclusions():
    """
    Calcula les exclusions per a l'indicador:
    - Dones amb episodis d'embar�s actiu o puerperi en els �ltims 90 dies.

    Defineix:
        - 'exclusions': Conjunt d'identificadors de dones excloses.
    """
    global exclusions
    exclusions = set()

    sql = """
        SELECT
            id_cip_sec,
            inici,
            fi
        FROM
            ass_embaras
        WHERE
            fi BETWEEN DATE'{_data_ext_menys22mesos}' AND DATE'{_data_ext}'
    """.format(_data_ext_menys22mesos=data_ext_menys22mesos, _data_ext=data_ext)
    for id_cip_sec, inici, fi in u.getAll(sql, "nodrizas"):
        if id_cip_sec in denominador and any(inici <= data_dx <= fi for data_dx in dx_ITS[id_cip_sec]):
            exclusions.add(id_cip_sec)

def get_numerador():
    """
    Calcula el numerador de l'indicador, que inclou:
    - Dones amb serologia VIH realitzada dins dels 6 mesos posteriors i/o un mes anterior al diagn�stic d'ITS.

    Defineix:
        - 'numerador': Conjunt d'identificadors amb serologia VIH realitzada.
    """
    global numerador
    numerador = set()

    sql = """
        SELECT
            id_cip_sec,
            dat
        FROM
            ass_variables
        WHERE
            ((agrupador = 388) OR (agrupador = 397) OR (agrupador = 398 AND val_num = 1))   -- Agrupadors per a serologia VIH
            AND dat BETWEEN DATE'{_data_ext_menys22mesos}' AND DATE'{_data_ext}'
        UNION
        SELECT
            id_cip_sec,
            dde
        FROM
            eqa_problemes
        WHERE
            ps = 101    -- Agrupador per a diagn�stic de VIH
            AND dde BETWEEN DATE'{_data_ext_menys22mesos}' AND DATE'{_data_ext}'
    """.format(_data_ext_menys22mesos=data_ext_menys22mesos, _data_ext=data_ext)
    for id_cip_sec, data_vih in u.getAll(sql, "nodrizas"):
        if id_cip_sec in denominador and any(data_dx - datetime.timedelta(days=30) <= data_vih <= data_dx + datetime.timedelta(days=180) for data_dx in dx_ITS[id_cip_sec]):
            numerador.add(id_cip_sec)

def create_resultats():
    """
    Genera els resultats finals per a l'indicador.

    Defineix:
        - 'resultats': Llista de tuples amb (id_cip_sec, numerador, denominador, excl).
    """
    global resultats
    resultats = [(id_cip_sec, 1 if id_cip_sec in numerador else 0, 1, 1 if id_cip_sec in exclusions else 0) for id_cip_sec in denominador]

def export_resultats():
    """
    Exporta els resultats a la base de dades.

    Crea una taula amb els resultats i la guarda a la base de dades definida ('DB_NAME').
    """
    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    u.listToTable(resultats, TB_NAME, DB_NAME)

if __name__ == '__main__':
    # Execuci� del c�lcul de l'indicador
    get_dextraccio();           print("get_dextraccio(): {}".format(datetime.datetime.now()))
    get_poblacio_ass();         print("get_poblacio_ass(): {}".format(datetime.datetime.now()))
    get_dx_ITS();               print("get_dx_ITS(): {}".format(datetime.datetime.now()))
    get_denominador();          print("get_denominador(): {}".format(datetime.datetime.now()))
    get_exclusions();           print("get_exclusions(): {}".format(datetime.datetime.now()))
    get_numerador();            print("get_numerador(): {}".format(datetime.datetime.now()))
    create_resultats();         print("create_resultats(): {}".format(datetime.datetime.now()))
    export_resultats();         print("export_resultats(): {}".format(datetime.datetime.now()))