import sisapUtils as u
import collections as c


class QISEQA_historic():
    def __init__(self):
        self.periode = 'A2407'
        self.get_data()
        self.upload_khalix()
    
    def get_data(self):
        indicadors = "EQA0230B,EQA0239A,EQA0316A,EQA0216A,EQA0222A,EQA0224A,EQA0238A,EQA0229A,EQA0232A,EQA0226A,EQA0231A"
        query = "{indicadors};{periode};AMBITOS#99;NUM,DEN;EDATS5;NOINSAT;SEXE"
        self.upload = c.Counter()
        for ind, aperiodo, br, analisi, edats, noinsat, sexe, value in u.LvClient().query(query.format(indicadors=indicadors, periode=self.periode)):
            indicador = 'QIS' + ind[:-1]
            self.upload[(br, indicador, analisi)] += int(float(value))
    
    def upload_khalix(self):
        cols = """(up varchar(5), indicador varchar(20), analisi varchar(3), n int)"""
        u.createTable('exp_khalix_qiseqa_up', cols, 'eqa_ind', rm=True)
        upload = [(br, indicador, analisi, n) for (br, indicador, analisi), n in self.upload.items()]
        u.listToTable(upload, 'exp_khalix_qiseqa_up', 'eqa_ind')
        sql = """select
                    indicador,
                    '{}',
                    up,
                    analisi,
                    'NOCAT',
                    'NOIMP',
                    'DIM6SET',
                    'N',
                    N
                from
                    eqa_ind.exp_khalix_qiseqa_up""".format(self.periode)
        u.exportKhalix(sql, 'QISEQA_{}'.format(self.periode), force=True)


class QISEQA():
    def __init__(self):
        self.get_data()
        self.upload_khalix()
    
    def get_data(self):
        sql = """select
                    up,
                    indicador,
                    conc,
                    sum(n)
                from
                    eqa_ind.exp_khalix_up_ind
                where
                    indicador in ('EQA0230B',
                'EQA0239A', 
                'EQA0316A',
                'EQA0216A',
                'EQA0222A',
                'EQA0224A',
                'EQA0238A',
                'EQA0229A',
                'EQA0232A',
                'EQA0226A',
                'EQA0231A',
                'EQA0228A',
                'EQA0313A')
                    and COMB in ('NOINSAT')
                group by
                    up,
                    indicador,
                    conc"""
        self.upload = []
        for up, indicador, analisi, n in u.getAll(sql, 'eqa_ind'):
            indicador = 'QIS' + indicador[:-1]
            self.upload.append((up, indicador, analisi, n))
        
        # sql = """select
        #             indicador,
        #             up,
        #             analisi,
        #             val
        #         from
        #             altres.vincat v
        #         where indicador in ('VINCAT03', 'VINCAT04', 'VINCAT05')"""
        # for indicador, up, analisi, n in u.getAll(sql, 'eqa_ind'):
        #     indicador = 'QISPROA' + indicador[-2:]
        #     self.upload.append((up, indicador, analisi, n))
    
    def upload_khalix(self):
        cols = """(up varchar(5), indicador varchar(20), analisi varchar(3), n int)"""
        u.createTable('exp_khalix_qiseqa_up', cols, 'eqa_ind', rm=True)
        u.listToTable(self.upload, 'exp_khalix_qiseqa_up', 'eqa_ind')
        sql = """select
                    indicador,
                    concat('A', 'periodo'),
                    ics_codi,
                    analisi,
                    'NOCAT',
                    'NOIMP',
                    'DIM6SET',
                    'N',
                    N
                from
                    eqa_ind.exp_khalix_qiseqa_up
                inner join NODRIZAS.CAT_CENTRES on
                    UP = scs_codi"""
        u.exportKhalix(sql, 'QISEQA', force=True)


if __name__ == "__main__":
    QISEQA()
    # QISEQA_historic()