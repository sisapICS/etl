# -*- coding: latin1 -*-

import collections as c
import sisapUtils as u
import math

validacio = True

nod = "nodrizas"
alt = 'altres'

pac = "mst_indicadors_indi"
tb_klx = 'exp_khalix_proavincat'


class PROA_VINCAT(object):
    """."""

    def __init__(self):
        """."""
        self.dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        self.year, self.month = self.dext.year, self.dext.month
        self.quarter = int(math.ceil(float(self.month) / 3))
        self.get_centres_agas()
        self.get_poblacio()
        self.get_diagnostics()
        self.get_tests()
        self.numeradors = c.defaultdict(set)
        self.denominadors = c.defaultdict(set)
        self.get_den_num()
        self.exclosos = c.defaultdict(set)
        self.get_resultats()
        self.export_khalix()
        #self.upload_pdp()

    def get_centres_agas(self):
        """."""
        self.centres = {}
        self.agas = set()
        self.centres_agas = {}
        sql = "select scs_codi, ics_codi, aga from cat_centres"
        for up, ics_codi, aga in u.getAll(sql, nod):
            self.centres[up] = {"ics_codi": ics_codi}
            self.agas.add(aga)
            self.centres_agas[up] = {"aga": aga}

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec,up,uba,upinf,ubainf,edat,ates,sexe,institucionalitzat,up_residencia \
               from assignada_tot \
               where maca = 0"
        self.poblacio = {id_cip_sec: {'up': up, 'uba': uba, 'upinf': upinf, 'ubainf': ubainf, 'edat': edat,
                                'ates': ates, 'sexe': sexe, 'insti': institucionalitzat, 'residencia': resi}
                         for (id_cip_sec, up, uba, upinf, ubainf, edat, ates, sexe, institucionalitzat, resi)
                         in u.getAll(sql, nod) if up in self.centres}

    def get_diagnostics(self):
        """."""

        self.diagnostics = set()

        codis_amigdalitis = ("C01-J02", "C01-J02.0", "C01-J02.8", "C01-J02.9", "C01-J03", "C01-J03.0", "C01-J03.00", "C01-J03.80", "C01-J03.9", "C01-J03.90")
        codis_amigdalitis_recurrrent_repeticio = ("C01-J03.91", "C01-J03.81", "C01-J03.01")
        codis_query = str(codis_amigdalitis + codis_amigdalitis_recurrrent_repeticio)

        sql = "select id_cip_sec \
               from import.{}, nodrizas.dextraccio \
               where pr_cod_ps in {} and pr_dde <= data_ext and \
               pr_dde > adddate(data_ext, interval -3 month)"
        jobs = [((sql.format(subtaula, codis_query), self.poblacio)) for subtaula in u.getSubTables("problemes")]

        for sub_diagnostics in u.multiprocess(sub_get_diagnostics, jobs, 4):
            self.diagnostics.update(sub_diagnostics)

    def get_tests(self):
        """."""
        agr_tests_ped = 760
        agr_tests_adults = 904
        self.tests = set()
        self.tests_positius = set()
        sql = ["select id_cip_sec, val \
               from ped_variables, dextraccio \
               where agrupador = {} and \
                     val < 9999 and \
                     dat <= data_ext and \
                     dat > adddate(data_ext, interval -3 month)".format(agr_tests_ped),
               "select id_cip_sec, valor \
                from eqa_variables, dextraccio \
                where agrupador = {} and \
                      valor < 9999 and \
                      data_var <= data_ext and \
                      data_var > adddate(data_ext, interval -3 month)".format(agr_tests_adults)]
        for query in sql:
            for id, valor in u.getAll(query, "nodrizas"):
                if id in self.poblacio:
                    self.tests.add(id)
                    if float(valor) == 1:
                        self.tests_positius.add(id)

    def get_den_num(self):
        """."""
        # Indicador VINCAT01: Num. TDR Realitzats / Num. Diagnostics FA/FAA
        self.denominadors['VINCAT01'] = self.diagnostics
        self.numeradors['VINCAT01']= self.tests

        # Indicador VINCAT02: Num. TDR Positius / Num. TDR Realitzats
        self.denominadors['VINCAT02'] = self.tests
        self.numeradors['VINCAT02'] = self.tests_positius

    def ageConverter_local(self,edat):
        if 0 <= edat <= 2:
            return 'E_00_02'
        elif 3 <= edat <= 11:
            return 'E_03_11'
        elif 12 <= edat <= 14:
            return 'E_12_14'
        elif 15 <= edat <= 44:
            return 'E_15_44'
        elif 45 <= edat <=75:
            return 'E_45_75'
        elif edat >= 75:
            return 'E_75_M'

    def get_resultats(self):
        """."""
        self.master = {}
        self.llistats = []
        self.ecap_resum = c.Counter()
        self.khalix_up = c.Counter()
        self.khalix_aga = c.Counter()

        for ind, pacients in self.denominadors.items():
            for id in pacients:
                up = self.poblacio[id]["up"]
                uba = self.poblacio[id]["uba"]
                ics_codi = self.centres[self.poblacio[id]["up"]]["ics_codi"]
                aga = self.centres_agas[self.poblacio[id]["up"]]["aga"]
                upinf = self.poblacio[id]["upinf"]
                ubainf = self.poblacio[id]["ubainf"]
                edat = self.poblacio[id]["edat"]
                sexe = self.poblacio[id]["sexe"]
                ates = self.poblacio[id]["ates"]
                insti = self.poblacio[id]["insti"]
                den = 1
                num = 1 if id in self.numeradors[ind] else 0
                excl = 0

                #Per taula self.master
                self.master[id, ind] = {"up": up, "uba": uba, "ics_codi": ics_codi, "aga": aga, "upinf": upinf,
                                        "ubainf": ubainf, "edat": edat, "sexe": sexe, "ates": ates, "insti": insti,
                                        "den": den, "num": num, "excl": excl}

                # Per taula ecap
                self.ecap_resum[(up, uba, ics_codi, aga, 'I', ind, 'NUM')] += num
                self.ecap_resum[(up, uba, ics_codi, aga, 'I', ind, 'DEN')] += den
                self.ecap_resum[(up, uba, ics_codi, aga, 'I', ind, 'EXCL')] = 0
                self.ecap_resum[(up, uba, ics_codi, aga, 'M', ind, 'NUM')] += num
                self.ecap_resum[(up, uba, ics_codi, aga, 'M', ind, 'DEN')] += den
                self.ecap_resum[(up, uba, ics_codi, aga, 'M', ind, 'EXCL')] = 0

                # Per taules de khalix up i aga
                edat= self.ageConverter_local(edat)
                self.khalix_up[(ics_codi, ind, edat, "NUM")] += num
                self.khalix_up[(ics_codi, ind, edat, "DEN")] += den
                self.khalix_aga[(aga, ind, edat, "NUM")] += num
                self.khalix_aga[(aga, ind, edat, "DEN")] += den

    def export_khalix(self):
        """."""
        cols = "(ent varchar(11), ind varchar(10), edat varchar(7), analisi varchar(3), valor int)"
        u.createTable(tb_klx + "_up", cols, alt, rm=True)
        upload = [k + (v,) for (k, v) in self.khalix_up.items()]
        u.listToTable(upload, tb_klx + "_up", alt)
        sql = "select ind, concat('A','periodo'), ent, analisi, edat, 'NOIMP', 'DIM6SET', 'N', valor \
               from {}.{} \
               where 5 and valor > 0".format(alt, tb_klx + "_up")
        u.exportKhalix(sql, "VINCAT_UP", quarter=True)

        u.createTable(tb_klx + "_aga", cols, alt, rm=True)
        upload = [k + (v,) for (k, v) in self.khalix_aga.items()]
        u.listToTable(upload, tb_klx + "_aga", alt)
        trim = "A" + str(self.year)[-2:] + "T" + str(self.quarter)
        sql = "select ind, concat('A','periodo'), ent, analisi, edat, 'NOIMP', 'DIM6SET', 'N', valor \
               from {}.{} \
               where valor > 0 and ent <> ''".format(alt, tb_klx + "_aga")
        u.exportKhalix(sql, "VINCAT_AGA", quarter=True)


            #    def upload_pdp(self):
        #"""."""
        #        pare = "INDI"
        #umiroot = "http://10.80.217.201/sisap-umi/indicador/codi/"
        #db = "pdp"
        ## resultats
        #tb = "altindicadors"
        #u.execute("delete from {} \
        #           where dataany = {} and \
        #                 datames = {} and \
        #                 indicador like '{}%'".format(tb, self.year,
        #                                              self.month, pare), db)
        #u.listToTable(self.indicadors, tb, db)
        ## llistats
        #tb = "altllistats"
        #u.execute("delete from {} \
        #           where indicador like '{}%'".format(tb, pare), db)
        #u.listToTable(self.llistats, tb, db)
        ## cataleg
        #tb = "altcataleg"
        #literals = {"AP35": "Avaluaci� de risc de su�cidi"}
        #upload = []
        #for i, indicador in enumerate(sorted(self.denominador)):
        #    this = (self.year, indicador, literals[indicador], i + 1, pare,
        #            1, 0, 0, 0, 0, 1,
        #            umiroot + indicador, "PCT")
        #    upload.append(this)
        #u.execute("delete from {} \
        #           where dataany = {} and \
        #                 pare = '{}'".format(tb, self.year, pare), db)
        #u.listToTable(upload, tb, db)
        ## cataleg pare
        #tb = "altcatalegpare"
        #upload = [(self.year, pare, "Estudi de recerca INDI", 1, "RECERCA")]
        #u.execute("delete from {} \
        #           where dataany = {} and \
        #                 pare = '{}'".format(tb, self.year, pare), db)
        #u.listToTable(upload, tb, db)


def sub_get_diagnostics(input):
    """."""

    sql, poblacio = input
    sub_diagnostics = set()

    for id, in u.getAll(sql, "nodrizas"):
        if id in poblacio:
            sub_diagnostics.add(id)
    
    return sub_diagnostics

if __name__ == "__main__":
    if u.IS_MENSUAL:
        sql = "select month(data_ext) from nodrizas.dextraccio"
        for m, in u.getAll(sql, 'nodrizas'):
            month = m
        if month in (3,6,9,12):
            PROA_VINCAT()
            