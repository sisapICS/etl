# -*- coding: utf-8 -*-

import sisapUtils as u
import collections as c


class Khalix():
    def __init__(self):
        self.create_table()
        self.create_indicators()
        self.upload()
        self.to_khalix()

    def create_table(self):
        self.table_up = 'exp_khalix_reso_mapa_up'
        self.db = 'altres'
        cols = '(indicador varchar(20), entity varchar(20), analisi varchar(20), edat varchar(20), pob varchar(20), sexe varchar(20), N varchar(2), val int)'
        u.createTable(self.table_up, cols, self.db, rm=True)
        self.table_uba = 'exp_khalix_reso_mapa_uba'
        self.db = 'altres'
        cols = '(indicador varchar(20), entity varchar(20), analisi varchar(20), edat varchar(20), pob varchar(20), sexe varchar(20), N varchar(2), val int)'
        u.createTable(self.table_uba, cols, self.db, rm=True)

    def create_indicators(self):
        self.indicadors_up = c.Counter()
        self.indicadors_uba = c.Counter()
        sql = """select
                    id_cip_sec,
                    ics_codi,
                    uba,
                    ics_codi,
                    ubainf,
                    edat,
                    sexe,
                    ates,
                    institu,
                    diabetis,
                    hipotiroidisme,
                    endocri,
                    trauma,
                    uro,
                    derma,
                    oftal,
                    otorrino,
                    cardio
                from
                    altres.mst_resolucio_mapa,
                    nodrizas.cat_centres
                where
                    scs_codi = up
                """
        pacients = {}
        for (id_cip_sec, up, uba, upinf, 
                ubainf, edat, sexe, ates, institu, 
                diabetis, hipotiroidisme, endocri, 
                trauma, uro, derma, oftal, otorrino, 
                cardio) in u.getAll(sql, 'altres'):
            pacients[id_cip_sec] = (up, uba, upinf, ubainf, edat, sexe, ates, institu, 
                                diabetis, hipotiroidisme, endocri, trauma, uro, derma, 
                                oftal, otorrino, cardio)
        for pacient in pacients:
            up, uba, upinf, ubainf, edat, sexe, ates, institu, diabetis, hipotiroidisme, endocri, trauma, uro, derma, oftal, otorrino, cardio = pacients[pacient]
            entity_m = up + 'M' + uba
            entity_i = upinf + 'I' + ubainf
            poblacions = set()
            if ates == 1:
                if institu == 0: poblacions.add('NOINSAT')
                elif institu == 1: poblacions.add('INSAT')
            if institu == 0: poblacions.add('NOINSASS')
            elif institu == 1: poblacions.add('INSASS')
            sexe = u.sexConverter(sexe)
            edat = u.ageConverter(int(edat))

            for pob in poblacions:
                # CONSEXT001
                # Per UBA: passem NOINSAT per� ni edat ni sexe
                if pob == 'NOINSAT':
                    for indicador in ('CONSEXT001A', 'CONSEXT001B', 'CONSEXT001C', 'CONSEXT001D', 'CONSEXT001E', 'CONSEXT001F','CONSEXT001G'):
                        self.indicadors_uba[indicador, entity_m, 'DEN', 'NOCAT', pob, 'DIM6SET', 'N'] += 1
                        self.indicadors_uba[indicador, entity_i, 'DEN', 'NOCAT', pob, 'DIM6SET', 'N'] += 1
                    indis = [('CONSEXT001A', endocri),
                                ('CONSEXT001B', trauma), 
                                ('CONSEXT001C', uro), 
                                ('CONSEXT001D', derma), 
                                ('CONSEXT001E', oftal), 
                                ('CONSEXT001F', otorrino),
                                ('CONSEXT001G', cardio)]
                    for pairs in indis:
                        if pairs[1] == 1:
                            self.indicadors_uba[pairs[0], entity_m, 'NUM', 'NOCAT', pob, 'DIM6SET', 'N'] += 1
                            self.indicadors_uba[pairs[0], entity_i, 'NUM', 'NOCAT', pob, 'DIM6SET', 'N'] += 1

                # Per UP: passem poblaci�, edat i sexe
                for indicador in ('CONSEXT001A', 'CONSEXT001B', 'CONSEXT001C', 'CONSEXT001D', 'CONSEXT001E', 'CONSEXT001F','CONSEXT001G'):
                    self.indicadors_up[indicador, up, 'DEN', edat, pob, sexe, 'N'] += 1
                indis = [('CONSEXT001A', endocri),
                            ('CONSEXT001B', trauma),
                            ('CONSEXT001C', uro),
                            ('CONSEXT001D', derma),
                            ('CONSEXT001E', oftal),
                            ('CONSEXT001F', otorrino),
                            ('CONSEXT001G', cardio)]
                for pairs in indis:
                    if pairs[1] == 1:
                        self.indicadors_up[pairs[0], up, 'NUM', edat, pob, sexe, 'N'] += 1
                
                # CONSEXT002
                # Per UBA: passem NOINSAT per� ni edat ni sexe
                if diabetis == 1:
                    self.indicadors_up['CONSEXT002AA', up, 'DEN', edat, pob, sexe, 'N'] += 1
                    if pob == 'NOINSAT':
                        self.indicadors_uba['CONSEXT002AA', entity_m, 'DEN', 'NOCAT', pob, 'DIM6SET', 'N'] += 1
                        self.indicadors_uba['CONSEXT002AA', entity_i, 'DEN', 'NOCAT', pob, 'DIM6SET', 'N'] += 1
                    if endocri == 1:
                        self.indicadors_up['CONSEXT002AA', up, 'NUM', edat, pob, sexe, 'N'] += 1
                        if pob == 'NOINSAT':
                            self.indicadors_uba['CONSEXT002AA', entity_m, 'NUM', 'NOCAT', pob, 'DIM6SET', 'N'] += 1
                            self.indicadors_uba['CONSEXT002AA', entity_i, 'NUM', 'NOCAT', pob, 'DIM6SET', 'N'] += 1
                if hipotiroidisme == 1:
                    self.indicadors_up['CONSEXT002AB', up, 'DEN', edat, pob, sexe, 'N'] += 1
                    if pob == 'NOINSAT':
                        self.indicadors_uba['CONSEXT002AB', entity_m, 'DEN', 'NOCAT', pob, 'DIM6SET', 'N'] += 1
                        self.indicadors_uba['CONSEXT002AB', entity_i, 'DEN', 'NOCAT', pob, 'DIM6SET', 'N'] += 1
                    if endocri == 1:
                        self.indicadors_up['CONSEXT002AB', up, 'NUM', edat, pob, sexe, 'N'] += 1
                        if pob == 'NOINSAT':
                            self.indicadors_uba['CONSEXT002AB', entity_m, 'NUM', 'NOCAT', pob, 'DIM6SET', 'N'] += 1
                            self.indicadors_uba['CONSEXT002AB', entity_i, 'NUM', 'NOCAT', pob, 'DIM6SET', 'N'] += 1
                    
    def upload(self):
        upload_uba = []
        for (indi, entity, analisi, edat, pob, sexe, n), val in self.indicadors_uba.items():
            upload_uba.append((indi, entity, analisi, edat, pob, sexe, n, val))
        u.listToTable(upload_uba, self.table_uba, self.db)

        upload_up = []
        for (indi, entity, analisi, edat, pob, sexe, n), val in self.indicadors_up.items():
            upload_up.append((indi, entity, analisi, edat, pob, sexe, n, val))
        u.listToTable(upload_up, self.table_up, self.db)

    def to_khalix(self):
        sql = """select indicador, concat('A','periodo'), entity, analisi, edat, pob, sexe, N, val from altres.exp_khalix_reso_mapa_up"""
        u.exportKhalix(sql, 'CONSEXT')
        sql = """select indicador, concat('A','periodo'), entity, analisi, edat, pob, sexe, N, val from altres.exp_khalix_reso_mapa_uba"""
        u.exportKhalix(sql, 'CONSEXT_UBA')

if __name__ == "__main__":
    Khalix()


