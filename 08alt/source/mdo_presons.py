# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
import os

import sisapUtils as u


db = '6951'

cols = "(dataany int, codi int, malaltia varchar(25), valor int)"
u.createTable('mdo_presons', cols, 'altres', rm=True)

class MDO(object):
    """."""

    def __init__(self):
        """."""
        u.printTime('inici')
        self.get_malalties()
        u.printTime('malalties')
        self.get_numeriques()
        u.printTime('numeriques')
        self.get_individuals()
        u.printTime('individuals')
        self.upload()
        u.printTime('fi')

    def upload(self):
        dades = [(year, mdo, self.malalties.get(mdo), n)
                 for (year, mdo, n)
                 in sorted(self.numeriques + self.individuals)]
        u.listToTable(dades, 'mdo_presons', 'altres')
        u.listToTable(self.numeriques, 'mdo_presons', 'altres')

    def get_malalties(self):
        """."""
        sql = "select md_cod, md_des from prstb020 where md_cod in (48,49,13,18,21,35,33,3)"
        self.malalties = {cod: des for (cod, des) in u.getAll(sql, db)}

    def get_numeriques(self):
        """."""
        numeriques = c.Counter()
        sql = "select mdn_any, \
                      mdn_codi_mdo, \
                      mdn_quantitat \
               from prstba15 \
               where mdn_any in (2019,2020,2021,2022,2023) \
               and mdn_codi_mdo in (48,49,13,18,21,35,33,3)"
        for year, malaltia, n in u.getAll(sql, db):
            numeriques[(year, malaltia)] += n
        self.numeriques = [(year, malaltia, n)
                           for (year, malaltia), n
                           in numeriques.items()]

    def get_individuals(self):
        """."""
        sql = "select mdi_pr_cod_u, mdi_codi_mdi, to_char(mdi_data_ins, 'YYYY') \
               from prstb315 \
               where mdi_tipus_mov = 'A' \
               and to_char(mdi_data_ins, 'YYYY') in ('2019','2020','2021', '2022', '2023') \
               and mdi_codi_mdi in (48,49,13,18,21,35,33,3)"
        individuals = c.defaultdict(set)
        for pacient, malaltia, year in u.getAll(sql, db):
            malaltia = int(malaltia)
            individuals[(year, malaltia)].add(pacient)
        self.individuals = [(year, malaltia, len(pacients))
                            for (year, malaltia), pacients
                            in individuals.items()]

if __name__ == '__main__':
    MDO()