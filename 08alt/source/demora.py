import sisaptools as u
import sisapUtils as u2


SQLS = [
    ("DEMORA", "select 'DEMORA'||visita||'F', \
                       'A'||substr(year, 3, 2)||lpad(month, 2, 0), b.br, \
                       'AGRESULT', 'NOCAT', 'TIPPROF'||tipprof, 'DIM6SET', \
                       'N', resultat \
                from demora a \
                inner join dbc_centres_tots b on a.up = b.up_cod \
                where nivell = 'up' and year = {} and month <= {} and \
                      br is not null and es_penitenciari = 0", True),
    ("DEMORA_UBA", "select 'DEMORA'||visita||'F', \
                           'A'||substr(year, 3, 2)||lpad(month, 2, 0), \
                           b.br||tipus||uba, 'AGRESULT', 'NOCAT', 'NOIMP', \
                           'DIM6SET', 'N', min(resultat) \
                from demora a \
                inner join dbc_centres_tots b on a.up = b.up_cod \
                where nivell = 'uba' and year = {} and month <= {} and \
                      br is not null and es_penitenciari = 0 \
                group by 'DEMORA'||visita||'F', \
                          'A'||substr(year, 3, 2)||lpad(month, 2, 0), \
                           b.br||tipus||uba", True),
    ("EXP_QC_DEMORA", "select 'QDEMORA'||visita||'F', \
                              'A'||substr(year, 3, 2)||lpad(month, 2, 0), \
                               b.br, 'AGRESULT', \
                               case when year = {0} and month = {1} \
                                    then  'ACTUAL' else 'PREVI' end, \
                               'TIPPROF'||tipprof, 'DIM6SET', 'N', resultat \
                       from demora a \
                       inner join dbc_centres_tots b on a.up = b.up_cod \
                       where nivell = 'up' and br is not null and \
                             es_penitenciari = 0 and \
                             visita in ('9C', '9T') and \
                             ((year = {0} and month = {1}) or \
                              (year = {2} and month = {3}))", False)    
]

sql = "select data_ext, adddate(data_ext, interval -1 month) from dextraccio"
dext, fa1m = u.Database("p2262", "nodrizas").get_one(sql)
year, month = dext.year, dext.month
year_fa1m, month_fa1m = fa1m.year, fa1m.month

cols = ["k{} varchar(16)".format(i) for i in range(8)] + ["v decimal(3,1)"]
with u.Database("exadata", "data") as exadata:
    with u.Database("p2262", "altres") as p2262:
        for file, sql, export in SQLS:
            dades = list(exadata.get_all(sql.format(year, month, year_fa1m, month_fa1m)))  # noqa
            p2262.create_table(file, cols, remove=True)
            p2262.list_to_table(dades, file)
            if export:
                u2.exportKhalix("select * from altres.{}".format(file), file)
