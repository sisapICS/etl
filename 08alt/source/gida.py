# -*- coding: latin1 -*-

"""
.
"""

import collections as c
from ctypes import Union
import dateutil.relativedelta as r

import sisapUtils as u


MASTER_TB = "mst_gida"
KHALIX_TB = "exp_khalix_gida"
KHALIX_UP = "GIDA";                         KHALIX_UP_JAIL = KHALIX_UP + "PRS"
KHALIX_UBA = "GIDA_UBA";                    KHALIX_UBA_JAIL = "GIDA_PRS_UBA"
CORTES_TB = "exp_khalix_gida_mensual"
CORTES_UP = "GIDA_MENSUAL";                 CORTES_UP_JAIL = "GIDA_PRS_MENSUAL"
QC_TB = "exp_qc_gida"
MEDICINA_TB = "exp_khalix_gida_med"
MEDICINA_UP = "GIDA_MED"
DATABASE = "altres"

TAXES = {
        "PABCESA": 6.13,
        "PABCESAP": 0.21,
        "PAFTAA": 0.69,
        "PANALSA": 0.06,
        "PANALSAP": 0.04,
        "PANTIC": 0.28,
        "PANSIE": 4.02,
        "PAPZB": 0.56,
        "PCEFALA": 5.92,
        "PCERVI": 3.13,
        "PCOLIC": 0.07,
        "PCONTD": 1.22,
        "PCONTU": 6.41,
        "PCOSEN": 0.04,
        "PCOSEO": 0.14,
        "PCREM": 2.38,
        "PCREPE": 0.32,
        "PDIAGN": 1.02,
        "PDIARR": 12.9,
        "PDISMEA": 0.16,
        "PDISMEAP": 0.03,
        "PDML": 10.07,
        "PEPIS": 0.42,
        "PEPISN": 0.05,
        "PFEBAN": 4.35,
        "PFEBSF": 4.47,
        "PFERID": 23.63,
        "PFERNE": 4.43,
        "PHERPLA": 0.18,
        "PINSOMA": 0.12,
        "PLDERM": 1.21,
        "PLESC": 0.85,
        "PMAREI": 12.39,
        "PMOLOR": 17.2,
        "PMUCO": 1.7,
        "PMUGN": 0.03,
        "PMULLP": 0.82,
        "PMURI": 34.67,
        "PMURIAP": 0.32,
        "PMVAGA": 1.31,
        "PODINO": 27.82,
        "PODON": 1.31,
        "PPICIN": 0.78,
        "PPICPE": 3.29,
        "PPLOIN": 0.04,
        "PPOLLS": 0.03,
        "PPRESA": 5.47,
        "PREGUR": 0.05,
        "PRESTN": 0.25,
        "PRESTREA": 0.66,
        "PRESVA": 60.34,
        "PREVACA": 0.14,
        "PREVACAP": 0.07,
        "PSCOVA": 0.59,
        "PSCOVAP": 0.05,
        "PSOSVA": 0.04,
        "PTOR�AAP": 0.37,
        "PTOS": 3.44,
        "PTURME": 1.22,
        "PULL": 5.96,
        "PUNGA": 1.01,
        "PURTIA": 2.69,
        "PVOLTA": 1.10,
        "PVOMIT": 13.45,
        "PVOMNE": 1.34,
        "PRAOADU": 0.2,
        "PRAOPED": 0.03,
        "PACEI": 0.08,
        "PVOLTAP": 0.14,
        "PUNGAP": 0.16,
        "PHERPAP": 0.04,
        "PAFTAAP": 0.21,
        "PODINAP": 2.34,
        "PODONTAP": 0.07,
        "PMELICAP": 0.08,
        "PDOLMA": 0,
        "PGRIPA": 0
    }

CONVERSIO = {"ACE_A": "ANTIC", "CERVI_A": "CERVI", "CONT_A": "CONTU",
             "CREM_A": "CREM", "C_ANSI_A": "ANSIE", "DIARR_A": "DIARR",
             "LUMB_A": "DML", "EPA_A": "PRESA", "ENTOR_A": "TURME",
             "EPIST_A": "EPIS", "FEBRE_A": "FEBSF", "FERIDA_A": "FERID",
             "PLEC_A": "LDERM", "MAREIG_A": "MAREI", "M_OID_A": "MOLOR",
             "M_ULL_A": "ULL", "M_URI_A": "MURI", "ODIN_A": "ODINO",
             "ODONT_A": "ODON", "PICAD_A": "PICPE", "SRVA_A": "RESVA",
             "VOMIT_A": "VOMIT", "ALT_A": "A", "CEFAL": "CEFAL_A",
             "HERP": "HERPL_A", "ABCES": "ABCES_A", "UNG": "UNG_A",
             "VOLT": "VOLT_A", "RESTR": "RESTRE_A", "MVAG": "MVAG_A",
             "REVAC": "REVAC_A", "AFTA": "AFTA_A", "INSOM": "INSOM_A",
             "URTI": "URTI_A", "LESFC_A": "LESC",
             "COLIC_AP": "COLIC", "CON_D_AP": "CONTD", "CEORE_AP": "COSEO",
             "CENAS_AP": "COSEN", "CREMA_AP": "CREPE", "DER_B_AP": "APZB",
             "DIARR_AP": "DIAGN", "EPIST_AP": "EPISN", "FEBRE_AP": "FEBAN",
             "FERID_AP": "FERNE", "MOCS_AP": "MUCO", "PICAD_AP": "PICIN",
             "PLORS_AP": "PLOIN", "POLLS_AP": "POLLS", "REGUR_AP": "REGUR",
             "RESTR_AP": "RESTN", "MUGUE_AP": "MUGN", "VARIC_AP": "SOSVA",
             "TOS_AP": "TOS", "VOMIT_AP": "VOMNE"}

# Descomentar las UPs a medida que las vayan migrando.
UP_MODS = {
    '07446': '08430',
    '05093': '08434',
    '07537': '08431',
    '05092': '08435',
    '05826': '08432',
    '05091': '08438',
    '07538': '08436',
    '06164': '08433',
    '05090': '08439',
    }


class GIDA(object):
    """."""

    def __init__(self):
        """."""
        self.recomptes = c.defaultdict(lambda: c.defaultdict(c.Counter))
        self.recomptes_med = c.defaultdict(lambda: c.defaultdict(c.Counter))
        self.mensual = c.Counter()
        self.get_edats()
        self.get_poblacio()
        self.get_centres()
        self.get_visites()
        self.get_usu_to_col()
        self.get_motius()
        self.get_col_to_uba()
        self.get_aguda()
        self.get_master()
        self.get_derivats()
        self.export_master()
        self.get_ubas()
        self.get_khalix()
        self.get_taxes()
        self.export_medicina()
        self.export_khalix()
        self.export_mensual()
        self.export_qc()
        self.export_ecap()

    def get_edats(self):
        """."""
        sql = """
                SELECT
                    id_cip_sec,
                    (YEAR(data_ext) - YEAR(usua_data_naixement)) - (date_format(data_ext, '%m%d') < date_format(usua_data_naixement, '%m%d'))
                FROM
                    import.assignada,
                    nodrizas.dextraccio
              UNION
                SELECT
                    id_cip_sec,
                    (YEAR(data_ext) - YEAR(usua_data_naixement)) - (date_format(data_ext, '%m%d') < date_format(usua_data_naixement, '%m%d'))
                FROM
                    import_jail.assignada,
                    nodrizas.dextraccio
              """
        self.edats = {id: "ADU" if edat > 14 else "PED"
                      for (id, edat) in u.getAll(sql, "import")}

    def get_poblacio(self):
        """."""
        self.poblacio = {}
        sql = "select id_cip_sec, up, ubainf, uba \
               from assignada_tot_with_jail where ates = 1"
        for id, up, uba, uba_m in u.getAll(sql, "nodrizas"):
            grup = self.edats[id]
            for ind in ("GESTINF05", "GESTINF06"):
                self.recomptes[ind][(up, uba, "UBA", grup)]["DEN"] += 1
                self.recomptes_med[ind][(up, uba_m, grup)]["DEN"] += 1
            self.poblacio[id] = (up, uba, uba_m)

    def get_centres(self):
        """."""
        sql = """select scs_codi, tip_eap, ics_codi from cat_centres
                union
                 select scs_codi, 'PRS', ics_codi from jail_centres
              """
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}
        self.jail_centres_up = tuple(row[0] for row in u.getAll(sql, "nodrizas") if row[1] == 'PRS')
        self.jail_centres_br = tuple(row[2] for row in u.getAll(sql, "nodrizas") if row[1] == 'PRS')

    def get_visites(self):
        """."""
        sqls = ("select id_cip_sec, visi_up, left(visi_col_prov_resp, 8) \
               from visites1, nodrizas.dextraccio \
               where visi_col_prov_resp like '3%' and \
                     s_espe_codi_especialitat not in ('EXTRA', '10102') and \
                     visi_tipus_visita not in ('9E', 'EXTRA', 'EXT') and \
                     visi_situacio_visita = 'R' and \
                     visi_servei_codi_servei in \
                        ('INF', 'INFPD', 'INFP', 'INFPE', 'INFG') and \
                     visi_modul_codi_modul not like 'VCA%' and \
                     visi_modul_codi_modul not like 'VCM%' and \
                     visi_modul_codi_modul not like 'VCP%' and \
                     visi_modul_codi_modul not like 'VCJ%' and \
                     visi_modul_codi_modul not in ('VCAP', 'VCAP2', 'VCAP3') and \
                     visi_data_visita between adddate(data_ext, interval -1 year) and data_ext",
                "select id_cip_sec, visi_up, left(visi_col_prov_resp, 8) \
                from import_jail.visites, nodrizas.dextraccio \
                where visi_col_prov_resp like '3%' and \
                        s_espe_codi_especialitat not in ('EXTRA', '10102') and \
                        visi_tipus_visita not in ('9E', 'EXTRA', 'EXT') and \
                        visi_situacio_visita = 'R' and \
                        visi_servei_codi_servei in \
                            ('INF', 'INFPD', 'INFP', 'INFPE', 'INFG') and \
                        visi_modul_codi_modul not like 'VCA%' and \
                        visi_modul_codi_modul not like 'VCM%' and \
                        visi_modul_codi_modul not like 'VCP%' and \
                        visi_modul_codi_modul not like 'VCJ%' and \
                        visi_modul_codi_modul not in ('VCAP', 'VCAP2', 'VCAP3') and \
                        visi_data_visita between adddate(data_ext, interval -1 year) and data_ext",)
        for sql in sqls:
            for id, up, col in u.getAll(sql, "import"):
                up = UP_MODS.get(up, up)
                if up in self.centres and id in self.edats:
                    grup = self.edats[id]
                    tipus = self.centres[up][0]
                    go = (tipus == "M",
                        tipus == "PRS",
                        (grup == "ADU" and tipus == "A"),
                        (grup == "PED" and tipus == "N"))
                    if any(go):
                        for ind in ("GESTINF03", "GESTINF04"):
                            self.recomptes[ind][(up, col, "COL", grup)]["DEN"] += 1

    def get_usu_to_col(self):
        """."""
        sql = "select codi_sector, ide_usuari, left(ide_numcol, 8) \
               from cat_pritb992 \
               where ide_numcol <> ''"
        self.usu_to_col = {row[:2]: row[2] for row in u.getAll(sql, "import")}

    def get_motius(self):
        """."""
        sql = """select sym_name
                 from icsap.klx_master_symbol
                 where dim_index = 4 and (
                    sym_index in (
                        select sym_index from icsap.klx_parent_child
                        where dim_index = 4 and parent_index in (
                            select sym_index from icsap.klx_master_symbol
                            where dim_index = 4 and
                                  sym_name in ('PMOTIUPRPE', 'PMOTIUTEL')
                             )
                        ) or
                    sym_name = 'PALPED')"""
        self.motius = {motiu: "PED" for motiu, in u.getAll(sql, "khalix")}

    def _get_motiu(self, motiu, grup):
        """."""
        motiu = CONVERSIO.get(motiu, motiu)
        if motiu == "A":
            motiu = "PAL{}".format(grup)
        elif motiu in ("RAO_A", "RAO_I"):
            motiu = "PRAO{}".format(grup)
        else:
            motiu = "P{}".format(motiu.replace("_", ""))
        if motiu == 'PACEAP': motiu = 'PACEI'
        elif motiu == 'PRAOAP': motiu = 'PRAOPED'
        elif motiu == 'PMULLAP': motiu = 'PMULLP'
        if motiu not in self.motius:
            self.motius[motiu] = "ADU"
        return motiu

    def get_aguda(self):
        """."""
        self.motius_nous = set()
        validacio_elisabet = []
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        first = (dext - r.relativedelta(months=1)).replace(day=1)
        sql = "select id_cip_sec, codi_sector, cesp_up, cesp_usu_alta, \
                      cesp_cod_mce, cesp_estat, cesp_data_alta \
               from aguda, nodrizas.dextraccio \
               where cesp_data_alta between \
                            adddate(data_ext, interval -1 year) and data_ext \
               union \
               select id_cip_sec, codi_sector, pi_up, pi_usu_alta, \
                      upper(replace(pi_anagrama, ' ', '')), \
                      if(pi_ser_derivat = '', 'I', 'M'), pi_data_alta \
               from ares, nodrizas.dextraccio \
               where pi_anagrama not in ('', 'TAP_A') and \
                     pi_data_alta between \
                            adddate(data_ext, interval -1 year) and data_ext"
        for db in ("import", "import_jail"):
            for id, sec, up, usu, motiu, estat, dat in u.getAll(sql, db):
                if id in self.edats:
                    if motiu not in CONVERSIO:
                        self.motius_nous.add(motiu)
                    grup = self.edats[id]
                    motiu = self._get_motiu(motiu, grup)
                    motiu_tip = self.motius[motiu]
                    if motiu == 'PACEAP': motiu = 'PACEI'
                    elif motiu == 'PRAOAP': motiu = 'PRAOPED'
                    elif motiu == 'PMULLAP': motiu = 'PMULLP'
                    if grup == motiu_tip:
                        up = UP_MODS.get(up, up)
                        if id in self.poblacio:
                            up_pac, uba, uba_m = self.poblacio[id]
                            if up == up_pac:
                                key = (up, uba, "UBA", grup)
                                key_m = (up, uba_m, grup)
                                self.recomptes["GESTINF05"][key][motiu] += 1
                                self.recomptes_med["GESTINF05"][key_m][motiu] += 1
                                if estat == "I":
                                    self.recomptes["GESTINF06"][key][motiu] += 1
                                    self.recomptes_med["GESTINF06"][key_m][motiu] += 1  # noqa
                                if dat >= first:
                                    key = ("B" + dat.strftime("%y%m"), up, motiu)
                                    self.mensual[("GESTINF05",) + key] += 1
                                    if estat == "I":
                                        self.mensual[("GESTINF06",) + key] += 1
                        col = self.usu_to_col.get((sec, usu))
                        key = (up, col, "COL", grup)
                        if key in self.recomptes["GESTINF03"]:
                            self.recomptes["GESTINF03"][key][motiu] += 1
                            if id in self.poblacio:
                                up_pac, uba, uba_m = self.poblacio[id]
                                key_8 = (up, uba, "UBA", "MIX")
                                equals = 0
                                if up == up_pac:
                                    # mirem totes les ubas de l'infermer
                                    # si hi ha alguna uba que sigui la q ha tractat el pacient
                                    ubas = self.col_to_uba[(up, col)]
                                    for ubi in ubas:
                                        if uba ==  ubi:
                                            self.recomptes["GESTINF08"][key_8]["NUM"] += 1
                                            equals = 1
                                self.recomptes["GESTINF08"][key_8]["DEN"] += 1
                                ubas = self.col_to_uba[(up, col)]
                                if (up == '00108' and 'INFV' in ubas) or (up == '00108' and uba =='INFV'):
                                    ubas = str(ubas)
                                    validacio_elisabet.append((id, uba, ubas, equals, up, up_pac, up, col))
                            if estat == "I":
                                self.recomptes["GESTINF04"][key][motiu] += 1
                            if dat >= first:
                                key = ("B" + dat.strftime("%y%m"), up, motiu)
                                self.mensual[("GESTINF03",) + key] += 1
                                if estat == "I":
                                    self.mensual[("GESTINF04",) + key] += 1
        cols = '(id_cip_sec int, uba_assignada varchar(5), ubas_prof varchar(100), ok int, up_ates varchar(5), up_pacient varchar(5), up_col varchar(10), coleg varchar(200))'
        u.createTable('gestinf08_elisabet', cols, 'altres', rm=True)
        u.listToTable(validacio_elisabet, 'gestinf08_elisabet', 'altres')

    def mail_motius(self):
        dext, = u.getOne("SELECT data_ext FROM dextraccio", "nodrizas")
        if len(self.motius_nous) > 0:
            upload = []
            for m in self.motius_nous:
                upload.append((m, dext))
            file_eap = u.tempFolder + 'Motius_nous_GIDA.csv'
            u.writeCSV(file_eap, [('MOTIU', 'DIA')] + upload, sep=";")
            # email
            subject = 'Nous motius GIDA'
            text = "Bon dia.\n\nAdjuntem arxiu .csv amb nous motius (anagrames) GIDA.\n"
            text += "\n\nSalutacions."

            u.sendGeneral('SISAP <sisap@gencat.cat>',
                ('roser.cantenys@catsalut.cat', 'sflayeh.bnm.ics@gencat.cat'),
                ('roser.cantenys@catsalut.cat', 'sflayeh.bnm.ics@gencat.cat'),
                subject,
                text,
                file_eap)

    def get_master(self):
        """."""
        self.master = c.defaultdict(c.Counter)
        for ind, dades in self.recomptes.items():
            for (up, prof, tip, grup), minidades in dades.items():
                if ind != 'GESTINF08':
                    den = minidades["DEN"]
                    for motiu, motiu_tip in self.motius.items():
                        if motiu_tip == grup:
                            key = (ind, up, prof, tip, grup, motiu)
                            self.master[key]["DEN"] = den
                            self.master[key]["NUM"] = minidades[motiu]
                else:
                    key = (ind, up, prof, tip, grup, 'TIPMOTIU')
                    self.master[key]["DEN"] = minidades['DEN']
                    self.master[key]["NUM"] = minidades['NUM'] if 'NUM' in minidades else 0

    def get_derivats(self):
        """."""
        for (ind, up, prof, tip, grup, motiu), dades in self.master.items():
            if ind == "GESTINF03":
                key = ("GESTINF02", up, prof, tip, "MIX", "TIPMOTIU")
                self.master[key]["DEN"] += dades["NUM"]
                if motiu not in ("PALADU", "PALPED"):
                    self.master[key]["NUM"] += dades["NUM"]
                key = ("GESTINF02A", up, prof, tip, "MIX", "TIPMOTIU")
                self.master[key]["DEN"] += dades["NUM"]
                if motiu in ("PALADU", "PALPED"):
                    self.master[key]["NUM"] += dades["NUM"]
            if ind in ("GESTINF03", "GESTINF04"):
                key = ("GESTINF07", up, prof, tip, grup, motiu)
                analysis = "DEN" if ind == "GESTINF03" else "NUM"
                self.master[key][analysis] = dades["NUM"]
                if motiu not in ("PALADU", "PALPED"):
                    key = ("GESTINF07A", up, prof, tip, grup, motiu)
                    analysis = "DEN" if ind == "GESTINF03" else "NUM"
                    self.master[key][analysis] = dades["NUM"]
                if motiu in ("PALADU", "PALPED"):
                    key = ("GESTINF07B", up, prof, tip, grup, motiu)
                    analysis = "DEN" if ind == "GESTINF03" else "NUM"
                    self.master[key][analysis] = dades["NUM"]

    def export_master(self):
        """."""
        u.createTable(MASTER_TB, "(ind varchar(10), up varchar(5), \
                                   prof varchar(10), tip varchar(3), \
                                   grup varchar(3), motiu varchar(10), \
                                   num int, den int)",
                      DATABASE, rm=True)
        upload = [k + (v["NUM"], v["DEN"]) for (k, v) in self.master.items()]
        u.listToTable(upload, MASTER_TB, DATABASE)

    def get_col_to_uba(self):
        """Per passar dades individuals a khalix dels indicadors de NUMCOL."""
        self.col_to_uba = c.defaultdict(set)
        sql = "select left(ide_numcol, 8), up, uab \
               from cat_professionals \
               where tipus = 'I'"
        for col, up, uba in u.getAll(sql, "import"):
            self.col_to_uba[(up, col)].add(uba)

    def get_ubas(self):
        """Nom�s pujarem UBAs amb EQA."""
        self.ubas = c.defaultdict(set)
        for db, grup in (("eqa_ind", "ADU"), ("pedia", "PED")):
            sql = "select up, uba from mst_ubas where tipus = 'I'"
            for key in u.getAll(sql, db):
                self.ubas[grup].add(key)
                self.ubas["MIX"].add(key)

    def get_khalix(self):
        """."""
        self.khalix = c.Counter()
        for (ind, up, prof, tip, grup, motiu), dades in self.master.items():
            br = self.centres[up][1]
            for analisi in ("NUM", "DEN"):
                self.khalix[(br, ind, motiu, analisi)] += dades[analisi]
            if tip == "COL":
                ubas = self.col_to_uba[(up, prof)]
            else:
                ubas = (prof,)
            for uba in ubas:
                if (up, uba) in self.ubas[grup]:
                    ent = "{}I{}".format(br, uba)
                    for analisi in ("NUM", "DEN"):
                        self.khalix[(ent, ind, motiu, analisi)] += dades[analisi]  # noqa

    def get_taxes(self):
        """."""
        for (ent, ind, motiu, analisi), n in self.khalix.items():
            if ind == "GESTINF05" and analisi == "DEN" and motiu in TAXES:
                taxa = 1000 * self.khalix[(ent, ind, motiu, "NUM")] / float(n)
                meta = TAXES[motiu]
                good = taxa >= meta
                self.khalix[(ent, "GESTINF01", motiu, "DEN")] = 1
                self.khalix[(ent, "GESTINF01", motiu, "NUM")] = 1 * good

    def export_medicina(self):
        """Bolet per MAPA-AP."""
        ubas = c.defaultdict(set)
        for db, grup in (("eqa_ind", "ADU"), ("pedia", "PED")):
            sql = "select up, uba from mst_ubas where tipus = 'M'"
            for key in u.getAll(sql, db):
                ubas[grup].add(key)
        master = c.defaultdict(c.Counter)
        for ind, dades in self.recomptes_med.items():
            for (up, uba, grup), minidades in dades.items():
                if (up, uba) in ubas[grup]:
                    den = minidades["DEN"]
                    for motiu, motiu_tip in self.motius.items():
                        if motiu_tip == grup:
                            key = (ind, up, uba, motiu)
                            master[key]["DEN"] = den
                            master[key]["NUM"] = minidades[motiu]
        khalix = c.Counter()
        for (ind, up, uba, motiu), dades in master.items():
            br = self.centres[up][1]
            ent = "{}M{}".format(br, uba)
            for analisi in ("NUM", "DEN"):
                khalix[(ent, ind, motiu, analisi)] += dades[analisi]
        cols = "(ent varchar(11), ind varchar(10), motiu varchar(10), \
                 analisi varchar(3), valor int)"
        u.createTable(MEDICINA_TB, cols, DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in khalix.items()]
        u.listToTable(upload, MEDICINA_TB, DATABASE)

    def export_khalix(self):
        """."""
        cols = "(ent varchar(11), ind varchar(10), motiu varchar(10), \
                 analisi varchar(3), valor int)"
        u.createTable(KHALIX_TB, cols, DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in self.khalix.items()]
        u.listToTable(upload, KHALIX_TB, DATABASE)
        # EAP
        for param, file in (("=", KHALIX_UP), (">", KHALIX_UBA)):
            sql = "select ind, 'Aperiodo', ent, analisi, motiu, \
                          'NOIMP', 'DIM6SET', 'N', valor \
                      from {_DATABASE}.{_KHALIX_TB} \
                      where left(ent,5) not in {_jail_centres_br} and length(ent) {_param} 5 and motiu not in ('PRAOI', 'PRAOPED', 'PRAOAP', 'PACEAP', 'PACEI') \
                         and valor > 0 \
                             union \
                   select ind, 'Aperiodo', ent, analisi, 'PRAOPED' motiu, \
                          'NOIMP', 'DIM6SET', 'N', sum(valor) \
                      from {_DATABASE}.{_KHALIX_TB} \
                      where left(ent,5) not in {_jail_centres_br} and length(ent) {_param} 5 and motiu in ('PRAOI', 'PRAOPED', 'PRAOAP')\
                          and valor > 0 \
                      group by ent, ind, analisi \
                              union \
                   select ind, 'Aperiodo', ent, analisi, 'PACEI' motiu, \
                          'NOIMP', 'DIM6SET', 'N', sum(valor) \
                      from {_DATABASE}.{_KHALIX_TB} \
                      where left(ent,5) not in {_jail_centres_br} and length(ent) {_param} 5 and motiu in ('PACEAP', 'PACEI') \
                         and valor > 0 \
                      group by ent, ind, analisi".format(_DATABASE=DATABASE, _KHALIX_TB=KHALIX_TB, _jail_centres_br=self.jail_centres_br, _param=param)
            if file == KHALIX_UBA:
                sql = sql +  " union \
                        select ind, 'Aperiodo', ent, analisi, motiu, \
                        'NOIMP', 'DIM6SET', 'N', valor \
                        from {_DATABASE}.{_MEDICINA_TB} where left(ent,5) not in {_jail_centres_br} and motiu not in ('PRAOPED', 'PRAOAP', 'PACEAP', 'PACEI') \
                        union \
                        select ind, 'Aperiodo', ent, analisi, 'PACEI', \
                        'NOIMP', 'DIM6SET', 'N', sum(valor) \
                        from {_DATABASE}.{_MEDICINA_TB} where left(ent,5) not in {_jail_centres_br} and motiu in  ('PACEAP', 'PACEI') \
                        group by ind, ent, analisi \
                        union \
                        select ind, 'Aperiodo', ent, analisi, 'PRAOPED', \
                        'NOIMP', 'DIM6SET', 'N', sum(valor) \
                        from {_DATABASE}.{_MEDICINA_TB} where left(ent,5) not in {_jail_centres_br} and motiu in  ('PRAOPED', 'PRAOAP') \
                        group by ind, ent, analisi".format(_DATABASE=DATABASE, _MEDICINA_TB=MEDICINA_TB, _jail_centres_br=self.jail_centres_br)
            u.exportKhalix(sql, file)
        # EAPP
        for param, file in (("=", KHALIX_UP_JAIL), (">", KHALIX_UBA_JAIL)):
            sql = "select ind, 'Aperiodo', ent, analisi, motiu, \
                          'NOIMP', 'DIM6SET', 'N', valor \
                      from {_DATABASE}.{_KHALIX_TB} \
                      where left(ent,5) in {_jail_centres_br} and length(ent) {_param} 5 and motiu not in ('PRAOI', 'PRAOPED', 'PRAOAP', 'PACEAP', 'PACEI') \
                         and valor > 0 \
                             union \
                   select ind, 'Aperiodo', ent, analisi, 'PRAOPED' motiu, \
                          'NOIMP', 'DIM6SET', 'N', sum(valor) \
                      from {_DATABASE}.{_KHALIX_TB} \
                      where left(ent,5) in {_jail_centres_br} and length(ent) {_param} 5 and motiu in ('PRAOI', 'PRAOPED', 'PRAOAP')\
                          and valor > 0 \
                      group by ent, ind, analisi \
                              union \
                   select ind, 'Aperiodo', ent, analisi, 'PACEI' motiu, \
                          'NOIMP', 'DIM6SET', 'N', sum(valor) \
                      from {_DATABASE}.{_KHALIX_TB} \
                      where left(ent,5) in {_jail_centres_br} and length(ent) {_param} 5 and motiu in ('PACEAP', 'PACEI') \
                         and valor > 0 \
                      group by ent, ind, analisi".format(_DATABASE=DATABASE, _KHALIX_TB=KHALIX_TB, _jail_centres_br=self.jail_centres_br, _param=param)
            if file == KHALIX_UBA_JAIL:
                sql = sql +  " union \
                        select ind, 'Aperiodo', ent, analisi, motiu, \
                        'NOIMP', 'DIM6SET', 'N', valor \
                        from {_DATABASE}.{_MEDICINA_TB} where left(ent,5) in {_jail_centres_br} and motiu not in ('PRAOPED', 'PRAOAP', 'PACEAP', 'PACEI') \
                        union \
                        select ind, 'Aperiodo', ent, analisi, 'PACEI', \
                        'NOIMP', 'DIM6SET', 'N', sum(valor) \
                        from {_DATABASE}.{_MEDICINA_TB} where left(ent,5) in {_jail_centres_br} and motiu in  ('PACEAP', 'PACEI') \
                        group by ind, ent, analisi \
                        union \
                        select ind, 'Aperiodo', ent, analisi, 'PRAOPED', \
                        'NOIMP', 'DIM6SET', 'N', sum(valor) \
                        from {_DATABASE}.{_MEDICINA_TB} where left(ent,5) in {_jail_centres_br} and motiu in  ('PRAOPED', 'PRAOAP') \
                        group by ind, ent, analisi".format(_DATABASE=DATABASE, _MEDICINA_TB=MEDICINA_TB, _jail_centres_br=self.jail_centres_br)
            u.exportKhalix(sql, file)

    def export_mensual(self):
        """."""
        cols = "(ind varchar(10), periode varchar(10), up varchar(10), \
                 motiu varchar(10), valor int)"
        u.createTable(CORTES_TB, cols, DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in self.mensual.items()]
        u.listToTable(upload, CORTES_TB, DATABASE)
        # EAP
        sql = "select ind, periode, ics_codi, 'NUM', motiu, \
                      'NOIMP', 'DIM6SET', 'N', valor \
                        from {_DATABASE}.{_CORTES_TB} inner join nodrizas.cat_centres on up = scs_codi \
                    where motiu not in ('PRAOPED', 'PRAOAP', 'PACEAP', 'PACEI') \
                    union \
                    select ind, periode, ics_codi, 'NUM', 'PACEI', \
                      'NOIMP', 'DIM6SET', 'N', sum(valor) \
                        from {_DATABASE}.{_CORTES_TB} inner join nodrizas.cat_centres on up = scs_codi \
                    where motiu not in ('PACEAP', 'PACEI') \
                    group by ind, periode, ics_codi \
                    union \
                    select ind, periode, ics_codi, 'NUM', 'PRAOPED', \
                      'NOIMP', 'DIM6SET', 'N', sum(valor) \
                        from {_DATABASE}.{_CORTES_TB} inner join nodrizas.cat_centres on up = scs_codi \
                    where motiu not in ('PRAOPED', 'PRAOAP') \
                    group by ind, periode, ics_codi".format(_DATABASE=DATABASE, _CORTES_TB=CORTES_TB)
        u.exportKhalix(sql, CORTES_UP)
        # EAPP
        sql = "select ind, periode, up, 'NUM', motiu, \
                      'NOIMP', 'DIM6SET', 'N', valor \
                        from {_DATABASE}.{_CORTES_TB} \
                    where up in {_jail_centres_up} and motiu not in ('PRAOPED', 'PRAOAP', 'PACEAP', 'PACEI') \
                    union \
                    select ind, periode, up, 'NUM', 'PACEI', \
                      'NOIMP', 'DIM6SET', 'N', sum(valor) \
                        from {_DATABASE}.{_CORTES_TB} \
                    where up in {_jail_centres_up} and motiu not in ('PACEAP', 'PACEI') \
                    group by ind, periode, up \
                    union \
                    select ind, periode, up, 'NUM', 'PRAOPED', \
                      'NOIMP', 'DIM6SET', 'N', sum(valor) \
                        from {_DATABASE}.{_CORTES_TB} \
                    where up in {_jail_centres_up} and motiu not in ('PRAOPED', 'PRAOAP') \
                    group by ind, periode, up".format(_DATABASE=DATABASE, _CORTES_TB=CORTES_TB, _jail_centres_up= self.jail_centres_up)
        u.exportKhalix(sql, CORTES_UP_JAIL)

    def export_qc(self):
        """."""
        indicadors = ("GESTINF05", "GESTINF06")
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        fa1m = dext - r.relativedelta(months=1)
        pactual = "A{}".format(dext.strftime("%y%m"))
        pprevi = "A{}".format(fa1m.strftime("%y%m"))
        sqls = []
        for x, y in ((pactual, 'ANUAL'), (pactual, 'ACTUAL'), (pprevi, 'PREVI')):  # noqa
            sqls.append("select ind, '{}', ent, 'DEN', '{}', 'TIPPROF4', \
                                sum(valor) \
                         from {} \
                         where ind in {} and \
                               length(ent) = 5 and analisi = 'DEN' and \
                               motiu in ('PALADU', 'PALPED')\
                         group by ind, ent".format(x, y, KHALIX_TB, indicadors))  # noqa
        sqls.append("select ind, '{}', ent, 'NUM', 'ANUAL', 'TIPPROF4', \
                            sum(valor) \
                     from {} \
                     where ind in {} and \
                           length(ent) = 5 and analisi = 'NUM' \
                            and motiu not in ('PRAOI', 'PRAOPED') \
                     group by ind, ent".format(pactual, KHALIX_TB, indicadors))
        sqls.append("select ind, replace(periode, 'B', 'A'), ics_codi, 'NUM', \
                            if(replace(periode, 'B', 'A') = '{}', 'ACTUAL', 'PREVI'), \
                            'TIPPROF4', sum(valor) \
                     from {} a \
                     inner join nodrizas.cat_centres b on a.up = b.scs_codi \
                     where ind in {} \
                     group by ind, periode, up".format(pactual, CORTES_TB, indicadors))  # noqa
        dades = []
        for sql in sqls:
            dades.extend([("Q" + row[0],) + row[1:]
                          for row in u.getAll(sql, DATABASE)])
        cols = ["k{} varchar(10)".format(i) for i in range(6)] + ["v int"]
        u.createTable(QC_TB, "({})".format(", ".join(cols)), DATABASE, rm=True)
        u.listToTable(dades, QC_TB, DATABASE)

    def export_ecap(self):
        """."""
        # resultats
        den_dup = tuple(["GESTINF0{}".format(i) for i in range(3, 7)])
        uba = "create or replace table exp_ecap_gida_uba as \
               select scs_codi up, substr(ent, 7, 99) uba, \
                      substr(ent, 6, 1) tipus, ind indicador, \
                      sum(if(analisi = 'NUM', valor, 0)) numerador, \
                      if(ind in {0}, max(if(analisi = 'DEN', valor, 0)), \
                       sum(if(analisi = 'DEN', valor, 0))) denominador, \
                      sum(if(analisi = 'NUM', valor, 0)) / \
                       if(ind in {0}, max(if(analisi = 'DEN', valor, 0)), \
                        sum(if(analisi = 'DEN', valor, 0))) resultat \
               from altres.exp_khalix_gida a \
               inner join nodrizas.cat_centres b on left(ent, 5) = ics_codi \
               where length(ent) > 5 and ind != 'GESTINF08' \
               group by scs_codi, substr(ent, 7, 99), \
                        substr(ent, 6, 1), ind".format(den_dup)
        u.execute(uba, DATABASE)
        nulls = "update exp_ecap_gida_uba \
                 set resultat = 0 where resultat is null"
        u.execute(nulls, DATABASE)
        # cataleg
        tpm = ["GESTINF0{}".format(i) for i in range(5, 7)]
        ind = [ind for ind,
               in u.getAll("select distinct indicador \
                            from altres.exp_ecap_gida_uba", DATABASE)]
        umi = "select nom, descripcio from indicador_indicador \
               where nom in {}".format(tuple(ind))
        cat = [(ind, nom, int(ind[8]), 'GESTINF', 0, 0, 0, 0, 0, 1, \
                "http://10.80.217.201/sisap-umi/indicador/codi/{}".format(ind),
                "TPM" if ind in tpm else "PCT")
                for (ind, nom) in u.getAll(umi, ("umi", "x0001"))]
        u.execute("create or replace table exp_ecap_gida_cataleg \
                   like exp_ecap_tir_cataleg", DATABASE)
        u.execute("alter table exp_ecap_gida_cataleg \
                   modify indicador varchar(10)", DATABASE)
        u.listToTable(cat, "exp_ecap_gida_cataleg", DATABASE)
        # pare
        par = "create or replace table exp_ecap_gida_catalegpare as \
               select 'GESTINF', 'Plans de Cures Estandarditzats per motius de salut aguts', \
                      6, 'ALTRES' as pantalla \
               from dual"
        u.execute(par, DATABASE)


if __name__ == "__main__":
    GIDA()
