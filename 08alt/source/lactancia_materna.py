# coding: iso-8859-1

import sisapUtils as u
from collections import Counter
import datetime as d
from os import path

current_year, current_month = u.getOne("select YEAR(data_ext), MONTH(data_ext) from dextraccio", "nodrizas")

if current_month == 6 and u.IS_MENSUAL:

    # PARAMETROS QUE PIDEN ACTUALIZAR, SON LAS COHORTES DE AÑOS DE NACIMIENTO
    YEAR_NAIX_INI = current_year - 1
    YEAR_NAIX_FI = current_year - 1

    # Llista de codis de lactància (variables agrupador 285 de lactància)
    codis_nen11 = set('PEPAL01')
    codis_nen12 = set(['PENDE051', 'PENDE052'])

    sql = """
        SELECT
            scs_codi, ics_desc, sap_desc, amb_desc
        from
            cat_centres
        where
            ep = '0208'
        """
    centros = {}
    for up, ics_desc, sap_desc, amb_desc in u.getAll(sql, 'nodrizas'):
        centros[up] = {'ics': ics_desc, 'sap': sap_desc, 'ambit': amb_desc}

    sql = """
        SELECT
            id_cip_sec,
            val_val,
            val_var
        FROM
            import.nen11
        """
    valormax = {}
    for id, valor, var in u.getAll(sql, 'nodrizas'):
        if var in codis_nen11:
            valor = int(float(valor.replace(',', '.')))
            if valor > 24:
                valor = 24
            if id in valormax:
                valor2 = valormax[id]
                if valor > valor2:
                    valormax[id] = valor
            else:
                valormax[id] = valor

    sql = """
        SELECT
            id_cip_sec,
            val_val,
            val_var
        FROM
            import.nen12
        """
    for id, valor, var in u.getAll(sql, 'nodrizas'):
        if var in codis_nen12:
            valor = int(float(valor.replace(',', '.')))
            if valor > 24:
                valor = 24
            if id in valormax:
                valor2 = valormax[id]
                if valor > valor2:
                    valormax[id] = valor
            else:
                valormax[id] = valor

    dnaix_ini = d.date(YEAR_NAIX_INI, 1, 1)
    dnaix_fin = d.date(YEAR_NAIX_FI, 12, 31)

    mesosLM = Counter()
    sql = """
        SELECT
            id_cip_sec,
            year(data_naix) as any_naix,
            up
        FROM
            nodrizas.ped_assignada
        WHERE
            data_naix BETWEEN DATE '{}' and DATE '{}'
        """.format(dnaix_ini, dnaix_fin)
    for id, anys, up in u.getAll(sql, 'nodrizas'):
        if up in centros:
            try:
                valor = valormax[id]
            except KeyError:
                valor = None
            mesosLM[(anys, valor)] += 1

    upload = []
    for (anys, valor), n in mesosLM.items():
        upload.append([anys, valor, n])

    filename = path.join(u.tempFolder, 'lactancia_materna_mes_{}.csv'.format(dnaix_fin))
    u.writeCSV(filename, [('any', 'lm_mes', 'n')] + sorted(upload), sep=';')

    me = "sisap@gencat.cat"
    to = "g.rodriguez@gencat.cat"
    cc = ["alejandrorivera@gencat.cat", "mquintana.apms.ics@gencat.cat"]
    subject = "Dades anuals de Lact�ncia Materna del darrer any {}".format(YEAR_NAIX_FI)
    text = "Bon dia,\n\nCom cada any per aquestes dates us enviem les dades anuals de l'ECAP de Lact�ncia Materna del darrer any {}, per a la mem�ria i avaluaci�.\n\nSalutacions:\n\nSISAP".format(YEAR_NAIX_FI)
    u.sendGeneral(me, to, cc, subject, text, filename)