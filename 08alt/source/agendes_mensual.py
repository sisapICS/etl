# -*- coding: utf-8 -*-

import csv
import datetime as d
import dateutil.relativedelta as du
import MySQLdb
import numpy as np
import pandas as pd
from pydruid.client import *
from pydruid.utils.aggregators import *
from pydruid.utils.postaggregator import *
from pydruid.utils.filters import *
# from textUtils import *
import sisapUtils as u
import subprocess as s
from contextlib import contextmanager
import os
import glob
import sys

sql = """select year(data_ext), month(data_ext) 
            from nodrizas.dextraccio"""
for y, m in u.getAll(sql, 'nodrizas'):
    year = y 
    month = m
dini = d.date(year, month, 1)

class Consulta(object):

    def __init__(self):
    
        Consulta.get_intervals(self)

        query = PyDruid('http://10.80.217.84:8082', 'druid/v2/')
        
        self.brs = []
        sql = "select ics_codi from nodrizas.cat_centres"
        for br, in u.getAll(sql, 'nodrizas'):
            self.brs.append(br)
        
        ambits =  query.topn(
                datasource='agendes',
                granularity='all',
                intervals='2000-01-01/2099-12-31',
                aggregations={'programades': doublesum('PROGRAMADES')},
                dimension='AMBIT',
                metric='programades',
                threshold=500
            )
                
        test = ambits.export_pandas()
        
        for i, row in enumerate(test.itertuples(), 1):
            Consulta.dades (self, row.AMBIT, query)
    
    def get_intervals(self):
        self.dini = dini
        month = '0' + str(self.dini.month)
        self.dateK = 'A' + str(self.dini.year)[-2:] + month[-2:]
        self.dfi = self.dini + du.relativedelta (day=1, months=1)
        self.prefix = '_' + month[-2:] + '_' + str(self.dini.year)
        self.nweeks = ((self.dini + du.relativedelta (day=1, months=1)) - self.dini).days/7.0          


    def dades (self, AMBIT, query): 
        data_file1 = []
        data_file2 = []
        top = query.groupby(
                datasource='agendes',
                granularity='day',
                intervals= str(self.dini) + '/' + str(self.dfi),
                dimensions = ['AMBIT', 'SAP', 'BR', 'UBA', 'TIPUS_CODI', 'METGE_INF', 'SERVEI', 'TORN'],
                aggregations={'programades': doublesum('PROGRAMADES'),'programables': doublesum('PROGRAMABLES'), 'tancades': doublesum('TANCADES'),'programades_realitzades': doublesum('PROGRAMADES_REALITZADES'), 'espontanies': doublesum('ESPONTANIES'), 'espontanies_realitzades': doublesum('ESPONTANIES_REALITZADES'), 'forcades': doublesum('FORCADES'), 'forcades_realitzades': doublesum('FORCADES_REALITZADES')},
                filter = Dimension ('AMBIT') == AMBIT,
                post_aggregations={'realitzades': Field('programades_realitzades') + Field('espontanies_realitzades') + Field('forcades_realitzades'), 'realitzadesMenysForcades': Field('programades_realitzades') + Field('espontanies_realitzades')}
                )
        df = top.export_pandas()
        # afegim que les programables ara s�n les programables + les tancades
        df['programables'] = df['programables'] + df['tancades']
        df['BR']= np.where(df['BR'] == 'B4374', 'BR366', df['BR'])
        df['TORN']= np.where(((df['TORN'] != 'M') & (df['TORN'] != 'T') & (df['TORN'] != 'N')), 'nan', df['TORN'])    
        
        df['classi'] = np.where(((df['METGE_INF']=='M')&df['SERVEI'].str.contains('PED')), 'TIPPROF2', np.where((df['METGE_INF']=='M'), 'TIPPROF1', np.where(((df['METGE_INF']=='I') | (df['SERVEI']=='INFG') | (df['SERVEI']=='INFP') | (df['SERVEI']=='INFPD')), 'TIPPROF4', 'TIPPROF6')))

        df5 = df.groupby(['timestamp', 'AMBIT', 'SAP', 'BR', 'classi', 'TIPUS_CODI', 'UBA', 'METGE_INF','TORN'],as_index=False)[['programables', 'programades', 'espontanies', 'forcades', 'programades_realitzades', 'espontanies_realitzades', 'forcades_realitzades','realitzades','realitzadesMenysForcades']].sum()
        df5['ponderat'] = np.where((df5['TIPUS_CODI']=='9D'), 3*df5['realitzades'], np.where(((df5['TIPUS_CODI']=='9T') | (df5['TIPUS_CODI']=='9E')), 10/12.0*df5['realitzades'], np.where((df5['TIPUS_CODI']=='9C'), df5['realitzades'], 0)))
        df5['ponderatPresencials'] = np.where((df5['TIPUS_CODI']=='9D'), 3*df5['realitzades'], np.where(((df5['TIPUS_CODI']=='9C')), df5['realitzades'], 0))
        df5['esponInser'] = np.where((df5['TIPUS_CODI']=='9C'), df5['espontanies_realitzades'] + df5['forcades_realitzades'], 0)
        df5['esponCentre'] = np.where(df5['TIPUS_CODI']=='9C', df5['espontanies_realitzades'], 0)
        df5['presencialsRealitzades'] = np.where((df5['TIPUS_CODI']=='9C'), df5['realitzades'], 0)
        df5['presencialsProgram'] = np.where((df5['TIPUS_CODI']=='9C'), df5['programables'], 0)

        df5['visitesMati'] = np.where((df5['TORN']=='M'), df5['realitzades'], 0)
        df5['visitesTarda'] = np.where((df5['TORN']=='T'), df5['realitzades'], 0)
        df5['visitesNit'] = np.where((df5['TORN']=='N'), df5['realitzades'], 0)
        
        df6 = df5.groupby(['timestamp', 'AMBIT', 'SAP', 'BR', 'classi', 'UBA', 'METGE_INF'],as_index=False)[['programables', 'programades', 'espontanies', 'forcades', 'programades_realitzades', 'espontanies_realitzades', 'forcades_realitzades','realitzades', 'realitzadesMenysForcades', 'ponderat', 'ponderatPresencials', 'esponInser', 'esponCentre', 'presencialsRealitzades',  'presencialsProgram', 'visitesMati', 'visitesTarda', 'visitesNit']].sum()       
        
        df6['TIPUS_CODI'] = 'T'
       
        df5['mes25'] = int(0)
        df5['mes28'] = int(0)
        df5['mes30'] = int(0)
        df5['mes40'] = int(0)
        df5['diesVisita'] = int(0)
        df5['diesStandard'] = int(0)
        
        df6['mes25'] = np.where(((df6['ponderat'] > 25) & (df6['TIPUS_CODI'] == 'T')), 1, 0)
        df6['mes28'] = np.where(((df6['ponderat'] > 28) & (df6['TIPUS_CODI'] == 'T')), 1, 0)
        df6['mes30'] = np.where(((df6['ponderat'] > 30) & (df6['TIPUS_CODI'] == 'T')), 1, 0)
        df6['mes40'] = np.where(((df6['ponderat'] > 40) & (df6['TIPUS_CODI'] == 'T')), 1, 0)
        
        df6['mes25Presencials'] = np.where(((df6['ponderatPresencials'] > 25) & (df6['TIPUS_CODI'] == 'T')), 1, 0)
        
        df6['agendaM'] = np.where(((100*(df6['visitesMati'] / df6['realitzades'])>=70) & (df6['TIPUS_CODI'] == 'T')), 1, 0)
        df6['agendaT'] = np.where(((100*(df6['visitesTarda'] / df6['realitzades'])>=70) & (df6['TIPUS_CODI'] == 'T')), 1, 0)
        df6['agendaL'] = np.where(((100*(df6['visitesMati'] / df6['realitzades'])<70) & (df6['TIPUS_CODI'] == 'T') & (100*(df6['visitesTarda'] / df6['realitzades'])<70)), 1, 0)
        
        df6['diesVisita'] = int(1)
        
        df6['diesStandard'] = np.where((((df6[["realitzades", "programables"]].max(axis=1)) > 20) & (df6['TIPUS_CODI'] == 'T')), 1, np.where((((df6[["realitzades", "programables"]].max(axis=1)) <= 20) & (df6['TIPUS_CODI'] == 'T')), df6[["realitzades", "programables"]].max(axis=1)/20.0, 0))

        df6 = df6.append(df5, ignore_index=True, sort=True)
               
        df7 = df6.groupby(['AMBIT', 'SAP', 'BR', 'classi', 'UBA', 'METGE_INF', 'TIPUS_CODI'],as_index=False)[['programables', 'programades', 'espontanies', 'forcades', 'programades_realitzades', 'espontanies_realitzades', 'forcades_realitzades','realitzades', 'ponderat', 'ponderatPresencials', 'esponInser', 'esponCentre', 'presencialsRealitzades',  'presencialsProgram', 'mes25', 'mes25Presencials', 'mes28', 'mes30', 'mes40', 'diesVisita', 'diesStandard', 'agendaM', 'agendaT', 'agendaL']].sum()       
        
        df7['numUBAs'] = np.where(df7['TIPUS_CODI'] == 'T', 1, 0)
        df7['numUBATarda'] = np.where(((df7['TIPUS_CODI'] == 'T') &  ((100*(df7['agendaT'] + df7['agendaL'])/(df7['agendaT'] + df7['agendaL'] + df7['agendaM']))>=10)), 1, 0)
        
        dfAgrupado = df7[df7['TIPUS_CODI'] == 'T']
        
        df7 = df7.merge (dfAgrupado[['realitzades', 'programables', 'diesStandard', 'numUBAs', 'numUBATarda', 'AMBIT', 'SAP', 'BR', 'classi', 'UBA', 'METGE_INF', 'TIPUS_CODI']], how='left', on= ('AMBIT', 'SAP', 'BR', 'classi', 'UBA', 'METGE_INF'), suffixes = ['','_Total'], indicator = False)

        df7['nWeeks'] = np.where(df7['TIPUS_CODI'] == 'T', self.nweeks, 0)
        
        for i, row in enumerate(df7.itertuples(), 1):
            if row.BR in self.brs:
                uba = row.UBA.encode("utf8")
                br = row.BR.encode("utf8")
                met_inf = row.METGE_INF.encode("utf8")
                ubaKhalix = '{}{}{}'.format(row.BR, row.METGE_INF, uba)
                if row.TIPUS_CODI == 'T':
                    tipus = 'TIPVISITA'
                else: 
                    tipus = 'VIS' + row.TIPUS_CODI
                if row.TIPUS_CODI == 'T':
                    data_file2.append(['AGENDQC1', self.dateK, ubaKhalix, 'DEN', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.nWeeks])  
                    data_file2.append(['AGENDQC2', self.dateK, ubaKhalix, 'DEN', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.nWeeks])
                    data_file2.append(['AGENDQC1', self.dateK, ubaKhalix, 'NUM', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.presencialsProgram])  
                    data_file2.append(['AGENDQC2', self.dateK, ubaKhalix, 'NUM', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.presencialsRealitzades])                   
                    
        u.listToTable(data_file2, 'agendes_mensual_2', 'altres')  
            
        df7 = df7.groupby(['AMBIT', 'SAP', 'BR', 'classi', 'TIPUS_CODI'],as_index=False)[['programables', 'programades', 'espontanies', 'forcades', 'programades_realitzades', 'espontanies_realitzades', 'forcades_realitzades','realitzades', 'ponderat', 'ponderatPresencials' , 'esponInser', 'esponCentre', 'presencialsRealitzades',  'presencialsProgram', 'mes25Presencials', 'mes25', 'mes28', 'mes30', 'mes40', 'diesVisita', 'diesStandard','realitzades_Total', 'programables_Total', 'diesStandard_Total', 'numUBAs_Total', 'numUBATarda_Total', 'agendaM', 'agendaT', 'agendaL', 'nWeeks']].sum()       
        dfAgrupado = df7[df7['TIPUS_CODI'] == 'T']
        df7 = df7.merge (dfAgrupado[['realitzades_Total', 'programables_Total', 'diesStandard_Total','numUBAs_Total', 'numUBATarda_Total', 'nWeeks','AMBIT', 'SAP', 'BR', 'classi', 'TIPUS_CODI']], how='left', on= ('AMBIT', 'SAP', 'BR', 'classi'), suffixes = ['','Equip'], indicator = False)
        
        for i, row in enumerate(df7.itertuples(), 1):
            if row.BR in self.brs:
                if row.TIPUS_CODI == 'T':
                    tipus = 'TIPVISITA'
                else: 
                    tipus = 'VIS' + row.TIPUS_CODI

                if row.TIPUS_CODI == 'T':
                    data_file1.append(['CARASSIS02', self.dateK, row.BR, 'NUM', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.ponderat])
                    data_file1.append(['CARASSIS02', self.dateK, row.BR, 'DEN', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.diesStandard])
                    data_file1.append(['CARASSIS05', self.dateK, row.BR, 'NUM', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.mes30])
                    data_file1.append(['CARASSIS05', self.dateK, row.BR, 'DEN', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.diesVisita])
                    data_file1.append(['QCCARASS01', self.dateK, row.BR, 'NUM', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.ponderatPresencials])
                    data_file1.append(['QCCARASS01', self.dateK, row.BR, 'DEN', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.diesStandard])  
                    data_file1.append(['QCCARASS02', self.dateK, row.BR, 'NUM', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.mes25Presencials])
                    data_file1.append(['QCCARASS02', self.dateK, row.BR, 'DEN', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.diesVisita]) 
                    data_file1.append(['AGENDQC1', self.dateK, row.BR, 'DEN', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.nWeeksEquip])  
                    data_file1.append(['AGENDQC2', self.dateK, row.BR, 'DEN', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.nWeeksEquip])
                    data_file1.append(['AGENDQC1', self.dateK, row.BR, 'NUM', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.presencialsProgram])  
                    data_file1.append(['AGENDQC2', self.dateK, row.BR, 'NUM', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.presencialsRealitzades])   
                    data_file1.append(['AGENDQC3', self.dateK, row.BR, 'NUM', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.esponInser])  
                    data_file1.append(['AGENDQC3', self.dateK, row.BR, 'DEN', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.presencialsRealitzades])      
                    data_file1.append(['AGENDQC5', self.dateK, row.BR, 'NUM', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.esponCentre])  
                    data_file1.append(['AGENDQC5', self.dateK, row.BR, 'DEN', 'MENSUAL', row.classi, 'DIM6SET', 'N', row.presencialsRealitzades]) 
        
        u.listToTable(data_file1, 'agendes_mensual_1', 'altres')            

        
if __name__ == "__main__":
    cols = """(indicador varchar(10), data varchar(10), br varchar(20), analisi varchar(10),
            tipus varchar(10), classi varchar(20), dim6set varchar(10), n varchar(1), val int)"""
    u.createTable('agendes_mensual_1', cols, 'altres', rm=True)
    cols = """(indicador varchar(10), data varchar(10), equip varchar(20), analisi varchar(10),
            tipus varchar(10), classi varchar(20), dim6set varchar(10), n varchar(1), val int)"""
    u.createTable('agendes_mensual_2', cols, 'altres', rm=True)
    Consulta()

