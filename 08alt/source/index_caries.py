# coding: iso-8859-1
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import string
import random
import operator


ops = { ">": operator.gt, "=": operator.eq }

#Validation
validation=False
up_codi=("00380","00365",'00368', '00110')
#up_codi=("00380",)

alt = "altres"
nod = "nodrizas"
assig=nod+".assignada_tot"
odn=nod+".odn_variables"
peces_pat="import.odn507"
peces_trat="import.odn508"
#limit=2000

rev_agr=309


#peces
pat_trat_tuples_list=[
("CA","OBTt"),
("RR","OBTt") ,
("OF","OBTt"),
("OBTp","OBTt"),
("CA","CON"),
("RR","CON"), 
("OF","CON"),
("OBTp","CON"), 
("CA","PF"),
("RR","PF"), 
("OF","PF"),
("OBTp","PF") ,
]


pat_list=tuple({pat_trat[0] for pat_trat in pat_trat_tuples_list})
trat_list=tuple({pat_trat[1] for pat_trat in pat_trat_tuples_list})

#Agrupador indicador de ARC
agr_ARC_dict={286:"=1501",
			  307:">0",
			  308:">0"}

caries_temp={307:"=0"}
caries_def= {308:"=0"}
caries_all={308:"=0",307:"=0"}


#Cada uno de los agrupadores puede ser clasificado en 3 categorias:
# --> mitjana : Suma de los indices de caries
# --> pacient: Cuenta los pacientes con index de caries =0
# --> obturacio: Calcula la O por paciente

# Dentro de estas categorias se tienen los diccionarios de
# {	codi:	agrupadores/condiciones para calcular ese indicador}
codi_indicadors= {"mitjana": {"IAD0011":308,"IAD0012":307,"IAD0021":307,"IAD0035":307,"IAD0036":308},
				  "pacient": {"IAD0013": (agr_ARC_dict,set.union),
				  			  "IAD0014":(caries_all,set.intersection),
				  			  "IAD0015":(caries_def, None),
							  "IAD0016":(caries_temp,None),
							  "IAD0022":(caries_temp,None),
							  "IAD0034":(caries_all, set.intersection)

				  },
				  "obturacio": {"IAD0017": ([0,float('Inf')], [307,308]),
				  				"IAD0019": ([1,50], [307]),
				  				"IAD0018": ([50,float('Inf')], [308])}
								}

nous_indicadors = Counter()

#Para calcular los subindices(en los que solo cambia la edad del denominador), se crea un diccionario con  {edat: subindex(letra de a hasta i)}

ind_edat={edat: letter for letter,edat in zip(string.ascii_lowercase[0:9],range(6,15))}
ind_edat_petits={edat: letter for letter,edat in zip(string.ascii_lowercase[0:5],range(1,6))}
ind_edat_adults = {}
for i in range(15,200):
	if 15<=i<=34:
		ind_edat_adults[i] = 'a'
	elif 35<=i<=44:
		ind_edat_adults[i] = 'b'
	elif 45<=i<=64:
		ind_edat_adults[i] = 'c'
	elif 64<=i<=74:
		ind_edat_adults[i] = 'd'
	else:
		ind_edat_adults[i] = 'e'

#Funciones	

def get_data_extraccio():
    """."""

    global data_ext, data_ext_menys1any, data_ext_menys2anys

    # Consulta SQL per obtenir dades d'extracci�
    sql = """
            SELECT
                data_ext,
                date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
                date_add(date_add(data_ext, INTERVAL -2 YEAR), INTERVAL + 1 DAY)
            FROM
                dextraccio
            """

    # Obtindre valors de la consulta SQL
    data_ext, data_ext_menys1any, data_ext_menys2anys = getOne(sql, "nodrizas")

def get_centres():
    """Obt� els Centres d'Atenci� Prim�ria."""

    global cat_centres

    sql = """
            SELECT
                scs_codi,
                ics_codi
            FROM
                cat_centres
        """
    cat_centres = {up: br for (up, br) in getAll(sql, "nodrizas")}

def get_revision_odn(agr,nens):
	"""Selecciona los pacientes que presenten el agrupador agr. Se filtra por los ids que esten en el set nens.
	   Devuelve un set de ids
	"""
	sql="select id_cip_sec from {}.odn_variables where agrupador= {}".format(nod,agr)
	return {id_cip_sec for (id_cip_sec,) in getAll(sql,nod) if id_cip_sec in nens}

def get_obturacio_pat_trat(obt_peces_id,agr_list):
	""" Obtiene para cada paciente al que se le haya calculado un index el numero de obturaciones totales que se almacenan en obt_peces a nivel de paciente y 
		de tipo de denticion (agr). 
		Las obturaciones se obtienen de obt_peces, un diccionario a nivel de paciente(id_cip_sec) y de denticion(agr): {id_cip_sec:{agr: n_obts}}, al igual que el index que se obtiene
		del numerador_by_agr {id_cip_sec:{agr: index}}
		El numero de obturaciones totales y el index depende de agr_list, una lista con los agr que determina si se estudia a nivel de denticion permanente 
		(agr = 307), denticion temporal (agr=308) o ambas. 
		Devuelve un diccionario de tuples: {id_cip_sec: (obts,index)}

	"""
	num=Counter()
	ids_with_index={id_cip_sec for agr in agr_list for id_cip_sec in numerador_by_agr[agr]}
	for id_cip_sec in ids_with_index:
		obts=0
		index= sum([numerador_by_agr[agr][id_cip_sec] for agr in agr_list])
		if index != 0:
			if id_cip_sec in obt_peces:
				obts= sum([obt_peces[id_cip_sec][agr] for agr in agr_list])
			num[id_cip_sec]=(obts,index)
		
	return num

def get_peces_dat_by_criteris(pat_trat_dict):
	"""Se obtiene para cada paciente (que tenga un index de caries) y para cada una de sus piezas dentales, las patologias o tratamientos que nos interesan 
	   y las fechas de diagnostico de cada una de ellas.
	   Devuelve  {id_cip_sec: {peca:criteri{fecha}}}	

	"""
	
	peces_dat=defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(set))))
	
	for (column,taula),criteris in pat_trat_dict.iteritems():
		sql='select id_cip_sec, d{2}d_cod_p, d{2}d_{1}, d{2}d_data from import.{0};'.format(taula,column,column[0])
		printTime(sql)
		for id_cip_sec,peca,pat_trat,data in getAll(sql,"import"):
			pat_trat= '{}{}'.format(pat_trat,column[0]) if pat_trat=='OBT' else pat_trat
			#peces_dat[id_cip_sec][peca].setdefault(pat_trat,set())
			peces_dat[id_cip_sec][peca][column[0]][pat_trat].add(data)
	return peces_dat

def get_absencies():
	peces_A=defaultdict(set)
	sql="select id_cip_sec, dfd_cod_p from import.odn506 where dfd_p_a='A' and dfd_mot_a not in ('N','A')"
	for id_cip_sec,peca in getAll(sql,"import"):
		peces_A[id_cip_sec].add(peca)
	return peces_A

def get_obt(obt_peces,id_cip_sec,agr):
	O= True 
	C= False
	obt_peces[id_cip_sec][agr]+=1
	return O,C,obt_peces

def get_indexes(rev_id,peces_dat,peces_A,numerador_by_agr):
	""" Calcula los indices de restauracion a nivel de paciente (id_cip_sec) y denticion (agr, 307 si es denticion temporal y 308 si es denticion definitiva)
		utilizando los datos de patologia y tratamiento por paciente y pieza guardados en peces_dat, las piezas ausentes peces_A y las incorpora al diccionario
		numerador_by_agr. Tambien consigue las obturaciones a nivel de paciente (id_cip_sec) y denticion (agr).
		Devuelve dos diccionarios,
			-> numerador_by_agr: {id_cip_sec {agr: index} }
			-> obt_peces: {id_cip_sec: {agr: n_obts} }
	"""
	#Cogemos los ids o que bien han tenido una patologia/tratamiento o que tienen una pieza ausente o ambos
	ids_with_data=peces_dat.viewkeys() | peces_A.viewkeys()

	obt_peces=defaultdict(Counter)

	#para cada paciente que ha tenido una revision odontologica
	for id_cip_sec in rev_id:
		numerador_by_agr[307].setdefault(id_cip_sec,0)
		numerador_by_agr[308].setdefault(id_cip_sec,0)

		if id_cip_sec in ids_with_data:
			#cogemos las piezas que tienen datos
			peca_with_data= peces_dat[id_cip_sec].viewkeys() | peces_A[id_cip_sec]

			for peca in peca_with_data:
				#Initialization
				C,A,O= False,False,False
				max_pat,max_trat='',''
				#Comprobar si es denticion def/temp
				agr= 308 if peca <= 50 else 307

				#CALCULO C
				#se coge la patologia mas reciente para el diente
				try:
					max_pat=max(peces_dat[id_cip_sec][peca]['p'].iteritems(), key=operator.itemgetter(1))[0]
				except:
					pass

				#se coge el tratamiento mas reciente para el diente
				try:
					max_trat=max(peces_dat[id_cip_sec][peca]['t'].iteritems(), key=operator.itemgetter(1))[0]
				except:
					pass
				
				C=  max_pat in ["CA","RR","OF","OBTp"]

				#CALCULO A
				#se mira si esta ausente la pieza si la patologia mas reciente es ca, rr, of o obt (C es True) o sino ha tenido ninguna patologia. 
				#En caso contrario no cuenta
				A= peca in peces_A[id_cip_sec] if C or max_pat=='' else False
				
				#CALCULO O
				#si no esta ausente la pieza, se calcula la O
				if not A:
					#se itera por las parejas patologia, tratamiento que definen una obturacion
					for pat,trat in pat_trat_tuples_list:
						# si el tratamiento mas reciente en esa pieza es 'OBTt' o
						# si esa pieza ha tenido esa patologia y ese tratamiento en ese diente Y
						# si la fecha del tratamiento es mas reciente o la misma que la patologia pareja o de la patología más reciente	
						if max_trat=='OBTt' or (peces_dat[id_cip_sec][peca]['p'][pat] and peces_dat[id_cip_sec][peca]['t'][trat] and \
							((max(peces_dat[id_cip_sec][peca]['p'][pat]) <= max(peces_dat[id_cip_sec][peca]['t'][trat])) \
								or \
								( max(peces_dat[id_cip_sec][peca]['t'][trat]) >= max(peces_dat[id_cip_sec][peca]['p'][max_pat]) ))) :
							
							#O es True y se cambia C a False
							O=True 
							if C:
								C=False
							#se anyade una obturacion al contador de obt para ese paciente y esa denticion, y se rompe el loop
							obt_peces[id_cip_sec][agr]+=1
							break
				

				#si la pieza esta ausente, C es False y para el COD se vuelve false para que no cuente en el index
				if A:
					C=False 
					if agr==307:
						A= False
				
				#Alguno de los tres cuenta como index
				if C or O or A:
					numerador_by_agr[agr][id_cip_sec]+=1
			
	return numerador_by_agr,obt_peces


def get_ids_by_agr(data_ext):
	""" Selecciona los id_cip_seq de ninos que presentan un ARC segun el agrupador en la tabla odn_variables y guarda el valor de este agrupador
		Devuelve un diccionario de {id_cip_sec:valor_agr}
	"""
	sql = "select id_cip_sec,valor,dat,agrupador from {0} where agrupador =286 ;".format(odn)
	agr_id_valor=defaultdict(dict)
	for id_cip_sec,valor,dat,agr in getAll(sql,nod):
		if dat and monthsBetween(dat,data_ext)<24:
			agr_id_valor[agr][id_cip_sec]=valor
	return agr_id_valor

def export_txt_file(name,header,rows):
    with open(name,"wb") as out_file:
        print(rows[0])
        row_format ="{}\t" * len(rows[0])+"\n"
        out_file.write(row_format.format(*header))
        for row in rows:
            out_file.write(row_format.format(*row))

def get_poblacio():
    global nens, nens_petits, dones, adults
    """ Se obtiene para cada id_cip_sec de nino, la upOrigen y su edad y se guarda en un diccionario.
        Devuelve {id_cip_sec: (up, edat)}
    """
    sql = "select id_cip_sec, upOrigen, edat from {} where edat >=6 and edat <= 14 and ates=1".format(assig)
    nens = {id_cip_sec: (up, edat) for id_cip_sec, up, edat in getAll(sql, nod)}

    """ Se obtiene para cada id_cip_sec de nino, la upOrigen y su edad y se guarda en un diccionario.
        Devuelve {id_cip_sec: (up, edat)}
    """
    sql = "select id_cip_sec, upOrigen, edat from {} where edat < 6 and edat > 0 and ates=1".format(assig)
    nens_petits = {id_cip_sec: (up, edat) for id_cip_sec, up, edat in getAll(sql, nod)}

    """ Se obtiene para cada id_cip_sec de mujer, la upOrigen y su br y se guarda en un diccionario.
        Devuelve {id_cip_sec: (up, edat)}
    """
    sql = "select id_cip_sec, up from {} where sexe = 'D' and ates=1".format(assig)
    dones = {id_cip_sec: {"up": up, "br": cat_centres[up]} for id_cip_sec, up in getAll(sql, "nodrizas") if up in cat_centres}
    """ Se obtiene para cada id_cip_sec de adulto, la upOrigen y su br y se guarda en un diccionario.
        Devuelve {id_cip_sec: (up, edat)}
    """
    sql = "select id_cip_sec, up, edat from {} where edat >= 15 and ates=1".format(assig)
    adults = {id_cip_sec: (up, edat) for id_cip_sec, up, edat in getAll(sql, nod)}

def get_embarassades():
    """."""

    global embarassades

    sql = "select id_cip_sec, inici, fi from ass_embaras where fi >= '{}' and inici <= date_sub('{}', interval - 90 day)".format(data_ext_menys1any, data_ext)
    embarassades = {id_cip_sec: {"inici": inici, "fi":fi} for id_cip_sec, inici, fi in getAll(sql, "nodrizas") if id_cip_sec in dones}

def get_caods():
    """."""

    global caods_embaras

    caods_embaras = dict()

    sql = "select id_cip_sec, dat, valor from odn_variables where agrupador = 308 and dat between '{}' and '{}'".format(data_ext_menys2anys, data_ext)
    for id_cip_sec, data, valor in getAll(sql, "nodrizas"):
        if id_cip_sec in embarassades:
			if embarassades[id_cip_sec]["inici"] <= data <= embarassades[id_cip_sec]["fi"]:
				if id_cip_sec in caods_embaras:
					if caods_embaras[id_cip_sec]["data_max"] < data:
						caods_embaras[id_cip_sec] = {"data_max": data, "valor" : valor}
				else:
					caods_embaras[id_cip_sec] = {"data_max": data, "valor": valor}

def get_uba_pacient_tables(nens,numerador,codi,numerador_type,id_to_hash,rev_ids,ind_edat):
	""" Se generan el indicador en cuenta que tipo de numerador es necesario y en el caso de ser a nivel de paciente, se guardan los no cumplidores.
		Se itera por el diccionario de nens, donde se obtiene la up y la edat. Con la edat se obtiene el subindicador al que pertenece.
		El indicador se calcula agrupando por la up y el subindicador y a la hora de iterar dentro de una misma up, por los subindicadores
		que se han ido calculando. Para calcular el global, se van sumando en num_total y den_total cada uno de los valores de los subindicadores.

		Devuelve doss listas de filas (tuple) a nivel de up y a nivel de paciente.
	"""
	inds_from_inds = {
		'IAD0021e': 'IAD0027',
		'IAD0022e': 'IAD0027',
		'IAD0012b': 'IAD0028',
		'IAD0016b': 'IAD0028',
		'IAD0011b': 'IAD0029',
		'IAD0015b': 'IAD0029',
		'IAD0011g': 'IAD0030',
		'IAD0015g': 'IAD0030'
	}
	uba_rows=[]
	row_pacient=[]
	pacient_rows=[]
	indicador=defaultdict(lambda: defaultdict(Counter))
	#sql = "select id_cip_sec, upOrigen,edat from {} where edat >=6 and edat <= 14 and ates=1 ".format(assig)
	for id_cip_sec in rev_ids:
		up,edat=nens[id_cip_sec]
		sub_index=ind_edat[edat]
		if numerador_type in ('mitjana','pacient'):
			indicador[up][sub_index]['DEN']+=1
			
		if id_cip_sec in numerador:
			if numerador_type=="mitjana":
				indicador[up][sub_index]['NUM']+=numerador[id_cip_sec]
				if codi+sub_index in inds_from_inds:
					hash, sector = id_to_hash[id_cip_sec]
					# print(inds_from_inds[codi+sub_index])
					if numerador[id_cip_sec] > 0:
						# print(numerador[id_cip_sec])
						nous_indicadors[(id_cip_sec, inds_from_inds[codi+sub_index], 'NUM', up, hash, sector)] += numerador[id_cip_sec]
						
			elif numerador_type=="obturacio":
				indicador[up][sub_index]['NUM']+=numerador[id_cip_sec][0]
				indicador[up][sub_index]['DEN']+=numerador[id_cip_sec][1]
						

			elif numerador_type=="pacient":
				indicador[up][sub_index]['NUM']+=1
				# if codi+sub_index in inds_from_inds:
				# 	hash, sector = id_to_hash[id_cip_sec]
				# 	nous_indicadors[(id_cip_sec, inds_from_inds[codi+sub_index], 'DEN', up, hash, sector)] += 1
                
			if codi == 'IAD0013':
				hash,sector=id_to_hash[id_cip_sec]
				for sub in (ind_edat[edat],''):
					pacient_rows.append((hash,sector, codi+sub,up,"sense","sense",None, 0))

		elif numerador_type=="pacient" and id_cip_sec not in numerador and codi != 'IAD0013':
			hash,sector=id_to_hash[id_cip_sec]
			for sub in (ind_edat[edat],''):
				pacient_rows.append((hash,sector, codi+sub,up,"sense","sense",None, 0))
			if codi+sub_index in inds_from_inds:
				hash, sector = id_to_hash[id_cip_sec]
				nous_indicadors[(id_cip_sec, inds_from_inds[codi+sub_index], 'DEN', up, hash, sector)] += 1

	for up in indicador:
		num_total=0
		den_total=0
		for sub_index in indicador[up]:
			num_total+= indicador[up][sub_index]['NUM']
			den_total+= indicador[up][sub_index]['DEN']
			if indicador[up][sub_index]['DEN'] != 0:
				uba_rows.append((up, codi+sub_index,"sense", "O", indicador[up][sub_index]['NUM'],indicador[up][sub_index]['DEN'],indicador[up][sub_index]['NUM']/float(indicador[up][sub_index]['DEN']) ))
		
		if den_total !=0:
			uba_rows.append((up, codi,"sense", "O", num_total,den_total, num_total/float(den_total)) )  
	#export_txt_file("{}_{}.txt".format(numerador_type,codi),["CIP","SECTOR","CODI","UP","Obturacions","Index"],row_pacient)
	
	return uba_rows,pacient_rows


def validation_by_up_pacient(up_tuple,table_name,table_columns):
	sql="select * from altres.exp_ecap_index_caries_{0} where up in ({1})".format(table_name,",".join(up_tuple))
	up_rows= [(row) for row in getAll(sql,alt)]
	export_table("exp_ecap_index_caries_{0}_prova".format(table_name),
				table_columns,
				alt,
				up_rows)
	
def get_id_hash():
    sql="select id_cip_sec,hash_d,codi_sector from import.u11"
    return {id_cip_sec:(hash_d,sector) for (id_cip_sec,hash_d,sector) in getAll(sql,"import")}

def get_cip_dict(hashd):
    sql="select usua_cip,usua_cip_cod from pdptb101 where usua_cip_cod='{}'".format(hashd)
    return getOne(sql,"pdp")[0]

def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)

def get_iad0025_26():
	sql = """
		SELECT id_cip_sec, edat, up
		FROM assignada_tot
		WHERE edat >= 18
	"""
	pob = {}
	for id, edat, up in getAll(sql, 'nodrizas'):
		pob[id] = {
			'edat': edat,
			'up': up
		}

	sql = """
		SELECT id_cip_sec, dfd_p_a, dfd_cod_p
		FROM odn506
	"""
	inds = defaultdict(set)
	count = Counter()
	sense_peces = set()
	for id, var, codi in getAll(sql, 'import'):
		if id in pob:
			edat = pob[id]['edat']
			up = pob[id]['up']
			if edat >= 35:
				inds[(up, 'IAD0026', 'DEN')].add(id)
			inds[(up, 'IAD0025', 'DEN')].add(id)
			if var == 'P' and codi not in (18,28,38,48):
				count[(up, 'IAD0025','NUM')] += 1
				sense_peces.add(id)
				
	for (up, ind, analisi) in list(inds.keys()):
		if ind == 'IAD0026':
			for id in list(inds[(up, ind, analisi)]):
				edat = pob[id]['edat']
				if edat >= 35 and id not in sense_peces:
					inds[(up, 'IAD0026','NUM')].add(id)
	resultat = []
	for (up, ind, analisi) in list(inds.keys()):
		resultat.append((ind, up, analisi, len(inds[(up, ind, analisi)])))
	for (up, ind, analisi) in list(count.keys()):
		resultat.append((ind, up, analisi, count[(up, ind, analisi)]))
	return resultat

def get_iad_cancer():
	printTime('inici iad_cancer')
	sql = """
		SELECT id_cip_sec, up
		FROM assignada_tot
		WHERE edat between 35
		AND 64
	"""
	pob = {}
	for id, up in getAll(sql, 'nodrizas'):
		pob[id] = up
	printTime('poblacio')
	sql = """
		SELECT id_cip_sec,
		case when dde >= DATE '{LAST12}'
		then 1 else 0 end
		FROM eqa_problemes
		WHERE ps = 1049
		AND dde <= DATE '{DEXTD}'
	""".format(LAST12=data_ext_menys1any, DEXTD=data_ext)
	inds = Counter()
	pacs = defaultdict()
	for id in pob:
		up = pob[id]
		inds[(up, 'IAD0031', 'DEN')] += 1
		inds[(up, 'IAD0032', 'DEN')] += 1
		pacs[(id, up, 'IAD0031')] = 0
		pacs[(id, up, 'IAD0032')] = 0
	printTime('proc pob')
	for id, data in getAll(sql, 'nodrizas'):
		if id in pob:
			up = pob[id]
			if data:
				inds[(up, 'IAD0032', 'NUM')] += 1
				pacs[(id, up, 'IAD0032')] = 1
			else:
				inds[(up, 'IAD0031', 'NUM')] += 1
				pacs[(id, up, 'IAD0031')] = 1
	printTime('eqa_problemes')
	
	resultat = []
	for (up, ind, analisi) in inds:
		resultat.append((ind, up, analisi, inds[(up, ind, analisi)]))
	printTime('resultat')
	resultat_pac = []
	for (id, up, ind) in pacs:
		resultat_pac.append((id, up, ind, pacs[(id, up, ind)]))
	printTime('resultat_pac')
	return (resultat, resultat_pac)

def get_iad_caries_notract():
	sql = """
		SELECT id_cip_sec, up, year(data_naix), edat
		FROM assignada_tot
	"""
	pob = {}
	for id, up, data, edat in getAll(sql, 'nodrizas'):
		pob[id] = (up, data, edat)
	
	sql = """
		SELECT id_cip_sec, dfd_cod_p
		FROM odn506
		WHERE dfd_p_a = 'P'
		AND dfd_estat = 'C'
	"""
	caries = defaultdict(set)
	for id, val in getAll(sql, 'import'):
		caries[38].add(id)
		if val > 48:
			caries[37].add(id)
		if val < 55:
			caries[39].add(id)
			caries[40].add(id)
	
	sql = """
		SELECT id_cip_sec, dtd_cod_p, dtd_data, dtd_tra
		FROM odn508
	"""
	protesis = defaultdict(set)
	tractaments = defaultdict(set)
	for id, peca, data, tract in getAll(sql, 'import'):
		protesis[id].add(data)
		tractaments[(id, data)].add(tract)
	
	protesis_rem = set()
	for id in protesis:
		data = max(protesis[id])
		if (id, data) in tractaments:
			if 'PPR' in tractaments[(id, data)] or 'PSR' in tractaments[(id, data)]:
				protesis_rem.add(id)
	
	# cuinetes
	resultats = Counter()
	for id in pob:
		(up, data, edat) = pob[id]
		edat_any = data_ext.year - data
		if edat_any == 5:
			resultats[(up, 'IAD0037', 'DEN')] += 1
			if id in caries[37]:
				resultats[(up, 'IAD0037', 'NUM')] += 1
		elif edat_any == 7:
			resultats[(up, 'IAD0038', 'DEN')] += 1
			if id in caries[38]:
				resultats[(up, 'IAD0038', 'NUM')] += 1
		elif edat_any == 12:
			resultats[(up, 'IAD0039', 'DEN')] += 1
			if id in caries[39]:
				resultats[(up, 'IAD0039', 'NUM')] += 1
		if 35 <= edat <= 44:
			resultats[(up, 'IAD0040', 'DEN')] += 1
			if id in caries[40]:
				resultats[(up, 'IAD0040', 'NUM')] += 1
		resultats[(up, 'IAD0041', 'DEN')] += 1
		if id in protesis_rem:
			resultats[(up, 'IAD0041', 'NUM')] += 1
	upload = []
	for (up, ind, analisi), val in resultats.items():
		upload.append((ind, up, analisi, val))
	return upload

def get_ind_from_ind():
	# nous_indicadors[(inds_from_inds[codi+sub_index], 'DEN')].add((id_cip_sec, up, hash, sector, 1))
	# indicador[up][sub_index]['NUM']+=1
	# uba_rows.append((up, codi,"sense", "O", num_total,den_total, num_total/float(den_total)) )
	pacients = []
	ubas = []
	indicador = defaultdict(Counter)
	for (id, codi, analisi, up, hash, sector) in nous_indicadors:
		if analisi == 'DEN':
				# print(codi)
				indicador[(up, codi)]['DEN'] += nous_indicadors[(id, codi, analisi, up, hash, sector)]
				if (id, codi, 'NUM', up, hash, sector) in nous_indicadors:
					indicador[(up, codi)]['NUM'] += nous_indicadors[(id, codi, 'NUM', up, hash, sector)]
				else:
					# pacient_rows.append((hash,sector, codi+sub,up,"sense","sense",None, 0))
					pacients.append((hash, sector, codi, up, 'sense', 'sense', None, 0))
	printTime('despres proc inds')
	# for (up, codi) in indicador:
	# 	if indicador[(up, codi)]['NUM'] > 0:
	# 		print(indicador[(up, codi)]['NUM'])

	for (up, codi) in indicador:
		num = indicador[(up, codi)]['NUM']
		den = indicador[(up, codi)]['DEN']
		ubas.append((up, codi, 'sense', 'O', num, den, num/float(den)))
	
	return ubas, pacients

def get_iad0033(rev_ids, nens, id_to_hash):
	#se obtiene el denominador total -> para cada rango de edad (letter-el subindex-), se obtienen {up: set(ids)}
	# global caries_primer
	caries_primer = set()
	sql = """
		SELECT id_cip_sec
		FROM odn_peces
		WHERE peca in (16,26,36,46)
		AND pato like '%@CA@%'
	"""
	for id, in getAll(sql, 'nodrizas'):
		caries_primer.add(id)
	sql = """
		SELECT id_cip_sec
		FROM odn508
		WHERE dtd_cod_p in (16, 26, 36, 46)
		AND dtd_tra in ('EX', 'EXO', 'OBT')
		AND dtd_pat = 'CA'
	"""
	for id, in getAll(sql, 'import'):
		caries_primer.add(id)
	
	# ho poso aixi a part perque sembla que amb la resta d'indicadors no funciona
	# s'ha d'averiguar per que
	print('VALIDACIO CARIES')
	iad0033 = defaultdict(Counter)
	pacient_rows = []
	for id in rev_ids:
		up,edat=nens[id]
		iad0033[up]['DEN'] += 1
		if id in caries_primer:
			iad0033[up]['NUM'] += 1
		else:
			hash, sector = id_to_hash[id]
			pacient_rows.append((hash,sector, 'IAD0033',up,"sense","sense",None, 0))
	uba_rows = []
	for up in iad0033:
		uba_rows.append((up, 'IAD0033', "sense", "O", iad0033[up]['NUM'], iad0033[up]['DEN'], iad0033[up]['NUM']/float(iad0033[up]['DEN'])))

	return uba_rows, pacient_rows

if __name__ == '__main__':
# comencem!

	#### VARIABLES COMUNES QUE SE NECESITAN PARA CASI TODOS LOS INDICADORES ###
	#se crea la tabla donde van a ir todos los indicadores
	printTime('inici')
	get_data_extraccio()
	get_centres()
	id_to_hash=get_id_hash()
	
	table_name="exp_ecap_index_caries_uba"
	uba_columns="(up varchar(5),indicador varchar(15),uba varchar(5),tipus varchar(5),numerador double, denominador int, resultat double)"
	createTable(table_name,uba_columns,alt,rm=True)

	table_name_pacient="exp_ecap_index_caries_pacient"
	pacient_columns="(hash varchar(100),sector int,indicador varchar(15),up varchar(5),uba varchar(5),upinf varchar(5),ubainf  varchar(5), exclos int)"	
	createTable(table_name_pacient,pacient_columns,alt,rm=True)

	#se obtienen para el agrupador de ARC (286), los ids de los pacientes -> {agr:set(ids)}
	numerador_by_agr=get_ids_by_agr(data_ext) 
	printTime('Agr-id_cip_sec')

	#para todas las patologias y tratamientos en pat_list y trat_list respectivamente, se obtiene los ids de los pacientes que la han sufrido y 
	#para cada uno de estos las piezas en donde lo han sufrido y la fecha de diagnostico
	print(pat_list)
	print(trat_list)
	peces_dat=get_peces_dat_by_criteris({
										("pat","odn507"):pat_list, 
										("tra","odn508"):trat_list		
										})
	
	printTime('{} peces_dat done'.format(len(peces_dat)))

	#Se obtienen las piezas ausentes
	peces_A=get_absencies()

	#Se obtienen la poblaci�n (ninos, mujeres...)
	get_poblacio()
	printTime('nens',len(nens))
	printTime('nens_petits',len(nens_petits))
	printTime('dones',len(dones))
	printTime('adults',len(adults))
	
# Se obtienen mujeres embarazadas y mujeres con CAOD realizadas en el �ltimo a�o
	get_embarassades()
	printTime("get_embarassades()", len(embarassades))
	get_caods()
	printTime("get_caods()", len(caods_embaras))

	#Se obtienen los ninos con revision odontologica
	rev_ids=get_revision_odn(rev_agr,nens)
	printTime("rev_ids", len(rev_ids))
	rev_ids_petits = get_revision_odn(rev_agr,nens_petits)
	printTime("rev_ids_petits", len(rev_ids_petits))
	rev_ids_adults = get_revision_odn(rev_agr, adults)
	printTime("rev_ids_adults", len(rev_ids_adults))

	#Se obtienen los index de caries, que se guardan a nivel de paciente (id_cip_sec) y denticion (agr) en numerador_by_agr y 
	#las obturaciones en obt_peces a nivel de paciente (id_cip_sec) y denticion (agr)
	numerador_by_agr,obt_peces=get_indexes(rev_ids,peces_dat,peces_A,numerador_by_agr)
	numerador_by_agr_petits,obt_peces_petits=get_indexes(rev_ids_petits,peces_dat,peces_A,numerador_by_agr)
	numerador_by_agr_adults,obt_peces_adults=get_indexes(rev_ids_adults,peces_dat,peces_A,numerador_by_agr)
	printTime('indexes')
	printTime(len(numerador_by_agr[307]))
	printTime(len(numerador_by_agr[308]))
	printTime(len(numerador_by_agr_petits[307]))
	printTime(len(numerador_by_agr_petits[308]))
	printTime(len(numerador_by_agr_adults[307]))
	printTime(len(numerador_by_agr_adults[308]))

	## MONTAR LOS INDICADORES ##

	for numerador_type, codi_agr_dict in codi_indicadors.iteritems():
		#Iteramos sobre tipo de numerador (mitjana,pacient,obturacio) y los diccionarios con los codigos globales
		for codi,agr_options in codi_agr_dict.iteritems():
			#Para cada codigo segun numerador type, con su diccionario de opciones correspondiente
			print(codi)
			print(agr_options) 
			if numerador_type=="mitjana":
				#si es mitjana, el numerador va a ser el valor del agrupador que hay en opciones para el indicador
				if codi == 'IAD0021': 
					num=numerador_by_agr_petits[agr_options]
				elif codi in ('IAD0035', 'IAD0036'): 
					num=numerador_by_agr_adults[agr_options]
				else:
					num=numerador_by_agr[agr_options]
					
			elif numerador_type=="pacient":
				#si es paciente, queremos a los pacientes con ciertos index de caries
				# Cuando se requiere mas de un index, es necesario especificar si son pacientes con ambos indices o con un index u otro, por eso en las
				#opciones para estos indicadores (agr_options) es un tuple (agr,set_operation) donde agr es un diccionario con {agr:valor agr} y set_operation es la
				# operacion "union" o "interseccion"
				agr,set_operation=agr_options
				print(set_operation)
				if set_operation:
					# se crean sets de ids que presentan el agrupador y el valor especificado y cada uno de estos sets se garda en una lista. La lista resultante de sets se pasa
					# a la operacion set en cuestion (union o interseccion) 
					if codi == 'IAD0034':
						num=set_operation(*[{id_cip_sec for id_cip_sec, val in numerador_by_agr_adults[agrupador].iteritems() if ops[valor[0]](val,int(valor[1:]))} for agrupador,valor in agr.iteritems()])
						printTime('Get adults amb {} OK'.format(agr))
					elif codi != 'IAD0022':
						num=set_operation(*[{id_cip_sec for id_cip_sec, val in numerador_by_agr[agrupador].iteritems() if ops[valor[0]](val,int(valor[1:]))} for agrupador,valor in agr.iteritems()])
						printTime('Get nens amb {} OK'.format(agr))
					else:
						num=set_operation(*[{id_cip_sec for id_cip_sec, val in numerador_by_agr_petits[agrupador].iteritems() if ops[valor[0]](val,int(valor[1:]))} for agrupador,valor in agr.iteritems()])
						printTime('Get nens petits amb {} OK'.format(agr))
				else:
					if codi == 'IAD0034':
						num={id_cip_sec for agrupador,valor in agr.iteritems() for id_cip_sec,val in numerador_by_agr_adults[agrupador].iteritems() if ops[valor[0]](val,int(valor[1:])) }
						printTime('Get adults amb {} OK'.format(agr))
					elif codi != 'IAD0022':
						num={id_cip_sec for agrupador,valor in agr.iteritems() for id_cip_sec,val in numerador_by_agr[agrupador].iteritems() if ops[valor[0]](val,int(valor[1:])) }
						printTime('Get nens amb {} OK'.format(agr))
					else:
						num={id_cip_sec for agrupador,valor in agr.iteritems() for id_cip_sec,val in numerador_by_agr_petits[agrupador].iteritems() if ops[valor[0]](val,int(valor[1:])) }
						printTime('Get nens petits amb {} OK'.format(agr))
				
			elif numerador_type=="obturacio":
				#se obtienen las piezas en las que hay que buscar por obturaciones y el la lista de agrupadores que se necesitan para el denominador de la O
				peces,agr_list=agr_options
					
				num=get_obturacio_pat_trat(obt_peces,agr_list)
				print(len(num))
					
			if codi in ('IAD0034', 'IAD0035', 'IAD0036'):
				uba_rows,pacient_rows=get_uba_pacient_tables(adults,num,codi,numerador_type,id_to_hash,rev_ids_adults,ind_edat_adults)
				printTime('Retrieving rows new table OK codi {}'.format(codi))
			elif codi not in ('IAD0021', 'IAD0022'):	
				uba_rows,pacient_rows=get_uba_pacient_tables(nens,num,codi,numerador_type,id_to_hash,rev_ids,ind_edat)
				printTime('Retrieving rows new table OK codi {}'.format(codi))
			else:
				uba_rows,pacient_rows=get_uba_pacient_tables(nens_petits,num,codi,numerador_type,id_to_hash,rev_ids_petits,ind_edat_petits)
				printTime('Retrieving rows new table OK codi {}'.format(codi))

			listToTable(uba_rows,table_name,alt)
			listToTable(pacient_rows,table_name_pacient,alt)
			printTime("Insert into the table {}".format(codi))

	uba_rows, pacient_rows = get_ind_from_ind()
	listToTable(uba_rows,table_name,alt)
	listToTable(pacient_rows,table_name_pacient,alt)

	if validation:
		validation_by_up_pacient(up_codi,"uba",uba_columns)
		validation_by_up_pacient(up_codi,"pacient",pacient_columns)

	# Montamos indicador IAD0033
	uba_rows, pacient_rows = get_iad0033(rev_ids, nens, id_to_hash)

	listToTable(uba_rows,table_name,alt)
	listToTable(pacient_rows,table_name_pacient,alt)
	printTime("Insert into the table {}".format('IAD0033'))
	# # Montamos indicador IAD00024

	res_IAD00024 = Counter()

	for id_cip_sec in embarassades:
		if id_cip_sec in caods_embaras:
			br = dones[id_cip_sec]["br"]
			res_IAD00024[("IAD00024", "{}{}".format(str(data_ext.year)[:2], str(data_ext.month).zfill(2)), br, "DEN")] += 1
			res_IAD00024[("IAD00024", "{}{}".format(str(data_ext.year)[:2], str(data_ext.month).zfill(2)), br, "NUM")] += caods_embaras[id_cip_sec]["valor"]

	res_IAD00024_export = list()

	for (indicador, periode, br, analisi), n in res_IAD00024.items():
		res_IAD00024_export.append((indicador, periode, br, analisi, n))
	columnes = "(indicador varchar(10), periode varchar(10), br varchar(5), analisi varchar(3), n int)"
	createTable("exp_ecap_IAD00024_uba", columnes, "altres", rm = True)
	listToTable(res_IAD00024_export, "exp_ecap_IAD00024_uba", "altres")

	upload = get_iad0025_26()
	columnes = "(indicador varchar(10), up varchar(10), analisi varchar(3), n int)"
	createTable("IAD0025_26", columnes, "altres", rm = True)
	listToTable(upload, "IAD0025_26", "altres")

	(upload, pacients_c) = get_iad_cancer()
	columnes = "(indicador varchar(10), up varchar(10), analisi varchar(3), n int)"
	createTable("iad_cancer", columnes, "altres", rm = True)
	listToTable(upload, "iad_cancer", "altres")
	columnes = "(id_cip_sec int, up varchar(10), indicador varchar(10), num int)"
	createTable("iad_cancer_llistat", columnes, "altres", rm = True)
	listToTable(pacients_c, "iad_cancer_llistat", "altres")

	upload = get_iad_caries_notract()
	columnes = "(indicador varchar(10), up varchar(10), analisi varchar(3), n int)"
	createTable("iad_caries", columnes, "altres", rm = True)
	listToTable(upload, "iad_caries", "altres")

    # export a klx
	sql = ["select indicador, concat('A', 'periodo'), ics_codi, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', numerador \
            from altres.exp_ecap_index_caries_uba a \
            inner join nodrizas.cat_centres b on a.up = b.scs_codi"]
	sql.append("select indicador, concat('A', 'periodo'), ics_codi, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', denominador \
                from altres.exp_ecap_index_caries_uba a \
                inner join nodrizas.cat_centres b on a.up = b.scs_codi")
	# Indicador iad0020
	sql.append("select 'IAD0020' indicador, concat('A', 'periodo'), ics_codi, 'NUM', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', sum(numerador) \
                from altres.exp_ecap_IAD0020_uba a \
                inner join nodrizas.cat_centres b on a.up = b.scs_codi group by ics_codi")
	sql.append("select 'IAD0020' indicador, concat('A', 'periodo'), ics_codi, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', sum(denominador) \
                from altres.exp_ecap_IAD0020_uba a \
                inner join nodrizas.cat_centres b on a.up = b.scs_codi group by ics_codi")
	# Indicador iad00024
	sql.append("select indicador, concat('A', 'periodo'), br, analisi, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', n \
			    from altres.exp_ecap_IAD00024_uba")
	# Indicadors iad0025 i iad0026
	sql.append("select indicador, concat('A', 'periodo'), ics_codi, analisi, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', n \
			    from altres.IAD0025_26 a inner join nodrizas.cat_centres b on a.up = b.scs_codi")
	# Indicadors iad0031 i iad0032
	sql.append("select indicador, concat('A', 'periodo'), ics_codi, analisi, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', n \
			    from altres.iad_cancer a inner join nodrizas.cat_centres b on a.up = b.scs_codi")
	sql.append("select indicador, concat('A', 'periodo'), ics_codi, analisi, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', n \
				from altres.iad_caries a inner join nodrizas.cat_centres b on a.up = b.scs_codi")
	
	exportKhalix(" union ".join(sql), "INDEX_CARIES")
