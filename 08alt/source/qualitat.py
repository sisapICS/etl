import sisapUtils as u


file = "QUALITAT"


sql = "select data_ext, adddate(data_ext, interval -1 month) from dextraccio"
dext, fa1m = u.getOne(sql, "nodrizas")
dates = [(dext.year, dext.month), (fa1m.year, fa1m.month)]

upload = []
sql = "select sisap_codi_indicador, \
              'A'||substr(sisap_anyo, 3, 2)||lpad(sisap_mes, 2, 0), \
              sisap_codi_catsalut, sisap_codi_br, \
              sisap_num_den, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', sisap_valor \
       from dwct.sisap_facts a \
       where sisap_anyo = {} and sisap_mes = {}"
for data in dates:
    upload.extend(u.getAll(sql.format(*data), "exadata"))

cols = ["k{} varchar(16)".format(i) for i in range(9)] + ["v decimal(16,2)"]
u.createTable(file, "({})".format(", ".join(cols)), "altres", rm=True)
u.listToTable(upload, file, "altres")

sql = """select DISTINCT K0, K1, case when K3 = 'BR056' THEN K3 else ICS_CODI end K2, K4, K5, K6, K7, K8, V 
        from altres.{taula} inner join  NODRIZAS.CAT_CENTRES_WITH_JAIL on K2 = SCS_CODI
        where k0 like 'AG%'
        union
        select distinct K0, K1, k3, K4, K5, K6, K7, K8, V 
        from altres.{taula} inner join  NODRIZAS.ass_centres on K3 = BR_ASSIR
        where k0 like 'AG%'"""
u.exportKhalix(sql.format(taula=file), file)
sql = """select DISTINCT K0, K1, case when K3 = 'BR056' THEN K3 else ICS_CODI end K2, K4, K5, K6, K7, K8, V 
        from altres.{taula} inner join  NODRIZAS.CAT_CENTRES_WITH_JAIL on K2 = SCS_CODI
        where k0 like 'CPR%'
        union
        select distinct K0, K1, k3, K4, K5, K6, K7, K8, V 
        from altres.{taula} inner join  NODRIZAS.ass_centres on K3 = BR_ASSIR
        where k0 like 'CPR%'"""
u.exportKhalix(sql.format(taula=file), file + "_CPR")