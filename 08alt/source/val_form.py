# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import dateutil.relativedelta
import datetime

# Par�metres de les taules on emmagatzemar les dades dels indicadors
db = "altres"
tb_agregat = "exp_khalix_up_val_form"
tb_mst = "mst_indicadors_pacient_val_form"
cols_agregat = "(indicador varchar(10), analisi varchar(3), up varchar(5), uba varchar(10), ubainf varchar(10), sexe varchar(4), institucionalitzat varchar(10), n int)"
cols_mst = "(indicador varchar(10), id_cip_sec int, up varchar(5), uba varchar(10), ubainf varchar(10), sexe varchar(4), institucionalitzat varchar(10), den int, num int)"

# Consulta SQL per obtenir la data d'extracci�
sql = """
        SELECT
            data_ext
        FROM
            dextraccio
        """
dext, = u.getOne(sql, "nodrizas")
dext_menys1any = dext - dateutil.relativedelta.relativedelta(years=1)
dext_menys30dies = dext - dateutil.relativedelta.relativedelta(days=30)


class ValoracionsFormularis(object):

    def __init__(self):

        self.get_poblacio();                        print("self.get_poblacio()", datetime.datetime.now())
        self.get_xml();                             print("self.get_xml()", datetime.datetime.now())
        self.get_placures();                        print("self.get_placures()", datetime.datetime.now())
        self.build_indicadors();                    print("self.build_indicadors()", datetime.datetime.now())
        self.export_indicadors();                   print("self.export_indicadors()", datetime.datetime.now())
        self.export_klx();                          print("self.export_klx()", datetime.datetime.now())

    def get_poblacio(self):
        """Obt� les dades b�siques de la poblaci� assignada atesa."""

        global poblacio_atesa

        sql = """
                SELECT
                    id_cip,
                    id_cip_sec,
                    up,
                    uba,
                    ubainf,
                    sexe,
                    institucionalitzat,
                    pcc,
                    maca,
                    atdom
                FROM
                    assignada_tot_with_jail
                WHERE
                    ates = 1
              """
        poblacio_atesa = {id_cip: (id_cip_sec, up, uba, ubainf, u.sexConverter(sexe), "INSAT" if institucionalitzat else "NOINSAT", pcc, maca, atdom)
                               for id_cip, id_cip_sec, up, uba, ubainf, sexe, institucionalitzat, pcc, maca, atdom in u.getAll(sql, 'nodrizas')} 

    def get_xml(self):
        """Obt� les dades dels formularis XML."""

        self.valoracions_cuidador_1any, self.valoracions_cliniques_1any, self.formularis_nafres_30dies = set(), set(), set()

        taules = tuple(u.getSubTables("xml")) + ("import_jail.xml", "import_jail.xml_detall")

        for taula, dades in u.multiprocess(sub_get_xml, taules, 4):

            if taula == "xml_detall":
                sub_valoracions_cuidador_1any, sub_valoracions_cliniques_1any = dades
                self.valoracions_cuidador_1any.update(sub_valoracions_cuidador_1any)
                self.valoracions_cliniques_1any.update(sub_valoracions_cliniques_1any)

            elif taula == "xml":
                sub_formularis_nafres_30dies, = dades
                self.formularis_nafres_30dies.update(sub_formularis_nafres_30dies)

    def get_placures(self):
        """Obt� les dades dels plans de cures oberts de nafres i ferides cr�niques."""

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    nodrizas.master_placures
                WHERE
                    pc_codi IN ('PC0035', 'PC0073', 'PC0074', 'PC0086', 'PC0087', 'PC0088', 'PC0111', 'PC0128', 'PC0129')
                    AND flag_pc_obert = 1
              UNION
                SELECT
                    id_cip_sec
                FROM
                    nodrizas.master_placures_jail
                WHERE
                    pc_codi IN ('PC0035', 'PC0073', 'PC0074', 'PC0086', 'PC0087', 'PC0088', 'PC0111', 'PC0128', 'PC0129')
                    AND flag_pc_obert = 1
              """
        self.placures_nafres_oberts = {id_cip_sec for id_cip_sec, in u.getAll(sql, "import")}

    def build_indicadors(self):
        """Construeix els indicadors a partir de les dades obtingudes."""

        self.resultat_agregat, self.resultat_mst = c.defaultdict(set), c.defaultdict(set)
        
        def agregar_indicador(indicador, analisi, dades_pacient):
            id_cip_sec, up, uba, ubainf, sexe, institucionalitzat, pcc, maca, atdom = dades_pacient
            self.resultat_mst[(indicador, id_cip_sec)] = [up, uba, ubainf, sexe, institucionalitzat, 1, 0 if analisi == "DEN" else 1] 
            self.resultat_agregat[(indicador, "DEN", up, uba, ubainf, sexe, institucionalitzat)].add(id_cip_sec)
            if analisi == "NUM":
                self.resultat_agregat[(indicador, "NUM", up, uba, ubainf, sexe, institucionalitzat)].add(id_cip_sec)

        for id_cip, dades_pacient in poblacio_atesa.items():
            
            id_cip_sec, _, _, _, _, _, pcc, maca, atdom = dades_pacient

            if pcc:

                # Indicador VALFORM01
                analisi = "NUM" if id_cip in self.valoracions_cuidador_1any else "DEN" # i no NUM
                agregar_indicador("VALFORM01", analisi, dades_pacient)

                # Indicador VALFORM04
                analisi = "NUM" if id_cip in self.valoracions_cliniques_1any else "DEN" # i no NUM
                agregar_indicador("VALFORM04", analisi, dades_pacient)
                
            if maca:

                # Indicador VALFORM02
                analisi = "NUM" if id_cip in self.valoracions_cuidador_1any else "DEN" # i no NUM
                agregar_indicador("VALFORM02", analisi, dades_pacient)

                # Indicador VALFORM05
                analisi = "NUM" if id_cip in self.valoracions_cliniques_1any else "DEN" # i no NUM
                agregar_indicador("VALFORM05", analisi, dades_pacient)

            if atdom:

                # Indicador VALFORM03
                analisi = "NUM" if id_cip in self.valoracions_cuidador_1any else "DEN" # i no NUM
                agregar_indicador("VALFORM03", analisi, dades_pacient)

                # Indicador VALFORM06
                analisi = "NUM" if id_cip in self.valoracions_cliniques_1any else "DEN" # i no NUM
                agregar_indicador("VALFORM06", analisi, dades_pacient)

            if id_cip_sec in self.placures_nafres_oberts:
                
                # Indicador VALFORM07
                analisi = "NUM" if id_cip in self.formularis_nafres_30dies else "DEN" # i no NUM
                agregar_indicador("VALFORM07", analisi, dades_pacient)

        # Processa els resultats agregats per exportar
        self.resultat_exportar_agregat = [(indicador, analisi, up, uba, ubainf, sexe, institucionalitzat, len(pacients))
                                         for (indicador, analisi, up, uba, ubainf, sexe, institucionalitzat), pacients in self.resultat_agregat.items()]
        # Processa els resultats detallats per exportar
        self.resultat_exportar_mst = [(indicador, id_cip_sec, up, uba, ubainf, sexe, institucionalitzat, den, num)
                                     for (indicador, id_cip_sec), (up, uba, ubainf, sexe, institucionalitzat, den, num) in self.resultat_mst.items()]

    def export_indicadors(self):
        """Crea les taules i exporta les dades dels indicadors de valoracions de formularis."""

        u.createTable(tb_agregat, cols_agregat, db, rm=True)
        u.createTable(tb_mst, cols_mst, db, rm=True)

        u.listToTable(self.resultat_exportar_agregat, tb_agregat, db)
        u.listToTable(self.resultat_exportar_mst, tb_mst, db)

    def export_klx(self):
        """Exportaci� de les dades a Longview."""
        
        # Arxiu per UPs
        
        file = "VALFORM"

        sql = """
                SELECT
                    indicador,
                    'Aperiodo',
                    ics_codi,
                    analisi,
                    'NOCAT',
                    institucionalitzat,
                    sexe,
                    'N',
                    sum(n)
                FROM
                    {_db}.{_tb_agregat} a
                INNER JOIN nodrizas.cat_centres b ON
                    up = scs_codi
                GROUP BY
                    indicador,
                    ics_codi,
                    analisi,
                    institucionalitzat,
                    sexe
              """.format(_db=db, _tb_agregat=tb_agregat)
        u.exportKhalix(sql, file)        
       
        # Arxiu per UBAs
        
        file = "VALFORM_UBA"

        sql = """
                SELECT
                    indicador,
                    'Aperiodo',
                    CONCAT(ics_codi, 'M', uba) AS uba_completa_med,
                    analisi,
                    'NOCAT',
                    institucionalitzat,
                    'DIM6SET',
                    'N',
                    sum(n)
                FROM
                    {_db}.{_tb_agregat} a
                INNER JOIN nodrizas.cat_centres b ON
                    up = scs_codi
                GROUP BY
                    indicador,
                    CONCAT(ics_codi, 'M', uba),
                    analisi,
                    institucionalitzat
              UNION
                SELECT
                    indicador,
                    'Aperiodo',
                    CONCAT(ics_codi, 'I', ubainf) AS uba_completa_inf,
                    analisi,
                    'NOCAT',
                    institucionalitzat,
                    'DIM6SET',
                    'N',
                    sum(n)
                FROM
                    {_db}.{_tb_agregat} a
                INNER JOIN nodrizas.cat_centres b ON
                    up = scs_codi
                GROUP BY
                    indicador,
                    CONCAT(ics_codi, 'I', ubainf),
                    analisi,
                    institucionalitzat
              """.format(_db=db, _tb_agregat=tb_agregat)
        u.exportKhalix(sql, file)
        
        # Arxiu per UPs PRS
        
        file = "VALFORM_JAIL"

        sql = """
                SELECT
                    indicador,
                    'Aperiodo',
                    ics_codi,
                    analisi,
                    'NOCAT',
                    institucionalitzat,
                    sexe,
                    'N',
                    sum(n)
                FROM
                    {_db}.{_tb_agregat} a
                INNER JOIN nodrizas.jail_centres b ON
                    up = scs_codi
                GROUP BY
                    indicador,
                    ics_codi,
                    analisi,
                    institucionalitzat,
                    sexe
              """.format(_db=db, _tb_agregat=tb_agregat)
        u.exportKhalix(sql, file)        
       
        # Arxiu per UBAs PRS
        
        file = "VALFORM_UBA_JAIL"

        sql = """
                SELECT
                    indicador,
                    'Aperiodo',
                    CONCAT(ics_codi, 'M', uba) AS uba_completa_med,
                    analisi,
                    'NOCAT',
                    institucionalitzat,
                    'DIM6SET',
                    'N',
                    sum(n)
                FROM
                    {_db}.{_tb_agregat} a
                INNER JOIN nodrizas.jail_centres b ON
                    up = scs_codi
                GROUP BY
                    indicador,
                    CONCAT(ics_codi, 'M', uba),
                    analisi,
                    institucionalitzat
              UNION
                SELECT
                    indicador,
                    'Aperiodo',
                    CONCAT(ics_codi, 'I', ubainf) AS uba_completa_inf,
                    analisi,
                    'NOCAT',
                    institucionalitzat,
                    'DIM6SET',
                    'N',
                    sum(n)
                FROM
                    {_db}.{_tb_agregat} a
                INNER JOIN nodrizas.jail_centres b ON
                    up = scs_codi
                GROUP BY
                    indicador,
                    CONCAT(ics_codi, 'I', ubainf),
                    analisi,
                    institucionalitzat
              """.format(_db=db, _tb_agregat=tb_agregat)
        u.exportKhalix(sql, file)

def sub_get_xml(table):

    if "xml_detall" in table:

        # Valoracions del cuidador dins d'un any
        sql = """
                SELECT
                    id_cip
                FROM
                    {_table}
                WHERE
                    xml_origen = 'GABINETS'
                    AND xml_tipus_orig = 'XML0000065'
                    AND camp_codi IN ('REL_RESULT', 'ACT_RESULT', 'HAB_RESULT', 'CON_RESULT')
                    AND camp_valor != 'Sense Valorar'
                    AND xml_data_alta >= '{_dext_menys1any}'
              """.format(_table=table, _dext_menys1any=dext_menys1any)
        sub_valoracions_cuidador_1any = {id_cip for id_cip, in u.getAll(sql, "import") if id_cip in poblacio_atesa}

        # Valoracions cl�niques dins d'un any
        sql = """
                SELECT
                    id_cip
                FROM
                    {_table}
                WHERE
                    xml_origen = 'GABINETS'
                    AND xml_tipus_orig IN ('VAL_NEC_2', 'VAL_NEC_9', 'VAL_NEC_14', 'VAL_NEC_AD')
                    AND camp_codi IN ('Activi_Result', 'Adaptacio_Result', 'Apren_Result', 'Comunicacio_Result', 'Conducta_Result', 'Elim_Result', 'Higiene_Result', 'Nutri_Result', 'Oxig_Result', 'Repos_Result', 'Relacions_Result', 'Sexualitat_Result', 'Dispositius_Result')
                    AND camp_valor != 'Sense Valorar'
                    AND xml_data_alta >= '{_dext_menys1any}'
              """.format(_table=table, _dext_menys1any=dext_menys1any)
        sub_valoracions_cliniques_1any = {id_cip for id_cip, in u.getAll(sql, "import") if id_cip in poblacio_atesa}

        return "xml_detall", (sub_valoracions_cuidador_1any, sub_valoracions_cliniques_1any)

    else:

        # Formularis de nafres dins dels �ltims 30 dies
        sql = """
                SELECT
                    id_cip
                FROM
                    {_table}
                WHERE
                    xml_origen = 'GABINETS'
                    AND xml_tipus_orig = 'XML0000075'
                    AND xml_data_alta >= '{_dext_menys30dies}'
              """.format(_table=table, _dext_menys30dies=dext_menys30dies)
        sub_formularis_nafres_30dies = {id_cip for id_cip, in u.getAll(sql, "import") if id_cip in poblacio_atesa}

        return "xml", (sub_formularis_nafres_30dies,)

if u.IS_MENSUAL:
    ValoracionsFormularis()