# coding: latin1

"""
EQPF a partir de prescripci�.
"""

import collections as c
import itertools as i
import simplejson as j

import sisapUtils as u


DEBUG = False

TABLE_KLX = "exp_khalix_eqpf_uba"
DATABASE = "altres"
FILE_UBA = "EQPF_PRESCRIPCIO_uba"

cataleg = "exp_ecap_eqpfp_cataleg"
catalegPare = "exp_ecap_eqpfp_catalegPare"
eqpfp_uba = "exp_ecap_eqpfp_uba"
eqpfp_pacients = "exp_ecap_eqpfp_pacient"
eqpfp_pacients_parcial = "exp_ecap_eqpfp_pacient_parcial"
db="altres"

class EQPF(object):
    """."""

    def __init__(self):
        """."""
        self.get_indicadors()
        self.get_centres()
        self.get_poblacio()
        self.create_tables()
        self.execute_pool()
        self.export_khalix()

    def get_indicadors(self):
        """."""
        file = "dades_noesb/eqpf_p_uba.json"
        try:
            stream = open(file)
        except IOError:
            stream = open("../" + file)
        self.indicadors = j.load(stream)

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi, medea from cat_centres"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_poblacio(self):
        """."""
        self.poblacio = c.defaultdict(lambda : c.defaultdict(dict))
        sql = "select codi_sector, id_cip_sec, \
               up, uba, ubainf, edat, sexe, ates, institucionalitzat, nivell_cobertura\
               from assignada_tot"
        for sec, id, up, uba, ubainf, edat, sex, at, ins, cob in u.getAll(sql, "nodrizas"):  # noqa
            age_k, sex_k = u.ageConverter(edat), u.sexConverter(sex)
            br, medea = self.centres[up]
            ents = (br, br + "M" + uba)
            pobs = ["{}INSASS".format("NO" if not ins else "")]
            if at:
                pobs.append("{}INSAT".format("NO" if not ins else ""))
            combs = i.product(ents, pobs, [age_k], [sex_k])
            self.poblacio[sec][id]['M'] = (tuple(combs), medea, cob == "PN", up)
            ents = (br, br + "I" + ubainf)
            combs = i.product(ents, pobs, [age_k], [sex_k])
            self.poblacio[sec][id]['I'] = (tuple(combs), medea, cob == "PN", up)

    def create_tables(self):
        """."""
        cols = ["k{} varchar(15)".format(i) for i in range(6)] + ["v decimal(12, 4)"]  # noqa
        cols_str = "({})".format(", ".join(cols))
        u.createTable(TABLE_KLX, cols_str, DATABASE, rm=True)

        # Hem de mostrar pacients que tenen numerador = 0, per taula PACIENTS
        cols = '(id_cip_sec double, up varchar(5), grup_codi varchar(10), sector varchar(4), exclos int, comentari varchar(3000))'
        u.createTable(eqpfp_pacients_parcial, cols, db, rm=True)


    def execute_pool(self):
        """."""
        taules = [k for (k, v)
                  in sorted(u.getSubTables("tractaments").items(),
                            key=lambda x: x[1], reverse=True)
                  if v > 0]
        for e in taules: print(e)
        jobs = [(taula, self.poblacio[taula[-4:]], self.indicadors)
                for taula in taules]
        if DEBUG:
            Sector(jobs[-1])
        else:
            u.multiprocess(Sector, jobs, 8)

    def export_khalix(self):
        """."""
        sql = """select k0, 'Aperiodo', k1, k2, 'NOCAT', 
                    'NOINSAT', 'DIM6SET', 'N', SUM(v)
                    from {}.{}
                    where length(k1) > 5
                    and k4 = 'NOINSAT'
                    group by K0, 'Aperiodo', k1, k2, 'NOCAT', 
                    'NOINSAT', 'DIM6SET', 'N'""".format(DATABASE, TABLE_KLX)
        u.exportKhalix(sql, FILE_UBA)

class Sector(object):
    """."""

    def __init__(self, params):
        """."""
        self.taula, self.poblacio, self.indicadors = params
        sql = """select atc_codi, atc_desc from import.cat_cpftb010_def"""
        self.cat_descrip = {}
        for atc, atc_desc in u.getAll(sql, 'import'):
            self.cat_descrip[atc] = atc_desc
        self.dades = c.Counter()
        self.get_excl_ram()
        self.get_den_pob()
        self.get_dades()
        self.get_khalix()
        self.get_pacients_llistat()
        self.get_ajustades()
        self.upload_khalix()

    def get_excl_ram(self):
        self.excl_ram = c.defaultdict(set)
        for indicador, params in self.indicadors.items():
            exclusions = set()
            sql = """select id_cip_sec from nodrizas.eqa_ram """
            if "excl_ram" in params:
                for excl in params["excl_ram"]:
                    exclusions.add(excl)
                if len(exclusions) == 1:
                    sql = sql +  """ where agr = {}"""
                    for id, in u.getAll(sql.format(next(iter(exclusions))), 'nodrizas'):
                        self.excl_ram[indicador].add(id)
                else:
                    sql = sql +  """ where agr in {}"""
                    for id, in u.getAll(sql.format(tuple(exclusions)), 'nodrizas'):
                        self.excl_ram[indicador].add(id)

    def get_den_pob(self):
        """."""
        for id in self.poblacio:
            for indicador, params in self.indicadors.items():
                if params["den"] == "pob":
                    self.dades[(id, indicador)] = {"den": 1, "num": 0}

    def get_dades(self):
        """."""
        sql = "select id_cip_sec, codi_sector, ppfmc_pf_codi, ppfmc_atccodi, \
                     ppfmc_pmc_data_ini > adddate(data_ext, interval -1 year),\
                     ppfmc_data_fi > data_ext, ppfmc_pmc_amb_cod_up, \
                     timestampdiff(MONTH, ppfmc_pmc_data_ini, ppfmc_data_fi) \
               from {}, nodrizas.dextraccio \
               where ppfmc_pmc_data_ini <= data_ext and \
                 (ppfmc_pmc_data_ini > adddate(data_ext, interval -1 year) or \
                  ppfmc_data_fi > data_ext)".format(self.taula)
        self.no_recomanats = set()
        self.no_numerador_llistat = c.defaultdict(set)
        self.excl_num = set()
        hipolipemiants = c.defaultdict(set)
        for id, sector, pf, atc, incid, obert, up, nmesos in u.getAll(sql, "import"):
            for indicador, params in self.indicadors.items():
                calcular = params["calcular"]
                aguda = "aguda" in params
                eap = "eap" in params
                mesos = "mesos" in params
                condicions = [calcular]
                condicions.append((obert and not aguda) or (incid and aguda))
                condicions.append(not eap or (id in self.poblacio and up == self.poblacio[id][3]))  # noqa
                condicions.append(not mesos or params["mesos"][0] <= nmesos <= params["mesos"][1])  # noqa
                if all(condicions):
                    key = (id, indicador)
                    hipolipemiants[id].add(atc[:5])
                    ind_no_rec = "no_recomanats" in params
                    es_den = (params["den"] in ("pob", "tot") or
                              any([atc[:n] in params["den"]
                                   for n in range(3, 8)]))
                    if es_den and key not in self.dades:
                        self.dades[key] = {"den": 1, "num": 1 * ind_no_rec}
                    if key in self.dades:
                        if atc in params["excl"]:
                            self.dades[key]["den"] = 0
                        if indicador in self.excl_ram:
                            if id in self.excl_ram[indicador]:
                                self.dades[key]["den"] = 0
                        cod = {k: params["num"].get(k, [])
                               for k in ("ATC", "PF")}
                        num = {"ATC": any([atc[:n] in cod["ATC"]
                                           for n in range(3, 8)]),
                               "PF": str(pf) in cod["PF"]}
                        es_num = any(num.values())
                        if es_num and not ind_no_rec:
                            self.dades[key]["num"] = 1
                            if key[1] in ('IF249P', 'IF301P', 'IF269P', 'IF299P'):
                                self.no_numerador_llistat[(id, up, indicador, sector, 0)].add(atc)
                        elif es_den and not es_num and ind_no_rec:
                            self.no_recomanats.add(key)
                        if atc in params.get("excl_num", []):
                            self.excl_num.add(key)
                            if key[1] not in ('IF249P', 'IF301P', 'IF269P', 'IF299P'):
                                self.no_numerador_llistat[(id, up, indicador, sector, 1)].add(None)
                        elif es_den and not es_num:
                            if key[1] not in ('IF249P', 'IF301P', 'IF269P', 'IF299P'):
                                self.no_numerador_llistat[(id, up, indicador, sector, 0)].add(atc)

        # exclusio IF280P -> IMP021: Cal excloure del denominador els pacients que tenen prescripci� activa d�un f�rmac
        # del grup ATC C10AC% SENSE cap altra hipolipemiant (C10AA% i/o C10AB%)
        keys = {(id, indicador): (up, sector, num) for (id, up, indicador, sector, num) in self.no_numerador_llistat}
        for id in hipolipemiants:
            if 'C10AA' not in hipolipemiants[id] and 'C10AB' not in hipolipemiants[id] and 'C10AC' in hipolipemiants[id]:
                self.dades[(id, 'IF280P')]["den"] = 0
                try:
                    self.no_recomanats.remove((id, 'IF280P'))
                    if (id, 'IF280P') in keys:
                        (up, sector, num) = keys[(id, 'IF280P')]
                        self.no_numerador_llistat.pop((id, up, 'IF280P', sector, num))
                except:
                    pass

    def get_khalix(self):
        """."""
        self.khalix = c.Counter()
        self.denominadors = c.Counter()
        self.factors = c.defaultdict(c.Counter)
        for (id, ind), n in self.dades.items():
            if n["den"] == 1 and id in self.poblacio:
                num = n["num"]
                for obj in (self.no_recomanats, self.excl_num):
                    if (id, ind) in obj:
                        num = 0
                dhd = self.indicadors[ind]["den"] == "pob"
                for prof in ('M', 'I'):
                    keys, medea, pn, up = self.poblacio[id][prof]
                    for ent, pob, age, sex in keys:
                        self.khalix[(ind, ent, "NUM", age, pob, sex)] += num
                        if dhd:
                            self.denominadors[(ind, ent, pob, age, sex, medea, pn)] += 1  # noqa
                            if len(ent) == 5 and prof == 'M':
                                self.factors[(ind, pob, age, sex, medea, pn)]["DEN"] += 1  # noqa
                                self.factors[(ind, pob, age, sex, medea, pn)]["NUM"] += num  # noqa
                        else:
                            self.khalix[(ind, ent, "DEN", age, pob, sex)] += 1
        self.poblacio = {}
        self.dades = {}
    
    def get_pacients_llistat(self):
        """Creem llistat de pacients, els que numerador = 0"""
        upload = []
        for key, val in self.no_numerador_llistat.items():
            comment = ''
            for e in val:
                if e != None and e in self.cat_descrip:
                    e = self.cat_descrip[e]
                    if comment == '':
                        comment += e
                    else:
                        comment = comment + ', ' + e
            key = list(key)
            key.append(comment)
            key = tuple(key)
            upload.append(key)
        u.listToTable(upload, eqpfp_pacients_parcial, DATABASE)
        self.no_numerador_llistat = c.defaultdict(set)
        print('some done')

    def get_ajustades(self):
        """."""
        total = c.defaultdict(c.Counter)
        especifics = {}
        for key, dades in self.factors.items():
            for analysis, n in dades.items():
                total[key[:2]][analysis] += float(n)
            especifics[key] = dades["NUM"] / float(dades["DEN"])
        pesos = {}
        for key, value in especifics.items():
            tot = (total[key[:2]]["NUM"] / total[key[:2]]["DEN"])
            pesos[key] = (value / tot) if tot else 1
        for (ind, ent, pob, age, sex, medea, pn), n in self.denominadors.items():  # noqa
            pes = pesos[(ind, pob, age, sex, medea, pn)]
            key = (ind, ent, "DEN", age, pob, sex)
            self.khalix[key] += (n * pes)

    def upload_khalix(self):
        """."""
        upload = [k + (v,) for (k, v) in self.khalix.items()]
        u.listToTable(upload, TABLE_KLX, DATABASE)
        self.khalix = {}


def create_cataleg():
    u.createTable(cataleg, 
                '(indicador varchar(8),literal varchar(300),ordre int,pare varchar(8),\
                llistat int,invers int,mmin double,mint double,mmax double,\
                toShow int,wiki varchar(250),tipusvalor varchar(5))',
                db, rm=True)
    upload = [
        ('IF249P', 'Seguiment recomanacions PHF-APC', 1, 'EQPFp', 1, 1, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/concepte/4610/ver/', 'PCT'),
        ('IF263P', 'Pacients amb IBP recomanats', 2, 'EQPFp', 1, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/2248/ver/', 'PCT'),
        ('IF301P', 'Percentatge de pacients amb AINE de durada > 3 mesos (poblacio estandarditzada)', 3, 'EQPFp', 1, 1, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3166/ver/', 'PCT'),
        ('IF269P', 'Percentatge de pacients amb prescripcio de condroprotectors (poblacio estandarditzada)', 4, 'EQPFp', 1, 1, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/2241/ver/', 'PCT'),
        ('IF280P', 'Pacients amb hipolipemiants recomanats', 5, 'EQPFp', 1, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/2250/ver/', 'PCT'),
        ('IF254PA', 'Percentatge de pacients amb insulines biosimilars', 8, 'EQPFp', 1, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4362/ver/', 'PCT'),
        ('IF254PB', 'Percentatge de pacients amb heparines biosimilars', 9, 'EQPFp', 1, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4363/ver/', 'PCT'),
        ('IF299P', 'Durada ansiolitics i hipnotics > 6 mesos', 6, 'EQPFp', 1, 1, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/3168/ver/', 'PCT'),
        ('IF292P', 'Pacients amb farmacs recomanats per la HBP', 7, 'EQPFp', 1, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/2236/ver/', 'PCT'),
    ]
    u.listToTable(upload, cataleg, db)
        
def create_catalegPare():
    u.createTable(catalegPare,  
                '(pare varchar(8), literal varchar(300), ordre int, pantalla varchar(20))',
                db, rm=True)
    upload = [('EQPFp', 'Indicadors de prescripcio activa', 9, 'EQPFIMP')] 
    u.listToTable(upload, catalegPare, db)

def crear_pacient():
    print('to do')
    d_ecap = c.defaultdict(list)
    sql = """select id_cip_sec, grup_codi, exclos, comentari from altres.exp_ecap_eqpfp_pacient_parcial"""
    for id, ind, exclos, comment in u.getAll(sql, "altres"):
        d_ecap[id].append((ind, exclos, comment))
    print(len(d_ecap))

    print('getting info')
    d_pob = c.defaultdict()
    sql = """select a.id_cip_sec, a.up, a.uba, a.upinf,
                a.ubainf, a.codi_sector 
                from nodrizas.assignada_tot a
                where edat > 14"""
    for id, up, uba, upinf, ubainf, sector in u.getAll(sql, "import"):
        d_pob[id] = (up, uba, upinf, ubainf, sector)
    print(len(d_pob))
    
    print('adding hash')
    id_hash = {}
    sql = """select id_cip_sec, hash_d from import.u11"""
    for id, hash in u.getAll(sql, "import"):
        id_hash[id] = hash

    # FER MERGE
    print("merging")
    upload = set()
    for id in d_ecap:
        try:
            hash = id_hash[id]
            inf = d_pob[id]
            for e in d_ecap[id]:
                upload.add((id, inf[0], inf[1], inf[2], inf[3], e[0], e[1], hash, inf[4], e[2]))
        except:
            pass
    upload = list(upload)
    # Hem de mostrar pacients que tenen numerador = 0 taula altres.exp_ecap_eqpfp_pacient_parcial
    cols = '(id_cip_sec double, up varchar(5), uba varchar(7), upinf varchar(5), ubainf varchar(7),\
            grup_codi varchar(10), exclos int, hash_d varchar(40), sector varchar(4), comentari varchar(3000))'
    u.createTable(eqpfp_pacients, cols, db, rm=True)
    u.listToTable(upload, eqpfp_pacients, db)


def crear_uba():
    print('getting ups')
    up_converter = {}
    sql = "select scs_codi, ics_codi from nodrizas.cat_centres"
    for up, ics in u.getAll(sql, "nodrizas"):
        up_converter[ics] = up
    print('getting info den')
    sql = """select SUBSTRING(k1, 1, 5) up, SUBSTRING(k1, 7, 8) uba, 
            substring(k1, 6, 1) tipus, k0, k2, sum(v) from altres.exp_khalix_eqpf_uba
            group by k0, k1, k2"""
    dic_info = c.defaultdict(lambda: [0,0])
    for up, uba, tipus, ind, component, val in u.getAll(sql, "altres"):
        key = (up_converter[up], uba, tipus, ind)
        if component == 'DEN':
            dic_info[key][1] = val
    print('getting info num')
    sql = """select up, uba, ubainf, grup_codi, count(*) 
            from altres.exp_ecap_eqpfp_pacient
            group by up, uba, grup_codi"""
    for up, uba, ubainf, ind, val in u.getAll(sql, "altres"):
        key = (up, uba, 'M', ind)
        dic_info[key][0] = val
        key = (up, ubainf, 'I', ind)
        dic_info[key][0] = val

    print('uploading')
    upload = []
    for k, v in dic_info.items():
        if v[1] == 0:
            if k[3] in ('IF249P', 'IF301P', 'IF269P', 'IF299P'):
                upload.append((k[0], k[1], k[2], k[3], v[0], v[1], 0, v[0]))
            else:
                upload.append((k[0], k[1], k[2], k[3], v[1]-v[0], v[1], 0, v[0]))
        else: 
            if k[3] in ('IF249P', 'IF301P', 'IF269P', 'IF299P'): #inversos
                upload.append((k[0], k[1], k[2], k[3], v[0], v[1], v[0]/v[1], v[0]))
            else:
                upload.append((k[0], k[1], k[2], k[3], v[1]-v[0], v[1], (v[1]-v[0])/v[1], v[0]))
    cols = '(up varchar(5), uba varchar(7), tipus varchar(1), indicador varchar(8),\
            numerador int, denominador int, resultat double, noresolts int)'
    u.createTable(eqpfp_uba, cols, db, rm=True)
    u.listToTable(upload, eqpfp_uba, db)

if __name__ == "__main__":
    EQPF()
    create_cataleg()
    create_catalegPare()
    crear_pacient()
    crear_uba()