# -*- coding: latin1 -*-

"""
.
"""

import collections as c
import sisapUtils as u
import dateutil.relativedelta
import re
from datetime import datetime

tb_up = "exp_khalix_escola_inclusiva"
db = "altres"
file = "ESINC"
# SexiA
subindicadors_ESINC011 = {"epileps": "A", "alergia": "B", "diabeti": "C", "Pcronic": "D", "maneig": "E"}
subindicadors_ESINC012 = {"SiE_Inter_capacitaci": "AA", "SiE_Inter_planif": "AB", "SiE_Inter_comicial": "AC", "SiE_Inter_alergia": "AD", 
                          "SiE_Inter_Glucago": "AE", "SiE_Inter_administra": "AF", "SiE_Inter_adminsulin": "AG", 
                          "SiE_Inter_CuraEspeci": {"7": "AH", "8": "AI", "2": "AJ", "3": "AK", "4": "AL", "5": "AM", "6": "AN"}, 
                          "SiE_Inter_ES_aspirac": "AP", "SiE_Inter_EStraqueos": "AQ", "SiE_Inter_ESautosond": "AR", "SiE_Inter_EScursonda": "AS", 
                          "SiE_Inter_ESbombains": "AT", "SiE_Inter_ESmonitora": "AU", "SiE_Inter_ESinsulina": "AV", "SiE_Inter_EScontrol": "AW", 
                          "SiE_Inter_ES_C_estom": "AX", "SiE_Inter_ESmedicaci": "AY", "SiE_Inter_ESbenestar": "AZ", "SiE_Inter_ESoxigenot": "BA", 
                          "SiE_Inter_ESprevenci": "BB", "SiE_Inter_ESproblema": "BC", "SiE_Inter_ESbombaper": "BD", "SiE_Inter_ESsupvital": "BE", 
                          "SiE_Inter_nutrienter": "BF", "SiE_Inter_ESCcomplex": "BG", "SiE_Inter_Altres": "BH"}

cursos="N_1_EI,N_2_EI,N_3_EI,N_1_EP,N_2_EP,N_3_EP,N_4_EP,N_5_EP,N_6_EP,N_1_ESO,N_2_ESO,N_3_ESO,N_4_ESO,N_1_BATX,N_2_BATX"

class EscolaInclusiva(object):
    """."""

    def __init__(self):
        """."""
        self.get_dextraccio()
        self.get_centres()
        self.get_poblacio()
        self.get_visites()
        self.get_cataleg_nens_escola()
        self.get_piic()
        self.indicadors = c.Counter()
        self.get_indicadors()
        self.export_taula()


    def get_dextraccio(self):
        """ . """

        sql = """
                SELECT
                    data_ext
                FROM
                    dextraccio
             """
        self.dext, = u.getOne(sql, "nodrizas")
        self.dext_menys1any = self.dext - dateutil.relativedelta.relativedelta(years=1)
        self.periode = "A{}".format((self.dext - dateutil.relativedelta.relativedelta(days=15)).strftime("%y%m"))

        print("Data d'extraccio: {}".format(self.dext))


    def get_centres(self):
        """ . """
        
        self.centres = {}

        sql = """
                SELECT
                    up_cod,
                    ics_codi,
                    up_des,
                    aga_cod,
                    aga_des
                FROM
                    cat_sisap_covid_dbc_rup a,
                    nodrizas.cat_centres b
                WHERE
                    a.up_cod = b.scs_codi
              """
        for up, br, up_desc, aga, aga_desc in u.getAll(sql, "import"):  # noqa
            self.centres[up] = {"br": br, "up_desc": up_desc, "aga": aga, "aga_desc": aga_desc}

        print("Nombre de centres: {}".format(len(self.centres)))


    def get_poblacio(self):
        """."""

        self.poblacio = dict()
        self.id_cip_set = set()
        self.hash_to_idcip = dict()

        print("Carregant hash-cips...")

        sql_1 = """
                    SELECT
                        hash_d,
                        id_cip 
                    FROM
                        u11                
                """
        for hash, id_cip in u.getAll(sql_1, "import"):
            self.hash_to_idcip[hash] = id_cip
            self.id_cip_set.add(id_cip)

        print("Carregant pob. assignada...")

        sql_2 = """
                    SELECT
                        id_cip,
                        up,
                        edat,
                        sexe,
                        maca,
                        pcc
                    FROM
                        assignada_tot
                """
        for id_cip, up, edat, sexe, maca, pcc in u.getAll(sql_2, "nodrizas"):
            if id_cip in self.id_cip_set and edat < 18:
                self.poblacio[id_cip] = {"up": up,
                                         "edat": edat,
                                         "sexe": sexe,
                                         "maca": maca,
                                         "pcc": pcc}

        print("Poblaci� assignada carregada.")


    def get_visites(self):
        """."""

        visitats_escola_inclusiva = c.defaultdict(set)
        self.visitats_escola_inclusiva = c.defaultdict(dict)
        self.visites_escola_inclusiva = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(dict)))
        self.professionals_emplenat_formulari_PASCE = c.defaultdict(set)

        print("Carregant visites...")

        sql_1 = """
                    SELECT
                        xml_cip,
                        xml_data_alta
                    FROM
                        sisap_xml
                    WHERE
                        xml_origen = 'GABINETS'
                        AND xml_tipus_orig = 'XML0000023'
                        AND camp_codi = 'SiE_TipPrograma' AND camp_valor = 1"""
        for hash, data_visita in u.getAll(sql_1, ("sidics", "x0002")):
            if hash in self.hash_to_idcip:
                    visitats_escola_inclusiva[hash].add(data_visita)


        sql_2 = """
                    SELECT
                        a.xml_id,
                        a.xml_cip,
                        a.xml_up,
                        a.xml_data_alta,
                        a.camp_codi,
                        a.camp_valor
                    FROM
                        sisap_xml a
                    WHERE
                        xml_origen = 'GABINETS'
                        AND xml_tipus_orig = 'XML0000023'
                """
        for visi_id, hash, up_escola, data_visita, camp_codi, valor in u.getAll(sql_2, ("sidics", "x0002")):
            if hash in visitats_escola_inclusiva and up_escola in self.centres:
                if data_visita in visitats_escola_inclusiva[hash]:
                    id_cip = self.hash_to_idcip[hash]
                    if id_cip in self.poblacio:
                        if id_cip not in self.visites_escola_inclusiva:
                            self.visitats_escola_inclusiva[id_cip]["up_escola"] = up_escola
                        if visi_id not in self.visites_escola_inclusiva[id_cip][visi_id]:
                            self.visites_escola_inclusiva[id_cip][visi_id]["data_visita"] = data_visita
                        if camp_codi != "DadesIdentifProfessional":
                            self.visites_escola_inclusiva[id_cip][visi_id][camp_codi] = valor
                        else:
                            if len(re.findall(r"\d+", valor))> 0:
                                self.visites_escola_inclusiva[id_cip][visi_id][camp_codi] = re.findall(r"\d+", valor)[0]
                                self.professionals_emplenat_formulari_PASCE[up_escola].add(re.findall(r"\d+", valor)[0])

        print("Visites carregades.")


    def get_cataleg_nens_escola(self):
        """ . """

        self.cataleg_nens_escola = c.defaultdict(dict)

        sql="""
                SELECT
                    up,
                    codi_escola,
                    {}
                FROM
                    cat_escoles
                WHERE
                    up != ''
                    AND codi_escola != ''
            """.format(cursos)
        for fields in u.getAll(sql, "nodrizas"):
            up, codi_escola = fields[:2]
            if up in self.centres:
                total_nens_escola = sum([int(n_nens) if n_nens else 0 for n_nens in fields[2:]])
                if total_nens_escola:
                    self.cataleg_nens_escola[up][codi_escola] = total_nens_escola
        for up in self.cataleg_nens_escola:
            total_nens_up = sum([self.cataleg_nens_escola[up][codi_escola] for codi_escola in self.cataleg_nens_escola[up]])
            self.cataleg_nens_escola[up]["Total"] = total_nens_up
        
        print("Carregat cataleg nens escolaritzats.")


    def get_piic(self):
        """."""

        sql_2 = """
                    SELECT
                        MI_CIP
                    FROM
                        PRSTB218 
                """
        for hash, in u.getAll(sql_2, "redics"):
            if hash in self.hash_to_idcip:
                id_cip = self.hash_to_idcip[hash]
                if id_cip in self.visites_escola_inclusiva:
                    self.poblacio[id_cip]["piic"] = 1

        print("Carregats els Plans d'Intervenci� Individualitzats i Compartits.")


    def get_indicadors(self):                    
        """."""

        print("Calculant indicadors...")

        # Nombre total de nens i nenes a escoles.
        for up_escola in self.cataleg_nens_escola:
            br_escola = self.centres[up_escola]["br"]
            for n in ("001", "001A", "001B", "002", "003", "004", "013"):    # Indicadors ESINC001 a ESINC005 i ESINC013
                self.indicadors[("ESINC{}".format(n), br_escola, "DEN")] = self.cataleg_nens_escola[up_escola]["Total"]
        
        for id_cip in self.visitats_escola_inclusiva:
            
            up_escola = self.visitats_escola_inclusiva[id_cip]["up_escola"]
            br_escola = self.centres[up_escola]["br"]
            sexe = self.poblacio[id_cip]["sexe"]
            edat = self.poblacio[id_cip]["edat"]

            # Nombre de nens i nenes identificats en alguna visita com escola inclusiva.
            self.indicadors[("ESINC001", br_escola, "NUM")] += 1
            self.indicadors[("ESINC006", br_escola, "DEN")] += 1
            # Nombre de nenes identificats en alguna visita com escola inclusiva.
            if sexe == "D":
                self.indicadors[("ESINC001A", br_escola, "NUM")] += 1
            # Nombre de nens identificats en alguna visita com escola inclusiva.
            elif sexe == "H":
                self.indicadors[("ESINC001B", br_escola, "NUM")] += 1
            # Nombre de nens i nenes amb etiqueta d�escola inclusiva i classificats com a PCC.
            if self.poblacio[id_cip]["pcc"]:
                self.indicadors[("ESINC003", br_escola, "NUM")] += 1
                self.indicadors[("ESINC005", br_escola, "DEN")] += 1
            # Nombre de nens i nenes amb etiqueta d�escola inclusiva i classificats com a MACA.
            if self.poblacio[id_cip]["maca"]:
                self.indicadors[("ESINC004", br_escola, "NUM")] += 1
                self.indicadors[("ESINC005", br_escola, "DEN")] += 1
            # Nombre de nens i nenes amb etiqueta d�escola inclusiva i amb PIIC realitzat.
            if "piic" in self.poblacio[id_cip]:
                self.indicadors[("ESINC005", br_escola, "NUM")] += 1
            # Suma de les edats dels nens i nenes amb etiqueta d�escola inclusiva.
            self.indicadors[("ESINC006", br_escola, "NUM")] += edat
            
            for visi_id in self.visites_escola_inclusiva[id_cip]:

                # Nombre total de visites d�escola inclusiva.
                self.indicadors[("ESINC002", br_escola, "NUM")] += 1
                for n in range(7,11):   # Indicadors ESINC007 a ESINC010
                    self.indicadors[("ESINC{}".format(str(n).zfill(3)), br_escola, "DEN")] += 1
                for i in subindicadors_ESINC011.values():   # Subindicadors ESINC011
                    self.indicadors[("ESINC011{}".format(i), br_escola, "DEN")] += 1
                for i in subindicadors_ESINC012.values():   # Subindicadors ESINC012
                    if type(i) != dict:
                        self.indicadors[("ESINC012{}".format(i), br_escola, "DEN")] += 1
                    else:
                        for j in i.values():
                            self.indicadors[("ESINC012{}".format(j), br_escola, "DEN")] += 1
                    
                # Nombre de primeres visites d�escola inclusiva.
                if int(self.visites_escola_inclusiva[id_cip][visi_id]["SiE_TipVisita_A"]) == 0:
                    self.indicadors[("ESINC007", br_escola, "NUM")] += 1
                # Nombre de visites de seguiment d�escola inclusiva.
                elif int(self.visites_escola_inclusiva[id_cip][visi_id]["SiE_TipVisita_A"]) == 1:
                    self.indicadors[("ESINC008", br_escola, "NUM")] += 1
                # Nombre de visites telef�niques d�escola inclusiva.
                if int(self.visites_escola_inclusiva[id_cip][visi_id]["SiE_TipVisita_B"]) == 1:
                    self.indicadors[("ESINC009", br_escola, "NUM")] += 1
                # Nombre de visites virtuals d�escola inclusiva.
                elif int(self.visites_escola_inclusiva[id_cip][visi_id]["SiE_TipVisita_B"]) == 2:
                    self.indicadors[("ESINC010", br_escola, "NUM")] += 1
                
                for camp_codi in self.visites_escola_inclusiva[id_cip][visi_id]:
                    # Nombre de visites d�escola inclusiva, segons el motiu de consulta.
                    for motiu, subindicador in subindicadors_ESINC011.items():
                        if motiu in camp_codi:
                            self.indicadors[("ESINC011{}".format(subindicador), br_escola, "NUM")] += 1
                            break
                    
                    # Nombre de visites d�escola inclusiva, segons la intervenci� especificada
                    for intervencio, subindicador_1 in subindicadors_ESINC012.items():
                        if type(subindicador_1) != dict:
                            if camp_codi == intervencio:
                                self.indicadors[("ESINC012{}".format(subindicador_1), br_escola, "NUM")] += 1
                                break
                        else:                
                            if camp_codi == intervencio:
                                valors = self.visites_escola_inclusiva[id_cip][visi_id][intervencio].split("|")
                                for valor, subindicador_2 in subindicador_1.items():
                                    if valor in valors:
                                        self.indicadors[("ESINC012{}".format(subindicador_2), br_escola, "NUM")] += 1
                                break

        # Nombre de professionals que han emplenat el formulari PASCE.
        for up_escola in self.professionals_emplenat_formulari_PASCE:
            valor = len(self.professionals_emplenat_formulari_PASCE[up_escola])
            br_escola = self.centres[up_escola]["br"]
            self.indicadors[("ESINC013", br_escola, "NUM")] = valor

        print("Indicadors calculats")


    def export_taula(self):
        """."""
        self.resultats = [[indicador, br_escola, analisis, n] for (indicador, br_escola, analisis), n in self.indicadors.items()]
        u.createTable(tb_up, "(indicador varchar(12), br varchar(5), analisis varchar(3), n int)", db, rm=True)
        u.listToTable(self.resultats, tb_up, db)

        sql = """
                SELECT
                    indicador,
                    "Aperiodo",
                    br,
                    analisis,
                    "NOCAT",
                    "NOIMP",
                    "DIM6SET",
                    "N",
                    n
                FROM
                    {}.{}
              """.format(db, tb_up)
        u.exportKhalix(sql, file)


        print("Proc�s Finalitzat!")


if __name__ == "__main__":
    EscolaInclusiva()