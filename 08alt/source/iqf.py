# -*- coding: utf-8 -*-

"""
.
"""

import collections as c

import sisaptools as t
import sisapUtils as u

TABLE = "IQF"


class IQF(object):
    """."""

    def get_seleccio(self):
        """."""
        sql = "select anyo, max(cast(substr(mes, 5, 2) as int)) \
               from dwcatsalut.indicadors_iqf \
               where es_basal = '0' \
               group by anyo"
        ultim = {a: m for (a, m) in t.Database("exadata", "data").get_all(sql)}
        sql = "select c_agrupacio_up_presc, mes, indicador, tipus, valor \
               from dwcatsalut.indicadors_iqf a \
               inner join dwcatsalut.indicadors_up b on a.up = b.c_up_presc \
               where a.es_basal = '0'"
        for up, per, ind, tip, val in t.Database("exadata", "data").get_all(sql):  # noqa
            keys = [(up, "mensual", per, "seleccio", ind, 100)]
            for mes in range(int(per[4:]), ultim[per[:4]] + 1):
                this = per[:4] + str(mes).zfill(2)
                keys.append((up, "anual", this, "seleccio", ind, 100))
            for key in keys:
                self.dades[key][tip] += val

    def get_dhd(self):
        """."""
        sql = "select up, 'anual', mes, 'dhd', indicador, 1, tipus, valor \
               from dwcatsalut.indicadors_iqf_up_acum \
               where es_basal = '0' and es_dhd_st = 1"
        for row in t.Database("exadata", "data").get_all(sql):
            self.dades[row[:-2]][row[-2]] = row[-1]

    def get_ups(self):
        """."""
        sql = "select up_codi, up_desc, ep_codi, ep_desc, gruptipusup_codi, \
                      gruptipusup_desc, tipusup_codi, tipusup_desc \
               from dwcatsalut.dim_cs_up_hist"
        self.ups = {row[0]: row for row
                    in t.Database("exadata", "data").get_all(sql)}

    def upload_data(self):
        """."""
        upload = [self.ups[key[0]] + key[1:] +
                  (dades["NUMERADOR"], dades["DENOMINADOR"],
                   key[-1] * dades["NUMERADOR"] / dades["DENOMINADOR"]
                   if dades["DENOMINADOR"] else 0)
                  for key, dades in self.dades.items()
                  if key[0] in self.ups]
        cols = ("up_codi varchar2(5)", "up_desc varchar2(255)",
                "up_ep_codi varchar2(4)", "up_ep_desc varchar2(255)",
                "up_grup_codi varchar2(2)", "up_grup_desc varchar2(255)",
                "up_tipus_codi varchar2(4)", "up_tipus_desc varchar2(255)",
                "periode_tipus varchar2(8)", "periode_codi varchar2(6)",
                "indicador_tipus varchar2(16)", "indicador_codi varchar(64)",
                "indicador_factor int", "numerador number",
                "denominador number", "resultat number")
        with t.Database("exadata", "data") as exadata:
            exadata.create_table(TABLE, cols, remove=True)
            exadata.list_to_table(upload, TABLE)
            exadata.set_grants("select", TABLE, "DWSISAP_ROL", inheritance=False)  # noqa
            exadata.set_statistics(TABLE)

    def to_khalix(self):
        """Roser."""
        # if u.IS_MENSUAL:
        sql = """select month(data_ext), year(data_ext)
                 from nodrizas.dextraccio"""
        for month, year in u.getAll(sql, 'nodrizas'):
            m = month
            y = year
        # ho generarem pels últims 6 mesos
        m_ini = (m - 6) % 12
        self.periodes = []
        while m_ini != m:
            month = str(m_ini) if len(str(m_ini)) == 2 else '0'+str(m_ini)
            year = str(y) if m_ini < m else str(y-1)
            self.periodes.append(year+month)
            m_ini = (m_ini + 1) % 12
            if not m_ini: m_ini = 12
        sql = """select scs_codi, ics_codi from nodrizas.cat_centres"""
        up_br = {}
        for up, br in u.getAll(sql, 'nodrizas'):
            up_br[up] = br
        sql = """SELECT up_codi, periode_codi,
                indicador_codi, numerador, denominador, PERIODE_TIPUS
                FROM dwsisap.iqf
                WHERE periode_codi in {} and
                      indicador_codi <> 'ENVAS'""".format(tuple(self.periodes))
        upload = []
        for up, periode, indicador, num, den, periode_tipus in u.getAll(sql, 'exadata'):  # noqa
            if up in up_br:
                periode = 'A' + periode[2:]
                upload.append((indicador.replace("RECOMANADA", "REC"), periode,
                               up_br[up], 'NUM', periode_tipus, 'NOIMP',
                               'DIM6SET', 'N', num))
                upload.append((indicador.replace("RECOMANADA", "REC"), periode,
                               up_br[up], 'DEN', periode_tipus, 'NOIMP',
                               'DIM6SET', 'N', den))
        cols = '(indicador varchar(64), periode varchar(10), up varchar(5), \
                 analisi varchar(10), nocat varchar(10), \
                 noimp varchar(5), dim6set varchar(7), n varchar(2), \
                 val double)'
        u.createTable('IQF', cols, 'altres', rm=True)
        u.listToTable(upload, 'IQF', 'altres')
        sql = """select * from altres.IQF"""
        u.exportKhalix(sql, 'IQF', force=True)

    def get_punts(self):
        """."""
        table = "IQF_PUNTS"
        db = "altres"
        file = "IQF_PUNTS"
        sql = "select scs_codi, ics_codi from nodrizas.cat_centres"
        centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}
        sql = "select distinct indicador \
               from dwcatsalut.indicadors_iqf_up_acum \
               where es_basal = '0' and \
                     mes = (select max(mes) \
                            from dwcatsalut.indicadors_iqf_up_acum \
                            where es_basal = '0' and \
                                  tipus = 'PUNTUACIO') and es_dhd_st = 1"
        dhd = set([ind.split("_")[2] for ind, in u.getAll(sql, "exadata")])
        sql = "select up, mes, replace(indicador, 'PUNTUACIO_', ''), \
                      valor \
               from dwcatsalut.indicadors_iqf_up_acum \
               where es_basal = '0' and tipus = 'PUNTUACIO' and \
                     mes in {}".format(tuple(self.periodes))
        upload = [(("DHD_ST_" + ind.replace("RECOMANADA", "REC"))
                    if ind in dhd else ind.replace("RECOMANADA", "REC"),
                    "A" + mes[2:],
                   centres[up], 'AGASSOL', "ANUAL", "NOIMP", "DIM6SET", "N",
                   val)
                  for (up, mes, ind, val) in u.getAll(sql, "exadata")
                  if up in centres]
        cols = ["k{} varchar(64)".format(i) for i in range(8)] + ["v double"]
        u.createTable(table, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(upload, table, db)
        sql = "select * from {}.{}".format(db, table)
        u.exportKhalix(sql, file)


if __name__ == "__main__":
    iqf = IQF()
    iqf.dades = c.defaultdict(c.Counter)
    iqf.get_seleccio()
    iqf.get_dhd()
    iqf.get_ups()
    iqf.upload_data()
    iqf.to_khalix()
    iqf.get_punts()
