import sisapUtils as u
import datetime as d



centres = set()
# volem BCN
sql = """select scs_codi from nodrizas.cat_centres
            where sap_codi in ('51','52','53','54')"""
for up, in u.getAll(sql, 'nodrizas'):
    centres.add(up)

revisats_llistat = {}
sql = """SELECT count(*) AS num_pacients_revisats, b.up , b.UBA
            FROM sisap_covid_hepatitis_c b, HEPATCINC2022 c
            WHERE b.cip=c.cip
            GROUP BY b.up, b.UBA"""
for revisats, up, uba in u.getAll(sql, 'pdp'):
    if up in centres:
        revisats_llistat[(up, uba)] = revisats

upload = []
sql = """SELECT COUNT(CIP) N, UP, up_desc_up_ics, UBA 
            FROM sisap_covid_hepatitis_c, centres cc 
            WHERE cc.up_codi_up_scs = up
            GROUP BY UP, up_desc_up_ics, uba"""
for n, up, up_des, uba in u.getAll(sql, 'pdp'):
    if up in centres:
        if (up, uba) in revisats_llistat:
            upload.append((up, up_des, uba, revisats_llistat[(up, uba)], n))
        else:
            upload.append((up, up_des, uba, 0, n))

SMS_DIA = d.datetime.now().date()
file_eap = u.tempFolder + 'Seguiment_hepatits_setmanal_{}.csv'.format(SMS_DIA)
u.writeCSV(file_eap, [('UP', 'DES_UP', 'UBA', 'REVISTATS', 'TOTALS')] + upload, sep=";")

# email
subject = 'Seguiment hepatitis C sense tractament setmanal'
text = "Bon dia.\n\nAdjuntem arxiu .csv amb les dades de seguiment setmanal de l'Hepatitis C sense tractament.\n"
text += "\n\nSalutacions."

u.sendGeneral('SISAP <sisap@gencat.cat>',
                      ('amasc@gencat.cat', 'rriel@gencat.cat', 'ialarcon.bcn.ics@gencat.cat',
                        'mjpardo.bcn.ics@gencat.cat', 'jlval.bcn.ics@gencat.cat', 'raquelgarcia@gencat.cat', 
                        'emartingr.bcn.ics@gencat.cat', 'xavier.major@gencat.cat'),
                       ('roser.cantenys@catsalut.cat', 'eballo.girona.ics@gencat.cat'),
                      subject,
                      text,
                      file_eap)

hash_converter = {}
sql = """SELECT cip, HASH_COVID FROM dwsisap.PDPTB101_RELACIO_NIA"""
for cip, exadata in u.getAll(sql, 'exadata'):
    hash_converter[cip[:13]] = exadata

upload = []
sql = """SELECT UP, UBA, CIP, LOGIN, REVISAT, ESTAT, ACTUACIO, VISITAVIRTUAL 
            FROM HEPATCINC2022 where cip IS NOT null"""
for UP, UBA, CIP, LOGIN, REVISAT, ESTAT, ACTUACIO, VISITAVIRTUAL in u.getAll(sql, 'pdp'):
    if CIP[:13] in hash_converter:
        HASH = hash_converter[CIP[:13]]
        upload.append((UP, UBA, HASH, LOGIN, REVISAT, ESTAT, ACTUACIO, VISITAVIRTUAL))

cols = """(UP varchar2(5), UBA varchar2(5), HASH varchar2(255), LOGIN varchar2(40), REVISAT date, ESTAT number, ACTUACIO number, VISITAVIRTUAL date)"""
u.createTable('hepat_c_estats_actuacions', cols, 'exadata', rm=True)
u.listToTable(upload, 'hepat_c_estats_actuacions', 'exadata')
u.grantSelect('hepat_c_estats_actuacions', ("DWSISAP_ROL", "DWAQUAS"), 'exadata')