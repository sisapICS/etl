# -*- coding: utf8 -*-

import collections as c
import sisaptools as u
import datetime as d

DATABASE = ("exadata", "data")

sql = """
SELECT servei,sisap_servei_codi, sisap_servei_class, sisap_catprof_class, up, extract(MONTH FROM data) AS mes, extract(YEAR FROM data) AS a�o, sisap_situacio_codi, tipus, etiqueta,
count (*)
FROM dwsisap.sisap_master_visites WHERE extract(year FROM data) IN ('2022','2023') AND up='00108' AND sisap_servei_class IS null
GROUP BY servei, up,extract(YEAR FROM data), extract(MONTH FROM data), sisap_servei_codi, sisap_servei_class, sisap_catprof_class,tipus, etiqueta, sisap_situacio_codi
"""
sql_p = "SELECT partition_name from all_tab_partitions WHERE table_name = 'SISAP_MASTER_VISITES' AND table_owner = 'DWSISAP'"
upload = []
with u.Database(*DATABASE) as conn:
    # for partition, in conn.get_all(sql_p):
        # print(partition)
    for servei,sisap_servei_codi, sisap_servei_class, sisap_catprof_class, up, mes, year, sisap_situacio_codi, tipus, etiqueta,val in conn.get_all(sql):
        upload.append((servei,sisap_servei_codi, sisap_servei_class, sisap_catprof_class, up, mes, year, sisap_situacio_codi, tipus, etiqueta,val))
    cols = ("servei varchar2(5)",
            "sisap_servei_codi varchar2(15)",
            "sisap_servei_class varchar2(15)",
            "sisap_cat_prof_class varchar2(15)",
            "up varchar2(5)",
            "data_mes varchar2(2)",
            "data_any varchar2(4)",
            "sisap_situacio_codi varchar2(4)",
            "tipus varchar2(4)",
            "etiqueta varchar2(4)",
            "valor number")
    conn.create_table('homologacions', cols, remove = True)
    conn.list_to_table(upload, 'homologacions')