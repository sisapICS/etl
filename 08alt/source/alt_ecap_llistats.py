import sisapUtils as u

DELETE_FROM_EXP_ECAP_ALT_PACIENT = False

db = "altres"
pf = "alt"

pac = "%s.exp_ecap_alt_pacient" % db
dext = "nodrizas.dextraccio"  # compte

table = pf + "Llistats"
indicadors = tuple({ind[0] for ind in
                    u.getAll("SELECT distinct grup_codi FROM {}".format(pac),
                             'nodrizas')})
if DELETE_FROM_EXP_ECAP_ALT_PACIENT:
    u.execute("""delete from {table} where indicador in {indicadors}
              """.format(table=table, indicadors=indicadors),
              'pdp')

query = """
        select
            up, uba,
            'M' as tipus,
            grup_codi, hash_d, sector, exclos, comentari
        from {pac} where uba <> ''
        union
        select
            upinf as up,
            ubainf as uba,
            'I' as tipus,
            grup_codi, hash_d, sector, exclos, comentari
        from {pac} where ubainf <> ''
        """.format(pac=pac)
u.exportPDP(query=query, table=table, truncate=True, pkOut=True, pkIn=True)

table = pf + "LlistatsData"
query = "select data_ext from %s limit 1" % dext
u.exportPDP(query=query, table=table, truncate=True)
