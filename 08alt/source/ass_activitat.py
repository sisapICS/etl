# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import os

import sisapUtils as u


nod = "nodrizas"


class Activitat(object):
    """."""

    def __init__(self):
        """."""
        self.year, = u.getOne("select year(data_ext) from dextraccio", nod)
        self.get_visites()
        self.get_embaras()
        self.get_centres()
        self.get_atesa()
        self.export_results()

    def get_visites(self):
        """."""
        self.visites = {}
        sql = "select id_cip_sec, visi_data_visita, visi_up from ass_visites \
               where visi_data_visita >= {}0101".format(self.year)
        for id, data, up in u.getAll(sql, nod):
            month = data.month
            key = (id, month)
            if key not in self.visites or data > self.visites[key][0]:
                self.visites[key] = (data, up)

    def get_embaras(self):
        """."""
        self.embaras = c.defaultdict(set)
        sql = "select id_cip_sec, inici, fi from ass_embaras"
        for id, inici, fi in u.getAll(sql, nod):
            self.embaras[id].add((inici, fi))

    def get_centres(self):
        """."""
        sql = "select up, br_assir, assir from ass_centres"
        self.centres = {up: (br, des) for (up, br, des) in u.getAll(sql, nod)}

    def get_atesa(self):
        """."""
        self.atesa = c.Counter()
        for (id, month), (data, up) in self.visites.items():
            if up in self.centres:
                br, des = self.centres[up]
                tipus = "NO_EMBARASSADES"
                if id in self.embaras:
                    for inici, fi in self.embaras[id]:
                        if inici <= data <= fi:
                            if (data - inici).days < 70:
                                tipus = "EMBARASSADES < 10"
                            else:
                                tipus = "EMBARASSADES >= 10"
                self.atesa[(self.year, month, br, des, tipus)] += 1

    def export_results(self):
        """."""
        file = u.tempFolder + "atesa_assir.csv"
        header = [("any", "mes", "br", "assir", "tipus", "n")]
        dades = [k + (n,) for (k, n) in self.atesa.items()]
        u.writeCSV(file, header + sorted(dades), sep=';')
        periode = u.getOne("select date_format(data_ext, '%Y-%m-%d') from dextraccio", nod)
        text = "Us fem arribar el fitxer d'activitat fins el " + str(periode[0]) + "."
        u.sendPolite(["cmartinezbu@gencat.cat",
                      "miriamburballa.bcn.ics@gencat.cat"],
                     "Activitat ASSIR", text, file)
        os.remove(file)


if __name__ == '__main__':
    Activitat()
