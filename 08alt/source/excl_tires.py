import sisapUtils as u

sql = """
   SELECT id_cip_sec, fit_cod
   FROM tires
"""

pacs = {}
for id, codi in u.getAll(sql, 'import'):
   pacs[codi] = id

sql = """
   SELECT a.dtr_cod_pac, c.pd_tipus
   FROM import.tires2 a
   INNER JOIN import.cat_cpftb025 c
   ON a.dtr_pd_cod = c.pd_codi
   WHERE c.pd_tipus in ('L','A','X','T','D')
"""
lax = set()
td = set()
for id, codi in u.getAll(sql, 'import'):
   if id in pacs:
      id_cip_sec = pacs[id]
      if codi in ('L','A','X'):
         lax.add(id_cip_sec)
      else:
         td.add(id_cip_sec)
upload = []
for id in lax:
   if id not in td:
      upload.append((id, 1))

cols = "(id_cip_sec int, val int)"
u.createTable('excl_tires', cols, 'altres', rm=True)
sql = "alter table excl_tires add index (id_cip_sec)"
u.execute(sql, 'altres')
u.listToTable(upload, 'excl_tires', 'altres')