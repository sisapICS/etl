# -*- coding: latin1 -*-


import sisapUtils as u
import collections as c

class MonitoratgeGlucosa():
    def __init__(self):
        self.get_den()
        self.create_indicators()
        self.upload_data()
        self.export_ecap()
    
    def get_den(self):
        # poblaci�
        self.poblacio = {}
        sql = """select
                    id_cip_sec,
                    up,
                    uba,
                    ubainf,
                    edat,
                    sexe
                from
                    nodrizas.assignada_tot at2"""
        for id, up, uba, uba_inf, edat, sexe in u.getAll(sql, 'nodrizas'):
            self.poblacio[id] = [up, uba, uba_inf, edat, sexe]

        # pacients amb DM
        self.tipus_dm2 = {}
        sql = """select id_cip_sec, ps
                from nodrizas.eqa_problemes ep 
                where ps in (18, 24)"""
        for id, ps in u.getAll(sql, 'nodrizas'):
            self.tipus_dm2[id] = ps
        
        self.tipus = c.defaultdict(set)
        sql = """select
                    id_cip_sec,
                    fit_dispensacio_domicili,
                    fit_mcg,
                    fit_cod_glucometro
                from
                    import.tires,
                    nodrizas.dextraccio de
                where
                    fit_data_alta <= de.data_ext
                    and (fit_dat_baixa = ''
                        or fit_dat_baixa >= de.data_ext)
                    and (fit_dispensacio_domicili = 'S'
                        or fit_mcg = 'S'
                        or fit_cod_glucometro != '')"""
        for id, dispensacio_dom, indicacio_mcg, glucometre in u.getAll(sql, 'import'):
            if glucometre:
                glucometre = int(glucometre)
                # pacients amb Dexcom One+
                if glucometre in (9005, 9008, 9010, 9014, 9016):
                    self.tipus['dexcom'].add(id)
                # pacients amb FreeStyle Libre 2 
                if glucometre in (9004, 9006, 9009, 9011, 9015, 9017):
                    self.tipus['freestyle'].add(id)
                # pacients amb Countour Care
                if glucometre in (9000, 9002):
                    self.tipus['countour'].add(id)
            # indicaci� de MCG
            if indicacio_mcg == 'S':
                self.tipus['indicacio_mcg'].add(id)
            # pacients amb dispensaci� domicil�ria prescrits per atenci� prim�ria
            if dispensacio_dom == 'S':
                self.tipus['dispen_domi'].add(id)

    
    def create_indicators(self):
        self.upload = c.Counter()
        # INDICADORS 1 I 2
        for id, tipus_dm in self.tipus_dm2.items():
            if id in self.poblacio:
                up, uba, uba_inf, edat, sexe = self.poblacio[id]
                num_indi1 = 1 if id in self.tipus['indicacio_mcg'] else 0
                num_indi2 = 1 if id in self.tipus['dispen_domi'] else 0
                self.upload[('MCG001', up, uba, 'M', edat, sexe, tipus_dm, 'NUM')] += num_indi1
                self.upload[('MCG002', up, uba, 'M', edat, sexe, tipus_dm, 'NUM')] += num_indi2
                self.upload[('MCG001', up, uba_inf, 'I', edat, sexe, tipus_dm, 'NUM')] += num_indi1
                self.upload[('MCG002', up, uba_inf, 'I', edat, sexe, tipus_dm, 'NUM')] += num_indi2
                self.upload[('MCG001', up, uba, 'M', edat, sexe, tipus_dm, 'DEN')] += 1
                self.upload[('MCG002', up, uba, 'M', edat, sexe, tipus_dm, 'DEN')] += 1
                self.upload[('MCG001', up, uba_inf, 'I', edat, sexe, tipus_dm, 'DEN')] += 1
                self.upload[('MCG002', up, uba_inf, 'I', edat, sexe, tipus_dm, 'DEN')] += 1
        for id in self.tipus['dispen_domi']:
            if id in self.poblacio and id in self.tipus_dm2:
                up, uba, uba_inf, edat, sexe = self.poblacio[id]
                tipus_dm = self.tipus_dm2[id]
                num_indi3a = 1 if id in self.tipus['dexcom'] else 0
                num_indi3b = 1 if id in self.tipus['freestyle'] else 0
                num_indi3c = 1 if id in self.tipus['countour'] else 0
                self.upload[('MCG00301', up, uba, 'M', edat, sexe, tipus_dm, 'NUM')] += num_indi3a
                self.upload[('MCG00302', up, uba, 'M', edat, sexe, tipus_dm, 'NUM')] += num_indi3b
                self.upload[('IMD001', up, uba, 'M', edat, sexe, tipus_dm, 'NUM')] += num_indi3c
                self.upload[('MCG00301', up, uba_inf, 'I', edat, sexe, tipus_dm, 'NUM')] += num_indi3a
                self.upload[('MCG00302', up, uba_inf, 'I', edat, sexe, tipus_dm, 'NUM')] += num_indi3b
                self.upload[('IMD001', up, uba_inf, 'I', edat, sexe, tipus_dm, 'NUM')] += num_indi3c
                self.upload[('MCG00301', up, uba, 'M', edat, sexe, tipus_dm, 'DEN')] += 1
                self.upload[('MCG00302', up, uba, 'M', edat, sexe, tipus_dm, 'DEN')] += 1
                self.upload[('IMD001', up, uba, 'M', edat, sexe, tipus_dm, 'DEN')] += 1
                self.upload[('MCG00301', up, uba_inf, 'I', edat, sexe, tipus_dm, 'DEN')] += 1
                self.upload[('MCG00302', up, uba_inf, 'I', edat, sexe, tipus_dm, 'DEN')] += 1
                self.upload[('IMD001', up, uba_inf, 'I', edat, sexe, tipus_dm, 'DEN')] += 1

    
    def upload_data(self):
        upload_table = [(ind, up, uba_inf, tipus, edat, sexe, tipus_dm, analisi, n) for (ind, up, uba_inf, tipus, edat, sexe, tipus_dm, analisi), n in self.upload.items()]
        cols = """(indi varchar(40), up varchar(5), uba varchar(5), tipus varchar(1),
                    edat int, sexe varchar(40), tipus_dm int, analisi varchar(3), n int)"""
        u.createTable('moni_glucosa', cols, 'altres', rm=True)
        u.listToTable(upload_table, 'moni_glucosa', 'altres')

        sql = """select
                        indi,
                        concat('A', 'periodo'),
                        concat(concat(ics_codi, tipus), uba),
                        analisi,
                        'NOCAT',
                        case
                            when tipus_dm = 18 then 'DM2'
                            else 'DM1'
                        end DM,
                        'DIM6SET',
                        'N',
                        sum(n)
                    from
                        altres.moni_glucosa g
                    inner join nodrizas.cat_centres cc 
                                                    on
                        cc.scs_codi = up
                    group by
                        indi,
                        concat(concat(ics_codi, tipus), uba),
                        analisi, dm"""
        u.exportKhalix(sql, 'MCG_IMD_UBA')

        sql = """select
                        indi,
                        concat('A', 'periodo'),
                        ics_codi,
                        analisi,
                        'NOCAT',
                        case
                            when tipus_dm = 18 then 'DM2'
                            else 'DM1'
                        end DM,
                        case when g.sexe = 'D' then 'DONA' else 'HOME' end SEXES,
                        'N',
                        sum(n)
                    from
                        altres.moni_glucosa g
                    inner join nodrizas.cat_centres cc 
                                                    on
                        cc.scs_codi = up
                    where tipus = 'M'
                    group by
                        indi,
                        ics_codi,
                        analisi, dm, sexes"""
        u.exportKhalix(sql, 'MCG_IMD')
    
    def export_ecap(self):
        sql = """select a.up, a.uba, 'I', a.indi, case when num is null then 0 else num end nume, den, num/den, Null, 0, 0 
                from
                (select up, uba, 'I', indi, sum(n) as den
                    from altres.moni_glucosa g    
                    where tipus = 'I'
                    and analisi = 'DEN'
                    group by up, uba, indi) a
                    left join (
                select up, uba, 'I', indi, sum(n) as num
                    from altres.moni_glucosa g    
                    where tipus = 'I'
                    and analisi = 'NUM'
                    group by up, uba, indi) b
                on a.up = b.up and a.uba = b.uba and a.indi = b.indi"""
        upload = [row for row in u.getAll(sql, 'altres')]
        cols = """(up varchar(5), uba varchar(5), tipus varchar(1), indicador varchar(40),
                    numerador int, denominador int, resultat decimal(36,4), noresolts int, mmin int, mmax int)"""
        u.createTable('exp_ecap_moni_glucosa', cols, 'altres', rm=True)
        u.listToTable(upload, 'exp_ecap_moni_glucosa', 'altres')

        cols = """(indi varchar(25), ind_desc varchar(255), 
                    ordre int,
                    pare varchar(50),
                    llistat int,
                    invers int,
                    mmin int,
                    mint int,
                    mmax int,
                    toShow int,
                    wiki varchar(250),
                    tipus_val varchar(3))"""
        u.createTable('exp_ecap_moni_glucosa_cat', cols, 'altres', rm=True)
        upload = [
            ('MCG001', 'Pacients amb indicaci� de MCG', 1, 'MCG', 0, 0 , 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5879/ver/', 'PCT'),
            ('MCG002', 'Pacients amb indicaci� de MCG prescrits per atenci� prim�ria', 2, 'MCG', 0, 0 , 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5880/ver/', 'PCT'),
            ('MCG00301', 'Pacients amb Dexcom One+', 3, 'MCG', 0, 0 , 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5882/ver/', 'PCT'),
            ('MCG00302', 'Pacients amb FreeStyle Libre 2', 4, 'MCG', 0, 0 , 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5883/ver/', 'PCT'),
            ('IMD001', 'Pacients amb Countour Care', 1, 'IMD', 0, 0 , 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5885/ver/', 'PCT')
        ]
        u.listToTable(upload, 'exp_ecap_moni_glucosa_cat', 'altres')


        cols = """(pare varchar(50), desc_pare varchar(255), ordre int, pantalla varchar(255))"""
        u.createTable('exp_ecap_moni_glucosa_catpare', cols, 'altres', rm=True)
        upload = [('MCG', 'Indicadors de monitoritzaci� cont�nua de glucosa', 10, 'ALTRES'),
                    ('IMD', """Indicadors d'indicaci� de material DM""", 11, 'ALTRES')]
        u.listToTable(upload, 'exp_ecap_moni_glucosa_catpare', 'altres')



if __name__ == "__main__":
    MonitoratgeGlucosa()
