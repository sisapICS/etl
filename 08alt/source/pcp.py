# -*- coding: utf-8 -*-

import sisapUtils as u
import collections as c
import datetime

db = "altres"
taula = "exp_khalix_pcp"
taula_vis = taula + "_vis"

class PCP():
    """Programa de col�laboraci� entre salut mental i addiccions i l'atenci� prim�ria i comunit�ria (PCP)"""

    def __init__(self):

        self.get_dextraccio();                      print("self.get_dextraccio()")
        self.get_centres();                         print("self.get_centres()")
        self.get_poblacio();                        print("self.get_poblacio()")
        self.get_visites();                         print("self.get_visites()")
        self.get_problemes();                       print("self.get_problemes()")
        self.get_variables();                       print("self.get_variables()")
        self.get_baixes();                          print("self.get_baixes()")
        self.get_indicadors();                      print("self.get_indicadors()")
        self.to_khalix();                           print("self.to_khalix()")
    
    def get_dextraccio(self):
        """Obt� les dades d'extracci� de les dades i la d'un any enrere i les emmagatzema en variables d'inst�ncia"""

        sql = """
                SELECT
                    data_ext,
                    date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY)
                FROM
                    dextraccio
              """
        self.dext, self.dext_menys1any = u.getOne(sql, "nodrizas")

    def get_centres(self):
        """Obt� informaci� sobre els Centres d'Atenci� Prim�ria i crea un diccionari amb els codis UP i BR"""

        self.centres = dict()

        sql = """
                SELECT
                    scs_codi,
                    ics_codi
                FROM
                    cat_centres
              """
        self.centres = {up: br for up, br in u.getAll(sql, "nodrizas")}

    def get_poblacio(self):
        """Aquesta funci� obt� dades sobre la poblaci� assignada i les emmagatzema en un diccionari."""

        self.poblacio = dict()
        sql = """
                SELECT
                    id_cip_sec,
                    up,
                    edat,
                    sexe,
                    institucionalitzat,
                    ates
                FROM
                    assignada_tot
              """
        for id_cip_sec, up, edat, sexe, institucionalitzat, ates in u.getAll(sql, "nodrizas"):
            if up in self.centres:
                edat_grup = u.ageConverter(edat)
                sexe = u.sexConverter(sexe)
                poblatip_ass = "INSASS" if institucionalitzat else "NOINSASS"
                if ates:
                    poblatip_at = "INSAT" if institucionalitzat else "NOINSAT"
                else:
                    poblatip_at = False
                self.poblacio[id_cip_sec] = {"up": up, "edat": edat, "edat_grup": edat_grup, "sexe": sexe, "poblatip_ass": poblatip_ass, "poblatip_at": poblatip_at}

    def get_visites(self):
        """."""

        sql = """
                SELECT
                    id_cip_sec,
                    visi_up,
                    s_espe_codi_especialitat,
                    visi_modul_codi_modul
                FROM
                    visites2,
                    nodrizas.dextraccio
                WHERE
                    visi_situacio_visita = 'R'
                    AND visi_data_visita BETWEEN adddate(adddate(data_ext, INTERVAL -1 MONTH), INTERVAL + 1 DAY) AND data_ext
                    AND s_espe_codi_especialitat IN ('30999', '10147', '02004')
                    AND visi_servei_codi_servei = 'SMP'
                    AND visi_modul_codi_modul IN ('PCPA', 'PCPI', 'PCPG')
              """
        self.visites = [(id_cip_sec, visi_up, codi_especialitat, codi_modul) for id_cip_sec, visi_up, codi_especialitat, codi_modul in u.getAll(sql, "import")]

    def get_problemes(self):
        """Obt� informaci� sobre els problemes de salut de la poblaci� necessaris pel c�lcul dels indicadors i crea un conjunt amb aquesta informaci�"""

        self.problemes = c.defaultdict(set)

        agrupadors_problemes = (85, 975, 976, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992)

        sql = """
                SELECT
                    id_cip_sec,
                    ps
                FROM
                    eqa_problemes
                WHERE
                    ps IN {}
              """.format(agrupadors_problemes)
        for id_cip_sec, agr in u.getAll(sql, "nodrizas"):
            self.problemes[agr].add(id_cip_sec)

    def get_variables(self):
        """Obt� informaci� sobre les variables de salut necess�ries pel c�lcul dels indicadors i crea un conjunt amb aquesta informaci�"""

        self.variables = c.defaultdict(set)

        agrupadors_variables = 910

        sql = """
                SELECT
                    id_cip_sec,
                    agrupador, 
                    valor
                FROM
                    eqa_variables
                WHERE
                    agrupador IN ({})
                    AND usar = 1
              """.format(agrupadors_variables)
        for id_cip_sec, agr, valor in u.getAll(sql, "nodrizas"):
            if agr == 910 and valor in (2, 6):
                self.variables["Persona Fumadora"].add(id_cip_sec)  

    def get_baixes(self):
        """ Obtenci� dels pacients que han tingut baixes m�diques durant l'�ltim any."""

        self.baixes = c.defaultdict(lambda: c.defaultdict(set))

        sql = """
                SELECT
                    id_cip_sec,
                	ilt_data_baixa,
	                CASE WHEN ilt_data_alta	= '0000-00-00' THEN data_ext ELSE ilt_data_alta END AS ilt_data_alta
                FROM
                    import.baixes,
                    nodrizas.dextraccio
                WHERE
                    ilt_prob_salut IN ('F90.2', 'F32.1', 'F41.0', 'F41.1', 'F41.9', 'F43.0', 'F43.2', 'F43.8', 'F50.9', 'F90', 'F90.0', 'F90.1', 'F90.8', 'F90.9', 'F91', 'F91.0', 'F91.1', 'F91.2', 'F91.3', 'F91.8', 'F91.9', 'F93', 'F93.0', 'F93.1', 'F93.2', 'F93.3', 'F93.8', 'F93.9', 'F94', 'F94.0', 'F94.1', 'F94.2', 'F94,8', 'F94.9', 'C01-F90.2', 'C01-F32.1', 'C01-F41.0', 'C01-F41.1', 'C01-F41.9', 'C01-F43.0', 'C01-F43.2', 'C01-F43.8', 'C01-F50.9', 'C01-F90', 'C01-F90.0', 'C01-F90.1', 'C01-F90.8', 'C01-F90.9', 'C01-F91', 'C01-F91.0', 'C01-F91.1', 'C01-F91.2', 'C01-F91.3', 'C01-F91.8', 'C01-F91.9', 'C01-F93', 'C01-F93.0', 'C01-F93.1', 'C01-F93.2', 'C01-F93.3', 'C01-F93.8', 'C01-F93.9', 'C01-F94', 'C01-F94.0', 'C01-F94.1', 'C01-F94.2', 'C01-F94,8', 'C01-F94.9')
                    AND (ilt_data_alta = '0000-00-00' OR ilt_data_baixa BETWEEN {} AND {})
              """.format(self.dext_menys1any, self.dext)
        for id_cip_sec, data_inici_baixa, data_fi_baixa in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                up = self.poblacio[id_cip_sec]["up"]
                if u.daysBetween(data_inici_baixa, data_fi_baixa) > 545:
                    data_fi_baixa = data_inici_baixa + datetime.timedelta(days=545)
                self.baixes[up][id_cip_sec].update(set([data for data in (data_inici_baixa + datetime.timedelta(days=n) for n in range((data_fi_baixa - data_inici_baixa).days + 1))]))

    def get_indicadors(self):
        """Calcula els indicadors PCP"""

        self.indicadors, self.indicadors_vis = c.Counter(), c.Counter()

        for id_cip_sec in self.poblacio:
            up = self.poblacio[id_cip_sec]["up"]
            br = self.centres[up]
            poblatip_ass = self.poblacio[id_cip_sec]["poblatip_ass"]
            poblatip_at = self.poblacio[id_cip_sec]["poblatip_at"]
            edat, sexe = self.poblacio[id_cip_sec]["edat"], self.poblacio[id_cip_sec]["sexe"]
            edat, sexe = u.ageConverter(edat), u.sexConverter(sexe)
            for ind in ("001", "002", "003", "004", "005", "006"):
                if ind == "005":
                    for sub_ind in ("A", "B", "C", "D", "E", "F", "G", "H", "I"):
                        self.indicadors[("PCP{}{}".format(ind, sub_ind), br, poblatip_ass, edat, sexe, "DEN")] += 1
                        if poblatip_at:
                            self.indicadors[("PCP{}{}".format(ind, sub_ind), br, poblatip_at, edat, sexe, "DEN")] += 1
                self.indicadors[("PCP{}".format(ind), br, poblatip_ass, edat, sexe, "DEN")] += 1
                if poblatip_at:
                    self.indicadors[("PCP{}".format(ind), br, poblatip_at, edat, sexe, "DEN")] += 1
    
            if id_cip_sec in self.variables["Persona Fumadora"]:
                self.indicadors[("PCP003", br, poblatip_ass, edat, sexe, "NUM")] += 1
                if poblatip_at:
                    self.indicadors[("PCP003", br, poblatip_at, edat, sexe, "NUM")] += 1

            agr_ind_problemes = {
                                975: "PCP001", 976: "PCP002", 85: "PCP004",
                                982: "PCP005",
                                983: "PCP005A", 984: "PCP005B", 985: "PCP005C",
                                986: "PCP005D", 987: "PCP005E", 988: "PCP005F",
                                989: "PCP005G", 990: "PCP005H", 991: "PCP005I",
                                981: "PCP006"
                                }
            for agrupador, ind in agr_ind_problemes.items():
                if id_cip_sec in self.problemes[agrupador]:
                    self.indicadors[(ind, br, poblatip_ass, edat, sexe, "NUM")] += 1
                    if poblatip_at:
                        self.indicadors[(ind, br, poblatip_at, edat, sexe, "NUM")] += 1

            # PCP007: Indicador descartat
            # if id_cip_sec in self.baixes[up]:
            #     self.indicadors[("PCP007", br, poblatip_ass, edat, sexe, "DEN")] += 1
            #     self.indicadors[("PCP007", br, poblatip_ass, edat, sexe, "NUM")] += len(self.baixes[up][id_cip_sec])
            #     if poblatip_at:
            #         self.indicadors[("PCP007", br, poblatip_at, edat, sexe, "DEN")] += 1
            #         self.indicadors[("PCP007", br, poblatip_at, edat, sexe, "NUM")] += len(self.baixes[up][id_cip_sec])

        for id_cip_sec, visi_up, codi_especialitat, codi_modul in self.visites:
            if visi_up in self.centres and id_cip_sec in self.poblacio:
                br_visita = self.centres[visi_up]
                edat, sexe = self.poblacio[id_cip_sec]["edat"], self.poblacio[id_cip_sec]["sexe"]
                edat, sexe = u.ageConverter(edat), u.sexConverter(sexe)
                for ind in ("PCPVIS01", "PCPVIS02", "PCPVIS03"):
                    self.indicadors_vis[(ind, br_visita, codi_especialitat, edat, sexe, "DEN")] += 1
                if codi_modul == "PCPA":
                    self.indicadors_vis[("PCPVIS01", br_visita, codi_especialitat, edat, sexe, "NUM")] += 1
                elif codi_modul == "PCPI":
                    self.indicadors_vis[("PCPVIS02", br_visita, codi_especialitat, edat, sexe, "NUM")] += 1
                elif codi_modul == "PCPG":
                    self.indicadors_vis[("PCPVIS03", br_visita, codi_especialitat, edat, sexe, "NUM")] += 1
                
           
    def to_khalix(self):
        """."""

        self.indicadors_export = [(indicador, br, poblatip, edat, sexe, analisi, n) for (indicador, br, poblatip, edat, sexe, analisi), n in self.indicadors.items()] 

        cols = "(indicador varchar(10), br varchar(5), poblatip varchar(10), edat varchar(10), sexe varchar(10), analisi varchar(3), n int)"
        u.createTable(taula, cols, db, rm=True)        

        u.listToTable(self.indicadors_export, taula, db) 

        sql = """
                SELECT
                    indicador,
                    concat('A', 'periodo'),
                    br,
                    analisi,
                    edat,
                    poblatip,
                    sexe,
                    'N',
                    n
                FROM
                    {}.{}
              """.format(db, taula)
        file = "PCP"
        u.exportKhalix(sql, file)

        # PCPVIS

        self.indicadors_vis_export = [(indicador, br, poblatip, edat, sexe, analisi, n) for (indicador, br, poblatip, edat, sexe, analisi), n in self.indicadors_vis.items()] 

        cols_vis = "(indicador varchar(10), br varchar(5), categ_professional varchar(10), edat varchar(10), sexe varchar(10), analisi varchar(3), n int)"
        u.createTable(taula_vis, cols_vis, db, rm=True)        

        u.listToTable(self.indicadors_vis_export, taula_vis, db) 

        sql = """
                SELECT
                    indicador,
                    concat('A', 'periodo'),
                    br,
                    analisi,
                    edat,
                    categ_professional,
                    sexe,
                    'N',
                    n
                FROM
                    {}.{}
              """.format(db, taula_vis)
        file = "PCPVIS"
        u.exportKhalix(sql, file)
        

if u.IS_MENSUAL:
    PCP()