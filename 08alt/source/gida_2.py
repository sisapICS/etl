# -*- coding: latin1 -*-

"""
.
"""

import collections as c
import dateutil.relativedelta as r

import sisapUtils as u


MASTER_TB = "mst_gida_2"
MASTER_TB = "mst_gida_v20"
KHALIX_TB = "exp_khalix_gida_2"
KHALIX_UP = "GIDA_2"
KHALIX_UBA = "GIDA_UBA_2"
CORTES_TB = "exp_khalix_gida_mensual_2"
CORTES_UP = "GIDA_MENSUAL_2"
QC_TB = "exp_qc_gida_2"
DATABASE = "altres"

TAXES = {
    "PABCESA": 2.07114167,
    "PAFTAA": 0.185053572,
    "PANTIC": 0.315232887,
    "PANSIE": 1.108964916,
    "PAPZB": 0.054125856,
    "PCEFALA": 1.460167308,
    "PCERVI": 0.590053399,
    "PCOLIC": 0.065378861,
    "PCONTD": 0.475705392,
    "PCONTU": 2.853347534,
    "PCOSEN": 0.030694041,
    "PCOSEO": 0.038918985,
    "PCREM": 1.459591958,
    "PCREPE": 0.131583397,
    "PDIAGN": 0.085068006,
    "PDIARR": 1.438499926,
    "PDML": 2.666980739,
    "PEPIS": 0.238669808,
    "PEPISN": 0.032089947,
    "PFEBAN": 0.28959204,
    "PFEBSF": 0.589705165,
    "PFERID": 9.789807754,
    "PFERNE": 1.37551492,
    "PHERPLA": 0.033244034,
    "PINSOMA": 0.045504671,
    "PLDERM": 0.370387366,
    "PLESC": 0.159753006,
    "PMAREI": 4.299228499,
    "PMOLOR": 4.08725073,
    "PMUCO": 0.141594656,
    "PMUGN": 0.058005527,
    "PMURI": 23.11674643,
    "PMVAGA": 0.433172665,
    "PODINO": 1.674979114,
    "PODON": 0.36353631,
    "PPELLA": 0.039510889,
    "PPICIN": 0.15224676,
    "PPICPE": 1.129295037,
    "PPLOIN": 0.04696953,
    "PPOLLS": 0.062616834,
    "PPRESA": 2.298605146,
    "PREGUR": 0.047950598,
    "PRESTN": 0.032562774,
    "PRESTREA": 0.126411516,
    "PRESVA": 2.004108466,
    "PREVACA": 0.203501732,
    "PREVACAP": 0.019981161,
    "PSCOVA": 0.483339118,
    "PSCOVAP": 0.052301939,
    "PSOSVA": 0.044861224,
    "PTOS": 0.173251837,
    "PTURME": 0.609437893,
    "PULL": 1.90590692,
    "PUNGA": 0.284558046,
    "PURTIA": 0.586162798,
    "PVOLTA": 0.252578406,
    "PVOMIT": 0.904370088,
    "PVOMNE": 0.099955026,
    }

CONVERSIO = {"ACE_A": "ANTIC", "CERVI_A": "CERVI", "CONT_A": "CONTU",
             "CREM_A": "CREM", "C_ANSI_A": "ANSIE", "DIARR_A": "DIARR",
             "LUMB_A": "DML", "EPA_A": "PRESA", "ENTOR_A": "TURME",
             "EPIST_A": "EPIS", "FEBRE_A": "FEBSF", "FERIDA_A": "FERID",
             "PLEC_A": "LDERM", "MAREIG_A": "MAREI", "M_OID_A": "MOLOR",
             "M_ULL_A": "ULL", "M_URI_A": "MURI", "ODIN_A": "ODINO",
             "ODONT_A": "ODON", "PICAD_A": "PICPE", "SRVA_A": "RESVA",
             "VOMIT_A": "VOMIT", "ALT_A": "A", "CEFAL": "CEFAL_A",
             "HERP": "HERPL_A", "ABCES": "ABCES_A", "UNG": "UNG_A",
             "VOLT": "VOLT_A", "RESTR": "RESTRE_A", "MVAG": "MVAG_A",
             "REVAC": "REVAC_A", "AFTA": "AFTA_A", "INSOM": "INSOM_A",
             "URTI": "URTI_A", "LESFC_A": "LESC",
             "COLIC_AP": "COLIC", "CON_D_AP": "CONTD", "CEORE_AP": "COSEO",
             "CENAS_AP": "COSEN", "CREMA_AP": "CREPE", "DER_B_AP": "APZB",
             "DIARR_AP": "DIAGN", "EPIST_AP": "EPISN", "FEBRE_AP": "FEBAN",
             "FERID_AP": "FERNE", "MOCS_AP": "MUCO", "PICAD_AP": "PICIN",
             "PLORS_AP": "PLOIN", "POLLS_AP": "POLLS", "REGUR_AP": "REGUR",
             "RESTR_AP": "RESTN", "MUGUE_AP": "MUGN", "VARIC_AP": "SOSVA",
             "TOS_AP": "TOS", "VOMIT_AP": "VOMNE"}


class GIDA(object):
    """."""

    def __init__(self):
        """."""
        self.recomptes = c.defaultdict(lambda: c.defaultdict(c.Counter))
        self.bd_completa = c.defaultdict(lambda: c.defaultdict(c.Counter))
        self.mensual = c.Counter()
        self.get_edats()
        self.get_poblacio()
        self.get_centres()
        self.get_visites()
        self.get_usu_to_col()
        self.get_motius()
        self.get_aguda()
        self.get_master()
        self.get_derivats()
        self.export_master()
        self.get_col_to_uba()
        self.get_ubas()
        self.get_khalix()
        self.get_taxes()
        self.export_khalix()
        self.export_mensual()
        self.export_qc()

    def get_edats(self):
        """."""
        sql = "select id_cip_sec, \
               (year(data_ext) - year(usua_data_naixement)) - \
               (date_format(data_ext, '%m%d') < \
                date_format(usua_data_naixement, '%m%d')) \
               from assignada, nodrizas.dextraccio"
        self.edats = {id: "ADU" if edat > 14 else "PED"
                      for (id, edat) in u.getAll(sql, "import")}

    def get_poblacio(self):
        """."""
        self.poblacio = {}
        sql = "select id_cip_sec, up, uba, ubainf, edat, sexe, ates, institucionalitzat, maca from assignada_tot where ates = 1"
        # considerem que uba és ubainf
        for id, up, uba_code, uba, edat, sexe, ates, institucionalitzat, maca in u.getAll(sql, "nodrizas"):
            grup = self.edats[id]
            for ind in ("GESTINF05", "GESTINF06"):
                self.recomptes[ind][(up, uba, "UBA", maca, grup)]["DEN"] += 1
            self.poblacio[id] = (up, uba, maca)

    def get_centres(self):
        """."""
        sql = "select scs_codi, tip_eap, ics_codi from cat_centres"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_visites(self):
        """."""
        sql = "select id_cip_sec, visi_up, visi_col_prov_resp \
               from visites1, nodrizas.dextraccio \
               where visi_col_prov_resp like '3%' and \
                     s_espe_codi_especialitat not in ('EXTRA', '10102') and \
                     visi_tipus_visita not in ('9E', 'EXTRA', 'EXT') and \
                     visi_situacio_visita = 'R' and \
                     visi_servei_codi_servei = 'INF' and \
                     visi_modul_codi_modul not like 'VCA%' and \
                     visi_modul_codi_modul not like 'VCM%' and \
                     visi_modul_codi_modul not like 'VCP%' and \
                     visi_modul_codi_modul not like 'VCJ%' and \
                     visi_modul_codi_modul not in ('VCAP', 'VCAP2', 'VCAP3') and \
                     visi_data_visita between adddate(data_ext, interval -1 year) and data_ext"  # noqa
        for id, up, col in u.getAll(sql, "import"):
            if up in self.centres and id in self.edats:
                grup = self.edats[id]
                tipus = self.centres[up][0]
                if id in self.poblacio:
                    maca = self.poblacio[id][2]
                    go = (tipus == "M",
                        (grup == "ADU" and tipus == "A"),
                        (grup == "PED" and tipus == "N"))
                    if any(go):
                        for ind in ("GESTINF03", "GESTINF04"):
                            self.recomptes[ind][(up, col, "COL", maca, grup)]["DEN"] += 1

    def get_usu_to_col(self):
        """."""
        sql = "select codi_sector, ide_usuari, ide_numcol \
               from cat_pritb992 \
               where ide_numcol <> ''"
        self.usu_to_col = {row[:2]: row[2] for row in u.getAll(sql, "import")}

    def get_motius(self):
        """."""
        sql = """select sym_name
                 from icsap.klx_master_symbol
                 where dim_index = 4 and (
                    sym_index in (
                        select sym_index from icsap.klx_parent_child
                        where dim_index = 4 and parent_index in (
                            select sym_index from icsap.klx_master_symbol
                            where dim_index = 4 and
                                  sym_name in ('PMOTIUPRPE', 'PMOTIUTEL')
                             )
                        ) or
                    sym_name = 'PALPED')"""
        self.motius = {motiu: "PED" for motiu, in u.getAll(sql, "khalix")}

    def _get_motiu(self, motiu, grup):
        """."""
        motiu = CONVERSIO.get(motiu, motiu)
        if motiu == "A":
            motiu = "PAL{}".format(grup)
        else:
            motiu = "P{}".format(motiu.replace("_", ""))
        if motiu not in self.motius:
            self.motius[motiu] = "ADU"
        return motiu

    def get_aguda(self):
        """."""
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        first = (dext - r.relativedelta(months=1)).replace(day=1)
        sql = "select id_cip_sec, codi_sector, cesp_up, cesp_usu_alta, \
                      cesp_cod_mce, cesp_estat, cesp_data_alta \
               from aguda, nodrizas.dextraccio \
               where cesp_data_alta between \
                            adddate(data_ext, interval -1 year) and data_ext \
               union \
               select id_cip_sec, codi_sector, pi_up, pi_usu_alta, \
                      upper(replace(pi_anagrama, ' ', '')), \
                      if(pi_ser_derivat = '', 'I', 'M'), pi_data_alta \
               from ares, nodrizas.dextraccio \
               where pi_anagrama not in ('', 'TAP_A') and \
                     pi_data_alta between \
                            adddate(data_ext, interval -1 year) and data_ext"
        for id, sec, up, usu, motiu, estat, dat in u.getAll(sql, "import"):
            if id in self.edats:
                grup = self.edats[id]
                motiu = self._get_motiu(motiu, grup)
                motiu_tip = self.motius[motiu]
                if grup == motiu_tip:
                    if id in self.poblacio:
                        up_pac, uba, maca = self.poblacio[id]
                        if up == up_pac:
                            key = (up, uba, "UBA", maca, grup)
                            self.recomptes["GESTINF05"][key][motiu] += 1
                            if estat == "I":
                                self.recomptes["GESTINF06"][key][motiu] += 1
                            if dat >= first:
                                key = ("B" + dat.strftime("%y%m"), up, motiu)
                                self.mensual[("GESTINF05",) + key] += 1
                                if estat == "I":
                                    self.mensual[("GESTINF06",) + key] += 1
                    col = self.usu_to_col.get((sec, usu))
                    key = (up, col, "COL", grup)
                    if key in self.recomptes["GESTINF03"]:
                        self.recomptes["GESTINF03"][key][motiu] += 1
                        if estat == "I":
                            self.recomptes["GESTINF04"][key][motiu] += 1
                        if dat >= first:
                            key = ("B" + dat.strftime("%y%m"), up, motiu)
                            self.mensual[("GESTINF03",) + key] += 1
                            if estat == "I":
                                self.mensual[("GESTINF04",) + key] += 1
                # print('motiu', motiu) # ('motiu', 'PFERID')
                # break

    def get_master(self):
        """."""
        self.master = c.defaultdict(c.Counter)
        for ind, dades in self.recomptes.items():
            for (up, prof, tip, maca, grup), minidades in dades.items():
                den = minidades["DEN"]
                for motiu, motiu_tip in self.motius.items():
                    if motiu_tip == grup:
                        key = (ind, up, prof, tip, grup, motiu, maca)
                        self.master[key]["DEN"] = den
                        self.master[key]["NUM"] = minidades[motiu]

    def get_gestinf05(self):
        [id][(ind, up, uba_code, uba, edat, sexe, ates, institucionalitzat, maca)]["DEN"] += 1
        """."""
        self.gestinf05 = c.defaultdict(c.Counter)
        for id, dades in self.db_completa.items():
            for (ind, up, uba_code, uba, edat, sexe, ates, institucionalitzat, maca), minidades in dades.items():
                den = minidades["DEN"]
                for motiu, motiu_tip in self.motius.items():
                    if motiu_tip == grup:
                        key = (id, ind, up, uba_code, uba, edat, sexe, ates, institucionalitzat, maca)
                        self.master[key]["DEN"] = den
                        self.master[key]["NUM"] = minidades[motiu]

    def get_derivats(self):
        """."""
        for (ind, up, prof, tip, grup, motiu, maca), dades in self.master.items():
            if ind == "GESTINF03":
                key = ("GESTINF02", up, prof, tip, "MIX", "TIPMOTIU", maca)
                self.master[key]["DEN"] += dades["NUM"]
                if motiu not in ("PALADU", "PALPED"):
                    self.master[key]["NUM"] += dades["NUM"]
            if ind in ("GESTINF03", "GESTINF04"):
                key = ("GESTINF07", up, prof, tip, grup, motiu, maca)
                analysis = "DEN" if ind == "GESTINF03" else "NUM"
                self.master[key][analysis] = dades["NUM"]

    def export_master(self):
        """."""
        u.createTable(MASTER_TB, "(ind varchar(10), up varchar(5), \
                                   prof varchar(10), tip varchar(3), \
                                   grup varchar(3), motiu varchar(10), \
                                   maca int, num int, den int)",
                      DATABASE, rm=True)
        upload = [k + (v["NUM"], v["DEN"]) for (k, v) in self.master.items()]
        u.listToTable(upload, MASTER_TB, DATABASE)

    def get_col_to_uba(self):
        """Per passar dades individuals a khalix dels indicadors de NUMCOL."""
        self.col_to_uba = c.defaultdict(set)
        sql = "select ide_numcol, up, uab \
               from cat_professionals \
               where tipus = 'I'"
        for col, up, uba in u.getAll(sql, "import"):
            self.col_to_uba[(up, col)].add(uba)

    def get_ubas(self):
        """Nom�s pujarem UBAs amb EQA."""
        self.ubas = c.defaultdict(set)
        for db, grup in (("eqa_ind", "ADU"), ("pedia", "PED")):
            sql = "select up, uba from mst_ubas where tipus = 'I'"
            for key in u.getAll(sql, db):
                self.ubas[grup].add(key)
                self.ubas["MIX"].add(key)

    def get_khalix(self):
        """."""
        self.khalix = c.Counter()
        for (ind, up, prof, tip, grup, motiu, maca), dades in self.master.items(): # afegir maca
        # for (ind, up, prof, tip, grup, motiu), dades in self.master.items(): # afegir maca
            br = self.centres[up][1]
            for analisi in ("NUM", "DEN"):
                self.khalix[(br, ind, motiu, analisi)] += dades[analisi]
            if tip == "COL":
                ubas = self.col_to_uba[(up, prof)]
            else:
                ubas = (prof,)
            for uba in ubas:
                if (up, uba) in self.ubas[grup]:
                    ent = "{}I{}".format(br, uba)
                    for analisi in ("NUM", "DEN"):
                        self.khalix[(ent, ind, motiu, analisi)] += dades[analisi]  # noqa

    def get_taxes(self):
        """."""
        for (ent, ind, motiu, analisi), n in self.khalix.items():
            if ind == "GESTINF05" and analisi == "DEN" and motiu in TAXES:
                taxa = 1000 * self.khalix[(ent, ind, motiu, "NUM")] / float(n)
                meta = TAXES[motiu]
                good = taxa >= meta
                self.khalix[(ent, "GESTINF01", motiu, "DEN")] = 1
                self.khalix[(ent, "GESTINF01", motiu, "NUM")] = 1 * good

    def export_khalix(self):
        """."""
        cols = "(ent varchar(11), ind varchar(10), motiu varchar(10), \
                 analisi varchar(3), valor int)"
        u.createTable(KHALIX_TB, cols, DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in self.khalix.items()]
        u.listToTable(upload, KHALIX_TB, DATABASE)
        for param, file in (("=", KHALIX_UP), (">", KHALIX_UBA)):
            sql = "select ind, 'Aperiodo', ent, analisi, motiu, \
                          'NOIMP', 'DIM6SET', 'N', valor \
                   from {}.{} \
                   where length(ent) {} 5 and \
                         valor > 0".format(DATABASE, KHALIX_TB, param)
            u.exportKhalix(sql, file)

    def export_mensual(self):
        """."""
        cols = "(ind varchar(10), periode varchar(10), up varchar(10), \
                 motiu varchar(10), valor int)"
        u.createTable(CORTES_TB, cols, DATABASE, rm=True)
        upload = [k + (v,) for (k, v) in self.mensual.items()]
        u.listToTable(upload, CORTES_TB, DATABASE)
        sql = "select ind, periode, ics_codi, 'NUM', motiu, \
                      'NOIMP', 'DIM6SET', 'N', valor \
                   from {}.{} inner join nodrizas.cat_centres on up = scs_codi"
        u.exportKhalix(sql.format(DATABASE, CORTES_TB), CORTES_UP)

    def export_qc(self):
        """."""
        indicadors = ("GESTINF05", "GESTINF06")
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        fa1m = dext - r.relativedelta(months=1)
        pactual = "A{}".format(dext.strftime("%y%m"))
        pprevi = "A{}".format(fa1m.strftime("%y%m"))
        sqls = []
        for x, y in ((pactual, 'ANUAL'), (pactual, 'ACTUAL'), (pprevi, 'PREVI')):  # noqa
            sqls.append("select ind, '{}', ent, 'DEN', '{}', 'TIPPROF4', \
                                sum(valor) \
                         from {} \
                         where ind in {} and \
                               length(ent) = 5 and analisi = 'DEN' and \
                               motiu in ('PALADU', 'PALPED')\
                         group by ind, ent".format(x, y, KHALIX_TB, indicadors))  # noqa
        sqls.append("select ind, '{}', ent, 'NUM', 'ANUAL', 'TIPPROF4', \
                            sum(valor) \
                     from {} \
                     where ind in {} and \
                           length(ent) = 5 and analisi = 'NUM' \
                     group by ind, ent".format(pactual, KHALIX_TB, indicadors))
        sqls.append("select ind, replace(periode, 'B', 'A'), ics_codi, 'NUM', \
                            if(replace(periode, 'B', 'A') = '{}', 'ACTUAL', 'PREVI'), \
                            'TIPPROF4', sum(valor) \
                     from {} a \
                     inner join nodrizas.cat_centres b on a.up = b.scs_codi \
                     where ind in {} \
                     group by ind, periode, up".format(pactual, CORTES_TB, indicadors))  # noqa
        dades = []
        for sql in sqls:
            dades.extend([("Q" + row[0],) + row[1:]
                          for row in u.getAll(sql, DATABASE)])
        cols = ["k{} varchar(10)".format(i) for i in range(6)] + ["v int"]
        u.createTable(QC_TB, "({})".format(", ".join(cols)), DATABASE, rm=True)
        u.listToTable(dades, QC_TB, DATABASE)


if __name__ == "__main__":
    GIDA()
