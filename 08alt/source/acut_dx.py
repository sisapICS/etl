
import sisapUtils as u

cols = """(up varchar(5), desc_up varchar(40), centre varchar(9), classe varchar(2), 
            desc_centre varchar(40), dx varchar(50), desc_dx varchar(80), desc_dx_c varchar(50), n int)"""
u.createTable('acut_dx_altes', cols, 'altres', rm = True)

codis = {}
sql = """SELECT ps_cod_o, ps_cod, ps_des, ps_des_c FROM prstb001"""
for o, cod, desc, desc_c in u.getAll(sql, 'redics'):
    codis[(o, cod)] = (desc, desc_c)

centres = {}
sql = """select a.cuag_up, c.u_pr_descripcio, a.cuag_codi_centre, a.cuag_classe_centre, b.cent_nom_centre
    from import.cat_cmbdtb002 a
    inner join import.cat_pritb010 b on a.cuag_sector = b.codi_sector and a.cuag_codi_centre = b.cent_codi_centre and a.cuag_classe_centre = b.cent_classe_centre
    inner join import.cat_pritb008 c on a.cuag_sector = c.codi_sector and a.cuag_up = c.u_pr_cod_up"""
for up, up_desc, centre, classe, nom_centre in u.getAll(sql, 'import'):
    centres[(up, classe, centre)] = (up_desc, nom_centre)

sql = """SELECT
                IAU_UP, IAU_CODI_CENTRE, IAU_CLASSE_CENTRE, iau_prob_principal, IAU_PROB_PRINCIPAl_O, count(1)
            from prstb152
            WHERE
                IAU_DATA_VISITA BETWEEN TO_CHAR(to_date('01/01/2022','dd/mm/yyyy'),'j')
                AND TO_CHAR(to_date('31/10/2022','dd/mm/yyyy'),'j')
            group by IAU_UP, IAU_CODI_CENTRE, IAU_CLASSE_CENTRE, iau_prob_principal, IAU_PROB_PRINCIPAl_O"""
upload = []
for s in u.sectors:
    print(s)
    for up, centre, classe, dx, c, n in u.getAll(sql, s):
        if (up, classe, centre) in centres:
            desc_up, desc_centre = centres[(up, classe, centre)]
            if (c, dx) in codis:
                desc_dx, desc_dx_c = codis[(c, dx)]
                upload.append((up, desc_up, centre, classe, desc_centre, dx, desc_dx, desc_dx_c, n))

u.listToTable(upload, 'acut_dx_altes', 'altres')
 