# coding: latin1

"""
.
"""

import collections as c

import sisaptools as u


TABLE = "exp_khalix_it_esp"


class Esperat(object):
    """."""

    def get_preliminars(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.up_to_br = {up: br for (up, br)
                         in u.Database("p2262", "nodrizas").get_all(sql)}
        sql = "select sector, valor from sisap_medea"
        self.sector_to_medea = {sector: valor for (sector, valor)
                                in u.Database("redics", "data").get_all(sql)}
        sql = "select id_cip_sec, gma_ind_cmplx \
               from gma a \
               where exists (select 1 from nodrizas.assignada_tot b \
                             where a.id_cip_sec = b.id_cip_sec)"
        self.id_to_gma = {id: float(gma) for (id, gma)
                          in u.Database("p2262", "import").get_all(sql)}

    def get_parametres(self):
        """."""
        raw_data = c.defaultdict(c.Counter)
        poblacio = c.defaultdict(c.Counter)
        sql = "select id_cip_sec, up, seccio_censal, edat, sexe, \
                      nivell_cobertura \
               from assignada_tot \
               where ates = 1 and edat between 15 and 65"
        for id, up, seccio, edat , sexe, estat in u.Database("p2262", "nodrizas").get_all(sql):  # noqa
            br = self.up_to_br[up]
            medea = self.sector_to_medea.get(seccio)
            gma = self.id_to_gma.get(id)
            poblacio[br]["edat"] += 1
            poblacio[br]["dones"] += 1
            poblacio[br]["pensionistes"] += 1
            raw_data[br]["edat"] += float(edat)
            if sexe == "D":
                raw_data[br]["dones"] += 1
            if estat == "PN":
                raw_data[br]["pensionistes"] += 1
            if medea is not None:
                poblacio[br]["medea"] += 1
                raw_data[br]["medea"] += medea
            if gma is not None:
                poblacio[br]["gma"] += 1
                raw_data[br]["gma"] += gma
        self.parametres = {}
        per_cent = ("dones", "pensionistes")
        for br, dades in raw_data.items():
            self.parametres[br] = {key: ((100.0 if key in per_cent else 1.0) *
                                         dades[key] / poblacio[br][key])
                                   for key in dades}
            self.parametres[br]["intercept"] = 1

    def get_indicadors(self):
        """."""
        matriu = {"IT001TOT": {"intercept": -67.84208817,
                               "edat": 3.593682612,
                               "dones": -0.905416762,
                               "pensionistes": 0.834987539,
                               "medea": 0.655936681,
                               "gma": -3.238449154},
                  "IT002TOT": {"intercept": -2.230437312,
                               "edat": -0.168613265,
                               "dones": 0.212036114,
                               "pensionistes": 0.531233452,
                               "medea": 0.155068166,
                               "gma": 1.272966641},
                  "IT003TOT": {"intercept": 1.61686419,
                               "edat": -0.033065076,
                               "dones": 0.007255905,
                               "pensionistes": -0.00766912,
                               "medea": 0.000785752,
                               "gma": 0.02742226},
                  "IT005TOT": {"intercept": -80.96487443,
                               "edat": -6.120661537,
                               "dones": 7.696910941,
                               "pensionistes": 19.28377429,
                               "medea": 5.628974417,
                               "gma": 46.20868906},
                  "IT006TOT": {"intercept": -61.97846976,
                               "edat": -5.15907655,
                               "dones": 5.917625397,
                               "pensionistes": 15.85345805,
                               "medea": 9.55651082,
                               "gma": 28.37935229}}
        upload = []
        for br, dades in self.parametres.items():
            for indicador, betes in matriu.items():
                try:
                    resultat = sum([beta * dades[key] for (key, beta)
                                    in betes.items()])
                except KeyError:
                    print(br, dades)
                else:
                    upload.append((br, indicador, resultat))
        cols = ("br varchar(5)", "indicador varchar(10)", "resultat double")
        with u.Database("p2262", "altres") as altres:
            altres.create_table(TABLE, cols, remove=True)
            altres.list_to_table(upload, TABLE)


if __name__ == "__main__":
    esperat = Esperat()
    esperat.get_preliminars()
    esperat.get_parametres()
    esperat.get_indicadors()
