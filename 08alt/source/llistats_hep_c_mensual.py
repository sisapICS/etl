
import sisapUtils as u
import datetime as d

if __name__ == "__main__":
    if u.IS_MENSUAL:
        centres = set()
        # volem BCN
        sql = """select scs_codi from nodrizas.cat_centres
                    where sap_codi in ('51','52','53','54')"""
        for up, in u.getAll(sql, 'nodrizas'):
            centres.add(up)

        
        sql_mensual = """SELECT a.UP, up_desc_up_ics, ESTAT, ESTAT_DESC, ACTUACIO, ACTUACIO_DESC, COUNT (*) AS N 
                FROM sisap_covid_hepatitis_c a, HEPATCINC2022 b, HEPATC_ESTAT C, HEPATC_ACTUACIO D,  centres cc
                WHERE a.cip=b.cip and ESTAT=ESTAT_CODI AND ACTUACIO=ACTUACIO_CODI AND cc.up_codi_up_scs = a.up
                GROUP BY a.UP, up_desc_up_ics, ESTAT, ESTAT_DESC, ACTUACIO, ACTUACIO_DESC"""

        upload = []
        for up, des, estat, estat_desc, actuacio, actuacio_desc, n in u.getAll(sql_mensual, 'pdp'):
            if up in centres:
                upload.append((up, des, estat, estat_desc, actuacio, actuacio_desc, n))

        SMS_DIA = d.datetime.now().date()
        file_eap = u.tempFolder + 'Seguiment_hepatits_mensual_{}.csv'.format(SMS_DIA)
        u.writeCSV(file_eap, [('UP', 'DES_UP', 'ESTAT', 'DESC_ESTAT', 'ACTUACIO', 'DESC_ACTUACIO', 'N')] + upload, sep=";")

        # email
        subject = 'Seguiment hepatitis C sense tractament mensual'
        text = "Bon dia.\n\nAdjuntem arxiu .csv amb les dades de seguiment mensuals de l'Hepatitis C sense tractament.\n"
        text += "\n\nSalutacions."

        u.sendGeneral('SISAP <sisap@gencat.cat>',
                            ('amasc@gencat.cat', 'rriel@gencat.cat', 'ialarcon.bcn.ics@gencat.cat',
                            'mjpardo.bcn.ics@gencat.cat', 'jlval.bcn.ics@gencat.cat', 'raquelgarcia@gencat.cat', 
                            'emartingr.bcn.ics@gencat.cat', 'xavier.major@gencat.cat'),
                            ('roser.cantenys@catsalut.cat', 'eballo.girona.ics@gencat.cat'),
                            subject,
                            text,
                            file_eap)