# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
from dateutil import relativedelta as rd
import sisapUtils as u
import sisaptools as t
import os

TEST = False
RECOVER = False

nod = "nodrizas"

SERVEI_CONVERT = {
    'MG': 'MF',
    'WMG': 'MF',
    'INF': 'INF',
    'WINF': 'INF',
    'URGMG': 'URG',
    'URGIN': 'URG',
    'EXTRA': 'EXTRA',
    'TRA': 'TRAU',
    'CIR': 'CIR',
    'DER': 'DER',
    'OPT': 'OPT',
    'MEDIN': 'MIN'
}

NOIMP = 'NOIMP'
KCLI = 'NOCLI'
KVAL = 'N'

if RECOVER:
    DEXTD = d.date(2024, 1, 30)
    DEXTD_P = d.date(2023, 12, 31)
    FILE_NAME = 'ACTASSIST_EAPP_{}.txt'.format(DEXTD.strftime("%m_%Y"))
    FILE_NAME_ANUAL = 'ACTASSIST_EAPP_ANUAL_{}.txt'.format(DEXTD.strftime("%m_%Y"))
else:
    DEXTD = t.Database("p2262", nod).get_one("select data_ext from dextraccio")[0]  # noqa
    DEXTD_P = t.Database("p2262", nod).get_one("select date_add(data_ext,interval - 1 month) from NODRIZAS.dextraccio")[0]  # noqa
    FILE_NAME = 'ACTASSIST_EAPP'
    FILE_NAME_ANUAL = 'ACTASSIST_EAPP_ANUAL'


def get_gedat(naix, vdat):
    # Get the current date
    # Get the difference between the current date and the birthday
    age = rd.relativedelta(vdat, naix)
    age = age.years
    gedat = u.ageConverter(age)
    return gedat


class Activitat(object):
    """."""

    def __init__(self):
        """."""
        cols = """(cuenta varchar(50), periode varchar(50), br varchar(50), 
                    KCLI varchar(50), edat_k varchar(50), KDTL varchar(50),
                    sex_k varchar(50), KVAL varchar(50), VALOR int)"""
        u.createTable('visites_eapp', cols, 'altres', rm=True)
        u.createTable('visites_eapp_anual', cols, 'altres', rm=True)
        u.printTime('get_centres')
        sql = """select scs_codi, ics_codi from nodrizas.cat_centres_with_jail
                    where sector = '6951'"""
        self.up_to_br = {}
        for up, br in u.getAll(sql, "nodrizas"):
            self.up_to_br[up] = br
        u.printTime('pob')
        self.poblacio = {}
        sql = """SELECT
                    hash,
                    ecap_up,
                    data_naixement,
                    CASE
                        WHEN sexe = 'D' THEN 'DONA'
                        WHEN sexe = 'H' THEN 'HOME'
                        WHEN sexe = 'M' THEN 'DONA'
                    END SEXE
                FROM
                    dwsisap.dbc_poblacio"""
        for id, up, dat_naix, sexe in u.getAll(sql, 'exadata'):
            self.poblacio[id] = (dat_naix, sexe, up) 
        u.printTime('get_visites')
        self.get_visites()
        self.to_upload()
        u.printTime('fi')

    def get_visites(self):
        """."""
        self.visites = c.defaultdict(set)
        sql = """
                SELECT
                    v.pacient,
                    CASE
                        WHEN v.SISAP_SERVEI_CODI = 'EXTRA' THEN v.SISAP_SERVEI_CODI
                        ELSE v.SERVEI
                    END SERVEI,
                    v.modul,
                    v.LLOC,
                    v.TIPUS,
                    v.ETIQUETA,
                    v.DATA,
                    extract(year FROM v.DATA) as year_v,
                    v.up,
                    CASE WHEN v.DATA > add_months(DATE '{DEXTD}',-1) THEN 'ACTUAL' 
                        WHEN v.DATA > add_months(DATE '{DEXTD}',-12) THEN 'BOTH'
                        ELSE 'PAST' 
                    END PERIODE_ANUAL
                FROM
                    dwsisap.sisap_master_visites v
                WHERE
                    v.situacio = 'R'
                    AND sector = '6951'
                    AND (SERVEI IN ('MG', 'WMG', 'INF',
                        'WINF', 'URGMG', 'URGIN', 'EXTRA',
                        'TRA', 'CIR', 'DER', 'OPT', 'MEDIN') 
                        OR SISAP_SERVEI_CODI = 'EXTRA')
                    AND v.data BETWEEN add_months(DATE '{DEXTD}',-13) AND DATE '{DEXTD}'
                    """.format(DEXTD=DEXTD)
        self.cuentas = c.Counter()
        self.cuentas_anuals = c.Counter()
        for cip, servei, modul, lloc, tip, tag, vdat, year_v, up, pa in u.getAll(sql,'exadata'):
            self.visites[cip].add((servei, modul, lloc, tip, tag, vdat, year_v, up, pa))
        
        print('tinc totes les visites, anem a tractar info')
        for cip, mul_visites in self.visites.items():
            if cip in self.poblacio:
                dat_naix, sex_k, up_ass = self.poblacio[cip]
                for servei, modul, lloc, tip, tag, vdat, year_v, up, pa in mul_visites:
                    if up in self.up_to_br:
                        if dat_naix <= vdat:
                            edat_k = get_gedat(dat_naix, vdat)
                            br = self.up_to_br[up]
                            ym = vdat.strftime('%y%m')
                            periode = "A{}".format(ym)
                            periode_anual = []
                            if pa == 'BOTH':
                                periode_anual.append("B{}".format(DEXTD_P.strftime('%y%m')))
                                periode_anual.append("B{}".format(DEXTD.strftime('%y%m')))
                            elif pa == 'ACTUAL':
                                periode_anual.append("B{}".format(DEXTD.strftime('%y%m')))
                            else: 
                                periode_anual.append("B{}".format(DEXTD_P.strftime('%y%m')))
                            extres = ''
                            tipus = ''
                            if servei == 'URGMG':
                                extres = 'MF'
                            elif servei == 'URGIN':
                                extres = 'INF'
                            servei = SERVEI_CONVERT[servei]
                            if servei in ('TRAU', 'CIR', 'DER', 'OPT', 'MIN'):
                                if tip == '9E':
                                    tipus = 'VIR'
                                elif tip in ('EPVI', 'EURG'):
                                    tipus = '1A'
                                elif tip in ('ESUC', 'EINT'):
                                    tipus = '2A'
                            elif servei in ('MF', 'INF', 'URG'):
                                if tip == '9E':
                                    tipus = 'VIR'
                                else:
                                    tipus = 'CEN'
                            elif servei == 'EXTRA':
                                tipus = 'CEN'

                            cuenta = "VIS{servei}{tipus}{extres}PRS".format(servei=servei, tipus=tipus, extres=extres)
                            if year_v == DEXTD.year or (year_v == DEXTD.year - 1 and DEXTD.month == 1):
                                self.cuentas[(cuenta, periode, br, edat_k, sex_k, NOIMP)] += 1
                            for p_anual in periode_anual:
                                self.cuentas_anuals[(cuenta, p_anual, br, edat_k, sex_k, NOIMP)] += 1

    def to_upload(self):
        upload = []
        for k, v in self.cuentas.items():
            upload.append([k[0], k[1], k[2], KCLI, k[3], k[5], k[4], KVAL, v])
        u.listToTable(upload, 'visites_eapp', 'altres')
        u.exportKhalix('select * from altres.visites_eapp', FILE_NAME, force=True)
        upload = []
        for k, v in self.cuentas_anuals.items():
            upload.append([k[0], k[1], k[2], KCLI, k[3], k[5], k[4], KVAL, v])
        u.listToTable(upload, 'visites_eapp_anual', 'altres')
        u.exportKhalix('select * from altres.visites_eapp_anual', FILE_NAME_ANUAL, force=True)

if __name__ == '__main__':
    if u.IS_MENSUAL:
        u.printTime('inici')
        Activitat()
        u.printTime('fi')
