# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
from dateutil import relativedelta as rd
import sisapUtils as u
import sisaptools as t
import os

TEST = False
RECOVER = False

nod = "nodrizas"
imp = "import"
imp_jail = "import_jail"

SERVEI_MAP = {
    'EXTRA': 'EXTRA',
    'EXT': 'EXTRA',
    'UAC': 'UAC',
    'UAAU': 'UAC',
    'ASO': 'TS',
    'AS': 'TS',
    'ASS': 'TS',
    'TS': 'TS',
    'TSOC': 'TS',
    'SS': 'TS',
    'RNAS': 'TS',
    'ASI': 'TS',
    'AASS': 'TS',
}

HOMOL_MAP = {
    'MG': 'MF',
    'INFP': 'INF',
    'LLEV': 'ALTRE',
    'GIN': 'ALTRE',
}

TIPUS_MAP = {
    '9C': 'C9C',
    '9R': 'C9R',
    '9D': 'D9D',
    '9T': '9T',
    '9E': '9E',
    '9Ec': '9EC'
}

ETIQUETAS = [
    'INFC',  # INFORME CL�NIC
    'PLME',  # PLA MEDICACIO
    'REIT',  # REVISIO IT
    'TRSA',  # TRANSPORT SANITARI
    'VALP',  # VALORACI� PROVES
    'VITA',  # V�DEO CONSULTA
]

KCAT = 'NOCAT'
KCLI = 'NOCLI'
KVAL = 'N'

if RECOVER:
    DEXTD = d.date(2024, 2, 29)
    FILE_NAME = 'ACTASSIST_UBA_{}.txt'.format(DEXTD.strftime("%m_%Y"))
else:
    DEXTD = t.Database("p2262", nod).get_one("select data_ext from dextraccio")[0]  # noqa
    FILE_NAME = 'ACTASSIST_UBA'


class Activitat(object):
    """."""

    def __init__(self):
        """."""
        cols = """(cuenta varchar(50), periode varchar(50), br varchar(50), 
                    professional varchar(50), detall varchar(20), VALOR int)"""
        u.createTable('simulacio_visites_uba', cols, 'altres', rm=True)
        u.printTime('get_centres')
        sql = """select scs_codi, ics_codi from nodrizas.cat_centres_with_jail"""
        self.up_to_br = {}
        for up, br in u.getAll(sql, "nodrizas"):
            self.up_to_br[up] = br
        u.printTime('pob')
        self.poblacio = {}
        sql = """SELECT
                    hash,
                    ecap_up,
                    data_naixement,
                    CASE
                        WHEN sexe = 'D' THEN 'DONA'
                        WHEN sexe = 'H' THEN 'HOME'
                        WHEN sexe = 'M' THEN 'DONA'
                    END SEXE
                FROM
                    dwsisap.dbc_poblacio"""
        for id, up, dat_naix, sexe in u.getAll(sql, 'exadata'):
            self.poblacio[id] = (dat_naix, sexe, up) 
        u.printTime('get_visites')
        self.get_visites()
        self.to_upload()
        u.printTime('fi')

    def get_visites(self):
        """."""
        self.visites = c.defaultdict(set)
        sql = """
                SELECT
                    v.pacient,
                    v.sector,
                    CASE
                        WHEN v.SISAP_SERVEI_CODI = 'EXTRA' THEN v.SISAP_SERVEI_CODI
                        ELSE v.SISAP_SERVEI_CLASS
                    END SERVEI,
                    v.modul,
                    v.LLOC,
                    v.TIPUS,
                    v.ETIQUETA,
                    v.DATA,
                    v.up,
                    v.RELACIO_PROVEIDOR,
                    v.professional,
                    CASE WHEN v.DATA > add_months(DATE '{DEXTD}',-13) THEN 'ARA'
                    ELSE 'PAST' END TEMPS
                FROM
                    dwsisap.sisap_master_visites v
                WHERE
                    v.situacio = 'R'
                    AND v.data BETWEEN add_months(DATE '{DEXTD}',-24) AND DATE '{DEXTD}'
                    """.format(DEXTD=DEXTD)
        self.cuentas = c.Counter()
        self.cuentas_anuals = c.Counter()
        for cip, sec, servei, modul, lloc, tip, tag, vdat, up, relacio, professional, temps in u.getAll(sql,'exadata'):
            self.visites[cip].add((sec, servei, modul, lloc, tip, tag, vdat, up, relacio, professional, temps))
        
        print('tinc totes les visites, anem a tractar info')
        for cip, mul_visites in self.visites.items():
            if cip in self.poblacio:
                dat_naix, sex_k, up_ass = self.poblacio[cip]
                for sec, servei, modul, lloc, tip, tag, vdat, up, relacio, professional, temps in mul_visites:
                    if up in self.up_to_br:
                        if dat_naix <= vdat:
                            br = self.up_to_br[up]
                            ym = DEXTD.strftime('%y%m')
                            periode = "B{}".format(ym)
                            if tip in TIPUS_MAP:  # ['9C', '9R', '9D', '9T', '9E']
                                if tip == '9E' and tag in ETIQUETAS:
                                    tipus = tip + tag
                                else:
                                    tipus = TIPUS_MAP[tip]
                            elif lloc in ['C', 'D']:
                                tipus = lloc + 'AL'
                            else:
                                tipus = 'ERROR'
                            if servei in ('AUX','NONE', 'GIN', 'LLE') or servei is None:
                                servei = 'ALTRE'
                            cuenta = "VIS{}{}".format(servei, tipus)
                            if up_ass is None:
                                detalle = 'VISNOASS'
                            elif relacio in ('3', '4'):
                                if up == up_ass:
                                    detalle = 'VISASSIG'
                                else:
                                    detalle = 'VISNOASS'
                            elif relacio in ('1', '2'):
                                detalle = 'VISASSIG'
                            else:
                                detalle = 'VISNO'
                            self.cuentas[(cuenta+'+2', periode, br, professional, detalle)] += 1
                            if temps == 'ARA':
                                self.cuentas[(cuenta, periode, br, professional, detalle)] += 1
        upload = []
        for k in self.cuentas:
            cuenta, periode, br, professional, detalle = k
            val = self.cuentas[k]
            upload.append([cuenta, periode, br, professional, detalle, val])
        u.listToTable(upload, 'simulacio_visites_uba', 'altres')

    def to_upload(self):
        cols = """(cuenta varchar(50), periode varchar(50), br varchar(50), VALOR int)"""
        u.createTable('exp_khalix_visites_uba', cols, 'altres', rm=True)
        # Filtrem x ubas de eqa
        ubas_eqa = set()
        sql = """select up, uba, tipus from eqa_ind.mst_ubas where tipus in ('I', 'M')
                    union
                    select up, uba, tipus from pedia.mst_ubas where tipus in ('I', 'M')"""
        for up, uba, tipus in u.getAll(sql, 'nodrizas'):
            ubas_eqa.add((up, uba, tipus))
        # Convert de ups
        br_2_up = {}
        sql = """select scs_codi, ics_codi from nodrizas.cat_centres"""
        for up, br in u.getAll(sql, 'nodrizas'):
            br_2_up[br] = up
        # Filtrem x professional i dni
        sql = """select ide_dni, up, uab, tipus
                    from import.cat_professionals
                    where tipus is not null
                    and uab is not null
                    and tipus in ('I', 'M')"""
        dni_2_uba = {}
        for dni, up, uba, tipus in u.getAll(sql, 'import'):
            if (up, uba, tipus) in ubas_eqa:
                dni_2_uba[(dni[:8], up)] = (uba, tipus)
        # dades x penjar
        upload = c.Counter()
        sql = """select cuenta, periode, br, professional, valor 
                            from altres.simulacio_visites_uba
                            WHERE cuenta not like '%+2'"""
        for cuenta, periode, br, dni, valor in u.getAll(sql, 'altres'):
            if br in br_2_up:
                up = br_2_up[br]
                if (dni[:8], up) in dni_2_uba:
                    uba, tipus = dni_2_uba[(dni[:8], up)]
                    upload[(cuenta, periode, br+tipus+uba)] += valor
        uploading = [(cuenta, periode, br, valor) for (cuenta, periode, br), valor in upload.items()]
        u.listToTable(uploading, 'exp_khalix_visites_uba', 'altres')
        u.exportKhalix("""select cuenta, periode, br, 'NOCLI', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', VALOR from altres.exp_khalix_visites_uba""", FILE_NAME)


if __name__ == '__main__':
    u.printTime('inici')
    Activitat()
    u.printTime('fi')
