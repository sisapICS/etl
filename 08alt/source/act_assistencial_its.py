import sisapUtils as u
import datetime as d
import collections as c
from dateutil import relativedelta as rd

u.printTime('inici')
DEXTD = u.getOne("select data_ext from dextraccio", "nodrizas")[0]
menys1 = DEXTD - rd.relativedelta(years=1)

CODIS_ITS = [
    'A50.4',
    'A52.9',
    'A53',
    'A53.9',
    'A54.9',
    'A56',
    'A56.1',
    'A56.8',
    'A57',
    'A59',
    'A59.9',
    'A60',
    'A60.0',
    'A60.9',
    'A63.0',
    'A64',
    'B16',
    'B16.9',
    'C01-A50.09',
    'C01-A50.1',
    'C01-A50.2',
    'C01-A50.59',
    'C01-A50.6',
    'C01-A50.7',
    'C01-A50.9',
    'C01-A51.0',
    'C01-A51.1',
    'C01-A51.2',
    'C01-A51.39',
    'C01-A51.49',
    'C01-A51.5',
    'C01-A51.9',
    'C01-A52.00',
    'C01-A52.10',
    'C01-A52.2',
    'C01-A52.3',
    'C01-A52.79',
    'C01-A52.8',
    'C01-A52.9',
    'C01-A53.0',
    'C01-A53.9',
    'C01-A54.00',
    'C01-A54.1',
    'C01-A54.24',
    'C01-A54.29',
    'C01-A54.30',
    'C01-A54.5',
    'C01-A54.6',
    'C01-A54.85',
    'C01-A54.89',
    'C01-A54.9',
    'C01-A55',
    'C01-A56.00',
    'C01-A56.11',
    'C01-A56.2',
    'C01-A56.3',
    'C01-A56.4',
    'C01-A56.8',
    'C01-A57',
    'C01-A58',
    'C01-A59.00',
    'C01-A59.8',
    'C01-A59.9',
    'C01-A60.00',
    'C01-A60.1',
    'C01-A60.9',
    'C01-A63.0',
    'C01-A63.8',
    'C01-A64',
    'C01-B16.0',
    'C01-B16.1',
    'C01-B16.2',
    'C01-B16.9',
    'C01-B18.0',
    'C01-B18.1',
    'C01-M71.10',
    'C01-Z22.4',
    'C01-N34.0',
    'C01-N34.1',
    'C01-N34.2',
    'C01-N34.3',
    'N34.0',
    'N34',
]

def get_gedat(naix, vdat):
    # Get the current date
    # Get the difference between the current date and the birthday
    age = rd.relativedelta(vdat, naix)
    age = age.years
    gedat = u.ageConverter(age)
    return gedat

def sub_get_seguiment(params):
    its_sec = set()
    tb, sql = params
    for id, sector, data, up, servei, modul, numcol, codis in u.getAll(sql.format(tb=tb, menys1=menys1, DEXTD=DEXTD), 'import'):
        for codi in codis.split('#'):
            if codi in CODIS_ITS:
                if isinstance(data, d.datetime):
                    data = data.date()
                its_sec.add((id, sector, data, up, servei, modul, numcol))
    u.printTime(tb)
    return its_sec

sql = """
    SELECT id_cip_sec, codi_sector, hash_d
    FROM u11
"""
hashos = c.defaultdict(dict)
for id, sec, hash in u.getAll(sql, 'import'):
    hashos[id][sec] = hash
# idcips = {hash: {sec: id} for id, sec, hash in u.getAll(sql, 'import')}
u.printTime('idcips')

sql = """
    SELECT hash_redics, hash_covid
    FROM dwsisap.pdptb101_relacio
"""
redics_covid = {}
for hash, covid in u.getAll(sql, 'exadata'):
    try:
        redics_covid[hash] = covid
    except KeyError:
        pass
u.printTime('conversor')

sql = """
    SELECT id_cip_sec, codi_sector, sc_datseg, sc_cupseg, sc_v_ser, sc_v_mod, sc_numcol, sc_coddiag
    FROM {tb}
    WHERE sc_coddiag is not null
    AND sc_datseg BETWEEN DATE '{menys1}' AND DATE '{DEXTD}'
"""
its = set()
tables = u.getSubTables('seguiment', 'import')
jobs = [(table, sql) for table in tables]
u.printTime('multiprocess')
for chunk in u.multiprocess(sub_get_seguiment, jobs, 8, close=True):
    its.update(chunk)
u.printTime('seguiment')
print(len(its))
sql = """
    SELECT id_cip_sec, codi_sector, visi_data_visita, visi_up, visi_servei_codi_servei, visi_modul_codi_modul, visi_col_prov_resp, visi_id, visi_rel_proveidor
    FROM visites1
    WHERE visi_situacio_visita = 'R'
"""

visites = set()
for id, sec, data, up, servei, modul, numcol, idvisita, rel_prov in u.getAll(sql, 'import'):
    if (id, sec, data, up, servei, modul, numcol) in its:
        visites.add((id, sec, data, up, servei, modul, numcol, rel_prov))
u.printTime('visites1')
print(len(visites))

sql = """
    SELECT sector, cod, servei_map, servei_class
    FROM dwsisap.sisap_map_serveis
"""

serveis = {(cod, sec): (smap, serv) for sec, cod, smap, serv in u.getAll(sql, 'exadata')}
u.printTime('serveis')

poblacio = {}
sql = """SELECT
            hash,
            ecap_up,
            data_naixement,
            CASE
                WHEN sexe = 'D' THEN 'DONA'
                WHEN sexe = 'H' THEN 'HOME'
                WHEN sexe = 'M' THEN 'DONA'
            END SEXE
        FROM
            dwsisap.dbc_poblacio"""
for id, up, dat_naix, sexe in u.getAll(sql, 'exadata'):
    poblacio[id] = (dat_naix, sexe, up)
u.printTime('poblacio')

# EAPs i EAPPs
sql = """select scs_codi, ics_codi from nodrizas.cat_centres_with_jail"""
centres = {}
for up, br in u.getAll(sql, "nodrizas"):
    centres[up] = br
u.printTime('centres')

sql = """select distinct up_assir, br_assir from nodrizas.ass_centres"""
for up, br in u.getAll(sql, "nodrizas"):
    centres[up] = br
u.printTime('centres')

sql = """select scs_cod, concat(concat(codi_centre, '_'), classe_centre) from permanent.centres_acut"""
for up, br in u.getAll(sql, "permanent"):
    centres[up] = br
u.printTime('centres')

DEXTD_P = u.getOne("select date_add(data_ext,interval - 1 month) from dextraccio", 'nodrizas')[0]  # noqa
cuentas = c.Counter()
cuentas_anuals = c.Counter()
# DEXTD_P = u.getOne("select date_add(data_ext,interval - 1 month) from dextraccio", "nodrizas")[0]
for (id, sec, data, up, servei, modul, numcol, relacio) in visites:
    if (id, sec, data, up, servei, modul, numcol) in its:
        if id in hashos and sec in hashos[id]:
            redics = hashos[id][sec]
            if redics in redics_covid and redics_covid[redics] in poblacio:
                naix, sexe_k, up_ass = poblacio[redics_covid[redics]]
                if up in centres:
                    edat_k = get_gedat(naix, data)
                    br = centres[up]
                    ym = data.strftime('%y%m')
                    periode = "A{}".format(ym)
                    periode_anual = "B{}".format(DEXTD.strftime('%y%m'))
                    if (servei, sec) in serveis:
                        (smap, serv) = serveis[(servei, sec)]
                    else:
                        serv = 'ALTRE'
                    if serv in ('AUX', 'NONE', 'URG', 'TS', 'ODO', 'UAC'):
                        serv = 'ALTRE'
                    cuenta = "VISITS{}".format(serv)
                    if up_ass is None:
                        detalle = 'VISNOASS'
                    elif relacio in ('3', '4'):
                        if up == up_ass:
                            detalle = 'VISASSIG'
                        else:
                            detalle = 'VISNOASS'
                    elif relacio in ('1', '2'):
                        detalle = 'VISASSIG'
                    else:
                        detalle = 'VISNO'
                    if data.year == DEXTD.year:
                        cuentas[(cuenta, periode, br, edat_k, sexe_k, detalle)] += 1
                        if smap and smap == 'ASSIR':
                            cuentaglob = 'VISITS{}'.format(serv)
                            cuentas[(cuentaglob, periode, br, edat_k, sexe_k, detalle)] += 1
                    cuentas_anuals[(cuenta, periode_anual, br, edat_k, sexe_k, detalle)] += 1
                    if smap and smap == 'ASSIR':
                        cuentaglob = 'VISITS{}'.format(serv)
                        cuentas_anuals[(cuentaglob, periode_anual, br, edat_k, sexe_k, detalle)] += 1
u.printTime('cuinetes')

KCAT = 'NOCAT'
KCLI = 'NOCLI'
KVAL = 'N'
cols = """(cuenta varchar(50), periode varchar(50), br varchar(50), 
            KCLI varchar(50), edat_k varchar(50), KDTL varchar(50),
            sex_k varchar(50), KVAL varchar(50), VALOR int)"""
u.createTable('simulacio_visites_its', cols, 'altres', rm=True)
u.createTable('simulacio_visites_its_anual', cols, 'altres', rm=True)
upload = []
for k, v in cuentas.items():
    upload.append([k[0], k[1], k[2], KCLI, k[3], k[5], k[4], KVAL, v])
u.listToTable(upload, 'simulacio_visites_its', 'altres')
sql = """
    SELECT cuenta, periode, br, kcli, edat_k, kdtl, sex_k, kval, valor
    FROM altres.simulacio_visites_its a
    INNER JOIN nodrizas.cat_centres b
    ON a.br = b.ics_codi
"""
u.exportKhalix(sql, 'VISITESITS')

sql = """
    SELECT cuenta, periode, a.br, kcli, edat_k, kdtl, sex_k, kval, sum(valor)
    FROM altres.simulacio_visites_its a
    INNER JOIN nodrizas.ass_centres b
    ON a.br = b.br_assir
    group by cuenta, periode, a.br, kcli, edat_k, kdtl, sex_k, kval
"""
u.exportKhalix(sql, 'VISITESITS_ASSIR')

sql = """
    SELECT cuenta, periode, br, kcli, edat_k, kdtl, sex_k, kval, valor
    FROM altres.simulacio_visites_its a
    INNER JOIN permanent.centres_acut b
    ON a.br = concat(concat(b.codi_centre, '_'), b.classe_centre)
"""
u.exportKhalix(sql, 'VISITESITS_CUAP')
u.printTime('taula 1 its')
upload = []
for k, v in cuentas_anuals.items():
    upload.append([k[0], k[1], k[2], KCLI, k[3], k[5], k[4], KVAL, v])
u.listToTable(upload, 'simulacio_visites_its_anual', 'altres')

sql = """
    SELECT cuenta, periode, br, kcli, edat_k, kdtl, sex_k, kval, valor
    FROM altres.simulacio_visites_its_anual a
    INNER JOIN nodrizas.cat_centres b
    ON a.br = b.ics_codi
"""
u.exportKhalix(sql, 'VISITESITS_ANUAL')

sql = """
    SELECT cuenta, periode, a.br, kcli, edat_k, kdtl, sex_k, kval, sum(valor)
    FROM altres.simulacio_visites_its_anual a
    INNER JOIN nodrizas.ass_centres b
    ON a.br = b.br_assir
    group by cuenta, periode, a.br, kcli, edat_k, kdtl, sex_k, kval
"""
u.exportKhalix(sql, 'VISITESITS_ANUAL_ASSIR')

sql = """
    SELECT cuenta, periode, br, kcli, edat_k, kdtl, sex_k, kval, valor
    FROM altres.simulacio_visites_its_anual a
    INNER JOIN permanent.centres_acut b
    ON a.br = concat(concat(b.codi_centre, '_'), b.classe_centre)
"""
u.exportKhalix(sql, 'VISITESITS_ANUAL_CUAP')
u.printTime('upload anual')
u.printTime('fi')