import sisapUtils as u
import sisaptools as t
from collections import defaultdict
import os

DEXTD = t.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]  # noqa
ym = DEXTD.strftime("%y%m")
m_Y = DEXTD.strftime("%m_%Y")
SEP = "{"

ACCOUNT = "POBRCA"
PERIOD = "A{ym}".format(ym=ym)
NOCLI = "NOCLI"
NOIMP = "NOIMP"

eap_filename = "POBRCA_EAP_{m_Y}.txt".format(m_Y=m_Y)
eap_file = u.tempFolder + eap_filename

aga_filename = "POBRCA_AGA_{m_Y}.txt".format(m_Y=m_Y)
aga_file = u.tempFolder + aga_filename

up_to_br = {
    up: br for (up, br) in
    u.getAll(
        """
        SELECT scs_codi, ics_codi
        FROM cat_centres
        """,
        "nodrizas")
    }

up_ecap = set(
    [row for row, in
     u.getAll("SELECT up_cod FROM dbc_centres WHERE es_ecap = 1",
              "exadata")
     ])

up_to_abs = {
    up: abs for (up, abs) in
    u.getAll(
        """
        SELECT
            abs_codi_up, abs_codi_abs
        FROM
            cat_rittb001
        WHERE
            abs_data_baixa = '47120101' AND
            abs_nom_abs <> 'INEXISTENT'
        """,
        "import")
    }

abs_to_aga = {
    int(abs): "AGA{}".format(str(aga).zfill(5))
    for (abs, aga) in
    u.getAll(
        """
        SELECT abs_cod, aga_cod
        FROM dbc_centres
        """,
        "exadata")
    }

pob_eap = defaultdict(int)
pob_aga = defaultdict(int)
pob_falta = defaultdict(int)

query = """
    SELECT
        up_assignada AS up,
        trunc(MONTHS_BETWEEN(DATE '{DEXTD}', ass_dna)/12) AS edat,
        CASE ass_sexe*1
            WHEN 1 THEN 'DONA'
            WHEN 0 THEN 'HOME'
            ELSE '9' END AS sexe_d,
        count(*) AS n
    FROM
        DWCATSALUT.RCA_PACIENTS
    WHERE
        up_assignada IS NOT null AND
        ass_csa = 'A'
    GROUP BY
        up_assignada,
        trunc(MONTHS_BETWEEN(DATE '{DEXTD}', ass_dna)/12),
        CASE ass_sexe*1
            WHEN 1 THEN 'DONA'
            WHEN 0 THEN 'HOME'
            ELSE '9' END
    """.format(DEXTD=DEXTD)

# si es vol recuperar tall oficial (s'ha de canviar dext a desembre):
"""
query = 
    SELECT
        c_up_ass_basica_assignada AS up,
        c_edat,
        CASE c_genere*1
            WHEN 1 THEN 'DONA'
            WHEN 0 THEN 'HOME'
            ELSE '9' END AS sexe_d,
        count(*) AS n
    FROM
        DWCATSALUT.RCA_pob_oficial
    WHERE
        c_any_assegurat = 2021 and
        c_up_ass_basica_assignada IS NOT null
    GROUP BY
        c_up_ass_basica_assignada,
        c_edat,
        CASE c_genere*1
            WHEN 1 THEN 'DONA'
            WHEN 0 THEN 'HOME'
            ELSE '9' END
"""

for up, edat, sexe_k, n in u.getAll(query, "exadata"):
    if sexe_k in ("HOME", "DONA") and isinstance(edat, int) and up in up_ecap:
        edat_k = u.ageConverter(edat, 1)
        if up in up_to_br:
            br = up_to_br[up]
            key = (ACCOUNT, PERIOD, br, NOCLI, edat_k, NOIMP, sexe_k, "N")
            pob_eap[key] += n
        if up in up_to_abs:
            abs_ = up_to_abs[up]
            aga = abs_to_aga[abs_]
            key = (ACCOUNT, PERIOD, aga, NOCLI, edat_k, NOIMP, sexe_k, "N")
            pob_aga[key] += n
        else:
            pob_falta[up] += n

name_dades = {
    eap_file: pob_eap,
    aga_file: pob_aga,
    }

for name, dades in name_dades.items():
    u.writeCSV(name, [k + (n,) for (k, n) in dades.items()], sep=SEP)

# u.writeCSV(aga_file,
#            [k + (n,) for (k, n) in pob_aga.items()],
#            sep=SEP
#            )

print("UP que falten (n): {}".format(len(pob_falta)))

for filename in (eap_filename, aga_filename):
    print(filename)
    try:
        u.sshPutFile(file=filename, folder="cargas", host="khalix")
    except Exception as e:
        os.rename(u.tempFolder + filename, u.rescueFolder + filename)
        print("EXCEPTION: {}, {}, {}".format(filename,
                                             u.tempFolder,
                                             u.rescueFolder)
              )
        print(str(e))
