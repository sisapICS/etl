# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
import dateutil.relativedelta as r

import sisapUtils as u


tb_br = "exp_khalix_up_econsulta"
tb_uba = "exp_khalix_uba_econsulta"
tb_qc = "exp_qc_econsulta"
db = "altres"

br_linies_pediatriques = ("BN016", "BN017", "BN033", "BN034", "BN036", "BN007", "BN010", "BN003", "BN015", "BN002", "BN001", "BN027", "BR375", "BN008", "BN011", "BN019", "BN018", "B7703", "B7757")
br_linies_pediatriques += ("BN037", "BN035", "BN039", "BN038", "BN026", "BN028")

class EConsulta(object):
    """."""

    def __init__(self):
        """."""

        self.get_centres();                                 print("get_centres(): executat")
        self.get_ubas();                                    print("get_ubas(): executat")
        self.get_professionals();                           print("get_professionals(): executat")
        self.get_poblacio();                                print("get_poblacio(): executat")
        self.get_dni_2_index_br_i_uba_i_ubainf();           print("get_dni_2_index_br_i_uba_i_ubainf(): executat")
        self.get_converses();                               print("get_converses(): executat")
        self.get_missatges();                               print("get_missatges(): executat")

        self.resultat = []

        self.get_ECONS0000_0002();                          print("get_ECONS0000_0002(): executat")
        self.get_ECONS0001();                               print("get_ECONS0001(): executat")
        self.get_ECONS0003();                               print("get_ECONS0003(): executat")
        self.get_ECONS0004();                               print("get_ECONS0004(): executat")
        self.get_ECONS0005();                               print("get_ECONS0005(): executat")
        self.get_ECONS0006_0007_0008();                     print("get_ECONS0006_0007_0008(): executat")

        self.export_br();                                   print("export_br(): executat")
        self.export_uba();                                  print("export_uba(): executat")
        self.export_qc();                                   print("export_qc(): executat")


    def get_centres(self):
        """."""

        self.centres, self.centres_liniespediatriques = dict(), dict()

        sql = """
                SELECT
                    scs_codi,
                    ics_codi
                FROM
                    cat_centres
              """
        for (up, br) in u.getAll(sql, "nodrizas"):
            if br in br_linies_pediatriques:
                self.centres_liniespediatriques[up] = br
            else:
                self.centres[up] = br

        # self.centres --> {'00002': 'BR001'}


    def get_ubas(self):
        """."""

        self.ubas, self.ubas_liniespediatriques = c.defaultdict(set), c.defaultdict(set)

        sql = """
                SELECT
                    up,
                    tipus,
                    uba
                FROM
                    mst_ubas
              """
        for up, tipus, uba in u.getAll(sql, 'eqa_ind'):
            if up in self.centres:
                self.ubas[(self.centres[up], tipus)].add(uba)
        for up, tipus, uba in u.getAll(sql, 'pedia'):
            if up in self.centres_liniespediatriques:
                self.ubas_liniespediatriques[(self.centres_liniespediatriques[up], tipus)].add(uba)


    def get_professionals(self):
        """."""

        self.professionals = c.defaultdict(set)
        self.professionals_liniespediatriques = c.defaultdict(set)
        self.dnis_2_uba = c.defaultdict(set)

        sql = """
                SELECT
                    lloc_codi_up,
                    lloc_codi_lloc_de_treball,
                    lloc_proveidor_dni_proveidor
                FROM
                    cat_pritb025
              """
        llocs_de_treball = {(self.centres[up], lloc): dni for (up, lloc, dni) in u.getAll(sql, 'import') if up in self.centres}
        llocs_de_treball_liniespediatriques = {(self.centres_liniespediatriques[up], lloc): dni for (up, lloc, dni) in u.getAll(sql, 'import') if up in self.centres_liniespediatriques}

        sqls_2 = (
                    """
                        SELECT
                            uab_codi_up,
                            'M',
                            uab_codi_uab,
                            uab_lloc_de_tr_codi_lloc_de_tr
                        FROM
                            cat_vistb039
                    """
                    ,
                    """
                        SELECT
                            uni_codi_up,
                            'I',
                            uni_codi_unitat,
                            uni_ambit_treball
                        FROM
                            cat_vistb059
                    """
                 )
        for sql_2 in sqls_2:
            for up, tipus, uba, lloc in u.getAll(sql_2, 'import'):
                if up in self.centres:
                    br = self.centres[up]
                    dni = llocs_de_treball.get((br, lloc))
                    if dni and (uba in self.ubas[(br, tipus)]):
                        self.professionals[(br, tipus)].add(dni)
                        self.dnis_2_uba[(dni, br)].add(uba)
                elif up in self.centres_liniespediatriques:
                    br = self.centres_liniespediatriques[up]
                    dni = llocs_de_treball_liniespediatriques.get((br, lloc))
                    if dni and (uba in self.ubas_liniespediatriques[(br, tipus)]):
                        self.professionals_liniespediatriques[(br, tipus)].add(dni)

        # self.professionals --> {('B0005', 'I'): set('37736268F', '40881819D', '45720594K', '46693252D', '47805771H', '74641649W', '78075692F'), ...}
        # self.ubas --> {('B0005', 'I'): set('INF01', 'INF02', 'INF03', 'INF04', 'INF05', 'INF06', 'INF07')}
    

    def get_poblacio(self):
        """
        entitats: (br, br + 'M' + uba, br + 'I' + ubainf
        """

        self.poblacio, self.poblacio_liniespediatriques, self.counter_poblacio_entitats, self.counter_poblacio_qc = dict(), dict(), c.Counter(), c.Counter()

        sql = """
                SELECT
                    id_cip_sec,
                    up,
                    uba,
                    ubainf,
                    edat
                FROM
                    assignada_tot
                WHERE
                    ates = 1
                    AND institucionalitzat = 0
              """
        for id_cip_sec, up, uba, ubainf, edat in u.getAll(sql, "nodrizas"):
            if up in self.centres:
                br = self.centres[up]
                if uba in self.ubas[(br, "M")] or ubainf in self.ubas[(br, "I")]:
                    es_adult = edat > 14
                    tipprof = ["TIPPROF", "TIPPROF4", "TIPPROF1" if es_adult else "TIPPROF2"]
                    for tip in tipprof:
                        self.counter_poblacio_qc[(br, tip)] += 1

                    entitats = (br, "".join((br, "M", uba)), "".join((br, "I", ubainf)))
                    for entitat in entitats:
                        self.counter_poblacio_entitats[entitat] += 1
                    
                    self.poblacio[id_cip_sec] = {"es_adult": es_adult, "entitats": entitats}

            if up in self.centres_liniespediatriques:
                br = self.centres_liniespediatriques[up]
                if uba in self.ubas_liniespediatriques[(br, "M")] or ubainf in self.ubas_liniespediatriques[(br, "I")]:
                    es_adult = edat > 14
                    entitats = (br, "".join((br, "M", uba)), "".join((br, "I", ubainf)))
                    self.poblacio_liniespediatriques[id_cip_sec] = {"es_adult": es_adult, "entitats": entitats}

        # self.poblacio --> {10567937L: {'es_adult': True, 'entitats': ('BR096', 'BR096MD', 'BR096IINF-D')}}
        # self.counter_poblacio_entitats --> Counter({'B0024': 16, 'B0024IIP1': 6, ...})
        # self.counter_poblacio_qc --> Counter({('B0383', 'TIPPROF'): 20, ('B0382', 'TIPPROF4'): 18, ...})
         

    def get_dni_2_index_br_i_uba_i_ubainf(self):
        """
            Index de poblacio[id_cip_sec]["entitats"]:    0 (BR)
                                                          1 (Metge)
                                                          2 (Infermeria)
            Exemple: 
                poblacio[id_cip_sec]["entitats"] = ('BR021', 'BR021MT2', 'BR021IIT2')                                                          
        """

        sql = """
                SELECT
                    codi_sector,
                    prov_dni_proveidor,
                    prov_categoria
                FROM
                    cat_pritb031
              """
        self.dni_2_index_br_i_uba_i_ubainf = {(sector, dni): (1 if categoria[0] == "1" else 2) for sector, dni, categoria in u.getAll(sql, "import")}

        # self.dni_2_index_br_i_uba_i_ubainf -->  {('6626', '43416206A'): 4, ('6519', '37312842'): 12, ...}


    def get_converses(self):
        """."""

        self.converses, self.converses_liniespediatriques = dict(), dict()

        sql = """
                SELECT
                    id_cip_sec,
                    codi_sector,
                    conv_id,
                    conv_autor,
                    conv_desti,
                    conv_estat,
                    conv_dini,
                    conv_dfi,
                    CASE WHEN conv_dini > adddate(data_ext, INTERVAL -1 MONTH) THEN 1 ELSE 0 END mensual,
                    b.data_ext
                FROM
                    econsulta_conv,
                    nodrizas.dextraccio b
                WHERE
                    conv_dini BETWEEN adddate(adddate(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY) AND data_ext
              """
        for id_cip_sec, sector, conv_id, conv_autor, conv_desti, conv_estat, conv_data_ini, conv_data_fi, mensual, data_ext in u.getAll(sql, "import"):  
            if id_cip_sec in self.poblacio:
                professional = (conv_autor if conv_autor else conv_desti)
                index_especialitat = self.dni_2_index_br_i_uba_i_ubainf.get((sector, professional))
                if index_especialitat:
                    entitats = self.poblacio[id_cip_sec]["entitats"]
                    tipprof = 4 if index_especialitat == 1 else 1 if self.poblacio[id_cip_sec]["es_adult"] else 2
                    self.converses[(sector, conv_id)] = {"id_cip_sec": id_cip_sec,
                                                         "br": entitats[0],
                                                         "entitats": entitats,
                                                         "professional": professional,
                                                         "iniciada_per_professional": not conv_desti,
                                                         "estat": conv_estat,
                                                         "data_ini": conv_data_ini,
                                                         "data_fi": conv_data_fi if conv_data_fi else data_ext,
                                                         "tipprof": "TIPPROF{}".format(tipprof),
                                                         "missatges": {},
                                                         "mensual": mensual}
            if id_cip_sec in self.poblacio_liniespediatriques:
                professional = (conv_autor if conv_autor else conv_desti)
                index_especialitat = self.dni_2_index_br_i_uba_i_ubainf.get((sector, professional))
                if index_especialitat:
                    entitats = [self.poblacio_liniespediatriques[id_cip_sec]["entitats"][i] for i in (0, index_especialitat)]
                    tipprof = 4 if index_especialitat == 1 else 1 if self.poblacio_liniespediatriques[id_cip_sec]["es_adult"] else 2
                    self.converses_liniespediatriques[(sector, conv_id)] = {"id_cip_sec": id_cip_sec,
                                                            "br": entitats[0],
                                                            "entitats": entitats,
                                                            "professional": professional,
                                                            "iniciada_per_professional": not conv_desti,
                                                            "estat": conv_estat,
                                                            "data_ini": conv_data_ini,
                                                            "data_fi": conv_data_fi if conv_data_fi else data_ext,
                                                            "tipprof": "TIPPROF{}".format(tipprof),
                                                            "missatges": {},
                                                            "mensual": mensual}

        # self.converses --> {('6102', '255849'): {'br': 'BR138', 'data_ini': datetime.date(2021, 5, 12), 'entitats': ['BR138', 'BR138M510'], 
        #                                          'estat': 'T', 'id_cip_sec': 259509L, 'iniciada_per_professional': False,
        #                                          'missatges': {}, 'professional': '43701309K', 'tipprof': 'TIPPROF4'}, ...}


    def get_missatges(self):
        """."""

        sql = """
                SELECT
                    codi_sector,
                    msg_conv_id,
                    msg_id,
                    msg_autor,
                    msg_desti,
                    msg_data,
                    prof_tanca
                FROM
                    econsulta_msg
              """
        for sector, conv_id, msg_id, msg_autor, msg_desti, msg_data, prof_tanca in u.getAll(sql, "import"):
            if (sector, conv_id) in self.converses:
                self.converses[(sector, conv_id)]["missatges"][msg_id] = {"professional": msg_autor if msg_autor else msg_desti,
                                                                          "enviat_per_professional": not msg_desti,
                                                                          "msg_data": msg_data,
                                                                          "prof_tanca": prof_tanca}
            if (sector, conv_id) in self.converses_liniespediatriques:
                self.converses_liniespediatriques[(sector, conv_id)]["missatges"][msg_id] = {"professional": msg_autor if msg_autor else msg_desti,
                                                                             "enviat_per_professional": not msg_desti,
                                                                             "msg_data": msg_data,
                                                                             "prof_tanca": prof_tanca}

        # self.converses --> {('6102', '255849'): {'br': 'BR138', 'data_ini': datetime.date(2021, 5, 12), 'entitats': ['BR138', 'BR138M510'], 
        #                                          'estat': 'T', 'id_cip_sec': 259509L, 'iniciada_per_professional': False,
        #                                          'missatges': {1L: {'enviat_per_professional': False, 'msg_data': datetime.date(2021, 5, 12), 'professional': '43701309K'},
        #                                                        2L: {'enviat_per_professional': True, 'msg_data': datetime.date(2021, 5, 17),'professional': '43701309K'}}, 
        #                                          'professional': '43701309K', 'tipprof': 'TIPPROF4'}, ...}                                                                          


    def get_ECONS0000_0002(self):
        """ 
        ECONS0000 - % de pacients que usen la econsulta (12 mesos)
            - NUM: Pacients que tenen almenys una conversa realitzada (tancada) en els últims 12 mesos. **
            - DEN: Població atesa assignada major de 18 anys.
        
        ECONS0002 - % de pacients que han usat la econsulta més d'una vegada (12 mesos)
            - NUM: Pacients que han realitizat més d'1 conversa (≥2 converses tancades) durant els últims 12 mesos. **
            - DEN: Població atesa assignada major de 18 anys.

           ** (S'inclouen TOTES les converses sigui quina sigui la categoria professional de qui la inicia o respon.)
        """

        num = {"ECONS0000": c.defaultdict(c.Counter), "ECONS0002": c.defaultdict(c.Counter)}
        pacients = c.defaultdict(c.Counter)
        qc = c.defaultdict(set)
        self.qc = list()

        for conversa in self.converses.values():
            if conversa["estat"] == "T":
                id_cip_sec = conversa["id_cip_sec"]
                for entitat in conversa["entitats"]:
                    pacients["ANUAL"][(entitat, id_cip_sec)] += 1
                    if conversa["mensual"]:
                        pacients["MENSUAL"][(entitat, id_cip_sec)] += 1
                periode = "A{}".format(conversa["data_ini"].strftime("%y%m"))
                for tipprof in ("TIPPROF", conversa["tipprof"]):
                    qc[(conversa["br"], tipprof, "ANUAL")].add(id_cip_sec)
                    qc[(conversa["br"], tipprof, periode)].add(id_cip_sec)
        for periode in pacients:
            for (entitat, id_cip_sec), n in pacients[periode].items():
                num["ECONS0000"][periode][entitat] += 1
                if n > 1:
                    num["ECONS0002"][periode][entitat] += 1
        for entitat, n in self.counter_poblacio_entitats.items():
            for ind in num:
                for periode in ("MENSUAL", "ANUAL"):
                    self.resultat.append((ind, entitat, "DEN", periode, n))
                    self.resultat.append((ind, entitat, "NUM", periode, num[ind][periode][entitat]))

        data_ext, = u.getOne("SELECT data_ext FROM dextraccio", "nodrizas")
        data_ext_menys_1mes = data_ext - r.relativedelta(months=1)
        periode_actual = "A{}".format(data_ext.strftime("%y%m"))
        periode_previ = "A{}".format(data_ext_menys_1mes.strftime("%y%m"))
        tipus = {"ANUAL": ("ANUAL", periode_actual),
                 periode_actual: ("ACTUAL", periode_actual),
                 periode_previ: ("PREVI", periode_previ)}

        for (br, tipprof), n in self.counter_poblacio_qc.items():
            for key, val in tipus.items():
                this = ("QECONS0000", val[1], br, "DEN", val[0], tipprof, n)
                self.qc.append(this)
                num = len(qc[(br, tipprof, key)])
                if num:
                    that = ("QECONS0000", val[1], br, "NUM", val[0], tipprof, num)
                    self.qc.append(that)


    def get_ECONS0001(self):
        """ 
        ECONS0001 - % de pacients que usen la econsulta (12 mesos)
            - NUM: Número de professionals que han realitzat almenys 1 conversa durant en els últims 12 mesos. 
                   Aquesta conversa pot ser iniciada tant pel professional com pel pacient. 
                   Les converses han d’estar tancades. No poden estar pendents de resposta ni obertes.
            - DEN: Número de professionals (Metges de família i Infermeres de família **) amb pacients assignats (que tenen UBA) dels EAP.

           ** Es tenen les categories diferenciades per les dues categories professionals.
        """
        
        ind = 'ECONS0001'
        professionals_amb_alguna_econsulta_tancada_periode = c.defaultdict(lambda: c.defaultdict(set))

        # Cerquem el nombre de professionals que han realitzat almenys 1 conversa durant en els últims 12 mesos, per BR.
        for conversa in self.converses.values():
            if conversa['estat'] == 'T':
                if (conversa['professional'], conversa['br']) in self.dnis_2_uba:
                    ubas_professional = self.dnis_2_uba[(conversa['professional'], conversa['br'])]
                    for uba_professional in ubas_professional:
                        professionals_amb_alguna_econsulta_tancada_periode["ANUAL"][conversa['br']].add(uba_professional)
                        if conversa['mensual']:
                            professionals_amb_alguna_econsulta_tancada_periode["MENSUAL"][conversa['br']].add(uba_professional)
        for (br, categoria_professional), ubas in self.ubas.items():
            den = len(ubas)
            for periode in ("MENSUAL", "ANUAL"):
                num = len([uba for uba in professionals_amb_alguna_econsulta_tancada_periode[periode][br] if uba in ubas])
                self.resultat.append((ind + categoria_professional, br, 'DEN', periode, den))
                self.resultat.append((ind + categoria_professional, br, 'NUM', periode, num))


    def get_ECONS0003(self):
        """ 
        ECONS0003 - Número de converses per cada 1.000 pacients atesos (12 mesos)
            - NUM: Número de converses tancades realitzades en els últims 12 mesos en els pacients inclosos en el denominador. 
                   Inclou les econsultes de TOTES les categories professionals.
            - DEN: Població atesa assignada major de 18 anys.

           ** Es tenen les categories diferenciades per les dues categories professionals.
        """

        ind = "ECONS0003"
        num = c.defaultdict(c.Counter)
        for conversa in self.converses.values():
            if conversa["estat"] == "T":
                for entitat in conversa["entitats"]:
                    num["ANUAL"][entitat] += 1
                    if conversa["mensual"]:
                        num["MENSUAL"][entitat] += 1
        for entitat, n in self.counter_poblacio_entitats.items():
            for periode in ("MENSUAL", "ANUAL"):
                self.resultat.append((ind, entitat, "DEN", periode, n))
                self.resultat.append((ind, entitat, "NUM", periode, num[periode][entitat]))


    def get_ECONS0004(self):
        """ 
        ECONS0004 - Número de missatges per conversa tancada (12 mesos)
            - NUM: Número de missatges de les converses incloses en el denominador dels últims 12 mesos.
            - DEN: Número de converses tancades de població atesa assignada a un professional majors d'edat.
        """

        ind = "ECONS0004"
        den = c.defaultdict(c.Counter)
        num = c.defaultdict(c.Counter)
        for conversa in self.converses.values():
            if conversa["estat"] == "T":
                for entitat in conversa["entitats"]:
                    den["ANUAL"][entitat] += 1
                    num["ANUAL"][entitat] += len(conversa["missatges"])
                    if conversa["mensual"]:
                        den["MENSUAL"][entitat] += 1
                        num["MENSUAL"][entitat] += len(conversa["missatges"])
        for periode in ("MENSUAL", "ANUAL"):
            for entitat, n in den[periode].items():
                self.resultat.append((ind, entitat, "DEN", periode, n))
                self.resultat.append((ind, entitat, "NUM", periode, num[periode][entitat]))


    def get_ECONS0005(self):
        """ 
        ECONS0005 - % de converses iniciades pel professional (12 mesos)
            - NUM: De les converses del denominador, les que hagi iniciat el propi professional, durant els últims 12 mesos.
            - DEN: Total de converses ja tancades, iniciades o rebudes pel professional dels població assignada a ell.
        """

        ind = "ECONS0005"
        den = c.defaultdict(c.Counter)
        num = c.defaultdict(c.Counter)
        for conversa in self.converses.values():
            if conversa["estat"] == "T":
                for entitat in conversa["entitats"]:
                    den["ANUAL"][entitat] += 1
                    num["ANUAL"][entitat] += conversa["iniciada_per_professional"]
                    if conversa["mensual"]:
                        den["MENSUAL"][entitat] += 1
                        num["MENSUAL"][entitat] += conversa["iniciada_per_professional"]
        for periode in ("MENSUAL", "ANUAL"):
            for entitat, n in den[periode].items():
                self.resultat.append((ind, entitat, "DEN", periode, n))
                self.resultat.append((ind, entitat, "NUM", periode, num[periode][entitat]))


    def get_ECONS0006_0007_0008(self):
        """ 
        ECONS0006 - % de converses contestades en menys de 24 hores (12 mesos)
            - NUM: Número de converses dels últims 12 mesos del denominador en què el primer missatge (el del pacient) s'ha respost pel professional en menys de 24 hores laborables.
            - DEN: Número de converses ja tancades, iniciades pels pacients d'un professional.
        
        ECONS0007 - % de converses contestades en menys de 48 hores (12 mesos)
            - NUM: Número de converses dels últims 12 mesos del denominador en què el primer missatge (el del pacient) s'ha respost pel professional en menys de 48 hores laborables.
            - DEN: Número de converses ja tancades, iniciades pels pacients d'un professional en els últims 12 mesos

        ECONS0008 - Mitjana de dies que triga el professional en respondre el primer missatge dels seus pacients (12 m)
            - NUM: Número de dies laborables que passen des del primer missatge de les converses del denominador, fins a la resposta del professional (el següent missatge del professional). Dels últims 12 mesos.
            - DEN: Número de converses tancades iniciades per una població assignada al professional i dirigides al professional.
        """

        ind = ("ECONS0006", "ECONS0007", "ECONS0008")
        den = c.defaultdict(c.Counter)
        num = {"ECONS0006": c.defaultdict(c.Counter), "ECONS0007": c.defaultdict(c.Counter), "ECONS0008": c.defaultdict(c.Counter)}

        for conversa in self.converses.values():
            if conversa["estat"] == "T" and not conversa["iniciada_per_professional"] and 1 in conversa["missatges"]:
                for entitat in conversa["entitats"]:
                    if conversa["mensual"]:
                        den["MENSUAL"][entitat] += 1
                    pregunta = conversa["missatges"][1]["msg_data"]
                    resposta = None
                    prof_tanca = False
                    for msg_id in range(2, 1000):
                        if msg_id in conversa["missatges"]:
                            if conversa["missatges"][msg_id]["enviat_per_professional"]:
                                resposta = conversa["missatges"][msg_id]["msg_data"]
                                prof_tanca = conversa["missatges"][msg_id]["prof_tanca"]
                                break
                        else:
                            break
                    den["ANUAL"][entitat] += 1
                    if resposta:
                        dies = self.conta_dies(pregunta, resposta)
                        num["ECONS0006"]["ANUAL"][entitat] += dies in (0, 1)
                        num["ECONS0007"]["ANUAL"][entitat] += dies in (0, 1, 2)
                        num["ECONS0008"]["ANUAL"][entitat] += 0 if prof_tanca else dies
                        if conversa["mensual"]:
                            num["ECONS0006"]["MENSUAL"][entitat] += dies in (0, 1)
                            num["ECONS0007"]["MENSUAL"][entitat] += dies in (0, 1, 2)
                            num["ECONS0008"]["MENSUAL"][entitat] += 0 if prof_tanca else dies
                    else:
                        tancament = conversa["data_fi"]
                        dies = self.conta_dies(pregunta, tancament)
                        num["ECONS0008"]["ANUAL"][entitat] += dies
                        if conversa["mensual"]:
                            num["ECONS0008"]["MENSUAL"][entitat] += dies                            
        for periode in ("MENSUAL", "ANUAL"):
            for entitat, n in den[periode].items():
                for i in ind:
                    self.resultat.append((i, entitat, "DEN", periode, n))
                    self.resultat.append((i, entitat, "NUM", periode, num[i][periode][entitat]))


        ind_liniespediatriques = ("ECONS0008",)
        den_liniespediatriques = c.defaultdict(c.Counter)
        num_liniespediatriques = {"ECONS0008": c.defaultdict(c.Counter)}

        for conversa in self.converses_liniespediatriques.values():
            if conversa["estat"] == "T" and not conversa["iniciada_per_professional"] and 1 in conversa["missatges"]:
                for entitat in conversa["entitats"]:
                    if conversa["mensual"]:
                        den_liniespediatriques["MENSUAL"][entitat] += 1
                    pregunta = conversa["missatges"][1]["msg_data"]
                    resposta = None
                    prof_tanca = False
                    for msg_id in range(2, 1000):
                        if msg_id in conversa["missatges"]:
                            if conversa["missatges"][msg_id]["enviat_per_professional"]:
                                resposta = conversa["missatges"][msg_id]["msg_data"]
                                prof_tanca = conversa["missatges"][msg_id]["prof_tanca"]
                                break
                        else:
                            break
                    den_liniespediatriques["ANUAL"][entitat] += 1
                    if resposta:
                        dies = self.conta_dies(pregunta, resposta)
                        num_liniespediatriques["ECONS0008"]["ANUAL"][entitat] += 0 if prof_tanca else dies
                        if conversa["mensual"]:
                            num_liniespediatriques["ECONS0008"]["MENSUAL"][entitat] += 0 if prof_tanca else dies
                    else:
                        tancament = conversa["data_fi"]
                        dies = self.conta_dies(pregunta, tancament)
                        num_liniespediatriques["ECONS0008"]["ANUAL"][entitat] += dies
                        if conversa["mensual"]:
                            num_liniespediatriques["ECONS0008"]["MENSUAL"][entitat] += dies                        
        for periode in ("MENSUAL", "ANUAL"):
            for entitat, n in den_liniespediatriques[periode].items():
                for i in ind_liniespediatriques:
                    self.resultat.append((i, entitat, "DEN", periode, n))
                    self.resultat.append((i, entitat, "NUM", periode, num_liniespediatriques[i][periode][entitat]))


    @staticmethod
    def conta_dies(inici, final):
        """."""
        total = (final - inici).days
        laborables = 0
        for i in range(total):
            dia = inici + d.timedelta(days=i)
            if u.isWorkingDay(dia):
                laborables += 1
        if laborables > 30:
            laborables = 30
        return laborables


    def export_br(self):
        """."""

        u.createTable(tb_br, "(indicador varchar(12), br varchar(5), analisis varchar(10), periode varchar(7), n int)", db, rm=True)
        u.listToTable([row for row in self.resultat if len(row[1]) == 5], tb_br, db)

        sql = """
                SELECT
                    indicador,
                    'Aperiodo',
                    br,
                    analisis,
                    CASE WHEN periode = 'ANUAL' THEN 'NOCAT' ELSE periode END 'NOCAT',
                    'NOINSAT',
                    'DIM6SET',
                    'N',
                    n
                FROM
                    {}.{}
                WHERE
                    indicador NOT LIKE '%AG%'
              """.format(db, tb_br)
        file = "ECONSULTA"
        u.exportKhalix(sql, file)


    def export_uba(self):
        """."""

        u.createTable(tb_uba, "(indicador varchar(12), ent varchar(11), analisis varchar(10), periode varchar(7), n int)", db, rm=True)
        u.listToTable([row for row in self.resultat if len(row[1]) > 5], tb_uba, db)

        sql = """
                SELECT
                    indicador,
                    'Aperiodo',
                    ent,
                    analisis,
                    CASE WHEN periode = 'ANUAL' THEN 'NOCAT' ELSE periode END 'NOCAT',
                    'NOINSAT',
                    'DIM6SET',
                    'N',
                    n
                FROM
                    {}.{}
                WHERE
                    indicador NOT LIKE '%AG%'
              """.format(db, tb_uba)
        file = "ECONSULTA_UBA"
        u.exportKhalix(sql, file)


    def export_qc(self):
        """."""
        cols = ["k{} varchar(10)".format(i) for i in range(6)] + ["v int"]
        u.createTable(tb_qc, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.qc, tb_qc, db)


if __name__ == "__main__":
    EConsulta()
