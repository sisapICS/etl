import collections as c
import multiprocessing as m

import sisaptools as u
import sisapUtils as u2


DEBUG = False
MASTER = "SISAP_POLIFARMACIA_PACIENT"
BACKUP = MASTER + "_BACKUP"
LV_TABLE = "EXP_LV_POLIFARMACIA"
CATSALUT = "SISAP_POLIFARMACIA_CATSALUT"
if DEBUG:
    MASTER += "_FMO"
    LV_TABLE += "_FMO"


def get_data(sql):
    """."""
    jobs = [sql.format(i) for i in range(16)]
    pool = m.Pool(16)
    dades = pool.map(worker, jobs, chunksize=1)
    pool.close()
    for chunk in dades:
        for row in chunk:
            yield row


def worker(sql):
    """."""
    return (list(u.Database("p2262", "nodrizas").get_all(sql)))


class Polifarmacia(object):
    """."""

    def __init__(self):
        """."""
        self.get_pocs()
        self.get_poblacio()
        self.get_estatines()
        self.get_indicadors()
        self.get_u11()
        self.get_exclosos()
        self.get_neos()
        self.create_table()
        self.execute_sector()
        self.export_lv()
        if u2.IS_MENSUAL:
            self.save_backup()
        for table in (MASTER, BACKUP):
            self.copy_exadata(table)
        self.get_catsalut_pacient()

    def get_pocs(self):
        """."""
        sql = "select id_cip_sec, pocs from pocs"
        self.pocs = {row[0]: row[1] for row
                     in u.Database("p2262", "nodrizas").get_all(sql)}

    def get_poblacio(self):
        """."""
        sql = "select codi_sector, id_cip_sec, up, uba, ubainf, edat, \
                      institucionalitzat, atdom, pcc, maca \
               from eqa_ind.assignada_tot partition(p{}) \
               where codi_sector != '6951'"
        self.poblacio = {}
        self.invers = {}
        self.poblacio_all = {}
        for row in get_data(sql):
            if row[0] not in self.poblacio:
                self.poblacio[row[0]] = {}
            pocs = self.pocs.get(row[1], 0)
            if row[6] or row[7] or pocs > 1:
                self.poblacio[row[0]][row[1]] = row[2:] + (pocs if pocs else 3,)  # noqa
                self.invers[row[1]] = row[0]
            self.poblacio_all[row[1]] = row[2:] + (3 if row[6] else pocs,)

    def get_estatines(self):
        """."""
        self.estatines = {key: {"num": set(), "den": set()}
                          for key in self.poblacio}
        sqls = [("den",
                 "select id_cip_sec from prt_tractaments partition(p{}) \
                  where farmac = 82"),
                ("num",
                 "select id_cip_sec from prt_problemes partition(p{}) \
                  where ps in (1, 7, 11, 439, 1077)")]
        for concepte, sql in sqls:
            for id, in get_data(sql):
                if id in self.invers:
                    self.estatines[self.invers[id]][concepte].add(id)

    def get_indicadors(self):
        """."""
        sqls = {"eqa0222":
                "select id_cip_sec, num \
                 from eqa_ind.mst_indicadors_pacient partition(eqa0222a) \
                 where clin + ci + excl = 0",
                "farm0008": "select id_cip_sec, alerta \
                             from altres.mst_alertes_pacient \
                             where indicador = 'FARM0008'"}
        self.indicadors = {key: {cod: {} for cod in sqls}
                           for key in self.poblacio}
        for cod, sql in sqls.items():
            for id, num in u.Database("p2262", "eqa_ind").get_all(sql):
                if id in self.invers:
                    self.indicadors[self.invers[id]][cod][id] = num

    def get_u11(self):
        """."""
        sql = "select id_cip_sec, hash_d from prt_u11 partition(p{})"
        self.u11 = {key: {} for key in self.poblacio}
        for id, hash in get_data(sql):
            if id in self.invers:
                self.u11[self.invers[id]][id] = hash

    def get_exclosos(self):
        """."""
        sql = "select pf_codi from cat_cpftb006 \
               where pf_ff_codi in ('GE','PO','CL') or \
                     pf_via_adm = 'B31' or \
                     pf_gt_codi like '23C%'"
        self.exclosos = set([pf for pf,
                             in u.Database("p2262", "import").get_all(sql)])

    def get_neos(self):
        """."""
        self.neos = {key: set() for key in self.poblacio}
        sql = "select id_cip_sec from prt_problemes partition(p{}) \
               where ps in (27, 734)"
        for id, in get_data(sql):
            if id in self.invers:
                self.neos[self.invers[id]].add(id)

    def create_table(self):
        """."""
        cols = ("sector varchar2(4)", "hash varchar2(40)", "num_atc int",
                "up varchar2(5)", "uba varchar2(5)", "ubainf varchar2(5)",
                "edat int", "pocs int", "institucionalitzat int", "atdom int",
                "pcc int", "maca int", "ibp_den int", "ibp_num int",
                "diab_den int", "diab_num int", "estat_den int",
                "estat_num int", "uri_den int", "uri_num int",
                "anticol_den int", "anticol_num int", "num_ind int",
                "to_show int")
        with u.Database("redics", "data") as conn:
            conn.create_table(MASTER, cols, remove=True)
            conn.execute("create unique index {0}_pk on {0} (sector, hash)".format(MASTER))  # noqa
            conn.set_grants("select", MASTER, ("PDP", "PREDUPRP"))
            conn.create_table(BACKUP, ("periode varchar2(6)",) + cols,
                              remove=False)
            conn.set_grants("select", BACKUP, ("PDP", "PREDUPRP"))

    def execute_sector(self):
        """."""
        sectors = [(sector, len(dades)) for sector, dades
                   in self.poblacio.items()]
        if DEBUG:
            Sector("6837", self.poblacio["6837"], self.estatines["6837"],
                   self.indicadors["6837"], self.u11["6837"], self.exclosos,
                   self.neos["6837"])
        else:
            pool = m.Pool(8)
            results = []
            for sector, n in sorted(sectors, key=lambda x: x[1], reverse=True):
                args = (sector, self.poblacio[sector], self.estatines[sector],
                        self.indicadors[sector], self.u11[sector],
                        self.exclosos, self.neos[sector])
                results.append(pool.apply_async(Sector, args=args))
            pool.close()
            pool.join()
            for result in results:
                try:
                    result.get()  # noqa
                except Exception:
                    raise

    def export_lv(self):
        """."""
        sql = """
            select up, uba, 'MEDIBP', 'NUM', sum(ibp_num) from {0} \
            group by up, uba
            union all
            select up, uba, 'MEDIBP', 'DEN', sum(ibp_den) from {0} \
            group by up, uba
            union all
            select up, uba, 'MEDADIAB', 'NUM', sum(diab_num) from {0} \
            group by up, uba
            union all
            select up, uba, 'MEDADIAB', 'DEN', sum(diab_den) from {0} \
            group by up, uba
            union all
            select up, uba, 'MEDESTATINES', 'NUM', sum(estat_num) from {0}
            group by up, uba
            union all
            select up, uba, 'MEDESTATINES', 'DEN', sum(estat_den) from {0}
            group by up, uba
            union all
            select up, uba, 'MEDURINARIS', 'NUM', sum(uri_num) from {0} \
            group by up, uba
            union all
            select up, uba, 'MEDURINARIS', 'DEN', sum(uri_den) from {0} \
            group by up, uba
            union all
            select up, uba, 'MEDANTICOL', 'NUM', sum(anticol_num) from {0} \
            group by up, uba
            union all
            select up, uba, 'MEDANTICOL', 'DEN', sum(anticol_den) from {0} \
            group by up, uba
        """
        dades = list(u.Database("redics", "data").get_all(sql.format(MASTER)))
        cols = ("up varchar(5)", "uba varchar(5)", "indicador varchar(16)",
                "analisi varchar(16)", "valor int")
        with u.Database("p2262", "altres") as conn:
            conn.create_table(LV_TABLE, cols, remove=True)
            conn.list_to_table(dades, LV_TABLE)
        sql = "select indicador, 'Aperiodo', ics_codi, analisi, 'NOCAT', \
                      'NOIMP', 'DIM6SET', 'N', sum(valor) \
               from altres.{} a \
               inner join nodrizas.cat_centres b \
                     on a.up = b.scs_codi \
               group by indicador, ics_codi, analisi".format(LV_TABLE)
        u2.exportKhalix(sql, "MEDPRIOR")
        sql = "select indicador, 'Aperiodo', \
                      concat(ics_codi, 'M', uba), analisi, 'NOCAT', 'NOIMP', \
                      'DIM6SET', 'N', valor \
                from altres.{} a \
                inner join nodrizas.cat_centres b \
                      on a.up = b.scs_codi".format(LV_TABLE)
        u2.exportKhalix(sql, "MEDPRIOR_UBA")

    def save_backup(self):
        """."""
        periode = "".join([u2.getKhalixDates()[i] for i in (0, 2)])
        sqls = ("delete {} where periode = '{}'",
                "insert into {} select '{}', a.* from {} a")
        with u.Database("redics", "data") as conn:
            for sql in sqls:
                this = sql.format(BACKUP, periode, MASTER)
                conn.execute(this)

    def copy_exadata(self, table):
        """."""
        sql = "select usua_cip, a.* \
               from preduffa.{} a \
               inner join pdptb101 b on a.hash = b.usua_cip_cod"
        dades = [(row[1], row[0]) + row[3:] for row
                 in u.Database("redics", "pdp").get_all(sql.format(table))]
        cols = u.Database("redics", "data").get_table_columns(table)
        with u.Database("redics", "data") as redics:
            columns = [redics.get_column_information(col, table, desti="ora")["create"]  # noqa
                       for col in cols]
        columns[1] = "cip varchar2(13)"
        with u.Database("exadata", "data") as exadata:
            exadata.create_table(table, columns, remove=True)
            exadata.set_grants("select", table, "DWSISAP_ROL", inheritance=False)  # noqa
            exadata.list_to_table(dades, table)

    def get_catsalut_pacient(self):
        """."""
        origen = "dwcatsalut.indicadors_catsalut_detall_pacients"
        sql = "select substr(b.cip, 0, 13) \
               from {0} a \
               inner join dwsisap.rca_cip_nia b \
                     on a.nia_pacient = b.nia \
                where indicador = 'POLIMED15' and \
                      mes = (select max(mes) from {0} \
                             where indicador = 'POLIMED15')".format(origen)
        raw = [cip for cip in u.Database("exadata", "data").get_all(sql)]
        temp = "politemp"
        with u.Database("redics", "pdp") as pdp:
            pdp.create_table(temp, ("cip varchar2(13)",))
            pdp.list_to_table(raw, temp)
            sql = "select id_cip_sec, sector, hash_d \
                   from politemp a \
                   inner join pdptb101 b on a.cip = b.usua_cip \
                   inner join preduffa.sisap_u11 c on b.usua_cip_cod = c.hash_a"  # noqa
            poli15 = {row[0]: row[1:] for row in pdp.get_all(sql)}
            pdp.drop_table(temp)
        upload = [poli15[id] + atributs
                  for id, atributs in self.poblacio_all.items()
                  if id in poli15]
        cols = ("sector varchar2(4)", "hash varchar2(40)", "up varchar2(5)",
                "uba varchar2(5)", "ubainf varchar2(5)", "edat int",
                "institucionalitzat int", "atdom int", "pcc int", "maca int",
                "pocs int")
        with u.Database("redics", "data") as conn:
            conn.create_table(CATSALUT, cols, remove=True)
            conn.execute("create unique index {0}_pk on {0} (sector, hash)".format(CATSALUT))  # noqa
            conn.set_grants("select", CATSALUT, ("PDP", "PREDUPRP"))
            conn.list_to_table(upload, CATSALUT)


class Sector(object):
    """."""

    def __init__(self, sector, poblacio, estatines, indicadors, u11, exclosos, neos):  # noqa
        """."""
        self.sector = sector
        self.poblacio = poblacio
        self.estatines = estatines
        self.indicadors = indicadors
        self.u11 = u11
        self.exclosos = exclosos
        self.neos = neos
        self.get_polifarmacia()
        self.get_master()
        self.upload_master()

    def get_polifarmacia(self):
        """."""
        sql = "select id_cip_sec, ppfmc_atccodi, ppfmc_pf_codi \
               from tractaments_s{}, nodrizas.dextraccio \
               where ppfmc_data_fi > data_ext and \
                     (ppfmc_durada > 360 or ppfmc_seg_evol = 'S') and \
                     length(ppfmc_atccodi) = 7".format(self.sector)
        farmacia = c.defaultdict(set)
        for id, atc, pf in u.Database("p2262", "import").get_all(sql):
            if id in self.poblacio and pf not in self.exclosos:
                farmacia[id].add(atc)
        self.polifarmacia = {id: len(atcs) for id, atcs in farmacia.items()
                             if len(atcs) > 10}
        codis = set(["G04BD02", "G04BD05", "G04BD06", "G04BD07", "G04BD08",
                     "G04BD09", "G04BD11", "G04BD12", "G04BD13", "G04AC53"])
        self.urinaris = set([id for id, atcs in farmacia.items()
                             if any([cod in atcs for cod in codis])])
        codis = set(["A03BA01", "A03BB01", "A03DB04", "G04BD02", "G04BD04",
                     "G04BD06", "G04BD07", "G04BD08", "G04BD09", "G04BD11",
                     "G04BD13", "G04CA53", "M03BA03", "M03BA53", "M03BX01",
                     "M03BX02", "M03BX08", "M03BX03", "N02AA01", "N02AA05",
                     "N02AA55", "N02AB02", "N02AB03", "N02AJ06", "N02AJ07",
                     "N02AJ08", "R05DA04", "N02AJ13", "N02AJ14", "N02AX02",
                     "N07BC02", "N03AE01", "N05BA01", "N05BA02", "N05BA51",
                     "N05CD05", "N05CD07", "N03AF01", "N03AF02", "N04AA01",
                     "N04AA02", "N04AA04", "N04BA03", "N04BC01", "G02CB01",
                     "N04BX02", "N04BX01", "N04BX04", "N05AA01", "N05AA02",
                     "N05AB02", "N05AB03", "N05AD01", "N05AG02", "N05AH02",
                     "N05AH03", "N05AH04", "N05AN01", "N05AX08", "N05AX13",
                     "N06AA02", "N06AA04", "N06AA06", "N06AA09", "N06AA10",
                     "N06AA12", "N06AB03", "N06AB04", "N06AB05", "N06AB08",
                     "N06AX05", "N06AX11", "N06CA02", "N06CA01", "R01BA53",
                     "R06AA09", "R06AA59", "R06AA02", "R06AA52", "R06AB02",
                     "R03DA12", "R06AD01", "R06AE05", "R06AX26", "R06AE07",
                     "R06AE57", "R06AX02", "A15ZZ91", "R06AX13", "R06AX27",
                     "R06AX17", "R06AE92", "N05BB01", "N02AA79", "R05CB10",
                     "N05CX92", "A03FA03", "A07DA03", "A07DA53", "C01BA03",
                     "M04AC51", "N04BB01", "J05AC91", "R03DA04", "A02BA02",
                     "M01AB15"])
        self.anticol = set([id for id, atcs in farmacia.items()
                            if len(atcs & codis) > 2])

    def get_master(self):
        """."""
        cols = ("sector varchar2(4)", "hash varchar2(40)", "num_atc int",
                "up varchar2(5)", "uba varchar2(5)", "ubainf varchar2(5)",
                "edat int", "pocs int", "institucionalitzat int", "atdom int",
                "pcc int", "maca int", "ibp_den int", "ibp_num int",
                "diab_den int", "diab_num int", "estat_den int",
                "estat_num int", "uri_den int", "uri_num int",
                "anticol_den int", "anticol_num int", "num_ind int",
                "to_show int")
        self.upload = []
        for id, (up, uba, ubainf, edat, insti, atdom, pcc, maca, pocs) in self.poblacio.items():  # noqa
            if id in self.polifarmacia:
                this = [self.sector, self.u11[id], self.polifarmacia[id], up, uba, ubainf, edat, pocs, insti, atdom, pcc, maca]  # noqa
                this.append(1 * (id in self.indicadors["eqa0222"]))
                this.append(self.indicadors["eqa0222"].get(id, 0))
                this.append(1 * (edat > 79 and id in self.indicadors["farm0008"]))  # noqa
                this.append(1 * (edat > 79 and self.indicadors["farm0008"].get(id, 0)))  # noqa
                this.append(1 * (edat > 79 and id in self.estatines["den"]))
                this.append(1 * (edat > 79 and id in self.estatines["den"] and id not in self.estatines["num"]))  # noqa
                this.append(1 * (id not in self.neos))
                this.append(1 * (id not in self.neos and id in self.urinaris))
                this.append(1)
                this.append(1 * (id in self.anticol))
                n_ind = sum([this[i] for i in (-9, -7, -5, -3, -1)])
                this.extend((n_ind, 1 * (n_ind > 0)))
                self.upload.append(this)

    def upload_master(self):
        """."""
        u.Database("redics", "data").list_to_table(self.upload, MASTER)


if __name__ == "__main__":
    Polifarmacia()
