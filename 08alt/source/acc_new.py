# coding: latin1

"""
.
"""

import collections as c
import datetime as d
import dateutil.relativedelta as r
import os
import pandas as pd
from pydruid.client import PyDruid
from pydruid.utils.aggregators import doublesum
from pydruid.utils.filters import Dimension

import sisapUtils as u
import sisaptools as t


tb = "exp_khalix_forats"
tb_uba = "exp_ecap_forats"
tb_qc = "exp_qc_forats"
tb_odn = "exp_khalix_forats_odn"
tb_ts = "exp_khalix_forats_ts"
db = "altres"
file_up = "ACCESSIBILITAT_FORATS"
file_uba = "ACCESSIBILITAT_FORATS_UBA"
file_odn = "ACCESSIBILITAT_FORATS_ODN"
file_ts = "ACCESSIBILITAT_FORATS_TS"

dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
fa1a = dext - r.relativedelta(years=1) + r.relativedelta(days=1)
fa1m = dext - r.relativedelta(months=1)

tipprof = {"MG": 1, "PED": 2,
           "ODN": 3, "ODO": 3,
           "INF": 4, "ENF": 4, "INFP": 4, "INFG": 4, "INFPD": 4, "INFGR": 4, "INFPE": 4,  # noqa
           "TS": 5, "AS": 5, "TSOC": 5, "ASS": 5, "ASO": 5,
           "GCAS": 6}
tipuba = {1: "M", 2: "M", 3: None, 4: "I", 5: None, 6: "I"}
no_uba = tuple([serv for serv, tip in tipprof.items() if tip in (3, 5)])


class Accessibilitat(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_ubas()
        self.get_fixes()
        self.get_excloses()
        self.get_vives()
        self.get_dades()
        self.get_khalix()
        self.get_ecap()
        self.export_khalix()
        self.export_ecap()
        self.set_catalegs()
        self.get_qc()
        # self.send_dap()

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}

    def get_ubas(self):
        """."""
        sql = "select up, uba, tipus from eqa_ind.mst_ubas union \
               select up, uba, tipus from pedia.mst_ubas"
        self.ubas = set([row for row in u.getAll(sql, "eqa_ind")])

    def get_fixes(self):
        """."""
        self.fixes_default = {"9C": 10, "9R": 10, "9T": 5}
        sqls = [("pdp",
                 "select dataany, datames, up, uab, tipus, sum(assignada) \
                  from pobindicadors \
                  where dataany in ({0} - 1, {0}) \
                  group by dataany, datames, up, uab, tipus".format(dext.year)),  # noqa
                ("altres",
                 "select {0}, {1}, up, uba, tipus, sum(ass) \
                  from exp_khalix_pobubageneral \
                  group by up, uba, tipus".format(dext.year, dext.month))]
        self.fixes = {}
        for conn, sql in sqls:
            for row in u.getAll(sql, conn):
                pob = row[-1]
                for tipus in self.fixes_default:
                    if tipus != "9T" and pob < 600:
                        key = row[:-1] + (tipus,)
                        if pob >= 500:
                            intents = 8
                        elif pob >= 400:
                            intents = 7
                        elif pob >= 300:
                            intents = 6
                        elif pob >= 200:
                            intents = 5
                        elif pob >= 100:
                            intents = 4
                        else:
                            intents = 3
                        self.fixes[key] = intents

    def get_excloses(self):
        """."""
        excloure = set([(1, 2), (1, 3), (2, 2), (2, 3)])
        self.excloses_agendes = set()
        self.excloses_ubas = set()
        sql = "select modu_codi_up, modu_id_modul, modu_codi_uab,\
                      cast(modu_act_agenda as int), \
                      cast(modu_subact_agenda as int) \
               from cat_vistb027 \
               where modu_act_agenda <> ''"
        for up, id, uba, act, subact in u.getAll(sql, "import"):
            if (act, subact) in excloure:
                self.excloses_agendes.add((id, up))
                self.excloses_ubas.add((up, uba))

    def get_vives(self):
        """."""
        if dext.month > 6:
            intervals = [(dext.strftime("%Y-01-01"), dext.strftime("%Y-06-30")),  # noqa
                         (dext.strftime("%Y-07-01"), dext.strftime("%Y-%m-%d"))]  # noqa
        else:
            intervals = [(dext.strftime("%Y-01-01"), dext.strftime("%Y-%m-%d"))]  # noqa
        self.vives = set()
        for interval in intervals:
            query = PyDruid('http://10.80.217.84:8082', 'druid/v2/')
            resul = query.groupby(
                    datasource='agendes',
                    granularity='day',
                    intervals='{}/{}'.format(*interval),
                    aggregations={'f1': doublesum('PROGRAMADES_REALITZADES'),
                                  'f2': doublesum('ESPONTANIES_REALITZADES'),
                                  'f3': doublesum('FORCADES_REALITZADES')},
                    dimensions=['UP', 'UBA', 'SERVEI_CODI'],
                    filter=(Dimension('TIPUS_CODI') == '9C') | (Dimension('TIPUS_CODI') == '9R')  # noqa
            )
            banned = (d.datetime(2023, 9, 13), d.datetime(2023, 9, 15),
                      d.datetime(2023, 10, 20), d.datetime(2023, 11, 10),
                      d.datetime(2023, 11, 13), d.datetime(2023, 11, 16))  # errors execuci�
            for row in resul:
                dia = d.datetime.strptime(row[u"timestamp"], "%Y-%m-%dT00:00:00.000Z")  # noqa
                if (dia < d.datetime(2022, 1, 1) or dia > d.datetime(2022, 4, 30)) and dia not in banned and dia < d.datetime(2024, 1, 1):  # noqa  
                    up, uba, servei = tuple(row[u"event"][k].encode("latin1")
                                            for k in (u"UP", u"UBA", u"SERVEI_CODI"))  # noqa
                    fetes = int(sum((row[u"event"][k] for k in ("f1", "f2", "f3"))))  # noqa
                    if up in self.centres and fetes and u.isWorkingDay(dia):
                        self.vives.add((up, uba, servei, dia, '9C'))
                        if dia > d.datetime(2021, 3, 11):
                            self.vives.add((up, uba, servei, dia, '9T'))
                            if dia > d.datetime(2022, 10, 18):
                                self.vives.add((up, uba, servei, dia, '9R'))

    def get_dades(self):
        """."""
        self.pactual = "A{}".format(dext.strftime("%y%m"))
        self.pprevi = "A{}".format(fa1m.strftime("%y%m"))
        sql = "select modul, up, uba, servei, dia, tipus, nvl(peticions, 0), \
                      forats_0_lab, forats_1_lab, forats_2_lab, forats_3_lab, \
                      forats_4_lab, forats_5_lab, forats_6_lab, forats_7_lab, \
                      forats_8_lab, forats_9_lab, forats_10_lab \
               from sisap_accessibilitat \
               where dia between to_date('{}', 'YYYYMMDD') and \
                                 to_date('{}', 'YYYYMMDD') and \
                     (uba is not null or \
                      servei in {})".format(fa1a.strftime("%Y%m%d"),
                                            dext.strftime("%Y%m%d"),
                                            no_uba)
        raw_data = c.defaultdict(list)
        existeixen = set()
        linies_odo = {387349: "00469", 387350: "00444", 387351: "00445",
                      387352: "00461", 4412141: "00500", 4412159: "00457",
                      4412160: "00491", 4412161: "00456", 397451: "00471",
                      397452: "00473", 397453: "08208"}
        for row in u.getAll(sql, "redics"):
            if row[0] in linies_odo:
                row = (row[0], linies_odo[row[0]]) + row[2:]
            if row[:2] not in self.excloses_agendes:
                raw_data[row[1:6]].append(row[6:])
        for row in self.vives:
            if row not in raw_data and row[:2] not in self.excloses_ubas:
                raw_data[row].append((0,) * 12)
        self.dades = c.defaultdict(lambda: c.Counter())
        self.dades_uba = c.defaultdict(lambda: c.Counter())
        self.dades_qc = c.Counter()
        self.dades_odn = c.Counter()
        self.dades_ts = c.Counter()
        for key, values in raw_data.items():
            up, uba, servei, dia, tipvis = key
            tipus_uba = tipuba[tipprof[servei]]
            _fixes = self.fixes.get((dia.year, dia.month, up, uba, tipus_uba, tipvis), self.fixes_default[tipvis])  # noqa
            dades = [sum(i) for i in zip(*values)]
            _peticions = dades[0]
            forats = dades[1:]
            if up in self.centres:
                periode = "A{}".format(dia.strftime("%y%m"))
                centre = self.centres[up]
                tipus = "TIPPROF{}".format(tipprof[servei])
                if tipus == "TIPPROF3":
                    ids = []
                    ids_odn = [(periode, centre)]
                    ids_ts = []
                elif tipus == "TIPPROF5":
                    ids = []
                    ids_odn = []
                    ids_ts = [(periode, centre)]
                else:
                    ids = [(periode, centre, tipus)]
                    ids_odn = []
                    ids_ts = []
                ids_uba = []
                if (up, uba, tipus_uba) in self.ubas:
                    entity = centre + tipus_uba + uba
                    ids.append((periode, entity, "NOIMP"))
                    ids_uba.append((periode, up, tipus_uba, uba))
                lletra = "" if tipvis == "9C" else tipvis[1]
                for peticions, sufix in [(_peticions, ""), (_fixes, "F")]:
                    if peticions:
                        for dies in (2, 3, 5, 10):
                            cuenta = "ACC{}D{}{}".format(dies, lletra, sufix)
                            numerador = min(sum(forats[:(dies + 1)]), peticions)  # noqa
                            for id in ids:
                                if dia.year == dext.year:
                                    self.dades[id][(cuenta, "DEN")] += peticions  # noqa
                                    self.dades[id][(cuenta, "NUM")] += numerador  # noqa
                                this = ("Q" + cuenta, id[1], tipus)
                                self.dades_qc[this + (self.pactual, "ANUAL", "DEN")] += peticions  # noqa
                                self.dades_qc[this + (self.pactual, "ANUAL", "NUM")] += numerador  # noqa
                                if periode in (self.pprevi, self.pactual):
                                    that = (periode, "ACTUAL" if periode == self.pactual else "PREVI")  # noqa
                                    self.dades_qc[this + that + ("DEN",)] += peticions  # noqa
                                    self.dades_qc[this + that + ("NUM",)] += numerador  # noqa
                            for idu in ids_uba:
                                if periode == self.pactual and dies != 3 and not lletra and sufix == "F":  # noqa  # si es treu F, treure tamb� el [:-1] de les linies seguents
                                    self.dades_uba[idu][cuenta[:-1], "DEN"] += peticions  # noqa
                                    self.dades_uba[idu][cuenta[:-1], "NUM"] += numerador  # noqa
                            for id in ids_odn:
                                if dia.year == dext.year and sufix == "F" and dies != 3:  # noqa
                                    self.dades_odn[id + (cuenta + "_ODN", "DEN")] += peticions  # noqa
                                    self.dades_odn[id + (cuenta + "_ODN", "NUM")] += numerador  # noqa
                            for id in ids_ts:
                                if dia.year == dext.year and sufix == "F" and dies != 3:  # noqa
                                    self.dades_ts[id + (cuenta + "_TS", "DEN")] += peticions  # noqa
                                    self.dades_ts[id + (cuenta + "_TS", "NUM")] += numerador  # noqa

    def get_khalix(self):
        """."""
        self.upload = []
        for (periode, centre, tipus), dades in self.dades.items():
            for (cuenta, analisis), n in dades.items():
                self.upload.append((cuenta, periode, centre, analisis, "NOCAT", tipus, "DIM6SET", "N", n))  # noqa
        self.upload_odn = [(cuenta, periode, centre, analisis, 'NOCAT',
                           'NOIMP', 'DIM6SET', 'N', n)
                           for (periode, centre, cuenta, analisis), n
                           in self.dades_odn.items()]
        self.upload_ts = [(cuenta, periode, centre, analisis, 'NOCAT',
                           'NOIMP', 'DIM6SET', 'N', n)
                           for (periode, centre, cuenta, analisis), n
                           in self.dades_ts.items()]

    def get_ecap(self):
        """."""
        self.upload_ecap = []
        for (periode, up, tipus_uba, uba), dades in self.dades_uba.items():
            for (cuenta, analisis), n in dades.items():
                if analisis == "DEN":
                    num = self.dades_uba[periode, up, tipus_uba, uba][cuenta, "NUM"]  # noqa
                    perc = float(num)/float(n)
                    self.upload_ecap.append((up, uba, tipus_uba, cuenta, num, n, perc))  # noqa

    def export_khalix(self):
        """."""
        columns = ["d{} varchar(16)".format(i) for i in range(8)]
        columns.append("n int")
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
        u.exportKhalix("select * from {}.{} where length(d2) = 5 and d1 in ('{}', '{}')".format(db, tb, self.pprevi, self.pactual), file_up)  # noqa
        u.exportKhalix("select * from {}.{} where length(d2) > 5 and d1 in ('{}', '{}')".format(db, tb, self.pprevi, self.pactual), file_uba)  # noqa
        u.createTable(tb_odn, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload_odn, tb_odn, db)
        u.exportKhalix("select * from {}.{} where d1 in ('{}', '{}')".format(db, tb_odn, self.pprevi, self.pactual), file_odn)  # noqa
        u.createTable(tb_ts, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload_ts, tb_ts, db)
        u.exportKhalix("select * from {}.{} where d1 in ('{}', '{}')".format(db, tb_ts, self.pprevi, self.pactual), file_ts)  # noqa

    def export_ecap(self):
        """."""
        columns = ["d{} varchar(11)".format(i) for i in range(4)]
        columns.append("num int")
        columns.append("den int")
        columns.append("resultat double")
        u.createTable(tb_uba, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload_ecap, tb_uba, db)

    def set_catalegs(self):
        """."""
        tb = "exp_ecap_forats_cataleg"
        cols = """(
                indicador varchar(10),
                literal varchar(300),
                ordre int,
                pare varchar(8),
                llistat int,
                invers int not null default 0,
                mmin double not null default 0,
                mint double not null default 0,
                mmax double not null default 0,
                toShow int,
                wiki varchar(250) not null default '')
            """
        u.createTable(tb, cols, db, rm=True)
        pare = "ACC"
        pare2 = "LONG"  # aquests es pugen a alt_ecap_indicadors.py
        upload = [("ACC2D", "Accessibilitat 48 hores",
                   1, pare, 0, 0, 0, 0, 0, 1,
                   "http://10.80.217.201/sisap-umi/indicador/codi/ACC2DF"),
                  ("ACC3D", "Accessibilitat 72 hores",
                   1, pare, 0, 0, 0, 0, 0, 1,
                   "http://10.80.217.201/sisap-umi/indicador/codi/ACC3DF"),
                  ("ACC5D", "Accessibilitat 5 dies",
                   2, pare, 0, 0, 0, 0, 0, 1,
                   "http://10.80.217.201/sisap-umi/indicador/codi/ACC5DF"),
                  ("ACC10D", "Accessibilitat 10 dies",
                   3, pare, 0, 0, 0, 0, 0, 1,
                   "http://10.80.217.201/sisap-umi/indicador/codi/ACC10DF"),
                  ("CONT0002", "�ndex del prove�dor assistencial principal",
                   1, pare2, 0, 0, 0, 0, 0, 1,
                   "http://10.80.217.201/sisap-umi/indicador/codi/CONT0002"),
                  ("VISUBA", "% Percentatge de visites realitzades en agendes UBA",  # noqa
                   2, pare2, 0, 0, 0, 0, 0, 1,
                   "http://10.80.217.201/sisap-umi/indicador/codi/VISUBA"),
                   ("CONT0002A", "�ndex del prove�dor assistencial principal anual",
                   3, pare2, 0, 0, 0, 0, 0, 1,
                   "http://10.80.217.201/sisap-umi/indicador/codi/CONT0002A")]
        u.listToTable(upload, tb, db)
        tb = "exp_ecap_forats_catalegpare"
        cols = "(pare varchar(8) not null default '',literal varchar(300) not null default '',ordre int, pantalla varchar(20))"  # noqa
        u.createTable(tb, cols, db, rm=True)
        upload = [(pare, "Accessibilitat", 2, "acc"),
                  (pare2, "Longitudinalitat", 1, "acc")]
        u.listToTable(upload, tb, db)

    def get_qc(self):
        """."""
        dades = [(cuenta, periodo, centro, analisis, anual, tipo, n)
                 for (cuenta, centro, tipo, periodo, anual, analisis), n
                 in self.dades_qc.items()]
        cols = ["k{} varchar(16)".format(i) for i in range(6)] + ["v int"]
        u.createTable(tb_qc, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(dades, tb_qc, db)

    @staticmethod
    def _pretty(entrada):
        """."""
        if isinstance(entrada, str):
            return(entrada.decode("latin1"))
        else:
            return(entrada)

    def send_dap(self):
        """."""
        tmp = "forats_tmp"
        tmp2 = "forats_tmp_eap"
        quinzenal = abs(dext.day - 15) < 4
        if quinzenal or u.IS_MENSUAL:
            create = "create or replace table {} as \
                      select d2 br, d0 indicador, d5 categoria, \
                             sum(if(d3 = 'NUM', n, 0)) numerador, \
                             sum(if(d3 = 'DEN', n, 0)) denominador \
                      from {} \
                      where d0 in ('ACC5DF', 'ACC10DF') and \
                            right(d5, 1) in ('1', '2', '3', '4') and \
                            length(d2) = 5 \
                      group by d2, d0, d5".format(tmp, tb)
            t.Database("p2262", "altres").execute(create)
            eap = "create or replace table {} as \
                   select a.*, b.* \
                   from nodrizas.cat_centres a \
                   inner join import.cat_sisap_covid_dbc_rup b \
                         on a.scs_codi = b.up_cod".format(tmp2)
            t.Database("p2262", "altres").execute(eap)
            enviaments = [
                {"titol": "ICS",
                 "file": "accessibilitat_ics.xlsx",
                 "pestanyes": [
                    ("EAP",
                     "select amb_desc, sap_desc, br, ics_desc, \
                             case when indicador = 'ACC5DF' \
                                  then '5 dies' else '10 dies' end indicador, \
                             round(100 * sum(numerador) / sum(denominador), 1)\
                      from {} a \
                      inner join nodrizas.cat_centres b on a.br = b.ics_codi \
                      where ep = '0208' \
                      group by amb_desc, sap_desc, br, ics_desc, indicador \
                      order by amb_desc, sap_desc, br, \
                               indicador desc".format(tmp),
                     ("DAP", "SAP", "BR", "EAP", "INDICADOR", "RESULTAT")),
                    ("Categoria",
                     "select amb_desc, sap_desc, br, ics_desc, \
                            case when indicador = 'ACC5DF' \
                                 then '5 dies' else '10 dies' end indicador, \
                            case when categoria = 'TIPPROF1' then 'Medicina' \
                                when categoria = 'TIPPROF2' then 'Pediatria' \
                                when categoria = 'TIPPROF4' then 'Infermeria' \
                                when categoria = 'TIPPROF3' then 'Odontologia'\
                                else categoria end grup, \
                            round(100 * numerador / denominador, 1) \
                     from {} a \
                     inner join nodrizas.cat_centres b on a.br = b.ics_codi \
                     where ep = '0208' \
                     order by amb_desc, sap_desc, br, \
                              indicador desc, categoria".format(tmp),
                     ("DAP", "SAP", "BR", "EAP", "INDICADOR",
                      "CATEGORIA", "RESULTAT"))],
                 "to": ["cfernandezs.apms.ics@gencat.cat", "ccarbonell.bcn.ics@gencat.cat", "dferrerv@gencat.cat",  # noqa
                        "eavellana.girona.ics@gencat.cat", "egavalda.ebre.ics@gencat.cat", "aforcada.cc.ics@gencat.cat",  # noqa
                        "mfeixes.pirineu.ics@gencat.cat", "nprat.mn.ics@gencat.cat", "pvaque.lleida.ics@gencat.cat",  # noqa
                        "rriel@gencat.cat", "direccioprimaria.lleida.ics@gencat.cat", "ebotella.bcn.ics@gencat.cat",  # noqa
                        "egarciama.tgn.ics@gencat.cat", "direccio.tgn.ics@gencat.cat", "illaveria.girona.ics@gencat.cat",  # noqa
                        "dap.girona.ics@gencat.cat", "direccioadjuntaap.lleida.ics@gencat.cat", "direccioapmn.ics@gencat.cat",  # noqa
                        "gerencia.cc.ics@gencat.cat", "gerencia.pirineu.ics@gencat.cat", "sferreres.ebre.ics@gencat.cat",  # noqa
                        "vrojas.bcn.ics@gencat.cat", "yzaragoza@ambitcp.catsalut.net", "caguilar.ebre.ics@gencat.cat",  # noqa
                        "mesteban.ebre.ics@gencat.cat", "lconangla.mn.ics@gencat.cat"],
                 "cc": ["mfabregase@gencat.cat", "srodoreda.mn.ics@gencat.cat",
                        "nurianadal@gencat.cat", "ffinaaviles@gencat.cat"]
                },
                {"titol": "Catalunya",
                 "file": "accessibilitat_cat.xlsx",
                 "pestanyes": [
                    ("EAP",
                     "select regio_des, aga_des, ep_des, up_cod, ics_desc, \
                             case when a.indicador = 'ACC5DF' \
                                  then '5 dies' else '10 dies' end indicador, \
                             round(100*sum(numerador) / sum(denominador), 1), \
                             y, x \
                      from {0} a \
                      inner join {1} b on a.br = b.ics_codi \
                      inner join \
                        (select indicador, \
                           round(100 * sum(numerador) / sum(denominador), 1) x\
                         from {0} \
                         group by indicador) c on a.indicador = c.indicador \
                      inner join \
                        (select indicador, regio_cod, \
                           round(100 * sum(numerador) / sum(denominador), 1) y\
                         from {0} a \
                         inner join {1} b on a.br = b.ics_codi \
                         group by indicador, regio_cod) d \
                        on a.indicador = d.indicador and \
                           b.regio_cod = d.regio_cod \
                      group by regio_des, aga_des, ep_des, up_cod, ics_desc, \
                               indicador \
                      order by regio_des, aga_des, ep_des, up_cod, ics_desc, \
                               a.indicador desc".format(tmp, tmp2),
                     ("REGIO", "AGA", "ENTITAT", "UP", "EAP", "INDICADOR",
                      "RESULTAT", "RESULTAT_REGIO", "RESULTAT_CAT")),
                    ("Categoria",
                     "select regio_des, aga_des, ep_des, up_cod, ics_desc, \
                            case when a.indicador = 'ACC5DF' \
                                 then '5 dies' else '10 dies' end indicador, \
                            case when a.categoria = 'TIPPROF1' then 'Medicina' \
                              when a.categoria = 'TIPPROF2' then 'Pediatria' \
                              when a.categoria = 'TIPPROF4' then 'Infermeria' \
                              when a.categoria = 'TIPPROF3' then 'Odontologia'\
                              else a.categoria end grup, \
                            round(100 * numerador / denominador, 1), y, x \
                     from {0} a \
                     inner join {1} b on a.br = b.ics_codi \
                     inner join \
                        (select indicador, categoria, \
                           round(100 * sum(numerador) / sum(denominador), 1) x\
                         from {0} \
                         group by indicador, categoria) c \
                      on a.indicador = c.indicador and \
                         a.categoria = c.categoria \
                     inner join \
                        (select indicador, categoria, regio_cod, \
                           round(100 * sum(numerador) / sum(denominador), 1) y\
                         from {0} a \
                         inner join {1} b on a.br = b.ics_codi \
                         group by indicador, categoria, regio_cod) d \
                        on a.indicador = d.indicador and \
                           a.categoria = d.categoria and \
                           b.regio_cod = d.regio_cod \
                     order by regio_des, aga_des, ep_des, up_cod, ics_desc, \
                              a.indicador desc, a.categoria".format(tmp, tmp2),
                     ("EP", "REGIO", "AGA", "UP", "EAP", "INDICADOR",
                      "CATEGORIA", "RESULTAT", "RESULTAT_REGIO",
                      "RESULTAT_CAT"))],
                 "to": ["nurianadal@gencat.cat", "srodoreda.mn.ics@gencat.cat"],  # noqa
                 "cc": ["mfabregase@gencat.cat", "ffinaaviles@gencat.cat"]
                }]
            for enviament in enviaments:
                with pd.ExcelWriter(enviament["file"], engine='xlsxwriter') as writer:  # noqa
                    workbook = writer.book
                    for titol, sql, cols in enviament["pestanyes"]:
                        dades = [map(Accessibilitat._pretty, row)
                                 for row in u.getAll(sql, "altres")]
                        df = pd.DataFrame(dades, columns=cols)
                        df.to_excel(writer, sheet_name=titol, index=False)
                        longitut = c.defaultdict(set)
                        for i, col in enumerate(cols):
                            longitut[i].add(len(col))
                        for row in dades:
                            for i, el in enumerate(row):
                                if isinstance(el, unicode):
                                    longitut[i].add(len(el))
                        f = workbook.add_format({"align": "center"})
                        for i, data in longitut.items():
                            writer.sheets[titol].set_column(i, i, max(data) + 2, f)  # noqa
                me = "SISAP <sisap@gencat.cat>"
                subject = "Dades accessibilitat ({})".format(enviament["titol"])  # noqa
                to = enviament["to"]
                cc = enviament["cc"]
                text = "Benvolguts,\n\nAdjuntem informaci� relativa als indicadors d'accessibilitat presencial.\n\nLes dades s�n acumulades des de l'1 de gener fins a {}.\n\nCordialment,\n\nSISAP".format(dext.strftime("%d/%m/%Y"))  # noqa
                u.sendGeneral(me, to, cc, subject, text, enviament["file"])
                os.remove(enviament["file"])


if __name__ == "__main__":
    Accessibilitat()
