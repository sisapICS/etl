import sisapUtils as u
import collections as c
import datetime as d
import sisaptools as t
import operator

pat_trat_tuples_list=[
("CA","OBTt"),
("RR","OBTt") ,
("OF","OBTt"),
("OBTp","OBTt"),
("CA","CON"),
("RR","CON"), 
("OF","CON"),
("OBTp","CON"), 
("CA","PF"),
("RR","PF"), 
("OF","PF"),
("OBTp","PF") ,
]

TE_LLISTAT = ['IAD0013', 'IAD0014', 'IAD0015', 'IAD0016', 'IAD0022', 'IAD0033', 'IAD0034']

indicadors_jail = "('IAD0011', 'IAD0014', 'IAD0018', 'IAD0025', 'IAD0026')"

class IAD():
	def __init__(self):
		self.dades = c.defaultdict(dict)
		u.printTime('inici')
		self.get_data_extraccio()
		u.printTime('get_data_extraccio')
		self.get_centres()
		u.printTime('get_centres')
		self.get_conversor()
		u.printTime('get_conversor')
		self.get_poblacio()
		u.printTime('get_poblacio')
		self.get_embarassades()
		u.printTime('get_embarassades')
		self.get_odn_variables()
		u.printTime('get_odn_variables')
		self.get_odns()
		u.printTime('get_odns')
		self.get_cancer()
		u.printTime('get_cancer')
		self.get_iad0020()
		u.printTime('get_iad0020')
		self.get_edats_khlx()
		u.printTime('get_edats_khlx')
		self.get_indicadors()
		u.printTime('get_indicadors')
		self.export_khalix()
		u.printTime('export_khalix')

	def get_data_extraccio(self):
		sql = """
            SELECT data_ext, date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY), date_add(date_add(data_ext, INTERVAL -2 YEAR), INTERVAL + 1 DAY)
            FROM dextraccio
        """
		self.DEXTD, self.menys1, self.menys2 = u.getOne(sql, 'nodrizas')
	
	def get_centres(self):
		sql = """
			SELECT scs_codi, ics_codi
			FROM cat_centres
            UNION
            SELECT scs_codi, ics_codi
            FROM jail_centres
		"""
		self.centres = {up: br for (up, br) in u.getAll(sql, 'nodrizas')}

		sql = """
            SELECT scs_codi, ics_codi
            FROM jail_centres
		"""
		self.jail_centres = {up: br for (up, br) in u.getAll(sql, 'nodrizas')}
	
	def get_conversor(self):
		self.id_hash = {}
		sql = """
			SELECT id_cip_sec, hash_d, codi_sector
			FROM u11
		"""
		for db in ("import", "import_jail"):
			self.id_hash.update({id: (hash, sec) for (id, hash, sec) in u.getAll(sql, db)})
	
	def get_poblacio(self):
		# 590560 -> nens

		sql = """
				SELECT id_cip_sec, up, uba
				FROM jail_atesa_avui_60
				WHERE (sortida = '0000-00-00' OR sortida BETWEEN '{}' AND '{}')
			""".format(self.menys1, self.DEXTD)
		poblacio_ultim_any = {id: (up, uba) for id, up, uba in u.getAll(sql, "nodrizas")}

		sql = """
			SELECT id_cip_sec, up, uba, upinf, ubainf, upOrigen, edat, sexe, ates, year(data_naix)
			FROM assignada_tot
		"""
		self.poblacio = {}
		for id, up, uba, upinf, ubainf, uporigen, edat, sexe, ates, data in u.getAll(sql, 'nodrizas'):
			if edat < 15:
				up_ass = uporigen
			else:
				up_ass = up
			self.dades['assignada'][id] = 1
			self.poblacio[id] = {
				'up': up_ass,
				'uba': uba,
				'upinf': upinf,
				'ubainf': ubainf,
				'edat': edat,
				'sexe': sexe,
				'ates': ates,
				'data': data
			}
			if edat >= 35:
				self.dades['>35a'][id] = 1
		sql = """
			SELECT id_cip_sec, cast(datediff(data_ext, usua_data_naixement)/365 as int), usua_sexe, year(usua_data_naixement)
			FROM import_jail.assignada, nodrizas.dextraccio
		"""
		for id, edat, sexe, data in u.getAll(sql, 'import_jail'):
			if id in poblacio_ultim_any:
				(up, uba) = poblacio_ultim_any[id]
				self.dades['assignada'][id] = 1
				self.poblacio[id] = {
					'up': up,
					'uba': uba,
					'upinf': up,
					'ubainf': uba,
					'edat': edat,
					'sexe': sexe,
					'ates': 1,
					'data': data
				}
				if edat >= 35:
					self.dades['>35a'][id] = 1

		# sql = """
		# 		SELECT id_cip_sec, usua_data_naixement, usua_sexe
		# 		FROM assignada
		# 	"""
		# for id, data, sexe in u.getAll(sql, 'import_jail'):
		# 	if id in poblacio_ultim_any:
		# 		self.dades['assignada'][id] = 1
		# 		self.poblacio[id] = {'up': poblacio_ultim_any[id]["up"],
		# 							 'uba': poblacio_ultim_any[id]['uba'],
		# 							 'upinf': poblacio_ultim_any[id]["up"],
		# 							 'ubainf': poblacio_ultim_any[id]['uba'],
		# 							 'edat': u.yearsBetween(data, self.DEXTD),
		# 							 'sexe': sexe,
		# 							 'ates': 1,
		# 							 'data': data.year}
		# 		if edat >= 35:
		# 			self.dades['>35a'][id] = 1

	def get_embarassades(self):
		sql = """
			SELECT id_cip_sec, inici, fi
			FROM ass_embaras
			WHERE fi >= DATE '{}'
			AND inici <= date_sub(DATE '{}', interval - 90 day)
		""".format(self.menys1, self.DEXTD)
		self.embarassades = {}
		for id, inici, fi in u.getAll(sql, 'nodrizas'):
			if id in self.poblacio and self.poblacio[id]['sexe'] == 'D':
				self.embarassades[id] = {
					'inici': inici,
					'fi': fi
				}
				self.dades['embaras'][id] = 1

	def get_odn_variables(self):
		# 3721138 amb agr = 309 (revisions)
		sql = """
			SELECT id_cip_sec, valor, dat, agrupador
			FROM odn_variables
			WHERE agrupador in (286, 308, 309)
		"""
		variables = c.defaultdict(lambda: c.defaultdict(set))
		for id, valor, data, agr in u.getAll(sql, 'nodrizas'):
			if id in self.poblacio:
				variables[agr][id].add((valor, data))
		for agr, ids in variables.items():
			self.treat_variables(agr, ids)

	def get_odns(self):
		sql = """
			SELECT id_cip_sec, dfd_cod_p,
			CASE WHEN dfd_p_a = 'A' AND dfd_mot_a not in ('N', 'A') THEN 1 ELSE 0 END, 
			CASE WHEN dfd_p_a = 'P' and dfd_cod_p not in (18, 28, 38, 48) THEN 2 ELSE 0 END,
			CASE WHEN dfd_p_a = 'P' AND dfd_estat = 'C' THEN 3 ELSE 0 END
			FROM {tb}
			WHERE (dfd_p_a = 'A' AND dfd_mot_a not in ('N', 'A'))
			OR (dfd_p_a = 'P' and dfd_cod_p not in (18, 28, 38, 48))
			OR (dfd_p_a = 'P' AND dfd_estat = 'C')
		"""
		odn506 = c.defaultdict(list)
		absencies = c.defaultdict(set)
		taules_db = [(tb, 'import') for tb in u.getSubTables('odn506', 'import')] + [('odn506', 'import_jail')]
		for tb, db in taules_db:
			for id, codp, absencia, peces, caries in u.getAll(sql.format(tb=tb), db):
				if id in self.poblacio:
					odn506[id].append((codp, peces, caries))
					if absencia:
						absencies[id].add(codp)
			u.printTime(tb)
		u.printTime('odn506')
		self.treat_peces(odn506)
		odn506 = None
		u.printTime('treat_peces')

		sql = """
			SELECT id_cip_sec, dpd_cod_p, dpd_pat, dpd_data
			FROM odn507
		"""
		patologies = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(set)))
		# for tb in u.getSubTables('odn507', 'import'):
		for db in ('import', 'import_jail'):
			for id, cod, pat, data in u.getAll(sql, db):
				if id in self.poblacio:
					pat = 'OBTp' if pat == 'OBT' else pat
					patologies[id][cod][pat].add(data)
		u.printTime('odn507')

		sql = """
			SELECT id_cip_sec, dtd_cod_p, dtd_tra, dtd_data, dtd_pat
			FROM odn508
		"""
		odn508 = c.defaultdict(set)
		tractaments = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(set)))
		# for tb in u.getSubTables('odn508', 'import'):
		for db in ('import', 'import_jail'):
			for id, codi, tract, data, pat in u.getAll(sql, db):
				if id in self.poblacio:
					tract = 'OBTt' if tract == 'OBT' else tract
					tractaments[id][codi][tract].add(data)
					odn508[id].add((codi, data, tract, pat))
		u.printTime('odn508')

		self.treat_caos(tractaments, patologies, absencies)
		patologies = None
		absencies = None
		u.printTime('treat_caos')

		sql = """
			SELECT id_cip_sec
			FROM odn_peces
			WHERE peca in (16,26,36,46)
			AND pato like '%@CA@%'
		"""
		caries_primer = set()
		for id, in u.getAll(sql, 'nodrizas'):
			if id in self.poblacio:
				caries_primer.add(id)
		u.printTime('odn_peces')
		
		self.treat_caries_protesis(odn508, caries_primer, tractaments)
		u.printTime('treat_caries_protesis')
		odn508 = None
		caries_primer = None
		tractaments = None

	def get_cancer(self):
		sql = """
			SELECT id_cip_sec,
			case when dde >= DATE '{LAST12}'
			then 1 else 0 end
			FROM eqa_problemes
			WHERE ps = 1049
			AND dde <= DATE '{DEXTD}'
		""".format(LAST12=self.menys1, DEXTD=self.DEXTD)
		for id, data in u.getAll(sql, 'nodrizas'):
			if id in self.poblacio:
				self.dades['cancer'][id] = 1
				if data:
					self.dades['cancer_12m'][id] = 1

	def get_iad0020(self):
		upload = []
		nens = c.defaultdict(dict)
		# Nens de 0-5 anys que tenen index COD positiu
		sql = "select distinct a.id_cip_sec, uporigen, uba, upinf, ubainf from nodrizas.assignada_tot a, \
				import.odn505 b where a.id_cip_sec = b.id_cip_sec and edat <= 5 and b.cfd_ind_cod > 0"
		for id, up, uba, upinf, ubainf in u.getAll(sql, "nodrizas"):
			self.dades['den_iad0020'][id] = 1
		
		nens_prob = c.defaultdict(dict)
		sql = "select id_cip_sec, tb_trac from import.odn511 where TB_TRAC IN ('FSC', 'CD', 'EHO')"
		for id, trac in u.getAll(sql, "import"):
			if trac == 'FSC':
				nens_prob[id][0] = 1
			elif trac == 'CD':
				nens_prob[id][1] = 1
			elif trac == 'EHO':
				nens_prob[id][2] = 1
		
		# Nens que hagin tingut FSC i (CD i/o EHO)
		for e in nens_prob.keys():
			if 0 in nens_prob[e] and nens_prob[e][0] == 1 and 1 in nens_prob[e] and 2 in nens_prob[e] and nens_prob[e][2] >= 1:
				self.dades['num_iad0020'][e] = 1

	def get_edats_khlx(self):
		sql = "select edat, khalix from khx_edats1a"
		self.edats1 = {edat: khalix for edat, khalix in u.getAll(sql, 'nodrizas')}
		sql = "select edat, khalix from khx_edats5a"
		self.edats5 = {edat: khalix for edat, khalix in u.getAll(sql, 'nodrizas')}

	def treat_variables(self, agr, ids):
		# self.variables[agr][id].add((valor, data))
		for id in ids:
			data_max = None
			for (valor, data) in ids[id]:
				if agr == 286 and data and u.monthsBetween(data, self.DEXTD) < 24:
					self.dades['286'][id] = valor
					if valor == 1501:
						self.dades['286=1501'][id] = 1
				elif agr == 308:
					if id in self.embarassades:
						inici = self.embarassades[id]['inici']
						fi = self.embarassades[id]['fi']
						if (not data_max and data and inici <= data <= fi) or (data_max and data and data > data_max and inici <= data <= fi):
							data_max = data
							self.dades['caods'][id] = valor
			if agr == 309:
				self.dades['revisio'][id] = 1

	def treat_caos(self, tractaments, patologies, absencies):
		for id in self.dades['revisio']:
			self.dades['307'][id] = 0
			self.dades['308'][id] = 0
			self.dades['307-308'][id] = 0
			self.dades['obt_307'][id] = 0
			self.dades['obt_308'][id] = 0
			self.dades['obturacions'][id] = 0
			if id in tractaments or id in patologies or id in absencies:
				peces = tractaments[id].viewkeys() | patologies[id].viewkeys() | absencies[id]
				for peca in peces:
					C,A,O= False,False,False
					max_pat,max_trat='',''
					agr= 308 if peca <= 50 else 307
					try:
						max_pat = max(patologies[id][peca].iteritems(), key=operator.itemgetter(1))[0]
					except:
						pass
					try:
						max_trat = max(tractaments[id][peca].iteritems(), key=operator.itemgetter(1))[0]
					except:
						pass
					C = max_pat in ["CA","RR","OF","OBTp"]
					A= peca in absencies[id] if C or max_pat == '' else False
					if not A:
						for pat,trat in pat_trat_tuples_list:
							if max_trat == 'OBTt' or (patologies[id][peca][pat] and tractaments[id][peca][trat] and \
							((max(patologies[id][peca][pat]) <= max(tractaments[id][peca][trat])) \
							or \
							(max(tractaments[id][peca][trat]) >= max(patologies[id][peca][max_pat])))):
								O = True
								if C:
									C = False
								if 'obt_'+str(agr) in self.dades and id in self.dades['obt_'+str(agr)]:
									self.dades['obt_'+str(agr)][id] += 1
									self.dades['obturacions'][id] += 1
								break
					if A:
						C = False
						if agr == 307:
							A = False
					if C or O or A:
						if str(agr) in self.dades and id in self.dades[str(agr)]:
							self.dades[str(agr)][id] += 1
							self.dades['307-308'][id] += 1
			if self.dades['307'][id] > 0:
				self.dades['307>0'][id] = 1
			elif self.dades['307'][id] == 0:
				self.dades['307=0'][id] = 1
			if self.dades['308'][id] > 0:
				self.dades['308>0'][id] = 1
			elif self.dades['308'][id] == 0:
				self.dades['308=0'][id] = 1

	def treat_caries_protesis(self, odn508, caries_primer, tractaments):
		for id in self.dades['revisio']:
			if id in caries_primer:
				self.dades['caries1'][id] = 1
			if id in odn508:
				for (peca, data, tract, pat) in odn508[id]:
					max_trat = max(tractaments[id][peca].iteritems(), key=operator.itemgetter(1))[0]
					if max_trat in ('PPR', 'PSR'):
						self.dades['protesi_removible'][id] = 1
					if peca in (16, 26, 36, 46) and tract in ('EX', 'EXO', 'OBT') and pat == 'CA':
						self.dades['caries1'][id] = 1

	def treat_peces(self, odn506):
		# id_cip_sec, dfd_p_a, dfd_cod_p, dfd_mot_a, dfd_estat
		# self.odn506[id].add((pa, codp, mota, estat))
		for id in odn506:
			self.dades['odn506'][id] = 1
			for (peca, peces, caries) in odn506[id]:
				if peces:
				# if pa == 'P' and peca not in (18, 28, 38, 48):
					if 'peces_propies' in self.dades and id in self.dades['peces_propies']:
						self.dades['peces_propies'][id] += 1
					else:
						self.dades['peces_propies'][id] = 1
				if caries:
				# if pa == 'P' and estat == 'C':
					self.dades['caries_notract'][id] = 1
					if peca > 48:
						self.dades['caries_notract_temp'][id] = 1
					if peca < 55:
						self.dades['caries_notract_def'][id] = 1

	def get_indicadors(self):
		self.dades['num_13'] = dict(list(self.dades['286=1501'].items() + self.dades['307>0'].items() + self.dades['308>0'].items()))
		self.dades['307-308=0'] = {id: 1 for id in self.dades['307=0'] if id in self.dades['308=0']}
		self.dades['307!=0'] = {id: 1 for id in self.dades['revisio'] if id not in self.dades['307=0']}
		self.dades['308!=0'] = {id: 1 for id in self.dades['revisio'] if id not in self.dades['308=0']}
		# self.dades['obturacions'] = dict(list(self.dades['obt_307'].items() + self.dades['obt_308'].items()))
		# self.dades['307-308'] = dict(list(self.dades['307'].items() + self.dades['308'].items()))
		self.dades['num_26'] = {id: 1 for id in self.dades['odn506'] if id not in self.dades['peces_propies']}
		self.dades['embaras_caods'] = {id: 1 for id in self.dades['embaras'] if id in self.dades['caods']}
		INDICADORS = {
			'IAD0011': {
				'NUM': self.dades['308'],
				'DEN': self.dades['revisio'],
				'type': 'MTJ',
				'edats': [6, 14],
				'edats_khlx': 1
			},
			'IAD0012': {
				'NUM': self.dades['307'],
				'DEN': self.dades['revisio'],
				'type': 'MTJ',
				'edats': [6, 14],
				'edats_khlx': 1
			},
			'IAD0013': {
				'NUM': self.dades['num_13'],
				'DEN': self.dades['revisio'],
				'type': 'PCT',
				'edats': [6, 14],
				'edats_khlx': 1
			},
			'IAD0014': {
				'NUM': self.dades['307-308=0'],
				'DEN': self.dades['revisio'],
				'type': 'PCT',
				'edats': [6, 14],
				'edats_khlx': 1
			},
			'IAD0015': {
				'NUM': self.dades['308=0'],
				'DEN': self.dades['revisio'],
				'type': 'PCT',
				'edats': [6, 14],
				'edats_khlx': 1
			},
			'IAD0016': {
				'NUM': self.dades['307=0'],
				'DEN': self.dades['revisio'],
				'type': 'PCT',
				'edats': [6, 14],
				'edats_khlx': 1
			},
			'IAD0017': {
				'NUM': self.dades['obturacions'],
				'DEN': self.dades['307-308'],
				'type': 'MTJ',
				'edats': [6, 14],
				'edats_khlx': 1
			},
			'IAD0018': {
				'NUM': self.dades['obt_308'],
				'DEN': self.dades['308'],
				'type': 'MTJ',
				'edats': [6, 14],
				'edats_khlx': 1
			},
			'IAD0019': {
				'NUM': self.dades['obt_307'],
				'DEN': self.dades['307'],
				'type': 'MTJ',
				'edats': [6, 14],
				'edats_khlx': 1
			},
			'IAD0020': {
				'NUM': self.dades['num_iad0020'],
				'DEN': self.dades['den_iad0020'],
				'type': 'PCT',
				'edats': [0, 5],
				'edats_khlx': 1
			},
			'IAD0021': {
				'NUM': self.dades['307'],
				'DEN': self.dades['revisio'],
				'type': 'MTJ',
				'edats': [1, 5],
				'edats_khlx': 1
			},
			'IAD0022': {
				'NUM': self.dades['307=0'],
				'DEN': self.dades['revisio'],
				'type': 'PCT',
				'edats': [1, 5],
				'edats_khlx': 1
			},
			'IAD0024': {
				'NUM': self.dades['caods'],
				'DEN': self.dades['embaras_caods'],
				'type': 'MTJ',
				'edats': [None, None],
				'edats_khlx': 5
			},
			'IAD0025': {
				'NUM': self.dades['peces_propies'],
				'DEN': self.dades['odn506'],
				'type': 'MTJ',
				'edats': [18, None],
				'edats_khlx': 5
			},
			'IAD0026': {
				'NUM': self.dades['num_26'],
				'DEN': self.dades['odn506'],
				'type': 'PCT',
				'edats': [35, None],
				'edats_khlx': 5
			},
			'IAD0027': {
				'NUM': self.dades['307'],
				'DEN': self.dades['307!=0'],
				'type': 'MTJ',
				'edats': [5, 5],
				'edats_khlx': 1
			},
			'IAD0028': {
				'NUM': self.dades['307'],
				'DEN': self.dades['307!=0'],
				'type': 'MTJ',
				'edats': [7, 7],
				'edats_khlx': 1
			},
			'IAD0029': {
				'NUM': self.dades['308'],
				'DEN': self.dades['308!=0'],
				'type': 'MTJ',
				'edats': [7, 7],
				'edats_khlx': 1
			},
			'IAD0030': {
				'NUM': self.dades['308'],
				'DEN': self.dades['308!=0'],
				'type': 'MTJ',
				'edats': [12, 12],
				'edats_khlx': 1
			},
			'IAD0031': {
				'NUM': self.dades['cancer'],
				'DEN': self.dades['assignada'],
				'type': 'PCT',
				'edats': [35, 64],
				'edats_khlx': 5
			},
			'IAD0032': {
				'NUM': self.dades['cancer_12m'],
				'DEN': self.dades['assignada'],
				'type': 'PCT',
				'edats': [35, 64],
				'edats_khlx': 5
			},
			'IAD0033': {
				'NUM': self.dades['caries1'],
				'DEN': self.dades['revisio'],
				'type': 'PCT',
				'edats': [6, 14],
				'edats_khlx': 1
			},
			'IAD0034': {
				'NUM': self.dades['307-308=0'],
				'DEN': self.dades['revisio'],
				'type': 'PCT',
				'edats': [15, None],
				'edats_khlx': 5
			},
			'IAD0035': {
				'NUM': self.dades['307'],
				'DEN': self.dades['revisio'],
				'type': 'MTJ',
				'edats': [15, None],
				'edats_khlx': 5
			},
			'IAD0036': {
				'NUM': self.dades['308'],
				'DEN': self.dades['revisio'],
				'type': 'MTJ',
				'edats': [15, None],
				'edats_khlx': 5
			},
			'IAD0037': {
				'NUM': self.dades['caries_notract_temp'],
				'type': 'PCT',
				'edats': [5, 5],
				'edats_khlx': 1
			},
			'IAD0038': {
				'NUM': self.dades['caries_notract'],
				'type': 'PCT',
				'edats': [7, 7],
				'edats_khlx': 1
			},
			'IAD0039': {
				'NUM': self.dades['caries_notract_def'],
				'type': 'PCT',
				'edats': [12, 12],
				'edats_khlx': 1
			},
			'IAD0040': {
				'NUM': self.dades['caries_notract_def'],
				'type': 'PCT',
				'edats': [35, 44],
				'edats_khlx': 5
			},
			'IAD0041': {
				'NUM': self.dades['protesi_removible'],
				'type': 'PCT',
				'edats': [None, None],
				'edats_khlx': 5
			}
		}
		inds_caries = ['IAD0037', 'IAD0038', 'IAD0039']
		resultat = c.Counter()
		res_ecap = c.defaultdict(c.Counter)
		res_pacient = list()
		pac_ecap = list()
		for ind, info in INDICADORS.items():
			num = info['NUM']
			den = info['DEN'] if 'DEN' in info else self.poblacio
			typ = info['type']
			(minedat, maxedat) = info['edats']
			edats_khlx = info['edats_khlx']
			for id in den:
				if id in self.poblacio:
					edat = self.poblacio[id]['edat'] if ind not in inds_caries else self.DEXTD.year - self.poblacio[id]['data']
					if (minedat and edat >= minedat and (not maxedat or (maxedat and edat <= maxedat))) or not minedat or (id < 0 and ind != 'IAD0026'):
						hash, sector = self.id_hash[id]
						up = self.poblacio[id]['up']
						uba = self.poblacio[id]['uba']
						upinf = self.poblacio[id]['upinf']
						ubainf = self.poblacio[id]['ubainf']
						sexe = self.poblacio[id]['sexe']
						ates = self.poblacio[id]['ates']
						if edats_khlx == 1:
							khalix = self.edats1
						else:
							khalix = self.edats5
						if ind in ('IAD0017', 'IAD0018', 'IAD0019') and ates:
							resultat[(ind, up, sexe, khalix[edat], 'DEN')] += den[id]
							res_ecap[(up, ind)]['DEN'] += den[id]
							# res_ecap[(upinf, ubainf, 'I', ind)]['DEN'] += den[id]
						elif ind in ('IAD0025', 'IAD0026', 'IAD0031', 'IAD0032', 'IAD0037', 'IAD0038', 'IAD0039', 'IAD0040'):
							resultat[(ind, up, sexe, khalix[edat], 'DEN')] += 1
							res_ecap[(up, ind)]['DEN'] += 1
							# res_ecap[(upinf, ubainf, 'I', ind)]['DEN'] += 1
						elif ates:
							resultat[(ind, up, sexe, khalix[edat], 'DEN')] += 1
							res_ecap[(up, ind)]['DEN'] += 1
							# res_ecap[(upinf, ubainf, 'I', ind)]['DEN'] += 1
						
						if id in num and ind in ('IAD0025', 'IAD0026', 'IAD0031', 'IAD0032', 'IAD0037', 'IAD0038', 'IAD0039', 'IAD0040'):
							if typ == 'PCT':
								resultat[(ind, up, sexe, khalix[edat], 'NUM')] += 1
								res_ecap[(up, ind)]['NUM'] += 1
								# res_ecap[(upinf, ubainf, 'I', ind)]['NUM'] += 1
							else:
								resultat[(ind, up, sexe, khalix[edat], 'NUM')] += num[id]
								res_ecap[(up, ind)]['NUM'] += num[id]
								# res_ecap[(upinf, ubainf, 'I', ind)]['NUM'] += num[id]
						elif id in num and ates:
							if typ == 'PCT':
								resultat[(ind, up, sexe, khalix[edat], 'NUM')] += 1
								res_ecap[(up, ind)]['NUM'] += 1
								# res_ecap[(upinf, ubainf, 'I', ind)]['NUM'] += 1
							else:
								resultat[(ind, up, sexe, khalix[edat], 'NUM')] += num[id]
								res_ecap[(up, ind)]['NUM'] += num[id]
								# res_ecap[(upinf, ubainf, 'I', ind)]['NUM'] += num[id]
						elif id not in num:
							res_pacient.append((hash, sector, ind, up))

			u.printTime(ind)
		cols = "(indicador varchar(13), up varchar(10), sexe varchar(1), edat varchar(10), analisi varchar(3), valor int)"
		u.createTable('iads', cols, 'altres', rm=True)
		upload = []
		for (ind, up, sexe, edat, analisi), val in resultat.items():
			upload.append((ind, up, sexe, edat, analisi, val))
		u.listToTable(upload, 'iads', 'altres')

		cols = "(up varchar(10), indicador varchar(13), uba varchar(20), tipus varchar(1), numerador int, denominador int, resultat double)"
		u.createTable('exp_ecap_index_caries_uba', cols, 'altres', rm=True)
		upload_ecap = []
		for (up, ind) in res_ecap:
			num = res_ecap[(up, ind)]['NUM'] if 'NUM' in res_ecap[(up, ind)] else 0
			den = res_ecap[(up, ind)]['DEN'] if 'DEN' in res_ecap[(up, ind)] else 0
			res = float(num)/float(den) if den else 0
			# if up not in self.jail_centres:
			# 	upload_ecap.append((up, ind, uba, tipus, num, den, res))
			if up in self.jail_centres and ind in indicadors_jail:
				upload_ecap.append((up, ind+'PRS', 'sense', 'O', num, den, res))
			else:
				upload_ecap.append((up, ind, 'sense', 'O', num, den, res))
		u.listToTable(upload_ecap, 'exp_ecap_index_caries_uba', 'altres')

		cols = "(hash varchar(100), sector int, indicador varchar(15), up varchar(5), uba varchar(5), upinf varchar(5), ubainf varchar(5), exclos int)"	
		u.createTable('exp_ecap_index_caries_pacient', cols, 'altres', rm=True)
		upload_pacient = []
		for (hash, sector, ind, up) in res_pacient:
			# if up not in self.jail_centres:
			# 	upload_pacient.append((hash, sector, ind, up, 'sense', 'sense', None, 0))
			if up in self.jail_centres and ind in indicadors_jail:
				upload_pacient.append((hash, sector, ind+'PRS', up, 'sense', 'sense', None, 0))
			elif ind in TE_LLISTAT:
				upload_pacient.append((hash, sector, ind, up, 'sense', 'sense', None, 0))
		u.listToTable(upload_pacient, 'exp_ecap_index_caries_pacient', 'altres')


	def export_khalix(self):

		# Indicadors Jail
		sql = """
				SELECT
					CONCAT(indicador, 'PRS'),
					CONCAT('A', 'periodo'),
					ics_codi,
					analisi,
					'NOCAT',
					'NOIMP',
					'DIM6SET',
					'N',
					sum(valor)
				FROM
					{_db}.{_taula} h,
					nodrizas.jail_centres CC
				WHERE
					CC.scs_codi = H.up
					AND indicador in {_indicadors_jail}
				GROUP BY
					indicador,
					ics_codi,
					analisi
			""".format(_db='altres', _taula='iads', _indicadors_jail=indicadors_jail)
		
		file = "INDEX_CARIES_JAIL"
		u.exportKhalix(sql, file)

		# # Indicadors AP
		edats1 = ('IAD0011', 'IAD0012', 'IAD0013', 'IAD0014', 'IAD0015', 'IAD0016', 'IAD0017', 'IAD0018', 'IAD0019',
				'IAD0020', 'IAD0021', 'IAD0022', 'IAD0027', 'IAD0028', 'IAD0029', 'IAD0030', 'IAD0033', 'IAD0037', 
				'IAD0038',  'IAD0039')
		sql = """
				SELECT
					indicador,
					CONCAT('A', 'periodo'),
					ics_codi,
					analisi,
					edat,
					'NOIMP',
					if(sexe='H', 'HOME', 'DONA'),
					'N',
					valor
				FROM
					{_db}.{_taula} h,
					nodrizas.cat_centres CC
				WHERE
					CC.scs_codi = H.up
					and indicador in {inds}
					and sexe in ('H', 'D')
			""".format(_db='altres', _taula='iads', inds=tuple(edats1))

		file = "IADODOPED"
		u.exportKhalix(sql, file)

		sql = """
				SELECT
					indicador,
					CONCAT('A', 'periodo'),
					ics_codi,
					analisi,
					edat,
					'NOIMP',
					if(sexe='H', 'HOME', 'DONA'),
					'N',
					valor
				FROM
					{_db}.{_taula} h,
					nodrizas.cat_centres CC
				WHERE
					CC.scs_codi = H.up
					and indicador not in {inds}
					and sexe in ('H', 'D')
			""".format(_db='altres', _taula='iads', inds=tuple(edats1))

		file = "IADODOAD"
		u.exportKhalix(sql, file)

IAD()