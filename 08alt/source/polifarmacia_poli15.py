# crontab p2262
# 0 7 2 * * docker compose -f /home/sisap/sisap/docker-compose.yml exec -T sisap python /sisap/08alt/source/polifarmacia_poli15.py

import sisaptools as u
import sisapUtils as u2


origen = "dwcatsalut.indicadors_catsalut"
indicador = "POLIMED15"
desti = "POLIMED"

sql = "select a.mes, b.br, a.numerador, a.denominador \
       from {0} a \
       inner join dbc_centres b on a.up = b.up_cod \
       where indicador = '{1}' and \
             mes = (select max(mes) from {0} \
                    where indicador = '{1}')".format(origen, indicador)
upload = []
for mes, entitat, num, den in u.Database("exadata", "data").get_all(sql):
    periode = "A" + mes[2:]
    for analisi, valor in (("NUM", num), ("DEN", den)):
        this = (indicador, periode, entitat, analisi, "NOCAT", "NOIMP",
                "DIM6SET", "N", valor)
        upload.append(this)

cols = ["k{} varchar(16)".format(i) for i in range(8)] + ["v int"]
with u.Database("p2262", "altres") as p2262:
    p2262.create_table(desti, cols)
    p2262.list_to_table(upload, desti)

u2.exportKhalix("select * from altres.{}".format(desti), desti, force=True)
mail = u.Mail()
mail.to.append("bpons@gencat.cat")
mail.cc.append("ffinaaviles@gencat.cat")
mail.subject = desti
mail.text = "S'ha generat l'arxiu {}.".format(desti)
mail.send()
