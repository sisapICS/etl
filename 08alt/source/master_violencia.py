# coding: latin1

import sisapUtils as u
import collections as c
import random
from dateutil import relativedelta as rd
import datetime as d
import sisaptools as t


RANDOM_NOISE = random.randint(1, 1000)
DEXTD = u.getOne("select data_ext from dextraccio", "nodrizas")[0]

DX = {
'C01-T74': "Ab�s, desatenci� i altres maltractaments de l'infant i l'adult, confirmats",
'T74': "Ab�s, desatenci� i altres maltractaments de l'infant i l'adult, confirmats",
'C01-T74.0': "Neglig�ncia o abandonament, confirmats",
'T74.0': "Neglig�ncia o abandonament, confirmats",
'C01-T74.01': "Neglig�ncia o abandonament de l'adult, confirmats",
'C01-T74.01XA': "Neglig�ncia o abandonament de l'adult, confirmats, assist�ncia inicial",
'C01-T74.01XD': "Neglig�ncia o abandonament de l'adult, confirmats, assist�ncia successiva",
'C01-T74.01XS': "Neglig�ncia o abandonament de l'adult, confirmats, seq�ela",
'C01-T74.02': "Neglig�ncia o abandonament de l'infant, confirmats",
'C01-T74.02XA': "Neglig�ncia o abandonament de l'infant, confirmats, assist�ncia inicial",
'C01-T74.02XD': "Neglig�ncia o abandonament de l'infant, confirmats, assist�ncia successiva",
'C01-T74.02XS': "Neglig�ncia o abandonament de l'infant, confirmats, seq�ela",
'C01-T74.1': "Maltractament f�sic, confirmat",
'T74.1': "Maltractament f�sic, confirmat",
'C01-T74.11': "Maltractament f�sic de l'adult, confirmat",
'C01-T74.11XA': "Maltractament f�sic de l'adult, confirmat, assist�ncia inicial",
'C01-T74.11XD': "Maltractament f�sic de l'adult, confirmat, assist�ncia successiva",
'C01-T74.11XS': "Maltractament f�sic de l'adult, confirmat, seq�ela",
'C01-T74.12': "Maltractament f�sic de l'infant, confirmat",
'C01-T74.12XA': "Maltractament f�sic de l'infant, confirmat, assist�ncia inicial",
'C01-T74.12XD': "Maltractament f�sic de l'infant, confirmat, assist�ncia successiva",
'C01-T74.12XS': "Maltractament f�sic de l'infant, confirmat, seq�ela",
'C01-T74.2': "Ab�s sexual, confirmat",
'T74.2': "Ab�s sexual, confirmat",
'C01-T74.21': "Ab�s sexual de l'adult, confirmat",
'C01-T74.21XA': "Ab�s sexual de l'adult, confirmat, assist�ncia inicial",
'C01-T74.21XD': "Ab�s sexual de l'adult, confirmat, assist�ncia successiva",
'C01-T74.21XS': "Ab�s sexual de l'adult, confirmat, seq�ela",
'C01-T74.22': "Ab�s sexual de l'infant, confirmat",
'C01-74.22XA': "Ab�s sexual de l'infant, confirmat, assist�ncia inicial",
'C01-T74.22XD': "Ab�s sexual de l'infant, confirmat, assist�ncia successiva",
'C01-T74.22XS': "Ab�s sexual de l'infant, confirmat, seq�ela",
'C01-T74.3': "Maltractament psicol�gic, confirmat",
'T74.3': "Maltractament psicol�gic, confirmat",
'C01-T74.31': "Maltractament psicol�gic de l'adult, confirmat",
'C01-T74.31XA': "Maltractament psicol�gic de l'adult, confirmat, assist�ncia inicial",
'C01-T74.31XD': "Maltractament psicol�gic de l'adult, confirmat, assist�ncia successiva",
'C01-T74.31XS': "Maltractament psicol�gic de l'adult, confirmat, seq�ela",
'C01-T74.32': "Maltractament psicol�gic de l'infant, confirmat",
'C01-T74.32XA': "Maltractament psicol�gic de l'infant, confirmat, assist�ncia inicial",
'C01-T74.32XD': "Maltractament psicol�gic de l'infant, confirmat, assist�ncia successiva",
'C01-T74.32XS': "Maltractament psicol�gic de l'infant, confirmat, seq�ela",
'C01-T74.4XXD': "S�ndrome de l'infant sacsejat, assist�ncia successiva",
'C01-T74.4XXS': "S�ndrome de l'infant sacsejat, seq�ela",
'C01-T74.4': "S�ndrome de l'infant sacsejat",
'C01-T74.4XXA': "S�ndrome de l'infant sacsejat, assist�ncia inicial",
'C01-T74.5': "Explotaci� sexual for�ada de l'infant i de l'adult , confirmada",
'C01-T74.51': "Explotaci� sexual for�ada de l'adult, confirmada ",
'C01-T74.51XA': "Explotaci� sexual for�ada de l'adult, confirmada, assist�ncia inicial",
'C01-T74.51XD': "Explotaci� sexual for�ada de l'adult, confirmada, assist�ncia succesiva",
'C01-T74.51XS': "Explotaci� sexual for�ada de l'adult, confirmada, seq�ela ",
'C01-T74.52': "Explotaci� sexual for�ada infantil, confirmada ",
'C01-T74.52XA': "Explotaci� sexual for�ada infantil, confirmada, assist�ncia inicial",
'C01-T74.52XD': "Explotaci� sexual for�ada infantil, confirmada, assist�ncia succesiva",
'C01-T74.52XS': "Explotaci� sexual for�ada infantil, confirmada, seq�ela ",
'C01-T74.6 ': "Explotaci� laboral for�ada de l'infant i de l'adult , confirmada",
'C01-T74.61': "Explotaci� laboral for�ada de l'adult, confirmada ",
'C01-T74.61XA': "Explotaci� laboral for�ada de l'adult, confirmada, assist�ncia inicial",
'C01-T74.61XD': "Explotaci� laboral for�ada de l'adult, confirmada, assist�ncia succesiva",
'C01-T74.61XS': "Explotaci� laboral for�ada de l'adult, confirmada, seq�ela ",
'C01-T74.62': "Explotaci� laboral for�ada infantil, confirmada ",
'C01-T74.62XA': "Explotaci� laboral for�ada infantil, confirmada, assist�ncia inicial",
'C01-T74.62XD': "Explotaci� laboral for�ada infantil, confirmada, assist�ncia succesiva",
'C01-T74.62XS': "Explotaci� laboral for�ada infantil, confirmada, seq�ela",
'T74.8': "Altres s�ndromes de maltractament ",
'C01-T74.9': "Maltractament no especificat, confirmat",
'T74.9': "Maltractament no especificat, confirmat",
'C01-T74.91': "Maltractament de l'adult no especificat, confirmat",
'C01-T74.91XA': "Maltractament de l'adult no especificat, confirmat, assist�ncia inicial",
'C01-T74.91XD': "Maltractament de l'adult no especificat, confirmat, assist�ncia successiva",
'C01-T74.91XS': "Maltractament de l'adult no especificat, confirmat, seq�ela",
'C01-T74.92': "Maltractament de l'infant no especificat, confirmat",
'C01-T74.92XA': "Maltractament de l'infant no especificat, confirmat, assist�ncia inicial",
'C01-T74.92XD': "Maltractament de l'infant no especificat, confirmat, assist�ncia successiva",
'C01-T74.92XS': "Maltractament de l'infant no especificat, confirmat, seq�ela",
'C01-N90.81': "Estat de mutilaci� genital femenina",
'N90.81': "Estat de mutilaci� genital femenina",
'C01-N90.810': "Estat de mutilaci� genital femenina, no especificat",
'C01-N90.811': "Estat de mutilaci� genital femenina, tipus I",
'C01-N90.812': "Estat de mutilaci� genital femenina, tipus II",
'C01-N90.813': "Estat de mutilaci� genital femenina, tipus III",
'C01-N90.818': "Altres estats de mutilaci� genital femenina",
'C01-T76': "Ab�s, desatenci� i altres maltractaments de l'infant i l'adult, sospitats",
'C01-T76.0': "Desatenci� o abandonament, sospitats",
'C01-T76.01': "Desatenci� o abandonament de l'adult, sospitats",
'C01-T76.01XA': "Desatenci� o abandonament de l'adult, sospitats, A",
'C01-T76.01XD': "Desatenci� o abandonament de l'adult, sospitats, D",
'C01-T76.01XS': "Desatenci� o abandonament de l'adult, sospitats, S",
'C01-T76.02': "Desatenci� o abandonament de l'infant, sospitats",
'C01-T76.02XA': "Desatenci� o abandonament de l'infant, sospitats, A",
'C01-T76.02XD': "Desatenci� o abandonament de l'infant, sospitats, D",
'C01-T76.02XS': "Desatenci� o abandonament de l'infant, sospitats, S",
'C01-T76.1': "Maltractament f�sic, sospitat",
'C01-T76.11': "Maltractament f�sic de l'adult, sospitat",
'C01-T76.11XA': "Maltractament f�sic de l'adult, sospitat, A",
'C01-T76.11XD': "Maltractament f�sic de l'adult, sospitat, D",
'C01-T76.11XS': "Maltractament f�sic de l'adult, sospitat, S",
'C01-T76.12': "Maltractament f�sic de l'infant, sospitat",
'C01-T76.12XA': "Maltractament f�sic de l'infant, sospitat, A",
'C01-T76.12XD': "Maltractament f�sic de l'infant, sospitat, D",
'C01-T76.12XS': "Maltractament f�sic de l'infant, sospitat, S",
'C01-T76.2': "Ab�s sexual, sospitat",
'C01-T76.21': "Ab�s sexual de l'adult, sospitat",
'C01-T76.21XA': "Ab�s sexual de l'adult, sospitat, A",
'C01-T76.21XD': "Ab�s sexual de l'adult, sospitat, D",
'C01-T76.21XS': "Ab�s sexual de l'adult, sospitat, S",
'C01-T76.22': "Ab�s sexual de l'infant, sospitat",
'C01-T76.22XA': "Ab�s sexual de l'infant, sospitat, A",
'C01-T76.22XD': "Ab�s sexual de l'infant, sospitat, D",
'C01-T76.22XS': "Ab�s sexual de l'infant, sospitat, S",
'C01-T76.3': "Maltractament psicol�gic, sospitat",
'C01-T76.31': "Maltractament psicol�gic de l'adult, sospitat",
'C01-T76.31XA': "Maltractament psicol�gic de l'adult, sospitat, A",
'C01-T76.31XD': "Maltractament psicol�gic de l'adult, sospitat, D",
'C01-T76.31XS': "Maltractament psicol�gic de l'adult, sospitat, S",
'C01-T76.32': "Maltractament psicol�gic de l'infant, sospitat",
'C01-T76.32XA': "Maltractament psicol�gic de l'infant, sospitat, A",
'C01-T76.32XD': "Maltractament psicol�gic de l'infant, sospitat, D",
'C01-T76.32XS': "Maltractament psicol�gic de l'infant, sospitat, S",
'C01-T76.5': "Explotaci� sexual for�ada, sospitada ",
'C01-T76.51': "Explotaci� sexual for�ada de l'adult, sospitada ",
'C01-T76.51XA': "Explotaci� sexual for�ada de l'adult, sospitada, A ",
'C01-T76.51XD': "Explotaci� sexual for�ada de l'adult, sospitada, D ",
'C01-T76.51XS': "Explotaci� sexual for�ada de l'adult, sospitada, S ",
'C01-T76.52': "Explotaci� sexual de l'infant, sospitada ",
'C01-T76.52XA': "Explotaci� sexual de l'infant, sospitada, A", 
'C01-T76.52XD': "Explotaci� sexual de l'infant, sospitada, D ",
'C01-T76.52XS': "Explotaci� sexual de l'infant, sospitada, S ",
'C01-T76.6': "Explotaci� laboral for�ada, sospitada ",
'C01-T76.61': "Explotaci� laboral for�ada de l'adult, sospitada ",
'C01-T76.61XA': "Explotaci� laboral for�ada de l'adult, sospitada, A ",
'C01-T76.61XD': "Explotaci� laboral for�ada de l'adult, sospitada, D ",
'C01-T76.61XS': "Explotaci� laboral for�ada de l'adult, sospitada, S ",
'C01-T76.62': "Explotaci� laboral for�ada de l'infant, sospitada ",
'C01-T76.62XA': "Explotaci� laboral for�ada de l'infant, sospitada, A", 
'C01-T76.62XD': "Explotaci� laboral for�ada de l'infant, sospitada, D ",
'C01-T76.62XS': "Explotaci� laboral for�ada de l'infant, sospitada, S ",
'C01-T76.9': "Maltractament no especificat, sospitat",
'C01-T76.91': "Maltractament de l'adult no especificat, sospitat",
'C01-T76.91XA': "Maltractament de l'adult no especificat, sospitat, A",
'C01-T76.91XD': "Maltractament de l'adult no especificat, sospitat, D",
'C01-T76.91XS': "Maltractament de l'adult no especificat, sospitat, S",
'C01-T76.92': "Maltractament de l'infant no especificat, sospitat",
'C01-T76.92XA': "Maltractament de l'infant no especificat, sospitat, A",
'C01-T76.92XD': "Maltractament de l'infant no especificat, sospitat, D",
'C01-T76.92XS': "Maltractament de l'infant no especificat, sospitat, S",
'Z04.4': "Assist�ncia per a exploraci� i observaci� consecutives a presumpta violaci�",
'C01-Z04.4': "Assist�ncia per a exploraci� i observaci� consecutives a presumpta violaci�",
'C01-Z04.41': "Assist�ncia per a exploraci� i observaci� consecutives a presumpta violaci� de l'adult",
'C01-Z04.42': "Assist�ncia per a exploraci� i observaci� consecutives a presumpta violaci� de l'infant",
'Z91.41': "Antecedents personals d'ab�s o maltractament en l'edat adulta",
'C01-Z91.41': "Antecedents personals d'ab�s o maltractament en l'edat adulta",
'C01-Z91.410': "Antecedents personals d'ab�s sexual o maltractament f�sic en l'edat adulta",
'C01-Z91.411': "Antecedents personals de maltractament psicol�gic en l'edat adulta",
'C01-Z91.412': "Antecedents personals de desatenci� en l'edat adulta",
'C01-Z91.419': "Antecedents personals d'ab�s o maltractament no especificat en l'edat adulta",
'C01-Z91.42': "Antecedents personals d'explotaci� laboral o sexual for�ada",
'C01-O9A.3': "Maltractament f�sic que complica l'embar�s, el part i el puerperi",
'C01-O9A.31': "Maltractament f�sic que complica l'embar�s",
'C01-O9A.311': "Maltractament f�sic que complica l'embar�s, primer trimestre",
'C01-O9A.312': "Maltractament f�sic que complica l'embar�s, segon trimestre",
'C01-O9A.313': "Maltractament f�sic que complica l'embar�s, tercer trimestre",
'C01-O9A.319': "Maltractament f�sic que complica l'embar�s, trimestre no especificat",
'C01-O9A.32': "Maltractament f�sic que complica el part",
'C01-O9A.33': "Maltractament f�sic que complica el puerperi",
'C01-O9A.4': "Ab�s sexual que complica l'embar�s, el part i el puerperi",
'C01-O9A.41': "Ab�s sexual que complica l'embar�s",
'C01-O9A.411': "Ab�s sexual que complica l'embar�s, primer trimestre",
'C01-O9A.412': "Ab�s sexual que complica l'embar�s, segon trimestre",
'C01-O9A.413': "Ab�s sexual que complica l'embar�s, tercer trimestre",
'C01-O9A.419': "Ab�s sexual que complica l'embar�s, trimestre no especificat",
'C01-O9A.5': "Maltractament psicol�gic que complica l'embar�s, el part i el puerperi",
'C01-O9A.51': "Maltractament psicol�gic que complica l'embar�s",
'C01-O9A.511': "Maltractament psicol�gic que complica l'embar�s, primer trimestre",
'C01-O9A.512': "Maltractament psicol�gic que complica l'embar�s, segon trimestre",
'C01-O9A.513': "Maltractament psicol�gic que complica l'embar�s, tercer trimestre",
'C01-O9A.519': "Maltractament psicol�gic que complica l'embar�s, trimestre no especificat",
'C01-O9A.52': "Maltractament psicol�gic que complica el part",
'C01-O9A.53': "Maltractament psicol�gic que complica el puerperi",
'C01-Z04.7': "Assist�ncia per a exploraci� i observaci� consecutives a presumpte maltractament f�sic",
'C01-Z04.71': "Ass. per a exploraci� i obs. consecutives a presumpte maltractament f�sic de l'adult",
'C01-Z04.72': "Ass. per a exploraci� i obs. consecutives a presumpte maltractament f�sic de l'infant",
'C01-Z69': "Assist�ncia per a serveis de salut mental per a v�ctima i autor d'ab�s o maltractament",
'C01-Z69.0': "Ass. per a serveis de salut mental per a problemes d'ab�s o maltractament de l'infant",
'C01-Z69.01': "Assist�ncia per a serveis de salut mental per a ab�s o maltractament parental de l'infant",
'C01-Z69.010': "Ass. per a serveis de salut mental per a v�ctima d'ab�s o maltr. parental de l'infant",
'C01-Z69.02': "Ass. per a serveis de salut mental per a ab�s o maltractament no parental de l'infant",
'C01-Z69.020': "Ass. per a serveis de salut mental per a v�ctima d'ab�s o maltr. no parental de l'infant",
'C01-Z69.1': "Ass. per a serv. de salut mental per a problemes d'ab�s o maltr. del c�njuge o la parella",
'C01-Z69.11': "Ass. per a serveis de salut mental per a v�ctima d'ab�s o maltr. pel c�njuge o la parella",
'C01-Z69.8': "Ass. per a serveis de salut mental per a v�ctima o autor d'AT d'ab�s o maltractament",
'C01-Z69.81': "Ass. per a serveis de salut mental per a v�ctima d'altres tipus d'ab�s o maltractament",
'C01-Z56.81': "Assetjament sexual a la feina",
'Z61.6 ': "Problemes relacionats amb el maltractament f�sic del nen",
'C01-Z62.81': "Antecedents personals d'ab�s o maltractament en la inf�ncia",
'C01-Z62.810': "Antecedents personals d'ab�s sexual o maltractament f�sic en la inf�ncia",
'C01-Z62.811': "Antecedents personals de maltractament psicol�gic en la inf�ncia",
'C01-Z62.812': "Antecedents personals de desatenci� en la inf�ncia",
'C01-Z62.813': "Antecedents personals d'explotaci� laboral o sexual for�ada en la inf�ncia ",
'C01-Z62.819': "Antecedents personals d'ab�s o maltractament no especificats en la inf�ncia",
'Y07': "ALTRES S�NDROMES DE MALTRACTAMENT",
'Y07.0': "S�NDROMES DE MALTRACTAMENT PER L'ESP�S O LA PARELLA",
'Y07.1': "S�NDROMES DE MALTRACTAMENT PEL PARE O LA MARE",
'Y07.2': "S�NDROMES DE MALTRACTAMENT PER UN CONEGUT O AMIC",
'Y07.3': "S�NDROMES DE MALTRACTAMENT PER AUTORIDADES OFICIALES",
'Y07.8': "S�NDROMES DE MALTRACTAMENT PER UNA ALTRA PERSONA ESPECIFICADA",
'Y07.9': "S�NDROMES DE MALTRACTAMENT PER UNA ALTRA PERSONA NO ESPECIFICADA"}

maltractament_infantil = [
    'C01-Z62.81',
    'C01-Z62.810',
    'C01-Z62.811',
    'C01-Z62.812',
    'C01-Z62.813',
    'C01-Z62.819'
]

def worker(params):
    """."""
    return([row for row in u.getAll(*params)])

def treat_vmasclista(dx):
    flag = 0
    if dx[:3] == 'T74' or dx[:7] == 'C01-T74':
        flag = 1
    elif dx[:6] == 'N90.81' or dx[:10] == 'C01-N90.81':
        flag = 1
    elif dx[:3] == 'O9A' or dx[:7] == 'C01-O9A':
        flag = 1
    elif dx[:3] == 'T76' or dx[:7] == 'C01-T76':
        flag = 1
    return flag

def grau_sospita(dx):
    grau = ''
    if dx[:3] == 'T74' or dx[:7] == 'C01-T74' or dx[:3] == 'N90' or dx[:7] == 'C01-N90' or dx[:7] == 'C01-O9A':
        grau = 'CONFIRMAT'
    elif dx[:3] == 'T76' or dx[:7] == 'C01-T76' or dx[:3] == 'Z04' or dx[:7] == 'C01-Z04' or dx[:7] == 'C01-Z69' or dx[:3] == 'Z61' or dx[:7] == 'C01-Z61' or dx[:7] == 'C01-Z56':
        grau = 'SOSPITA'
    elif dx[:3] == 'T91' or dx[:7] == 'C01-Z91' or dx[:7] == 'C01-Z62':
        grau = 'ANTECEDENT'
    return grau

def tipus_violencia_class(dx):
    tipus = 'No especificada'
    if dx[:5] == 'T74.0' or dx[:9] == 'C01-T74.0' or dx[:5] == 'T76.0' or dx[:9] == 'C01-T76.0' or dx == 'C01-Z91.412' or dx == 'C01-Z62.812':
        tipus = 'Neglig�ncia, desatenci� o abandonament'
    elif dx[:5] == 'T74.1' or dx[:9] == 'C01-T74.1' or dx[:9] == 'C01-T74.4' or dx[:5] == 'T76.1' or dx[:9] == 'C01-T76.1' or dx == 'C01-Z61.6' or dx == 'Z61.6' or dx[:5] == 'Z04.7' or dx[:9] == 'C01-Z04.7' or dx[:5] == 'O9A.3' or dx[:9] == 'C01-O9A.3':
        tipus = 'Maltractament f�sic'
    elif dx[:5] == 'T74.3' or dx[:9] == 'C01-T74.3' or dx[:5] == 'T76.3' or dx[:9] == 'C01-T76.3' or dx[:5] == 'O9A.5' or dx[:9] == 'C01-O9A.5' or dx == 'C01-Z91.411' or dx == 'C01-Z62.811':
        tipus = 'Maltractament psicol�gic'
    elif dx[:5] == 'T74.2' or dx[:9] == 'C01-T74.2' or dx[:5] == 'T76.2' or dx[:9] == 'C01-T76.2' or dx[:5] == 'O9A.4' or dx[:9] == 'C01-O9A.4' or dx[:5] == 'Z04.4' or dx[:9] == 'C01-Z04.4' or dx == 'C01-Z56.81':
        tipus = 'Viol�ncia sexual'
    elif dx[:5] == 'T74.5' or dx[:9] == 'C01-T74.5' or dx[:5] == 'T76.5' or dx[:9] == 'C01-T76.5':
        tipus = 'Explotaci� sexual'
    elif dx[:5] == 'T74.6' or dx[:9] == 'C01-T74.6' or dx[:5] == 'T76.6' or dx[:9] == 'C01-T76.6':
        tipus = 'Explotaci� laboral'
    elif dx[:3] == 'N90' or dx[:7] == 'C01-N90':
        tipus = 'Mutilaci� genital femenina'
    return tipus


# def treat_tractaments(data_dx, data_baixa, tractaments):
#     if data_baixa is None:
#         data_baixa = DEXTD
#     flag_tractament = 0
#     for (talta, tbaixa) in tractaments:
#         if tbaixa is None:
#             tbaixa = DEXTD
#         if talta <= data_dx and tbaixa >= data_baixa:
#             flag_tractament = 1
#         elif talta >= data_dx and talta <= data_baixa:
#             flag_tractament = 1
#         elif tbaixa >= data_dx and tbaixa <= data_baixa:
#             flag_tractament = 1
#     return flag_tractament

def treat_visites(data_dx, visites):
    total_visites = 0
    dx_12 = data_dx - rd.relativedelta(years=1)
    for data in visites:
        if isinstance(data, d.datetime):
            data = data.date()
        if isinstance(dx_12, d.datetime):
            dx_12 = dx_12.date()
        if isinstance(data_dx, d.datetime):
            data_dx = data_dx.date()
        if dx_12 <= data <= data_dx:
            total_visites += 1
    return total_visites

def treat_comunicats(data_dx, comunicats):
    flag = 0
    total = 0
    for data_comunicat in comunicats:
        if abs(u.daysBetween(data_dx, data_comunicat)) <= 5:
            flag = 1
        if u.daysBetween(data_comunicat, data_dx) > 5:
            total += 1
    return (flag, total)

def treat_embaras(data_dx, embarassos):
    # (inici, fi, tanca, dur)
    flag_embaras = 0
    flag_puerperi = 0
    for (inici, fi) in embarassos:
        if fi is None:
            fi = DEXTD
        if inici <= data_dx and fi >= data_dx:
            flag_embaras = 1
        puerperi_mes = fi + rd.relativedelta(days=90)
        if puerperi_mes >= data_dx and data_dx >= fi:
            flag_puerperi = 1
    if flag_puerperi == 1 and flag_embaras == 1:
        flag_embaras = 0
    return (flag_embaras, flag_puerperi)

class Master_violencia():
    def __init__(self):
        cols = """( id int,
                    edat int, 
                    sexe varchar2(3), 
                    nacionalitat varchar2(3),
                    provincia_assig  varchar2(3), 
                    regio varchar2(3), 
                    regio_desc varchar2(300), 
                    tipus_atencio  varchar2(300), 
                    codi_dx varchar2(30), 
                    data_dx date, 
                    data_baixa_dx date, 
                    edat_dx int, 
                    grau varchar2(30),
                    tipus_violencia varchar2(300),
                    n_visites_ap int,
                    flag_psicofarmacs int,
                    flag_polimedicacio int,
                    flag_maltracte_infantil int,
                    flag_antecedents_maltracte_infantil int,
                    flag_comunicat_lesions_associat int,
                    total_comunicats_lesions_previs int,
                    flag_embaras int,
                    flag_puerperi int,
                    flag_masclista int,
                    data_ultim_PVS date,
                    valor_ultim_PVS int,
                    data_ultim_RVG date,
                    valor_ultim_RVG int,
                    data_ultim_benestar_emocional date,
                    valor_ultim_benestar_emocional int,
                    data_ultim_alcohol date,
                    valor_ultim_alcohol int,
                    data_ultim_opioides date,
                    valor_ultim_opioides int,
                    data_ultim_cannabis date,
                    valor_ultim_cannabis int,
                    data_ultim_sedants date,
                    valor_ultim_sedants int,
                    data_ultim_cocaina date,
                    valor_ultim_cocaina int,
                    data_ultim_estimulants date,
                    valor_ultim_estimulants int,
                    data_ultim_alucinogens date,
                    valor_ultim_alucinogens int,
                    data_ultim_tabac date,
                    valor_ultim_tabac int,
                    data_ultim_inhalants date,
                    valor_ultim_inhalants int,
                    data_ultim_altres_drogues date,
                    valor_ultim_altres_drogues int,
                    data_ultim_seguiment_professional date,
                    valor_ultim_seguiment_professional int,
                    data_ultim_utilitzacio_recursos date,
                    valor_ultim_utilitzacio_recursos int,
                    data_ultim_denuncia date,
                    valor_ultim_denuncia int,
                    data_ultim_kit_emergencia date,
                    valor_ultim_kit_emergencia int,
                    data_ultim_risc_suicidi date,
                    val_ultim_risc_suicidi int
                    )"""

        u.createTable('master_violencia', cols, 'exadata', rm=True)
        u.grantSelect('master_violencia', 'DWSISAP_ROL', 'exadata')
        self.get_dx()
        self.get_tractaments()
        self.get_variables()
        self.get_comunicats()
        self.get_embaras()
        self.get_visites()
        self.get_socio_demografic()
        self.get_catalegs()
        self.upload_data()
    
    def get_dx(self):
        print('problemes')
        sql = "show create table problemes"
        tables = u.getOne(sql, "import")[1].split("UNION=(")[1][:-1].split(",")
        sql = """select id_cip, pr_cod_ps, pr_dde, pr_data_baixa from {}"""
        jobs = [(sql.format(table), "import") for table in tables]
        self.dx = c.defaultdict(set)
        for dxs in u.multiprocess(worker, jobs):
            for (id, ps, dda, ddb) in dxs:
                if ps in DX.keys():
                    self.dx[id].add((ps, dda, ddb))
    
    def get_tractaments(self):
        print('tractaments')
        sql = "show create table tractaments"
        tables = u.getOne(sql, "import")[1].split("UNION=(")[1][:-1].split(",")
        sql = """select id_cip, ppfmc_pmc_data_ini, ppfmc_data_fi from {} where ppfmc_atccodi like 'N05%' or ppfmc_atccodi like 'N06%' and ppfmc_pmc_data_ini >= DATE '2019-01-01'"""
        jobs = [(sql.format(table), "import") for table in tables]
        self.tractaments = c.defaultdict(set)
        for tractaments in u.multiprocess(worker, jobs):
            for (id, dda, ddb) in tractaments:
                self.tractaments[id].add((dda, ddb))
        
        # polimedicacio
        sql = """
            SELECT id_cip
            FROM {tb} t
            WHERE ppfmc_data_fi > DATE '2019-01-01'
            AND (ppfmc_durada > 360 OR ppfmc_seg_evol = 'S')
            AND length(ppfmc_atccodi) = 7
            AND NOT EXISTS
                (SELECT 1
                FROM cat_cpftb006 c
                WHERE
                c.pf_codi = t.ppfmc_pf_codi
                AND (c.pf_ff_codi in ('GE','PO','CL')
                OR c.pf_via_adm = 'B31'
                OR c.pf_gt_codi like '23C%')) 
        """
        self.polimedicats = c.Counter()
        jobs = [(sql.format(tb=table), "import") for table in tables]
        # self.tractaments = c.defaultdict(set)
        for tractaments in u.multiprocess(worker, jobs):
            for id in tractaments:
                self.polimedicats[id] += 1
    
    def get_variables(self):
        print('variables')
        sql = "show create table variables"
        tables = u.getOne(sql, "import")[1].split("UNION=(")[1][:-1].split(",")
        variables = [
            'EP3001',
            'VP3001',
            'EZ5101',
            'VP2003',
            'VP2101',
            'VP2201',
            'VP2301',
            'VP2401',
            'VP2501',
            'VP2601',
            'VP2705',
            'VP2801',
            'VP2901',
            'AZ2105',
            'AZ2106',
            'AZ2104',
            'AZ2102',
            'PP1001'
        ]
        sql = """select id_cip, vu_cod_vs, vu_dat_act, vu_val from {tb} where vu_cod_vs in {variables} and vu_dat_act >= DATE '2018-01-01'"""
        jobs = [(sql.format(tb=table,variables=tuple(variables)), "import") for table in tables]
        self.variables = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict()))
        for variables in u.multiprocess(worker, jobs):
            for (id, variable, dda, val) in variables:
                self.variables[id][variable][dda] = val

    def get_comunicats(self):
        print('comunicats')
        sql = "show create table comunicats"
        tables = u.getOne(sql, "import")[1].split("UNION=(")[1][:-1].split(",")
        sql = """select id_cip, com_data from {} where com_incident = 'AGR' and com_data >= DATE '2019-01-01'"""
        jobs = [(sql.format(table), "import") for table in tables]
        self.comunicats = c.defaultdict(set)
        for comunicats in u.multiprocess(worker, jobs):
            for (id, data) in comunicats:
                self.comunicats[id].add(data)

    def get_embaras(self):
        print('embaras')

        self.conversor = {}
        sql = """select id_cip, id_cip_sec from u11"""
        for id_cip, id_cip_sec in u.getAll(sql, 'import'):
            self.conversor[id_cip_sec] = id_cip

        sql = """select id_cip_sec, inici, fi, emb_c_tanca from ass_embaras where inici >= DATE '2018-04-01'"""
        self.embarassos = c.defaultdict(set)
        for (id, inici, fi, motiu) in u.getAll(sql, 'nodrizas'):
            try:
                if motiu == 'Tr':
                    fi = None
                self.embarassos[self.conversor[id]].add((inici, fi))
            except:
                pass
        # #  visita de puerperi
        #  sql = """select id_cip, pavi_data_a from assir221 where pavi_seg_tipus in ('PSD', 'PSC', 'PDOM', 'CONS', 'VTEL', 'VC')"""
        #  self.puerperi = c.defaultdict(set)
        #  for id, data in u.getAll(sql, 'import'):
        #     self.puerperi[id].add(data)

    def get_visites(self):
        sql = "select id_cip_sec, hash_d from u11"
        self.ids = {}
        for id, hash in u.getAll(sql, 'import'):
            self.ids[hash] = id
        sql = """
            SELECT hash_redics, hash_covid
            FROM dwsisap.pdptb101_relacio
        """
        self.hashos = {}
        for redics, covid in u.getAll(sql, 'exadata'):
            try:
                self.hashos[covid] = self.ids[redics]
            except KeyError:
                continue
        
        sql = """
            SELECT pacient, data
            FROM dwsisap.sisap_master_visites
            WHERE up in (
                SELECT up_cod
                FROM dwsisap.dbc_rup
                WHERE tip_cod = 20
            )
        """
        self.visites = c.defaultdict(set)
        for hash, data in u.getAll(sql, 'exadata'):
            try:
                self.visites[self.hashos[hash]].add(data)
            except KeyError:
                continue

    def get_socio_demografic(self):
        print('socio demogr�fic')
        self.user = {}
        sql = """select id_cip, up, edat, sexe, data_naix, nacionalitat, centre_codi from nodrizas.assignada_tot"""
        for id, up, edat, sexe, data_naix, nacionalitat, centre in u.getAll(sql, 'nodrizas'):
            self.user[id] = (up, edat, sexe, data_naix, nacionalitat, centre)
    
    def get_catalegs(self):
        print('cat�legs')
        self.centres = {}
        sql = """SELECT up_cod, tip_cod, SUBTIP_COD, REGIO_COD, REGIO_DES FROM dwsisap.dbc_rup"""
        for up, tip, subtip, regio, regio_desc in u.getAll(sql, 'exadata'):
            if tip == '10':
                tipus = 'atenci� hospital�ria'
            elif tip == '60':
                tipus = 'assist�ncia salut mental'
            elif subtip == '21':
                tipus = 'equip atenci� prim�ria'
            elif subtip == '22':
                tipus = 'ASSIR'
            elif subtip == '24':
                tipus = 'atenci� continuada'
            elif subtip == '38':
                tipus = 'drogodepend�ncies'
            self.centres[up] = [tipus, regio, regio_desc]
    
    def upload_data(self):
        print('comencem cuinetes')
        print(len(self.dx.keys()))
        upload = []
        for id in self.dx:
            antecedents = 0
            if id in self.user:
                up, edat, sexe, data_naix, nacionalitat, centre = self.user[id]
                if up in self.centres:
                    sindrome_infant_sacsejat = 0
                    tipus_atencio, regio, regio_desc = self.centres[up]
                    provincia_assig = centre[1:3]
                    for (codi_dx, data_dx, data_baixa_dx) in self.dx[id]:
                        if sindrome_infant_sacsejat and codi_dx[:9] == 'C01-T74.4':
                            continue 
                        sindrome_infant_sacsejat = 1 if codi_dx[:9] == 'C01-T74.4' else 0
                        edat_dx = int((data_dx - data_naix).days / 365.2425)
                        grau = grau_sospita(codi_dx)
                        tipus_violencia = tipus_violencia_class(codi_dx)
                        flag_psicofarmacs = 0
                        if id in self.tractaments:
                            # flag_psicofarmacs = treat_tractaments(data_dx, data_baixa_dx, self.tractaments[id])
                            for (dda, ddb) in self.tractaments[id]:
                                if data_dx >= dda and data_dx <= ddb:
                                    flag_psicofarmacs = 1
                        flag_polimedicacio = 0
                        if id in self.polimedicats and self.polimedicats[id] > 10:
                            flag_polimedicacio = 1
                        flag_infantil = 0
                        flag_antecedents_infantil = 0
                        if antecedents or codi_dx in maltractament_infantil:
                            flag_antecedents_infantil = 1
                        if edat_dx < 18:
                            flag_infantil = 1
                            antecedents = 1
                        flag_masclista = treat_vmasclista(codi_dx)
                        flag_comunicat = 0
                        total_comunicats = 0
                        if id in self.comunicats:
                            (flag_comunicat, total_comunicats) = treat_comunicats(data_dx, self.comunicats[id])
                        total_visites = 0
                        if id in self.visites:
                            total_visites = treat_visites(data_dx, self.visites[id])
                        flag_embaras = 0
                        flag_puerperi = 0
                        if id in self.embarassos:
                            (flag_embaras, flag_puerperi) = treat_embaras(data_dx, self.embarassos[id])
                        last_pvs, val_last_pvs, last_rvg, val_last_rvg, last_benestar, val_last_benestar, last_alcohol, val_last_alcohol = None, None, None, None, None, None, None, None
                        last_opioides, val_last_opioides, last_cannabis, val_last_cannabis, last_sedants, val_last_sedants, last_sedants, val_last_sedants = None, None, None, None, None, None, None, None
                        last_cocaina, val_last_cocaina, last_estimulants, val_last_estimulants, last_alucinogens, val_last_alucinogens, last_tabac, val_last_tabac = None, None, None, None, None, None, None, None
                        last_inhalants, val_last_inhalants, last_drogues, val_last_drogues, last_seguiment, val_last_seguiment, last_recursos, val_last_recursos = None, None, None, None, None, None, None, None
                        last_denuncia, val_last_denuncia, last_kit, val_last_kit, last_suicidi, val_last_suicidi = None, None, None, None, None, None
                        if id in self.variables:
                            (last_pvs, val_last_pvs) = treat_variables(self.variables[id]['EP3001'], data_dx, False) if 'EP3001' in self.variables[id] else (None, None)
                            (last_rvg, val_last_rvg) = treat_variables(self.variables[id]['VP3001'], data_dx, False) if 'VP3001' in self.variables[id] else (None, None)
                            (last_benestar, val_last_benestar) = treat_variables(self.variables[id]['EZ5101'], data_dx, True) if 'EZ5101' in self.variables[id] else (None, None)
                            (last_alcohol, val_last_alcohol) = treat_variables(self.variables[id]['VP2003'], data_dx, True) if 'VP2003' in self.variables[id] else (None, None)
                            (last_opioides, val_last_opioides) = treat_variables(self.variables[id]['VP2101'], data_dx, True) if 'VP2101' in self.variables[id] else (None, None)
                            (last_cannabis, val_last_cannabis) = treat_variables(self.variables[id]['VP2201'], data_dx, True) if 'VP2201' in self.variables[id] else (None, None)
                            (last_sedants, val_last_sedants) = treat_variables(self.variables[id]['VP2301'], data_dx, True) if 'VP2301' in self.variables[id] else (None, None)
                            (last_cocaina, val_last_cocaina) = treat_variables(self.variables[id]['VP2401'], data_dx, True) if 'VP2401' in self.variables[id] else (None, None)
                            (last_estimulants, val_last_estimulants) = treat_variables(self.variables[id]['VP2501'], data_dx, True) if 'VP2501' in self.variables[id] else (None, None)
                            (last_alucinogens, val_last_alucinogens) = treat_variables(self.variables[id]['VP2601'], data_dx, True) if 'VP2601' in self.variables[id] else (None, None)
                            (last_tabac, val_last_tabac) = treat_variables(self.variables[id]['VP2705'], data_dx, True) if 'VP2705' in self.variables[id] else (None, None)
                            (last_inhalants, val_last_inhalants) = treat_variables(self.variables[id]['VP2801'], data_dx, True) if 'VP2801' in self.variables[id] else (None, None)
                            (last_drogues, val_last_drogues) = treat_variables(self.variables[id]['VP2901'], data_dx, True) if 'VP2901' in self.variables[id] else (None, None)
                            (last_seguiment, val_last_seguiment) = treat_variables(self.variables[id]['AZ2105'], data_dx, True) if 'AZ2105' in self.variables[id] else (None, None)
                            (last_recursos, val_last_recursos) = treat_variables(self.variables[id]['AZ2106'], data_dx, True) if 'AZ2106' in self.variables[id] else (None, None)
                            (last_denuncia, val_last_denuncia) = treat_variables(self.variables[id]['AZ2104'], data_dx, True) if 'AZ2104' in self.variables[id] else (None, None)
                            (last_kit, val_last_kit) = treat_variables(self.variables[id]['AZ2102'], data_dx, True) if 'AZ2102' in self.variables[id] else (None, None)
                            (last_suicidi, val_last_suicidi) = treat_variables(self.variables[id]['PP1001'], data_dx, True) if 'PP1001' in self.variables[id] else (None, None)
                            # n_visites_ap despres de total_visites
                        upload.append((id + RANDOM_NOISE, edat, sexe, nacionalitat, provincia_assig, regio, regio_desc, tipus_atencio, codi_dx, data_dx, data_baixa_dx, edat_dx, grau, tipus_violencia, total_visites, flag_psicofarmacs, flag_polimedicacio, flag_infantil, flag_antecedents_infantil, flag_comunicat, total_comunicats, flag_embaras, flag_puerperi, flag_masclista,
                                    last_pvs, val_last_pvs, last_rvg, val_last_rvg, last_benestar, val_last_benestar, last_alcohol, val_last_alcohol, last_opioides, val_last_opioides, last_cannabis, val_last_cannabis, last_sedants, val_last_sedants, last_cocaina, val_last_cocaina, last_estimulants, val_last_estimulants, last_alucinogens, val_last_alucinogens, last_tabac, val_last_tabac,
                                    last_inhalants, val_last_inhalants, last_drogues, val_last_drogues, last_seguiment, val_last_seguiment, last_recursos, val_last_recursos, last_denuncia, val_last_denuncia, last_kit, val_last_kit, last_suicidi, val_last_suicidi))
        u.listToTable(upload, 'master_violencia', 'exadata')

def treat_variables(dates_vals, data_dx, post):
    dates_vals = c.OrderedDict(sorted(dates_vals.items()))
    last = None
    valor = None
    for data, val in dates_vals.items():
        if (post and data < data_dx + rd.relativedelta(days=5)) or (not post and data < data_dx):
            last = data
            valor = val
    return (last, valor)


if __name__ == "__main__":
    try:
        Master_violencia()
        mail = t.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "master violencia b� :)"
        text = ''
        mail.text = text
        mail.send()
    except Exception as e:
        text = str(e)
        mail = t.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "master violencia malament :("
        mail.text = text
        mail.send()
        raise
