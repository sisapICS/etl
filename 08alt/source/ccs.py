# -*- coding: utf-8 -*-

"""
.
"""

import collections as c

import sisapUtils as u


TABLE = "exp_khalix_ccs"
DATABASE = "altres"
FILE = "CCS"
EXCLOSOS = ("C01-A31.0", "C01-A37.11", "C01-A43.0", "C01-A48.1", "C01-B01.2",
            "C01-B25.0", "C01-B38.2", "C01-B39.0", "C01-B39.2", "C01-B59",
            "C01-J00", "C01-J01", "C01-J01.00", "C01-J01.01", "C01-J01.10",
            "C01-J01.11", "C01-J01.20", "C01-J01.21", "C01-J01.30",
            "C01-J01.31", "C01-J01.4", "C01-J01.40", "C01-J01.8", "C01-J01.9",
            "C01-J01.90", "C01-J02.0", "C01-J02.8", "C01-J02.9", "C01-J03.0",
            "C01-J03.00", "C01-J03.01", "C01-J03.80", "C01-J03.81",
            "C01-J03.9", "C01-J03.90", "C01-J03.91", "C01-J04.0", "C01-J04.2",
            "C01-J05", "C01-J05.0", "C01-J06", "C01-J06.9", "C01-J09",
            "C01-J09.X", "C01-J09.X1", "C01-J09.X2", "C01-J09.X3",
            "C01-J09.X9", "C01-J10.00", "C01-J10.08", "C01-J10.1",
            "C01-J10.89", "C01-J11", "C01-J11.00", "C01-J11.1", "C01-J11.2",
            "C01-J11.8", "C01-J11.81", "C01-J11.89", "C01-J12.0", "C01-J12.1",
            "C01-J12.2", "C01-J12.3", "C01-J12.8", "C01-J12.81", "C01-J12.89",
            "C01-J12.9", "C01-J13", "C01-J14", "C01-J15", "C01-J15.0",
            "C01-J15.1", "C01-J15.20", "C01-J15.211", "C01-J15.29",
            "C01-J15.3", "C01-J15.4", "C01-J15.5", "C01-J15.6", "C01-J15.7",
            "C01-J15.8", "C01-J15.9", "C01-J16.0", "C01-J16.8", "C01-J17",
            "C01-J18.0", "C01-J18.1", "C01-J18.2", "C01-J18.8", "C01-J18.9",
            "C01-J20.0", "C01-J20.1", "C01-J20.2", "C01-J20.3", "C01-J20.4",
            "C01-J20.5", "C01-J20.6", "C01-J20.7", "C01-J20.8", "C01-J20.9",
            "C01-J21.0", "C01-J21.1", "C01-J21.8", "C01-J21.9", "C01-J22",
            "C01-J30.9", "C01-J40", "C01-J44.0", "C01-J85.1", "C01-J85.2",
            "C01-J85.3", "C01-J98.09", "C01-R05", "C01-R07.0", "C01-J04.1",
            "C01-J04.10", "C01-J04.11", "C01-J10.01", "C01-U07.1")

class CCS(object):
    """."""

    def __init__(self):
        """."""
        self.get_codis()
        self.get_centres()
        self.get_problemes()
        self.upload_data()
        self.export_khalix()

    def get_codis(self):
        """."""
        sql = "select col3, col12 from sisap_cat_ccs where col12 is not null"
        self.codis = {row[0]: row[1] for row in u.getAll(sql, "redics")}

    def get_centres(self):
        """."""
        sqls = ("select up, br_assir from ass_centres",
                "select scs_codi, ics_codi from urg_centres",
                "select scs_codi, ics_codi from cat_centres")
        self.centres = {}
        for sql in sqls:
            for up, br in u.getAll(sql, "nodrizas"):
                self.centres[up] = br

    def get_problemes(self):
        """."""
        tables = u.getSubTables("problemes")
        exclosos = set(EXCLOSOS)
        sql = "select pr_cod_ps, pr_up, date_format(pr_dde, '%y%m') \
               from {}, nodrizas.dextraccio \
               where pr_dde between 20190101 and data_ext and \
                     pr_up <> ''"
        jobs = [(sql.format(table), "import") for table in tables]
        self.dades = c.Counter()
        for sector in u.multiprocess(u.get_data, jobs):
            for ps, up, dat in sector:
                if ps not in exclosos:
                    grup = "CCS" + self.codis.get(ps, '99')
                    br = self.centres.get(up, "ND" + up)
                    entra = 1 * (up in self.centres)
                    self.dades[(br, grup, "A" + dat, entra)] += 1

    def upload_data(self):
        """."""
        upload = [k + (v,) for (k, v) in self.dades.items()]
        cols = "(up varchar(9), grup varchar(5), periode varchar(5), \
                 khalix int, recompte int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(upload, TABLE, DATABASE)

    def export_khalix(self):
        """."""
        sql = "select 'CCSRAW', periode, up, 'NOCLI', grup, 'NOIMP', \
                      'DIM6SET', 'N', recompte \
               from {}.{} \
               where khalix = 1".format(DATABASE, TABLE)
        u.exportKhalix(sql, FILE)


if __name__ == "__main__":
    CCS()
