# -*- coding: utf8 -*-

"""
.
"""

import collections as c

import sisapUtils as u


TABLE = "exp_khalix_esoap"
DATABASE = "altres"
FILE = "ESOAP"

COPIAR = {"EQA": {"sql": "select 'dummy', up, edat, indicador, conc, 1, sum(n) \
                          from odn.exp_khalix_up_ind \
                          where indicador in {_indicadors_vells} and comb = 'NOINSAT' \
                          GROUP BY up, edat, indicador, conc \
                          union all \
                          select 'dummy', up, edat, indicador, conc, 1, sum(n) \
                          from odn.exp_khalix_up_ind_jail \
                          where indicador in {_indicadors_vells} and comb = 'NOINSAT' \
                          GROUP BY up, edat, indicador, conc",
                  "ind": {("EQA9113", (0, 200)): "ESOAP0401",
                          ("EQA9104", (0, 200)): "ESOAP0402",
                          ("EQA9103", (0, 200)): "ESOAP0403",
                          ("EQA9118", (0, 200)): "ESOAP0404",
                          ("EQA9110", (0, 200)): "ESOAP0506",
                          ("EQA9102", (0, 200)): "ESOAP0701",
                          ("EQA9102", (16, 200)): "ESOAP0701PRS",
                          ("EQA9117", (0, 200)): "ESOAP0702",
                          ("EQA9117", (16, 200)): "ESOAP0702PRS",
                          ("EQA9108", (0, 200)): "ESOAP0703",
                          ("EQA9108", (16, 200)): "ESOAP0703PRS"}},
          "COB": {"sql": "select id_cip_sec, uporigen, edat, 'dummy', 'NUM', 1, num \
                          from altres.mst_pob_odn_pacient \
                          union all \
                          select id_cip_sec, uporigen, edat, 'dummy', 'DEN', 1, den \
                          from altres.mst_pob_odn_pacient \
                          union all \
                          select id_cip_sec, uporigen, edat, 'dummy', 'NUM', 1, num \
                          from altres.mst_pob_odn_pacient_jail \
                          union all \
                          select id_cip_sec, uporigen, edat, 'dummy', 'DEN', 1, den \
                          from altres.mst_pob_odn_pacient_jail",
                  "ind": {("dummy", (0, 14)): "ESOAP0101A",
                          ("dummy", (15, 200)): "ESOAP0101B",
                          ("dummy", (16, 200)): "ESOAP0101PRS"}},
          "PRODO": {"sql": "select id_cip_sec, up, edat, indicador, 'NUM', tipus_dent, num \
                            from altres.mst_prestacions_odn_pacient \
                            where indicador in {_indicadors_vells}",
                    "ind": {("PRODOA0001", (5, 14)): "ESOAP0501",
                            # ("PRODOA0002", (5, 14)): "ESOAP0502",
                            ("PRODOA0003", (0, 14)): "ESOAP0601",
                            ("PRODOA0004", (0, 14)): "ESOAP0601",
                            ("PRODOA0003", (15, 200)): "ESOAP0602",
                            ("PRODOA0003", (16, 200)): "ESOAP0602PRS",
                            ("PRODOA0004", (15, 200)): "ESOAP0602"}}}


class ESOAP(object):
    """."""

    def __init__(self):
        """."""
        self.create_table();                        print("create_table")
        self.get_cataleg();                         print("get_cataleg")
        self.get_periode();                         print("get_periode")
        self.get_centres();                         print("get_centres")
        self.get_pacients();                        print("get_pacients")
        self.get_ultima_caod_positiva();            print("get_ultima_caod_positiva")
        self.get_risc_caries();                     print("get_risc_caries")
        self.cond_den_caod_risc = self.ultima_caod_positiva | self.pacients_risc_caries
        self.get_copiar();                          print("get_copiar")
        self.get_denominadors();                    print("get_denominadors")
        self.get_accessibilitat();                  print("get_accessibilitat")
        self.get_longitudinalitat();                print("get_longitudinalitat")
        self.get_dbs();                             print("get_dbs")
        self.get_revisions();                       print("get_revisions")
        self.export_khalix();                       print("export_khalix")

    def create_table(self):
        """."""
        cols = "(up varchar(5), indicador varchar(13), poblacio varchar(10), \
                 analisi varchar(3), periode varchar(5), recompte int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)

    def get_cataleg(self):
        """."""
        self.cataleg = {}
        self.cataleg_invers = c.defaultdict(set)
        sql = "select distinct uporigen, up from cat_linies"
        for uporigen, up in u.getAll(sql, "nodrizas"):
            self.cataleg[uporigen] = up
            self.cataleg_invers[up].add(uporigen)

    def get_periode(self):
        """."""
        sql = "select data_ext from dextraccio"
        self.dextraccio = u.getOne(sql, "nodrizas")[0]
        self.periode = "A{}".format(self.dextraccio.strftime("%y%m"))

    def get_centres(self):
        """."""
        self.centres = {row[0]: row[1] for row
                   in u.getAll("select ics_codi, scs_codi from cat_centres \
                                union all select ics_codi, scs_codi from jail_centres",
                               "nodrizas")}
        self.eap_centres = {up for up, in u.getAll("select scs_codi from cat_centres", "nodrizas")}
        self.jail_centres = {up for up, in u.getAll("select scs_codi from jail_centres", "nodrizas")}
        
    def get_pacients(self):
        """."""

        sql = "SELECT id_cip_sec, hash_d FROM u11"
        self.pacients = {hash_d: id_cip_sec for id_cip_sec, hash_d in u.getAll(sql, "import")}

        sql = "SELECT id_cip_sec, up, upOrigen FROM assignada_tot WHERE edat <= 16"
        self.pacients_up = {id_cip_sec: (up_a, up_o) for id_cip_sec, up_a, up_o in u.getAll(sql, "nodrizas")}

    def get_ultima_caod_positiva(self):
        """."""

        sql = """
                SELECT
                    id_cip_sec,
                    agrupador,
                    dat,
                    valor
                FROM
                    odn_variables
                WHERE
                    agrupador IN (307, 308)
              """
        ultima_caod_positiva = c.defaultdict(dict)
        for id_cip_sec, agr, data, valor in u.getAll(sql, "nodrizas"):
            ultima_caod_positiva[(agr, id_cip_sec)][data] = max(ultima_caod_positiva[(agr, id_cip_sec)].get(data, float('-inf')), valor)

        self.ultima_caod_positiva = set()
        for (agr, id_cip_sec), dades in ultima_caod_positiva.items():
            if dades[max(dades)] >= 1:
                self.ultima_caod_positiva.add(id_cip_sec)

    def get_risc_caries(self):
        """."""

        self.pacients_risc_caries = set()
        
        sql = "select c_cip, c_up, c_up_origen, c_edat_anys, \
                    decode(oe_ca_def, null, 0, 1),\
                    decode(oe_ca_temp, null, 0, 1), oc_risc \
            from dbs \
            where c_cip like '{}%' and c_edat_anys < 15"
        jobs = [sql.format(format(digit, 'x').upper()) for digit in range(16)]
        dades = u.multiprocess(_worker, jobs)
        for chunk in dades:
            for hash_d, up_a, up_o, edat, ca_d, ca_t, risc in chunk:
                id_cip_sec = self.pacients.get(hash_d)
                if id_cip_sec and (risc or ca_d or ca_t):
                    self.pacients_risc_caries.add(id_cip_sec)

    def _get_conversio(self, up, edat):
        """."""
        if edat in ("EC01", "EC24", "EC59", "EC1014"):
            return self.cataleg.get(up, up)
        else:
            return up

    def get_copiar(self):
        """."""
        resultat = c.Counter()
        self.poblacio = c.Counter()
        for domini, dades in COPIAR.items():
            conv = {}
            for (indicador_vell, edats), indicador_nou in dades["ind"].items():
                tipus_centre = "PRS" if indicador_nou[-3:]== "PRS" else "EAP"
                for edat in range(edats[0], edats[1] + 1):
                    conv[(indicador_vell, u.ageConverter(edat), tipus_centre)] = indicador_nou
            indicadors_vells = (indicador_vell for (indicador_vell, edat, tipus_eap) in conv)
            sql = dades["sql"].format(_indicadors_vells=tuple(indicadors_vells))
            for id_cip_sec, up, edat, indicador_vell, analisis, tipus_dent, n in u.getAll(sql, "import"):
                tipus_eap = "EAP" if up not in self.jail_centres else "PRS"
                up_fix = self._get_conversio(up, edat)
                if (indicador_vell, edat, tipus_eap) in conv:
                    indicador_nou = conv[(indicador_vell, edat, tipus_eap)]
                    if indicador_nou != "ESOAP0501" or (indicador_nou == "ESOAP0501" and id_cip_sec in self.cond_den_caod_risc):                    
                        resultat[(up, indicador_nou, "ODOORIG", analisis)] += n
                        resultat[(up_fix, indicador_nou, "ODOASSIG", analisis)] += n
                        if indicador_nou == "ESOAP0601":
                            subindicador_nou = "ESOAP0601A" if tipus_dent == "DTEMP" else "ESOAP0601B"  # noqa
                            resultat[(up, subindicador_nou, "ODOORIG", analisis)] += n
                            resultat[(up_fix, subindicador_nou, "ODOASSIG", analisis)] += n
                if domini == "COB" and analisis == "DEN":
                    self.poblacio[(id_cip_sec, up, "ODOORIG", edat)] += n
                    self.poblacio[(id_cip_sec, up_fix, "ODOASSIG", edat)] += n  # noqa
        upload = [k + (self.periode, v) for (k, v) in resultat.items()]
        u.listToTable(upload, TABLE, DATABASE)

    def get_denominadors(self):
        """."""
        entren = c.defaultdict(set)
        for (indicador_vell, edats), indicador_nou in COPIAR["PRODO"]["ind"].items():
            for edat in range(edats[0], edats[1] + 1):
                entren[u.ageConverter(edat)].add(indicador_nou)
                if indicador_nou == "ESOAP0601":
                    entren[u.ageConverter(edat)].update(["ESOAP0601A", "ESOAP0601B"])  # noqa
        resultat = c.Counter()
        for (id_cip_sec, up, pob, edat), n in self.poblacio.items():
            for indicador_nou in entren[edat]:
                if (indicador_nou[-3:] == "PRS" and up in self.jail_centres) or (indicador_nou[-3:] != "PRS" and up in self.eap_centres):
                    if indicador_nou != "ESOAP0501" or (indicador_nou == "ESOAP0501" and id_cip_sec in self.cond_den_caod_risc):
                        resultat[(up, indicador_nou, pob, "DEN")] += n
        upload = [k + (self.periode, v) for (k, v) in resultat.items()]
        u.listToTable(upload, TABLE, DATABASE)

    def get_accessibilitat(self):
        """."""
        indicadors = {"ACC2D": "ESOAP0201A", "ACC5D": "ESOAP0201B",
                      "ACC10D": "ESOAP0201C"}
        sql = "select d2, d0, d3, d1, n from exp_khalix_forats \
               where d5 = 'TIPPROF3' and \
                     d1 = '{}' and \
                     d0 in {}".format(self.periode, tuple(indicadors.keys()))
        resultat = c.Counter()
        for br, _ind, analisi, periode, n in u.getAll(sql, "altres"):
            up = self.centres[br]
            ind = indicadors[_ind]
            keys = [(up, ind, "ODOASSIG", analisi, periode)]
            if up in self.cataleg_invers:
                keys.extend([(orig, ind, "ODOORIG", analisi, periode)
                             for orig in self.cataleg_invers[up]])
            else:
                keys.append((up, ind, "ODOORIG", analisi, periode))
            for key in keys:
                resultat[key] += n
        upload = [k + (v,) for (k, v) in resultat.items()]
        u.listToTable(upload, TABLE, DATABASE)

    def get_longitudinalitat(self):
        """."""
        indicador = "ESOAP0301"
        sql = "select up, analisi, periode, resultat \
               from exp_long_cont_up \
               where indicador = 'CONT0002' and detalle = 'TIPPROF3'"
        resultat = c.Counter()
        for br, analisi, periode, n in u.getAll(sql, "altres"):
            up = self.centres.get(br)
            if up:
                keys = [(up, indicador, "ODOASSIG", analisi, periode)]
                if up in self.cataleg_invers:
                    keys.extend([(orig, indicador, "ODOORIG", analisi, periode)
                                for orig in self.cataleg_invers[up]])
                else:
                    keys.append((up, indicador, "ODOORIG", analisi, periode))
                for key in keys:
                    resultat[key] += n
        upload = [k + (v,) for (k, v) in resultat.items()]
        u.listToTable(upload, TABLE, DATABASE)

    def get_dbs(self):
        """."""
        resultat = c.Counter()
        sql = "select c_up, c_up_origen, c_edat_anys, \
                      decode(oe_ca_def, null, 0, 1),\
                      decode(oe_ca_temp, null, 0, 1), oc_risc, ot_fluor, ot_fluor_sc \
               from dbs \
               where c_cip like '{}%' and c_edat_anys < 15"
        jobs = [sql.format(format(digit, 'x').upper()) for digit in range(16)]
        dades = u.multiprocess(_worker, jobs)
        for chunk in dades:
            for up_a, up_o, edat, ca_d, ca_t, risc, fluor_1, fluor_2 in chunk:
                keys = []
                risc = 1 if risc or ca_d or ca_t else 0
                ind = "05" if risc == 1 else "07"
                keys.append((up_a, "ESOAP05" + ind, "ODOASSIG", "DEN"))
                keys.append((up_o, "ESOAP05" + ind, "ODOORIG", "DEN"))
                if (fluor_1 and u.yearsBetween(fluor_1, self.dextraccio) == 0) or (fluor_2 and u.yearsBetween(fluor_2, self.dextraccio) == 0):
                    keys.append((up_a, "ESOAP05" + ind, "ODOASSIG", "NUM"))
                    keys.append((up_o, "ESOAP05" + ind, "ODOORIG", "NUM"))
                if edat >= 6:
                    keys.append((up_a, "ESOAP0503", "ODOASSIG", "DEN"))
                    keys.append((up_o, "ESOAP0503", "ODOORIG", "DEN"))
                    if risc == 1:
                        keys.append((up_a, "ESOAP0503", "ODOASSIG", "NUM"))
                        keys.append((up_o, "ESOAP0503", "ODOORIG", "NUM"))
                if edat in (7, 12):
                    subi = "A" if edat == 7 else "B"
                    keys.append((up_a, "ESOAP0504" + subi, "ODOASSIG", "DEN"))
                    keys.append((up_o, "ESOAP0504" + subi, "ODOORIG", "DEN"))
                    if ca_d or ca_t:
                        keys.append((up_a, "ESOAP0504" + subi, "ODOASSIG", "NUM"))  # noqa
                        keys.append((up_o, "ESOAP0504" + subi, "ODOORIG", "NUM"))  # noqa
                for key in keys:
                    resultat[key] += 1
        upload = [k + (self.periode, v) for (k, v) in resultat.items()]
        u.listToTable(upload, TABLE, DATABASE)

    def get_revisions(self):
        """."""
        ind = "ESOAP0405"
        y = u.getOne("select year(data_ext) from dextraccio", "nodrizas")[0]
        sql = "select id_cip_sec from odn_variables, dextraccio \
               where agrupador = 309 and \
                     dat between date_add(data_ext,interval - 16 month) and \
                                 date_add(data_ext,interval + 1 month)"
        revisats = set([id for id, in u.getAll(sql, "nodrizas")])
        sql = "select id_cip_sec, up, uporigen from assignada_tot \
               where year(data_naix) between {0} - 16 and {0} - 3 and \
                     ates = 1 and maca = 0 and \
                     if(edat < 15, institucionalitzat_ps, institucionalitzat) = 0".format(y)  # noqa
        dades = c.Counter()
        for id, up, orig in u.getAll(sql, "nodrizas"):
            dades[(up, ind, "ODOASSIG", "DEN")] += 1
            dades[(orig, ind, "ODOORIG", "DEN")] += 1
            if id in revisats:
                dades[(up, ind, "ODOASSIG", "NUM")] += 1
                dades[(orig, ind, "ODOORIG", "NUM")] += 1
        upload = [k + (self.periode, v) for (k, v) in dades.items()]
        u.listToTable(upload, TABLE, DATABASE)

    def export_khalix(self):
        """."""
        sql = "select distinct indicador, periode, ics_codi, analisi, \
                      'NOCAT', poblacio, 'DIM6SET', 'N', recompte \
               from {}.{} a \
               inner join nodrizas.cat_centres b \
                    on a.up = b.scs_codi \
               where indicador not like '%PRS'".format(DATABASE, TABLE)
        u.exportKhalix(sql, FILE)

        sql = "select distinct indicador, periode, ics_codi, analisi, \
                      'NOCAT', 'NOIMP', 'DIM6SET', 'N', recompte \
               from {}.{} a \
               inner join nodrizas.jail_centres b \
                    on a.up = b.scs_codi \
               where indicador like '%PRS'".format(DATABASE, TABLE)
        u.exportKhalix(sql, FILE + "_JAIL")


def _worker(sql):
    return [row for row in u.getAll(sql, "redics")]


if __name__ == "__main__":
    if u.IS_MENSUAL:
        ESOAP()