# -*- coding: utf8 -*-

import sisapUtils as u

class export_khalix_agendes():
    def __init__(self):
        self.agendes()
        self.agendes_anual()
        self.agendes_mensual()
        self.agendes_uba()
        self.agendes_uba_anual()
        self.agendes_uba_mensual()

    def agendes(self):
        sql = """select indicador, concat('A', 'periodo'),
                    br, analisi, tipus, classi,
                    dim6set, n, SUM(val)
                    from altres.agendes_1
                    where indicador like 'AGEND%'
                    group by indicador, concat('A', 'periodo'),
                    br, analisi, tipus, classi,
                    dim6set, n
                    union 
                    select indicador, concat('A', 'periodo'),
                    br, analisi, 'NOCAT', classi,
                    dim6set, n, SUM(val)
                    from altres.agendes_1
                    where indicador not like 'AGEND%'
                    group by indicador, concat('A', 'periodo'),
                    br, analisi, 'NOCAT', classi,
                    dim6set, n"""
        u.exportKhalix(sql, 'AGENDES')

    def agendes_anual(self):
        # me'n falten 3 els GRPEQAPOBA i el -PT I AGENDQC4
        sql = """select indicador, concat('A', 'periodo'),
            br, analisi, 'ANUAL', classi,
            dim6set, n, SUM(val)
            from altres.agendes_anual
            group by indicador, concat('A', 'periodo'),
            br, analisi, 'ANUAL', classi,
            dim6set, n"""
        u.exportKhalix(sql, 'AGENDES_ANUAL')

    def agendes_mensual(self):
        # me'n falten 3 els GRPEQAPOBA i el -PT I AGENDQC4
        sql = """select indicador, concat('A', 'periodo'),
            br, analisi, 'MENSUAL', classi,
            dim6set, n, SUM(val)
            from altres.agendes_mensual_1
            group by indicador, concat('A', 'periodo'),
            br, analisi, 'MENSUAL', classi,
            dim6set, n"""
        u.exportKhalix(sql, 'AGENDES_MENSUAL')

    def agendes_uba(self):
        sql = """select indicador, concat('A', 'periodo'),
                    br, analisi, tipus, classi,
                    dim6set, n, SUM(val)
                    from altres.agendes_2
                    where indicador like 'AGEND%'
                    group by indicador, concat('A', 'periodo'),
                    br, analisi, tipus, classi,
                    dim6set, n
                    union 
                    select indicador, concat('A', 'periodo'),
                    br, analisi, 'NOCAT', classi,
                    dim6set, n, SUM(val)
                    from altres.agendes_2
                    where indicador not like 'AGEND%'
                    group by indicador, concat('A', 'periodo'),
                    br, analisi, 'NOCAT', classi,
                    dim6set, n"""
        u.exportKhalix(sql, 'AGENDES_UBA')

    def agendes_uba_anual(self):
        sql = """select * from altres.agendes_anual_uba"""
        u.exportKhalix(sql, 'AGENDES_UBA_ANUAL')

    def agendes_uba_mensual(self):
        # nomes tinc AGENDQC1 I 1
        sql = """select indicador, concat('A', 'periodo'),
                equip, analisi, 'MENSUAL', classi,
                dim6set, n, SUM(val)
                from altres.agendes_mensual_2
                group by indicador, concat('A', 'periodo'),
                equip, analisi, 'MENSUAL', classi,
                dim6set, n"""
        u.exportKhalix(sql, 'AGENDES_UBA_MENSUAL')

if __name__ == "__main__":
    if u.IS_MENSUAL:
        export_khalix_agendes()