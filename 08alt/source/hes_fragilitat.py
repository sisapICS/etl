import sisapUtils as u
import collections as c
import datetime as d
from dateutil.relativedelta import relativedelta
import sisaptools as t
import os

DEXTD = u.getOne("select data_ext from dextraccio", "nodrizas")[0]
days = DEXTD.isocalendar()[2] - 1
setmana = DEXTD.isocalendar()[1]
data_ext = DEXTD - relativedelta(days=days)

sql = """
    SELECT id_cip_sec, up, ics_desc
    FROM assignada_tot a
    INNER JOIN cat_centres b
    ON a.up = b.scs_codi
"""
pob = {}
ups = {}
for id, up, desc in u.getAll(sql, 'nodrizas'):
    pob[id] = up
    ups[up] = desc

sql = """
    SELECT id_cip_sec, data_var
    FROM eqa_variables
    WHERE agrupador = 918
    AND data_var >= DATE '{data}'
""".format(data=data_ext)

dades = set()
recompte = 0
for id, data in u.getAll(sql, 'nodrizas'):
    dades.add(id)
    recompte += 1

sql = """
    SELECT id_cip_sec,
    CASE es_cod
        WHEN 'ER0001' THEN 'pcc'
        WHEN 'ER0002' THEN 'maca'
        ELSE 'error'
    END as var
    FROM estats
"""

estats = c.defaultdict(set)
for id, codi in u.getAll(sql, 'import'):
    estats[codi].add(id)

res = c.defaultdict(c.Counter)
for id in dades:
    pcc = 0
    maca = 0
    if id in pob:
        up = pob[id]
        desc = ups[up]
        if id in estats['pcc']:
            res[(up, desc)][ 'PCC'] += 1
        if id in estats['maca']:
            res[(up, desc)][ 'MACA'] += 1
        if id not in estats['pcc'] and id not in estats['maca']:
            res[(up, desc)][ 'NO PCC/MACA'] += 1

upload = []
for (up, desc) in res:
    upload.append((up, desc, res[(up, desc)]['PCC'], res[(up, desc)]['MACA'], res[(up, desc)]['NO PCC/MACA']))
            
file = u.tempFolder + 'hes_fragilitat.csv'
cols = [('up', 'up_desc', 'pcc', 'maca', 'no pcc/maca')]
u.writeCSV(file, cols + upload, sep=',')
u.sendGeneral(
            me='sisap@gencat.cat',
            to=['anavarros.girona.ics@gencat.cat'],
            cc=['nuriacantero@gencat.cat', 'arene.girona.ics@gencat.cat'],
            subject='Registre fragilitat a HES',
            text= """
            Hola,
            
            La setmana {setmana} s'han registrat {recompte} nous registres de la variable VIG Fragil a HES.""".format(setmana=setmana, recompte=recompte),
            file=file
            )
os.remove(file)