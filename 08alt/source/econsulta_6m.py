# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
import dateutil.relativedelta as r

import sisapUtils as u


tb_br = 'exp_khalix_up_econsulta_6m'
tb_uba = 'exp_khalix_uba_econsulta_6m'
#tb_qc = 'exp_qc_econsulta_6m'
db = 'altres'


class EConsulta_6M(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_ubas()
        self.get_poblacio()
        self.get_professionals()
        self.get_dni_espe()
        self.get_converses()
        self.get_missatges()
        self.resultat = []
        self.get_econs0008()
        self.export_br()
        self.export_uba()
        # self.export_qc()

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, 'nodrizas')}

    def get_ubas(self):
        """."""
        self.ubas = c.defaultdict(set)
        sql = "select up, tipus, uba from mst_ubas"
        for up, tipus, uba in u.getAll(sql, 'eqa_ind'):
            if up in self.centres:
                self.ubas[(self.centres[up], tipus)].add(uba)
        print(len(self.ubas))
        sql = "select up, tipus, uba from mst_ubas"
        for up, tipus, uba in u.getAll(sql, 'pedia'):
            if up in self.centres:
                self.ubas[(self.centres[up], tipus)].add(uba)
        print(len(self.ubas))

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, up, uba, ubainf, edat from assignada_tot \
               where ates = 1 and institucionalitzat = 0"
        self.poblacio_pac = {}
        self.poblacio_ent = c.Counter()
        self.poblacio_edat = {}
        self.poblacio_qc = c.Counter()
        for id, up, uba, ubainf, edat in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                br = self.centres[up]
                if uba in self.ubas[(br, 'M')] or ubainf in self.ubas[(br, 'I')]:  # noqa
                    adult = edat > 14
                    entities = (br, ''.join((br, 'M', uba)),
                                ''.join((br, 'I', ubainf)))
                    self.poblacio_pac[id] = entities
                    for entity in entities:
                        self.poblacio_ent[entity] += 1
                    self.poblacio_edat[id] = adult
                    tipus = ["TIPPROF", "TIPPROF4", "TIPPROF1" if adult else "TIPPROF2"]  # noqa
                    for tip in tipus:
                        self.poblacio_qc[(br, tip)] += 1

    def get_professionals(self):
        """."""
        sql = "select lloc_codi_up, lloc_codi_lloc_de_treball, \
                      lloc_proveidor_dni_proveidor \
               from cat_pritb025"
        llocs = {(self.centres[up], lloc): dni for (up, lloc, dni)
                 in u.getAll(sql, 'import') if up in self.centres}
        params = (('uab_codi_up', 'M', 'uab_codi_uab',
                   'uab_lloc_de_tr_codi_lloc_de_tr', 'cat_vistb039'),
                  ('uni_codi_up', 'I', 'uni_codi_unitat',
                   'uni_ambit_treball', 'cat_vistb059'))
        sql = "select {}, '{}', {}, {} from {}"
        self.professionals = c.defaultdict(set)
        for param in params:
            for up, tipus, uba, lloc in u.getAll(sql.format(*param), 'import'):
                if up in self.centres:
                    br = self.centres[up]
                    dni = llocs.get((br, lloc))
                    if dni and (uba in self.ubas[(br, tipus)]):
                        self.professionals[(br, tipus)].add(dni)

    def get_dni_espe(self):
        """."""
        sql = "select codi_sector, prov_dni_proveidor, prov_categoria \
               from cat_pritb031"
        self.dni_espe = {row[:2]: (1 if row[2][0] == '1' else 2)
                         for row in u.getAll(sql, 'import')}

    def get_converses(self):
        """."""
        sql = "select id_cip_sec, codi_sector, conv_id, conv_autor, \
                      conv_desti, conv_estat, conv_dini \
               from econsulta_conv, nodrizas.dextraccio \
               where conv_dini between \
                adddate(adddate(data_ext, interval -6 month), interval +1 day) \
                and data_ext"
        self.converses = {}
        for pac, sector, id, autor, desti, estat, ini in u.getAll(sql, 'import'):  # noqa
            if pac in self.poblacio_pac:
                prof = (autor if autor else desti)
                espe = self.dni_espe.get((sector, prof))
                if espe:
                    entities = [self.poblacio_pac[pac][i] for i in (0, espe)]
                    tipprof = 4 if espe == 1 else 1 if self.poblacio_edat[pac] else 2  # noqa
                    self.converses[(sector, id)] = {'pac': pac,
                                                    'br': entities[0],
                                                    'entities': entities,
                                                    'prof': prof,
                                                    'inicia': not desti,
                                                    'estat': estat,
                                                    'ini': ini,
                                                    'tipprof': "TIPPROF{}".format(tipprof),  # noqa
                                                    'msg': {}}

    def get_missatges(self):
        """."""
        sql = "select codi_sector, msg_conv_id, msg_id, msg_autor, \
                      msg_desti, msg_data \
               from econsulta_msg"
        for sector, id_c, id, autor, desti, data in u.getAll(sql, 'import'):
            if (sector, id_c) in self.converses:
                this = {'prof': autor if autor else desti,
                        'inicia': not desti,
                        'data': data}
                self.converses[(sector, id_c)]['msg'][id] = this

    def get_econs0008(self):
        """."""
        ind = 'ECONS0008_6M'
        den = c.Counter()
        num = c.Counter()
        for conversa in self.converses.values():
            if conversa['estat'] == 'T' and not conversa['inicia'] and 1 in conversa["msg"]:
                for entity in conversa['entities']:
                    den[entity] += 1
                    pregunta = conversa['msg'][1]['data']
                    resposta = None
                    for id in range(2, 1000):
                        if id in conversa['msg']:
                            if conversa['msg'][id]['inicia']:
                                resposta = conversa['msg'][id]['data']
                                break
                        else:
                            break
                    if resposta:
                        dies = EConsulta_6M.conta_dies(pregunta, resposta)
                        num[entity] += dies
        for entity, n in den.items():
            self.resultat.append((ind, entity, 'DEN', n))
            self.resultat.append((ind, entity, 'NUM', num[entity]))

    @staticmethod
    def conta_dies(inici, final):
        """."""
        total = (final - inici).days
        laborables = 0
        for i in range(total):
            dia = inici + d.timedelta(days=i)
            if u.isWorkingDay(dia):
                laborables += 1
        return laborables

    def export_br(self):
        """."""
        u.createTable(tb_br, '(indicador varchar(12), br varchar(5), \
                               analisis varchar(10), n int)', db, rm=True)
        u.listToTable([row for row in self.resultat if len(row[1]) == 5],
                      tb_br, db)
        sql = "select indicador, 'Aperiodo', br, analisis, 'NOCAT', \
               'NOINSAT', 'DIM6SET', 'N', n from {}.{}".format(db, tb_br)
        file = 'ECONSULTA_6M'
        u.exportKhalix(sql, file)

    def export_uba(self):
        """."""
        u.createTable(tb_uba, '(indicador varchar(12), ent varchar(11), \
                                analisis varchar(10), n int)', db, rm=True)
        u.listToTable([row for row in self.resultat if len(row[1]) > 5],
                      tb_uba, db)
        sql = "select indicador, 'Aperiodo', ent, analisis, 'NOCAT', \
               'NOINSAT', 'DIM6SET', 'N', n from {}.{}".format(db, tb_uba)
        file = 'ECONSULTA_UBA_6M'
        u.exportKhalix(sql, file)

    def export_qc(self):
        """."""
        cols = ["k{} varchar(10)".format(i) for i in range(6)] + ["v int"]
        u.createTable(tb_qc, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.qc, tb_qc, db)


if __name__ == '__main__':
    EConsulta_6M()
