# coding: latin1

from sisapUtils import *
from collections import defaultdict, Counter
from itertools import product

"""
Nota: Tot i que calculem els indicadors PRODOP0007 i PRODOP0007, no els enviem a LV, ja que es calculen directament des de visualitzaci�.
"""

tb = "mst_prestacions_odn_up"
tb_pacient = "mst_prestacions_odn_pacient"
file_up = "PRESTACIONS_ODN"

indicadors_jail = "('PRODOA0001', 'PRODOA0002', 'PRODOA0003', 'PRODOA0004', 'PRODOA0005', 'PRODOA0006', 'PRODOA0008', 'PRODOA0009', \
                    'PRODOP0001', 'PRODOP0002', 'PRODOP0003', 'PRODOP0004', 'PRODOP0005', 'PRODOP0006', 'PRODOP0008', 'PRODOP0009')"

codis = {'FC': {'NUM': {'p': 'PRODOP0001', 'a': 'PRODOA0001'}},
         'SEG': {'NUM': {'p': ['PRODOP0002', 'PRODOP0007'], 'a': ['PRODOA0002', 'PRODOA0007']}},
         'EX': {'NUM': {'p': 'PRODOP0003', 'a': 'PRODOA0003'}},
         'EXO': {'NUM': {'p': 'PRODOP0004', 'a': 'PRODOA0004'}}, 
         'OBT_CON': {'NUM': {'p': 'PRODOP0005', 'a': 'PRODOA0005'}, 'DEN': {'p': 'PRODOP0007', 'a': 'PRODOA0007'}}, # Codis OBT o CON
         'TT': {'NUM': {'p': 'PRODOP0006', 'a': 'PRODOA0006'}},
         'END': {'NUM': {'p': 'PRODOP0008', 'a': 'PRODOA0008'}},
         'FDP': {'NUM': {'p': 'PRODOP0009', 'a': 'PRODOA0009'}},
        }

class prestacions(object):
    """ Classe principal a instanciar per gestionar les prestacions odontol�giques. """
    
    def __init__(self):

        printTime('inici')
        self.dades_pre = defaultdict(Counter)
        self.dades_pre_pacient = defaultdict(Counter)
        self.get_dates()
        self.get_poblacio()
        self.get_prodoa0009PRS()
        self.get_prevencio_neteja()
        self.get_restauracio_cirurgia()
        self.get_calculs()
        self.export_data()
        printTime('fi')
        
    def get_dates(self):
        """ Obt� les dates de la data d'extracci� de les dades. """

        sql = """
                SELECT
                    data_ext,
                    date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY)
                FROM
                    dextraccio
              """
        self.dext, self.dext_menys1any = getOne(sql, "nodrizas")        
    
    def get_poblacio(self):
        """ Obt� informaci� de la poblaci� (up, edat, sexe) dels pacients a partir de la taula assignada_tot de nodrizas. """

        self.poblacio = {}

        sql = """
                SELECT
                    id_cip_sec,
                    upOrigen,
                    edat,
                    sexe
                FROM
                    assignada_tot
              """
        self.poblacio.update({id_cip_sec: (up, ageConverter(edat, 5), sexConverter(sexe)) 
                            for id_cip_sec, up, edat, sexe in getAll(sql, "nodrizas")})

        sql = """
                SELECT
                    id_cip_sec,
                    ingres
                FROM
                    jail_atesa_avui_60
                WHERE 
                    (sortida = '0000-00-00' OR sortida BETWEEN '{}' AND '{}')
                    -- AND durada >= 60
            """.format(self.dext_menys1any, self.dext)
        poblacio_ultim_any_jail = {id_cip_sec: ingres for id_cip_sec, ingres in getAll(sql, "nodrizas")}

        sql = """
                SELECT
                    id_cip_sec,
                    up,
                    edat,
                    sexe
                FROM
                    jail_assignada
              """
        self.poblacio.update({id_cip_sec: (up, ageConverter(edat, 5), sexConverter(sexe)) 
                             for id_cip_sec, up, edat, sexe in getAll(sql, "nodrizas")
                             if id_cip_sec in poblacio_ultim_any_jail})

    def get_prodoa0009PRS(self):
        """
        Inicialitza els valors dels indicadors "PRODOA0009PRS" i "PRODOP0009PRS" a 0 
        per a tots els conjunts de combinacions possibles de par�metres (UP, edat, sexe, 
        tipus de dent i an�lisi) en el diccionari `self.dades_pre`.

        Aquesta preparaci� �s necess�ria perqu�, segons la petici� de Paqui, 
        els indicadors han d'estar disponibles a LV encara que el seu valor sigui 0, ja que 
        en algun moment podrien apar�ixer casos amb dades.
        """

        ups_jail = ["07673", "07674", "07675", "07677", "07678", "07679", "07682", "15801"]
        edats_jail = ["EC1519", "EC2024", "EC2529", "EC3034", "EC3539", "EC4044", 
                      "EC4549", "EC5054", "EC5559", "EC6064", "EC6569", "EC7074", 
                      "EC7579", "EC8084", "EC8589"]
        sexes_jail = ["DONA", "HOME"]
        tipus_dent_jail = ["DDEF", "DTEMP", "NOIMP"]
        indicadors_jail = ["PRODOA0009PRS", "PRODOP0009PRS"]
        analisis_jail = ["DEN", "NUM"]

        # Inicialitza el diccionari amb valors predeterminats utilizant product
        for up, edat, sexe, tipus_dent, indicador, analisi in product(
            ups_jail, edats_jail, sexes_jail, tipus_dent_jail, indicadors_jail, analisis_jail):
            self.dades_pre[(up, edat, sexe, indicador, tipus_dent)][analisi] = 0

    def get_prevencio_neteja(self):
        """ Obt� informaci� de procediments de prevenci� i neteja dental de la taula odn511 (prstb511) de la base de dades. 
            - FC: Fluritzaci� amb cubetes (t�cnica utilitzades per prevenir la c�ries dental mitjan�ant l'aplicaci� t�pica de fluor)
            - FSC: Fluoritzaci� sense cubetes (t�cnica utilitzades per prevenir la c�ries dental mitjan�ant l'aplicaci� t�pica de fluor)
            - TT: Tartrectom�a (t�cnica per eliminar la placa i el sarro dental acumulat a la superf�cie dels dents)
        """
        
        self.prevencio_neteja = defaultdict(lambda: defaultdict(list))

        sql = """
                SELECT
                    id_cip_sec,
                    tb_data,
                    IF(tb_trac = 'FSC', 'FC', tb_trac)
                FROM
                    odn511
                WHERE
                    TB_TRAC IN ('FC', 'FSC', 'TT')
                    AND tb_data BETWEEN DATE'{}' AND DATE'{}'
              """.format(self.dext_menys1any, self.dext)
        for db in ("import", "import_jail"):
            for id_cip_sec, dat, tractament in getAll(sql, db):
                self.prevencio_neteja[tractament][id_cip_sec].append(dat)
                
    def get_restauracio_cirurgia(self):
        """ Obt� informaci� de tractaments d'odontologia restaurativa i cirurgia dental de la base de la taula odn508 (prstb508) de la base de dades 
            - SEG: Segellat (aplicaci� d'un material protector als dents, per evitar caries i altres danys)
            - EX: Exod�ncia (extracci� d'una pe�a dental que est� danyada, infectada, mal col�locada o no t� prou espai per cr�ixer correctament)
            - EXO: Exod�ncia quir�rgica (extracci� d'una pe�a dental a quir�fan)
            - OBT: Obturaci� (restauraci� dental que s'utilitza per reparar una pe�a dental danyada per c�ries, fissures o altres problemes. a.k.a. empastament dental)
            - CON: Reconstrucci� (restauraci� dental que s'utilitza per reparar una pe�a dental amb una major p�rdua d'estructura dental)
        """
        
        self.restauracio_cirurgia = defaultdict(lambda: defaultdict(lambda: defaultdict(set)))
        
        sql = """
                SELECT
                    id_cip_sec,
                    dtd_cod_p,
                    dtd_tra,
                    dtd_data
                FROM
                    odn508
                WHERE
                    dtd_tra IN ('SEG', 'EX', 'EXO', 'OBT', 'CON', 'END', 'CIA', 'RAD', 'FDP')
                    AND dtd_et = 'E'
                    AND dtd_data BETWEEN DATE'{}' AND DATE'{}'
              """.format(self.dext_menys1any, self.dext)
        for db in ("import", "import_jail"):
            for id_cip_sec, peca, tractament, dat in getAll(sql, db):
                tractament = 'OBT_CON' if tractament in ('OBT', 'CON') else tractament
                tractament = 'END' if tractament in ('END', 'CIA', 'RAD') else tractament
                tipus_dent = 'DDEF' if peca <= 50 else 'DTEMP'
                self.restauracio_cirurgia[tractament][tipus_dent][id_cip_sec].add((peca, dat))

    def get_calculs(self):
        """ Calculem els indicadors a partir de les dades recollides. """

        for tractament in self.prevencio_neteja:
            for analisi in codis[tractament]:
                indicadors_p = codis[tractament][analisi]['p']
                indicadors_p = [indicadors_p] if not isinstance(indicadors_p, list) else indicadors_p
                indicadors_a = codis[tractament][analisi]['a']
                indicadors_a = [indicadors_a] if not isinstance(indicadors_a, list) else indicadors_a

                for id_cip_sec, dates_tractament in self.prevencio_neteja[tractament].items():
                    poblacio_info = self.poblacio.get(id_cip_sec)
                    if poblacio_info:
                        up, edat_lv, sexe_lv = poblacio_info
                        for indicador_p, indicador_a in zip(tuple(indicadors_p), tuple(indicadors_a)):
                            self.dades_pre[up, edat_lv, sexe_lv, indicador_p, 'NOIMP'][analisi] += 1
                            self.dades_pre_pacient[id_cip_sec, up, edat_lv, sexe_lv, indicador_p, 'NOIMP'][analisi] += 1
                            if indicador_a[:10] != 'PRODOA0007':
                                self.dades_pre[up, edat_lv, sexe_lv, indicador_a, 'NOIMP'][analisi] += len(dates_tractament)
                                self.dades_pre_pacient[id_cip_sec, up, edat_lv, sexe_lv, indicador_a, 'NOIMP'][analisi] += len(dates_tractament)

        for tractament in self.restauracio_cirurgia:
            for analisi in codis[tractament]:
                indicadors_p = codis[tractament][analisi]['p']
                indicadors_p = [indicadors_p] if not isinstance(indicadors_p, list) else indicadors_p
                indicadors_a = codis[tractament][analisi]['a']
                indicadors_a = [indicadors_a] if not isinstance(indicadors_a, list) else indicadors_a
                
                for tipus_dent in self.restauracio_cirurgia[tractament]:
                    for id_cip_sec, detall_tractament in self.restauracio_cirurgia[tractament][tipus_dent].items():
                        poblacio_info = self.poblacio.get(id_cip_sec)
                        if poblacio_info:
                            up, edat_lv, sexe_lv = poblacio_info
                            for indicador_p, indicador_a in zip(indicadors_p, indicadors_a):
                                self.dades_pre[up, edat_lv, sexe_lv, indicador_p, tipus_dent][analisi] += 1
                                self.dades_pre_pacient[id_cip_sec, up, edat_lv, sexe_lv, indicador_p, tipus_dent][analisi] += 1
                                if indicador_a != 'PRODOA0007':
                                    self.dades_pre[up, edat_lv, sexe_lv, indicador_a, tipus_dent][analisi] += len(detall_tractament)
                                    self.dades_pre_pacient[id_cip_sec, up, edat_lv, sexe_lv, indicador_a, tipus_dent][analisi] += len(detall_tractament)
    
    def export_data(self):
        """ Creem les taules i exportem les dades processades. """

        upload = [
            [up, edat_lv, sexe_lv, indicador, tipus_dent, round(float(valors["NUM"]) / float(valors["DEN"]), 2) if valors["DEN"] else valors["NUM"]]
            for (up, edat_lv, sexe_lv, indicador, tipus_dent), valors in self.dades_pre.items()
            ]
        columns = "(up varchar(5), edat varchar(10), sexe varchar(5), indicador varchar(10), tipus_dent varchar(10), n decimal(8,2))"
        createTable(tb, columns, "altres", rm=True)
        listToTable(upload, tb, "altres")

        upload_pacient = [
            [id_cip_sec, up, edat_lv, sexe_lv, indicador, tipus_dent, valors.get("DEN", 0), valors["NUM"]]
            for (id_cip_sec, up, edat_lv, sexe_lv, indicador, tipus_dent), valors in self.dades_pre_pacient.items()
            ]
        columns = "(id_cip_sec int, up varchar(5), edat varchar(10), sexe varchar(5), indicador varchar(10), tipus_dent varchar(10), den decimal(8,2), num decimal(8,2))"
        createTable(tb_pacient, columns, "altres", rm=True)
        listToTable(upload_pacient, tb_pacient, "altres")
        
        sql = """
                SELECT
                    indicador,
                    concat('A', 'periodo'),
                    ics_codi,
                    'NOCLI',
                    edat,
                    tipus_dent,
                    sexe,
                    'N',
                    n
                FROM
                    {_altres}.{_tb} a
                INNER JOIN export.khx_centres b ON
                    a.up COLLATE latin1_general_ci = b.scs_codi
                WHERE
                    indicador NOT LIKE '%007'
            """.format(_altres="altres", _tb=tb)
        exportKhalix(sql, file_up)

        sql = """
                SELECT
                    CONCAT(indicador, 'PRS'),
                    concat('A', 'periodo'),
                    ics_codi,
                    'NOCLI',
                    edat,
                    tipus_dent,
                    sexe,
                    'N',
                    n
                FROM
                    {_altres}.{_tb} a
                INNER JOIN nodrizas.jail_centres b ON
                    a.up = b.scs_codi
                WHERE
                    indicador IN {_indicadors_jail}
            """.format(_altres="altres", _tb=tb, _indicadors_jail=indicadors_jail)
        exportKhalix(sql, file_up+"_JAIL")

if IS_MENSUAL:
    prestacions()