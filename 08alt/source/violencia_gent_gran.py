# coding: latin1

import sisapUtils as u
import collections as c
import datetime

# Obtenci� de la data d'extracci� de les dades

sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
            CASE
                WHEN (MONTH(data_ext) = 3 AND DAY(data_ext) >= 25) OR (MONTH(data_ext) = 4 AND DAY(data_ext) <= 5) OR
                     (MONTH(data_ext) = 6 AND DAY(data_ext) >= 25) OR (MONTH(data_ext) = 7 AND DAY(data_ext) <= 5) OR
                     (MONTH(data_ext) = 9 AND DAY(data_ext) >= 25) OR (MONTH(data_ext) = 10 AND DAY(data_ext) <= 5) OR
                     (MONTH(data_ext) = 12 AND DAY(data_ext) = 31)
                THEN 1
                ELSE 0
            END AS condicio_execucio
        FROM
            dextraccio
    """
data_ext, data_ext_menys1any, condicio_execucio = u.getOne(sql, 'nodrizas')

if u.IS_MENSUAL and condicio_execucio:
        
    # Obtenci� de la regi� de cada centre pel cat�leg de centres

    sql = """
            SELECT
                up_cod,
                regio_des
            FROM 
                cat_sisap_covid_dbc_rup
        """
    regio_des_dict = {up: regio_des for up, regio_des in u.getAll(sql, "import")}

    # Obtenci� del cat�leg de centres

    sql = """
            SELECT
                scs_codi,
                amb_desc,
                sap_desc,
                ics_desc
            FROM
                cat_centres
        """
    cat_centres = {up: {"amb_desc": amb_desc, "sap_desc": sap_desc, "ics_desc": ics_desc, "regio_des": regio_des_dict.get(up, None)} for up, amb_desc, sap_desc, ics_desc in u.getAll(sql, "nodrizas")}

    # Obtenci� de poblaci� assignada major de o amb 60 anys

    sql = """
            SELECT 
                id_cip_sec,
                up,
                edat
            FROM
                assignada_tot
            WHERE 	
                edat >= 60
        """
    poblacio = {id_cip_sec: {"up": up, "edat": edat} for id_cip_sec, up, edat in u.getAll(sql, "nodrizas") if up in cat_centres}


    # Obenci� dels problemes

    def sub_get_problemes(sql):

        sub_problemes = {id_cip_sec for id_cip_sec, in u.getAll(sql, "import") if id_cip_sec in poblacio}
        
        return sub_problemes

    problemes = set()

    codis_ps = ('C01-T74', 'T74', 'C01-T74.0', 'T74.0', 'C01-T74.01', 'C01-T74.01XA', 'C01-T74.01XD', 'C01-T74.01XS', 'C01-T74.02',
                'C01-T74.02XA', 'C01-T74.02XD', 'C01-T74.02XS', 'C01-T74.1', 'T74.1', 'C01-T74.11', 'C01-T74.11XA', 'C01-T74.11XD',
                'C01-T74.11XS', 'C01-T74.12', 'C01-T74.12XA', 'C01-T74.12XD', 'C01-T74.12XS', 'C01-T74.2', 'T74.2', 'C01-T74.21', 
                'C01-T74.21XA', 'C01-T74.21XD', 'C01-T74.21XS', 'C01-T74.22', 'C01-T74.22XA', 'C01-T74.22XD', 'C01-T74.22XS', 
                'C01-T74.3', 'T74.3', 'C01-T74.31', 'C01-T74.31XA', 'C01-T74.31XD', 'C01-T74.31XS', 'C01-T74.32', 'C01-T74.32XA', 
                'C01-T74.32XD', 'C01-T74.32XS', 'C01-T74.5', 'C01-T74.51', 'C01-T74.51XA', 'C01-T74.51XD', 'C01-T74.51XS', 
                'C01-T74.52', 'C01-T74.52XA', 'C01-T74.52XD', 'C01-T74.52XS', 'C01-T74.6', 'C01-T74.61', 'C01-T74.61XA', 
                'C01-T74.61XD', 'C01-T74.61XS', 'C01-T74.62', 'C01-T74.62XA', 'C01-T74.62XD', 'C01-T74.62XS', 'T74.8', 'C01-T74.9',
                'T74.9', 'C01-T74.91', 'C01-T74.91XA', 'C01-T74.91XD', 'C01-T74.91XS', 'C01-T74.92', 'C01-T74.92XA', 'C01-T74.92XD',
                'C01-T74.92XS', 'C01-N90.81', 'N90.81', 'C01-N90.810', 'C01-N90.811', 'C01-N90.812', 'C01-N90.813', 'C01-N90.818', 
                'C01-T76', 'C01-T76.0', 'C01-T76.01', 'C01-T76.01XA', 'C01-T76.01XD', 'C01-T76.01XS', 'C01-T76.02', 'C01-T76.1', 
                'C01-T76.11', 'C01-T76.11XA', 'C01-T76.11XD', 'C01-T76.11XS', 'C01-T76.12', 'C01-T76.2', 'C01-T76.21', 
                'C01-T76.21XA', 'C01-T76.21XD', 'C01-T76.21XS', 'C01-T76.3', 'C01-T76.31', 'C01-T76.31XA', 'C01-T76.31XD', 
                'C01-T76.31XS', 'C01-T76.5', 'C01-T76.51', 'C01-T76.51XA', 'C01-T76.51XD', 'C01-T76.51XS', 'C01-T76.6', 
                'C01-T76.61', 'C01-T76.61XA', 'C01-T76.61XD', 'C01-T76.61XS', 'C01-T76.9', 'C01-T76.91', 'C01-T76.91XA', 
                'C01-T76.91XD', 'C01-T76.91XS', 'Z04.4', 'C01-Z04.4', 'C01-Z04.41', 'C01-Z04.42', 'C01-Z04.7', 'C01-Z04.71', 
                'C01-Z04.72', 'C01-Z69', 'C01-Z69.1', 'C01-Z69.11', 'C01-Z69.8', 'C01-Z69.81', 'C01-Z56.81', 'Y07', 'Y07.0', 
                'Y07.1', 'Y07.2', 'Y07.3', 'Y07.8', 'Y07.9', 'IRE08625', 'IRE08632', 'IRE08596')

    sql = """
            SELECT
                id_cip_sec 
            FROM
                {_table}
            WHERE
                pr_dde BETWEEN '{_data_ext_menys1any}' AND '{_data_ext}'
                AND pr_data_baixa = ''
                AND pr_cod_ps in {_codis_ps}
        """
    jobs = [sql.format(_table=table, _data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext, _codis_ps=codis_ps) for table in u.getSubTables("problemes") if table[-6:] != '_s6951']
    count = 0
    for sub_problemes in u.multiprocess(sub_get_problemes, jobs, 4):
        problemes.update(sub_problemes)
        count+=1
        print("    probs: {}/{}. {}".format(count, len(jobs), datetime.datetime.now()))
        
    # Creaci� dels resultats
        
    resultats_up = c.defaultdict(c.Counter)
    resultats = list()

    for id_cip_sec in poblacio:
        up = poblacio[id_cip_sec]["up"]
        resultats_up[up]["DEN"] += 1
        resultats_up[up]["NUM"] += 1 if id_cip_sec in problemes else 0

    total_den, total_num = 0, 0
    for up in resultats_up:
        amb_desc, sap_desc, ics_desc, regio_des = (cat_centres[up][k] for k in ("amb_desc", "sap_desc", "ics_desc", "regio_des"))
        den, num = (resultats_up[up][k] for k in ("DEN", "NUM"))
        total_den += den
        total_num += num
        taxa = round(float(num)*1000/den, 3)
        resultats.append((up, amb_desc, sap_desc, ics_desc, regio_des, den, num, taxa)) 

    total_taxa = round(float(total_num)*1000/total_den, 3)
    resultats.append((None, "TOTAL CATALUNYA", None, None, None, total_den, total_num, total_taxa)) 
        
    # Exportem resultats a taula

    tb_name = "peticio_contel_viol_gent_gran"
    db_name = "altres"
    columnes = "(up varchar(5), amb_desc varchar(40), sap_desc varchar(40), ics_desc varchar(40), regio_des varchar(255), den int, num int, taxa int)"

    u.createTable(tb_name, columnes, db_name, rm=True)
    u.listToTable(resultats, tb_name, db_name)

    # Exportem resulats a arxiu txt

    arxiu_resultats = u.tempFolder + 'peticio_violencia_gent_gran_{_data_ext_any}_{_data_ext_mes}_{_data_ext_dia}.csv'.format(_data_ext_any=data_ext.year, _data_ext_mes=str(data_ext.month).zfill(2), _data_ext_dia=data_ext.day)
    u.writeCSV(arxiu_resultats, [('UP', 'AMB_DESC', 'SAP_DESC', 'ICS_DESC', 'REGIO_DESC', 'DEN', 'NUM', 'TAXA')] + resultats, sep=";")

    # Exportem arxiu txt via email

    email_capcalera = "Petici� gent gran"
    email_text = """

    Benvolguts/des,

    Us adjuntem les incid�ncies per 1000 dels darrers 12 mesos dels casos de maltractament envers la gent gran (>= 60 anys).

    Rebeu una cordial salutaci�,

    Sistema d'Informaci� dels Serveis d'Atenci� Prim�ria (SISAP)
    Direcci� Assistencial d'Atenci� Prim�ria
    Institut Catal� de la Salut
    Departament de Salut | Generalitat de Catalunya

    Gran Via de les Corts Catalanes, 587 | 08007 Barcelona | Tel. 934 824 616

    manuelquintana@gencat.cat | gencat.cat/ics

    ---

    Aquest missatge s'adre�a exclusivament a la persona destinat�ria i pot contenir informaci� privilegiada o confidencial. Si no sou la persona destinat�ria indicada, us recordem que la utilitzaci�, divulgaci� i/o c�pia sense autoritzaci� est� prohibida en virtut de la legislaci� vigent. Si heu rebut aquest missatge per error, us demanem que ens ho feu saber immediatament per aquesta via i que el destru�u.

    ---

    Abans d'imprimir aquest correu, assegureu-vos que �s realment necessari.
    """

    u.sendGeneral('SISAP <sisap@gencat.cat>',
                  'jccontel@gencat.cat',
                  ('mquintana.apms.ics@gencat.cat', 'alejandrorivera@gencat.cat'),
                  email_capcalera,
                  email_text,
                  arxiu_resultats)