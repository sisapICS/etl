# -*- coding: latin1 -*-


from sisapUtils import *
from multiprocessing import Pool
import sisaptools as tools
import collections as c

AGREGAT = True
INDIVIDUAL = False

def doIt(param):
    origen,table,desti,ups,up_descs = param
    result = c.defaultdict(dict)
    sql = """SELECT
                count(1),
                alv_up_peticio,
                alv_estat
            FROM
                {origen}
            WHERE
                alv_tipus = 'DER'
                AND ALV_MEMO like 'Cal revisar.'
                AND alv_text6 != 'Derivacions TAO'
                AND ALV_DATA_AL >= DATE '2024-06-20'
            GROUP BY
                alv_up_peticio,
                alv_estat""".format(origen=origen)
    for n, up, estat in getAll(sql,'import'):
        if up in ups:
            if estat == 'P': estat = 'Pendent'
            elif estat == 'R': estat = 'Revisat'
            result[up][estat] = n
    if result:
        upload = []
        for up in result:
            up_desc = up_descs[up]
            pendents = 0
            revisats = 0
            for estat, n in result[up].items():
                if estat == 'Pendent':
                    pendents = n
                else:
                    revisats = n
            upload.append((up, up_desc, revisats, pendents, revisats + pendents))
        listToTable(upload,table,desti)


def doIt2(params):
    sector,table,desti,ups,up_descs = params
    result = c.defaultdict(dict)
    upload = []
    sql = """SELECT
                alv_cip,
                alv_up_peticio,
                ALV_DATA_AL,
                alv_estat
            FROM
                prstb550
            WHERE
                alv_tipus = 'DER'
                AND ALV_MEMO like 'Cal revisar.'
                AND alv_text6 != 'Derivacions TAO'
                AND ALV_DATA_AL >= DATE '2024-06-20'"""
    for cip, up, data, estat in getAll(sql,sector):
        if up in ups:
            if estat == 'P': estat = 'Pendent'
            elif estat == 'R': estat = 'Revisat'
            upload.append((cip, up, data, estat))
    if upload:
        listToTable(upload,table,desti)

if __name__ == '__main__':
    ups = [
            '00151',
            '00152',
            '00153',
            '00154',
            '00155',
            '00156',
            '00187',
            '00158',
            '00159',
            '00168',
            '00169',
            '00170',
            '00195',
            '00172',
            '00173',
            '00174',
            '00197',
            '00175',
            '00176',
            '00198',
            '00199',
            '00200',
            '00201',
            '00177',
            '00178',
            '00202',
            '00203',
            '00181',
            '00182',
            '00183',
            '00204',
            '00160',
            '00163',
            '00164',
            '00165',
            '00166',
            '00167',
            '00193',
            '00161',
            '00162',
            '00189',
            '00205',
            '00206',
            '00157',
            '03449',
            '04819',
            '04376',
            '04056',
            '04547',
            '04546',
            '05239',
            '00179',
            '04957',
            '07961',
            '07962',
            '08096',
            '08117',
            '08118'
    ]
    sql = """select scs_codi, ics_desc from nodrizas.cat_centres"""
    up_descs = {}
    for up, up_desc in getAll(sql, 'nodrizas'):
        up_descs[up] = up_desc
    if AGREGAT:
        table = 'sisap_avisos_setmanals'
        desti = 'redics'
        createTable(table,'(up varchar2(5), up_desc varchar2(250), revisats int, pendents int, totals int)',desti,rm=True)
        printTime('sectors')
        p = Pool(8)
        p.map(doIt, [(origen,table,desti,ups,up_descs) for (origen, n) in getSubTables("alertes").items()])
        p.close()
        printTime('fin')
        upload = []
        sql = """SELECT * FROM sisap_avisos_setmanals     """
        for row in getAll(sql, 'redics'):
            upload.append(row)
        file_eap = tempFolder + 'alertes.csv'
        writeCSV(file_eap, [('UP', 'UP_DESC', 'REVISTATS', 'PRENDENTS', 'TOTALS')] + upload, sep=";")
        # email
        sendGeneral('SISAP <sisap@gencat.cat>',
            ('mooliveras@gencat.cat', 'roser.cantenys@catsalut.cat', 'eballo.girona.ics@gencat.cat'),
            ('mooliveras@gencat.cat', 'roser.cantenys@catsalut.cat', 'eballo.girona.ics@gencat.cat'),
            'Alertes clíniques',
            """Bon dia, \n Adjuntem l'arxiu de seguiment d'alertes clíniques. \n Atentament,""",
            file_eap)
    if INDIVIDUAL:
        table = 'sisap_avisos_setmanals_ind'
        desti = 'exadata'
        createTable(table,'(cip varchar2(15), up varchar2(5), data date, estat varchar2(10))',desti,rm=True)
        grantSelect(table, "DWSISAP_ROL", desti)
        printTime('sectors')
        p = Pool(8)
        p.map(doIt2, [(sector,table,desti,ups,up_descs) for sector in sectors])
        p.close()
        printTime('fin')
        

    