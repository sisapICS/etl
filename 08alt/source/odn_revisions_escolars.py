#  coding: latin1

import sisapUtils as u
import collections as c


def get_poblacio():
    
    global assignada_tot
    
    sql= """
        SELECT
            id_cip_sec,
            up,
            sexe
        FROM
            assignada_tot
          """
    assignada_tot = {id_cip_sec: {"up": up, "sexe": sexe} for id_cip_sec, up, sexe in u.getAll(sql, "nodrizas")}


def get_centres():
    
    global cat_centres
    
    sql = """
            SELECT
                scs_codi,
                amb_desc,
                sap_desc,
                ics_desc
            FROM
                cat_centres
          """
    cat_centres = {up: {"amb_desc": amb_desc, "sap_desc": sap_desc, "ics_desc": ics_desc} \
                    for up, amb_desc, sap_desc, ics_desc in u.getAll(sql, "nodrizas")}
    

def get_localitats():
    
    global cat_localitats
        
    cat_localitats = dict()

    sql = """
            SELECT
                codi_up,
                desc_localitat
            FROM
                dwdimics.dim_up_catsalut
          """
    for up, localitat in u.getAll(sql, "exadata"):
        if up in cat_centres:
            cat_localitats[up] = localitat
        

def get_curs_escolar():
    
    global curs_escolar
    
    curs_escolar = dict()

    sql = """
            SELECT
                id_cip_sec,
                CASE
                    WHEN YEAR(data_naix) = {} THEN 'PRIMER'
                    ELSE 'SIS�'
                END AS 'CURS'
            FROM
                assignada_tot
            WHERE
                YEAR(data_naix) IN ({}, {})
        """.format(data_ext.year-7, data_ext.year-12, data_ext.year-7)
    for id_cip_sec, curs in u.getAll(sql, "nodrizas"):
        curs = "Primer" if curs == data_ext.year-7 else "Sis�"
        curs_escolar[id_cip_sec] = curs
        

def get_revisions_escoles():
    
    global revisions_escoles
    
    sql = """
            SELECT
                id_cip_sec,
                ro_escola
            FROM
                odn510
            WHERE
                ((YEAR (ro_data) = {} AND MONTH (ro_data) >= 9)
                 OR (YEAR (ro_data) = {} AND MONTH (ro_data) <= 6))
                AND ro_estat = 'F'
                AND ro_escola <> ''
          """.format(data_ext.year-1, data_ext.year)
    revisions_escoles = {id_cip_sec: {"escola": escola.title(), "curs": curs_escolar[id_cip_sec]} \
                         for id_cip_sec, escola in u.getAll(sql, "import") if id_cip_sec in curs_escolar}


def get_fitxa_dental_escoles():
    
    global fitxa_dental_escoles
    
    sql = """
            SELECT
                id_cip_sec,
                cfd_ind_caod,
                cfd_ind_cod
            FROM
                odn505
          """
    fitxa_dental_escoles = {id_cip_sec: {"cfd_caod": cfd_caod, "cfd_cod": cfd_cod} \
                            for id_cip_sec, cfd_caod, cfd_cod in u.getAll(sql, "import") if id_cip_sec in revisions_escoles}
    

def get_resultats():
    
    global resultat_llista

    resultat_counter = c.defaultdict(c.Counter)
    resultat_llista = list()

    for id_cip_sec in fitxa_dental_escoles:
        cfd_caod, cfd_cod = fitxa_dental_escoles[id_cip_sec]["cfd_caod"], fitxa_dental_escoles[id_cip_sec]["cfd_cod"]
        escola, curs = revisions_escoles[id_cip_sec]["escola"], revisions_escoles[id_cip_sec]["curs"]
        lliures_caries = 1 if (cfd_caod + cfd_cod == 0) else 0
        up, sexe = assignada_tot[id_cip_sec]["up"], assignada_tot[id_cip_sec]["sexe"]
        amb_desc, sap_desc, ics_desc = cat_centres[up]["amb_desc"], cat_centres[up]["sap_desc"], cat_centres[up]["ics_desc"]
        escola_alter = escola.upper()
        escola_alter.replace("ESCOLA", "").replace(" ", "")
        localitat = cat_localitats.get(up, "")
        resultat_counter[(amb_desc.title(), sap_desc.title(), up, ics_desc.title(), localitat.title(), escola.title(), curs.title(), sexe.title())]["COUNT_PACIENTS"] += 1
        resultat_counter[(amb_desc.title(), sap_desc.title(), up, ics_desc.title(), localitat.title(), escola.title(), curs.title(), sexe.title())]["COUNT_PACIENTS_LLIURES_CARIES"] += lliures_caries

    for (amb_desc, sap_desc, up, ics_desc, localitat, escola, curs, sexe), dades in resultat_counter.items():
        COUNT_PACIENTS = dades["COUNT_PACIENTS"]
        COUNT_PACIENTS_LLIURES_CARIES = dades["COUNT_PACIENTS_LLIURES_CARIES"]
        resultat_llista.append((amb_desc, sap_desc, up, ics_desc, localitat, escola, curs, sexe, COUNT_PACIENTS, COUNT_PACIENTS_LLIURES_CARIES))
        

def export_dades():
    
    global tb_db, tb_nom

    tb_nom = "revisions_odonto_escolars_up_{}_{}".format(data_ext.year-1, data_ext.year)
    tb_db = "test"
    tb_columnes = "(amb_desc varchar(40), sap_desc varchar(40), up varchar(5), ics_desc varchar(40), localitat varchar(40), escola varchar(80), curs varchar(40), sexe varchar(40), COUNT_PACIENTS int, COUNT_PACIENTS_LLIURES_CARIES int)"

    u.createTable(tb_nom, tb_columnes, tb_db, rm = True)
    u.listToTable(sorted(resultat_llista), tb_nom, tb_db)


def send_email():

    me = "SISAP <sisap@gencat.cat>"
    to = "mquintana@ambitcp.catsalut.net"
    cc = "alejandrorivera@gencat.cat"
    subject = "Dades de l'�ltimc curs sobre revisions escolars d'odontolog�a"
    text = "Bon dia\n\nEt faig arribar les dades de revisions escolars d'odontolog�a.\nLes pots consultar mitjan�ant la seg�ent query:\n\n    SELECT\n        *\n    FROM\n        {}.{}\n\nSalutacions,\n\nEl teu bot de confian�a *__*".format(tb_db, tb_nom)
    u.sendGeneral(me, to, cc, subject, text)


def get_dextraccio():

    global data_ext

    sql = """
            SELECT
                data_ext 
            FROM
                dextraccio
          """
    data_ext, = u.getOne(sql, "nodrizas")

if u.IS_MENSUAL:
    get_dextraccio();                           print("get_dextraccio()")
    if data_ext.month == 6:
        get_poblacio();                         print("get_poblacio()")
        get_centres();                          print("get_centres()")
        get_localitats();                       print("get_localitats()")
        get_curs_escolar();                     print("get_curs_escolar()")
        get_revisions_escoles();                print("get_revisions_escoles()")
        get_fitxa_dental_escoles();             print("get_fitxa_dental_escoles()")
        get_resultats();                        print("get_resultats()")
        export_dades();                         print("export_dades()")
        send_email();                           print("send_email()")