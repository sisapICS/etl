# -*- coding: latin1 -*-

"""
.
"""

import collections as c
import sisapUtils as u

indicadors_no_exportar_LV = ('AGC0406', 'AGC0410', 'AGC0411', 'AGC0412', 'AGC0413', 
                             'AGC0507', 'AGC0509', 'AGC0517', 'AGC0518', 'AGC0519', 
                             'AGC0606', 'AGC0610', 'AGC0611', 'AGC0612', 'AGC0613', 
                             'AGC0707', 'AGC0709', 'AGC0717', 'AGC0718', 'AGC0719')

map_act = {
    'SE': 'PJ',  # SE - Salut i Escola: altres                          PJ - Projectes institucionals (PAFES, Salut i Escola)
    'OTR': 'AL', # OTR - No identificat / desconegut                    AL - Altres 
    'SB': 'PG',  # SB - Salut bucodental                                PG - Participaci� en grups
    'PR': 'PG',  # PR - Activitat f�sica i alimentaci� saludable        PG - Participaci� en grups
    'CO': 'MC',  # CO - ?                                               MC - Suport a les malaties cr�niques
    'CA': 'PG'   # CA - ?                                               PG - Participaci� en grups
    }

map_tem = {
    'PS': 'AL',  # PS - ?
    'VA': 'AL',  # VA - ?
    'OTR': 'AL', # OTR - No identificat / desconegut
    'II': 'AL',  # II - ?
    'CR': 'AL',  # CR - ?
    'CO': 'AL'}  # CO - ?

sub_inds_tip = {
    'G': '01',   # Grupal
    'GC': '02',  # Grupal i Comunit�ria
    'C': '03'    # Comunit�ria
    }

sub_inds_act = {
    'SI': '01',  # SI - Sessions informatives
    'TA': '02',  # TA - Tabac, alcohol, drogues i altres adiccions
    'PJ': '03',  # PJ - Projectes institucionals (PAFES, Salut i Escola)
    'PG': '04',  # PG - Participaci� en grups
    'AL': '05',  # AL - Altres
    'SE': '06',  # SE - Salut i Escola: altres
    'ST': '08',  # ST - ?
    'MC': '09',  # MC - Suport a les malaties cr�niques
    'SB': '10',  # SB - Salut bucodental
    'PR': '11',  # PR - ?
    'CO': '12',  # CO - ?
    'CA': '13'   # CA - ?
    }

sub_inds_tem = {
    'AF': '01',  # AF - Activitat f�sica i alimentaci� saludable
    'MC': '02',  # MC - Suport a les malaties cr�niques
    'SE': '03',  # SE - Salut i Escola: altres
    'SM': '04',  # SM - Salut mental i benestar
    'ES': '05',  # ES - Envelliment saludable
    'MI': '06',  # MI - Salut maternoinfantil
    'PS': '07',  # PS - ?
    'AL': '08',  # AL - Altres
    'VA': '09',  # VA - ?
    'TA': '11',  # TA - Tabac, alcohol, drogues i altres adiccions
    'SB': '12',  # SB - Salut bucodental
    'CU': '13',  # CU - Cuidadors
    'VI': '14',  # VI - Viol�ncia i abusos
    'RE': '15',  # RE - Risc d'exclusi� social
    'SS': '16',  # SS - Prevenci� i promoci� de la salut sexual i reproductiva
    'II': '17',  # II - ?
    'CR': '18',  # CR - ?
    'CO': '19',  # CO - ?
    'S2': '20',  # S2 - Salut i Escola: Afectivitat i socialitzaci�
    'S3': '21',  # S3 - Salut i Escola: H�bits saludables
    'S1': '22',  # S1 - Salut i Escola: Benestar emocional
    'S4': '23',  # S4 - Salut i Escola: Seguretat i riscos
    'S5': '24',  # S5 - Salut i Escola: Presentaci� de programa
    None: '25'
    }

def get_dates():
    """ Obtenci� de la data d'extracci� de les dades, aix� com la data d'un any enrere. """

    global data_ext_menys1any, data_ext

    sql = """
            SELECT
                date_format(date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY), '%Y%m%d'),
                date_format(data_ext, '%Y%m%d')
            FROM
                dextraccio
            """
    data_ext_menys1any, data_ext = u.getOne(sql, "nodrizas")

def get_centres():
    """ Obtenci� dels codis identificadors dels centres. """

    global centres

    sql = """
            SELECT
                scs_codi,
                ics_codi
            FROM
                cat_centres
            """
    centres = {up: br for up, br in u.getAll(sql, "nodrizas")}

def get_poblacio():
    """ Obtenci� de la poblaci� atesa, no institucionalitzada i no ATDOM imputada per centre. """
   
    sql = """
            SELECT
                id_cip_sec,
                up
            FROM
                assignada_tot
            WHERE
                ates = 1
                AND institucionalitzat = 0
                AND atdom = 0
            """
    for id_cip_sec, up, in u.getAll(sql, "nodrizas"):
        br = centres.get(up)
        if br:
            for ind in ('AGC01', 'AGC0201', 'AGC03'):
                resultat[(br, ind, 'DEN')] += 1
                if ind in ('AGC01', 'AGC03'):
                    for v in sub_inds_tip.values():
                        resultat[(br, '{}{}'.format(ind, v), 'DEN')] += 1

def sub_get_sessions(sql):

    sub_sessions = {(codi_sector, grup_num) for codi_sector, grup_num in u.getAll(sql, "import")}
    
    return sub_sessions

def get_sessions():
    """ Obtenci� dels identificadors de les sessions grupals realitzades l'�ltim any. """

    global sessions;        sessions = set()

    sql = """
            SELECT
                codi_sector,
                sgru_num_grup
            FROM
                {}
            WHERE
                sgru_data BETWEEN '{}' AND '{}'
            """

    jobs = [sql.format(table, data_ext_menys1any, data_ext) for table in u.getSubTables("grupal5")]

    for sub_sessions in u.multiprocess(sub_get_sessions, jobs, 8):
        sessions.update(sub_sessions)

def sub_get_participants(sql):

    sub_participants = c.Counter()

    for codi_sector, grup_num, up in u.getAll(sql, "import"):
        br = centres.get(up)
        grup_id = (codi_sector, grup_num)
        if grup_id in sessions and br:
            sub_participants[br] += 1
    
    return sub_participants

def get_participants():
    """ Obtenci� del nombre participants en les sessions grupals realitzades l'�ltim any (cada registre de grupal3 correspon a un pacient). """

    sql = """
            SELECT
                codi_sector,
                pagr_num_grup,
                pagr_up
            FROM
                {}
            WHERE
                pagr_data_alta BETWEEN '{}' AND '{}'
            """
            
    jobs = [sql.format(table, data_ext_menys1any, data_ext) for table in u.getSubTables("grupal3")]

    for sub_participants in u.multiprocess(sub_get_participants, jobs, 8):
        for br, n in sub_participants.items():
            resultat[(br, 'AGC0201', 'NUM')] += n

def sub_get_activitats(sql):

    sub_activitats = []

    for codi_sector, grup_num, up, grup_tipus, hora_ini, hora_fi, n_sessions, codi_act, codi_tem in u.getAll(sql, "import"):
        br = centres.get(up)
        grup_id = (codi_sector, grup_num)
        if br and (grup_tipus == 'C' or grup_id in sessions):
            n_sessions = n_sessions if n_sessions else 1
            hores = n_sessions * ((hora_fi - hora_ini) / 3600.0) if hora_ini and hora_fi else 0
            codi_act = map_act.get(codi_act, codi_act)
            codi_tem = map_tem.get(codi_tem, codi_tem)

            sub_ind_tip = sub_inds_tip.get(grup_tipus)
            sub_ind_act= sub_inds_act.get(codi_act, '05')
            sub_ind_tem = sub_inds_tem.get(codi_tem, '08')

            sub_activitats.append((br, hores, sub_ind_tip, sub_ind_act, sub_ind_tem))
    
    return sub_activitats

def get_activitats():
    """."""

    sql = """
            SELECT
                codi_sector,
                grup_num,
                grup_codi_up,
                grup_tipus,
                grup_hora_ini,
                grup_hora_fi,
                grup_num_sessions,
                grup_tipus_activitat,
                grup_activitat
            FROM
                {}
            WHERE
                grup_data_ini BETWEEN '{}' AND '{}'
                AND grup_validada = 'S'
                AND grup_tipus_activitat <> ''
                AND grup_activitat <> ''
            """
            
    jobs = [sql.format(table, data_ext_menys1any, data_ext) for table in u.getSubTables("grupal4")]

    for sub_activitats in u.multiprocess(sub_get_activitats, jobs, 8):
        for br, hores, sub_ind_tip, sub_ind_act, sub_ind_tem in sub_activitats:

            # NUM d'AGC01 = DEN d'AGC04 i AGC05

            resultat[(br, 'AGC01', 'NUM')] += 1
            resultat[(br, 'AGC01{}'.format(sub_ind_tip), 'NUM')] += 1

            for _sub_ind_act in sub_inds_act.values():
                    resultat[(br, 'AGC04{}'.format(_sub_ind_act), 'DEN')] += 1
            resultat[(br, 'AGC04{}'.format(sub_ind_act), 'NUM')] += 1

            resultat[(br, 'AGC05', 'DEN')] += 1
            for _sub_ind_tem in sub_inds_tem.values():
                    resultat[(br, 'AGC05{}'.format(_sub_ind_tem), 'DEN')] += 1
            resultat[(br, 'AGC05{}'.format(sub_ind_tem), 'NUM')] += 1

            # NUM d'AGC03 = DEN d'AGC06 i AGC07

            resultat[(br, 'AGC03', 'NUM')] += hores
            resultat[(br, 'AGC03{}'.format(sub_ind_tip), 'NUM')] += hores
            
            for _sub_ind_act in sub_inds_act.values():
                    resultat[(br, 'AGC06{}'.format(_sub_ind_act), 'DEN')] += hores
            resultat[(br, 'AGC06{}'.format(sub_ind_act), 'NUM')] += hores

            for _sub_ind_tem in sub_inds_tem.values():
                    resultat[(br, 'AGC07{}'.format(_sub_ind_tem), 'DEN')] += hores
            resultat[(br, 'AGC07{}'.format(sub_ind_tem), 'NUM')] += hores

            # Resta de NUM

            if sub_ind_tem in ('03','20','21','22','23','24'):
                resultat[(br, 'AGC0525', 'NUM')] += 1

            if sub_ind_tem in ('03','20','21','22','23','24'):
                resultat[(br, 'AGC0725', 'NUM')] += hores

def export_klx():
    """."""

    tb_name = 'exp_khalix_grupal_comunitaria'
    db_name = 'altres'
    col_names = '(br varchar(5), ind varchar(10), tipus varchar(3), n double)'
    file = 'GRUPAL_COMUNITARIA'

    u.createTable(tb_name, col_names, db_name, rm=True)
    upload = [(br, ind, tipus, n) for (br, ind, tipus), n in resultat.items()]
    u.listToTable(upload, tb_name, db_name)

    sql = """
            SELECT
                ind,
                'Aperiodo',
                br,
                tipus,
                'NOCAT',
                'NOIMP',
                'DIM6SET',
                'N',
                n
            FROM
                {}.{}
            WHERE
                ind NOT IN {}
            """.format(db_name, tb_name, indicadors_no_exportar_LV)
    u.exportKhalix(sql, file)

if __name__ == '__main__':

    resultat = c.Counter()

    get_dates();                           print("get_dates()")
    get_centres();                         print("get_centres()")
    get_poblacio();                        print("get_poblacio()")
    get_sessions();                        print("get_sessions()")
    get_participants();                    print("get_participants()")
    get_activitats();                      print("get_activitats()")
    export_klx();                          print("export_klx()")