import sisapUtils as u
import sisaptools as t
import collections as c
import datetime as d

DEXTD = u.getOne("select data_ext from dextraccio", "nodrizas")[0]

pob = c.defaultdict(dict)
sql = """
    SELECT id_cip_sec,
    CASE es_cod
        WHEN 'ER0001' THEN 'PCC'
        WHEN 'ER0002' THEN 'MACA'
        ELSE 'error'
    END
    FROM estats
    where es_dde <= DATE '{DEXTD}'
    """.format(DEXTD=DEXTD)

for id, codi in u.getAll(sql, 'import'):
    pob[id][codi] = 1
u.printTime('estats')
piic = set()
piirV = set()
piirT = set()
piirF = c.defaultdict(set)
maca = set()
sql = """
    select id_cip_sec, left(mi_cod_var, 8), right(mi_cod_var, 2)
    from piic
    where left(mi_cod_var, 8) in ('T4101001', 'T4101002', 'T4101017', 'T4101014', 'T4101015')
    and mi_data_reg between adddate(DATE '{DEXTD}', interval -1 year) and DATE '{DEXTD}'
""".format(DEXTD=DEXTD)
for id, codi, val in u.getAll(sql, 'import'):
    if codi == 'T4101001':
        piic.add(id)
    elif codi == 'T4101002':
        maca.add(id)
    elif codi == 'T4101014':
        piirV.add((id, val))
    elif codi == 'T4101015':
        piirT.add((id, val))
    else:
        piirF[id].add(val)
u.printTime('piic 1')
for (id, val) in piirV:
    if (id, val) in piirT:
        piic.add(id)
u.printTime('piic 2')
for id in piirF:
    if len(piirF[id]) == 4:
        piic.add(id)
u.printTime('piic 3')
for id in list(piic):
    if id in pob and 'MACA' in pob[id] and id not in maca:
        piic.remove(id)
u.printTime('piic')

upload = []
for id in pob:
    pcc = pob[id]['PCC'] if 'PCC' in pob[id] else 0
    maca = pob[id]['MACA'] if 'MACA' in pob[id] else 0
    in_piic = 0
    if id in piic:
        in_piic = 1
    upload.append((id, pcc, maca, in_piic))
cols = "(id_cip_sec int, pcc int, maca int, piic int)"
u.createTable('mst_piic', cols, 'altres', rm=True)
u.listToTable(upload, 'mst_piic', 'altres')

sql = """
    SELECT id_cip_sec
    FROM assignada_tot
"""
pob = set()
for id, in u.getAll(sql, 'nodrizas'):
    pob.add(id)

sql = """
    SELECT id_cip_sec
    FROM xml_detall
    WHERE xml_data_alta between adddate(DATE '{DEXTD}', interval -1 year) and DATE '{DEXTD}'
    AND camp_codi IN ('REL_RESULT', 'ACT_RESULT', 'HAB_RESULT', 'CON_RESULT')
    AND camp_valor != 'Sense Valorar'
""".format(DEXTD=DEXTD)
valoracio = set()
for id, in u.getAll(sql, 'import'):
        valoracio.add(id)

for id in pob:
    if id in valoracio:
        upload.append((id, 1))
    else:
        upload.append((id, 0))
cols = "(id_cip_sec int, valoracio int)"
u.createTable('valoracio_cuidador', cols, 'altres', rm=True)
u.listToTable(upload, 'valoracio_cuidador', 'altres')
u.printTime('fi')
