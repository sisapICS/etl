# -*- coding: utf-8 -*-

import collections as c
import sisapUtils as u
import sisaptools as t
import datetime as d
from dateutil.relativedelta import relativedelta

DEXTD = t.Database("p2262", "nodrizas").get_one("select data_ext from dextraccio")[0]
LAST12 = DEXTD + relativedelta(years=-1)

class Atesa():
    def __init__(self):
        u.printTime('Inici')
        self.get_ups_litoral()
        u.printTime('litoral')
        self.get_assignada()
        u.printTime('assignada')
        self.idciptohash()
        u.printTime('idcip to hash')
        self.seguiment_ups()
        u.printTime('seguiment')
        self.get_visites()
        u.printTime('visites')
        self.upload()
        u.printTime('Fi')


    def get_ups_litoral(self):
        
        sql = """
            SELECT up_codi_up_scs, up_desc_up_ics FROM gcctb008 a
            INNER JOIN gcctb007 b 
            ON a.up_codi_up_ics = b.up_codi_up_ics
            WHERE dap_codi_dap = '51'
        """
        self.ups_litoral = {}
        self.ups_litoral['00441'] = 'G�tic'
        u.printTime("length ups_litoral " + str(len(self.ups_litoral)))
        '''for e, sector in enumerate(u.sectors):
            for up, desc in u.getAll(sql, sector):
                self.ups_litoral[up] = desc'''

    def get_assignada(self):
        sql = """
            SELECT id_cip_sec, up
            FROM assignada_tot"""
        # where up = '00441'
        self.up_assig = c.defaultdict()
        for id_cip, up in u.getAll(sql, 'nodrizas'):
            if up in self.ups_litoral:
                self.up_assig[id_cip] = up
        u.printTime("length up_assig " + str(len(self.up_assig)))

    def idciptohash(self):
        hashes = {}
        sql = "select id_cip_sec, hash_d from u11"
        for id_cip, hash_d in u.getAll(sql, 'import'):
            hashes[hash_d] = id_cip
        sql = "select hash_covid, hash_redics from dwsisap.pdptb101_relacio"
        self.conversor = {}
        for hash_c, hash_r in u.getAll(sql, 'exadata'):
            # hash covid a id_cip
            if hash_r in hashes:
                self.conversor[hash_c] = hashes[hash_r]
        u.printTime("length conversor " + str(len(self.conversor)))

    def get_visites(self):
        self.visites = c.Counter()
        poblacio = c.defaultdict(set)
        self.visites2 = c.Counter()
        poblacio2 = c.defaultdict(set)
        sql = """
            SELECT pacient, up, data
            FROM dwsisap.sisap_master_visites
            WHERE data > DATE '{L12}' and data < DATE '{DEXT}'
            AND situacio = 'R'
            AND REL_PROVEIDOR IN (3,4)
            AND up = '00441'
        """.format(L12=LAST12, DEXT=DEXTD)
        for hash_c, up, data in u.getAll(sql, 'exadata'):
            #if up in self.ups_litoral and hash_c in self.conversor and self.conversor[hash_c] in self.up_assig:
            #if hash_c in self.conversor and self.conversor[hash_c] in self.up_assig:
            # aixi veiem quins pacients han estat atesos a up = '00441' pero no hi estan assignats
            if hash_c in self.conversor and self.conversor[hash_c] not in self.up_assig:
                #if up != self.up_assig[self.conversor[hash_c]]:
                self.visites[up] += 1
                poblacio[up].add(hash_c)
                if up in self.ups_antigues[self.conversor[hash_c]]:
                    # and self.dates[(self.conversor[cip], up)] < data:
                    self.visites2[up] += 1
                    poblacio2[up].add(hash_c)
        u.printTime("length visites " + str(self.visites['00441']))
        u.printTime("length poblacio " + str(len(poblacio['00441'])))
        u.printTime("length visites " + str(self.visites2['00441']))
        u.printTime("length poblacio " + str(len(poblacio2['00441'])))
        self.poblacio = {}
        self.poblacio2 = {}
        for up in poblacio:
            self.poblacio[up] = len(poblacio[up])
        for up in poblacio2:
            self.poblacio2[up] = len(poblacio2[up])

    def seguiment_ups(self):
        self.ups_antigues = c.defaultdict(list)
        self.dates = c.defaultdict()
        sql = """
            SELECT id_cip_sec, huab_up_codi, huab_data_final
            FROM moviments
            WHERE huab_data_final is not null
            AND huab_data_final <> ''
        """
        for id_cip, up, data in u.getAll(sql, 'import'):
            if up in self.ups_litoral:
                self.ups_antigues[id_cip].append(up)
                self.dates[(id_cip, up)] = data
        u.printTime("length dates " + str(len(self.dates)))

    def upload(self):
        cols = '(up varchar(5), desc_up varchar(100), pob_no_assig int, vis_no_assig int, pob_prev_assig int, vis_prev_assig int)'
        u.createTable('at_no_assig', cols, 'altres', rm=True)
        upload = []
        for up in self.visites:
            if up in self.visites2:
                upload.append((up, self.ups_litoral[up], self.poblacio[up], self.visites[up], self.poblacio2[up], self.visites2[up]))
            else:
                upload.append((up, self.ups_litoral[up], self.poblacio[up], self.visites[up], None, None))
        u.listToTable(upload, 'at_no_assig', 'altres')

if __name__ == "__main__":
    inici = d.datetime.now()
    try:
        Atesa()
        fi = d.datetime.now()
        txt = """Hora inici {},
                hora fi {},
                total de {}""".format(str(inici), str(fi), str(fi-inici))
        mail = t.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "at_no_ass acabat!!"
        mail.text = txt
        mail.send()
        u.printTime('Fi')
    except Exception as e:
        fi = d.datetime.now()
        txt = """Hora inici {},
                hora fi {},
                total de {},
                error {}""".format(str(inici), str(fi), str(fi-inici), str(e))
        mail = t.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "Error at_no_ass :("
        mail.text = txt
        mail.send()
        raise