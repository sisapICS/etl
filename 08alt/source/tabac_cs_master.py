# coding: latin1

"""
.
"""

import collections as c
import datetime as d
import sisaptools as u
import sisapUtils as t
from dateutil.relativedelta import relativedelta
import datetime as d
t.printTime('inici')
dext = d.date(2023, 11, 22)
# sql = "select data_ext from dextraccio"
# for d, in u.Database("p2262", "nodrizas").get_all(sql):
#     dext = d

menys1 = dext - relativedelta(years=1)

sql = """
    select distinct a.id_cip_sec, b.up
    from eqa_tabac a
    inner join assignada_tot b
    on a.id_cip_sec = b.id_cip_sec
    where b.edat > 14
    and DATE '{DEXTD}' between dalta and dbaixa
    and a.last = 1
    and (a.tab  = 1
    or (a.tab = 2
    and a.dlast >= DATE '{menys1}'))
""".format(DEXTD=dext, menys1 = menys1)

dades = []
# la taula validacio tindra el format: (id pacient, up, complidor)
den_iesiap0408 = c.defaultdict()
for id,up in t.getAll(sql, 'nodrizas'):
    den_iesiap0408[id] = up
    # self.dades[("IESIAP0408", up, up)]['DEN'] += 1
print(str(len(den_iesiap0408)))
t.printTime('despres sql 1')
sql = """
    SELECT distinct id_cip_sec
    FROM eqa_variables
    WHERE agrupador = 827
    AND data_var >= DATE '{menys1}'
""".format(menys1=menys1)
num = set()
for id, in t.getAll(sql, 'nodrizas'):
    if id in den_iesiap0408:
        num.add(id)
print(str(len(num)))
t.printTime('despres sql 2')
for id in den_iesiap0408:
    if id in num:
        dades.append((id, den_iesiap0408[id], 1))
    else:
        dades.append((id, den_iesiap0408[id], 0))
print(str(len(dades)))
t.printTime('despres processar')
cols = "(id_cip_sec int(11), up varchar(5), complidor int(1))"
t.createTable('tabac_validacio', cols, 'altres', rm=True)
t.listToTable(dades,'tabac_validacio','altres')
t.printTime('fi')

mail = u.Mail()
mail.to.append("nuriacantero@gencat.cat")
mail.subject = "tabac_cs_master b� :)"
text = ''
mail.text = text
mail.send()

# sql = """
#     SELECT id_cip_sec, up
#     FROM assignada_tot
#     WHERE edat >=15 
#     AND edat < 60
#     AND ates = 1
# """

# pob = c.defaultdict()
# for id, up in t.getAll(sql, 'nodrizas'):
#     pob[id] = up
# t.printTime('pob')

# num_cat0303 = set()
# sql = """select id_cip_sec
#         from eqa_problemes
#         where ps = 85"""
# for id, in t.getAll(sql, "nodrizas"):
#     if id in pob:
#         num_cat0303.add(id)
# t.printTime('problemes')

# sql = """
#     select id_cip_sec, agrupador
#     from eqa_variables
#     where agrupador = 1020 and
#     usar = 1 and
#     data_var between adddate(DATE '{dextd}', interval -2 year) and
#     DATE '{dextd}'
# """.format(dextd=dext)
# for id, agr in t.getAll(sql, "nodrizas"):
#     if id in pob:
#         num_cat0303.add(id)
# t.printTime('variables')

# dades = []
# for id in pob:
#     if id in num_cat0303:
#         dades.append((id, pob[id], 1))
#     else:
#         dades.append((id, pob[id], 0))
# t.printTime('despres sql 2')

# cols = "(id_cip_sec int(11), up varchar(5), complidor int(1))"
# t.createTable('cat0303_validacio', cols, 'altres', rm=True)
# t.listToTable(dades,'cat0303_validacio','altres')
# t.printTime('fi')