import sisapUtils as u
import collections as c
import datetime as d
import sisaptools as t
from dateutil import relativedelta as rd

global dades
dades = c.defaultdict(dict)
DEXTD = u.getOne("select data_ext from dextraccio", "nodrizas")[0]
menys1 = DEXTD - rd.relativedelta(years=1)
centres = {}
sql = """
    SELECT scs_codi, ics_codi
    FROM cat_centres
"""
for up, br in u.getAll(sql, 'nodrizas'):
    centres[br] = up

# codi ambit = 00 vol dir que son NO ICS
sql = """
    SELECT up_codi_up_scs
    FROM centres
    WHERE amb_codi_amb <> '00'
"""
es_ics = set()
for up, in u.getAll(sql, 'pdp'):
    es_ics.add(up)

profs = {
    'TIPPROF1': 'M',
    'TIPPROF4': 'I'
}

def get_query(query):

    for ind, _a, br, analisi, c, tipprof, d, value in u.LvClient().query(query):
        if br in centres and centres[br] in es_ics:
            if tipprof in profs:
                dades[(ind, centres[br], c, profs[tipprof], d)][analisi] = float(value)
            else:
                dades[(ind, centres[br], c, tipprof, d)][analisi] = float(value)

def get_qc():
    # afegir PAMBITOS per fer per UBA amb estructura I/M + BR + UBA
    # s'ha de canviar VISURG a "VISURG2;A2411;AMBITOS###;NUM,DEN;ANUAL;TIPPROF1,TIPPROF4;DIM6SET"
    queries = ["VISURG2;A{}{};AMBITOS###;NUM,DEN;ANUAL;TIPPROF1,TIPPROF4;DIM6SET",
            "VISINFCEN,VISMFCEN;AYR{};AMBITOS###;NOCLI;EDATS5;VISASSIG,VISNOASS;SEXE",
            "AGEND04;AYR{};AMBITOS###;NUM,DEN;VIS9C,VIS9R;TIPPROF1,TIPPROF4;DIM6SET",
            "AGEND02;AYR{};AMBITOS###;DEN;VIS9C,VIS9R;TIPPROF1,TIPPROF4;DIM6SET",
            "AGEND03;AYR{};AMBITOS###;NUM;VIS9C,VIS9R;TIPPROF1,TIPPROF4;DIM6SET"
    ]

    for query in queries:
        if query[:6] == 'VISURG':
            get_query(query.format(str(DEXTD.year)[-2:],str(DEXTD.month).zfill(2)))
        else:
            get_query(query.format(str(DEXTD.year)[-2:]))
        # get_query(query.format('23'))
    u.printTime('queries')
    sql = """
        SELECT up, trim(TO_CHAR(DATA, 'DAY')), sisap_servei_codi, COUNT(1)
        FROM dwsisap.SISAP_MASTER_VISITES smv 
        WHERE DATA >= DATE '{menys1}' AND DATA < DATE '{DEXTD}'
        AND situacio = 'R'
        AND atributs_agenda_cod = '2.4'
        AND (TIPUS_CLASS_DESC = 'Centre') -- or TIPUS_CLASS_DESC LIKE 'Telef%')
        AND SISAP_SERVEI_CODI IN ('MF', 'INF')
        GROUP BY up, trim(TO_CHAR(DATA, 'DAY')), sisap_servei_codi
    """.format(menys1=menys1, DEXTD=DEXTD)
    visites = c.defaultdict(dict)
    profes = {
        'MF': 'M',
        'INF': 'I'
    }
    for up, dia, tipus, val in u.getAll(sql, 'exadata'):
        visites[(up, profes[tipus])][dia] = val
    # print(visites.keys())
    # construim indicadors
    upload = set()
    for (ind, up, vis, prof, dim) in list(dades.keys()):
        # pantalla % VISURG:
        # query = "VISURG;AYR23;AMBITOS###;NUM,DEN;MENSUAL;TIPPROF#99;DIM6SET"
        # ens interessen nomes TIPPROF1 = METGE I TIPPROF4 = INFERMERIA
        # agafem num i den de cadascun i calculem NUM/DEN * 100
        
        if ind == 'VISURG2':
            num = dades[(ind, up, vis, prof, dim)]['NUM'] if 'NUM' in dades[(ind, up, vis, prof, dim)] else 0
            den = dades[(ind, up, vis, prof, dim)]['DEN']
            upload.add((up, prof, ind[:-1], num, den, float(num)/den))
            if (up, prof) in visites:
                dilluns = visites[(up, prof)]['MONDAY'] if 'MONDAY' in visites[(up, prof)] else 0
                dimarts = visites[(up, prof)]['TUESDAY'] if 'TUESDAY' in visites[(up, prof)] else 0
                dimecres = visites[(up, prof)]['WEDNESDAY'] if 'WEDNESDAY' in visites[(up, prof)] else 0
                dijous = visites[(up, prof)]['THURSDAY'] if 'THURSDAY' in visites[(up, prof)] else 0
                divendres = visites[(up, prof)]['FRIDAY'] if 'FRIDAY' in visites[(up, prof)] else 0
                dissabte = visites[(up, prof)]['SATURDAY'] if 'SATURDAY' in visites[(up, prof)] else 0
                diumenge = visites[(up, prof)]['SUNDAY'] if 'SUNDAY' in visites[(up, prof)] else 0
                total = dilluns+dimarts+dimecres+dijous+divendres+dissabte+diumenge
                altres = dimarts+dimecres+dijous
                pc_dilluns = dilluns/float(total)
                pc_divendres = divendres/float(total)
                pc_altres = altres/float(total)
                upload.add((up, prof, 'URG DLL', dilluns, total, pc_dilluns))
                upload.add((up, prof, 'URG DV', divendres, total, pc_divendres))
                upload.add((up, prof, 'URG ALTRES', altres, total, pc_altres))
        
        # pantalla % VISIT NO ASSIG
        # query = "VISINF,VISMF;AYR23;AMBITOS###;NOCLI;EDATS5;VISASSIG,VISNOASS;SEXE"
        # agafem per cada ind (VISINF/VISMF) i calculem VISNOASS/(VISASSIG+VISNOASS) * 100
        elif ind in ('VISINFCEN', 'VISMFCEN'):
            visnoass = dades[(ind, up, vis, 'VISNOASS', dim)]['NOCLI'] if 'NOCLI' in dades[(ind, up, vis, 'VISNOASS', dim)] else 0
            visassig = dades[(ind, up, vis, 'VISASSIG', dim)]['NOCLI'] if 'NOCLI' in dades[(ind, up, vis, 'VISASSIG', dim)] else 0
            if ind == 'VISINFCEN':
                professional = 'I'
            else:
                professional = 'M'
            upload.add((up, professional, 'VISPROF', visnoass, visassig+visnoass, float(visnoass)/(visassig+visnoass)))
        
        # pantalla % NO REALITZ MF
        # NUM/DEN AGEND04 agafar nomes VIS9C,VIS9R -> query = "AGEND04;AYR23;AMBITOS###;NUM,DEN;VIS9C,VIS9R;TIPPROF1;DIM6SET"
        # agafem DEN AGEND02 -> query = "AGEND02;AYR23;AMBITOS###;DEN;VIS9C,VIS9R;TIPPROF1;DIM6SET"
        # agafem NUM AGEND03 -> query = "AGEND03;AYR23;AMBITOS###;NUM;VIS9C,VIS9R;TIPPROF1;DIM6SET"
        # per calcular percentatge hem de fer: 1 - (den_04/(den_02+num_03+num_04)) * 100

        # pantalla % NO REALITZ INF
        # el mateix que la pantalla anterior pero canviant TIPPROF1 per TIPPROF4 (es poden calcular juntes les dues pantalles)
        elif ind in ('AGEND04'):
            den_04_c = dades[(ind, up, 'VIS9C', prof, dim)]['DEN'] if 'DEN' in dades[(ind, up, 'VIS9C', prof, dim)] else 0
            den_04_r = dades[(ind, up, 'VIS9R', prof, dim)]['DEN'] if 'DEN' in dades[(ind, up, 'VIS9R', prof, dim)] else 0
            den_04 = den_04_c + den_04_r
            den_02_c = dades[('AGEND02', up, 'VIS9C', prof, dim)]['DEN'] if 'DEN' in dades[('AGEND02', up, 'VIS9C', prof, dim)] else 0
            den_02_r = dades[('AGEND02', up, 'VIS9R', prof, dim)]['DEN'] if 'DEN' in dades[('AGEND02', up, 'VIS9R', prof, dim)] else 0
            den_02 = den_02_c + den_02_r
            num_03_c = dades[('AGEND03', up, 'VIS9C', prof, dim)]['NUM'] if 'NUM' in dades[('AGEND03', up, 'VIS9C', prof, dim)] else 0
            num_03_r = dades[('AGEND03', up, 'VIS9R', prof, dim)]['NUM'] if 'NUM' in dades[('AGEND03', up, 'VIS9R', prof, dim)] else 0
            num_03 = num_03_c + num_03_r
            num_04_c = dades[(ind, up, 'VIS9C', prof, dim)]['NUM'] if 'NUM' in dades[(ind, up, 'VIS9C', prof, dim)] else 0
            num_04_r = dades[(ind, up, 'VIS9R', prof, dim)]['NUM'] if 'NUM' in dades[(ind, up, 'VIS9R', prof, dim)] else 0
            num_04 = num_04_c + num_04_r
            upload.add((up, prof, ind, den_02+num_03+num_04-den_04, den_02+num_03+num_04, 1-(float(den_04)/(den_02+num_03+num_04))))
    u.printTime('cuinetes')

    cols = "(up varchar(10), prof varchar(1), ind varchar(10), num int, den int, resultat double)"
    u.createTable('eina_oferta', cols, 'altres', rm=True)
    u.listToTable(list(upload), 'eina_oferta', 'altres')
    upload_pdp = []
    for (up, prof, ind, num, den, res) in upload:
        upload_pdp.append((DEXTD.year, DEXTD.month, up, prof, ind, num, den, res))
    return upload_pdp

def get_dades():
    sql = "select a.sym_name, b.description \
       from icsap.klx_master_symbol a \
       inner join icsap.klx_sym_desc b \
             on a.dim_index = b.minor_obj_id and a.sym_index = b.micro_obj_id \
       where dim_index = 2 and lang_code = 'FR'"
    
    conversor = {row[0]: row[1] for row in u.getAll(sql, 'khalix')}
    # laborables = {
    #     1: 4.4,
    #     2: 4.2,
    #     3: 4,
    #     4: 4.2,
    #     5: 4.2,
    #     6: 3.8,
    #     7: 4.6,
    #     8: 4.2,
    #     9: 4,
    #     10: 4.6,
    #     11: 4,
    #     12: 3.8
    # }
    inici = d.date(DEXTD.year,1,1)
    laborables = c.Counter()
    setmanes = c.Counter()
    while inici < d.date(DEXTD.year+1,1,1):
        treballa = u.isWorkingDay(inici)
        setmanes[inici.month] += 1
        if treballa:
            laborables[inici.month] += 1
        inici = inici + rd.relativedelta(days=1)
    for mes, val in laborables.items():
        laborables[mes] = float(val)/(setmanes[mes]/float(7))
    dades = c.defaultdict(dict)
    query = "DEMAND9C,DEMAND9R,DEMAND9T,DEMAND9E,DEMAND9EC;A{}{};PAMBITOS###;NUM;NOCAT;TIPPROF1,TIPPROF4;DIM6SET".format(str(DEXTD.year)[-2:],str(DEXTD.month).zfill(2))
    for ind, a, br, analisi, _c, tipprof, _d, value in u.LvClient().query(query):
        br = conversor[br] if br in conversor else br
        # estructura: BR000 + M/I + uba
        if br[0:5] in centres and centres[br[0:5]] in es_ics:
            up = centres[br[0:5]]
            uba = br[6:]
            if tipprof in profs:
                dades[(int(a[-2:]), up, uba, profs[tipprof])][ind] = float(value)
            else:
                dades[(int(a[-2:]), up, uba, tipprof)][ind] = float(value)
    u.printTime('queries')
    upload = set()
    for (mes, up, uba, prof) in list(dades.keys()):
        d9c = dades[(mes, up, uba, prof)]['DEMAND9C'] if 'DEMAND9C' in dades[(mes, up, uba, prof)] else 0
        d9r = dades[(mes, up, uba, prof)]['DEMAND9R'] if 'DEMAND9R' in dades[(mes, up, uba, prof)] else 0
        d9t = dades[(mes, up, uba, prof)]['DEMAND9T'] if 'DEMAND9T' in dades[(mes, up, uba, prof)] else 0
        d9e = dades[(mes, up, uba, prof)]['DEMAND9E'] if 'DEMAND9E' in dades[(mes, up, uba, prof)] else 0
        d9ec = dades[(mes, up, uba, prof)]['DEMAND9EC'] if 'DEMAND9EC' in dades[(mes, up, uba, prof)] else 0
        dpres = d9c + d9r
        lab = laborables[mes]
        upload.add((DEXTD.year, mes, up, uba, prof, 'DEMAND9C+9R', dpres, lab, dpres/float(lab)))
        upload.add((DEXTD.year, mes, up, uba, prof, 'DEMAND9T', d9t, lab, d9t/float(lab)))
        upload.add((DEXTD.year, mes, up, uba, prof, 'DEMAND9E', d9e, lab, d9e/float(lab)))
        upload.add((DEXTD.year, mes, up, uba, prof, 'DEMAND9EC', d9ec, lab, d9ec/float(lab)))
    # cols = "(up varchar(10), uba varchar(10), periode int, prof varchar(1), indicador varchar(12), num int, den double, res double)"
    # u.createTable('uba_oferta', cols, 'altres', rm=True)
    # u.listToTable(list(upload), 'uba_oferta', 'altres')
    # upload_pdp = []
    # for (up, uba, mes, prof, ind, num, den, res) in upload:
    #     upload_pdp.append(('2024', str(mes), up, uba, prof, ind, num, den, res))
    return upload

if __name__ == "__main__":
    try:
        upload_qc = get_qc()
        upload = get_dades()
        sql = "delete from oferta_qc where dataany = {year} and datames = {month}".format(year=DEXTD.year, month=DEXTD.month)
        u.listToTable(upload_qc, 'OFERTA_QC', 'pdp')
        sql = "delete from oferta where dataany = {year} and datames = {month}".format(year=DEXTD.year, month=DEXTD.month)
        u.listToTable(list(upload), 'OFERTA', 'pdp')
        mail = t.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "SI calcul oferta"
        mail.send()
        u.printTime('penjat')
    except ValueError:
        mail = t.Mail()
        mail.to.append("nuriacantero@gencat.cat")
        mail.subject = "NO calcul oferta"
        mail.send()
        u.printTime('no penjat')