# coding: latin1

import sisapUtils as u
import collections as c


class ViolenciaMasclista():
    def __init__(self):

        self.get_dextraccio()
        self.get_converters()
        self.get_data()
        self.cooking()

    def get_dextraccio(self):
        """
        Obt� la data d'extracci� i calcula dates rellevants:
        - 'data_ext': Data d'extracci� actual.
        - 'data_ext_menys1any': Fa un any des de la data d'extracci�.
        - 'data_ext_menys22mesos': Fa 22 mesos des de la data d'extracci�.
        """

        sql = """
            SELECT
                data_ext,
                date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
                date_add(date_add(data_ext, INTERVAL -22 MONTH), INTERVAL + 1 DAY)
            FROM
                dextraccio
        """
        self.data_ext, self.data_ext_menys1any, self.data_ext_menys22mesos = u.getOne(sql, "nodrizas")

    def get_converters(self):

        # on es visiten les dones a l'assir

        self.assignada = c.defaultdict(lambda: c.defaultdict(set))
        sql = """
                SELECT
                    id_cip_sec,
                    visi_up_assir
                FROM
                    ass_imputacio_up_assir
              """
        for id_cip_sec, up_assir in u.getAll(sql, 'nodrizas'):
            self.assignada[id_cip_sec]["up_assir"].add(up_assir)

        sql = """
                SELECT
                    id_cip_sec,
                    visi_up
                FROM
                    ass_imputacio_up
              """
        for id_cip_sec, up in u.getAll(sql, 'nodrizas'):
            self.assignada[id_cip_sec]["up"].add(up)

    def get_data(self):

        # Embarassades

        self.embarassades = {}
        sql = """
                SELECT
                    id_cip_sec,
                    inici,
                    fi
                FROM
                    ass_embaras
                WHERE
                    inici BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
                    OR fi BETWEEN DATE'{_data_ext_menys1any}' AND DATE'{_data_ext}'
              """.format(_data_ext_menys1any=self.data_ext_menys1any, _data_ext=self.data_ext)
        for id_cip_sec, data_inici, data_fi in u.getAll(sql, 'nodrizas'):
            self.embarassades[id_cip_sec] = (data_inici, data_fi)

        # q�estionari PVS > 0

        self.questionaris = {}
        sql = """
                SELECT
                    id_cip_sec,
                    dat
                FROM
                    ass_variables
                WHERE
                    agrupador IN (894) -- Agrupador per q�estionari PVS
                    AND val_num = 1    -- PVS positiu
                    AND dat BETWEEN DATE'{_data_ext_menys22mesos}' AND DATE'{_data_ext}'
              """.format(_data_ext_menys22mesos=self.data_ext_menys22mesos, _data_ext=self.data_ext)
        for id_cip_sec, data_questionari in u.getAll(sql, 'nodrizas'):
            self.questionaris[id_cip_sec] = data_questionari

        # Viol�ncia masclista

        self.violencia = set()
        sql = """
                SELECT
                    id_cip_sec
                FROM
                    eqa_problemes
                WHERE
                    ps = 992
              """
        for id_cip_sec, in u.getAll(sql, 'nodrizas'):
            self.violencia.add(id_cip_sec)

    def cooking(self):

        # DEN: Nombre dones self.embarassades amb q�estionari PVS positiu (>0)
        # NUM: Nombre de dones self.embarassades amb q�estionari PVS positiu (>0) i diagn�stic de viol�ncia masclista

        den, num = set(), set()
        for id_cip_sec in set(self.embarassades) & set(self.questionaris) & set(self.assignada):
            data_inici, data_fi = self.embarassades[id_cip_sec]
            data_questionari = self.questionaris[id_cip_sec]
            if data_inici <= data_questionari <= data_fi:
                den.add(id_cip_sec)
                if id_cip_sec in self.violencia:
                    num.add(id_cip_sec)

        result, result_assir = c.defaultdict(c.Counter), c.defaultdict(c.Counter)
        llistat, llistat_assir = [], []
        for id_cip_sec in den:
            es_num = 1 if id_cip_sec in num else 0
            for up in self.assignada[id_cip_sec]["up"]:
                result['DEN'][up] += 1
                result['NUM'][up] += es_num
                llistat.append((es_num, id_cip_sec, up))
            for up_assir in self.assignada[id_cip_sec]["up_assir"]:
                result_assir['DEN'][up_assir] += 1
                result_assir['NUM'][up_assir] += es_num
                llistat_assir.append((es_num, id_cip_sec, up_assir))

        upload, upload_assir = [], []
        for analisi in result:
            for up in result[analisi]:
                n = result[analisi][up]
                upload.append((up, analisi, n))
        for analisi in result_assir:
            for up_assir in result_assir[analisi]:
                n = result_assir[analisi][up_assir]
                upload_assir.append((up_assir, analisi, n))

        cols = '(up varchar(5), analisi varchar(5), val int)'
        u.createTable('AGASSDXVM_up', cols, 'altres', rm=True)
        u.listToTable(upload, 'AGASSDXVM_up', 'altres')
        u.createTable('AGASSDXVM', cols, 'altres', rm=True)
        u.listToTable(upload_assir, 'AGASSDXVM', 'altres')

        cols = '(num int, id int, up varchar(5))'
        u.createTable('AGASSDXVM_llistat_up', cols, 'altres', rm=True)
        u.listToTable(llistat, 'AGASSDXVM_llistat_up', 'altres')
        u.createTable('AGASSDXVM_llistat', cols, 'altres', rm=True)
        u.listToTable(llistat_assir, 'AGASSDXVM_llistat', 'altres')


if __name__ == "__main__":
    ViolenciaMasclista()


