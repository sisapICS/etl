import sisapUtils as u

VALIDA_RESIS = False

db = "altres"
uba = "{db}.exp_ecap_alt_uba".format(db=db)


def recover_validaresis():
    y, m = u.getOne(
        "select year(data_ext), date_format(data_ext,'%c') from dextraccio",
        'nodrizas')
    ora_cond = {
        "ALTINDICADORS": """indicador like 'RES%'
                            and dataany = {y}
                            and datames = {m}
                         """.format(y=y, m=m),
        "ALTCATALEG": "pare = 'QCRESV' and dataany = {y}".format(y=y),
        "ALTCATALEGPARE": "pare = 'QCRESV' and dataany = {y}".format(y=y),
        }
    for ora, cond in ora_cond.items():
        query = """
            insert into {ora} select * from {ora}_BACKUP where {cond}
            """.format(ora=ora, cond=cond)
        u.execute(query, 'pdp')


tb_defs = {
    "altIndicadors": {
        "my": uba,
        "dat": True, "datAny": False, "trunc": False,
        "query": "SELECT * FROM {my}",
        },
    "altIndicadorsICS": {
        "my": uba,
        "dat": True, "datAny": False, "trunc": False,
        "query":
            """
            SELECT indicador, sum(numerador)/sum(denominador)
            FROM {my}
            WHERE tipus = 'M'
            GROUP BY indicador
            """,
        },
    "TirDetall": {
        "my": "{db}.exp_ecap_tir_uba_detall".format(db=db),
        "dat": True, "datAny": False,  "trunc": False,
        "query": "SELECT * FROM {my} WHERE uba <> ''",
        },
    "infIndicadors": {
        "my": "{db}.mst_dag".format(db=db),
        "dat": True, "datAny": False, "trunc": False,
        "query":
            """
            SELECT
                up, usuari, 'I', 'DAG001', motiu, sum(num), sum(den)
            FROM {my}
            GROUP BY
                up, usuari, motiu
            """,
        },
    "infindicadorsData": {
        "my": "nodrizas.dextraccio",
        "dat": False, "datAny": False, "trunc": True,
        "query": "SELECT data_ext FROM {my} LIMIT 1",
        },
    "accIndicadors": {
        "dat": True, "datAny": False, "trunc": False,
        "query":
            """
            SELECT * FROM altres.exp_ecap_forats
            union SELECT * FROM altres.exp_ecap_organitzacio
            union SELECT d0, d1, d2, d3, sum(num), sum(den), sum(num)/sum(den)
                FROM altres.exp_ecap_long_uba
                WHERE d3 = 'CONT0002'
                GROUP BY d0, d1, d2, d3
            union
                select
                    scs_codi up,
                    substr(entity, 7, 5) uba,
                    substr(entity, 6, 1) tipus,
                    indicador,
                    sum(IF(analisi = 'NUM', resultat, 0)) as NUM,
                    sum(IF(analisi = 'DEN', resultat, 0)) as DEN,
                    sum(IF(analisi = 'NUM', resultat, 0))/ sum(IF(analisi = 'DEN', resultat, 0)) as res
                from
                    altres.exp_long_cont_uba_A
                inner join nodrizas.cat_centres cc on
                    cc.ics_codi = substr(entity, 1, 5)
                where
                    indicador = 'CONT0002A'
                group by scs_codi, ENTITY
            """,
        },
    "altCataleg": {
        "my": "{db}.exp_ecap_alt_cataleg".format(db=db),
        "dat": False, "datAny": True, "trunc": False,  # datAny=True
        "query": "SELECT * FROM {my}",
        },
    "altCatalegPare": {
        "my": "{db}.exp_ecap_alt_catalegPare".format(db=db),
        "dat": False, "datAny": True, "trunc": False,  # datAny=True
        "query": "SELECT * FROM {my}",
        },
    "altCatalegExclosos": {
        "my": "{db}.exp_ecap_alt_catalegExclosos".format(db=db),
        "dat": False, "datAny": False, "trunc": True,
        "query": "SELECT * FROM {my}",
        },
    "accCataleg": {
        "my": "{db}.exp_ecap_forats_cataleg".format(db=db),  # datAny=True
        "dat": False, "datAny": True, "trunc": False,
        "query": "SELECT * FROM {my}",
        },
    "accCatalegpare": {
        "my": "{db}.exp_ecap_forats_catalegpare".format(db=db),  # datAny=True
        "dat": False, "datAny": True, "trunc": False,
        "query": "SELECT * FROM {my}",
        },
    "Tirdetallcataleg": {
        "my": "{db}.exp_ecap_tir_catalegDetall".format(db=db),
        "dat": False, "datAny": False, "trunc": True,
        "query": "SELECT * FROM {my}",
        },
    "TirdetallcatalegColumna": {
        "my": "{db}.exp_ecap_tir_catalegDetallcolumna".format(db=db),
        "dat": False, "datAny": False, "trunc": True,
        "query": "SELECT * FROM {my}",
        },
}

for tb in tb_defs:
    query = tb_defs[tb]["query"].format(my=tb_defs[tb]["my"] if "my" in tb_defs[tb] else "")  # noqa
    print(tb, query, tb_defs[tb]["dat"], tb_defs[tb]["datAny"], tb_defs[tb]["trunc"])
    u.exportPDP(query=query,
                table=tb,
                dat=tb_defs[tb]["dat"],
                datAny=tb_defs[tb]["datAny"],
                truncate=tb_defs[tb]["trunc"]
                )

if VALIDA_RESIS:
    recover_validaresis()

# SCRATCH

# query = "select * from {}".format(uba)
# table = "altIndicadors"
# u.exportPDP(query=query, table=table, dat=True)

# query = """
#     select
#         indicador, sum(numerador)/sum(denominador)
#     from {}
#     where
#         tipus = 'M'
#     group by
#         indicador""".format(uba)
# table = "altIndicadorsICS"
# u.exportPDP(query=query, table=table, dat=True)

# query = "select * from {} where uba <> ''".format(tirdetall)
# table = "TirDetall"
# u.exportPDP(query=query, table=table, dat=True)

# query = """
#     select
#         up, usuari, 'I', '{}', motiu, sum(num), sum(den)
#     from {}
#     group by
#         up, usuari, motiu
#     """.format(indicadorDAG, dag)
# table = "infIndicadors"
# u.exportPDP(query=query, table=table, dat=True)

# query = "select data_ext from {} limit 1".format(dext)
# table = "infindicadorsData"
# u.exportPDP(query=query, table=table, truncate=True)

# query = """
#         SELECT * FROM altres.exp_ecap_forats
#         union SELECT * FROM altres.exp_ecap_organitzacio
#         union SELECT d0, d1, d2, d3, sum(num), sum(den), sum(num)/sum(den)
#             FROM permanent.exp_ecap_long_uba
#             WHERE d3 = 'CONT0002'
#             GROUP BY d0, d1, d2, d3
#     """
# table = "accIndicadors"
# u.exportPDP(query=query, table=table, dat=True)

# cat = [
#     ["{db}.exp_ecap_alt_cataleg".format(db=db), "altCataleg"],
#     ["{db}.exp_ecap_alt_catalegPare".format(db=db),'altCatalegPare'],
#     ["{db}.exp_ecap_alt_catalegExclosos".format(db=db), 'altCatalegExclosos'],  # noqa
#     ["{db}.exp_ecap_tir_catalegDetall".format(db=db), 'Tirdetallcataleg'],
#     ["{db}.exp_ecap_tir_catalegDetallcolumna".format(db=db), 'TirdetallcatalegColumna'],  # noqa
#     ["{db}.exp_ecap_forats_cataleg".format(db=db), 'accCataleg'],
#     ["{db}.exp_ecap_forats_catalegpare".format(db=db), 'accCatalegpare'],
# ]

# for r in cat:
#     my, ora = r[0], r[1]
#     query = "select * from %s" % my
#     if ora in ["altCataleg", "altCatalegPare", "accCataleg", "accCatalegpare"]:  # noqa
#         u.exportPDP(query=query, table=ora, datAny=True)
#     else:
#         u.exportPDP(query=query, table=ora, truncate=True)
