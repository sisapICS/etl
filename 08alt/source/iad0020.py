from sisapUtils import *
from collections import defaultdict

def denominador():
    upload = []
    print('agafem nens')
    nens = defaultdict(dict)
    # Nens de 0-5 anys que tenen index COD positiu
    sql = "select distinct a.id_cip_sec, uporigen, uba, upinf, ubainf from nodrizas.assignada_tot a, \
            import.odn505 b where a.id_cip_sec = b.id_cip_sec and edat <= 5 and b.cfd_ind_cod > 0"
    for id, up, uba, upinf, ubainf in getAll(sql, "nodrizas"):
        nens[id]['up'] = up
        nens[id]['uba'] = uba
        v = [id, 'IAD0020', up, uba, upinf, ubainf, 0]
        upload.append(v)
    
    print('creem denominador')
    denominador = defaultdict(list)
    ids = set()
    for e in nens.keys():
        if (nens[e]['up'], nens[e]['uba']) not in denominador.keys():
            denominador[(nens[e]['up'], nens[e]['uba'])] = [1,0]
        else:
            denominador[(nens[e]['up'], nens[e]['uba'])][0] += 1
        ids.add(e) 

    return ids, denominador, nens, upload


def numerador(ids, numerador, nens):
    # Nens que hagin tingut ('FSC', 'CD', 'EHO') [els 3 camps, AND]
    print('calculant numerador')
    nens_prob = dict()
    sql = "select id_cip_sec, tb_trac from import.odn511 where TB_TRAC IN ('FSC', 'CD', 'EHO')"
    for id, trac in getAll(sql, "import"):
        if id in ids:
            if id not in nens_prob.keys():
                nens_prob[id] = [0,0,0]
            if trac == 'FSC':
                nens_prob[id][0] = 1
            elif trac == 'CD':
                nens_prob[id][1] = 1
            elif trac == 'EHO':
                nens_prob[id][2] = 1
    
    # Nens que hagin tingut FSC i (CD i/o EHO)
    for e in nens_prob.keys():
        if nens_prob[e][0] == 1 and nens_prob[e][1] + nens_prob[e][2] >= 1:
            try:
                numerador[(nens[e]['up'], nens[e]['uba'])][1] += 1
            except:
                pass
    
    return numerador


if __name__ == '__main__':
    ids, denominador, nens, upload_pacient = denominador() 
    print(len(ids))
    indicador = numerador(ids, denominador, nens)
    print('creem taula') 
    # Creating tables
    db = 'altres'

    # EXP_ECAP_INDICADOR_UBA FORMAT TABLE CREATION (per 08/source/prof_final)
    table = 'exp_ecap_IAD0020_uba'
    createTable(table, "(up varchar(5), uba varchar(5), tipus varchar(5), numerador int, denominador int, resultat double)", db, rm=True)
    upload = []
    for e in indicador.keys():
        res = float(indicador[e][1])/float(indicador[e][0])
        upload.append([e[0], e[1], 'M', indicador[e][1], indicador[e][0], res])
    listToTable(upload, table, db)

    # EXP_ECAP_INDICADOR_PACIENT FORMAT TABLE CREATION (per 08/source/prof_final)
    table = 'exp_ecap_IAD0020_pacient'
    createTable(table, "(id_cip_sec int, indicador varchar(7), up varchar(5), uba varchar(5), upinf varchar(5), ubainf varchar(5), exclos int)", db, rm=True)
    listToTable(upload_pacient, table, db)


    # Creating dataframe
    # df = pd.DataFrame(columns=['UP', 'EDAT', 'DEN', 'NUM_1', 'NUM_2', 'NUM_3'])
    # for e in indicador.keys(): 
    #     df = df.append({'UP':e[0], 'EDAT':e[1], 'DEN':indicador[e][0], 'NUM_1':indicador[e][1], 'NUM_2':indicador[e][2], 'NUM_3':indicador[e][3]}, ignore_index=True)
    # df.to_csv("indicador_odonto_nou.csv")