# -*- coding: utf8 -*-

"""
.
"""

import collections as c

import sisaptools as u


TABLE = "GRUPS_VALIDACIO"
LOGINS = "GRUPS_DIM_USUARI"
DATABASE = ("redics", "pdp")
USERS = ("PREDUPRP", "PREDUMMP", "PREDUFFA")


class Grups(object):
    """."""

    def get_rols(self):
        """."""
        sql = "select codi_sector, rol_usu from cat_pritb799 \
               where rol_rol = 'ECAP_BENES_EMOCIONAL'"
        self.benestar = set([row for row
                             in u.Database("p2262", "import").get_all(sql)])
        sql = "select codi_sector, ide_usuari from cat_pritb992 \
               where ide_categ_prof_c = '11199'"
        self.nutri = set([row for row
                          in u.Database("p2262", "import").get_all(sql)])
        sql = "select codi_sector, ide_usuari from cat_pritb992 \
               where ide_categ_prof_c = '03005'"
        self.fisio = set([row for row
                          in u.Database("p2262", "import").get_all(sql)])
        sql = "select codi_sector, ide_usuari from cat_pritb992 \
               where ide_categ_prof_c = '10778'"
        self.higie = set([row for row
                          in u.Database("p2262", "import").get_all(sql)])

    def get_seguiments(self):
        """."""
        sql = "select codi_sector, sgru_num_grup \
               from grupal5 \
               where sgru_data >= '20210101'"
        self.seguiments = set([row for row
                               in u.Database("p2262", "import").get_all(sql)])

    def get_referents(self):
        """."""
        sql = "select codi_sector, ide_dni, ide_usuari \
               from cat_pritb992 where ide_dni is not null"
        dni_to_usu = {row[:2]: row[2] for row
                      in u.Database("p2262", "import").get_all(sql)}
        base = "select codi_sector, {1}_num_grup, {1}_{2} \
                from cat_prstb{0} where {1}_{2} is not null"
        params = [("287", "pfgr", "dni_proveidor"),
                  ("293", "afgr", "dni")]
        self.referents = c.defaultdict(set)
        for param in params:
            sql = base.format(*param)
            for sector, codi, dni in u.Database("p2262", "import").get_all(sql):  # noqa
                if (sector, dni) in dni_to_usu:
                    usu = dni_to_usu[(sector, dni)]
                    self.referents[(sector, codi)].add(usu)

    def get_grups(self):
        """."""
        sql = "select codi_sector, grup_num, grup_codi_up, grup_tipus, \
                      grup_tipus_activitat, grup_activitat, grup_data_ini, \
                      grup_hora_ini, grup_hora_fi, grup_num_sessions, \
                      grup_assistents, grup_usu_alta, grup_usu_modif, \
                      grup_titol, grup_validada \
               from grupal4, nodrizas.dextraccio \
               where grup_data_ini between '20210101' and data_ext"
        upload = []
        self.usuaris = set()
        for row in u.Database("p2262", "import").get_all(sql):
            (sector, codi, up, tipus, activitat, tematica, data, hini, hfi,
             nsessions, assistents, usu_a, usu_m, titol, _validada) = row
            id = (sector, codi)
            validada = _validada == "S"
            necessita_seguiment = tipus != "C"
            te_seguiment = id in self.seguiments
            compleix = validada and (te_seguiment or not necessita_seguiment)
            n_sessions = nsessions if nsessions else 1
            hores = 0
            if hini and hfi:
                hores = n_sessions * ((hfi - hini) / 3600.0)
            usuaris = [usu_a, usu_m] + list(self.referents[id])
            es_benestar = any([(sector, usu) in self.benestar for usu in usuaris])  # noqa
            es_nutri = any([(sector, usu) in self.nutri for usu in usuaris])
            es_fisio = any([(sector, usu) in self.fisio for usu in usuaris])
            es_higie = any([(sector, usu) in self.higie for usu in usuaris])
            this = (int("".join(map(str, id))), sector, codi, up, usu_a, usu_m,
                    tipus, activitat, tematica, titol, data, 1 * validada,
                    1 * necessita_seguiment, 1 * te_seguiment, 1 * compleix,
                    n_sessions, hores, assistents, 1 * es_benestar,
                    1 * es_nutri, 1 * es_fisio,  1 * es_higie)
            upload.append(this)
            for usu in usuaris:
                self.usuaris.add((sector, usu))
        cols = ("id int", "codi_sector varchar2(4)", "codi_grup int", "up varchar2(5)",  # noqa
                "usuari_alta varchar2(16)", "usuari_modificacio varchar2(16)",
                "tipus varchar2(2)", "activitat varchar2(2)",
                "tematica varchar2(2)", "titol varchar2(255)", "data date",
                "validada int", "necessita_seguiment int", "te_seguiment int",
                "compleix_criteris int", "sessions int", "hores number(25, 3)",
                "assistents int", "es_benestar int", "es_nutricionista int",
                "es_fisioterapeuta int", "es_higienista int")
        with u.Database(*DATABASE) as conn:
            conn.create_table(TABLE, cols, remove=True)
            conn.list_to_table(upload, TABLE)
            conn.set_grants("select", TABLE, USERS)
            conn.set_statistics(TABLE)

    def get_usuaris(self):
        """."""
        upload = []
        sql = "select codi_sector, ide_usuari, ide_dni, ide_nom_usuari, \
                      ide_categ_prof_c, ide_categ_prof \
               from cat_pritb992"
        for sector, usuari, dni, nom, cat_c, cat_d in u.Database("p2262", "import").get_all(sql):  # noqa
            id = (sector, usuari)
            if id in self.usuaris:
                es_benestar = id in self.benestar
                es_nutricionista = id in self.nutri
                es_fisioterapeuta = id in self.fisio
                es_higienista = id in self.higie
                this = (sector, usuari, dni, nom, cat_c, cat_d,
                        1 * es_benestar, 1 * es_nutricionista,
                        1 * es_fisioterapeuta, 1 * es_higienista)
                upload.append(this)
        cols = ("codi_sector varchar2(4)", "usuari varchar2(16)",
                "dni varchar2(16)", "nom varchar2(255)",
                "categoria_codi varchar2(5)", "categoria varchar2(255)",
                "es_benestar int", "es_nutricionista int",
                "es_fisioterapeuta int", "es_higienista int")
        with u.Database(*DATABASE) as conn:
            conn.create_table(LOGINS, cols, remove=True)
            conn.list_to_table(upload, LOGINS)
            conn.set_grants("select", LOGINS, USERS)
            conn.set_statistics(LOGINS)

    def get_dim(self):
        """."""
        dims = [("activitat", "GRU_TIPUS"), ("TEMATICA", "GRU_ACTIVITAT")]
        for dim, var in dims:
            sql = "select distinct rv_low_value, rv_meaning \
                   from cat_pritb000 \
                   where rv_column = '{}' and rv_type <> 'OLD'".format(var)
            dades = list(u.Database("p2262", "import").get_all(sql))
            cols = ("codi varchar(16)", "literal varchar(255)")
            taula = "GRUPS_DIM_" + dim
            with u.Database(*DATABASE) as conn:
                conn.create_table(taula, cols, remove=True)
                conn.list_to_table(dades, taula)
                conn.set_grants("select", taula, USERS)
                conn.set_statistics(taula)


if __name__ == '__main__':
    grups = Grups()
    grups.get_rols()
    grups.get_seguiments()
    grups.get_referents()
    grups.get_grups()
    grups.get_usuaris()
    grups.get_dim()
