#  coding: latin1

import sisapUtils as u
import collections as c
import datetime
from collections import OrderedDict


sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY)
        FROM
            dextraccio        
      """
data_ext, data_ext_menys1any = u.getOne(sql, "nodrizas")

print("La data_ext es {}".format(data_ext))

if data_ext.month == 12 and data_ext.day == 31:

    # Poblaci�n diana infantojuvenil
    sql = """
            SELECT
                id_cip_sec,
                edat,
                data_naix
            FROM
                assignada_tot
          """
    poblacio = {id_cip_sec: {"edat": edat, "data_naix": data_naix} for id_cip_sec, edat, data_naix in u.getAll(sql, "nodrizas")}
    poblacio_diana_infantojuvenil = {id_cip_sec for id_cip_sec, edat, data_naix in u.getAll(sql, "nodrizas") if edat <= 14}
    poblacio_diana_adulta = {id_cip_sec for id_cip_sec, edat, data_naix in u.getAll(sql, "nodrizas") if edat > 14}
    print("La poblacio_diana_infantojuvenil es {}".format(len(poblacio_diana_infantojuvenil)))

    # Poblaci�n diana embarassada
    sql = """
            SELECT
                id_cip_sec,
                num
            FROM
                odn.mst_indicadors_pacient
            WHERE
                indicador = 'EQA9108'
                AND institucionalitzat = 0
                AND ates = 1
          """
    poblacio_diana_embarassada = {id_cip_sec for id_cip_sec, num in u.getAll(sql, "odn")}
    poblacio_diana_embarassada_atesa = {id_cip_sec for id_cip_sec, num in u.getAll(sql, "odn") if num == 1}
    print("La poblacio_diana_embarassada es {}".format(len(poblacio_diana_embarassada)))
    print("La poblacio_diana_embarassada_atesa es {}".format(len(poblacio_diana_embarassada_atesa)))

    # Fechas embarazos
    sql = """
            SELECT
                id_cip_sec,
                inici,
                fi
            FROM
                nodrizas.ass_embaras
          """
    dates_embaras = {id_cip_sec: (inici, fi) for id_cip_sec, inici, fi in u.getAll(sql, "odn") if id_cip_sec in poblacio_diana_embarassada}

    # Poblaci�n diana discapacitada

    sql = """
            SELECT
                id_cip_sec,
                dde
            FROM
                eqa_problemes
            WHERE
                ps = 301
          """
    poblacio_diana_discapacitada = {id_cip_sec: data_ps for id_cip_sec, data_ps in u.getAll(sql, "nodrizas") if id_cip_sec in poblacio}
    print("La poblacio_diana_discapacitada es {}".format(len(poblacio_diana_discapacitada)))

    def sub_get_problemes(subtable):

        sub_problemes = set()

        sql = """
            SELECT
                id_cip_sec,
                pr_dde
            FROM
                {}
            WHERE
                pr_cod_ps in (select criteri_codi from nodrizas.eqa_criteris where agrupador = 1049)
                and (pr_dba >= '{}' or pr_dba = '0000-00-00')
                and pr_dde <= '{}'
          """.format(subtable, data_ext_menys1any, data_ext)

        for id_cip_sec, data_ps in u.getAll(sql, "import"):
            if id_cip_sec in poblacio:
                sub_problemes.add((id_cip_sec, data_ps))

        return sub_problemes

    # Poblaci�n diana oncol�gica

    poblacio_diana_oncologica = c.defaultdict(set)

    for sub_problemes in u.multiprocess(sub_get_problemes, u.getSubTables("problemes"), 4):
        for id_cip_sec, data_ps in sub_problemes:
            poblacio_diana_oncologica.setdefault(id_cip_sec, data_ps)
            poblacio_diana_oncologica[id_cip_sec] = min(poblacio_diana_oncologica[id_cip_sec], data_ps)

    print("La poblacio_diana_oncologica es {}".format(len(poblacio_diana_oncologica)))

    # Conversor poblaci�n diana hash_redics - id_cip_sec
    sql = """
            SELECT
                hash_d,
                id_cip_sec            
            FROM
                eqa_u11
          """
    conv_hashredics2idcipsec = {hash_redics: id_cip_sec for hash_redics, id_cip_sec in u.getAll(sql, "nodrizas") if id_cip_sec in poblacio}
    print("conv_hashredics2idcipsec")

    # Conversor poblaci�n diana hash_covid - hash_redics

    sql = """
            SELECT
                hash_covid,
                hash_redics            
            FROM
                dwsisap.pdptb101_relacio
          """
    conv_hashcovid2hashredics = {hash_covid: hash_redics for hash_covid, hash_redics in u.getAll(sql, "exadata") if hash_redics in conv_hashredics2idcipsec}
    print("conv_hashcovid2hashredics")
       
    # Aplicaci�n de sustancias remineralizantes, antis�pticas y/o desensibilizantes

    poblacio_fluoritzacions, poblacio_tartrectomies, poblacio_actuacions_dates = c.defaultdict(list), c.defaultdict(list), c.defaultdict(set)

    sql = """
            SELECT
                id_cip_sec,
                tb_trac,
                tb_data
            FROM
                import.odn511
            WHERE
                EXTRACT(YEAR FROM tb_data) = EXTRACT(YEAR FROM DATE'{}')
          """.format(data_ext)
    for id_cip_sec, tractament, data in u.getAll(sql, "nodrizas"):
        if id_cip_sec in poblacio:
            if tractament in ('FC', 'FSC'):
                poblacio_fluoritzacions[id_cip_sec].append(data)
            elif tractament == "TT":
                poblacio_tartrectomies[id_cip_sec].append(data)
            poblacio_actuacions_dates[id_cip_sec].add(data)

    print("La poblacio_fluoritzacions es {}".format(len(poblacio_fluoritzacions)))
    print("La poblacio_tartrectomies es {}".format(len(poblacio_tartrectomies)))

    # Aplicaci�n de fosas y fisuras

    poblacio_segellats, poblacio_obturacions, poblacio_exodoncies, poblacio_tractament_canins_traumatisme = c.defaultdict(list), c.defaultdict(list), c.defaultdict(list), c.defaultdict(list)

    sql = """
            SELECT
                id_cip_sec,
                dtd_tra,
                dtd_pat,
                dtd_data,
                dtd_cod_p
            FROM
                import.odn508
            WHERE
                dtd_et = 'E'
                AND EXTRACT(YEAR FROM dtd_data) = EXTRACT(YEAR FROM DATE'{}')
          """.format(data_ext)
    for id_cip_sec, tractament, patologia, data, peca in u.getAll(sql, "nodrizas"):
        if id_cip_sec in poblacio:
            if tractament == "SEG":
                poblacio_segellats[id_cip_sec].append(data)
            elif tractament == "OBT":
                poblacio_obturacions[id_cip_sec].append(data)
            elif tractament in ('EX', 'EXO'):
                poblacio_exodoncies[id_cip_sec].append(data)
            if patologia == "TRU" and peca in (11, 12, 13, 21, 22, 23, 31, 32, 33, 41, 42, 43):
                poblacio_tractament_canins_traumatisme[id_cip_sec].append(data)
            poblacio_actuacions_dates[id_cip_sec].add(data)

    print("La poblacio_segellats es {}".format(len(poblacio_segellats)))
    print("La poblacio_obturacions es {}".format(len(poblacio_obturacions)))
    print("La poblacio_exodoncies es {}".format(len(poblacio_exodoncies)))
    print("La poblacio_tractament_canins_traumatisme es {}".format(len(poblacio_tractament_canins_traumatisme)))

    # Poblaci�n atendida por odontolog�a

    poblacio_visitada_odn, visites_odn_dades, revisions_odn_dades = set(), c.defaultdict(list), c.defaultdict(list)

    sql = """
            SELECT
                pacient,
                edat,
                data
            FROM
                dwsisap.SISAP_MASTER_VISITES
            WHERE
                situacio = 'R'
                AND SISAP_CATPROF_CLASS = 'ODO'
                AND EXTRACT(YEAR FROM DATA) = EXTRACT(YEAR FROM DATE'{}')
          """.format(data_ext)
    for hash_covid, edat_visita, data_visita in u.getAll(sql, "exadata"):
        data_visita = data_visita.date()
        if hash_covid in conv_hashcovid2hashredics:
            hash_redics = conv_hashcovid2hashredics[hash_covid]
            if hash_redics in conv_hashredics2idcipsec:
                id_cip_sec = conv_hashredics2idcipsec[hash_redics]
                if id_cip_sec in poblacio:
                    poblacio_visitada_odn.add(id_cip_sec)
                    visites_odn_dades[id_cip_sec].append((data_visita, edat_visita))
                    if data_visita not in poblacio_actuacions_dates[id_cip_sec]:
                        revisions_odn_dades[id_cip_sec].append((data_visita, edat_visita))

    print("La poblacio_visitada_odn es {}".format(len(poblacio_visitada_odn)))

    # Revisions escolars

    revisions_escolars_dades = c.defaultdict(list)

    sql = """
            SELECT
                pacient,
                edat,
                data
            FROM
                dwsisap.SISAP_MASTER_VISITES
            WHERE
                situacio = 'R'
                AND (modul IN ('RODN', 'I-COL')
		             OR ETIQUETA IN ('RODN', 'RODON'))
                AND (edat BETWEEN 3 AND 5 OR edat BETWEEN 6 AND 7 OR edat BETWEEN 10 AND 12)
                AND EXTRACT(YEAR FROM DATA) = EXTRACT(YEAR FROM DATE'{}')
          """.format(data_ext)
    for hash_covid, edat_visita, data_visita in u.getAll(sql, "exadata"):
        data_visita = data_visita.date()
        if hash_covid in conv_hashcovid2hashredics:
            hash_redics = conv_hashcovid2hashredics[hash_covid]
            if hash_redics in conv_hashredics2idcipsec:
                id_cip_sec = conv_hashredics2idcipsec[hash_redics]
                if id_cip_sec in poblacio:
                    revisions_escolars_dades[id_cip_sec].append((data_visita, edat_visita))

    print("La poblacio_visitada_odn es {}".format(len(poblacio_visitada_odn)))


    # Exportaci� dades

    tb_name = "dades_ministeri_odontologia_{}".format(data_ext.year)
    db_name = "altres"
    columnes = "(id_cip_sec INT, edat INT, diana_infantojuvenil INT, diana_embaras INT, data_embaras_inici INT, data_embaras_fi INT, diana_embaras_atesa INT, diana_oncologic INT, data_oncologic_inici INT, diana_discapacitat INT, data_discapacitat_inici INT, visitat_odn INT, visites_odn INT, revisions_odn INT, consultes_odn_entre_0_5_anys INT, consultes_odn_entre_6_14_anys INT, consultes_odn_entre_0_14_anys INT, revisions_odn_entre_0_5_anys INT, revisions_odn_entre_6_14_anys INT, revisions_odn_entre_0_14_anys INT, visites_odn_embaras INT, revisions_odn_embaras INT, visites_odn_oncologic INT, revisions_odn_oncologic INT, revisions_escolars_p4 INT, revisions_escolars_primer_primaria INT, revisions_escolars_sise_primaria INT, fluoracio INT, fluoracio_entre_0_5_anys INT, fluoracio_entre_6_14_anys INT, fluoracio_entre_0_14_anys INT, fluoracio_embaras INT, fluoracio_oncologic INT, segellat INT, segellat_entre_0_5_anys INT, segellat_entre_6_14_anys INT, segellat_entre_0_14_anys INT, segellat_embaras INT, segellat_oncologic INT, obturacio INT, obturacio_entre_0_5_anys INT, obturacio_entre_6_14_anys INT, obturacio_entre_0_14_anys INT, obturacio_embaras INT, obturacio_oncologic INT, exodoncia INT, exodoncia_entre_0_5_anys INT, exodoncia_entre_6_14_anys INT, exodoncia_entre_0_14_anys INT, exodoncia_embaras INT, exodoncia_oncologic INT, tartrectomia INT, tartrectomia_entre_0_5_anys INT, tartrectomia_entre_6_14_anys INT, tartrectomia_entre_0_14_anys INT, tartrectomia_embaras INT, tartrectomia_oncologic INT, tract_incisiu_canins_traumatisme INT, tract_incisiu_canins_traumatisme_entre_0_5_anys INT, tract_incisiu_canins_traumatisme_entre_6_14_anys INT, tract_incisiu_canins_traumatisme_entre_0_14_anys INT, tract_incisiu_canins_traumatisme_embaras INT, tract_incisiu_canins_traumatisme_oncologic INT)"

    dades = list()

    for id_cip_sec in poblacio:
        edat = poblacio[id_cip_sec]["edat"]
        diana_infantojuvenil = 1 if id_cip_sec in poblacio_diana_infantojuvenil else 0
        diana_embaras = 1 if id_cip_sec in poblacio_diana_embarassada else 0
        data_embaras_inici = dates_embaras[id_cip_sec][0] if id_cip_sec in dates_embaras else None
        data_embaras_fi = dates_embaras[id_cip_sec][1] if id_cip_sec in dates_embaras else None
        diana_embaras_atesa = 1 if id_cip_sec in poblacio_diana_embarassada_atesa else 0
        diana_oncologic = 1 if id_cip_sec in poblacio_diana_oncologica else 0
        data_oncologic_inici = poblacio_diana_oncologica[id_cip_sec] if id_cip_sec in poblacio_diana_oncologica else None
        diana_discapacitat = 1 if id_cip_sec in poblacio_diana_discapacitada else 0
        data_discapacitat_inici = poblacio_diana_discapacitada[id_cip_sec] if id_cip_sec in poblacio_diana_discapacitada else None
        try: visitat_odn = 1 if id_cip_sec in poblacio_visitada_odn else 0
        except: visitat_odn = None
        try: visites_odn = sum(15 <= edat_visita for _, edat_visita in visites_odn_dades[id_cip_sec]) if visitat_odn else 0
        except: visites_odn = None
        try: revisions_odn = sum(15 <= edat_visita for _, edat_visita in revisions_odn_dades[id_cip_sec]) if visitat_odn else 0
        except: revisions_odn = None
        try: consultes_odn_entre_0_5_anys = sum(edat_visita <= 5 for _, edat_visita in visites_odn_dades[id_cip_sec]) if visitat_odn else 0
        except: consultes_odn_entre_0_5_anys = None
        try: consultes_odn_entre_6_14_anys = sum(6 <= edat_visita <= 14 for _, edat_visita in visites_odn_dades[id_cip_sec]) if visitat_odn else 0
        except: consultes_odn_entre_6_14_anys = None
        try: consultes_odn_entre_0_14_anys = sum(edat_visita <= 14 for _, edat_visita in visites_odn_dades[id_cip_sec]) if visitat_odn else 0
        except: consultes_odn_entre_0_14_anys = None
        try: revisions_odn_entre_0_5_anys = sum(edat_visita <= 5 for _, edat_visita in revisions_odn_dades[id_cip_sec]) if visitat_odn else 0
        except: revisions_odn_entre_0_5_anys = None
        try: revisions_odn_entre_6_14_anys = sum(6 <= edat_visita <= 14 for _, edat_visita in revisions_odn_dades[id_cip_sec]) if visitat_odn else 0
        except: revisions_odn_entre_6_14_anys = None
        try: revisions_odn_entre_0_14_anys = sum(edat_visita <= 14 for _, edat_visita in revisions_odn_dades[id_cip_sec]) if visitat_odn else 0
        except: revisions_odn_entre_0_14_anys = None
        try: visites_odn_embaras = sum(data_embaras_inici <= data_visita <= data_embaras_fi for data_visita, _ in visites_odn_dades[id_cip_sec]) if visitat_odn and diana_embaras else 0
        except: visites_odn_embaras = None
        try: revisions_odn_embaras = sum(data_embaras_inici <= data_visita <= data_embaras_fi for data_visita, _ in revisions_odn_dades[id_cip_sec]) if visitat_odn and diana_embaras else 0
        except: revisions_odn_embaras = None
        try: visites_odn_oncologic = sum(poblacio_diana_oncologica[id_cip_sec] <= data_visita for data_visita, _ in visites_odn_dades[id_cip_sec]) if visitat_odn and diana_oncologic else 0
        except: visites_odn_oncologic = None
        try: revisions_odn_oncologic = sum(poblacio_diana_oncologica[id_cip_sec] <= data_visita  for data_visita, _ in revisions_odn_dades[id_cip_sec]) if visitat_odn and diana_oncologic else 0
        except: revisions_odn_oncologic = None
        try: revisions_escolars_p4 = sum(3 <= edat_visita <= 5 for _, edat_visita in revisions_escolars_dades[id_cip_sec]) if id_cip_sec in revisions_escolars_dades  else 0
        except: revisions_escolars_p4 = None
        try: revisions_escolars_primer_primaria = sum(6 <= edat_visita <= 7 for _, edat_visita in revisions_escolars_dades[id_cip_sec]) if id_cip_sec in revisions_escolars_dades else 0
        except: revisions_escolars_primer_primaria = None
        try: revisions_escolars_sise_primaria = sum(10 <= edat_visita <= 12 for _, edat_visita in revisions_escolars_dades[id_cip_sec]) if id_cip_sec in revisions_escolars_dades else 0
        except: revisions_escolars_sise_primaria = None
        try: fluoracio = len(poblacio_fluoritzacions[id_cip_sec]) if id_cip_sec in poblacio_fluoritzacions else 0
        except: fluoracio = None
        try: fluoracio_entre_0_5_anys = sum(u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 5 for data_visita in poblacio_fluoritzacions[id_cip_sec]) if fluoracio else 0
        except: fluoracio_entre_0_5_anys = None
        try: fluoracio_entre_6_14_anys = sum(6 <= u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 14 for data_visita in poblacio_fluoritzacions[id_cip_sec]) if fluoracio else 0
        except: fluoracio_entre_6_14_anys = None
        try: fluoracio_entre_0_14_anys = sum(u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 14 for data_visita in poblacio_fluoritzacions[id_cip_sec]) if fluoracio else 0
        except: fluoracio_entre_0_14_anys = None
        try: fluoracio_embaras = sum(data_embaras_inici <= data_visita <= data_embaras_fi for data_visita in poblacio_fluoritzacions[id_cip_sec]) if fluoracio and diana_embaras else 0
        except: fluoracio_embaras = None
        try: fluoracio_oncologic = sum(poblacio_diana_oncologica[id_cip_sec] <= data_visita for data_visita in poblacio_fluoritzacions[id_cip_sec]) if fluoracio and diana_oncologic else 0
        except: fluoracio_oncologic = None
        try: segellat = len(poblacio_segellats[id_cip_sec]) if id_cip_sec in poblacio_segellats else 0
        except: segellat = None
        try: segellat_entre_0_5_anys = sum(u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 5 for data_visita in poblacio_segellats[id_cip_sec]) if segellat else 0
        except: segellat_entre_0_5_anys = None
        try: segellat_entre_6_14_anys = sum(6 <= u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 14 for data_visita in poblacio_segellats[id_cip_sec]) if segellat else 0
        except: segellat_entre_6_14_anys = None
        try: segellat_entre_0_14_anys = sum(u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 14 for data_visita in poblacio_segellats[id_cip_sec]) if segellat else 0
        except: segellat_entre_0_14_anys = None
        try: segellat_embaras = sum(data_embaras_inici <= data_visita <= data_embaras_fi for data_visita in poblacio_segellats[id_cip_sec]) if segellat and diana_embaras else 0
        except: segellat_embaras = None
        try: segellat_oncologic = sum(poblacio_diana_oncologica[id_cip_sec] <= data_visita for data_visita in poblacio_segellats[id_cip_sec]) if segellat and diana_oncologic else 0
        except: segellat_oncologic = None
        try: obturacio = len(poblacio_obturacions[id_cip_sec]) if id_cip_sec in poblacio_obturacions else 0
        except: obturacio = None
        try: obturacio_entre_0_5_anys = sum(u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 5 for data_visita in poblacio_obturacions[id_cip_sec]) if obturacio else 0
        except: obturacio_entre_0_5_anys = None
        try: obturacio_entre_6_14_anys = sum(6 <= u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 14 for data_visita in poblacio_obturacions[id_cip_sec]) if obturacio else 0
        except: obturacio_entre_6_14_anys = None
        try: obturacio_entre_0_14_anys = sum(u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 14 for data_visita in poblacio_obturacions[id_cip_sec]) if obturacio else 0
        except: obturacio_entre_0_14_anys = None
        try: obturacio_embaras = sum(data_embaras_inici <= data_visita <= data_embaras_fi for data_visita in poblacio_obturacions[id_cip_sec]) if obturacio and diana_embaras else 0
        except: obturacio_embaras = None
        try: obturacio_oncologic = sum(poblacio_diana_oncologica[id_cip_sec] <= data_visita for data_visita in poblacio_obturacions[id_cip_sec]) if obturacio and diana_oncologic else 0
        except: obturacio_oncologic = None
        try: exodoncia = len(poblacio_exodoncies[id_cip_sec]) if id_cip_sec in poblacio_exodoncies else 0
        except: exodoncia = None
        try: exodoncia_entre_0_5_anys = sum(u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 5 for data_visita in poblacio_exodoncies[id_cip_sec]) if exodoncia else 0
        except: exodoncia_entre_0_5_anys = None
        try: exodoncia_entre_6_14_anys = sum(6 <= u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 14 for data_visita in poblacio_exodoncies[id_cip_sec]) if exodoncia else 0
        except: exodoncia_entre_6_14_anys = None
        try: exodoncia_entre_0_14_anys = sum(u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 14 for data_visita in poblacio_exodoncies[id_cip_sec]) if exodoncia else 0
        except: exodoncia_entre_0_14_anys = None
        try: exodoncia_embaras = sum(data_embaras_inici <= data_visita<= data_embaras_fi for data_visita in poblacio_exodoncies[id_cip_sec]) if exodoncia and diana_embaras else 0
        except: exodoncia_embaras = None
        try: exodoncia_oncologic = sum(poblacio_diana_oncologica[id_cip_sec] <= data_visita for data_visita in poblacio_exodoncies[id_cip_sec]) if exodoncia and diana_oncologic else 0
        except: exodoncia_oncologic = None
        try: tartrectomia = len(poblacio_tartrectomies[id_cip_sec]) if id_cip_sec in poblacio_tartrectomies else 0
        except: tartrectomia = None
        try: tartrectomia_entre_0_5_anys = sum(u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 5 for data_visita in poblacio_tartrectomies[id_cip_sec]) if tartrectomia else 0
        except: tartrectomia_entre_0_5_anys = None
        try: tartrectomia_entre_6_14_anys = sum(6 <= u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 14 for data_visita in poblacio_tartrectomies[id_cip_sec]) if tartrectomia else 0
        except: tartrectomia_entre_6_14_anys = None
        try: tartrectomia_entre_0_14_anys = sum(u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 14 for data_visita in poblacio_tartrectomies[id_cip_sec]) if tartrectomia else 0
        except: tartrectomia_entre_0_14_anys = None
        try: tartrectomia_embaras = sum(data_embaras_inici <= data_visita <= data_embaras_fi for data_visita in poblacio_tartrectomies[id_cip_sec]) if tartrectomia and diana_embaras else 0
        except: tartrectomia_embaras = None
        try: tartrectomia_oncologic = sum(poblacio_diana_oncologica[id_cip_sec] <= data_visita for data_visita in poblacio_tartrectomies[id_cip_sec]) if tartrectomia and diana_oncologic else 0
        except: tartrectomia_oncologic = None
        try: tract_incisiu_canins_traumatisme = len(poblacio_tractament_canins_traumatisme[id_cip_sec]) if id_cip_sec in poblacio_tractament_canins_traumatisme else 0
        except: tract_incisiu_canins_traumatisme = None
        try: tract_incisiu_canins_traumatisme_entre_0_5_anys = sum(u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 5 for data_visita in poblacio_tractament_canins_traumatisme[id_cip_sec]) if tract_incisiu_canins_traumatisme else 0
        except: tract_incisiu_canins_traumatisme_entre_0_5_anys = None
        try: tract_incisiu_canins_traumatisme_entre_6_14_anys = sum(6 <= u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 14 for data_visita in poblacio_tractament_canins_traumatisme[id_cip_sec]) if tract_incisiu_canins_traumatisme else 0
        except: tract_incisiu_canins_traumatisme_entre_6_14_anys = None
        try: tract_incisiu_canins_traumatisme_entre_0_14_anys = sum(u.yearsBetween(poblacio[id_cip_sec]["data_naix"], data_visita) <= 14 for data_visita in poblacio_tractament_canins_traumatisme[id_cip_sec]) if tract_incisiu_canins_traumatisme else 0
        except: tract_incisiu_canins_traumatisme_entre_0_14_anys = None
        try: tract_incisiu_canins_traumatisme_embaras = sum(data_embaras_inici <= data_visita <= data_embaras_fi for data_visita in poblacio_tractament_canins_traumatisme[id_cip_sec]) if tract_incisiu_canins_traumatisme and diana_embaras else 0
        except: tract_incisiu_canins_traumatisme_embaras = None
        try: tract_incisiu_canins_traumatisme_oncologic = sum(poblacio_diana_oncologica[id_cip_sec] <= data_visita for data_visita in poblacio_tractament_canins_traumatisme[id_cip_sec]) if tract_incisiu_canins_traumatisme and diana_oncologic else 0
        except: tract_incisiu_canins_traumatisme_oncologic = None

        registre = [id_cip_sec, edat, diana_infantojuvenil, diana_embaras, data_embaras_inici, data_embaras_fi, diana_embaras_atesa, diana_oncologic, data_oncologic_inici, diana_discapacitat, data_discapacitat_inici, visitat_odn, visites_odn, revisions_odn, consultes_odn_entre_0_5_anys, consultes_odn_entre_6_14_anys, consultes_odn_entre_0_14_anys, revisions_odn_entre_0_5_anys, revisions_odn_entre_6_14_anys, revisions_odn_entre_0_14_anys, visites_odn_embaras, revisions_odn_embaras, visites_odn_oncologic, revisions_odn_oncologic, revisions_escolars_p4, revisions_escolars_primer_primaria, revisions_escolars_sise_primaria, fluoracio, fluoracio_entre_0_5_anys, fluoracio_entre_6_14_anys, fluoracio_entre_0_14_anys, fluoracio_embaras, fluoracio_oncologic, segellat, segellat_entre_0_5_anys, segellat_entre_6_14_anys, segellat_entre_0_14_anys, segellat_embaras, segellat_oncologic, obturacio, obturacio_entre_0_5_anys, obturacio_entre_6_14_anys, obturacio_entre_0_14_anys, obturacio_embaras, obturacio_oncologic, exodoncia, exodoncia_entre_0_5_anys, exodoncia_entre_6_14_anys, exodoncia_entre_0_14_anys, exodoncia_embaras, exodoncia_oncologic, tartrectomia, tartrectomia_entre_0_5_anys, tartrectomia_entre_6_14_anys, tartrectomia_entre_0_14_anys, tartrectomia_embaras, tartrectomia_oncologic, tract_incisiu_canins_traumatisme, tract_incisiu_canins_traumatisme_entre_0_5_anys, tract_incisiu_canins_traumatisme_entre_6_14_anys, tract_incisiu_canins_traumatisme_entre_0_14_anys, tract_incisiu_canins_traumatisme_embaras, tract_incisiu_canins_traumatisme_oncologic]

        dades.append(registre)

    u.createTable(tb_name, columnes, db_name, rm=1)
    u.listToTable(dades, tb_name, db_name)

    print("Taula creada")

    # Generem dades email

    A1, = u.getOne("select sum(diana_infantojuvenil) from {}".format(tb_name), db_name)
    A2, = u.getOne("select sum(visitat_odn) from {} where diana_infantojuvenil = 1".format(tb_name), db_name)
    A3 = 100 * (A2/A1)
    A4, = u.getOne("select sum(consultes_odn_entre_0_5_anys) from {}".format(tb_name), db_name)
    A5, = u.getOne("select sum(consultes_odn_entre_6_14_anys) from {}".format(tb_name), db_name)
    A6, = u.getOne("select sum(consultes_odn_entre_0_14_anys) from {}".format(tb_name), db_name)
    A7, = u.getOne("select sum(revisions_odn_entre_0_5_anys) from {}".format(tb_name), db_name)
    A8, = u.getOne("select sum(revisions_odn_entre_6_14_anys) from {}".format(tb_name), db_name)
    A9, = u.getOne("select sum(revisions_odn_entre_0_14_anys) from {}".format(tb_name), db_name)
    A10, = u.getOne("select sum(fluoracio_entre_0_5_anys) from {}".format(tb_name), db_name)
    A11, = u.getOne("select sum(fluoracio_entre_6_14_anys) from {}".format(tb_name), db_name)
    A12, = u.getOne("select sum(fluoracio_entre_0_14_anys) from {}".format(tb_name), db_name)
    A13, = u.getOne("select sum(segellat_entre_0_5_anys) from {}".format(tb_name), db_name)
    A14, = u.getOne("select sum(segellat_entre_6_14_anys) from {}".format(tb_name), db_name)
    A15, = u.getOne("select sum(segellat_entre_0_14_anys) from {}".format(tb_name), db_name)
    A16, = u.getOne("select sum(obturacio_entre_0_5_anys) from {}".format(tb_name), db_name)
    A17, = u.getOne("select sum(obturacio_entre_6_14_anys) from {}".format(tb_name), db_name)
    A18, = u.getOne("select sum(obturacio_entre_0_14_anys) from {}".format(tb_name), db_name)
    A19, = u.getOne("select sum(exodoncia_entre_0_5_anys) from {}".format(tb_name), db_name)
    A20, = u.getOne("select sum(exodoncia_entre_6_14_anys) from {}".format(tb_name), db_name)
    A21, = u.getOne("select sum(exodoncia_entre_0_14_anys) from {}".format(tb_name), db_name)
    A22, = u.getOne("select sum(tartrectomia_entre_0_5_anys) from {}".format(tb_name), db_name)
    A23, = u.getOne("select sum(tartrectomia_entre_6_14_anys) from {}".format(tb_name), db_name)
    A24, = u.getOne("select sum(tartrectomia_entre_0_14_anys) from {}".format(tb_name), db_name)
    A25, = u.getOne("select sum(tract_incisiu_canins_traumatisme_entre_0_5_anys) from {}".format(tb_name), db_name)
    A26, = u.getOne("select sum(tract_incisiu_canins_traumatisme_entre_6_14_anys) from {}".format(tb_name), db_name)
    A27, = u.getOne("select sum(tract_incisiu_canins_traumatisme_entre_0_14_anys) from {}".format(tb_name), db_name)
    A28, = u.getOne("select sum(revisions_escolars_p4) from {}".format(tb_name), db_name)
    A29, = u.getOne("select sum(revisions_escolars_primer_primaria) from {}".format(tb_name), db_name)
    A30, = u.getOne("select sum(revisions_escolars_sise_primaria) from {}".format(tb_name), db_name)
    B1, = u.getOne("select sum(diana_embaras) from {}".format(tb_name), db_name)
    B2, = u.getOne("select sum(diana_embaras_atesa) from {}".format(tb_name), db_name)
    B3 = 100 * (B2/B1)
    B4, = u.getOne("select sum(visites_odn_embaras) from {}".format(tb_name), db_name)
    B5, = u.getOne("select sum(revisions_odn_embaras) from {}".format(tb_name), db_name)
    B6, = u.getOne("select sum(fluoracio_embaras) from {}".format(tb_name), db_name)
    B7, = u.getOne("select sum(tartrectomia_embaras) from {}".format(tb_name), db_name)
    C1, = u.getOne("select sum(diana_oncologic) from {}".format(tb_name), db_name)
    C2, = u.getOne("select sum(visites_odn_oncologic) from {}".format(tb_name), db_name)
    C3, = u.getOne("select sum(revisions_odn_oncologic) from {}".format(tb_name), db_name)
    C4, = u.getOne("select sum(fluoracio_oncologic) from {}".format(tb_name), db_name)
    C5, = u.getOne("select sum(tartrectomia_oncologic) from {}".format(tb_name), db_name)
    C6, = u.getOne("select sum(exodoncia_oncologic) from {}".format(tb_name), db_name)
    D1, = u.getOne("select sum(diana_discapacitat) from {} where edat > 14".format(tb_name), db_name)
    D2, = u.getOne("select sum(visites_odn) from {} where diana_discapacitat = 1".format(tb_name), db_name)
    D3, = u.getOne("select sum(revisions_odn) from {} where diana_discapacitat = 1".format(tb_name), db_name)
    D4, = u.getOne("select sum(fluoracio) from {} where diana_discapacitat = 1".format(tb_name), db_name)
    D5, = u.getOne("select sum(segellat) from {} where diana_discapacitat = 1".format(tb_name), db_name)
    D6, = u.getOne("select sum(obturacio) from {} where diana_discapacitat = 1".format(tb_name), db_name)
    D7, = u.getOne("select sum(tract_incisiu_canins_traumatisme) from {} where diana_discapacitat = 1".format(tb_name), db_name)
    D8, = u.getOne("select sum(exodoncia) from {} where diana_discapacitat = 1".format(tb_name), db_name)
    D9, = u.getOne("select sum(tartrectomia) from {} where diana_discapacitat = 1".format(tb_name), db_name)

    print("Dades calculades")


    indicadors = OrderedDict([
        ("A: INFANTOJUVENIL", OrderedDict([
            ("A1", ("Poblaci�n diana", A1)),
            ("A2", ("N�mero ni�os atendidos", A2)),
            ("A3", ("Cobertura alcanzada (%)", A3)),
            ("A4", ("N�mero de consultas de 0-5 a�os", A4)),
            ("A5", ("N�mero de consultas de 6-14 a�os", A5)),
            ("A6", ("N�mero de consultas de 0-14 a�os", A6)),
            ("A7", ("N�mero de revisiones de 0-5 a�os", A7)),
            ("A8", ("N�mero de revisiones de 6-14 a�os", A8)),
            ("A9", ("N�mero de revisiones de 0-14 a�os", A9)),
            ("A10", ("Aplicaci�n de sustancias remineralizantes, antis�pticas y/o desensibilizantes en poblaci�n de 0-5 a�os", A10)),
            ("A11", ("Aplicaci�n de sustancias remineralizantes, antis�pticas y/o desensibilizantes en poblaci�n de 6-14 a�os", A11)),
            ("A12", ("Aplicaci�n de sustancias remineralizantes, antis�pticas y/o desensibilizantes en poblaci�n de 0-14 a�os", A12)),
            ("A13", ("Sellados de fosas y fisuras en poblaci�n de 0-5 a�os", A13)),
            ("A14", ("Sellados de fosas y fisuras en poblaci�n de 6-14 a�os", A14)),
            ("A15", ("Sellados de fosas y fisuras en poblaci�n de 0-14 a�os", A15)),
            ("A16", ("Obturaciones en poblaci�n de 0-5 a�os", A16)),
            ("A17", ("Obturaciones en poblaci�n de 6-14 a�os", A17)),
            ("A18", ("Obturaciones en poblaci�n de 0-14 a�os", A18)),
            ("A19", ("Exodoncias en poblaci�n de 0-5 a�os", A19)),
            ("A20", ("Exodoncias en poblaci�n de 6-14 a�os", A20)),
            ("A21", ("Exodoncias en poblaci�n de 0-14 a�os", A21)),
            ("A22", ("Tartrectom�as en poblaci�n de 0-5 a�os", A22)),
            ("A23", ("Tartrectom�as en poblaci�n de 6-14 a�os", A23)),
            ("A24", ("Tartrectom�as en poblaci�n de 0-14 a�os", A24)),
            ("A25", ("Tratamiento en incisivos y caninos definitivos por traumatismo en poblaci�n 0-5 a�os", A25)),
            ("A26", ("Tratamiento en incisivos y caninos definitivos por traumatismo en poblaci�n 6-14 a�os", A26)),
            ("A27", ("Tratamiento en incisivos y caninos definitivos por traumatismo en poblaci�n 0-14 a�os", A27)),
            ("A28", ("Revisiones escolares infantil 4 (P4)", A28)),
            ("A29", ("Revisiones escolares primero de primaria", A29)),
            ("A30", ("Revisiones escolares sexto de primaria", A30)),
        ])),
        ("B: MUJERES EMBARAZADAS", OrderedDict([
            ("B1", ("Poblaci�n diana estimada embarazada", B1)),
            ("B2", ("N�mero de embarazadas atendidas", B2)),
            ("B3", ("Cobertura alcanzada (%)", B3)),
            ("B4", ("N�mero de consultas en atenci�n primaria", B4)),
            ("B5", ("N�mero de revisiones", B5)),            
            ("B6", ("Aplicaci�n sustancias remineralizantes, antis�pticas y/o desensibilizantes", B6)),
            ("B7", ("Tartrectom�as", B7)),
        ])),
        ("C: PERSONAS DIAGNOSTICADAS DE PROCESOS ONCOL�GICOS CERVICOFACIALES", OrderedDict([
            ("C1", ("N�mero total de pacientes oncol�gicos atendidos", C1)),
            ("C2", ("N�mero total de consultas odontol�gicas de atenci�n primaria", C2)),
            ("C3", ("N�mero total de revisiones", C3)),
            ("C4", ("Aplicaci�n sustancias remineralizantes, antis�pticas y/o desensibilizantes", C4)),
            ("C5", ("Tartrectom�as", C5)),
            ("C6", ("Exodoncias", C6)),
        ])),
        ("D: PERSONAS CON DISCAPACIDAD", OrderedDict([
            ("D1", ("N�mero total de personas con discapacidad atendidas", D1)),
            ("D2", ("N�mero de consultas de atenci�n primaria", D2)),
            ("D3", ("N�mero de revisiones de atenci�n primaria", D3)),
            ("D4", ("Aplicaci�n sustancias remineralizantes, antis�pticas y/o desensibilizantes", D4)),
            ("D5", ("Sellados de fosas y fisuras", D5)),
            ("D6", ("Obturaciones", D6)),
            ("D7", ("Tratamiento en incisivos y caninos definitivos por traumatismo", D7)),
            ("D8", ("Exodoncias", D8)),
            ("D9", ("Tartrectom�as", D9)),
        ]))
    ])

    # Salutaci�

    salutacio = "Estimados compa�eros,\n\n"

    # Cos del email

    cos = "Es un placer para nosotros presentarles el informe anual de odontolog�a correspondiente al a�o {} en la comunidad aut�noma de Catalu�a. A continuaci�n, les proporcionamos una visi�n general de los datos relevantes en este �mbito.\n\n".format(data_ext.year)

    # Dades del email
    dades_email = ""
    for agrupacio, indicadors_agrupacio in indicadors.items():
        dades_email += "  - {}\n".format(agrupacio)
        for descriptiu, valor in indicadors_agrupacio.values():
            if valor == int(valor):
                # Si es un n�mero entero, formatearlo sin decimales
                valor_formatejat = "{:,.0f}".format(valor).replace(",", ".")
            else:
                # Si tiene decimales, formatearlo con dos decimales
                valor_formatejat = "{:,.2f}".format(valor).replace(",", "&").replace(".", ",").replace("&", ".")
            dades_email += "     - {}: {}\n".format(descriptiu, valor_formatejat)

    # Agra�ments

    agraiments = "\nAgradecemos su atenci�n y estamos a su disposici�n para cualquier consulta adicional.\n\nAtentamente,\n\nSISAP"

    # Combinar tot en un text

    email_capcalera = "Informe anual de odontolog�a del a�o {} en Catalu�a".format(data_ext.year)
    email_text = salutacio + cos + dades_email + agraiments
    # Enviar email

    u.sendGeneral('SISAP <sisap@gencat.cat>',
                  ('mfigueras.girona.ics@gencat.cat', 'elisabetcaula@gencat.cat'),
                  ('mquintana.apms.ics@gencat.cat', 'alejandrorivera@gencat.cat'),
                  email_capcalera,
                  email_text)

    print("Email enviat")