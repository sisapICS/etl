# -*- coding: utf-8 -*-

from sisapUtils import *
import time
from collections import defaultdict,Counter
from datetime import *

file_name = "IEPFAR"

table_name = "exp_ecap_iepfarm_uba2"
uba_columns = "(up varchar(5),indicador varchar(15),numerador int, denominador int)"

khalix = True


exclusions = { ('07674','M15'), ('07675','M21'),('07675','M26')}

psicofarmacos_atc = set(['N03AG01', 'N05CM02', 'N05AX08', 'N04BA02', 'N06BA09', 'N06AA09', 'N05BA06', 'N05AH06', 'N05AH05',
                         'N05AH04', 'N05AH03', 'N05AH02', 'N05BA12', 'N07BB04', 'N03AX09', 'N07BB01', 'N05BA10', 'N05AG02', 
                         'N05BA05', 'N05AN01', 'N06AX05', 'N05BA01', 'N03AE01', 'N07BC02', 'N02AB03', 'N02AA01', 'N04AA02', 
                         'N06AB04', 'N05AX12', 'N05AX13', 'N05AD01', 'N06AB05', 'N06BA04', 'N05BA09', 'N05BA08', 'N05AL03', 
                         'N05AF05', 'N05AL01', 'N05BA02', 'N05AE04', 'N06AB06', 'N07BC51', 'N03AF01', 'N05CD06', 'N05CD01', 
                         'N06AB03', 'N05AB03', 'N05AB02', 'N02BF01', 'N06AA04', 'N03AX11', 'N02BF02', 'N05AC01', 'N03AX14', 
                         'N06AX11', 'N06AX16', 'N05AA02', 'N03AA02', 'N05AA01'])

depot = set(['711282', '700662', '700661', '700660', '711278', '741256', '741264', '711288','700659', '741207', '701736', '711280', '701735','700535','665966',
             '700536', '665967', '700533', '665965','700532','665964','700535','665966','700536','665967','700533','665965','700532','665964',
             '682963', '732561', '732562', '732790', '732791'])

antipisoc = set(['N05AF05', 'N05AB03', 'N05AB02', 'N05AH03', 'N05AG02', 'N05AX08', 'N05AC01', 'N05AX12', 'N05AX13',
                'N05AL01', 'N05AD01', 'N05AL03', 'N05AH06', 'N05AH05', 'N05AH04', 'N05AA02', 'N05AH02', 'N05AE04', 'N05AA01'])     

antipisoc_2 = set(['N05AF05', 'N05AB03', 'N05AB02', 'N05AH03', 'N05AG02', 'N05AX08', 'N05AC01', 'N05AX12', 'N05AX13',
                'N05AD01', 'N05AL03', 'N05AH06', 'N05AH05', 'N05AH04', 'N05AA02', 'N05AH02', 'N05AE04', 'N05AA01'])     


benzo = set(['N05CD06', 'N05CD01', 'N05CD08', 'N05BA06', 'N05BA12', 'N05BA09', 'N05BA08', 'N03AE01', 'N05BA05', 'N05BA10', 'N05BA02', 'N05BA01'])

preglabina = set(['N02BF02'])

benzo_dosi_maxima = {'N03AE01': 20, 'N05BA01': 40 , 'N05BA02': 100, 'N05BA05': 100, 'N05BA06': 10, 'N05BA08': 12,
                     'N05BA09': 30, 'N05BA10': 75, 'N05BA12': 8, 'N05CD06': 2, 'N05CD08': 15, 'N05CD01': 30, 
                     'N05BA14': 10, 'N05CD05': 0.5, 'N05CD09': 0.25, 'N05CD10': 0.3, 'N05CD11': 2, 'N05CF02': 10,
                     'N05BA51': 40, 'N05BA55': 100}

# Cada subindicador, para el set de farmacos que deben tener prescrito actualmente los ids del numerador y, si procede, del denominador

tipus_farmac_per_indicador = {'1A': {'num': psicofarmacos_atc                                 },
                              '1B': {'num': psicofarmacos_atc,    'den': psicofarmacos_atc    },
                              '2' : {'num': depot,                'den': antipisoc_2            }, 
                            #   '3':  {'num': benzo                                             },                              
                              '3B': {'num': benzo,                'den': benzo                },
                              '4' : {'num': antipisoc,            'den': antipisoc            },
                            #   '5' : {'num': preglabina,           'den': preglabina           },
                              '5B': {'num': preglabina                                        },
                              '6' : {'num': benzo,                'den': benzo                },
                              '7' : {'num': benzo,                'den': benzo                }}

#Diccionario para en la busqueda general a tractaments, asociar cada codigo a un subindicador y al numerador o denominador

codis_farmac_per_indicador = defaultdict(set)
for ind_codi in tipus_farmac_per_indicador:
    for num_o_den, set_codis_farmac in tipus_farmac_per_indicador[ind_codi].iteritems():
        for codi_farmac in set_codis_farmac:
            codis_farmac_per_indicador[codi_farmac].add((ind_codi, num_o_den))

def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""

    sql = """
            SELECT
                data_ext
            FROM
                dextraccio 
          """
    current_date, = getOne(sql, "nodrizas")
    return current_date	


def get_conversor_idcipsec_2_hash():
    """."""

    conversor_idcipsec_2_hash = dict()
    
    sql = """
            SELECT
                id_cip_sec,
                hash_d
            FROM
                import_jail.u11
          """
    for id_cip_sec, hash in getAll(sql,'import_jail'):
        conversor_idcipsec_2_hash[id_cip_sec] = hash
    return conversor_idcipsec_2_hash    


def get_problemes(current_date):
    """ Selecciona de import.problemas los ids que presenten un pr_cod_ps dentro de las opciones de la variable in_statement o que empiecen con los caracteres que hay 
        en la variable like_statement, que se ha 'traducido' a query de sql en la variable where_like_statement. import.problemes se filtra como se hace para crear eqa.problemes
        (pr_cod_o_ps = 'C' and pr_hist = 1, y que la fecha de baixa logica del servicio sea nula).
        De lo id que resultan, se coge la fecha de diagnostico (dde) y la de fin del problema de salud (dba). Si dba es nula, se le asigna la del fin del periodo de calculo.
        Por ultimo, se a�aden a un set aquellos id_cip_sec para los que la dba es mayor o igual a la del fin del periodo (es mas reciente o la misma).
    """
    
    like_statement = ['G40%','G41%','C01-G40%','C01-G41%']
    where_like_statement = " ".join("OR pr_cod_ps LIKE '{}'".format(pare) for pare in like_statement)
    in_statement = ('F41.1','M79.2')

    sql = """
            SELECT
                id_cip_sec,
                pr_dba
            FROM
                problemes
            WHERE
                pr_cod_o_ps = 'C'
                AND pr_hist = 1
                AND pr_data_baixa = 0
                AND (pr_cod_ps IN {} {})
          """.format(in_statement,where_like_statement)
    
    pacients = set()
    for id, dba in getAll(sql, "import_jail"):
        if not dba or dba >= current_date:
            pacients.add(id)
    return pacients


def get_pacients_dosi_superior_benzo(current_date, benzo_dosi_maxima):
    """."""

    pf_cod_atc_where_statement = "".join("OR (pf_cod_atc = '{}' AND ((ppfmc_posologia*ce_quantitat) / ppfmc_freq*24) > {})".format(pf_cod_atc, dosi_maxima) \
                                for pf_cod_atc, dosi_maxima in benzo_dosi_maxima.items())[3:]

    sql = """
            SELECT
                id_cip_sec
            FROM
                import_jail.tractaments,
                import.cat_cpftb001,
                import.cat_cpftb006
            WHERE
                ppfmc_pf_codi = ce_pf_codi
                AND ce_pf_codi = pf_codi
                AND ppfmc_data_fi > '{}'
                AND ({})
          """.format(current_date, pf_cod_atc_where_statement)
    pacients = set()
    for id, in getAll(sql, "import_jail"):
        pacients.add(id)
    return pacients


def get_sets_codis_farmac_per_tipus(tipus_farmac_per_indicador):
    """."""

    sets_codis_farmac_per_tipus = defaultdict(set)
    for ind_codi in tipus_farmac_per_indicador:
        for num_o_den in tipus_farmac_per_indicador[ind_codi]:
            set_codis_farmac = tipus_farmac_per_indicador[ind_codi][num_o_den]
            for codi_farmac in set_codis_farmac:
                try: 
                    int(codi_farmac)
                    key = 'ppfmc_pf_codi'
                except:
                    key = 'ppfmc_atccodi'
                sets_codis_farmac_per_tipus[key].add(codi_farmac)
    return sets_codis_farmac_per_tipus


def get_pacients_indicadors_num_o_den(set_codis_farmac, current_date, codi_field):
    """."""

    pacients_indicadors_num_o_den = defaultdict(lambda: defaultdict(set))

    sql = """
            SELECT
                id_cip_sec,
                {0},
                ppfmc_pmc_data_ini,
                ppfmc_data_fi
            FROM
                tractaments
            WHERE
                {0} IN {1}
          """.format(codi_field, tuple(set_codis_farmac))
    for id_cip_sec, codi_farmac, inici_tract, fi_tract in getAll(sql, "import_jail"):
        if inici_tract <= current_date <= fi_tract:
            for (ind_codi, num_o_den) in codis_farmac_per_indicador[str(codi_farmac)]:
                if ind_codi not in ("1B", "3B", "4"):
                    pacients_indicadors_num_o_den[(ind_codi, num_o_den)][id_cip_sec].add((codi_farmac, inici_tract))
                elif ind_codi in ("1B", "3B", "4"):
                    estava = 0
                    for (codi_farmac_pre, inici_tract_pre) in pacients_indicadors_num_o_den[(ind_codi, num_o_den)][id_cip_sec]:
                        if codi_farmac == codi_farmac_pre:
                            estava = 1
                            if inici_tract < inici_tract_pre:
                                pacients_indicadors_num_o_den[(ind_codi, num_o_den)][id_cip_sec].remove((codi_farmac_pre, inici_tract_pre))
                                pacients_indicadors_num_o_den[(ind_codi, num_o_den)][id_cip_sec].add((codi_farmac, inici_tract))
                                break
                    if not estava:
                        pacients_indicadors_num_o_den[(ind_codi, num_o_den)][id_cip_sec].add((codi_farmac, inici_tract))

    return pacients_indicadors_num_o_den    


def get_indicador(codi, pac, num, den = None):
    """."""
    indicador = defaultdict(Counter)
    llistat = list()
    denominadors = defaultdict(int)

    sql = """
            SELECT
                id_cip_sec,
                up,
                uba,
                codi_sector
            FROM
                jail_assignada
          """
    for id_cip_sec, up, uba, sector in getAll(sql, "nodrizas"):
        if not (up, uba) in exclusions:
            denominador = id_cip_sec in den if den else True 
            if denominador:
                denominadors[(up, uba)] += 1
                number_den = 1 if pac else len(den[id_cip_sec])
                indicador[up]["DEN"] += number_den
                if id_cip_sec in num:
                    hash = conversor_idcipsec_2_hash[id_cip_sec]
                    number_num = 1 if pac else len(num[id_cip_sec])
                    indicador[up]["NUM"] += number_num
                    if codi != "IEPFAR001A":
                        llistat.append((up, uba, 'M', codi, hash, sector, 0))
        
    return [(up,codi,indicador[up]["NUM"],indicador[up]["DEN"]) for up in indicador], llistat, denominadors


if __name__ == '__main__':
    
    # Obtenci�n de la fecha actual
    current_date = get_date_dextraccio()

    # Obtenci�n del conversor id_cip_sec - hash
    conversor_idcipsec_2_hash = get_conversor_idcipsec_2_hash()

    # Creacion de tabla en altres para exportar a LV 
    createTable(table_name, uba_columns, "altres", rm = True)

    # Pacientes con problemas de salud registrados
    prob_salut_preg = get_problemes(current_date)

    # Pacientes con dosis de benzodiacepinas superiores a las que marca la ficha t�cnica
    dosi_superior_benzo = get_pacients_dosi_superior_benzo(current_date, benzo_dosi_maxima)

    # Diccionario que asocia, para cada indicador / subindicador y tipo (num / den) un tuple con los elementos siguientes:
    #      -> n_prescripcions (int), el numero de farmacos al que el paciente debe estar prescrito para entrar en el tipo correspondiente
    #      -> minim_dies_prescripcio (int), el numero de dias minimos para los que tiene que estar hecha la prescripcion
    #      -> set_idcipsecs_condicio (set_ids), un set de pacientes al que tienen que pertenecer los pacientes prescritos con los farmacos correspondientes 
    # La key 'PAC' indica si el indicador es a nivel de n�mero de pacientes (1) o de n�mero de prescripciones (0)
    # EL DICCIONARIO CON LOS FARMACOS PARA CADA SUBINDICADOR Y TIPO ES OTRO, el tipus_farmac_per_indicador

    opcions_per_indicador = {"1A": {"num": (5, None, None),                 'PAC': 1                            },
                             "1B": {"num": (5, None, None),                 'PAC': 1,    "den":(1, None, None)  },
                             "2":  {"num": (1, None, None),                 'PAC': 1,    "den":(1, None, None)  },
                            #  "3":  {"num": (3,None,None),                   'PAC': 1                            },
                             "3B": {"num": (2, None, None),                 'PAC': 1,    "den":(1, None, None)  },
                             "4":  {"num": (3, None, None),                 'PAC': 1,    "den":(1, None, None)  },
                            #  "5":  {"num": (1, None, prob_salut_preg),      'PAC': 0,    "den":(1, None, None)  },
                             "5B": {"num": (1, None, None),                 'PAC': 1                            },
                             "6":  {"num": (1, 8*7,  None),                 'PAC': 1,    "den":(1, None, None)  },
                             "7":  {"num": (1, None, dosi_superior_benzo),  'PAC': 1,    "den":(1, None, None)  }}

    # Obtenci�n para cada tipo de codigo (atc o pf) el set de farmacos y asi hacer una select conjunta por tipo de codigo
    sets_codis_farmac_per_tipus = get_sets_codis_farmac_per_tipus(tipus_farmac_per_indicador)
    
    # Obtenci�n para el subindicador y tipo (num/den) el id_cip_sec del paciente y un set (farm, fecha_inicio) para cada farmaco que corresponde
    pacients_indicadors_num_o_den_total = defaultdict(lambda: defaultdict(set))
    for codi_field, set_codis_farmac in sets_codis_farmac_per_tipus.iteritems():
        pacients_indicadors_num_o_den = get_pacients_indicadors_num_o_den(set_codis_farmac, current_date, codi_field)
        for (ind_codi, tipus) in pacients_indicadors_num_o_den:
            for id_cip_sec in pacients_indicadors_num_o_den[(ind_codi, tipus)]:
                pacients_indicadors_num_o_den_total[(ind_codi, tipus)][id_cip_sec] = pacients_indicadors_num_o_den[(ind_codi, tipus)][id_cip_sec]

    # Montaje de cada uno de los indicadores utilizando la info de opcions_per_indicador
    for ind_codi in opcions_per_indicador:

        # Exportaci� PRSLLISTATS per indicador

        ind_nom = "IEPFAR00" + ind_codi
        num_den_idcipsecs = defaultdict(lambda: defaultdict(set))
        for tipus in opcions_per_indicador[ind_codi]:
            if tipus == 'PAC':
                continue
            n_prescripcions, minim_dies_prescripcio, set_idcipsecs_condicio = opcions_per_indicador[ind_codi][tipus]
            for id_cip_sec in pacients_indicadors_num_o_den_total[(ind_codi, tipus)]:
                if set_idcipsecs_condicio:
                    if id_cip_sec not in set_idcipsecs_condicio:
                        continue
                set_fechas = {inici_tract for farm_codi, inici_tract in pacients_indicadors_num_o_den_total[(ind_codi, tipus)][id_cip_sec] if daysBetween(inici_tract,current_date) >= minim_dies_prescripcio} if minim_dies_prescripcio else pacients_indicadors_num_o_den_total[(ind_codi, tipus)][id_cip_sec]
                if len(set_fechas) >= n_prescripcions:
                    num_den_idcipsecs[tipus][id_cip_sec] = set_fechas
        pac = opcions_per_indicador[ind_codi]["PAC"]
        num = num_den_idcipsecs['num']
        den = num_den_idcipsecs['den'] if 'den' in opcions_per_indicador[ind_codi] else None

        rows, llistat, denominadors = get_indicador(ind_nom, pac, num, den)

        listToTable(rows, table_name, 'altres')
        listToTable(llistat, 'PRSLLISTATS', 'pdp')

        # Exportaci� PRSINDICADORS per indicador

        export_indicadors_tb = "export_ecap_{}".format(ind_nom)
        columnes = "(up varchar(5), uba varchar(9), ind_codi varchar(20), num int, den int, res double)"
        createTable(export_indicadors_tb, columnes, "altres", rm=True)

        indicadors = set()
        indicadors_prs = defaultdict(Counter)
        sql = """SELECT up, uab, count(*) FROM pdp.PRSLLISTATS 
                 WHERE INDICADOR = '{}'
                 GROUP BY up, uab""".format(ind_nom)
        for up, uba, num in getAll(sql, 'pdp'):
            deno = denominadors[(up, uba)]
            if deno != 0:
                res = float(num)/float(deno)
            else:
                res = float(num)
            indicadors_prs[(up, ind_nom)]['NUM'] += float(num)
            indicadors_prs[(up, ind_nom)]['DEN'] += float(deno)
            indicadors.add((current_date.year, current_date.month, up, uba, ind_nom, num, deno, res))
        listToTable([row[2:] for row in indicadors], export_indicadors_tb,'altres')
        for (up, ind), vals in indicadors_prs.items():
            num = vals['NUM'] if 'NUM' in vals else 0
            den = vals['DEN'] if 'DEN' in vals else 0
            if den:
                res = float(num)/float(den)
                indicadors.add((current_date.year, current_date.month, up, 'PRS', ind, num, den, res))
        listToTable(list(indicadors), 'PRSINDICADORS', 'pdp')

    # Exportaci� PRSCATALEGPARE

    export_catalegpare_tb = "export_catalegpare_iepfarm"
    columnes = "(pare varchar(10), literal varchar(300), ordre int, subtotal int)"
    createTable(export_catalegpare_tb, columnes, "altres", rm=True)

    pare = [(current_date.year, 'IEPFAR', 'Indicadors de farm�cia d\'equips penitenciaris', 11, 0)]
    listToTable([row[1:] for row in pare], export_catalegpare_tb,'altres')

    listToTable(pare, 'PRSCATALEGPARE', 'pdp')

    # Exportaci� PRSCATALEG

    export_cataleg_tb = "export_cataleg_iepfarm"
    columnes = "(indicador varchar(10), literal varchar(300), ordre int, pare varchar(10), llistat int, invers int, mmin int, mint int, mmax int, toshow int, wiki varchar(250), tipus varchar(2), ncol int)"
    createTable(export_cataleg_tb, columnes, "altres", rm=True)

    fills = [(current_date.year, 'IEPFAR001B', 'Pacients en tractament amb 5 o m�s psicof�rmacs', 1001, 'IEPFAR', 1, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/2365/ver/', 'M', 0),
             (current_date.year, 'IEPFAR002', 'Pacients en tractament amb antipsic�tics depot', 1002, 'IEPFAR', 1, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/2366/ver/', 'M', 0),
             (current_date.year, 'IEPFAR003B', 'Percentatge de pacients tractats amb 2 o m�s benzodiazepines', 1003, 'IEPFAR', 1, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4344/ver/', 'M', 0),
             (current_date.year, 'IEPFAR004', 'Pacients en tractament amb 3 o m�s antipsic�tics', 1004, 'IEPFAR', 1, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/2368/ver/', 'M', 0),
            #  (current_date.year, 'IEPFAR005', 'Pacients tractats amb pregabalina', 1005, 'IEPFAR', 1, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/2369/ver/', 'M', 0),
             (current_date.year, 'IEPFAR005B', 'Prevalen�a de pacients tractats amb pregabalina', 1006, 'IEPFAR', 1, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4351/ver/', 'M', 0),
             (current_date.year, 'IEPFAR006', 'Pacients en tractament amb �s perllongat de Benzodiazepines', 1007, 'IEPFAR', 1, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/2955/ver/', 'M', 0),
             (current_date.year, 'IEPFAR007', 'Pacients tractats amb benzodiacepines amb dosis superiors a la que marca la fitxa t�cnica', 1008, 'IEPFAR', 1, 0, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/4345/ver/', 'M', 0)]
    listToTable([row[1:] for row in fills], export_cataleg_tb, 'altres')
    
    listToTable(fills, 'PRSCATALEG', 'pdp')

    # Exportaci� LV

    query_template_string = """
                                SELECT
                                    indicador,
                                    concat('A', 'periodo'),
                                    ics_codi,
                                    {strg},
                                    'NOCAT',
                                    'NOIMP',
                                    'DIM6SET',
                                    'N',
                                    {var}
                                FROM
                                    altres.{taula} a
                                INNER JOIN nodrizas.jail_centres b ON
                                    a.up = scs_codi
                                WHERE
                                    a.{var} != 0
                            """
    query_string_1 = query_template_string.format(strg = "'NUM'", var = "numerador", taula = table_name)
    query_string_2 = query_template_string.format(strg = "'DEN'", var = "denominador", taula = table_name)
    query_string_sencera = query_string_1 + " UNION " + query_string_2
    exportKhalix(query_string_sencera, file_name)