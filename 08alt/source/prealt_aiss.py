import sisapUtils as u
import datetime as d
import collections as c

DEXTD = u.getOne("select data_ext from dextraccio", "nodrizas")[0]
sql = """
    SELECT id_cip_sec, up, uba
    FROM assignada_tot
"""
pob = {}
for id, up, uba in u.getAll(sql, 'nodrizas'):
    pob[id] = (up, uba)
u.printTime('assignada_tot')
sql = """
    SELECT id_cip_sec
    FROM prealt1
    WHERE enll_data_alt between date_add(DATE '{DEXTD}',interval -1 year)
    AND DATE '{DEXTD}'
    AND enll_tipus_origen = 'H'
    """.format(DEXTD=DEXTD)

prealt = set()
for id, in u.getAll(sql, 'import'):
    prealt.add(id)
u.printTime('prealt1')
conversor = {}
sql = """
    SELECT id_cip_sec, hash_d
    FROM u11
"""

for id, hash in u.getAll(sql, 'import'):
    conversor[hash] = id
u.printTime('u11')
# sql = """
#     SELECT hash_redics, nia
#     FROM dwsisap.pdptb101_relacio_nia
# """
# nia_id = {}
# for hash, nia in u.getAll(sql, 'exadata'):
#     try:
#         nia_id[nia] = conversor[hash]
#     except:
#         pass

sql = """
    SELECT b.hash_redics
    FROM dwcatsalut.tf_cmbdha a
    INNER JOIN dwsisap.pdptb101_relacio_nia b
    ON a.nia = b.nia
    WHERE a.data_alta between add_months(DATE '{DEXTD}', -12)
    AND DATE '{DEXTD}'
    AND a.nia is not null
""".format(DEXTD=DEXTD)

hosp = set()
for hash, in u.getAll(sql, 'exadata'):
    try:
        hosp.add(conversor[hash])
    except:
        pass
u.printTime('tf_cmbdha')
# construim indicador 1: pre-alt / pob general
# construim indicador 2: pre-alt / hosp
upload = []
for id in pob:
    num = 1 if id in prealt else 0
    up, uba = pob[id]
    upload.append((id, 'GEN', up, uba, num))
    if id in hosp:
        upload.append((id, 'HOSP', up, uba, num))

cols = "(id_cip_sec int, ind varchar(5), up varchar(5), uba varchar(7), num int)"
u.createTable('prealt_aiss', cols, 'altres', rm=True)
u.listToTable(upload, 'prealt_aiss', 'altres')
u.printTime('fi')
