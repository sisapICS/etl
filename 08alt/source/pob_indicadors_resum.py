from sisapUtils import *
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

from sisapUtils import *
import csv, os, sys

#Altres indicadors pantalla

bd = "altres"
conn = connect((bd,'aux'))
c = conn.cursor()

origen="assignada_tot"
assignada = "nodrizas.assignada_tot"

tipus=[['M','up','uba'],['I','upinf','ubainf']]

pob = {}
sql = 'select id_cip_sec, up, edat from assignada_tot'
for id, up, edat in getAll(sql, 'nodrizas'):
    pob_p, pob_a =  0, 0
    if edat <15:
        pob_p = 1
    else:
        pob_a = 1
    if up in pob:
        pob[up]['pob'] += 1
        pob[up]['pob_p'] += pob_p
        pob[up]['pob_a'] += pob_a
    else:
        pob[up] = {'pob': 1, 'pob_a': pob_a, 'pob_p': pob_p}

tresum = "%s.exp_ecap_pobresum" % bd
tresumEAP = "%s.exp_ecap_pobresumEAP" % bd
den ="denominadors_assignada"
catalegresum="exp_ecap_catalegresum"
path="./dades_noesb/pob_indicadors_resum.txt"
instit="if(edat between 0 and 14,institucionalitzat_ps,institucionalitzat)"

variable = {"POBMES65":"edat","POBIMM":"nacionalitat","POBATES":"ates","POBMACA":"maca","POBPCC":"pcc","POBINST":"instit","POBMEN2":"edat"}
where = {"POBMES65":"edat >65","POBIMM":"if(nacionalitat='','724',nacionalitat)<>'724'","POBATES":"ates=1","POBMACA":"maca=1","POBPCC":"pcc=1","POBINST":"instit=1","POBMEN2":"edat<2"}


c.execute("drop table if exists %s" % origen)
c.execute("create table %s like %s" % (origen,assignada))
c.execute("insert into %s select * from %s" % (origen,assignada))
c.execute("alter table %s add column instit int" % origen)
c.execute("update %s set instit=%s" % (origen,instit))


c.execute("drop table if exists %s" % tresum)
c.execute("create table %s(up VARCHAR(5) NOT NULL DEFAULT'',uba VARCHAR(5) NOT NULL DEFAULT'', tipus varchar(1) not null default'',indicador varchar(10) not null default'',num int,den int,ind double)" % (tresum))

c.execute("drop table if exists %s" % tresumEAP)
c.execute("create table %s(up VARCHAR(5) NOT NULL DEFAULT'',indicador varchar(10) not null default'',num int,den int,ind double)" % (tresumEAP))

query="insert into %(tresum)s \
select \
    %(up)s  \
    ,%(uba)s  \
    ,'%(tip)s' \
    ,'%(indicador)s' \
  	 ,count(*)\
    ,0 \
    ,0 \
from \
    %(origen)s a \
where \
    %(where)s \
group by \
     %(up)s  \
    ,%(uba)s  "


  
with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for i in p:
      ind,desc,ordre= i[0],i[1],i[2]
      c.execute("select distinct index_name from information_schema.statistics where table_schema='%s' and table_name='%s'" % (bd,origen))
      idxs = c.fetchall()
      for idx in idxs:
        c.execute("alter table %s drop %s" % (origen,"PRIMARY KEY" if idx[0] == "PRIMARY" else ("index " + idx[0])))
      c.execute("alter table %s add index (up,uba,upinf,ubainf,%s)" % (origen,variable[ind])) 
      for r in tipus:
         tip,up,uba=r[0],r[1],r[2]
         c.execute(query % {"tresum":tresum,"up":up,"uba":uba,"tip":tip,"indicador":ind,"origen":origen,"where":where[ind]})

c.execute("select distinct index_name from information_schema.statistics where table_schema='%s' and table_name='%s'" % (bd,origen))
idxs = c.fetchall()
for idx in idxs:
   c.execute("alter table %s drop %s" % (origen,"PRIMARY KEY" if idx[0] == "PRIMARY" else ("index " + idx[0])))
c.execute("alter table %s add index (up,uba,upinf,ubainf)" % origen)
      
c.execute("drop table if exists %s" % den)
c.execute("create table %s (up varchar(5) not null default'',uba varchar(5) not null default'',tipus varchar(5) not null default'',den int)" % den)
for r in tipus:
   tip,up,uba=r[0],r[1],r[2]
   c.execute("insert into %(den)s select %(up)s, %(uba)s, '%(tip)s',count(*) from %(origen)s group by %(up)s, %(uba)s" % {"den":den,"up":up,"uba":uba,"tip":tip,"origen":origen})

c.execute("alter table %s add index (up,uba,tipus,den)" % den)
c.execute("update %s a inner join %s b on a.up=b.up and a.uba=b.uba and a.tipus=b.tipus set a.den=b.den" %(tresum,den))
c.execute("update %s  set ind=num/den" % tresum)
c.execute("alter table %s add unique (up,uba,tipus,indicador)" % tresum)

c.execute("drop table if exists %s" % catalegresum)
c.execute("create table %s(indicador varchar(10) not null default'', literal varchar(1000) not null default'', ordre int)" % catalegresum)

upload = []
sql = "select up,indicador,sum(num) from {0} where tipus='M' group by up,indicador".format(tresum)
for up, indica, num in getAll(sql, 'altres'):
    dena = 0
    if indica == 'POBMES65':
        dena = pob[up]['pob_a']
    elif indica == 'POBMEN2':
        dena = pob[up]['pob_p']
    else:
        dena = pob[up]['pob']
    upload.append([up, indica, num, dena, num/dena])
listToTable(upload, tresumEAP, 'altres')
    
with open(path, 'rb') as file:
   p=csv.reader(file, delimiter='@', quotechar='|')
   for i in p:
      ind,desc,ordre= i[0],i[1],i[2]
      c.execute("insert ignore into %s select up,uba,tipus,'%s' indicador,0 num,den, 0 ind from %s" % (tresum,ind,den))
      c.execute("insert into %s values('%s','%s','%s')" % (catalegresum,ind,desc,ordre))

print strftime("%Y-%m-%d %H:%M:%S")