import sisapUtils as u
import collections as c
import dateutil.relativedelta as du
import datetime as d


TABLE = "exp_proactivitat"
DATABASE = "altres"


def main():
    """."""
    cols = '(indicador varchar(20), periode varchar(5), up varchar(5), \
             uba varchar(10), analisi varchar(3), marc varchar(10), n int)'
    u.createTable(TABLE, cols, DATABASE, rm=True)
    Prescripcions()
    IT()
    Comptadors()
    to_khalix()
    to_ladybug()


def to_khalix():
    """."""
    now = "A" + "".join(map(str, u.getKhalixDates()[1:]))
    for arxiu, simbol in (("ACTUAL", "="), ("PREVI", "!=")):
        sql = """select indicador, periode, concat(ics_codi, 'M', uba),
                        analisi, marc, 'NOIMP', 'DIM6SET', 'N', n
                 from {}.{} p
                 inner join nodrizas.cat_centres c on p.up = c.scs_codi
                 where periode {} '{}'""".format(DATABASE, TABLE, simbol, now)
        u.exportKhalix(sql, 'PROACT_UBA_{}'.format(arxiu))
        sql = """select indicador, periode, ics_codi,
                        analisi, marc, 'NOIMP', 'DIM6SET', 'N', sum(n)
                 from {}.{} p
                 inner join nodrizas.cat_centres c on p.up = c.scs_codi
                 where periode {} '{}'
                 group by indicador, periode, ics_codi,
                          analisi, marc, 'NOIMP', 'DIM6SET', 'N'""".format(DATABASE, TABLE, simbol, now)  # noqa
        u.exportKhalix(sql, 'PROACT_{}'.format(arxiu))


def to_ladybug():
    """."""
    indicadors = ("PROACT01", "PROACT02", "PROACT03", "PROACT05A")
    mst = "mst_control_eqa"
    _periode = u.getKhalixDates()
    periode = "".join((_periode[0], _periode[2]))
    sql = "delete from {0} \
           where periode = {1} and \
                 indicador in {2}".format(mst, periode, indicadors)
    u.execute(sql, ('ladybug', 'shiny'))
    lv = "A" + _periode[1] + _periode[2]
    sql = "select {}, sector, amb_desc, indicador, \
                  sum(if(analisi = 'NUM', n, 0)), \
                  sum(if(analisi = 'DEN', n, 0)) \
           from altres.exp_proactivitat a \
           inner join nodrizas.cat_centres b on a.up  = b.scs_codi \
           where periode = '{}' and indicador in {} and marc = 'MENSUAL' \
           group by sector, amb_desc, indicador".format(periode, lv, indicadors)  # noqa
    dades = list(u.getAll(sql, "nodrizas"))
    u.listToTable(dades, mst, ('ladybug', 'shiny'))
    sql = "select nom, descripcio from umi.indicador_indicador \
           where nom in {}".format(indicadors)
    dim = [(cod, des, "Proactivitat") for cod, des
           in u.getAll(sql, ("centres", "x0001"))]
    cat = "cat_control_indicadors"
    sql = "delete from {} where indicador in {}".format(cat, indicadors)
    u.execute(sql, ('ladybug', 'shiny'))
    u.listToTable(dim, cat, ('ladybug', 'shiny'))
    """
    # historic
    sql = "select ics_codi, sector, amb_desc from cat_centres"
    br_to_amb = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}
    periodes = ["A24" + str(i).zfill(2) for i in range(1, 13)]
    periodes.append("A2501")
    for peri in periodes:
        historic = {}
        peri_int= int("20" + peri[1:])
        creuament = "{};{};AMBITOS###;NUM,DEN;MENSUAL;NOIMP;DIM6SET".format(",".join(indicadors), peri)  # noqa
        for ind, _a, br, analisi, _b, _c, _d, value in u.LvClient().query(creuament):  # noqa
            sec, amb = br_to_amb[br]
            key = (peri_int, sec, amb, ind)
            if key not in historic:
                historic[key] = {"NUM": 0, "DEN": 0}
            historic[key][analisi] += int(float(value))
        upload = [k + (v["NUM"], v["DEN"]) for k, v in historic.items()]
        sql = "delete from {0} \
               where periode = {1} and \
                     indicador in {2}".format(mst, peri_int, indicadors)
        u.execute(sql, ('ladybug', 'shiny'))
        u.listToTable(upload, mst, ('ladybug', 'shiny'))
    """


class Prescripcions(object):
    """."""

    def __init__(self):
        """."""
        self.get_prescripcions()
        self.upload_data()

    def get_prescripcions(self):
        dext, = u.getOne('select data_ext from nodrizas.dextraccio', 'nodrizas')  # noqa
        day = str(dext.day) if len(str(dext.day)) == 2 else '0'+str(dext.day)
        month = str(dext.month) if len(str(dext.month)) == 2 else '0'+str(dext.month)  # noqa
        year = str(dext.year)
        dat_str = day + '/' + month + '/' + year
        sql = """SELECT c_up, c_metge,
                    CASE 
                        WHEN (av_prescripcions_cad_data < (to_date('{dat_str}', 'dd/mm/yyyy') + 14)
                            AND av_prescripcions_cad_data > to_date('{dat_str}', 'DD/MM/YYYY')) 
                                THEN 'to_caducar'
                        WHEN (av_prescripcions_cad_data > add_months(to_date('{dat_str}', 'dd/mm/yyyy'), -2)
                            AND av_prescripcions_cad_data < to_date('{dat_str}', 'DD/MM/YYYY'))
                                THEN 'caducat'
                        WHEN av_prescripcions_cad_data > (to_date('{dat_str}', 'dd/mm/yyyy') + 14)
                            THEN 'ok' 
                        ELSE 'hiper_caducat'
                        END CADUCITATS
                    FROM DBS
                    WHERE av_prescripcions_cad_data IS NOT null""".format(dat_str=dat_str)  # noqa
        self.prescripcions = c.Counter()
        self.indi_1 = c.Counter()
        self.indi_2 = c.Counter()
        for up, metge, caducitat in u.getAll(sql, 'redics'):
            if caducitat == 'to_caducar':
                self.indi_1[(up, metge)] += 1
            if caducitat == 'caducat':
                self.indi_2[(up, metge)] += 1

    def upload_data(self):
        upload = []
        periode = "A" + "".join(u.getKhalixDates()[1:])
        for (up, metge), n in self.indi_1.items():
            upload.append(('PROACT01', periode, up, metge, 'NUM', 'MENSUAL', n))
            upload.append(('PROACT01', periode, up, metge, 'DEN', 'MENSUAL', 1))
        for (up, metge), n in self.indi_2.items():
            upload.append(('PROACT02', periode, up, metge, 'NUM', 'MENSUAL', n))
            upload.append(('PROACT02', periode, up, metge, 'DEN', 'MENSUAL', 1))
        u.listToTable(upload, TABLE, DATABASE)


class IT(object):
    """."""

    def __init__(self):
        """."""
        self.periode = "A" + "".join(u.getKhalixDates()[1:])
        self.get_03()

    @staticmethod
    def _get_visites(params):
        """."""
        return ([(id, dat) for (id, dat) in u.getAll(*params)
                 if id in IT.it and dat in IT.it[id]])

    def get_03(self):
        """."""
        IT.it = c.defaultdict(set)
        sql = "select id_cip_sec, ilt_data_baixa \
               from baixes, nodrizas.dextraccio \
               where ilt_data_baixa between \
                        adddate(data_ext, interval -1 year) and data_ext and \
                     if(ilt_data_alta = 0, data_ext, \
                                           ilt_data_alta) - ilt_data_baixa > 4"
        for id, dat in u.getAll(sql, "import"):
            IT.it[id].add(dat)
        sql = "show create table visites1"
        tables = u.getOne(sql, "import")[1].split("UNION=(")[1][:-1].split(",")
        sql = "select id_cip_sec, visi_dia_peticio \
               from {}, nodrizas.dextraccio \
               where visi_dia_peticio between \
                        adddate(data_ext, interval -1 year) and data_ext and \
                     visi_data_visita > visi_dia_peticio"
        jobs = [(sql.format(table), "import") for table in tables]
        complidors = set()
        for chunk in u.multiprocess(u.get_data, jobs):
            for row in chunk:
                complidors.add(row)
        sql = "select id_cip_sec, up, uba from assignada_tot where edat > 15"
        resultat = c.Counter()
        for id, up, uba in u.getAll(sql, "nodrizas"):
            if id in IT.it:
                den = 0
                num = 0
                for dat in IT.it[id]:
                    den += 1
                    if (id, dat) in complidors:
                        num += 1
                resultat[("PROACT03", self.periode, up, uba, "DEN", 'MENSUAL')] += den  # noqa
                resultat[("PROACT03", self.periode, up, uba, "NUM", 'MENSUAL')] += num  # noqa
        upload = [k + (v,) for k, v in resultat.items()]
        u.listToTable(upload, TABLE, DATABASE)


class Comptadors():
    """."""

    def __init__(self):
        """."""
        self.indicador = "PROACT05"
        self.get_poblacio()
        self.get_data()
        self.upload()

    def get_poblacio(self):
        """."""
        sql = "select id_cip_sec, up, uba from assignada_tot"
        self.poblacio = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_data(self):
        """."""
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        fa1m = dext - du.relativedelta(months=1)
        actual = "A" + str(dext.year)[2:] + str(dext.month).zfill(2)
        previ = "A" + str(fa1m.year)[2:] + str(fa1m.month).zfill(2)
        sql = """select id_cip_sec, pro_data_alta, pro_origen_accio, pro_tipus_pro
                 from proactivitat, nodrizas.dextraccio
                 where pro_data_alta between \
                    adddate(data_ext, interval -1 year) and data_ext"""
        self.recomptes = c.Counter()
        for id, dat, ori, tipus in u.getAll(sql, "import"):
            if id in self.poblacio:
                periode = "A" + str(dat.year)[2:] + str(dat.month).zfill(2)
                up, uba = self.poblacio[id]
                analisis = ["DEN"]
                if ori in ('ALERTA', 'SENSE'):
                    analisis.append("NUM")
                for analisi in analisis:
                    self.recomptes[(self.indicador, actual, up, uba, analisi, "ANUAL")] += 1  # noqa
                    if tipus == 'PLME':
                        self.recomptes[(self.indicador + 'A', actual, up, uba, analisi, "ANUAL")] += 1  # noqa
                    elif tipus == 'IT':
                        self.recomptes[(self.indicador + 'B', actual, up, uba, analisi, "ANUAL")] += 1  # noqa
                    if periode in (actual, previ):
                        self.recomptes[(self.indicador, periode, up, uba, analisi, "MENSUAL")] += 1  # noqa
                        if tipus == 'PLME':
                            self.recomptes[(self.indicador + 'A', periode, up, uba, analisi, "MENSUAL")] += 1  # noqa
                        elif tipus == 'IT':
                            self.recomptes[(self.indicador + 'B', periode, up, uba, analisi, "MENSUAL")] += 1  # noqa

    def upload(self):
        """."""
        upload = [k + (v,) for (k, v) in self.recomptes.items()]
        u.listToTable(upload, TABLE, DATABASE)


if __name__ == "__main__":
    main()
