# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
from dateutil import relativedelta as rd
import sisapUtils as u
import sisaptools as t
import os

TEST = False
RECOVER = False

nod = "nodrizas"
imp = "import"
imp_jail = "import_jail"

SERVEI_MAP = {
    'EXTRA': 'EXTRA',
    'EXT': 'EXTRA',
    'UAC': 'UAC',
    'UAAU': 'UAC',
    'ASO': 'TS',
    'AS': 'TS',
    'ASS': 'TS',
    'TS': 'TS',
    'TSOC': 'TS',
    'SS': 'TS',
    'RNAS': 'TS',
    'ASI': 'TS',
    'AASS': 'TS',
}

HOMOL_MAP = {
    'MG': 'MF',
    'INFP': 'INF',
    'LLEV': 'ALTRE',
    'GIN': 'ALTRE',
}

TIPUS_MAP = {
    '9C': 'C9C',
    '9R': 'C9R',
    '9D': 'D9D',
    '9T': '9T',
    '9E': '9E',
}

ETIQUETAS = [
    'INFC',  # INFORME CLÍNIC
    'PLME',  # PLA MEDICACIO
    'REIT',  # REVISIO IT
    'TRSA',  # TRANSPORT SANITARI
    'VALP',  # VALORACIÓ PROVES
    'VITA',  # VÍDEO CONSULTA
    'ECTA',  # ECONSULTA
]

# KCLI = 'NOCLI'  # ara per u.ageConverter()
KCAT = 'NOCAT'
KCLI = 'NOCLI'
KDTL = 'NOIMP'
# KCTL = 'DIM6SET'  # ara per sexe HOME/DONA (que fem amb sense sexe?)
KVAL = 'N'

if RECOVER:
    DEXTD = d.date(2020, 12, 31)
else:
    DEXTD = t.Database("p2262", nod).get_one("select data_ext from dextraccio")[0]  # noqa

FILE_NAME = 'ACTASSIST_YTD_{}_B.txt'.format(DEXTD.strftime("%m_%Y"))


def get_gedat(naix, vdat):
    # Get the current date
    # Get the difference between the current date and the birthday
    age = rd.relativedelta(vdat, naix)
    age = age.years
    gedat = u.ageConverter(age)
    return gedat


class Activitat(object):
    """."""

    def __init__(self):
        """."""
        # self.year, = u.getOne("select year(data_ext) from dextraccio", nod)
        self.get_centres()
        self.get_map_serveis()
        self.get_poblacio()
        self.get_visites()

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi from cat_centres_with_jail"
        self.centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}

    def get_map_serveis(self):
        """."""
        db = 'redics'
        sql = """select SECTOR, COD, 
                case when SERVEI_MAP = 'EXTRA' then SERVEI_MAP else SERVEI_CLASS end SERVEI
                from IMPORT.CAT_SISAP_MAP_SERVEIs
                where SERVEI_CLASS not IN ('ALTRE', 'URG', 'AUX')"""
        # provar sinó amb where SERVEI_CLASS != 'ALTRE'
        self.map_serveis = {}
        for sector, cod, servei in u.getAll(sql, 'import'):
            self.map_serveis[(sector, cod)] = servei

    # def get_map_serveis(self):
    #     """."""
    #     db = 'redics'
    #     sql = "select sector, cod, homol from sisap_covid_cat_serveis"
    #     self.map_serveis = {}
    #     for sec, servei, homol in u.getAll(sql, db):
    #         if servei in SERVEI_MAP:
    #             self.map_serveis[(sec, servei)] = SERVEI_MAP[servei]
    #         elif homol in HOMOL_MAP:
    #             self.map_serveis[(sec, servei)] = HOMOL_MAP[homol]
    #         else:
    #             self.map_serveis[(sec, servei)] = homol
    #     self.valores = set()
    #     for k, v in self.map_serveis.items():
    #         self.valores.add(v)
    #     print(self.valores)

    #     """
    #     imprime lista de homologaci�n de servicios para UMI
    #     Ojo la cuenta khalix es "VIS"+serv_homol+tipus_homol
    #     y tipus_homol (tipus homologado, que combina:
    #         visi_tipus_visita
    #         visi_lloc_visita y
    #         visi_etiqueta) no se incluye aqu�
    #     Que es exclusiva de servicios.
    #     """
    #     columns_dom = {
    #         'altres': '(homologacio varchar(20), sector varchar(5), servei varchar(20))',
    #         'pdp': '(homologacio varchar2(20), sector varchar2(5), servei varchar2(20))'
    #     }
    #     for domini, cols in columns_dom.items():
    #         u.createTable('homo_serveis_visitestot', cols, domini)
    #     upload = []
    #     out = c.defaultdict(set)
    #     for k, v in self.map_serveis.items():
    #         out[v].add(k)
    #     for k, v in out.items():
    #         if k != 'ALTRE':
    #             for v1 in list(v):
    #                 print("{homol}@{sector}@{servei}".format(
    #                     homol=k,
    #                     sector=v1[0],
    #                     servei=v1[1])
    #                       )
    #                 upload.append((k, v1[0], v1[1]))
    #     for domini in ('pdp', 'altres'):
    #         u.listToTable(upload, 'homo_serveis_visitestot', domini)

    def get_poblacio(self):
        self.gedats = {}
        self.sexes = {}
        sex_recode = {'H': 'HOME',
                      'D': 'DONA',
                      'M': 'DONA',
                      }
        sql = """
            SELECT
                id_cip_sec,
                usua_data_naixement,
                usua_sexe
            FROM
                assignadawithjail
            """
        for cip, naix, sex in u.getAll(sql, imp):
            self.gedats[cip] = naix
            self.sexes[cip] = sex_recode[sex]

    def get_visites(self):
        """."""
        limit = "limit 10000" if TEST else ""
        tbs = (
            ('visi_vc', imp),
            ('visites{}'.format('' if RECOVER else '1'), imp),
            ('visi_vc', imp_jail),
            ('visites', imp_jail),
        )
        self.visites = {}
        sql = """
            SELECT
                id_cip_sec,
                codi_sector,
                visi_servei_codi_servei,
                visi_modul_codi_modul,
                visi_lloc_visita,
                visi_tipus_visita,
                visi_etiqueta,
                visi_data_visita,
                -- date_format(visi_data_visita, '%y%m'),
                visi_up
            FROM {{tb}}
            WHERE
                visi_situacio_visita = 'R' AND
                visi_data_visita BETWEEN DATE '{YR}-01-01' AND DATE '{DEXTD}'
            {limit}
        """.format(YR=DEXTD.year,
                   DEXTD=DEXTD,
                   limit=limit)
        self.cuentas = c.Counter()
        for tb, db in tbs:
            for cip, sec, serv, modul, lloc, tip, tag, vdat, up in u.getAll(
                    sql.format(tb=tb),
                    db):
                if up in self.centres and cip in self.gedats:
                    if self.gedats[cip] <= vdat:
                        edat_k = get_gedat(self.gedats[cip], vdat)
                        sex_k = self.sexes[cip]
                        br = self.centres[up]
                        ym = vdat.strftime('%y%m')
                        periode = "B{}".format(ym)
                        if (sec, serv) in self.map_serveis:
                            servei = self.map_serveis[(sec, serv)]
                            if tip in TIPUS_MAP:  # ['9C', '9R', '9D', '9T', '9E']
                                if tip == '9E' and tag in ETIQUETAS:
                                    if tag == 'ECTA':  # ECONSULTA
                                        tipus = tip + 'C'  # '9EC'
                                    else:
                                        tipus = tip + tag
                                else:
                                    tipus = TIPUS_MAP[tip]
                            elif lloc in ['C', 'D']:
                                tipus = lloc + 'AL'
                            else:
                                tipus = 'ERROR'
                            cuenta = "VIS{}{}".format(servei, tipus)
                            # print(cip, 'VIS' + servei + tipus, up)
                            # self.cuentas[(servei, tipus)] += 1
                            self.cuentas[(cuenta, periode, br, edat_k, sex_k)] += 1
            print(len(self.cuentas))
        upload = []
        for k, v in self.cuentas.items():
            upload.append([k[0], k[1], k[2], KCLI, k[3], KDTL, k[4], KVAL, v])
        u.writeCSV(u.tempFolder + FILE_NAME, upload, sep="{")


if __name__ == '__main__':
    u.printTime('inici')
    Activitat()
    u.printTime('fi')
    try:
        u.sshPutFile(file=FILE_NAME, folder="cargas", host="khalix")
    except Exception as e:
        os.rename(u.tempFolder + FILE_NAME, u.rescueFolder + FILE_NAME)
        print("EXCEPTION: {}, {}, {}".format(FILE_NAME,
                                             u.tempFolder,
                                             u.recueFolder))
        print(str(e))
    u.printTime('ftp')


# SCRATCH
"""
set(['MG', 'EXTRA', 'ALTRE', 'TS', 'ODO', 'PED', 'UAC', 'INF'])

PREDUFFA.sisap_covid_cat_serveis.HOMOL
('ALTRE', 3258) <- VISALSER
('MG', 34) <- VISMF
('INFP', 9) <- VISINF
('INF', 65) <- VISINF
('LLEV', 59) <- VISALT
('PED', 30) <- VISPED
('GIN', 36) <- VISALT
('ODO', 50) <- VISODO
('TS', 43) <- VISTS

Extraccions
Inclou servei d�extraccions (codis: EXTRA, EXT)

Atenci� al ciutad�
Inclou servei d�atenci� al ciutad� (codis: UAC, UAAU)
"""
