/* version: SISAP */
/* FLAGS seg1··seg6 -> null: no toca, 0: vencido y no seguido, 1: vencido y seguido */

use altres;

drop table if exists altres.exp_ecap_aprop_catalegPare;
create table altres.exp_ecap_aprop_catalegPare (
	pare varchar(8) not null default ''
	,literal varchar(300) not null default ''
	,ordre int
    ,pantalla varchar(20))
;

insert into altres.exp_ecap_aprop_catalegPare
 (pare, literal, ordre, pantalla)
values
 ('APROP01','Estudi APROP (captura la fractura)',0, 'ALTRES')
;

drop table if exists altres.exp_ecap_aprop_cataleg;
create table if not exists altres.exp_ecap_aprop_cataleg (
	indicador varchar(8)
	,literal varchar(300)
	, ordre int
	, pare varchar(8)
	, llistat int
	, invers int not null default 0
	, mmin double not null default 0
	, mint double not null default 0
	, mmax double not null default 0
	, toShow int not null default 1
	, wiki varchar(250) not null default ''
	, tipusvalor varchar(5))
;

insert into altres.exp_ecap_aprop_cataleg
 (indicador, literal, ordre, pare, llistat, tipusvalor)
values
 ('APROP101', 'APROP seguiment a les 6 setmanes', 1, 'APROP01', 1, 'PCT'),
 ('APROP102', 'APROP seguiment als 3 mesos', 2, 'APROP01', 1, 'PCT'),
 ('APROP103', 'APROP seguiment als 6 mesos', 3, 'APROP01', 1, 'PCT'),
 ('APROP104', 'APROP seguiment als 12 mesos', 4, 'APROP01', 1, 'PCT'),
 ('APROP105', 'APROP seguiment als 18 mesos', 5, 'APROP01', 1, 'PCT'),
 ('APROP106', 'APROP seguiment als 24 mesos', 6, 'APROP01', 1, 'PCT')
;

DROP TABLE if exists altres.aprop_ctl_seg;
CREATE TABLE if not exists altres.aprop_ctl_seg  (primary key(id_cip_sec)) as
SELECT
	id_cip_sec
	, CASE WHEN TIMESTAMPDIFF(week,  min(au_dat_act), data_ext) >=6	 THEN 0 ELSE null END AS seg1
	, CASE WHEN TIMESTAMPDIFF(month, min(au_dat_act), data_ext) >=3   THEN 0 ELSE null END AS seg2
	, CASE WHEN TIMESTAMPDIFF(month, min(au_dat_act), data_ext) >=6   THEN 0 ELSE null END AS seg3
	, CASE WHEN TIMESTAMPDIFF(month, min(au_dat_act), data_ext) >=12  THEN 0 ELSE null END AS seg4
	, CASE WHEN TIMESTAMPDIFF(month, min(au_dat_act), data_ext) >=18  THEN 0 ELSE null END AS seg5
	, CASE WHEN TIMESTAMPDIFF(month, min(au_dat_act), data_ext) >=24  THEN 0 ELSE null END AS seg6
FROM
	import.activitats2, nodrizas.dextraccio
WHERE
	au_cod_ac = 'XL001' AND au_val=1															-- inclou: accepten participar al APROP
	AND au_up IN (
			'00002','00006','00007','00009','00014','00016','00018','00023','00041','00044',	-- inclou: informat des de centres intervenci�
			'00045','00047','00048','00051','00052','00055','00059','00068','00104','00110',
			'00122','00149','00158','00159','00180','00183','00186','00194','00345','00365',
			'00368','00374','00378','00380','00381','00397','01097','04376','04713',			 
			'00003','00008','00011','00020','00021','00025','00027','00031','00040','00042',	 -- inclou: informat des de centres control
			'00049','00050','00054','00060','00061','00062','00065','00066','00105','00114',
			'00119','00121','00171','00182','00184','00185','00347','00353','00372','00377',
			'00379','00391','00395','00396','04374','05239','06175')
GROUP BY
	id_cip_sec
;


-- exclou: que rebutjin participar (qualsevol moment)
drop temporary table if exists altres.aprop_exclusions;
create temporary table if not exists altres.aprop_exclusions (id_cip_sec int(11), primary key(id_cip_sec)) ENGINE=MEMORY as 
select a.id_cip_sec from altres.aprop_ctl_seg a inner join import.activitats2 v
on a.id_cip_sec = v.id_cip_sec
where au_cod_ac ='XL001' and au_val=0
group by id_cip_sec
;

-- exclou: que tinguin causa de exclusio informada
insert ignore into altres.aprop_exclusions
select a.id_cip_sec from altres.aprop_ctl_seg a inner join  import.variables2 v
on a.id_cip_sec = v.id_cip_sec
where vu_cod_vs in ('XA000B','XA000C')
;

-- aplico exclusions
delete altres.aprop_ctl_seg from altres.aprop_ctl_seg inner join altres.aprop_exclusions
on altres.aprop_ctl_seg.id_cip_sec = altres.aprop_exclusions.id_cip_sec
;

drop temporary table if exists altres.aprop_exclusions;

drop temporary table if exists altres.aprop_seguiments;
create temporary table if not exists altres.aprop_seguiments (id_cip_sec int(11), val int(1) not null, primary key(id_cip_sec, val)) ENGINE=MEMORY;
insert ignore into altres.aprop_seguiments
select id_cip_sec, au_val as val from import.activitats2 where au_cod_ac='XL002' and au_val <> ''
;

update altres.aprop_ctl_seg a inner join altres.aprop_seguiments v
on a.id_cip_sec=v.id_cip_sec 
set a.seg1=1
where a.seg1=0 and v.val=1 
;


update altres.aprop_ctl_seg a inner join altres.aprop_seguiments v
on a.id_cip_sec=v.id_cip_sec 
set a.seg2=1
where a.seg2=0 and v.val=2 
;

update altres.aprop_ctl_seg a inner join altres.aprop_seguiments v
on a.id_cip_sec=v.id_cip_sec 
set a.seg3=1
where a.seg3=0 and v.val=3 
;

update altres.aprop_ctl_seg a inner join altres.aprop_seguiments v
on a.id_cip_sec=v.id_cip_sec 
set a.seg4=1
where a.seg4=0 and v.val=4 
;

update altres.aprop_ctl_seg a inner join altres.aprop_seguiments v
on a.id_cip_sec=v.id_cip_sec 
set a.seg5=1
where a.seg5=0 and v.val=5 
;

update altres.aprop_ctl_seg a inner join altres.aprop_seguiments v
on a.id_cip_sec=v.id_cip_sec 
set a.seg6=1
where a.seg6=0 and v.val=6 
;

drop temporary table if exists altres.aprop_seguiments;


drop table if exists altres.mst_aprop_ctl_seg_uba;
create table if not exists altres.mst_aprop_ctl_seg_uba as
select
	i.id_cip_sec
	,a.up
	,a.uba
	,a.upinf
	,a.ubainf
	,i.seg1, i.seg2, i.seg3, i.seg4, i.seg5, i.seg6
from
	altres.aprop_ctl_seg i
   left join
	nodrizas.assignada_tot a
	on i.id_cip_sec = a.id_cip_sec
;

drop table if exists altres.aprop_ctl_seg;



-- els pacients sense up assignada (no se sap si es control o intervencio) o up distinta a control o intervencio no es pot decidir si li cal seguiment
update altres.mst_aprop_ctl_seg_uba
set seg1 = null, seg2 = null, seg3 = null, seg4 = null, seg5 = null, seg6 = null
where up not in (
			'00002','00006','00007','00009','00014','00016','00018','00023','00041','00044',	-- inclou: informat des de centres intervenci�
			'00045','00047','00048','00051','00052','00055','00059','00068','00104','00110',
			'00122','00149','00158','00159','00180','00183','00186','00194','00345','00365',
			'00368','00374','00378','00380','00381','00397','01097','04376','04713',			 
			'00003','00008','00011','00020','00021','00025','00027','00031','00040','00042',	 -- inclou: informat des de centres control
			'00049','00050','00054','00060','00061','00062','00065','00066','00105','00114',
			'00119','00121','00171','00182','00184','00185','00347','00353','00372','00377',
			'00379','00391','00395','00396','04374','05239','06175')
		or up is null
;


-- seguiments que no calen en el grup control, els ESBORREM: seg1(6w), seg2(3m) i seg5(18m)
update altres.mst_aprop_ctl_seg_uba
set seg1 = null, seg2 = null, seg5 =null
where up in
	('00003','00008','00011','00020','00021','00025','00027','00031','00040','00042',	 -- inclou: informat des de centres control
	'00049','00050','00054','00060','00061','00062','00065','00066','00105','00114',
	'00119','00121','00171','00182','00184','00185','00347','00353','00372','00377',
	'00379','00391','00395','00396','04374','05239','06175')
;

drop temporary table if exists altres.u11_aprop;
create temporary table if not exists altres.u11_aprop (
	id_cip_sec int(11)
	,codi_sector varchar(4) null
	,hash_d varchar(40) not null default ''
	,primary key(id_cip_sec)) ENGINE=MEMORY as 
select
	a.id_cip_sec, codi_sector, hash_d
from
	altres.mst_aprop_ctl_seg_uba a
   inner join
    import.u11 u 
	on a.id_cip_sec = u.id_cip_sec
;


drop table if exists altres.exp_ecap_aprop_pacient;
create table if not exists altres.exp_ecap_aprop_pacient as
select
	i.id_cip_sec
	,i.up
	,i.uba
	,i.upinf
	,i.ubainf
	,'APROP101' as grup_codi
	,0 as exclos
	,u.hash_d
	,u.codi_sector as sector
from
	altres.mst_aprop_ctl_seg_uba i
   inner join
	altres.u11_aprop u
	on i.id_cip_sec = u.id_cip_sec
where
	seg1 = 0
	and i.up is not null
;

insert into altres.exp_ecap_aprop_pacient
select
	i.id_cip_sec
	,i.up
	,i.uba
	,i.upinf
	,i.ubainf
	,'APROP102' as grup_codi
	,0 as exclos
	,u.hash_d
	,u.codi_sector as sector
from
	altres.mst_aprop_ctl_seg_uba i
   inner join
	altres.u11_aprop u
	on i.id_cip_sec = u.id_cip_sec
where
	seg2 = 0
	and i.up is not null
;

insert into altres.exp_ecap_aprop_pacient
select
	i.id_cip_sec
	,i.up
	,i.uba
	,i.upinf
	,i.ubainf
	,'APROP103' as grup_codi
	,0 as exclos
	,u.hash_d
	,u.codi_sector as sector
from
	altres.mst_aprop_ctl_seg_uba i
   inner join
	altres.u11_aprop u
	on i.id_cip_sec = u.id_cip_sec
where
	seg3 = 0
	and i.up is not null
;

insert into altres.exp_ecap_aprop_pacient
select
	i.id_cip_sec
	,i.up
	,i.uba
	,i.upinf
	,i.ubainf
	,'APROP104' as grup_codi
	,0 as exclos
	,u.hash_d
	,u.codi_sector as sector
from
	altres.mst_aprop_ctl_seg_uba i
   inner join
	altres.u11_aprop u
	on i.id_cip_sec = u.id_cip_sec
where
	seg4 = 0
	and i.up is not null
;

insert into altres.exp_ecap_aprop_pacient
select
	i.id_cip_sec
	,i.up
	,i.uba
	,i.upinf
	,i.ubainf
	,'APROP105' as grup_codi
	,0 as exclos
	,u.hash_d
	,u.codi_sector as sector
from
	altres.mst_aprop_ctl_seg_uba i
   inner join
	altres.u11_aprop u
	on i.id_cip_sec = u.id_cip_sec
where
	seg5 = 0
	and i.up is not null
;

insert into altres.exp_ecap_aprop_pacient
select
	i.id_cip_sec
	,i.up
	,i.uba
	,i.upinf
	,i.ubainf
	,'APROP106' as grup_codi
	,0 as exclos
	,u.hash_d
	,u.codi_sector as sector
from
	altres.mst_aprop_ctl_seg_uba i
   inner join
	altres.u11_aprop u
	on i.id_cip_sec = u.id_cip_sec
where
	seg6 = 0
	and i.up is not null
;

drop temporary table if exists altres.u11_aprop;

-- 'indicadores' aprop  por uba
drop table if exists altres.exp_ecap_aprop_uba;
create table if not exists altres.exp_ecap_aprop_uba as
select up, uba, 'M' as tipus, 'APROP101' as indicador 
	,sum(seg1) as numerador
	,count(seg1) as denominador
	,avg(seg1) as resultat
from altres.mst_aprop_ctl_seg_uba
where up is not null
group by up, uba
having avg(seg1) >= 0
union
select up, uba, 'M' as tipus, 'APROP102' as indicador
	,sum(seg2) as numerador
	,count(seg2) as denonminador
	,avg(seg2) as resultat
from altres.mst_aprop_ctl_seg_uba
where up is not null
group by up, uba
 having avg(seg2) >= 0
union
select up, uba, 'M' as tipus, 'APROP103' as indicador
	,sum(seg3) as numerador
	,count(seg3) as denonminador
	,avg(seg3) as resultat
from altres.mst_aprop_ctl_seg_uba
where up is not null
group by up, uba
 having avg(seg3) >= 0
union
select up, uba, 'M' as tipus, 'APROP104' as indicador
	,sum(seg4) as numerador
	,count(seg4) as denonminador
	,avg(seg4) as resultat
from altres.mst_aprop_ctl_seg_uba
where up is not null
group by up, uba
 having avg(seg4) >= 0
union
select up, uba, 'M' as tipus, 'APROP105' as indicador
	,sum(seg5) as numerador
	,count(seg5) as denonminador
	,avg(seg5) as resultat
from altres.mst_aprop_ctl_seg_uba
where up is not null
group by up, uba
 having avg(seg5) >= 0
union
select up, uba, 'M' as tipus, 'APROP106' as indicador
	,sum(seg6) as numerador
	,count(seg6) as denonminador
	,avg(seg6) as resultat
from altres.mst_aprop_ctl_seg_uba
where up is not null
group by up, uba
 having avg(seg6) >= 0
union
select upinf, ubainf, 'I' as tipus, 'APROP101' as indicador 
	,sum(seg1) as numerador
	,count(seg1) as denominador
	,avg(seg1) as resultat
from altres.mst_aprop_ctl_seg_uba
where upinf is not null
group by upinf, ubainf
having avg(seg1) >= 0
union
select upinf, ubainf, 'I' as tipus, 'APROP102' as indicador
	,sum(seg2) as numerador
	,count(seg2) as denonminador
	,avg(seg2) as resultat
from altres.mst_aprop_ctl_seg_uba
where upinf is not null
group by upinf, ubainf
 having avg(seg2) >= 0
union
select upinf, ubainf, 'I' as tipus, 'APROP103' as indicador
	,sum(seg3) as numerador
	,count(seg3) as denonminador
	,avg(seg3) as resultat
from altres.mst_aprop_ctl_seg_uba
where upinf is not null
group by upinf, ubainf
 having avg(seg3) >= 0
union
select upinf, ubainf, 'I' as tipus, 'APROP104' as indicador
	,sum(seg4) as numerador
	,count(seg4) as denonminador
	,avg(seg4) as resultat
from altres.mst_aprop_ctl_seg_uba
where upinf is not null
group by upinf, ubainf
 having avg(seg4) >= 0
union
select upinf, ubainf, 'I' as tipus, 'APROP105' as indicador
	,sum(seg5) as numerador
	,count(seg5) as denonminador
	,avg(seg5) as resultat
from altres.mst_aprop_ctl_seg_uba
where upinf is not null
group by upinf, ubainf
 having avg(seg5) >= 0
union
select upinf, ubainf, 'I' as tipus, 'APROP106' as indicador
	,sum(seg6) as numerador
	,count(seg6) as denonminador
	,avg(seg6) as resultat
from altres.mst_aprop_ctl_seg_uba
where upinf is not null
group by upinf, ubainf
 having avg(seg6) >= 0
;

