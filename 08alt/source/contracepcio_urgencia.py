# coding: latin1

import sisapUtils as u
from os import path

current_year, current_month = u.getOne("select YEAR(data_ext), MONTH(data_ext) from dextraccio", "nodrizas")
YEAR_NAIX_FI = current_year - 1

if current_month == 6 and u.IS_MENSUAL:
    TABLE_O = "anticoncepcio"
    DATABASE_O = "permanent"
    TABLE_D = "sisap_peticio_anticoncepcio"
    DATABASE_D = "redics"
    USERS = ("PREDUPRP", "PREDUMMP", "PREDUMBO")


    cols = ["TOTAL_ACO number" if col == "TOTAL_ACO" else
            u.getColumnInformation(col, TABLE_O, DATABASE_O, desti="ora")["create"]
            for col in u.getTableColumns(TABLE_O, DATABASE_O)]
    cols_noms = tuple(tuple(col.split(" ")[0] for col in cols))
    cols_s = "({})".format(", ".join(cols))
    u.createTable(TABLE_D, cols_s, DATABASE_D, rm=True)
    data = list(u.getAll("select * from {}".format(TABLE_O), DATABASE_O))
    u.listToTable(data, TABLE_D, DATABASE_D)
    u.grantSelect(TABLE_D, USERS, DATABASE_D)

    filename = path.join(u.tempFolder, 'contracepcio_urgencia_{}.csv'.format(YEAR_NAIX_FI))
    u.writeCSV(filename, (cols_noms,) + tuple(sorted(data)), sep=';')

    me = "sisap@gencat.cat"
    to = "g.rodriguez@gencat.cat"
    cc = ["alejandrorivera@gencat.cat", "mquintana.apms.ics@gencat.cat"]
    subject = "Dades anuals d'Anticoncepci� d'Urg�ncia del darrer any {}".format(YEAR_NAIX_FI)
    text = "Bon dia,\n\nCom cada any per aquestes dates us enviem les dades anuals de l'ECAP d'Anticoncepci� d'Urg�ncia del darrer any {}, per a la mem�ria i avaluaci�.\n\nSalutacions:\n\nSISAP".format(YEAR_NAIX_FI)
    u.sendGeneral(me, to, cc, subject, text, filename)
