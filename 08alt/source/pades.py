# -*- coding: utf-8 -*-

import collections as c
import sisapUtils as u
import datetime as d
import sisaptools as su
from dateutil.relativedelta import relativedelta

# DEXTD = su.Database("p2262", 'nodrizas').get_one("select data_ext from dextraccio")[0]
DEXTD = u.getOne("select data_ext from dextraccio", "nodrizas")[0]
menys12 = DEXTD - relativedelta(years=1)
menys24 = DEXTD - relativedelta(years=2)
dies15 = DEXTD - relativedelta(days=15)
PERIOD = "A{}".format(DEXTD.strftime("%y%m"))
class Pades():
    def __init__(self):
        u.printTime('inici')
        self.grups = c.defaultdict(set)
        self.get_cataleg_pades()
        u.printTime('get_cataleg_pades')
        self.get_poblacio()
        u.printTime('get_poblacio')
        self.get_actius()
        u.printTime('get_actius')
        self.get_variables()
        u.printTime('get_variables')
        self.get_pcc_maca()
        u.printTime('get_pcc_maca')
        self.get_prescripcions()
        u.printTime('get_prescripcions')
        self.get_activitats()
        u.printTime('get_activitats')
        self.get_placures()
        u.printTime('get_placures')
        self.get_derivacions()
        u.printTime('get_derivacions')
        self.get_piic()
        u.printTime('get_piic')
        self.get_visites()
        u.printTime('get_visites')
        self.get_indicadors()
        u.printTime('get_indicadors')
        self.create_table()
        u.printTime('create_table')
        self.upload()
        u.printTime('upload')
        self.export()
        u.printTime('export')

    def get_cataleg_pades(self):
        sql = """
            SELECT eap_up, pades_up
            FROM dwsisap.rel_pades_eap
            WHERE usar = 1
        """
        self.cataleg_pades = {up: pades for up, pades in u.getAll(sql, 'exadata')}

    def get_pcc_maca(self):
        sql = """
            SELECT id_cip_sec, es_cod
            FROM estats
            WHERE es_dde <= DATE '{DEXTD}'
        """.format(DEXTD=DEXTD)
        for id, codi in u.getAll(sql, 'import'):
            if id in self.grups['pades_act'] or id in self.grups['pades_exitus']:
                if codi == 'ER0001':
                    self.grups['pcc'].add(id)
                else:
                    self.grups['maca'].add(id)

    def export(self):
        sql = """
            SELECT indicador, periode, concat('PD', up) as entity, analisi, nocat, noimp, dim6set, n, valor
            FROM altres.indicadors_pades
        """
        u.exportKhalix(sql, 'INDPADES')

    def create_table(self):
        cols = "(indicador varchar(10), periode varchar(5), up varchar(5), analisi varchar(3), \
                nocat varchar(7), noimp varchar(5), dim6set varchar(7), n varchar(1), valor int)"
        u.createTable('indicadors_pades', cols, 'altres', rm=True)

        cols = "(indicador varchar(10), cip varchar(15), up varchar(10), num int)"
        u.createTable('pades_llistat', cols, 'altres', rm=True)

    def upload(self):
        llistat_upload = []
        # self.llistat[(ind, PERIOD, up, uba, 'M', 'NUM', edat, 'NOIMP', sexe, 'N')] += 1
        for (ind, periode, up, analisi, edat, noimp, sexe, n), val in self.llistat.items():
            llistat_upload.append((ind, periode, up, analisi, edat, noimp, sexe, n, val))
        u.listToTable(llistat_upload, 'indicadors_pades', 'altres')
        u.listToTable(self.pacients, 'pades_llistat', 'altres')

    def get_indicadors(self):

        inds = {
            'PAD001': {
                'NUM': self.grups['pades'],
                'DEN': self.grups['pob_total']
            },
            'PAD002': {
                'NUM': self.grups['VA0304'],
                'DEN': self.grups['pades']
            },
            'PAD002A': {
                'NUM': self.grups['VA0304_3'],
                'DEN': self.grups['VA0304']
            },
            'PAD002B': {
                'NUM': self.grups['VA0304_2'],
                'DEN': self.grups['VA0304']
            },
            'PAD002C': {
                'NUM': self.grups['VA0304_1'],
                'DEN': self.grups['VA0304']
            },
            'PAD003': {
                'NUM': ((self.grups['Barthel'] & (self.grups['pfeifer'] | self.grups['demencia'] | self.grups['parla']) & self.grups['nutricional'] & self.grups['caigudes'] & (self.grups['emocional'] | self.grups['demencia']) & self.grups['social']) | self.grups['Fragilitat'] | self.grups['valoracio_ped']),
                'DEN': self.grups['pades']
            },
            'PAD004': {
                'NUM': self.grups['VA0303'],
                'DEN': self.grups['maca']
            },
            'PAD005': {
                'NUM': self.grups['PC0004'],
                'DEN': self.grups['pades']
            }, # falta seguiment ultims 3 mesos
            'PAD006': {
                'NUM': self.grups['PC0093'],
                'DEN': self.grups['pades']
            }, # falta seguiment ultims 7 dies
            'PAD007': {
                'NUM': self.grups['IA020'],
                'DEN': self.grups['pades']
            },
            'PAD008': {
                'NUM': self.grups['Barber'],
                'DEN': self.grups['pades']
            },
            'PAD009': {
                'NUM': self.grups['VA2003'],
                'DEN': self.grups['pades']
            },
            'PAD010': {
                'NUM': self.grups['derivacions'],
                'DEN': self.grups['pades']
            },
            'PAD011': {
                'NUM': self.grups['piic'],
                'DEN': self.grups['maca']
            },
            'PAD101': {
                'NUM': self.grups['seguiment'],
                'DEN': self.grups['pades_act']
            },
            'PAD102': {
                'NUM': self.grups['opioides'],
                'DEN': self.grups['pades_act']
            },
            'PAD102A': {
                'NUM': self.grups['laxants'],
                'DEN': self.grups['opioides']
            },
            'PAD103': {
                'NUM': self.grups['domicili'],
                'DEN': self.grups['pades_act']
            }
        }
        self.llistat = c.Counter()
        self.pacients = []
        for ind, params in inds.items():
            num = params['NUM']
            den = params['DEN']
            for id in den:
                if id in self.pob:
                    if ind == 'PAD001':
                        up = self.pob[id]['up']
                        sexe = self.pob[id]['sexe']
                        if up in self.cataleg_pades:
                            in_num = 0
                            self.llistat[(ind, PERIOD, self.cataleg_pades[up], 'DEN', 'NOCAT', 'NOIMP', sexe, 'N')] += 1
                            if id in self.ups:
                                for pades in self.ups[id]:
                                    if id in num:
                                        self.llistat[(ind, PERIOD, pades, 'NUM', 'NOCAT', 'NOIMP', sexe, 'N')] += 1
                                        in_num = 1
                            # self.pacients.append((ind, id, self.cataleg_pades[up], in_num))
                    elif id in self.ups:
                        for up in self.ups[id]:
                            # up = self.pob[id]['up']
                            # edat = self.pob[id]['edat']
                            edat = 'NOCAT'
                            sexe = self.pob[id]['sexe']
                            self.llistat[(ind, PERIOD, up, 'DEN', edat, 'NOIMP', sexe, 'N')] += 1
                            in_num = 0
                            # self.llistat[(ind, PERIOD, upinf, ubainf, 'I', 'DEN', edat, 'NOIMP', sexe, 'N')] += 1
                            if id in num:
                                in_num = 1
                                self.llistat[(ind, PERIOD, up, 'NUM', edat, 'NOIMP', sexe, 'N')] += 1
                            try:
                                if up in ('00235', '00237'):
                                    cip = self.id_cip_to_cip[id]
                                    self.pacients.append((ind, cip, up, in_num))
                            except KeyError:
                                continue
                                # self.llistat[(ind, PERIOD, upinf, ubainf, 'I', 'NUM', edat, 'NOIMP', sexe, 'N')] += 1
            u.printTime(ind)
                
    def get_visites(self):
        # sql = """
        #     SELECT pacient, data,
        #     case when lloc = 'D' then 1 else 0 end as pr1,
        #     case when data >= DATE '{dies15}' and tipus in ('9C','9R')
        #     and situacio = 'R'
        #     and SISAP_CATPROF_CLASS in ('INF','MF','PSICO')
        #     then 1 else 0 end as pr2
        #     FROM dwsisap.sisap_master_visites
        # """.format(dies15=dies15)
        sql_p = """SELECT partition_name from all_tab_partitions
            WHERE table_name = 'SISAP_MASTER_VISITES' AND table_owner = 'DWSISAP'"""

        sql_1 = """
            SELECT pacient, data
            FROM dwsisap.sisap_master_visites
            WHERE data between DATE '{menys12}' and DATE '{DEXTD}'
            and lloc = 'D'
        """.format(menys12=menys12, DEXTD=DEXTD)
        sql_2 = """
            SELECT pacient
            FROM dwsisap.sisap_master_visites
            WHERE data between DATE '{dies15}' and DATE '{DEXTD}'
            and tipus in ('9C','9R','9T','9D')
            and situacio = 'R'
            and SISAP_CATPROF_CLASS in ('INF','MF','PSICO')
        """.format(dies15=dies15,DEXTD=DEXTD)
        # for partition, in u.getAll(sql_p, 'exadata'):
        for hash_d, data in u.getAll(sql_1,'exadata'):
            try:
                id = self.conversor[hash_d]
                if isinstance(data, d.datetime):
                    data=data.date()
                if isinstance(self.altes[id], d.datetime):
                    self.altes[id] = self.altes[id].date()
                if (data-self.altes[id]).days <= 7:
                    self.grups['domicili'].add(id)
                # if (DEXTD-data.date()).days <= 7:
                    # self.grups['domicili'].add(hash_d)
            except KeyError:
                continue
        u.printTime('despres sql_1')
        u.printTime(str(len(self.grups['domicili'])))
        for hash_d, in u.getAll(sql_2,'exadata'):
            try:
                self.grups['seguiment'].add(self.conversor[hash_d])
                # self.grups['seguiment'].add(hash_d)
            except:
                continue
        u.printTime('despres sql_2')
        u.printTime(str(len(self.grups['seguiment'])))
        # visites = c.defaultdict(list)
        # for hash_d, data, lloc, seguiment in u.getAll(sql, 'exadata'):
        #     visites[hash_d].append((data,lloc,seguiment))
        # u.printTime('despres sql visites')
        # for hash_d in visites:
        #     if hash_d in self.conversor and self.conversor[hash_d] in self.altes:
        #         for (data, lloc, seguiment) in visites[hash_d]:
        #             if lloc == 1 and (data-self.altes[self.conversor[hash_d]]).days <= 7:
        #                 self.grups['domicili'].add(self.conversor[hash_d])
        #             if seguiment == 1:
        #                 self.grups['seguiment'].add(self.conversor[hash_d])

    def get_piic(self):
        sql = """
            SELECT id_cip_sec, left(mi_cod_var, 8), mi_cod_var, mi_data_reg
            FROM piic
            WHERE mi_data_reg between DATE '{menys24}' and DATE '{DEXTD}'
            AND left(mi_cod_var, 8) in ('T4101017','T4101014', 'T4101015', 'T4101001', 'T4101002')
        """.format(menys24=menys24, DEXTD=DEXTD)
        piirV = c.defaultdict(set)
        piirT = c.defaultdict(set)
        piirF = c.defaultdict(set)
        c_2025 = c.defaultdict(dict)
        # piic = set()
        maca = set()
        for id, codi, var, data in u.getAll(sql, 'import'):
            entra = self.complidor(id, data)
            if entra:
                if codi == 'T4101014':
                    piirV[id].add(var[-2:])
                    if var[-1] == '5':
                        c_2025['c2'][id] = 1
                    elif var[-1] == '7':
                        c_2025['c3'][id] = 1 
                elif codi == 'T4101015':
                    piirT[id].add(var[-2:])
                elif codi == 'T4101017':
                    piirF[id].add(var[-2:])
                elif codi == 'T4101002':
                    maca.add(id)
                else:
                    self.grups['piic'].add(id)
        for id in piirV:
            if id in piirT:
                for var in piirV[id]:
                    if var in piirT[id]:
                        self.grups['piic'].add(id)
        for id in piirF:
            if len(piirF[id]) == 4:
                self.grups['piic'].add(id)
            if len(piirF[id]) >= 3: # compleix criteri 1 del 2025
                if '05' in piirF[id] and '06' in piirF[id] and '07' in piirF[id] and id in c_2025['c2'] and id in c_2025['c3']:
                    self.grups['piic'].add(id)

        for id in list(self.grups['piic']):
            if id not in maca and id in self.grups['maca']:
                self.grups['piic'].remove(id)

    def get_derivacions(self):
        sql = """
            SELECT hash, oc_data
            FROM DWSISAP.MASTER_DERIVACIONS
            WHERE FLAG_PETICIO_SOLLICITADA = 1
            AND FLAG_PETICIO_ANULLADA = 0
            AND INF_SERVEI_D_CODI in ('CONV', 'EAIA', 'HDIA', 'LEST', 'PAL', '05999')
            AND oc_data between DATE '{menys24}' and DATE '{DEXTD}'
        """.format(menys24=menys24, DEXTD=DEXTD)
        for hash, data in u.getAll(sql, 'exadata'):
            try:
                id = self.conversor[hash]
                entra = self.complidor(id, data)
                if entra:
                    self.grups['derivacions'].add(self.conversor[hash])
            except KeyError:
                continue

    def complidor_placures(self, id, data1, data2):
        if isinstance(data1, d.datetime):
            data1 = data1.date()
        if isinstance(data2, d.datetime):
            data2 = data2.date()
        if id in self.grups['pades_exitus']:
            defuncio = self.data_defuncio[id]
            def3 = defuncio - relativedelta(months=3)
            if data1 >= def3:
                return True
            elif data2 and data2 >= def3:
                return True
        elif id in self.grups['pades']:
            if id in (6567393, 7376974, 2517390):
                print(str(id) + ' pades')
            menys3 = DEXTD - relativedelta(months=3)
            if data1 >= menys3:
                if id in (6567393, 7376974, 2517390):
                    print(str(id) + ' data1')
                return True
            elif data2 and data2 >= menys3:
                if id in (6567393, 7376974, 2517390):
                    print(str(id) + ' data2')
                return True
        return False

    def get_placures(self):
        sql = """
            SELECT hash_d, pc_codi, data_inici, pc_ultima_intervencio_data
            FROM dwsisap.master_placures
            WHERE pc_codi in ('PC0004', 'PC0093')
        """
        for hash_d, codi, data1, data2 in u.getAll(sql, 'exadata'):
            try:
                id = self.conversor[self.hashes_inv[hash_d]]
                entra = self.complidor_placures(id, data1, data2)
                if id in (6567393, 7376974, 2517390):
                    print(str(id) + ' ' + codi)
                if entra:
                    self.grups[codi].add(id)
                    if id in (6567393, 7376974, 2517390):
                        print(str(id) + ' ' + codi)
            except KeyError:
                continue
        
    def get_activitats(self):
        sql = """
            SELECT id_cip_sec, au_cod_ac, au_dat_act
            FROM activitats2
            WHERE au_cod_ac in ('IA020')
        """
        for id, codi, data in u.getAll(sql, 'import'):
            entra = self.complidor(id, data)
            if entra:
                self.grups[codi].add(id)
        
        GRUPS = {
            89: 'Barber',
            918: 'Fragilitat',
            88: 'pfeifer',
            999: 'nutricional',
            751: 'caigudes',
            940: 'caigudes',
            1079: 'Barthel'
        }

        sql = """
            SELECT id_cip_sec, data_var, agrupador
            FROM eqa_variables
            WHERE agrupador in {codis}
        """.format(codis=tuple(GRUPS.keys()))
        for id, data, codi in u.getAll(sql, 'nodrizas'):
            entra = self.complidor(id, data)
            if entra:
                if id in self.pob:
                    edat = self.pob[id]['edat']
                    if edat < 15 and codi == 89:
                        self.grups['valoracio_ped'].add(id)
                    elif edat >= 15:
                        self.grups[GRUPS[codi]].add(id)
                # self.grups[GRUPS[codi]].add(id)
        
    def get_prescripcions(self):
        codis = [762,1021,1022]
        sql = """
            SELECT id_cip_sec, farmac
            FROM eqa_tractaments
            WHERE farmac in {codis} and data_fi > DATE '{DEXTD}'
        """.format(codis=tuple(codis), DEXTD=DEXTD)
        for id, codi in u.getAll(sql, 'nodrizas'):
            if id in self.grups['pades_act']:
                if codi in (762,1021):
                    self.grups['opioides'].add(id)
                else:
                    self.grups['laxants'].add(id)
    
    def complidor(self, id, data):
        if isinstance(data, d.datetime):
            data = data.date()
        if id in self.grups['pades_exitus']:
            defuncio = self.data_defuncio[id]
            def12 = defuncio - relativedelta(years=1)
            if data >= def12:
                return True
        elif id in self.grups['pades']:
            if data >= menys12:
                return True
        return False

    def get_variables(self):
        va2003 = set(['VA2001', 'VA2002', 'VA2003', 'VD1001', 'VD3001', 'VN1001', 'VN1002', 'VN8100', 'VP511', 'VP521', 'VR1001', 'VMEDG', 'EP5105', 'TIRS', 'VZ3005', 'VA030I'])
        sql = """
            SELECT id_cip_sec, vu_cod_vs, vu_val, vu_dat_act
            FROM variables2
            WHERE vu_cod_vs in ('VA0304','VA0303')
            OR vu_cod_vs in {codis}
        """.format(codis=tuple(va2003))
        last_va0304 = c.defaultdict(set)
        for id, codi, val, data in u.getAll(sql, 'import'):
            entra = self.complidor(id, data)
            if entra:
                if codi == 'VA0304':
                    last_va0304[id].add((val, data))
                    self.grups['VA0304'].add(id)
                    # if val == 1:
                    #     self.grups['VA0304_1'].add(id)
                    # elif val == 2:
                    #     self.grups['VA0304_2'].add(id)
                    # elif val == 3:
                    #     self.grups['VA0304_3'].add(id)
                elif codi == 'VA0303':
                    if val in (1,2,3):
                        self.grups['VA0303'].add(id)
                elif codi in va2003:
                    self.grups['VA2003'].add(id)
                elif codi in ('VMEDG', 'EP5105'):
                    self.grups['emocional'].add(id)
                elif codi in ('TIRS', 'VZ3005'):
                    self.grups['social'].add(id)
                elif codi == 'VA030I':
                    self.grups['Fragilitat'].add(id)
                else:
                    self.grups[codi].add(id)
        
        for id in last_va0304:
            datamax = None
            lastval = None
            for (val, data) in last_va0304[id]:
                if (datamax is not None and data > datamax) or datamax is None:
                    datamax = data
                    lastval = val
            if lastval == 1:
                self.grups['VA0304_1'].add(id)
            elif lastval == 2:
                self.grups['VA0304_2'].add(id)
            elif lastval == 3:
                self.grups['VA0304_3'].add(id)

        sql = """
            SELECT id_cip_sec, ps, dde
            FROM eqa_problemes
            WHERE ps in (89, 783)
        """
        for id, codi, data in u.getAll(sql, 'nodrizas'):
            entra = self.complidor(id, data)
            if entra:
                if id in self.pob and self.pob[id]['edat'] >= 15:
                    if codi == 89:
                        self.grups['demencia'].add(id)
                    else:
                        self.grups['parla'].add(id)

    def get_actius(self):
        # sql = """
        #     SELECT id_cip_sec, dde
        #     FROM eqa_problemes
        #     WHERE ps = 831
        #     and dde between DATE '{menys12}'
        #     and DATE '{DEXTD}'
        # """.format(menys12=menys12,DEXTD=DEXTD)
        # self.altes = c.defaultdict()
        # for id, dde in u.getAll(sql, 'nodrizas'):
        #     self.grups['pades'].add(id)
        #     self.altes[id] = dde
        print(str(len(self.conversor)))
        u.printTime('self.conversor')
        self.altes = c.defaultdict()
        sql = """
            SELECT id_cip_sec, apu_data_alta
            FROM pades
            WHERE APU_PAT_CODI = 'PADES'
        """
        for id, data in u.getAll(sql, 'import'):
            if id in self.grups['pades'] and id in self.vius:
                self.grups['pades_act'].add(id)
                self.altes[id] = data
        print(str(len(self.altes)))
        u.printTime('self.altes')
        u.printTime('pades_act ' + str(len(self.grups['pades_act'])))

    def get_poblacio(self):
        # sql = """
        #     SELECT id_cip_sec, up, khalix, sexe, maca, pcc
        #     FROM assignada_tot a
        #     INNER JOIN khx_edats5a b
        #     ON a.edat = b.edat
        # """

        sql = """
            SELECT up_cod
            FROM dwsisap.dbc_rup
            WHERE subtip_des = 'PADES'
        """
        ups_pades = set()
        # ups_pades.add('00235')
        # ups_pades.add('00237')
        for up, in u.getAll(sql, 'exadata'):
            ups_pades.add(up)
        u.printTime('ups_pades')
        sql = """
            SELECT id_cip_sec, hash_d
            FROM u11
        """
        self.id_cip_to_hash = {}
        for id, hash in u.getAll(sql, 'import'):
            self.id_cip_to_hash[hash] = id
        u.printTime('u11')
        sql = """   
            SELECT hash_redics, hash_covid, substr(cip, 0, 13)
            FROM dwsisap.pdptb101_relacio_nia
        """
        self.hashes = {}
        self.hashes_inv = {}
        self.conversor = {}
        self.cip_hash = {}
        self.id_cip_to_cip = {}
        self.ups = c.defaultdict(set)
        for redics, covid, cip in u.getAll(sql, 'exadata'):
            try:
                self.hashes[covid] = redics
                self.hashes_inv[redics] = covid
                self.conversor[covid] = self.id_cip_to_hash[redics]
                self.cip_hash[cip] = self.id_cip_to_hash[redics]
                self.id_cip_to_cip[self.id_cip_to_hash[redics]] = cip
            except KeyError:
                continue
        u.printTime('pdptb101')
        sql = """
            SELECT pacient, up
            FROM dwsisap.sisap_master_visites
            WHERE data >= DATE '{menys12}'
            AND up in {ups}
            AND tipus = '9D'
        """.format(menys12=menys12, ups=tuple(list(ups_pades)))
        visites = set()
        for hash, up in u.getAll(sql, 'exadata'):
            try:
                id = self.id_cip_to_hash[self.hashes[hash]]
                visites.add(id)
                self.ups[id].add(up)
            except KeyError:
                continue
        u.printTime('visites')
        sql = """
            SELECT id_cip_sec
            FROM eqa_problemes
            WHERE ps = 969
            AND dde <= DATE '{DEXTD}'
        """.format(DEXTD=DEXTD)
        excl = set()
        for id, in u.getAll(sql, 'nodrizas'):
            excl.add(id)
        u.printTime('problemes')
        for id in visites:
            if id not in excl:
                self.grups['pades'].add(id)
                if id in (7353657, 8239117, 6552482):
                    print(str(id))
        print('pades ' + str(len(self.grups['pades'])))
        sql = """
            SELECT id_cip_sec, usua_data_situacio
            FROM assignada
            WHERE usua_situacio = 'D'
            AND usua_data_situacio >= DATE '{menys12}'
        """.format(menys12=menys12)
        self.data_defuncio = {}
        for id, data in u.getAll(sql, 'import'):
            if id in self.grups['pades']:
                self.grups['pades_exitus'].add(id)
                self.data_defuncio[id] = data
            self.grups['pob_total'].add(id)
        u.printTime('pades_exitus ' + str(len(self.grups['pades_exitus'])))
        sql = """
            SELECT hash, rca_up, sexe, edat
            FROM dwsisap.dbc_poblacio
            WHERE situacio = 'D'
        """
        self.pob = c.defaultdict()
        for covid, up, sexe, edat in u.getAll(sql, 'exadata'):
            try:
                id = self.conversor[covid]
                if id in self.grups['pob_total']:
                    sexe_final = 'HOME' if sexe == 'H' else 'DONA'
                    self.pob[id] = {
                        'up': up,
                        'sexe': sexe_final,
                        'edat': edat
                    }
            except KeyError:
                continue

        sql = """
            SELECT id_cip_sec, up, sexe, edat, ates
            FROM assignada_tot
        """
        # self.pob = c.defaultdict()
        self.vius = set()
        for id, up, sexe, edat, ates in u.getAll(sql, 'nodrizas'):
            sexe_final = 'HOME' if sexe == 'H' else 'DONA'
            self.pob[id] = {
                'up': up,
                'sexe': sexe_final,
                'edat': edat
            }
            if ates:
                self.grups['pob_total'].add(id)
            self.vius.add(id)
        u.printTime('pob_total ' + str(len(self.grups['pob_total'])))
    
    # def get_conversor(self):
    #     # sql = "select id_cip_sec, hash_d from u11"
    #     self.conversor = c.defaultdict()
    #     # conv = c.defaultdict()
    #     # for id, hash_d in u.getAll(sql, 'import'):
    #     #     if id in self.pob:
    #     #         conv[hash_d] = id

    #     sql = "select hash_redics, hash_covid from dwsisap.pdptb101_relacio"
    #     for hash_r, hash_c in u.getAll(sql, 'exadata'):
    #         try:
    #             self.conversor[hash_c] = self.id_cip_to_hash[hash_r]
    #         except:
    #             continue
    #     print(str(len(self.conversor)))
    #     u.printTime('conversor')
Pades()
# u.printTime('inici')
# sql = """
#     SELECT up_cod
#     FROM dwsisap.dbc_rup
#     WHERE subtip_des = 'PADES'
# """
# ups = set()
# for up, in u.getAll(sql, 'exadata'):
#     ups.add(up)
# u.printTime('pades')

# sql = """
#     SELECT hash, rca_up
#     FROM dwsisap.dbc_poblacio
# """
# assignacio = {}
# for hash, up in u.getAll(sql, 'exadata'):
#     assignacio[hash] = up
# u.printTime('assignacio')

# sql = """
#     SELECT pacient, up
#     FROM dwsisap.sisap_master_visites
#     WHERE data >= DATE '2024-01-01'
#     -- AND data <= DATE '2023-12-31'
#     AND up in {ups}
# """.format(ups=tuple(list(ups)))
# res = c.defaultdict(set)
# for id, up in u.getAll(sql, 'exadata'):
#     if id in assignacio:
#         res[assignacio[id]].add(id)
# u.printTime('visites')

# hash_cip = {}
# sql = """
#     SELECT cip, hash
#     FROM dwsisap.rca_cip_nia
# """
# for cip, hash in u.getAll(sql, 'exadata'):
#     hash_cip[hash] = cip
# u.printTime('hash_cip')

# upload = []
# up = '00154'
# if up in res:
#     for id in res[up]:
#         if id in hash_cip:
#             upload.append((up, hash_cip[id]))
#     # upload.append((up, len(res[up])))
# u.printTime('tractament')
# cols = "(up varchar(10), cip varchar(14))"
# u.createTable('validacio_pades', cols, 'altres', rm=True)
# u.listToTable(upload, 'validacio_pades', 'altres')
# u.printTime('fi')