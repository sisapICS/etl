# -*- coding: utf-8 -*-

import sisapUtils as u
import collections as c


ESPECIALISTES_INTERES = {
    '10104': 
        {'ps': 18,
        'def': 'ENDOCRI'}, # hipotiroidisme 
    '10098': 
        {'ps': 951,
        'def': 'TRAUMA'}, # total, no codis
    '10096': 
        {'ps': 950,
        'def': 'UROLOGIA'}, # total, no codis + aquest 950
    # derma total
    '10103':
        {'def': 'DERMA'},
    # oftal total
    '10110':
        {'def': 'OFTALMOLOGIA'},
    # otorrino total
    '10111':
        {'def': 'OTORRINO'},
    # cardio total
    '10131':
        {'def': 'CARDIO'}
}

INDICADORS = {
        ('10104', 'pri', '')  :  'P_POB_ENDO_P',
        ('10104', 'esp', '')  :  'P_POB_ENDO_A',
        ('10098', 'pri', '')  :  'P_POB_TRAUMA_P',
        ('10098', 'esp', '')  :  'P_POB_TRAUMA_A',
        ('10096', 'pri', '')  :  'P_POB_URO_P',
        ('10096', 'esp', '')  :  'P_POB_URO_A',
        ('10103', 'pri', '')  :  'P_POB_DERMA_P',
        ('10103', 'esp', '')  :  'P_POB_DERMA_A',
        ('10110', 'pri', '')  :  'P_POB_OFTAL_P',
        ('10110', 'esp', '')  :  'P_POB_OFTAL_A',
        ('10111', 'pri', '')  :  'P_POB_OTORRINO_P',
        ('10111', 'esp', '')  :  'P_POB_OTORRINO_A',
        ('10131', 'pri', '')  :  'P_POB_CARDIO_P',
        ('10131', 'esp', '')  :  'P_POB_CARDIO_A',
        ('10104', 'pri', 'dm2')  :  'P_MAL_DM2_P',
        ('10104', 'esp', 'dm2')  :  'P_MAL_DM2_A',
        ('10104', 'pri', 'hipo')  :  'P_MAL_HIPO_P',
        ('10104', 'esp', 'hipo')  :  'P_MAL_HIPO_A',
}

ESPECIALISTES = ('10104', '10098', '10096', '10103', '10110', '10111', '10131')
QUI_DERIVA = ('pri', 'esp')


class EXTERNES_1():
    def __init__(self):
        self.create_tables()
        self.get_dextraccio()
        self.get_externes()
        self.get_assignada()
        self.taxes_poblacionals()
        self.taxes_malaltia()
        self.to_khalix()
    
    def create_tables(self):
        print('create_tables')
        self.table = 'resolucio_mapa'
        self.table_mencia = 'validacio_resolucio_mapa_mencia'
        self.table_souhel = 'validacio_resolucio_mapa_souhel'
        self.db = 'altres'
        cols = '(indicador varchar(100), tipus_indi varchar(15), especialista varchar(10), qui_deriva varchar(10), malaltia varchar(10), pob varchar(10), up varchar(10), uba varchar(10), edat varchar(10), sexe varchar(10), tipus_prof varchar(10), analisi varchar(10), n int)'
        u.createTable(self.table, cols, self.db, rm=True)
        cols = '(indicador varchar(50), especialista varchar(50), qui_deriva varchar(50), malaltia varchar(50), pacient varchar(50), edat varchar(50), sexe varchar(50))'
        u.createTable(self.table_mencia, cols, self.db, rm=True)
        u.createTable(self.table_souhel, cols, self.db, rm=True)

    def get_dextraccio(self):
        print('dextraccio')
        dext, = u.getOne('select data_ext from nodrizas.dextraccio', 'nodrizas')
        day = str(dext.day) if len(str(dext.day)) == 2 else '0'+str(dext.day)
        month = str(dext.month) if len(str(dext.month)) == 2 else '0'+str(dext.month)
        year = str(dext.year)
        self.dat_str = year + '-' + month + '-' + day
    
    def get_externes(self):
        print('externes')
        especialitats = tuple(ESPECIALISTES_INTERES.keys())
        self.visitats = c.defaultdict(set)
        # ho fem de mes a mes
        # for n in (0,1,2,3,4,5,6,7,8,9,10,11):
        #     print(n)
        #     sql = """SELECT 
        #                 a.hash_redics, 
        #                 c.AEA_VVI_ESASS,
        #                 c.AEA_VVI_DVISI,
        #                 c.AEA_VRC_PRASS
        #             FROM 
        #                 DWCATSALUT.CMBD_AEA c INNER JOIN dwsisap.RCA_CIP_NIA b
        #                 ON c.nia = b.nia 
        #                 INNER JOIN DWSISAP.PDPTB101_RELACIO a
        #                 ON a.hash_covid = b.hash 
        #             WHERE 
        #                 c.AEA_VVI_ESASS in {}
        #                 AND (MONTHS_BETWEEN(to_date('{}', 'YYYY-MM-DD'),c.AEA_VVI_DVISI) <= {}
        #                     and MONTHS_BETWEEN(to_date('{}', 'YYYY-MM-DD'),c.AEA_VVI_DVISI) <= {})""".format(especialitats, self.dat_str, n, self.dat_str, n+1)
        #             # si volguessim nom�s prim�ria AND c.AEA_VRC_PRASS='1'
        sql = """SELECT 
                    a.hash_redics, 
                    c.AEA_VVI_ESASS,
                    c.AEA_VVI_DVISI,
                    c.AEA_VRC_PRASS
                FROM 
                    DWCATSALUT.CMBD_AEA c INNER JOIN dwsisap.RCA_CIP_NIA b
                    ON c.nia = b.nia 
                    INNER JOIN DWSISAP.PDPTB101_RELACIO a
                    ON a.hash_covid = b.hash 
                WHERE 
                    c.AEA_VVI_ESASS in {}
                    AND MONTHS_BETWEEN(to_date('{}', 'YYYY-MM-DD'),c.AEA_VVI_DVISI) <= 12""".format(especialitats, self.dat_str)
                # si volguessim nom�s prim�ria AND c.AEA_VRC_PRASS='1'    
        for hash_r, especialista, data_visita, qui_deriva in u.getAll(sql, 'exadata'):
            if qui_deriva == '1': 
                qui_deriva = 'pri'
                self.visitats[(especialista, qui_deriva)].add(hash_r)
                try:
                    if hash_r in self.visitats[(especialista, 'esp')]:
                        self.visitats[(especialista, 'esp')].pop(hash_r)
                except:
                    pass
            else: 
                qui_deriva = 'esp'
                try:
                    if hash_r not in self.visitats[(especialista, 'pri')]:
                        self.visitats[(especialista, qui_deriva)].add(hash_r)
                except:
                    pass

        for especialista_d in self.visitats:
            print(especialista_d, len(self.visitats[especialista_d]))
    
    def get_assignada(self):
        print('assignada')
        sql = """SELECT
                    c_cip,
                    c_up,
                    c_metge,
                    c_infermera,
                    c_edat_anys,
                    C_SEXE,
                    CASE
                        WHEN MONTHS_BETWEEN(CURRENT_DATE , C_ULTIMA_VISITA_EAP) <= 12 THEN 1
                        ELSE 0
                    END ates,
                    CASE WHEN PS_DIABETIS2_DATA IS NOT null THEN 1 ELSE 0 END diabetis,
	                CASE WHEN PS_HIPOTIROIDISME_DATA IS NOT null THEN 1 ELSE 0 END hipotiroidisme,
                    CASE WHEN C_INSTITUCIONALITZAT IS NULL or C_INSTITUCIONALITZAT = '0' THEN 0 ELSE 1 END insti
                FROM
                    dbs"""
        self.poblacio = {}
        self.diabetics = set()
        self.hipotoroidisme = set()
        for id, up, uba, ubainf, edat, sexe, ates, dm2, hipo, insti in u.getAll(sql, 'redics'):
            sexe = u.sexConverter(sexe)
            edat = u.ageConverter(edat)
            self.poblacio[id] = [up, uba, up, ubainf, edat, sexe, ates, insti]
            if dm2 == 1: self.diabetics.add(id)
            if hipo == 1: self.hipotoroidisme.add(id)
        print(len(self.diabetics))
        print(len(self.hipotoroidisme))

    def taxes_poblacionals(self):
        print('taxes_poblacionals')
        """visites a l\'especialitat/poblacions LV (4), 12 mesos"""
        # poblacions LV
        # insat: institucipnalitzat + ates
        # noinsat: no institucionalitxat ates
        # insass: institu + assignats
        # noinsass: no insti + assignat
        self.poblacions_LV = c.Counter()
        for id, (up, uba, upinf, ubainf, edat, sexe, ates, insti) in self.poblacio.items():
            if insti == 1 and ates == 1:
                self.poblacions_LV[('INSAT', up, uba, edat, sexe, 'M')] += 1
                self.poblacions_LV[('INSAT', upinf, ubainf, edat, sexe, 'I')] += 1
            if insti == 1 and ates == 0:
                self.poblacions_LV[('INSASS', up, uba, edat, sexe, 'M')] += 1
                self.poblacions_LV[('INSASS', upinf, ubainf, edat, sexe, 'I')] += 1
            if insti == 0 and ates == 1:
                self.poblacions_LV[('NOINSAT', up, uba, edat, sexe, 'M')] += 1
                self.poblacions_LV[('NOINSAT', upinf, ubainf, edat, sexe, 'I')] += 1
            if insti == 0 and ates == 0:
                self.poblacions_LV[('NOINSASS', up, uba, edat, sexe, 'M')] += 1
                self.poblacions_LV[('NOINSASS', upinf, ubainf, edat, sexe, 'I')] += 1
        self.visites_especialistes_num = c.defaultdict(c.Counter)
        self.visites_especialistes_num_malaltia = c.defaultdict(c.Counter)
        especialistes = set()
        self.souhel = set()
        self.mencia = set()
        for especialista, pacients in self.visitats.items(): 
            especialistes.add(especialista)
            for p in pacients:
                if p in self.poblacio:
                    up, uba, upinf, ubainf, edat, sexe, ates, insti = self.poblacio[p]
                    indicador = INDICADORS[(especialista[0], especialista[1], '')]
                    if up == '00441' and uba == '4T': self.souhel.add((indicador, especialista[0], especialista[1], '', p, edat, sexe))
                    elif up == '00295' and ubainf == 'IM13': self.mencia.add((indicador, especialista[0], especialista[1], '', p, edat, sexe))
                    if insti == 1 and ates == 1:
                        self.visites_especialistes_num[especialista][('INSAT', up, uba, edat, sexe, 'M')] += 1
                        self.visites_especialistes_num[especialista][('INSAT', upinf, ubainf, edat, sexe, 'I')] += 1
                    if insti == 1 and ates == 0:
                        self.visites_especialistes_num[especialista][('INSASS', up, uba, edat, sexe, 'M')] += 1
                        self.visites_especialistes_num[especialista][('INSASS', upinf, ubainf, edat, sexe, 'I')] += 1
                    if insti == 0 and ates == 1:
                        self.visites_especialistes_num[especialista][('NOINSAT', up, uba, edat, sexe, 'M')] += 1
                        self.visites_especialistes_num[especialista][('NOINSAT', upinf, ubainf, edat, sexe, 'I')] += 1
                    if insti == 0 and ates == 0:
                        self.visites_especialistes_num[especialista][('NOINSASS', up, uba, edat, sexe, 'M')] += 1
                        self.visites_especialistes_num[especialista][('NOINSASS', upinf, ubainf, edat, sexe, 'I')] += 1
                    ps = []
                    if especialista[0] == '10104':
                        if p in self.diabetics: ps.append('dm2')
                        if p in self.hipotoroidisme: ps.append('hipo')
                        for psi in ps:
                            indicador = INDICADORS[(especialista[0], especialista[1], psi)]
                            if up == '00441' and uba == '4T': self.souhel.add((indicador, especialista[0], especialista[1], psi, p, edat, sexe))
                            elif up == '00295' and ubainf == 'IM13': self.mencia.add((indicador, especialista[0], especialista[1], psi, p, edat, sexe))
                            if insti == 1 and ates == 1:
                                self.visites_especialistes_num_malaltia[especialista][('INSAT', up, uba, edat, sexe, 'M', psi)] += 1
                                self.visites_especialistes_num_malaltia[especialista][('INSAT', upinf, ubainf, edat, sexe, 'I', psi)] += 1
                            if insti == 1 and ates == 0:
                                self.visites_especialistes_num_malaltia[especialista][('INSASS', up, uba, edat, sexe, 'M', psi)] += 1
                                self.visites_especialistes_num_malaltia[especialista][('INSASS', upinf, ubainf, edat, sexe, 'I', psi)] += 1
                            if insti == 0 and ates == 1:
                                self.visites_especialistes_num_malaltia[especialista][('NOINSAT', up, uba, edat, sexe, 'M', psi)] += 1
                                self.visites_especialistes_num_malaltia[especialista][('NOINSAT', upinf, ubainf, edat, sexe, 'I', psi)] += 1
                            if insti == 0 and ates == 0:
                                self.visites_especialistes_num_malaltia[especialista][('NOINSASS', up, uba, edat, sexe, 'M', psi)] += 1
                                self.visites_especialistes_num_malaltia[especialista][('NOINSASS', upinf, ubainf, edat, sexe, 'I', psi)] += 1
        upload = []
        for especialista_t, poblacio in self.visites_especialistes_num.items():
            # especialista = ESPECIALISTES_INTERES[especialista]['def']
            especialista, qui_deriva = especialista_t
            for key, num in poblacio.items():
                pob, upinf, ubainf, edat, sexe, tipus = key
                indicador = INDICADORS[(especialista, qui_deriva, '')]
                upload.append((indicador, 'PCT_POB', especialista, qui_deriva, '', pob, upinf, ubainf, edat, sexe, tipus, 'NUM', num))
        
        for key, den in self.poblacions_LV.items():
            pob, upinf, ubainf, edat, sexe, tipus = key
            for especialista in ESPECIALISTES:
                for qui_deriva in QUI_DERIVA:
                    indicador = INDICADORS[(especialista, qui_deriva, '')]
                    upload.append((indicador, 'PCT_POB', especialista, qui_deriva, '', pob, upinf, ubainf, edat, sexe, tipus, 'DEN', den))

        u.listToTable(upload, self.table, self.db)
        uploadmencia, uploadsouhel = [], []
        for e in self.mencia:
            uploadmencia.append(e)
        for e in self.souhel:
            uploadsouhel.append(e)
        # descomentar per quan validem!
        # u.listToTable(uploadmencia, self.table_mencia, self.db)
        # u.listToTable(uploadsouhel, self.table_souhel, self.db)

    def taxes_malaltia(self):
        print('taxes_malaltia')
        """visites a l\'especialitat/poblaci� LV amb patologia"""
        self.poblacions_LV_pat = c.Counter()
        for id, (up, uba, upinf, ubainf, edat, sexe, ates, insti) in self.poblacio.items():
            ps = []
            if id in self.diabetics: ps.append('dm2')
            if id in self.hipotoroidisme: ps.append('hipo')
            if insti == 1 and ates == 1:
                for psi in ps:
                    self.poblacions_LV_pat[('INSAT', up, uba, edat, sexe, 'M', psi)] += 1
                    self.poblacions_LV_pat[('INSAT', upinf, ubainf, edat, sexe, 'I', psi)] += 1
            if insti == 1 and ates == 0:
                for psi in ps:
                    self.poblacions_LV_pat[('INSASS', up, uba, edat, sexe, 'M', psi)] += 1
                    self.poblacions_LV_pat[('INSASS', upinf, ubainf, edat, sexe, 'I', psi)] += 1
            if insti == 0 and ates == 1:
                for psi in ps:
                    self.poblacions_LV_pat[('NOINSAT', up, uba, edat, sexe, 'M', psi)] += 1
                    self.poblacions_LV_pat[('NOINSAT', upinf, ubainf, edat, sexe, 'I', psi)] += 1
            if insti == 0 and ates == 0:
                for psi in ps:
                    self.poblacions_LV_pat[('NOINSASS', up, uba, edat, sexe, 'M', psi)] += 1
                    self.poblacions_LV_pat[('NOINSASS', upinf, ubainf, edat, sexe, 'I', psi)] += 1
        upload = []
        done = c.defaultdict(set)
        for especialista, poblacio in self.visites_especialistes_num_malaltia.items():
            especialista, qui_deriva = especialista
            for key, num in poblacio.items():
                if key[6] != '':
                    pob, upinf, ubainf, edat, sexe, tipus, psi = key
                    indicador = INDICADORS[(especialista, qui_deriva, psi)]
                    upload.append((indicador, 'PCT_MALALTIA', especialista, qui_deriva, psi, pob, upinf, ubainf, edat, sexe, tipus, 'NUM', num))
                    if (pob, upinf, ubainf, edat, sexe, tipus, psi) in self.poblacions_LV_pat:
                        den = self.poblacions_LV_pat[(pob, upinf, ubainf, edat, sexe, tipus, psi)]
                    else: den = 0
                    if num > den: print(key)
                    upload.append((indicador, 'PCT_MALALTIA', especialista, qui_deriva, psi, pob, upinf, ubainf, edat, sexe, tipus, 'DEN', den))
                    done[key].add((especialista, qui_deriva))

        for key, den in self.poblacions_LV_pat.items():
            pob, upinf, ubainf, edat, sexe, tipus, psi = key
            if key not in done:
                especialista = '10104'
                for qui_deriva in QUI_DERIVA:
                    indicador = INDICADORS[(especialista, qui_deriva, psi)]
                    upload.append((indicador, 'PCT_MALALTIA', especialista, qui_deriva, psi, pob, upinf, ubainf, edat, sexe, tipus, 'DEN', den))
                    upload.append((indicador, 'PCT_MALALTIA', especialista, qui_deriva, psi, pob, upinf, ubainf, edat, sexe, tipus, 'NUM', 0))
            elif key in done:
                especialista = '10104'
                for qui_deriva in QUI_DERIVA:
                    if (especialista, qui_deriva) not in done[key]:
                        indicador = INDICADORS[(especialista, qui_deriva, psi)]
                        upload.append((indicador, 'PCT_MALALTIA', especialista, qui_deriva, psi, pob, upinf, ubainf, edat, sexe, tipus, 'DEN', den))
                        upload.append((indicador, 'PCT_MALALTIA', especialista, qui_deriva, psi, pob, upinf, ubainf, edat, sexe, tipus, 'NUM', 0))
        u.listToTable(upload, self.table, self.db)

    def to_khalix(self):
        sql = """select tipus_indi, especialista, malaltia, pob, up, uba, edat, sexe, tipus_prof, analisi, sum(n) 
                from altres.resolucio_mapa
                where analisi = 'NUM'
                group by tipus_indi, especialista, malaltia, pob, up, uba, edat, sexe, tipus_prof, analisi
                UNION
                select distinct tipus_indi, especialista, malaltia, pob, up, uba, edat, sexe, tipus_prof, analisi, N
                from altres.resolucio_mapa
                where analisi = 'DEN'"""
        u.exportKhalix(sql, 'INDI_RES_MAPA')


if __name__ == "__main__":
    EXTERNES_1()
    
# 'E1110',
# 'E1111',
# 'E11319',
# 'E113292',
# 'E113293',
# 'E113312',
# 'E113393',
# 'E113513',
# 'E113519',
# 'E113553',
# 'E1136',
# 'E1141',
# 'E1142',
# 'E1144',
# 'E1152',
# 'E11621',
# 'E11622',
# 'E1321',
# 'E1329',
# 'E13311',
# 'E13319',
# 'E133311',
# 'E133411',
# 'E133551',
# 'E133553',
# 'E1340',
# 'E1341',
# 'E1342',
# 'E1352',
# 'E13621',
# 'E13622',
# 'E122',
# 'E132',
# 'E142',
# 'E130',
# 'E138',
# 'E121',
# 'E134',
# 'E139',
# 'E137',
# 'E123',
# 'E124',
# 'E11311',
# 'E1151',
# 'E1140',
# 'E1143',
# 'E1149',
# 'E1349',
# 'E1122',
# 'E1121',
# 'E08',
# 'E080',
# 'E0801',
# 'E081',
# 'E0800',
# 'E0810',
# 'E0811',
# 'E082',
# 'E0829',
# 'E083',
# 'E0839',
# 'E084',
# 'E085',
# 'E0859',
# 'E086',
# 'E0861',
# 'E08610',
# 'E08618',
# 'E0862',
# 'E08620',
# 'E08628',
# 'E0863',
# 'E08630',
# 'E08638',
# 'E0864',
# 'E08641',
# 'E08649',
# 'E0865',
# 'E0869',
# 'E088',
# 'E089',
# 'E09',
# 'E090',
# 'E0900',
# 'E0901',
# 'E091',
# 'E0910',
# 'E0911',
# 'E092',
# 'E0929',
# 'E093',
# 'E0939',
# 'E094',
# 'E095',
# 'E0959',
# 'E096',
# 'E0961',
# 'E09610',
# 'E09618',
# 'E0962',
# 'E09620',
# 'E09628',
# 'E0963',
# 'E09630',
# 'E09638',
# 'E0964',
# 'E09641',
# 'E09649',
# 'E0965',
# 'E0969',
# 'E098',
# 'E099',
# 'E11',
# 'E110',
# 'E1100',
# 'E1101',
# 'E112',
# 'E1129',
# 'E113',
# 'E1139',
# 'E114',
# 'E115',
# 'E1159',
# 'E116',
# 'E1161',
# 'E11610',
# 'E11618',
# 'E1162',
# 'E11620',
# 'E11628',
# 'E1163',
# 'E11630',
# 'E11638',
# 'E1164',
# 'E11641',
# 'E11649',
# 'E1165',
# 'E1169',
# 'E118',
# 'E119',
# 'E13',
# 'E130',
# 'E1300',
# 'E1301',
# 'E131',
# 'E1310',
# 'E1311',
# 'E132',
# 'E133',
# 'E1339',
# 'E134',
# 'E135',
# 'E1359',
# 'E136',
# 'E1361',
# 'E13610',
# 'E13618',
# 'E1362',
# 'E13620',
# 'E13628',
# 'E1363',
# 'E13630',
# 'E13638',
# 'E1364',
# 'E13641',
# 'E13649',
# 'E1365',
# 'E1369',
# 'E138',
# 'E139',
# 'E11',
# 'E110',
# 'E111',
# 'E112',
# 'E113',
# 'E114',
# 'E115',
# 'E116',
# 'E117',
# 'E118',
# 'E119',
# 'E12',
# 'E125',
# 'E13',
# 'E131',
# 'E133',
# 'E135',
# 'E136',
# 'E14',
# 'E140',
# 'E141',
# 'E143',
# 'E144',
# 'E145',
# 'E146',
# 'E147',
# 'E148',
# 'E149')