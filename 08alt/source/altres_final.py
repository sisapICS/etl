import sisapUtils as u
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db = "altres"
conn = u.connect((db, 'aux'))
c = conn.cursor()

ecap_ind = "exp_ecap_alt_uba"
ecap_pac = "exp_ecap_alt_pacient"
cataleg = "exp_ecap_alt_cataleg"
catalegPare = "exp_ecap_alt_catalegPare"
catalegExclosos = "exp_ecap_alt_catalegExclosos"


taules = [
    ['exp_ecap_tir_uba', 'exp_ecap_tir_pacient', 'exp_ecap_tir_cataleg',
     'exp_ecap_tir_catalegPare', 'exp_ecap_tir_catalegexclosos'],
    ['exp_ecap_tao_uba', 'exp_ecap_tao_pacient', 'exp_ecap_tao_cataleg',
     'exp_ecap_tao_catalegPare', 'exp_ecap_tao_catalegexclosos'],
    ['exp_ecap_pcc_uba', 'exp_ecap_pcc_pacient', 'exp_ecap_pcc_cataleg',
     'exp_ecap_pcc_catalegPare', ''],
    ['exp_ecap_aprop_uba', 'exp_ecap_aprop_pacient', 'exp_ecap_aprop_cataleg',
     'exp_ecap_aprop_catalegPare', ''],
    ['exp_ecap_alertes_uba', 'exp_ecap_alertes_pacient',
     'exp_ecap_alertes_cataleg', 'exp_ecap_alertes_catalegPare', ''],
    ['exp_ecap_gc_uba', 'exp_ecap_gc_pacient',
     'exp_ecap_gc_cataleg', 'exp_ecap_gc_catalegpare', ''],  
    ['exp_ecap_prof_uba', 'exp_ecap_prof_pacient', 'exp_ecap_prof_cataleg',
     'exp_ecap_prof_catalegPare', ''],
    ['exp_ecap_eqpfp_uba', 'exp_ecap_eqpfp_pacient',
     'exp_ecap_eqpfp_cataleg', 'exp_ecap_eqpfp_catalegPare', ''], 
    ['exp_ecap_inf_uba', '',
     'exp_ecap_inf_cataleg', 'exp_ecap_inf_catalegpare', ''],
    ['exp_ecap_atdom_uba', '',
     'exp_ecap_atdom_cataleg', 'exp_ecap_atdom_catalegpare', ''],
    ['exp_ecap_gida_uba', '',
     'exp_ecap_gida_cataleg', 'exp_ecap_gida_catalegpare', ''],
    ['exp_ecap_moni_glucosa', '', 'exp_ecap_moni_glucosa_cat',
     'exp_ecap_moni_glucosa_catpare', '']
    ]

for tb in [ecap_ind, ecap_pac, cataleg, catalegPare, catalegExclosos]:
    c.execute("drop table if exists {}".format(tb))

c.execute("""
          create table {} (
              up varchar(5) not null default '',
              uba varchar(20) not null default '',
              tipus varchar(1) not null default '',
              indicador varchar(10) not null default '',
              numerador int,
              denominador int,
              resultat double,
              noresolts int,
              mmin int not null default 0,
              mmax int not null default 0)
          """.format(ecap_ind)
          )

c.execute("""
          create table {} (
              id_cip_sec double,
              up varchar(5) not null default '',
              uba varchar(20) not null default '',
              upinf varchar(5) not null default '',
              ubainf varchar(7) not null default '',
              grup_codi varchar(10) not null default '',
              exclos int,
              hash_d varchar(40) not null default '',
              sector varchar(4) not null default '',
              comentari varchar(500) not null default '')
          """.format(ecap_pac)
          )

c.execute("""
          create table {} (
              indicador varchar(10),
              literal varchar(300),
              ordre int,
              pare varchar(10),
              llistat int,
              invers int,
              mmin double,
              mint double,
              mmax double,
              toShow int,
              wiki varchar(250),
              tipusvalor varchar(5))
          """.format(cataleg)
          )

c.execute("""
          create table {} (
              pare varchar(10) not null default '',
              literal varchar(300) not null default '',
              ordre int,
              pantalla varchar(20))
          """.format(catalegPare)
          )

c.execute("""
          create table {} (
              codi int,
              descripcio varchar(150),
              ordre int)
          """.format(catalegExclosos)
          )

c.execute("""
          alter table {} add unique(codi,descripcio,ordre)
          """.format(catalegExclosos)
          )

for r in taules:
    uba, llist, cat, catP, catE = r[0], r[1], r[2], r[3], r[4]
    if uba in ('exp_ecap_resi_uba', 'exp_ecap_atdom_uba'):
        pass
    elif uba not in ('exp_ecap_eqpfp_uba', 'exp_ecap_gc_uba', 'exp_ecap_moni_glucosa'):
        c.execute("""
                  insert into {} select *, Null, 0, 0 from {} where uba <> ''
                  """.format(ecap_ind, uba)
                  )
    elif uba == 'exp_ecap_eqpfp_uba':
        c.execute("""
                  insert into {} select *, 0, 0 from {} where uba <> ''
                  """.format(ecap_ind, uba)
                  )
    elif uba in ('exp_ecap_gc_uba', 'exp_ecap_moni_glucosa'):
        c.execute("""
                  insert into {} select up, uba, tipus, indicador, numerador, denominador, resultat, Null, mmin, mmax from {} where uba <> ''
                  """.format(ecap_ind, uba)
                  )

    if llist.endswith("pacient"):
        if llist != 'exp_ecap_eqpfp_pacient':
            c.execute("""
                    insert into {} select *, Null from {} where uba <> ''
                    """.format(ecap_pac, llist)
                    )
        elif llist == 'exp_ecap_eqpfp_pacient':
            c.execute("""
                    insert into {} select * from {} where uba <> ''
                    """.format(ecap_pac, llist)
                    )
    c.execute("insert into {} select * from {}".format(cataleg, cat))
    c.execute("insert into {} select * from {} where pantalla != 'VALIDACIO'".format(catalegPare, catP))
    if catE.endswith("catalegexclosos"):
        c.execute("""
                  insert ignore into {} select * from {}
                  """.format(catalegExclosos, catE)
                  )

c.execute("""
          update {} set uba = '' where uba = 0 and grup_codi like ('PLA%')
          """.format(ecap_pac)
          )

conn.close()

print strftime("%Y-%m-%d %H:%M:%S")
