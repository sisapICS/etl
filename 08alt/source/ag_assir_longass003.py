# -*- coding: latin1 -*-
import collections as c
import sisapUtils as u


INDICADOR = "LONGASS003"
TB_NAME = "exp_khalix_{}_up".format(INDICADOR)
DB_NAME = "altres"
COL_NAMES = "(up varchar(5), tipus varchar(1), indicador varchar(10), numerador int, denominador int)"


# Obtenim la data d'extracci� actual i un any enrere
sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY)
        FROM
            dextraccio
      """
data_ext, data_ext_menys1any = u.getOne(sql, "nodrizas")


def get_centres():
    """
    Obtenci� de les unitats productives (UPs) dels centres de la taula `ass_centres`.
    
    Guarda els UP en un set global 'ass_centres' amb format set.
    """

    global ass_centres

    sql = """
            SELECT
                up
            FROM
                ass_centres
           """
    # Executem la consulta SQL i guardem els resultats en un conjunt
    ass_centres = {up for up, in u.getAll(sql, "nodrizas")}


def get_professionals():
    """
    Obtenci� dels professionals assignats (llevadores i ginec�legs) del sistema ASSIR.
    
    Guarda els professionals en un diccionari global 'professional' amb el format:
    {dni: tipus} on 'tipus' pot ser 'L' (llevadora) o 'G' (ginec�leg).
    """
    global professional
    
    sql = """
            SELECT
                LEFT(ide_dni, 8),
                tipus
            FROM
                cat_professionals_assir
          """
    professional = {dni: tipus for dni, tipus in u.getAll(sql, "import")}


def get_pacients():
    """
    Obtenci� dels poblacio_ass assignats amb les seves unitats prove�dores (UP).
    
    Guarda els poblacio_ass en un diccionari global 'poblacio_ass' amb el format:
    {id_cip_sec: {visi_up}}.
    """
    global poblacio_ass;        poblacio_ass = c.defaultdict(set)
    
    sql = """
            SELECT
                id_cip_sec,
                visi_up
            FROM
                ass_imputacio_up
          """
    for id_cip_sec, up in u.getAll(sql, "nodrizas"):
        poblacio_ass[id_cip_sec].add(up)


def get_embarassades():
    """
    Obtenci� de les embarassades i les seves dates rellevants.
    
    Guarda la informaci� en un diccionari global 'embarassades' amb el format:
    {id_cip_sec: {"inici_embaras": data_inici, "fi_embaras": data_fi, "fi_puerperi": data_fi + 120 dies}}.
    """
    global embarassades;        embarassades = c.defaultdict(dict)

    sql = """
        SELECT
            id_cip_sec,
            inici,
            fi,
            date_add(fi, INTERVAL 120 DAY) AS fi_puerperi
        FROM
            ass_embaras
        WHERE
            (inici BETWEEN '{_data_ext_menys1any}' AND '{_data_ext}'
             OR fi BETWEEN '{_data_ext_menys1any}' AND '{_data_ext}')
            AND temps > 150
    """.format(_data_ext_menys1any=data_ext_menys1any, _data_ext=data_ext)
    for id_cip_sec, inici_embaras, fi_embaras, fi_puerperi in u.getAll(sql, "nodrizas"):
        if id_cip_sec in poblacio_ass:
            embarassades[id_cip_sec] = {
                "inici_embaras": inici_embaras,
                "fi_embaras": fi_embaras,
                "fi_puerperi": fi_puerperi
            }


def get_visites_embaras():
    """
    Obtenci� de les visites realitzades durant l'embar�s.
    
    Guarda les visites en un diccionari global 'visites_embaras' amb el format:
    {(id_cip_sec, tipus): {"TOTAL": num_visites, dni: num_visites_per_dni}}.
    """
    global visites_embaras;         visites_embaras = c.defaultdict(c.Counter)
    
    sql = """
            SELECT
                id_cip_sec,
                visi_up,
                visi_data_visita,
                LEFT(visi_dni_prov_resp, 8)
            FROM
                ass_visites
          """
    for id_cip_sec, up, data_visita, dni in u.getAll(sql, "nodrizas"):
        if id_cip_sec in embarassades and up in ass_centres:
            # Comprovem si la visita est� dins del per�ode d'embar�s i �s entre setmana
            if (embarassades[id_cip_sec]["inici_embaras"] <= data_visita <= embarassades[id_cip_sec]["fi_embaras"]) and data_visita.weekday() < 5:
                tipus = professional.get(dni)
                if tipus:
                    visites_embaras[(id_cip_sec, tipus)]["TOTAL"] += 1
                    visites_embaras[(id_cip_sec, tipus)][dni] += 1


def get_indicador():
    """
    C�lcul de l'indicador LONGASS003.
    
    Guarda el resultat en un diccionari global 'indicador' amb el format:
    {(up, tipus): {"NUM": numerador, "DEN": denominador}}.

    L�gica:
    -------
    1. Per a cada pacient (`id_cip_sec`) i tipus de professional (`tipus`) en el 
       diccionari `visites_embaras`, s'obt� el **major nombre de visites** fetes per un mateix professional
       durant l'embar�s.
    
    2. Les visites totals realitzades es guarden en el denominador, mentre que el nombre 
       de visites fetes pel professional majoritari es guarden en el numerador.

    Condicions:
    -----------
    - **Pacients que NO es comptabilitzen en l'indicador**:
        - Aquells que han tingut **menys de 3 visites** amb el professional majoritari.
        - Aquells que han tingut **m�s de 50 visites** amb el professional majoritari.
    """
    global indicador
    indicador = c.defaultdict(c.Counter)
    
    for id_cip_sec, tipus in visites_embaras:
        # Utilitzem max() per trobar el nombre m�s alt de visites de cada pacient amb el mateix professional, excloent 'TOTAL'
        major_nombre_visites_amb_llevadora = max(visites_embaras[(id_cip_sec, tipus)][dni] for dni in visites_embaras[(id_cip_sec, tipus)] if dni != 'TOTAL')
        # Comprovem si el nombre de visites m�xim es troba dins del rang perm�s (3 a 50)
        if 3 <= major_nombre_visites_amb_llevadora <= 50:
            # Actualitzem el numerador i el denominador per cada unitat productiva (UP) del pacient
            for up in poblacio_ass[id_cip_sec]:
                indicador[(up, tipus)]["DEN"] += visites_embaras[(id_cip_sec, tipus)]["TOTAL"]
                indicador[(up, tipus)]["NUM"] += major_nombre_visites_amb_llevadora


def export_table():
    """
    Exporta les dades de l'indicador a la base de dades.
    """

    u.createTable(TB_NAME, COL_NAMES, DB_NAME, rm=True)
    upload = [(up, tipus, INDICADOR, v["NUM"], v["DEN"]) for ((up, tipus), v) in indicador.items()]
    u.listToTable(upload, TB_NAME, DB_NAME)


if __name__ == "__main__":
    # Executem totes les funcions per calcular i exportar l'indicador
    get_centres();                  print("get_centres()")
    get_professionals();            print("get_professionals()")
    get_pacients();                 print("get_pacients()")
    get_embarassades();             print("get_embarassades()")
    get_visites_embaras();          print("get_visites_embaras()")
    get_indicador();                print("get_indicador()")
    export_table();                 print("export_table()")