# coding: latin1

"""
EQPF a partir de prescripci�.
"""

import collections as c
import itertools as i
import simplejson as j

import sisapUtils as u


DEBUG = False

TABLE_KLX = "exp_khalix_eqpf_uba_historic"
DATABASE = "altres"
FILE_UBA = "EQPF_PRESCRIPCIO_uba_h"

db="altres"

class EQPF(object):
    """."""

    def __init__(self):
        """."""
        self.get_indicadors()
        self.get_centres()
        self.get_poblacio()
        for p in ('2023-01-31', '2023-02-28', '2023-03-31', '2023-04-30'):
            print(p)
            self.dext = p 
            self.periode_khalix = 'A'+ p[2:4] + p[5:7]
            self.execute_pool()

    def get_indicadors(self):
        """."""
        file = "dades_noesb/eqpf_p_uba.json"
        try:
            stream = open(file)
        except IOError:
            stream = open("../" + file)
        self.indicadors = j.load(stream)

    def get_centres(self):
        """."""
        sql = "select scs_codi, ics_codi, medea from cat_centres"
        self.centres = {row[0]: row[1:] for row in u.getAll(sql, "nodrizas")}

    def get_poblacio(self):
        """."""
        self.poblacio = c.defaultdict(lambda : c.defaultdict(dict))
        sql = "select codi_sector, id_cip_sec, \
               up, uba, ubainf, edat, sexe, ates, institucionalitzat, nivell_cobertura\
               from assignada_tot"
        for sec, id, up, uba, ubainf, edat, sex, at, ins, cob in u.getAll(sql, "nodrizas"):  # noqa
            age_k, sex_k = u.ageConverter(edat), u.sexConverter(sex)
            br, medea = self.centres[up]
            ents = (br, br + "M" + uba)
            pobs = ["{}INSASS".format("NO" if not ins else "")]
            if at:
                pobs.append("{}INSAT".format("NO" if not ins else ""))
            combs = i.product(ents, pobs, [age_k], [sex_k])
            self.poblacio[sec][id]['M'] = (tuple(combs), medea, cob == "PN", up)
            ents = (br, br + "I" + ubainf)
            combs = i.product(ents, pobs, [age_k], [sex_k])
            self.poblacio[sec][id]['I'] = (tuple(combs), medea, cob == "PN", up)

    def execute_pool(self):
        """."""
        taules = [k for (k, v)
                  in sorted(u.getSubTables("tractaments").items(),
                            key=lambda x: x[1], reverse=True)
                  if v > 0]
        for e in taules: print(e)
        jobs = [(taula, self.poblacio[taula[-4:]], self.indicadors, self.dext, self.periode_khalix)
                for taula in taules]
        if DEBUG:
            Sector(jobs[-1])
        else:
            u.multiprocess(Sector, jobs, 8)


class Sector(object):
    """."""

    def __init__(self, params):
        """."""
        self.taula, self.poblacio, self.indicadors, self.dext, self.periode_khalix = params
        sql = """select atc_codi, atc_desc from import.cat_cpftb010_def"""
        self.cat_descrip = {}
        for atc, atc_desc in u.getAll(sql, 'import'):
            self.cat_descrip[atc] = atc_desc
        self.dades = c.Counter()
        self.get_den_pob()
        self.get_dades()
        self.get_khalix()
        self.get_ajustades()
        self.upload_khalix()

    def get_den_pob(self):
        """."""
        for id in self.poblacio:
            for indicador, params in self.indicadors.items():
                if params["den"] == "pob":
                    self.dades[(id, indicador)] = {"den": 1, "num": 0}

    def get_dades(self):
        """."""
        sql = "select id_cip_sec, codi_sector, ppfmc_pf_codi, ppfmc_atccodi, \
                     ppfmc_pmc_data_ini > adddate(date '{dext}', interval -1 year),\
                     ppfmc_data_fi > date '{dext}', ppfmc_pmc_amb_cod_up, \
                     timestampdiff(MONTH, ppfmc_pmc_data_ini, ppfmc_data_fi) \
               from {taula} \
               where ppfmc_pmc_data_ini <= date '{dext}' and \
                 (ppfmc_pmc_data_ini > adddate(date '{dext}', interval -1 year) or \
                  ppfmc_data_fi > date '{dext}')".format(taula=self.taula, dext=self.dext)
        print(sql)
        self.no_recomanats = set()
        self.no_numerador_llistat = c.defaultdict(set)
        self.excl_num = set()
        for id, sector, pf, atc, incid, obert, up, nmesos in u.getAll(sql, "import"):
            for indicador, params in self.indicadors.items():
                calcular = params["calcular"]
                aguda = "aguda" in params
                eap = "eap" in params
                mesos = "mesos" in params
                condicions = [calcular]
                condicions.append((obert and not aguda) or (incid and aguda))
                condicions.append(not eap or (id in self.poblacio and up == self.poblacio[id][3]))  # noqa
                condicions.append(not mesos or params["mesos"][0] <= nmesos <= params["mesos"][1])  # noqa
                if all(condicions):
                    key = (id, indicador)
                    ind_no_rec = "no_recomanats" in params
                    es_den = (params["den"] in ("pob", "tot") or
                              any([atc[:n] in params["den"]
                                   for n in range(3, 8)]))
                    if es_den and key not in self.dades:
                        self.dades[key] = {"den": 1, "num": 1 * ind_no_rec}
                    if key in self.dades:
                        if atc in params["excl"]:
                            self.dades[key]["den"] = 0
                        cod = {k: params["num"].get(k, [])
                               for k in ("ATC", "PF")}
                        num = {"ATC": any([atc[:n] in cod["ATC"]
                                           for n in range(3, 8)]),
                               "PF": str(pf) in cod["PF"]}
                        es_num = any(num.values())
                        if es_num and not ind_no_rec:
                            self.dades[key]["num"] = 1
                        elif es_den and not es_num and ind_no_rec:
                            self.no_recomanats.add(key)
                        if atc in params.get("excl_num", []):
                            self.excl_num.add(key)

    def get_khalix(self):
        """."""
        self.khalix = c.Counter()
        self.denominadors = c.Counter()
        self.factors = c.defaultdict(c.Counter)
        for (id, ind), n in self.dades.items():
            if n["den"] == 1 and id in self.poblacio:
                num = n["num"]
                for obj in (self.no_recomanats, self.excl_num):
                    if (id, ind) in obj:
                        num = 0
                dhd = self.indicadors[ind]["den"] == "pob"
                for prof in ('M', 'I'):
                    keys, medea, pn, up = self.poblacio[id][prof]
                    for ent, pob, age, sex in keys:
                        self.khalix[(ind, ent, "NUM", age, pob, sex, self.periode_khalix)] += num
                        if dhd:
                            self.denominadors[(ind, ent, pob, age, sex, medea, pn)] += 1  # noqa
                            if len(ent) == 5 and prof == 'M':
                                self.factors[(ind, pob, age, sex, medea, pn)]["DEN"] += 1  # noqa
                                self.factors[(ind, pob, age, sex, medea, pn)]["NUM"] += num  # noqa
                        else:
                            self.khalix[(ind, ent, "DEN", age, pob, sex, self.periode_khalix)] += 1
        self.poblacio = {}
        self.dades = {}

    def get_ajustades(self):
        """."""
        total = c.defaultdict(c.Counter)
        especifics = {}
        for key, dades in self.factors.items():
            for analysis, n in dades.items():
                total[key[:2]][analysis] += float(n)
            especifics[key] = dades["NUM"] / float(dades["DEN"])
        pesos = {}
        for key, value in especifics.items():
            tot = (total[key[:2]]["NUM"] / total[key[:2]]["DEN"])
            pesos[key] = (value / tot) if tot else 1
        for (ind, ent, pob, age, sex, medea, pn), n in self.denominadors.items():  # noqa
            pes = pesos[(ind, pob, age, sex, medea, pn)]
            key = (ind, ent, "DEN", age, pob, sex, self.periode_khalix)
            self.khalix[key] += (n * pes)

    def upload_khalix(self):
        """."""
        upload = [k + (v,) for (k, v) in self.khalix.items()]
        u.listToTable(upload, TABLE_KLX, DATABASE)
        self.khalix = {}


def export_khalix():
    """."""
    sql = """select k0, k6, k1, k2, 'NOCAT', 'NOINSAT', 'DIM6SET', 'N', sum(v) 
                from altres.exp_khalix_eqpf_uba_historic
                where length(k1) > 5
                group by k0, k6, k1, k2, 'NOCAT', 'NOINSAT', 'DIM6SET', 'N'"""
    u.exportKhalix(sql, FILE_UBA)
        
if __name__ == "__main__":
    cols = ["k{} varchar(15)".format(ij) for ij in range(7)] + ["v decimal(12, 4)"]  # noqa
    cols_str = "({})".format(", ".join(cols))
    u.createTable(TABLE_KLX, cols_str, DATABASE, rm=True)
    EQPF()
