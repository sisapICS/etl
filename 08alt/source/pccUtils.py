# coding: iso-8859-1

Indicadors = [['ESIAP0602'],]


indicadorsDict = {
        'ESIAP0602': {
                'literal': 'Taxa de PCC i MACA amb PIIC (x cada 1000 persones de poblaci� assignada)',
                'ordre': 1,
                'pare': 'ALT01',
                'llistat': 1,
                'invers': 0,
                'mmin': 0,
                'mmint': 0,
                'mmax': 0,
                'toShow': 1,
                'wiki': 'http://10.80.217.201/sisap-umi/indicador/indicador/3141/ver/',
                'tipusvalor': 'TPM',
                'literalPare': 'Atenci� a la cronicitat',
                'ordrePare': 4,
                'pantalla': 'ALTRES'
        },
}
                
               