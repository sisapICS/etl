# coding: iso-8859-1
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
from dateutil.relativedelta import relativedelta
import csv
import re
import operator
import random

file_name='FRAGILITAT'

nod="nodrizas"
imp="import"
alt="altres"
agr_ps = 764
agr_vs = {917: "= 1", 918: ">= 0.20"}
codi='IAD0023'



class Fragilitat(object):
    def __init__(self):
        self.current_date=self.get_date_dextraccio()
        
        self.problemes=set(criteri_codi for criteri_codi, in getAll("SELECT criteri_codi FROM eqa_criteris WHERE agrupador = {}".format(agr_ps), "nodrizas"))
        self.variables={criteri_codi: agr_vs[agrupador] for agrupador, criteri_codi, in getAll("SELECT agrupador, criteri_codi FROM eqa_criteris WHERE agrupador in {}".format(str(tuple(agr_vs.keys()))), "nodrizas")}

        self.edat_codis={'FRAG0001':(operator.ge,0),
                         'FRAG0002':(operator.ge,65),
                         'FRAG0003':(operator.ge,75)
                        }
        self.tipus_pob= {(0,1):{'INSASS'}, (1,1):{'INSASS','INSAT'}, 
                         (0,0):{'NOINSASS'}, (1,0):{'NOINSASS','NOINSAT'}}

    def get_date_dextraccio(self):
        """Get current extraction date. Returns date in datetime.date format"""
        sql="select data_ext from {0}.dextraccio;".format(nod)
        return getOne(sql,nod)[0]

    def get_problemes(self,table):
        pac=set()
        sql='select id_cip_sec, pr_dde, pr_dba from import.{} where pr_cod_ps in {} and pr_hist = 1 and pr_data_baixa = 0'.format(table,str(tuple(self.problemes)))
        #return {id for id,dde,dba in getAll(sql,imp) if dde < self.current_date <= dba}
        for id,dde,dba in getAll(sql,imp):
            dba=self.current_date if not dba else dba
            if dde < self.current_date <= dba:
                pac.add(id)
        return pac

    def get_variables(self,table):
        pac=set()

        filter = str()
        for i, (vu_cod_vs, vu_val) in enumerate(self.variables.items()):
            if i == 0:
                filter += "(vu_cod_vs = '{}' AND vu_val {}) ".format(vu_cod_vs, vu_val)
            else:
                filter += "OR (vu_cod_vs = '{}' AND vu_val {}) ".format(vu_cod_vs, vu_val)
        sql='select id_cip_sec from import.{} where {}'.format(table,filter)
        
        for id, in getAll(sql,imp):
            pac.add(id)
        return pac

    def get_ares(self,table):
        pac=set()
        sql='select id_cip_sec, pi_data_inici, pi_data_fi from import.{} where pi_codi_pc in {}'.format(table,str(tuple(self.problemes)))

        for id, data_inici, data_fi in getAll(sql,imp):
            if data_inici < self.current_date and not data_fi:
                pac.add(id)
            elif data_inici < self.current_date <= data_fi:
                pac.add(id)
        return pac

    def get_multiprocess_ps(self):
        return set.union(*[id for id in multiprocess(self.get_problemes, getSubTables('problemes'))])
    
    def get_multiprocess_vs(self):
        return set.union(*[id for id in multiprocess(self.get_variables, getSubTables('variables'), 8)])
    
    def get_multiprocess_ares(self):
        return set.union(*[id for id in multiprocess(self.get_ares, getSubTables('ares'))])

    def get_indicador(self):
        num_ps=self.get_multiprocess_ps()
        num_vs=self.get_multiprocess_vs()
        num_ares=self.get_multiprocess_ares()
        num = set().union(num_ps, num_vs, num_ares)
        indicador= defaultdict(Counter)
        sql="select id_cip_sec, up,edat,sexe,ates,institucionalitzat from {}.assignada_tot".format(nod)
        for id,up,edat,sexe,ates,ins in getAll(sql,nod):
            sexe=sexConverter(sexe) 
            for codi,(opr,ref) in self.edat_codis.iteritems():
                if opr(edat,ref):
                    edat_conv=ageConverter(edat)
                    for pob in self.tipus_pob[(ates,ins)]:
                        indicador[(up,codi,pob,edat_conv,sexe)]["DEN"]+=1
                        if id in num:
                            indicador[(up,codi,pob,edat_conv,sexe)]["NUM"]+=1        

        return [(up,codi,pob,edat,sexe,indicador[(up,codi,pob,edat,sexe)]["NUM"],indicador[(up,codi,pob,edat,sexe)]["DEN"]) for up,codi,pob,edat,sexe in indicador ]
    
def export_table(table,columns,db,rows):
    createTable(table, columns, db, rm=True)
    listToTable(rows, table, db)


def push_to_khalix(taula,taula_centres,file_name,khalix=False):
    query_string="select indicador,concat('A','periodo'),ics_codi,'{var}',edat,tipus_pob,sexe,'N',{tipus} from altres.{taula} a inner join nodrizas.{taula_centres} b on a.up=scs_codi where a.{tipus} != 0"
    query_final=" union ".join([query_string.format(taula=taula,taula_centres=taula_centres,var=var,tipus=tipus) for (var,tipus) in zip(["NUM","DEN"],["numerador","denominador"])])
    print(query_final)
    if khalix:
        exportKhalix(query_final,file_name)

if __name__ == '__main__':
    printTime('inici')
   
    #Se crea la tabla
    table_name="exp_khalix_fragilitat"
    columns="(up varchar(5),indicador varchar(30),tipus_pob varchar(30),edat varchar(30),sexe varchar(30),numerador int, denominador int)"
    createTable(table_name,columns,alt,rm=True)

    frag=Fragilitat()
    rows=frag.get_indicador()
    export_table(table_name,columns,alt,rows)
    push_to_khalix(table_name,"cat_centres",file_name, khalix=True)