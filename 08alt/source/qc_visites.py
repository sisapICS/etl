# coding: latin1

"""
.
"""

import collections as c
import dateutil.relativedelta as r

import sisapUtils as u


TABLE = "exp_qc_visites"
DATABASE = "altres"
TABLE_ECAP = "exp_ecap_organitzacio"


class Organitzacio(object):
    """."""

    def __init__(self):
        """."""
        self.get_periodes()
        self.get_visites()
        self.get_problemes()
        self.get_medea()
        self.get_poblacio()
        self.get_esperada()
        self.export_qc()
        self.export_ecap()

    def get_periodes(self):
        """."""
        dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
        fa1m = dext - r.relativedelta(months=1)
        self.pactual = "A{}".format(dext.strftime("%y%m"))
        self.pprevi = "A{}".format(fa1m.strftime("%y%m"))
        self.periodes = {"ANUAL": self.pactual, "ACTUAL": self.pactual,
                         "PREVI": self.pprevi}

    def get_visites(self):
        """."""
        self.visites = c.defaultdict(lambda: c.defaultdict(c.Counter))
        self.al_teu_costat = c.defaultdict(lambda: c.defaultdict(c.Counter))
        self.econsulta = c.defaultdict(c.Counter)
        sql = "select pac_id, case when prof_categ = '10888' then 2 \
                    when prof_categ like '3%' then 4 \
                    else 1 end tipprof, modul_uba <> '',\
                      modul_up, modul_serv, modul_codi, modul_atributs, \
                      vis_data, vis_tipus_homol, vis_etiqueta \
               from ag_longitudinalitat_new partition(p{}) \
               where pac_up = modul_up and \
                     prof_categ in ('30999', '10117', '10999', \
                                    '10116', '10112', '10888')"
        jobs = [(sql.format(i), "nodrizas") for i in range(13)]
        resultat = u.multiprocess(u.get_data, jobs)
        for worker in resultat:
            for id, categ, uba, up, servei, modul, attr, data, tip, eti in worker:  # noqa
                es_uba = attr == "1.1"
                es_urg = attr == "2.4"
                self.visites[id][(categ, "ANUAL")][(uba, es_urg)] += 1
                if tip in ("9C", "9R") and (es_uba or es_urg):
                    self.al_teu_costat[id][(categ, "ANUAL")][es_uba] += 1
                elif tip == "9E" and eti == "ECTA":
                    self.econsulta[id][(categ, "ANUAL")] += 1
                periode = "A{}".format(data.strftime("%y%m"))
                if periode in (self.pactual, self.pprevi):
                    this = "ACTUAL" if periode == self.pactual else "PREVI"
                    self.visites[id][(categ, this)][(uba, es_urg)] += 1
                    if tip == "9E" and eti == "ECTA":
                        self.econsulta[id][(categ, this)] += 1

    def get_problemes(self):
        """."""
        self.problemes = c.defaultdict(set)
        sql = "select id, ps from inf_problemes"
        for id, ps in u.getAll(sql, "nodrizas"):
            for ind in ("HTA", "DM"):
                if ps == ind:
                    self.problemes[id].add(ind)

    def get_medea(self):
        """."""
        sql = "select scs_codi, medea from cat_centres"
        self.medea = {row[0]: row[1] for row in u.getAll(sql, "nodrizas")}

    def get_poblacio(self):
        """."""
        self.resultat = c.Counter()
        self.resultat_uba = c.Counter()
        self.factor_tot = c.Counter()
        self.factor_grup = c.defaultdict(c.Counter)
        people = {}
        sql = "select id_cip_sec, up, uba, ubainf, edat, sexe \
               from assignada_tot"
        adults = 0
        for id, up, uba, ubainf, edat, sexe in u.getAll(sql, "nodrizas"):
            people[id] = (up, uba, ubainf, edat, sexe)
            
        for id, (up, uba, ubainf, edat, sexe) in people.items():
            if id in self.visites:
                visites = self.visites.pop(id)
                if edat > 14: 
                    adults = 1
                else: 
                    adults = 0
                edat_k = u.ageConverter(edat)
                sexe_k = u.sexConverter(sexe)
                problemes = self.problemes[id]
                for i in (1, 2, 4):
                    tp = "TIPPROF{}".format(i)
                    for periode in self.periodes:
                        if adults and i != 2:
                            self.resultat[(periode, "FREQOBS", up, edat_k, sexe_k, tp, "DEN")] += 1  # noqa
                            for ps in problemes:
                                self.resultat[(periode, "FREQ{}".format(ps), up, edat_k, sexe_k, tp, "DEN")] += 1  # noqa
                            self.factor_tot[(periode, "DEN")] += 1
                            self.factor_grup[(edat_k, sexe_k, self.medea[up])][(periode, "DEN")] += 1  # noqa
                        vis_tot = sum(visites[(i, periode)].values())
                        vis_uba = visites[(i, periode)][(1, True)] + visites[(i, periode)][(1, False)]  # noqa
                        vis_urg = visites[(i, periode)][(1, True)] + visites[(i, periode)][(0, True)]  # noqa
                        atc_tot = self.al_teu_costat[id][(i, periode)][True] + self.al_teu_costat[id][(i, periode)][False]  # noqa
                        atc_uba = self.al_teu_costat[id][(i, periode)][True]
                        atc_urg = self.al_teu_costat[id][(i, periode)][False]
                        vvi_ecta = self.econsulta[id][(i, periode)]
                        if vis_tot:
                            if adults and i != 2: 
                                self.resultat[(periode, "FREQOBS", up, edat_k, sexe_k, tp, "NUM")] += vis_tot  # noqa
                                for ps in problemes:
                                    self.resultat[(periode, "FREQ{}".format(ps), up, edat_k, sexe_k, tp, "NUM")] += vis_tot  # noqa
                            self.resultat[(periode, "VISUBA", up, edat_k, sexe_k, tp, "DEN")] += vis_tot  # noqa
                            if periode == "ANUAL":
                                self.resultat_uba[("VISUBA", up, uba, "M", tp, "DEN")] += vis_tot  # noqa
                                if ubainf:
                                    self.resultat_uba[("VISUBA", up, ubainf, "I", tp, "DEN")] += vis_tot  # noqa
                            self.resultat[(periode, "VISURG", up, edat_k, sexe_k, tp, "DEN")] += vis_tot  # noqa
                            if adults and i != 2: 
                                self.resultat[(periode, "VVIRTUALS", up, edat_k, sexe_k, tp, "DEN")] += vis_tot  # noqa
                                self.factor_tot[(periode, "NUM")] += vis_tot
                                self.factor_grup[(edat_k, sexe_k, self.medea[up])][(periode, "NUM")] += vis_tot  # noqa
                            if vis_uba:
                                self.resultat[(periode, "VISUBA", up, edat_k, sexe_k, tp, "NUM")] += vis_uba  # noqa
                                if periode == "ANUAL":
                                    self.resultat_uba[("VISUBA", up, uba, "M", tp, "NUM")] += vis_uba  # noqa
                                    if ubainf:
                                        self.resultat_uba[("VISUBA", up, ubainf, "I", tp, "NUM")] += vis_uba  # noqa
                            if vis_urg:
                                self.resultat[(periode, "VISURG", up, edat_k, sexe_k, tp, "NUM")] += vis_urg  # noqa
                            if atc_tot:
                                self.resultat[(periode, "VISUBA2", up, edat_k, sexe_k, tp, "DEN")] += atc_tot  # noqa
                                self.resultat[(periode, "VISURG2", up, edat_k, sexe_k, tp, "DEN")] += atc_tot  # noqa
                                if atc_uba:
                                    self.resultat[(periode, "VISUBA2", up, edat_k, sexe_k, tp, "NUM")] += atc_uba  # noqa
                                if atc_urg:    
                                    self.resultat[(periode, "VISURG2", up, edat_k, sexe_k, tp, "NUM")] += atc_urg  # noqa
                            if vvi_ecta and i != 2:
                                if adults:
                                    self.resultat[(periode, "VVIRTUALS", up, edat_k, sexe_k, tp, "NUM")] += vvi_ecta  # noqa

    def get_esperada(self):
        """."""
        for periode in self.periodes:
            total = self.factor_tot[(periode, "NUM")] / float(self.factor_tot[(periode, "DEN")])  # noqa
            pesos = {key: (dades[(periode, "NUM")] / float(dades[(periode, "DEN")])) / total  # noqa
                     for (key, dades) in self.factor_grup.items()}
            for (per, ind, up, edat, sexe, tp, an), n in self.resultat.items():
                if per == periode and ind == "FREQOBS":
                    ind_n = "FREQESP"
                    if an == "DEN":
                        pes = pesos[(edat, sexe, self.medea[up])]
                        val = n * pes
                        self.resultat[(per, ind_n, up, edat, sexe, tp, an)] = val  # noqa
                    else:
                        self.resultat[(per, ind_n, up, edat, sexe, tp, an)] = n

    def export_qc(self):
        """."""
        centres = {up: br for (up, br)
                   in u.getAll("select scs_codi, ics_codi from cat_centres",
                               "nodrizas")}
        dades = c.Counter()
        for (per, ind, up, edat, sexe, tip, ana), n in self.resultat.items():
            dades[(ind, self.periodes[per], centres[up], ana, per, tip)] += n
        upload = [k + (v,) for (k, v) in dades.items()]
        cols = ["k{} varchar(16)".format(i)
                for i in range(6)] + ["v decimal(12, 4)"]
        u.createTable(TABLE, "({})".format(", ".join(cols)), DATABASE, rm=True)
        u.listToTable(upload, TABLE, DATABASE)
        upload = [(ind, self.periodes["ANUAL"], centres[up] + tipus + uba,
                   analisi, "ANUAL", tp, n)
                  for (ind, up, uba, tipus, tp, analisi), n
                  in self.resultat_uba.items()]
        u.listToTable(upload, TABLE, DATABASE)

    def export_ecap(self):
        """."""
        cols = "(up varchar(5), uba varchar(5), tipus varchar(1), \
                 ind varchar(10), num int, den int, res double)"
        u.createTable(TABLE_ECAP, cols, DATABASE, rm=True)
        upload = []
        for (ind, up, uba, tipus, tp, analisi), n in self.resultat_uba.items():
            if analisi == "DEN" and ((tipus == "M" and tp == "TIPPROF1") or (tipus == "I" and tp == "TIPPROF4")):  # noqa
                den = float(n)
                num = self.resultat_uba[(ind, up, uba, tipus, tp, "NUM")]
                res = num / den
                upload.append((up, uba, tipus, ind, num, den, res))
        u.listToTable(upload, TABLE_ECAP, DATABASE)


if __name__ == "__main__":
    Organitzacio()
