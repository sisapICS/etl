# -*- coding: utf8 -*-

"""
Longiudinalitat com a temps
"""

import collections as c
import sisapUtils as u

DEBUG = False

db='permanent'

class Continuitat(object):
    """."""

    def __init__(self):
        """."""

        self.get_responsable()
        self.get_pob()
        self.create_tables()
        self.get_visites()
        self.find_minimum()
        self.get_taula()
        self.export_files()

    def get_responsable(self):
        """."""
        u.printTime("Responsable")
        sql = "select lloc_codi_up, lloc_codi_lloc_de_treball, lloc_numcol \
               from cat_pritb025 where lloc_data_baixa = '4712-01-01'"
        llocs = {(up, lloc): col[0:8] for (up, lloc, col)
                 in u.getAll(sql, 'import')}
        sql = "select {}, '{}', {}, {} from {} where {}"
        self.responsable = {}
        for param in (("uab_codi_up", "M", "uab_codi_uab",
                       "uab_lloc_de_tr_codi_lloc_de_tr", "cat_vistb039", "uab_data_baixa= '4712-01-01'"),
                      ("uni_codi_up", "I", "uni_codi_unitat",
                       "uni_ambit_treball", "cat_vistb059", "uni_data_baixa= 0")):
            this = sql.format(*param)
            for up, tipus, uba, lloc in u.getAll(this, 'import'):
                if uba and lloc and (up, lloc) in llocs:
                    col = llocs[(up, lloc)]
                    self.responsable[(up, tipus, uba)]= col[0:8]

    def get_pob(self):
        """."""
        u.printTime("Assignada")
        sql = "select id_cip_sec, up, uba, edat, sexe from nodrizas.rip_assignada"
        self.pob = {id: (up, uba, edat, sexe) for (id, up, uba, edat, sexe) in u.getAll(sql, 'nodrizas')}
    
    def create_tables(self):
        """export a permanent"""
        u.printTime("create tables")
        
        tb = "long_temps_extended_rip"
        cols = ("id_cip_sec int", "sector varchar(5)", "numcol varchar(10)",  "data_min date",  "data_max date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
    
    def get_visites(self):
        """Agafem visites de tota la vida"""
        u.printTime("Visites")
        self.visitespacients = {}
        sql = "select id_cip_sec, codi_sector, visi_col_prov_resp, visi_data_visita \
                from {} \
                where visi_situacio_visita = 'R' and visi_data_baixa=0 and visi_col_prov_resp<>''"
                
        jobs = ([sql.format(table), "import"] for table in u.getSubTables("visites"))  # noqa
        resultat = u.multiprocess(_get_data, jobs, 8)
        print(resultat)
        
    def find_minimum(self):
        """."""
        u.printTime("find_minimum")
        u.execute("CREATE INDEX index_sector ON permanent.long_temps_extended_rip(sector)", 'permanent')
        self.minimums_dates = {}
        self.maximums_dates = {}
        for s in u.sectors:
            print(s)
            sql = """select id_cip_sec, numcol, data_min, data_max 
                        from permanent.long_temps_extended_rip
                        where sector = '{}'""".format(s)
            for id, col, data_min, data_max in u.getAll(sql, 'permanent'):
                if id in self.pob:
                    if (id, col[0:8]) in self.minimums_dates:
                        d1 = self.minimums_dates[(id, col[0:8])]
                        if data_min < d1:
                            self.minimums_dates[(id, col[0:8])] = data_min
                    else:
                        self.minimums_dates[(id,  col[0:8])] = data_min
                    if (id, col[0:8]) in self.maximums_dates:
                        d1 = self.maximums_dates[(id, col[0:8])]
                        if data_max > d1:
                            self.maximums_dates[(id, col[0:8])] = data_max
                    else:
                        self.maximums_dates[(id,  col[0:8])] = data_max

    def get_taula(self):
        """Posem a cada pacient segons professional assignat a la seva up i uba la primera visita"""
        u.printTime("Càlculs")
        self.upload = []
        for id, (up, uba, edat, sexe) in self.pob.items():
            colM = self.responsable[(up, 'M', uba)] if (up, 'M', uba) in self.responsable else None # ok
            primera_visitaM = self.minimums_dates[(id, colM)] if (id, colM) in self.minimums_dates else None
            ultima_visitaM = self.maximums_dates[(id, colM)] if (id, colM) in self.maximums_dates else None
            self.upload.append([id, up, uba, edat, sexe, colM, primera_visitaM, ultima_visitaM])
    
    def export_files(self):
        """export a permanent"""
        u.printTime("export")
        tb = "long_temps_rip"
        cols = ("id_cip_sec int", "up varchar(5)", "uba varchar(5)", 
                    "edat int", "sexe varchar(1)",
                    "numcolM varchar(10)",  "primera_visitaM date", "ultima_visitaM date")
        u.createTable(tb, "({})".format(", ".join(cols)), db, rm=True)
        u.listToTable(self.upload, tb, db)  


def _get_data(param):
    """."""
    print('multiprocess')
    print(param)
    u.printTime("_get_data")
    if DEBUG:
        param[0] += " limit 1000"
    visitespacients_min = {}
    visitespacients_max = {}
    for id, sector, col, data in u.getAll(*param):
        if id in poblacio:
            if (id, sector, col[0:8]) in visitespacients_min:
                d1 = visitespacients_min[(id, sector, col[0:8])]
                if data < d1:
                    visitespacients_min[(id, sector, col[0:8])]= data
            else:
                visitespacients_min[(id, sector, col[0:8])]=data
            if (id, sector, col[0:8]) in visitespacients_max:
                d1 = visitespacients_max[(id, sector, col[0:8])]
                if data < d1:
                    visitespacients_max[(id, sector, col[0:8])]= data
            else:
                visitespacients_max[(id, sector, col[0:8])]=data
    upload = []
    for key, data in visitespacients_min.items():
        row = [key[0], key[1], key[2], data]
        if key in visitespacients_max:
            data_max = visitespacients_max[key]
        else: data_max = None
        row.append(data_max)
        upload.append(row)
    u.listToTable(upload, 'long_temps_extended_rip', db)
    print('end multiprocess')
    return 1

if __name__ == '__main__':
    executar = False
    if True:
        if executar:
            sql = "select id_cip_sec, up, uba, edat, sexe from nodrizas.rip_assignada"
            poblacio = {id: (up, uba, edat, sexe) for (id, up, uba, edat, sexe) in u.getAll(sql, 'nodrizas')}
            u.printTime("Inici")
            
            Continuitat()

            u.printTime("Fi")