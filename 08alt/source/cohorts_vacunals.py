# coding: latin1

"""
Vacunes segons any de naixement
"""

import urllib as w
import os
import sys,datetime
from time import strftime
from collections import defaultdict,Counter

import sisapUtils as u
import sisaptools as t


db = ("shiny", "maia")

codis_vacunes = { 'Triple v�rica (XRP)': 38,
                  'T�tanus': 49,
                  'Poliomieltitis (VPI)': 311,
                  'Haemophilus influenzae (Hib)': 312,
                  'Hepatitis B': 15,
                  'Meningoc�ccica invasora serogrup C (MCC)': 313,
                  'Malaltia neumoc�ccica invasora 13 valent(VNC13)': 977,
                  'Varicel�la (VVZ)': 314,
                  'Virus del papil�loma hum� (VPH)': 782,
                  'Hepatitis A': 34,
                  'Rotavirus': 790,
                  'Tos ferina': 631,
                  'Malaltia neumoc�ccica invasora 23 valent(VNC23)': 48,
                  'Malaltia meningoc�ccica invasora serogrup B': 548,
                  'Malaltia meningoc�ccica tetravalent (MACYW)': 883,
                  'Herpes zoster': 920,
                  'Malaltia neumoc�ccica invasora 20 valent(VNC20)': 978,
                  }   

class vacunesCohorts(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_rs()
        self.get_centres()
        self.get_vacunes()
        self.get_pob()        
        self.export_data()
        self.export_cataleg()
        self.table_dates()
        
    def get_rs(self):
        """."""
        self.rs_cat = {}
        sql = 'select rs_cod, rs_des from cat_sisap_agas'
        for rs, des in u.getAll(sql, 'import'):
            self.rs_cat[int(rs)] = des
    
    def get_centres(self):
        """centres ICS"""
        self.centres = {}
        upload = []
        sql = "select scs_codi, ics_codi, ics_desc, right(rs, 2), amb_desc, ep from cat_centres"
        for up, br, desc, rs, ambit, ep in u.getAll(sql, 'nodrizas'):
            self.centres[up] = br
            try:
                descRS = self.rs_cat[int(rs)]
            except:
                descRS = 'SENSE'
            upload.append([up, br, desc, descRS, ambit, ep])
           
        t.Database(*db).list_to_table(upload, tb_eap)
    
    def get_vacunes(self):
        """aconseguim vacunes"""
        self.vacunats = {}
        sql = 'select id_cip_sec, agrupador, dosis from ped_vacunes'
        for id, agr, dosi in u.getAll(sql, 'nodrizas'):
            self.vacunats[(id, agr)] = dosi

        sql = 'select id_cip_sec, agrupador, dosis from eqa_vacunes'
        for id, agr, dosi in u.getAll(sql, 'nodrizas'):
            self.vacunats[(id, agr)] = dosi
                
    def get_pob(self):
        """agafem població segons criteris edat"""
        self.resultats = Counter()
        sql = "select id_cip_sec, sexe, year(data_naix), up, ates, institucionalitzat from assignada_tot"
        for id, sexe, any_naix, up, ates, resis in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                for c in codis_vacunes:
                    agr = codis_vacunes[c]
                    self.resultats[(ates, resis, any_naix, up, sexe, agr, 'den')] += 1
                    if (id, agr) in self.vacunats:
                        dosi = self.vacunats[(id, agr)]
                        self.resultats[(ates, resis, any_naix, up, sexe,agr, 'num1')] += 1 if dosi > 0 else 0
                        self.resultats[(ates, resis, any_naix, up, sexe, agr, 'num2')] += 1 if dosi > 1 else 0
                        self.resultats[(ates, resis, any_naix, up, sexe, agr, 'num3')] += 1 if dosi > 2 else 0
                        self.resultats[(ates, resis, any_naix, up, sexe, agr, 'num4')] += 1 if dosi > 3 else 0
                        self.resultats[(ates, resis, any_naix, up, sexe, agr,'num5')] += 1 if dosi > 4 else 0
                        self.resultats[(ates, resis, any_naix, up, sexe, agr,'num6')] += 1 if dosi > 5 else 0
                        self.resultats[(ates, resis, any_naix, up, sexe, agr,'num7')] += 1 if dosi > 6 else 0
                        self.resultats[(ates, resis, any_naix, up, sexe, agr,'num8')] += 1 if dosi > 7 else 0
                            
    def export_data(self):
        """."""
        upload = []
        for (ates, resis, any_naix, up, sexe, agr, tip), n in self.resultats.items():
            if tip == 'den':
                num1, num2, num3, num4, num5, num6, num7, num8 = self.resultats[(ates, resis, any_naix, up, sexe, agr, 'num1')],self.resultats[(ates, resis, any_naix,up, sexe, agr, 'num2')],self.resultats[(ates, resis, any_naix,up, sexe, agr, 'num3')],self.resultats[(ates, resis, any_naix,up, sexe, agr, 'num4')],self.resultats[(ates, resis, any_naix,up, sexe, agr, 'num5')],self.resultats[(ates, resis, any_naix,up, sexe, agr, 'num6')],self.resultats[(ates, resis, any_naix,up, sexe, agr, 'num7')],self.resultats[(ates, resis, any_naix,up, sexe, agr, 'num8')]
                upload.append([ates, resis, any_naix, up, sexe, agr, num1, num2, num3, num4, num5, num6, num7, num8, n])

        t.Database(*db).list_to_table(upload, tb)   
        
    def export_cataleg(self):
        """."""
        upload = []
        for c in codis_vacunes:
            agr = codis_vacunes[c]
            upload.append([agr, c])
            
        t.Database(*db).list_to_table(upload, cataleg)

    def table_dates(self):
        """."""
        upload = []
        sql = "select data_ext from dextraccio"
        for data_ext, in u.getAll(sql, 'nodrizas'):
            upload.append([data_ext])
           
        t.Database(*db).list_to_table(upload, tb_date)

   
if __name__ == '__main__':
    u.printTime("Inici")
    
    tb = "mst_cohorts_vacunals"
    columns =  ["ates int", "residents int", "Cohort int", "up varchar(5)", "sexe varchar(10)", "codi_vacuna int", "1dosi int", "2dosis int", "3dosis int", "4dosis int", "5dosis int", "6dosis int", "7dosis int", "8dosis int", "poblacio int"]     
    t.Database(*db).create_table(tb, columns, remove=True)
    
    cataleg = "mst_cohorts_cataleg"
    columns = ["codi_vacuna int", "literal varchar(300)"]
    t.Database(*db).create_table(cataleg, columns, remove=True)
    
    tb_date = "mst_cohorts_data"
    columns = ["data_calcul date"]
    t.Database(*db).create_table(tb_date, columns, remove=True)
    
    tb_eap = "mst_cohorts_eaps"
    columns = ["up varchar(5)", "br varchar(5)", "descripcio varchar(500)", "RS varchar(100)", "ambit varchar(100)", "ep varchar(5)"]
    t.Database(*db).create_table(tb_eap, columns, remove=True)
    
    vacunesCohorts()
    
    u.printTime("Fi")