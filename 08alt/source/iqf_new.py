# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


TABLE = "IQF_NEW"
TABLE_PUNTS = "IQF_NEW_PUNTS"
TABLE_UBA = "IQF_NEW_UBA"
TABLE_PUNTS_UBA = "IQF_NEW_PUNTS_UBA"

converters = {
    'BIOSIMILARS_EAP': 'BIOSIMILARS_EAP',
    'GLARGINA': 'GLARGINA',
    'ENOXAPARINA': 'ENOXAPARINA',
    'ANTIHIPERTENSIUS': 'ANTIHIPERTENSIUS',
    'IBP': 'IBP',
    'OSTEOPOROSI': 'OSTEOPOROSI',
    'HIPOCOLESTEROLEMIANTS': 'HIPOCOLESTEROLEMIANTS',
    'ANTIDEPRESSIUS_1A': 'ANTIDEPRESSIUS_1A',
    'ANTIDEPRESSIUS_2A': 'ANTIDEPRESSIUS_2A',
    'HIPOGLUCEMIANTS_MONOTERAPIA_RECOMANADA': 'HIPOGLUCEMIANTS_MONOTERAPIA_REC',
    'HIPOGLUCEMIANTS_BITERAPIA_RECOMANADA': 'HIPOGLUCEMIANTS_BITERAPIA_REC',
    'MPOC_SELECCIO': 'MPOC_SELECCIO',
    'MATMA_EAP': 'MATMA_EAP',
    'MATMA_DINAMICA': 'MATMA_DINAMICA',
    'DHD_ST_AINES': 'DHD_ST_AINES',
    'DHD_ST_SYSADOA': 'DHD_ST_SYSADOA',
    'DHD_ST_ANTIULCEROSOS': 'DHD_ST_ANTIULCEROSOS',
    'DHD_ST_BENZODIAZEPINES': 'DHD_ST_BENZODIAZEPINES',
    'DHD_ST_ANTIBACTERIANS': 'DHD_ST_ANTIBACTERIANS',
    'DHD_ST_ANTIESPASMODICS': 'DHD_ST_ANTIESPASMODICS',
    'DHD_AINES': 'DHD_AINES', 
    'DHD_SYSADOA': 'DHD_SYSADOA',
    'DHD_ANTIULCEROSOS': 'DHD_ST_ANTIULCEROSOS',
    'DHD_BENZODIAZEPINES': 'DHD_ST_BENZODIAZEPINES',
    'DHD_ANTIBACTERIANS': 'DHD_ST_ANTIBACTERIANS',
    'DHD_ANTIESPASMODICS': 'DHD_ST_ANTIESPASMODICS'
}

converters_punts = {
    'PUNTUACIO_BIOSIMILARS_EAP': 'BIOSIMILARS_EAP',
    'PUNTUACIO_GLARGINA': 'GLARGINA',
    'PUNTUACIO_ENOXAPARINA': 'ENOXAPARINA',
    'PUNTUACIO_ANTIHIPERTENSIUS': 'ANTIHIPERTENSIUS',
    'PUNTUACIO_IBP': 'IBP',
    'PUNTUACIO_OSTEOPOROSI': 'OSTEOPOROSI',
    'PUNTUACIO_HIPOCOLESTEROLEMIANTS': 'HIPOCOLESTEROLEMIANTS',
    'PUNTUACIO_ANTIDEPRESSIUS_1A': 'ANTIDEPRESSIUS_1A',
    'PUNTUACIO_ANTIDEPRESSIUS_2A': 'ANTIDEPRESSIUS_2A',
    'PUNTUACIO_HIPOGLUCEMIANTS_MONOTERAPIA_RECOMANADA': 'HIPOGLUCEMIANTS_MONOTERAPIA_REC',
    'PUNTUACIO_HIPOGLUCEMIANTS_BITERAPIA_RECOMANADA': 'HIPOGLUCEMIANTS_BITERAPIA_REC',
    'PUNTUACIO_MPOC_SELECCIO': 'MPOC_SELECCIO',
    'PUNTUACIO_MATMA_EAP': 'MATMA_EAP',
    'PUNTUACIO_MATMA_DINAMICA': 'MATMA_DINAMICA',
    'PUNTUACIO_AINES': 'DHD_ST_AINES',
    'PUNTUACIO_SYSADOA': 'DHD_ST_SYSADOA',
    'PUNTUACIO_ANTIULCEROSOS': 'DHD_ST_ANTIULCEROSOS',
    'PUNTUACIO_BENZODIAZEPINES': 'DHD_ST_BENZODIAZEPINES',
    'PUNTUACIO_ANTIBACTERIANS': 'DHD_ST_ANTIBACTERIANS',
    'PUNTUACIO_ANTIESPASMODICS': 'DHD_ST_ANTIESPASMODICS'
}


class IQF_New():
    def __init__(self):
        self.indicadors()
        self.indicadors_uba()
        self.punts()
    
    def indicadors(self):
        to_khalix = []
        cuinetes = c.defaultdict(c.defaultdict)
        cuines_sumatoris = c.defaultdict(c.Counter)
        mesos = set()
        sql = """SELECT
                    T2.UP_AGRUPADA_VIGENT_CODI,
                    T1.MES,
                    T1.INDICADOR,
                    T1.TIPUS,
                    SUM(T1.VALOR)
                FROM
                    dwcatsalut.INDICADORS_IQF T1
                LEFT JOIN dwcatsalut.DIM_MAPA_UP T2
                    ON T1.UP = T2.UP_CODI
                WHERE
                    T1.INDICADOR IN ('BIOSIMILARS_EAP', 'GLARGINA', 'ENOXAPARINA',
                                'ANTIHIPERTENSIUS', 'IBP', 'OSTEOPOROSI', 'HIPOCOLESTEROLEMIANTS',
                                'ANTIDEPRESSIUS_1A', 'ANTIDEPRESSIUS_2A', 'MATMA_EAP', 'MATMA_DINAMICA')
                    AND T1.anyo = (select max(anyo) from dwcatsalut.INDICADORS_IQF)
                    AND length(T1.up) = 5
                    AND T1.ES_BASAL = '0'
                GROUP BY
                    T2.UP_AGRUPADA_VIGENT_CODI,
                    T1.MES,
                    T1.INDICADOR,
                    T1.TIPUS"""
        for up, mes, indi, tipus, val in u.getAll(sql, 'exadata'):
            mesos.add(int(mes))
            cuinetes[(up, converters[indi], tipus[:3])][mes] = val
        
        for up, indi, tipus in cuinetes.keys():
            for mes_data in cuinetes[(up, indi, tipus)]:
                for mes in mesos:
                    if mes >= int(mes_data):
                        val_mes = cuinetes[(up, indi, tipus)][mes_data]
                        cuines_sumatoris[(up, indi, tipus)][mes] += val_mes

        for (up, indi, tipus), dict_mesos in cuines_sumatoris.items():
            for mes, val in dict_mesos.items():
                to_khalix.append((up, 'A' + str(mes)[2:], indi, tipus, val))
                
        sql = """SELECT
                    T2.UP_AGRUPADA_VIGENT_CODI,
                    T1.MES,
                    T1.INDICADOR,
                    T1.TIPUS,
                    SUM(T1.VALOR)
                FROM
                    dwcatsalut.INDICADORS_IQF T1
                LEFT JOIN dwcatsalut.DIM_MAPA_UP T2
                    ON T1.UP = T2.UP_CODI
                WHERE
                    T1.INDICADOR IN ('HIPOGLUCEMIANTS_MONOTERAPIA_RECOMANADA',
                                'HIPOGLUCEMIANTS_BITERAPIA_RECOMANADA', 'MPOC_SELECCIO')
                    AND T1.anyo = (select max(anyo) from dwcatsalut.INDICADORS_IQF)
                    AND length(T1.up) = 5
                    AND T1.ES_BASAL = '0'
                GROUP BY
                    T2.UP_AGRUPADA_VIGENT_CODI,
                    T1.MES,
                    T1.INDICADOR,
                    T1.TIPUS"""
        for up, mes, indi, tipus, val in u.getAll(sql, 'exadata'):
            to_khalix.append((up, 'A' + mes[2:], converters[indi], tipus[:3], val))

        sql = """SELECT
            UP_AGRUPADA_VIGENT_CODI,
            mes,
            INDICADOR,
            TIPUS,
            SUM(VALOR)
        FROM
            dwcatsalut.INDICADORS_IQF_UP_ACUM t1
        LEFT JOIN dwcatsalut.DIM_MAPA_UP T2
            ON T1.UP = T2.UP_CODI
        WHERE
            INDICADOR IN ('DHD_ST_AINES', 'DHD_ST_SYSADOA', 'DHD_ST_ANTIULCEROSOS', 'DHD_ST_BENZODIAZEPINES', 'DHD_ST_ANTIBACTERIANS', 'DHD_ST_ANTIESPASMODICS')
            AND anyo = (select max(anyo) from dwcatsalut.INDICADORS_IQF)
            AND length(up) = 5
            and ES_BASAL = '0'
        GROUP BY
            UP_AGRUPADA_VIGENT_CODI,
            MES,
            INDICADOR,
            TIPUS"""
        for up, mes, indi, tipus, val in u.getAll(sql, 'exadata'):
            to_khalix.append((up, 'A' + mes[2:], converters[indi], tipus[:3], val))

        cols = """(up varchar(5), periode varchar(10), indicador varchar(255), tipus varchar(3), valor int)"""
        u.createTable(TABLE, cols, 'altres', rm=True)
        u.listToTable(to_khalix, TABLE, 'altres')
        sql = """select distinct indicador,
                    periode,
                    ics_codi,
                    tipus,
                    'NOCAT',
                    'NOIMP',
                    'DIM6SET',
                    'N',
                    valor from altres.iqf_new p inner join nodrizas.cat_centres cc
                ON
                    p.up = cc.scs_codi"""
        u.exportKhalix(sql, 'IQF_NEW', force=True)
    
    def indicadors_uba(self):
        to_khalix = []
        cuinetes = c.defaultdict(c.defaultdict)
        cuines_sumatoris = c.defaultdict(c.Counter)
        mesos = set()
        print('lets go 1')
        sql = """SELECT
                    T2.UP_AGRUPADA_VIGENT_CODI,
                    T1.MES,
                    T1.INDICADOR,
                    T1.TIPUS,
                    p.uab,
                    p.tipus,
                    SUM(VALOR)
                FROM
                    dwcatsalut.INDICADORS_IQF T1
                LEFT JOIN dwcatsalut.DIM_MAPA_UP T2
                    ON T1.UP = T2.UP_CODI
                INNER JOIN dwsisap.PROFESSIONALS p 
                	ON SUBSTR(p.IDE_NUMCOL,1,8) = SUBSTR(t1.professional_assistencial,1,8)
                WHERE
                    T1.INDICADOR IN ('BIOSIMILARS_EAP', 'GLARGINA', 'ENOXAPARINA',
                                'ANTIHIPERTENSIUS', 'IBP', 'OSTEOPOROSI', 'HIPOCOLESTEROLEMIANTS',
                                'ANTIDEPRESSIUS_1A', 'ANTIDEPRESSIUS_2A', 'MATMA_EAP', 'MATMA_DINAMICA')
                    AND T1.anyo = (select max(anyo) from dwcatsalut.INDICADORS_IQF)
                    AND length(T1.up) = 5
                    AND T1.ES_BASAL = '0'
                    AND p.uab IS NOT null
                    AND p.TIPUS = 'M'
                GROUP BY
                    T2.UP_AGRUPADA_VIGENT_CODI,
                    T1.MES,
                    T1.INDICADOR,
                    T1.TIPUS,
                    p.uab,
                    p.tipus"""
        for up, mes, indi, tipus, uba, prof, val in u.getAll(sql, 'exadata'):
            mesos.add(int(mes))
            cuinetes[(up, converters[indi], tipus[:3], uba, prof)][mes] = val
        
        for up, indi, tipus, uba, prof in cuinetes.keys():
            for mes_data in cuinetes[(up, indi, tipus, uba, prof)]:
                for mes in mesos:
                    if mes >= int(mes_data):
                        val_mes = cuinetes[(up, indi, tipus, uba, prof)][mes_data]
                        cuines_sumatoris[(up, indi, tipus, uba, prof)][mes] += val_mes

        for (up, indi, tipus, uba, prof), dict_mesos in cuines_sumatoris.items():
            for mes, val in dict_mesos.items():
                to_khalix.append((up, uba, prof, 'A' + str(mes)[2:], indi, tipus, val))
        print('lets go 2')    
        sql = """SELECT
                    T2.UP_AGRUPADA_VIGENT_CODI,
                    T1.MES,
                    T1.INDICADOR,
                    T1.TIPUS,
                    p.uab,
                    p.tipus,
                    SUM(VALOR)
                FROM
                    dwcatsalut.INDICADORS_IQF T1
                LEFT JOIN dwcatsalut.DIM_MAPA_UP T2
                    ON T1.UP = T2.UP_CODI
                INNER JOIN dwsisap.PROFESSIONALS p 
                	ON SUBSTR(p.IDE_NUMCOL,1,8) = SUBSTR(t1.professional_assistencial,1,8)
                WHERE
                    T1.INDICADOR IN ('HIPOGLUCEMIANTS_MONOTERAPIA_RECOMANADA',
                                'HIPOGLUCEMIANTS_BITERAPIA_RECOMANADA', 'MPOC_SELECCIO')
                    AND T1.anyo = (select max(anyo) from dwcatsalut.INDICADORS_IQF)
                    AND length(T1.up) = 5
                    AND T1.ES_BASAL = '0'
                    AND p.uab IS NOT null
                    AND p.TIPUS = 'M'
                GROUP BY
                    T2.UP_AGRUPADA_VIGENT_CODI,
                    T1.MES,
                    T1.INDICADOR,
                    T1.TIPUS,
                    p.uab,
                    p.tipus"""
        for up, mes, indi, tipus, uba, prof, val in u.getAll(sql, 'exadata'):
            to_khalix.append((up, uba, prof, 'A' + mes[2:], converters[indi], tipus[:3], val))

        print('lets go 3')
        sql = """SELECT
                    UP_AGRUPADA_VIGENT_CODI,
                    mes,
                    INDICADOR,
                    t1.TIPUS,
                    p.uab,
                    p.tipus,
                    SUM(VALOR)
                FROM
                    dwcatsalut.INDICADORS_IQF_UP_PROF_ACUM t1
                LEFT JOIN dwcatsalut.DIM_MAPA_UP T2
                    ON T1.UP = T2.UP_CODI
                INNER JOIN dwsisap.PROFESSIONALS p 
                    ON SUBSTR(p.IDE_NUMCOL,1,8) = SUBSTR(t1.professional_assistencial,1,8)    
                WHERE
                    INDICADOR IN ('DHD_AINES', 'DHD_SYSADOA', 'DHD_ANTIULCEROSOS', 'DHD_BENZODIAZEPINES', 'DHD_ANTIBACTERIANS', 'DHD_ANTIESPASMODICS')
                    AND anyo = (select max(anyo) from dwcatsalut.INDICADORS_IQF)
                    AND length(t1.up) = 5
                    and ES_BASAL = '0'
                    AND p.uab IS NOT null
                    AND p.TIPUS = 'M'
                GROUP BY
                    UP_AGRUPADA_VIGENT_CODI,
                    MES,
                    INDICADOR,
                    T1.TIPUS,
                    p.uab,
                    p.tipus"""
        for up, mes, indi, tipus, uba, prof, val in u.getAll(sql, 'exadata'):
            to_khalix.append((up, uba, prof, 'A' + mes[2:], converters[indi], tipus[:3], val))

        print('upload')
        cols = """(up varchar(5), uba varchar(5), prof varchar(2), periode varchar(10), indicador varchar(255), tipus varchar(3), valor int)"""
        u.createTable(TABLE_UBA, cols, 'altres', rm=True)
        u.listToTable(to_khalix, TABLE_UBA, 'altres')
        sql = """select distinct indicador,
                    periode,
                    concat(concat(ics_codi, prof), MU.uba),
                    p.tipus,
                    'NOCAT',
                    'NOIMP',
                    'DIM6SET',
                    'N',
                    valor from altres.IQF_NEW_UBA p inner join nodrizas.cat_centres cc
                ON
                    p.up = cc.scs_codi
                inner join eqa_ind.mst_ubas mu
             	on p.up = mu.up and p.uba = mu.uba
                where p.uba is not null and mu.tipus = 'M'"""
        u.exportKhalix(sql, 'IQF_NEW_UBA', force=True)

    def punts(self):
        print('punts uba')
        punts = set()
        sql = """
            SELECT
                t2.UP_AGRUPADA_VIGENT_CODI,
                MES,
                INDICADOR,
                t1.TIPUS,
                p.uab,
                p.tipus,
                t1.VALOR
            FROM
                dwcatsalut.INDICADORS_IQF_UP_PROF_ACUM t1
            LEFT JOIN dwcatsalut.DIM_MAPA_UP T2
                        ON T1.UP = T2.UP_CODI
                INNER JOIN dwsisap.PROFESSIONALS p 
                        ON SUBSTR(p.IDE_NUMCOL,1,8) = SUBSTR(t1.professional_assistencial,1,8)
            WHERE
                INDICADOR IN ('PUNTUACIO_BIOSIMILARS_EAP',
                'PUNTUACIO_GLARGINA', 'PUNTUACIO_ENOXAPARINA',
                'PUNTUACIO_AINES', 'PUNTUACIO_SYSADOA', 
                'PUNTUACIO_ANTIULCEROSOS', 'PUNTUACIO_BENZODIAZEPINES',
                'PUNTUACIO_ANTIBACTERIANS', 'PUNTUACIO_ANTIESPASMODICS',
                'PUNTUACIO_ANTIHIPERTENSIUS', 'PUNTUACIO_IBP',
                'PUNTUACIO_OSTEOPOROSI', 'PUNTUACIO_HIPOCOLESTEROLEMIANTS',
                'PUNTUACIO_ANTIDEPRESSIUS_1A', 'PUNTUACIO_ANTIDEPRESSIUS_2A',
                'PUNTUACIO_HIPOGLUCEMIANTS_MONOTERAPIA_RECOMANADA', 
                'PUNTUACIO_HIPOGLUCEMIANTS_BITERAPIA_RECOMANADA',
                'PUNTUACIO_MPOC_SELECCIO', 'PUNTUACIO_MATMA_EAP',
                'PUNTUACIO_MATMA_DINAMICA')
                AND length(p.up) = 5
                AND p.uab IS NOT NULL
                AND p.tipus = 'M'
                AND MES IN (
                SELECT
                    MAX(MES)
                FROM
                    dwcatsalut.INDICADORS_IQF_UP_ACUM)"""
        for up, mes, indi, tipus, uab, prof, valor in u.getAll(sql, 'exadata'):
            punts.add((converters_punts[indi], 'A' + mes[2:], up, uab, prof, 'AGASSOL', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', valor))

        cols = """(indicador varchar(255), periode varchar(10), up varchar(5), uba varchar(5), prof varchar(10), agassol varchar(10), NOCAT varchar(10), NOIMP varchar(10), DIM6SET varchar(10), N varchar(1), valor int)"""
        u.createTable(TABLE_PUNTS_UBA, cols, 'altres', rm=True)
        u.listToTable(list(punts), TABLE_PUNTS_UBA, 'altres')
        sql = """select 
                distinct indicador,
                                    periode,
                                    concat(concat(ics_codi, prof), MU.uba),
                                    'AGASSOL',
                                    'NOCAT',
                                    'NOIMP',
                                    'DIM6SET',
                                    'N',
                                    SUM(valor)
                from
                altres.IQF_NEW_PUNTS_UBA  p inner join nodrizas.cat_centres cc
                                ON
                                    p.up = cc.scs_codi
                                inner join eqa_ind.mst_ubas mu
                                on p.up = mu.up and p.uba = mu.uba
                                where p.uba is not null and mu.tipus = 'M'
                GROUP BY indicador,
                                    periode,
                                    concat(concat(ics_codi, prof), MU.uba)"""
        u.exportKhalix(sql, 'IQF_NEW_PUNTS_UBA', force=True)

        print('punts up')
        punts = set()
        sql = """SELECT
            UP,
            MES,
            INDICADOR,
            TIPUS,
            VALOR
        FROM
            dwcatsalut.INDICADORS_IQF_UP_ACUM
        WHERE
            INDICADOR IN ('PUNTUACIO_BIOSIMILARS_EAP',
            'PUNTUACIO_GLARGINA', 'PUNTUACIO_ENOXAPARINA',
            'PUNTUACIO_AINES', 'PUNTUACIO_SYSADOA', 
            'PUNTUACIO_ANTIULCEROSOS', 'PUNTUACIO_BENZODIAZEPINES',
            'PUNTUACIO_ANTIBACTERIANS', 'PUNTUACIO_ANTIESPASMODICS',
            'PUNTUACIO_ANTIHIPERTENSIUS', 'PUNTUACIO_IBP',
            'PUNTUACIO_OSTEOPOROSI', 'PUNTUACIO_HIPOCOLESTEROLEMIANTS',
            'PUNTUACIO_ANTIDEPRESSIUS_1A', 'PUNTUACIO_ANTIDEPRESSIUS_2A',
            'PUNTUACIO_HIPOGLUCEMIANTS_MONOTERAPIA_RECOMANADA', 
            'PUNTUACIO_HIPOGLUCEMIANTS_BITERAPIA_RECOMANADA',
            'PUNTUACIO_MPOC_SELECCIO', 'PUNTUACIO_MATMA_EAP',
            'PUNTUACIO_MATMA_DINAMICA')
            AND length(up) = 5
            AND MES IN (
            SELECT
                MAX(MES)
            FROM
                dwcatsalut.INDICADORS_IQF_UP_ACUM)"""
        for up, mes, indi, tipus, valor in u.getAll(sql, 'exadata'):
            punts.add((converters_punts[indi], 'A' + mes[2:], up, 'AGASSOL', 'NOCAT', 'NOIMP', 'DIM6SET', 'N', valor))

        cols = """(indicador varchar(255), periode varchar(10), up varchar(5), tipus varchar(10), NOCAT varchar(10), NOIMP varchar(10), DIM6SET varchar(10), N varchar(1), valor int)"""
        u.createTable(TABLE_PUNTS, cols, 'altres', rm=True)
        u.listToTable(list(punts), TABLE_PUNTS, 'altres')
        sql = """select
                    indicador,
                    periode,
                    ics_codi,
                    tipus,
                    nocat,
                    noimp,
                    dim6set,
                    n,
                    valor
                from
                    altres.iqf_new_punts p
                inner join nodrizas.cat_centres cc
                ON
                    p.up = cc.scs_codi"""
        u.exportKhalix(sql, 'IQF_NEW_PUNTS', force=True)


class SISAP_ECAP():
    def __init__(self):
        self.get_punts()
        self.catalegs()
        self.indicadors()
        self.llistats()
    
    def get_punts(self):
        FILE_PUNTS = "./dades_noesb/iqf_new_punts.txt"
        self.punts = {indicador: punts for indicador, punts in u.readCSV(FILE_PUNTS, sep='@')}
    
    def catalegs(self):
        cataleg_pare = [
            ('IQFUNI', 'Indicadors universals', 2),
            ('IQFDHD', u'Indicadors d hiperprescripcio', 3),
            ('IQFSEL', u'Indicadors de seleccio', 4),
            ('NTEAP', u'Novetats terapeutiques', 1),
        ]
        cols = """(pare varchar(100), literal varchar(100), ordre int)"""
        u.createTable('exp_ecap_iqf_catpare', cols, 'altres', rm=True)
        u.listToTable(cataleg_pare, 'exp_ecap_iqf_catpare', 'altres')
        table = """iqfcatalegpare"""
        query = """select pare, literal, ordre from altres.exp_ecap_iqf_catpare"""
        u.exportPDP(query=query, table=table, datAny=True,fmo=False)

        cataleg = [
            ('GLARGINA', 'Utilitzaci� d\'insulina glargina biosimilar', 1, 'IQFUNI', 1, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4412/ver/'),
            ('ENOXAPARINA', 'Utilitzaci� d\'enoxaparina biosimilar', 2, 'IQFUNI', 1, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4414/ver/'),
            ('BIOSIMILARS_EAP', 'Utilitzaci� de medicaments biosimilars', 3, 'IQFUNI', 1, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5691/ver/'),

            ('DHD_AINES', 'Hiperprescripci� d\'AINE i d\'altres medicaments per a patologies musculoesquel�tiques', 1, 'IQFDHD', 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4416/ver/'),
            ('DHD_SYSADOA', 'Hiperprescripci� de f�rmacs d\'acci� lenta sobre els smptomes de l\'artrosi (SYSADOA)', 2, 'IQFDHD', 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4417/ver/'),
            ('DHD_ST_ANTIULCEROSOS', 'Hiperprescripci� d\'antiulcerosos', 3, 'IQFDHD', 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4418/ver/'),
            ('DHD_ST_BENZODIAZEPINES', 'Hiperprescripci� de benzodiazepines i f�rmacs relacionats', 4, 'IQFDHD', 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4419/ver/'),
            ('DHD_ST_ANTIBACTERIANS', 'Hiperprescripci� d\'antibacterians d\'�s sist�mic', 5, 'IQFDHD', 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4420/ver/'),
            ('DHD_ST_ANTIESPASMODICS', 'Hiperprescripci� d\'antiespasm�dics urinaris', 6, 'IQFDHD', 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4421/ver/'),

            ('ANTIHIPERTENSIUS', 'Utilitzaci� d\'antihipertensius recomanats', 1, 'IQFSEL', 1, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4422/ver/'),
            ('IBP', 'Utilitzaci� d\'inhibidors de la bomba de protons recomanats', 2, 'IQFSEL', 1, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4423/ver/'),
            ('OSTEOPOROSI', 'Utilitzaci� de medicaments per a l\'osteoporosi recomanats', 3, 'IQFSEL', 1, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5699/ver/'),
            ('HIPOCOLESTEROLEMIANTS', 'Utilitzaci� d\'hipocolesterolemiants recomanats', 4, 'IQFSEL', 1, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4425/ver/'),
            ('ANTIDEPRESSIUS_1A', 'Utilitzaci� d\'antidepressius de primera elecci� recomanats', 5, 'IQFSEL', 1, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4426/ver/'),
            ('ANTIDEPRESSIUS_2A', 'Utilitzaci� d\'antidepressius no ISRS de segona elecci� recomanats', 6, 'IQFSEL', 1, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4427/ver/'),
            ('MPOC_SELECCIO', 'Utilitzaci� de ter�pies inhalades triples amb LAMA, LABA i CI en pacients amb diagn�stic de MPOC', 7, 'IQFSEL', 1, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4430/ver/'),
            ('HIPOGLUCEMIANTS_BITERAPIA_REC', 'Combinacions recomanades de dos HNI en pacients DM2', 8, 'IQFSEL', 1, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5689/ver/'),
            ('HIPOGLUCEMIANTS_MONOTERAPIA_REC', 'F�rmacs recomanats en pacients DM2 amb un HNI', 9, 'IQFSEL', 1, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4424/ver/'),

            ('MATMA', 'Utilitzaci� medicaments sense valor terap�utic afegit amb alternatives m�s adequades (llista fixa)', 1, 'NTEAP', 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/4407/ver/')
        ]
        cols = """(indicador varchar(32), literal varchar(300), ordre_grup int, pare varchar(50), llistat int, toshow int, wiki varchar(1000))"""
        u.createTable('exp_ecap_iqf_cat', cols, 'altres', rm=True)
        u.listToTable(cataleg, 'exp_ecap_iqf_cat', 'altres')
        table = """iqfcataleg"""
        query = """select indicador, literal, ordre_grup, pare, llistat, toshow, wiki from altres.exp_ecap_iqf_cat"""
        u.exportPDP(query=query, table=table, datAny=True,fmo=False)  # cataleg

    def indicadors(self):
        cooking = c.defaultdict(dict)
        sql = """select
                    P.up UP,
                    P.uba UBA,
                    prof,
                    indicador,
                    P.tipus,
                    valor
                from
                    altres.IQF_NEW_UBA p
                inner join nodrizas.cat_centres cc
                            on
                    p.up = cc.scs_codi
                inner join eqa_ind.mst_ubas mu
                            on
                    p.up = mu.up
                    and p.uba = mu.uba
                where
                    p.uba is not null
                    and mu.tipus = 'M'
                    and PERIODE in (
                    select
                        MAX(PERIODE)
                    from
                        altres.IQF_NEW_UBA)"""
        for up, uba, prof, indi, tipus, val in u.getAll(sql, 'altres'):
            cooking[(up, uba, prof, indi)][tipus] = val
        

        sql = """select
                    distinct indicador,
                    p.up,
                    prof,
                    MU.uba,
                    SUM(valor)
                from
                    altres.IQF_NEW_PUNTS_UBA p
                inner join eqa_ind.mst_ubas mu
                                                on
                    p.up = mu.up
                    and p.uba = mu.uba
                where
                    p.uba is not null
                    and mu.tipus = 'M'
                    and periode in (
                    select
                        max(periode)
                    from
                        altres.IQF_NEW_PUNTS_UBA)
                group by
                    indicador,
                    p.up,
                    prof,
                    MU.uba"""
        for indi, up, prof, uba, val in u.getAll(sql, 'altres'):
            cooking[(up, uba, prof, indi)]['PUNTS'] = val
        
        upload = []
        for (up, uba, prof, indi), vals in cooking.items():
            num = vals['NUM'] if 'NUM' in vals else 0
            den = vals['DEN'] if 'DEN' in vals else 0
            punts = vals['PUNTS'] if 'PUNTS' in vals else 0
            punts_max = int(self.punts[indi]) if indi in self.punts else 0
            resultat = num/den if den != 0 else 0
            situacio = 0
            if punts == 0: situacio = 1
            if punts != 0 and punts != punts_max: situacio = 2
            if punts == punts_max: situacio = 3
            if punts_max == -1: situacio = 0
            upload.append((up, uba, prof, indi, punts_max, punts, resultat, situacio))

        cols = """(up varchar(5), uba varchar(20)
                ,tipus varchar(1)
                ,indicador varchar(32)
                ,punts_max int
                ,punt int
                ,resultat int
                ,situacio int)"""
        u.createTable('exp_ecap_iqf_ind', cols, 'altres', rm=True)
        u.listToTable(upload, 'exp_ecap_iqf_ind', 'altres')
        table = """iqfindicadors"""
        query = """select * from altres.exp_ecap_iqf_ind where uba != ''"""
        u.exportPDP(query=query, table=table, dat=True,fmo=False)  # indicadors
    
    def llistats(self):
        # en un futur podem fer-ho per ATC, caldria concatenar-los
        table = """iqfLlistatsData"""
        query = """select data_ext from nodrizas.dextraccio"""
        u.exportPDP(query=query,table=table,truncate=True,fmo=False)

        cols = """(
            up varchar(5),
            uba varchar(5),
            tipus varchar(1),
            indicador varchar(32),
            hash varchar(40),
            sector varchar(4)
        )"""
        u.createTable('exp_ecap_iqf_pacients', cols, 'altres', rm=True)
        llistat = set()
        sql = """select c_up, dbs.C_METGE, 'M', hash_redics, indicador, c_sector 
                from DWCATSALUT.INDICADORS_IQF_DETALL_NO_COMPLIDORS nc
                INNER JOIN dwsisap.PDPTB101_RELACIO_nia c
                ON substr(nc.cip_pacient,1,13) = c.cip 
                INNER JOIN dwsisap.dbs dbs 
                ON c.HASH_COVID = dbs.C_CIP"""
        for up, uba, tipus, hash, indicador, sector in u.getAll(sql, 'exadata'):
            if indicador in converters:
                indicador_sisap = converters[indicador]
                llistat.add((up, uba, tipus, indicador_sisap, hash, sector))
        u.listToTable(llistat, 'exp_ecap_iqf_pacients', 'altres')
        
        table = """iqfLlistats"""
        query = """select * from altres.exp_ecap_iqf_pacients"""
        u.exportPDP(query=query,table=table,truncate=True,fmo=False)

if __name__ == "__main__":
    IQF_New()
    SISAP_ECAP()