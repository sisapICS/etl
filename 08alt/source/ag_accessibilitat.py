# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict, Counter


nod = 'nodrizas'
imp = 'import'
db = 'altres'

recompte = Counter()
upload = []
recompteUBA = Counter()
uploadUBA = []

def serveiConverter(servei):
    if servei == 'OMEDFAM' or servei == 'OPEDIAT':
        classe = 'M'
    elif servei == 'OINFERM':
        classe = 'I'
    else:
        classe = 'E'
    return classe
        

serveis = {cod: hom for (cod, hom) in getAll('select distinct servei, codi_khalix from cat_ct_der_serveio', imp)}

sql = 'select up, uba, servei, ofertada from ag_internet'
for up, uba, servei, ofertada in getAll(sql, nod):
    recompte['AGACCINT',up, servei, 'DEN'] += 1
    recompte['AGACCINT',up, servei, 'NUM'] += ofertada
    recompteUBA['AGACCINT',up, uba, serveiConverter(servei), servei, 'DEN'] += 1
    recompteUBA['AGACCINT',up, uba, serveiConverter(servei), servei, 'NUM'] += ofertada

sql = "select modu_codi_up, modu_servei_codi_servei, sum(if(modu_model_agenda = 2, 1, 0)) capes, sum(if(modu_internet = 'P9C', 1, 0))  internet, count(1) total \
from import.cat_vistb027 where modu_data_baixa = '47120101' and modu_codi_uab <> '' and modu_descripcio not like 'RG MUTUAM%' \
group by modu_codi_up, modu_servei_codi_servei"
for up, servei, capes, internet, total in getAll(sql, imp):
    serv = serveis.get(servei, 'OALTEAP')
    recompte['AGDACAPA', up, serv, 'DEN'] += total
    recompte['AGDACAPA', up, serv, 'NUM'] += capes
    recompte['AGDAINT', up, serv, 'DEN'] += total
    recompte['AGDAINT', up, serv, 'NUM'] += internet

sql = "select modu_codi_up, modu_codi_uab,modu_servei_codi_servei, sum(if(modu_model_agenda = 2, 1, 0)) capes, sum(if(modu_internet = 'P9C', 1, 0))  internet, count(1) total \
from import.cat_vistb027 where modu_data_baixa = '47120101' and modu_codi_uab <> '' and modu_descripcio not like 'RG MUTUAM%' \
group by modu_codi_up, modu_codi_uab,modu_servei_codi_servei"
for up, uba, servei, capes, internet, total in getAll(sql, imp):
    serv = serveis.get(servei, 'OALTEAP')
    recompteUBA['AGDACAPA', up, uba, serveiConverter(serv), serv, 'DEN'] += total
    recompteUBA['AGDACAPA', up, uba, serveiConverter(serv), serv, 'NUM'] += capes
    recompteUBA['AGDAINT', up, uba, serveiConverter(serv), serv, 'DEN'] += total
    recompteUBA['AGDAINT', up, uba, serveiConverter(serv), serv, 'NUM'] += internet

for (indicador, up, servei, tip), rec in recompte.items():
    upload.append([indicador, up, servei, tip, rec])
for (indicador,up, uba, classe, servei, tip), rec in recompteUBA.items():
    uploadUBA.append([indicador,up, uba, classe, servei, tip, rec])
    
table = 'exp_khalix_ag_accessibilitat'
create = "(indicador varchar(10), up varchar(5) not null default'', servei varchar(10), tipus varchar(10), recompte int)"
createTable(table,create,db, rm=True)
tableUBA = 'exp_khalix_ag_accessibilitat_uba'
createUBA = "(indicador varchar(10), up varchar(5) not null default'', uba varchar(10), classe varchar(1), servei varchar(10), tipus varchar(10), recompte int)"
createTable(tableUBA,createUBA,db, rm=True)

listToTable(upload, table, db)
listToTable(uploadUBA, tableUBA, db)


error = []
centres="export.khx_centres"
sql = "select  indicador, concat('A','periodo'), ics_codi, tipus, 'NOCAT', servei, 'DIM6SET','N',recompte from {0}.{1} a inner join {2} b on a.up=b.scs_codi".format(db, table, centres)
file = 'AG_ACC_UP'
error.append(exportKhalix(sql, file))

sql = "select  indicador, concat('A','periodo'),concat(ics_codi,classe,uba), tipus, 'NOCAT', servei, 'DIM6SET', 'N', recompte from {0}.{1} a inner join {2} b on a.up=b.scs_codi".format(db, tableUBA, centres)
file = 'AG_ACC_UBA'
error.append(exportKhalix(sql, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
