# coding: latin1

"""
Cataleg i Cataleg Pare de indicadors de qc_residencies
"""

import os
import simplejson as j

import sisapUtils as u


db = "altres"
conn = u.connect((db, 'aux'))
c = conn.cursor()

cataleg = 'exp_ecap_atdom_cataleg'
catalegPare = 'exp_ecap_atdom_catalegpare'


class CATALEGS(object):
    """."""

    def __init__(self):
        """."""
        self.get_json()
        self.get_catalegPare()
        self.get_cataleg()
        self.aux_tables()

    def get_json(self):
        filepath = ['dades_noesb', 'atdom.json']
        try:
            stream = open(os.sep.join(filepath))
        except IOError:
            stream = open(os.sep.join([".."] + filepath))
        self.catalegs = j.load(stream)

    def get_catalegPare(self):
        cataleg = self.catalegs['catalegPare']
        camps = ['PARE', 'LITERAL', 'ORDRE', 'PANTALLA']
        self.catPare = [tuple([cataleg[camp] for camp in camps])]
    
    def get_cataleg(self):
        cataleg = self.catalegs['cataleg']
        camps = [
            "INDICADOR",
            "LITERAL",
            "ORDRE",
            "PARE",
            "LLISTAT",
            "INVERS",
            "MMIN",
            "MINT",
            "MMAX",
            "TOSHOW",
            "WIKI",
            "TIPUSVALOR"
        ]
        self.catIndi = []
        for item in cataleg:
            self.catIndi.append(
                tuple([
                    item[camp].encode('latin-1')
                    if camp == "LITERAL"
                    else item[camp]
                    for camp in camps]
                    )
                )

    def aux_tables(self):
        for tb in [cataleg, catalegPare]:
            c.execute("drop table if exists {}".format(tb))

        c.execute("""
                create table {} (
                    indicador varchar(8),
                    literal varchar(300),
                    ordre int,
                    pare varchar(8),
                    llistat int,
                    invers int,
                    mmin double,
                    mint double,
                    mmax double,
                    toShow int,
                    wiki varchar(250),
                    tipusvalor varchar(5))
                """.format(cataleg)
                )

        c.execute("""
                create table {} (
                    pare varchar(8) not null default '',
                    literal varchar(300) not null default '',
                    ordre int,
                    pantalla varchar(20))
                """.format(catalegPare)
                )
        
        u.listToTable(self.catPare, catalegPare, (db, 'aux'))
        u.listToTable(self.catIndi, cataleg, (db, 'aux'))


if __name__ == "__main__":
    CATALEGS()
