import sisapUtils as u

table = "ass_professionals_visites_12m"
file = "ASSPROFESSIONALS12MESOS"

if u.IS_MENSUAL:
    
    dext_month = u.getOne("SELECT MONTH(data_ext) FROM dextraccio", "nodrizas")
    
    if dext_month in (5, 11):

        sql = """
                SELECT
                    LEFT(ide_dni, 8),
                    UPPER(LEFT(CONCAT('S', sha1(CONCAT(up, tipus, uab))), 10))
                FROM
                    cat_professionals_assir
              """
        cat_ass_professionals = {dni: hash for dni, hash in u.getAll(sql, "import")}

        sql = """
                SELECT
                    LEFT(visi_dni_prov_resp, 8)
                FROM
                    nodrizas.ass_visites,
                    nodrizas.dextraccio
                WHERE
                    visi_data_visita BETWEEN date_add(data_ext, INTERVAL -1 YEAR) AND data_ext
              """
        ass_professionals_visita = {dni for dni, in u.getAll(sql, "nodrizas")}

        ass_professionals_visites_12m = [(cat_ass_professionals[dni],) for dni in ass_professionals_visita if dni in cat_ass_professionals]

        u.createTable(table, "(hash varchar(10))", "altres", rm = 1)
        u.listToTable(ass_professionals_visites_12m, table, "altres")
        sql = """
                SELECT
                    hash,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                FROM 
                    altres.ass_professionals_visites_12m
              """
        u.exportKhalix(sql, file)