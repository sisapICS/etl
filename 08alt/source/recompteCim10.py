# coding: iso-8859-1
import sisapUtils as u
import os
from collections import Counter

imp = "import"
nod = "nodrizas"

exclusions = ['664261',
              ]

sql = "SELECT data_ext, date_format(data_ext,'%Y%m') FROM dextraccio"
data_ext, dcalcul = u.getOne(sql,nod)

# Problemes

OutFile_ps = u.tempFolder + 'recompte_Cim10_' + dcalcul + '.txt'
OutFileErrors_ps = u.tempFolder + 'codis_problemes_no_cataleg_' + dcalcul + '.txt'

u.printTime('Inici Recompte Problemes')

descCim10_ps = {}
sql = "select ps_cod,ps_des from cat_prstb001"
for ps, desc in u.getAll(sql, imp):
    descCim10_ps[ps] = {'desc': desc}

recompteCim10_ps = Counter()
recompteErrors_ps = Counter()
enviar_errors = False
sql = """
    SELECT pr_cod_ps, codi_sector
    FROM problemes, nodrizas.dextraccio
    WHERE
        pr_dde BETWEEN date_add(data_ext, interval -1 year) AND data_ext AND
        pr_cod_o_ps = 'C' AND
        pr_hist = 1 AND
        (pr_data_baixa = 0 OR pr_data_baixa > data_ext)
    """
for ps, codi_sector in u.getAll(sql, imp):
    try:
        desc = descCim10_ps[ps]['desc']
        recompteCim10_ps[ps, desc, codi_sector] += 1
    except KeyError:
        if ps not in exclusions:
            enviar_errors = True
            recompteErrors_ps[ps, codi_sector] += 1

with u.openCSV(OutFile_ps) as c:
    for (ps, desc, codi_sector), count in recompteCim10_ps.items():
        c.writerow([ps, desc, codi_sector, count])

with u.openCSV(OutFileErrors_ps) as c:
    for (ps, codi_sector), count in recompteErrors_ps.items():
        c.writerow([ps, codi_sector, count])

# Variables

OutFile_vs = u.tempFolder + 'recompte_variables_' + dcalcul + '.txt'

u.printTime('Inici Recompte Variables')

desc_vs = {}
sql = "select vs_cod,vs_des from cat_prstb004"
for vs, desc in u.getAll(sql, imp):
    desc_vs[vs] = {'desc': desc}

recompte_vs = Counter()
sql = """
      SELECT vu_cod_vs, codi_sector
      FROM import.variables1
      """
for vs, codi_sector in u.getAll(sql, imp):
    if vs in desc_vs:
        desc = desc_vs[vs]['desc']
    else:
        desc = ""
    recompte_vs[vs, desc, codi_sector] += 1

with u.openCSV(OutFile_vs) as c:
    for (vs, desc, codi_sector), count in recompte_vs.items():
        c.writerow([vs, desc, codi_sector, count])

# Activitats

OutFile_ac = u.tempFolder + 'recompte_activitats_' + dcalcul + '.txt'

u.printTime('Inici Recompte Activitats')

desc_ac = {}
sql = "select ac_cod,ac_des from cat_prstb002"
for ac, desc in u.getAll(sql, imp):
    desc_ac[ac] = {'desc': desc}

recompte_ac = Counter()
sql = """
      SELECT au_cod_ac, codi_sector 
      FROM import.activitats1
      """
for ac, codi_sector in u.getAll(sql, imp):
    if ac in desc_ac:
        desc = desc_ac[ac]['desc']
    else:
        desc = ""
    recompte_ac[ac, desc, codi_sector] += 1

with u.openCSV(OutFile_ac) as c:
    for (ac, desc, codi_sector), count in recompte_ac.items():
        c.writerow([ac, desc, codi_sector, count])

u.printTime('Fi Recomptes')

# Enviament dels correus
arxius_enviar = [OutFile_ps, OutFile_vs, OutFile_ac]
if u.IS_MENSUAL:
    text = "Us fem arribar els fitxers d'aquest mes amb els recomptes de codis de cim10, variables i activitats dels darrers 12 mesos."  # noqa
    u.sendSISAP(['colmos@gencat.cat', 'lbelmonte@catsalut.cat', 'auroragomez.bcn.ics@gencat.cat'],
                'Recomptes cim10, variables i activitats',
                'Carmen',
                text,
                arxius_enviar)
    if enviar_errors:
        text = "Us fem arribar el fitxer amb els codis de problemes de salut que no creuen amb el cat�leg"  # noqa
        u.sendSISAP(['mfabregase@gencat.cat', 'lmendezboo@gencat.cat'],
                    'Codis problemes que no creuen',
                    'Mireia i Leo',
                    text,
                    OutFileErrors_ps)

try:
    os.remove(OutFile_ps)
    os.remove(OutFile_vs)
    os.remove(OutFile_ac)
    os.remove(OutFileErrors_ps)
except Exception:
    pass

u.printTime('Fi Proc�s')
