#  coding: latin1
import sisapUtils as u
import collections as c
import datetime
import numpy as np
import re

rols = ("BEN", "NUT", "FIS")

# Mapeig dels tipus d'activitat
map_activitat_tipus = {"SE": "PJ", "OTR": "AL", "SB": "PG", "PR": "PG", "CO": "MC", "CA": "PG"}
map_activitat = {"PS": "AL", "VA": "AL", "OTR": "AL", "II": "AL", "CR": "AL", "CO": "AL"}

# Claus per a les activitats grupals i/o comunit�ries
keys_act_grupal_io_comunitaria = {"C": "B", "G": "A", "GC": "C"}

# Claus pels tipus d'activitat
keys_tipus_activitat = {"SI": "A", "TA": "B", "PJ": "C", "PG": "D", "AL": "E", "ST": "G", "MC": "H"}

# Grups segons el tipus de visita
keys_tipus_visita = {"9C": "A", "9R": "A", "9T": "B", "9D": "C", "9E": "D"}

# Claus per a les tem�tiques d'activitat
keys_activitat_tematica = {"AF": "A",   "MC": "B", "SE": "C", "SM": "D", "ES": "E", "MI": "F", "AL": "G", "TA": "I",
                           "SB": "J", "CU": "K", "VI": "L", "RE": "M", "SS": "N", "S2": "O", "S3": "P", "S1": "Q", "S4": "R", "S5": "S"}

# UPs susceptibles de canviar el seu codi
UP_MODS = {
            '07446': '08430',
            '05093': '08434',
            '07537': '08431',
            '05092': '08435',
            '05826': '08432',
            '05091': '08438',
            '07538': '08436',
            '06164': '08433',
            '05090': '08439',
          }


def get_data_extraccio():
    """."""

    global data_ext, data_ext_menys3mesos, data_ext_menys6mesos, data_ext_menys1any, data_ext_menys1any6setmanes, data_ext_menys15mesos, data_ext_menys18mesos, data_ext_menys2anys

    # Consulta SQL per obtenir dades d'extracci�
    sql = """
            SELECT
                data_ext,
                date_add(date_add(data_ext, INTERVAL -3 MONTH), INTERVAL + 1 DAY),
                date_add(date_add(data_ext, INTERVAL -6 MONTH), INTERVAL + 1 DAY),
                date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
                date_add(date_add(date_add(data_ext, INTERVAL - 6 WEEK), INTERVAL -1 YEAR), INTERVAL + 1 DAY),
                date_add(date_add(data_ext, INTERVAL -15 MONTH), INTERVAL + 1 DAY),
                date_add(date_add(data_ext, INTERVAL -18 MONTH), INTERVAL + 1 DAY),
                date_add(date_add(data_ext, INTERVAL -2 YEAR), INTERVAL + 1 DAY)
            FROM
                dextraccio
            """

    # Obtindre valors de la consulta SQL
    data_ext, data_ext_menys3mesos, data_ext_menys6mesos, data_ext_menys1any, data_ext_menys1any6setmanes, data_ext_menys15mesos, data_ext_menys18mesos, data_ext_menys2anys = u.getOne(sql, "nodrizas")


def get_centres():
    """Obt� els Centres d'Atenci� Prim�ria."""

    global cat_centres, br_sectors

    sql = """
            SELECT
                sector,
                scs_codi,
                ics_codi
            FROM
                cat_centres
        """
    cat_centres = {up: br for (_, up, br) in u.getAll(sql, "nodrizas")}
    br_sectors = {br: codi_sector for (codi_sector, _, br) in u.getAll(sql, "nodrizas")}


def get_assignada():
    """Obt� dades sobre la poblaci� assignada i les emmagatzema en diverses estructures de dades. """

    global assignada_tot, khalix_up_pob_general, khalix_up_pob_detallada

    assignada_tot = dict()  # Diccionari per emmagatzemar la poblaci� assignada
    khalix_up_pob_general, khalix_up_pob_detallada = c.Counter(), c.Counter()

    sql_1 = """
                SELECT
                    id_cip_sec,
                    codi_sector,
                    up,
                    data_naix,
                    sexe,
                    'NOINSAT' AS poblatip
                FROM
                    assignada_tot
                WHERE
                    ates = 1
                    AND institucionalitzat = 0
                    AND atdom = 0
            """
    # Obtenci� de dades de la taula 'assignada_tot'
    for id_cip_sec, codi_sector, up, data_naix, sexe, poblatip in u.getAll(sql_1, "nodrizas"):
        up = UP_MODS.get(up, up)
        br = cat_centres.get(up)
        if br:
            edat = u.yearsBetween(data_naix, data_ext)
            edat_lv, sexe = u.ageConverter(edat), u.sexConverter(sexe)
            etapa = 'adulta' if edat >= 14 else "pedia"
            # Emmagatzemar la informaci� de la poblaci� assignada en el diccionari 'assignada_tot'
            if not "-" in edat_lv:
                assignada_tot[id_cip_sec] = {"codi_sector": codi_sector, "up": up, "br": br, "edat": edat, "edat_lv": edat_lv, "etapa": etapa, "sexe": sexe, "poblatip": poblatip}

            # Afegir informaci� detallada sobre la poblaci� assignada al conjunt 'khalix_up_pob_detallada'
            khalix_up_pob_detallada[(br, edat_lv, sexe, poblatip)] += 1
            # Actualitzar el comptador 'khalix_up_pob_general' amb el nombre total de poblaci� assignada atesa per UP
            khalix_up_pob_general[br] += 1

def get_professionals_referents():
    """."""

    global conversor_usuaris_rols

    conversor_usuaris_rols = dict()

    sql_1 = """
                SELECT
                    rol_usu,
                    'BEN' as rol
                FROM
                    cat_pritb799
                WHERE
                    ROL_ROL LIKE '%BEN%'
            UNION
                SELECT
                    ide_usuari,
                    CASE WHEN ide_categ_prof_c = '11199' THEN 'NUT'
                         WHEN ide_categ_prof_c = '03005' THEN 'FIS'
                         WHEN ide_categ_prof_c = '10778' THEN 'HIG'
                    END AS rol
                FROM
                    cat_pritb992
                WHERE
                    ide_categ_prof_c IN ('11199', '03005', '10778')
            """
    conversor_usuaris_rols = {usuari: rol for usuari, rol in u.getAll(sql_1, "import")}

    
def get_variables():
    """."""

    global variables, agrupadors_vs, codis_agrupadors_vs, codis_agrupadors_cr
    variables = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(dict))))

    # Agrupadors utilitzats i condicions que utilitzarem
    agrupadors_vs = {10:        {"desc": "RCV",                             "etapa": ("adulta",),           "percentil": False,     "periodes": ("sempre", "ultims 12 mesos", "ultims 15 mesos")},
                     19:        {"desc": "IMC",                             "etapa": ("mixta", "adulta"),   "percentil": False,     "periodes": ("sempre", "ultims 12 mesos", "ultims 15 mesos",)},
                     20:        {"desc": "HBA1C",                           "etapa": ("mixta", "adulta"),   "percentil": False,     "periodes": ("sempre", "ultims 12 mesos", "ultims 15 mesos", "ultims 18 mesos",)},
                     87:        {"desc": "Barthel",                         "etapa": ("mixta",),            "percentil": False,     "periodes": ("sempre",)},
                     268:       {"desc": "Pes",                             "etapa": ("adulta",),           "percentil": False,     "periodes": ("ultims 15 mesos",)},
                     756:       {"desc": "Pes percentil",                   "etapa": ("pedia",),            "percentil": True,      "periodes": ("ultims 12 mesos", "ultims 15 mesos", "ultims 24 mesos",)},
                     826:       {"desc": "Escala de benestar mental",       "etapa": ("adulta",),           "percentil": False,     "periodes": ("ultims 12 mesos", "ultims 15 a 3 mesos")},
                     947:       {"desc": "MNA",                             "etapa": ("mixta",),            "percentil": False,     "periodes": ("ultims 12 mesos", "ultims 15 mesos",)},
                     948:       {"desc": "Disf�gia",                        "etapa": ("mixta",),            "percentil": False,     "periodes": ("ultims 12 mesos", "ultims 15 mesos",)},
                     "IA0201":  {"desc": "IA0201",                          "etapa": ("mixta",),            "percentil": False,     "periodes": ("sempre", "ultims 15 mesos",)}, 
                     "IA0202":  {"desc": "IA0202",                          "etapa": ("mixta",),            "percentil": False,     "periodes": ("sempre", "ultims 15 mesos",)}, 
                     "IA0203":  {"desc": "IA0203",                          "etapa": ("mixta",),            "percentil": False,     "periodes": ("sempre", "ultims 15 mesos",)}, 
                     "IA0204":  {"desc": "IA0204",                          "etapa": ("mixta",),            "percentil": False,     "periodes": ("sempre", "ultims 15 mesos",)}, 
                     "IA0205":  {"desc": "IA0205",                          "etapa": ("mixta",),            "percentil": False,     "periodes": ("sempre", "ultims 15 mesos",)}, 
                     "IA0206":  {"desc": "IA0206",                          "etapa": ("mixta",),            "percentil": False,     "periodes": ("sempre", "ultims 15 mesos",)},
                     "VL3001":  {"desc": "VL3001",                          "etapa": ("mixta",),            "percentil": False,     "periodes": ("sempre", "ultims 15 mesos",)},
                     "VL4001":  {"desc": "VL4001",                          "etapa": ("mixta",),            "percentil": False,     "periodes": ("sempre", "ultims 15 mesos",)},
                     "VL5003":  {"desc": "VL5003",                          "etapa": ("mixta",),            "percentil": False,     "periodes": ("sempre", "ultims 15 mesos",)},
                     "VP70C0":  {"desc": "VP70C0",                          "etapa": ("mixta",),            "percentil": False,     "periodes": ("sempre", "ultims 15 mesos",)},
                     "EZ3001":  {"desc": "EZ3001",                          "etapa": ("mixta",),            "percentil": False,     "periodes": ("sempre", "ultims 15 mesos",)},
                     "IN5301":  {"desc": "IN5301",                          "etapa": ("mixta",),            "percentil": False,     "periodes": ("sempre", "ultims 15 mesos",)},
                    }

    # Trobem els codis_vs que van lligats als agrupadors utilitzats
    codis_vs, codis_cr = tuple(), tuple()
    codis_agrupadors_vs, codis_agrupadors_cr = c.defaultdict(set), c.defaultdict(set)
    sql = """
            SELECT
                taula,
                agrupador,
                criteri_codi
            FROM
                eqa_criteris
            WHERE
                agrupador IN {}
            """.format(tuple(agrupadors_vs.keys()))
    for taula, agrupador, codi_vs in u.getAll(sql, "nodrizas"):
        if not agrupadors_vs[agrupador]["percentil"] and taula == "variables":
            codis_vs += (codi_vs,)
            codis_agrupadors_vs[codi_vs].add(agrupador)
        elif taula == "laboratori":
            codis_cr += (codi_vs,)
            codis_agrupadors_cr[codi_vs].add(agrupador)

    for elem_vs in agrupadors_vs:
        if isinstance(elem_vs, str):
            codis_vs += (elem_vs,)
            codis_agrupadors_vs[elem_vs].add(elem_vs)

    # Crear una cadena de consulta amb codis_vs
    codis_vs_codi_in = "("

    for vs_codi in codis_vs:
        codis_vs_codi_in += "'{}', ".format(vs_codi)

    codis_vs_codi_in = codis_vs_codi_in[:-2] + ")"

    # Explotem les taules de variables
    sql = """
            SELECT
                id_cip_sec,
                vu_cod_vs,
                vu_dat_act,
                vu_val,
                vu_usu
            FROM
                {}
            WHERE
                vu_cod_vs IN {}
                AND vu_dat_act <= '{}'
          """
    jobs = [sql.format(table, codis_vs_codi_in, data_ext) for table in u.getSubTables("variables")]
    count=0
    for sub_variables in u.multiprocess(sub_get_variables, jobs, 2):
        for (agrupador_desc, etapa, periode, br, id_cip_sec), tot in sub_variables.items():
            variables[(agrupador_desc, etapa, periode)][br][id_cip_sec]["tot"].update(tot)
        count+=1
        print("    varia: {}/{}. {}".format(count, len(jobs), datetime.datetime.now()))


    # Explotem eqa_variables
    sql = """
            SELECT
                id_cip_sec,
                agrupador,
                data_var,
                valor
            FROM
                nodrizas.eqa_variables
            WHERE
                agrupador IN (20)
                AND data_var <= '{}'
          """.format(data_ext)
    for id_cip_sec, agrupador, data_vs, valor in u.getAll(sql, "nodrizas"):
        if id_cip_sec in assignada_tot:
            br, etapa = [assignada_tot[id_cip_sec][key] for key in ["br", "etapa"]]
            agrupador_desc, periodes = [agrupadors_vs[agrupador][key] for key in ["desc", "periodes"]]
            condicions = {"ultims 12 mesos": (data_ext_menys1any <= data_vs <= data_ext),
                          "ultims 15 mesos": (data_ext_menys15mesos <= data_vs <= data_ext),
                          "ultims 18 mesos": (data_ext_menys18mesos <= data_vs <= data_ext),
                          "ultims 15 a 3 mesos": (data_ext_menys15mesos <= data_vs <= data_ext_menys3mesos),
                          "ultims 24 mesos": (data_ext_menys2anys <= data_vs <= data_ext),
                          "sempre": (data_vs <= data_ext),}

            for periode in periodes:
                if condicions[periode]:
                    if etapa in agrupadors_vs[agrupador]["etapa"]:        
                        variables[(agrupador_desc, etapa, periode)][br][id_cip_sec]["tot"][data_vs] = valor
                    if "mixta" in agrupadors_vs[agrupador]["etapa"]:
                        variables[(agrupador_desc, "mixta", periode)][br][id_cip_sec]["tot"][data_vs] = valor

    print("    eqa_variables. {}".format(datetime.datetime.now()))

    # Variables amb valor de percentil (pes)

    sql = """
            SELECT
                id_cip_sec,
                756,
                data,
                percentil
            FROM
                ped_percentils
            WHERE
                data <= '{}' 
        """.format(data_ext)
    for id_cip_sec, agrupador, data_vs, valor in u.getAll(sql, "nodrizas"):
        if id_cip_sec in assignada_tot:
            br, etapa = [assignada_tot[id_cip_sec][key] for key in ["br", "etapa"]]
            agrupador_desc, periodes = [agrupadors_vs[agrupador][key] for key in ["desc", "periodes"]]
            condicions = {"ultims 12 mesos": (data_ext_menys1any <= data_vs <= data_ext),
                          "ultims 15 mesos": (data_ext_menys15mesos <= data_vs <= data_ext),
                          "ultims 18 mesos": (data_ext_menys18mesos <= data_vs <= data_ext),
                          "ultims 15 a 3 mesos": (data_ext_menys15mesos <= data_vs <= data_ext_menys3mesos),
                          "ultims 24 mesos": (data_ext_menys2anys <= data_vs <= data_ext),
                          "sempre": (data_vs <= data_ext),}

            for periode in periodes:
                if condicions[periode]:
                    if etapa in agrupadors_vs[agrupador]["etapa"]:        
                        variables[(agrupador_desc, etapa, periode)][br][id_cip_sec]["tot"][data_vs] = valor
                    if "mixta" in agrupadors_vs[agrupador]["etapa"]:
                        variables[(agrupador_desc, "mixta", periode)][br][id_cip_sec]["tot"][data_vs] = valor

    print("    ped_percentils. {}".format(datetime.datetime.now()))


    # Registrem l'�ltim valor de cada variable presa a cada pacient
    
    for (agrupador_desc, etapa, periode) in variables:
        for br in variables[(agrupador_desc, etapa, periode)]:
            for id_cip_sec in variables[(agrupador_desc, etapa, periode)][br]:
                data_ultim_vs = max(variables[(agrupador_desc, etapa, periode)][br][id_cip_sec]["tot"])
                valor_ultim_vs = variables[(agrupador_desc, etapa, periode)][br][id_cip_sec]["tot"][data_ultim_vs]
                variables[(agrupador_desc, etapa, periode)][br][id_cip_sec]["ultim"] = {"valor": valor_ultim_vs, "data": data_ultim_vs}

    print("    �ltima variable. {}".format(datetime.datetime.now()))
                        
    return variables


def sub_get_variables(sql):
    
    sub_variables = c.defaultdict(dict)
    for id_cip_sec, codi_vs, data_vs, valor, usuari in u.getAll(sql, "import"):
        if id_cip_sec in assignada_tot: # POSSIBLE CANVI SETEMBRE 2024: and not (codi_vs[:4] == 'IA02' and conversor_usuaris_rols.get(usuari, None) != "FIS"):            
            br, etapa = [assignada_tot[id_cip_sec][key] for key in ["br", "etapa"]]
            for agrupador in codis_agrupadors_vs[codi_vs]:
                agrupador_desc, periodes = [agrupadors_vs[agrupador][key] for key in ["desc", "periodes"]]
                condicions = {"ultims 12 mesos": (data_ext_menys1any <= data_vs <= data_ext),
                              "ultims 15 mesos": (data_ext_menys15mesos <= data_vs <= data_ext),
                              "ultims 18 mesos": (data_ext_menys18mesos <= data_vs <= data_ext),
                              "ultims 15 a 3 mesos": (data_ext_menys15mesos <= data_vs <= data_ext_menys3mesos),
                              "ultims 24 mesos": (data_ext_menys2anys <= data_vs <= data_ext),
                              "sempre": (data_vs <= data_ext),}

                for periode in periodes:
                    if condicions[periode]:
                        if etapa in agrupadors_vs[agrupador]["etapa"]:        
                            sub_variables[(agrupador_desc, etapa, periode, br, id_cip_sec)][data_vs] = valor
                        if "mixta" in agrupadors_vs[agrupador]["etapa"]:
                            sub_variables[(agrupador_desc, "mixta", periode, br, id_cip_sec)][data_vs] = valor
                        
    return sub_variables


def sub_get_indicadors(sector):

    # def get_interaccions_referent_pacients():
    """Obt� les visites i activitats grupals o grupal-comunit�ries."""

    interaccions_referent_pacients = c.defaultdict(lambda: c.defaultdict(lambda: {"visita i/o activitat": c.defaultdict(set),
                                                                                  "visita i/o activitat liderada referent": c.defaultdict(set),
                                                                                  "visites amb referent": c.defaultdict(c.Counter),
                                                                                  "activitat liderada referent": c.defaultdict(dict),
                                                                                  "activitat liderada referent finalitzada": c.defaultdict(dict),
                                                                                  "activitat participacio referent": set(),
                                                                                  "activitat liderada referent seleccionats/proposats/exclosos": set()}))
    visites_eap_ultim_any = c.defaultdict(lambda: c.defaultdict(c.Counter))
    activitats_amb_pacients = set()
    assistencia_grupals = c.defaultdict(dict)

    # Consulta SQL per obtenir les visites
    sql = """
            SELECT DISTINCT
                id_cip_sec,
                codi_sector,
                codi_grup,
                tipus_intervencio,
                data_intervencio,
                data_fi_grupal,
                nombre_assistencies,
                rol_creador,
                rol_participacio,
                procedencia,
                pagr_situacio
            FROM
                pacients_activitats_nous_professionals
            WHERE
                codi_sector = '{}'
                AND (data_intervencio BETWEEN '{}' AND '{}' OR data_fi_grupal BETWEEN '{}' AND '{}')
                AND ((tipus_intervencio in ('9D', '9C', '9E', '9R', '9T') AND procedencia = 'visites')
                      OR (tipus_intervencio in ('G', 'GC') AND procedencia = 'grupal4'))
            """.format(sector, data_ext_menys15mesos, data_ext, data_ext_menys15mesos, data_ext)
    
    # Recorre els resultats de la consulta
    for id_cip_sec, codi_sector, grup_id_sector, tipus_intervencio, data_intervencio, data_fi_grupal, nombre_assistencies, rol_creador, rol_participacio, taula_procedencia, pagr_situacio in u.getAll(sql, "nodrizas"):
        if id_cip_sec in assignada_tot:

            condicions = {"ultims 12 mesos": (data_ext_menys1any <= data_intervencio <= data_ext),
                          "ultims 15 a 3 mesos": (data_ext_menys15mesos <= data_intervencio <= data_ext_menys3mesos),
                          "ultims 18 a 6 mesos": (data_ext_menys18mesos <= data_intervencio <= data_ext_menys6mesos),
                          "finalitzada ultims 12 mesos": (data_ext_menys1any <= data_fi_grupal <= data_ext) if data_fi_grupal else False,
                          "finalitzada ultims 15 a 3 mesos": (data_ext_menys15mesos <= data_fi_grupal <= data_ext_menys3mesos) if data_fi_grupal else False,}

            if taula_procedencia == "visites" or (taula_procedencia == "grupal4" and pagr_situacio == "S"):
                grup_id = (codi_sector, grup_id_sector)
                br = assignada_tot[id_cip_sec]["br"]
                rols_participacio = tuple(rol_participacio[i:i+3] for i in range(0, len(rol_participacio), 3))

                for periode, condicio in condicions.items():
                    if condicio:
                        interaccions_referent_pacients[rol_creador][periode]["visita i/o activitat"][id_cip_sec].add(data_intervencio)
                        if taula_procedencia == "visites":
                            interaccions_referent_pacients[rol_creador][periode]["visites amb referent"][id_cip_sec][keys_tipus_visita[tipus_intervencio]] += 1
                            interaccions_referent_pacients[rol_creador][periode]["visita i/o activitat liderada referent"][id_cip_sec].add(data_intervencio)
                            if periode == "ultims 12 mesos":
                                visites_eap_ultim_any[rol_creador][br]["all"] += 1
                                # Incrementa el comptador de visites segons el tipus de visita
                                visites_eap_ultim_any[rol_creador][br][keys_tipus_visita[tipus_intervencio]] += 1
                        elif taula_procedencia == "grupal4":
                            activitats_amb_pacients.add(grup_id)
                            if rol_creador:
                                interaccions_referent_pacients[rol_creador][periode]["activitat liderada referent"][id_cip_sec][grup_id] = nombre_assistencies
                                if data_fi_grupal <= data_ext:
                                    if periode == "finalitzada ultims 12 mesos":
                                        interaccions_referent_pacients[rol_creador][periode]["activitat liderada referent finalitzada"][id_cip_sec][grup_id] = (nombre_assistencies, data_fi_grupal)
                                    if periode == "finalitzada ultims 15 a 3 mesos":
                                        interaccions_referent_pacients[rol_creador][periode]["activitat liderada referent finalitzada"][id_cip_sec][grup_id] = (nombre_assistencies, data_fi_grupal)
                                interaccions_referent_pacients[rol_creador][periode]["visita i/o activitat liderada referent"][id_cip_sec].add(data_intervencio)
                            for rol_participacio in rols_participacio:
                                interaccions_referent_pacients[rol_participacio][periode]["activitat participacio referent"].add(id_cip_sec)
                                interaccions_referent_pacients[rol_participacio][periode]["visita i/o activitat"][id_cip_sec].add(data_intervencio)
            if taula_procedencia == "grupal4" and condicions["ultims 12 mesos"]:
                interaccions_referent_pacients[rol_creador]["ultims 12 mesos"]["activitat liderada referent seleccionats/proposats/exclosos"].add(id_cip_sec)

    # def get_activitats_grupals_io_comunitaries():
    """Obt� informaci� sobre els grups i els guarda en un diccionari."""

    activitats_grupals_io_comunitaries = c.defaultdict(dict)

    # Consulta SQL per obtenir la informaci� dels grups
    sql = """
            SELECT
                codi_sector,
                codi_grup,
                up_intervencio, 
                n_assistents, 
                n_sessions,
                hores,
                tipus_activitat,
                tema_activitat, 
                tipus_intervencio,
                rol_participacio, 
                rol_creador
            FROM
                activitats_grupals_nous_professionals
            WHERE
                codi_sector = '{}'
                AND data_intervencio BETWEEN '{}' AND '{}'
          """.format(sector, data_ext_menys1any, data_ext)
    for codi_sector, grup_id_sector, up, n_assistents, n_sessions, hores, activitat_tipus, activitat_tematica, act_grupal_io_comunitaria, \
        rol_participacio_brut, rol_creador in u.getAll(sql, "nodrizas"):
        up = UP_MODS.get(up, up)
        br = cat_centres.get(up)
        if br:
            grup_id = (codi_sector, grup_id_sector)
            if (act_grupal_io_comunitaria in ("G", "GC") and grup_id in activitats_amb_pacients) or act_grupal_io_comunitaria == "C":
                rols_participacio = tuple(rol_participacio_brut[i:i+3] for i in range(0, len(rol_participacio_brut), 3))
                rols_creador_o_participacio = set(rols_participacio + (rol_creador,)) if rol_creador else set(rols_participacio)
                activitat_tipus = map_activitat_tipus.get(activitat_tipus, activitat_tipus)
                activitat_tematica = map_activitat.get(activitat_tematica, activitat_tematica)
                for rol in rols_creador_o_participacio:
                    activitats_grupals_io_comunitaries[rol][grup_id] = {"br": br,
                                                                        "n_assistents": n_assistents,
                                                                        "n_sessions": n_sessions,
                                                                        "hores": hores, 
                                                                        "act_grupal_io_comunitaria": act_grupal_io_comunitaria, 
                                                                        "activitat_tipus": activitat_tipus, 
                                                                        "activitat_tematica": activitat_tematica,
                                                                        "rols_participacio": rols_participacio,
                                                                        "rol_creador": rol_creador}


    # def get_prescripcions_socials():
    """ Obt� les dades de les prescripcions socials realitzades durant el per�ode d'an�lisi 
        per a cada centre. A m�s a m�s, aquestes es classifiquen en funci� de si ha hagut 
        seguiment o no (camp de la taula "so_seguiment" / variable python "seguiment_ps")
        i, en cas de que no, es determina si no hi ha seguiment per motius personals
        (camp de la taula "so_seguiment" / variable python "seguiment_ps" amb valor "P")
        o no personal (valor "A"). El camp de la taula "so_seguiment" nom�s t� valor
        si no hi ha seguiment. """

    prescripcions_socials = c.defaultdict(lambda: c.defaultdict(c.Counter))
    prescripcions_socials_seguiment_per_data = c.defaultdict(lambda: c.defaultdict(dict))

    sql = """
            SELECT
                id_cip_sec,
                so_usu_alta,
                so_opcio,
                so_data_alta,
                CASE WHEN so_seguiment = 'S' THEN 'S' ELSE 'N' END so_seguiment
            FROM
                presc_social_s{}
            WHERE
                so_data_alta BETWEEN '{}' AND '{}'
            """.format(sector, data_ext_menys1any, data_ext)
    for id_cip_sec, usu_alta, opcio_ps, data_alta, seguiment_ps in u.getAll(sql, "import"):
        if id_cip_sec in assignada_tot and usu_alta in conversor_usuaris_rols:
            rol = conversor_usuaris_rols[usu_alta]
            br = assignada_tot[id_cip_sec]["br"]
            prescripcions_socials[rol][id_cip_sec]["total"] += 1
            prescripcions_socials[rol][id_cip_sec][seguiment_ps] += 1
            prescripcions_socials_seguiment_per_data[rol][id_cip_sec][data_alta] = seguiment_ps
            if seguiment_ps == "N" and opcio_ps == "P":
                prescripcions_socials[rol][id_cip_sec]["N-Personal"] += 1

    # def get_derivacions():
    """."""

    derivacions, derivacions_endocrinologia = {}, c.defaultdict(set)

    sql = """
            SELECT
                id_cip_sec,
                oc_data,
                especialitat_final
            FROM
                derivacions_s{}
            WHERE
                ((((especialitat_final IN ('TRAUMATOLOGIA I ORTOPEDIA', 'MEDICINA F�SICA I REHABILITACI�', 'REUMATOLOGIA')
                    OR inf_servei_d_codi_desc IN ('TRAUMATOLOGIA', 'TRAUMA ESCOLIOSI', 'TRAUMATOLOGIA:  MA/COLZE', 'TRAUMATOLOGIA - UNITAT RAQUIS', 'TRAUMATOLOGIA GENOLL', 'REUMATOLOGIA PEDIATRICA', 'PEDIATRIA: TRAUMATOLOGIA', 'TRAUMATOLOGIA:  MALUC', 'TRAUMATOLOGIA:  RAQUIS', 'TRAUMATOLOGIA:  ESPATLLA', 'TRAUMATOLOGIA ORTOPEDIA INFANTIL', 'REHABILITACI� AP', 'PEDIATRIA: REUMATOLOGIA'))
                AND inf_servei_d_codi_desc <> 'REHABILITACI� PAO�S')
                OR especialitat_final = 'ENDOCRINOLOGIA')
                AND oc_data BETWEEN '{}' AND '{}')
          """.format(sector, data_ext_menys1any6setmanes, data_ext)
    for id_cip_sec, data_derivacio, especialitat_final in u.getAll(sql, "import"):
        if especialitat_final != 'ENDOCRINOLOGIA':
            derivacions.setdefault(id_cip_sec, data_derivacio)
            if data_derivacio > derivacions[id_cip_sec]:
                derivacions[id_cip_sec] = data_derivacio
        else:
            derivacions_endocrinologia[id_cip_sec].add(data_derivacio)


    # def get_problemes():
    """ Obt� les dades dels problemes de salut especificats a l'objecte ps_dict que han estat 
        detectats als pacients assignats i atesos durant el per�ode compr�s pel nombre de mesos 
        anteriors al final del per�ode d'an�lisi especificat a la clau "periode" de ps_dict. """

    problemes = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(set)))
    codis_ps = tuple()
    codis_agrupadors_ps = c.defaultdict(list)
    codis_like_ps = set()

#     self.desnutricio_malnutricio_dates = c.defaultdict(lambda: c.defaultdict(dict))
#     self.disfagia_dates = c.defaultdict(lambda: c.defaultdict(dict))

    agrupadors_ps = {1:    {"desc": "CI",                                    "periodes": ("sempre",)},
                     18:   {"desc": "DM2",                                   "periodes": ("actiu",)},
                     53:   {"desc": "IRC",                                   "periodes": ("actiu",)},
                     169:  {"desc": "Malaltia inflamat�ria intestinal",      "periodes": ("actiu",)},
                     212:  {"desc": "AVC",                                   "periodes": ("sempre",)},
                     941:  {"desc": "Disf�gia",                              "periodes": ("actiu", "actiu ultims 12 mesos", "actiu ultims 15 mesos",)},
                     943:  {"desc": "Malnutrici� o desnutrici�",             "periodes": ("actiu", "actiu ultims 12 mesos", "actiu ultims 15 mesos",)},
                     944:  {"desc": "Celiaqu�a",                             "periodes": ("actiu",)},
                     945:  {"desc": "Intoler�ncia o al�l�rgia aliment�ria",  "periodes": ("actiu",)},
                     951:  {"desc": "Lumb�lgia",                             "periodes": ("actiu ultims 12 mesos. 6 setmanes evoluci�", "actiu ultims 15 mesos. 6 setmanes evoluci�")},
                     969:  {"desc": "Dol",                                   "periodes": ("actiu ultims 12 mesos",)},
                     970:  {"desc": "Dolor persistent",                      "periodes": ("actiu ultims 12 mesos",)},
                     979:  {"desc": "Simptomatologia mental lleu/moderada",  "periodes": ("actiu ultims 12 mesos",)},
                     980:  {"desc": "Simptomatologia mental lleu/moderada",  "periodes": ("actiu ultims 12 mesos",)},
                     1008: {"desc": "Dolor cr�nic columna",                  "periodes": ("actiu ultims 12 mesos. 6 setmanes evoluci�", "actiu ultims 15 mesos. 6 setmanes evoluci�", "6 setmanes evoluci�. iniciat ultim any 6 mesos",)},
                     1009: {"desc": "S�ndrome subacromial",                  "periodes": ("actiu ultims 12 mesos. 6 setmanes evoluci�",)},
                     1010: {"desc": "Artrosi espatlla",                      "periodes": ("actiu ultims 12 mesos. 6 setmanes evoluci�",)},
                     1011: {"desc": "Gono-coxartrosi",                       "periodes": ("actiu ultims 12 mesos. 6 setmanes evoluci�",)},
                     1012: {"desc": "Gon�lgia",                              "periodes": ("actiu ultims 12 mesos. 6 setmanes evoluci�", "actiu ultims 15 mesos. 6 setmanes evoluci�", "6 setmanes evoluci�. iniciat ultim any 6 mesos",)},
                     1014: {"desc": "Senilitat",                             "periodes": ("actiu ultims 12 mesos. 6 setmanes evoluci�", "actiu ultims 15 mesos. 6 setmanes evoluci�")},
                     1015: {"desc": "Dolor d'espatlla de llarga durada",     "periodes": ("actiu ultims 12 mesos. 6 setmanes evoluci�", "actiu ultims 15 mesos. 6 setmanes evoluci�", "6 setmanes evoluci�. iniciat ultim any 6 mesos",)},
                     1047: {"desc": "Patologia de columna lumbar",           "periodes": ("actiu ultims 15 mesos. 6 setmanes evoluci�",)},
                     1048: {"desc": "Patolog�a de columna cervical",         "periodes": ("actiu ultims 15 mesos. 6 setmanes evoluci�",)},
                    }

    sql = """
            SELECT
                agrupador,
                criteri_codi 
            FROM
                eqa_criteris
            WHERE
                agrupador IN {}
            """.format(tuple(agrupadors_ps.keys()))
    for agrupador, codi_ps in u.getAll(sql, "nodrizas"):
        codi_ps = codi_ps.replace("C01-", "")
        codis_ps += (codi_ps,)
        codis_agrupadors_ps[codi_ps].append({"desc": agrupadors_ps[agrupador]["desc"], "periodes": agrupadors_ps[agrupador]["periodes"]})

    codis_sense_agrupador_ps = {"M22%":    {"desc": "Gono-femoropatelar", "periodes": ("actiu ultims 12 mesos. 6 setmanes evoluci�",)}}
                                
    for codi_ps, detalls in codis_sense_agrupador_ps.items():
        codis_ps += (codi_ps,)
        codis_agrupadors_ps[codi_ps].append({"desc": detalls["desc"], "periodes": detalls["periodes"]})
        if '%' in codi_ps:
            codis_like_ps.add(codi_ps[:-1])

    # Crear una cadena de consulta amb codis ps
    codis_ps_codi_in = "("
    codis_ps_codi_like = ""

    for ps_codi in codis_ps:
        if not "%" in ps_codi:
            codis_ps_codi_in += "'{}', ".format(ps_codi)
            if "C01" not in ps_codi:
                codis_ps_codi_in += "'C01-{}', ".format(ps_codi)
        else:
            codis_ps_codi_like += "OR pr_cod_ps LIKE '{}' ".format(codi_ps)
            if "C01" not in ps_codi:
                codis_ps_codi_like += "OR pr_cod_ps LIKE 'C01-{}' ".format(codi_ps)

    codis_ps_codi_in = codis_ps_codi_in[:-2] + ")"

    sql = """
            SELECT
                id_cip_sec,
                pr_cod_ps,
                pr_dde,
                CASE WHEN pr_dba <> '0000-00-00' THEN pr_dba
                        ELSE STR_TO_DATE('{}', '%Y-%m-%d')
                END AS pr_dba_p
            FROM
                problemes_s{}
            WHERE 
                (pr_cod_ps IN {} {})
                AND pr_dde <= STR_TO_DATE('{}', '%Y-%m-%d')
          """.format(data_ext, sector, codis_ps_codi_in, codis_ps_codi_like, data_ext)
    for id_cip_sec, codi_ps, data_inici_ps, data_fi_ps in u.getAll(sql, "import"):
        if id_cip_sec in assignada_tot:
            codi_ps = codi_ps.replace("C01-", "")
            br, etapa = [assignada_tot[id_cip_sec][key] for key in ["br", "etapa"]]
            condicions = {"actiu": (data_inici_ps <= data_ext <= data_fi_ps),
                          "actiu ultims 12 mesos": (data_inici_ps <= data_ext and data_ext_menys1any <= data_fi_ps),
                          "actiu ultims 12 mesos. 6 setmanes evoluci�": (data_inici_ps <= data_ext and data_ext_menys1any <= data_fi_ps and u.daysBetween(data_inici_ps, data_fi_ps)>=6*7),
                          "actiu ultims 15 mesos. 6 setmanes evoluci�": (data_inici_ps <= data_ext and data_ext_menys15mesos <= data_fi_ps and u.daysBetween(data_inici_ps, data_fi_ps)>=6*7),
                          "6 setmanes evoluci�. iniciat ultim any 6 mesos": (data_inici_ps <= data_ext and u.daysBetween(data_inici_ps, data_fi_ps)>=6*7 and data_ext_menys1any6setmanes <= data_inici_ps),
                          "ultims 15 a 3 mesos": (data_ext_menys15mesos <= data_inici_ps <= data_ext_menys3mesos),
                          "actiu ultims 15 mesos": (data_inici_ps <= data_ext and data_ext_menys15mesos <= data_fi_ps),
                          "sempre": (data_inici_ps <= data_ext),}
            if codi_ps in codis_agrupadors_ps:
                for agrupador in codis_agrupadors_ps[codi_ps]:
                    agrupador_desc, periodes = [agrupador[key] for key in ["desc", "periodes"]]
                    for periode in periodes:
                        if condicions[periode]:
                            problemes[(agrupador_desc, etapa, periode)][br][id_cip_sec].add((data_inici_ps, data_fi_ps))
                            problemes[(agrupador_desc, "mixta", periode)][br][id_cip_sec].add((data_inici_ps, data_fi_ps))
            for codi_like_ps in codis_like_ps:
                if codi_ps.startswith(codi_like_ps):
                    for agrupador in codis_agrupadors_ps[codi_like_ps+"%"]:
                        agrupador_desc, periodes = [agrupador[key] for key in ["desc", "periodes"]]
                        for periode in periodes:
                            if condicions[periode]:
                                problemes[(agrupador_desc, etapa, periode)][br][id_cip_sec].add((data_inici_ps, data_fi_ps))
                                problemes[(agrupador_desc, "mixta", periode)][br][id_cip_sec].add((data_inici_ps, data_fi_ps))             

    # def get_indicadors():
    """."""

    resultats_counter = c.defaultdict(lambda: c.defaultdict(c.Counter))
    resultats_cero = c.defaultdict(lambda: c.defaultdict(c.Counter))
    poblacio_descartada_den_benpat04 = c.defaultdict(set)
    poblacio_descartada_den_nutpat13 = c.defaultdict(set)
    poblacio_descartada_den_fispat07 = c.defaultdict(set)

    if 1: # IND

        # IND01, IND03, IND04, IND05, IND06, IND07

        for rol in rols:
            for grup_id, grup_dades in activitats_grupals_io_comunitaries[rol].items():

                br = grup_dades["br"]
                n_assistents = grup_dades["n_assistents"]
                hores = grup_dades["hores"]
                act_grupal_io_comunitaria = grup_dades["act_grupal_io_comunitaria"]
                activitat_tipus = grup_dades["activitat_tipus"]
                activitat_tematica = grup_dades["activitat_tematica"]
                # br, edat_lv, sexe, poblatip, ind, "NUM"
                resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}01".format(rol), "NUM")] += 1
                resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}03".format(rol), "NUM")] += hores
                resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}05T".format(rol), "DEN")] += 1
                resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}07T".format(rol), "DEN")] += hores

                for key_activitat_tipus in keys_tipus_activitat.values():
                    resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}04{}".format(rol, key_activitat_tipus), "DEN")] += 1
                for key_activitat_tematica in keys_activitat_tematica.values():
                    resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}05{}".format(rol, key_activitat_tematica), "DEN")] += 1
                for key_activitat_tipus in keys_tipus_activitat.values():
                    resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}06{}".format(rol, key_activitat_tipus), "DEN")] += hores
                for key_activitat_tematica in keys_activitat_tematica.values():
                    resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}07{}".format(rol, key_activitat_tematica), "DEN")] += hores

                if act_grupal_io_comunitaria in keys_act_grupal_io_comunitaria:
                    resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}01{}".format(rol, keys_act_grupal_io_comunitaria[act_grupal_io_comunitaria]), "NUM")] += 1

                if activitat_tipus in keys_tipus_activitat:
                    resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}04{}".format(rol, keys_tipus_activitat.get(activitat_tipus, "E")), "NUM")] += 1
                    resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}06{}".format(rol, keys_tipus_activitat.get(activitat_tipus, "E")), "NUM")] += hores

                if activitat_tematica in keys_activitat_tematica:
                    resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}05{}".format(rol, keys_activitat_tematica.get(activitat_tematica, "G")), "NUM")] += 1
                    if keys_activitat_tematica[activitat_tematica] in ("C", "O", "P", "Q", "R", "S"):
                        resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}05T".format(rol), "NUM")] += 1                
                    resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}07{}".format(rol, keys_activitat_tematica.get(activitat_tematica, "G")), "NUM")] += hores
                    if keys_activitat_tematica[activitat_tematica] in ("C", "O", "P", "Q", "R", "S"):
                        resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}07T".format(rol), "NUM")] += hores                

            # Indicadors amb denominador igual a la poblaci� assignada atesa: IND01, IND03

            indicadors_poblacio_assignada_atesa = ["{}01", "{}01A", "{}01B", "{}01C", "{}03"]

            for br, poblacio_up in khalix_up_pob_general.items():
                if br_sectors[br] == sector:
                    for indicador in indicadors_poblacio_assignada_atesa:
                        resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', indicador.format(rol), "DEN")] = poblacio_up

        # BEN13 / NUT11 / FIS13

        for rol, ind_num in {"BEN": 13, "NUT": 11, "FIS": 13}.items():
            for br, visites_up in visites_eap_ultim_any[rol].items():
                resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}{}".format(rol, ind_num), "DEN")] = 0
                resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}{}".format(rol, ind_num), "NUM")] = visites_up["all"]
                for sub_ind in ["A", "B", "C", "D"]:
                    resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}{}{}".format(rol, ind_num, sub_ind), "DEN")] += visites_up["all"]
                    resultats_counter[rol]["GEN"][(br, 'NOCAT', 'DIM6SET', 'NOIMP', "{}{}{}".format(rol, ind_num, sub_ind), "NUM")] += visites_up[sub_ind]
        
        print("Done INDGEN")
        
    if 1: # INDPOB

        br_a_num_pob04 = c.defaultdict(set)

        # BENPOB01, BENPOB02, BENPOB03, BENPOB04, BENPOB05, BENPOB06, BENPOB08, BENPOB09

        indicadors_pob = ("BENPOB01", "BENPOB02", "BENPOB03", "BENPOB04", "BENPOB05", "BENPOB06", "BENPOB08", "BENPOB09", 
                          "NUTPOB01", "NUTPOB02", "NUTPOB03", "NUTPOB04", "NUTPOB05", "NUTPOB06", "NUTPOB08", "NUTPOB09",
                          "FISPOB01", "FISPOB02", "FISPOB03", "FISPOB04", "FISPOB05", "FISPOB06", "FISPOB08", "FISPOB09")

        for (br, edat_lv, sexe, poblatip), n in khalix_up_pob_detallada.items():
            if br_sectors[br] == sector:
                for indicador in indicadors_pob:
                    rol = indicador[:3]
                    resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, indicador, "DEN")] += n

        # INDPOB01

        for rol in ("BEN", "NUT", "FIS"):
            ind = "{}POB01".format(rol)
            for id_cip_sec in interaccions_referent_pacients[rol]["ultims 12 mesos"]["visites amb referent"]:
                br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
                if (br, edat_lv, sexe, poblatip, ind, "DEN") in resultats_counter[rol]["POB"]:
                    resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "NUM")] += 1

        # INDPOB02

        for rol in ("BEN", "NUT", "FIS"):
            ind = "{}POB02".format(rol)
            for id_cip_sec in interaccions_referent_pacients[rol]["ultims 12 mesos"]["activitat liderada referent"]:
                br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
                if (br, edat_lv, sexe, poblatip, ind, "DEN") in resultats_counter[rol]["POB"]:
                    resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "NUM")] += 1

        # INDPOB03

        for rol in ("BEN", "NUT", "FIS"):
            ind = "{}POB03".format(rol)
            for id_cip_sec in interaccions_referent_pacients[rol]["ultims 12 mesos"]["activitat participacio referent"]:
                br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
                if (br, edat_lv, sexe, poblatip, ind, "DEN") in resultats_counter[rol]["POB"]:
                    resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "NUM")] += 1

        # INDPOB04

        for rol in ("BEN", "NUT", "FIS"):
            ind = "{}POB04".format(rol)
            for id_cip_sec in interaccions_referent_pacients[rol]["ultims 12 mesos"]["visita i/o activitat"]:
                br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
                if (br, edat_lv, sexe, poblatip, ind, "DEN") in resultats_counter[rol]["POB"]:
                    resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "NUM")] += 1
                    br_a_num_pob04[rol].add(br)


        # INDPOB05, INDPOB06

        for rol in ("BEN", "NUT", "FIS"):
            for id_cip_sec in prescripcions_socials[rol]:
                br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
                if br in br_a_num_pob04[rol]:
                    if id_cip_sec not in interaccions_referent_pacients[rol]["ultims 12 mesos"]["visita i/o activitat"]:
                        ind = "{}POB05".format(rol)
                        if (br, edat_lv, sexe, poblatip, ind, "DEN") in resultats_counter[rol]["POB"]:
                            resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "NUM")] += 1
                    else:
                        ind = "{}POB06".format(rol)
                        if (br, edat_lv, sexe, poblatip, ind, "DEN") in resultats_counter[rol]["POB"]:
                            resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "NUM")] += 1

        # INDPOB07

        for rol in ("BEN", "NUT", "FIS"):
            ind = "{}POB07".format(rol)
            for id_cip_sec in interaccions_referent_pacients[rol]["ultims 12 mesos"]["visita i/o activitat"]:
                br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
                if br in br_a_num_pob04[rol]:
                    resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "DEN")] += 1
                    if id_cip_sec in interaccions_referent_pacients[rol]["ultims 12 mesos"]["visites amb referent"]:
                        for _, n_visites in interaccions_referent_pacients[rol]["ultims 12 mesos"]["visites amb referent"][id_cip_sec].items():
                            resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "NUM")] += n_visites
                
        # INDPOB08

        for rol in ("BEN", "NUT", "FIS"):
            ind = "{}POB08".format(rol)
            for id_cip_sec in (set(interaccions_referent_pacients[rol]["ultims 12 mesos"]["activitat liderada referent"]) | interaccions_referent_pacients[rol]["ultims 12 mesos"]["activitat participacio referent"]):
                br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
                if (br, edat_lv, sexe, poblatip, ind, "DEN") in resultats_counter[rol]["POB"]:
                    resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "NUM")] += 1

        # INDPOB09

        for rol in ("BEN", "NUT", "FIS"):
            ind = "{}POB09".format(rol)
            for id_cip_sec in interaccions_referent_pacients[rol]["ultims 12 mesos"]["visita i/o activitat"]:
                if id_cip_sec not in (set(interaccions_referent_pacients[rol]["ultims 12 mesos"]["activitat liderada referent"]) | interaccions_referent_pacients[rol]["ultims 12 mesos"]["activitat participacio referent"]):
                    br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
                    if (br, edat_lv, sexe, poblatip, ind, "DEN") in resultats_counter[rol]["POB"]:
                        resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "NUM")] += 1

        # INDPOB10

        for rol in ("BEN", "NUT", "FIS"):
            ind = "{}POB10".format(rol)
            for id_cip_sec in interaccions_referent_pacients[rol]["ultims 12 mesos"]["visita i/o activitat"]:
                br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
                if br in br_a_num_pob04[rol]:
                    resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "DEN")] += 1
                    if id_cip_sec in prescripcions_socials[rol]:
                        resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "NUM")] += 1

        # INDPOB11

        for rol in ("BEN", "NUT", "FIS"):
            ind = "{}POB11".format(rol)
            for id_cip_sec in prescripcions_socials[rol]:
                br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
                resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "DEN")] += 1
                ultima_data_alta = max(prescripcions_socials_seguiment_per_data[rol][id_cip_sec])
                if prescripcions_socials_seguiment_per_data[rol][id_cip_sec][ultima_data_alta] == "S":
                    resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "NUM")] += 1

        # INDPOB12, INDPOB13
        
        for rol in ("BEN", "NUT", "FIS"):
            ind12, ind13 = "{}POB12".format(rol), "{}POB13".format(rol)
            for id_cip_sec in interaccions_referent_pacients[rol]["finalitzada ultims 12 mesos"]["activitat liderada referent finalitzada"]:
                br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
                for grup_id, (nombre_assistencies, _) in interaccions_referent_pacients[rol]["finalitzada ultims 12 mesos"]["activitat liderada referent finalitzada"][id_cip_sec].items():
                    try:
                        nombre_sessions = activitats_grupals_io_comunitaries[rol][grup_id]["n_sessions"]
                    except:
                        nombre_sessions = 0
                    if nombre_sessions != 0:
                        resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind12, "DEN")] += 1
                        resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind13, "DEN")] += 1
                        resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind12, "NUM")] += 1 if nombre_assistencies > 0 else 0
                        resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind13, "NUM")] += 1 if float(nombre_assistencies) / float(nombre_sessions) >= 0.8 else 0

        # INDPOB14

        for rol in ("BEN", "NUT", "FIS"):
            ind = "{}POB14".format(rol)
            for id_cip_sec in interaccions_referent_pacients[rol]["ultims 12 mesos"]["activitat liderada referent seleccionats/proposats/exclosos"]:
                br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
                resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "DEN")] += 1
                if id_cip_sec in interaccions_referent_pacients[rol]["ultims 12 mesos"]["activitat liderada referent"]:
                    resultats_counter[rol]["POB"][(br, edat_lv, sexe, poblatip, ind, "NUM")] += 1

        print("Done INDPOB")
        
    if 1: # BENPAT

        # BENPAT01

        for br, pacients_dol in problemes[("Dol", "mixta", "actiu ultims 12 mesos")].items():
            for id_cip_sec in pacients_dol.keys():
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                poblacio_descartada_den_benpat04[br].add(id_cip_sec)
                resultats_counter["BEN"]["PAT"][(br, edat_lv, sexe, poblatip, "BENPAT01", "DEN")] += 1
                if id_cip_sec in interaccions_referent_pacients["BEN"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["BEN"]["PAT"][(br, edat_lv, sexe, poblatip, "BENPAT01", "NUM")] += 1


        # BENPAT02

        for br, pacients_dolor in problemes[("Dolor persistent", "mixta", "actiu ultims 12 mesos")].items():
            for id_cip_sec in pacients_dolor.keys():
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                poblacio_descartada_den_benpat04[br].add(id_cip_sec)
                resultats_counter["BEN"]["PAT"][(br, edat_lv, sexe, poblatip, "BENPAT02", "DEN")] += 1
                if id_cip_sec in interaccions_referent_pacients["BEN"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["BEN"]["PAT"][(br, edat_lv, sexe, poblatip, "BENPAT02", "NUM")] += 1           

        # BENPAT03

        for br, pacients_mental in problemes[("Simptomatologia mental lleu/moderada", "mixta", "actiu ultims 12 mesos")].items():
            for id_cip_sec in pacients_mental.keys():
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                poblacio_descartada_den_benpat04[br].add(id_cip_sec)
                resultats_counter["BEN"]["PAT"][(br, edat_lv, sexe, poblatip, "BENPAT03", "DEN")] += 1
                if id_cip_sec in interaccions_referent_pacients["BEN"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["BEN"]["PAT"][(br, edat_lv, sexe, poblatip, "BENPAT03", "NUM")] += 1          


        # BENPAT04

        for id_cip_sec in assignada_tot:
            codi_sector, br = (assignada_tot[id_cip_sec][key] for key in ("codi_sector", "br"))
            if codi_sector == sector and id_cip_sec not in poblacio_descartada_den_benpat04[br]:
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                resultats_counter["BEN"]["PAT"][(br, edat_lv, sexe, poblatip, "BENPAT04", "DEN")] += 1
                if id_cip_sec in interaccions_referent_pacients["BEN"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["BEN"]["PAT"][(br, edat_lv, sexe, poblatip, "BENPAT04", "NUM")] += 1

        print("Done BENPAT")            
        
    if 1: # NUTPAT

        # NUTPAT01

        for br in set(problemes[("Malnutrici� o desnutrici�", "mixta", "actiu ultims 12 mesos")]) | set(variables[("MNA", "mixta", "ultims 12 mesos")]):
            if br_sectors[br] == sector:
                pacients_candidats = (set(problemes[("Malnutrici� o desnutrici�", "mixta", "actiu ultims 12 mesos")][br]) | set(variables[("MNA", "mixta", "ultims 12 mesos")][br]))
                for id_cip_sec in pacients_candidats:
                    pacient_candidat_den = False
                    if id_cip_sec in problemes[("Malnutrici� o desnutrici�", "mixta", "actiu ultims 12 mesos")][br]:
                        pacient_candidat_den = True
                    elif id_cip_sec in variables[("MNA", "mixta", "ultims 12 mesos")][br]:
                        for valor in variables[("MNA", "mixta", "ultims 12 mesos")][br][id_cip_sec]["tot"].values():
                            if valor <= 23.5:
                                pacient_candidat_den = True
                                break
                    if pacient_candidat_den:
                        sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                        poblacio_descartada_den_nutpat13[br].add(id_cip_sec)
                        resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT01", "DEN")] += 1
                        if id_cip_sec in interaccions_referent_pacients["NUT"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                            resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT01", "NUM")] += 1

        # NUTPAT02

        for br in set(variables[("IMC", "adulta", "ultims 12 mesos")]) | set(variables[("IMC", "adulta", "sempre")]):
            if br_sectors[br] == sector:
                pacients_candidats = (set(variables[("IMC", "adulta", "ultims 12 mesos")][br]) | set(variables[("IMC", "adulta", "sempre")][br]))
                for id_cip_sec in pacients_candidats:
                    pacient_candidat_den = False
                    if id_cip_sec in variables[("IMC", "adulta", "ultims 12 mesos")][br]:
                        for valor in variables[("IMC", "adulta", "ultims 12 mesos")][br][id_cip_sec]["tot"].values():
                            if valor > 35:
                                pacient_candidat_den = True
                                break
                    elif id_cip_sec in variables[("IMC", "adulta", "sempre")][br]:
                        valor = variables[("IMC", "adulta", "sempre")][br][id_cip_sec]["ultim"]["valor"]
                        if valor > 35:
                            pacient_candidat_den = True
                    if pacient_candidat_den:
                        sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                        poblacio_descartada_den_nutpat13[br].add(id_cip_sec)
                        resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT02", "DEN")] += 1
                        if id_cip_sec in interaccions_referent_pacients["NUT"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                            resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT02", "NUM")] += 1

        # NUTPAT04 

        for br in set(variables[("Pes percentil", "pedia", "ultims 12 mesos")]) | set(variables[("Pes percentil", "pedia", "ultims 24 mesos")]):
            if br_sectors[br] == sector:
                pacients_candidats = (set(variables[("Pes percentil", "pedia", "ultims 12 mesos")][br]) | set(variables[("Pes percentil", "pedia", "ultims 24 mesos")][br]))
                for id_cip_sec in pacients_candidats:
                    pacient_candidat_den = False
                    if id_cip_sec in variables[("Pes percentil", "pedia", "ultims 12 mesos")][br]:
                        for valor in variables[("Pes percentil", "pedia", "ultims 12 mesos")][br][id_cip_sec]["tot"].values():
                            if valor > ">P95":
                                pacient_candidat_den = True
                                break
                    elif id_cip_sec in variables[("Pes percentil", "pedia", "ultims 24 mesos")][br]:
                        data, valor = tuple([variables[("Pes percentil", "pedia", "ultims 24 mesos")][br][id_cip_sec]["ultim"][key] for key in ("data", "valor")])
                        if valor > ">P95":
                            pacient_candidat_den = True
                    if pacient_candidat_den:
                        sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                        poblacio_descartada_den_nutpat13[br].add(id_cip_sec)
                        resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT04", "DEN")] += 1
                        if id_cip_sec in interaccions_referent_pacients["NUT"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                            resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT04", "NUM")] += 1     

        # NUTPAT05

        for br in set(problemes[("Disf�gia", "mixta", "actiu ultims 12 mesos")]) | set(variables[("Disf�gia", "mixta", "ultims 12 mesos")]):
            if br_sectors[br] == sector:
                pacients_candidats = (set(problemes[("Disf�gia", "mixta", "actiu ultims 12 mesos")][br]) | set(variables[("Disf�gia", "mixta", "ultims 12 mesos")][br]))
                for id_cip_sec in pacients_candidats:
                    pacient_candidat_den = False
                    if id_cip_sec in problemes[("Disf�gia", "mixta", "actiu ultims 12 mesos")][br].keys():
                        pacient_candidat_den = True
                    if id_cip_sec in variables[("Disf�gia", "mixta", "ultims 12 mesos")][br]:
                        for valor in variables[("Disf�gia", "mixta", "ultims 12 mesos")][br][id_cip_sec]["tot"].values():
                            if valor >= 3:
                                pacient_candidat_den = True
                                break
                    if pacient_candidat_den:
                        sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                        poblacio_descartada_den_nutpat13[br].add(id_cip_sec)
                        resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT05", "DEN")] += 1
                        if id_cip_sec in interaccions_referent_pacients["NUT"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                            resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT05", "NUM")] += 1    

        # NUTPAT06

        for br, pacients_candidats in problemes[("Celiaqu�a", "mixta", "actiu")].items():
            for id_cip_sec in pacients_candidats.keys():
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                poblacio_descartada_den_nutpat13[br].add(id_cip_sec)
                resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT06", "DEN")] += 1
                if id_cip_sec in interaccions_referent_pacients["NUT"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT06", "NUM")] += 1                                                  

        # NUTPAT07

        for br, pacients_candidats in problemes[("Malaltia inflamat�ria intestinal", "mixta", "actiu")].items():
            for id_cip_sec in pacients_candidats.keys():
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                poblacio_descartada_den_nutpat13[br].add(id_cip_sec)
                resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT07", "DEN")] += 1
                if id_cip_sec in interaccions_referent_pacients["NUT"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT07", "NUM")] += 1     

        # NUTPAT08

        for br, pacients_candidats in problemes[("IRC", "mixta", "actiu")].items():
            for id_cip_sec in pacients_candidats.keys():
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                poblacio_descartada_den_nutpat13[br].add(id_cip_sec)
                resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT08", "DEN")] += 1
                if id_cip_sec in interaccions_referent_pacients["NUT"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT08", "NUM")] += 1  

        # NUTPAT09

        for br, pacients_candidats in problemes[("Intoler�ncia o al�l�rgia aliment�ria", "mixta", "actiu")].items():
            for id_cip_sec in pacients_candidats.keys():
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                poblacio_descartada_den_nutpat13[br].add(id_cip_sec)
                resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT09", "DEN")] += 1
                if id_cip_sec in interaccions_referent_pacients["NUT"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT09", "NUM")] += 1  

        # NUTPAT10

        for br, pacients_candidats in problemes[("DM2", "adulta", "actiu")].items():
            for id_cip_sec in pacients_candidats.keys():
                if 14 < assignada_tot[id_cip_sec]["edat"] <= 80 and id_cip_sec in variables[("HBA1C", "adulta", "ultims 18 mesos")][br]:
                    glicades_sup10 = 0
                    for valor in variables[("HBA1C", "adulta", "ultims 18 mesos")][br][id_cip_sec]["tot"].values():
                        if valor > 10:
                            glicades_sup10 += 1
                    if variables[("HBA1C", "adulta", "ultims 18 mesos")][br][id_cip_sec]["ultim"]["valor"] > 10 and glicades_sup10 >= 2:
                        sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                        poblacio_descartada_den_nutpat13[br].add(id_cip_sec)
                        resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT10", "DEN")] += 1
                        if id_cip_sec in interaccions_referent_pacients["NUT"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                            resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT10", "NUM")] += 1

        # NUTPAT11

        pacients_nutpat11_num = set()
        for br in ((set(variables[("IMC", "adulta", "ultims 12 mesos")]) | set(variables[("IMC", "adulta", "sempre")])) & (set(variables[("RCV", "adulta", "ultims 12 mesos")]) | set(variables[("RCV", "adulta", "sempre")]))):
            if br_sectors[br] == sector:
                pacients_candidats = ((set(variables[("IMC", "adulta", "ultims 12 mesos")][br]) | set(variables[("IMC", "adulta", "sempre")][br])) & (set(variables[("RCV", "adulta", "ultims 12 mesos")][br]) | set(variables[("RCV", "adulta", "sempre")][br])))
                for id_cip_sec in pacients_candidats:
                    condicio_imc, condicio_rcv = False, False
                    if id_cip_sec in variables[("IMC", "adulta", "ultims 12 mesos")][br]:
                        for valor in variables[("IMC", "adulta", "ultims 12 mesos")][br][id_cip_sec]["tot"].values():
                            if 30 <= valor <= 35:
                                condicio_imc = True
                                break
                    elif id_cip_sec in variables[("IMC", "adulta", "sempre")][br]:
                        valor = variables[("IMC", "adulta", "sempre")][br][id_cip_sec]["ultim"]["valor"]
                        if 30 <= valor <= 35:
                            condicio_imc = True
                    if id_cip_sec in variables[("RCV", "adulta", "ultims 12 mesos")][br]:
                        for valor in variables[("RCV", "adulta", "ultims 12 mesos")][br][id_cip_sec]["tot"].values():
                            if valor > 10:
                                condicio_rcv = True
                                break
                    elif id_cip_sec in variables[("RCV", "adulta", "sempre")][br]:
                        valor = variables[("RCV", "adulta", "sempre")][br][id_cip_sec]["ultim"]["valor"]
                        if valor > 10:
                            condicio_rcv = True
                    if condicio_imc and condicio_rcv:
                        sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                        poblacio_descartada_den_nutpat13[br].add(id_cip_sec)
                        resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT11", "DEN")] += 1
                        if id_cip_sec in interaccions_referent_pacients["NUT"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                            resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT11", "NUM")] += 1
                            pacients_nutpat11_num.add(id_cip_sec)

        # NUTPAT12

        pacients_nutpat12_num = set()
        for br in (set(variables[("IMC", "adulta", "ultims 12 mesos")]) | set(variables[("IMC", "adulta", "sempre")])) & (set(problemes[("AVC", "adulta", "sempre")]) | set(problemes[("CI", "adulta", "sempre")])):
            if br_sectors[br] == sector:
                pacients_candidats = (set(variables[("IMC", "adulta", "ultims 12 mesos")][br]) | set(variables[("IMC", "adulta", "sempre")][br])) & (set(problemes[("AVC", "adulta", "sempre")][br]) | set(problemes[("CI", "adulta", "sempre")][br]))
                for id_cip_sec in pacients_candidats:
                    pacient_candidat_den = False
                    if id_cip_sec in variables[("IMC", "adulta", "ultims 12 mesos")][br]:
                        for valor in variables[("IMC", "adulta", "ultims 12 mesos")][br][id_cip_sec]["tot"].values():
                            if 30 <= valor <= 35:
                                pacient_candidat_den = True
                                break
                    elif id_cip_sec in variables[("IMC", "adulta", "sempre")][br]:
                        valor = variables[("IMC", "adulta", "sempre")][br][id_cip_sec]["ultim"]["valor"]
                        if 30 <= valor <= 35:
                            pacient_candidat_den = True
                    if pacient_candidat_den:
                        sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                        poblacio_descartada_den_nutpat13[br].add(id_cip_sec)
                        resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT12", "DEN")] += 1
                        if id_cip_sec in interaccions_referent_pacients["NUT"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                            pacients_nutpat12_num.add(id_cip_sec)
                            resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT12", "NUM")] += 1

        # NUTPAT13

        for id_cip_sec in assignada_tot:
            codi_sector, br = (assignada_tot[id_cip_sec][key] for key in ("codi_sector", "br"))
            if codi_sector == sector and id_cip_sec not in poblacio_descartada_den_nutpat13[br]:
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT13", "DEN")] += 1
                if id_cip_sec in interaccions_referent_pacients["NUT"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT13", "NUM")] += 1

        # NUTPAT14

        for br, pacients_candidats in problemes[("DM2", "adulta", "actiu")].items():
            for id_cip_sec in pacients_candidats.keys():
                if 14 < assignada_tot[id_cip_sec]["edat"] <= 80 and id_cip_sec in variables[("HBA1C", "adulta", "ultims 18 mesos")][br]:
                    glicades_8a10 = 0
                    for valor in variables[("HBA1C", "adulta", "ultims 18 mesos")][br][id_cip_sec]["tot"].values():
                        if 8 <= valor <= 10:
                            glicades_8a10 += 1
                    if 8 <= variables[("HBA1C", "adulta", "ultims 18 mesos")][br][id_cip_sec]["ultim"]["valor"] <= 10 and glicades_8a10 >= 2:
                        sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                        poblacio_descartada_den_nutpat13[br].add(id_cip_sec)
                        resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT14", "DEN")] += 1
                        if id_cip_sec in interaccions_referent_pacients["NUT"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                            resultats_counter["NUT"]["PAT"][(br, edat_lv, sexe, poblatip, "NUTPAT14", "NUM")] += 1

        print("Done NUTPAT")
                    
    if 1: # FISPAT

        resultats_fispat = list()

        # FISPAT01

        for br, pacients_candidats in problemes[("Dolor cr�nic columna", "mixta", "actiu ultims 12 mesos. 6 setmanes evoluci�")].items():
            for id_cip_sec in pacients_candidats.keys():
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                poblacio_descartada_den_fispat07[br].add(id_cip_sec)
                resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT01", "DEN")] += 1
                num=0
                if id_cip_sec in interaccions_referent_pacients["FIS"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT01", "NUM")] += 1
                    num=1
                br = assignada_tot[id_cip_sec]["br"]
                resultats_fispat.append((id_cip_sec, br, edat_lv, sexe, poblatip, "FISPAT01", 1, num, len(variables[("IA0201", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0201", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0202", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0202", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0203", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0203", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0204", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0204", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0205", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0205", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0206", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0206", "mixta", "ultims 15 mesos")][br] else 0))
                

        # FISPAT02

        for br, pacients_candidats in problemes[("S�ndrome subacromial", "mixta", "actiu ultims 12 mesos. 6 setmanes evoluci�")].items():
            for id_cip_sec in pacients_candidats.keys():
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                poblacio_descartada_den_fispat07[br].add(id_cip_sec)
                resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT02", "DEN")] += 1
                num=0
                if id_cip_sec in interaccions_referent_pacients["FIS"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT02", "NUM")] += 1
                    num=1
                br = assignada_tot[id_cip_sec]["br"]
                resultats_fispat.append((id_cip_sec, br, edat_lv, sexe, poblatip, "FISPAT02", 1, num, len(variables[("IA0201", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0201", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0202", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0202", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0203", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0203", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0204", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0204", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0205", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0205", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0206", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0206", "mixta", "ultims 15 mesos")][br] else 0))

        # FISPAT03

        for br, pacients_candidats in problemes[("Artrosi espatlla", "mixta", "actiu ultims 12 mesos. 6 setmanes evoluci�")].items():
            for id_cip_sec in pacients_candidats.keys():
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                poblacio_descartada_den_fispat07[br].add(id_cip_sec)
                resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT03", "DEN")] += 1
                num=0
                if id_cip_sec in interaccions_referent_pacients["FIS"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT03", "NUM")] += 1
                    num=1
                br = assignada_tot[id_cip_sec]["br"]
                resultats_fispat.append((id_cip_sec, br, edat_lv, sexe, poblatip, "FISPAT03", 1, num, len(variables[("IA0201", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0201", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0202", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0202", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0203", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0203", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0204", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0204", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0205", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0205", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0206", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0206", "mixta", "ultims 15 mesos")][br] else 0))

        # FISPAT04

        for br, pacients_candidats in problemes[("Gono-coxartrosi", "mixta", "actiu ultims 12 mesos. 6 setmanes evoluci�")].items():
            for id_cip_sec in pacients_candidats.keys():
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                poblacio_descartada_den_fispat07[br].add(id_cip_sec)
                resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT04", "DEN")] += 1
                num=0
                if id_cip_sec in interaccions_referent_pacients["FIS"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT04", "NUM")] += 1
                    num=1
                br = assignada_tot[id_cip_sec]["br"]
                resultats_fispat.append((id_cip_sec, br, edat_lv, sexe, poblatip, "FISPAT04", 1, num, len(variables[("IA0201", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0201", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0202", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0202", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0203", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0203", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0204", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0204", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0205", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0205", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0206", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0206", "mixta", "ultims 15 mesos")][br] else 0))

        # FISPAT05

        for br, pacients_candidats in problemes[("Gono-femoropatelar", "mixta", "actiu ultims 12 mesos. 6 setmanes evoluci�")].items():
            for id_cip_sec in pacients_candidats.keys():
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                poblacio_descartada_den_fispat07[br].add(id_cip_sec)
                resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT05", "DEN")] += 1
                num=0
                if id_cip_sec in interaccions_referent_pacients["FIS"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT05", "NUM")] += 1
                    num=1
                br = assignada_tot[id_cip_sec]["br"]
                resultats_fispat.append((id_cip_sec, br, edat_lv, sexe, poblatip, "FISPAT05", 1, num, len(variables[("IA0201", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0201", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0202", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0202", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0203", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0203", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0204", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0204", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0205", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0205", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0206", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0206", "mixta", "ultims 15 mesos")][br] else 0))

        # FISPAT06

        for br, pacients_candidats in problemes[("Senilitat", "mixta", "actiu ultims 12 mesos. 6 setmanes evoluci�")].items():
            for id_cip_sec in pacients_candidats.keys():
                pacient_candidat_den = False
                if id_cip_sec not in variables[("Barthel", "mixta", "sempre")][br]:
                    pacient_candidat_den = True
                elif variables[("Barthel", "mixta", "sempre")][br][id_cip_sec]["ultim"]["valor"] > 80:
                    pacient_candidat_den = True
                if pacient_candidat_den:
                    sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                    poblacio_descartada_den_fispat07[br].add(id_cip_sec)
                    resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT06", "DEN")] += 1
                    num=0
                    if id_cip_sec in interaccions_referent_pacients["FIS"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                        resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT06", "NUM")] += 1
                        num=1
                    br = assignada_tot[id_cip_sec]["br"]
                    resultats_fispat.append((id_cip_sec, br, edat_lv, sexe, poblatip, "FISPAT06", 1, num, len(variables[("IA0201", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0201", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0202", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0202", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0203", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0203", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0204", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0204", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0205", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0205", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0206", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0206", "mixta", "ultims 15 mesos")][br] else 0))


        # FISPAT08

        for br, pacients_candidats in problemes[("Dolor d'espatlla de llarga durada", "mixta", "actiu ultims 12 mesos. 6 setmanes evoluci�")].items():
            for id_cip_sec in pacients_candidats.keys():
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                poblacio_descartada_den_fispat07[br].add(id_cip_sec)
                resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT08", "DEN")] += 1
                num=0
                if id_cip_sec in interaccions_referent_pacients["FIS"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT08", "NUM")] += 1
                    num=1
                br = assignada_tot[id_cip_sec]["br"]
                resultats_fispat.append((id_cip_sec, br, edat_lv, sexe, poblatip, "FISPAT08", 1, num, len(variables[("IA0201", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0201", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0202", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0202", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0203", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0203", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0204", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0204", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0205", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0205", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0206", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0206", "mixta", "ultims 15 mesos")][br] else 0))

        # FISPAT09

        for br, pacients_candidats in problemes[("Gon�lgia", "mixta", "actiu ultims 12 mesos. 6 setmanes evoluci�")].items():
            for id_cip_sec, dates in pacients_candidats.items():
                for data_ini_dx, data_fi_dx in dates:
                    data_fi_dx_corretgida = data_ext if data_fi_dx > data_ext else data_fi_dx
                    if u.monthsBetween(data_ini_dx, data_fi_dx_corretgida) >= 6:
                        sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                        poblacio_descartada_den_fispat07[br].add(id_cip_sec)
                        resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT09", "DEN")] += 1
                        num=0
                        if id_cip_sec in interaccions_referent_pacients["FIS"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                            resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT09", "NUM")] += 1
                            num=1
                        br = assignada_tot[id_cip_sec]["br"]
                        resultats_fispat.append((id_cip_sec, br, edat_lv, sexe, poblatip, "FISPAT09", 1, num, len(variables[("IA0201", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0201", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0202", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0202", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0203", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0203", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0204", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0204", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0205", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0205", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0206", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0206", "mixta", "ultims 15 mesos")][br] else 0))

                        break

        # FISPAT07

        for id_cip_sec in assignada_tot:
            codi_sector, br = (assignada_tot[id_cip_sec][key] for key in ("codi_sector", "br"))
            if codi_sector == sector and id_cip_sec not in poblacio_descartada_den_fispat07[br]:
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT07", "DEN")] += 1
                num=0
                if id_cip_sec in interaccions_referent_pacients["FIS"]["ultims 12 mesos"]["visita i/o activitat liderada referent"]:
                    resultats_counter["FIS"]["PAT"][(br, edat_lv, sexe, poblatip, "FISPAT07", "NUM")] += 1
                    num=1
                br = assignada_tot[id_cip_sec]["br"]
                resultats_fispat.append((id_cip_sec, br, edat_lv, sexe, poblatip, "FISPAT07", 1, num, len(variables[("IA0201", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0201", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0202", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0202", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0203", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0203", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0204", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0204", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0205", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0205", "mixta", "ultims 15 mesos")][br] else 0, len(variables[("IA0206", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]) if id_cip_sec in variables[("IA0206", "mixta", "ultims 15 mesos")][br] else 0))

        print("Done FISPAT")
                    
    if 1: # BENRES

        # # BENRES01

        # for id_cip_sec in set(interaccions_referent_pacients["BEN"]["ultims 12 mesos"]["activitat liderada referent"]):
        #     br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
        #     if (br, edat_lv, sexe, poblatip, "BENPOB01", "DEN") in resultats_counter["BEN"]["POB"]:
        #         resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES01", "DEN")] += 1
        #         if id_cip_sec in variables[("Escala de benestar mental", "adulta", "ultims 12 mesos")][br]:
        #             resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES01", "NUM")] += 1
        #             resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES01A", "DEN")] += 1
        #             resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES01B", "DEN")] += 1
        #             data_antiga = min(variables[("Escala de benestar mental", "adulta", "ultims 12 mesos")][br][id_cip_sec]["tot"])
        #             primer_valor_test = variables[("Escala de benestar mental", "adulta", "ultims 12 mesos")][br][id_cip_sec]["tot"][data_antiga]
        #             if primer_valor_test >= 27:
        #                 resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES01A", "NUM")] += 1
        #             else:
        #                 resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES01B", "NUM")] += 1
        
        # BENRES02, BENRES03, BENRES04

        for id_cip_sec, dades_grupals in interaccions_referent_pacients["BEN"]["finalitzada ultims 15 a 3 mesos"]["activitat liderada referent finalitzada"].items():
            min_data_fi_grupal = min(dades_grupals[dada][1] for dada in dades_grupals)
            br, sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("br", "sexe", "edat_lv", "poblatip")])
            if (br, edat_lv, sexe, poblatip, "BENPOB01", "DEN") in resultats_counter["BEN"]["POB"]:
                resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES02", "DEN")] += 1
                if id_cip_sec in variables[("Escala de benestar mental", "adulta", "ultims 15 a 3 mesos")][br]:
                    nombre_tests = len(variables[("Escala de benestar mental", "adulta", "ultims 15 a 3 mesos")][br][id_cip_sec]["tot"])
                    if nombre_tests > 1:
                        resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES02", "NUM")] += 1
                        resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES03", "DEN")] += 1
                        resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES04", "DEN")] += 1
                        resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES11", "DEN")] += 1

                        data_keys = sorted(variables[("Escala de benestar mental", "adulta", "ultims 15 a 3 mesos")][br][id_cip_sec]["tot"].keys(), reverse=True)
                        data_antiga, data_ultima = data_keys[1], data_keys[0]  
                        dies_entre_vs = 1 if 30 <= u.daysBetween(data_antiga, data_ultima) < 120 else 0
                        test_fet_despres_grupal = 1 if min_data_fi_grupal <= data_ultima else 0

                        if dies_entre_vs and test_fet_despres_grupal:
                            primer_valor_test = variables[("Escala de benestar mental", "adulta", "ultims 15 a 3 mesos")][br][id_cip_sec]["tot"][data_antiga]
                            ultim_valor_test = variables[("Escala de benestar mental", "adulta", "ultims 15 a 3 mesos")][br][id_cip_sec]["tot"][data_ultima]
                            
                            if ultim_valor_test - primer_valor_test > 0:
                                resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES03", "NUM")] += 1
                            else:
                                resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES11", "NUM")] += 1
                            if ultim_valor_test > 27 and primer_valor_test <= 27:
                                resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, "BENRES04", "NUM")] += 1

        condicions_indicadors = [
            ("BENRES05", {"IA0201": ("descens", 1), "IA0202": ("descens", 1), "IA0203": ("descens", 1), "IA0204": ("descens", 1), "IA0205": ("descens", 1), "IA0206": ("augment", 1)}, "millora i no empitjorament"),
            ("BENRES06", {"VP70C0": ("descens BENRES06", 1)}, "millora"),
            ("BENRES07", {"IN5301": ("descens BENRES07", 1)}, "millora"),
            ("BENRES08", {"EZ3001": ("ascens BENRES08", 1)}, "millora"),
            ("BENRES12", {"IA0201": ("descens", 1), "IA0202": ("descens", 1), "IA0203": ("descens", 1), "IA0204": ("descens", 1), "IA0205": ("descens", 1), "IA0206": ("augment", 1)}, "empitjorament i no millora"),
            ("BENRES13", {"VP70C0": ("augment BENRES13", 1)}, "empitjorament"),
        ]
        for codi, descs_vs, criteri in condicions_indicadors:
            codis_vs = set(descs_vs.keys())
            brs_variables = reduce(set.intersection, (set(variables[(codi_vs, "mixta", "ultims 15 mesos")]) for codi_vs in codis_vs))
            for br in brs_variables:
                pacients_variables = reduce(set.intersection, (set(variables[(codi_vs, "mixta", "ultims 15 mesos")][br]) for codi_vs in codis_vs))
                pacients_candidats = pacients_variables & set(interaccions_referent_pacients["BEN"]["finalitzada ultims 15 a 3 mesos"]["activitat liderada referent finalitzada"])
                for id_cip_sec in pacients_candidats:
                    condicions_vs, dates_vs = c.defaultdict(dict), c.defaultdict(dict)
                    for codi_vs in codis_vs:
                        if len(variables[(codi_vs, "mixta", "sempre")][br][id_cip_sec]["tot"]) >= 2:
                            sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][clau] for clau in ("sexe", "edat_lv", "poblatip")])
                            
                            data_keys = sorted(variables[(codi_vs, "mixta", "sempre")][br][id_cip_sec]["tot"].keys(), reverse=True)
                            data_antiga, data_ultima = data_keys[1], data_keys[0]  
                            valor_antic, valor_ultim = (variables[(codi_vs, "mixta", "sempre")][br][id_cip_sec]["tot"][data] for data in [data_antiga, data_ultima])
                            
                            condicions_vs["antiga"][codi_vs], condicions_vs["�ltima"][codi_vs] = valor_antic, valor_ultim
                            dates_vs[codi_vs]["antiga"], dates_vs[codi_vs]["�ltima"] = data_antiga, data_ultima
                        
                    falten_codis_vs = 0 if len(condicions_vs["�ltima"]) == len(condicions_vs["antiga"]) == len(codis_vs) else 1
                    dies_entre_vs = 1 if all(30 <= u.daysBetween(dates_vs[codi_vs]["antiga"], dates_vs[codi_vs]["�ltima"]) < 120 for codi_vs in dates_vs) else 0
                    dades_grupals = interaccions_referent_pacients["BEN"]["finalitzada ultims 15 a 3 mesos"]["activitat liderada referent finalitzada"][id_cip_sec]
                    min_data_fi_grupal = min(dades_grupals[dada][1] for dada in dades_grupals)
                    test_fet_despres_grupal = 1 if min_data_fi_grupal <= data_ultima else 0

                    if not falten_codis_vs and dies_entre_vs and test_fet_despres_grupal:

                        resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, codi, "DEN")] += 1

                        empitjorament = 0
                        millora = 0

                        if descs_vs[codi_vs][0] == "descens BENRES06":
                            if condicions_vs["antiga"][codi_vs] >= 15 and condicions_vs["�ltima"][codi_vs] < 15:
                                millora = 1
                            elif 10 <= condicions_vs["antiga"][codi_vs] < 15 and condicions_vs["�ltima"][codi_vs] < 10:
                                millora = 1
                            elif 5 <= condicions_vs["antiga"][codi_vs] < 10 and condicions_vs["�ltima"][codi_vs] < 5:
                                millora = 1

                        if descs_vs[codi_vs][0] == "descens BENRES07":
                            if assignada_tot[id_cip_sec] >= 18:
                                if 8 <= condicions_vs["antiga"][codi_vs] <= 14 and condicions_vs["�ltima"][codi_vs] < 8:
                                    millora = 1
                                elif 15 <= condicions_vs["antiga"][codi_vs] <= 21 and condicions_vs["�ltima"][codi_vs] < 15:
                                    millora = 1
                                elif 22 <= condicions_vs["antiga"][codi_vs] <= 28 and condicions_vs["�ltima"][codi_vs] < 21:
                                    millora = 1

                        if descs_vs[codi_vs][0] == "ascens BENRES08":
                            if assignada_tot[id_cip_sec] >= 18:
                                if 3 <= condicions_vs["antiga"][codi_vs] <= 8 and 8 < condicions_vs["�ltima"][codi_vs]:
                                    millora = 1
                                elif 9 <= condicions_vs["antiga"][codi_vs] <= 11 and 11 < condicions_vs["�ltima"][codi_vs]:
                                    millora = 1

                        elif descs_vs[codi_vs][0] == "augment BENRES13":
                            if condicions_vs["antiga"][codi_vs] < 15 and 15 <= condicions_vs["�ltima"][codi_vs]:
                                empitjorament = 1
                            elif condicions_vs["antiga"][codi_vs] < 10 and 10 <= condicions_vs["�ltima"][codi_vs] :
                                empitjorament = 1
                            elif condicions_vs["antiga"][codi_vs] < 5 and 5 <= condicions_vs["�ltima"][codi_vs]:
                                empitjorament = 1

                        else:

                            for codi_vs in codis_vs:
                                if descs_vs[codi_vs][0] == "augment":
                                    if condicions_vs["�ltima"][codi_vs] < condicions_vs["antiga"][codi_vs]*descs_vs[codi_vs][1]:
                                        empitjorament = 1
                                elif descs_vs[codi_vs][0] == "descens":
                                    if condicions_vs["�ltima"][codi_vs] > condicions_vs["antiga"][codi_vs]*descs_vs[codi_vs][1]:
                                        empitjorament = 1

                                if descs_vs[codi_vs][0] == "augment":
                                    if condicions_vs["�ltima"][codi_vs] > condicions_vs["antiga"][codi_vs]*descs_vs[codi_vs][1]:
                                        millora = 1
                                elif descs_vs[codi_vs][0] == "descens":
                                    if condicions_vs["�ltima"][codi_vs] < condicions_vs["antiga"][codi_vs]*descs_vs[codi_vs][1]:
                                        millora = 1


                        if (criteri == "millora i no empitjorament" and millora and not empitjorament) or (criteri == "empitjorament i no millora" and empitjorament and not millora) or (criteri == "millora" and millora) or (criteri == "empitjorament" and empitjorament):
                            resultats_counter["BEN"]["RES"][(br, edat_lv, sexe, poblatip, codi, "NUM")] += 1


        print("Done BENRES")
                                
    if 1: # NUTRES:

        # NUTRES01

        for br in set(problemes[("Malnutrici� o desnutrici�", "mixta", "actiu ultims 15 mesos")]) | set(variables[("MNA", "mixta", "ultims 15 mesos")]):
            pacients_candidats = (set(problemes[("Malnutrici� o desnutrici�", "mixta", "actiu ultims 15 mesos")][br]) | set(variables[("MNA", "mixta", "ultims 15 mesos")][br])) & (set(interaccions_referent_pacients["NUT"]["finalitzada ultims 15 a 3 mesos"]["activitat liderada referent finalitzada"]) | set(interaccions_referent_pacients["NUT"]["ultims 15 a 3 mesos"]["visites amb referent"]))
            for id_cip_sec in pacients_candidats:
                pacient_candidat_den = False
                if id_cip_sec in problemes[("Malnutrici� o desnutrici�", "mixta", "actiu ultims 15 mesos")][br].keys():
                    pacient_candidat_den = True
                elif id_cip_sec in variables[("MNA", "mixta", "ultims 15 mesos")][br]:
                    for valor in variables[("MNA", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"].values():
                        if valor <= 23.5:
                            pacient_candidat_den = True
                            break
                if pacient_candidat_den:
                    sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                    resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES01", "DEN")] += 1
                    condicio_num_malnutricio, condicio_num_mna = False, False
                    if not id_cip_sec in problemes[("Malnutrici� o desnutrici�", "mixta", "actiu")][br].keys():
                        condicio_num_malnutricio = True
                    if id_cip_sec in variables[("MNA", "mixta", "ultims 15 mesos")][br]:
                        if variables[("MNA", "mixta", "ultims 15 mesos")][br][id_cip_sec]["ultim"]["valor"] > 23.5:
                            condicio_num_mna = True
                    else:
                        condicio_num_mna = True
                    if condicio_num_malnutricio and condicio_num_mna:
                        resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES01", "NUM")] += 1

        # NUTRES02

        for br in set(problemes[("Disf�gia", "mixta", "actiu ultims 15 mesos")]) | set(variables[("Disf�gia", "mixta", "ultims 15 mesos")]):
            pacients_candidats = (set(problemes[("Disf�gia", "mixta", "actiu ultims 15 mesos")][br]) | set(variables[("Disf�gia", "mixta", "ultims 15 mesos")][br])) & set(interaccions_referent_pacients["NUT"]["ultims 15 a 3 mesos"]["visita i/o activitat liderada referent"])
            for id_cip_sec in pacients_candidats:
                pacient_candidat_den = False
                if id_cip_sec in problemes[("Disf�gia", "mixta", "actiu ultims 15 mesos")][br].keys():
                    pacient_candidat_den = True
                elif id_cip_sec in variables[("Disf�gia", "mixta", "ultims 15 mesos")][br]:
                    for valor in variables[("Disf�gia", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"].values():
                        if valor >= 3:
                            pacient_candidat_den = True
                            break
                if pacient_candidat_den:
                    sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                    resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES02", "DEN")] += 1
                    condicio_num_ps, condicio_num_vs = False, False
                    if not id_cip_sec in problemes[("Disf�gia", "mixta", "actiu")][br].keys():
                        condicio_num_ps = True
                    if id_cip_sec in variables[("Disf�gia", "mixta", "ultims 15 mesos")][br]:
                        data_max = variables[("Disf�gia", "mixta", "ultims 15 mesos")][br][id_cip_sec]["ultim"]["data"]
                        valor_min = 9999999
                        for data_vs in variables[("Disf�gia", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"]:
                            if data_vs != data_max:
                                valor = variables[("Disf�gia", "mixta", "ultims 15 mesos")][br][id_cip_sec]["tot"][data_vs]
                                valor_min = valor if valor < valor_min else valor_min
                        if variables[("Disf�gia", "mixta", "ultims 15 mesos")][br][id_cip_sec]["ultim"]["valor"] < valor_min:
                            condicio_num_vs = True
                    else:
                        condicio_num_vs = True
                    if condicio_num_ps and condicio_num_vs:
                        resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES02", "NUM")] += 1

        # NUTRES03
        
        for br in variables[("Pes percentil", "pedia", "ultims 15 mesos")]:
            for id_cip_sec in set(variables[("Pes percentil", "pedia", "ultims 15 mesos")][br]) & set(interaccions_referent_pacients["NUT"]["ultims 15 a 3 mesos"]["visita i/o activitat liderada referent"]):
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                for valor in variables[("Pes percentil", "pedia", "ultims 15 mesos")][br][id_cip_sec]["tot"].values():
                    if valor >= ">P95":
                        resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES03", "DEN")] += 1
                        if variables[("Pes percentil", "pedia", "ultims 15 mesos")][br][id_cip_sec]["ultim"]["valor"] < ">P95":
                            resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES03", "NUM")] += 1
                        break # Break important per no seguir iterant mesures de pes del �ltims 15 mesos

        # NUTRES04-05

        for br in (set(variables[("IMC", "adulta", "ultims 15 mesos")]) | set(variables[("IMC", "adulta", "sempre")])):
            for id_cip_sec in (set(variables[("IMC", "adulta", "ultims 15 mesos")][br]) | set(variables[("IMC", "adulta", "ultims 15 mesos")][br])) & set(interaccions_referent_pacients["NUT"]["ultims 15 a 3 mesos"]["visita i/o activitat liderada referent"]):
                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                candidat_den = False
                if id_cip_sec in variables[("IMC", "adulta", "ultims 15 mesos")][br]:
                    for valor in variables[("IMC", "adulta", "ultims 15 mesos")][br][id_cip_sec]["tot"].values():                            
                        if valor > 35:
                            candidat_den = True
                            break
                elif id_cip_sec in variables[("IMC", "adulta", "sempre")][br]:
                    if variables[("IMC", "adulta", "sempre")][br][id_cip_sec]["ultim"]["valor"] > 35:
                        candidat_den = True
                if candidat_den:
                    resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES04", "DEN")] += 1
                    resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES05", "DEN")] += 1
                    
                    if id_cip_sec in variables[("Pes", "adulta", "ultims 15 mesos")][br]:
                        for valor in variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["tot"].values():                            
                            data_max = variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["ultim"]["data"]
                            valor_max = 0
                            for data_vs in variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["tot"]:
                                if data_vs != data_max:
                                    valor = variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["tot"][data_vs]
                                    valor_max = valor if valor_max < valor else valor_max
                        if variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["ultim"]["valor"] <= valor_max*0.95:
                            resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES04", "NUM")] += 1
                            if variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["ultim"]["valor"] <= valor_max*0.90:
                                resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES05", "NUM")] += 1

        # NUTRES06-07
        for br in (set(variables[("IMC", "adulta", "ultims 15 mesos")]) | set(variables[("IMC", "adulta", "sempre")])) & (set(variables[("RCV", "adulta", "ultims 15 mesos")]) | set(variables[("RCV", "adulta", "sempre")])):
            pacients_candidats = (set(variables[("IMC", "adulta", "ultims 15 mesos")][br]) | set(variables[("IMC", "adulta", "sempre")][br])) & ((set(variables[("RCV", "adulta", "ultims 15 mesos")][br]) | set(variables[("RCV", "adulta", "sempre")][br]))) & set(interaccions_referent_pacients["NUT"]["ultims 15 a 3 mesos"]["visita i/o activitat liderada referent"])
            for id_cip_sec in pacients_candidats:
                condicio_imc, condicio_rcv = False, False
                if id_cip_sec in variables[("IMC", "adulta", "ultims 15 mesos")][br]:
                    for valor in variables[("IMC", "adulta", "ultims 15 mesos")][br][id_cip_sec]["tot"].values():
                        if 30 <= valor <= 35:
                            condicio_imc = True
                            break
                elif id_cip_sec in variables[("IMC", "adulta", "sempre")][br]:
                    if 30 <= variables[("IMC", "adulta", "sempre")][br][id_cip_sec]["ultim"]["valor"] <= 35:
                        condicio_imc = True
                if id_cip_sec in variables[("RCV", "adulta", "ultims 15 mesos")][br]:
                    for valor in variables[("RCV", "adulta", "ultims 15 mesos")][br][id_cip_sec]["tot"].values():
                        if valor > 10:
                            condicio_rcv = True
                            break
                elif id_cip_sec in variables[("RCV", "adulta", "sempre")][br]:
                    valor = variables[("RCV", "adulta", "sempre")][br][id_cip_sec]["ultim"]["valor"]
                    if valor > 10:
                        condicio_rcv = True
                if condicio_imc and condicio_rcv:
                    sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                    resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES06", "DEN")] += 1
                    resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES07", "DEN")] += 1
                    
                    if id_cip_sec in variables[("Pes", "adulta", "ultims 15 mesos")][br]:
                        for valor in variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["tot"].values():                            
                            data_max = variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["ultim"]["data"]
                            valor_max = 0
                            for data_vs in variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["tot"]:
                                if data_vs != data_max:
                                    valor = variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["tot"][data_vs]
                                    valor_max = valor if valor_max < valor else valor_max
                        if variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["ultim"]["valor"] <= valor_max*0.95:
                            resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES06", "NUM")] += 1
                            if variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["ultim"]["valor"] <= valor_max*0.90:
                                resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES07", "NUM")] += 1

        # NUTRES08-09

        pacients_nutres08_den = set()
        for br in (set(variables[("IMC", "adulta", "ultims 15 mesos")]) | set(variables[("IMC", "adulta", "sempre")])) & (set(problemes[("AVC", "adulta", "sempre")]) | set(problemes[("CI", "adulta", "sempre")])):
            pacients_candidats = (set(variables[("IMC", "adulta", "ultims 15 mesos")][br]) | set(variables[("IMC", "adulta", "sempre")][br])) & (set(problemes[("AVC", "adulta", "sempre")][br]) | set(problemes[("CI", "adulta", "sempre")][br])) & set(interaccions_referent_pacients["NUT"]["ultims 15 a 3 mesos"]["visita i/o activitat liderada referent"])
            for id_cip_sec in pacients_candidats:
                candidat_den = False
                if id_cip_sec in variables[("IMC", "adulta", "ultims 15 mesos")][br]:
                    for valor in variables[("IMC", "adulta", "ultims 15 mesos")][br][id_cip_sec]["tot"].values():
                        if 30 <= valor <= 35:
                            candidat_den = True
                            break
                elif id_cip_sec in variables[("IMC", "adulta", "sempre")][br]:
                    if 30 <= variables[("IMC", "adulta", "sempre")][br][id_cip_sec]["ultim"]["valor"] <= 35:
                        candidat_den = True
                if candidat_den:
                    sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                    pacients_nutres08_den.add(id_cip_sec)
                    resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES08", "DEN")] += 1
                    resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES09", "DEN")] += 1
                    
                    if id_cip_sec in variables[("Pes", "adulta", "ultims 15 mesos")][br]:
                        for valor in variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["tot"].values():                            
                            data_max = variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["ultim"]["data"]
                            valor_max = 0
                            for data_vs in variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["tot"]:
                                if data_vs != data_max:
                                    valor = variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["tot"][data_vs]
                                    valor_max = valor if valor_max < valor else valor_max
                        if variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["ultim"]["valor"] <= valor_max*0.95:
                            resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES08", "NUM")] += 1
                            if variables[("Pes", "adulta", "ultims 15 mesos")][br][id_cip_sec]["ultim"]["valor"] <= valor_max*0.90:
                                resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES09", "NUM")] += 1
        u.createTable("pacients_nutres08_den", "(id_cip_sec int(11))", "altres", rm=True)
        u.listToTable([[id] for id in pacients_nutres08_den], "pacients_nutres08_den", "altres")

        # NUTRES10

        for br in (set(variables[("HBA1C", "adulta", "sempre")]) & set(problemes[("DM2", "adulta", "actiu")])):
            pacients_candidats = (set(variables[("HBA1C", "adulta", "sempre")][br])) & set(problemes[("DM2", "adulta", "actiu")][br]) & set(interaccions_referent_pacients["NUT"]["ultims 18 a 6 mesos"]["visita i/o activitat liderada referent"])
            for id_cip_sec in pacients_candidats:
                if 14 < assignada_tot[id_cip_sec]["edat"] <= 80:
                    if id_cip_sec in variables[("HBA1C", "adulta", "sempre")][br]:
                        data_primera_visita_periode = min(interaccions_referent_pacients["NUT"]["ultims 18 a 6 mesos"]["visita i/o activitat liderada referent"][id_cip_sec])
                        ultima_data_abans_data_primera_visita_periode = next((data for data in reversed(variables[("HBA1C", "adulta", "sempre")][br][id_cip_sec]["tot"].keys()) if data <= data_primera_visita_periode), None)
                        if ultima_data_abans_data_primera_visita_periode:
                            ultim_valor_abans_data_primera_visita_periode = variables[("HBA1C", "adulta", "sempre")][br][id_cip_sec]["tot"][ultima_data_abans_data_primera_visita_periode]
                            if 8 <= ultim_valor_abans_data_primera_visita_periode:
                                sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                                resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES10", "DEN")] += 1
                                if id_cip_sec in variables[("HBA1C", "adulta", "sempre")][br]:
                                    if variables[("HBA1C", "adulta", "sempre")][br][id_cip_sec]["ultim"]["valor"] < 8:
                                        resultats_counter["NUT"]["RES"][(br, edat_lv, sexe, poblatip, "NUTRES10", "NUM")] += 1

        print("Done NUTRES")

    if 1: # FISRES:

        condicions_indicadors = [
            ("FISRES01", {"IA0201": ("descens", 1), "IA0202": ("descens", 1), "IA0203": ("descens", 1), "IA0204": ("descens", 1), "IA0205": ("descens", 1), "IA0206": ("augment", 1)}, [], [], "millora i no empitjorament"),
            ("FISRES01A", {"IA0201": ("descens", 1), "IA0202": ("descens", 1), "IA0203": ("descens", 1), "IA0204": ("descens", 1), "IA0205": ("descens", 1), "IA0206": ("augment", 1)}, [("Dolor cr�nic columna", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�")], [], "millora i no empitjorament"),
            ("FISRES01B", {"IA0201": ("descens", 1), "IA0202": ("descens", 1), "IA0203": ("descens", 1), "IA0204": ("descens", 1), "IA0205": ("descens", 1), "IA0206": ("augment", 1)}, [("Dolor d'espatlla de llarga durada", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�")], [], "millora i no empitjorament"),
            ("FISRES01C", {"IA0201": ("descens", 1), "IA0202": ("descens", 1), "IA0203": ("descens", 1), "IA0204": ("descens", 1), "IA0205": ("descens", 1), "IA0206": ("augment", 1)}, [("Gon�lgia", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�")], [], "millora i no empitjorament"),
            ("FISRES01D", {"IA0201": ("descens", 1), "IA0202": ("descens", 1), "IA0203": ("descens", 1), "IA0204": ("descens", 1), "IA0205": ("descens", 1), "IA0206": ("augment", 1)}, [("Senilitat", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�"),], [("Barthel", 80)], "millora i no empitjorament"),
            ("FISRES02", {"VL4001": ("descens", 0.9)}, [("Lumb�lgia", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�"), ("Patologia de columna lumbar", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�")], [], "millora"),
            ("FISRES03", {"VL3001": ("descens", 0.9)}, [("Patolog�a de columna cervical", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�")], [], "millora"),
            ("FISRES04", {"VL5003": ("descens", 0.9)}, [("Dolor d'espatlla de llarga durada", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�")], [], "millora"),
            ("FISRES05", {"IA0201": ("descens", 1), "IA0202": ("descens", 1), "IA0203": ("descens", 1), "IA0204": ("descens", 1), "IA0205": ("descens", 1), "IA0206": ("augment", 1)}, [], [], "empitjorament i no millora"),
            ("FISRES05A", {"IA0201": ("descens", 1), "IA0202": ("descens", 1), "IA0203": ("descens", 1), "IA0204": ("descens", 1), "IA0205": ("descens", 1), "IA0206": ("augment", 1)}, [("Dolor cr�nic columna", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�")], [], "empitjorament i no millora"),
            ("FISRES05B", {"IA0201": ("descens", 1), "IA0202": ("descens", 1), "IA0203": ("descens", 1), "IA0204": ("descens", 1), "IA0205": ("descens", 1), "IA0206": ("augment", 1)}, [("Dolor d'espatlla de llarga durada", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�")], [], "empitjorament i no millora"),
            ("FISRES05C", {"IA0201": ("descens", 1), "IA0202": ("descens", 1), "IA0203": ("descens", 1), "IA0204": ("descens", 1), "IA0205": ("descens", 1), "IA0206": ("augment", 1)}, [("Gon�lgia", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�")], [], "empitjorament i no millora"),
            ("FISRES05D", {"IA0201": ("descens", 1), "IA0202": ("descens", 1), "IA0203": ("descens", 1), "IA0204": ("descens", 1), "IA0205": ("descens", 1), "IA0206": ("augment", 1)}, [("Senilitat", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�"),], [("Barthel", 80)], "empitjorament i no millora"),
            ("FISRES06", {"VL4001": ("descens", 1.1)}, [("Lumb�lgia", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�"), ("Patologia de columna lumbar", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�")], [], "empitjorament"),
            ("FISRES07", {"VL3001": ("descens", 1.1)}, [("Patolog�a de columna cervical", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�")], [], "empitjorament"),
            ("FISRES08", {"VL5003": ("descens", 1.1)}, [("Dolor d'espatlla de llarga durada", "mixta", "actiu ultims 15 mesos. 6 setmanes evoluci�")], [], "empitjorament"),
        ]

        for codi, descs_vs, descs_ps, descs_vs_2, criteri in condicions_indicadors:
            codis_vs = set(descs_vs.keys())
            brs_variables = reduce(set.intersection, (set(variables[(codi_vs, "mixta", "ultims 15 mesos")]) for codi_vs in codis_vs))
            for br in brs_variables:
                pacients_variables = reduce(set.intersection, (set(variables[(codi_vs, "mixta", "ultims 15 mesos")][br]) for codi_vs in codis_vs))
                pacients_problemes = reduce(set.intersection, (set(problemes[desc_ps][br]) for desc_ps in descs_ps)) if descs_ps else set()
                pacients_candidats = (pacients_variables & pacients_problemes & set(interaccions_referent_pacients["FIS"]["finalitzada ultims 15 a 3 mesos"]["activitat liderada referent finalitzada"])) if pacients_problemes else (pacients_variables & set(interaccions_referent_pacients["FIS"]["finalitzada ultims 15 a 3 mesos"]["activitat liderada referent finalitzada"]))
                for id_cip_sec in pacients_candidats:
                    condicions_vs, dates_vs = c.defaultdict(dict), c.defaultdict(dict)
                    for codi_vs in codis_vs:
                        if len(variables[(codi_vs, "mixta", "sempre")][br][id_cip_sec]["tot"]) >= 2:
                            sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][clau] for clau in ("sexe", "edat_lv", "poblatip")])
                            
                            data_keys = sorted(variables[(codi_vs, "mixta", "sempre")][br][id_cip_sec]["tot"].keys(), reverse=True)
                            data_antiga, data_ultima = data_keys[1], data_keys[0]  
                            valor_antic, valor_ultim = (variables[(codi_vs, "mixta", "sempre")][br][id_cip_sec]["tot"][data] for data in [data_antiga, data_ultima])
                            
                            condicions_vs["antiga"][codi_vs], condicions_vs["�ltima"][codi_vs] = valor_antic, valor_ultim
                            dates_vs[codi_vs]["antiga"], dates_vs[codi_vs]["�ltima"] = data_antiga, data_ultima
                        
                    falten_codis_vs = 0 if len(condicions_vs["�ltima"]) == len(condicions_vs["antiga"]) == len(codis_vs) else 1
                    dies_entre_vs = 1 if all(30 <= u.daysBetween(dates_vs[codi_vs]["antiga"], dates_vs[codi_vs]["�ltima"]) < 120 for codi_vs in dates_vs) else 0
                    dades_grupals = interaccions_referent_pacients["FIS"]["finalitzada ultims 15 a 3 mesos"]["activitat liderada referent finalitzada"][id_cip_sec]
                    min_data_fi_grupal = min(dades_grupals[dada][1] for dada in dades_grupals)
                    test_fet_despres_grupal = 1 if min_data_fi_grupal <= data_ultima else 0

                    cond_vs_2 = 0
                    if descs_vs_2:
                        for desc_vs, valor_llindar in descs_vs_2:
                            if id_cip_sec not in variables[(desc_vs, "mixta", "sempre")][br] or variables[(desc_vs, "mixta", "sempre")][br][id_cip_sec]["ultim"]["valor"] > valor_llindar:
                                cond_vs_2 = 1
                    else:
                        cond_vs_2 = 1

                    if not falten_codis_vs and dies_entre_vs and cond_vs_2 and test_fet_despres_grupal:

                        empitjorament = 0
                        millora = 0

                        for codi_vs in codis_vs:
                            if descs_vs[codi_vs][0] == "augment":
                                if condicions_vs["�ltima"][codi_vs] < condicions_vs["antiga"][codi_vs]*descs_vs[codi_vs][1]:
                                    empitjorament = 1
                            elif descs_vs[codi_vs][0] == "descens":
                                if condicions_vs["�ltima"][codi_vs] > condicions_vs["antiga"][codi_vs]*descs_vs[codi_vs][1]:
                                    empitjorament = 1

                            if descs_vs[codi_vs][0] == "augment":
                                if condicions_vs["�ltima"][codi_vs] > condicions_vs["antiga"][codi_vs]*descs_vs[codi_vs][1]:
                                    millora = 1
                            elif descs_vs[codi_vs][0] == "descens":
                                if condicions_vs["�ltima"][codi_vs] < condicions_vs["antiga"][codi_vs]*descs_vs[codi_vs][1]:
                                    millora = 1

                        resultats_counter["FIS"]["RES"][(br, edat_lv, sexe, poblatip, codi, "DEN")] += 1
                        if (criteri == "millora i no empitjorament" and millora and not empitjorament) or (criteri == "empitjorament i no millora" and empitjorament and not millora) or (criteri == "millora" and millora) or (criteri == "empitjorament" and empitjorament):
                            resultats_counter["FIS"]["RES"][(br, edat_lv, sexe, poblatip, codi, "NUM")] += 1

        print("Done FISRES")

    if 1: # NUTDER

        # NUTDER01
        
        for br in variables[("IMC", "adulta", "ultims 12 mesos")]:
            if br_sectors[br] == codi_sector:
                for id_cip_sec in set(variables[("IMC", "adulta", "ultims 12 mesos")][br]):
                    sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                    for data_vs, valor in sorted(variables[("IMC", "adulta", "sempre")][br][id_cip_sec]["tot"].items()):
                        if valor > 35:
                            if data_ext_menys1any < data_vs:
                                resultats_counter["NUT"]["DER"][(br, edat_lv, sexe, poblatip, "NUTDER01", "DEN")] += 1
                                if any(data_derivacio >= data_vs for data_derivacio in derivacions_endocrinologia[id_cip_sec]):
                                    resultats_counter["NUT"]["DER"][(br, edat_lv, sexe, poblatip, "NUTDER01", "NUM")] += 1
                            break

        # NUTDER02
        
        for br in (set(variables[("HBA1C", "adulta", "ultims 12 mesos")]) & set(problemes[("DM2", "adulta", "actiu")])):
            for id_cip_sec in (set(variables[("HBA1C", "adulta", "ultims 12 mesos")][br])) & set(problemes[("DM2", "adulta", "actiu")][br]):
                if 14 < assignada_tot[id_cip_sec]["edat"] <= 80:
                    sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][key] for key in ("sexe", "edat_lv", "poblatip")])
                    glicades_sup10 = 0
                    for valor in variables[("HBA1C", "adulta", "ultims 12 mesos")][br][id_cip_sec]["tot"].values():
                        if valor > 10:
                            glicades_sup10 += 1
                    if glicades_sup10 >= 2:
                        for data_vs, valor in sorted(variables[("HBA1C", "adulta", "sempre")][br][id_cip_sec]["tot"].items()):
                            if valor > 10:
                                if data_ext_menys1any < data_vs:
                                    resultats_counter["NUT"]["DER"][(br, edat_lv, sexe, poblatip, "NUTDER02", "DEN")] += 1
                                    if any(data_derivacio >= data_vs for data_derivacio in derivacions_endocrinologia[id_cip_sec]):
                                        resultats_counter["NUT"]["DER"][(br, edat_lv, sexe, poblatip, "NUTDER02", "NUM")] += 1
                                break

        print("Done NUTDER")

    if 1: # FISDER

        condicions_indicadors = [
            ("FISDER01", "Dolor cr�nic columna"),
            ("FISDER02", "Dolor d'espatlla de llarga durada"),
            ("FISDER03", "Gon�lgia"),
        ]
        for codi, desc_ps in condicions_indicadors:
            for br in problemes[(desc_ps, "mixta", "6 setmanes evoluci�. iniciat ultim any 6 mesos")]:
                for id_cip_sec in problemes[(desc_ps, "mixta", "6 setmanes evoluci�. iniciat ultim any 6 mesos")][br]:
                    sexe, edat_lv, poblatip = tuple([assignada_tot[id_cip_sec][clau] for clau in ("sexe", "edat_lv", "poblatip")])
                    resultats_counter["FIS"]["DER"][(br, edat_lv, sexe, poblatip, codi, "DEN")] += 1
                    min_data_inici_ps = min([data_inici_ps for data_inici_ps, _ in problemes[(desc_ps, "mixta", "6 setmanes evoluci�. iniciat ultim any 6 mesos")][br][id_cip_sec]])
                    if id_cip_sec in derivacions: 
                        if min_data_inici_ps <= derivacions[id_cip_sec]:
                            resultats_counter["FIS"]["DER"][(br, edat_lv, sexe, poblatip, codi, "NUM")] += 1

        print("Done FISDER")

    # Indicadors amb numerador cero

    for rol in ("BEN", "NUT", "FIS"):
        for ind_cat in ("GEN", "PAT", "POB", "RES", "DER"):
            if ind_cat in resultats_counter[rol]:
                for br, edat_lv, sexe, poblatip, ind, tip in resultats_counter[rol][ind_cat]:
                    if tip == "DEN" and (br, edat_lv, sexe, poblatip, ind, "NUM") not in resultats_counter[rol][ind_cat]:
                        resultats_cero[rol][ind_cat][(br, edat_lv, sexe, poblatip, ind, "NUM")] = 0
                resultats_counter[rol][ind_cat].update(resultats_cero[rol][ind_cat])

    # def export_klx():
    """."""

    db_name = "altres"
    tb_arrel = "exp_khalix_"

    rols_dict = {"BEN": "benestar_emocional", "NUT": "nutricionistes", "FIS": "fisioterapeutes"}
    ind_cats = ("GEN", "PAT", "POB", "RES", "DER")

    for rol, tb_rol in rols_dict.items():
        for ind_cat in ind_cats:
            if ind_cat in resultats_counter[rol]:

                tb_name = tb_arrel + tb_rol + "_" + ind_cat.lower() if ind_cat != "GEN" else tb_arrel + tb_rol
                arxiu = rol.upper() + ind_cat.upper() if ind_cat != "GEN" else rol.upper()

                upload_data = [(ind, br, edat, sexe, poblatip, analisis, n) for (br, edat, sexe, poblatip, ind, analisis), n in resultats_counter[rol][ind_cat].items()]
                u.listToTable(upload_data, tb_name, db_name)

def create_taules():
    """."""

    db_name = "altres"
    tb_arrel = "exp_khalix_"

    rols_dict = {"BEN": "benestar_emocional", "NUT": "nutricionistes", "FIS": "fisioterapeutes"}
    ind_cats = ("GEN", "PAT", "POB", "RES", "DER")

    for rol, tb_rol in rols_dict.items():
        for ind_cat in ind_cats:

            tb_name = tb_arrel + tb_rol + "_" + ind_cat.lower() if ind_cat != "GEN" else tb_arrel + tb_rol

            u.createTable(tb_name, "(ind varchar(10), br varchar(5), edat varchar(30), sexe varchar(7), poblatip varchar(30), analisis varchar(5), n double)", db_name, rm=True)

def export_klx():

    db_name = "altres"
    tb_arrel = "exp_khalix_"

    rols_dict = {"BEN": "benestar_emocional", "NUT": "nutricionistes", "FIS": "fisioterapeutes"}
    ind_cats = ("GEN", "PAT", "POB", "RES", "DER")

    for rol, tb_rol in rols_dict.items():
        for ind_cat in ind_cats:

            tb_name = tb_arrel + tb_rol + "_" + ind_cat.lower() if ind_cat != "GEN" else tb_arrel + tb_rol
            arxiu = rol.upper() + ind_cat.upper() if ind_cat != "GEN" else rol.upper()

            sql = """
                    SELECT DISTINCT
                        ind,
                        "Aperiodo",
                        br,
                        analisis,
                        edat,
                        poblatip,
                        sexe,
                        "N",
                        n
                    FROM
                        {}.{}
                """.format(db_name, tb_name)
            any_row = u.getOne(sql, db_name)
            if any_row:
                u.exportKhalix(sql, arxiu)

def get_indicadors():
    """."""
    # for sector in u.sectors:
    #     sub_get_indicadors(sector)
    #     print(sector)
    u.multiprocess(sub_get_indicadors, u.sectors, 4)
    

if u.IS_MENSUAL:

    get_data_extraccio();                           print("get_data_extraccio()")
    get_centres();                                  print("get_centres()")
    get_assignada();                                print("get_assignada()")
    get_professionals_referents();                  print("get_professionals_referents()")
    create_taules();                                print("create_taules()")
    get_variables();                                print("get_variables()")
    get_indicadors();                               print("get_indicadors()")
    export_klx();                                   print("export_klx()")