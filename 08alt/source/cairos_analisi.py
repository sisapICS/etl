# -*- coding: utf-8 -*-

import collections as c
import sisapUtils as u
import datetime as d
import sisaptools as su
from dateutil.relativedelta import relativedelta

u.printTime('inici')

DEXTD = u.getOne('select data_ext from dextraccio', 'nodrizas')[0]

sql = """
    SELECT id_cip_sec
    FROM pacients_activitats_nous_professionals
    WHERE procedencia = 'grupal4'
    AND rol_creador = 'BEN'
    AND year(data_intervencio) = 2023
    AND tipus_activitat IN ('PG', 'TA')
    AND tema_activitat = 'SM'
    AND pagr_situacio = 'S'
"""
activitat = set()
for id, in u.getAll(sql, 'nodrizas'):
    activitat.add(id)
u.printTime('activitat')

sql = """
    SELECT id_cip, id_cip_sec, hash_d
    FROM u11
"""
idcips = {hash: (idcip, id) for idcip, id, hash in u.getAll(sql, 'import')}
sql = """
    SELECT hash_redics, hash_covid
    FROM dwsisap.pdptb101_relacio_nia
"""
conversor = {}
for redics, covid in u.getAll(sql, 'exadata'):
    try:
        (idcip, id) = idcips[redics]
        conversor[covid] = (idcip, id)
    except KeyError:
        pass
u.printTime('conversor')

sql = "select partition_name, high_value from all_tab_partitions where table_name = 'SISAP_MASTER_VISITES'"
parts = {part: val for part, val in u.getAll(sql, 'exadata')}
partitions = set()
inici = d.date(2022,1,1)
for part, val in parts.items():
    try:
        data = val[10:20]
        data = d.datetime.strptime(data, '%Y-%m-%d')
        if data.date() >= inici and data.date() <= DEXTD:
            partitions.add(part)
    except:
        pass

def get_visites(params):
    part, sql = params
    visites = []
    for pacient, sector, data, up, origen, citacio, programacio_class, internet, servei, modul, atribut1, atribut2, tipus, tipus_class, etiqueta, situacio, sisap_situacio_codi in u.getAll(sql.format(part=part, DEXTD=DEXTD), 'exadata'):
        try:
            (idcip, id) = conversor[pacient]
            if id in activitat:
                visites.append((idcip, id, sector, data, up, origen, citacio, programacio_class, internet, servei, modul, atribut1, atribut2, tipus, tipus_class, etiqueta, situacio, sisap_situacio_codi))
        except KeyError:
            pass
    u.printTime(part)
    return visites


sql = """
    SELECT pacient, sector, data, up, origen, citacio, programacio_class, internet, servei, modul, atribut1, atribut2, tipus, tipus_class, etiqueta, situacio, sisap_situacio_codi
    FROM dwsisap.sisap_master_visites partition ({part})
    WHERE data BETWEEN DATE '2022-01-01' AND DATE '{DEXTD}'
"""
visites = []
jobs = [(part, sql) for part in partitions]
visites = []
for chunk in u.multiprocess(get_visites, jobs, 8, close=True):
    visites = visites + chunk
u.printTime('visites')

cols = "(id_cip int, id_cip_sec int, sector varchar(4), data_visita date, up varchar(10), origen varchar(18), citacio varchar(1), programacio varchar(1), internet varchar(1),\
        servei varchar(5), modul varchar(5), atribut1 varchar(5), atribut2 varchar(5), tipus varchar(4), tipus_class varchar(10), etiqueta varchar(4), situacio varchar(1), codi_situacio varchar(4))"
u.createTable('visites_cairos', cols, 'nodrizas', rm=True)
u.listToTable(visites, 'visites_cairos', 'nodrizas')
u.printTime('fi visites')


def get_tractaments(params):
    table, sql = params
    tractaments = []
    for idcip, id, atc, inici, fi, esp in u.getAll(sql.format(tb=table), 'import'):
        if id in activitat:
            tractaments.append((idcip, id, atc, inici, fi, esp))
    u.printTime(str(table))
    return tractaments

sql = """
    SELECT id_cip, id_cip_sec, ppfmc_atccodi, ppfmc_pmc_data_ini, ppfmc_data_fi, ppfmc_desc_esp
    FROM {tb}
    WHERE year(ppfmc_data_fi) >= 2022
    AND ppfmc_atccodi IN (
    'N03AE01', 'N05BA01', 'N05BA02', 'N05BA04', 'N05BA05', 'N05BA06',
    'N05BA08', 'N05BA09', 'N05BA10', 'N05BA11', 'N05BA12', 'N05BA13',
    'N05BA14', 'N05BA21', 'N05BA24', 'N05BA51', 'N05BA55', 'N05BA91',
    'N05CD01', 'N05CD02', 'N05CD03', 'N05CD05', 'N05CD06', 'N05CD08',
    'N05CD09', 'N05CD10', 'N05CD11', 'N05CF01', 'N05CF02', 'N05CF03')
"""
tables = u.getSubTables('tractaments')
jobs = [(table, sql) for table in tables]
tractaments = []
for chunk in u.multiprocess(get_tractaments, jobs, 8, close=True):
        tractaments = tractaments + chunk
u.printTime('tractaments')
cols = "(id_cip int, id_cip_sec int, atc varchar(7), data_inici date, data_fi date, desc_esp varchar(5))"
u.createTable('tractaments_cairos', cols, 'nodrizas', rm=True)
u.listToTable(tractaments, 'tractaments_cairos', 'nodrizas')
u.printTime('fi tractaments')


def get_variables(params):
    table, sql = params
    variables = []
    for idcip, id, data, val, up, usu in u.getAll(sql.format(tb=table), 'import'):
        if id in activitat:
            variables.append((idcip, id, data, val, up, usu))
    u.printTime(str(table))
    return variables
sql = """
    SELECT id_cip, id_cip_sec, vu_dat_act, vu_val, vu_up, vu_usu
    FROM {tb}
    WHERE vu_cod_vs = 'EZ5101'
"""

tables = u.getSubTables('variables')
jobs = [(table, sql) for table in tables]
variables = []
for chunk in u.multiprocess(get_variables, jobs, 8, close=True):
        variables = variables + chunk
u.printTime('variables')
cols = "(id_cip int, id_cip_sec int, data date, valor double, up varchar(5), usuari varchar(12))"
u.createTable('variables_cairos', cols, 'nodrizas', rm=True)
u.listToTable(variables, 'variables_cairos', 'nodrizas')
u.printTime('fi variables')