# coding: latin1

import sisapUtils as u
from datetime import datetime


def get_indicadors():
    sql = """select date_format(data_ext, '%d/%m/%Y'),
            date_format(date_add(data_ext,interval - 12 month), '%d/%m/%Y')
            from nodrizas.dextraccio"""
    for data_now, data_1_year in u.getAll(sql, 'nodrizas'):
        data_now = data_now
        data_1_year = data_1_year


    d1, d2 = {}, {}
    sql_num = """SELECT DISTINCT
            t151838.cei_dcomu_b_trunc   AS c1,
            t152338.anyact              AS c2,
            t152338.periode             AS c3,
            concat(t151946.oeit_norg, ' (') AS c4,
            t151838.eit_nsre            AS c5,
            t151838.oeit_coeit_c        AS c6,
            t151838.ultim_estat         AS c7,
            t151838.cei_dcomu_darrer_trunc as c8,
            t151838.pac_nia 			AS nia
        FROM
            bislt_pro_h.ft_icam_sictbcei_detall   t181862 /* Fets_FT_ICAM_SICTBCEI_DETALL_IS3_NIA */,
            bislt_pro_d.ep_up                     t152010 /* Dim_EP_UP_ICAM_AGG_Confirmacio */,
            bislt_pro_d.d_hc3_diagnostics         t152106 /* Dim_DIAGNOSTICS_UNIC_ICAM_AGG_Baixa */,
            bislt_pro_d.d_icam_sictboei           t151946 /* Dim_D_ICAM_SICTBOEI_ICAM_AGG_Confirmacio */,
            bislt_pro_d.d_dies                    t152338 /* Dim_D_DIES_ICAM_AGG_Data_efecte_darr_comunicat */,
            bislt_pro_h.ft_icam_sictbcei          t151838 /* Fets_FT_ICAM_SICTBCEI */
        WHERE
            ( t152106.dgn_cdi_codif NOT IN (
                ' B34.2',
                ' U07.1',
                ' Z20.828',
                ' Z29.8',
                'Z20.822'
            ) and t151838.upr_cup_st_c = t152010.cod_up
            AND t151838.DGN_IDU_B = t152106.dgn_idu
            AND t151838.cei_dcomu_darrer_trunc = t152338.data
            AND t151838.oeit_div_c = t151946.oeit_div
            AND t151838.oeit_coeit_c = t151946.oeit_coeit
            AND t151838.oeit_toeit_c = t151946.oeit_toeit
            AND t151838.eit_nsre = t181862.eit_nsre
            AND t152010.c_st_up = '21'
            --AND T152338.f_anyact = 1 /*Per si es vol filtrar un any sense haver de realitzar la consulta per tot històric*/
            AND t181862.eit_ctc = 'C'
            AND ((t151838.ultim_estat = 'A'
                        AND (t151838.cei_dcomu_darrer_trunc - t151838.CEI_DCOMU_B_TRUNC) + 1 >= 366
                        AND (to_date('{data_now}','DD/MM/YYYY') - t151838.CEI_DCOMU_B_TRUNC) + 1 >= 480
                        AND (to_date('{data_1_year}','DD/MM/YYYY') - t151838.CEI_DCOMU_B_TRUNC) + 1 <= 480
                )
                OR (t151838.ultim_estat <> 'A'
                    AND (to_date('{data_now}','DD/MM/YYYY') - t151838.CEI_DCOMU_B_TRUNC) + 1 >= 480
                    AND (to_date('{data_1_year}','DD/MM/YYYY') - t151838.CEI_DCOMU_B_TRUNC) + 1 <= 480)
            )
            )""".format(data_now=data_now, data_1_year=data_1_year)
    for c1, c2, c3, c4, c5, c6, c7, c8, nia in u.getAll(sql_num, 'exadata'):
        d1[(c6, c5, c4, c2, c3, nia)] = [c1, c7, c8]


    sql_den = """SELECT
            MIN(t180794.data_solicitud_trunc) AS c1,
            COUNT(DISTINCT t151838.eit_nsre) AS c2,
            concat(concat(concat(t151946.oeit_norg, ' ('), t151838.oeit_coeit_c), ')') AS c3,
            t151838.eit_nsre       AS c4,
            t152338.anyact         AS c5,
            t152338.periode        AS c6,
            concat(t151946.oeit_norg, ' (') AS c7,
            t151838.oeit_coeit_c   AS c8,
            t151838.pac_nia 	   AS nia
        FROM
            bislt_pro_h.ft_icam_sictbcei_detall   t181862 /* Fets_FT_ICAM_SICTBCEI_DETALL_IS3_NIA */, ( bislt_pro_d.d_dies                    t152338 /* Dim_D_DIES_ICAM_AGG_Data_efecte_darr_comunicat */
            INNER JOIN ( bislt_pro_d.d_icam_sictboei           t151946 /* Dim_D_ICAM_SICTBOEI_ICAM_AGG_Confirmacio */
            INNER JOIN ( bislt_pro_d.d_hc3_diagnostics         t152106 /* Dim_DIAGNOSTICS_UNIC_ICAM_AGG_Baixa */
            INNER JOIN ( bislt_pro_d.ep_up                     t152010 /* Dim_EP_UP_ICAM_AGG_Confirmacio */
            INNER JOIN bislt_pro_h.ft_icam_sictbcei          t151838 /* Fets_FT_ICAM_SICTBCEI */ ON t151838.upr_cup_st_c = t152010.cod_up ) ON t151838.dgn_idu_b = t152106.dgn_idu ) ON t151838.oeit_div_c = t151946.oeit_div
            AND t151838.oeit_coeit_c=t151946.oeit_coeit AND t151838.oeit_toeit_c=t151946.oeit_toeit) ON t151838.cei_dcomu_darrer_trunc=t152338.data)
            LEFT OUTER JOIN bislt_pro_h.ft_is3                    t180794 /* Fets_FT_IS3_ICAM_Agregada */ ON CAST(t151838.eit_nsre AS CHARACTER(25)) = CAST(substr(t180794.pre_id, 0, instr(t180794.pre_id, '-') - 1) AS CHARACTER(25))
        WHERE
            ( t152106.dgn_cdi_codif NOT IN (
                ' B34.2',
                ' U07.1',
                ' Z20.828',
                ' Z29.8',
                'Z20.822'
            )
            AND t151838.eit_nsre = t181862.eit_nsre
            AND t152010.c_st_up = '21'
            --AND T152338.f_anyact = 1 /*Per si es vol filtrar un any sense haver de realitzar la consulta per tot històric*/
            AND t181862.eit_ctc = 'C'
            AND ((t151838.ultim_estat = 'A'
                        AND (t151838.cei_dcomu_darrer_trunc - t151838.CEI_DCOMU_B_TRUNC) + 1 >= 366
                        AND (to_date('{data_now}','DD/MM/YYYY') - t151838.CEI_DCOMU_B_TRUNC) + 1 >= 480
                        AND (to_date('{data_1_year}','DD/MM/YYYY') - t151838.CEI_DCOMU_B_TRUNC) + 1 <= 480
                )
                OR (t151838.ultim_estat <> 'A'
                    AND (to_date('{data_now}','DD/MM/YYYY') - t151838.CEI_DCOMU_B_TRUNC) + 1 >= 480
                    AND (to_date('{data_1_year}','DD/MM/YYYY') - t151838.CEI_DCOMU_B_TRUNC) + 1 <= 480)
            )
            )
        GROUP BY
            t151838.oeit_coeit_c,
            t151838.eit_nsre,
            t152338.anyact,
            t152338.periode,
            concat(t151946.oeit_norg, ' ('),
            concat(concat(concat(t151946.oeit_norg, ' ('), t151838.oeit_coeit_c), ')'),
            t151838.pac_nia""".format(data_now=data_now, data_1_year=data_1_year)
    for c1, c2, c3, c4, c5, c6, c7, c8, nia in u.getAll(sql_den, 'exadata'):
        if nia == 4112338: print(c1, c2, c3, c4, c5, c6, c7, c8, nia)
        d2[(c8, c4, c7, c5, c6, nia)] = [c1, c2, c3]

    print(len(d1.keys()))
    print(len(d2.keys()))

    upload = []
    for key, value in d1.items():
        d1_c6, d1_c5, d1_c4, d1_c2, d1_c3, d1_nia = key 
        d1_c1, d1_c7, d1_c8 = value
        if key in d2:
            d2_c8, d2_c4, d2_c7, d2_c5, d2_c6, d2_nia = key 
            d2_c1, d2_c2, d2_c3 = d2[key]
            assolida = 0
            if d2_c1 and d1_c1 and d1_c8:
                if d2_c1 < d1_c1: assolida = 0
                elif d1_c7 == 'A' and d2_c1 > d1_c8: assolida = 0
                elif (d2_c1 - d1_c1).days + 1 >= 366 and (d2_c1 - d1_c1).days + 1 <= 400: assolida = 1
            upload.append((d1_c6, d2_c3, d1_c2, assolida, d1_nia))


    cols = """(up varchar2(10), up_darrer_cuminicat varchar2(50), total_it number, it_assolida number, nia number)"""
    table = 'sgam003'
    u.createTable(table, cols, 'exadata', rm=True)
    u.listToTable(upload, table, 'exadata')
    u.execute('grant select on sgam003 to DWSISAP_ROL', 'exadata')


if __name__ == "__main__":
    get_indicadors()

