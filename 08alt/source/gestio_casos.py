# -*- coding: latin1 -*-

import collections as c
import sisapUtils as u
import itertools as it
import dateutil.relativedelta
import datetime
import csv

##############################################################################################
"""
Llen�ament de retroactius:

1. Canviar data_ext, mensual = 1
2. Llen�ar tamb� retroactiu de 02nod/source/master_placures.py (per executar correctament la funci� get_placures() d'aquest script).
   
3. Si es tracta d'un retroactiu de fa m�s d'un any, s'haur� de modificar la funci� get_08(), per tal que no doni
   problemes la taula visites2, havent de fer multiproc�s.
"""
##############################################################################################

tb_klx = 'exp_khalix_up_gescasos'
tb_pac = 'mst_indicadors_pacient_gescasos'
tb_ecap = ('exp_ecap_gc_uba', 'exp_ecap_gc_pacient',
           'exp_ecap_gc_cataleg', 'exp_ecap_gc_catalegPare')
indicadors_amb_llistats_ecap = ('GC0001A', 'GC0002', 'GC0003', 'GC0004', 'GC0005', 'GC0007', 'GC0010', 'GC0011', 'GC0011A', 'GC0012', 'GC0013', 'GC0014')
indicadors_inversos = ('GC0020', 'GC0021', 'GC0022', 'GC0023')
db = 'altres'

sql = """
        SELECT
            data_ext
        FROM
            dextraccio
        """
dext, = u.getOne(sql, "nodrizas")
dext_menys1any = dext - dateutil.relativedelta.relativedelta(years=1)
dext_menys13mesos = dext - dateutil.relativedelta.relativedelta(months=13)
dext_menys26mesos = dext - dateutil.relativedelta.relativedelta(months=26)
dext_menys2anys = dext - dateutil.relativedelta.relativedelta(years=2)


class GestioCasos(object):

    def __init__(self):

        self.definicions_indicadors = dict()
        self.pacients = dict()
        
        self.get_professionals_GCAS();              print("self.get_professionals_GCAS()", datetime.datetime.now())
        self.get_poblacio();                        print("self.get_poblacio()", datetime.datetime.now())
        self.get_poblacio_pcc_maca();               print("self.get_poblacio_pcc_maca()", datetime.datetime.now())
        self.create_tables_klx();                   print("self.create_tables_klx()", datetime.datetime.now())
        self.get_metes();                           print("self.get_metes()", datetime.datetime.now())
        self.get_denominador();                     print("self.get_denominador()", datetime.datetime.now())
        self.get_placures();                        print("self.get_placures()", datetime.datetime.now())
        self.get_piic();                            print("self.get_piic()", datetime.datetime.now())
        self.get_visites();                         print("self.get_visites()", datetime.datetime.now())
        self.get_activitats();                      print("self.get_activitats()", datetime.datetime.now())
        self.get_ingresos();                        print("self.get_ingresos()", datetime.datetime.now())
        self.get_00();                              print("self.get_00()", datetime.datetime.now())
        self.get_01();                              print("self.get_01()", datetime.datetime.now())
        self.get_02();                              print("self.get_02()", datetime.datetime.now())
        self.get_03();                              print("self.get_03()", datetime.datetime.now())
        self.get_04();                              print("self.get_04()", datetime.datetime.now())
        self.get_05();                              print("self.get_05()", datetime.datetime.now())
        # self.get_06();                              print("self.get_06()", datetime.datetime.now())
        self.get_07();                              print("self.get_07()", datetime.datetime.now())
        self.get_08();                              print("self.get_08()", datetime.datetime.now())
        self.get_09();                              print("self.get_09()", datetime.datetime.now())
        self.get_10();                              print("self.get_10()", datetime.datetime.now())
        self.get_11();                              print("self.get_11()", datetime.datetime.now())
        self.get_12();                              print("self.get_12()", datetime.datetime.now())
        self.get_13();                              print("self.get_13()", datetime.datetime.now())
        self.get_14();                              print("self.get_14()", datetime.datetime.now())
        self.get_15();                              print("self.get_15()", datetime.datetime.now())
        self.get_16();                              print("self.get_16()", datetime.datetime.now())
        self.get_17();                              print("self.get_17()", datetime.datetime.now())
        self.get_18();                              print("self.get_18()", datetime.datetime.now())
        self.get_19();                              print("self.get_19()", datetime.datetime.now())
        self.get_20_21_22_23();                     print("self.get_20_21_22_23()", datetime.datetime.now())        
        self.export_mst();                          print("self.export_mst()", datetime.datetime.now())
        self.export_klx();                          print("self.export_klx()", datetime.datetime.now())
        self.get_u11();                             print("self.get_u11()", datetime.datetime.now())
        self.upload_tables_ecap();                  print("self.upload_tables_ecap()", datetime.datetime.now())


    def get_professionals_GCAS(self):
        """Obt� els conjunts de identificadors d'usuari i n�meros de col�legiat dels professionals de Gesti� de Casos."""

        global id_usuaris_professionals_GCAS, numcols_professionals_GCAS

        sql = """
                SELECT
                    ide_usuari
                FROM
                    import.cat_professionals
                WHERE
                    tipus = 'GCAS'
              """
        id_usuaris_professionals_GCAS = {ide_usuari for ide_usuari, in u.getAll(sql, "import")}

        sql = """
                SELECT
                    LEFT(ide_numcol, 8)
                FROM
                    cat_pritb799 a
                INNER JOIN cat_pritb992 b ON
                    a.rol_usu = b.ide_usuari
                    AND a.codi_sector = b.codi_sector
                WHERE
                    rol_rol = 'ECAP_GEST'
                    AND ide_numcol <> ''
              """
        numcols_professionals_GCAS = tuple(set([numcol for numcol, in u.getAll(sql, 'import')]))

    def get_poblacio(self):
        """Obt� les dades b�siques de la poblaci� assignada atesa, aix� com conversors d'id_cip_sec a hash_d i a cip."""

        global poblacio, nia2hash, nia2u11

        poblacio = dict()

        sql = """
                SELECT
                    id_cip_sec,
                    up,
                    data_naix,
                    institucionalitzat
                FROM
                    assignada_tot
                WHERE
                    ates = 1
              """
        for id_cip_sec, up, data_naix, institucionalitzat in u.getAll(sql, 'nodrizas'):
            edat = u.yearsBetween(data_naix, dext)
            poblacio[id_cip_sec] = {"up": up, "edat": edat, "institucionalitzat": institucionalitzat}

        sql = """
                SELECT
                    nia,
                    hash_redics
                FROM
                    dwsisap.pdptb101_relacio_nia
              """
        nia2hash = {hash_r: nia for nia, hash_r in u.getAll(sql, 'exadata')}

        sql = """
                SELECT
                    id_cip_sec,
                    hash_d
                FROM
                    u11
              """
        nia2u11 = {nia2hash[hash_r]: id_cip_sec for id_cip_sec, hash_r in u.getAll(sql, 'import') if hash_r in nia2hash}

    def get_poblacio_pcc_maca(self):
        """Obt� els pacients catalogats com a Pacient Cr�nic Complex (PCC), Model d'Atenci� a la Cronicitat Avan�ada (MACA) i ambd�s."""

        self.maca, self.pcc, self.pcc_maca = c.defaultdict(), c.defaultdict(), c.defaultdict()

        sql = """
                SELECT
                    id_cip_sec,
                    es_cod,
                    es_dde
                FROM
                    estats
                """.format(dext_menys2anys)
        for id_cip_sec, cod, data_ini in u.getAll(sql, "import"):
            if id_cip_sec in poblacio:
                es_pcc = 1 if cod == 'ER0001' else 0
                es_maca = 1 if cod == 'ER0002' else 0
                if es_maca and (id_cip_sec not in self.maca or data_ini <= self.maca[id_cip_sec]):
                    self.maca[id_cip_sec] = data_ini
                if es_pcc and (id_cip_sec not in self.pcc or data_ini <= self.pcc[id_cip_sec]):
                    self.pcc[id_cip_sec] = data_ini
                if id_cip_sec not in self.pcc_maca or data_ini <= self.pcc_maca[id_cip_sec]:
                    self.pcc_maca[id_cip_sec] = data_ini

    def create_tables_klx(self):
        """Crea les taules on s'exportar� les dades dels indicadors de Gesti� de casos."""

        u.createTable(tb_klx, '(indicador varchar(10), up varchar(10), tipus varchar(10), n int)', db, rm=True)
        u.createTable(tb_pac, '(id_cip_sec int, indicador varchar(10), up varchar(10), compleix int)', db, rm=True)

    def get_metes(self):
        """ Obt� les metes establertes pels indicadors de Gesti� de Casos. """

        def readCSV(file, sep='@', header=False):

            with open(file, 'r') as f:
                primera = True
                c = csv.reader(f, delimiter=sep)
                for row in c:
                    if header and primera:
                        primera = False
                    else:
                        yield row

        FILE_METES = "./dades_noesb/alt_gc_metes.txt"
        self.METES = {indicador: (mmin, mint, mmax) for indicador, mmin, mint, mmax in readCSV(FILE_METES, sep='@')}

    def get_denominador(self):
        """Obt� els pacients que han estat dintre del Programa de Gesti� de Casos durant l'�ltim any, i quants d'ells segueixena actius."""

        global GCAS_dates_sortida

        self.denominador, self.GCAS_dates, GCAS_dates_sortida, GCAS_dates_sortida_tot, self.actius, self.pacients_GCAS_per_up = dict(), c.defaultdict(set), dict(), c.defaultdict(set), set(), c.defaultdict(set)
        resultat = list()
        denominador_de = (1, 2, 3, 4, 8, 9, 10, 11, 16, 18)

        sql = """
                SELECT 
                    id_cip_sec,
                    gesc_estat,
                    gesc_data_alta,
                    CASE WHEN gesc_data_sortida = 0 THEN '{}' ELSE gesc_data_sortida END gesc_data_sortida
                FROM 
                    gescasos
                WHERE
                    gesc_estat IN ('A', 'T')
                    AND gesc_data_alta <= '{}'
                    AND (gesc_data_sortida = 0
                         OR gesc_data_sortida BETWEEN '{}' AND '{}')
              """.format(dext, dext, dext_menys1any, dext)
        for id_cip_sec, estat, data_alta, data_sortida in u.getAll(sql, 'import'):
            data_sortida = datetime.datetime.strptime(data_sortida, '%Y-%m-%d').date()
            if u.daysBetween(data_alta, data_sortida) >= 30 and id_cip_sec in poblacio:
                self.GCAS_dates[id_cip_sec].add((data_alta, data_sortida))
                GCAS_dates_sortida_tot[id_cip_sec].add(data_sortida)
                up = poblacio[id_cip_sec]["up"]
                self.denominador[id_cip_sec] = up
                self.pacients_GCAS_per_up[up].add(id_cip_sec)
                if estat == 'A':
                    self.actius.add(id_cip_sec)
        for id_cip_sec in GCAS_dates_sortida_tot:
            GCAS_dates_sortida[id_cip_sec] = (min(GCAS_dates_sortida_tot[id_cip_sec]), max(GCAS_dates_sortida_tot[id_cip_sec]))
            up = self.denominador[id_cip_sec]
            for i in denominador_de:
                if i != 8:
                    self.pacients[(id_cip_sec, 'GC{}'.format(str(i).zfill(4)), up)] = False
        for up, pac in self.pacients_GCAS_per_up.items():
            for i in denominador_de:
                resultat.append(('GC{}'.format(str(i).zfill(4)), up, 'DEN', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_placures(self):
        """Obt� les dades relacionades amb els Plans de Cures i les intervencions lligades a aquests."""

        self.PLACU = c.defaultdict(lambda: c.defaultdict(set))
        self.PLACU_risccaigudes = c.defaultdict(lambda: c.defaultdict(set))
        self.PLACU_intervencions = c.defaultdict(lambda: c.defaultdict((lambda: c.defaultdict(set))))

        sql = """
                SELECT
                    id_cip_sec,
                    pc_id,
                    pc_codi,
                    data_inici,
                    pc_ultima_intervencio_data,
                    flag_aguda,
                    flag_pc_alta_anual
                FROM
                    master_placures
                WHERE
                    data_fi >= '{}'
              """.format(dext_menys2anys)
        for id_cip_sec, PLACU_id, PLACU_codi, PLACU_data_inici, PLACU_data_ultima_intervencio, PLACU_aguda, PLACU_alta_anual in u.getAll(sql, "nodrizas"):
            if id_cip_sec in poblacio:
                up = poblacio[id_cip_sec]["up"]
                self.PLACU[up][id_cip_sec].add((PLACU_data_inici, PLACU_data_ultima_intervencio, PLACU_aguda, PLACU_alta_anual, PLACU_id))
                if PLACU_codi == 'PC0103':
                    self.PLACU_risccaigudes[up][id_cip_sec].add((PLACU_data_inici, PLACU_data_ultima_intervencio, PLACU_aguda, PLACU_alta_anual, PLACU_id))

        sql = """
                SELECT
                    id_cip_sec,
                    pc_id,
                    intervencio_data
                FROM
                    master_placures_intervencions
                WHERE
                    intervencio_data <= '{}'
              """.format(dext)
        for id_cip_sec, PLACU_id, PLACU_data_intervencio in u.getAll(sql, "nodrizas"):
            if id_cip_sec in poblacio:
                up = poblacio[id_cip_sec]["up"]
                self.PLACU_intervencions[up][id_cip_sec][PLACU_id].add(PLACU_data_intervencio)

    def get_piic(self):
        """Obt� els pacients amb el pla d'intervenci� individualitzat compartit (PIIC) (que en indicadors creuarem amb les persones amb diagn�stic de PCC o MACA)."""

        self.piic = c.defaultdict((lambda: c.defaultdict(set)))
        piirT = set()
        piirV = set()
        piirF = c.defaultdict(set)
        maca = set()
        sql = """
                SELECT
                    id_cip_sec,
                    mi_data_reg,
                    mi_cod_var,
                    left(mi_cod_var, 8)
                FROM
                    piic
                WHERE
                    left(mi_cod_var, 8) IN ('T4101001', 'T4101015', 'T4101014', 'T4101017', 'T4101002')
                    AND mi_data_reg BETWEEN '{}' AND '{}'
              """.format(dext_menys2anys, dext)
        for id_cip_sec, data_reg, codi, codi8 in u.getAll(sql, "import"):
            if id_cip_sec in poblacio:
                up = poblacio[id_cip_sec]["up"]
                if codi8 == 'T4101001':
                    self.piic[up][id_cip_sec].add(data_reg)
                elif codi8 == 'T4101014':
                    piirT.add((id_cip_sec, up, codi[-2:], data_reg))
                elif codi8 == 'T4101015':
                    piirV.add((id_cip_sec, up, codi[-2:]))
                elif codi8 == 'T4101017':
                    piirF[(id_cip_sec, up, data_reg)].add((codi[-2:]))
                else: # T4101002
                    maca.add(id_cip_sec)
        u.printTime('despres sql')
        for (id, up, codi, data) in piirT:
            if (id, up, codi) in piirV:
                self.piic[up][id].add(data)
        u.printTime('despres text-variable')
        for (id, up, data) in list(piirF.keys()):
            if len(piirF[(id, up, data)]) == 4:
                self.piic[up][id].add(data)
        u.printTime('despres F')
        for up in list(self.piic.keys()):
            for id in list(self.piic[up].keys()):
                if id in self.maca and id not in maca:
                    self.piic[up].pop(id, None)
        u.printTime('despres maca')

    def get_visites(self):
        """Obt� les visites realitzades als distints centres, i tamb� les realitzades espec�ficament per Gesti� de Casos."""

        self.visites, self.visites_professionals_GCAS = c.defaultdict(lambda: c.defaultdict(set)), c.defaultdict(lambda: c.defaultdict(set))

        serveis = ('INFG', 'UGC', 'GCAS')

        sql = """
                SELECT
                    id_cip_sec,
                    visi_data_visita,
                    visi_servei_codi_servei,
                    visi_col_prov_resp,
                    visi_tipus_visita
                FROM
                    visites2
                WHERE 
                    visi_data_visita BETWEEN '{}' AND '{}'
                    AND visi_situacio_visita = 'R'
                    AND s_espe_codi_especialitat NOT IN ('EXTRA', '10102')
                    AND visi_tipus_visita NOT IN ('EXTRA', 'EXT')
                    AND visi_modul_codi_modul NOT LIKE 'VCA%'
                    AND visi_modul_codi_modul NOT LIKE 'VCM%'
                    AND visi_modul_codi_modul NOT LIKE 'VCP%'
                    AND visi_modul_codi_modul NOT LIKE 'VCJ%'
                    AND visi_modul_codi_modul NOT IN ('VCAP', 'VCAP2', 'VCAP3')
            """.format(dext_menys1any, dext)
        for id_cip_sec, VISI_data, VISI_servei, VISI_numcol, VISI_tipus in u.getAll(sql, "import"):
            if id_cip_sec in poblacio:
                up = poblacio[id_cip_sec]["up"]
                self.visites[up][id_cip_sec].add((VISI_data, VISI_tipus))
                if VISI_servei in serveis or VISI_numcol[:8] in numcols_professionals_GCAS:
                    self.visites_professionals_GCAS[up][id_cip_sec].add((VISI_data, VISI_tipus))

    def get_activitats(self):
        """Obt� les dades de les taules activitats d'import que ser�n necessaries pel c�lcul dels indicadors de Gesti� de Casos."""

        self.activitats, self.activitats_professionals_GCAS = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(set))), c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(set)))
        codis_atc = dict()

        sql = """
                SELECT
                    criteri_codi,
                    agrupador
                FROM
                    eqa_criteris
                WHERE
                    agrupador IN (1031, 1034)
                """
        for criteri_codi, agrupador in u.getAll(sql, "nodrizas"):
            codis_atc[criteri_codi] = agrupador
        codis_act_tuple = tuple(codis_atc.keys())

        sql = """
                SELECT
                    id_cip_sec,
                    au_cod_ac,
                    au_dat_act,
                    au_usu
                FROM
                    import.{}
                WHERE
                    au_cod_ac IN {}
            """
        subtaules_activitats = get_subtaules_activitats()
        jobs = [(sql.format(table, codis_act_tuple), codis_atc, GCAS_dates_sortida, id_usuaris_professionals_GCAS) for table in subtaules_activitats]
        for sub_activitats, sub_activitats_professionals_GCAS in u.multiprocess(sub_get_activitats, jobs, 4):
        # for job in jobs:
            # sub_activitats, sub_activitats_professionals_GCAS = sub_get_activitats(job)
            for (agrupador, up, id_cip_sec, ACTIV_data) in sub_activitats:
                self.activitats[agrupador][up][id_cip_sec].add(ACTIV_data)
            for (agrupador, up, id_cip_sec, ACTIV_data) in sub_activitats_professionals_GCAS:
                self.activitats_professionals_GCAS[agrupador][up][id_cip_sec].add(ACTIV_data)

    def get_ingresos(self):

        self.ingressos = c.defaultdict(set)
        self.reingressos = c.defaultdict(set)

        sql = """
                SELECT
                    nia,
                    data_ingres
                FROM
                    dwcatsalut.tf_cmbdha
                WHERE
                    nia IS NOT NULL
                    AND data_alta BETWEEN DATE '{}' AND DATE '{}'
              """.format(dext_menys2anys, dext)
        for nia, ingres in u.getAll(sql, 'exadata'):
            if nia in nia2u11:
                id_cip_sec = nia2u11[nia]
                if id_cip_sec in poblacio:
                    self.ingressos[id_cip_sec].add(ingres)

        for id_cip_sec, dates in self.ingressos.items():
            dates = sorted(list(dates))
            for i in range(0, len(dates)-1):
                if (dates[i+1] - dates[i]).days <= 30:
                    self.reingressos[id_cip_sec].add(dates[i])

    def get_00(self):

        ind = 'GC0000'
        self.definicions_indicadors[ind] = 'Pacients inclosos en gesti� de casos'

        resultat = list()
        inclosos = c.Counter()
        
        for id_cip_sec in poblacio:
            up = poblacio[id_cip_sec]["up"]
            inclosos[up] += 1
        for up, n in inclosos.items():
            resultat.append(('GC0000', up, 'DEN', n))
        for up, n in self.pacients_GCAS_per_up.items():
            resultat.append(('GC0000', up, 'NUM', len(n)))

        u.listToTable(resultat, tb_klx, db)

    def get_01(self):

        ind = 'GC0001'
        self.definicions_indicadors[ind] = 'Pacients en gesti� de casos amb pla de cures'
        indA = 'GC0001A'
        self.definicions_indicadors[indA] = 'Pacients en gesti� de casos amb pla de cures cr�nic'
        indB = 'GC0001B'
        self.definicions_indicadors[indB] = 'Pacients en gesti� de casos amb pla de cures agut'

        resultat = list()
        numerador, numeradorA, numeradorB = c.defaultdict(set), c.defaultdict(set), c.defaultdict(set)

        for up in self.PLACU:
            for id_cip_sec in self.PLACU[up]:
                if id_cip_sec in self.denominador:
                    GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                    for PLACU in self.PLACU[up][id_cip_sec]:
                        PLACU_data_inici, PLACU_data_ultima_intervencio, PLACU_aguda, _, PLACU_id = PLACU
                        if u.yearsBetween(PLACU_data_inici, GCAS_data_sortida_min) == 0 or u.yearsBetween(PLACU_data_inici, GCAS_data_sortida_max) == 0:
                            numerador[up].add(id_cip_sec)
                            self.pacients[(id_cip_sec, ind, up)] = True
                            if (id_cip_sec, indA, up) not in self.pacients:
                                self.pacients[(id_cip_sec, indA, up)] = False
                            if (id_cip_sec, indB, up) not in self.pacients:
                                self.pacients[(id_cip_sec, indB, up)] = False
                            if not PLACU_aguda:
                                numeradorA[up].add(id_cip_sec)
                                self.pacients[(id_cip_sec, indA, up)] = True
                            elif PLACU_aguda:
                                numeradorB[up].add(id_cip_sec)
                                self.pacients[(id_cip_sec, indB, up)] = True
                        if self.PLACU_intervencions[up][id_cip_sec][PLACU_id]:
                            for PLACU_data_intervencio in self.PLACU_intervencions[up][id_cip_sec][PLACU_id]:
                                if u.yearsBetween(PLACU_data_intervencio, GCAS_data_sortida_min) == 0 or u.yearsBetween(PLACU_data_intervencio, GCAS_data_sortida_max) == 0:
                                    numerador[up].add(id_cip_sec)
                                    self.pacients[(id_cip_sec, ind, up)] = True
                                    if not PLACU_aguda:
                                        numeradorA[up].add(id_cip_sec)
                                        self.pacients[(id_cip_sec, indA, up)] = True
                                    elif PLACU_aguda:
                                        numeradorB[up].add(id_cip_sec)
                                        self.pacients[(id_cip_sec, indB, up)] = True

        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))
            resultat.append((indA, up, 'DEN', len(pac)))
            resultat.append((indB, up, 'DEN', len(pac)))

        for up, pac in numeradorA.items():
            resultat.append((indA, up, 'NUM', len(pac)))

        for up, pac in numeradorB.items():
            resultat.append((indB, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_02(self):

        ind = 'GC0002'
        self.definicions_indicadors[ind] = 'Pacients en gesti� de casos amb valoraci� integral'

        resultat = list()
        agrupadors_numerador = c.defaultdict(dict)
        numerador = c.defaultdict(set)
        denominador = c.defaultdict(set)
        sql = """
                SELECT
                    id_cip_sec,
                    agrupador,
                    data_var
                FROM
                    eqa_variables
                WHERE
                    agrupador IN (88, 89, 999, 751, 940, 1079)
              """
        for id_cip_sec, agrupador, REGIS_data in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in GCAS_dates_sortida:
                GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                if u.yearsBetween(REGIS_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(REGIS_data, GCAS_data_sortida_max) == 0:
                    agrupadors_numerador[id_cip_sec][agrupador] = 1
        sql = """
                SELECT
                    id_cip_sec,
                    ps,
                    dde
                FROM
                    eqa_problemes
                WHERE
                    ps in (86, 783)
              """

        for id_cip_sec, agrupador, data_dx in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in GCAS_dates_sortida:
                # GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                # if u.yearsBetween(data_dx, GCAS_data_sortida_min) == 0 or u.yearsBetween(data_dx, GCAS_data_sortida_max) == 0:
                agrupadors_numerador[id_cip_sec][agrupador] = 1

        codis = {
            'VMEDG': 'emocional',
            'EP5105': 'emocional',
            'TIRS': 'social',
            'VZ3005': 'social',
            'VA030I': 'fragilitat',
            'VA0302': 'fragilitat'
        }

        sql = """
            SELECT
                id_cip_sec, vu_cod_vs, vu_dat_act
            FROM
                variables2
            WHERE
                vu_cod_vs in {keys}
            """.format(keys=tuple(codis.keys()))

        for id_cip_sec, codi, data_dx in u.getAll(sql, 'import'):
            if id_cip_sec in GCAS_dates_sortida:
                GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                if u.yearsBetween(data_dx, GCAS_data_sortida_min) == 0 or u.yearsBetween(data_dx, GCAS_data_sortida_max) == 0:
                    agrupadors_numerador[id_cip_sec][codis[codi]] = 1

        for id_cip_sec in agrupadors_numerador:
            edat, institucionalitzat = poblacio[id_cip_sec]["edat"], poblacio[id_cip_sec]["institucionalitzat"]
            # cond_demencia_o_pfeiffer = 1 if agrupadors_numerador[id_cip_sec][86] + agrupadors_numerador[id_cip_sec][863] else 0
            cond_nutricional = 1 if 999 in agrupadors_numerador[id_cip_sec] else 0
            cond_funcional = 1 if 1079 in agrupadors_numerador[id_cip_sec] else 0
            cond_fragilitat = 1 if 918 in agrupadors_numerador[id_cip_sec] or 'fragilitat' in agrupadors_numerador[id_cip_sec] else 0
            cond_caigudes = 1 if 751 in agrupadors_numerador[id_cip_sec] or 940 in agrupadors_numerador[id_cip_sec] else 0
            cond_cognitiu = 1 if 88 in agrupadors_numerador[id_cip_sec] or 86 in agrupadors_numerador[id_cip_sec] or 783 in agrupadors_numerador[id_cip_sec] else 0
            cond_emocional = 1 if 'emocional' in agrupadors_numerador[id_cip_sec]  or 86 in agrupadors_numerador[id_cip_sec] else 0
            cond_social = 1 if 'social' in agrupadors_numerador[id_cip_sec] else 0
            cond_social_pedia = 1 if 89 in agrupadors_numerador[id_cip_sec] else 0
            if (edat > 14 and not institucionalitzat and ((cond_funcional and cond_cognitiu and cond_nutricional and cond_caigudes and cond_emocional and cond_social) or cond_fragilitat)) or  (edat > 14 and institucionalitzat and (cond_funcional and cond_cognitiu and cond_nutricional and cond_caigudes and cond_emocional)) or (edat <= 14 and cond_funcional and cond_social_pedia):
                up = self.denominador[id_cip_sec]
                numerador[up].add(id_cip_sec)
                self.pacients[(id_cip_sec, ind, up)] = True

        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_03(self):

        ind = 'GC0003'
        self.definicions_indicadors[ind] = "Pacients en gesti� de casos amb registre d'adher�ncia al tractament"
        
        resultat = list()
        numerador = c.defaultdict(set)

        sql = """
                SELECT
                    id_cip_sec,
                    data_var
                FROM
                    eqa_variables
                WHERE
                    agrupador = 701
                    AND data_var BETWEEN '{}' AND '{}'
              """.format(dext_menys2anys, dext)
        for id_cip_sec, REGIS_data in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.denominador:
                GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                if u.yearsBetween(REGIS_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(REGIS_data, GCAS_data_sortida_max) == 0:
                    up = self.denominador[id_cip_sec]
                    numerador[up].add(id_cip_sec)
                    self.pacients[(id_cip_sec, ind, up)] = True

        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_04(self):

        ind = 'GC0004'
        self.definicions_indicadors[ind] = 'Pacients en gesti� de casos amb cribratge de caigudes'

        resultat = list()
        numerador = c.defaultdict(set)

        for up in self.PLACU_risccaigudes:
            for id_cip_sec in self.PLACU_risccaigudes[up]:
                if id_cip_sec in self.denominador:
                    GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                    for PLACU in self.PLACU_risccaigudes[up][id_cip_sec]:
                        PLACU_data_inici, PLACU_data_ultima_intervencio = PLACU[0], PLACU[1]
                        if u.yearsBetween(PLACU_data_inici, GCAS_data_sortida_min) == 0 or u.yearsBetween(PLACU_data_inici, GCAS_data_sortida_max) == 0:
                            numerador[up].add(id_cip_sec)
                            self.pacients[(id_cip_sec, ind, up)] = True
                            break
                        if PLACU_data_ultima_intervencio:
                            if u.yearsBetween(PLACU_data_ultima_intervencio, GCAS_data_sortida_min) == 0 or u.yearsBetween(PLACU_data_ultima_intervencio, GCAS_data_sortida_max) == 0:
                                numerador[up].add(id_cip_sec)
                                self.pacients[(id_cip_sec, ind, up)] = True
                                break

        sqls = (("""
                    SELECT
                        id_cip_sec,
                        data_var
                    FROM
                        eqa_variables
                    WHERE
                        agrupador IN (97, 751, 940)
                        AND data_var BETWEEN '{}' AND '{}'
                 """.format(dext_menys2anys, dext), "nodrizas"),
                ("""
                    SELECT
                        id_cip_sec,
                        snnv_data_alta,
                        snnv_dus
                    FROM
                        pla2
                    WHERE
                        snnv_cod_nic = 6490
                        AND (snnv_data_alta BETWEEN '{}' AND '{}'
                        OR snnv_dus BETWEEN '{}' AND '{}')
                 """.format(dext_menys2anys, dext, dext_menys2anys, dext), "import"),
                ("""
                    SELECT
                        id_cip_sec,
                        cinp_data_alta
                    FROM
                        grupal1
                    WHERE
                        cinp_cod_int = '6490'
                        AND cinp_data_alta BETWEEN '{}' AND '{}'
                 """.format(dext_menys2anys, dext),
                 'import'),
                ("""
                    SELECT
                        id_cip_sec
                    FROM
                        eqa_problemes
                    WHERE
                        ps IN (183, 184)
                        AND dde <= '{}'
                 """.format(dext), 'nodrizas'))
        for sql, db_sql in sqls:
            for output_query in u.getAll(sql, db_sql):
                if len(output_query) == 1:
                    id_cip_sec, = output_query
                    if id_cip_sec in self.denominador:
                        up = self.denominador[id_cip_sec]
                        numerador[up].add(id_cip_sec)
                        self.pacients[(id_cip_sec, ind, up)] = True
                elif len(output_query) == 2:
                    id_cip_sec, REGIS_data = output_query
                    if id_cip_sec in self.denominador:
                        GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                        if u.yearsBetween(REGIS_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(REGIS_data, GCAS_data_sortida_max) == 0:                        
                            up = self.denominador[id_cip_sec]
                            numerador[up].add(id_cip_sec)
                            self.pacients[(id_cip_sec, ind, up)] = True
                elif len(output_query) == 3:
                    id_cip_sec, snnv_data_alta, snnv_dus = output_query
                    if id_cip_sec in self.denominador:
                        GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                        if u.yearsBetween(snnv_data_alta, GCAS_data_sortida_min) == 0 or u.yearsBetween(snnv_data_alta, GCAS_data_sortida_max) == 0:
                            up = self.denominador[id_cip_sec]
                            numerador[up].add(id_cip_sec)
                            self.pacients[(id_cip_sec, ind, up)] = True
                        elif snnv_dus:
                            if u.yearsBetween(snnv_dus, GCAS_data_sortida_min) == 0 or u.yearsBetween(snnv_dus, GCAS_data_sortida_max) == 0:
                                up = self.denominador[id_cip_sec]
                                numerador[up].add(id_cip_sec)
                                self.pacients[(id_cip_sec, ind, up)] = True

        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_05(self):

        ind = 'GC0005'
        self.definicions_indicadors[ind] = 'Pacients en gesti� de casos PCC o MACA amb PIIC'
        
        resultat = list()
        denominador, numerador = c.defaultdict(set), c.defaultdict(set)

        for id_cip_sec in self.pcc_maca:
            if id_cip_sec in self.denominador:
                GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                if self.pcc_maca[id_cip_sec] <= GCAS_data_sortida_max:
                    up = self.denominador[id_cip_sec]
                    denominador[up].add(id_cip_sec)
                    self.pacients[(id_cip_sec, ind, up)] = False
                    if self.piic[up][id_cip_sec]:
                        for REGIS_data in self.piic[up][id_cip_sec]:
                            if u.yearsBetween(REGIS_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(REGIS_data, GCAS_data_sortida_max) == 0:
                                numerador[up].add(id_cip_sec)
                                self.pacients[(id_cip_sec, ind, up)] = True
            
        for up, pac in denominador.items():
            resultat.append((ind, up, 'DEN', len(pac)))
        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    # def get_06(self):

    #     ind = 'GC0006'
    #     self.definicions_indicadors[ind] = 'Pacients en gesti� de casos MACA amb pla de decisions anticipades'

    #     resultat = list()
    #     denominador, numerador = c.defaultdict(set), c.defaultdict(set)
    #     pda = c.defaultdict(set)
        
    #     sql = """
    #             SELECT
    #                 id_cip_sec,
    #                 mi_data_reg
    #             FROM
    #                 piic
    #             WHERE
    #                 mi_cod_var = 'T4101002'
    #                 AND mi_val_txt <> ''
    #                 AND mi_data_reg BETWEEN '{}' AND '{}'
    #           """.format(dext_menys2anys, dext)
    #     for id_cip_sec, REGIS_data in u.getAll(sql, 'import'):
    #         pda[id_cip_sec].add(REGIS_data)

    #     for id_cip_sec in self.maca:
    #         if id_cip_sec in self.denominador:
    #             GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
    #             if self.maca[id_cip_sec] <= GCAS_data_sortida_max:
    #                 up = self.denominador[id_cip_sec]
    #                 denominador[up].add(id_cip_sec)
    #                 self.pacients[(id_cip_sec, ind, up)] = False
    #                 if up in self.piic:
    #                     if id_cip_sec in self.piic[up]:
    #                         for REGIS_data in self.piic[up][id_cip_sec]:
    #                             if u.yearsBetween(REGIS_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(REGIS_data, GCAS_data_sortida_max) == 0:
    #                                 numerador[up].add(id_cip_sec)
    #                                 self.pacients[(id_cip_sec, ind, up)] = True
    #                                 break

    #     for up, pac in denominador.items():
    #         resultat.append((ind, up, 'DEN', len(pac)))
    #     for up, pac in numerador.items():
    #         resultat.append((ind, up, 'NUM', len(pac)))

    #     u.listToTable(resultat, tb_klx, db)

    def get_07(self):

        ind = 'GC0007'
        self.definicions_indicadors[ind] = "Pacients en gesti� de casos amb registre de l'estat nutricional"

        resultat = list()
        denominador, numerador = c.defaultdict(set), c.defaultdict(set)
        candidats_numerador = c.defaultdict(set)

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    eqa_problemes
                WHERE
                    ps IN (838, 704)
                    AND dde <= '{}'
              """.format(dext)
        sonda = set([id_cip_sec for id_cip_sec, in u.getAll(sql, 'nodrizas')])

        for id_cip_sec, up in self.denominador.items():
            if id_cip_sec not in sonda:
                denominador[up].add(id_cip_sec)
                self.pacients[(id_cip_sec, ind, up)] = False

        sqls = (
                """
                    SELECT
                        id_cip_sec,
                        data_var
                    FROM
                        eqa_variables
                    WHERE
                        agrupador = 1060
                        AND data_var BETWEEN '{}' AND '{}'
                """.format(dext_menys2anys, dext),
                """
                    SELECT
                        id_cip_sec,
                        xml_data_alta
                    FROM
                        import.xml
                    WHERE
                        xml_tipus_orig = 'MNA'
                        AND xml_data_alta BETWEEN '{}' AND '{}'
                """.format(dext_menys2anys, dext)
                )
        for sql in sqls:
            for id_cip_sec, REGIS_data in u.getAll(sql, 'nodrizas'):
                if id_cip_sec in self.denominador:
                    candidats_numerador[id_cip_sec].add(REGIS_data)

        for id_cip_sec in candidats_numerador:
            up = self.denominador[id_cip_sec]
            if id_cip_sec not in sonda:
                GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                for REGIS_data in candidats_numerador[id_cip_sec]:
                    if u.yearsBetween(REGIS_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(REGIS_data, GCAS_data_sortida_max) == 0:
                        numerador[up].add(id_cip_sec)
                        self.pacients[(id_cip_sec, ind, up)] = True
                        break

        for up, pac in denominador.items():
            resultat.append((ind, up, 'DEN', len(pac)))
        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_08(self):

        ind = 'GC0008'
        self.definicions_indicadors[ind] = "Freq�entaci� dels pacients en gesti� de casos"

        resultat = list()
        numerador = c.Counter()

        for id_cip_sec in self.denominador:
            up = self.denominador[id_cip_sec]
            if id_cip_sec in self.visites_professionals_GCAS[up]:
                numerador[up] += len(self.visites_professionals_GCAS[up][id_cip_sec])
                self.pacients[(id_cip_sec, ind, up)] = True

        for up, n_visites in numerador.items():
            resultat.append((ind, up, 'NUM', n_visites))
        
        u.listToTable(resultat, tb_klx, db)
            
    def get_09(self):

        ind = 'GC0009'
        self.definicions_indicadors[ind] = 'Pacients en gesti� de casos inclosos en el programa ATDOM'

        resultat = list()
        numerador = c.defaultdict(set)

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    eqa_problemes
                WHERE
                    ps = 45
                    AND dde <= '{}'
              """.format(dext)
        for id_cip_sec, in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.denominador:
                up = self.denominador[id_cip_sec]
                numerador[up].add(id_cip_sec)
                self.pacients[(id_cip_sec, ind, up)] = True

        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_10(self):

        ind = 'GC0010'
        self.definicions_indicadors[ind] = 'Pacients en gesti� de casos amb c�lcul del grau de complexitat'
        indA = 'GC0010A'
        self.definicions_indicadors[indA] = 'Pacients en gesti� de casos amb complexitat alta'
        indB = 'GC0010B'
        self.definicions_indicadors[indB] = 'Pacients en gesti� de casos amb complexitat mitja'
        indC = 'GC0010C'
        self.definicions_indicadors[indC] = 'Pacients en gesti� de casos amb complexitat baixa'

        resultat = list()
        numerador, numeradorA, numeradorB, numeradorC = c.defaultdict(set), c.defaultdict(set), c.defaultdict(set), c.defaultdict(set)
        candidats_numerador = c.defaultdict(set)

        sql = """
                SELECT
                    id_cip_sec,
                    vu_dat_act,
                    vu_val 
                FROM
                    import.variables
                WHERE
                    vu_dat_act BETWEEN '{}' AND '{}'
                    AND vu_cod_vs = 'VA0304'
              """.format(dext_menys2anys, dext)
        for id_cip_sec, REGIS_data, REGIS_valor in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.denominador:
                GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                if u.yearsBetween(REGIS_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(REGIS_data, GCAS_data_sortida_max) == 0:
                    if id_cip_sec not in candidats_numerador:
                        candidats_numerador[id_cip_sec] = (REGIS_data, REGIS_valor)
                    else:
                        if REGIS_data > candidats_numerador[id_cip_sec][0]:
                            candidats_numerador[id_cip_sec] = (REGIS_data, REGIS_valor)

        for id_cip_sec in candidats_numerador:
            up = self.denominador[id_cip_sec]
            numerador[up].add(id_cip_sec)
            REGIS_valor = candidats_numerador[id_cip_sec][1]
            if REGIS_valor == 3:
                numeradorA[up].add(id_cip_sec)
            elif REGIS_valor == 2:
                numeradorB[up].add(id_cip_sec)
            elif REGIS_valor == 1:
                numeradorC[up].add(id_cip_sec)
            self.pacients[(id_cip_sec, ind, up)] = True
            self.pacients[(id_cip_sec, indA, up)] = 1 if REGIS_valor == 3 else 0
            self.pacients[(id_cip_sec, indB, up)] = 1 if REGIS_valor == 2 else 0
            self.pacients[(id_cip_sec, indC, up)] = 1 if REGIS_valor == 1 else 0
            
        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))
            resultat.append((indA, up, 'DEN', len(pac)))
            resultat.append((indB, up, 'DEN', len(pac)))
            resultat.append((indC, up, 'DEN', len(pac)))

        for up, pac in numeradorA.items():
            resultat.append((indA, up, 'NUM', len(pac)))

        for up, pac in numeradorB.items():
            resultat.append((indB, up, 'NUM', len(pac)))

        for up, pac in numeradorC.items():
            resultat.append((indC, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_11(self):

        ind = 'GC0011'
        self.definicions_indicadors[ind] = 'Pacients en gesti� de casos amb cuidador principal identificat'
        indA = 'GC0011A'
        self.definicions_indicadors[indA] = "Pacients en gesti� de casos amb valoraci� de l'entorn del cuidador"
        
        resultat = list()
        numerador, numeradorA, denominadorA = c.defaultdict(set), c.defaultdict(set), c.defaultdict(set)
        candidats_numerador, candidats_numeradorA, exclusio_den_11a = set(), set(), set()

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    import.cuidador
                WHERE
                    cui_per_ref = 'C'
              """
        for id_cip_sec, in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.denominador:
                candidats_numerador.add(id_cip_sec)
        
        sql = """
                SELECT
                    id_cip_sec
                FROM
                    import.prealt2
                WHERE
                    val_var = 'VI03'
                    and val_hist = '1'
                    and val_val = 'S'
              """
        for id_cip_sec, in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.denominador:
                candidats_numerador.add(id_cip_sec)
                exclusio_den_11a.add(id_cip_sec)

        sql = """
                SELECT
                    id_cip_sec,
                    xml_data_alta
                FROM
                    import.xml_detall
                WHERE
                    xml_origen = 'GABINETS'
                    AND xml_tipus_orig = 'XML0000065'
                    AND xml_data_alta BETWEEN '{}' AND '{}'
                    AND camp_codi IN ('REL_RESULT', 'ACT_RESULT', 'HAB_RESULT', 'CON_RESULT')
                    AND camp_valor != 'Sense Valorar'
            """.format(dext_menys2anys, dext)
        for id_cip_sec, REGIS_data in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in candidats_numerador:
                GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                if u.yearsBetween(REGIS_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(REGIS_data, GCAS_data_sortida_max) == 0:
                    candidats_numeradorA.add(id_cip_sec)

        for id_cip_sec in candidats_numerador:
            up = self.denominador[id_cip_sec]
            numerador[up].add(id_cip_sec)
            if id_cip_sec not in exclusio_den_11a:
                denominadorA[up].add(id_cip_sec)
                self.pacients[(id_cip_sec, indA, up)] = False
            self.pacients[(id_cip_sec, ind, up)] = True

        for id_cip_sec in candidats_numeradorA:
            up = self.denominador[id_cip_sec]
            if id_cip_sec in numerador[up]:
                numeradorA[up].add(id_cip_sec)
                self.pacients[(id_cip_sec, indA, up)] = True

        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))
        for up, pac in denominadorA.items():
            resultat.append((indA, up, 'DEN', len(pac)))

        for up, pac in numeradorA.items():
            resultat.append((indA, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_12(self):

        ind = 'GC0012'
        self.definicions_indicadors[ind] = 'Pacients en gesti� de casos amb valoraci� de la qualitat de vida'
        
        resultat = list()
        denominador, numerador = c.defaultdict(set), c.defaultdict(set)
        deteriorament_cognitiu = set()

        sql = """
                SELECT
                    id_cip_sec,
                    dde
                FROM
                    eqa_problemes
                WHERE
                    ps IN (86, 1056)
              """
        for id_cip_sec, data_deteriorament_cognitiu in u.getAll(sql, "nodrizas"):
            if id_cip_sec in GCAS_dates_sortida:
                _, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                if data_deteriorament_cognitiu <= GCAS_data_sortida_max:
                    deteriorament_cognitiu.add(id_cip_sec)

        for id_cip_sec, up in self.denominador.items():
            if id_cip_sec not in deteriorament_cognitiu:
                denominador[up].add(id_cip_sec)
                self.pacients[(id_cip_sec, ind, up)] = False
                
        sql = """
                SELECT
                    id_cip_sec,
                    data_var
                FROM
                    eqa_variables
                WHERE
                    data_var BETWEEN '{}' AND '{}'
                    AND agrupador = 1026
              """.format(dext_menys2anys, dext)
        for id_cip_sec, REGIS_data in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.denominador:
                up = self.denominador[id_cip_sec]
                if id_cip_sec in denominador[up]:
                    GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                    if u.yearsBetween(REGIS_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(REGIS_data, GCAS_data_sortida_max) == 0:
                        numerador[up].add(id_cip_sec)
                        self.pacients[(id_cip_sec, ind, up)] = True

        for up, pac in denominador.items():
            resultat.append((ind, up, 'DEN', len(pac)))
        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_13(self):

        ind = 'GC0013'
        self.definicions_indicadors[ind] = 'Seguiment de pacients amb MPOC en gesti� de casos'

        resultat = list()
        denominador, numerador = c.defaultdict(set), c.defaultdict(set)
        agrupadors_numerador = c.defaultdict(set)

        sql = """
                SELECT
                    id_cip_sec,
                    dde
                FROM
                    eqa_problemes
                WHERE
                    ps = 62
                    AND dde <= '{}'
              """.format(dext)
        for id_cip_sec, data_mpoc in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.denominador:
                _, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                if data_mpoc <= GCAS_data_sortida_max:
                    up = self.denominador[id_cip_sec]
                    denominador[up].add(id_cip_sec)
                    self.pacients[(id_cip_sec, ind, up)] = False

        sql = """
                SELECT
                    id_cip_sec,
                    data_var,
                    agrupador
                FROM
                    eqa_variables
                WHERE
                    agrupador IN (795, 1028, 1029)
                    AND data_var BETWEEN '{}' AND '{}'
              """.format(dext_menys2anys, dext)
        for id_cip_sec, REGIS_data, agrupador in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.denominador:
                up = self.denominador[id_cip_sec]
                if id_cip_sec in denominador[up]:
                    GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                    if u.yearsBetween(REGIS_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(REGIS_data, GCAS_data_sortida_max) == 0:
                        agrupadors_numerador[id_cip_sec].add(agrupador)

        for id_cip_sec, agrupadors in agrupadors_numerador.items():
            up = self.denominador[id_cip_sec]
            if len(agrupadors) == 3:
                numerador[up].add(id_cip_sec)
                self.pacients[(id_cip_sec, ind, up)] = True

        for up, pac in denominador.items():
            resultat.append((ind, up, 'DEN', len(pac)))
        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_14(self):

        ind = 'GC0014'
        self.definicions_indicadors[ind] = 'Seguiment de pacients amb Insufici�ncia Card��aca en gesti� de casos'

        resultat = list()
        denominador, numerador = c.defaultdict(set), c.defaultdict(set)
        agrupadors_numerador = c.defaultdict(set)

        sql = """
                SELECT
                    id_cip_sec,
                    dde
                FROM
                    eqa_problemes
                WHERE
                    ps = 21
                    AND dde <= '{}'
              """.format(dext)
        for id_cip_sec, data_ic in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.denominador:
                _, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                if data_ic <= GCAS_data_sortida_max:
                    up = self.denominador[id_cip_sec]
                    denominador[up].add(id_cip_sec)
                    self.pacients[(id_cip_sec, ind, up)] = False

        sql = """
                SELECT
                    id_cip_sec,
                    data_var,
                    agrupador
                FROM
                    eqa_variables
                WHERE
                    agrupador IN (427, 1028, 1030)
                    AND data_var BETWEEN '{}' AND '{}'
              """.format(dext_menys2anys, dext)
        for id_cip_sec, REGIS_data, agrupador in u.getAll(sql, 'nodrizas'):
            if id_cip_sec in self.denominador:
                up = self.denominador[id_cip_sec]
                if id_cip_sec in denominador[up]:
                    GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                    if u.yearsBetween(REGIS_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(REGIS_data, GCAS_data_sortida_max) == 0:
                        agrupadors_numerador[id_cip_sec].add(agrupador)

        for id_cip_sec, agrupadors in agrupadors_numerador.items():
            up = self.denominador[id_cip_sec]
            if len(agrupadors) == 3:
                numerador[up].add(id_cip_sec)
                self.pacients[(id_cip_sec, ind, up)] = True

        for up, pac in denominador.items():
            resultat.append((ind, up, 'DEN', len(pac)))
        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_15(self):

        visites_totals, visites_pacients_GCAS, visites_presencials_pacients_GCAS, visites_no_presencials_pacients_GCAS, visites_domiciliaries_pacients_GCAS, visites_telefoniques_pacients_GCAS = c.Counter(), c.Counter(), c.Counter(), c.Counter(), c.Counter(), c.Counter()

        ind = 'GC0015'
        self.definicions_indicadors[ind] = 'Visites totals dels pacients en gesti� de casos'
        indA = 'GC0015A'
        self.definicions_indicadors[indA] = 'Percentatge de visites presencials dels pacients en gesti� de casos'
        indB = 'GC0015B'
        self.definicions_indicadors[indB] = 'Percentatge de visites no presencials dels pacients en gesti� de casos'
        indC = 'GC0015C'
        self.definicions_indicadors[indC] = 'Percentatge de visites domicili�ries dels pacients en gesti� de casos'
        indD = 'GC0015D'
        self.definicions_indicadors[indD] = 'Percentatge de visites telef�niques dels pacients en gesti� de casos'

        resultat = list()
        numerador = c.defaultdict(set)

        for up in self.visites:
            for id_cip_sec in self.visites[up]:
                visites_totals[up] += len(self.visites[up][id_cip_sec])
                self.pacients[(id_cip_sec, ind, up)] = False
                if id_cip_sec in self.denominador:
                    for VISI_data, VISI_tipus in self.visites[up][id_cip_sec]:
                        for data_alta, data_sortida in self.GCAS_dates[id_cip_sec]:
                            if data_alta <= VISI_data <= data_sortida:
                                visites_pacients_GCAS[up] += 1
                                self.pacients[(id_cip_sec, ind, up)] = True
                                self.pacients[(id_cip_sec, indA, up)] = False
                                self.pacients[(id_cip_sec, indB, up)] = False
                                self.pacients[(id_cip_sec, indC, up)] = False
                                self.pacients[(id_cip_sec, indD, up)] = False
                                if VISI_tipus in ('9C', '9R'):
                                    visites_presencials_pacients_GCAS[up] += 1
                                    self.pacients[(id_cip_sec, indA, up)] = True
                                elif VISI_tipus == '9E':
                                    visites_no_presencials_pacients_GCAS[up] += 1
                                    self.pacients[(id_cip_sec, indB, up)] = True
                                elif VISI_tipus == '9D':
                                    visites_domiciliaries_pacients_GCAS[up] += 1
                                    self.pacients[(id_cip_sec, indC, up)] = True
                                elif VISI_tipus == '9T':
                                    visites_telefoniques_pacients_GCAS[up] += 1
                                    self.pacients[(id_cip_sec, indD, up)] = True
                                break

        for up, n_visites in visites_totals.items():
            resultat.append((ind, up, 'DEN', n_visites))
        for up, n_visites in visites_pacients_GCAS.items():
            resultat.append((ind, up, 'NUM', n_visites))
            resultat.append((indA, up, 'DEN', n_visites))
            resultat.append((indB, up, 'DEN', n_visites))
            resultat.append((indC, up, 'DEN', n_visites))
            resultat.append((indD, up, 'DEN', n_visites))
        for up, n_visites in visites_presencials_pacients_GCAS.items():
            resultat.append((indA, up, 'NUM', n_visites))
        for up, n_visites in visites_no_presencials_pacients_GCAS.items():
            resultat.append((indB, up, 'NUM', n_visites))
        for up, n_visites in visites_domiciliaries_pacients_GCAS.items():
            resultat.append((indC, up, 'NUM', n_visites))
        for up, n_visites in visites_telefoniques_pacients_GCAS.items():
            resultat.append((indD, up, 'NUM', n_visites))

        u.listToTable(resultat, tb_klx, db)
        
    def get_16(self):

        ind = 'GC0016'
        self.definicions_indicadors[ind] = 'Percentatge de pacients en gesti� de casos amb derivaci�'

        resultat = list()
        numerador = c.defaultdict(set)

        for id_cip_sec in self.denominador:
            up = self.denominador[id_cip_sec]
            num = 0
            if id_cip_sec in self.activitats[1031][up]:
                GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                for ACTIV_data in self.activitats[1031][up][id_cip_sec]:
                    if u.yearsBetween(ACTIV_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(ACTIV_data, GCAS_data_sortida_max) == 0:
                        num = 1
                        break
            if num:
                numerador[up].add(id_cip_sec)
                self.pacients[(id_cip_sec, ind, up)] = True

        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_17(self):

        ind = 'GC0017'
        self.definicions_indicadors[ind] = 'Percentatge de pacients amb derivaci� que no estan en gesti� de casos'

        resultat = list()
        denominador, numerador = c.defaultdict(set), c.defaultdict(set)

        for id_cip_sec in poblacio:
            if id_cip_sec not in self.denominador:
                up = poblacio[id_cip_sec]["up"]
                denominador[up].add(id_cip_sec)
                self.pacients[(id_cip_sec, ind, up)] = False

        for up in denominador:
            for id_cip_sec in denominador[up]:
                if id_cip_sec in self.activitats_professionals_GCAS[1031][up] and max(self.activitats_professionals_GCAS[1031][up][id_cip_sec]) > dext_menys1any:
                    numerador[up].add(id_cip_sec)
                    self.pacients[(id_cip_sec, ind, up)] = True

        for up, pac in denominador.items():
            resultat.append((ind, up, 'DEN', len(pac)))
        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)


    def get_18(self):

        ind = 'GC0018'
        self.definicions_indicadors[ind] = 'Percentatge de pacients en gesti� de casos amb coordinaci�'

        resultat = list()
        numerador = c.defaultdict(set)

        for id_cip_sec in self.denominador:
            up = self.denominador[id_cip_sec]
            num = 0
            if id_cip_sec in self.activitats[1034][up]:
                GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                for ACTIV_data in self.activitats[1034][up][id_cip_sec]:
                    if u.yearsBetween(ACTIV_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(ACTIV_data, GCAS_data_sortida_max) == 0:
                        num = 1
                        break
            if num:
                numerador[up].add(id_cip_sec)
                self.pacients[(id_cip_sec, ind, up)] = True

        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)

    def get_19(self):

        ind = 'GC0019'
        self.definicions_indicadors[ind] = 'Percentatge de pacients amb coordinaci� que no estan en gesti� de casos'

        resultat = list()
        denominador, numerador = c.defaultdict(set), c.defaultdict(set)

        for id_cip_sec in poblacio:
            if id_cip_sec not in self.denominador:
                up = poblacio[id_cip_sec]["up"]
                self.pacients[(id_cip_sec, ind, up)] = False
                denominador[up].add(id_cip_sec)

        for up in denominador:
            for id_cip_sec in denominador[up]:
                num = 1 if id_cip_sec in self.activitats_professionals_GCAS[1034][up] and max(self.activitats_professionals_GCAS[1034][up][id_cip_sec]) > dext_menys1any else 0
                self.pacients[(id_cip_sec, ind, up)] = bool(num)
                if num:
                    numerador[up].add(id_cip_sec)

        for up, pac in denominador.items():
            resultat.append((ind, up, 'DEN', len(pac)))
        for up, pac in numerador.items():
            resultat.append((ind, up, 'NUM', len(pac)))

        u.listToTable(resultat, tb_klx, db)


    def get_20_21_22_23(self):

        ind_20 = 'GC0020'
        self.definicions_indicadors[ind_20] = "Pacients en gesti� de casos PCC amb ingr�s a l'hospital"
        ind_21 = 'GC0021'
        self.definicions_indicadors[ind_21] = "Pacients en gesti� de casos MACA amb ingr�s a l'hospital"
        ind_22 = 'GC0022'
        self.definicions_indicadors[ind_22] = "Pacients en gesti� de casos PCC que reingressa a l'hospital"
        ind_23 = 'GC0023'
        self.definicions_indicadors[ind_23] = "Pacients en gesti� de casos MACA que reingressa a l'hospital"

        resultat = list()
        inclosos = c.Counter()
        denominador_20, denominador_21, denominador_22, denominador_23 = c.defaultdict(set), c.defaultdict(set), c.defaultdict(set), c.defaultdict(set)
        numerador_20, numerador_21, numerador_22, numerador_23 = c.defaultdict(set), c.defaultdict(set), c.defaultdict(set), c.defaultdict(set)
    
        for id_cip_sec in self.pcc:
            if id_cip_sec in self.denominador:
                GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                if self.pcc[id_cip_sec] <= GCAS_data_sortida_max:
                    up = self.denominador[id_cip_sec]
                    self.pacients[(id_cip_sec, ind_20, up)] = False
                    denominador_20[up].add(id_cip_sec)
                    if id_cip_sec in self.ingressos:
                        for ingres_data in self.ingressos[id_cip_sec]:
                            if u.yearsBetween(ingres_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(ingres_data, GCAS_data_sortida_max) == 0:
                                self.pacients[(id_cip_sec, ind_20, up)] = True
                                numerador_20[up].add(id_cip_sec)
                    self.pacients[(id_cip_sec, ind_22, up)] = False
                    denominador_22[up].add(id_cip_sec)
                    if id_cip_sec in self.reingressos:
                        for ingres_data in self.reingressos[id_cip_sec]:
                            if u.yearsBetween(ingres_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(ingres_data, GCAS_data_sortida_max) == 0:
                                self.pacients[(id_cip_sec, ind_22, up)] = True
                                numerador_22[up].add(id_cip_sec)

        for id_cip_sec in self.maca:
            if id_cip_sec in self.denominador:
                GCAS_data_sortida_min, GCAS_data_sortida_max = GCAS_dates_sortida[id_cip_sec]
                if self.maca[id_cip_sec] <= GCAS_data_sortida_max:
                    up = self.denominador[id_cip_sec]
                    self.pacients[(id_cip_sec, ind_21, up)] = False
                    denominador_21[up].add(id_cip_sec)
                    if id_cip_sec in self.ingressos:
                        for ingres_data in self.ingressos[id_cip_sec]:
                            if u.yearsBetween(ingres_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(ingres_data, GCAS_data_sortida_max) == 0:
                                self.pacients[(id_cip_sec, ind_21, up)] = True
                                numerador_21[up].add(id_cip_sec)
                    self.pacients[(id_cip_sec, ind_23, up)] = False
                    denominador_23[up].add(id_cip_sec)
                    if id_cip_sec in self.reingressos:
                        for ingres_data in self.reingressos[id_cip_sec]:
                            if u.yearsBetween(ingres_data, GCAS_data_sortida_min) == 0 or u.yearsBetween(ingres_data, GCAS_data_sortida_max) == 0:
                                self.pacients[(id_cip_sec, ind_23, up)] = True
                                numerador_23[up].add(id_cip_sec)

        for up, pac in denominador_20.items():
            resultat.append((ind_20, up, 'DEN', len(pac)))
        for up, pac in numerador_20.items():
            resultat.append((ind_20, up, 'NUM', len(pac)))

        for up, pac in denominador_21.items():
            resultat.append((ind_21, up, 'DEN', len(pac)))
        for up, pac in numerador_21.items():
            resultat.append((ind_21, up, 'NUM', len(pac)))

        for up, pac in denominador_22.items():
            resultat.append((ind_22, up, 'DEN', len(pac)))
        for up, pac in numerador_22.items():
            resultat.append((ind_22, up, 'NUM', len(pac)))

        for up, pac in denominador_23.items():
            resultat.append((ind_23, up, 'DEN', len(pac)))
        for up, pac in numerador_23.items():
            resultat.append((ind_23, up, 'NUM', len(pac)))         

        u.listToTable(resultat, tb_klx, db)

    def export_mst(self):
        """Exportaci� de les dades master."""

        u.listToTable([k + (1 * v,) for (k, v) in self.pacients.items()], tb_pac, db)

    def export_klx(self):
        """Exportaci� de les dades a Longview."""

        error = []
        file = 'GESTIO_CASOS'

        sql = """
                SELECT
                    indicador,
                    'Aperiodo',
                    ics_codi,
                    tipus,
                    'NOCAT',
                    'NOIMP',
                    'DIM6SET',
                    'N',
                    n
                FROM
                    {0}.{1} a
                INNER JOIN nodrizas.cat_centres b ON
                    up = scs_codi
              """.format(db, tb_klx)
        error.append(u.exportKhalix(sql, file))

    def get_u11(self):

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    {}
                WHERE
                    compleix = 0
              """.format(tb_pac)
        pacients = set([id_cip_sec for id_cip_sec, in u.getAll(sql, db)])

        sql = """
                SELECT
                    id_cip_sec,
                    hash_d,
                    codi_sector
                FROM
                    u11
              """
        self.u11 = {row[0]: row[1:] for row in u.getAll(sql, 'import') if row[0] in pacients}


    def upload_tables_ecap(self):
        """Exportaci� de les dades que s'enviar�n a eCAP."""

        ecap_resultat = {}
        indicadors = set()
        ups = set()

        for tb in tb_ecap:
            orig = tb.replace('gc', 'alertes')
            u.createTable(tb, 'like {}'.format(orig), db, rm=True)
        u.execute("ALTER TABLE {} ADD COLUMN mmin INT DEFAULT 0, ADD COLUMN mmax INT DEFAULT 0".format(tb_ecap[0]), db)

        sql = """
                SELECT
                    indicador,
                    up,
                    tipus,
                    n
                FROM
                    {}
              """.format(tb_klx)
        for ind, up, analisis, n in u.getAll(sql, db):
            id = (up, 'sense', 's', ind)
            if id not in ecap_resultat:
                ecap_resultat[id] = [0, 0]
            ecap_resultat[id][0 if analisis == 'NUM' else 1] = n
            ups.add(up)
            indicadors.add(ind)

        for up, ind in it.product(ups, indicadors):
            id = (up, 'sense', 's', ind)
            if id not in ecap_resultat:
                ecap_resultat[id] = [0, 0]

        u.listToTable([k + (v[0], v[1], (float(v[0]) / v[1]) if v[1] else 0) + (self.METES[k[3]][0],) + (self.METES[k[3]][2],)  for (k, v) in ecap_resultat.items() if k[3] in self.METES], tb_ecap[0], db)
        
        sql = """
                SELECT
                    id_cip_sec,
                    indicador,
                    up
                FROM
                    {}
                WHERE
                    compleix = 0
                    AND indicador in {}
              """.format(tb_pac, indicadors_amb_llistats_ecap)
        ecap_pacient = [(id_cip, up, 'sense', None, None, ind, 0) + self.u11[id_cip] for (id_cip, ind, up) in u.getAll(sql, db)]
        u.listToTable(ecap_pacient, tb_ecap[1], db)

        pare = 'GC'
        link = 'http://10.80.217.201/sisap-umi/indicador/codi/{}'

        u.listToTable([(pare, 'Indicadors de gesti� de casos', 8, 'ALTRES')], tb_ecap[3], db)

        ecap_cataleg = [(ind, self.definicions_indicadors.get(ind), ordre, pare, 1 if ind in indicadors_amb_llistats_ecap else 0, 1 if ind in indicadors_inversos else 0, self.METES[ind][0], self.METES[ind][1], self.METES[ind][2], 1, link.format(ind), 'PCT') for ordre, ind in enumerate(sorted(indicadors), start=1)]
        u.listToTable(ecap_cataleg, tb_ecap[2], db)


def sub_get_subtaules_visites(table):
    """Obt� els noms de les taules de visites dels �ltims 13 mesos."""

    sql = "select visi_data_visita from {} limit 1".format(table)
    visi_data_visita, = u.getOne(sql, "import")
    if visi_data_visita >= dext_menys13mesos:
        return table

def get_subtaules_activitats():
    """Obt� els noms de les taules de visites dels �ltims 26 mesos."""
    
    subtaules_activitats, subtaules_dates_activitats = list(), list()

    for taula in u.getSubTables("activitats"):
        sql = "select au_dat_act from {} LIMIT 1".format(taula) 
        data_random, = u.getOne(sql, "import")
        if data_random > dext_menys26mesos:
            subtaules_dates_activitats.append((data_random, taula))
    subtaules_dates_activitats.sort()
    for _, subtaula_activitats in subtaules_dates_activitats:
        subtaules_activitats.append(subtaula_activitats)

    return subtaules_activitats

def sub_get_activitats(input):
    """Obt� les dades d'una subtaula d'activitats d'import que ser�n necessaries pel c�lcul dels indicadors de Gesti� de Casos."""

    sql, codis_atc, GCAS_dates_sortida, id_usuaris_professionals_GCAS = input

    sub_activitats, sub_activitats_professionals_GCAS = list(), list()

    for id_cip_sec, ACTIV_atc, ACTIV_data, ACTIV_professional in u.getAll(sql, "import"):
        if id_cip_sec in poblacio:
            up = poblacio[id_cip_sec]["up"]
            agrupador = codis_atc[ACTIV_atc]
            sub_activitats.append((agrupador, up, id_cip_sec, ACTIV_data))
            if ACTIV_professional in id_usuaris_professionals_GCAS:
                sub_activitats_professionals_GCAS.append((agrupador, up, id_cip_sec, ACTIV_data))


    return sub_activitats, sub_activitats_professionals_GCAS

if __name__ == '__main__':
    GestioCasos()