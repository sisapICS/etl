# coding: iso-8859-1

import collections as c
import os

import sisapUtils as u
import sisaptools as t

import datetime as d
from datetime import datetime
from dateutil.relativedelta import relativedelta

"""
Time Execution:
(datetime.date(2021, 1, 1), datetime.date(2021, 3, 31))
--------------------------------------------- get_centres
Time execution CENTRES: time 0:00:00.033000 / nrow 304
------------------------------------------------------ get_atc
Time execution CENTRES: time 0:00:00.769000 / nrow 6878
--------------------------------------------- get_cim10mcdesc
Time execution CENTRES: time 0:00:02.981000 / nrow 109702
--------------------------------------------- get_poblacio
Time execution POBLACIO: time 0:03:30.939000 / nrow 5801451
--------------------------------------------- get_problemes
Time execution PROBLEMES: time 0:22:03.069000 / nrow 132984
--------------------------------------------- get_ram
Time execution RAM: time 0:00:09.551000 / nrow 204164
--------------------------------------------- get_hash (u11 - import)
Time execution HASH: time 0:01:48.363000 / nrow 5801451
--------------------------------------------- get_prescripcio
Time execution PRESCRIPCIO: time 0:51:39.131000 / nrow 503319
--------------------------------------------- get_ppftb023
Time execution PRESCRIPCIO DX: time 2:18:37.847000 / nrow 305628
----------------------------------------------- get_upload
Time execution export_data: 0:00:00.665000
---------------------------------------------- TIME EXECUTION
START: 2021-07-12 17:18:52.939000
END: 2021-07-12 20:57:24.166000
DELTA: 3:38:31.227000 

"""
 
# TEST_UP='00276' # ABS BADALONA-6 (Llefi�)
# TEST_IDCIPSEC = 9502868

TODAY = d.datetime.now().date()
DIAEXEC = d.date(2022, 9, 30)
#DIAEXEC = d.datetime.now().date()
DATE1 = DIAEXEC # - relativedelta(days=18)
DATE0 = DATE1 - relativedelta(months=3) # + relativedelta(days=2)
print(DATE0, DATE1)


cod_cim10_pare = {'A': "Determinades malalties infeccioses i parasitaries (A00-B99)",
                  'B': "Determinades malalties infeccioses i parasitaries (A00-B99)",
                  'C': "Neoplasies (C00-D49)",
                  'D': "Neoplasies (C00-D49)",
                  #'D': "Malalties de la sang i els �rgans hematopo�tics i determinats trastorns que afecten el mecanisme immunitari (D50-D89)"
                  'E': "Malalties endocrines, nutricionals i metaboliques (E00-E89)",
                  'F': "Trastorns mentals, del comportament i del neurodesenvolupament (F01-F99)",
                  'G': "Malalties del sistema nervios (G00-G99)",
                  'H': "Malalties ull i els annexos oculars (H00-H59) Malalties orella i apofisi mastoide (H60-H95)",
                  'I': "Malalties aparell circulatori (I00-I99)",
                  'J': "Malalties aparell respiratori (J00-J99)",
                  'K': "Malalties aparell digestiu (K00-K95)",
                  'L': "Malalties de la pell i el teixit subcutani (L00-L99)",
                  'M': "Malalties del sistema musculoesqueletic i el teixit connectiu (M00-M99)",
                  'N': "Malalties aparell genitourinari (N00-N99)",
                  'O': "Embaras, part i puerperi (O00-O9A)",
                  'P': "Determinades afeccions originades en el periode perinatal (P00-P96)",
                  'Q': "Malformacions congenites, deformitats congenites i anomalies cromosomiques congenites (Q00-Q99)",
                  'R': "Signes, simptomes i resultats anomals analisis no classificats a cap altre lloc (R00-R99)",
                  'S': "Lesions, intoxicacions i altres consequencies de causes externes (S00-T88)",
                  'T': "Lesions, intoxicacions i altres consequencies de causes externes (S00-T88)",
                  'V': "Causes externes de morbiditat (V00-Y99)",
                  'W': "Causes externes de morbiditat (V00-Y99)",
                  'Y': "Causes externes de morbiditat (V00-Y99)",
                  'Z': "Factors que influeixen en estat de salut i contacte amb els serveis sanitaris (Z00-Z99)",
                  'U': "Codis per a proposits especials (U00-U85)"}

class ucem(object):

    def __init__(self):
        '''.'''

        ts = datetime.now()
        self.get_centres()
        print('Time execution CENTRES: time {} / nrow {}'.format(datetime.now() - ts, len(self.centres)))

        ts = datetime.now()
        self.get_atc()
        print('Time execution CENTRES: time {} / nrow {}'.format(datetime.now() - ts, len(self.atc)))

        ts = datetime.now()
        self.get_cim10mcdesc()
        print('Time execution CENTRES: time {} / nrow {}'.format(datetime.now() - ts, len(self.cim10mc)))

        ts = datetime.now()
        self.get_poblacio()
        print('Time execution POBLACIO: time {} / nrow {}'.format(datetime.now() - ts, len(self.poblacio)))

        ts = datetime.now()
        self.get_problemes()
        print('Time execution PROBLEMES: time {} / nrow {}'.format(datetime.now() - ts, len(self.mpoc)))

        ts = datetime.now()
        self.get_ram()
        print('Time execution RAM: time {} / nrow {}'.format(datetime.now() - ts, len(self.ram)))

        ts = datetime.now()
        self.get_hash()
        print('Time execution HASH: time {} / nrow {}'.format(datetime.now() - ts, len(self.hash)))

        ts = datetime.now()
        self.get_presc()
        print('Time execution PRESCRIPCIO: time {} / nrow {}'.format(datetime.now() - ts, len(self.presc)))

        ts = datetime.now()
        self.get_dexpresc()
        print('Time execution PRESCRIPCIO DX: time {} / nrow {}'.format(datetime.now() - ts, len(self.prescdx)))

        ts = datetime.now()
        self.get_upload()
        print('Time execution get_upload: {} / nrow {}'.format(datetime.now() - ts, len(self.upload)))

        ts = datetime.now()
        self.export_data()
        print('Time execution export_data: {}'.format(datetime.now() - ts))

        ts = datetime.now()
        self.upload_data()
        print('Time execution upload_data: {}'.format(datetime.now() - ts))


    def get_codagr(self, cod):
        """ . """
        codagr = ''
        if cod in ('C01-J02.0', 'C01-J02.8', 'C01-J02.9',
                   'C01-J03', 'C01-J03.0', 'C01-J03.00',
                   'C01-J03.80', 'C01-J03.9', 'C01-J03.90',
                   'J02', 'J02.0', 'J02.8', 'J02.9',
                   'J03', 'J03.0', 'J03.8', 'J03.9'):
            codagr = 'Amigdalitis Aguda'
        if cod in ('C01-H65.199','C01-H65.90','C01-H65.92',
                  'C01-H66.90',
                  'H65','H65.0','H65.1',
                  'H66.9'):
            codagr = 'Otitis'
        if cod in ('C01-J00',
               'J00'):
            codagr = 'CVA'
        if cod in ('C01-J10.1','C01-J10.89',
               'C01-J11.1','C01-J11.2','C01-J11.89',
               'J10','J10.1','J10.8',
               'J11','J11.1','J11.8'):
            codagr = 'Grip'
        if cod in ('C01-N30', 'C01-N30.0', 'C01-N30.00', 'C01-N30.9', 'C01-N30.90',
                   'C01-N39.0',
                   'N30', 'N30.0'):
            codagr = 'ITU'
        if cod in ('C01-J20', 'C01-J20.3', 'C01-J20.4', 'C01-J20.5',
               'C01-J20.6', 'C01-J20.7', 'C01-J20.8', 'C01-J20.9',
               'J20','J20.9', 'J21.1'):
            codagr = 'Bronquitis Aguda'
        if cod in ('C01-J13',
                   'C01-J14',
                   'C01-J15.9',
                   'C01-J18.1', 'C01-J18.8', 'C01-J18.9',
                   'J13',
                   'J14',
                   'J15.9',
                   'J18.1', 'J18.8', 'J18.9',
                   'J84.9'):
            codagr = 'Pneumonia'
        if cod in ('C01-K02',
                   'C01-K02.3',
                   'C01-K02.5', 'C01-K02.51', 'C01-K02.52', 'C01-K02.53',
                   'C01-K02.6', 'C01-K02.61', 'C01-K02.62', 'C01-K02.63',
                   'C01-K02.7',
                   'C01-K02.9',
                   'K02',
                   'K02.0',
                   'K02.1',
                   'K02.2', 'K02.51', 'K02.3',
                   'K02.52', 'K02.53',
                   'K02.61', 'K02.62', 'K02.63',
                   'K02.9'):
            codagr = 'Caries'
        if cod in ('K04', 'K04.0', 'K04.1', 'K04.2', 'K04.3', 'K04.4', 'K04.5', 'K04.9',
                   'C01-K04',
                   'C01-K04.0','C01-K04.01','C01-K04.02',
                   'C01-K04.1',
                   'C01-K04.2',
                   'C01-K04.3',
                   'C01-K04.4',
                   'C01-K04.5',
                   'C01-K04.9','C01-K04.90','C01-K04.99'):
            codagr = 'Pulpitis'
        return codagr

    def get_centres(self):
        '''FLT: Centres ICS (ep='0208')'''
        print("--------------------------------------------- get_centres")
        self.centres = {}
        db = "nodrizas"
        tb = "cat_centres"
        flt = ""
        sql = "select scs_codi, ics_desc, sap_codi, sap_desc, amb_codi, amb_desc from {}.{} where ep='0208'".format(db, tb)
        for up, desc, sap_codi, sap_desc, amb_codi, amb_desc in u.getAll(sql, db):
            self.centres[up] = {'desc': desc, 'sap_codi': sap_codi, 'sap_desc': sap_desc, 'amb_codi': amb_codi, 'amb_desc': amb_desc}
        print('Ejemplo CENTRES: ', next(iter( self.centres.items() )) )
        #print("Validation up TEST:", self.centres[TEST_UP])
        #print(self.centres)

    def get_atc(self):
        '''.'''
        print("------------------------------------------------------ get_atc")
        self.atc = {}
        db = "import"
        tb = "cat_cpftb010"
        flt = ""
        sql = "select atc_codi, atc_desc from {}.{}".format(db, tb)
        for atc, atc_desc in u.getAll(sql, db):
            self.atc[atc] = {'atc_desc': atc_desc}
        print('N de registros ATC: {}', len(self.atc))
        print('Ejemplo ATC: ', next(iter( self.atc.items() )) )

    def get_cim10mcdesc(self):
        '''.'''
        print("--------------------------------------------- get_cim10mcdesc")
        self.cim10mc = {}
        db = "import"
        tb = "cat_prstb001"
        flt = ""
        sql = "select ps_cod, ps_des from {}.{} where ps_cod_o='C'".format(db, tb)
        for cod, cod_desc in u.getAll(sql, db):
            if cod[0:4] == 'C01-':
                cod_pare = cod[4]
                cod_pare_desc = cod_cim10_pare.get(cod_pare)
                cod_agr_desc = self.get_codagr(cod)
            else:
                cod_pare = cod[0]
                cod_pare_desc = cod_cim10_pare.get(cod_pare)
                cod_agr_desc = self.get_codagr(cod)                
            self.cim10mc[cod] = {'cod_desc': cod_desc,
                                 'cod_agr_desc': cod_agr_desc,
                                 'cod_pare': cod_pare,
                                 'cod_pare_desc': cod_pare_desc}
        print('N de registros CIM10MC: {}', len(self.cim10mc))
        print('Ejemplo CIM10MC: ', next(iter( self.cim10mc.items() )) )

    def get_poblacio(self):
        """FLT: Poblacio ICS (ep='0208')"""
        print("--------------------------------------------- get_poblacio")
        self.poblacio = {}
        sql = "select id_cip_sec, up, edat, sexe, ates, institucionalitzat, maca, pcc \
               from assignada_tot where ep='0208'"
        for id_cip_sec, up, edat, sexe, ates, ins, maca, pcc in u.getAll(sql, "nodrizas"):
            if 0 <= edat <= 1:
                edatc = 'Edat: 0-1'
            elif 2 <= edat <= 7:
                edatc = 'Edat: 2-7'
            elif 8 <= edat <= 14:
                edatc = 'Edat: 8-14'
            elif 15 <= edat <= 44:
                edatc = 'Edat: 15-44'
            elif 45 <= edat <= 64:
                edatc = 'Edat: 45-64'
            elif 65 <= edat <= 74:
                edatc = 'Edat: 65-74'
            elif 75 <= edat <= 84:
                edatc = 'Edat: 75-84'
            elif 85 <= edat:
                edatc = 'Edat: >=85'
            else:
                None
                print(id_cip_sec, edat)
            self.poblacio[id_cip_sec] = {'up': up, 'edat': edat, 'edatc': edatc, 'sexe': sexe,
                                         'ates': ates, 'ins': ins, 'maca': maca, 'pcc': pcc}
        print('Ejemplo POBLACIO: ', next(iter( self.poblacio.items() )) )

    def get_problemes(self):
        '''.'''
        print("--------------------------------------------- get_problemes")
        self.problemes = {}
        self.mpoc = {}
        self.allergiaps = {}
        db = 'import'
        tb = 'problemes'
        sql = "select id_cip_sec, pr_cod_ps, pr_dde, pr_dba from {}.{} \
               where pr_cod_ps in ('C01-J44.0','C01-J44.1','C01-J44.9', \
               'J44.0','J44.1','J44.9', \
               'C01-Z88.0','C01-Z88.1') \
               and pr_dde < {}".format(db, tb, DATE1)
        for id_cip_sec, cod, dat, datfi in u.getAll(sql, db):
            if id_cip_sec in self.poblacio:
                if cod in ('C01-J44.0','C01-J44.1','C01-J44.9','J44.0','J44.1','J44.9'):
                    self.mpoc[id_cip_sec] = True
                if cod in ('C01-Z88.0','C01-Z88.1'):
                    self.allergiaps[id_cip_sec] = True
        print('N de registros MPOC: {}', len(self.mpoc))
        print('N de registros ALLERGIA: {}', len(self.allergiaps))
        #print('Ejemplo PROBLEMES: ', next(iter( self.problemes.items() )) )

    def get_ram(self):
        '''.'''
        print("--------------------------------------------- get_ram")
        self.ram = {}
        db = 'nodrizas'
        tb = 'eqa_ram'
        sql = "select id_cip_sec, gravetat from {}.{} where agr=505".format(db, tb)
        for id_cip_sec, gra in u.getAll(sql, db):
            if id_cip_sec in self.poblacio:
                self.ram[id_cip_sec] = {'gra': gra}
        print('N de registros RAM: ', len(self.ram))
        print('Ejemplo RAM: ', next(iter( self.ram.items() )) )

    def get_hash(self):
        '''.'''
        print("--------------------------------------------- get_hash (u11 - import)")
        self.hash = {}
        self.idhash = {}
        self.hashid = {}
        db = 'import'
        tb = 'u11'
        sql = 'select id_cip_sec, hash_d, codi_sector from {}.{}'.format(db, tb)
        for id_cip_sec, hash_d, sector in u.getAll(sql, db):
            if id_cip_sec in self.poblacio:
                self.hash[hash_d] = True
                self.idhash[id_cip_sec] = {'hash': hash_d, 'sector': sector}
                self.hashid[hash_d] = {'id_cip_sec': id_cip_sec, 'sector': sector}
        print("Ejemplo HASH: ", next(iter( self.hash.items() )) )
        print("N de registros HASH (u11): ", len(self.hash))

    def get_presc(self):
        '''.'''
        print("--------------------------------------------- get_prescripcio")
        self.presc = {}
        db = 'redics'
        tb = 'ppftb016'
        sql = "select ppfmc_pmc_usuari_cip, codi_sector, ppfmc_pmc_codi, \
                      ppfmc_num_prod, pf_desc, pf_cod_atc, \
                      ppfmc_pmc_data_ini, ppfmc_data_fi \
               from {} \
                where substr(pf_cod_atc,1,3) in ('J01') \
                    and ppfmc_pmc_data_ini between date '{}' \
                        and date '{}'".format(tb,
                                              DATE0.strftime('%Y-%m-%d'),
                                              DATE1.strftime('%Y-%m-%d'))

        print(sql)
        for hash, sector, pmc, nprod, pf_desc, atc, dat, datfi in u.getAll(sql, db):
            self.presc[(sector, pmc, nprod)] = {'hash': hash, 'pf_desc': pf_desc, 'atc': atc, 'dat': dat.date(), 'datfi': datfi.date()}
        print('N de registros PRESCRIPCIO: ', len(self.presc))
        print('Ejemplo PRESCRIPCIO: ', next(iter( self.presc.items() )) )

    def get_dexpresc(self):
        '''.'''
        print("--------------------------------------------- get_ppftb023")
        self.prescdx = {}
        db = 'redics'
        tb = 'ppftb023'
        sql = "select codi_sector, dgppf_pmc_codi, dgppf_num_prod, dgppf_ps_cod, dgppf_data_alta from {}".format(tb)
        for sector, pmc, nprod, cod, dat in u.getAll(sql, db):
            if (sector, pmc, nprod) in self.presc:
                self.prescdx[(sector, pmc, nprod)] = {'cod': cod, 'dat': dat}
        #print('N de registros PRESCRIPCIO DX: ', len(self.prescdx))
        #print('Ejemplo PRESCRIPCIO DX: ', next(iter( self.prescdx.items() )) )

    def get_upload(self):
        '''.'''
        print("----------------------------------------------- get_upload")
        self.upload = []
        for (sector, pmc, nprod) in self.presc:
                hash = self.presc[(sector, pmc, nprod)]['hash']
                if hash in self.hash:
                    pf_desc = self.presc[(sector, pmc, nprod)]['pf_desc']
                    atc = self.presc[(sector, pmc, nprod)]['atc']
                    atc_desc = self.atc[atc]['atc_desc']
                    presdx =  1 if (sector, pmc, nprod) in self.prescdx else 0
                    cod = self.prescdx[(sector, pmc, nprod)]['cod'] if (sector, pmc, nprod) in self.prescdx else None
                    cod_desc = self.cim10mc[cod]['cod_desc'] if cod in self.cim10mc else None
                    cod_agr_desc = self.cim10mc[cod]['cod_agr_desc'] if cod in self.cim10mc else None
                    cod_pare_desc = self.cim10mc[cod]['cod_pare_desc'] if cod in self.cim10mc else None                    
                    dat = self.presc[(sector, pmc, nprod)]['dat']
                    datfi = self.presc[(sector, pmc, nprod)]['datfi']
                    id_cip_sec = self.hashid[hash]['id_cip_sec']
                    up = self.poblacio[id_cip_sec]['up']
                    up_desc = self.centres[up]['desc']
                    sap = self.centres[up]['sap_codi']
                    sap_desc = self.centres[up]['sap_desc']
                    amb = self.centres[up]['amb_codi']
                    amb_desc = self.centres[up]['amb_desc']
                    edat = self.poblacio[id_cip_sec]['edat']
                    edatc = self.poblacio[id_cip_sec]['edatc']
                    sexe = self.poblacio[id_cip_sec]['sexe']
                    ins = self.poblacio[id_cip_sec]['ins']
                    maca = self.poblacio[id_cip_sec]['maca']
                    pcc = self.poblacio[id_cip_sec]['pcc']
                    mpoc =  1 if id_cip_sec in self.mpoc else 0
                    allergiaram = 1 if id_cip_sec in self.ram else 0
                    allergiaps = 1 if id_cip_sec in self.allergiaps else 0
                    allergia = 1 if allergiaram==1 or allergiaps==1 else 0 
                    self.upload.append((id_cip_sec,
                                        hash, 
                                        sector,
                                        up, up_desc,
                                        sap, sap_desc,
                                        amb, amb_desc,
                                        edatc,
                                        sexe,
                                        ins,
                                        pcc,
                                        maca,
                                        mpoc,
                                        allergiaram, allergiaps, allergia,
                                        pmc, nprod,
                                        pf_desc, atc_desc, dat, datfi,
                                        presdx,
                                        cod, cod_desc, cod_agr_desc, cod_pare_desc))
        print("Ejemplo upload", self.upload[0])
        print("N de registros upload:", len(self.upload))

        self.upload_agr = c.Counter()
        for id_cip_sec, hash, sector, up, up_desc, sap, sap_desc, amb, amb_desc, edatc, sexe, ins, pcc, maca, mpoc, allergiaram, allergiaps, allergia, pmc, nprod, pf_desc, atc_desc, dat, datfi, presdx, cod, cod_desc, cod_agr_desc, cod_pare_desc in self.upload:
            self.upload_agr[(amb_desc, sap_desc, up_desc, atc_desc, presdx, cod, cod_desc, cod_agr_desc, cod_pare_desc, edatc, sexe, ins, pcc, maca, mpoc, allergia)] += 1
        
        self.uploadfin = []
        for (amb_desc, sap_desc, up_desc, atc_desc, presdx, cod, cod_desc, cod_agr_desc, cod_pare_desc, edatc, sexe, ins, pcc, maca, mpoc, allergia), n in self.upload_agr.items():
            self.uploadfin.append((amb_desc, sap_desc, up_desc, atc_desc, presdx, cod, cod_desc, cod_agr_desc, cod_pare_desc, edatc, sexe, ins, pcc, maca, mpoc, allergia, n))

    def export_data(self):
        """."""
        file = u.tempFolder + 'UCEM_ABlligatsDX_{}_{}.csv'
        file = file.format(DATE0.strftime('%Y%m%d'),
                           DATE1.strftime('%Y%m%d'))
        print(file)
        u.writeCSV(file,
                    [('amb_desc', 'sap_desc', 'up_desc', 'atc_desc', 'presdx', 'cod', 'cod_desc', 'codagr_desc', 'codpare_desc', 'edat', 'sexe', 'ins', 'pcc', 'maca', 'mpoc', 'allergia', 'N')] + self.uploadfin)
        with t.SFTP("sisap") as sftp:
            sftp.put(file, "rdehesa/AbLligatsDX_{}.csv".format(DIAEXEC.strftime("%Y_%m_%d")))
            os.remove(file)

    def upload_data(self):
        """."""
        print("---------------------------------------------- upload_data")
        tb = "ucem_ablligatsdx"
        db = "test"
        columns = ["id_cip_sec varchar(10)",
                   "hash varchar(40)",
                   #"cip varchar2(13)",
                   "sector varchar(4)",
                   "up varchar(5)",
                   "up_desc varchar(40)",
                   "sap varchar(5)",
                   "sap_desc varchar(40)",
                   "amb varchar(5)",
                   "amb_desc varchar(40)",
                   "edatc varchar(40)",
                   "sexe varchar(1)",
                   "insti varchar(1)",
                   "maca varchar(1)",
                   "pcc varchar(1)",
                   "mpoc varchar(1)",
                   "allergia varchar(1)",
                   "allergiaps varchar(1)",
                   "pmc int",
                   "nprod int",
                   "pf_desc varchar(100)",
                   "atc_desc varchar(100)",
                   "dat date",
                   "datfi date",
                   "prescdx varchar(1)",
                   "cod varchar(50)",
                   "cod_desc varchar(60)",
                   "cod_agr_desc varchar(60)",
                   "cod_pare_desc varchar(100)"]
        u.createTable(tb, "({})".format(", ".join(columns)), db, rm=True)
        u.listToTable(self.upload, tb, db)
    
def enviament_mail():
    subject = 'AB lligats a DX'
    text = "Bon dia.\n\nInformem que ja es troba disponible a la sftp l'arxiu de seguiment d'antibi�tics lligats a diagn�stics.\n"
    text += "\n\nSalutacions."

    u.sendGeneral('SISAP <sisap@gencat.cat>',
                        'rpdehesa@gencat.cat',
                        'roser.cantenys@catsalut.cat',
                        subject,
                        text)

if __name__ == "__main__":
    if u.IS_MENSUAL:
        sql = """select month(data_ext), year(data_ext) from nodrizas.dextraccio"""
        month, year = u.getOne(sql, 'nodrizas')
        print(month, year)
        if month in (1,4,7,10):
            if month == 10:
                DIAEXEC = d.date(year, 9, 30)
            elif month == 1:
                DIAEXEC = d.date(year-1, 12, 31)
            elif month == 4:
                DIAEXEC = d.date(year, 3, 31)
            elif month == 7:
                DIAEXEC = d.date(year, 6, 30)
            DATE1 = DIAEXEC # - relativedelta(days=18)
            DATE0 = DATE1 - relativedelta(months=3) # + relativedelta(days=2)
            print(DATE0, DATE1)
            clock_in = datetime.now()
            ucem()
            clock_out = datetime.now()
            print("---------------------------------------------- TIME EXECUTION")
            print('''START: {}'''.format(clock_in))
            print('''END: {}'''.format(clock_out))
            print('''DELTA: {}'''.format (clock_out - clock_in))
            enviament_mail()

