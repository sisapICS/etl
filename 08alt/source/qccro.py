import sisapUtils as u
import sisaptools as su
import collections as c
from dateutil import relativedelta as rd
import datetime as d

# data_ext = d.date(2024, 1, 31)
data_ext = u.getOne("select data_ext from dextraccio", "nodrizas")[0]
data_ext_menys1any = data_ext - rd.relativedelta(years=1)

class Indicadors():
    def __init__(self):
        u.printTime('inici')
        self.get_pob();                     u.printTime('get_pob')
        self.get_pcc_maca();                u.printTime('get_pcc_maca')
        self.get_visites();                 u.printTime('get_visites')
        self.get_hosp();                    u.printTime('get_hosp')
        self.get_ingres_intermedia();       u.printTime('get_ingres_intermedia')
        self.get_piic();                    u.printTime('get_piic')
        self.get_valoracions();             u.printTime('get_valoracions')
        self.treat_dades();                 u.printTime('treat_dades')
        self.get_indicadors();              u.printTime('get_indicadors')
        self.export_table();                u.printTime('export_table')
        self.export_khalix();               u.printTime('export_khalix')

    def get_ingres_intermedia(self):
        sql = """
            SELECT nia, dies_est
            FROM dwcatsalut.cmbd_ss
            WHERE d_ingres >= DATE '{data_ext_menys1any}'
        """.format(data_ext_menys1any=data_ext_menys1any)
        self.intermedia = c.Counter()
        self.ingres_intermedia = c.Counter()
        for nia, dies in u.getAll(sql, 'exadata'):
            if nia is not None and int(nia) in self.nia_u11:
                id_cip_sec = self.nia_u11[int(nia)]
                self.intermedia[id_cip_sec] += dies
                self.ingres_intermedia[id_cip_sec] += 1
        print(str(len(self.ingres_intermedia.keys())))
        print(str(len(self.intermedia.keys())))
    def get_valoracions(self):
        self.valoracions = c.defaultdict(set)
        codis = {
            918: 'fragilitat',
            88: 'cognitiu',
            999: 'nutricional',
            751: 'caigudes',
            940: 'caigudes',
            89: 'social_pedia',
            1079: 'funcional'
        }
        sql = """
            SELECT id_cip_sec, agrupador
            FROM eqa_variables
            WHERE agrupador in {codis}
            AND data_var between DATE '{data_ext_menys1any}' and DATE '{data_ext}'
        """.format(codis=tuple(codis.keys()), data_ext_menys1any=data_ext_menys1any, data_ext=data_ext)
        for id_cip_sec, agr in u.getAll(sql, 'nodrizas'):
            self.valoracions[codis[agr]].add(id_cip_sec)
        variables = {
            'VMEDG': 'emocional',
            'EP5105': 'emocional',
            'TIRS': 'social',
            'VZ3005': 'social',
            'VA030I': 'fragilitat'
        }
        sql = """
            SELECT id_cip_sec, vu_cod_vs
            FROM variables1
            WHERE vu_cod_vs in {keys}
            AND vu_dat_act >= DATE '{data_ext_menys1any}'
        """.format(keys=tuple(variables.keys()), data_ext_menys1any=data_ext_menys1any)
        for id_cip_sec, codi in u.getAll(sql, 'import'):
            self.valoracions[variables[codi]].add(id_cip_sec)
        sql = """
            SELECT id_cip_sec, ps
            FROM eqa_problemes
            WHERE ps in (86, 521, 722, 783)
            AND dde <= DATE '{data_ext}'
        """.format(data_ext=data_ext)
        self.neos = set()
        for id_cip_sec, ps in u.getAll(sql, 'nodrizas'):
            if ps == 86:
                self.valoracions['cognitiu'].add(id_cip_sec)
                self.valoracions['emocional'].add(id_cip_sec)
            elif ps in (521, 722):
                self.neos.add(id_cip_sec)
            else:
                self.valoracions['cognitiu'].add(id_cip_sec)

    def get_piic(self):
        # self.pob[id_cip_sec]['pcc']
        piic, piir, piirV = {}, {}, {}
        piirT, pirda = {}, {}
        pirda_pre = {}
        sql = """
            SELECT
                id_cip_sec,
                if(mi_cod_var like ('T4101014%'),'var','text'),
                right(mi_cod_var,2)
            FROM piic
            WHERE
                (mi_cod_var like ('T4101015%') or mi_cod_var like ('T4101014%'))
                -- and mi_ddb = 0
                and mi_data_reg between adddate(DATE '{data_ext}', interval -1 year) and DATE '{data_ext}'
            """.format(data_ext=data_ext)
        for id_cip_sec, camp, valor in u.getAll(sql, 'import'):
            id_cip_sec = int(id_cip_sec)
            if camp == 'var':
                piirV[(id_cip_sec, valor)] = True
            elif camp == 'text':
                piirT[(id_cip_sec, valor)] = True
        u.printTime('piic 1')
        for (id_cip_sec, valor), r in piirV.items():
            if (id_cip_sec, valor) in piirT:
                piic[id_cip_sec] = True
                piir[id_cip_sec] = True

        sql = """
            SELECT
                id_cip_sec,
                if(mi_cod_var = 'T4101001', 'rec',
                    if(mi_cod_var = 'T4101002', 'dant',
                    '0')
                )
            FROM piic
            -- WHERE mi_ddb = 0
            WHERE mi_data_reg between adddate(DATE '{data_ext}', interval -1 year) and DATE '{data_ext}'
            """.format(data_ext=data_ext)
        # hauria de ser piir ben fet + 1002
        for id_cip_sec, tipii in u.getAll(sql, 'import'):
            # piic[int(id_cip_sec)] = int(id_cip_sec)
            if tipii == 'rec':
                piic[int(id_cip_sec)] = True
                piir[int(id_cip_sec)] = True
            elif tipii == 'dant':
                pirda_pre[int(id_cip_sec)] = True
        u.printTime('piic 2')
        sql = """
            SELECT id_cip_sec, right(mi_cod_var, 2)
            FROM piic
            WHERE left(mi_cod_var, 8) = 'T4101017'
            AND mi_data_reg between adddate(DATE '{data_ext}', interval -1 year) and DATE '{data_ext}'
        """.format(data_ext=data_ext)
        crisis = c.defaultdict(set)
        for id_cip_sec, codi in u.getAll(sql, 'import'):
            crisis[id_cip_sec].add(codi)
        u.printTime('piic 3')
        c_2025 = c.defaultdict(dict)
        for id_cip_sec in crisis:
            if len(crisis[id_cip_sec]) == 4:
                piic[int(id_cip_sec)] = True
            if len(crisis[id_cip_sec]) >= 3:
                if '05' in crisis[id_cip_sec] and '06' in crisis[id_cip_sec] and '07' in crisis[id_cip_sec]:
                    c_2025['c1'][int(id_cip_sec)] = True
        u.printTime('crisis')
        for id_cip_sec in pirda_pre:
            if id_cip_sec in piic:
                pirda[int(id_cip_sec)] = True
        u.printTime('pirda_pre')
        
        # afegim criteris del piic 2025 que han de conviure amb el piic 2024
        sql = """
            SELECT id_cip_sec, right(mi_cod_var, 1)
            FROM piic
            WHERE mi_cod_var in ('T410101405', 'T410101407')
            AND mi_data_reg between adddate(DATE '{data_ext}', interval -1 year) and DATE '{data_ext}'
        """.format(data_ext=data_ext)
        for id_cip_sec, codi in u.getAll(sql, 'import'):
            if codi == '5':
                c_2025['c2'][int(id_cip_sec)] = True
            else:
                c_2025['c3'][int(id_cip_sec)] = True

        self.piic = set()
        for id_cip_sec in piic:
            if id_cip_sec in self.pob:
                maca = self.pob[id_cip_sec]['maca'] if 'maca' in self.pob[id_cip_sec] else 0
                if maca == 1:
                    if id_cip_sec in pirda:
                        self.piic.add(id_cip_sec)
                else:
                    self.piic.add(id_cip_sec)
                if id_cip_sec in c_2025['c1'] and id_cip_sec in c_2025['c2'] and id_cip_sec in c_2025['c3']:
                    self.piic.add(id_cip_sec)
        u.printTime('self.piic')

    def export_khalix(self):
        sql = """
            SELECT indicador, 'Aperiodo', concat(b.ics_codi, a.tipus, a.uba), analisi, 'NOCAT', inst, 'DIM6SET', 'N', val
            FROM altres.inds_pcc_maca_uba a
            INNER JOIN nodrizas.cat_centres b
            ON a.up = b.scs_codi
            WHERE up <> ''
        """
        file_name = 'QCCRO_UBA'
        u.exportKhalix(sql, file_name)
        sql = """
            SELECT indicador, 'Aperiodo', b.ics_codi, analisi, edat, inst, sexe, 'N', val
            FROM altres.inds_pcc_maca a
            INNER JOIN nodrizas.cat_centres b
            ON a.up = b.scs_codi
            WHERE up <> ''
        """
        file_name = 'QCCRO'
        u.exportKhalix(sql, file_name)

    def add_aga(self):
        sql = """
            SELECT indicador, up, aga, rs, analisi, val
            FROM inds_pcc_maca
        """
        dades = set()
        for ind, up, aga, rs, analisi, val in u.getAll(sql, 'altres'):
            dades.add((ind, up, aga, aga[-2:], rs, analisi, val))

        agas = {}
        sql = """
            SELECT DISTINCT aga_cod, aga_des
            FROM dwsisap.dbc_centres
        """
        for codi, desc in u.getAll(sql, 'exadata'):
            agas[codi] = str(desc)
        
        table_name = 'inds_pcc_maca'
        db = 'altres'
        cols = "(indicador varchar(20), up varchar(10), aga varchar(8), aga_desc varchar(60), rs varchar(4), analisi varchar(5), val int)"
        u.createTable(table_name, cols, db, rm=True)
        upload = []
        for (ind, up, aga, aga_codi, rs, analisi, val) in dades:
            if aga_codi in agas:
                upload.append((ind, up, aga, agas[aga_codi], rs, analisi, val))
        u.listToTable(upload, table_name, db)

    def get_pob(self):
        self.pob = c.defaultdict()
        sql = """
            SELECT id_cip_sec, up, uba, upinf, ubainf, edat, institucionalitzat, ates, sexe
            FROM assignada_tot a
            INNER JOIN cat_centres b
            ON a.up = b.scs_codi
            -- WHERE institucionalitzat = 0
            -- WHERE ates = 1
        """
        for id_cip_sec, up, uba, upinf, ubainf, edat, inst, ates, sexe in u.getAll(sql, 'nodrizas'):
            self.pob[id_cip_sec] = {
                'up': up,
                'uba': uba,
                'upinf': upinf,
                'ubainf': ubainf,
                'edat': edat,
                'institucionalitzat': inst,
                'ates': ates,
                'sexe': sexe,
                'exitus': 0
            }
        sql = """
            SELECT id_cip_sec, up, uab, uabinf
            FROM assignadahistorica_s2024
        """
        pob23 = {id_cip_sec: (up, uba, ubainf) for id_cip_sec, up, uba, ubainf in u.getAll(sql, 'import')}
        sql = """
            SELECT id_cip_sec, up, uab, uabinf
            FROM assignadahistorica_s2023
        """
        pob22 = {id_cip_sec: (up, uba, ubainf) for id_cip_sec, up, uba, ubainf in u.getAll(sql, 'import')}
        sql = """
            SELECT id_cip_sec, timestampdiff(year, usua_data_naixement, usua_data_situacio), usua_sexe, year(usua_data_situacio)
            FROM assignada
            WHERE usua_situacio = 'D'
            AND usua_data_situacio >= DATE '{menys1}'
        """.format(menys1=data_ext_menys1any)
        for id_cip_sec, edat, sexe, any_s in u.getAll(sql, 'import'):
            up, uba, ubainf = None, None, None
            if any_s == 2024 and id_cip_sec in pob22:
                (up, uba, ubainf) = pob22[id_cip_sec]
            elif any_s == 2025 and id_cip_sec in pob23:
                (up, uba, ubainf) = pob23[id_cip_sec]
            self.pob[id_cip_sec] = {
                'up': up,
                'uba': uba,
                'upinf': up,
                'ubainf': ubainf,
                'edat': edat,
                'institucionalitzat': 0,
                'ates': 1,
                'sexe': sexe,
                'exitus': 1
            }
    
    def get_pcc_maca(self):
        sql = """
            SELECT id_cip_sec, if(es_cod = 'ER0001', 1, 0), if (es_cod = 'ER0002', 1, 0)
            FROM estats
        """
        for id_cip_sec, pcc, maca in u.getAll(sql, 'import'):
            if id_cip_sec in self.pob:
                self.pob[id_cip_sec]['pcc'] = pcc
                self.pob[id_cip_sec]['maca'] = maca
    
    def get_visites(self):
        self.visites = c.defaultdict(c.Counter)
        self.pacients = c.defaultdict(set)
        tables = u.getSubTables('visites1', 'import')
        sql = """
            SELECT id_cip_sec,
            case left(visi_col_prov_resp, 1)
            WHEN  1 THEN 'MED'
            WHEN 3 THEN 'INF' end as var,
            visi_data_visita,
            visi_tipus_visita,
            visi_lloc_visita
            FROM {tb}
            WHERE visi_situacio_visita = 'R'
            AND s_espe_codi_especialitat not in ('EXTRA','10102')
            AND left(visi_col_prov_resp, 1) in ('1', '3')
            AND visi_data_visita between DATE '{menys12}'
            AND DATE '{data_ext}'
        """
        jobs = [(table, sql) for table in tables]
        for (chunk, set_chunk) in u.multiprocess(sub_get_responsables, jobs, 6, close=True):
            for cip, vars in chunk.items():
                for var, val in vars.items():
                    if val >= 0:
                        self.visites[cip][var] += val
            for var, set_pac in set_chunk.items():
                self.pacients[var].update(set_pac)
        print(len(self.visites.keys()))
    
    def get_hosp(self):
        self.hosp = c.defaultdict(set)
        self.urgencies = c.Counter()
        sql = """
            SELECT nia, hash_redics, hash_covid
            FROM dwsisap.pdptb101_relacio_nia
        """
        nia_hash = {}
        redics_covid = {}
        for nia, hash_r, hash_c in u.getAll(sql, 'exadata'):
            nia_hash[hash_r] = nia
            redics_covid[hash_r] = hash_c
        u.printTime('pdptb101_relacio_nia')

        sql = """
            SELECT id_cip_sec, hash_d
            FROM u11
        """
        self.nia_u11 = {}
        self.hash_u11 = {}
        for id_cip_sec, hash_r in u.getAll(sql, 'import'):
            if hash_r in nia_hash:
                self.nia_u11[nia_hash[hash_r]] = id_cip_sec
            if hash_r in redics_covid: #hash_c a id_cip_sec
                self.hash_u11[redics_covid[hash_r]] = id_cip_sec
        u.printTime('u11')
        # RES078AA
        sql = """SELECT c_nia, c_up FROM dwsisap.cmbd_urg
                WHERE c_nia != 99999999
                AND c_data_entr > DATE '{data_ext_menys1any}'""".format(data_ext_menys1any=data_ext_menys1any)
        for nia, up in u.getAll(sql, 'exadata'):
            if nia in self.nia_u11:
                id_cip_sec = self.nia_u11[nia]
                if up == '01028':
                    self.hosp['sem'].add(id_cip_sec)
                else:
                    self.hosp['hosp'].add(id_cip_sec)
                self.hosp['urgencies'].add(id_cip_sec)
                self.urgencies[id_cip_sec] += 1
        u.printTime('cs_urgencies')

        # RES078AB
        sql = """SELECT pacient FROM dwsisap.sisap_master_visites
                WHERE data > DATE '{data_ext_menys1any}'
                AND up IN (SELECT up_cod FROM dwsisap.dbc_rup 
                WHERE subtip_cod = '24')""".format(data_ext_menys1any=data_ext_menys1any)
        for hash_c, in u.getAll(sql, 'exadata'):
            if hash_c in self.hash_u11:
                id_cip_sec = self.hash_u11[hash_c]
                self.hosp['centre'].add(id_cip_sec)
                self.hosp['urgencies'].add(id_cip_sec)
                self.urgencies[id_cip_sec] += 1
        u.printTime('sisap_master_visites')

        # seleccionem els ingressos segons nova definicio UMI i urgencies restants tmb
        sql = """SELECT nia, data_ingres, data_alta - data_ingres AS dies_hosp, CASE WHEN C_ALTA = '6' THEN 1 ELSE 0 END exitus,
                CASE WHEN t_act = '134' AND c_alta = '1' THEN 1 ELSE 0 END urg
                FROM dwcatsalut.tf_cmbdha
                WHERE nia IS NOT NULL
                AND data_alta > DATE '{data_ext_menys1any}'
                AND ser_alta <> '40101'
                AND pr_ingres <> '7'""".format(data_ext_menys1any=data_ext_menys1any)
        ingressos = c.defaultdict(set)
        self.ingres = c.Counter()
        self.dies_hospitalitzats = c.Counter()
        self.exitus_hosp = set()
        for nia, ingres, dies, exitus, urg in u.getAll(sql, 'exadata'):
            if nia in self.nia_u11:
                id_cip_sec = self.nia_u11[nia]
                if urg:
                    self.hosp['urgencies'].add(id_cip_sec)
                    self.hosp['hosp'].add(id_cip_sec)
                    self.urgencies[id_cip_sec] += 1
                else:
                    self.hosp['ingres_12m'].add(id_cip_sec)
                    ingressos[id_cip_sec].add(ingres)
                    self.ingres[id_cip_sec] += 1
                    self.dies_hospitalitzats[id_cip_sec] += dies
                    if exitus == 1:
                        self.exitus_hosp.add(id_cip_sec)
        for id_cip_sec in ingressos:
            dates = sorted(list(ingressos[id_cip_sec]))
            for i in range(0, len(dates)-1):
                if (dates[i+1] - dates[i]).days <= 30:
                    self.hosp['reingres'].add(id_cip_sec)

    def treat_dades(self):
        self.dades = c.defaultdict(dict)
        for id_cip_sec in self.pob:
            edat = self.pob[id_cip_sec]['edat']
            pcc = self.pob[id_cip_sec]['pcc'] if 'pcc' in self.pob[id_cip_sec] else 0
            maca = self.pob[id_cip_sec]['maca'] if 'maca' in self.pob[id_cip_sec] else 0
            exitus = self.pob[id_cip_sec]['exitus']
            if not exitus:
                self.dades['total'][id_cip_sec] = 1
            else:
                self.dades['exitus_total'][id_cip_sec] = 1
            if edat < 15:
                self.dades['<15'][id_cip_sec] = 1
            if edat >= 65 and not exitus:
                self.dades['>=65'][id_cip_sec] = 1
            if edat >= 80 and not exitus:
                self.dades['>=80'][id_cip_sec] = 1
            if edat <14 and not exitus:
                self.dades['<14'][id_cip_sec] = 1
            if pcc == 1:
                if not exitus:
                    self.dades['pcc'][id_cip_sec] = 1
                self.dades['pcc+exitus'][id_cip_sec] = 1
                if edat >= 0 and edat <= 14 and not exitus:
                    self.dades['pcc_pedia'][id_cip_sec] = 1
            if maca == 1:
                if not exitus:
                    self.dades['maca'][id_cip_sec] = 1
                self.dades['maca+exitus'][id_cip_sec] = 1
        u.printTime('treat pob')
        for id_cip_sec in self.visites:
            if id_cip_sec in self.pob:
                for var, val in self.visites[id_cip_sec].items():
                    self.dades[var][id_cip_sec] = self.visites[id_cip_sec][var]
        u.printTime('treat visites')
        for var in self.pacients:
            for id_cip_sec in self.pacients[var]:
                if id_cip_sec in self.pob:
                    self.dades['PAC_'+var][id_cip_sec] = 1
        u.printTime('treat pacients')
        for id_cip_sec in self.neos:
            if id_cip_sec in self.pob:
                self.dades['neos'][id_cip_sec] = 1
        u.printTime('treat neos')
        for var in self.hosp:
            for id_cip_sec in self.hosp[var]:
                if id_cip_sec in self.pob:
                    self.dades[var][id_cip_sec] = 1
                    if id_cip_sec in self.dades['pcc']:
                        self.dades[var+'_pcc'][id_cip_sec] = 1
                    if id_cip_sec in self.dades['maca']:
                        self.dades[var+'_maca'][id_cip_sec] = 1
        u.printTime('treat hosp')
        for id_cip_sec in self.urgencies:
            self.dades['total_urg'][id_cip_sec] = self.urgencies[id_cip_sec]
        u.printTime('urgencies')
        for id_cip_sec in self.piic:
            if id_cip_sec in self.pob:
                self.dades['piic'][id_cip_sec] = 1
        u.printTime('treat piic')
        for id_cip_sec in self.valoracions['funcional']:
            if id_cip_sec in self.pob and id_cip_sec not in self.dades['<15'] and id_cip_sec in self.valoracions['cognitiu'] and id_cip_sec in self.valoracions['nutricional'] and id_cip_sec in self.valoracions['caigudes'] and id_cip_sec in self.valoracions['emocional'] and id_cip_sec in self.valoracions['social']:
                self.dades['valoracio'][id_cip_sec] = 1
            elif id_cip_sec in self.pob and id_cip_sec in self.dades['<15'] and id_cip_sec in self.valoracions['social_pedia']:
                self.dades['valoracio'][id_cip_sec] = 1
            # if id_cip_sec in self.risc_social and id_cip_sec in self.dades['<15']:
            #     self.dades['valoracio'][id_cip_sec] = 1
        for id_cip_sec in self.valoracions['fragilitat']:
            # if id_cip_sec not in self.excl_valoracio and id_cip_sec not in self.dades['<15']:
            self.dades['valoracio'][id_cip_sec] = 1
        u.printTime('valoracio')
        for id_cip_sec in self.dies_hospitalitzats:
            if id_cip_sec in self.dades['dies']:
                self.dades['dies'][id_cip_sec] += self.dies_hospitalitzats[id_cip_sec]
                self.dades['dies_hosp'][id_cip_sec] += self.dies_hospitalitzats[id_cip_sec]
            else:
                self.dades['dies'][id_cip_sec] = self.dies_hospitalitzats[id_cip_sec]
                self.dades['dies_hosp'][id_cip_sec] = self.dies_hospitalitzats[id_cip_sec]
        u.printTime('dies_hospitalitzats')
        for id_cip_sec in self.exitus_hosp:
            self.dades['exitus'][id_cip_sec] = 1
            self.dades['exitus_total'][id_cip_sec] = 1
        u.printTime('exitus')
        for id_cip_sec in self.intermedia:
            if id_cip_sec in self.dades['dies']:
                self.dades['dies'][id_cip_sec] += self.intermedia[id_cip_sec]
            else:
                self.dades['dies'][id_cip_sec] = self.intermedia[id_cip_sec]
        u.printTime('intermedia')
        for id_cip_sec in self.ingres_intermedia:
            if id_cip_sec in self.dades['ingres_intermedia']:
                self.dades['ingres_intermedia'][id_cip_sec] += self.ingres_intermedia[id_cip_sec]
            else:
                self.dades['ingres_intermedia'][id_cip_sec] = self.ingres_intermedia[id_cip_sec]
            self.dades['intermedia'][id_cip_sec] = 1
        u.printTime('ingres intermedia')
        for id_cip_sec in self.dades['dies']:
            self.dades['dies'][id_cip_sec] = 365 - self.dades['dies'][id_cip_sec]
        u.printTime('dies')

    def get_indicadors(self):
        inds = {
            'QCCRO001': {
                'NUM': self.dades['pcc'],
                'DEN': self.dades['total']
            },
            'QCCRO001A': {
                'NUM': self.dades['pcc'],
                'DEN': self.dades['>=80']
            },
            'QCCRO001B': {
                'NUM': self.dades['pcc'],
                'DEN': self.dades['>=65']
            },
            'QCCRO002': {
                'NUM': self.dades['maca'],
                'DEN': self.dades['total']
            },
            'QCCRO002A': {
                'NUM': self.dades['maca'],
                'DEN': self.dades['>=80']
            },
            'QCCRO002B': {
                'NUM': self.dades['maca'],
                'DEN': self.dades['>=65']
            },
            'QCCRO003': {
                'NUM': self.dades['ingres_12m'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO004': {
                'NUM': self.dades['ingres_12m'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO005': {
                'NUM': self.dades['reingres'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO006': {
                'NUM': self.dades['reingres'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO007': {
                'NUM': self.dades['INF'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO007P': {
                'NUM': self.dades['P_INF'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO007T': {
                'NUM': self.dades['T_INF'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO007D': {
                'NUM': self.dades['D_INF'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO008': {
                'NUM': self.dades['PAC_INF'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO008P': {
                'NUM': self.dades['PAC_P_INF'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO008T': {
                'NUM': self.dades['PAC_T_INF'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO008D': {
                'NUM': self.dades['PAC_D_INF'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO009': {
                'NUM': self.dades['INF'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO009P': {
                'NUM': self.dades['P_INF'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO009T': {
                'NUM': self.dades['T_INF'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO009D': {
                'NUM': self.dades['D_INF'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO010': {
                'NUM': self.dades['PAC_INF'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO010P': {
                'NUM': self.dades['PAC_P_INF'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO010T': {
                'NUM': self.dades['PAC_T_INF'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO010D': {
                'NUM': self.dades['PAC_D_INF'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO011': {
                'NUM': self.dades['MED'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO011P': {
                'NUM': self.dades['P_MED'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO011T': {
                'NUM': self.dades['T_MED'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO011D': {
                'NUM': self.dades['D_MED'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO012': {
                'NUM': self.dades['PAC_MED'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO012P': {
                'NUM': self.dades['PAC_P_MED'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO012T': {
                'NUM': self.dades['PAC_T_MED'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO012D': {
                'NUM': self.dades['PAC_D_MED'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO013': {
                'NUM': self.dades['MED'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO013P': {
                'NUM': self.dades['P_MED'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO013T': {
                'NUM': self.dades['T_MED'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO013D': {
                'NUM': self.dades['D_MED'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO014': {
                'NUM': self.dades['PAC_MED'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO014P': {
                'NUM': self.dades['PAC_P_MED'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO014T': {
                'NUM': self.dades['PAC_T_MED'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO014D': {
                'NUM': self.dades['PAC_D_MED'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO015': {
                'NUM': self.dades['urgencies'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO015A': {
                'NUM': self.dades['hosp'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO015B': {
                'NUM': self.dades['centre'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO015C': {
                'NUM': self.dades['sem'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO016': {
                'NUM': self.dades['urgencies'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO016A': {
                'NUM': self.dades['hosp'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO016B': {
                'NUM': self.dades['centre'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO016C': {
                'NUM': self.dades['sem'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO017': {
                'NUM': self.dades['piic'],
                'DEN': self.dades['pcc']
            },
            'QCCRO017A': {
                'NUM': self.dades['piic'],
                'DEN': self.dades['pcc_pedia']
            },
            'QCCRO018': {
                'NUM': self.dades['piic'],
                'DEN': self.dades['maca']
            },
            'QCCRO019': {
                'NUM': self.ingres,
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO020': {
                'NUM': self.ingres,
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO021': {
                'NUM': self.dades['dies'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO022': {
                'NUM': self.dades['dies'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO023': {
                'NUM': self.dades['valoracio'],
                'DEN': self.dades['pcc']
            },
            'QCCRO024': {
                'NUM': self.dades['valoracio'],
                'DEN': self.dades['maca']
            },
            'QCCRO025': {
                'NUM': self.dades['dies_hosp'],
                'DEN': self.dades['ingres_12m_pcc']
            },
            'QCCRO026': {
                'NUM': self.dades['exitus'],
                'DEN': self.dades['ingres_12m_pcc']
            },
            'QCCRO027': {
                'NUM': self.dades['dies_hosp'],
                'DEN': self.dades['ingres_12m_maca']
            },
            'QCCRO028': {
                'NUM': self.dades['exitus'],
                'DEN': self.dades['ingres_12m_maca']
            },
            'QCCRO029': {
                'NUM': self.dades['intermedia'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO030': {
                'NUM': self.dades['intermedia'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO031': {
                'NUM': self.dades['ingres_intermedia'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO032': {
                'NUM': self.dades['ingres_intermedia'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO033': {
                'NUM': self.dades['exitus_total'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO034': {
                'NUM': self.dades['exitus_total'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO035': {
                'NUM': self.dades['total_urg'],
                'DEN': self.dades['pcc+exitus']
            },
            'QCCRO036': {
                'NUM': self.dades['total_urg'],
                'DEN': self.dades['maca+exitus']
            },
            'QCCRO037': {
                'NUM': self.dades['neos'],
                'DEN': self.dades['maca']
            },
            'QCCRO038': {
                'NUM': (set(self.dades['pcc']) | set(self.dades['maca'])) & set(self.dades['piic']),
                'DEN': self.dades['<14']
            }
        }
        khalix = {}
        sql = "select edat, khalix from khx_edats5a"
        for edat, edat_khlx in u.getAll(sql, 'nodrizas'):
            khalix[edat] = edat_khlx
        self.resultat = c.Counter()
        self.resultat_up = c.Counter()
        landing = []
        # self.pacients = []
        for ind, analisi in inds.items():
            u.printTime(ind)
            num = analisi['NUM']
            den = analisi['DEN']
            for id_cip_sec in den:
                if id_cip_sec in self.pob:
                    up = self.pob[id_cip_sec]['up']
                    uba = self.pob[id_cip_sec]['uba']
                    upinf = self.pob[id_cip_sec]['upinf']
                    ubainf = self.pob[id_cip_sec]['ubainf']
                    inst = self.pob[id_cip_sec]['institucionalitzat']
                    ates = self.pob[id_cip_sec]['ates']
                    sexe = self.pob[id_cip_sec]['sexe']
                    if sexe == 'D':
                        sexe = 'DONA'
                    else:
                        sexe = 'HOME'
                    edat = self.pob[id_cip_sec]['edat']
                    poblacions = []
                    pobl = 'INS'
                    if not inst:
                        pobl = 'NO' + pobl
                    if ates:
                        poblacions.append(pobl + 'ATCRO')
                    poblacions.append(pobl + 'ASSCRO')
                    try:
                        for poblacio in poblacions:
                            # inst = 'INSATCRO' if self.pob[id_cip_sec]['institucionalitzat'] == 1 else 'NOINSATCRO'
                            self.resultat[(ind, up, uba, 'M', poblacio, 'DEN')] += den[id_cip_sec]
                            self.resultat[(ind, upinf, ubainf, 'I', poblacio, 'DEN')] += den[id_cip_sec]
                            self.resultat_up[(ind, up, poblacio, khalix[edat], sexe, 'DEN')] += den[id_cip_sec]
                            c_num = 0
                            if ind in ('QCCRO038',) and id_cip_sec in num:
                                self.resultat[(ind, up, uba, 'M', poblacio, 'NUM')] += 1 if id_cip_sec in num else 0
                                self.resultat[(ind, upinf, ubainf, 'I', poblacio, 'NUM')] += 1 if id_cip_sec in num else 0
                                self.resultat_up[(ind, up, poblacio, khalix[edat], sexe, 'NUM')] += 1 if id_cip_sec in num else 0
                            elif id_cip_sec in num:
                                self.resultat[(ind, up, uba, 'M', poblacio, 'NUM')] += num[id_cip_sec]
                                self.resultat[(ind, upinf, ubainf, 'I', poblacio, 'NUM')] += num[id_cip_sec]
                                self.resultat_up[(ind, up, poblacio, khalix[edat], sexe, 'NUM')] += num[id_cip_sec]
                                c_num = 1
                            elif ind in ('QCCRO021', 'QCCRO022'):
                                self.resultat[(ind, up, uba, 'M', poblacio, 'NUM')] += 365
                                self.resultat[(ind, upinf, ubainf, 'I', poblacio, 'NUM')] += 365
                                self.resultat_up[(ind, up, poblacio, khalix[edat], sexe, 'NUM')] += 365
                            if ind in ('QCCRO023', 'QCCRO024'): #peticio landing
                                landing.append((id_cip_sec, ind, up, uba, ubainf, poblacio, c_num))
                    except KeyError:
                        continue
        cols = "(id_cip_sec int, ind varchar(10), up varchar(10), uba varchar(20), ubainf varchar(20), poblacio varchar(20), num int)"
        u.createTable('landing_qccro', cols, 'altres', rm=True)
        u.listToTable(landing, 'landing_qccro', 'altres')

    def export_table(self):
        table_name = 'inds_pcc_maca_uba'
        db = 'altres'
        cols = "(indicador varchar(20), up varchar(20), uba varchar(20), tipus varchar(2), inst varchar(12), analisi varchar(5), val int)"
        u.createTable(table_name, cols, db, rm=True)
        upload = []
        for (ind, up, uba, tipus, inst, analisi) in self.resultat:
            upload.append((ind, up, uba, tipus, inst, analisi, self.resultat[(ind, up, uba, tipus, inst, analisi)]))
        u.listToTable(upload, table_name, db)

        table_name = 'inds_pcc_maca'
        db = 'altres'
        cols = "(indicador varchar(20), up varchar(20), inst varchar(12), edat varchar(6), sexe varchar(4), analisi varchar(5), val int)"
        u.createTable(table_name, cols, db, rm=True)
        upload = []
        for (ind, up, inst, edat, sexe, analisi) in self.resultat_up:
            upload.append((ind, up, inst, edat, sexe, analisi, self.resultat_up[(ind, up, inst, edat, sexe, analisi)]))
        u.listToTable(upload, table_name, db)
        # table_name = 'validacio_valoracio'
        # db = 'altres'
        # cols = "(indicador varchar(20), id_cip_sec int, numerador int)"
        # u.createTable(table_name, cols, db, rm=True)
        # upload = []
        # for (ind, id_cip_sec, num) in self.pacients:
        #     upload.append((ind, id_cip_sec, num))
        # u.listToTable(upload, table_name, db)
        # table_name = 'validacio_pcc_maca'
        # db = 'altres'
        # cols = "(indicador varchar(20), id_cip_sec int, num int)"
        # u.createTable(table_name, cols, db, rm=True)
        # upload = []
        # for (id_cip_sec, num, den) in self.resultat:
        #     upload.append((ind, up, aga, rs, analisi, self.resultat[(ind, up, aga, rs, analisi)]))
        # u.listToTable(self.pacients, table_name, db)

def sub_get_responsables(params):
    table, sql = params
    d_resp = c.defaultdict(c.Counter)
    set_resp = c.defaultdict(set)
    menys12 = data_ext - rd.relativedelta(years=1)
    print(sql.format(tb=table, data_ext=data_ext, menys12=menys12))
    # print(sql.format(tb=table))
    for cip, var, data, tipus, lloc in u.getAll(sql.format(tb=table, data_ext=data_ext, menys12=menys12), 'import'):
        if tipus not in ('9E', 'EXTRA', 'EXT'):
            d_resp[cip][var] += 1
            set_resp[var].add(cip)
        if tipus == '9T':
            d_resp[cip]['T_'+var] += 1
            set_resp['T_'+var].add(cip)
        if tipus in ('9R', '9C'):
            d_resp[cip]['P_'+var] += 1
            set_resp['P_'+var].add(cip)
        if tipus in ('9D'):
            d_resp[cip]['D_'+var] += 1
            set_resp['D_'+var].add(cip)
    u.printTime(table)
    print(len(d_resp.keys()))
    return (d_resp, set_resp)

Indicadors()