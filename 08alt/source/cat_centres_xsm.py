import sisapUtils as u

centres_ecap = ("01021", "00856", "00889", "01020", "03249", "03601",
                "03973", "00231", "03600", "00902", "05645", "07717",
                "07718", "08095", "01022", "00425", "01022", "07724",
                "01808", "01868", "01869", "03085", "16060", "16137", 
                "00926", "00974", "16122")

tb_nom = "CAT_CENTRES_XSM"

dades = list()
sql = """
        SELECT
            UP_COD,
            UP_DES,
            ES_ICS,
            TIP_COD,
            TIP_DES,
            SUBTIP_COD,
            SUBTIP_DES,
            ABS_COD,
            ABS_DES,
            SECTOR_COD,
            SECTOR_DES,
            AGA_COD,
            AGA_DES,
            REGIO_COD,
            REGIO_DES,
            EP_COD,
            EP_DES,
            TIT_COD,
            TIT_DES
        FROM
            dwsisap.DBC_RUP
        WHERE
            subTIP_COD IN (38, 61, 64, 62)
        ORDER BY 1
      """
for up_cod, up_des, es_ics, tip_cod, tip_des, subtip_cod, subtip_des, abs_cod, abs_des, sector_cod, sector_des, aga_cod, aga_des, regio_cod, regio_des, ep_cod, ep_des, tit_cod, tit_des in u.getAll(sql, "exadata"):
    row = (up_cod, up_des, es_ics, tip_cod, tip_des, subtip_cod, subtip_des, abs_cod, abs_des, sector_cod, sector_des, aga_cod, aga_des, regio_cod, regio_des, ep_cod, ep_des, tit_cod, tit_des, 1 if up_cod in centres_ecap else 0)
    dades.append(row)

dades.sort()

tb_columnes = '(UP_COD varchar2(10), UP_DES varchar2(100), ES_ICS number, TIP_COD varchar2(10), TIP_DES varchar2(100), SUBTIP_COD varchar2(10), SUBTIP_DES varchar2(100), ABS_COD varchar2(3), ABS_DES varchar2(255), SECTOR_COD varchar2(4), SECTOR_DES varchar2(255), AGA_COD number(38,0), AGA_DES varchar2(255), REGIO_COD varchar2(2), REGIO_DES varchar2(255), EP_COD varchar2(100), EP_DES varchar2(100), TIT_COD varchar2(2000), TIT_DES varchar2(2000), ECAP number(1))'
u.createTable(tb_nom, tb_columnes, "exadata", rm = True)
u.listToTable(dades, tb_nom, "exadata")

u.grantSelect(tb_nom, 'DWSISAP_ROL', 'exadata')
u.grantSelect(tb_nom, 'DWPFABREGAT', 'exadata')
u.grantSelect(tb_nom, 'DWRENV_SALUT_MENTAL_SELECT', 'exadata')
