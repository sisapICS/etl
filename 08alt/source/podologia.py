#  coding: latin1

import sisapUtils as u
import collections as c

# Inicialitzaci� de les constants i mapes utilitzats
tb = "exp_khalix_podologia"
db = "altres"
file = "POD"

map_activitat_tipus = {"SE": "PJ", "OTR": "AL", "SB": "PG", "PR": "PG", "CO": "MC", "CA": "PG"}
map_activitat = {"PS": "AL", "VA": "AL", "OTR": "AL", "II": "AL", "CR": "AL", "CO": "AL"}
keys_act_grupal_i_o_comunitaria = {"C": "B", "G": "A", "GC": "C"}
keys_activitat_tipus = {"SI": "A", "TA": "B", "PJ": "C", "PG": "D", "AL": "E", "ST": "G", "MC": "H"}
keys_activitat_tematica = {"AF": "A", "MC": "B", "SE": "C", "SM": "D", "ES": "E", "MI": "F", "AL": "G", "TA": "I",
                           "SB": "J", "CU": "K", "VI": "L", "RE": "M", "SS": "N", "S2": "O", "S3": "P", "S1": "Q", "S4": "R", "S5": "S"}
indicadors_poblacio_atesa_assignada = ["BEN01", "BEN01A", "BEN01B", "BEN01C", "BEN02", "BEN03", "BEN08"]

UP_MODS = {
    '07446': '08430',
    '05093': '08434',
    '07537': '08431',
    '05092': '08435',
    '05826': '08432',
    '05091': '08438',
    '07538': '08436',
    '06164': '08433',
    '05090': '08439',
    }

class Podologia(object):
    """Classe principal per a l'an�lisi dels indicadors de podologia (POD)."""

    def __init__(self):
        """Inicialitza l'objecte Podologia i executa les funcions necess�ries en ordre."""

        self.get_dates();                               print("self.get_dates()")
        self.get_centres();                             print("self.get_centres()")
        self.get_assignada();                           print("self.get_assignada()")
        self.get_problemes();                           print("self.get_problemes()")
        self.get_variables();                           print("self.get_variables()")
        self.get_derivacions_podologia();               print("self.get_derivacions_podologia()")
        self.get_indicadors();                          print("self.get_indicadors()")
        self.exportar();                                print("self.exportar()")


    def get_dates(self):
        """Obt� la data d'extracci� de dades, la d'un any enrere, i les guarda en les variables d'inst�ncia."""

        sql = """
                SELECT
                    date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
                    data_ext
                FROM
                    dextraccio
              """
        self.dext_menys1any, self.dext = u.getOne(sql, "nodrizas")


    def get_centres(self):
        """Obt� els codis UP i BR dels Centres d'Atenci� Prim�ria i els guarda a l'atribut self.centres."""

        sql = """
                SELECT
                    scs_codi,
                    ics_codi
                FROM
                    cat_centres
              """
        self.centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}


    def get_assignada(self):
        """Obt� informaci� necess�ria de la poblaci� assignada i atesa adulta, aix� com un conversor de hash a id_cip_sec."""
        
        self.poblacio = dict()
        self.hash2idcipsec = dict()

        sql_1 = """
                    SELECT
                        id_cip_sec,
                        up,
                        edat,
                        sexe
                    FROM
                        assignada_tot
                    WHERE
                        ates = 1
                        AND edat >= 14
                """
        for id_cip_sec, up, edat, sexe in u.getAll(sql_1, "nodrizas"):
            up = UP_MODS.get(up, up)
            if up in self.centres:
                self.poblacio[id_cip_sec] = {"up": up, "edat": edat, "sexe": sexe}

        sql_2 = """
                    SELECT
                        hash_d,
                        id_cip_sec 
                    FROM
                        u11
                """
        for hash, id_cip_sec in u.getAll(sql_2, "import"):
            if id_cip_sec in self.poblacio:
                self.hash2idcipsec[hash] = id_cip_sec


    def get_problemes(self):
        """Obt� la informaci� dels problemes de salut a considerar i la guarda a l'atribut self.problemes."""

        self.problemes = c.defaultdict(set)

        ps_dict = {18: "DM2",
                   24: "DM1",
                   218: "Manca dels dos peus",
                   703: "Manca dels dos peus"}

        sql = """
                SELECT
                    id_cip_sec,
                    ps,
                    dde
                FROM
                    eqa_problemes
                WHERE
                    ps IN {}
                    AND dde <= '{}'
              """.format(str(tuple(ps_dict.keys())), self.dext)
        for id_cip_sec, ps, data_episodi in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                self.problemes[ps_dict[ps]].add(id_cip_sec)


    def get_variables(self):
        """Obt� les variables relacionades amb els indicadors i les guarda en les variables d'inst�ncia."""

        self.variables = c.defaultdict(set)
        self.variables_valors = c.defaultdict(lambda : c.defaultdict(set))
        self.variables_dates = c.defaultdict(lambda : c.defaultdict(dict))

        vars_dict = {962: {"desc": "C�lcul del risc en exploraci� podol�gica"}}

        sql = """
                SELECT
                    id_cip_sec,
                    agrupador,
                    data_var,
                    valor,
                    usar
                FROM
                    eqa_variables
                WHERE
                    agrupador = {}
                    AND data_var BETWEEN '{}' AND '{}'
              """.format(962, self.dext_menys1any, self.dext)   #.format(str(tuple(vars_dict.keys())), self.dext_menys1any)
        for id_cip_sec, agrupador, data_var, valor, usar in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                self.variables_dates[vars_dict[agrupador]["desc"]][id_cip_sec][data_var] = valor                
                # Si ens la variable considerada �s la �ltima feta al pacient
                if usar == 1:
                    self.variables[vars_dict[agrupador]["desc"]].add(id_cip_sec)
                    self.variables_valors[vars_dict[agrupador]["desc"]][valor].add(id_cip_sec)
                    # Si un c�lcul del risc en exploraci� podol�gica t� un valor de variable superior a 1
                    if vars_dict[agrupador]["desc"] == "C�lcul del risc en exploraci� podol�gica" and valor > 1:
                        self.variables[vars_dict[agrupador]["desc"] + " amb resultat > 1"].add(id_cip_sec)
                # Si s'han fet, al menys, dos c�lculs del risc en exploraci� podol�gica
                else:
                    if vars_dict[agrupador]["desc"] == "C�lcul del risc en exploraci� podol�gica":
                        self.variables[vars_dict[agrupador]["desc"] + " (dos cops)"].add(id_cip_sec)

        # Comprovar si l'�ltim c�lcul del risc en exploraci� podol�gica �s superior al primer fet durant l'any (o viceversa)
        for id_cip_sec in self.variables_dates["C�lcul del risc en exploraci� podol�gica"]:
            # Si hi ha m�s d'un c�lcul del risc en exploraci� podol�gica realitzat l'�ltim any
            if len(self.variables_dates["C�lcul del risc en exploraci� podol�gica"][id_cip_sec]) > 1:
                dates_ordenades = sorted(self.variables_dates["C�lcul del risc en exploraci� podol�gica"][id_cip_sec].keys())
                primer_calcul = self.variables_dates["C�lcul del risc en exploraci� podol�gica"][id_cip_sec][dates_ordenades[0]]
                ultim_calcul = self.variables_dates["C�lcul del risc en exploraci� podol�gica"][id_cip_sec][dates_ordenades[-1]]
                if primer_calcul > ultim_calcul:
                    self.variables["C�lcul del risc en exploraci� podol�gica (VALOR DESCENDENT)"].add(id_cip_sec)
                elif primer_calcul < ultim_calcul:
                    self.variables["C�lcul del risc en exploraci� podol�gica (VALOR ASCENDENT)"].add(id_cip_sec)

    def get_derivacions_podologia(self):
        """Obt� les derivacions de pacients a podologia i les guarda en les variables d'inst�ncia."""

        self.derivacions_podologia = c.defaultdict(set)

        sql = """
                SELECT 
                    id_cip_sec,
                    oc_numid
                FROM
                    derivacions
                WHERE
                    inf_servei_d_codi = 'OOOOO'
                    AND oc_data BETWEEN '{}' AND '{}'
              """.format(self.dext_menys1any, self.dext)
        for id_cip_sec, numid in u.getAll(sql, "import"):
            if id_cip_sec in self.poblacio:
                self.derivacions_podologia[id_cip_sec].add(numid)


    def get_indicadors(self):
        """Calcula els indicadors de podologia utilitzant les dades obtingudes i els guarda a la variable self.resultats."""

        self.resultats = c.Counter()

        cond_ind = {"POD01":  {"dens": self.problemes["DM1"] | self.problemes["DM2"],
                               "nums": self.variables["C�lcul del risc en exploraci� podol�gica"],
                               "excls": self.problemes["Manca dels dos peus"]},
                    "POD02":  {"dens": self.problemes["DM1"],
                               "nums": self.variables["C�lcul del risc en exploraci� podol�gica"],
                               "excls": self.problemes["Manca dels dos peus"]},
                    "POD03":  {"dens": self.problemes["DM2"] - self.problemes["DM1"],
                               "nums": self.variables["C�lcul del risc en exploraci� podol�gica"],
                               "excls": self.problemes["Manca dels dos peus"]},
                    "POD04":  {"dens": (self.problemes["DM1"] | self.problemes["DM2"]) & self.variables["C�lcul del risc en exploraci� podol�gica amb resultat > 1"],
                               "nums": set(self.derivacions_podologia.keys()),
                               "excls": set()},
                    "POD04A": {"dens": self.problemes["DM1"] & self.variables["C�lcul del risc en exploraci� podol�gica amb resultat > 1"],
                               "nums": set(self.derivacions_podologia.keys()),
                               "excls": set()},
                    "POD04B": {"dens": (self.problemes["DM2"] - self.problemes["DM1"]) & self.variables["C�lcul del risc en exploraci� podol�gica amb resultat > 1"],
                               "nums": set(self.derivacions_podologia.keys()),
                               "excls": set()},
                    "POD05":  {"dens": (self.problemes["DM1"] | self.problemes["DM2"]) & self.variables["C�lcul del risc en exploraci� podol�gica amb resultat > 1"],
                               "nums_compteig": self.derivacions_podologia,
                               "excls": set()},
                    "POD05A": {"dens": self.problemes["DM1"] & self.variables["C�lcul del risc en exploraci� podol�gica amb resultat > 1"],
                               "nums_compteig": self.derivacions_podologia,
                               "excls": set()},
                    "POD05B": {"dens": (self.problemes["DM2"] - self.problemes["DM1"]) & self.variables["C�lcul del risc en exploraci� podol�gica amb resultat > 1"],
                               "nums_compteig": self.derivacions_podologia,
                               "excls": set()},
                    "POD06":  {"dens": self.problemes["DM1"] | self.problemes["DM2"],
                               "nums": self.variables["C�lcul del risc en exploraci� podol�gica (dos cops)"],
                               "excls": self.problemes["Manca dels dos peus"]},
                    "POD06A": {"dens": self.problemes["DM1"],
                               "nums": self.variables["C�lcul del risc en exploraci� podol�gica (dos cops)"],
                               "excls": self.problemes["Manca dels dos peus"]},
                    "POD06AA":{"dens": self.problemes["DM1"],
                               "nums": self.variables["C�lcul del risc en exploraci� podol�gica (VALOR DESCENDENT)"],
                               "excls": self.problemes["Manca dels dos peus"]},
                    "POD06AB":{"dens": self.problemes["DM1"],
                               "nums": self.variables["C�lcul del risc en exploraci� podol�gica (VALOR ASCENDENT)"],
                               "excls": self.problemes["Manca dels dos peus"]},
                    "POD06B": {"dens": (self.problemes["DM2"] - self.problemes["DM1"]),
                               "nums": self.variables["C�lcul del risc en exploraci� podol�gica (dos cops)"],
                               "excls": self.problemes["Manca dels dos peus"]},
                    "POD06BA":{"dens": (self.problemes["DM2"] - self.problemes["DM1"]),
                               "nums": self.variables["C�lcul del risc en exploraci� podol�gica (VALOR DESCENDENT)"],
                               "excls": self.problemes["Manca dels dos peus"]},
                    "POD06BB":{"dens": (self.problemes["DM2"] - self.problemes["DM1"]),
                               "nums": self.variables["C�lcul del risc en exploraci� podol�gica (VALOR ASCENDENT)"],
                               "excls": self.problemes["Manca dels dos peus"]}                               
                    }

        sub_ind_valors = (("A", 1), ("B", 2), ("C", 3), ("D", 4), ("E", 5), ("F", 6))

        for ind in cond_ind:
            for id_cip_sec in cond_ind[ind]["dens"]:
                if "nums" in cond_ind[ind]:
                    num = 1 if id_cip_sec in cond_ind[ind]["nums"] else 0
                elif "nums_compteig" in cond_ind[ind]:
                    num = len(cond_ind[ind]["nums_compteig"][id_cip_sec])
                excl = 1 if id_cip_sec in cond_ind[ind]["excls"] else 0
                up = self.poblacio[id_cip_sec]["up"]
                br = self.centres[up]
                edat = self.poblacio[id_cip_sec]["edat"]
                edat_lv = u.ageConverter(edat)
                sexe = self.poblacio[id_cip_sec]["sexe"]
                sexe_lv = u.sexConverter(sexe)
                if not excl:
                    self.resultats[(ind, br, edat_lv, sexe_lv, "DEN")] += 1
                    self.resultats[(ind, br, edat_lv, sexe_lv, "NUM")] += num
                    if num and ind in ("POD01", "POD02", "POD03"):
                        for sub_ind, valor in sub_ind_valors:
                            self.resultats[(ind+sub_ind, br, edat_lv, sexe_lv, "DEN")] += num
                            sub_num = 1 if id_cip_sec in self.variables_valors["C�lcul del risc en exploraci� podol�gica"][valor] else 0
                            self.resultats[(ind+sub_ind, br, edat_lv, sexe_lv, "NUM")] += sub_num



    def exportar(self):
        """Exporta els a la taula definida a la variable "tb", despr�s exporta les dades de la taula a LV."""

        self.resultats = [[ind, br, edat, sexe, analisis, n] for (ind, br, edat, sexe, analisis), n in self.resultats.items()]
        u.createTable(tb, "(indicador varchar(12), br varchar(5), edat varchar(10), sexe varchar(4), analisis varchar(3), n int)", db, rm=True)
        u.listToTable(self.resultats, tb, db)

        sql = """
                SELECT
                    indicador,
                    "Aperiodo",
                    br,
                    analisis,
                    edat,
                    "NOIMP",
                    sexe,
                    "N",
                    n
                FROM
                    {}.{}
              """.format(db, tb)
        u.exportKhalix(sql, file)


if u.IS_MENSUAL:
    Podologia()