import sisapUtils as u
import datetime as d
import collections as c
from dateutil import relativedelta as rd

u.printTime('inici')
DEXTD = u.getOne("select data_ext from dextraccio", "nodrizas")[0]
menys45 = DEXTD - rd.relativedelta(days=45)
menys52 = DEXTD - rd.relativedelta(days=52)
menys174 = DEXTD - rd.relativedelta(days=174)
menys30 = DEXTD - rd.relativedelta(days=30)
menys365 = DEXTD - rd.relativedelta(days=365)
sql = """
    SELECT id_cip_sec, hash_d
    FROM u11
"""
idcip_hash = {}
for id, hash in u.getAll(sql, 'import'):
    idcip_hash[id] = hash
u.printTime('u11')

sql = """
    SELECT cip, hash_redics, hash_covid
    FROM dwsisap.pdptb101_relacio_nia
"""
cip_covid = {}
redics_covid = {}
for cip, redics, covid in u.getAll(sql, 'exadata'):
    cip_covid[cip] = covid
    redics_covid[redics] = covid
u.printTime('pdptb101')

sql = "select scs_codi, aga, gap_codi from cat_centres"
centres = {up: (aga, gap) for up, aga, gap in u.getAll(sql, 'nodrizas')}
u.printTime('centres')

sql = """
    SELECT cip, id_it, tipus_it, data_baixa, up
    FROM dwsisap.master_it
    WHERE tipus_it in ('AD', 'NA')
    AND data_baixa >= DATE '{menys365}'
    AND data_baixa <= DATE '{menys45}'
    AND data_alta is null
""".format(menys365=menys365, menys45=menys45)

its = {}
for cip, id, tipus, data, up in u.getAll(sql, 'exadata'):
    try:
        its[id] = (cip_covid[cip], tipus, data, up)
    except KeyError:
        pass
u.printTime('its')

# agafem codi del problema de salut relacionat amb la IT
sql = """
    SELECT eit_nsre, dgn_cdi_darrer
    FROM bislt_pro_h.ft_icam_sictbcei
    WHERE dgn_cdi_darrer like 'M%'
    AND cei_dcomu_b_trunc >= DATE '{menys365}'
""".format(menys365=menys365)
its_in = set()
exclusions_gen = ['M04', 'M05', 'M06', 'M07', 'M08', 'M09', 'M10', 'M11', 'M12', 'M13', 'M14', 'M30',
                'M31', 'M32', 'M33', 'M34', 'M35', 'M36', 'M45', 'M46', 'M47', 'M48', 'M49', 'M80', 'M84']
for id, dgn in u.getAll(sql, 'exadata'):
    if dgn not in ('M79.7', 'M79.2', 'M54.81') and dgn[:3] not in exclusions_gen:
        its_in.add(id)
u.printTime('dgns')

sql = """
    SELECT id_cip_sec, inici
    FROM ass_embaras
    WHERE inici < DATE '{menys174}'
""".format(menys174=menys174)
excl_embaras = set()
for id, inici in u.getAll(sql, 'nodrizas'):
    try:
        excl_embaras.add(redics_covid[idcip_hash[id]])
    except KeyError:
        pass
u.printTime('embaras')

sql = """
    SELECT id_cip_sec
    FROM assignada_tot
    WHERE edat > 62
"""
excl_edat = set()
for id, in u.getAll(sql, 'nodrizas'):
    try:
        excl_edat.add(redics_covid[idcip_hash[id]])
    except KeyError:
        pass
u.printTime('assignada')

sql = """
    SELECT hash
    FROM dwsisap.master_derivacions
    WHERE inf_servei_d_codi in ('10150', '63104', 'CST04')
    AND oc_data <= DATE '{menys365}'
""".format(menys365=menys365)
excl_dolor = set()
for hash, in u.getAll(sql, 'exadata'):
    excl_dolor.add(hash)
u.printTime('derivacions')

sql = """
    SELECT hash
    FROM dwsisap.master_proves
    WHERE der_data_prog >= DATE '{menys30}'
""".format(menys30=menys30)
excl_proves = set()
for hash, in u.getAll(sql, 'exadata'):
    excl_proves.add(hash)
u.printTime('proves')

resultat = c.Counter()
upload = []
for id_it in its_in:
    if id_it in its:
        (hash, tipus, baixa, up) = its[id_it]
        if hash not in excl_embaras and hash not in excl_edat and hash not in excl_dolor and hash not in excl_proves:
            if up in centres:
                (aga, gap) = centres[up]
                resultat[(up, aga, gap, tipus)] += 1
                upload.append((id_it, hash, tipus, baixa, up, aga, gap))
u.printTime('process')

cols = [('id_it', 'hash_covid', 'tipus_it', 'data_baixa', 'up', 'aga', 'gap')]
u.writeCSV('./llistat_peticio_utit_365.csv', cols+upload, sep=',')
u.printTime('llistat')

cols = [('up', 'aga', 'gap', 'tipus_it', 'total')]
upload_count = []
for (up, aga, gap, tipus), val in resultat.items():
    upload_count.append((up, aga, gap, tipus, val))
u.writeCSV('./res_peticio_utit_365.csv', cols+upload_count, sep=',')
u.printTime('fi')

