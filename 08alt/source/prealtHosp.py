# coding: latin1

"""
.
"""

import collections as c

import sisapUtils as u


dades = c.defaultdict(lambda: c.Counter())
sql = "select enll_codi_up, enll_desc_up, enll_desc_origen, \
              enll_data_alt, enll_exclos \
       from prealt1, nodrizas.dextraccio \
       where enll_data_alt between 20050101 and data_ext and \
             enll_tipus_origen <> 'D'"
for up, desc, desc_back, dat, exclos in u.getAll(sql, "import"):
    hospital = desc if desc else desc_back
    key = (up, hospital, dat.year, dat.month)
    dades[key]["sol"] += 1
    if exclos == 'N' or exclos == '' or exclos is None:
        dades[key]["incl"] += 1

if u.IS_MENSUAL:
    tb = "SISAP_PREALT"
    db = "redics"
    user = "PREDURAP"
    u.createTable(tb, "(up varchar(5), hospital varchar2(255), dat_any int, \
                        dat_mes int, inclosos int, solicitats int)",
                  db, rm=True)
    data = [key + (count["incl"], count["sol"])
            for (key, count) in dades.items()]
    u.listToTable(data, tb, db)
    u.execute("grant select on {} to {}".format(tb, user), db)
    text = "Hem actualitzat la taula PREDUFFA.{} a REDICS.".format(tb)
    u.sendPolite('gervasi.melsio@gmail.com', 'Prealt', text)
