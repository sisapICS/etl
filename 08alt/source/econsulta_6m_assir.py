# -*- coding: utf8 -*-

"""
en població ASSIR:
    eConsulta 6 mesos
cambios:
    centres: {up_assir: br_assir}
    poblacion: {cip: set([br_assir, ...)]}
    professionals_assir: set([dni, ...])
    converses: {[sector, id_conversa]: {atributos: valor},}
        el atributo 'msg' contiene las respuestas y su fechas
        filtra por cip de {poblacio} y dni de {professionals_assir}
"""

import collections as c
import datetime as d

import sisapUtils as u


TB_BR = 'exp_khalix_up_econs_assir'
TB_UBA = 'exp_khalix_uba_econs_assir'
DB = 'altres'


class EConsulta_6M_ASS(object):
    """."""

    def __init__(self):
        """."""
        self.get_centres()
        self.get_cip_brs()
        self.get_professionals_assir()
        self.get_converses()
        self.get_missatges()
        self.get_econs0008()
        self.export_br()

    @staticmethod
    def conta_dies(inici, final):
        """."""
        total = (final - inici).days
        laborables = 0
        for i in range(total):
            dia = inici + d.timedelta(days=i)
            if u.isWorkingDay(dia):
                laborables += 1
        return laborables

    def get_centres(self):
        """."""
        sql = "SELECT distinct up_assir, br_assir FROM ass_centres"
        self.centres = {up: br for (up, br) in u.getAll(sql, 'nodrizas')}

    def get_cip_brs(self):
        self.cip_brs = c.defaultdict(set)
        sql = "select id_cip_sec, visi_up from ass_imputacio_up"
        for cip, up in u.getAll(sql, 'nodrizas'):
            if up in self.centres:
                br = self.centres[up]
                self.cip_brs[cip].add(br)

    def get_professionals_assir(self):
        self.professionals_assir = set()
        sql = "select visi_dni_prov_resp from ass_imputacio_dni"
        for dni, in u.getAll(sql, 'nodrizas'):
            self.professionals_assir.add(dni)

    def get_converses(self):
        """."""
        db = 'import'
        sql = """
            WITH periode as (
                SELECT
                    data_ext - interval 6 month + interval 1 day as ini,
                    data_ext as fi
                FROM
                    nodrizas.dextraccio)
            SELECT
                id_cip_sec, codi_sector, conv_id, conv_autor,
                conv_desti, conv_estat, conv_dini, CASE WHEN conv_dini > adddate(data_ext, INTERVAL -1 MONTH) THEN 1 ELSE 0 END mensual
            FROM
                econsulta_conv, periode, nodrizas.dextraccio
            WHERE
                conv_dini BETWEEN periode.ini and periode.fi
        """
        self.converses = {}
        for pac, sector, id_cnv, autor, desti, estat, ini, mensual in u.getAll(sql, db):
            if pac in self.cip_brs:
                prof = (autor if autor else desti)
                if prof in self.professionals_assir:
                    entities = [br for br in self.cip_brs[pac]]
                    self.converses[(sector, id_cnv)] = {
                        'pac': pac,
                        'entities': entities,
                        'prof': prof,
                        'inicia': not desti,
                        'estat': estat,
                        'ini': ini,
                        'msg': {},
                        'mensual': mensual
                        }

    def get_missatges(self):
        """."""
        sql = """
            SELECT
                codi_sector,
                msg_conv_id,
                msg_id,
                msg_autor,
                msg_desti,
                msg_data
            FROM
                econsulta_msg
        """
        for sector, id_c, id_m, autor, desti, data in u.getAll(sql, 'import'):
            if (sector, id_c) in self.converses:
                this = {'prof': autor if autor else desti,
                        'inicia': not desti,
                        'data': data}
                self.converses[(sector, id_c)]['msg'][id_m] = this

    def get_econs0008(self):
        """."""
        ind = 'ECONS0008'
        den = c.defaultdict(c.Counter)
        num = c.defaultdict(c.Counter)
        self.resultat = []

        for conversa in self.converses.values():
            if conversa["estat"] == "T" and not conversa["inicia"] and 1 in conversa["msg"]:
                for entitat in conversa["entities"]:
                    den["ANUAL"][entitat] += 1   
                    if conversa["mensual"]:
                        den["MENSUAL"][entitat] += 1
                    pregunta = conversa["msg"][1]["data"]
                    resposta = None
                    for msg_id in range(2, 1000):
                        if msg_id in conversa["msg"]:
                            if conversa["msg"][msg_id]["inicia"]:
                                resposta = conversa["msg"][msg_id]["data"]
                                break
                        else:
                            break
                    if resposta:
                        dies = self.conta_dies(pregunta, resposta)
                        num["ANUAL"][entitat] += dies
                        if conversa["mensual"]:
                            num["MENSUAL"][entitat] += dies
        for periode in ("MENSUAL", "ANUAL"):
            for entitat, n in den[periode].items():
                self.resultat.append((ind, entitat, "DEN", periode, n))
                self.resultat.append((ind, entitat, "NUM", periode, num[periode][entitat]))

    @staticmethod
    def conta_dies(inici, final):
        """."""
        total = (final - inici).days
        laborables = 0
        for i in range(total):
            dia = inici + d.timedelta(days=i)
            if u.isWorkingDay(dia):
                laborables += 1
        if laborables > 30:
            laborables = 30
        return laborables

    def export_br(self):
        """."""
        file_name = 'ECONSULTA_6M_ASSIR'

        cols = "(indicador varchar(12), br varchar(5), analisis varchar(10), periode varchar(7), n int)"  # noqa
        rows = [row for row in self.resultat]
        u.createTable(TB_BR, cols, DB, rm=True)
        u.listToTable(rows, TB_BR, DB)
        sql = """
            SELECT
                indicador, 'Aperiodo',
                br, analisis, CASE WHEN periode = 'ANUAL' THEN 'NOCAT' ELSE periode END 'NOCAT', 'NOIMP', 'DIM6SET', 'N', n
            FROM {}.{}
            """.format(DB, TB_BR)
        u.exportKhalix(sql, file_name)


if __name__ == '__main__':
    EConsulta_6M_ASS()
