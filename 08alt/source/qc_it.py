# -*- coding: latin1 -*-

import sisapUtils as u
import collections as c
import numpy as np

import datetime


class QC_ITs():
    def __init__(self):
        cols ='(indicador varchar(20), periode varchar(10), up varchar(10), uba varchar(10), analisi varchar(10), n int)'
        u.createTable('qc_its', cols, 'altres', rm=True)
        self.dext_mensual_i, self.dext_mensual_t, self.dext_anual  = u.getOne('select date_add(data_ext,interval - 1 month), data_ext, date_add(data_ext,interval - 1 year) from nodrizas.dextraccio', 'nodrizas')
        self.dext_mensual_i = datetime.datetime(year = self.dext_mensual_i.year, month=self.dext_mensual_i.month, day=self.dext_mensual_i.day)
        self.dext_mensual_t = datetime.datetime(year = self.dext_mensual_t.year, month=self.dext_mensual_t.month, day=self.dext_mensual_t.day)
        self.dext_anual = datetime.datetime(year = self.dext_anual.year, month=self.dext_anual.month, day=self.dext_anual.day)
        self.get_data()
        self.calcul_indicadors()
        self.uploading()
    
    def get_data(self):
        self.data = {}
        self.cooking = c.Counter()
        mes =  str(self.dext_mensual_t.month) if len(str(self.dext_mensual_t.month)) == 2 else '0' + str(self.dext_mensual_t.month)
        dia =  str(self.dext_mensual_t.day) if len(str(self.dext_mensual_t.day)) == 2 else '0' + str(self.dext_mensual_t.day)
        dext_mensual_t = str(self.dext_mensual_t.year) + '-' + mes + '-' + dia
        self.altes_icam = c.defaultdict(set)
        sql = """SELECT id_it, cip, up, uba, edat_baixa, 
                    motiu_baixa, data_baixa, 
                    CASE WHEN data_alta IS NULL THEN DATE '{dext}' ELSE data_alta END data_alta,
                    up_alta, up_baixa, 
                    CASE WHEN num_dies IS NULL THEN DATE '{dext}' - data_baixa ELSE num_dies END num_dies,
                    visites_presencials, VISITES_NO_PRESENCIALS, actiu, data_exitus,
                    data_enviament_ecap, trunc(data_resposta), dies, 
                    osteomuscular, salut_mental, trauma, signes, altres, covid,
                    visita_baixa, data_comunicacio, proposta_alta, situacio
                    FROM dwsisap.master_it
                    where (data_alta >= date '{dext}' - 365 or data_alta is null)
                    and motiu_baixa = 'C'""".format(dext=dext_mensual_t)
        print(sql)
        for (id_it, cip, up, uba, edat_baixa, 
                    motiu_baixa, data_baixa, data_alta, up_alta, up_baixa, num_dies,
                    visites_presencials, VISITES_NO_PRESENCIALS, actiu, data_exitus,
                    data_enviament_ecap, data_resposta, dies, 
                    osteomuscular, salut_mental, trauma, signes, altres, covid,
                    visita_baixa, data_comunicacio, proposta_alta, situacio) in u.getAll(sql, 'exadata'):
            self.data[id_it] = [cip, up, uba, edat_baixa, 
                    motiu_baixa, data_baixa, data_alta, up_alta, up_baixa, num_dies,
                    visites_presencials, VISITES_NO_PRESENCIALS, actiu, data_exitus,
                    data_enviament_ecap, data_resposta, dies, 
                    osteomuscular, salut_mental, trauma, signes, altres, covid,
                    visita_baixa, data_comunicacio, proposta_alta, situacio]
            if up_alta in ('01064', '07858', '07859', '07860'):
                self.altes_icam[cip].add((data_alta, osteomuscular, salut_mental, trauma, altres, covid))
                if data_alta > self.dext_anual:
                    self.cooking['QCIT004', 'B', up, uba, 'DEN'] += 1
                if data_alta > self.dext_mensual_i:
                    self.cooking['QCIT004', 'A', up, uba, 'DEN'] += 1
        # actius
        print('actius')
        sql = """SELECT B.ECAP_UP, B.ECAP_UBA, COUNT(1)
                    FROM dwcatsalut.rca_pacients A, DWSISAP.DBC_POBLACIO B
                    WHERE A.ASS_NIA = B.NIA AND A.afiliacio_taf = 'AC' AND  A.afiliacio_raec = 'T'
                    AND A.ASS_CSA = 'A' and b.edat >= 16
                    GROUP BY B.ECAP_UP, B.ECAP_UBA"""
        for up, uba, n in u.getAll(sql, 'exadata'):
            for ind in ('QCIT002_OST', 'QCIT002_SM', 'QCIT002_TRAU', 'QCIT002_SG', 'QCIT002_ALT', 'QCIT003_OST', 'QCIT003_SM', 'QCIT003_TRAU', 'QCIT003_SG', 'QCIT003_ALT', 'QCIT002', 'QCIT003'):
                for p in ('A', 'B'):
                    self.cooking[ind, p, up, uba, 'DEN'] += n
        sql = 'select scs_codi from nodrizas.cat_centres'
        self.primaria = set()
        for up, in u.getAll(sql, 'nodrizas'):
            self.primaria.add(up)
        

    def calcul_indicadors(self):
        quants = 0
        for it, (cip, up, uba, edat_baixa, 
                    motiu_baixa, data_baixa, data_alta, up_alta, up_baixa, num_dies,
                    visites_presencials, VISITES_NO_PRESENCIALS, actiu, data_exitus,
                    data_enviament_ecap, data_resposta, dies, 
                    osteomuscular, salut_mental, trauma, signes, altres, covid,
                    visita_baixa, data_comunicacio, proposta_alta, situacio) in self.data.items():
            # anuals
            self.cooking['QCIT001', 'B', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
            self.cooking['QCIT001', 'B', up, uba, 'DEN'] += 1
            self.cooking['QCIT002', 'B', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
            if data_baixa > self.dext_anual:
                self.cooking['QCIT003', 'B', up, uba, 'NUM'] += 1
            if osteomuscular == 1:
                self.cooking['QCIT001_OST', 'B', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                self.cooking['QCIT001_OST', 'B', up, uba, 'DEN'] += 1
                self.cooking['QCIT002_OST', 'B', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                if data_baixa > self.dext_anual:
                    self.cooking['QCIT003_OST', 'B', up, uba, 'NUM'] += 1
            elif salut_mental == 1:
                self.cooking['QCIT001_SM', 'B', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                self.cooking['QCIT001_SM', 'B', up, uba, 'DEN'] += 1
                self.cooking['QCIT002_SM', 'B', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                if data_baixa > self.dext_anual:
                    self.cooking['QCIT003_SM', 'B', up, uba, 'NUM'] += 1
            elif trauma == 1:
                self.cooking['QCIT001_TRAU', 'B', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                self.cooking['QCIT001_TRAU', 'B', up, uba, 'DEN'] += 1
                self.cooking['QCIT002_TRAU', 'B', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                if data_baixa > self.dext_anual:
                    self.cooking['QCIT003_TRAU', 'B', up, uba, 'NUM'] += 1
            elif signes == 1:
                self.cooking['QCIT001_SG', 'B', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                self.cooking['QCIT001_SG', 'B', up, uba, 'DEN'] += 1
                self.cooking['QCIT002_SG', 'B', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                if data_baixa > self.dext_anual:
                    self.cooking['QCIT003_SG', 'B', up, uba, 'NUM'] += 1
            elif altres == 1:
                self.cooking['QCIT001_ALT', 'B', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                self.cooking['QCIT001_ALT', 'B', up, uba, 'DEN'] += 1
                self.cooking['QCIT002_ALT', 'B', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                if data_baixa > self.dext_anual:
                    self.cooking['QCIT003_ALT', 'B', up, uba, 'NUM'] += 1
            # QCIT004
            if cip in self.altes_icam:
                for data_alta_icam, osteomuscular_icam, salut_mental_icam, trauma_icam, altres_icam, covid_icam in self.altes_icam[cip]:
                    if 0 <= (data_baixa - data_alta_icam).days <= 7:
                        if osteomuscular_icam == osteomuscular and salut_mental_icam == salut_mental and trauma_icam == trauma and altres_icam == altres and covid_icam == covid:
                            self.cooking['QCIT004', 'B', up, uba, 'NUM'] += 1
            # mensuals
            if data_alta >= self.dext_mensual_i:
                self.cooking['QCIT001', 'A', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                self.cooking['QCIT001', 'A', up, uba, 'DEN'] += 1
                self.cooking['QCIT002', 'A', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                if data_baixa > self.dext_mensual_i:
                    self.cooking['QCIT003', 'A', up, uba, 'NUM'] += 1      
                if osteomuscular == 1:
                    self.cooking['QCIT001_OST', 'A', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                    self.cooking['QCIT001_OST', 'A', up, uba, 'DEN'] += 1
                    self.cooking['QCIT002_OST', 'A', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                    if data_baixa > self.dext_mensual_i:
                        self.cooking['QCIT003_OST', 'A', up, uba, 'NUM'] += 1
                elif salut_mental == 1:
                    self.cooking['QCIT001_SM', 'A', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                    self.cooking['QCIT001_SM', 'A', up, uba, 'DEN'] += 1
                    self.cooking['QCIT002_SM', 'A', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                    if data_baixa > self.dext_mensual_i:
                        self.cooking['QCIT003_SM', 'A', up, uba, 'NUM'] += 1
                elif trauma == 1:
                    self.cooking['QCIT001_TRAU', 'A', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                    self.cooking['QCIT001_TRAU', 'A', up, uba, 'DEN'] += 1
                    self.cooking['QCIT002_TRAU', 'A', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                    if data_baixa > self.dext_mensual_i:
                        self.cooking['QCIT003_TRAU', 'A', up, uba, 'NUM'] += 1
                elif signes == 1:
                    self.cooking['QCIT001_SG', 'A', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                    self.cooking['QCIT001_SG', 'A', up, uba, 'DEN'] += 1
                    self.cooking['QCIT002_SG', 'A', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                    if data_baixa > self.dext_mensual_i:
                        self.cooking['QCIT003_SG', 'A', up, uba, 'NUM'] += 1
                elif altres == 1:
                    self.cooking['QCIT001_ALT', 'A', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                    self.cooking['QCIT001_ALT', 'A', up, uba, 'DEN'] += 1
                    self.cooking['QCIT002_ALT', 'A', up, uba, 'NUM'] += num_dies if num_dies <= 545 else 545
                    if data_baixa > self.dext_mensual_i:
                        self.cooking['QCIT003_ALT', 'A', up, uba, 'NUM'] += 1
                # QCIT004
                if cip in self.altes_icam:
                    for data_alta_icam, osteomuscular_icam, salut_mental_icam, trauma_icam, altres_icam, covid_icam in self.altes_icam[cip]:
                        if 0 <= (data_baixa - data_alta_icam).days <= 7:
                            if osteomuscular_icam == osteomuscular and salut_mental_icam == salut_mental and trauma_icam == trauma and altres_icam == altres and covid_icam == covid:
                                self.cooking['QCIT004', 'A', up, uba, 'NUM'] += 1

            # # SGAM
            # if not data_exitus and covid == 0 and num_dies <= 545: # refer
            #     quants += 1
            #     # anual
            #     if (self.dext_anual - data_baixa).days <= 366:
            #         if (self.dext_mensual_t - data_baixa).days >= 366:
            #             if self.dext_anual <= data_alta and data_alta >= self.dext_mensual_t:
            #                 if (data_alta - data_baixa).days >= 366 and (data_alta - data_baixa).days < 480:
            #                     self.cooking['QCIT365', 'B', up, uba, 'DEN'] += 1
            #                     self.cooking['QCIT365A', 'B', up, uba, 'DEN'] += 1
            #                     # self.cooking['QCIT365casa', 'B', up, uba, 'DEN'] += 1
            #                     if data_comunicacio:
            #                         if self.dext_anual <= data_comunicacio <= self.dext_mensual_t and 366 <= (data_comunicacio - data_baixa).days <= 480:
            #                             if situacio != 'A':
            #                                 self.cooking['QCIT365', 'B', up, uba, 'NUM'] += 1
            #                                 if proposta_alta:
            #                                     self.cooking['QCIT365A', 'B', up, uba, 'NUM'] += 1
            #     elif (self.dext_anual - data_baixa).days <= 480:
            #         if 480 <= (self.dext_mensual_t - data_baixa).days:
            #             if data_alta >= self.dext_mensual_t:
            #                 self.cooking['QCIT365', 'B', up, uba, 'DEN'] += 1
            #                 self.cooking['QCIT365A', 'B', up, uba, 'DEN'] += 1
            #                 # self.cooking['QCIT365casb', 'B', up, uba, 'DEN'] += 1
            #                 if data_comunicacio:
            #                     if self.dext_anual <= data_comunicacio <= self.dext_mensual_t and 366 <= (data_comunicacio - data_baixa).days <= 480:
            #                         if situacio != 'A':
            #                             self.cooking['QCIT365', 'B', up, uba, 'NUM'] += 1
            #                             if proposta_alta:
            #                                 self.cooking['QCIT365A', 'B', up, uba, 'NUM'] += 1
            #     # mensual
            #     if (self.dext_mensual_i - data_baixa).days <= 366: 
            #         if 366 <= (self.dext_mensual_t - data_baixa).days:
            #             if self.dext_mensual_i <= data_alta:
            #                 if data_alta <= self.dext_mensual_t:
            #                     if 366 < (data_alta - data_baixa).days and (data_alta - data_baixa).days < 480:
            #                         self.cooking['QCIT365', 'A', up, uba, 'DEN'] += 1
            #                         self.cooking['QCIT365A', 'A', up, uba, 'DEN'] += 1
            #                         if data_comunicacio:
            #                             if self.dext_mensual_i <= data_comunicacio:
            #                                 if data_comunicacio <= self.dext_mensual_t and 366 <= (data_comunicacio - data_baixa).days and (data_comunicacio - data_baixa).days <= 480:
            #                                     if situacio != 'A':
            #                                         self.cooking['QCIT365', 'A', up, uba, 'NUM'] += 1
            #                                         if proposta_alta:
            #                                             self.cooking['QCIT365A', 'A', up, uba, 'NUM'] += 1
            #     elif (self.dext_mensual_i - data_baixa).days <= 480:
            #         if 480 <= (self.dext_mensual_t - data_baixa).days:
            #             if data_alta >= self.dext_mensual_t:
            #                 self.cooking['QCIT365', 'A', up, uba, 'DEN'] += 1
            #                 self.cooking['QCIT365A', 'A', up, uba, 'DEN'] += 1
            #                 if data_comunicacio:
            #                     if self.dext_mensual_i <= data_comunicacio:
            #                         if data_comunicacio <= self.dext_mensual_t:
            #                             if 366 <= (data_comunicacio - data_baixa).days and (data_comunicacio - data_baixa).days <= 480:
            #                                 if situacio != 'A':
            #                                     self.cooking['QCIT365', 'A', up, uba, 'NUM'] += 1
            #                                     if proposta_alta:
            #                                         self.cooking['QCIT365A', 'A', up, uba, 'NUM'] += 1
            # PAM
            if data_enviament_ecap:
                self.cooking['QCITPAM01', 'B', up, uba, 'DEN'] += 1
                if dies >= 0:
                    dies_laborables = 0
                    inici = data_enviament_ecap
                    while inici <= data_resposta:
                        if u.isWorkingDay(inici):
                            dies_laborables += 1
                        inici = inici + datetime.timedelta(days=1) 
                    if dies_laborables <= 3:
                        self.cooking['QCITPAM01', 'B', up, uba, 'NUM'] += 1
                        self.cooking['QCITPAM02', 'B', up, uba, 'DEN'] += 1
                        if data_resposta:
                            if data_alta == data_resposta or (data_alta - data_resposta).days <= 7:
                                self.cooking['QCITPAM02', 'B', up, uba, 'NUM'] += 1
                if data_enviament_ecap >= self.dext_mensual_i and data_enviament_ecap <= self.dext_mensual_t:
                    self.cooking['QCITPAM01', 'A', up, uba, 'DEN'] += 1
                    if dies >= 0:
                        dies_laborables = 0
                        inici = data_enviament_ecap
                        while inici <= data_resposta:
                            if u.isWorkingDay(inici):
                                dies_laborables += 1
                            inici = inici + datetime.timedelta(days=1) 
                        if dies_laborables <= 3:
                            self.cooking['QCITPAM01', 'A', up, uba, 'NUM'] += 1
                            self.cooking['QCITPAM02', 'A', up, uba, 'DEN'] += 1
                            if data_resposta:
                                if data_alta == data_resposta or (data_alta - data_resposta).days <= 7:
                                    self.cooking['QCITPAM02', 'A', up, uba, 'NUM'] += 1
            # QCITPRO
            if num_dies >= 5:
                if up_alta in self.primaria:
                    if data_baixa >= self.dext_anual:
                        self.cooking['QCITPRO', 'B', up, uba, 'DEN'] += 1
                        if visita_baixa == 1:
                            self.cooking['QCITPRO', 'B', up, uba, 'NUM'] += 1
                    if data_baixa >= self.dext_mensual_i:
                        self.cooking['QCITPRO', 'A', up, uba, 'DEN'] += 1
                        if visita_baixa == 1:
                            self.cooking['QCITPRO', 'A', up, uba, 'NUM'] += 1
        print(quants)
    
    def uploading(self):
        self.upload = [(ind, periode, up, uba, analisi, n) for (ind, periode, up, uba, analisi), n in self.cooking.items()]
        u.listToTable(self.upload, 'qc_its', 'altres')
        upload_sgam = []
        sql = """SELECT ECAP_UP, ECAP_UBA, sum(s.IT_ASSOLIDA) num, count(1) den
                FROM dwsisap.DBC_POBLACIO dp INNER JOIN dwsisap.sgam003 S
                ON DP.NIA = S.NIA 
                WHERE SITUACIO = 'A'
                AND ecap_up IS NOT NULL AND ecap_uba IS NOT null
                GROUP BY ECAP_UP, ECAP_UBA"""
        for up, uba, num, den in u.getAll(sql, 'exadata'):
            upload_sgam.append(('SGAM03_AP', 'B', up, uba, 'NUM', num))
            upload_sgam.append(('SGAM03_AP', 'B', up, uba, 'DEN', den))
        u.listToTable(upload_sgam, 'qc_its', 'altres')


class UPLOAD():
    def __init__(self):
        self.upload_ECAP()
        self.upload_LV()
    
    def upload_ECAP(self):
        # pare, literal, ordre
        cataleg_pare = [('QCIT', 'Indicadors IT', 1),]
        cols = '(indicador varchar(20), descripcio varchar(50), ordre int)'
        u.createTable('exp_ecap_qc_it_catpare', cols, 'altres', rm=True)
        u.listToTable(cataleg_pare, 'exp_ecap_qc_it_catpare', 'altres')
        query = 'select * from altres.exp_ecap_qc_it_catpare'
        table = 'itcatalegpare'
        u.exportPDP(query=query,table=table,datAny=True)
        cataleg = [
                ('QCIT001', 'Durada mitjana de les IT', 1, 'QCIT', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5283/ver/'),
                ('QCIT001_ALT', 'Durada mitjana de les IT (altres)', 1, 'QCIT001', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5370/ver/'),
                ('QCIT001_OST', 'Durada mitjana de les IT (osteomuscular)', 1, 'QCIT001', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5371/ver/'),
                ('QCIT001_SG', 'Durada mitjana de les IT (signes diversos)', 1, 'QCIT001', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5372/ver/'),
                ('QCIT001_SM', 'Durada mitjana de les IT (Salut Mental)', 1, 'QCIT001', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5373/ver/'),
                ('QCIT001_TRAU', 'Durada mitjana de les IT (Traumatologia)', 1, 'QCIT001', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5374/ver/'),
                ('QCIT002', 'Dies d\'IT per poblaci� activa', 2, 'QCIT', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5284/ver/'),
                ('QCIT002_ALT', 'Dies d\'IT per poblaci� activa (altres)', 2, 'QCIT002', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5375/ver/'),
                ('QCIT002_OST', 'Dies d\'IT per poblaci� activa (osteomuscular)', 2, 'QCIT002', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5376/ver/'),
                ('QCIT002_SG', 'Dies d\'IT per poblaci� activa (signes diversos)', 2, 'QCIT002', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5377/ver/'),
                ('QCIT002_SM', 'Dies d\'IT per poblaci� activa (Salut Mental)', 2, 'QCIT002', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5378/ver/'),
                ('QCIT002_TRAU', 'Dies d\'IT per poblaci� activa (Traumatologia)', 2, 'QCIT002', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5379/ver/'),
                ('QCIT003', 'Incid�ncia d\'IT per poblaci� activa', 3, 'QCIT', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5289/ver/'),
                ('QCIT003_ALT', 'Incid�ncia d\'IT per poblaci� activa (altres)', 3, 'QCIT003', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5380/ver/'),
                ('QCIT003_OST', 'Incid�ncia d\'IT per poblaci� activa (osteomuscular)', 3, 'QCIT003', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5381/ver/'),
                ('QCIT003_SG', 'Incid�ncia d\'IT per poblaci� activa (signes diversos)', 3, 'QCIT003', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5382/ver/'),
                ('QCIT003_SM', 'Incid�ncia d\'IT per poblaci� activa (Salut Mental)', 3, 'QCIT003', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5383/ver/'),
                ('QCIT003_TRAU', 'Incid�ncia d\'IT per poblaci� activa (Traumatologia)', 3, 'QCIT003', 0, 1, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5384/ver/'),
                ('QCIT004', 'Percentatge de pacients amb baixa despr�s de l\'alta de l\'ICAM', 4, 'QCIT', 0, 1, 0, 0, 0, 1, 'http://10.80.217.201/sisap-umi/indicador/indicador/5870/ver/'),
                # ('QCIT365', 'Revisió de les IT >365 dies i comunicació del resultat a SGAM per IS3 abans dels 480 dies', 4, 'QCIT', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5285/ver/'),
                # ('QCIT365A', 'Revisió de les IT >365 dies i comunicació del resultat a SGAM per IS3 abans dels 480 dies amb proposta d\'alta', 5, 'QCIT', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5287/ver/'),
                ('QCITPAM01', 'Percentatge de PAM contestades en 3 dies', 6, 'QCIT', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5270/ver/'),
                ('QCITPAM02', 'Percentatge de PAM contestades en 3 dies i amb alta del pacient', 7, 'QCIT', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5271/ver/'),
                ('QCITPRO', 'Percentatge de pacients amb una baixa(IT) > 5 dies que tenen almenys una visita amb data de sol�licitud igual a la data de baixa', 8, 'QCIT', 0, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5288/ver/'),
                ('SGAM03_AP', 'Revisi� de les IT >365 dies i comunicaci� del resultat a SGAM', 9, 'QCIT', 1, 0, 0, 0, 0, 1, 'http://sisap-umi.eines.portalics/indicador/indicador/5133/ver/')
            ]
        cols = '(indicador varchar(20), literal varchar(300), ordre int, pare varchar(20), llistat int, invers int, mmin int, mmint int, mmax int, toshow int, wiki varchar(500))'
        u.createTable('exp_ecap_qc_it_cat', cols, 'altres', rm=True)
        u.listToTable(cataleg, 'exp_ecap_qc_it_cat', 'altres')
        query = 'select * from altres.exp_ecap_qc_it_cat'
        table = 'itcataleg'
        u.exportPDP(query=query,table=table,datAny=True)
        data = c.defaultdict(dict)
        sql = """select indicador, up, uba, analisi, n
                from altres.qc_its
                where periode = 'B'
                and up != ''"""
        for indicador, up, uba, analisi, n in u.getAll(sql, 'altres'):
            data[(indicador, up, uba)][analisi] = n
        computing_metes = c.defaultdict(list)
        for indicador, up, uba in data:
            if 'DEN' in data[(indicador, up, uba)]:
                den = data[(indicador, up, uba)]['DEN']
            if 'NUM' in data[(indicador, up, uba)]:
                num = data[(indicador, up, uba)]['NUM']
            else:
                num = 0
            res = num/float(den) if indicador[0:7] in ('QCIT001', 'QCIT002') else num/float(den)*100
            computing_metes[(up, indicador)].append((uba, num, den, res))
            
        upload = []
        for (up, indicador) in computing_metes:
            computing = []
            for (uba, num, den, res) in computing_metes[(up, indicador)]:
                computing.append(res)
            p20 = np.percentile(computing, 20)
            p80 = np.percentile(computing, 80)
            for (uba, num, den, res) in computing_metes[(up, indicador)]:
                if indicador in ('QCIT001', 'QCIT002', 'QCIT003', 'QCIT365', 'QCIT365A', 'QCITPRO'):    
                    upload.append((up, uba, indicador, num, den, res, p20, p80))
                elif indicador == 'QCITPAM02':
                    upload.append((up, uba, indicador, num, den, res, 0, 0))
                elif indicador == 'QCITPAM01':
                    upload.append((up, uba, indicador, num, den, res, 25, 35.82))
                else:
                    upload.append((up, uba, indicador, num, den, res, 0, 0))

        cols = '(up varchar(5), uba varchar(5), indicador varchar(20), num int, den int, res double, mmin double, mmax double)'
        u.createTable('exp_ecap_qc_it', cols, 'altres', rm=True)
        u.listToTable(upload, 'exp_ecap_qc_it', 'altres')
        query = 'select * from altres.exp_ecap_qc_it'
        table = 'itindicadors'
        u.exportPDP(query=query,table=table,dat=True)

        sql = """SELECT ECAP_UP, ECAP_UBA, 'M', 'SGAM03_AP', HASH_REDICS, ECAP_SECTOR, 0 
            FROM dwsisap.DBC_POBLACIO dp INNER JOIN dwsisap.sgam003 S
            ON DP.NIA = S.NIA 
            INNER JOIN dwsisap.PDPTB101_RELACIO rel 
            ON dp.hash = rel.HASH_COVID 
            WHERE SITUACIO = 'A'
            AND ecap_up IS NOT NULL AND ecap_uba IS NOT NULL
            AND it_assolida = 0 """
        upload = [row for row in u.getAll(sql, 'exadata')]
        cols = """(up varchar(5),
                    uba varchar(5),
                    tipus varchar(1), 
                    indicador varchar(20),
                    hash varchar(40), 
                    sector varchar(5),
                    exclos int)"""
        u.createTable('exp_ecap_qc_it_pac', cols, 'altres', rm=True)
        u.listToTable(upload, 'exp_ecap_qc_it_pac', 'altres')
        query = 'select * from altres.exp_ecap_qc_it_pac'
        table = 'itindicadorsllistat'
        u.exportPDP(query=query,table=table,truncate=True)
        
    def upload_LV(self):
        dext_mensual_t,  = u.getOne('select data_ext from nodrizas.dextraccio', 'nodrizas')
        mes = str(dext_mensual_t.month) if len(str(dext_mensual_t.month)) == 2 else '0'+str(dext_mensual_t.month)
        any = str(dext_mensual_t.year)[2:]
        periode = any+mes
        sql = """
            select
                INDICADOR,
                CONCAT(a.periode, {}),
                CONCAT(ics_codi, 'M', uba),
                ANALISI,
                'NOCAT',
                'NOIMP',
                'DIM6SET',
                'N',
                N
            from
                altres.qc_its a
            inner join NODRIZAS.cat_centres b
                            on a.up = scs_codi
            """.format(periode)
        file = "QCIT_UBA"
        u.exportKhalix(sql, file)
        sql = """
            select
                INDICADOR,
                CONCAT(a.periode, {}),
                ics_codi,
                ANALISI,
                'NOCAT',
                'NOIMP',
                'DIM6SET',
                'N',
                sum(N)
            from
                altres.qc_its a
            inner join NODRIZAS.cat_centres b
                                        on
                a.up = scs_codi
            group by
                INDICADOR,
                A.periode,
                ics_codi,
                analisi
            """.format(periode)
        file = "QCIT"
        u.exportKhalix(sql, file)

if __name__ == "__main__":
    QC_ITs()
    UPLOAD()
