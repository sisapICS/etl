# coding: latin1

"""
.
"""

import collections as c
import sisapUtils as u
import sisaptools as t
import dateutil.relativedelta
from datetime import date
import pandas as pd


def centraletes_hores():
    cols = """(indicador varchar(10), 
            centre varchar(50), up varchar(5), centre_local varchar(20),
            sector varchar(10), organitzacio varchar(50), analisi varchar(5), dia varchar(10), 
            hora varchar(10), control varchar(10), value decimal(15,2))"""
    u.createTable('centraletes', cols, 'altres', rm = True)

    sql = """select month(date_add(sysdate(),interval - 15 day)), 
                year(date_add(sysdate(),interval - 15 day))  
                from nodrizas.dextraccio"""
    for mes, any in u.getAll(sql, 'nodrizas'):
        m_ext, y_ext = mes, any

    info_centres = {}
    sql = """SELECT sede, up, concat(concat(centre, '/'), classe) centre_classe, sector, organizacion
                    FROM DWMN_DIM.CALL_CENTER_CATALOG_UNIF 
                    WHERE baja IS NULL
                    AND up IS NOT null"""
    for seu, up, centre, sector, organitzacio in u.getAll(sql, 'exadata'):
        info_centres[up] = [seu, centre, sector, organitzacio]

    # CENTLF01, 03
    CENTLF = c.Counter()
    CENTLF0506 =c.Counter()
    CENTLF01_den = {}
    CENTLF04 = c.defaultdict(set)
    CENTLF07_den = c.Counter()
    CENTLF07_num = c.defaultdict(c.Counter)
    CENTLF07_num_ll = c.defaultdict(set)
    sql = """SELECT up, to_char(hora_inicio, 'day'), extract(HOUR FROM CAST(hora_inicio AS timestamp)),
            RESULTADO_DESVIO, desvio, gestion, LLAMANTE, extract(day FROM CAST(hora_inicio AS timestamp))
            FROM DWMN_FACT.CALL_CENTER t, DWMN_DIM.CALL_CENTER_CATALOG_UNIF  c
            WHERE t.TELEFONO = c.TLF_RI_PRINCIPAL
            AND c.baja IS NULL
            AND c.up IS NOT NULL
            AND RESULTADO_DESVIO != 'No Aplicable'
            AND extract(HOUR FROM CAST(hora_inicio AS timestamp)) BETWEEN 8 AND 19
            AND to_char(hora_inicio, 'd') NOT IN ('1','7')
            AND extract(MONTH FROM hora_inicio) = {} AND extract(YEAR FROM HORA_INICIO) = {}""".format(m_ext, y_ext)
    for up, day, hour, res_desvio, desvio, gestion, llamante, dia_mes in u.getAll(sql, 'exadata'):
        for ind in ('CENTLF03','CENTLF05'):
            CENTLF[(ind, up, day, hour, 'DEN')] += 1 # ok
        # FER PER CENTLF04
        CENTLF07_num[(ind, up, dia_mes)][llamante] += 1 # ok
        CENTLF04[('CENTLF04', up, dia_mes, 'DEN')].add(llamante)  # ok 
        CENTLF[('CENTLF01', up, day, hour, 'NUM')] += 1 # ok
        CENTLF01_den[('CENTLF01', up, day, hour, 'DEN')] = 1  # ok
        if res_desvio == 'Contestada':
            CENTLF[('CENTLF03', up, day, hour, 'NUM')] += 1 # ok
            CENTLF07_den[('CENTLF07', up, 'DEN')] += 1
            CENTLF07_num_ll[(ind, up, dia_mes)].add(llamante)
            CENTLF04[('CENTLF04', up, dia_mes, 'NUM')].add(llamante)  # ok
            CENTLF0506[('CENTLF05', up, day, hour, 'NUM')] += desvio
            CENTLF0506[('CENTLF06', up, day, hour, 'NUM', 'Contestada')] += gestion
            CENTLF0506[('CENTLF06', up, day, hour, 'DEN', 'Contestada')] += 1
        elif res_desvio == 'Colgó Usuario':
            CENTLF0506[('CENTLF06', up, day, hour, 'NUM', 'No Contestada')] += gestion
            CENTLF0506[('CENTLF06', up, day, hour, 'DEN', 'No Contestada')] += 1

    # CENTLF02
    CENTLF02_num = c.Counter()
    CENTLF02_den = c.defaultdict(set)
    sql = """SELECT up, extract(day FROM CAST(hora_inicio AS timestamp))
                FROM DWMN_FACT.CALL_CENTER t, DWMN_DIM.CALL_CENTER_CATALOG_UNIF c
                WHERE t.TELEFONO = c.TLF_RI_PRINCIPAL
                AND RESULTADO_DESVIO != 'No Aplicable'
                AND extract(HOUR FROM CAST(hora_inicio AS timestamp)) BETWEEN 8 AND 19
                AND to_char(hora_inicio, 'd') NOT IN ('1','7')
                AND extract(MONTH FROM hora_inicio) = {} AND extract(YEAR FROM HORA_INICIO) = {}""".format(m_ext, y_ext)
    for up, day in u.getAll(sql, 'exadata'):
        CENTLF02_num[('CENTLF02', up, 'NUM')] += 1 # ok
        CENTLF02_den[up].add(day)  # ok

    upload = []
    for key, value in CENTLF.items():
        up = key[1]
        if up in info_centres:
            seu, centre, sector, organitzacio = info_centres[up]
        else:
            seu, centre, sector, organitzacio = '', '', '', ''
        if key[0] == 'CENTLF06':
            upload.append((key[0], seu, up, centre, sector, organitzacio, key[4], key[2], key[3], 'Contestada', value))
            upload.append((key[0], seu, up, centre, sector, organitzacio, key[4], key[2], key[3], 'No Contestada', value))
        else:
            upload.append((key[0], seu, up, centre, sector, organitzacio, key[4], key[2], key[3], 'DIM6SET', value))
    for key, value in CENTLF01_den.items():
        up = key[1]
        if up in info_centres:
            seu, centre, sector, organitzacio = info_centres[up]
        else:
            seu, centre, sector, organitzacio = '', '', '', ''
        upload.append((key[0], seu, up, centre, sector, organitzacio, key[4], key[2], key[3], 'DIM6SET', value))
    
    atesos = c.Counter()
    sql = """select up from nodrizas.assignada_tot
            where ates = 1"""
    for up, in u.getAll(sql, 'nodrizas'):
        atesos[up] += 1

    admins = {}
    sql = """SELECT up_ics, COUNT(DISTINCT NIF) 
            FROM dwsisap.CAT_ADMINISTRATIUS ca 
            GROUP BY UP_ICS"""
    for up, n in u.getAll(sql, 'exadata'):
        admins[up] = n
    
    for key, value in CENTLF02_num.items():
        centlf_02_den = len(CENTLF02_den[key[1]])
        up = key[1]
        if up in info_centres:
            seu, centre, sector, organitzacio = info_centres[up]
            upload.append((key[0]+'A', seu, up, centre, sector, organitzacio, key[2], 'NOCAT', 'NOIMP', 'DIM6SET', value))  # OK
            if up in admins:
                upload.append((key[0]+'A', seu, up, centre, sector, organitzacio, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', centlf_02_den*admins[up]))  # OK
            else:
                upload.append((key[0]+'A', seu, up, centre, sector, organitzacio, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 0))  # OK
            upload.append((key[0]+'B', seu, up, centre, sector, organitzacio, key[2], 'NOCAT', 'NOIMP', 'DIM6SET', value*1000))  # OK
            if up in atesos:
                upload.append((key[0]+'B', seu, up, centre, sector, organitzacio, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', centlf_02_den*atesos[up]))  # OK
            else:
                upload.append((key[0]+'B', seu, up, centre, sector, organitzacio, 'DEN', 'NOCAT', 'NOIMP', 'DIM6SET', 0))  # OK
    for key in CENTLF0506:
        up = key[1]
        if key[1] in info_centres:
            seu, centre, sector,  organitzacio = info_centres[key[1]]
        else:
            seu, centre, sector, organitzacio = '', '', '', ''
        if key[0] == 'CENTLF06':
            upload.append((key[0], seu, up, centre, sector, organitzacio, key[4], key[2], key[3], key[5], CENTLF0506[key]))
        else:
            upload.append((key[0], seu, up, centre, sector, organitzacio, key[4], key[2], key[3], 'DIM6SET', CENTLF0506[key]))
    indi4 = c.Counter()
    for key, value in CENTLF04.items():
        indi4[(key[0], key[1], key[3])] += len(value)
    for k, v in indi4.items():
        up = k[1]
        if up in info_centres:
            seu, centre, sector, organitzacio = info_centres[up]
        else:
            seu, centre, sector, organitzacio = '', '', '', ''
        upload.append((k[0], seu, up, centre, sector, organitzacio, k[2], 'NOCAT', 'NOIMP', 'DIM6SET', v))
    # den 7
    for k, v in CENTLF07_den.items():
        up = k[1]
        if k[1] in info_centres:
            seu, centre, sector, organitzacio = info_centres[up]
        else:
            up, centre, sector, organitzacio = '', '', '', ''
        upload.append((k[0], seu, up, centre, sector, organitzacio, k[2], 'NOCAT', 'NOIMP', 'DIM6SET', v))
    num07 = c.Counter()
    for k in CENTLF07_num:
        for k1 in CENTLF07_num[k]:
            if k1 in CENTLF07_num_ll[k]:
                num07[('CENTLF07', k[1], 'NUM')] += CENTLF07_num[k][k1]
    for k, v in num07.items():
        up = k[1]
        if k[1] in info_centres:
            seu, centre, sector, organitzacio = info_centres[up]
        else:
            seu, centre, sector, organitzacio = '', '', '', ''
        upload.append((k[0], seu, up, centre, sector, organitzacio, k[2], 'NOCAT', 'NOIMP', 'DIM6SET', v))

    print(len(upload))
    u.listToTable(upload, 'centraletes', 'altres')

    
def export_khalix():
    pany, month = u.getOne('select year(date_add(current_date(), interval -17 day)), month(date_add(current_date(), interval -17 day))  from nodrizas.dextraccio', 'nodrizas')
    month = str(month) if month >= 10 else '0'+str(month)
    periode = 'A' + str(pany)[2:] + month
    sql = """select indicador, '{}', ics_codi, analisi, control, dia, hora, sum(value)
    from (select c.indicador, cat.ics_codi, c.analisi, 
            case when c.control = 'Contestada' then 'CONTESTADA'
                when c.control = 'No Contestada' then 'NOCONTESTADA'
                else 'NARESPOSTATRUCADA' end control,
                case when c.dia = 'friday' then 'D5'
                when c.dia = 'saturday' then 'D6'
                when c.dia = 'sunday' then 'D7'
                when c.dia = 'monday' then 'D1'
                when c.dia = 'tuesday' then 'D2'
                when c.dia = 'wednesday' then 'D3'
                when c.dia = 'thursday' then 'D4' 
                else 'NADIASETMANA' end DIA,
                case when c.hora = '0' then 'HORA00'
                when c.hora = '1' then 'HORA01'
                when c.hora = '2' then 'HORA02'
                when c.hora = '3' then 'HORA03'
                when c.hora = '4' then 'HORA04'
                when c.hora = '5' then 'HORA05'
                when c.hora = '6' then 'HORA06'
                when c.hora = '7' then 'HORA07'
                when c.hora = '8' then 'HORA08'
                when c.hora = '9' then 'HORA09'
                when c.hora = '10' then 'HORA10'
                when c.hora = '11' then 'HORA11'
                when c.hora = '12' then 'HORA12'
                when c.hora = '13' then 'HORA13'
                when c.hora = '14' then 'HORA14'
                when c.hora = '15' then 'HORA15'
                when c.hora = '16' then 'HORA16'
                when c.hora = '17' then 'HORA17'
                when c.hora = '18' then 'HORA18'
                when c.hora = '19' then 'HORA19'
                when c.hora = '20' then 'HORA20'
                when c.hora = '21' then 'HORA21'
                when c.hora = '22' then 'HORA22'
                when c.hora = '23' then 'HORA23'
                else 'NAHORADIA'
                end hora, c.value
                from altres.centraletes c, nodrizas.cat_centres cat
                where c.up = cat.scs_codi) a
            group by indicador, ics_codi, analisi, control, dia, hora"""
    u.exportKhalix(sql.format(periode), 'CENTLF', force=True)

if __name__ == "__main__":
    centraletes_hores()
    export_khalix()
    mail = t.Mail()
    mail.to.append("roser.cantenys@catsalut.cat")
    mail.to.append("bpons@gencat.cat")
    mail.to.append("nmorenom.bnm.ics@gencat.cat")
    mail.subject = "UPLOADING CENTRALETES"
    mail.text = 'Hola Belen!. \n Could you mirar de carregar l\'arxiu de centraletes? Thank you:))))). \n Atentament el friendy reminder, \n Roser'
    mail.send()