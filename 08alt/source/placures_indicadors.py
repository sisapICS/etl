# coding: UTF-8
import collections as c
import sisapUtils as u
import dateutil.relativedelta

tb_br = "exp_khalix_up_pla_cures"
tb_br_jail = tb_br + "_jail"
tb_uba = "exp_khalix_uba_pla_cures"
tb_uba_jail = tb_uba + "_jail"
tb_pac = "mst_indicadors_pacient_placures"
db = "altres"

DIAGNOSTICS_DESC = {1: "CI",
                    3: "coag",
                    7: "AVC",
                    18: "DM2",
                    21: "ICC",
                    24: "DM1",
                    45: "ATDOM",
                    47: "Hipercolesterolemia",
                    53: "MRC",
                    55: "HTA",
                    58: "Asma",
                    62: "MPOC",
                    91: "UPP",
                    100: "Bronquitis cr�nica",
                    136: "Bronqui�ctasi",
                    180: "Fibril�laci� Auricular",
                    183: "Risc caigudes",
                    184: "Caigudes freq�ents",
                    239: "Obesitat",
                    254: "Anticoncepci� oral",
                    267: "Sobrepes",
                    833: "�lcera venosa",
                    884: "�lcera d'extremitat inferior",
                    885: "�lcera arterial",
                    887: "�lcera peu diab�tic",
                    890: "Peu de risc",
                    998: "Tabaquisme",
                    }  

VARIABLES_DESC = {103: "Cuidador identificat",
                  240: "Feu/activitat f�sica", 
                  241: "Grau Activitat f�sica", 
                  242: "Activitat f�sica", 
                  243: "Motivaci� exercici", 
                  859: "Activitat f�sica CBPAAT",
                  910: "Tabaquisme",
                 }

TRACTAMENTS_DESC = {3: "ACO",
                    255: "Anticoncepci� hormonal",
                    446: "Broncodilatadors"}

PARAMS_CONTROL_HTA = {482: {"tipus": "MAPA", "valor": 140, "valor_>60": 145},
                      483: {"tipus": "MAPA", "valor": 85, "valor_>60": 90},
                      906: {"tipus": "AMPA", "valor": 145, "valor_>60": 145},
                      907: {"tipus": "AMPA", "valor": 90, "valor_>60": 90},
                      674: {"tipus": "PAC", "valor": 150, "valor_>60": 160},
                      675: {"tipus": "PAC", "valor": 95, "valor_>60": 95},
                      }

PC_CATEGORIES_CODIS_ACTIU = {"actiu_asma": ("PC0007", "PC0008", "PC0009"),
                             "actiu_DM2": ("PC0031", "PC0032"),
                             "actiu_insuficiencia_venosa_periferica": ("PC0048",),
                             "actiu_lesions_relacionades_humitat": ("PC0073",),
                             "actiu_lesions_dependencia": ("PC0075",),
                             "actiu_ulceres_arterials": ("PC0086",),
                             "actiu_ulceres_neuropatiques": ("PC0174",),
                             "actiu_ulceres_neuroisquemiques": ("PC0173",),
                             "actiu_mpoc": ("PC0051", "PC0052", "PC0053", "PC0054"),
                             "actiu_icc": ("PC0041", "PC0042", "PC0043", "PC0044"),
                             "actiu_TAO": ("PC0067",),
                             "actiu_nafresiferidescroniques": ("PC0035", "PC0087", "PC0074", "PC0073", "PC0088", "PC0086"),
                             "actiu_ulceres_neuropatiques_neuroisquemiques": ("PC0087",),
                             "actiu_malalties_arterials_periferiques": ("PC0049",),
                             "actiu_neuropaties_periferiques": ("PC0068",),
                             "actiu_lesio_cutania_neoplasica": ("PC0111",),
                             "actiu_lesio_humitat_nens": ("PC0128",),
                             "actiu_pressio_nens": ("PC0129",),
                             "actiu_UPP": ("PC0074",),
                            }

class PlaCures(object):
    """ . """

    def __init__(self):
        """ . """

        self.placu = c.Counter()
        self.placu_pacs = []

        self.get_dextraccio()
        self.get_centres()
        self.get_poblacio()
        self.get_placures()
        self.get_problemes()

        self.get_placures_diagnostics()
        self.get_placures_noms()
        self.get_variables()
        self.get_tabaquisme()
        self.get_activitat_fisica()
        self.get_bon_control_HTA()
        self.get_reduccio_pes()
        self.get_zarit_reduit()
        self.get_tractaments()
        self.get_seguiment_TAO()
        self.get_bon_control_INR()
        self.get_peu_diabetic()
        self.get_tecnica_inhaladors()
        self.get_HBA1C()
        self.get_PLACU001()
        self.get_PLACU002()
        self.get_PLACU012()
        self.get_PLACU003()
        self.get_PLACU004()
        self.get_PLACU005()
        self.get_PLACU006()
        self.get_PLACU007()
        self.get_PLACU008()
        self.get_PLACU009()
        self.get_PLACU010()
        self.get_PLACU011()
        self.get_PLACU013()
        self.get_PLACU014()
        self.get_PLACU015()
        self.get_PLACU016()
        self.get_PLACU017()
        self.get_PLACU018()
        self.get_PLACU019()
        self.get_PLACU020()
        self.get_PLACU021()
        self.get_PLACU022()
        self.get_PLACU023()
        self.get_PLACU024()
        self.get_PLACU025()
        self.get_PLACU026()
        self.get_PLACU027()
        self.get_PLACU028()
        self.get_PLACU029()
        self.get_PLACU030()
        self.get_PLACU031()
        self.get_PLACU032_042()
        self.get_PLACU033_034()
        self.get_PLACU035_036()
        self.get_PLACU037_038()
        self.get_PLACU039()
        self.get_PLACU040()
        self.get_PLACU041()
        # self.get_PLACU043_044_045()
        self.get_PLACU046()
        self.get_PLACU047()
        self.export_resultat()
        if u.IS_MENSUAL:
            self.export_br()
            self.export_uba()

    def get_dextraccio(self):
        sql = """
                SELECT
                    data_ext
                FROM
                    dextraccio
	          """
        self.data_ext, = u.getOne(sql, "nodrizas")
        self.data_ext_menys1any = self.data_ext - dateutil.relativedelta.relativedelta(years=1)
        self.data_ext_menys2anys = self.data_ext - dateutil.relativedelta.relativedelta(years=2)
        self.data_ext_menys3mesos = self.data_ext - dateutil.relativedelta.relativedelta(months=3)
        self.periode = "A{}".format((self.data_ext - dateutil.relativedelta.relativedelta(days=15)).strftime("%y%m"))
        print("Data d'extraccio: {}".format(self.data_ext))

    def get_centres(self):
        """ . """
        
        self.cat_centres = {}

        sql = """
                SELECT
                    sector,
                    scs_codi,
                    ics_codi,
                    ics_desc,
                    amb_desc,
                    sap_desc,
                    'EAP'
                FROM
                    cat_centres
              UNION    
                SELECT
                    sector,
                    scs_codi,
                    ics_codi,
                    ics_desc,
                    amb_desc,
                    sap_desc,
                    'EAPP'
                FROM
                    jail_centres
	          """
        for sector, up, br, up_desc, amb_desc, sap_desc, tipus_centre in u.getAll(sql, "nodrizas"):  # noqa
            self.cat_centres[up] = {"sector": sector, "br": br, "up_desc": up_desc, "amb_desc": amb_desc, "sap_desc": sap_desc}
        print("Nombre de centres: {}".format(len(self.cat_centres)))

    def get_poblacio(self):
        """ . """
        self.poblacio = c.defaultdict(dict)
        self.poblacio_programa = c.defaultdict(set)
        self.poblacio_atesa_inf = set()
        sql = """
                SELECT
                    id_cip_sec,
                    codi_sector,
                    up,
                    uba,
                    ubainf,
                    edat,
                    sexe,
                    institucionalitzat,
                    pcc,
                    maca,
                    ates_inf
                FROM
                    assignada_tot_with_jail
                WHERE
                    ates = 1
              """
        for id_cip_sec, codi_sector, up, uba, ubainf, edat, sexe, institucionalitzat, pcc, maca, ates_inf in u.getAll(sql, "nodrizas"):
            if up in self.cat_centres:
                self.poblacio[id_cip_sec] = {"codi_sector": codi_sector, "up": up, "uba": uba, "ubainf": ubainf, "edat": edat, "sexe": sexe, "institucionalitzat": institucionalitzat}
                if pcc:
                    self.poblacio_programa["PCC"].add(id_cip_sec)
                if maca:
                    self.poblacio_programa["MACA"].add(id_cip_sec)
                if ates_inf:
                    self.poblacio_atesa_inf.add(id_cip_sec)
        print("Poblacio atesa assignada: {} persones".format(len(self.poblacio)))
        print("Poblacio atesa programa PCC: {} persones".format(len(self.poblacio_programa["PCC"])))
        print("Poblacio atesa programa MACA: {} persones".format(len(self.poblacio_programa["MACA"])))
        print("Poblacio atesa per infermeria: {} persones".format(len(self.poblacio_atesa_inf)))


    def get_placures(self):
        """."""
        self.placures_intervencions_ultim_any = c.defaultdict(lambda: c.defaultdict(set))
        self.placures_dict = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(set)))
        self.placures_dict['alta'] = c.defaultdict(lambda: c.defaultdict(dict))
        self.placures_dict['tancat_nafresiferidescroniques'] = c.defaultdict(lambda: c.defaultdict(lambda: c.defaultdict(dict)))

        sql = """
                SELECT
                    up_assignada,
                    id_cip_sec,
                    pc_id,
                    pc_codi,
                    data_inici,
                    data_fi,
                    pc_ultima_intervencio_data,
                    flag_aguda,    
                    flag_pc_alta_anual,
                    flag_pc_actiu,
                    flag_pc_seguiment_i_cronicitat,
                    flag_pc_tanc_ultany_naf_fer_cr,
                    flag_pc_alta_mensual,
                    flag_pc_alta_anual_deixarfumar
                FROM
                    master_placures
                WHERE
                    data_inici <= '{}'
                    AND data_fi >= '{}'
              UNION
                SELECT
                    up_assignada,
                    id_cip_sec,
                    pc_id,
                    pc_codi,
                    data_inici,
                    data_fi,
                    pc_ultima_intervencio_data,
                    flag_aguda,    
                    flag_pc_alta_anual,
                    flag_pc_actiu,
                    flag_pc_seguiment_i_cronicitat,
                    flag_pc_tanc_ultany_naf_fer_cr,
                    flag_pc_alta_mensual,
                    flag_pc_alta_anual_deixarfumar
                FROM
                    master_placures_jail
                WHERE
                    data_inici <= '{}'
                    AND data_fi >= '{}'
              """.format(self.data_ext, self.data_ext_menys1any, self.data_ext, self.data_ext_menys1any)
        for up, id_cip_sec, pc_id, pc_codi, pc_data_inici, pc_data_fi, pc_ultima_intervencio_data, flag_pc_aguda, flag_pc_alta, flag_pc_actiu, flag_pc_seguiment_i_cronicitat, flag_pc_tanc_ultany_naf_fer_cr, flag_pc_alta_mensual, flag_pc_alta_deixarfumar in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                if pc_ultima_intervencio_data:
                    if self.data_ext_menys1any < pc_ultima_intervencio_data <= self.data_ext:
                        self.placures_intervencions_ultim_any[up][id_cip_sec].add(pc_id)
                if flag_pc_alta:
                    self.placures_dict["alta"][up][id_cip_sec][pc_id] = pc_codi
                if flag_pc_aguda and self.data_ext_menys1any <= pc_data_inici <= self.data_ext:
                    self.placures_dict['obert_al_ultim_any_aguda'][up][id_cip_sec].add(pc_id)
                if flag_pc_alta_mensual:
                    self.placures_dict['alta_mensual'][up][id_cip_sec].add(pc_id)
                if flag_pc_alta_deixarfumar:
                    self.placures_dict['alta_deixarfumar'][up][id_cip_sec].add(pc_id)   
                if flag_pc_actiu:
                    self.placures_dict['actius'][up][id_cip_sec].add(pc_id)
                    if pc_codi == "PC0035" and u.monthsBetween(pc_data_inici, self.data_ext) >= 6:
                        self.placures_dict['actiu_estudi_ulcera_EEII_mes6mesos'][up][id_cip_sec].add(pc_id)
                    for pc_categoria, pc_codis in PC_CATEGORIES_CODIS_ACTIU.items():
                        if pc_codi in pc_codis:
                            self.placures_dict[pc_categoria][up][id_cip_sec].add(pc_id)
                if flag_pc_seguiment_i_cronicitat:
                    self.placures_dict['seguiment_cronicitat'][up][id_cip_sec].add(pc_id)
                if flag_pc_tanc_ultany_naf_fer_cr:
                    self.placures_dict['tancat_nafresiferidescroniques'][up][id_cip_sec][pc_codi][pc_id] = {"dies": u.daysBetween(pc_data_inici, pc_data_fi)} 
        
        print("Fet: self.get_placures()")

    def get_placures_diagnostics(self):
        """."""

        self.placures_dx = c.defaultdict(lambda:c.defaultdict(set))

        diagnostics_codi_desc = {"IRE02050": "�lcera neurop�tica",
                                 "IRE02052": "�lcera neuroisqu�mica",
                                 "IRE07661": "�lcera lesions relacionades amb la humitat",
                                 "IRE07416": "�lcera lesions relacionades amb la humitat"}

        sql = """
                SELECT
                    id_cip_sec,
                    pc_id, 
                    diagnostics_codi
                FROM
                    master_placures_diagnostics
                WHERE
                    diagnostics_data_inici <= '{}'
                    AND (diagnostics_data_fi = '0000-00-00' OR diagnostics_data_fi > '{}')
              UNION
                SELECT
                    id_cip_sec,
                    pc_id, 
                    diagnostics_codi
                FROM
                    master_placures_diagnostics_jail
                WHERE
                    diagnostics_data_inici <= '{}'
                    AND (diagnostics_data_fi = '0000-00-00' OR diagnostics_data_fi > '{}')
              """.format(self.data_ext, self.data_ext, self.data_ext, self.data_ext)
        for id_cip_sec, pc_id, diagnostics_codi in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                up = self.poblacio[id_cip_sec]["up"]
                if diagnostics_codi in diagnostics_codi_desc:
                    self.placures_dx[diagnostics_codi_desc[diagnostics_codi]][id_cip_sec].add(pc_id)
                self.placures_dx["tot"][id_cip_sec].add(pc_id)
        
        print("Fet: self.get_placures_diagnostics()")       

    def get_placures_noms(self):
        """."""

        self.placures_noms = dict()
        sql = """
                SELECT
                    DISTINCT pc_codi,
                    pc_nom
                FROM
                    cat_prstb335
              """
        self.placures_noms["PC0087"] = "�LCERA NEUROP�TICA/NEUROISQU�MICA" # Parche, porque lo han quitado del cat�logo
        for pc_codi, pc_nom in u.getAll(sql, "import"):
            self.placures_noms[pc_codi] = pc_nom
        
        print("Fet: self.get_placures_noms()")       

    def get_problemes(self):
        self.problemes = c.defaultdict(lambda: c.defaultdict(set))
        sql = """
                SELECT
                    id_cip_sec,
                    ps,
                    pr_gra
                FROM
                    eqa_problemes
                WHERE
                    ps IN {}
                    AND dde <= '{}' 
              """.format(str(tuple(str(int(ind)) for ind in DIAGNOSTICS_DESC.keys())), self.data_ext)
        for id_cip_sec, agr, gravetat in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                agr_desc = DIAGNOSTICS_DESC[agr]
                self.problemes[agr_desc][id_cip_sec].add(gravetat)

        for agr_desc in self.problemes:
            print("Recolectada la informaci� de diagn�stics de {}: {} persones".format(agr_desc, len(self.problemes[agr_desc])))

    def get_variables(self):
        """."""

        self.variables = c.defaultdict(dict)
        self.variables_tot = c.defaultdict(lambda: c.defaultdict(set))

        sql = """
                SELECT
                    id_cip_sec,
                    agrupador,
                    valor,
                    data_var
                FROM
                    eqa_variables,
                    dextraccio
                WHERE
                    data_var <= '{}'
                    AND agrupador IN {}          -- self.variables["Tabaquisme"]
              """.format(self.data_ext, tuple(VARIABLES_DESC.keys()))

        for id_cip_sec, agrupador, valor, data_var in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                agrupador_desc = VARIABLES_DESC[agrupador]
                if agrupador == 243 and valor in (0, 5):
                    valor = 9999 if valor == 0 else 0
                if not id_cip_sec in self.variables[agrupador_desc]:
                    self.variables[agrupador_desc][id_cip_sec] = (data_var, valor)
                else:
                    if data_var > self.variables[agrupador_desc][id_cip_sec][0]:
                        self.variables[agrupador_desc][id_cip_sec] = (data_var, valor)

                if agrupador in (240, 241, 242, 243, 859) and self.data_ext_menys2anys < data_var <= self.data_ext_menys3mesos:
                    self.variables_tot[agrupador_desc][id_cip_sec].add(valor)      
        
        print("Poblacio atesa que va constatar un consum actiu de tabac: {} persones".format(len(self.variables["Tabaquisme"])))

    def get_cessacio_tabaquica(self):
        self.cessacio_tabaquica = c.defaultdict(set)
        sql = """
                SELECT
                    id_cip_sec,
                    num
                FROM
                    eqa0305a_ind2
              """
        for id_cip_sec, num in u.getAll(sql, 'eqa_ind'):
            if id_cip_sec in self.poblacio:
                self.cessacio_tabaquica['den'].add(id_cip_sec)
                if num:
                    self.cessacio_tabaquica['num'].add(id_cip_sec)
        print("De {} persones fumadores de la poblaci� atesa assignada entre 15 i 80 anys, {} han deixat de fumar al llarg del periode d'avaluaci�".format(len(self.cessacio_tabaquica['den']), len(self.cessacio_tabaquica['num'])))

    def get_tabaquisme(self):
        """."""

        self.tabaquisme_abans_12_mesos, self.tabaquisme_last = set(), set()

        # Fumadors de fa un any i amb etiqueta last=1
        sql = """
                SELECT
                    id_cip_sec,
                    dalta,
                    dbaixa,
                    last
                FROM
                    eqa_tabac
                WHERE
                    tab = 1
              """
        for id_cip_sec, dalta, dbaixa, last in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                if dalta <= self.data_ext_menys1any <= dbaixa:
                    self.tabaquisme_abans_12_mesos.add(id_cip_sec)
                if last == 1:
                    self.tabaquisme_last.add(id_cip_sec)

        # Ja no fumen
        sql = """
                SELECT
                    id_cip_sec
                FROM
                    eqa_tabac
                WHERE
                    tab = 2
                    AND ('{}' BETWEEN dalta AND dbaixa) 
              """.format(self.data_ext)
        self.cessacio_tabaquica = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in self.tabaquisme_abans_12_mesos}

        print("Poblacio atesa que va constatar un consum actiu de tabac abans del periode d'avaluaci� i no durant aquest: {} persones".format(len(self.tabaquisme_abans_12_mesos)))
        print("Poblacio atesa que ha deixat de fumar al llarg del periode d'avaluaci�: {} persones".format(len(self.cessacio_tabaquica)))

    def get_activitat_fisica(self):
        self.fisicament_actius = set()
        sql_ca331 = """
                        SELECT
                            id_cip_sec
                        FROM
                            eqa_variables a,
                            dextraccio
                        WHERE
                            ((agrupador = 242 AND valor = 1) OR (agrupador = 859 AND valor >= 4)) 
                            AND usar = 1
                            AND data_var BETWEEN date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY) AND data_ext
                    """
        for id_cip_sec, in u.getAll(sql_ca331, "nodrizas"):
            self.fisicament_actius.add(id_cip_sec)
        print("Poblacio amb control de l'activitat f�sica favorable: {} persones".format(len(self.fisicament_actius)))

    def get_bon_control_HTA(self):
        """ . """
        self.bon_control_HTA = set()
        control_HTA = c.defaultdict(lambda : c.defaultdict(c.Counter))
        sql = """
                SELECT
                    id_cip_sec,
                    agrupador,
                    valor,
                    data_var
                FROM
                    eqa_variables,
                    dextraccio
                WHERE
                    agrupador IN {} AND
                    data_var BETWEEN date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY) AND data_ext""".format(str(tuple(PARAMS_CONTROL_HTA.keys())))

        for id_cip_sec, agr, valor, data in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.problemes["HTA"]:
                edat = self.poblacio[id_cip_sec]["edat"]
                valor_llindar = PARAMS_CONTROL_HTA[agr]["valor"] if edat < 60 else PARAMS_CONTROL_HTA[agr]["valor_>60"]
                if "data" not in control_HTA[id_cip_sec][PARAMS_CONTROL_HTA[agr]["tipus"]]:
                    control_HTA[id_cip_sec][PARAMS_CONTROL_HTA[agr]["tipus"]]["data"] = data
                    if valor <= valor_llindar:
                        control_HTA[id_cip_sec][PARAMS_CONTROL_HTA[agr]["tipus"]]["valor"] += 1
                    else:
                        control_HTA[id_cip_sec][PARAMS_CONTROL_HTA[agr]["tipus"]]["valor"] -= 1000
                else:
                    if data == control_HTA[id_cip_sec][PARAMS_CONTROL_HTA[agr]["tipus"]]["data"]:
                        if valor <= valor_llindar:
                            control_HTA[id_cip_sec][PARAMS_CONTROL_HTA[agr]["tipus"]]["valor"] += 1
                        else:
                            control_HTA[id_cip_sec][PARAMS_CONTROL_HTA[agr]["tipus"]]["valor"] -= 1000
                    elif data > control_HTA[id_cip_sec][PARAMS_CONTROL_HTA[agr]["tipus"]]["data"]:
                        del control_HTA[id_cip_sec][PARAMS_CONTROL_HTA[agr]["tipus"]]
                        control_HTA[id_cip_sec][PARAMS_CONTROL_HTA[agr]["tipus"]]["data"] = data
                        if valor <= valor_llindar:
                            control_HTA[id_cip_sec][PARAMS_CONTROL_HTA[agr]["tipus"]]["valor"] += 1
                        else:
                            control_HTA[id_cip_sec][PARAMS_CONTROL_HTA[agr]["tipus"]]["valor"] -= 1000
        for id_cip_sec in control_HTA:
            if "MAPA" in control_HTA[id_cip_sec]:
                data_mapa = control_HTA[id_cip_sec]["MAPA"]["data"]
                if control_HTA[id_cip_sec]["MAPA"]["valor"] == 2:
                    self.bon_control_HTA.add(id_cip_sec)
                elif control_HTA[id_cip_sec]["MAPA"]["valor"] < 0:
                    if "AMPA" in control_HTA[id_cip_sec]:
                        data_ampa = control_HTA[id_cip_sec]["AMPA"]["data"]
                        if data_mapa < data_ampa:
                            if control_HTA[id_cip_sec]["AMPA"]["valor"] == 2:
                                self.bon_control_HTA.add(id_cip_sec)
                            elif control_HTA[id_cip_sec]["AMPA"]["valor"] < 0:
                                if "PAC" in control_HTA[id_cip_sec]:
                                    data_pac = control_HTA[id_cip_sec]["PAC"]["data"]
                                    if data_ampa < data_pac:
                                        if control_HTA[id_cip_sec]["PAC"]["valor"] == 2:
                                            self.bon_control_HTA.add(id_cip_sec)
                    elif "PAC" in control_HTA[id_cip_sec]:
                        data_pac = control_HTA[id_cip_sec]["PAC"]["data"]
                        if data_mapa < data_pac:
                            if control_HTA[id_cip_sec]["PAC"]["valor"] == 2:
                                self.bon_control_HTA.add(id_cip_sec)
            elif "AMPA" in control_HTA[id_cip_sec]:
                data_ampa = control_HTA[id_cip_sec]["AMPA"]["data"]
                if control_HTA[id_cip_sec]["AMPA"]["valor"] == 2:
                    self.bon_control_HTA.add(id_cip_sec)
                elif control_HTA[id_cip_sec]["AMPA"]["valor"] < 0:
                    if "PAC" in control_HTA[id_cip_sec]:
                        data_pac = control_HTA[id_cip_sec]["PAC"]["data"]
                        if data_ampa < data_pac:
                            if control_HTA[id_cip_sec]["PAC"]["valor"] == 2:
                                self.bon_control_HTA.add(id_cip_sec)
            elif "PAC" in control_HTA[id_cip_sec]:
                if control_HTA[id_cip_sec]["PAC"]["valor"] == 2:
                    self.bon_control_HTA.add(id_cip_sec)
        print("Poblacio amb control de l'HTA favorable: {} persones".format(len(self.bon_control_HTA)))

    def get_reduccio_pes(self):
        """ . """
        self.reduccio_pes = set()
        pes_maxim_dict = dict()
        pes_actual_dict = dict()

        sql = """
                SELECT
                    id_cip_sec,
                    agrupador,
                    data_var,
                    valor,
                    usar
                FROM
                    eqa_variables,
                    dextraccio
                WHERE
                    agrupador IN (19, 268)
                    AND data_var BETWEEN date_add(date_add(data_ext, INTERVAL -2 YEAR), INTERVAL + 1 DAY) AND data_ext
                    AND valor <> 0
              """
        for id_cip_sec, agrupador, data_var, valor, usar in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.problemes["Obesitat"] or id_cip_sec in self.problemes["Sobrepes"]:
                if agrupador == 19 and valor < 25 and usar == 1:
                    self.reduccio_pes.add(id_cip_sec)
                elif agrupador == 268:
                    if id_cip_sec in pes_maxim_dict:
                        if valor > pes_maxim_dict[id_cip_sec]:
                            pes_maxim_dict[id_cip_sec] = valor
                    else:
                        pes_maxim_dict[id_cip_sec] = valor
                    if usar == 1:
                        pes_actual_dict[id_cip_sec] = valor
        for id_cip_sec in pes_actual_dict:
            pes_maxim = pes_maxim_dict[id_cip_sec]
            pes_actual = pes_actual_dict[id_cip_sec]
            if (pes_maxim - pes_actual) / pes_maxim >= 0.05:
                self.reduccio_pes.add(id_cip_sec)
        print("Poblacio amb sobrep�s o obesitat en qu� consta una reducci� del 5% del pes respecte el pes inicial o que ja tenen un IMC inferior a 25: {} persones".format(len(self.reduccio_pes)))

    def get_zarit_reduit(self):
        """."""
        #TODO:en el �ltimo a�o? solo el ultimo independientemente del a�o?

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    eqa_variables,
                    dextraccio
                WHERE
                    ((agrupador = 908 AND valor < 35)
                     OR (agrupador = 909 AND valor < 17)) 
                    AND usar = 1
                    AND data_var BETWEEN date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY) AND data_ext
              """
        self.zarit_reduit = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in self.poblacio}
        print("Poblacio amb Test de Zarit realitzat, amb puntuaci� Zarit redu�t < 17 o Zarit < 35: {} persones".format(len(self.zarit_reduit)))

    def get_tractaments(self):
        """."""

        self.tractaments = c.defaultdict(set)

        sql = """
                SELECT
                    id_cip_sec,
                    farmac,
                    tancat,
                    pres_orig,
                    data_fi
                FROM
                    eqa_tractaments
                WHERE
                    farmac IN {}
              """.format(tuple(TRACTAMENTS_DESC.keys()))
        for id_cip_sec, agrupador, tancat, data_inici, data_fi in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                tractament_desc = TRACTAMENTS_DESC[agrupador]
                if agrupador == 3 and tancat == 0:
                    self.tractaments[tractament_desc].add(id_cip_sec)
                elif agrupador == 255:
                    self.tractaments[tractament_desc].add(id_cip_sec)
                elif agrupador not in (3, 255):
                    if self.data_ext_menys1any < data_inici <= self.data_ext:
                        self.tractaments[tractament_desc].add(id_cip_sec)

        print("Poblacio en tractament amb Anticoagulants tipus AVK: {} persones".format(len(self.tractaments["ACO"])))

    def get_seguiment_TAO(self):
        """."""

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    eqa_variables_tao,
                    dextraccio
                WHERE
                    usar = 6
                    AND data BETWEEN date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY) AND data_ext
              """
        self.seguiment_TAO = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in self.poblacio}

        print("Poblacio amb control de TAO a l'AP: {} persones".format(len(self.seguiment_TAO)))

    def get_bon_control_INR(self):
        """."""

        sql = """
                SELECT
                    id_cip_sec,
                    num
                FROM
                    eqa_inr
                WHERE
                    den = 1
              """
        self.bon_control_INR = {id_cip_sec: num for id_cip_sec, num in u.getAll(sql, "eqa_ind") if id_cip_sec in self.poblacio}

        print("Poblacio amb control de l'INR favorable: {} persones".format(len(self.bon_control_INR)))

    def get_peu_diabetic(self):
        """."""
        self.cribatge_peu, self.exploracio_peu_diabetic = set(), set()

        sql = """
                SELECT 
                    id_cip_sec,
                    agrupador
                FROM
                    eqa_variables
                WHERE
                    agrupador IN (43, 477, 478)
                    AND '{}' < data_var <= '{}'
                    AND usar = 1
              """.format(self.data_ext_menys1any, self.data_ext)
        for id_cip_sec, agrupador in u.getAll(sql, "nodrizas"):
            if id_cip_sec in self.poblacio:
                if agrupador == 43:
                    self.cribatge_peu.add(id_cip_sec)
                elif agrupador in (477, 478):
                    self.exploracio_peu_diabetic.add(id_cip_sec)

        print("Poblacio amb cribatge del peu realitzat: {} persones".format(len(self.cribatge_peu)))
        print("Poblacio amb exploraci� del peu diab�tic realitzada: {} persones".format(len(self.exploracio_peu_diabetic)))

    def get_tecnica_inhaladors(self):
        """."""

        sql = """
                SELECT 
                    id_cip_sec
                FROM
                    eqa_variables
                WHERE
                    agrupador = 64
                    AND '{}' < data_var <= '{}'
              """.format(self.data_ext_menys1any, self.data_ext)
        self.tecnica_inhaladors = {id_cip_sec for id_cip_sec, in u.getAll(sql, "nodrizas") if id_cip_sec in self.poblacio}

        print("Poblacio amb control de t�cnica d'inhaladors realitzada: {} persones".format(len(self.tecnica_inhaladors)))

    def get_HBA1C(self):
        """."""
        self.HBA1C_inf8 = {}
        sql = """
                SELECT
                    id_cip_sec,
                    valor
                FROM
                    eqa_variables
                WHERE
                    agrupador = 20
                    AND usar = 1
                    AND data_var BETWEEN '{}' AND '{}'
                    AND valor <= 8
              """.format(self.data_ext_menys1any, self.data_ext)
        for id_cip_sec, valor in u.getAll(sql, "nodrizas"):
            self.HBA1C_inf8[id_cip_sec] = valor
        print("Poblacio amb control de glicada realitzat: {} persones".format(len(self.HBA1C_inf8)))

    def get_dades_pacient(self, id_cip_sec):
        """ Funci� per obtenir les dades del pacient necessaries pel c�lcul dels indicadors. """

        edat = u.ageConverter(self.poblacio[id_cip_sec]["edat"])
        sexe = u.sexConverter(self.poblacio[id_cip_sec]["sexe"])
        insti = self.poblacio[id_cip_sec]["institucionalitzat"]
        codi_sector = self.poblacio[id_cip_sec]["codi_sector"]
        up = self.poblacio[id_cip_sec]["up"]
        up_desc = self.cat_centres[up]["up_desc"]
        ubainf = self.poblacio[id_cip_sec]["ubainf"]
        amb_desc = self.cat_centres[up]["amb_desc"]
        sap_desc = self.cat_centres[up]["sap_desc"]

        return codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf 

    def get_PLACU001(self):
        """ Percentatge de persones amb pla de cures donat d'alta. """
        for id_cip_sec in self.poblacio:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU001", "% de persones amb P.C. donat d'alta", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict["alta"][up]:
                self.placu[("PLACU001", "% de persones amb P.C. donat d'alta", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU001", up, ubainf, id_cip_sec in self.placures_dict["alta"][up]))
        print("PLACU001")

    def get_PLACU002(self):
        """ Percentatge de persones ateses per infermeria amb pla de cures donat d'alta. """
        for id_cip_sec in self.poblacio_atesa_inf:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU002", "% de persones ateses per infermeria amb P.C. donat d'alta", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict["alta"][up]:
                self.placu[("PLACU002", "% de persones ateses per infermeria amb P.C. donat d'alta", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU002", up, ubainf, id_cip_sec in self.placures_dict["alta"][up]))
        print("PLACU002")

    def get_PLACU003(self):
        """ Percentatge de persones amb pla de cures actius. """
        for id_cip_sec in self.poblacio:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU003", "% de persones amb P.C. donat d'alta", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actius'][up]:
                self.placu[("PLACU003", "% de persones amb P.C. donat d'alta", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU003", up, ubainf, id_cip_sec in self.placures_dict['actius'][up]))
        print("PLACU003")

    def get_PLACU004(self):
        """ Percentatge de persones ateses per infermeria amb pla de cures actiu. """
        for id_cip_sec in self.poblacio_atesa_inf:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU004", "% de persones ateses per infermeria amb P.C. actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actius'][up]:
                self.placu[("PLACU004", "% de persones ateses per infermeria amb P.C. actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU004", up, ubainf, id_cip_sec in self.placures_dict['actius'][up]))
        print("PLACU004")

    def get_PLACU005(self):
        """ Percentatge de persones amb ICC i pla de cures de seguiment i cronicitat. """
        for id_cip_sec in self.problemes["ICC"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU005", "% de persones amb ICC i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                self.placu[("PLACU005", "% de persones amb ICC i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU005", up, ubainf, id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]))
        print("PLACU005")

    def get_PLACU006(self):
        """ Percentatge de persones amb DM2 i pla de cures de seguiment i cronicitat. """
        for id_cip_sec in self.problemes["DM2"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU006", "% de persones amb DM2 i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                self.placu[("PLACU006", "% de persones amb DM2 i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU006", up, ubainf, id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]))
        print("PLACU006")

    def get_PLACU007(self):
        """ Percentatge de persones amb MPOC i pla de cures de seguiment i cronicitat. """
        for id_cip_sec in self.problemes["MPOC"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU007", "% de persones amb MPOC i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                self.placu[("PLACU007", "% de persones amb MPOC i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU007", up, ubainf, id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]))
        print("PLACU007")

    def get_PLACU008(self):
        """ Percentatge de persones amb HTA i pla de cures de seguiment i cronicitat. """
        for id_cip_sec in self.problemes["HTA"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU008", "% de persones amb HTA i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                self.placu[("PLACU008", "% de persones amb HTA i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU008", up, ubainf, id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]))
        print("PLACU008")

    def get_PLACU009(self):
        """ Percentatge de persones amb obesitat i pla de cures de seguiment i cronicitat. """
        for id_cip_sec in self.problemes["Obesitat"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU009", "% de persones amb obesitat i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                self.placu[("PLACU009", "% de persones amb obesitat i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU009", up, ubainf, id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]))
        print("PLACU009")

    def get_PLACU010(self):
        """ Percentatge de persones incloses al programa PCC amb pla de cures de seguiment i cronicitat. """
        for id_cip_sec in self.poblacio_programa["PCC"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU010", "% de persones PCC amb P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                self.placu[("PLACU010", "% de persones PCC amb P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU010", up, ubainf, id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]))
        print("PLACU010")

    def get_PLACU011(self):
        """ Percentatge de persones incloses al programa MACA amb pla de cures de seguiment i cronicitat. """
        for id_cip_sec in self.poblacio_programa["MACA"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU011", "% de persones MACA amb P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                self.placu[("PLACU011", "% de persones MACA amb P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU011", up, ubainf, id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]))
        print("PLACU011")

    def get_PLACU012(self):
        """ Percentatge de persones ATDOM i pla de cures de seguiment i cronicitat. """
        for id_cip_sec in self.problemes["ATDOM"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU012", "% de persones ATDOM amb P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                self.placu[("PLACU012", "% de persones ATDOM amb P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU012", up, ubainf, id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]))
        print("PLACU012")

    def get_PLACU013(self):
        """ Percentatge de persones amb peu diab�tic i pla de cures de seguiment i cronicitat. """
        poblacio_programa_peu_diabetic = self.problemes["�lcera peu diab�tic"].copy()
        poblacio_programa_peu_diabetic.update(self.problemes["Peu de risc"])
        for id_cip_sec in poblacio_programa_peu_diabetic:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU013", "% de persones amb peu diab�tic i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                self.placu[("PLACU013", "% de persones amb peu diab�tic i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU013", up, ubainf, id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]))
        print("PLACU013")

    def get_PLACU014(self):
        """ Percentatge de persones amb risc de caiguda o caiguda i pla de cures de seguiment i cronicitat. """
        poblacio_programa_risc_caiguda = self.problemes["Risc caigudes"].copy()
        poblacio_programa_risc_caiguda.update(self.problemes["Caigudes freq�ents"])
        for id_cip_sec in poblacio_programa_risc_caiguda:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU014", "% de persones amb risc de caiguda o caiguda i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                self.placu[("PLACU014", "% de persones amb risc de caiguda o caiguda i P.C. de seguiment i cronicitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU014", up, ubainf, id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]))
        print("PLACU014")

    def get_PLACU015(self):
        """ Percentatge de persones en tractament amb ACO i seguiment a AP amb pla de cures. """
        poblacio_denominador = self.tractaments["ACO"] & self.seguiment_TAO
        for id_cip_sec in poblacio_denominador:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU015", "% de persones amb bon control de l'HTA", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                self.placu[("PLACU015", "% de persones amb bon control de l'HTA", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU015", up, ubainf, id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]))
        print("PLACU015")

    def get_PLACU016(self):
        """ Mitjana de dies en actiu dels plans de cures relacionats amb nafres i ferides cr�niques. """
        pc_codis = {
                    "PC0087": {"ind": "PLACU016A", "def": "Mitjana de dies en actiu dels plans de cures d'�lcera neuroisqu�mica"},
                    "PC0074": {"ind": "PLACU016B", "def": "Mitjana de dies en actiu dels plans de cures de lesi� per pressi�"},
                    "PC0073": {"ind": "PLACU016C", "def": "Mitjana de dies en actiu dels plans de cures d'�lcera relacionada amb la humitat"},
                    "PC0088": {"ind": "PLACU016D", "def": "Mitjana de dies en actiu dels plans de cures d'�lcera venosa"},
                    "PC0086": {"ind": "PLACU016E", "def": "Mitjana de dies en actiu dels plans de cures d'�lcera arterial"}
                    }
        for up in self.placures_dict['tancat_nafresiferidescroniques']:
            up_desc = self.cat_centres[up]["up_desc"]
            amb_desc = self.cat_centres[up]["amb_desc"]
            sap_desc = self.cat_centres[up]["sap_desc"]
            for id_cip_sec in self.placures_dict['tancat_nafresiferidescroniques'][up]:
                codi_sector = self.poblacio[id_cip_sec]["codi_sector"]
                ubainf = self.poblacio[id_cip_sec]["ubainf"]
                edat = u.ageConverter(self.poblacio[id_cip_sec]["edat"])  # TODO: estoy poniendo a todos la misma edad, cuando quizas el pc acabo hace a�os
                sexe = u.sexConverter(self.poblacio[id_cip_sec]["sexe"])
                insti = self.poblacio[id_cip_sec]["institucionalitzat"]
                for pc_codi in self.placures_dict['tancat_nafresiferidescroniques'][up][id_cip_sec]:
                    for pc_id in self.placures_dict['tancat_nafresiferidescroniques'][up][id_cip_sec][pc_codi]:
                        self.placu[("PLACU016", "Mitjana de dies en actiu dels plans de cures relacionats amb nafres i ferides cr�niques", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
                        self.placu[("PLACU016", "Mitjana de dies en actiu dels plans de cures relacionats amb nafres i ferides cr�niques", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += self.placures_dict['tancat_nafresiferidescroniques'][up][id_cip_sec][pc_codi][pc_id]["dies"]
                        if pc_codi in pc_codis:
                            self.placu[(pc_codis[pc_codi]["ind"], pc_codis[pc_codi]["def"], edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
                            self.placu[(pc_codis[pc_codi]["ind"], pc_codis[pc_codi]["def"], edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += self.placures_dict['tancat_nafresiferidescroniques'][up][id_cip_sec][pc_codi][pc_id]["dies"]
        print("PLACU016")

    def get_PLACU017(self):
        """ Percentatge plans de cures donats d'alta que es troben actius. """
        for up in self.placures_dict["alta"]:
            up_desc = self.cat_centres[up]["up_desc"]
            amb_desc = self.cat_centres[up]["amb_desc"]
            sap_desc = self.cat_centres[up]["sap_desc"]
            for id_cip_sec in self.placures_dict["alta"][up]:
                codi_sector = self.poblacio[id_cip_sec]["codi_sector"]
                ubainf = self.poblacio[id_cip_sec]["ubainf"]
                edat = u.ageConverter(self.poblacio[id_cip_sec]["edat"])
                sexe = u.sexConverter(self.poblacio[id_cip_sec]["sexe"])
                insti = self.poblacio[id_cip_sec]["institucionalitzat"]
                self.placu[("PLACU017", "% de P.C. donats d'alta que es troben actius", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
                if id_cip_sec in self.placures_dict['actius'][up]:
                    self.placu[("PLACU017", "% de P.C. donats d'alta que es troben actius", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                self.placu_pacs.append((id_cip_sec, "PLACU017", up, ubainf, id_cip_sec in self.placures_dict['actius'][up]))
        print("PLACU017")

    def get_PLACU018(self):
        """ Percentatge d'�s dels plans de cures. """
        for up in self.placures_dict["alta"]:
            up_desc = self.cat_centres[up]["up_desc"]
            amb_desc = self.cat_centres[up]["amb_desc"]
            sap_desc = self.cat_centres[up]["sap_desc"]
            for id_cip_sec in self.placures_dict["alta"][up]:
                codi_sector = self.poblacio[id_cip_sec]["codi_sector"]
                edat = u.ageConverter(self.poblacio[id_cip_sec]["edat"])
                sexe = u.sexConverter(self.poblacio[id_cip_sec]["sexe"])
                insti = self.poblacio[id_cip_sec]["institucionalitzat"]
                ubainf = self.poblacio[id_cip_sec]["ubainf"]
                for pc_id in self.placures_dict["alta"][up][id_cip_sec]:
                    pc_codi = self.placures_dict["alta"][up][id_cip_sec][pc_id]
                    pc_nom = self.placures_noms[pc_codi]
                    self.placu[("PLACU018", pc_codi, "% d'�s del P.C. del tipus \"{}\"".format(pc_nom.lower()), edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1        
                    if pc_id in self.placures_dict['alta_mensual'][up][id_cip_sec]:
                        self.placu[("PLACU018", pc_codi, "% d'�s del P.C. del tipus \"{}\"".format(pc_nom.lower()), edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VMENSUAL", "NUM")] += 1                                
                self.placu_pacs.append((id_cip_sec, "PLACU018", up, ubainf, id_cip_sec in self.placures_dict['alta_mensual'][up]))

        print("PLACU018")

    def get_PLACU019(self):
        """ Percentatge de persones que han deixat de fumar. """

        DEN_EQA0305 = set()
        NUM_EQA0305 = set()

        for agr_desc in [DIAGNOSTICS_DESC[agr] for agr in (1, 18, 21, 55, 58, 62, 100, 254)]:
            for id_cip_sec in self.problemes[agr_desc]:
                DEN_EQA0305.add(id_cip_sec)
        
        for id_cip_sec in self.tractaments["Anticoncepci� hormonal"]:
            DEN_EQA0305.add(id_cip_sec)

        for id_cip_sec in self.tabaquisme_abans_12_mesos:
            DEN_EQA0305.add(id_cip_sec)
        
        for id_cip_sec in self.cessacio_tabaquica:
            NUM_EQA0305.add(id_cip_sec)

        for id_cip_sec in DEN_EQA0305:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            if id_cip_sec in self.placures_dict['alta_deixarfumar'][up]:
                self.placu[("PLACU019", "% de P.C. donats d'alta que es troben actius", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
                if id_cip_sec in NUM_EQA0305:
                    self.placu[("PLACU019", "% de P.C. donats d'alta que es troben actius", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                self.placu_pacs.append((id_cip_sec, "PLACU019", up, ubainf, id_cip_sec in NUM_EQA0305))

        print("PLACU019")

    def get_PLACU020(self):
        """ Percentatge de persones amb bon control de l'activitat f�sica. """

        DEN_EQA0306 = c.defaultdict(lambda: {"fr=1": 0, "sedentaris=1": 0})
        NUM_EQA0306 = set()
        EXC_EQA0306 = set()

        for agr_desc in [DIAGNOSTICS_DESC[agr] for agr in (18, 24, 47, 55, 239, 267)]:
            for id_cip_sec in self.problemes[agr_desc]:
                DEN_EQA0306[id_cip_sec]["fr=1"] = 1

        for id_cip_sec, (_, valor) in self.variables["IMC"].items():
            if valor > 30:  # agrupador=19 and valor>30 and usar=1
                DEN_EQA0306[id_cip_sec]["fr=1"] = 1

        for id_cip_sec, (_, valor) in self.variables["Grau MPOC"].items():
            if valor == 4:  # agrupador=232 and valor=4 and usar=1
                EXC_EQA0306[id_cip_sec].add(id_cip_sec)

        for id_cip_sec, (_, valor) in self.variables["Oxigenoter�pia domiciliaria"].items():
            if valor == -2:  # agrupador=789 and valor=-2 and usar=1
                EXC_EQA0306[id_cip_sec].add(id_cip_sec)

        for id_cip_sec in self.tabaquisme_last:
            DEN_EQA0306[id_cip_sec]["fr=1"] = 1

        for agrupador_desc in self.variables_tot:
            for id_cip_sec, valors in self.variables_tot[agrupador_desc].items():
                min_valor_1any = min(valors)
                max_valor_1any = max(valors)
                ultim_valor = self.variables[agrupador_desc][id_cip_sec][1]
                if agrupador_desc == "Feu/activitat f�sica":
                    if max_valor_1any in (3, 4, 5):
                        DEN_EQA0306[id_cip_sec]["sedentaris=1"] = 1
                    if ultim_valor < max_valor_1any or ultim_valor in (1, 2): 
                        NUM_EQA0306.add(id_cip_sec)
                elif agrupador_desc == "Grau Activitat f�sica":
                    if min_valor_1any in (0, 1):
                        DEN_EQA0306[id_cip_sec]["sedentaris=1"] = 1
                    if min_valor_1any < ultim_valor or ultim_valor in (2, 3, 4):
                        NUM_EQA0306.add(id_cip_sec)
                elif agrupador_desc == "Activitat f�sica":
                    if max_valor_1any == 3:
                        DEN_EQA0306[id_cip_sec]["sedentaris=1"] = 1
                    if ultim_valor < max_valor_1any or ultim_valor == 1: 
                        NUM_EQA0306.add(id_cip_sec)
                elif agrupador_desc == "Motivaci� exercici":
                    if min_valor_1any in (0, 1, 2, 5):
                        DEN_EQA0306[id_cip_sec]["sedentaris=1"] = 1
                    if min_valor_1any < ultim_valor or ultim_valor in (3, 4):
                        NUM_EQA0306.add(id_cip_sec)
                elif agrupador_desc == "Activitat f�sica CBPAAT":
                    if min_valor_1any < 4:
                        DEN_EQA0306[id_cip_sec]["sedentaris=1"] = 1
                    if min_valor_1any < ultim_valor or ultim_valor > 3:
                        NUM_EQA0306.add(id_cip_sec)

        for agr_desc in [DIAGNOSTICS_DESC[agr] for agr in (62,)]:
            for id_cip_sec in self.problemes[agr_desc]:
                gravetats_problema = self.problemes[agr_desc][id_cip_sec]
                if 4 in gravetats_problema: # ps=62 and pr_gra=4
                    EXC_EQA0306.add(id_cip_sec)

        for id_cip_sec in DEN_EQA0306:
            if DEN_EQA0306[id_cip_sec]["fr=1"] and DEN_EQA0306[id_cip_sec]["sedentaris=1"]:
                if id_cip_sec not in EXC_EQA0306:
                    codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
                    if id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                        self.placu[("PLACU020", "% de P.C. donats d'alta que es troben actius", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
                        if id_cip_sec in NUM_EQA0306:
                            self.placu[("PLACU020", "% de P.C. donats d'alta que es troben actius", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                        self.placu_pacs.append((id_cip_sec, "PLACU020", up, ubainf, id_cip_sec in NUM_EQA0306))
        print("PLACU020")

    def get_PLACU021(self):
        """ Percentatge de persones amb bon control de l'HTA. """
        exclusio = set(self.problemes['CI'].keys()) | set(self.problemes['AVC'].keys()) | set(self.problemes['DM2'].keys()) | set(self.problemes['MRC'].keys()) | set(self.problemes['DM1'])
        for up in self.placures_dict['seguiment_cronicitat']:
            up_desc = self.cat_centres[up]["up_desc"]
            amb_desc = self.cat_centres[up]["amb_desc"]
            sap_desc = self.cat_centres[up]["sap_desc"]
            for id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                if id_cip_sec in self.problemes["HTA"] and id_cip_sec not in exclusio:
                    codi_sector = self.poblacio[id_cip_sec]["codi_sector"]
                    edat = u.ageConverter(self.poblacio[id_cip_sec]["edat"])
                    sexe = u.sexConverter(self.poblacio[id_cip_sec]["sexe"])
                    insti = self.poblacio[id_cip_sec]["institucionalitzat"]
                    ubainf = self.poblacio[id_cip_sec]["ubainf"]
                    self.placu[("PLACU021", "% de persones amb bon control de l'HTA", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
                    if id_cip_sec in self.bon_control_HTA:
                        self.placu[("PLACU021", "% de persones amb bon control de l'HTA", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                    self.placu_pacs.append((id_cip_sec, "PLACU021", up, ubainf, id_cip_sec in self.bon_control_HTA))
        print("PLACU021")

    def get_PLACU022(self):
        """ Percentatge de persones amb reducci� de pes en obesitat i sobrep�s. """
        poblacio_sobrepes_o_obesitat = self.problemes["Obesitat"].copy()
        poblacio_sobrepes_o_obesitat.update(self.problemes["Sobrepes"])
        for up in self.placures_dict['seguiment_cronicitat']:
            up_desc = self.cat_centres[up]["up_desc"]
            amb_desc = self.cat_centres[up]["amb_desc"]
            sap_desc = self.cat_centres[up]["sap_desc"]
            for id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                if id_cip_sec in poblacio_sobrepes_o_obesitat:
                    codi_sector = self.poblacio[id_cip_sec]["codi_sector"]
                    edat = u.ageConverter(self.poblacio[id_cip_sec]["edat"])
                    sexe = u.sexConverter(self.poblacio[id_cip_sec]["sexe"])
                    insti = self.poblacio[id_cip_sec]["institucionalitzat"]
                    ubainf = self.poblacio[id_cip_sec]["ubainf"]
                    self.placu[("PLACU022", "% de persones amb reducci� de pes en obesitat i sobrep�s", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
                    if id_cip_sec in self.reduccio_pes:
                        self.placu[("PLACU022", "% de persones amb reducci� de pes en obesitat i sobrep�s", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                    self.placu_pacs.append((id_cip_sec, "PLACU022", up, ubainf, id_cip_sec in self.reduccio_pes))
        print("PLACU022")

    def get_PLACU023(self):
        """ Percentatge de cuidadors d"ATDOM sense claudicaci� familiar. """
        for up in self.placures_dict['seguiment_cronicitat']:
            up_desc = self.cat_centres[up]["up_desc"]
            amb_desc = self.cat_centres[up]["amb_desc"]
            sap_desc = self.cat_centres[up]["sap_desc"]
            for id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                if id_cip_sec in self.problemes["ATDOM"] and id_cip_sec in self.variables["Cuidador identificat"]:
                    codi_sector = self.poblacio[id_cip_sec]["codi_sector"]
                    edat = u.ageConverter(self.poblacio[id_cip_sec]["edat"])
                    sexe = u.sexConverter(self.poblacio[id_cip_sec]["sexe"])
                    insti = self.poblacio[id_cip_sec]["institucionalitzat"]
                    ubainf = self.poblacio[id_cip_sec]["ubainf"]
                    self.placu[("PLACU023", "% de cuidadors d'ATDOM sense claudicaci� familiar", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
                    if id_cip_sec in self.zarit_reduit:
                        self.placu[("PLACU023", "% de cuidadors d'ATDOM sense claudicaci� familiar", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                    self.placu_pacs.append((id_cip_sec, "PLACU023", up, ubainf, id_cip_sec in self.zarit_reduit))
        print("PLACU023")

    def get_PLACU024(self):
        """ Percentatge de persones amb bon control de l'INR. """
        for up in self.placures_dict['seguiment_cronicitat']:
            up_desc = self.cat_centres[up]["up_desc"]
            amb_desc = self.cat_centres[up]["amb_desc"]
            sap_desc = self.cat_centres[up]["sap_desc"]
            for id_cip_sec in self.placures_dict['seguiment_cronicitat'][up]:
                if id_cip_sec in self.bon_control_INR:
                    codi_sector = self.poblacio[id_cip_sec]["codi_sector"]
                    edat = u.ageConverter(self.poblacio[id_cip_sec]["edat"])
                    sexe = u.sexConverter(self.poblacio[id_cip_sec]["sexe"])
                    insti = self.poblacio[id_cip_sec]["institucionalitzat"]
                    ubainf = self.poblacio[id_cip_sec]["ubainf"]
                    self.placu[("PLACU024", "% de persones amb bon control de l'INR", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
                    if self.bon_control_INR[id_cip_sec]:
                        self.placu[("PLACU024", "% de persones amb bon control de l'INR", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                    self.placu_pacs.append((id_cip_sec, "PLACU024", up, ubainf, id_cip_sec in self.zarit_reduit))
        print("PLACU024")

    def get_PLACU025(self):
        """ Percentatge de persones amb diagn�stic de MPOC i pla de cures MPOC actiu. """
        for id_cip_sec in self.problemes["MPOC"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU025", "% de persones amb diagn�stic de MPOC i pla de cures MPOC actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_mpoc'][up]:
                self.placu[("PLACU025", "% de persones amb diagn�stic de MPOC i pla de cures MPOC actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU025", up, ubainf, id_cip_sec in self.placures_dict['actiu_mpoc'][up]))
        print("PLACU025")
        
    def get_PLACU026(self):
        """ Percentatge de poblaci� amb diagn�stic de MPOC o bronqui�ctasi i pla de cures MPOC amb valoraci� de la t�cnica dels inhaladors. """
        for id_cip_sec in (set(self.problemes["MPOC"]) | set(self.problemes["Bronqui�ctasi"])) & self.tractaments["Broncodilatadors"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            if id_cip_sec in self.placures_dict['actiu_mpoc'][up]:
                self.placu[("PLACU026", "% de poblaci� amb diagn�stic de MPOC o bronqui�ctasi i pla de cures MPOC amb valoraci� de la t�cnica dels inhaladors", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
                if id_cip_sec in self.tecnica_inhaladors:
                    self.placu[("PLACU026", "% de poblaci� amb diagn�stic de MPOC o bronqui�ctasi i pla de cures MPOC amb valoraci� de la t�cnica dels inhaladors", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                self.placu_pacs.append((id_cip_sec, "PLACU026", up, ubainf, id_cip_sec in self.tecnica_inhaladors))
        print("PLACU026")
                
    def get_PLACU027(self):
        """ Percentatge de persones amb diagn�stic d'ICC amb pla de cures ICC actiu. """
        for id_cip_sec in self.problemes["ICC"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU027", "% de persones amb diagn�stic d'ICC amb pla de cures ICC actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_icc'][up]:
                self.placu[("PLACU027", "% de persones amb diagn�stic d'ICC amb pla de cures ICC actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU027", up, ubainf, id_cip_sec in self.placures_dict['actiu_icc'][up]))
        print("PLACU027")
                
    def get_PLACU028(self):
        """ Percentatge de persones en tractament amb ACO i seguiment a atenci� prim�ria amb pla de cures de TAO actiu. """
        poblacio_denominador = self.tractaments["ACO"] & self.seguiment_TAO
        for id_cip_sec in poblacio_denominador:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU028", "% de persones en tractament amb ACO i seguiment a atenci� prim�ria amb pla de cures de TAO actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_TAO'][up]:
                self.placu[("PLACU028", "% de persones en tractament amb ACO i seguiment a atenci� prim�ria amb pla de cures de TAO actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU028", up, ubainf, id_cip_sec in self.placures_dict['actiu_TAO'][up]))
        print("PLACU028")
                
    def get_PLACU029(self):
        """ Percentatge de poblaci� infantil diagnosticada d'asma amb pla de cures d'asma actiu. """
        for id_cip_sec in self.problemes["Asma"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            if edat < 15:
                self.placu[("PLACU029", "% de poblaci� infantil diagnosticada d'asma amb pla de cures d'asma actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
                if id_cip_sec in self.placures_dict['actiu_asma'][up]:
                    self.placu[("PLACU029", "% de poblaci� infantil diagnosticada d'asma amb pla de cures d'asma actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                self.placu_pacs.append((id_cip_sec, "PLACU029", up, ubainf, id_cip_sec in self.placures_dict['actiu_asma'][up]))
        print("PLACU029")
                
    def get_PLACU030(self):
        """ Percentatge de poblaci� amb DM2 amb cribratge peus i pla de cures de DM2 actiu. """
        for id_cip_sec in self.problemes["DM2"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU030", "% de poblaci� amb DM2 amb cribratge peus i pla de cures de DM2 actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_DM2'][up] and id_cip_sec in self.exploracio_peu_diabetic:
                self.placu[("PLACU030", "% de poblaci� amb DM2 amb cribratge peus i pla de cures de DM2 actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU030", up, ubainf, id_cip_sec in self.exploracio_peu_diabetic and id_cip_sec in self.placures_dict['actiu_DM2'][up]))
        print("PLACU030")
                
    def get_PLACU031(self):
        """ Percentatge de persones amb DM2 + cribratge peus amb pla de cures de DM2 actiu. """
        for id_cip_sec in set(self.problemes["DM2"]) & self.cribatge_peu:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU031", "% de persones amb DM2 + cribratge peus amb pla de cures de DM2 actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_DM2'][up]:
                self.placu[("PLACU031", "% de persones amb DM2 + cribratge peus amb pla de cures de DM2 actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU031", up, ubainf, id_cip_sec in self.placures_dict['actiu_DM2'][up]))
        print("PLACU031")
                
    def get_PLACU032_042(self):
        """ PLACU032: Percentatge de persones amb diagn�stic de nafra i pla de cures de nafres actiu. 
            PLACU042: Percentatge de persones amb qualsevol diagn�stic de nafra i pla de cures d'estudi d'�lcera obert m�s de 6 mesos.
        """
        for id_cip_sec in set(self.problemes["UPP"]) | set(self.problemes["�lcera venosa"]) | set(self.problemes["�lcera d'extremitat inferior"]) | set(self.problemes["�lcera arterial"]) | set(self.problemes["�lcera peu diab�tic"]):
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU032", "% de persones amb diagn�stic de nafra i pla de cures de nafres actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            self.placu[("PLACU042", "% de persones amb qualsevol diagn�stic de nafra i pla de cures d'estudi d'�lcera obert m�s de 6 mesos", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            num_32 = 0
            if id_cip_sec in set(self.placures_dict['actiu_nafresiferidescroniques'][up]) | set(self.placures_dict['actiu_ulceres_neuropatiques'][up]) | set(self.placures_dict['actiu_ulceres_neuroisquemiques'][up]) | set(self.placures_dict['actiu_lesio_cutania_neoplasica'][up]) | set(self.placures_dict['actiu_lesio_humitat_nens'][up]) | set(self.placures_dict['actiu_pressio_nens'][up]):
                self.placu[("PLACU032", "% de persones amb diagn�stic de nafra i pla de cures de nafres actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                num_32 = 1
            num_42 = 0
            if id_cip_sec in self.placures_dict['actiu_estudi_ulcera_EEII_mes6mesos'][up]:
                self.placu[("PLACU042", "% de persones amb dx de nafres i ferides cr�niques�i pla de cures d'estudi d'�lcera EEII que estigui obert m�s de 6 mesos", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                num_42 = 1
            self.placu_pacs.append((id_cip_sec, "PLACU032", up, ubainf, num_32))
            self.placu_pacs.append((id_cip_sec, "PLACU042", up, ubainf, num_42))
        print("PLACU032"); print("PLACU042")
                
    def get_PLACU033_034(self):
        """ PLACU033: Percentatge de persones amb diagn�stic d'�lcera venosa i pla de cures d'�lcera venosa.
            PLACU034: Percentatge de persones amb diagn�stic d'�lcera venosa i pla de cures d'�lcera venosa + pla de cures d'insufici�ncia venosa perif�rica.
        """
        for id_cip_sec in self.problemes["�lcera venosa"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU033", "% de persones amb diagn�stic d'�lcera venosa i pla de cures d'�lcera venosa", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            self.placu[("PLACU034", "% de persones amb diagn�stic d'�lcera venosa i pla de cures d'�lcera venosa + pla de cures d'insufici�ncia venosa perif�rica", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_ulceres_neuropatiques_neuroisquemiques'][up]:
                self.placu[("PLACU033", "% de persones amb diagn�stic d'�lcera venosa i pla de cures d'�lcera venosa", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                if id_cip_sec in self.placures_dict['actiu_insuficiencia_venosa_periferica'][up]:
                    self.placu[("PLACU034", "% de persones amb diagn�stic d'�lcera venosa i pla de cures d'�lcera venosa + pla de cures d'insufici�ncia venosa perif�rica", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU033", up, ubainf, id_cip_sec in self.placures_dict['actiu_ulceres_neuropatiques_neuroisquemiques'][up]))
            self.placu_pacs.append((id_cip_sec, "PLACU034", up, ubainf, id_cip_sec in self.placures_dict['actiu_ulceres_neuropatiques_neuroisquemiques'][up] and id_cip_sec in self.placures_dict['actiu_insuficiencia_venosa_periferica'][up]))
        print("PLACU033"); print("PLACU034")
                
    def get_PLACU035_036(self):
        """ PLACU035: Percentatge de persones amb diagn�stic d'�lcera arterial i pla de cures d'�lcera arterial. 
            PLACU036: Percentatge de persones amb diagn�stic d'�lcera arterial i pla de cures d'�lcera arterial + pla de cures de malaltia arterial perif�rica.
        """
        for id_cip_sec in self.problemes["�lcera arterial"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU035", "% de persones amb diagn�stic d'�lcera arterial i pla de cures d'�lcera arterial", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            self.placu[("PLACU036", "% de persones amb diagn�stic d'�lcera arterial i pla de cures d'�lcera arterial + pla de cures de malaltia arterial perif�rica", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_ulceres_arterials'][up]:
                self.placu[("PLACU035", "% de persones amb diagn�stic d'�lcera arterial i pla de cures d'�lcera arterial", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                if id_cip_sec in self.placures_dict['actiu_malalties_arterials_periferiques'][up]:
                    self.placu[("PLACU036", "% de persones amb diagn�stic d'�lcera arterial i pla de cures d'�lcera arterial + pla de cures de malaltia arterial perif�rica", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU035", up, ubainf, id_cip_sec in self.placures_dict['actiu_ulceres_arterials'][up]))
            self.placu_pacs.append((id_cip_sec, "PLACU036", up, ubainf, id_cip_sec in self.placures_dict['actiu_ulceres_arterials'][up] and id_cip_sec in self.placures_dict['actiu_malalties_arterials_periferiques'][up]))
        print("PLACU035"); print("PLACU036")
           
    def get_PLACU037_038(self):
        """ PLACU037: Percentatge de persones amb diagn�stic lesi� per pressi� i pla de cures de lesi� per pressi�. 
            PLACU038: Percentatge de persones amb diagn�stic lesi� per pressi� i pla de cures de lesi� per pressi� + pla de cures de prevenci� de lesions.
        """
        for id_cip_sec in self.problemes["UPP"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU037", "% de persones amb diagn�stic lesi� per pressi� i pla de cures de lesi� per pressi�", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            self.placu[("PLACU038", "% de persones amb diagn�stic lesi� per pressi� i pla de cures de lesi� per pressi� + pla de cures de prevenci� de lesions", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_UPP'][up]:
                self.placu[("PLACU037", "% de persones amb diagn�stic lesi� per pressi� i pla de cures de lesi� per pressi�", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                if id_cip_sec in self.placures_dict['actiu_lesions_dependencia'][up]:
                    self.placu[("PLACU038", "% de persones amb diagn�stic lesi� per pressi� i pla de cures de lesi� per pressi� + pla de cures de prevenci� de lesions", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU037", up, ubainf, id_cip_sec in self.placures_dict['actiu_UPP'][up]))
            self.placu_pacs.append((id_cip_sec, "PLACU038", up, ubainf, id_cip_sec in self.placures_dict['actiu_UPP'][up] and id_cip_sec in self.placures_dict['actiu_lesions_dependencia'][up]))
        print("PLACU037"); print("PLACU038")
           
    def get_PLACU039(self):
        """ PLACU039: Percentatge de persones amb diagn�stic �lcera neuroisqu�mica i/o neurop�tica amb pla de cures �lcera neuroisqu�mica. 
            PLACU039A: Percentatge de persones amb diagn�stic �lcera neurop�tica amb pla de cures �lcera neurop�tica. 
            PLACU039B: Percentatge de persones amb diagn�stic �lcera neuroisqu�mica amb pla de cures �lcera neuroisqu�mica. 
        """
        for id_cip_sec in set(self.placures_dx["�lcera neurop�tica"]) | set(self.placures_dx["�lcera neuroisqu�mica"]):
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU039", "% de persones amb diagn�stic �lcera neuroisqu�mica i/o neurop�tica amb pla de cures �lcera neuroisqu�mica/neurop�tica", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_ulceres_neuropatiques_neuroisquemiques'][up]:
                self.placu[("PLACU039", "% de persones amb diagn�stic �lcera neuroisqu�mica i/o neurop�tica amb pla de cures �lcera  neuroisqu�mica/neurop�tica", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU039", up, ubainf, id_cip_sec in self.placures_dict['actiu_ulceres_neuropatiques_neuroisquemiques'][up]))
        print("PLACU039")

        for id_cip_sec in self.placures_dx["�lcera neurop�tica"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU039A", "% de persones amb diagn�stic �lcera neurop�tica i pla de cures �lcera neurop�tica", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_ulceres_neuropatiques'][up]:
                self.placu[("PLACU039A", "% de persones amb diagn�stic �lcera neurop�tica i pla de cures �lcera neurop�tica", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU039A", up, ubainf, id_cip_sec in self.placures_dict['actiu_ulceres_neuropatiques'][up]))
        print("PLACU039A")

        for id_cip_sec in self.placures_dx["�lcera neuroisqu�mica"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU039B", "% de persones amb diagn�stic �lcera neuroisqu�mica i pla de cures �lcera neuroisqu�mica", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_ulceres_neuroisquemiques'][up]:
                self.placu[("PLACU039B", "% de persones amb diagn�stic �lcera neuroisqu�mica i pla de cures �lcera neuroisqu�mica", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU039B", up, ubainf, id_cip_sec in self.placures_dict['actiu_ulceres_neuroisquemiques'][up]))
        print("PLACU039B")
           
    def get_PLACU040(self):
        """ PLACU040: Percentatge de persones amb diagn�stic + pla de cures �lcera neurop�tica/neuroisqu�mica amb pla de cures neuropatia perif�rica.
            PLACU040A: Percentatge de persones amb diagn�stic + pla de cures �lcera neurop�tica amb pla de cures neuropatia perif�rica.
            PLACU040B: Percentatge de persones amb diagn�stic + pla de cures �lcera neuroisqu�mica amb pla de cures neuropatia perif�rica.
        """
        for id_cip_sec in set(self.placures_dx["�lcera neurop�tica"]) | set(self.placures_dx["�lcera neuroisqu�mica"]):
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU040", "% de persones amb diagn�stic + pla de cures �lcera neurop�tica/neuroisqu�mica amb pla de cures neuropatia perif�rica", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_ulceres_neuropatiques_neuroisquemiques'][up] and id_cip_sec in self.placures_dict['actiu_neuropaties_periferiques'][up]:
                self.placu[("PLACU040", "% de persones amb diagn�stic + pla de cures �lcera neurop�tica/neuroisqu�mica amb pla de cures neuropatia perif�rica", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU040", up, ubainf, id_cip_sec in self.placures_dict['actiu_ulceres_neuropatiques_neuroisquemiques'][up] and id_cip_sec in self.placures_dict['actiu_neuropaties_periferiques'][up]))
        print("PLACU040")

        for id_cip_sec in self.placures_dx["�lcera neurop�tica"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU040A", "% de persones amb diagn�stic + pla de cures �lcera neurop�tica amb pla de cures neuropatia perif�rica.", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_ulceres_neuropatiques'][up] and id_cip_sec in self.placures_dict['actiu_neuropaties_periferiques'][up]:
                self.placu[("PLACU040A", "% de persones amb diagn�stic + pla de cures �lcera neurop�tica amb pla de cures neuropatia perif�rica.", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU040A", up, ubainf, id_cip_sec in self.placures_dict['actiu_ulceres_neuropatiques'][up] and id_cip_sec in self.placures_dict['actiu_neuropaties_periferiques'][up]))
        print("PLACU040A")

        for id_cip_sec in self.placures_dx["�lcera neuroisqu�mica"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU040B", "% de persones amb diagn�stic + pla de cures �lcera neurop�tica amb pla de cures neuropatia perif�rica.", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_ulceres_neuroisquemiques'][up] and id_cip_sec in self.placures_dict['actiu_neuropaties_periferiques'][up]:
                self.placu[("PLACU040B", "% de persones amb diagn�stic + pla de cures �lcera neurop�tica amb pla de cures neuropatia perif�rica.", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU040B", up, ubainf, id_cip_sec in self.placures_dict['actiu_ulceres_neuroisquemiques'][up] and id_cip_sec in self.placures_dict['actiu_neuropaties_periferiques'][up]))
        print("PLACU040B")


    def get_PLACU041(self):
        """ Percentatge de persones amb diagn�stic lesions relacionades amb la humitat i pla de cures lesions relacionades amb la humitat. """
        for id_cip_sec in self.placures_dx["�lcera lesions relacionades amb la humitat"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            self.placu[("PLACU041", "% de persones amb diagn�stic lesions relacionades amb la humitat i pla de cures lesions relacionades amb la humitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
            if id_cip_sec in self.placures_dict['actiu_lesions_relacionades_humitat'][up]:
                self.placu[("PLACU041", "% de persones amb diagn�stic lesions relacionades amb la humitat i pla de cures lesions relacionades amb la humitat", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
            self.placu_pacs.append((id_cip_sec, "PLACU041", up, ubainf, id_cip_sec in self.placures_dict['actiu_lesions_relacionades_humitat'][up]))
        print("PLACU041")

    # def get_PLACU043_044_045(self):
    #     """ PLACU043: Taxa x 1000 de persones amb pla de cures agut.
    #         PLACU044: Taxa x 1000 de persones amb pla de cures agut + un diagn�stic.
    #         PLACU045: Taxa x 1000 de persones amb pla de cures agut + un diagn�stic + una intervenci� registrada.
    #     """
    #     for id_cip_sec in self.poblacio:
    #         codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
    #         self.placu[("PLACU043", "Taxa x 1000 de persones amb pla de cures agut", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
    #         self.placu[("PLACU044", "Taxa x 1000 de persones amb pla de cures agut + un diagn�stic", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
    #         self.placu[("PLACU045", "Taxa x 1000 de persones amb pla de cures agut + un diagn�stic + una intervenci� registrada", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
    #         num_43, num_44, num_45 = 0, 0, 0
    #         if id_cip_sec in self.placures_dict['obert_al_ultim_any_aguda'][up]:
    #             for pc_id in self.placures_dict['obert_al_ultim_any_aguda'][up][id_cip_sec]:
    #                 self.placu[("PLACU043", "Taxa x 1000 de persones amb pla de cures agut", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
    #                 num_43 += 1
    #                 if id_cip_sec in self.placures_dx["tot"]:
    #                     if pc_id in self.placures_dx["tot"][id_cip_sec]:
    #                         self.placu[("PLACU044", "Taxa x 1000 de persones amb pla de cures agut + un diagn�stic", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
    #                         num_44 += 1
    #                         if id_cip_sec in self.placures_intervencions_ultim_any[up]:
    #                             if pc_id in self.placures_intervencions_ultim_any[up][id_cip_sec]:
    #                                 self.placu[("PLACU045", "Taxa x 1000 de persones amb pla de cures agut + un diagn�stic + una intervenci� registrada", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
    #                                 num_45 += 1
    #         self.placu_pacs.append((id_cip_sec, "PLACU043", up, ubainf, num_43))
    #         self.placu_pacs.append((id_cip_sec, "PLACU044", up, ubainf, num_44))
    #         self.placu_pacs.append((id_cip_sec, "PLACU045", up, ubainf, num_45))
    #     print("PLACU043"); print("PLACU044"); print("PLACU045")

    def get_PLACU046(self):
        """ Percentatge de poblaci� entre 14 i 80 anys + diagn�stic DM2 + pla de cures de DM amb glicada inferior o igual a 8%. """
        for id_cip_sec in self.problemes["DM2"]:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            if 14 < edat <= 80 and id_cip_sec in self.placures_dict['actiu_DM2'][up]:
                self.placu[("PLACU046", "% de poblaci� entre 14 i 80 anys + diagn�stic DM2 + pla de cures de DM amb glicada inferior o igual a 8%", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
                if id_cip_sec in self.HBA1C_inf8:
                    self.placu[("PLACU046", "% de poblaci� entre 14 i 80 anys + diagn�stic DM2 + pla de cures de DM amb glicada inferior o igual a 8%", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                self.placu_pacs.append((id_cip_sec, "PLACU046", up, ubainf, id_cip_sec in self.exploracio_peu_diabetic and id_cip_sec in self.placures_dict['actiu_DM2'][up]))
        print("PLACU046")

    def get_PLACU047(self):
        """ Percentatge de persones amb bon control de l'INR i pla de cures de TAO actiu actiu. """
        for id_cip_sec in set(self.problemes["Fibril�laci� Auricular"]) & self.tractaments["ACO"] & self.seguiment_TAO:
            codi_sector, up, up_desc, amb_desc, sap_desc, edat, sexe, insti, ubainf = self.get_dades_pacient(id_cip_sec)
            if id_cip_sec in self.placures_dict['actiu_TAO'][up]:
                self.placu[("PLACU047", "% de persones amb bon control de l'INR i pla de cures de TAO actiu actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "DEN")] += 1
                if id_cip_sec in self.bon_control_INR:
                    if self.bon_control_INR[id_cip_sec]:
                        self.placu[("PLACU047", "% de persones amb bon control de l'INR i pla de cures de TAO actiu actiu", edat, sexe, insti, amb_desc, sap_desc, codi_sector, up, up_desc, ubainf, "VANUAL", "NUM")] += 1
                self.placu_pacs.append((id_cip_sec, "PLACU047", up, ubainf, id_cip_sec in self.exploracio_peu_diabetic and id_cip_sec in self.placures_dict['actiu_DM2'][up]))
        print("PLACU047")
        
    def export_resultat(self):
        """ Percentatge de persones amb bon control de l'INR. """
        resultat_counter = c.defaultdict(c.Counter)
        self.resultat = c.defaultdict(list)
        for entity, n in self.placu.items():
            if entity[0] == "PLACU018":
                indicador, pc_codi, _, edat, sexe, insti, _, _, codi_sector, up, _, ubainf, vperiode, analisis = entity
            else:
                indicador, _, edat, sexe, insti, _, _, codi_sector, up, _, ubainf, _, analisis = entity
            br = self.cat_centres[up]["br"]
            br_ubainf = br + "I" + ubainf
            insti_ates = "INSAT" if insti else "NOINSAT"

            coletilla_jail = "_jail" if codi_sector == '6951' else ""
            
            if indicador == "PLACU018":
                resultat_counter["br{}".format(coletilla_jail)][(indicador, self.periode, br, analisis, edat, pc_codi, insti_ates, sexe, vperiode)] += n
                resultat_counter["ubainf{}".format(coletilla_jail)][(indicador, self.periode, br_ubainf, analisis, pc_codi, insti_ates, vperiode)] += n
            else:
                resultat_counter["br{}".format(coletilla_jail)][(indicador, self.periode, br, analisis, edat, None, insti_ates, sexe, 'VANUAL')] += n
                resultat_counter["ubainf{}".format(coletilla_jail)][(indicador, self.periode, br_ubainf, analisis, None, insti_ates, 'VANUAL')] += n
        for centre in resultat_counter:
            for entity, n in resultat_counter[centre].items():
                entity = list(entity)
                entity.append(n)
                self.resultat[centre].append(entity)

        u.createTable(tb_br, "(indicador varchar(10), periode varchar(6), br varchar(5), analisis varchar(3), edat varchar(6), pc_codi varchar(20), detalls varchar(7), sexe varchar(4), vperiode varchar(8), n int)", db, rm=True)
        u.listToTable(self.resultat["br"], tb_br, db)

        u.createTable(tb_br_jail, "(indicador varchar(10), periode varchar(6), br varchar(5), analisis varchar(3), edat varchar(6), pc_codi varchar(20), detalls varchar(7), sexe varchar(4), vperiode varchar(8), n int)", db, rm=True)
        u.listToTable(self.resultat["br_jail"], tb_br_jail, db)

        u.createTable(tb_uba, "(indicador varchar(10), periode varchar(6), uba varchar(11), analisis varchar(3), pc_codi varchar(20), detalls varchar(7), vperiode varchar(8), n int)", db, rm=True)
        u.listToTable(self.resultat["ubainf"], tb_uba, db)

        u.createTable(tb_uba_jail, "(indicador varchar(10), periode varchar(6), uba varchar(11), analisis varchar(3), pc_codi varchar(20), detalls varchar(7), vperiode varchar(8), n int)", db, rm=True)
        u.listToTable(self.resultat["ubainf_jail"], tb_uba_jail, db)

        u.createTable(tb_pac, "(id_cip_sec int, indicador varchar(10), up varchar(5), uba varchar(11), compleix int)", db, rm=True)
        u.execute("""ALTER TABLE {} 
                     ADD INDEX idx_id_cip_sec (id_cip_sec),
                     ADD INDEX idx_indicador (indicador)""".format(tb_pac), db)
        u.listToTable([(id, ind, up, uba, 1 * compleix) for (id, ind, up, uba, compleix) in self.placu_pacs], tb_pac, db)

    def export_br(self):
        """."""
        sql_1 = "select indicador, periode, br, analisis, edat, detalls, sexe, 'N', n from {}.{} where indicador <> 'PLACU018' \
                 group by indicador, periode, br, analisis, edat, detalls, sexe".format(db, tb_br)
        file_1 = "PLACURES"
        u.exportKhalix(sql_1, file_1)

        sql_2 = "select indicador, periode, br, analisis, pc_codi, detalls, vperiode, 'N', sum(n) from {}.{} where indicador = 'PLACU018' \
                 group by indicador, periode, br, analisis, pc_codi, detalls, vperiode".format(db, tb_br)
        file_2 = "PLACURES018"
        u.exportKhalix(sql_2, file_2)

        sql_1 = "select indicador, periode, br, analisis, edat, detalls, sexe, 'N', n from {}.{} where indicador <> 'PLACU018' \
                 group by indicador, periode, br, analisis, edat, detalls, sexe".format(db, tb_br_jail)
        file_1 = "PLACURES"
        u.exportKhalix(sql_1, file_1+"_EAPP")

        sql_2 = "select indicador, periode, br, analisis, pc_codi, detalls, vperiode, 'N', sum(n) from {}.{} where indicador = 'PLACU018' \
                 group by indicador, periode, br, analisis, pc_codi, detalls, vperiode".format(db, tb_br_jail)
        file_2 = "PLACURES018"
        u.exportKhalix(sql_2, file_2+"_EAPP")        

    def export_uba(self):
        """."""
        sql_1 = "select indicador, periode, uba, analisis, 'NOCAT', detalls, 'DIM6SET', 'N', sum(n) from {}.{} where indicador <> 'PLACU018' \
                 group by indicador, periode, uba, analisis, detalls".format(db, tb_uba)
        file_1 = "PLACURES_UBA"
        u.exportKhalix(sql_1, file_1)

        sql_2 = "select indicador, periode, uba, analisis, pc_codi, detalls, vperiode, 'N', sum(n) from {}.{} where indicador = 'PLACU018' \
                 group by indicador, periode, uba, analisis, pc_codi, detalls, vperiode".format(db, tb_uba)
        file_2 = "PLACURES018_UBA"
        u.exportKhalix(sql_2, file_2)

        sql_1 = "select indicador, periode, uba, analisis, 'NOCAT', detalls, 'DIM6SET', 'N', sum(n) from {}.{} where indicador <> 'PLACU018' \
                 group by indicador, periode, uba, analisis, detalls".format(db, tb_uba_jail)
        file_1 = "PLACURES_UBA"
        u.exportKhalix(sql_1, file_1+"_EAPP")

        sql_2 = "select indicador, periode, uba, analisis, pc_codi, detalls, vperiode, 'N', sum(n) from {}.{} where indicador = 'PLACU018' \
                 group by indicador, periode, uba, analisis, pc_codi, detalls, vperiode".format(db, tb_uba_jail)
        file_2 = "PLACURES018_UBA"
        u.exportKhalix(sql_2, file_2+"_EAPP")        


if __name__ == "__main__":
    PlaCures()