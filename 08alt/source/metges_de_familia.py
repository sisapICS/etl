# coding: utf8

"""
.
"""

import sisapUtils as u

if u.IS_MENSUAL and int(u.getKhalixDates()[2]) in (3, 6, 9, 12):
    provincies = {}
    sql = """SELECT codi_up, substr(codi_localitat, 0, 2) FROM dwdimics.DIM_UP_CATSALUt"""
    for up, provincia in u.getAll(sql, 'exadata'):
        if provincia == '08': prov = 'Barcelona'
        elif provincia == '17': prov = 'Girona'
        elif provincia == '43': prov = 'Tarragona'
        elif provincia == '25': prov = 'Lleida'
        else: prov == '' 
        provincies[up] = prov

    sql = """SELECT UP, IDE_DNI, IDE_NUMCOL, NOM, 
                COGNOM1, COGNOM2, SERVEI, ADULTS, 
                NENS, NENS+ADULTS AS PACIENTS  
                FROM PROFESSIONALS
                WHERE tipus = 'M'"""

    upload = []
    for up, dni, col, nom, cog1, cog2, servei, adults, nens, pacients in u.getAll(sql, 'pdp'):
        if up in provincies:
            upload.append((up, provincies[up], dni, col, nom, cog1, cog2, servei, adults, nens, pacients))

    # creació csv
    file_eap = u.tempFolder + 'metges_de_familia.csv'
    u.writeCSV(file_eap, [('UP', 'PROVINCIA', 'DNI', 'NUM_COLEGIAT', 'NOM', 'COGNOM1', 'COGNOM2', 'SERVEI', 'ADULTS', 'NENS', 'PACIENTS')] + upload, sep=";")

    # email
    subject = 'Professionals metges de família'
    text = "Bon dia.\n\nAdjuntem arxiu .csv amb les dades actualitzades deL nombre de profesisonals metges de família del SPS dels diferents EAP.\n"
    text += "\n\nSalutacions."
    u.sendGeneral('SISAP <sisap@gencat.cat>',
                    'lidia.domingo@gencat.cat',
                    'roser.cantenys@catsalut.cat',
                    subject,
                    text,
                    file_eap)

    # eliminem arxiu
    u.remove(file_eap)