# coding: latin1

import sisapUtils as u
import collections as c


class Helena():

    def __init__(self):
        self.get_data()
        self.compute_indi()
        self.export_sisap_ecap()

    def get_data(self):
        """ Obtï¿½ les dades necessï¿½ries de la base de dades """

        # Consulta per obtenir pacients atesos de 50 a 74
        # anys assignats a una unitat, no institucionalitzats ni maca
        sql = """
                SELECT
                    id_cip_sec,
                    up,
                    uba,
                    ubainf,
                    edat
                FROM
                    assignada_tot
                WHERE
                    edat BETWEEN 50 AND 74
                    AND ates = 1
                    AND institucionalitzat = 0
                    AND maca = 0
              """
        # Desa la informaciï¿½ dels pacients en un diccionari
        self.atesos_ultim_any = {id_cip_sec: (up, uba, ubainf, edat) for id_cip_sec, up, uba, ubainf, edat in u.getAll(sql, 'nodrizas')}  # noqa

        # Consulta per obtenir pacients atesos en el darrer mes
        self.atesos_ultim_mes = {}
        sql = """
                SELECT
                    id_cip_sec,
                    visi_data_visita
                FROM
                    import.visites1,
                    nodrizas.dextraccio
                WHERE
                    visi_data_visita BETWEEN
                        date_add(data_ext, INTERVAL - 1 MONTH)
                        AND data_ext
                    AND visi_situacio_visita = 'R'
                    AND visi_tipus_visita IN ('9C', '9D', '9R')
              """
        # Desa l'ï¿½ltima visita de cada pacient atï¿½s en el darrer mes
        for id_cip_sec, data_visita in u.getAll(sql, 'import'):
            if id_cip_sec in self.atesos_ultim_any:
                self.atesos_ultim_mes.setdefault(id_cip_sec, data_visita)
                if data_visita < self.atesos_ultim_mes[id_cip_sec]:
                    self.atesos_ultim_mes[id_cip_sec] = data_visita

        # Consulta per obtenir pacients que estan en tractament amb estatines
        sql = """
                SELECT
                    id_cip_sec
                FROM
                    eqa_tractaments
                WHERE
                    farmac = 82
                    AND tancat = 0
              """
        # Desa els pacients que compleixen amb el tractament d'estatines
        self.estatines = {id_cip_sec for id_cip_sec, in u.getAll(sql, 'nodrizas') if id_cip_sec in self.atesos_ultim_any}  # noqa

        # Inicialitza estructures per emmagatzemar
        # dades del cï¿½lcul d'indicadors
        self.regicor_mensual = c.defaultdict()
        self.regicor_anual = c.defaultdict(set)
        self.itb_anual = c.defaultdict(set)
        self.regicor_excl_mensual = set()
        self.regicor_excl_anual = set()
        self.regicor_excl_YK2001 = set()
        regicor_candidats_1 = c.defaultdict(set)
        regicor_candidats_2 = c.defaultdict()
        self.regicor_candidats = set()
        self.itb_0_9 = c.defaultdict(set)
        self.regicor_anual_tipus = c.defaultdict(lambda: c.defaultdict())

        # Consulta per obtenir dades clï¿½niques dels pacients
        sql = """
                SELECT
                    id_cip_sec,
                    vu_cod_vs,
                    vu_val,
                    vu_dat_act,
                    CASE
                        WHEN vu_dat_act >=
                            date_add(data_ext, INTERVAL - 1 MONTH)
                            THEN 1
                        END ultim_mes,
                    CASE
                        WHEN vu_dat_act >=
                            date_add(data_ext, INTERVAL - 12 MONTH)
                            THEN 1
                        END ultim_any,
                    CASE
                        WHEN vu_dat_act <
                            date_add(data_ext, INTERVAL - 1 MONTH)
                            THEN 1
                        END no_ultim_mes,
                    CASE
                        WHEN vu_dat_act <
                            date_add(data_ext, INTERVAL - 12 MONTH)
                            THEN 1
                        END no_ultim_any
                FROM
                    import.variables,
                    nodrizas.dextraccio
                WHERE
                    vu_cod_vs IN ('VK405', 'YK2001',
                            'YK2002', 'TK201D', 'TK201E')
                    AND vu_dat_act BETWEEN
                        date_add(data_ext, INTERVAL - 60 MONTH)
                        AND data_ext
              """
        # Processa la informaciï¿½ per identificar
        # candidats a certs indicadors i exclusions
        for id_cip_sec, variable, val, data_var, ultim_mes, ultim_any, no_ultim_mes, no_ultim_any in u.getAll(sql, 'import'):  # noqa
            if id_cip_sec in self.atesos_ultim_any:

                if variable == 'VK405':
                    if no_ultim_any:
                        self.regicor_excl_anual.add(id_cip_sec)
                    if no_ultim_mes:
                        self.regicor_excl_mensual.add(id_cip_sec)
                    if val >= 7:
                        regicor_candidats_1[id_cip_sec].add((data_var, True))
                    regicor_candidats_1[id_cip_sec].add((data_var, False))

                elif variable in ('YK2001', 'YK2002'):
                    if variable == 'YK2001':
                        self.regicor_excl_YK2001.add(id_cip_sec)
                    elif variable == 'YK2002':
                        if id_cip_sec in self.atesos_ultim_mes:
                            data_visi = self.atesos_ultim_mes[id_cip_sec]
                            if data_visi < data_var:
                                self.regicor_excl_mensual.add(id_cip_sec)
                    if ultim_mes == 1:
                        # Desa la primera mediciï¿½ mensual
                        self.regicor_mensual.setdefault(id_cip_sec, (data_var, val))  # noqa
                        if data_var < self.regicor_mensual[id_cip_sec][0]:
                            self.regicor_mensual[id_cip_sec] = (data_var, val)
                    elif ultim_any == 1:
                        # Desa totes les medicions anuals
                        self.regicor_anual[id_cip_sec].add((data_var, val))
                        # Desa l'ï¿½ltima mediciï¿½ anual
                        self.regicor_anual_tipus[variable].setdefault(id_cip_sec, (data_var, val))  # noqa
                        if self.regicor_anual_tipus[variable][id_cip_sec][0] < data_var:  # noqa
                            self.regicor_anual_tipus[variable][id_cip_sec] = (data_var, val)  # noqa

                elif ultim_any == 1:
                    # Desa les medicions de l'ITB
                    self.itb_anual[id_cip_sec].add((data_var, val))
                    if val < 0.9:
                        self.itb_0_9[id_cip_sec].add(data_var)

        # # Identifica candidats a ser avaluats
        for id_cip_sec in regicor_candidats_1:
            for (data_var, candidat) in regicor_candidats_1[id_cip_sec]:
                if id_cip_sec in regicor_candidats_2:
                    last_date, candidat = regicor_candidats_2[id_cip_sec]
                    if data_var > last_date:
                        regicor_candidats_2[id_cip_sec] = [data_var, candidat]
                else:
                    regicor_candidats_2[id_cip_sec] = [data_var, candidat]

        for id_cip_sec in regicor_candidats_2:
            data_var, candidat = regicor_candidats_2[id_cip_sec]
            if candidat:
                self.regicor_candidats.add(id_cip_sec)

        self.exclusions = c.defaultdict(set)
        # Consulta per obtenir exclusions basades en certs
        # problemes de salut (e.g., MCV, DM1)
        sql = """
                SELECT
                    id_cip_sec
                FROM
                    eqa_problemes
                WHERE
                    ps IN (1, 7, 11, 24)
              """
        self.exclusions['PS'] = {id_cip_sec for id_cip_sec, in u.getAll(sql, 'nodrizas') if id_cip_sec in self.atesos_ultim_any}  # noqa

        # Obtï¿½ els hashes dels pacients
        sql = """
                SELECT
                    id_cip_sec,
                    hash_d
                FROM
                    u11
              """
        conversor = {hash: id_cip_sec for id_cip_sec, hash in u.getAll(sql, 'import') if id_cip_sec in self.atesos_ultim_any}  # noqa

        # Consulta per obtenir dades addicionals
        # sobre els pacients (formularis de REGICOR)
        sql = """
                SELECT
                    hash_redics,
                    trunc(DATA),
                    VALUE_ARCH
                FROM
                    DWCATSALUT.FORMULARIS_REGICOR r
                INNER JOIN
                    dwsisap.PDPTB101_RELACIO_nia p
                ON
                    substr(r.cip, 1, 13) = p.cip
                WHERE
                    codi = 'V000000015190'
                    AND VALUE_ARCH IN (0, 1, 2)
              """
        self.regicor_is3 = c.defaultdict(lambda: c.defaultdict(set))
        for hash_redics, data_regicor, val in u.getAll(sql, 'exadata'):
            val = int(val)
            id_cip_sec = conversor.get(hash_redics)
            try:
                data_regicor = data_regicor.date()
            except:
                data_regicor = data_regicor
            if id_cip_sec and val in (1, 2):
                self.regicor_is3[val][id_cip_sec].add(data_regicor)
            if id_cip_sec and val in (0, 1):
                self.itb_anual[id_cip_sec].add((data_regicor, val))

    def compute_indi(self):
        """ Calcula els indicadors a partir de les dades obtingudes """

        indicadors = c.defaultdict(lambda: c.defaultdict(set))

        # Cï¿½lcul de l'indicador REGICOR9MES
        for id_cip_sec in self.atesos_ultim_mes:
            if id_cip_sec not in self.exclusions['PS'] and id_cip_sec not in self.regicor_excl_mensual:  # noqa
                indicadors['REGICOR9MES']['DEN'].add(id_cip_sec)
                if id_cip_sec in self.regicor_mensual:
                    if self.atesos_ultim_mes[id_cip_sec] <= self.regicor_mensual[id_cip_sec][0]:  # noqa
                        indicadors['REGICOR9MES']['NUM'].add(id_cip_sec)

        # Cï¿½lcul de l'indicador REGICOR9ANY
        for id_cip_sec in self.atesos_ultim_any.keys():
            if id_cip_sec not in self.exclusions['PS'] and id_cip_sec not in self.regicor_excl_anual:  # noqa
                indicadors['REGICOR9ANY']['DEN'].add(id_cip_sec)
                if id_cip_sec in self.regicor_anual:
                    indicadors['REGICOR9ANY']['NUM'].add(id_cip_sec)

        # Cï¿½lcul de l'indicador REGICOR9ITB
        for id_cip_sec in self.atesos_ultim_any.keys():
            if id_cip_sec in self.regicor_anual_tipus['YK2001'] and id_cip_sec in self.regicor_anual_tipus['YK2002']:  # noqa
                data_YK2001, val_YK2001 = self.regicor_anual_tipus['YK2001'][id_cip_sec]  # noqa
                data_YK2002, val_YK2002 = self.regicor_anual_tipus['YK2002'][id_cip_sec]  # noqa
                if (val_YK2001 >= 7 and val_YK2002 >= 7) and (data_YK2001 == data_YK2002):  # noqa
                    indicadors['REGICOR9ITB']['DEN'].add(id_cip_sec)
                    if id_cip_sec in self.itb_anual:
                        for (data_ITB, _) in self.itb_anual[id_cip_sec]:
                            if data_ITB >= data_YK2001:
                                indicadors['REGICOR9ITB']['NUM'].add(id_cip_sec)  # noqa

        # Cï¿½lcul de l'indicador REGICOR9STATIN
        for id_cip_sec in self.atesos_ultim_any.keys():
            if id_cip_sec in self.regicor_anual_tipus['YK2001'] and id_cip_sec in self.regicor_anual_tipus['YK2002'] and (id_cip_sec in self.regicor_is3[1] or id_cip_sec in self.itb_0_9):  # noqa
                data_YK2001, val_YK2001 = self.regicor_anual_tipus['YK2001'][id_cip_sec]  # noqa
                data_YK2002, val_YK2002 = self.regicor_anual_tipus['YK2002'][id_cip_sec]  # noqa
                if (val_YK2001 >= 7 and val_YK2002 >= 7) and (data_YK2001 == data_YK2002):  # noqa
                    if id_cip_sec in self.itb_0_9:
                        for data_patologic in self.itb_0_9[id_cip_sec]:
                            if data_patologic >= data_YK2001:
                                indicadors['REGICOR9STATIN']['DEN'].add(id_cip_sec)  # noqa
                                if id_cip_sec in self.estatines:
                                    indicadors['REGICOR9STATIN']['NUM'].add(id_cip_sec)  # noqa
                    if id_cip_sec in self.regicor_is3[1]:
                        for data_patologic in self.regicor_is3[1][id_cip_sec]:
                            if data_patologic >= data_YK2001:
                                indicadors['REGICOR9STATIN']['DEN'].add(id_cip_sec)  # noqa
                                if id_cip_sec in self.estatines:
                                    indicadors['REGICOR9STATIN']['NUM'].add(id_cip_sec)  # noqa

        # Cï¿½lcul de l'indicador REGICOR9
        indicadors['REGICOR9']['DEN'] = (self.regicor_candidats & set(self.atesos_ultim_any.keys())) - self.regicor_excl_YK2001  # noqa
        indicadors['REGICOR9']['NUM'] = set(((self.regicor_candidats & set(self.atesos_ultim_any.keys())) - self.regicor_excl_YK2001) - self.estatines)  # noqa

        # Procï¿½s similar per calcular els altres indicadors
        # Aquests es basen en la combinaciï¿½ de diferents variables i condicions

        # Desa la informaciï¿½ final a la base de dades
        patients_table = []
        for indi in ('REGICOR9MES', 'REGICOR9ANY', 'REGICOR9ITB', 'REGICOR9STATIN', 'REGICOR9'):  # noqa
            for id_cip_sec in indicadors[indi]['DEN']:
                up, uba, ubainf, edat = self.atesos_ultim_any[id_cip_sec]
                num = 1 if id_cip_sec in indicadors[indi]['NUM'] else 0
                patients_table.append((indi, id_cip_sec, up, uba, ubainf, edat, 1, num))  # noqa

        cols = """(indicador varchar(32), pacient int, up varchar(5), uba varchar(5), uba_inf varchar(5), edat int, den int, num int)"""  # noqa
        u.createTable('regicor_helena', cols, 'altres', rm=True)
        u.listToTable(patients_table, 'regicor_helena', 'altres')
        u.execute("ALTER TABLE regicor_helena ADD INDEX idx_pacient (pacient)", "altres")  # noqa

    def export_sisap_ecap(self):
        """ Exporta els indicadors i catï¿½legs necessaris per SISAP ECAP """

        # Defineix el catï¿½leg d'indicadors
        up_cataleg = [
            ('REGICOR9MES', "Nou regicor calculat en l'�ltim mes", 1, 'HELENA', 1, 0, 0, 0, 0, 1, ''),  # noqa
            ('REGICOR9ANY', "Nou regicor calculat en els �ltims 12 mesos", 2, 'HELENA', 1, 0, 0, 0, 0, 1, ''),  # noqa
            ('REGICOR9ITB', "Nou regicor calculat amb ITB registrat", 3, 'HELENA', 1, 0, 0, 0, 0, 1, ''),  # noqa
            ('REGICOR9STATIN', "Nou regicor alterat amb estatines prescrites", 4, 'HELENA', 1, 0, 0, 0, 0, 1, ''),  # noqa
            ('REGICOR9', "Pacients candidats a calcular el nou regicor", 5, 'HELENA', 1, 0, 0, 0, 0, 1, '')]  # noqa
        cols = """(indi varchar(30), desc_indi varchar(250), ordre int, pare varchar(50), llistat int, invers int, mmin int, mint int, mmax int, toshow int, wiki varchar(250))"""  # noqa
        u.createTable('exp_ecap_helena_cat', cols, 'altres', rm=True)
        u.listToTable(up_cataleg, 'exp_ecap_helena_cat', 'altres')
        u.exportPDP('select * from altres.exp_ecap_helena_cat', 'helenacataleg', datAny=True)  # noqa

        # Exporta la relaciï¿½ d'indicadors pare
        up_cataleg_pare = [('HELENA', 'Indicadors REGICOR', 1)]
        cols = """(pare varchar(50), descripcio varchar(250), ordre int)"""
        u.createTable('exp_ecap_helena_cat_pare', cols, 'altres', rm=True)
        u.listToTable(up_cataleg_pare, 'exp_ecap_helena_cat_pare', 'altres')
        u.exportPDP('select * from altres.exp_ecap_helena_cat_pare', 'helenacatalegpare', datAny=True)  # noqa

        # Exporta informaciï¿½ addicional de pacients
        sql = """
                SELECT
                    up,
                    uba,
                    'M',
                    indicador,
                    hash_d,
                    codi_sector,
                    0
                FROM
                    altres.regicor_helena,
                    import.u11
                WHERE
                    pacient = id_cip_sec
                    and ((num = 0 and indicador in ('REGICOR9ANY', 'REGICOR9MES', 'REGICOR9ITB', 'REGICOR9STATIN'))
                    or (num = 1 and indicador = 'REGICOR9'))
                    AND UBA IS NOT NULL
                union
                SELECT
                    up,
                    uba_inf,
                    'I',
                    indicador,
                    hash_d,
                    codi_sector,
                    0
                FROM
                    altres.regicor_helena,
                    import.u11
                WHERE
                    pacient = id_cip_sec
                    and ((num = 0 and indicador in ('REGICOR9ANY', 'REGICOR9MES', 'REGICOR9ITB', 'REGICOR9STATIN'))
                    or (num = 1 and indicador = 'REGICOR9'))
                    AND UBA_INF IS NOT NULL
              """
        up_llistats = [(up, uba, tipus, indi, hash, sector, excl) for up, uba, tipus, indi, hash, sector, excl in u.getAll(sql, 'altres')]  # noqa
        cols = """(up varchar(5), uba varchar(5), tipus varchar(5), indicador varchar(50), hash_d varchar(40), codi_sector varchar(5), exclos int)"""  # noqa
        u.createTable('exp_ecap_helena_pac', cols, 'altres', rm=True)
        u.listToTable(up_llistats, 'exp_ecap_helena_pac', 'altres')
        u.exportPDP("""select * from altres.exp_ecap_helena_pac
            where up in ('00040',
            '00042',
            '00045',
            '00047',
            '00048',
            '00059',
            '00061',
            '00062',
            '00041',
            '00044',
            '00050',
            '00051',
            '00052',
            '00054',
            '00055',
            '00060',
            '00065',
            '00066',
            '00068',
            '01097',
            '00101',
            '00102',
            '00103',
            '00104',
            '00105',
            '00106',
            '00107',
            '00108',
            '00109',
            '00110',
            '00111',
            '00112',
            '00114',
            '00115',
            '00116',
            '00119',
            '00120',
            '00121',
            '00122',
            '00124',
            '00129',
            '00130',
            '00133',
            '00274',
            '00275',
            '00276',
            '00278',
            '00283',
            '00291',
            '00293',
            '00295',
            '00296',
            '05166',
            '00196',
            '00273',
            '00277',
            '00279',
            '00280',
            '00281',
            '00282',
            '00288',
            '00302',
            '00303',
            '00305',
            '00309',
            '01083',
            '00285',
            '00286',
            '00306',
            '00307',
            '04054',
            '04055',
            '00336',
            '00337',
            '00339',
            '00341',
            '00348',
            '00349',
            '00351',
            '00362',
            '00364',
            '00366',
            '00367',
            '00370',
            '00373',
            '00385',
            '00386',
            '00340',
            '00345',
            '00346',
            '00347',
            '00350',
            '00353',
            '00354',
            '00363',
            '00365',
            '00368',
            '00372',
            '00374',
            '00377',
            '00378',
            '00379',
            '00380',
            '00381',
            '00391',
            '00395',
            '00396',
            '00397',
            '01122',
            '01123',
            '04704',
            '04713',
            '00371',
            '00338',
            '00342',
            '00343',
            '00344',
            '00356',
            '00357',
            '00358',
            '00359',
            '00360',
            '00361',
            '00369',
            '00376',
            '00387',
            '00388',
            '00389',
            '00390',
            '04548',
            '05945',
            '06187',
            '06188',
            '06189',
            '00043',
            '00046',
            '02791',
            '00117',
            '01886',
            '00127',
            '00702',
            '07086') and uba != ''""", 'helenallistats', truncate=True)  # noqa

        # Exporta els resultats dels indicadors
        sql = """
                SELECT
                    indicador,
                    up,
                    uba,
                    'M',
                    sum(num),
                    sum(den)
                FROM
                    altres.regicor_helena
                GROUP BY
                    indicador,
                    up,
                    uba
                UNION
                SELECT
                    indicador,
                    up,
                    uba_inf,
                    'I',
                    sum(num),
                    sum(den)
                FROM
                    altres.regicor_helena
                WHERE
                    uba_inf != ''
                GROUP BY
                    indicador,
                    up,
                    uba_inf
              """
        up_uba = [(up, uba, tipus, indicador, num, den, float(num)/float(den)) for indicador, up, uba, tipus, num, den in u.getAll(sql, 'altres')]  # noqa
        cols = """(up varchar(5), uba varchar(5), tipus varchar(1), indicador varchar(50), num int, den int, res decimal(36,4))"""  # noqa
        u.createTable('exp_ecap_helena', cols, 'altres', rm=True)
        u.listToTable(up_uba, 'exp_ecap_helena', 'altres')
        u.exportPDP("""select * from altres.exp_ecap_helena where up in ('00040',
                    '00042',
                    '00045',
                    '00047',
                    '00048',
                    '00059',
                    '00061',
                    '00062',
                    '00041',
                    '00044',
                    '00050',
                    '00051',
                    '00052',
                    '00054',
                    '00055',
                    '00060',
                    '00065',
                    '00066',
                    '00068',
                    '01097',
                    '00101',
                    '00102',
                    '00103',
                    '00104',
                    '00105',
                    '00106',
                    '00107',
                    '00108',
                    '00109',
                    '00110',
                    '00111',
                    '00112',
                    '00114',
                    '00115',
                    '00116',
                    '00119',
                    '00120',
                    '00121',
                    '00122',
                    '00124',
                    '00129',
                    '00130',
                    '00133',
                    '00274',
                    '00275',
                    '00276',
                    '00278',
                    '00283',
                    '00291',
                    '00293',
                    '00295',
                    '00296',
                    '05166',
                    '00196',
                    '00273',
                    '00277',
                    '00279',
                    '00280',
                    '00281',
                    '00282',
                    '00288',
                    '00302',
                    '00303',
                    '00305',
                    '00309',
                    '01083',
                    '00285',
                    '00286',
                    '00306',
                    '00307',
                    '04054',
                    '04055',
                    '00336',
                    '00337',
                    '00339',
                    '00341',
                    '00348',
                    '00349',
                    '00351',
                    '00362',
                    '00364',
                    '00366',
                    '00367',
                    '00370',
                    '00373',
                    '00385',
                    '00386',
                    '00340',
                    '00345',
                    '00346',
                    '00347',
                    '00350',
                    '00353',
                    '00354',
                    '00363',
                    '00365',
                    '00368',
                    '00372',
                    '00374',
                    '00377',
                    '00378',
                    '00379',
                    '00380',
                    '00381',
                    '00391',
                    '00395',
                    '00396',
                    '00397',
                    '01122',
                    '01123',
                    '04704',
                    '04713',
                    '00371',
                    '00338',
                    '00342',
                    '00343',
                    '00344',
                    '00356',
                    '00357',
                    '00358',
                    '00359',
                    '00360',
                    '00361',
                    '00369',
                    '00376',
                    '00387',
                    '00388',
                    '00389',
                    '00390',
                    '04548',
                    '05945',
                    '06187',
                    '06188',
                    '06189',
                    '00043',
                    '00046',
                    '02791',
                    '00117',
                    '01886',
                    '00127',
                    '00702',
                    '07086')""", 'helenaindicadors', dat=True)  # noqa

        # Exporta la data d'extracciï¿½ de dades
        u.exportPDP('select data_ext from nodrizas.dextraccio', 'helenaLlistatsData', truncate=True)  # noqa


# Executa la classe si s'executa el script directament
if __name__ == "__main__":
    Helena()
