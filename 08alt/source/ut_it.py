
import sisapUtils as u
import collections as c
import datetime as d
from dateutil import relativedelta

DEXTD = u.getOne('select data_ext from dextraccio', 'nodrizas')[0]

UTS = ['15981', '15774', '15795', '15978']

class UT_IT():
    def __init__(self):
        u.printTime('inici')
        self.dades = c.defaultdict(set)
        self.get_assistencial()
        u.printTime('get_assistencial')
        self.get_its()
        u.printTime('get_its')
        self.get_visites()
        u.printTime('get_visites')
        # self.get_ordres_cliniques()
        self.get_derivacions()
        u.printTime('get_derivacions')
        self.get_proves()
        u.printTime('get_proves')
        self.find_min_data_visita()
        u.printTime('get_mins')
        self.get_indicadors()
        u.printTime('get_indicadors')
        self.build_indicadors()
        u.printTime('build_indicadors')
        self.upload_table()
        u.printTime('fi')

    def get_assistencial(self):
        sql = """SELECT hash
                    FROM dwcatsalut.rca_pacients A, DWSISAP.DBC_POBLACIO B
                    WHERE A.ASS_NIA = B.NIA AND A.afiliacio_taf = 'AC' AND  A.afiliacio_raec = 'T'
                    AND A.ASS_CSA = 'A'"""
        self.assistencial = set()
        for hash, in u.getAll(sql, 'exadata'):
            self.assistencial.add(hash)

    def get_proves(self):
        codis_emg = ['PD00005', 'PD00007', 'PD00008', 'PD00010', 'PD00649']
        sql = """
            SELECT hash, oc_data, agrupador_prova
            FROM dwsisap.master_proves
            WHERE agrupador_prova in ('GRUP RM', 'GRUP ECOGRAFIES')
            OR inf_codi_prova in {codis_emg}
        """.format(codis_emg=tuple(codis_emg))
        for hash, data, agrupador in u.getAll(sql, 'exadata'):
            if hash in self.pob:
                alta = self.pob[hash]['alta']
                baixa = self.pob[hash]['baixa']
                if alta and isinstance(alta, d.datetime):
                    alta = alta.date()
                if isinstance(baixa, d.datetime):
                    baixa = baixa.date()
                if isinstance(data, d.datetime):
                    data = data.date()
                if data >= baixa and alta and data <= alta:
                    self.dades['OC'].add((hash, self.pob[hash]['it']))
                    if agrupador in ('GRUP RM', 'GRUP ECOGRAFIES'):
                        self.dades[agrupador].add((hash, self.pob[hash]['it']))
                    else:
                        self.dades['GRUP EMG'].add((hash, self.pob[hash]['it']))

    def upload_table(self):
        cols = "(indicador varchar(10), up varchar(10), analisi varchar(3), val int)"
        upload = []
        for indicador, up, analisi in self.resultat:
            upload.append((indicador, up, analisi, self.resultat[(indicador, up, analisi)]))
        u.createTable('ut_it', cols, 'altres', rm=True)
        u.listToTable(upload, 'ut_it', 'altres')

    def build_indicadors(self):
        # UTIT01: numero de visites presencials amb IT finalitzada
        # UTIT02: numero de visites no presencials amb IT finalitzada
        # UTIT03: numero de visites presencials amb IT finalitzada / numero de pacients amb IT finalitzada
        # UTIT03A: numero de visites presencials del metge de la UTIT finalitzada / numero de pacients amb IT finalitzada
        # UTIT03B: numero de visites presencials del fisio de la UTIT finalitzada / numero de pacients amb IT finalitzada
        # UTIT04: numero de pacients amb IT finalitzada i una visita a fisio / numero de pacients amb IT finalitzada
        # UTIT05: numero de pacients amb IT finalitzada i > 70 dies de primera visita / numero de pacients amb > 70 dies de primera visita
        # UTIT06: numero de dies entre primera visita i data alta en IT finalitzada / numero IT finalitzada amb visita UTIT
        # UTIT06A: numero de dies entre primera visita i data alta en IT finalitzada / numero IT finalitzada amb visita UTIT amb fisio
        # UTIT06B: numero de dies entre primera visita i data alta en IT finalitzada / numero IT finalitzada sense visita UTIT amb fisio
        # UTIT07: numero de pacients amb IT finalitzada amb OC / numero de pacients amb IT finalitzada
        # UTIT07A: numero de pacients amb IT finalitzada amb RMN / numero de pacients amb IT finalitzada
        # UTIT07B: numero de pacients amb IT finalitzada amb EMG / numero de pacients amb IT finalitzada
        # UTIT07C: numero de pacients amb IT finalitzada amb ECO / numero de pacients amb IT finalitzada
        # UTIT08: numero de pacients amb IT finalitzada i derivacio (REHA/TRAUMA) / numero de pacients amb IT finalitzada
        # UTIT08A: 08 pero nomes REHA
        # UTIT08B: 08 pero nomes TRAUMA
        self.resultat = c.Counter()
        for hash, it, visita in self.dades['presencial']:
            if hash in self.ups:
                up = self.ups[hash]
                self.resultat[('UTIT01', up, 'NUM')] += 1
                self.resultat[('UTIT03', up, 'NUM')] += 1

        for hash, it, visita in self.dades['telefonica']:
            if hash in self.ups:
                up = self.ups[hash]
                self.resultat[('UTIT02', up, 'NUM')] += 1
        
        for hash, it, visita in self.dades['visita metge finalitzada']:
            if hash in self.ups:
                up = self.ups[hash]
                self.resultat[('UTIT03A', up, 'NUM')] += 1
        
        for hash, it, visita in self.dades['visita fisio finalitzada']:
            if hash in self.ups:
                up = self.ups[hash]
                self.resultat[('UTIT03B', up, 'NUM')] += 1

        for hash, dies, it in self.dades['dies']:
            if hash in self.ups:
                up = self.ups[hash]
                self.resultat[('UTIT06', up, 'NUM')] += dies
        
        for hash, dies, it in self.dades['dies fisio']:
            if hash in self.ups:
                up = self.ups[hash]
                self.resultat[('UTIT06A', up, 'NUM')] += dies

        for hash, dies, it in self.dades['dies no fisio']:
            if hash in self.ups:
                up = self.ups[hash]
                self.resultat[('UTIT06B', up, 'NUM')] += dies
        
        analisi_num = {
            'UTIT04': self.dades['visita fisio'],
            'UTIT05': self.dades['> 70 dies finalitzades'],
            'UTIT07': self.dades['OC'],
            'UTIT07A': self.dades['GRUP RM'],
            'UTIT07B': self.dades['GRUP EMG'],
            'UTIT07C': self.dades['GRUP ECOGRAFIES'],
            'UTIT08': self.dades['derivacions'],
            'UTIT08A': self.dades['REHABILITACIO'],
            'UTIT08B': self.dades['TRAUMATOLOGIA']
        }
        analisi_den = {
            'UTIT03': self.dades['finalitzada'],
            'UTIT03A': self.dades['finalitzada'],
            'UTIT03B': self.dades['finalitzada'],
            'UTIT04': self.dades['finalitzada'],
            'UTIT05': self.dades['> 70 dies totes'],
            'UTIT06': self.dades['ateses finalitzades'],
            'UTIT06A': self.dades['fisio ateses finalitzades'],
            'UTIT06B': self.dades['no fisio ateses finalitzades'],
            'UTIT07': self.dades['finalitzada'],
            'UTIT07A': self.dades['finalitzada'],
            'UTIT07B': self.dades['finalitzada'],
            'UTIT07C': self.dades['finalitzada'],
            'UTIT08': self.dades['finalitzada'],
            'UTIT08A': self.dades['finalitzada'],
            'UTIT08B': self.dades['finalitzada']
        }
        for indicador in analisi_den:
            dades = analisi_den[indicador]
            # tipus = analisi[indicador]['tipus']
            print(indicador + ' DEN')
            for hash, it in dades:
                if hash in self.ups:
                    up = self.ups[hash]
                    self.resultat[(indicador, up, 'DEN')] += 1
        for indicador in analisi_num:
            dades = analisi_num[indicador]
            # tipus = analisi[indicador]['tipus']
            print(indicador + ' NUM')
            for hash, it in dades:
                if hash in self.ups:
                    up = self.ups[hash]
                    self.resultat[(indicador, up, 'NUM')] += 1


    def find_min_data_visita(self):
        self.mins = c.defaultdict()
        fisio = {}
        for hash in self.visites:
            # self.visites[hash].add((up, data, 'presencial', servei))
            for up, it, id_visita, data, tipus, servei, prof in self.visites[hash]:
                if (hash, it) in fisio:
                    if not fisio[(hash, it)] and servei == 'REHA' and up in UTS:
                        fisio[(hash, it)] = True
                else:
                    fisio[(hash, it)] = True if servei == 'REHA' and up in UTS else False
                if (hash, it) in self.mins and tipus == 'presencial' and up in UTS:
                    data_min = self.mins[(hash, it)]['data']
                    if data <= data_min:
                        self.mins[(hash, it)] = {
                            'data': data,
                            'it': it,
                            'up': up,
                            'tipus': tipus,
                            'servei': servei,
                            'fisio': fisio[(hash, it)]
                        }
                else:
                    self.mins[(hash, it)] = {
                        'data': data,
                        'it': it,
                        'up': up,
                        'tipus': tipus,
                        'servei': servei,
                        'fisio': fisio[(hash, it)]
                    }


    def get_indicadors(self):
        for hash in self.visites:
            for up, it, id_visita, data, tipus, servei, prof in self.visites[hash]:
                if self.pob[hash]['alta'] is not None:
                    self.dades[tipus].add((hash, it, id_visita))
                    if servei == 'REHA':
                        self.dades['visita fisio'].add((hash, it))
                    if tipus == 'presencial' and prof and prof[0] == 'M' and up in UTS:
                        self.dades['visita metge finalitzada'].add((hash, it, id_visita))
                    elif tipus == 'presencial' and servei == 'REHA' and up in UTS:
                        self.dades['visita fisio finalitzada'].add((hash, it, id_visita))
        
        for hash in self.pob:
            if self.pob[hash]['alta'] is not None:
                self.dades['finalitzada'].add((hash, self.pob[hash]['it']))
            self.dades['totes'].add((hash, self.pob[hash]['it']))
        
        for (hash, it) in self.mins:
            data = self.mins[(hash, it)]['data'].date()
            self.dades['ateses finalitzades'].add((hash, it))
            if self.mins[(hash, it)]['fisio']:
                self.dades['fisio ateses finalitzades'].add((hash, it))
            else:
                self.dades['no fisio ateses finalitzades'].add((hash, it))
            if hash in self.pob:
                data_it = self.pob[hash]['alta'].date() if self.pob[hash]['alta'] is not None else DEXTD
                dies = (data_it-data).days
                if dies > 70:
                    self.dades['> 70 dies totes'].add((hash, it))
                if self.pob[hash]['alta'] is not None:
                    dies = (data_it-data).days
                    self.dades['dies'].add((hash, dies, it))
                    if self.mins[(hash, it)]['fisio']:
                        self.dades['dies fisio'].add((hash, dies, it))
                    if dies > 70:
                        self.dades['> 70 dies finalitzades'].add((hash, it))
        for (hash, dies, it) in self.dades['dies']:
            if (hash, dies, it) not in self.dades['dies fisio']:
                self.dades['dies no fisio'].add((hash, dies, it))
        
        for hash in self.derivacions:
            for it, data, servei in self.derivacions[hash]:
                self.dades['derivacions'].add((hash, self.pob[hash]['it']))
                self.dades[servei].add((hash, self.pob[hash]['it']))

    def get_derivacions(self):
        sql = """
            SELECT hash, inf_servei_d_codi_desc, oc_data
            FROM dwsisap.master_derivacions
            WHERE inf_servei_d_codi_desc in ('REHABILITACIO', 'TRAUMATOLOGIA')
        """
        self.derivacions = c.defaultdict(set)
        for hash, servei, data in u.getAll(sql, 'exadata'):
            if hash in self.pob:
                if self.pob[hash]['alta'] is not None:
                    dins_it = visita_it(data, self.pob[hash])
                    if dins_it:
                        self.derivacions[hash].add((self.pob[hash]['it'], data, servei))

    def _iterate_workers(self, resultat, objecte, ups, assistencial):
        """."""
        # self.visites[hash].add((up, self.pob[hash]['it'], visita, data, 'presencial', servei))
        for worker in resultat:
            for hash in worker:
                for up, it, visita, data, tipus, servei, prof in worker[hash]:
                    objecte[hash].add((up, it, visita, data, tipus, servei, prof))
                    if up in UTS and hash in assistencial:
                        ups[hash] = up

    def get_visites(self):
        self.ups = {}
        mesos_a_tractar = []
        for i in range (0,12):
            mes_inf = DEXTD - relativedelta.relativedelta(months=i+1)
            mes_sup = DEXTD - relativedelta.relativedelta(months=i)
            mesos_a_tractar.append((mes_inf, mes_sup))
        sql = """
            SELECT pacient, data, visi_id, up, tipus, sisap_servei_codi, sisap_catprof_class
            FROM dwsisap.sisap_master_visites
            WHERE tipus in ('9C', '9R', '9T', '9E')
            AND data between DATE '{mes_inf}' AND DATE '{mes_sup}'
            -- AND data between add_months(DATE 'DEXTD', -12) AND DATE 'DEXTD'
        """
        jobs = [(sql, mes_inf, mes_sup, self.pob) for (mes_inf, mes_sup) in mesos_a_tractar]
        vis = u.multiprocess(sub_get_visites, jobs, 6)
        self.visites = c.defaultdict(set)
        self._iterate_workers(vis, self.visites, self.ups, self.assistencial)
        # for hash, data, visita, up, tipus, servei in u.getAll(sql.format(mes_inf=mes_inf, mes_sup=mes_sup), 'exadata'):
        #     if hash in self.pob:
        #         dins_it = self.visita_it(data, self.pob[hash])
        #         if dins_it == 1:
        #             if tipus in ('9C', '9R'):
        #                 self.visites[hash].add((up, self.pob[hash]['it'], visita, data, 'presencial', servei))
        #             else:
        #                 self.visites[hash].add((up, self.pob[hash]['it'], visita, data, 'telefonica', servei))

    def get_its(self):
        sql = """
            SELECT b.hash, id_it, data_baixa, data_alta, up_baixa, up_alta, a.up, a.uba
            FROM dwsisap.master_it a
            INNER JOIN dwsisap.rca_cip_nia b
            ON a.cip = substr(b.cip, 0, 13)
            WHERE osteomuscular = 1
            AND tipus_it = 'AS'
        """
        self.pob = c.defaultdict()
        for hash, it, baixa, alta, up_baixa, up_alta, up, uba in u.getAll(sql, 'exadata'):
            if alta is None:
                dies = (DEXTD - baixa.date()).days
            else:
                dies = (alta.date() - baixa.date()).days
            if dies >= 45 and dies <= 365:
                self.pob[hash] = {
                    'baixa': baixa,
                    'alta': alta,
                    'up_baixa': up_baixa,
                    'up_alta': up_alta,
                    'up': up,
                    'uba': uba,
                    'it': it
                }
        print(len(self.pob))

def visita_it(visita, dades):
        baixa = dades['baixa'].date()
        alta = dades['alta'].date() if dades['alta'] is not None else DEXTD
        if visita.date() >= baixa and visita.date() <= alta:
            return 1
        return 0

def sub_get_visites(params):
    sql, mes_inf, mes_sup, pob = params
    print(str(mes_inf) + ' ' + str(mes_sup))
    visites = c.defaultdict(set)
    for hash, data, visita, up, tipus, servei, professional in u.getAll(sql.format(mes_inf=mes_inf, mes_sup=mes_sup), 'exadata'):
        if hash in pob:
            dins_it = visita_it(data, pob[hash])
            if dins_it == 1:
                if tipus in ('9C', '9R'):
                    visites[hash].add((up, pob[hash]['it'], visita, data, 'presencial', servei, professional))
                else:
                    visites[hash].add((up, pob[hash]['it'], visita, data, 'telefonica', servei, professional))
    return visites

UT_IT()