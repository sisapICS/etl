# -*- coding: latin1 -*-

"""
.
"""

import collections as c

import sisapUtils as u


DEBUG = False
TABLE = "SISAP_CENTRES_VISITES"
DATABASE = "pdp"


class Visites(object):
    """."""

    def __init__(self):
        """."""
        self.get_cataleg()
        self.get_jobs()
        self.get_visites()
        self.upload_data()

    def get_cataleg(self):
        """."""
        sql = "select sector, codi, classe, oficial from sisap_centres_cataleg"
        self.cataleg = {row[:3]: row[3] for row in u.getAll(sql, DATABASE)}

    def get_jobs(self):
        """."""
        inici = (2017, 1)
        final = u.getOne("select year(data_ext), month(data_ext) from dextraccio", "nodrizas")
        self.jobs = []
        sql = "select year(visi_data_visita), month(visi_data_visita) from {} limit 1"  # noqa
        for taula in u.getSubTables("visites"):
            this = u.getOne(sql.format(taula), "import")
            if inici <= this <= final:
                self.jobs.append((taula, this))

    def get_visites(self):
        """."""
        self.visites = []
        if DEBUG:
            dades = [self._get_visites_worker(self.jobs[0])]
        else:
            dades = u.multiprocess(self._get_visites_worker, self.jobs, 12)
        for periode, recompte in dades:
            for key, n in recompte:
                self.visites.append(periode + (key, n["c"], n["t"]))

    def upload_data(self):
        """."""
        cols = "(exercici int, periode int, centre varchar(10), visites int, \
                 total int)"
        u.createTable(TABLE, cols, DATABASE, rm=True)
        u.listToTable(self.visites, TABLE, DATABASE)

    def _get_visites_worker(self, params):
        """."""
        taula, periode = params
        visites = c.defaultdict(c.Counter)
        sql = "select codi_sector, visi_centre_codi_centre, \
                      visi_centre_classe_centre, \
                      case when visi_lloc_visita = 'C' and \
                                visi_tipus_visita not in ('9T', '9E') \
                      then 1 else 0 end \
               from {} \
               where visi_situacio_visita = 'R'".format(taula)
        for row in u.getAll(sql, "import"):
            key = row[:-1]
            centre = row[-1]
            if key in self.cataleg:
                visites[self.cataleg[key]]["t"] += 1
                if centre:
                    visites[self.cataleg[key]]["c"] += 1
        return (periode, [row for row in visites.items()])


if __name__ == "__main__":
    if u.IS_MENSUAL:
        Visites()
