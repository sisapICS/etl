import os
import os.path
import ssl
import urllib

import sisapUtils as u


TABLE = "exp_dedicacio"
DATABASE = "altres"
ARXIU = "DEDICACIO"

ARXIU_TMP = os.path.join(u.tempFolder, "escreix.csv")
URL = "http://p400.ecap.intranet.gencat.cat/sisap/csv/escreix/hds6ds87d/2876cvhcx7"  # noqa
CTX = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
CTX.set_ciphers('HIGH:!DH:!aNULL')
urllib.urlretrieve(url=URL, filename=ARXIU_TMP, context=CTX)


sql = "select a.ide_dni, concat(b.ics_codi, a.tipus, a.uab) \
       from import.cat_professionals a \
       inner join nodrizas.cat_centres b on a.up = b.scs_codi \
       where a.uab <> '' and b.ep = '0208'"
ubas = {u.getNif(row[0]): row[1] for row in u.getAll(sql, "import")}

ara_y = int(u.getKhalixDates()[0])
ara_m = int(u.getKhalixDates()[2])
abans_y = ara_y if ara_m > 1 else (ara_y - 1)
abans_m = (ara_m - 1) if ara_m > 1 else 12
existeixen = set([(int(year), int(mes))
                  for nif, br, cat, mes, year, tip_c, tip, val
                  in u.readCSV(ARXIU_TMP, sep=";", header=True)])
periodes = set([(ara_y, ara_m), (abans_y, abans_m)]) & existeixen

escreix = {(ubas[nif], int(year), int(mes)): (float(val)/100)
           for nif, br, cat, mes, year, tip_c, tip, val
           in u.readCSV(ARXIU_TMP, sep=";", header=True)
           if nif in ubas and (int(year), int(mes)) in periodes}
os.remove(ARXIU_TMP)

dedicacio = []
sql = "select concat(b.ics_codi, a.tipus, a.uba) \
       from (select * from eqa_ind.mst_ubas \
             union select * from pedia.mst_ubas) a \
       inner join nodrizas.cat_centres b on a.up = b.scs_codi \
       where b.ep = '0208'"
for uba, in u.getAll(sql, "import"):
    for y, m in periodes:
        periode = "A" + str(y)[-2:] + str(m).zfill(2)
        valor = 1 + escreix.get((uba, y, m), 0)
        this = (periode, uba, valor)
        dedicacio.append(this)


cols = "(periode varchar(16), uba varchar(16), dedicacio double)"
u.createTable(TABLE, cols, DATABASE, rm=True)
u.listToTable(dedicacio, TABLE, DATABASE)
sql = "select 'DEDICACIO', periode, uba, 'NOCLI', 'NOCAT', 'NOIMP',\
              'DIM6SET', 'N', dedicacio \
       from {}.{}".format(DATABASE, TABLE)
u.exportKhalix(sql, ARXIU)
