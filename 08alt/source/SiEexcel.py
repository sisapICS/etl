# coding: iso-8859-1
from sisapUtils import *
import sys,datetime
from time import strftime
from collections import defaultdict,Counter
import re

prstb690="SISAP_XML"
redics="redics"
nod="nodrizas"
alt="altres"

#Campos que se quieren coger

camps_codi=['SiE_Curs','Pais','SiE_Escola','SexePacient','SiE_Origen_Visita','SiE_TipVisita_A',
            'SiE_TipVisita_B','SiE_TipVisita_C','SiE_TipVisita_D','SiE_Aliment_A',"SiE_Aliment_Altres",
            "SiE_Aliment_HabitIna","SiE_SexiAfec_A","SiE_SexiAfec_Altres",
            "SiE_SexiAfec_Anticon","SiE_SexiAfec_Conturg","SiE_SexiAfec_ITS", "SiE_Consum_A", 
            "SiE_Consum_Altres_1","SiE_Consum_Tabac","SiE_Consum_Alcohol","SiE_Consum_Cannabis","SiE_Consum_Psicofarm",
            "SiE_AltresTemes"]

#Creando dinamicamente algunos campos que son subtipos y repiten parte del nombre 
for motiu in ["BenestarE", "SalutSoc","DerivatA"]:
    camps_codi.append("SiE_{}".format(motiu))
    camps_codi.append("SiE_{}_Altres".format(motiu))

for tipo_maltrat in ["Familiar","Carrer","Parella","ASexual","A","Altres"]:
    camps_codi.append("SiE_Maltrat_{}".format(tipo_maltrat))

for plan in ["ConActi","Tabac","AltrDro","ITS","RefEduSa","ImpliFam","EscolAct","Informac","Coordina","PlaniAct","RiscSuic","HabAlime"]:
    camps_codi.append("SiE_PlaTera_{}".format(plan))

#Diccionario para traducir el nombre del campo al nombre que va a salir en el archivo de texto
keys={"Curs":"Curs", "Pais": "Pais d'origen","TipVisita": "Tipus de visita", "Escola":'Escola', "Origen_Visita":"Origen de la visita",
      "SexePa":"Gènere", "Aliment": "Salud alimentaria","SexiA":"Sexualitat i afectivitat","Bene":"Benestar Emocional","SalutSoc":"Salut social i entorn",
      "Consum":"Consum substàncies", "AltresT":"Altres temes", "Maltrat":"Maltractament i violència", "Deriv":"Derivacions", "PlaTera":"Plans terapèutics"}

#Para traducir el valor "No presencial" de los campos sobre maltrato, para que especifique que tipo de maltrato
no_presen_dict ={tipus:etiqueta.format(tipus,"") for (tipus,etiqueta) in zip(["Familiar","Carrer","Parella","ASexual"],["Maltractament/violència {0} no presencial"]*3+["Assetjament sexual no presencial {1}"])}

def get_date_dextraccio():
    """Get current extraction date. Returns date in datetime.date format"""
    sql="select data_ext from {0}.dextraccio;".format(nod)
    return getOne(sql,nod)[0]

def get_dades(current_date):
    """Selecciona el numero de visitas del programa SiE (XML_TIPUS_ORIG='XML0000023') asociadas a cada una de las etiquetas que presentan los campos en camps_codi. 
       Agrupa por up de registro. Si alguno de los camp_codi tiene un Altres o Altres_1 al final, se añaden las visitas al camp_codi correspondiente con la etiqueta
       Altres. Si la etiqueta contiene Altres, se convierte a Altres solamente y si la etiqueta es 'No presencial', se utiliza el diccionario no_present_dict para 
       asociarlo al tipo de maltrato que corresponde.
       Devuelve un diccionario: {camp_codi: {up: {camp_etiq: n}}}
    """
    sql="select xml_cip,xml_up, xml_data_alta, camp_codi,camp_etiqueta from {} where XML_TIPUS_ORIG='XML0000023' and camp_codi in {}".format(prstb690,tuple(camps_codi))
    print sql
    dades=defaultdict(lambda: defaultdict(Counter))
    for cip,up, data,camp_codi,camp_etiq in getAll(sql,redics):
        if up and monthsBetween(data,current_date) < 12:
            if camp_codi.endswith("Altres") or camp_codi.endswith("Altres_1") :
                indices = [s.start() for s in re.finditer('_', camp_codi)]
                i,j=indices[:2]
                camp_codi=camp_codi[i:j]
                camp_etiq="Altres"
            if "Altres" in camp_etiq:
                camp_etiq="Altres"
            if camp_etiq == "No presencial":
                indices = [s.start() for s in re.finditer('_', camp_codi)]
                camp_etiq=no_presen_dict[camp_codi[indices[1]+1:]]

            dades[camp_codi][up][camp_etiq]+=1
    return dades

def get_nens_visites_escola(current_date):
    """Selecciona el numero de visitas y el numero de nens atendidos por up de registro del xml y por escuela del programa SiE. 
       Devuelve dos estructuras:
       -> escoles_up: dict, {up: {escola: n}}
       -> escoles_up_nens: dict, {up: {escola: set(ids)}}
    """
    sql="select xml_cip,xml_up, xml_data_alta, camp_etiqueta from {} where XML_TIPUS_ORIG='XML0000023' and camp_codi= 'SiE_Escola'".format(prstb690)
    print sql
    escoles_up=defaultdict(Counter)
    escoles_up_nens=defaultdict(lambda: defaultdict(set))
    for cip,up, data, escola in getAll(sql,redics):
        if up and monthsBetween(data,current_date) < 12:
            escoles_up[up][escola]+=1
            escoles_up_nens[up][escola].add(cip)
    return escoles_up,escoles_up_nens


    
def export_txt_file(name,header,rows):
    with open(name,"wb") as out_file:
        print(rows[0])
        row_format ="{}\t" * len(rows[0])+"\n"
        out_file.write(row_format.format(*header))
        for row in rows:
            out_file.write(row_format.format(*row))

if __name__ == '__main__':
    printTime('inici')
    camps_agr= {camp_codi:keys[key] for key in keys for camp_codi in camps_codi if key in camp_codi }
    print(camps_agr)
    current_date=get_date_dextraccio()
    dades=get_dades(current_date)

    rows= [(up, camps_agr[camp_codi],camp_etiq, n) for camp_codi in camps_codi for up in dades[camp_codi] for (camp_etiq,n) in dades[camp_codi][up].iteritems()  ]
    name="SiE_excel.txt"
    header=["UP","CAMP_CODI","VALOR","N"]
    export_txt_file(name,header,rows)
    
    escoles_up,escoles_up_nens=get_nens_visites_escola(current_date)
    rows=[(up, escola,escoles_up[up][escola], len(escoles_up_nens[up][escola])) for up in escoles_up for escola in escoles_up[up]]

    name="SiE_vis_nens.txt"
    header=["UP","ESCOLA","VISITES","NENS ATESOS"]
    export_txt_file(name,header,rows)
