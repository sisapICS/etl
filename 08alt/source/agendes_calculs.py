import collections as c
import datetime as d

import sisapUtils as u


indicadors = ("AGENDQC1", "AGENDQC2")

sql = "select a.sym_name, b.description \
       from icsap.klx_master_symbol a \
       inner join icsap.klx_sym_desc b \
             on a.dim_index = b.minor_obj_id and a.sym_index = b.micro_obj_id \
       where dim_index = 2 and lang_code = 'FR'"
conversio = {row[0]: row[1] for row in u.getAll(sql, "khalix")}

taules = [("agendes_mensual_1", "agendes_anual", "AMBITOS", "br", None),
          ("agendes_mensual_2", "agendes_anual_uba", "PAMBITOS", "equip", conversio)]  # noqa
database = "altres"

dext, = u.getOne("select data_ext from dextraccio", "nodrizas")
avui = d.date(dext.year, dext.month, 15)
actual = "A" + str(avui.year)[2:] + str(avui.month).zfill(2)


for origen, desti, jerarquia, entitat, convertor in taules:
    sql = "select indicador, data, {}, analisi, 'ANUAL', classi, dim6set, \
                'N', val\
        from {}.{} \
        where indicador in {}".format(entitat, database, origen, indicadors)
    anual = c.Counter()
    for row in u.getAll(sql, database):
        anual[row[:-1]] = int(row[-1])

    for i in range(1, 12):
        dia = avui - d.timedelta(days=i * 30)
        periode = "A" + str(dia.year)[2:] + str(dia.month).zfill(2)
        params = (",".join(indicadors), periode, jerarquia)
        print(params)
        query = "{};{};{}###;NUM,DEN;MENSUAL;TIPPROF###;DIM6SET"
        for ind, _a, br, analisi, _c, tipprof, _d, value in u.LvClient().query(query.format(*params)):
            entra = ind != "AGENDQC1" or dext.year > 2023 or dia.year == dext.year  # noqa
            if entra:
                this = br if not convertor else convertor.get(br)
                if this:
                    anual[(ind, actual, this, analisi, 'ANUAL', tipprof, _d, 'N')] += int(float(value))  # noqa
    upload = [k + (v,) for k, v in anual.items()]
    u.execute("create table if not exists {} as \
            select * from {} where 1 = 0".format(desti, origen), database)
    u.execute("delete from {} \
            where indicador in {}".format(desti, indicadors), database)
    u.listToTable(upload, desti, database)
