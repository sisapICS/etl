from sisapUtils import *
from collections import defaultdict
import csv, os
 
OutFile = tempFolder + 'professionals.txt'
db ='altres'
imp = "import"
centres="export.khx_centres"

printTime('inici professionals') 
 
sql = "select up, uab, tipus, concat(cognom1,concat(' ',concat(cognom2,concat(', ',nom)))),ide_usuari,ide_numcol from cat_professionals\
 union  select up, uab, tipus, concat(cognom1,concat(' ',concat(cognom2,concat(', ',nom)))),ide_usuari,ide_numcol from cat_professionals_assir"
profess = {}
for up,uba,tipus,nom,usu,numcol in getAll(sql,imp):
    profess[up,uba,tipus] = {'nom':nom,'usu':usu,'numcol':numcol}


with open(OutFile,'wb') as file:
    w = csv.writer(file, delimiter='@', quotechar='|')
    for (up,uba,tipus),d in profess.items():
        nom =d['nom']
        usu =d['usu']
        ncol =d['numcol']
        try:
            w.writerow([up,uba,tipus,nom,usu,ncol])
        except KeyError:
            continue
profess.clear()

table ="professionals" 
sql = 'drop table if exists {}'.format(table)
execute(sql,db)
sql = "create table {} (up varchar(5) not null default'',uba varchar(15) not null default'',tipus varchar(1) not null default'', nom varchar(200) not null default'',usuari varchar(10) not null default'',numcol varchar(15) not null default'')".format(table)
execute(sql,db)
loadData(OutFile,table,db)
  
printTime('export a Khalix')
error = []

taulaK ="%s.%s" % (db,table)
query ="select concat(ics_codi,tipus,uba),nom,usuari,numcol,up from %(taula)s a inner join %(centres)s b on a.up=scs_codi where tipus in ('M','I')" % {'taula': taulaK,'centres':centres}
file ="PROFESSIONALS" 
error.append(exportKhalix(query,file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
    
sql= 'drop table if exists {}'.format(table)  
execute(sql,db)  

printTime('Fi professionals') 
