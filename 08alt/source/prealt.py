# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime
from collections import defaultdict,Counter

debug = False

db = "altres"
nod = "nodrizas"
imp = "import"

OutFile = tempFolder + 'prealt.txt'
CslOutFile = tempFolder + 'Csl_prealt.txt'
TaulaMy = 'exp_khalix_prealt'
CslTaulaMy = 'exp_khalix_csl_prealt'

catedat = 'khx_edats5a'
prealt1 = 'prealt1'
prealt2 = 'prealt2'
gescas = 'gescasos'
PraltCaract = "('SITEN01','SITEN02','SITEN03','SITEN04','SITEN05','SITEN06','SITEN07')"


def KhxOrigen(origen):
    if origen == 'H':
        return 'ALTAHOSPI'
    elif origen == 'S':
        return 'ALTASOCIO'
    elif origen == 'D':
        return 'ALTAHDIA'
    else:
        return 'SENSEORI'

def KhxCaract(caract):
    if caract == 'SITEN01':
        return 'PACFRAGIL'
    elif caract == 'SITEN02':
        return 'PACFIVIDA'
    elif caract == 'SITEN04':
        return 'PACCURES'
    elif caract == 'SITEN05':
        return 'PACTRACT'
    else:
        return 'EXCLOS'
        
def KhxIndicador(indicador):

    return 'PREALT' if indicador == 'P' else 'GESCASOS'
    
printTime('Pob')

centres = {}
sql = 'select scs_codi,ics_codi from cat_centres'
for up,br in getAll(sql,nod):
    centres[up] = br
    
assig = {}
sql = 'select id_cip_sec,up,pcc,edat,sexe from assignada_tot'
for id,up,pcc,edat,sexe in getAll(sql,nod):
    assig[id] = {'br':centres[up],'pcc':pcc,'edat':edat,'sexe':sexe} 
    
sql = "select data_ext from dextraccio"
dext, = getOne(sql,nod)
      
printTime('Inici Prealt')   

prealtConv = defaultdict(list)
sql = "select id_cip_sec, val_var from {0}, nodrizas.dextraccio where val_var in {1} and val_data between date_add(data_ext,interval -1 year) and data_ext {2}".format(prealt2, PraltCaract,' limit 10' if debug else '')
for id,val in getAll(sql,imp):
    prealtConv[id].append(val)
    
prealt = Counter()
CslPrealt = Counter()
done,doneC = {},{}
sql="select id_cip_sec,enll_up,enll_tipus_origen,DATEDIFF(enll_data_postalt,enll_data_alt)-truncate(((DATEDIFF(enll_data_postalt,enll_data_alt)-(DAYOFWEEK(enll_data_postalt)-0)+7)/7),0)\
    ,DATEDIFF(enll_data_postalt,enll_data_ins_alt)-truncate(((DATEDIFF(enll_data_postalt,enll_data_ins_alt)-(DAYOFWEEK(enll_data_postalt)-0)+7)/7),0) from {0},nodrizas.dextraccio\
    where ENLL_EXCLOS NOT IN ('S') and enll_data_alt between date_add(data_ext,interval -1 year) and data_ext {1}".format(prealt1,' limit 10' if debug else '')
for id,e_up,tipus,temps1,temps2 in getAll(sql,imp):
    indicador = 'P'
    try:
        up = assig[id]['br']
        edat = assig[id]['edat']
        sexe = assig[id]['sexe']
    except KeyError:
        continue
    prealt[KhxIndicador(indicador),up,'ALTES',ageConverter(edat,5),KhxOrigen(tipus),sexConverter(sexe)] += 1
    try:
        pcc = assig[id]['pcc']
    except KeyError:
        pcc = 0
    if pcc == 1:
        CslPrealt[up,'PCCPREAL'] += 1
        if temps1 <= 1 and temps1 <> None:
            CslPrealt[up,'PCCPREAL24'] += 1
    if temps1 <= 2 and temps1 <> None:
        prealt[KhxIndicador(indicador),up,'ALTES48h1',ageConverter(edat,5),KhxOrigen(tipus),sexConverter(sexe)] += 1
        if pcc == 1:
            CslPrealt[up,'PCCPREAL48'] += 1
    if temps2 <= 2 and temps2 <> None:
        prealt[KhxIndicador(indicador),up,'ALTES48h2',ageConverter(edat,5),KhxOrigen(tipus),sexConverter(sexe)] += 1
    try:
        Caract = prealtConv[id]
    except KeyError:
        continue
    if id not in done:
        done[id] = True
        prealt[KhxIndicador(indicador),up,'PACIENTS',ageConverter(edat,5),'SENSEORI',sexConverter(sexe)] += 1
    for codi in Caract:
        if (id,codi) not in doneC:
            doneC[id,codi] = True
            prealt[KhxIndicador(indicador),up,KhxCaract(codi),ageConverter(edat,5),'SENSEORI',sexConverter(sexe)] += 1
done = {}
sql="select id_cip_sec,gesc_up,gesc_data_alta,data_naixement,sexe,gesc_data_sortida from {0},nodrizas.dextraccio where gesc_estat not in ('D')\
        and gesc_data_alta <= data_ext {1}".format(gescas,' limit 10' if debug else '')
for id, g_up,data,dnaix,sexe,sortida in getAll(sql,imp):
    edat = yearsBetween(dnaix,dext)
    indicador = 'G'
    try:
        up = assig[id]['br']
    except KeyError:
        continue
    b = monthsBetween(data,dext)
    if 0 <= b <= 11:
        prealt[KhxIndicador(indicador),up,'ALTES',ageConverter(edat,5),'SENSEORI',sexConverter(sexe)] += 1
    s = 0
    if sortida <> None:    
        s = monthsBetween(sortida,dext)
    if 0<= s <= 11:
        if id not in done:
            done[id] = True
            prealt[KhxIndicador(indicador),up,'PACIENTS',ageConverter(edat,5),'SENSEORI',sexConverter(sexe)] += 1

with openCSV(OutFile) as c:
    for (indicador,br,analisi,edat,origen,sexe),count in prealt.items():
        if analisi <> 'EXCLOS':
            c.writerow([indicador,br,analisi,edat,origen,sexe,count])
    
execute('drop table if exists {}'.format(TaulaMy),db)
execute('create table {} (cuenta varchar(10),br varchar(5),analisi varchar(10),edat varchar(10),origen varchar(10),sexe varchar(10),recompte double)'.format(TaulaMy),db)
loadData(OutFile,TaulaMy,db)

with openCSV(CslOutFile) as c:
    for (up,indicador),count in CslPrealt.items():
        c.writerow([up,indicador,count])
    
execute('drop table if exists {}'.format(CslTaulaMy),db)
execute('create table {} (br varchar(5),indicador varchar(10),recompte double)'.format(CslTaulaMy),db)
loadData(CslOutFile,CslTaulaMy,db)

error = []
sql = "select cuenta,concat('A','periodo'),br,analisi,edat,origen,sexe,'N',recompte from {}.{}".format(db,TaulaMy)
file = 'PREALT_GESCASOS'
error.append(exportKhalix(sql,file))

sql = "select indicador,concat('A','periodo'),br,'NOCLI','NOCAT','NOIMP','DIM6SET','N',recompte from {}.{}".format(db,CslTaulaMy)
file = 'CATSALUT_PREALT'
error.append(exportKhalix(sql,file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
 
printTime('Fi')