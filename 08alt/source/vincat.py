import datetime as t
import sisapUtils as u
import sisaptools as tools
import multiprocessing as m
import collections as c

SEND = True


# class Vincat_trimestral():
#     def __init__(self):
#         cols = """(indicador varchar(10), up varchar(5),
#                   num int, den int, res double)"""
#         u.createTable('vincat', cols, 'altres', rm=True)
#         self.tancament_mes, = u.getOne("""select date '2024-03-31' from nodrizas.dextraccio""", 'nodrizas')
#         month = str(self.tancament_mes.month) if len(str(self.tancament_mes.month)) == 2 else '0' + str(self.tancament_mes.month)  # noqa
#         day = str(self.tancament_mes.day) if len(str(self.tancament_mes.day)) == 2 else '0' + str(self.tancament_mes.day)  # noqa
#         self.tancament_mes = str(self.tancament_mes.year) + '/'+ month + '/' + day
#         self.any_tract, = u.getOne("""select extract(year from date '2024-03-31') from nodrizas.dextraccio""", 'nodrizas')
#         self.get_data()

#     def get_data(self):
#         pob = c.defaultdict(dict)
#         sql = """select id_cip_sec, up, edat
#                 from nodrizas.assignada_tot"""
#         for id, up, edat in u.getAll(sql, 'nodrizas'):
#             pob[id]['up'] = up
#             pob[id]['edat'] = edat
#         print('pob ok')
#         dx_associat = c.defaultdict()
#         sql = """select dgppf_pmc_codi, dgppf_num_prod,
#                     dgppf_ps_cod, dgppf_versio
#                     from import.dx_prescripcio
#                     where dgppf_ps_cod in ('C01-J02.0',
#                     'C01-J02.8', 'C01-J02.9',
#                     'C01-J03','C01-J03.0', 'C01-J03.00',
#                     'C01-J03.8', 'C01-J03.80',
#                     'C01-J03.9', 'C01-J03.90')
#                     and dgppf_data_alta <= STR_TO_DATE('{tancament_mes}','%Y/%m/%d')""".format(tancament_mes=self.tancament_mes)
#         print(sql)
#         for codi, num, dx, versio in u.getAll(sql, 'import'):
#             if (codi, num) in dx_associat:
#                 if dx_associat[(codi, num)][1] < versio:
#                     dx_associat[(codi, num)] = [dx, versio]
#             else:
#                 dx_associat[(codi, num)] = [dx, versio]
#         print('dx associats ok')
#         exclusions = c.defaultdict(set)
#         sql = """select id_cip_sec
#                 from nodrizas.eqa_variables
#                 where agrupador = 40
#                 and data_var <= STR_TO_DATE('{tancament_mes}','%Y/%m/%d')""".format(tancament_mes=self.tancament_mes)
#         print(sql)
#         for id, in u.getAll(sql, 'nodrizas'):
#             exclusions['tot'].add(id)
#         print('exclusions ok')
#         faa_repeticio = c.defaultdict(set)
#         sql = """select id_cip_sec, pr_cod_ps, pr_dde, pr_dba
#                  from import.problemes
#                  where pr_cod_ps in ('C01-I00', 'C01-I01.9',
#                  'C01-J02.0', 'C01-J02.8', 'C01-J02.9', 'C01-J03',
#                  'C01-J03.0', 'C01-J03.00', 'C01-J03.8',
#                  'C01-J03.80', 'C01-J03.9', 'C01-J03.90')
#                  and pr_dde <= STR_TO_DATE('{tancament_mes}','%Y/%m/%d')""".format(tancament_mes=self.tancament_mes)
#         print(sql)
#         for id, codi, data, baixa in u.getAll(sql, 'nodrizas'):
#             if codi in ('C01-I00', 'C01-I01.9'):
#                 if not baixa:
#                     exclusions['indi1'].add(id)
#             else:
#                 faa_repeticio[id].add(data)
#         print('faa repeticio ok')
#         print('anem x den')
#         den = c.defaultdict(c.Counter)
#         den_dat = c.defaultdict(lambda: c.defaultdict(set))
#         sql = """select id_cip_sec, ppfmc_pmc_codi, ppfmc_num_prod, ppfmc_pmc_data_ini
#                 from import.tractaments
#                 where ppfmc_atccodi like 'J01%'
#                 and ppfmc_pmc_data_ini
#                     >= STR_TO_DATE('{any_tract}-01-01', '%Y-%m-%d')
#                 and ppfmc_pmc_data_ini
#                     <= STR_TO_DATE('{tancament_mes}', '%Y/%m/%d')""".format(any_tract=self.any_tract, tancament_mes=self.tancament_mes)
#         print(sql)
#         for id, codi, num, data in u.getAll(sql, 'import'):
#             if (codi, num) in dx_associat:
#                 excl = 0
#                 # MIRAR TEMA FAA DE REPETICI
#                 if id in faa_repeticio:
#                     dates = faa_repeticio[id]
#                     if data in dates:
#                         for d in dates:
#                             if d <= data and d >= data - t.timedelta(days=30):
#                                 excl = 1
#                 if excl == 0:
#                     if id in pob:
#                         if pob[id]['edat'] <= 14:
#                             if id not in exclusions['indi1'] and id not in exclusions['tot']:
#                                 den['indi1'][id] += 1
#                                 den_dat['indi1'][id].add(data)
#         print(len(den['indi2']))
#         countingerrors = 0
#         sql = """select id_cip_sec, eli_codi_element, eli_dia_inici from import.atic
#                  where eli_codi_element = 'IRE01549'
#                  and eli_codi_element between STR_TO_DATE('01/01/{any_tract}','%d/%m/%Y')
#                  and STR_TO_DATE('{tancament_mes}','%Y/%m/%d')""".format(any_tract=self.any_tract, tancament_mes=self.tancament_mes)
#         print(sql)
#         for id, codi, data in u.getAll(sql, 'nodrizas'):
#             excl = 0
#             # MIRAR TEMA FAA DE REPETICI
#             if id in faa_repeticio:
#                 dates = faa_repeticio[id]
#                 if data in dates:
#                     for d in dates:
#                         try:
#                             if d < data:
#                                 if d > (data - t.timedelta(days=30)):
#                                     excl = 1
#                         except:
#                             countingerrors += 1              
#             if excl == 0:
#                 if id in pob:
#                     if pob[id]['edat'] >= 15:
#                         if id not in exclusions['tot']:
#                             den['indi2'][id] += 1
#                             den_dat['indi2'][id].add(data)
#         sql = """select id_cip_sec, pr_cod_ps, pr_dde
#                  from import.problemes
#                  where pr_cod_ps in (
#                  'C01-J02.0', 'C01-J02.8', 'C01-J02.9', 'C01-J03',
#                  'C01-J03.0', 'C01-J03.00', 'C01-J03.8',
#                  'C01-J03.80', 'C01-J03.9', 'C01-J03.90')
#                  and pr_dde between STR_TO_DATE('01/01/{any_tract}','%d/%m/%Y')
#                  and STR_TO_DATE('{tancament_mes}','%Y/%m/%d')""".format(any_tract=self.any_tract, tancament_mes=self.tancament_mes)
#         print(sql)
#         for id, codi, data in u.getAll(sql, 'nodrizas'):
#             excl = 0
#             # MIRAR TEMA FAA DE REPETICI
#             if id in faa_repeticio:
#                 dates = faa_repeticio[id]
#                 if data in dates:
#                     for d in dates:
#                         try:
#                             if d < data:
#                                 if d > (data - t.timedelta(days=30)):
#                                     excl = 1
#                         except:
#                             countingerrors += 1              
#             if excl == 0:
#                 if id in pob:
#                     if pob[id]['edat'] >= 15:
#                         if id not in exclusions['tot']:
#                             den['indi2'][id] += 1
#                             den_dat['indi2'][id].add(data)
#         print(countingerrors)
#         print(len(den['indi1']))

#         print('anem x num')
#         # numerodor
#         num = c.defaultdict(c.Counter)
#         sql = """select id_cip_sec, vu_dat_act, vu_val, vu_cod_vs
#                     from import.variables1
#                     where vu_cod_vs = 'TA5001'
#                     and vu_dat_act >= STR_TO_DATE('01/01/{any_tract}','%d/%m/%Y')""".format(any_tract=self.any_tract)
#         print(sql)
#         for id, data, val, codi in u.getAll(sql, 'import'):
#             # CAL TRACTAR DATES
#             if id in den_dat['indi1']:
#                 dates_tractament = den_dat['indi1'][id]
#                 for d_t in dates_tractament:
#                     if d_t >= data - t.timedelta(days=10):
#                         if d_t <= data + t.timedelta(days=10):
#                             num['indi1'][id] += 1
#             if id in den_dat['indi2']:
#                 dates_tractament = den_dat['indi2'][id]
#                 for d_t in dates_tractament:
#                     if d_t >= data - t.timedelta(days=10):
#                         if d_t <= data + t.timedelta(days=10):
#                             den['indi3'][id] += 1

#         sql = """select id_cip_sec, vu_dat_act, vu_val, vu_cod_vs
#                     from import.variables1
#                     where vu_cod_vs = 'VA5001'
#                     and vu_dat_act >= STR_TO_DATE('01/01/{any_tract}','%d/%m/%Y')""".format(any_tract=self.any_tract)
#         print(sql)
#         for id, data, val, codi in u.getAll(sql, 'import'):
#             # CAL TRACTAR DATES
#             if id in den_dat['indi2']:
#                 dates_tractament = den_dat['indi2'][id]
#                 for d_t in dates_tractament:
#                     if d_t >= data - t.timedelta(days=10):
#                         if d_t <= data + t.timedelta(days=10):
#                             num['indi2'][id] += 1
#                             if id in den['indi3']:
#                                 if val in (0, 1):
#                                     num['indi3'][id] += 1
#         print(len(num['indi3']), '3')
#         print(len(num['indi2']), '2')
#         print(len(num['indi1']), '1')
#         print('lets go cooking final')
#         upload = c.defaultdict(c.Counter)
#         for indicador in den:
#             for id in den[indicador]:
#                 if id in pob:
#                     up = pob[id]['up']
#                     upload[(indicador, up)]['den'] += den[indicador][id]
#         for indicador in num:
#             for id in den[indicador]:
#                 if id in pob:
#                     up = pob[id]['up']
#                     upload[(indicador, up)]['num'] += num[indicador][id]

#         list2tab = []
#         for (indicador, up) in upload:
#             if 'den' in upload[(indicador, up)]:
#                 den = upload[(indicador, up)]['den']
#                 if 'num' in upload[(indicador, up)]:
#                     num = upload[(indicador, up)]['num']
#                 else:
#                     num = 0
#                 list2tab.append((indicador, up, num, den, num/float(den)))
#         u.listToTable(list2tab, 'vincat', 'altres')

def _worker(params):
    taula, tancament_mes_txt = params
    sql = """select dgppf_pmc_codi, dgppf_num_prod,
                dgppf_data_alta,
                dgppf_ps_cod, dgppf_versio
                from import.{taula}
                where dgppf_ps_cod in ('C01-J02.0',
                'C01-J02.8', 'C01-J02.9',
                'C01-J03','C01-J03.0', 'C01-J03.00',
                'C01-J03.8', 'C01-J03.80',
                'C01-J03.9', 'C01-J03.90')
                and dgppf_data_alta <= STR_TO_DATE('{tancament_mes}','%Y/%m/%d')
                and dgppf_data_alta >= date_add(STR_TO_DATE('{tancament_mes}','%Y/%m/%d'),
                interval - 1 month)""".format(tancament_mes=tancament_mes_txt, taula=taula)
    print(sql)
    dx_associat = c.defaultdict()
    for codi, num, dgppf_data_alta, dx, versio in u.getAll(sql, 'import'):
        if (codi, num, dgppf_data_alta) in dx_associat:
            if dx_associat[(codi, num, dgppf_data_alta)][1] < versio:
                dx_associat[(codi, num, dgppf_data_alta)] = [dx, versio]
        else:
            dx_associat[(codi, num, dgppf_data_alta)] = [dx, versio]
    print('fi unoooo')
    return dx_associat


class Vincat():
    def __init__(self):
        cols = """(indicador varchar(10), up varchar(5),
                  analisi varchar(5), val int)"""
        u.createTable('vincat', cols, 'altres', rm=True)
        self.tancament_mes, = u.getOne("""select data_ext from nodrizas.dextraccio""", 'nodrizas')
        # self.tancament_mes, = u.getOne("""select date '2023-12-31' from nodrizas.dextraccio""", 'nodrizas')
        month = str(self.tancament_mes.month) if len(str(self.tancament_mes.month)) == 2 else '0' + str(self.tancament_mes.month)  # noqa
        day = str(self.tancament_mes.day) if len(str(self.tancament_mes.day)) == 2 else '0' + str(self.tancament_mes.day)  # noqa
        self.tancament_mes_txt = str(self.tancament_mes.year) + '/'+ month + '/' + day
        self.get_data()
        self.upload_khalix()

    def get_data(self):
        pob = c.defaultdict(dict)
        sql = """select id_cip_sec, up, edat
                from nodrizas.assignada_tot"""
        for id, up, edat in u.getAll(sql, 'nodrizas'):
            pob[id]['up'] = up
            pob[id]['edat'] = edat
        print('pob ok')
        subtables = u.getSubTables("dx_prescripcio", ordenat=True)
        jobs = [(subtable, self.tancament_mes_txt) for subtable in subtables]
        pool = m.Pool(8)
        dx_associat_pool = pool.map(_worker, jobs)
        dx_associat = c.defaultdict()
        for chunk in dx_associat_pool:
            for key, val in chunk.items():
                dx_associat[key] = val

        # print('dx_associats', dx_associat)
        print('dx associats ok')
        exclusions = c.defaultdict(set)
        sql = """select id_cip_sec
                from nodrizas.eqa_variables
                where agrupador = 40
                and data_var <= STR_TO_DATE('{tancament_mes}','%Y/%m/%d')
                and data_var >= date_add(STR_TO_DATE('{tancament_mes}','%Y/%m/%d'),interval - 1 month)""".format(tancament_mes=self.tancament_mes_txt)
        print(sql)
        for id, in u.getAll(sql, 'nodrizas'):
            exclusions['tot'].add(id)
        print('exclusions ok')
        faa_repeticio = c.defaultdict(set)
        sql = """select id_cip_sec, ps, dde
                 from nodrizas.eqa_problemes
                 where ps in (761, 1075)
                 and dde <= STR_TO_DATE('{tancament_mes}','%Y/%m/%d')
                 """.format(tancament_mes=self.tancament_mes_txt)
        print(sql)
        for id, codi, data in u.getAll(sql, 'nodrizas'):
            if codi == 761:
                exclusions['VINCAT03'].add(id)
            else:
                faa_repeticio[id].add(data)
        print('faa repeticio ok')
        print('anem x den')
        self.den = c.defaultdict(c.Counter)
        den_dat = c.defaultdict(lambda: c.defaultdict(set))
        sql = """select id_cip_sec, ppfmc_pmc_codi, ppfmc_num_prod, ppfmc_pmc_data_ini
                from import.tractaments
                where ppfmc_atccodi like 'J01%'
                and ppfmc_pmc_data_ini between
                date_add(STR_TO_DATE('{tancament_mes}','%Y/%m/%d'),interval - 1 month)
                and STR_TO_DATE('{tancament_mes}','%Y/%m/%d')""".format(tancament_mes=self.tancament_mes_txt)
        print(sql)
        counting_cases = 0
        for id, codi, num, data in u.getAll(sql, 'import'):
            # print(codi, num, data)
            if (codi, num, data) in dx_associat:
                # print('te dx')
                counting_cases += 1
                excl = 0
                # MIRAR TEMA FAA DE REPETICI
                if id in faa_repeticio:
                    dates = faa_repeticio[id]
                    if data in dates:
                        for d in dates:
                            if d <= data and d >= data - t.timedelta(days=30):
                                excl = 1
                if excl == 0:
                    if id in pob:
                        if pob[id]['edat'] <= 14:
                            if id not in exclusions['VINCAT03'] and id not in exclusions['tot']:
                                self.den['VINCAT03'][id] += 1
                                den_dat['VINCAT03'][id].add(data)
                                # print('here')
        print(counting_cases)
        countingerrors = 0
        sql = """select id_cip_sec, eli_codi_element, eli_dia_inici from import.atic
                 where eli_codi_element = 'IRE01549'
                 and eli_codi_element between date_add(STR_TO_DATE('{tancament_mes}','%Y/%m/%d'),interval - 2 month)
                 and STR_TO_DATE('{tancament_mes}','%Y/%m/%d')""".format(tancament_mes=self.tancament_mes_txt)
        print(sql)
        for id, codi, data in u.getAll(sql, 'nodrizas'):
            excl = 0
            # MIRAR TEMA FAA DE REPETICI
            if id in faa_repeticio:
                dates = faa_repeticio[id]
                if data in dates:
                    for d in dates:
                        try:
                            if d < data:
                                if d > (data - t.timedelta(days=30)):
                                    excl = 1
                        except:
                            countingerrors += 1              
            if excl == 0:
                if id in pob:
                    if pob[id]['edat'] >= 15:
                        if id not in exclusions['tot']:
                            self.den['VINCAT04'][id] += 1
                            den_dat['VINCAT04'][id].add(data)
        sql = """select id_cip_sec, dde
                 from nodrizas.eqa_problemes
                 where ps = 1075
                 and dde <= STR_TO_DATE('{tancament_mes}','%Y/%m/%d')
                 and dde >= date_add(STR_TO_DATE('{tancament_mes}','%Y/%m/%d'),interval - 2 month)""".format(tancament_mes=self.tancament_mes_txt)
        print(sql)
        for id, data in u.getAll(sql, 'nodrizas'):
            excl = 0
            # MIRAR TEMA FAA DE REPETICI
            if id in faa_repeticio:
                dates = faa_repeticio[id]
                if data in dates:
                    for d in dates:
                        try:
                            if d < data:
                                if d > (data - t.timedelta(days=30)):
                                    excl = 1
                        except:
                            countingerrors += 1              
            if excl == 0:
                if id in pob:
                    if pob[id]['edat'] >= 15:
                        if id not in exclusions['tot']:
                            self.den['VINCAT04'][id] += 1
                            den_dat['VINCAT04'][id].add(data)
        print('anem x num')
        # numerodor
        self.num = c.defaultdict(c.Counter)
        sql = """select
                    id_cip_sec,
                    agrupador,
                    data_var,
                    valor
                from
                    nodrizas.eqa_variables
                where
                    agrupador in (904, 1076)
                    and data_var between
                    date_add(STR_TO_DATE('{tancament_mes}','%Y/%m/%d'),interval - 1 month)
                    and STR_TO_DATE('{tancament_mes}','%Y/%m/%d')""".format(tancament_mes=self.tancament_mes_txt)
        print(sql)
        for id, agrupador, data, val in u.getAll(sql, 'import'):
            # CAL TRACTAR DATES
            if agrupador == 904:
                if id in den_dat['VINCAT03']:
                    dates_tractament = den_dat['VINCAT03'][id]
                    for d_t in dates_tractament:
                        if d_t >= data - t.timedelta(days=10):
                            if d_t <= data + t.timedelta(days=10):
                                self.num['VINCAT03'][id] += 1
                if id in den_dat['VINCAT04']:
                    dates_tractament = den_dat['VINCAT04'][id]
                    for d_t in dates_tractament:
                        if d_t >= data - t.timedelta(days=10):
                            if d_t <= data + t.timedelta(days=10):
                                self.den['VINCAT05'][id] += 1
            else:
                if id in den_dat['VINCAT04']:
                    dates_tractament = den_dat['VINCAT04'][id]
                    for d_t in dates_tractament:
                        if d_t >= data - t.timedelta(days=10):
                            if d_t <= data + t.timedelta(days=10):
                                self.num['VINCAT04'][id] += 1
                                if id in self.den['VINCAT05']:
                                    if val in (0, 1):
                                        self.num['VINCAT05'][id] += 1
        sql = """select
                    id_cip_sec,
                    agrupador,
                    dat,
                    val
                from
                    nodrizas.ped_variables
                where
                    agrupador in (904, 1076)
                    and dat between
                    date_add(STR_TO_DATE('{tancament_mes}','%Y/%m/%d'),interval - 1 month)
                    and STR_TO_DATE('{tancament_mes}','%Y/%m/%d')""".format(tancament_mes=self.tancament_mes_txt)
        print(sql)
        for id, agrupador, data, val in u.getAll(sql, 'import'):
            # CAL TRACTAR DATES
            if agrupador == 904:
                if id in den_dat['VINCAT03']:
                    dates_tractament = den_dat['VINCAT03'][id]
                    for d_t in dates_tractament:
                        if d_t >= data - t.timedelta(days=10):
                            if d_t <= data + t.timedelta(days=10):
                                self.num['VINCAT03'][id] += 1
                if id in den_dat['VINCAT04']:
                    dates_tractament = den_dat['VINCAT04'][id]
                    for d_t in dates_tractament:
                        if d_t >= data - t.timedelta(days=10):
                            if d_t <= data + t.timedelta(days=10):
                                self.den['VINCAT05'][id] += 1
            else:
                if id in den_dat['VINCAT04']:
                    dates_tractament = den_dat['VINCAT04'][id]
                    for d_t in dates_tractament:
                        if d_t >= data - t.timedelta(days=10):
                            if d_t <= data + t.timedelta(days=10):
                                self.num['VINCAT04'][id] += 1
                                if id in self.den['VINCAT05']:
                                    if val in (0, 1):
                                        self.num['VINCAT05'][id] += 1

        print('lets go cooking final')
        upload = c.defaultdict(c.Counter)
        for indicador in self.den.keys():
            print(indicador)
            for id in self.den[indicador]:
                if id in pob:
                    up = pob[id]['up']
                    upload[(indicador, up)]['den'] += self.den[indicador][id]
        # print('upload de dens', upload)
        # print(self.num)
        for indicador in self.num.keys():
            print(indicador)
            for id in self.num[indicador]:
                if id in pob:
                    up = pob[id]['up']
                    upload[(indicador, up)]['num'] += self.num[indicador][id]
        list2tab = []
        for (indicador, up) in upload:
            if 'den' in upload[(indicador, up)]:
                den = upload[(indicador, up)]['den']
                list2tab.append((indicador, up, 'DEN', den))
                if 'num' in upload[(indicador, up)]:
                    num = upload[(indicador, up)]['num']
                    list2tab.append((indicador, up, 'NUM', num))
                else:
                    list2tab.append((indicador, up, 'NUM', 0))
        u.listToTable(list2tab, 'vincat', 'altres')

    def upload_khalix(self):
        sql = """select
                    indicador,
                    concat('A', 'periodo'),
                    ics_codi,
                    analisi,
                    'NOCAT',
                    'NOIMP',
                    'DIM6SET',
                    'N',
                    val
                from
                    altres.vincat v
                    inner join nodrizas.cat_centres cc 
                on cc.scs_codi = v.up"""
        u.exportKhalix(sql,'VINCAT_NEW', force=True)

if __name__ == "__main__":
    Vincat()
    # try:
    #     Vincat_trimestral()
    #     if SEND:
    #         upload = []
    #         sql = """select indicador, aga, up, ics_desc, sum(num) num, sum(den) den, sum(num)/sum(den)*100 resultat
    #                     from ALTRES.vincat a, nodrizas.cat_centres b 
    #                     where a.up = b.scs_codi
    #                     group by indicador, aga, up, ics_desc"""
    #         for row in u.getAll(sql, 'nodrizas'):
    #             upload.append(row)
    #         file_eap = u.tempFolder + 'VINCAT.csv'
    #         u.writeCSV(file_eap, [('INDICADOR', 'AGA', 'UP', 'UP_DESC', 'NUM', 'DEN', 'RESULTAT')] + upload, sep=";")
    #         # email
    #         subject = 'Seguiment VINCAT'
    #         text = "Bon dia.\n\nAdjuntem arxiu .csv amb les dades de seguiment trimestrals acumulades de VINCAT.\n"
    #         text += "\n\nSalutacions."
    #         u.sendGeneral('SISAP <sisap@gencat.cat>',
    #                             ('shernandezbaeza@iconcologia.net', 'roser.cantenys@catsalut.cat'),
    #                             ('shernandezbaeza@iconcologia.net', 'roser.cantenys@catsalut.cat'),
    #                             subject,
    #                             text,
    #                             file_eap)

    # except Exception as e:
    #     text = str(e)
    #     mail = tools.Mail()
    #     mail.to.append("roser.cantenys@catsalut.cat")
    #     mail.subject = "VINCAT trimestral malament"
    #     mail.text = text
    #     mail.send()
    #     raise
