# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
from dateutil import relativedelta as rd
import sisapUtils as u
import sisaptools as t
import os

TEST = False
RECOVER = False

nod = "nodrizas"
imp = "import"
imp_jail = "import_jail"


convert_tipus_2_indi = {
    '9C': 'DEMAND9C',
    '9R': 'DEMAND9R',
    '9D': 'DEMAND9D',
    '9T': 'DEMAND9T',
    '9E': 'DEMAND9E',
    '9Ec': 'DEMAND9EC'
}

PROF_CONV = {
    'MF': 'TIPPROF1',
    'INF': 'TIPPROF4',
    'PED': 'TIPPROF2'
}


class Demanda():
    def __init__(self):
        cols = '(periode varchar(5), indi varchar(10), up varchar(10), uba varchar(10), professional varchar(10), analisi varchar(10), n int)'
        u.createTable('demanda_agendes', cols, 'altres', rm = True)
        self.get_ubas()
        print('tinc basics')
        self.get_visites()
        print('tinc visites')
        # self.computing_den()
        print('anem a penjar')
        self.uploading()
        self.upload_khalix()

    def get_ubas(self):
        self.cip_2_prof = {}
        sql = """SELECT c_cip, c_up, c_metge, c_infermera FROM dwsisap.dbs"""
        for cip, up, uba, ubainf in u.getAll(sql, 'exadata'):
            self.cip_2_prof[cip] = [up, uba, ubainf]

    def get_visites(self):
        sql = """select data_ext from nodrizas.dextraccio"""
        self.dextraccio, = u.getOne(sql, 'nodrizas')
        self.dextraccio_fa1mes = self.dextraccio - rd.relativedelta(months=1)
        self.inici = d.date(self.dextraccio_fa1mes.year, self.dextraccio_fa1mes.month, 1)
        self.data = c.Counter()
        sql = """SELECT pacient, TIPUS, SISAP_CATPROF_CLASS, data FROM DWSISAP.SISAP_MASTER_VISITES
                    WHERE DATA between date '{}' and date '{}'
                    AND SISAP_SERVEI_CLASS IN ('MF', 'URG', 'INF', 'PED')
                    AND SISAP_CATPROF_CLASS IN ('MF', 'INF', 'PED')
                    AND (RELACIO_PROVEIDOR IN (1,2,3) OR RELACIO_PROVEIDOR IS NULL)
                    AND TIPUS IN ('9C', '9R', '9D', '9T', '9E', '9Ec')""".format(self.inici, self.dextraccio)
        for pacient, tipus, prof, data_visita in u.getAll(sql, 'exadata'):
            periode_visita = "A{}{}".format(str(data_visita.year)[2:], str(data_visita.month).zfill(2))
            if pacient in self.cip_2_prof:
                up, uba, ubainf = self.cip_2_prof[pacient]
                if prof == 'INF':
                    self.data[(periode_visita, ubainf, up, tipus, PROF_CONV[prof])] += 1
                else:
                    self.data[(periode_visita, uba, up, tipus, PROF_CONV[prof])] += 1
            else:
                self.data[(periode_visita, '', up, tipus, PROF_CONV[prof])] += 1

    def computing_den(self):
        """."""
        total = (self.dextraccio - self.inici).days
        self.laborables = 0
        for i in range(total):
            dia = self.inici + d.timedelta(days=i)
            if u.isWorkingDay(dia):
                self.laborables += 1

    def uploading(self):
        upload = []
        for (periode_visita, uba, up, tipus, prof), n in self.data.items():
            indi = convert_tipus_2_indi[tipus] 
            upload.append((periode_visita, indi, up, uba, prof, 'NUM', n))
        u.listToTable(upload, 'demanda_agendes', 'altres')
    
    def upload_khalix(self):
        sql = """select indi, periode, 
                case when professional = 'TIPPROF4' THEN concat(concat(ics_codi, 'I'), uba) 
                else concat(concat(ics_codi, 'M'), uba) end UBA,
                analisi, 'NOCAT', professional, 'DIM6SET',
                'N', n from ALTRES.demanda_agendes a,
                NODRIZAS.CAT_CENTRES b
                where a.up = b.scs_codi
                and uba != ''"""
        u.exportKhalix(sql, 'DEMANDAAGENDES_UBA')
        sql = """select
                    indi,
                    periode,
                    ics_codi,
                    analisi,
                    'NOCAT',
                    professional,
                    'DIM6SET',
                    'N',
                    sum(n)
                from
                    ALTRES.demanda_agendes a,
                    NODRIZAS.CAT_CENTRES b
                where
                    a.up = b.scs_codi
                    and analisi = 'NUM'
                group by
                    indi,
                    ics_codi,
                    analisi,
                    professional
                """
        u.exportKhalix(sql, 'DEMANDAAGENDES')


if __name__ == '__main__':
    Demanda()