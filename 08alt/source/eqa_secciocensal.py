# coding: iso-8859-1
from sisapUtils import *
import csv,os,sys
from time import strftime

if IS_MENSUAL:
    printTime('Inici càlcul EQA secció censal')
    db = "altres"
    nod="nodrizas"
    eqa="eqa_ind"

    pac = "mst_indicadors_pacient"
    edats_k = "khx_edats5a"
    centres="export.khx_centres"
    eqaind="eqa_ind"

    sc_pre = "eqa_sc_up_ind"
    seccens = "exp_sc_up_ind"

    sexe = "if(sexe='H','HOME','DONA')"
    comb = "concat(if(institucionalitzat=1,'INS','NOINS'),if(ates=1,'AT','ASS'))"
    condicio_sc ="excl_edat=0 and excl=0 and ci=0 and clin=0 and (institucionalitzat=1 or (institucionalitzat=0 and maca=0))"
    conceptes=[['NUM','num'],['DEN','den']]

    OutFile= tempFolder + 'eqa_secciocensal.txt'

    tipusp=[['INSAT'],['INSASS'],['NOINSAT'],['NOINSASS']]


    ekhalix={}
    sql="select edat,khalix from {}".format(edats_k)
    for e,k in getAll(sql,nod):
        e=int(e)
        ekhalix[e]={'khalix':k}

    eqasc={}    
    sql="select id_cip_sec,up, seccio_censal seccio_censal,edat,{0} sexe,{1} comb,ind_codi,grup_codi,if(ind_codi like ('EQD%'),1,0),num,den from {2} where {3}" .format(sexe,comb,pac,condicio_sc)
    for id,up,sc,edat,sexe,comb,indcodi,pares,n,num,den in getAll(sql,eqa):
        id=int(id)
        edat=int(edat)
        num=int(num)
        den=int(den)
        if n==1:
            indcodi=indcodi           
        else:
            indcodi=pares
        try:
            edatk=ekhalix[edat]['khalix']
        except KeyError:
            ok=1
        if (up,sc,edatk,sexe,comb,indcodi) in eqasc:
            eqasc[(up,sc,edatk,sexe,comb,indcodi)]['num']+=num
            eqasc[(up,sc,edatk,sexe,comb,indcodi)]['den']+=den
        else:
            eqasc[(up,sc,edatk,sexe,comb,indcodi)]={'num':num,'den':den}

    with open(OutFile,'wb') as file:
        w= csv.writer(file, delimiter='@', quotechar='|')
        for (up,sc,edatk,sexe,comb,indcodi),d in eqasc.items():   
            num=d['num']
            den=d['den']
            w.writerow([up,sc,edatk,sexe,comb,indcodi,num,den])
          
    sql="drop table if exists {}".format(sc_pre)
    execute(sql,db)
    sql="create table {} (up varchar(5) not null default'',seccio_censal varchar(10) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',ind_codi varchar(10) not null default'',num double,den double)".format(sc_pre)
    execute(sql,db)
    loadData(OutFile,sc_pre,db)
    sql="drop table if exists {}".format(seccens)
    execute(sql,db)
    sql="create table {} (up varchar(5) not null default'',seccio_censal varchar(10) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',conc varchar(6) not null default '',n double)".format(seccens)
    execute(sql,db)
    for r in conceptes:
        conc,var=r[0],r[1]
        sql="insert into {0} select up,seccio_censal,edat,sexe,comb,ind_codi indicador,'{1}',{2} n from {3} where comb like '%%AT%%'".format(seccens,conc,var,sc_pre)
        execute(sql,db)
        sql="insert into {0} select up,seccio_censal,edat,sexe,replace(comb,'AT','ASS'),ind_codi indicador,'{1}',sum({2}) n from {3} group by 1,2,3,4,5,6".format(seccens,conc,var,sc_pre)
        execute(sql,db)

    printTime('export a Khalix')
    error= []

    taula="%s.exp_sc_up_ind" % db
    for t in tipusp:
        tp=t[0]
        query="select indicador,concat('B','periodo'),ics_codi,if(seccio_censal ='','NOSC',concat('S',right(seccio_censal,9))),conc,concat('S',edat),comb,sexe,'N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi where comb='%(tpob)s'" % {'taula': taula,'centres':centres,'tpob':tp}
        file="EQA_SECCIO_CENSAL_%s" % tp
        error.append(exportKhalix(query,file))

    taula="%s.exp_sc_up_ind" % db
    for t in tipusp:
        tp=t[0]
        query="select indicador,concat('B','periodo'),ics_codi,if(seccio_censal ='','NOSC',concat('S',left(seccio_censal,7))),conc,edat,comb,sexe,'N',sum(n) from %(taula)s a inner join %(centres)s b on a.up=scs_codi where comb='%(tpob)s' group by ics_codi,indicador,if(seccio_censal ='','NOSC',concat('S',left(seccio_censal,7))),conc,edat,comb,sexe" % {'taula': taula,'centres':centres,'tpob':tp}
        file="EQA_DISTRICTE_%s" % tp
        error.append(exportKhalix(query,file))
        
    if any(error):
        sys.exit("he hagut d'escriure al directori de rescat")

    printTime('Fi càlcul EQA secció censal')    