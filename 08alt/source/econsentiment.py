# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict,Counter
import csv,os,sys
from time import strftime

db = "altres"
imp = "import"
nod = "nodrizas"

OutFile = tempFolder + 'econsentiment.txt'
TaulaMy = 'exp_khalix_econs_up'

OutFileAss = tempFolder + 'econsentimentAss.txt'
TaulaMyAss = 'exp_khalix_econs_ass'

printTime('Inici Pob')

centres = {}
sql = 'select scs_codi,ics_codi from cat_centres'
for up,br in getAll(sql,nod):
    centres[up] = br

centresAssir,brAssir = {},{}
sql = 'select up,up_assir,br_assir from ass_centres'
for up,up_ass,br_ass in getAll(sql,nod):
    centresAssir[up] = up_ass
    brAssir[up_ass] = br_ass
    
Pob = {}
sql = 'select id_cip_sec,usua_uab_up from assignada'
for id,up in getAll(sql,imp):
    try:
        Pob[id] = {'up':up,'br':centres[up]}
    except KeyError:
        continue
        
PobAssir = defaultdict(set)
econsAssir = defaultdict(Counter)
sql = 'select id_cip_sec,visi_up from ass_imputacio_up'
for id,up in getAll(sql,nod):
    if up in centresAssir:
        up_assir = centresAssir[up]
        PobAssir[id].add(up_assir)
        econsAssir[up_assir]['den'] += 1

        
def econsConverter(mail,sms):
    if mail == 'A' and sms == 'A':
        return 'ECAA'
    elif mail == 'A' and sms == 'T':
        return 'ECAT'
    elif mail == 'T' and sms == 'A':
        return 'ECTA'
    elif mail == 'T' and sms == 'T':
        return 'ECTT'
    elif mail == 'A' and sms in ('', None):
        return 'ECAB'
    elif mail == '' and sms == 'A':
        return 'ECBA'
    elif mail == '' and sms in ('', None):
        return 'ECBB'
    elif mail == 'T' and sms in ('', None):
        return 'ECTB'
    elif mail == '' and sms == 'T':
        return 'ECBT'
    elif mail == 'A' and sms == 'N':
        return 'ECAN'
    elif mail == 'N' and sms == 'A':
        return 'ECNA'
    elif mail == 'T' and sms == 'N':
        return 'ECTN'
    elif mail == 'N' and sms == 'T':
        return 'ECNT'
    elif mail == 'N' and sms == 'N':
        return 'ECNN'
    elif mail == 'N' and sms in ('', None):
        return 'ECNB'
    elif mail == '' and sms == 'N':
        return 'ECBN'
    else:
        print mail, sms
        return 'error'


printTime('Inici econsentiment')

econs = Counter()
sql = 'select id_cip_sec,usua_com_email,usua_sms from econsentiment'
for id,mail,sms in getAll(sql,imp):
    try:
        up = Pob[id]['up']
        br = Pob[id]['br']
        econs[(up,br,econsConverter(mail,sms))] += 1
    except KeyError:
        pass
    if id in PobAssir:
        ups = PobAssir[id]
        for upassir in ups:
            econsAssir[(upassir)]['num'] += 1
        
with openCSV(OutFile) as c:
    for (up,br,indicador),count in econs.items():
        c.writerow([up,br,indicador,count])
    
execute('drop table if exists {}'.format(TaulaMy),db)
execute('create table {} (up varchar(5),br varchar(5),indicador varchar(10), recompte double)'.format(TaulaMy),db)
loadData(OutFile,TaulaMy,db)

with openCSV(OutFileAss) as c:
    for (up),count in econsAssir.items():
        num = count['num']
        den = count['den']
        try:
            brass = brAssir[up]
        except KeyError:
            continue
        c.writerow([up,num,den])

execute('drop table if exists {}'.format(TaulaMyAss),db)
execute('create table {} (up varchar(5), num double,den double)'.format(TaulaMyAss),db)
loadData(OutFileAss,TaulaMyAss,db)

error = []

sql = "select indicador,concat('A','periodo'),br,'NOCLI','NOCAT','NOIMP','DIM6SET','N',recompte from {}.{}".format(db,TaulaMy)
file = 'econsentiment'
error.append(exportKhalix(sql,file))

sql = "select 'ECONSASSIR',concat('A','periodo'),br_assir,'NUM','NOCAT','NOIMP','DIM6SET','N',num from {0}.{1} a inner join nodrizas.ass_centres b on a.up=b.up_assir \
       union select 'ECONSASSIR',concat('A','periodo'),br_assir,'DEN','NOCAT','NOIMP','DIM6SET','N',den from {0}.{1} a inner join nodrizas.ass_centres b on a.up=b.up_assir ".format(db,TaulaMyAss)
file = 'econs_ASSIR'
error.append(exportKhalix(sql,file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

        
printTime('Final')