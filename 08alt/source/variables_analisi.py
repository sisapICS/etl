# coding: utf8

"""
Procés per obtenir les variables bàsiques per a anàlisis de factors que influeixen en els indicadors
"""

import urllib as w
import sys,datetime
from time import strftime
import sisapUtils as u
import sisaptools as t
from collections import Counter

db = ("shiny", "korra")

file_idescat = "dades_noesb/taula_korra_ist_idescat.txt"

class VariAnalisi(object):
    """Classe principal a instanciar."""

    def __init__(self):
        """Execució seqüencial."""
        self.get_centres()
        self.get_gma()
        self.get_medea()
        self.get_atdom()
        self.get_professions()
        self.get_idescat()
        self.get_poblacio()
        self.export_data()        

    def get_centres(self):
        """EAP ICS."""
        u.printTime("centres")

        sql = ("select scs_codi, ics_codi, amb_codi, sap_codi, ep, aquas \
                from cat_centres", "nodrizas")
        self.centres = {up: (br, ep, aquas) for (up, br, amb, sap, ep, aquas)
                        in u.getAll(*sql)}  
    
    def get_gma(self):
        """Obté la complexitat del gma"""
        u.printTime("gma")
        self.gma = {}
        sql = ("select id_cip_sec, gma_ind_cmplx \
                from gma", "import")
        for id, cmplx in u.getAll(*sql):
            self.gma[id] = cmplx
            
    def get_medea(self):
        """Ýndex MEDEA del pacient segons el seu codi de secció censal."""
        u.printTime("medea")
        self.medea = {}
        valors = {sector: valor for (sector, valor)
                  in u.getAll("select sector, valor from sisap_medea",
                              "redics")}
        sql = ("select id_cip_sec, sector_censal from crg", "import")
        for id, sector in u.getAll(*sql):
            if sector in valors:
                self.medea[id] = valors[sector]

    def get_atdom(self):
        """Pacients en ATDOM a final de període."""
        u.printTime("atdom")
        self.atdom = {}
        sql = ("select id_cip_sec from eqa_problemes where ps = 45",
               "nodrizas")
        for id, in u.getAll(*sql):
            self.atdom[id] = True
  
    def get_professions(self):
        """Extreure professions de taula permanent. Canviar quan taula estigui a import. de moment agregat perquè no tinc manera d ajuntar id cip sec i hash covid"""
        u.printTime("professions")
        self.professions = Counter()
        
        sql = "select up, edat, sexe, cod_niv1 from sisap_covid_recerca_professions where cod_niv1 in (2,9)"
        for up, edat, sexe, prof in u.getAll(sql, ('backup', 'analisi')):
            br = self.centres[up][0] if up in self.centres else None
            edat_k = u.ageConverter(edat)
            sexe_k = u.sexConverter(sexe)
            self.professions[(br, edat_k, sexe_k, str(prof))] +=1
    
    def get_idescat(self):
        """Obtenim els indicadors de idescat"""
        u.printTime("idescat")
        indicadors_idescat = {}       
        
        for (ac, ac_nom, seccio, seccio_llarga, ist, decils_ist,pob_ocupada,treb_baixa_qualitat,pob_estudis_baixos,pob_jove_sense_estudis_postobligatoris,imm_renda_baixa,renda_minima) in u.readCSV(file_idescat, sep='@'):
            indicadors_idescat[(seccio_llarga)] = {'ist': ist, 'ocu': pob_ocupada, 'baixa': treb_baixa_qualitat, 'estudis': pob_estudis_baixos, 'joves': pob_jove_sense_estudis_postobligatoris, 'imm': imm_renda_baixa, 'renda': renda_minima}
        
        self.idescat_ind = {}
        sql = """SELECT id_cip_sec, seccio FROM import.rca"""
        for id, seccio in u.getAll(sql, 'import'):
            ist = indicadors_idescat[(seccio)]['ist'] if seccio in indicadors_idescat else None
            ocu = indicadors_idescat[(seccio)]['ocu'] if seccio in indicadors_idescat else None
            baixa = indicadors_idescat[(seccio)]['baixa'] if seccio in indicadors_idescat else None
            estudis = indicadors_idescat[(seccio)]['estudis'] if seccio in indicadors_idescat else None
            joves = indicadors_idescat[(seccio)]['joves'] if seccio in indicadors_idescat else None
            imm = indicadors_idescat[(seccio)]['imm'] if seccio in indicadors_idescat else None
            renda = indicadors_idescat[(seccio)]['renda'] if seccio in indicadors_idescat else None
            if ist != None:
                self.idescat_ind[id] = {'ist': ist, 'ocu': ocu, 'baixa': baixa, 'estudis': estudis, 'joves': joves, 'imm': imm, 'renda': renda}
        
    
    def get_poblacio(self):
        """."""
        u.printTime("poblacions")
        self.dades = Counter()

        renta = set([nac for nac, in u.getAll("select concat(codi_nac, '') \
                                               from cat_nacionalitat \
                                               where renta = 0", "nodrizas")])
        sql = ("select id_cip_sec, up, edat, sexe, ates, \
                       institucionalitzat, nacionalitat, nivell_cobertura, pcc, maca \
                from assignada_tot", "nodrizas")
        for id, up, edat, sexe, ates, insti, nac, cob, pcc, maca in u.getAll(*sql):
            if up in self.centres:
                br = self.centres[up][0]
                ep = self.centres[up][1]
                aquas = self.centres[up][2]
                cob1 = 1 if cob == 'PN' else 0
                cob2 = 0
                if cob1 == 1 and edat < 65:
                    cob2 = 1
                nac1 = 1 if nac in renta else 0
                atdom1 = self.atdom[id] if id in self.atdom else 0
                edat_k = u.ageConverter(edat)
                sexe_k = u.sexConverter(sexe)
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'rec')]+= 1
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'edat')]+= edat
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'insti')]+= insti
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'immi')]+= nac1
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'cob')]+= cob1
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'cob65')]+= cob2
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'pcc')]+= pcc
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'maca')]+= maca
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'atdom')]+= atdom1
                self.dades[(up, br, ep, ates, edat_k, sexe_k, 'aquas')]+= aquas
                if id in self.gma:
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'den_gma')]+= 1
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'num_gma')]+= self.gma[id]
                if id in self.medea:
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'den_medea')]+= 1
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'num_medea')]+= self.medea[id]
                if id in self.idescat_ind:
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'den_seccio')]+= 1
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'ist')]+=  float(self.idescat_ind[(id)]['ist']) 
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'ocu')]+=  float(self.idescat_ind[(id)]['ocu'])
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'baixa')]+=  float(self.idescat_ind[(id)]['baixa']) 
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'estudis')]+=  float(self.idescat_ind[(id)]['estudis']) 
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'joves')]+=  float(self.idescat_ind[(id)]['joves'])
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'imm')]+=  float(self.idescat_ind[(id)]['imm']) 
                    self.dades[(up, br, ep, ates, edat_k, sexe_k, 'renda')]+=  float(self.idescat_ind[(id)]['renda'])
                    
    
    def export_data(self):
        """."""
        u.printTime("export")
        upload = []
        for (up, br, ep, ates, edat, sexe, tipus), n in self.dades.items():
            if tipus == 'rec':
                prof9, prof2 = 0, 0
                sum_dona = n if sexe == 'DONA' else 0
                if ates == 1:
                    prof9 = self.professions[(br, edat, sexe, '9')]
                    prof2 = self.professions[(br, edat, sexe, '2')]
                upload.append([up, br, ep, ates, edat, sexe, n, sum_dona, self.dades[(up, br, ep, ates, edat, sexe,'edat')], self.dades[(up, br, ep, ates, edat, sexe,'insti')],
                self.dades[(up, br, ep, ates, edat, sexe,'immi')], self.dades[(up, br, ep, ates, edat, sexe,'cob')], self.dades[(up, br, ep, ates, edat, sexe,'cob65')],self.dades[(up, br, ep, ates, edat, sexe,'pcc')],
                self.dades[(up, br, ep, ates, edat, sexe,'maca')], self.dades[(up, br, ep, ates, edat, sexe,'atdom')], self.dades[(up, br, ep, ates, edat, sexe,'den_gma')],
                self.dades[(up, br, ep, ates, edat, sexe,'num_gma')], self.dades[(up, br, ep, ates, edat, sexe,'den_medea')],  self.dades[(up, br, ep, ates, edat, sexe,'num_medea')], self.dades[(up, br, ep, ates, edat, sexe,'aquas')],
                prof9, prof2, self.dades[(up, br, ep, ates, edat, sexe,'den_seccio')], self.dades[(up, br, ep, ates, edat, sexe,'ist')], self.dades[(up, br, ep, ates, edat, sexe,'ocu')], self.dades[(up, br, ep, ates, edat, sexe,'baixa')],
                self.dades[(up, br, ep, ates, edat, sexe,'estudis')], self.dades[(up, br, ep, ates, edat, sexe,'joves')], self.dades[(up, br, ep, ates, edat, sexe,'imm')], self.dades[(up, br, ep, ates, edat, sexe,'renda')]])
        t.Database(*db).list_to_table(upload, tb)
        
                
if __name__ == '__main__':
    u.printTime("Inici")
    
    tb = "mst_variables_analisi"
    columns =  ["up varchar(5)", "br varchar(5)", "ep varchar(10)", "ates int", "edat varchar(10)", "sexe varchar(5)", "poblacio int", "sum_dona double", "sum_edat double", "institucionalitzats int",
                "immigrants_renta_baixa int", "pensionistes int", "pens_menys_65 int", "pcc int", "maca int", "atdom int", "den_gma int", "num_gma double", 
                "den_medea int", "num_medea double", "aquas double", "prof_grup9 int", "prof_grup2 int", "den_seccio int", "ist double", "pob_ocupada double", "treb_baixa_qualitat double", "estudis_baixos double", "jove_sense_estudis double", "imm_renda_baixa_idescat double",
                "renda_minima int"]     
    t.Database(*db).create_table(tb, columns, remove=True)
    
    VariAnalisi()
    
    u.printTime("Fi")
