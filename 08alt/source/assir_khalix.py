import sisapUtils as u
# import time
# from collections import defaultdict, Counter
# from datetime import *

file_name = "AG_ASSIR"

codis = ["LONGASS001", "AGASSEMAIL", "AGASSDEP", "LONGASS002"]

taulas = ["exp_khalix_{}_up".format(codi) for codi in codis]

query_template_string = """
    SELECT
        indicador, concat('A','periodo'), br_assir, {strg},
        'NOCAT', 'NOIMP', 'DIM6SET', 'N', sum({var})
    FROM
        {db}.{taula} a INNER JOIN
        nodrizas.ass_centres b
            ON a.up = b.up
    WHERE
        a.{var} != 0
    GROUP BY
        br_assir
    """
query_string = "UNION".join(
    query_template_string.format(strg="'NUM'",
                                 var="numerador",
                                 taula=taula_name_kh,
                                 db="altres") +
    "UNION" +
    query_template_string.format(strg="'DEN'",
                                 var="denominador",
                                 taula=taula_name_kh,
                                 db="altres")
    for taula_name_kh in taulas)
query_string = query_string + """ UNION
                                SELECT 'AGASSDXVM', concat('A','periodo'), br_assir, analisi, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', sum(val) FROM (
                                SELECT DISTINCT  br_assir, analisi, val
                                FROM ALTRES.AGASSDXVM a,
                                nodrizas.ass_centres b
                                WHERE
                                a.up = b.up_assir) a
                                GROUP BY 'AGASSDXVM', concat('A','periodo'), br_assir, analisi, 'NOCAT', 'NOIMP', 'DIM6SET', 'N'"""
print(query_string)
u.exportKhalix(query_string, file_name)

# bolet per Longass03, per noves dimensions
file_name = "LONGASS"

query_string = """select
                    'LONGASS003',
                    concat('A', 'periodo'),
                    br_assir,
                    'NUM',
                    'NOCAT',
                    case
                        when TIPUS = 'G' then 'TIPGIN'
                        else 'TIPLLE'
                    end TIPUSG,
                    'DIM6SET',
                    'N',
                    sum(numerador)
                from
                    altres.exp_khalix_LONGASS003_up a,
                    nodrizas.ass_centres b
                where
                    a.up = b.up
                group by
                    'LONGASS003',
                    concat('A', 'periodo'),
                    br_assir,
                    'NUM',
                    'NOCAT',
                    TIPUSG,
                    'DIM6SET',
                    'N'
                union
                select
                    'LONGASS003',
                    concat('A', 'periodo'),
                    br_assir,
                    'DEN',
                    'NOCAT',
                    case
                        when TIPUS = 'G' then 'TIPGIN'
                        else 'TIPLLE'
                    end TIPUSG,
                    'DIM6SET',
                    'N',
                    sum(denominador)
                from
                    altres.exp_khalix_LONGASS003_up a,
                    nodrizas.ass_centres b
                where
                    a.up = b.up
                group by
                    'LONGASS003',
                    concat('A', 'periodo'),
                    br_assir,
                    'DEN',
                    'NOCAT',
                    TIPUSG,
                    'DIM6SET',
                    'N'"""
print(query_string)

u.exportKhalix(query_string, file_name)
