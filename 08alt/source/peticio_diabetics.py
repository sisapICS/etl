import sisapUtils as u
import collections as c
from datetime import date

assignats = set()
validar = set()
sql = """select id_cip_sec, up from nodrizas.assignada_tot"""
for id, up in u.getAll(sql, 'nodrizas'):
    assignats.add(id)
    if up == "00295":
        validar.add(id)

# seleccionem tires x pacients
print('tires')
diabetics = {}
sql = """select id_cip_sec, tires,
	case when round(tires/7,0) > 6 then 7 else round(tires/7,0) end as tires_dia,
    case when dm2 = 1 then '2' else '0' end tipus_dm
    from altres.tir_pacient"""
for id, tires_s, tires_d, tipus_dm in u.getAll(sql, 'altres'):
    if id not in assignats:
        print('no assignat')
    diabetics[id] = [str(tipus_dm), tires_s, tires_d, None]

# mirem tipus de dm
sql = """select id_cip_sec, ps, dde from nodrizas.eqa_problemes
            where ps = 24 or ps = 487 or ps = 18"""
for id, ps, dde in u.getAll(sql, 'nodrizas'):
    if ps == 24: ps = '1'
    elif ps == 487: ps = 'G'
    elif ps == 18: ps = '2'
    if id in diabetics:
        if str(diabetics[id][0]) == '0':
            diabetics[id][0] = ps
        if diabetics[id][3] == None or diabetics[id][3] > dde: diabetics[id][3] = dde
    else:
        diabetics[id] = [str(ps), 0, 0, dde]

# tires rapides i lentes
tires_r_l = {}
print('tires')
sql = """select id_cip_sec, ppfmc_atccodi from import.tractaments
        where (ppfmc_atccodi like 'A10AC%' or ppfmc_atccodi like 'A10AB%' 
        or ppfmc_atccodi like 'A10AD%' or ppfmc_atccodi like'A10AE%')
        and ppfmc_data_fi > current_date"""
for id, codi in u.getAll(sql, 'import'):
    if id in diabetics:
        if id in tires_r_l:
            if codi[0:5] in ('A10AD','A10AE','A10AC'): tires_r_l[id][0] = 1
            elif codi[0:5] == 'A10AB': tires_r_l[id][1] = 1
        else:
            l, r = 0, 0
            if codi[0:5] in ('A10AD','A10AE','A10AC'): l = 1
            elif codi[0:5] == 'A10AB': r = 1
            tires_r_l[id] = [l, r]

# obtenim ups i edats
print('info')
pacients = {}
menors = set()
sql = """select id_cip_sec, up, edat from nodrizas.assignada_tot"""
for id, up, edat in u.getAll(sql, 'nodrizas'):
    if edat < 18: e = 0
    elif edat >= 18 and edat <= 65: e = 1
    else: e = 2
    if id in diabetics:
        pacients[id] = [up, e]


# problemes deficits
print('deficits')
deficits = set()
sql = """select id_cip_sec from nodrizas.eqa_problemes 
        where ps in
        (61, 68, 86, 98, 114, 218, 301, 302, 303, 
        306, 345, 346, 348, 349, 428, 429, 463, 
        703, 783, 788, 877, 878)"""
for id, in u.getAll(sql, 'nodrizas'):
    deficits.add(id)

# ajudes 3a persones
print('3a pers')
necessitat_3a = set()
sql = """select id_cip_sec from import.problemes
        where pr_cod_ps  in ('IRE02618', 'C01-T38.3X4A', 'C01-T38.3X1S', 
        'IRE02618', 'IRI12646', 'IRE15578', 'IRE05912', 'E16.0', 
        'E16.1', 'E16.2', 'C01-E16.0', 'C01-E16.1', 'C01-E16.2', 
        'C01-E13.64', 'C01-E13.641', 'C01-E11.64', 'C01-E11.649', 
        'C01-T38.3X2S','C01-T38.3X3', 'C01-T38.3X3A', 'C01-T38.3X3D', 
        'C01-T38.3X3S', 'C01-T38.3X4', 'C01-T38.3X4D', 'C01-T38.3X4S', 
        'C01-T38.3X1', 'C01-T38.3X1A', 'C01-T38.3X1D', 'C01-T38.3X2', 
        'C01-T38.3X2A', 'C01-T38.3X2D', 'C01-E11.641', 'C01-E13.649')
        and pr_dde >= date_add(current_date, interval -2 year)"""
for id, in u.getAll(sql, 'nodrizas'):
    necessitat_3a.add(id)

# embarassades
print('embarassos')
embarassades = set()
embarassades_utim_any = set()
sql = """select id_cip_sec,
        case when fi = date('2022-11-09') then 1 else 0 end embarassada_ara 
        from nodrizas.ass_embaras ae where fi >= date('2021-11-09')"""
for id, activa in u.getAll(sql, 'nodrizas'):
    embarassades_utim_any.add(id)
    if activa == 1:
        embarassades.add(id)

# inestables
print('inestables')
inestables = set()
sql = """select id_cip_sec from nodrizas.eqa_problemes
        where ps in (21,27,40,160,228,258,275,276,506,511,521,522,529,560,593,
        646,663,677,708,721,722,730,731,732,733,734,787,831,876)"""
for id, in u.getAll(sql, 'nodrizas'):
    inestables.add(id)

print('cocinitas')
t = c.Counter()
val_souhel = []
exa = []
for id, (tipus_dm, tires_s, tires_d, data_alta) in diabetics.items():
    if id in tires_r_l:
        l, r = tires_r_l[id]
    else: l, r = 0, 0
    if id in pacients:
        up, edat = pacients[id]
        if id in deficits: d = 1
        else: d = 0
        if id in necessitat_3a: n3 = 1
        else: n3 = 0
        if id in embarassades: embaras = 1
        else: embaras = 0
        if id in inestables: inestable = 1
        else: inestable = 0
        if id in embarassades_utim_any: emb_ultim_any = 1
        else: emb_ultim_any = 0
        t[up, edat, l, r, tires_s, tires_d, d, n3, embaras, inestable] += 1
        exa.append((id, up, edat, l, r, tires_s, tires_d, d, n3, embaras, inestable, emb_ultim_any, str(tipus_dm), data_alta))
        if id in validar:
            val_souhel.append((id, up, edat, l, r, tires_s, tires_d, d, n3, embaras, inestable))

# cols = """(id varchar(50), up varchar(5), edat int, L int, R int, tires_setmana int, tires_dia int, d int, n3 int, embaras int, inestabe int)"""
# u.createTable('val_s_diabetis', cols, 'test', rm=True)
# u.listToTable(val_souhel, 'val_s_diabetis', 'test')

# upload = []
# for k, v in t.items():
#     upload.append((k[0], k[1], k[2], k[3], k[4], k[5], k[6], k[7], k[8], k[9], v))

# cols = """(up varchar(5), edat int, L int, R int, tires_setmana int, tires_dia int, d int, n3 int, embaras int, inestabe int, val int)"""
# u.createTable('diabetis_ministerio', cols, 'test', rm=True)
# u.listToTable(upload, 'diabetis_ministerio', 'test')

print('to exadata')

id_cip_2_hash = {}
sql = """select id_cip_sec, hash_d from import.u11"""
for id_cip, hash in u.getAll(sql, 'import'):
    id_cip_2_hash[id_cip] = hash

hash_2_cip = {}
sql = """SELECT usua_cip, usua_cip_cod from pdptb101"""
for cip, hash in u.getAll(sql, 'pdp'):
    hash_2_cip[hash] = cip

today = date.today()

upload = []
i = 0
for k in exa:
    id_cip_sec = k[0]
    try:
        hash = id_cip_2_hash[id_cip_sec]
        cip = hash_2_cip[hash]
        upload.append((cip, k[1], k[2], k[3], k[4], k[5], k[6], k[7], k[8], k[9], k[10], k[11], k[12], k[13], today))
    except:
        i+=1
        pass
print('els q no he trobat', i)

cols = """(cip varchar2(20), up varchar2(5), edat int, L int, R int, tires_setmana int, 
            tires_dia int, d int, n3 int, embaras_actiu int, inestabe int,
            embaras_ultim_any int, tipus_dm varchar2(1), data_alta_dm date, dat_actualitzacio date)"""
u.createTable('diabetis_tires_2', cols, 'exadata', rm=True)
u.listToTable(upload, 'diabetis_tires_2', 'exadata')
u.grantSelect('diabetis_tires_2', 'DWSISAP_ROL', 'exadata')



