# coding: iso-8859-1
from sisapUtils import *
from collections import defaultdict, Counter


nod = 'nodrizas'
db = 'altres'

#ag_LONG

def get_especialitats():
    especialitats = {}
    for espe, desc, cont in readCSV('./dades_noesb/especialitats.txt', sep='@'):
        if int(cont) == 0:
            especialitats[espe] = True
    return especialitats


def get_tipus_visita():
    tipus_visita = {}
    for espe, desc, cont in readCSV('./dades_noesb/tipus_visita.txt', sep='@'):
        if int(cont) == 0:
            tipus_visita[espe] = True
    return tipus_visita


def get_poblacio():
    pob = {}
    sql = 'select id_cip_sec, up, uba, ubainf, institucionalitzat from assignada_tot'
    for id, up, uba, ubainf, insti in getAll(sql, nod):
        if insti == 0:
            pob[id] = {'up': up, 'uba': uba, 'ubainf': ubainf}
    return pob


def get_pacients(pob):
    pacients = {}
    sql = 'select id_cip_sec from ag_long_pacients'
    for id, in getAll(sql, nod):
        try:
            up = pob[id]['up']
            uba = pob[id]['uba']
            ubainf = pob[id]['ubainf']
        except KeyError:
            continue
        pacients[id] = {'up': up, 'uba': uba, 'ubainf': ubainf}
    return pacients


def get_longitudinalitat(pacients, especialitats, tipus_visita):
    eap = set([up for up, in getAll("select scs_codi from cat_centres", "nodrizas")])
    recompte = Counter()
    recompte_uba = Counter()
    ag_long = []
    ag_long_uba = []
    sql = "select id_cip_sec, especialitat, tipus, relacio, numcol, delegat, gcasos, date_format(dat,'%Y%m'), up from ag_long_visites"
    for id, espe, tipus, rel, numcol, delegat, gescas, periode, up_vis in getAll(sql, nod):
        if id in pacients and (up_vis in eap or gescas):
            up = pacients[id]['up']
            uba = pacients[id]['uba']
            ubainf = pacients[id]['ubainf']
            den = 1
            num = 0
            excl = 0
            if espe in especialitats:
                excl = 1
            if tipus in tipus_visita:
                excl = 1
            if excl == 1:
                den = 0
            if (int(rel) == 1 or int(numcol) == 1 or int(delegat) == 1 or int(gescas) == 1) and den == 1:
                num = 1
            recompte[up,'NUM'] += num
            recompte[up,'DEN'] += den
            recompte_uba[(up, uba, 'M'), 'NUM'] += num
            recompte_uba[(up, uba, 'M'), 'DEN'] += den
            if ubainf:
                recompte_uba[(up, ubainf, 'I'), 'NUM'] += num
                recompte_uba[(up, ubainf, 'I'), 'DEN'] += den
    for (up, tip), rec in recompte.items():
        ag_long.append([up, tip , rec])
    listToTable(ag_long, table_long, db)
    for ((up, uba, categ), tip), rec in recompte_uba.items():
        ag_long_uba.append([up, uba, categ, tip , rec])
    listToTable(ag_long_uba, table_long_uba, db)

especialitats = get_especialitats()
tipus_visita = get_tipus_visita()
pob = get_poblacio()
pacients = get_pacients(pob)
table_long = 'exp_khalix_ag_longitudinalitat'
create = "(up varchar(5) not null default'', tipus varchar(10), recompte int)"
createTable(table_long,create,db, rm=True)
table_long_uba = 'exp_khalix_ag_longitudinalitat_uba'
create = "(up varchar(5) not null default'', uba varchar(5), categ varchar(1), tipus varchar(10), recompte int)"
createTable(table_long_uba,create,db, rm=True)
get_longitudinalitat(pacients, especialitats, tipus_visita)

error = []
centres="export.khx_centres"
sql = "select  'AGACCONT', concat('A','periodo'), ics_codi, tipus, 'NOCAT', 'NOIMP', 'DIM6SET','N',recompte from {0}.{1} a inner join {2} b on a.up=b.scs_codi".format(db, table_long, centres)
file = 'AG_LONG_UP'
error.append(exportKhalix(sql, file))

sql = "select  'AGACCONT', concat('A','periodo'), concat(ics_codi, categ, uba), tipus, 'NOCAT', 'NOIMP', 'DIM6SET','N',recompte from {0}.{1} a inner join {2} b on a.up=b.scs_codi".format(db, table_long_uba, centres)
file = 'AG_LONG_UBA'
error.append(exportKhalix(sql, file))

if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")
