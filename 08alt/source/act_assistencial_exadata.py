# -*- coding: utf8 -*-

"""
.
"""

import collections as c
import datetime as d
from dateutil import relativedelta as rd
import sisapUtils as u
import sisaptools as t
import os

TEST = False
RECOVER = False

nod = "nodrizas"
imp = "import"
imp_jail = "import_jail"

SERVEI_MAP = {
    'EXTRA': 'EXTRA',
    'EXT': 'EXTRA',
    'UAC': 'UAC',
    'UAAU': 'UAC',
    'ASO': 'TS',
    'AS': 'TS',
    'ASS': 'TS',
    'TS': 'TS',
    'TSOC': 'TS',
    'SS': 'TS',
    'RNAS': 'TS',
    'ASI': 'TS',
    'AASS': 'TS',
}

HOMOL_MAP = {
    'MG': 'MF',
    'INFP': 'INF',
    'LLEV': 'ALTRE',
    'GIN': 'ALTRE',
}

TIPUS_MAP = {
    '9C': 'C9C',
    '9R': 'C9R',
    '9D': 'D9D',
    '9T': '9T',
    '9E': '9E',
    '9Ec': '9EC'
}

ETIQUETAS = [
    'INFC',  # INFORME CLÝNIC
    'PLME',  # PLA MEDICACIO
    'REIT',  # REVISIO IT
    'TRSA',  # TRANSPORT SANITARI
    'VALP',  # VALORACIÓ PROVES
    'VITA',  # VÝDEO CONSULTA
]

KCAT = 'NOCAT'
KCLI = 'NOCLI'
KVAL = 'N'

if RECOVER:
    DEXTD = d.date(2024, 1, 30)
    DEXTD_P = d.date(2023, 12, 31)
    FILE_NAME = 'ACTASSIST_YTD_{}.txt'.format(DEXTD.strftime("%m_%Y"))
    FILE_NAME_ANUAL = 'ACTASSIST_ANUAL_{}.txt'.format(DEXTD.strftime("%m_%Y"))
else:
    DEXTD = t.Database("p2262", nod).get_one("select data_ext from dextraccio")[0]  # noqa
    DEXTD_P = t.Database("p2262", nod).get_one("select date_add(data_ext,interval - 1 month) from NODRIZAS.dextraccio")[0]  # noqa
    FILE_NAME = 'ACTASSIST_YTD'
    FILE_NAME_ANUAL = 'ACTASSIST_ANUAL'



def get_gedat(naix, vdat):
    # Get the current date
    # Get the difference between the current date and the birthday
    age = rd.relativedelta(vdat, naix)
    age = age.years
    gedat = u.ageConverter(age)
    return gedat

class Activitat(object):
    """."""

    def __init__(self):
        """."""
        cols = """(cuenta varchar(50), periode varchar(50), br varchar(50), 
                    KCLI varchar(50), edat_k varchar(50), KDTL varchar(50),
                    sex_k varchar(50), KVAL varchar(50), VALOR int)"""
        u.createTable('simulacio_visites_exadata', cols, 'altres', rm=True)
        u.createTable('simulacio_visites_exadata_anual', cols, 'altres', rm=True)
        u.printTime('get_centres')
        sql = """select scs_codi, ics_codi from nodrizas.cat_centres"""
        self.up_to_br = {}
        for up, br in u.getAll(sql, "nodrizas"):
            self.up_to_br[up] = br
        u.printTime('pob')
        self.poblacio = {}
        sql = """SELECT
                    hash,
                    ecap_up,
                    data_naixement,
                    CASE
                        WHEN sexe = 'D' THEN 'DONA'
                        WHEN sexe = 'H' THEN 'HOME'
                        WHEN sexe = 'M' THEN 'DONA'
                    END SEXE
                FROM
                    dwsisap.dbc_poblacio"""
        for id, up, dat_naix, sexe in u.getAll(sql, 'exadata'):
            self.poblacio[id] = (dat_naix, sexe, up) 
        u.printTime('get_visites')
        self.get_visites()
        # self.get_visites_noassates()
        self.to_upload()
        u.printTime('fi')

    def get_visites(self):
        """."""
        self.visites = c.defaultdict(set)
        sql = """
                SELECT
                    v.pacient,
                    v.sector,
                    CASE
                        WHEN v.SISAP_SERVEI_CODI = 'EXTRA' THEN v.SISAP_SERVEI_CODI
                        ELSE v.SISAP_SERVEI_CLASS
                    END SERVEI,
                    v.modul,
                    v.LLOC,
                    v.TIPUS,
                    v.ETIQUETA,
                    v.DATA,
                    extract(year FROM v.DATA) as year_v,
                    v.up,
                    v.RELACIO_PROVEIDOR,
                    CASE WHEN v.DATA > add_months(DATE '{DEXTD}',-1) THEN 'ACTUAL' 
                        WHEN v.DATA > add_months(DATE '{DEXTD}',-12) THEN 'BOTH'
                        ELSE 'PAST' 
                    END PERIODE_ANUAL
                FROM
                    dwsisap.sisap_master_visites v
                WHERE
                    v.situacio = 'R'
                    AND v.data BETWEEN add_months(DATE '{DEXTD}',-13) AND DATE '{DEXTD}'
                    """.format(DEXTD=DEXTD)
        self.cuentas = c.Counter()
        self.cuentas_anuals = c.Counter()
        for cip, sec, servei, modul, lloc, tip, tag, vdat, year_v, up, relacio, pa in u.getAll(sql,'exadata'):
            self.visites[cip].add((sec, servei, modul, lloc, tip, tag, vdat, year_v, up, relacio, pa))
        
        print('tinc totes les visites, anem a tractar info')
        for cip, mul_visites in self.visites.items():
            if cip in self.poblacio:
                dat_naix, sex_k, up_ass = self.poblacio[cip]
                for sec, servei, modul, lloc, tip, tag, vdat, year_v, up, relacio, pa in mul_visites:
                    if up in self.up_to_br:
                        if dat_naix <= vdat:
                            edat_k = get_gedat(dat_naix, vdat)
                            br = self.up_to_br[up]
                            ym = vdat.strftime('%y%m')
                            periode = "A{}".format(ym)
                            periode_anual = []
                            if pa == 'BOTH':
                                periode_anual.append("B{}".format(DEXTD_P.strftime('%y%m')))
                                periode_anual.append("B{}".format(DEXTD.strftime('%y%m')))
                            elif pa == 'ACTUAL':
                                periode_anual.append("B{}".format(DEXTD.strftime('%y%m')))
                            else: 
                                periode_anual.append("B{}".format(DEXTD_P.strftime('%y%m')))
                            if tip in TIPUS_MAP:  # ['9C', '9R', '9D', '9T', '9E']
                                if tip == '9E' and tag in ETIQUETAS:
                                    tipus = tip + tag
                                else:
                                    tipus = TIPUS_MAP[tip]
                            elif lloc in ['C', 'D']:
                                tipus = lloc + 'AL'
                            else:
                                tipus = 'ERROR'
                            if servei in ('AUX','NONE', 'GIN', 'LLE') or servei is None:
                                servei = 'ALTRE'
                            cuenta = "VIS{}{}".format(servei, tipus)
                            if up_ass is None:
                                detalle = 'VISNOASS'
                            elif relacio in ('3', '4'):
                                if up == up_ass:
                                    detalle = 'VISASSIG'
                                else:
                                    detalle = 'VISNOASS'
                            elif relacio in ('1', '2'):
                                detalle = 'VISASSIG'
                            else:
                                detalle = 'VISNO'
                            if year_v == DEXTD.year or (year_v == DEXTD.year - 1 and DEXTD.month == 1):
                                self.cuentas[(cuenta, periode, br, edat_k, sex_k, detalle)] += 1
                            for p_anual in periode_anual:
                                self.cuentas_anuals[(cuenta, p_anual, br, edat_k, sex_k, detalle)] += 1

    def get_visites_noassates(self):
        cat_serveis = {}
        sql = """SELECT sector, cod, 
                CASE WHEN servei_map = 'EXTRA' THEN servei_map
                ELSE servei_class END SERVEI 
                FROM DWSISAP.SISAP_MAP_SERVEIS map_serveis"""
        for sector, codi, servei in u.getAll(sql, 'exadata'):
            cat_serveis[(sector, codi)] = servei
        poblacio = {}
        sql = """SELECT
                    hash,
                    ecap_up,
                    data_naixement,
                    CASE
                        WHEN sexe = 'D' THEN 'DONA'
                        WHEN sexe = 'H' THEN 'HOME'
                        WHEN sexe = 'M' THEN 'DONA'
                    END SEXE
                FROM
                    dwsisap.dbc_poblacio
                WHERE
                    ecap_up IS NOT NULL"""
        for id, up, dat_naix, sexe in u.getAll(sql, 'exadata'):
            poblacio[id] = (dat_naix, sexe, up) 
        tables = getOne('show create table visites2', 'import')[1].split('UNION=(')[1][:-1].split(',')
        jobs = [(t, poblacio, edatKHLX) for t in tables]
        resul = multiprocess(do_it, jobs)

    def to_upload(self):
        upload = []
        for k, v in self.cuentas.items():
            upload.append([k[0], k[1], k[2], KCLI, k[3], k[5], k[4], KVAL, v])
        u.listToTable(upload, 'simulacio_visites_exadata', 'altres')
        u.exportKhalix('select * from altres.simulacio_visites_exadata', FILE_NAME, force=True)
        upload = []
        for k, v in self.cuentas_anuals.items():
            upload.append([k[0], k[1], k[2], KCLI, k[3], k[5], k[4], KVAL, v])
        u.listToTable(upload, 'simulacio_visites_exadata_anual', 'altres')
        u.exportKhalix('select * from altres.simulacio_visites_exadata_anual', FILE_NAME_ANUAL, force=True)

if __name__ == '__main__':
    if u.IS_MENSUAL:
        u.printTime('inici')
        Activitat()
        u.printTime('fi')
