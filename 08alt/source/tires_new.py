# -*- coding: utf-8 -*-

import sisapUtils as u
import collections as c
import csv,os
from time import strftime
print strftime("%Y-%m-%d %H:%M:%S")

db="altres"

path="./dades_noesb/tir_indicadors.txt"

dm="tir_pacient"
pac="mst_tir_pacient_new"
ecap_pac = "exp_ecap_tir_pacient_new"
ecap_marca_exclosos = "if(maca=1,1,if(institucionalitzat=1,2,if(ates=0,5,0)))"
condicio_ecap_llistat = "llistat=1 and excl=0 and num=0"
condicio_ecap_ind = "ates=1 and institucionalitzat=0 and maca=0 and excl=0"
ecap_ind = "exp_ecap_tir_uba" 
ecap_detall1 = "ecap_tir_uba_detall" 
ecap_detall = "exp_ecap_tir_uba_detall" 
tipus=[['M','up','uba'],['I','upinf','ubainf']] 
uba_in="mst_tir_ubas"

up = "eqa_tir_khalix_up_pre_new"
khalix_pre = "eqa_tir_khalix_uba_ind_new"
khalix_preuba = "eqa_tir_khalix_uba_ind"
khalix = "exp_khalix_tir_up_ind"
khalixuba = "exp_khalix_tir_uba_ind"
condicio_khalix = "excl=0 and (institucionalitzat=1 or (institucionalitzat=0 and maca=0))"
condicio_khalix_uba=""
conceptes=[['NUM','num'],['DEN','den'],['PACTIRES','tires'],['NTIRES','ntires']]
ecap_cataleg_exclosos = [[0,'Pacients que formen part de l indicador',0],[1,'Pacients MACA',2],[2,'Pacients institucionalitzats',3],[3,'Pacients exclosos per edat',4],[4,'Pacients exclosos per motius clinics',5],[5,'Pacients no atesos el darrer any',1],[9,'Pacients que compleixen els criteris de l&#39;indicador',9]]

precat="tir_indicadors"

problemes="nodrizas.eqa_problemes"
assignada="nodrizas.assignada_tot_with_jail"
u11 = "mst_tir_u11"
u11nod = "nodrizas.eqa_u11"
edats_k = "nodrizas.khx_edats5a"


catalegKhx = "exp_khalix_tir_cataleg"
cataleg = "exp_ecap_tir_cataleg"
catalegDetall = "exp_ecap_tir_catalegDetall"
catalegDetallcolumna = "exp_ecap_tir_catalegDetallcolumna"
catalegPare = "exp_ecap_tir_catalegPare"
catalegExclosos = "exp_ecap_tir_catalegExclosos"


agrupadorDM="ps in ('18','24','487')"
dmtipo2="ps=18"
dmtipo1="ps=24"
dmtipog="ps=487"

INDICADORS = {
    "TIR001": {
        "concepte": "D",
        "farmac": set(),
        "ps": 18,
        "n_tires": 2,
        "mcg": set(),
        "bomba":  set()
    },
    "TIR002": {
        "concepte": "AdoNS",
        "farmac": set([22,]),
        "ps": 18,
        "n_tires": 2,
        "mcg": set(),
        "bomba":  set()
    },
    "TIR003": {
        "concepte": "AdoS",
        "farmac": set([414,]),
        "ps": 18,
        "n_tires": 2,
        "mcg": set(),
        "bomba":  set()
    },
    "TIR004": {
        "concepte": "InsL",
        "farmac": set([413,]),
        "ps": 18,
        "n_tires": 2,
        "mcg": set(),
        "bomba":  set()
    },
    "TIR005": {
        "concepte": "InsR",
        "farmac": set([412,1090,1091]),
        "ps": 18,
        "n_tires": 49,
        "mcg": set(),
        "bomba":  set()
    },
    "TIR007": {
        "concepte": "DG",
        "farmac": set(),
        "ps": 487,
        "n_tires": 49,
        "mcg": set(),
        "bomba":  set()
    },
    "TIR008A": {
        "concepte": "DM1A",
        "farmac": set([412,413,1090,1091]),
        "ps": 24,
        "dm_tipus": "",
        "n_tires": 70,
        "mcg": set(['S',]),
        "bomba":  set()
    },
    "TIR008B": {
        "concepte": "DM1A",
        "farmac": set([412,413,1090,1091]),
        "ps": 18,
        "n_tires": "70",
        "mcg": set(['S',]),
        "bomba":  set()
    },
    "TIR009": {
        "concepte": "DM1A",
        "farmac": set([412,413,1090,1091]),
        "ps": 24,
        "n_tires": 70,
        "mcg": set(['N',]),
        "bomba":  set(['S',])
    },
    "TIR010": {
        "concepte": "DM1S",
        "farmac": set([412,413,1090,1091]),
        "ps": 24,
        "n_tires": 49,
        "mcg": set(['N',]),
        "bomba":  set(['N',])
    }
}

agrupatires = [['tires between 0 and 1','T01'],
               ['tires between 1.01 and 2','T12'],
               ['tires between 2.01 and 7','T37'],
               ['tires between 7.01 and 21','T821'],
               ['tires between 21.01 and 50','T2250'],
               ['tires > 50','T50']]

tipus_dm = [['D','Dieta','1'],
            ['AdoNS','ADO no secretagogs','2'],
            ['AdoS','ADO secretagogs','3'],
            ['InsL','Insulina lenta','4'],
            ['InsR','Insulina rapida','5'],
            ['DM1A','Diabetics tipus 1 amb bomba insulina','6'],
            ['DM1B','Diabetics tipus 1 sense bomba insulina','7']]
cat_agrupatires =[['T01','Entre 0 i 1','C1'],
                  ['T12','Entre 1 i 2','C2'],
                  ['T37','Entre 3 i 7','C3'],
                  ['T821','Entre 8 i 21','C4'],
                  ['T2249','Entre 22 i 49','C5'],
                  ['T50','50 o +','C6']]
total_tires="C1+C2+C3+C4+C5+C6"

criteri_edat="edat>14"


class Tires():
    def __init__(self):
        self.get_poblacio()
        self.get_diabetics()
        self.get_indi_info()
        self.get_tires()
        self.get_farmacs()
        self.get_monitoratge_glucosa()
        self.get_exclusions()
        self.get_coverters()
        self.get_indicador()
        self.upload_indicadors()

    def get_poblacio(self):
        print('get_poblacio')
        self.poblacio = {}
        sql = """select id_cip_sec,up,uba,upinf,
                ubainf,maca,
                institucionalitzat,
                {ecap_exclosos},
                edat, sexe, ates
                from {assignada}""".format(ecap_exclosos=ecap_marca_exclosos,
                                                    assignada=assignada)
        print(sql)
        for id, up, uba, upinf, ubainf, maca, institucionalitzat, excl_ecap, edat, sexe, ates in u.getAll(sql, 'nodrizas'):
            self.poblacio[id] = [up, uba, upinf, ubainf, maca, institucionalitzat, excl_ecap, edat, sexe, ates]
    
    def get_diabetics(self):
        print('get_diabetics')
        fets = set()
        """prenem la poblaci� diab�tica, 
            dm1 (24) i dm gestacional (487) 
            han de prevaldre sobre dm2 (18)"""
        self.dm = c.defaultdict(set)
        sql = """select id_cip_sec, ps from nodrizas.eqa_problemes, nodrizas.dextraccio 
                    where ps in (18, 24) or (ps = 487 and
                    (dde >= date_add(data_ext,interval -9 month)))
                    order by ps desc"""
        for id, ps in u.getAll(sql, 'nodrizas'):
            if id not in fets:
                self.dm[ps].add(id)
                fets.add(id)
    
    def get_indi_info(self):
        print('get_indi_info')
        self.indicadors_info = {}
        with open(path, 'rb') as file:
            p=csv.reader(file, delimiter='@', quotechar='|')
            for ti in p:
                ind,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9]
                self.indicadors_info[ind] = [desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor]

    def get_tires(self):
        print('get_tires')
        sql = """select id_cip_sec, tires from nodrizas.eqa_tires"""
        self.tires = {}
        for id, n_tires in u.getAll(sql, 'nodrizas'):
            self.tires[id] = n_tires
    
    def get_farmacs(self):
        print('get_farmacs')
        self.tractaments = c.defaultdict(set)
        self.pacients_amb_farmacs = set()
        farmacs = set()
        for indi in INDICADORS:
            farmac = INDICADORS[indi]['farmac']
            farmacs = farmacs.union(farmac)
        sql = """select id_cip_sec, farmac from nodrizas.eqa_tractaments  
                    where farmac in {}
                    and tancat = 0""".format(tuple(farmacs))
        for id, farmac in u.getAll(sql, 'nodrizas'):
            self.tractaments[farmac].add(id)
            self.pacients_amb_farmacs.add(id)
    
    def get_monitoratge_glucosa(self):
        print('get_monitoratge_glucosa')
        self.mcg = c.defaultdict(set)
        self.bomba = c.defaultdict(set)
        sql = """select id_cip_sec, fit_porta_bomba, fit_mcg from import.tires"""
        for id, bomba, mcg in u.getAll(sql, 'import'):
            self.mcg[mcg].add(id)
            self.bomba[bomba].add(id)
    
    def get_exclusions(self):
        print('get_exclusions')
        self.exclusions = set()
        sql = "select id_cip_sec from altres.excl_tires where val = 1"
        for id, in u.getAll(sql, 'altres'):
            self.exclusions.add(id)
    
    def get_coverters(self):
        print('get_coverters')
        self.converters = {}
        sql = """select id_cip_sec, codi_sector, hash_d from import.u11"""
        for id, sector, hash in u.getAll(sql, 'import'):
            self.converters[id] = [sector, hash]

    def get_indicador(self):
        print('get_indicador')
        self.taula_pacients = []
        self.taula_pacients_ecap = []
        self.taula_uba_khalix = []
        comptador_num = c.Counter()
        comptador_den = c.Counter()
        comptador_tires = c.Counter()
        comptador_tires_global = c.Counter()
        sql = """select edat, khalix from nodrizas.khx_edats5a"""
        khalix_converter = {int(edat): khalix for (edat, khalix) in u.getAll(sql, 'nodrizas')}
        for indicador in INDICADORS:
            farmacs = INDICADORS[indicador]['farmac']
            ps = INDICADORS[indicador]['ps']
            n_tires = INDICADORS[indicador]['n_tires']
            tipodm = INDICADORS[indicador]['concepte']
            dm_tipus = ''
            mcg = INDICADORS[indicador]['mcg']
            bomba = INDICADORS[indicador]['bomba']
            for id_cip_sec in self.poblacio:
                num = 0
                up, uba, upinf, ubainf, maca, institucionalitzat, excl_ecap, edat, sexe, ates = self.poblacio[id_cip_sec]
                compleix_farm = 0
                if not farmacs:
                    if id_cip_sec not in self.pacients_amb_farmacs:
                        compleix_farm = 1
                else:
                    for farm in farmacs:
                        if id_cip_sec in self.tractaments[farm]:
                            compleix_farm = 1
                compleix_mcg = 0
                if not mcg: compleix_mcg = 1
                else:
                    for e in mcg:
                        if id_cip_sec in self.mcg[e]:
                            compleix_mcg = 1
                compleix_bomba = 0
                if not mcg: compleix_bomba = 1
                else:
                    for e in bomba:
                        if id_cip_sec in self.bomba[e]:
                            compleix_bomba = 1
                den = 1 if id_cip_sec in self.dm[ps] and compleix_farm and compleix_mcg and compleix_bomba else 0
                if den:
                    if id_cip_sec in self.tires:
                        tires = self.tires[id_cip_sec]
                        if tires <= n_tires:
                            num = 1
                    gruptires = ''
                    llistat = 1
                    indicador_f = indicador
                    if edat < 15:
                        indicador_f = indicador + 'P'
                    excl = 1 if id in self.exclusions else 0 
                    self.taula_pacients.append((id_cip_sec, indicador_f, tipodm, tires,
                                        gruptires, up, uba, upinf, ubainf, edat, sexe, ates, 
                                        institucionalitzat, maca, num, den, excl, llistat))
                    if llistat == 1 and excl == 0 and num == 0:
                        if id_cip_sec in self.converters:
                            sector, hash = self.converters[id_cip_sec]
                            self.taula_pacients_ecap.append((id_cip_sec, up, uba, upinf, ubainf, 
                                            indicador_f, excl_ecap, hash, sector))
                    edat_khalix = khalix_converter[edat]
                    sexe_khalix = 'HOME' if sexe == 'H' else 'DONA'
                    ins = 'INS' if institucionalitzat else 'NOINS'
                    at = 'AT' if ates else 'ASS'
                    comb = ins + at
                    comptador_num[(up, uba, 'M', edat_khalix, sexe_khalix, comb, indicador)] += num
                    comptador_den[(up, uba, 'M', edat_khalix, sexe_khalix, comb, indicador)] += 1
                    comptador_tires[(up, uba, 'M', edat_khalix, sexe_khalix, comb, indicador)] += 1 if tires > 0 else 0
                    comptador_tires_global[(up, uba, 'M', edat_khalix, sexe_khalix, comb, indicador)] += tires
                    comptador_num[(upinf, ubainf, 'M', edat_khalix, sexe_khalix, comb, indicador)] += num
                    comptador_den[(upinf, ubainf, 'M', edat_khalix, sexe_khalix, comb, indicador)] += 1
                    comptador_tires[(upinf, ubainf, 'M', edat_khalix, sexe_khalix, comb, indicador)] += 1 if tires > 0 else 0
                    comptador_tires_global[(upinf, ubainf, 'M', edat_khalix, sexe_khalix, comb, indicador)] += tires
        for up, uba, tipus, edat_khalix, sexe_khalix, comb, indicador in comptador_den:
            den = comptador_den[(up, uba, tipus, edat_khalix, sexe_khalix, comb, indicador)]
            num = comptador_num[(up, uba, tipus, edat_khalix, sexe_khalix, comb, indicador)] if (up, uba, tipus, edat_khalix, sexe_khalix, comb, indicador) in comptador_num else 0
            tires = comptador_tires[(up, uba, tipus, edat_khalix, sexe_khalix, comb, indicador)] if (up, uba, tipus, edat_khalix, sexe_khalix, comb, indicador) in comptador_tires else 0
            ntires = comptador_tires_global[(up, uba, tipus, edat_khalix, sexe_khalix, comb, indicador)] if (up, uba, tipus, edat_khalix, sexe_khalix, comb, indicador) in comptador_tires_global else 0
            self.taula_uba_khalix.append((up, uba, tipus, edat_khalix, sexe_khalix, comb, indicador, num, den, tires, ntires))

    def upload_indicadors(self):
        print('upload_indicadors')
        cols = """(id_cip_sec double, indicador varchar(10),
                    tipodm varchar(5), tires double,
                    gruptires varchar(20), up varchar(5),
                    uba varchar(7), upinf varchar(5), 
                    ubainf varchar(7), edat double,
                    sexe varchar(1), ates double,
                    institucionalitzat double,
                    maca double, num double,
                    den double, excl double, llistat double)"""
        u.createTable(pac, cols, 'altres', rm=True)
        u.listToTable(self.taula_pacients, pac, 'altres')

        cols = """(id_cip_sec double ,up varchar(5), uba varchar(7),
                    upinf varchar(5), ubainf varchar(7), grup_codi varchar(10),
                    exclos int, hash_d varchar(40), sector varchar(4))"""
        u.createTable(ecap_pac, cols, 'altres', rm=True)
        u.listToTable(self.taula_pacients_ecap, ecap_pac, 'altres')

        cols = """(up varchar(5), uba varchar(5), tipus varchar(1), edat varchar(8),
                    sexe varchar(4), comb varchar(8), indicador varchar(10),num double,
                    den double, tires double, ntires double)"""
        u.createTable(khalix_pre, cols, 'altres', rm=True)
        u.listToTable(self.taula_uba_khalix, khalix_pre, 'altres')


if __name__ == "__main__":
    Tires()

# c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double,tires double, ntires double)" % khalix_pre)
# c.execute("insert into %s select up,edat,sexe,comb,indicador,sum(num) num,sum(den) den,sum(if(tires>0,1,0)) tires,round(sum(tires),2) ntires from %s group by 1,2,3,4,5" % (khalix_pre,up))
# c.execute("drop table if exists %s" % khalix)
# c.execute("create table %s (up varchar(5) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',conc varchar(10) not null default '',n double)" % khalix)
# for r in conceptes:
#     conc,var=r[0],r[1]
#     c.execute("insert into %s select up,edat,sexe,comb,indicador,'%s',%s n from %s where comb like '%%AT%%'" % (khalix,conc,var,khalix_pre))
#     c.execute("insert into %s select up,edat,sexe,replace(comb,'AT','ASS'),indicador,'%s',sum(%s) n from %s group by 1,2,3,4,5" % (khalix,conc,var,khalix_pre))      

# c.execute("drop table if exists %s" % ecap_ind)
# c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',indicador varchar(8) not null default '',numerador int,denominador int,resultat double)" % ecap_ind)
# for r in tipus:
#     tip,up,uba=r[0],r[1],r[2]
#     c.execute("""insert into %s select %s,%s,'%s' tipus,indicador,
#             sum(num) numerador,sum(den) denominador,sum(num)/sum(den) resultat 
#             from %s inner join nodrizas.cat_centres on up=scs_codi 
#             where %s and %s group by 1,2,3,4""" % (ecap_ind,up,uba,tip,pac,condicio_ecap_ind,criteri_edat))
   

# c.execute("drop table if exists %s" % uba_in)
# c.execute("create table %s(up varchar(5) not null default '', uba varchar(7) not null default '',tipus varchar(1) not null default '')" % uba_in)
# c.execute("insert into %s select up,uba,tipus from %s group by up,uba,tipus" % (uba_in,ecap_ind))

# c.execute("alter table %s add unique(up,uba,tipus,indicador)" % ecap_ind)
# with open(path, 'rb') as file:
#    p=csv.reader(file, delimiter='@', quotechar='|')
#    for ti in p:
#       i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9]
#       c.execute("insert ignore into %s select up,uba,tipus,'%s' indicador,0 numerador,0 denominador, 0 resultat from (select up,uba,tipus from %s) a" % (ecap_ind,i,uba_in)) 


# c.execute("drop table if exists %s" % ecap_detall1)
# c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',dm varchar(10) not null default '',tires varchar(10) not null default '',valor double)" % ecap_detall1)

# for r in tipus:
#     tip,up,uba=r[0],r[1],r[2]
#     c.execute("insert into %s select %s,%s,'%s' tipus,tipodm dm,gruptires tires, count(*) valor from %s where %s and %s group by 1,2,3,4,5" % (ecap_detall1,up,uba,tip,pac,condicio_ecap_ind,criteri_edat))

# c.execute("alter table %s add index (up,uba,tipus,dm,tires,valor)" % ecap_detall1)


# c.execute("drop table if exists %s" % ecap_detall)
# c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',dm varchar(10) not null default '',total double)" % ecap_detall)

# for s in tipus_dm:
#    codi,literal,ordre=s[0],s[1],s[2]
#    c.execute("insert into %s select up,uba,tipus,'%s' dm,0 total from %s group by 1,2,3,4" % (ecap_detall,codi,ecap_detall1))

# for s in cat_agrupatires:
#    codi,literal,columna=s[0],s[1],s[2]
#    c.execute("alter table %s add column %s double" % (ecap_detall,columna))
#    c.execute("update %s set %s=0" % (ecap_detall,columna))
#    c.execute("update %s a inner join %s b on a.up=b.up and a.uba=b.uba and a.tipus=b.tipus and a.dm=b.dm set %s=valor where tires='%s'" % (ecap_detall,ecap_detall1,columna,codi))

# c.execute("update %s set total=%s" % (ecap_detall,total_tires)) 

# c.execute("drop table if exists %s" % upuba)
# c.execute("create table %s (id_cip_sec double, up varchar(5) not null default'',uba varchar(7) not null default'',upinf varchar(5) not null default'',ubainf varchar(7) not null default'',edat varchar(8) not null default '',sexe varchar(4) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double, tires double)" % upuba)
# c.execute("insert into %s select id_cip_sec,up,uba,upinf,ubainf,khalix edat,%s sexe,%s comb,indicador,num,den,tires from %s a inner join %s b on a.edat=b.edat where %s" % (upuba,sexe,comb,pac,edats_k,condicio_khalix))

# c.execute("drop table if exists %s" % khalix_preuba)
# c.execute("create table %s (up varchar(5) not null default '',uba varchar(7) not null default '',tipus varchar(1) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',num double,den double,tires double, ntires double)" % khalix_preuba)
# for r in tipus:
#     tip,up,uba=r[0],r[1],r[2]
#     c.execute("insert into %s select %s,%s,'%s' tipus,comb,indicador,sum(num) num,sum(den) den,sum(if(tires>0,1,0)) tires,round(sum(tires),2) ntires from %s group by 1,2,3,4,5" % (khalix_preuba,up,uba,tip,upuba))

# c.execute("drop table if exists %s" % khalixuba)
# c.execute("create table %s (up varchar(5) not null default'',uba varchar(7) not null default '',tipus varchar(1) not null default '',comb varchar(8) not null default '',indicador varchar(10) not null default'',conc varchar(10) not null default '',n double)" % khalixuba)
# for r in conceptes:
#     conc,var=r[0],r[1]
#     c.execute("insert into %s select up,uba,tipus,comb,indicador,'%s',%s n from %s where comb like '%%AT%%'" % (khalixuba,conc,var,khalix_preuba))
#     c.execute("insert into %s select up,uba,tipus,replace(comb,'AT','ASS'),indicador,'%s',sum(%s) n from %s group by 1,2,3,4,5" % (khalixuba,conc,var,khalix_preuba))      

      
# c.execute("drop table if exists %s,%s,%s,%s,%s,%s" % (cataleg,catalegPare,catalegExclosos,catalegKhx,catalegDetall,catalegDetallcolumna))
# c.execute("create table %s (indicador varchar(8),literal varchar(300),ordre int,pare varchar(8),llistat int,invers int,mmin double,mint double,mmax double,toShow int,wiki varchar(250),tipusvalor varchar(5))" % cataleg)
# c.execute("drop table if exists %s" % precat)
# c.execute("create table %s (indicador varchar(8), literal varchar(300),grup varchar(8),grup_desc varchar(300),baixa_ int, dbaixa varchar(80),llistats int,ordre int,grup_ordre int,tipusvalor varchar(5), pantalla varchar(20))" % precat)
# with open(path, 'rb') as file:
#    p=csv.reader(file, delimiter='@', quotechar='|')
#    for ti in p:
#       i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor, pantalla = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9],ti[10]
#       if baixa=="1":
#          ok=1
#       else:
#          c.execute("insert into %s values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (precat,i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor, pantalla))


# c.execute("insert into %s select distinct indicador,literal,ordre,grup pare,llistats llistat,0 invers,0 mmin,0 mint,0 mmax,1 toShow,concat('http://10.80.217.201/sisap-umi/indicador/codi/',indicador) wiki,tipusvalor from %s  where baixa_=0" % (cataleg,precat))
# c.execute("create table %s (pare varchar(8) not null default '',literal varchar(300) not null default '',ordre int, pantalla varchar(20))" % catalegPare)
# c.execute("insert into %s select distinct grup pare,grup_desc literal,grup_ordre ordre, pantalla from %s where baixa_=0" % (catalegPare,precat))
# c.execute("create table %s (indicador varchar(10) not null default'',desc_ind varchar(300) not null default'',grup varchar(10) not null default'', grup_desc varchar(300) not null default'')" % catalegKhx)
# with open(path, 'rb') as file:
#    p=csv.reader(file, delimiter='@', quotechar='|')
#    for ti in p:
#       i,desc,grup,grupdesc,baixa,dbaixa,llistat,ordre,grup_ordre,tipusvalor = ti[0],ti[1],ti[2],ti[3],ti[4],ti[5],ti[6],ti[7],ti[8],ti[9]
#       c.execute("insert into %s Values('%s','%s','%s','%s')" % (catalegKhx,i,desc,grup,grupdesc))     

         
# c.execute("create table %s (codi int,descripcio varchar(150),ordre int)" % catalegExclosos)
# for r in ecap_cataleg_exclosos:
#     codi,desc,ordre=r[0],r[1],r[2]
#     c.execute("insert into %s VALUES (%d,'%s',%d)" % (catalegExclosos,codi,desc,ordre))
    
# c.execute("create table %s (codi varchar(10) not null default'',literal varchar(30) not null default'',ordre int)" % catalegDetall)
# c.execute("create table %s (columna varchar(10) not null default'',literal varchar(30) not null default'')" % catalegDetallcolumna)
# for s in tipus_dm:
#    codi,literal,ordre=s[0],s[1],s[2]
#    c.execute("insert into %s values('%s','%s','%s')" % (catalegDetall,codi,literal,ordre))
# for s in cat_agrupatires:
#    codi,literal,columna=s[0],s[1],s[2]
#    c.execute("insert into %s values('%s','%s')" % (catalegDetallcolumna,columna,literal))

# # No mostrem Ps a khalix ni a ecap
# u.execute("""delete from exp_khalix_tir_uba_ind
#             where indicador not in (select INDICADOR from exp_khalix_tir_cataleg)""", 'altres')
# u.execute("""delete from exp_khalix_tir_up_ind
#             where indicador not in (select INDICADOR from exp_khalix_tir_cataleg)""", 'altres')
# u.execute("""delete from exp_ecap_tir_uba
#             where indicador not in (select INDICADOR from exp_khalix_tir_cataleg)""", 'altres')
# u.execute("""delete from exp_ecap_tir_pacient
#             where grup_codi not in (select INDICADOR from exp_khalix_tir_cataleg)""", 'altres')
