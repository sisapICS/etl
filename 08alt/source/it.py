# coding: iso-8859-1

from sisapUtils import *
import csv,os,sys
from time import strftime

debug = False

db="altres"
nod="nodrizas"
imp="import"

ass="assignada_tot"
baixes="baixes"
dextr="nodrizas.dextraccio"

path0 = os.path.realpath("../")
path = path0.replace("\\","/") + "/08alt/dades_noesb/it_indicadors.txt" 
OutFile= tempFolder + 'it.txt'

criteri_actius="edat between 16 and 65 and relacio='T' and nivell_cobertura='AC'"

conceptes=[['NUM','num'],['DEN','den']]
tkhalix="exp_khalix_it_up_ind_uba"

fa12mesos=int(11)
ara=int(0)


printTime('Inici calcul') 
indicadors={}
with open(path, 'rb') as file:
    p=csv.reader(file, delimiter='@', quotechar='|')
    for ind in p:
        i,desc,icurt = ind[0],ind[1],ind[2]
        indicadors[icurt]={'i':i,'d':desc}

def days_between(d1, d2):
    return abs((d2 - d1).days)

sql="select data_ext,date_add(data_ext,interval -1 year) from {}".format(dextr)
for d,e in getAll(sql,nod):
    dext=d
    fa1any=e
    
actius,it1,it2,it3={},{},{},{}
sql="select id_cip_sec,up,uba from {0} where {1}".format(ass,criteri_actius)
for id,up,uba in getAll(sql,nod):
    id=int(id)
    actius[id]={'up':up,'uba':uba}
    if (up,uba) in it1:
        it1[(up,uba)]['den']+=1
    else:
        try:
            it1[(up,uba)]={'num':0,'den':1}
        except KeyError:
            ok=1
    if (up,uba) in it3:
        it3[(up,uba)]['den']+=1
    else:
        try:
            it3[(up,uba)]={'num':0,'den':1}
        except KeyError:
            ok=1

sql="select id_cip_sec,ilt_data_baixa,ilt_data_alta,\
        datediff(if(ilt_data_alta=0 or ilt_data_alta > data_ext,data_ext,ilt_data_alta),if(ilt_data_baixa <= date_add(data_ext,interval -1 year),date_add(date_add(data_ext,interval +1 day),interval -1 year),ilt_data_baixa))+1 \
        from {0},{1} {2}".format(baixes,dextr,' limit 10' if debug else '')
for id,baixa,alta,dies in getAll(sql,imp):
    id=int(id)
    alta2=alta
    if alta==None:
        alta=dext
        diesa=0
    else:
        diesa=days_between(baixa,alta)+1
    durada=days_between(baixa,alta)+1
    try:
        if actius[id]:
            up=actius[id]['up']
            uba=actius[id]['uba']
            b=monthsBetween(baixa,dext)
            if ara<=b<=fa12mesos:
                if 2<durada<274:
                    if (up,uba) in it1:
                        it1[(up,uba)]['num']+=1
            try:                
                c=monthsBetween(alta2,dext) 
            except AttributeError:
                c=2000
            if ara<=c<=fa12mesos:
                if 2<diesa<274:
                    if (up,uba) in it2:
                        it2[(up,uba)]['den']+=1
                        it2[(up,uba)]['num']+=diesa
                    else:
                        try:
                            it2[(up,uba)]={'num':diesa,'den':1}
                        except KeyError:
                            ok=1
            if baixa<=dext and alta>fa1any:
                if durada>2:
                    if (up,uba) in it3:
                        it3[(up,uba)]['num']+=dies   
    except KeyError:
        continue
        
with open(OutFile,'wb') as file:
    w= csv.writer(file, delimiter='@', quotechar='|')
    for (up,uba),d in it1.items():
        num=d['num']
        den=d['den']
        ind=indicadors['IT1']['i']
        try:
            w.writerow([up,uba,ind,num,den])
        except KeyError:
            continue
    for (up,uba),d in it2.items():
        num=d['num']
        den=d['den']
        ind=indicadors['IT2']['i']
        try:
            w.writerow([up,uba,ind,num,den])
        except KeyError:
            continue
    for (up,uba),d in it3.items():
        num=d['num']
        den=d['den']
        ind=indicadors['IT3']['i']
        try:
            w.writerow([up,uba,ind,num,den])
        except KeyError:
            continue

table="it_uba"
sql= 'drop table if exists {}'.format(table)
execute(sql,db)
sql= "create table {} (up varchar(5) not null default'',uba varchar(5) not null default'',indicador varchar(20) not null default'',num double,den double)".format(table)
execute(sql,db)
loadData(OutFile,table,db)
sql= 'drop table if exists {}'.format(tkhalix)
execute(sql,db)
sql= "create table {} (up varchar(5) not null default'',uba varchar(5) not null default'',indicador varchar(20) not null default'',conc varchar(5) not null default'',n double)".format(tkhalix)
execute(sql,db)
for r in conceptes:
    conc,var=r[0],r[1]
    sql="insert into {0} select up,uba,indicador,'{1}',{2} n from {3}".format(tkhalix,conc,var,table)
    execute(sql,db)


error= []
centres="export.khx_centres"

taula="%s.%s" % (db,tkhalix)
query="select indicador,concat('A','periodo'),ics_codi,conc,'NOCAT','NOIMP','DIM6SET','N',sum(n) from %(taula)s a inner join %(centres)s b on a.up=scs_codi group by ics_codi,indicador,conc" % {'taula': taula,'centres':centres}
file="IT"
error.append(exportKhalix(query,file))

query="select indicador,concat('A','periodo'),concat(ics_codi,'M',uba),conc,'NOCAT','NOIMP','DIM6SET','N',n from %(taula)s a inner join %(centres)s b on a.up=scs_codi" % {'taula': taula,'centres':centres}
file="IT_IND"
error.append(exportKhalix(query,file))


if any(error):
    sys.exit("he hagut d'escriure al directori de rescat")

printTime('fi calcul') 
