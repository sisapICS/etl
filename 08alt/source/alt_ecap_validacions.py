# -*- coding: utf-8 -*-

from sisapUtils import *
import csv,os
from time import strftime

db="pdp"
conn = connect(db)
c=conn.cursor()

c.execute("INSERT INTO altcatalegpare VALUES (2022, 'EQPFP_V', 'Indicadors de prescripció activa validació', 9, 'FARMAVAL')")
c.execute("""INSERT INTO ALTCATALEG
	SELECT DATAANY, CONCAT(indicador, '_V'),
	literal, ORDRE, 'EQPFP_V', llistat, invers,
	mmin, mint, mmax, toshow, wiki, tipusvalor
	FROM ALTCATALEG a
	WHERE indicador
	IN ('IF249P','IF254P', 'IF263P', 'IF301P', 'IF269P', 'IF299P', 'IF292P', 'IF280P')
	and DATAANY = '2022'""")
c.execute("""DELETE FROM ALTLLISTATS WHERE 
            INDICADOR in ('IF249P_V','IF254P_V', 'IF263P_V', 'IF301P_V', 
            'IF269P_V', 'IF299P_V', 'IF292P_V', 'IF280P_V')""")
c.execute("""INSERT INTO ALTLLISTATS
            SELECT UP, UAB, TIPUS, CONCAT(INDICADOR, '_V'), IDPAC, SECTOR, EXCLOS, comentari
            FROM ALTLLISTATS a
            WHERE INDICADOR in ('IF249P','IF254P', 'IF263P', 'IF301P', 'IF269P', 'IF299P', 'IF292P', 'IF280P')
            and ((up = '00195' AND UAB = 'MG004')
            OR (up = '00195' AND UAB = 'MG004')
            OR (UP = '00371'AND UAB = 'UAB06')
            OR (up = '00106' AND UAB = 'UAB-D')
            OR (up = '08208' and UAB = 'MG11'))""")
c.execute("""INSERT INTO ALTINDICADORS
            SELECT DATAANY, DATAMES, UP, UAB, TIPUS,
            CONCAT(INDICADOR, '_V'), NUMERADOR, DENOMINADOR, RESULTAT, NORESOLTS, MMIN, MMAX
            FROM ALTINDICADORS a
            WHERE INDICADOR in ('IF249P','IF254P', 'IF263P', 'IF301P', 'IF269P', 'IF299P', 'IF292P', 'IF280P')
            and ((up = '00195' AND UAB = 'MG004' AND TIPUS = 'M')
            OR (up = '00195' AND UAB = 'MG004' AND TIPUS = 'M')
            OR (UP = '00371'AND UAB = 'UAB06' AND TIPUS = 'M')
            OR (up = '00106' AND UAB = 'UAB-D' AND TIPUS = 'M')
            OR (up = '08208' and UAB = 'MG11' AND TIPUS = 'M'))
            AND DATAANY = 2022 AND DATAMES = 4""")