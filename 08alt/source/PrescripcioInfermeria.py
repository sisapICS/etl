# coding: iso-8859-1

import sisapUtils as u
import collections as c
import datetime as d
from datetime import datetime
from datetime import date
import dateutil
from dateutil.relativedelta import relativedelta
import urllib
import ssl
import os

ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
ctx.set_ciphers('HIGH:!DH:!aNULL')

# Calcular a nivell d'EAP, ASSIR i/o CUAP
CALC_EAP = True
CALC_ASS = False
CALC_CUAP = False

# Nom de les taules enviades a la connexi� 'nodrizas', base de dades 'altres'

tb_br = 'exp_khalix_up_prescripcio_infermeria'
tb_uba = 'exp_khalix_uba_prescripcio_infermeria'
db = 'altres'

# C�rrega de l'arxiu de n�mines, que inclou una ponderaci� de la c�rrega de treball de cada centre en el que treballa un professional

ARXIU_PROFESSIONALS = os.path.join(
    u.tempFolder,
    'plantilla_infermeres_PrescripcioInfermeria.csv'
    )

URL = "http://p400.ecap.intranet.gencat.cat/sisap/csv/capitol1personaltransposat/df54Ed8easdf/lGFH56dkjhlk"
urllib.urlretrieve(url=URL, filename=ARXIU_PROFESSIONALS, context=ctx)


class PrescripcioInfermeria(object):
    """ . """

    def __init__(self):
        """ . """

        print("Inici del la importaci� de dades")
        ts = d.datetime.now()
        self.get_periode()
        self.get_centres()
        self.get_poblacio()
        self.get_professionals()
        self.get_professionals_infermeria_autonomes()
        self.get_tractaments()
        self.get_absorbents_i_aposits()
        print("         -   Temps d'importaci� de les dades: {}".format(d.datetime.now() - ts))
        print("C�lcul dels indicadors")
        ts = d.datetime.now()
        self.resultat_eap = []
        self.resultat_ass = []
        self.ind_eap = c.defaultdict(lambda: c.Counter())
        self.ind_ass = c.defaultdict(lambda: c.Counter())
        self.ind_cuap = c.defaultdict(lambda: c.Counter())
        self.get_PI001_PI003()
        self.get_PI004()
        self.get_PI006_PI007_PI008_PI009()
        self.get_PI010()
        self.get_PI011()
        self.export_br()
        if CALC_EAP:
            self.export_uba()
        print("         -   Temps del c�lcul dels indicadors: {}".format(d.datetime.now() - ts))

    def get_periode(self):
        """ Obtenci� de les dates de refer�ncia per la desc�rrega de dades """

        sql = """
                SELECT
                    data_ext
                FROM
                    dextraccio
              """
        self.dextraccio = u.getOne(sql, "nodrizas")[0]
        # self.dextraccio = date(2022,1,31)
        self.dextraccio_menys_1mes = self.dextraccio - dateutil.relativedelta.relativedelta(months=1) + dateutil.relativedelta.relativedelta(days=1)
        self.dextraccio_menys_1any = self.dextraccio - dateutil.relativedelta.relativedelta(years=1) + dateutil.relativedelta.relativedelta(days=1)
        self.periode = "A{}".format((self.dextraccio - dateutil.relativedelta.relativedelta(days=15)).strftime("%y%m"))
        print("         -   La data d'extracci� �s el {}".format(self.dextraccio))

    def get_centres(self):
        """ Obtenci� dels cat�leg de centres EAP, ASSIR i/o CUAP """
        if CALC_EAP:
            # Diccionari cat�leg de centres EAP i diccionari conversor de codi ics a codi scs
            self.cat_centres_eap, self.br_up = dict(), dict()
            sql_eap = """
                        SELECT
                            sector,
                            scs_codi,
                            ics_codi
                        FROM
                            cat_centres
                    """
            for sector, up, br in u.getAll(sql_eap, 'nodrizas'):
                self.cat_centres_eap[up] = br
                self.br_up[br] = up
            print("         -   Nombre de centres EAP: {}".format(len(self.cat_centres_eap)))

        if CALC_ASS:
            # Diccionari cat�leg de centres ASSIR
            self.cat_centres_ass, self.up_2_up_ass = dict(), dict()
            sql_ass = """
                        SELECT
                            up,
                            br,
                            up_assir,
                            br_assir
                        FROM
                            ass_centres
                    """
            for up, br, up_ass, br_ass in u.getAll(sql_ass, 'nodrizas'):
                self.cat_centres_ass[up] = {"br": br, "up_ass": up_ass, "br_ass": br_ass}
            print("         -   Nombre de centres ASSIR: {}".format(len(self.cat_centres_ass)))

        if CALC_CUAP:
            self.cat_centres_cuap, self.up_2_up_cuap = dict(), dict()    # Diccionari cat�leg de centres PAC/CUAP
            sql_cuap = """
                            SELECT
                                scs_codi,
                                ics_codi
                            FROM
                                urg_centres
                       """
            for up, br in u.getAll(sql_cuap, 'nodrizas'):
                self.cat_centres_cuap[up] = br
            print("         -   Nombre de centres PAC/CUAP: {}".format(len(self.cat_centres_cuap)))

    def get_poblacio(self):
        """ Obtenci� dels cat�legs de poblaci� assignada """
        self.poblacio, self.set_poblacio = c.defaultdict(dict), set()
        sql = """
                SELECT
                    id_cip_sec,
                    up,
                    uba,
                    ubainf
                FROM
                    assignada_tot
              """
        for id_cip_sec, up, uba, ubainf in u.getAll(sql, 'nodrizas'):
            self.poblacio[up][id_cip_sec] = {'uba': uba, 'ubainf': ubainf}
            self.set_poblacio.add(id_cip_sec)
        print("         -   Nombre total de pacients assignats: {}".format(len(self.set_poblacio)))

    def get_professionals(self):
        """
        Obtenci� dels cat�legs de professionals d'EAP, ASSIR i/o CUAP
        get_professionals_1: Obtenci� del NIF dels professionals per estandaritzar-los i poder creuar-los a get_professionals_2
        """
        self.dni_numcol, self.professionals_den_011 = dict(), c.defaultdict(set)
        sql_numcol_dni = """
                            SELECT
                                ide_dni,
                                ide_numcol,
                                codi_sector
                            FROM
                                cat_pritb992
                            WHERE
                                ide_numcol <> ''
                                AND ide_dni <> ''
                         """
        for nif, numcol, sector in u.getAll(sql_numcol_dni, "import"):
            nif = nif.replace(" ", "")
            if len(nif) == 9:
                nif = nif[:8]
            elif len(nif) == 10:
                nif = nif[1:9]
            self.dni_numcol[nif] = numcol[:8]
            self.professionals_den_011[sector].add(numcol)

        """
        get_professionals_2:
            Obtenci� del cat�leg de professionals d'infermer��a
            pels indicadors PI001, PI003 i PI010, que inclouen la ponderaci�
            de la c�rrega de treball de cada centre
            en el que treballa un professional
        """

        self.cat_professionals_eap_001_003_010, self.set_professionals_eap_001_003_010 = c.defaultdict(dict), set()
        # self.cat_professionals_ass_001_003_010, self.set_professionals_ass_001_003_010 = c.defaultdict(dict), set()
        # self.cat_professionals_cuap_001_003_010, self.set_professionals_cuap_001_003_010 = c.defaultdict(dict), set()

        for (nif, _, _, _, _, _, categoria, _, particions,
                br1, br1_ponderacio,
                br2, br2_ponderacio,
                br3, br3_ponderacio,
                br4, br4_ponderacio,
                br5, br5_ponderacio,
                br6, br6_ponderacio,
                br7, br7_ponderacio,
                br8, br8_ponderacio,
                br9, br9_ponderacio,
                br10, br10_ponderacio,
                br11, br11_ponderacio,
                br12, br12_ponderacio,
                br13, br13_ponderacio,
                br14, br14_ponderacio,
                br15, br15_ponderacio,
                br16, br16_ponderacio,
                br17, br17_ponderacio,
                br18, br18_ponderacio,
                br19, br19_ponderacio,
                br20, br20_ponderacio) in u.readCSV(
                    ARXIU_PROFESSIONALS,
                    sep=";",
                    header=True):

            particions = int(float(particions))
            # Transformem el NIF per poder trobar-lo al cat�leg self.dni_numcol i obtenir el n�mero de col�legiat

            nif = nif.replace(" ", "")
            if len(nif) == 9:
                nif = nif[:8]
            elif len(nif) == 10:
                nif = nif[1:9]

            if nif in self.dni_numcol:
                numcol = self.dni_numcol[nif]
                for particio in range(1, particions + 1):
                    br = locals()["br{}".format(particio)]
                    if br in self.br_up:
                        up = self.br_up[br]
                        ponderacio = float(locals()["br{}_ponderacio".format(particio)])
                        # ponderacio = round(float(locals()["br{}_ponderacio".format(particio)]), 2)
                        if categoria in ("AGDINF", "AGDINFPED"):
                            if CALC_EAP and up in self.cat_centres_eap:
                                self.cat_professionals_eap_001_003_010[up][numcol] = {'nif': nif, 'cat': categoria, 'ponderacio': ponderacio}
                                self.set_professionals_eap_001_003_010.add(numcol)
                            # if CALC_CUAP:
                            #     self.cat_professionals_cuap_001_003_010[up][numcol] = {'nif': nif, 'cat': categoria, 'ponderacio': ponderacio}
                            #     self.set_professionals_cuap_001_003_010.add(numcol)
                        # elif categoria == "AGDLLE":
                        #     if CALC_ASS:
                        #         self.cat_professionals_ass_001_003_010[up][numcol] = {'nif': nif, 'cat': categoria, 'ponderacio': ponderacio}
                        #         self.set_professionals_ass_001_003_010.add(numcol)

                        # TODO: Descartem utilitzar cat�leg per CUAP i ASSIR. ASSIR nom�s t� llevadores a tres centres, i de CUAP no �s pot extreure ponderaci�.

        """ get_professionals_3: Obtenci� del cat�leg de professionals d'infermer��a i medicina per la resta d'indicadors a nivell de centre """

        if CALC_EAP:

            self.cat_professionals_eap, self.set_professionals_eap = c.defaultdict(lambda: c.defaultdict(dict)), c.defaultdict(set)

            sql_eap = """
                        SELECT
                            ide_numcol,
                            ide_dni,
                            up,
                            uab
                        FROM
                            cat_professionals
                        where
                            tipus <> 'A'
                    """
            for numcol, nif, up, uba in u.getAll(sql_eap, 'import'):
                numcol = numcol[:8]
                nif = nif[:8]
                if len(numcol) > 0:
                    categoria = 'I' if numcol[0] == '3' else 'M'
                    self.set_professionals_eap[categoria].add(numcol)
                    self.set_professionals_eap['A'].add(numcol)
                    if uba is not '':
                        self.cat_professionals_eap[categoria][up][nif] = uba

            print("Nombre de professionals de medicina EAP: {}".format(
                len(self.set_professionals_eap['M'])))
            print("Nombre de professionals d'infermer��a EAP: {}".format(
                len(self.set_professionals_eap['I'])))
            print("Nombre de professionals totals EAP: {}".format(
                len(self.set_professionals_eap['A'])))

        if CALC_ASS:
            self.cat_professionals_ass, self.set_professionals_ass = c.defaultdict(lambda: c.defaultdict(dict)), c.defaultdict(set)
            sql_ass = """
                        SELECT
                            ide_numcol,
                            up,
                            uab
                        FROM
                            cat_professionals_assir
                    """
            for numcol, up, uba_ass in u.getAll(sql_ass, 'import'):
                numcol = numcol[:8]
                categoria = 'I' if numcol[0] == '3' else 'M'
                self.cat_professionals_ass[categoria][up][numcol] = uba_ass
                self.set_professionals_ass[categoria].add(numcol)
                self.set_professionals_ass['A'].add(numcol)

            print("Nombre de professionals de ginecolog��a ASS: {}".format(
                len(self.set_professionals_ass['M'])))
            print("Nombre de professionals llevadores ASS: {}".format(
                len(self.set_professionals_ass['I'])))
            print("Nombre de professionals totals ASS: {}".format(
                len(self.set_professionals_ass['A'])))

        if CALC_CUAP:
            self.cat_professionals_cuap, self.set_professionals_cuap = c.defaultdict(lambda: c.defaultdict(dict)), c.defaultdict(set)
            sql_cuap = """
                        SELECT
                            visi_col_prov_resp,
                            visi_up
                        FROM
                            visites1
                    """
            for numcol, up in u.getAll(sql_cuap, 'import'):
                if up in self.cat_centres_cuap:
                    numcol = numcol[:8]
                    categoria = 'I' if numcol[0] == '3' else 'M'
                    self.set_professionals_cuap[categoria].add(numcol)
                    self.set_professionals_cuap['A'].add(numcol)
                    self.cat_professionals_cuap[categoria][up][numcol] = uba

            print("         -   Nombre de professionals de medicina CUAP: {}".format(len(self.set_professionals_cuap['M'])))
            print("         -   Nombre de professionals d'infermer��a CUAP: {}".format(len(self.set_professionals_cuap['I'])))
            print("         -   Nombre de professionals totals CUAP: {}".format(len(self.set_professionals_cuap['A'])))

    def get_professionals_infermeria_autonomes(self):
        """ . """

        self.professionals_infermeria_autonomes = dict()

        sql = """
                SELECT
                    usu_num_col,
                    usu_nif,
                    usu_data_alta,
                    usu_data_modif
                FROM
                    import.presc_inf,
                    nodrizas.dextraccio
                WHERE
                    usu_autonom = 'S'
                    AND usu_data_alta <= data_ext
                    AND (usu_data_modif = 0 or usu_data_modif <= data_ext)
              """
        for numcol, nif, data_alta, dat_modificacio in u.getAll(sql, 'import'):
            if numcol:
                numcol = numcol[:8]
                data_activacio = dat_modificacio if dat_modificacio is not None else data_alta
                self.professionals_infermeria_autonomes[numcol] = data_activacio
            if nif:
                if len(nif) == 9:
                    nif = nif[:8]
                elif len(nif) == 10:
                    nif = nif[1:9]
                numcol = self.dni_numcol.get(nif, None)
                if numcol:
                    data_activacio = dat_modificacio if dat_modificacio is not None else data_alta
                    if not numcol in self.professionals_infermeria_autonomes:
                        self.professionals_infermeria_autonomes[numcol] = data_activacio
                    elif data_activacio < self.professionals_infermeria_autonomes[numcol]:
                        self.professionals_infermeria_autonomes[numcol] = data_activacio

        print("         -   Nombre de professionals activats: {}".format(len(self.professionals_infermeria_autonomes)))

    def get_tractaments(self):
        """ . """

        self.tractaments_eap = c.defaultdict(lambda: c.defaultdict(list))
        self.tractaments_eap_001_003_010 = c.defaultdict(lambda: c.defaultdict(list))
        self.tractaments_ass = c.defaultdict(lambda: c.defaultdict(list))
        self.tractaments_cuap = c.defaultdict(lambda: c.defaultdict(list))

        sql = """
                SELECT
                    id_cip_sec,
                    ppfmc_pf_codi,
                    ppfmc_pmc_data_ini,
                    ppfmc_data_fi,
                    ppfmc_pmc_amb_cod_up,
                    ppfmc_pmc_amb_num_col
                FROM
                    tractaments
                WHERE
                    ppfmc_pmc_data_ini BETWEEN '{}' AND '{}'
            """.format(self.dextraccio_menys_1any, self.dextraccio)
        for id_cip_sec, pf, data_ini, data_fi, up, numcol in u.getAll(sql, 'import'):
            numcol = numcol[:8]
            if CALC_EAP:
                if id_cip_sec in self.set_poblacio and up in self.cat_centres_eap:
                    self.tractaments_eap[up][numcol].append((id_cip_sec, pf, data_ini, data_fi))
                    if numcol in self.set_professionals_eap_001_003_010:
                        self.tractaments_eap_001_003_010[up][numcol].append((id_cip_sec, pf, data_ini, data_fi))
            if CALC_ASS:
                if id_cip_sec in self.set_poblacio and up in self.cat_centres_ass:
                    if numcol in self.set_professionals_ass['A']:
                        self.tractaments_ass[up][numcol].append((id_cip_sec, pf, data_ini, data_fi))
            if CALC_CUAP:
                if id_cip_sec in self.set_poblacio and up in self.cat_centres_cuap and numcol in self.set_professionals_cuap['A']:
                    self.tractaments_cuap[up][numcol].append((id_cip_sec, pf, data_ini, data_fi))

        if CALC_EAP:
            print("prescripcions durant l'�ltim any (EAP): {}".format(
                len(self.tractaments_eap)))
        if CALC_ASS:
            print("prescripcions durant l'�ltim any (ASSIR): {}".format(
                len(self.tractaments_ass)))
        if CALC_CUAP:
            print("prescripcions durant l'�ltim any (CUAP): {}".format(
                len(self.tractaments_cuap)))

    def get_absorbents_i_aposits(self):
        """ . """

        self.absorbents, self.aposits = set(), set()

        sql = """
                SELECT
                    pf_codi,
                    pf_gt_codi
                FROM
                    cpftb006
                WHERE
                    pf_gt_codi IN ('01D00', '23C01', '23C04', '23C00', '23C06', '23C05', '23C03', '23C02', '23C')
              """
        for pf, gt in u.getAll(sql, 'redics'):
            if gt in ('23C01', '23C04', '23C00', '23C06', '23C05', '23C03', '23C02', '23C'):
                self.absorbents.add(pf)
            elif gt == '01D00':
                self.aposits.add(pf)

        print("         -   CPFTB006 - Nombre de codis PF (absorbents): {}".format(len(self.absorbents)))
        print("         -   CPFTB006 - Nombre de codis PF (ap�sits): {}".format(len(self.aposits)))

    def get_PI001_PI003(self):
        """
            PI001 - % Infermeres amb prescripci� activada
            PI003 - % Infermeres utilitzen la prescripci� aut�noma
        """

        if CALC_EAP:

            denominador_eap_001, numerador_eap_001, denominador_eap_003, numerador_eap_003 = c.Counter(), c.Counter(), c.Counter(), c.Counter()

            for up in self.cat_professionals_eap_001_003_010:
                br = self.cat_centres_eap[up]
                for numcol in self.cat_professionals_eap_001_003_010[up]:
                    denominador_eap_001[br] += 1
                    if numcol in self.professionals_infermeria_autonomes:
                        data_activacio = self.professionals_infermeria_autonomes[numcol]
                        if data_activacio <= self.dextraccio:
                            # PI001 / PI003
                            numerador_eap_001[br] += 1
                            denominador_eap_003[br] += 1
                            if up in self.tractaments_eap_001_003_010:
                                if numcol in self.tractaments_eap_001_003_010[up]:
                                    for _, _, data_ini, _ in self.tractaments_eap_001_003_010[up][numcol]:
                                        if self.dextraccio_menys_1mes <= data_ini <= self.dextraccio:
                                            numerador_eap_003[br] += 1
                                            break

            # PI001
            for entity, n in denominador_eap_001.items():
                self.resultat_eap.append(('PI001', self.periode, entity, 'DEN', n))
                self.resultat_eap.append(('PI001', self.periode, entity, 'NUM', numerador_eap_001[entity]))
            # PI003
            for entity, n in denominador_eap_003.items():
                self.resultat_eap.append(('PI003', self.periode, entity, 'DEN', n))
                self.resultat_eap.append(('PI003', self.periode, entity, 'NUM', numerador_eap_003[entity]))

        if CALC_ASS:

            denominador_ass_001, numerador_ass_001, denominador_ass_003, numerador_ass_003 = c.Counter(), c.Counter(), c.Counter(), c.Counter()

            for up in self.cat_professionals_ass:
                br = self.cat_centres_ass[up]["br"]
                for numcol in self.cat_professionals_ass['I'][up]:
                    denominador_ass_001[br] += 1
                    if numcol in self.professionals_infermeria_autonomes:
                        data_activacio = self.professionals_infermeria_autonomes[numcol]
                        if data_activacio <= self.dextraccio:
                            # PI001 / PI003
                            numerador_ass_001[br] += 1
                            denominador_ass_003[br] += 1
                            if up in self.tractaments_ass:
                                if numcol in self.tractaments_ass[up]:
                                    for _, _, data_ini, _ in self.tractaments_ass[up][numcol]:
                                        if self.dextraccio_menys_1mes <= data_ini <= self.dextraccio:
                                            numerador_ass_003[br] += 1
                                            break

            # PI001_ASS
            for entity, n in denominador_ass_001.items():
                self.resultat_ass.append(('PI001_ASS', self.periode, entity, 'DEN', n))
                self.resultat_ass.append(('PI001_ASS', self.periode, entity, 'NUM', numerador_ass_001[entity]))
            # PI003_ASS
            for entity, n in denominador_ass_003.items():
                self.resultat_ass.append(('PI003_ASS', self.periode, entity, 'DEN', n))
                self.resultat_ass.append(('PI003_ASS', self.periode, entity, 'NUM', numerador_ass_003[entity]))

        if CALC_CUAP:

            denominador_cuap_001, numerador_cuap_001, denominador_cuap_003, numerador_cuap_003 = c.Counter(), c.Counter(), c.Counter(), c.Counter()

            for up in self.cat_professionals_cuap:
                br = self.cat_centres_cuap[up]
                for numcol in self.cat_professionals_cuap['I'][up]:
                    denominador_cuap_001[br] += 1
                    if numcol in self.professionals_infermeria_autonomes:
                        data_activacio = self.professionals_infermeria_autonomes[numcol]
                        if data_activacio <= self.dextraccio:
                            # PI001 / PI003
                            numerador_cuap_001[br] += 1
                            denominador_cuap_003[br] += 1
                            if up in self.tractaments_cuap:
                                if numcol in self.tractaments_cuap[up]:
                                    for _, _, data_ini, _ in self.tractaments_cuap[up][numcol]:
                                        if self.dextraccio_menys_1mes <= data_ini <= self.dextraccio:
                                            numerador_cuap_003[br] += 1
                                            break
            # PI001_CUAP
            for entity, n in denominador_cuap_001.items():
                self.resultat_cuap.append(('PI001_CUAP', self.periode, entity, 'DEN', n))
                self.resultat_cuap.append(('PI001_CUAP', self.periode, entity, 'NUM', numerador_cuap_001[entity]))
            # PI003_CUAP
            for entity, n in denominador_cuap_003.items():
                self.resultat_cuap.append(('PI003_CUAP', self.periode, entity, 'DEN', n))
                self.resultat_cuap.append(('PI003_CUAP', self.periode, entity, 'NUM', numerador_cuap_003[entity]))

        print("         -   Executat: get_PI001_PI003()")

    def get_PI004(self):
        """ PI004 - Taxa de prescripcions infermeres per poblaci� estandarditzada """
        if CALC_EAP:
            denominador_eap_004, numerador_eap_004, denominador_eap_004_12m, numerador_eap_004_12m = c.Counter(), c.Counter(), c.Counter(), c.Counter()
            for up in self.poblacio:
                if up in self.cat_centres_eap:
                    br = self.cat_centres_eap[up]
                    for id_cip_sec in self.poblacio[up]:
                        ubainf = self.poblacio[up][id_cip_sec]['ubainf']
                        denominador_eap_004[br] += 1
                        denominador_eap_004[br + "I" + ubainf] += 1
                        denominador_eap_004_12m[br] += 1
                        denominador_eap_004_12m[br + "I" + ubainf] += 1
                    for numcol in self.tractaments_eap[up]:
                        if numcol in self.set_professionals_eap['I']:
                            for id_cip_sec, _, data_ini, _ in self.tractaments_eap[up][numcol]:
                                if id_cip_sec in self.poblacio[up]:
                                    ubainf = self.poblacio[up][id_cip_sec]['ubainf']
                                    numerador_eap_004_12m[br] += 1
                                    numerador_eap_004_12m[br + "I" + ubainf] += 1
                                    if self.dextraccio_menys_1mes <= data_ini <= self.dextraccio:
                                        numerador_eap_004[br] += 1
                                        numerador_eap_004[br + "I" + ubainf] += 1

            # PI004
            for entity, n in denominador_eap_004.items():
                self.resultat_eap.append(('PI004', self.periode, entity, 'DEN', n))
                self.resultat_eap.append(('PI004', self.periode, entity, 'NUM', numerador_eap_004[entity]))
            for entity, n in denominador_eap_004_12m.items():
                self.resultat_eap.append(('PI004_12m', self.periode, entity, 'DEN', n))
                self.resultat_eap.append(('PI004_12m', self.periode, entity, 'NUM', numerador_eap_004_12m[entity]))

            print("         -   Executat: get_PI004()")

    def get_PI006_PI007_PI008_PI009(self):
        """
            PI006 - % Nous absorbents INF darrera setmana / mes
            PI007 - % Absorbents INF en plans med. actius
        """

        if CALC_EAP:

            denominador_eap_006, numerador_eap_006, denominador_eap_007, numerador_eap_007 = c.Counter(), c.Counter(), c.Counter(), c.Counter()
            denominador_eap_008, numerador_eap_008, denominador_eap_009, numerador_eap_009 = c.Counter(), c.Counter(), c.Counter(), c.Counter()

            for up in self.tractaments_eap:
                br = self.cat_centres_eap[up]
                for numcol in self.tractaments_eap[up]:
                    if numcol in self.set_professionals_eap['A']:
                        for id_cip_sec, pf, data_ini, data_fi in self.tractaments_eap[up][numcol]:
                            if id_cip_sec in self.poblacio[up]:
                                # Absorbents
                                if pf in self.absorbents:
                                    ubainf = self.poblacio[up][id_cip_sec]['ubainf']
                                    # PI006
                                    if self.dextraccio_menys_1mes <= data_ini <= self.dextraccio:
                                        denominador_eap_006[br] += 1
                                        denominador_eap_006[br + "I" + ubainf] += 1
                                        if numcol in self.set_professionals_eap['I']:
                                            numerador_eap_006[br] += 1
                                            numerador_eap_006[br + "I" + ubainf] += 1
                                    # PI007
                                    if data_ini <= self.dextraccio < data_fi:
                                        denominador_eap_007[br] += 1
                                        denominador_eap_007[br + "I" + ubainf] += 1
                                        if numcol in self.set_professionals_eap['I']:
                                            numerador_eap_007[br] += 1
                                            numerador_eap_007[br + "I" + ubainf] += 1
                                # Aposits
                                if pf in self.aposits:
                                    ubainf = self.poblacio[up][id_cip_sec]['ubainf']
                                    # PI008
                                    if self.dextraccio_menys_1mes <= data_ini <= self.dextraccio:
                                        denominador_eap_008[br] += 1
                                        denominador_eap_008[br + "I" + ubainf] += 1
                                        if numcol in self.set_professionals_eap['I']:
                                            numerador_eap_008[br] += 1
                                            numerador_eap_008[br + "I" + ubainf] += 1
                                    # PI009
                                    if data_ini <= self.dextraccio < data_fi:
                                        denominador_eap_009[br] += 1
                                        denominador_eap_009[br + "I" + ubainf] += 1
                                        if numcol in self.set_professionals_eap['I']:
                                            numerador_eap_009[br] += 1
                                            numerador_eap_009[br + "I" + ubainf] += 1
            
            # PI006
            for entity, n in denominador_eap_006.items():
                self.resultat_eap.append(('PI006', self.periode, entity, 'DEN', n))
                self.resultat_eap.append(('PI006', self.periode, entity, 'NUM', numerador_eap_006[entity]))
            # PI007
            for entity, n in denominador_eap_007.items():
                self.resultat_eap.append(('PI007', self.periode, entity, 'DEN', n))
                self.resultat_eap.append(('PI007', self.periode, entity, 'NUM', numerador_eap_007[entity]))
            # PI008
            for entity, n in denominador_eap_008.items():
                self.resultat_eap.append(('PI008', self.periode, entity, 'DEN', n))
                self.resultat_eap.append(('PI008', self.periode, entity, 'NUM', numerador_eap_008[entity]))
            # PI009
            for entity, n in denominador_eap_009.items():
                self.resultat_eap.append(('PI009', self.periode, entity, 'DEN', n))
                self.resultat_eap.append(('PI009', self.periode, entity, 'NUM', numerador_eap_009[entity]))

        print("         -   Executat: get_PI006_PI007_PI008_PI009()")


    def get_PI010(self):
        """
            PI010 - Taxa prescripcions per infermera activada
        """

        if CALC_EAP:

            denominador_eap_010, numerador_eap_010 = c.Counter(), c.Counter()

            for up in self.cat_professionals_eap_001_003_010:
                br = self.cat_centres_eap[up]
                for numcol in self.cat_professionals_eap_001_003_010[up]:
                    nif = self.cat_professionals_eap_001_003_010[up][numcol]["nif"]
                    if numcol in self.professionals_infermeria_autonomes:
                        data_activacio = self.professionals_infermeria_autonomes[numcol]
                        if data_activacio <= self.dextraccio:
                            denominador_eap_010[br] += 1
                            if nif in self.cat_professionals_eap['I'][up]:
                                ubainf_prof = self.cat_professionals_eap['I'][up][nif]
                                if ubainf_prof:
                                    denominador_eap_010[br + "I" + ubainf_prof] += 1
                            if up in self.tractaments_eap:
                                if numcol in self.tractaments_eap[up]:
                                    for id_cip_sec, _, data_ini, _ in self.tractaments_eap[up][numcol]:
                                        if id_cip_sec in self.poblacio[up]:
                                            if self.dextraccio_menys_1mes <= data_ini <= self.dextraccio:
                                                ubainf_pacient = self.poblacio[up][id_cip_sec]['ubainf']
                                                numerador_eap_010[br] += 1
                                                numerador_eap_010[br + "I" + ubainf_pacient] += 1
            for entity, n in denominador_eap_010.items():
                self.resultat_eap.append(('PI010', self.periode, entity, 'DEN', n))
                self.resultat_eap.append(('PI010', self.periode, entity, 'NUM', numerador_eap_010[entity]))

        print("         -   Executat: get_PI010()")


    def get_PI011(self):
        """
            PI011 - % prescripcions fetes per les infermeres del total de prescripcions
        """

        if CALC_EAP:

            denominador_eap_011, numerador_eap_011, denominador_eap_011_12m, numerador_eap_011_12m = c.Counter(), c.Counter(), c.Counter(), c.Counter()
            
            for up in self.tractaments_eap:
                br = self.cat_centres_eap[up]
                for numcol in self.tractaments_eap[up]:
                    for id_cip_sec, _, data_ini, _ in self.tractaments_eap[up][numcol]:
                        if id_cip_sec in self.poblacio[up]:
                            ubainf = self.poblacio[up][id_cip_sec]['ubainf']
                            denominador_eap_011_12m[br] += 1
                            denominador_eap_011_12m[br + "I" + ubainf] += 1
                            if self.dextraccio_menys_1mes <= data_ini:
                                denominador_eap_011[br] += 1
                                denominador_eap_011[br + "I" + ubainf] += 1
                            if numcol in self.set_professionals_eap["I"]:
                                numerador_eap_011_12m[br] += 1
                                numerador_eap_011_12m[br + "I" + ubainf] += 1
                                if self.dextraccio_menys_1mes <= data_ini:
                                    numerador_eap_011[br] += 1
                                    numerador_eap_011[br + "I" + ubainf] += 1
                                
            for entity, n in denominador_eap_011.items():
                self.resultat_eap.append(('PI011', self.periode, entity, 'DEN', n))
                self.resultat_eap.append(('PI011', self.periode, entity, 'NUM', numerador_eap_011[entity]))
            for entity, n in denominador_eap_011_12m.items():
                self.resultat_eap.append(('PI011_12m', self.periode, entity, 'DEN', n))
                self.resultat_eap.append(('PI011_12m', self.periode, entity, 'NUM', numerador_eap_011_12m[entity]))

        if CALC_ASS:
            denominador_ass_011 = c.Counter()
            numerador_ass_011 = c.Counter()
            denominador_ass_011_12m = c.Counter()
            numerador_ass_011_12m = c.Counter()
            for up in self.cat_professionals_ass['I']:
                if up in self.cat_centres_ass:
                    br = self.cat_centres_ass[up]["br"]
                    for numcol in self.cat_professionals_ass['I'][up]:
                        if numcol in self.set_professionals_ass['I']:
                            denominador_ass_011[br] += 1
                            for id_cip_sec, _, data_ini, _ in self.tractaments_ass[up][numcol]:
                                if id_cip_sec in self.set_poblacio:
                                    ubainf = self.poblacio[up][id_cip_sec]['ubainf']
                                    numerador_ass_011_12m[br] += 1
                                    numerador_ass_011_12m[br + "I" + ubainf] += 1
                                    denominador_ass_011_12m[br] += 1
                                    denominador_ass_011_12m[br + "I" + ubainf] += 1
                                    if self.dextraccio_menys_1mes <= data_ini:
                                        numerador_ass_011[br] += 1
                                        numerador_ass_011[br + "I" + ubainf] += 1
                                        denominador_ass_011[br] += 1
                                        denominador_ass_011[br + "I" + ubainf] += 1
            for up in self.cat_professionals_ass['M']:
                if up in self.cat_centres_ass:
                    br = self.cat_centres_ass[up]["br"]
                    for numcol in self.cat_professionals_ass['M'][up]:
                        if numcol in self.set_professionals_ass['M']:
                            for id_cip_sec, _, data_ini, _ in self.tractaments_ass[up][numcol]:
                                if id_cip_sec in self.set_poblacio:
                                    ubainf = self.poblacio[up][id_cip_sec]['ubainf']
                                    denominador_ass_011_12m[br] += 1
                                    denominador_ass_011_12m[br + "M" + ubainf] += 1
                                    if self.dextraccio_menys_1mes <= data_ini:
                                        denominador_ass_011[br] += 1
                                        denominador_ass_011[br + "M" + ubainf] += 1
            for entity, n in denominador_ass_011.items():
                self.resultat_ass.append(('PI011_ASS', self.periode, entity, 'DEN', n))
                self.resultat_ass.append(('PI011_ASS', self.periode, entity, 'NUM', numerador_ass_011[entity]))
            for entity, n in denominador_ass_011_12m.items():
                self.resultat_ass.append(('PI011_ASS_12m', self.periode, entity, 'DEN', n))
                self.resultat_ass.append(('PI011_ASS_12m', self.periode, entity, 'NUM', numerador_ass_011_12m[entity]))
        print("         -   Executat: get_PI011()")


    def export_br(self):
        """."""
        u.createTable(tb_br, '(indicador varchar(13), periode varchar(6), br varchar(5), analisis varchar(10), n double)', db, rm=True)
        if CALC_EAP:
            u.listToTable([row for row in self.resultat_eap if len(row[2]) == 5], tb_br, db)
        if CALC_ASS:
            u.listToTable([row for row in self.resultat_ass if len(row[2]) == 5], tb_br, db)
        sql = "select indicador, periode, br, analisis, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', n from {}.{}".format(db, tb_br)
        file = 'PRESINF'
        u.exportKhalix(sql, file)


    def export_uba(self):
        """."""
        u.createTable(tb_uba, '(indicador varchar(13), periode varchar(6), ent varchar(11), analisis varchar(10), n double)', db, rm=True)
        u.listToTable([row for row in self.resultat_eap if len(row[2]) > 5], tb_uba, db)
        sql = "select indicador, periode, ent, analisis, 'NOCAT', 'NOIMP', 'DIM6SET', 'N', n from {}.{}".format(db, tb_uba)
        file = 'PRESINF_UBA'
        u.exportKhalix(sql, file)


if (CALC_EAP or CALC_ASS or CALC_CUAP):
    PrescripcioInfermeria()