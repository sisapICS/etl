#  coding: latin1

import sisapUtils as u
import collections as c
import datetime

sql = """
        SELECT
            data_ext,
            date_add(date_add(data_ext, INTERVAL -3 MONTH), INTERVAL + 1 DAY),
            date_add(date_add(data_ext, INTERVAL -1 YEAR), INTERVAL + 1 DAY),
            date_add(date_add(data_ext, INTERVAL -15 MONTH), INTERVAL + 1 DAY),
            date_add(date_add(data_ext, INTERVAL -2 YEAR), INTERVAL + 1 DAY)
        FROM
            dextraccio
        """
dext, dext_menys3mesos, dext_menys1any, dext_menys15mesos, dext_menys2anys = u.getOne(sql, "test")

tb = "exp_khalix_csma"
tb_pat = "exp_khalix_csma_pat"
db = "altres"
file = "CSMA_{}".format(dext.month)


class CSMA(object):
    """ . """

    def __init__(self):
        """ . """

        self.get_centres();                                 print("self.get_centres()")
        self.get_assignada();                               print("self.get_assignada()")
        self.get_professionals_csma();                      print("self.get_professionals_csma()")
        self.get_visites_de_consulta();                     print("self.get_visites_de_consulta()")
        self.get_problemes();                               print("self.get_problemes()")
        self.get_indicadors();                              print("self.get_indicadors()")
        self.export_klx();                                  print("self.export_klx()")

    def get_centres(self):
        """."""

        sql = """
                SELECT
                    scs_codi,
                    ics_codi
                FROM
                    cat_centres
              """
        self.centres = {up: br for (up, br) in u.getAll(sql, "nodrizas")}


    def get_assignada(self):
        """."""
        
        self.poblacio = dict()
        self.poblacio_up = c.Counter()
        self.poblacio_detallada = list()

        sql_1 = """
                    SELECT
                        id_cip_sec,
                        up,
                        edat,
                        sexe,
                        institucionalitzat,
                        ates
                    FROM
                        assignada_tot
                """
        for id_cip_sec, up, edat, sexe, institucionalitzat, ates in u.getAll(sql_1, "nodrizas"):
            up = UP_MODS.get(up, up)
            edat_grup = u.ageConverter(edat)
            sexe = u.sexConverter(sexe)
            poblatip = ["INSASS" if institucionalitzat else "NOINSASS"]
            if ates:
                poblatip.append("INSAT" if institucionalitzat else "NOINSAT")
            if up in self.centres:
                self.poblacio[id_cip_sec] = {"up": up, "edat": edat, "edat_grup": edat_grup, "sexe": sexe, "poblatip": poblatip, "ates": ates}

        sql_2 = """
                    SELECT
                        up,
                        sum(n)
                    FROM
                        exp_khalix_up_pob
                    WHERE
                        comb IN ('INSAT', 'NOINSAT')
                    GROUP BY up	
                """
        for up, n in u.getAll(sql_2, "eqa_ind"):
            up = UP_MODS.get(up, up)
            if up in self.centres:
                self.poblacio_up[up] += n
        for up, n in u.getAll(sql_2, "pedia"):
            up = UP_MODS.get(up, up)
            if up in self.centres:
                self.poblacio_up[up] += n

        sql_3 = """
                    SELECT
                        up,
                        edat,
                        sexe,
                        comb AS poblatip,
                        n
                    FROM
                        exp_khalix_up_pob
                """
        for up, edat, sexe, poblatip, n in u.getAll(sql_3, "eqa_ind"):
            up = UP_MODS.get(up, up)
            if up in self.centres:
                self.poblacio_detallada.append((up, edat, sexe, poblatip, n))
        for up, edat, sexe, poblatip, n in u.getAll(sql_3, "pedia"):
            up = UP_MODS.get(up, up)
            if up in self.centres:
                self.poblacio_detallada.append((up, edat, sexe, poblatip, n))


    def get_visites_de_consulta(self):
        """."""

        self.visites_de_consulta_ultim_any = c.defaultdict(c.Counter)

        sql = """
                SELECT
                    id_cip_sec
                FROM
                    visites2
                WHERE
                    visi_situacio_visita = 'R'
                    AND visi_data_visita BETWEEN '{}' AND '{}'
              """.format(dext_menys12mesos, dext)
        for id_cip_sec,  in u.getAll(sql, "import"):
            if id_cip_sec in self.poblacio:
                if dext_menys1any <= data_visita:
                    up = self.poblacio[id_cip_sec]["up"]
                    self.visitats_ultim_any.add(id_cip_sec)
                    self.visites_de_consulta_ultim_any["all"][up].add(id_cip_sec)


    def get_problemes(self):
        """."""

        self.DM2 = c.defaultdict(set)
        self.desnutricio_malnutricio_ultims12mesos = c.defaultdict(set)
        self.desnutricio_malnutricio_ultims15a3mesos = c.defaultdict(set)
        self.desnutricio_malnutricio_dates = c.defaultdict(lambda: c.defaultdict(dict))
        self.disfagia_ultims12mesos = c.defaultdict(set)
        self.disfagia_ultims15a3mesos = c.defaultdict(set)
        self.disfagia_dates = c.defaultdict(lambda: c.defaultdict(dict))
        self.celiaquia = c.defaultdict(set)
        self.malaltia_inflamatoria_intestinal = c.defaultdict(set)
        self.malaltia_renal_cronica = c.defaultdict(set)
        self.intolerancia_alergia_alimentaria = c.defaultdict(set)
        self.CI_o_AVC_adults = c.defaultdict(set)
        self.CI_o_AVC_adults_mes3mesos = c.defaultdict(set)

        codis_ps = tuple()
        codis_ps_agrupadors = dict()
        sql = """
                SELECT
                    agrupador,
                    criteri_codi 
                FROM
                    eqa_criteris
                WHERE
                    agrupador IN (1, 18, 53, 169, 212, 646, 677, 941, 943, 944, 945)
              """
        for agrupador, codi_ps in u.getAll(sql, "nodrizas"):
            codis_ps += (codi_ps,)
            codis_ps_agrupadors[codi_ps] = agrupador

        sql = """
                SELECT
                    id_cip_sec,
                    pr_cod_ps,
                    pr_dde,
                    CASE WHEN pr_dba <> '0000-00-00' THEN pr_dba
                         ELSE data_ext
                    END AS pr_dba_p
                FROM
                    {},
                    test.dextraccio
                WHERE 
                    pr_cod_ps IN {}
              """
        jobs = [sql.format(table, codis_ps) for table in u.getSubTables("problemes") if table[-6:] != '_s6951']
        # for problemes in u.multiprocess(sub_get_problemes, jobs, 4):
        problemes = sub_get_problemes(sql.format("problemes_s6520", codis_ps))
        if 1:
            for id_cip_sec in problemes:
                if id_cip_sec in self.poblacio:
                    up = self.poblacio[id_cip_sec]["up"]
                    for codi_ps in problemes[id_cip_sec]:
                        agrupador = codis_ps_agrupadors[codi_ps]
                        for (data_ps, data_fi_ps) in problemes[id_cip_sec][codi_ps]:
                            # Condicions CI o AVC
                            if agrupador in (1, 212):
                                self.CI_o_AVC_adults[up].add(id_cip_sec)
                                if data_ps < dext_menys3mesos:
                                    self.CI_o_AVC_adults_mes3mesos[up].add(id_cip_sec)
                            elif agrupador == 18:
                                self.DM2[up].add(id_cip_sec)
                            elif agrupador in (53, 646, 677):
                                self.malaltia_renal_cronica[up].add(id_cip_sec)   
                            elif agrupador == 169:
                                self.malaltia_inflamatoria_intestinal[up].add(id_cip_sec)            
                            elif agrupador == 941:
                                if dext_menys1any <= data_fi_ps:
                                    self.disfagia_ultims12mesos[up].add(id_cip_sec)
                                self.disfagia_dates[up][id_cip_sec][data_ps] = data_fi_ps
                            elif agrupador == 943:
                                if dext_menys1any <= data_fi_ps:
                                    self.desnutricio_malnutricio_ultims12mesos[up].add(id_cip_sec)
                                if dext_menys15mesos <= data_ps <= dext_menys3mesos:
                                    self.desnutricio_malnutricio_ultims15a3mesos[up].add(id_cip_sec)
                                self.desnutricio_malnutricio_dates[up][id_cip_sec][data_ps] = data_fi_ps
                            elif agrupador == 944:
                                self.celiaquia[up].add(id_cip_sec)
                            elif agrupador == 945:
                                self.intolerancia_alergia_alimentaria[up].add(id_cip_sec)
 

    def get_indicadors(self):
        """."""

        self.resultat = c.Counter()
        self.resultat_pat = c.Counter()
        self.resultat_pob = c.Counter()
        self.resultat_res = c.Counter()
        resultat_cero = c.Counter()
        resultat_cero_pat = c.Counter()
        resultat_cero_pob = c.Counter()
        resultat_cero_res = c.Counter()

        # CSMA01, CSMA03, CSMA04, CSMA05, CSMA06, CSMA07

        for grup_id in self.grups:
            up = self.grups[grup_id]["up"]
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            n_assistents = self.grups[grup_id]["n_assistents"]
            hores = self.grups[grup_id]["hores"]
            act_grupal_i_o_comunitaria = self.grups[grup_id]["act_grupal_i_o_comunitaria"]
            activitat_tipus = self.grups[grup_id]["activitat_tipus"]
            activitat_tematica = self.grups[grup_id]["activitat_tematica"]

            self.resultat[(br, "CSMA01", "NUM")] += 1
            self.resultat[(br, "CSMA03", "NUM")] += hores
            self.resultat[(br, "CSMA05T", "DEN")] += 1
            self.resultat[(br, "CSMA07T", "DEN")] += hores

            for key_activitat_tipus in keys_activitat_tipus.values():
                self.resultat[(br, "CSMA04{}".format(key_activitat_tipus), "DEN")] += 1
            for key_activitat_tematica in keys_activitat_tematica.values():
                self.resultat[(br, "CSMA05{}".format(key_activitat_tematica), "DEN")] += 1
            for key_activitat_tipus in keys_activitat_tipus.values():
                self.resultat[(br, "CSMA06{}".format(key_activitat_tipus), "DEN")] += hores
            for key_activitat_tematica in keys_activitat_tematica.values():
                self.resultat[(br, "CSMA07{}".format(key_activitat_tematica), "DEN")] += hores

            if act_grupal_i_o_comunitaria in keys_act_grupal_i_o_comunitaria:
                self.resultat[(br, "CSMA01{}".format(keys_act_grupal_i_o_comunitaria[act_grupal_i_o_comunitaria]), "NUM")] += 1

            if activitat_tipus in keys_activitat_tipus:
                self.resultat[(br, "CSMA04{}".format(keys_activitat_tipus.get(activitat_tipus, "E")), "NUM")] += 1
                self.resultat[(br, "CSMA06{}".format(keys_activitat_tipus.get(activitat_tipus, "E")), "NUM")] += hores

            if activitat_tematica in keys_activitat_tematica:
                self.resultat[(br, "CSMA05{}".format(keys_activitat_tematica.get(activitat_tematica, "G")), "NUM")] += 1
                if keys_activitat_tematica[activitat_tematica] in ("C", "O", "P", "Q", "R", "S"):
                    self.resultat[(br, "CSMA05T", "NUM")] += 1                
                self.resultat[(br, "CSMA07{}".format(keys_activitat_tematica.get(activitat_tematica, "G")), "NUM")] += hores
                if keys_activitat_tematica[activitat_tematica] in ("C", "O", "P", "Q", "R", "S"):
                    self.resultat[(br, "CSMA07T", "NUM")] += hores                

        # CSMA01, CSMA03

        for up in self.poblacio_up:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for indicador in indicadors_poblacio_atesa_assignada:
                self.resultat[(br, indicador, "DEN")] = self.poblacio_up[up]

        # NUT11

        for up in self.visites_de_consulta_ultim_any["all"]:
            up = UP_MODS.get(up, up)
            if up in self.centres:
                br = self.centres[up]
                self.resultat[(br, "NUT11", "NUM")] += self.visites_de_consulta_ultim_any["all"][up]
                self.resultat[(br, "NUT11", "DEN")] = 0
                self.resultat[(br, "NUT11A", "DEN")] += self.visites_de_consulta_ultim_any["all"][up]
                self.resultat[(br, "NUT11B", "DEN")] += self.visites_de_consulta_ultim_any["all"][up]
                self.resultat[(br, "NUT11C", "DEN")] += self.visites_de_consulta_ultim_any["all"][up]
                self.resultat[(br, "NUT11D", "DEN")] += self.visites_de_consulta_ultim_any["all"][up]
                if up in self.visites_de_consulta_ultim_any["A"]:
                    self.resultat[(br, "NUT11A", "NUM")] += self.visites_de_consulta_ultim_any["A"][up]
                if up in self.visites_de_consulta_ultim_any["B"]:
                    self.resultat[(br, "NUT11B", "NUM")] += self.visites_de_consulta_ultim_any["B"][up]
                if up in self.visites_de_consulta_ultim_any["C"]:
                    self.resultat[(br, "NUT11C", "NUM")] += self.visites_de_consulta_ultim_any["C"][up]
                if up in self.visites_de_consulta_ultim_any["D"]:
                    self.resultat[(br, "NUT11D", "NUM")] += self.visites_de_consulta_ultim_any["D"][up]

        poblacio_descartada_den_nutpat13 = c.defaultdict(set)

        # NUTPAT01

        for up in self.desnutricio_malnutricio_ultims12mesos:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.desnutricio_malnutricio_ultims12mesos[up]:
                if id_cip_sec in self.MNA_inferior_igual_23_5_ultims12mesos_adults[up]:
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    edat_grup = self.poblacio[id_cip_sec]["edat_grup"]    
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]
                    poblacio_descartada_den_nutpat13[up].add(id_cip_sec)
                    for poblatip in poblatips:
                        self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT01", "DEN")] += 1
                        if id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                            self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT01", "NUM")] += 1

        # NUTPAT02
        
        for up in self.IMCs_sup35_ultims12mesos_adults:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.IMCs_sup35_ultims12mesos_adults[up]:
                sexe = self.poblacio[id_cip_sec]["sexe"]
                edat_grup = self.poblacio[id_cip_sec]["edat_grup"]    
                poblatips = self.poblacio[id_cip_sec]["poblatip"]
                poblacio_descartada_den_nutpat13[up].add(id_cip_sec)
                for poblatip in poblatips:
                    self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT02", "DEN")] += 1
                    if id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                        self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT02", "NUM")] += 1                

        # NUTPAT04

        for up in self.pes_sup95_ultims24mesos_pedia:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.pes_sup95_ultims24mesos_pedia[up]:
                sexe = self.poblacio[id_cip_sec]["sexe"]
                edat_grup = self.poblacio[id_cip_sec]["edat_grup"]    
                poblatips = self.poblacio[id_cip_sec]["poblatip"]                        
                poblacio_descartada_den_nutpat13[up].add(id_cip_sec)
                for poblatip in poblatips:
                    self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT04", "DEN")] += 1
                    if id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                        self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT04", "NUM")] += 1      

        # NUTPAT05

        for up in self.disfagia_ultims12mesos:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.disfagia_ultims12mesos[up]:
                if id_cip_sec in self.cribatge_disfagia_adults[up]:
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    edat_grup = self.poblacio[id_cip_sec]["edat_grup"]    
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]                             
                    poblacio_descartada_den_nutpat13[up].add(id_cip_sec)
                    for poblatip in poblatips:
                        self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT05", "DEN")] += 1
                        if id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                            self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT05", "NUM")] += 1  

        # NUTPAT06

        for up in self.celiaquia:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.celiaquia[up]:
                sexe = self.poblacio[id_cip_sec]["sexe"]
                edat_grup = self.poblacio[id_cip_sec]["edat_grup"]    
                poblatips = self.poblacio[id_cip_sec]["poblatip"]                        
                poblacio_descartada_den_nutpat13[up].add(id_cip_sec)
                for poblatip in poblatips:
                    self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT06", "DEN")] += 1
                    if id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                        self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT06", "NUM")] += 1                                                    

        # NUTPAT07

        for up in self.malaltia_inflamatoria_intestinal:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.malaltia_inflamatoria_intestinal[up]:
                sexe = self.poblacio[id_cip_sec]["sexe"]
                edat_grup = self.poblacio[id_cip_sec]["edat_grup"]    
                poblatips = self.poblacio[id_cip_sec]["poblatip"]                        
                poblacio_descartada_den_nutpat13[up].add(id_cip_sec)
                for poblatip in poblatips:
                    self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT07", "DEN")] += 1
                    if id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                        self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT07", "NUM")] += 1

        # NUTPAT08

        for up in self.malaltia_renal_cronica:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.malaltia_renal_cronica[up]:
                sexe = self.poblacio[id_cip_sec]["sexe"]
                edat_grup = self.poblacio[id_cip_sec]["edat_grup"]    
                poblatips = self.poblacio[id_cip_sec]["poblatip"]                        
                poblacio_descartada_den_nutpat13[up].add(id_cip_sec)
                for poblatip in poblatips:
                    self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT08", "DEN")] += 1
                    if id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                        self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT08", "NUM")] += 1

        # NUTPAT09

        for up in self.intolerancia_alergia_alimentaria:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.intolerancia_alergia_alimentaria[up]:
                sexe = self.poblacio[id_cip_sec]["sexe"]
                edat_grup = self.poblacio[id_cip_sec]["edat_grup"]    
                poblatips = self.poblacio[id_cip_sec]["poblatip"]                        
                poblacio_descartada_den_nutpat13[up].add(id_cip_sec)
                for poblatip in poblatips:
                    self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT09", "DEN")] += 1
                    if id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                        self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT09", "NUM")] += 1

        # NUTPAT10

        for up in self.DM2:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            if up in self.HBA1C_sup_10_ultims12mesos_adults:
                for id_cip_sec in self.DM2[up] & self.HBA1C_sup_10_ultims12mesos_adults[up]:
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    edat_grup = self.poblacio[id_cip_sec]["edat_grup"]    
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]                         
                    poblacio_descartada_den_nutpat13[up].add(id_cip_sec)
                    for poblatip in poblatips:
                        self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT10", "DEN")] += 1
                        if id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                            self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT10", "NUM")] += 1  

        # NUTPAT11

        ups = self
        for up in self.RCV_superior_10_ultims12mesos_adults:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            if up in self.ultim_IMC_between_30_35_adults:
                for id_cip_sec in self.RCV_superior_10_ultims12mesos_adults[up] & self.IMCs_between_30_35_ultims12mesos_adults[up]:
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    edat_grup = self.poblacio[id_cip_sec]["edat_grup"]    
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]                         
                    poblacio_descartada_den_nutpat13[up].add(id_cip_sec)
                    for poblatip in poblatips:
                        self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT11", "DEN")] += 1
                        if id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                            self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT11", "NUM")] += 1

        # NUTPAT12

        for up in self.IMCs_between_30_35_ultims12mesos_adults:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            if up in self.CI_o_AVC_adults:
                for id_cip_sec in self.IMCs_between_30_35_ultims12mesos_adults[up] & self.CI_o_AVC_adults[up]:
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    edat_grup = self.poblacio[id_cip_sec]["edat_grup"]    
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]                         
                    poblacio_descartada_den_nutpat13[up].add(id_cip_sec)
                    for poblatip in poblatips:
                        self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT12", "DEN")] += 1
                        if id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                            self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT12", "NUM")] += 1

        # NUTPAT13

        for id_cip_sec in self.poblacio:
            up = self.poblacio[id_cip_sec]["up"]
            up = UP_MODS.get(up, up)
            if id_cip_sec not in poblacio_descartada_den_nutpat13[up]:
                br = self.centres[up]
                sexe = self.poblacio[id_cip_sec]["sexe"]
                edat_grup = self.poblacio[id_cip_sec]["edat_grup"]    
                poblatips = self.poblacio[id_cip_sec]["poblatip"]                         
                for poblatip in poblatips:
                    self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT13", "DEN")] += 1
                    if id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                        self.resultat_pat[(br, edat_grup, sexe, poblatip, "NUTPAT13", "NUM")] += 1

        # NUTPOB01, NUTPOB02, NUTPOB03, NUTPOB04, NUTPOB05, NUTPOB06, NUTPOB08

        indicadors_pob = ("NUTPOB01", "NUTPOB02", "NUTPOB03", "NUTPOB04", "NUTPOB05", "NUTPOB06", "NUTPOB08", "NUTPOB09")
        for up, edat, sexe, poblatip, n in self.poblacio_detallada:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for indicador in indicadors_pob:
                self.resultat_pob[(br, edat, sexe, poblatip, indicador, "DEN")] += n

        # NUTPOB01
    
        for up in self.pacients_visitats_NUT:
            if up in self.centres:
                br = self.centres[up]
                for id_cip_sec in self.pacients_visitats_NUT[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    edat = u.ageConverter(edat)
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]
                    for poblatip in poblatips:
                        if (br, edat, sexe, poblatip, "NUTPOB01", "DEN") in self.resultat_pob:
                            self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB01", "NUM")] += 1

        # NUTPOB02

        for up in self.pacients_activitats_liderades_NUT:
            if up in self.centres:
                br = self.centres[up]
                for id_cip_sec in self.pacients_activitats_liderades_NUT[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    edat = u.ageConverter(edat)
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]
                    for poblatip in poblatips:
                        if (br, edat, sexe, poblatip, "NUTPOB02", "DEN") in self.resultat_pob:
                            self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB02", "NUM")] += 1

        # NUTPOB03

        for up in self.pacients_activitats_amb_participacio_NUT:
            if up in self.centres:
                br = self.centres[up]
                for id_cip_sec in self.pacients_activitats_amb_participacio_NUT[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    edat = u.ageConverter(edat)
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]
                    for poblatip in poblatips:
                        if (br, edat, sexe, poblatip, "NUTPOB03", "DEN") in self.resultat_pob:
                            self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB03", "NUM")] += 1

        # NUTPOB04

        for up in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT:
            if up in self.centres:
                br = self.centres[up]
                for id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    edat = u.ageConverter(edat)
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]
                    for poblatip in poblatips:
                        if (br, edat, sexe, poblatip, "NUTPOB04", "DEN") in self.resultat_pob:
                                self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB04", "NUM")] += 1 

        # NUTPOB05, NUTPOB06

        for up in self.prescripcions_socials:
            up = UP_MODS.get(up, up)
            if up in self.centres:
                br = self.centres[up]
                for id_cip_sec in self.prescripcions_socials[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    edat = u.ageConverter(edat)
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]
                    for poblatip in poblatips:
                        if id_cip_sec not in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                            if (br, edat, sexe, poblatip, "NUTPOB05", "DEN") in self.resultat_pob:
                                self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB05", "NUM")] += 1
                        else:
                            if (br, edat, sexe, poblatip, "NUTPOB06", "DEN") in self.resultat_pob:
                                self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB06", "NUM")] += 1

        # NUTPOB07

        for up in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT:
            if up in self.centres:
                br = self.centres[up]
                for id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    edat = u.ageConverter(edat)                
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]
                    for poblatip in poblatips:
                        self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB07", "DEN")] += 1
        for (up, edat, sexe, poblatip), n in self.visites_de_consulta_detall_ultim_any.items():
            if up in self.centres:
                br = self.centres[up]  
                self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB07", "NUM")] = n

        # NUTPOB08

        for up in self.pacients_activitats_liderades_o_amb_participacio_NUT:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.pacients_activitats_liderades_o_amb_participacio_NUT[up]:
                edat = self.poblacio[id_cip_sec]["edat"]
                edat = u.ageConverter(edat)
                sexe = self.poblacio[id_cip_sec]["sexe"]
                poblatips = self.poblacio[id_cip_sec]["poblatip"]      
                for poblatip in poblatips:
                    self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB08", "NUM")] += 1

        # NUTPOB09

        for up in self.pacients_nomes_visitats_NUT:
            if up in self.centres:
                br = self.centres[up]
                for id_cip_sec in self.pacients_nomes_visitats_NUT[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    edat = u.ageConverter(edat)                    
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]
                    for poblatip in poblatips:
                        if (br, edat, sexe, poblatip, "NUTPOB09", "DEN") in self.resultat_pob:
                            self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB09", "NUM")] += 1

        # NUTPOB10

        for up in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.pacients_visita_io_activitats_liderades_o_amb_participacio_NUT[up]:
                edat = self.poblacio[id_cip_sec]["edat"]
                edat = u.ageConverter(edat)                    
                sexe = self.poblacio[id_cip_sec]["sexe"]
                poblatips = self.poblacio[id_cip_sec]["poblatip"]
                for poblatip in poblatips:
                    self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB10", "DEN")] += 1
                    if up in self.prescripcions_socials:
                        if id_cip_sec in self.prescripcions_socials[up]:
                            self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB10", "NUM")] += 1

        # NUTPOB11

        for up in self.prescripcions_socials:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.prescripcions_socials[up]:
                edat = self.poblacio[id_cip_sec]["edat"]
                edat = u.ageConverter(edat)                    
                sexe = self.poblacio[id_cip_sec]["sexe"]
                poblatips = self.poblacio[id_cip_sec]["poblatip"]
                for poblatip in poblatips:
                    self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB11", "DEN")] += 1
                    if id_cip_sec in self.prescripcions_socials_seguiment_per_data:
                        ultima_data_alta = max(self.prescripcions_socials_seguiment_per_data[id_cip_sec])
                        if self.prescripcions_socials_seguiment_per_data[id_cip_sec][ultima_data_alta] == "S":
                            self.resultat_pob[(br, edat, sexe, poblatip, "NUTPOB11", "NUM")] += 1

        # NUTRES01

        for up in self.visites_de_consulta_ultims15a3mesos["all"]:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.visites_de_consulta_ultims15a3mesos["all"][up]:
                if id_cip_sec in self.desnutricio_malnutricio_ultims15a3mesos[up] or id_cip_sec in self.MNA_inferior_igual_23_5_ultims15a3mesos_adults[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    edat = u.ageConverter(edat)
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]
                    for poblatip in poblatips:
                        self.resultat_res[(br, edat, sexe, poblatip, "NUTRES01", "DEN")] += 1
                        if id_cip_sec in self.MNA_superior_23_5[up]:
                            self.resultat_res[(br, edat, sexe, poblatip, "NUTRES01", "NUM")] += 1
                        elif id_cip_sec in self.desnutricio_malnutricio_dates[up]:
                            data_ps_ultima_desnutricio_malnutricio = max(self.desnutricio_malnutricio_dates[up][id_cip_sec])
                            data_fi_ps_ultima_desnutricio_malnutricio = self.desnutricio_malnutricio_dates[up][id_cip_sec][data_ps_ultima_desnutricio_malnutricio]
                            if data_fi_ps_ultima_desnutricio_malnutricio < dext:
                                self.resultat_res[(br, edat, sexe, poblatip, "NUTRES01", "NUM")] += 1
        # NUTRES02

        for up in self.visites_de_consulta_ultims15a3mesos["all"]:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.visites_de_consulta_ultims15a3mesos["all"][up]:
                if id_cip_sec in self.disfagia_ultims15a3mesos[up] or id_cip_sec in self.cribatge_disfagia_superior_igual_3_ultims15a3mesos_adults[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    edat = u.ageConverter(edat)
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]
                    for poblatip in poblatips:
                        self.resultat_res[(br, edat, sexe, poblatip, "NUTRES02", "DEN")] += 1
                        if id_cip_sec in self.cribatge_disfagia_descendent_ultims_15_mesos_adults[up]:
                            self.resultat_res[(br, edat, sexe, poblatip, "NUTRES02", "NUM")] += 1
                        elif id_cip_sec in self.disfagia_dates[up]:
                            data_dx_ultima_disfagia = max(self.disfagia_dates[up][id_cip_sec])
                            data_dx_tancament_ultima_disfagia = self.disfagia_dates[up][id_cip_sec][data_ultima_disfagia]
                            if data_dx_tancament_ultima_disfagia < dext:
                                self.resultat_res[(br, edat, sexe, poblatip, "NUTRES01", "NUM")] += 1

        # NUTRES03

        for up in self.visites_de_consulta_ultims15a3mesos["all"]:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.visites_de_consulta_ultims15a3mesos["all"][up]:
                if id_cip_sec in self.pes_superior_percentil_95_ultims15a3mesos_pedia[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    edat = u.ageConverter(edat)
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]
                    for poblatip in poblatips:
                        self.resultat_res[(br, edat, sexe, poblatip, "NUTRES03", "DEN")] += 1
                        if id_cip_sec in self.ultim_pes_inferior_percentil_95_pedia_ultims15a3mesos[up]:
                            self.resultat_res[(br, edat, sexe, poblatip, "NUTRES03", "NUM")] += 1

        # NUTRES04

        for up in self.visites_de_consulta_ultims15a3mesos["all"]:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.visites_de_consulta_ultims15a3mesos["all"][up]:
                if id_cip_sec in self.IMCs_sup35_ultims15a3mesos_adults[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    edat = u.ageConverter(edat)
                    sexe = self.poblacio[id_cip_sec]["sexe"]
                    poblatips = self.poblacio[id_cip_sec]["poblatip"]
                    for poblatip in poblatips:
                        self.resultat_res[(br, edat, sexe, poblatip, "NUTRES04", "DEN")] += 1
                        if id_cip_sec in self.pes_descendent_5_percent_ultims15a3mesos_adults[up]:
                            self.resultat_res[(br, edat, sexe, poblatip, "NUTRES04", "NUM")] += 1
        
        # NUTRES05

        for up in self.visites_de_consulta_ultims15a3mesos["all"]:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.visites_de_consulta_ultims15a3mesos["all"][up]:
                if id_cip_sec in self.IMCs_sup35_ultims15a3mesos_adults[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    if 14 < edat:
                        edat = u.ageConverter(edat)
                        sexe = self.poblacio[id_cip_sec]["sexe"]
                        poblatips = self.poblacio[id_cip_sec]["poblatip"]
                        for poblatip in poblatips:
                            self.resultat_res[(br, edat, sexe, poblatip, "NUTRES05", "DEN")] += 1
                            if id_cip_sec in self.pes_descendent_10_percent_ultims15a3mesos_adults[up]:
                                self.resultat_res[(br, edat, sexe, poblatip, "NUTRES05", "NUM")] += 1
        
        # NUTRES06

        for up in self.visites_de_consulta_ultims15a3mesos["all"]:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.visites_de_consulta_ultims15a3mesos["all"][up]:
                if id_cip_sec in self.IMCs_between_30_35_ultims15a3mesos_adults[up] & self.RCV_superior_10_ultims15a3mesos_adults[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    if 14 < edat:
                        edat = u.ageConverter(edat)
                        sexe = self.poblacio[id_cip_sec]["sexe"]
                        poblatips = self.poblacio[id_cip_sec]["poblatip"]
                        for poblatip in poblatips:
                            self.resultat_res[(br, edat, sexe, poblatip, "NUTRES06", "DEN")] += 1
                            if id_cip_sec in self.pes_descendent_5_percent_ultims15a3mesos_adults[up]:
                                self.resultat_res[(br, edat, sexe, poblatip, "NUTRES06", "NUM")] += 1
        
        # NUTRES07

        for up in self.visites_de_consulta_ultims15a3mesos["all"]:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.visites_de_consulta_ultims15a3mesos["all"][up]:
                if id_cip_sec in self.IMCs_between_30_35_ultims15a3mesos_adults[up] & self.RCV_superior_10_ultims15a3mesos_adults[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    if 14 < edat:
                        edat = u.ageConverter(edat)
                        sexe = self.poblacio[id_cip_sec]["sexe"]
                        poblatips = self.poblacio[id_cip_sec]["poblatip"]
                        for poblatip in poblatips:
                            self.resultat_res[(br, edat, sexe, poblatip, "NUTRES07", "DEN")] += 1
                            if id_cip_sec in self.pes_descendent_10_percent_ultims15a3mesos_adults[up]:
                                self.resultat_res[(br, edat, sexe, poblatip, "NUTRES07", "NUM")] += 1
        
        # NUTRES08

        for up in self.visites_de_consulta_ultims15a3mesos["all"]:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.visites_de_consulta_ultims15a3mesos["all"][up]:
                if id_cip_sec in self.IMCs_between_30_35_ultims15a3mesos_adults[up] & self.CI_o_AVC_adults_mes3mesos[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    if 14 < edat:
                        edat = u.ageConverter(edat)
                        sexe = self.poblacio[id_cip_sec]["sexe"]
                        poblatips = self.poblacio[id_cip_sec]["poblatip"]
                        for poblatip in poblatips:
                            self.resultat_res[(br, edat, sexe, poblatip, "NUTRES08", "DEN")] += 1
                            if id_cip_sec in self.pes_descendent_5_percent_ultims15a3mesos_adults[up]:
                                self.resultat_res[(br, edat, sexe, poblatip, "NUTRES08", "NUM")] += 1
        
        # NUTRES09

        for up in self.visites_de_consulta_ultims15a3mesos["all"]:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.visites_de_consulta_ultims15a3mesos["all"][up]:
                if id_cip_sec in self.IMCs_between_30_35_ultims15a3mesos_adults[up] & self.CI_o_AVC_adults_mes3mesos[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    if 14 < edat:
                        edat = u.ageConverter(edat)
                        sexe = self.poblacio[id_cip_sec]["sexe"]
                        poblatips = self.poblacio[id_cip_sec]["poblatip"]
                        for poblatip in poblatips:
                            self.resultat_res[(br, edat, sexe, poblatip, "NUTRES09", "DEN")] += 1
                            if id_cip_sec in self.pes_descendent_10_percent_ultims15a3mesos_adults[up]:
                                self.resultat_res[(br, edat, sexe, poblatip, "NUTRES09", "NUM")] += 1

        # NUTRES10

        for up in self.visites_de_consulta_ultims15a3mesos["all"]:
            up = UP_MODS.get(up, up)
            br = self.centres[up]
            for id_cip_sec in self.visites_de_consulta_ultims15a3mesos["all"][up]:
                if id_cip_sec in self.DM2[up] & self.HBA1C_sup_10_ultims15a3mesos_adults[up]:
                    edat = self.poblacio[id_cip_sec]["edat"]
                    if 14 < edat:
                        edat = u.ageConverter(edat)
                        sexe = self.poblacio[id_cip_sec]["sexe"]
                        poblatips = self.poblacio[id_cip_sec]["poblatip"]
                        for poblatip in poblatips:
                            self.resultat_res[(br, edat, sexe, poblatip, "NUTRES10", "DEN")] += 1
                            if id_cip_sec in self.HBA1C_dates[up]:
                                dates = list(self.HBA1C_dates[up][id_cip_sec].keys())
                                dates.sort()
                                ultim_valor = 999
                                for data in dates:
                                    if data < dext_menys3mesos:
                                        ultim_valor = self.HBA1C_dates[up][id_cip_sec][data]
                                    else:
                                        break
                                if ultim_valor <= 10:
                                    self.resultat_res[(br, edat, sexe, poblatip, "NUTRES10", "NUM")] += 1

        # Indicadors amb numerador cero

        for br, ind, tip in self.resultat:
            if tip == "DEN":
                if (br, ind, "NUM") not in self.resultat:
                    resultat_cero[(br, ind, "NUM")] = 0
        self.resultat.update(resultat_cero)

        for br, edat, sexe, poblatip, ind, tip in self.resultat_pat:
            if tip == "DEN":
                if (br, edat, sexe, poblatip, ind, "NUM") not in self.resultat_pat:
                    resultat_cero_pat[(br, edat, sexe, poblatip, ind, "NUM")] = 0
        self.resultat_pat.update(resultat_cero_pat)
        
        for br, edat, sexe, poblatip, ind, tip in self.resultat_pob:
            if tip == "DEN":
                if (br, edat, sexe, poblatip, ind, "NUM") not in self.resultat_pob:
                    resultat_cero_pob[(br, edat, sexe, poblatip, ind, "NUM")] = 0
        self.resultat_pob.update(resultat_cero_pob)

        for br, edat, sexe, poblatip, ind, tip in self.resultat_res:
            if tip == "DEN":
                if (br, edat, sexe, poblatip, ind, "NUM") not in self.resultat_res:
                    resultat_cero_res[(br, edat, sexe, poblatip, ind, "NUM")] = 0
        self.resultat_res.update(resultat_cero_res)


    def export_klx(self):
        """."""

        u.createTable(tb, "(ind varchar(10), br varchar(5), analisis varchar(3), n double)", db, rm=True)
        upload = [(ind, br, analisis, n) for (br, ind, analisis), n in self.resultat.items()]
        u.listToTable(upload, tb, db)
        sql = """
                SELECT
                    ind,
                    "Aperiodo",
                    br,
                    analisis,
                    "NOCAT",
                    "NOIMP",
                    "DIM6SET",
                    "N",
                    n
                FROM
                    {}.{}
              """.format(db, tb)
        u.exportKhalix(sql, file, force=1)

        u.createTable(tb_pat, "(ind varchar(10), br varchar(5), edat varchar(30), sexe varchar(4), poblatip varchar(30), analisis varchar(3), n double)", db, rm=True)
        upload = [(ind, br, edat, sexe, poblatip, analisis, n) for (br, edat, sexe, poblatip, ind, analisis), n in self.resultat_pat.items()]
        u.listToTable(upload, tb_pat, db)
        sql_pat = """
                    SELECT
                        ind,
                        "Aperiodo",
                        br,
                        analisis,
                        edat,
                        poblatip,
                        sexe,
                        "N",
                        n
                        FROM
                        {}.{}
                  """.format(db, tb_pat)
        u.exportKhalix(sql_pat, file_pat, force=1)

        u.createTable(tb + "_pob", "(ind varchar(10), br varchar(5), edat varchar(30), sexe varchar(4), poblatip varchar(30), analisis varchar(3), n double)", db, rm=True)
        upload_pob = [(ind, br, edat, sexe, poblatip, analisis, n) for (br, edat, sexe, poblatip, ind, analisis), n in self.resultat_pob.items()]
        u.listToTable(upload_pob, tb + "_pob", db)
        sql_pob = """
                SELECT
                    ind,
                    "Aperiodo",
                    br,
                    analisis,
                    edat,
                    poblatip,
                    sexe,
                    "N",
                    n
                FROM
                    {}.{}
              """.format(db, tb + "_pob")
        u.exportKhalix(sql_pob, file_pob, force=1)

        u.createTable(tb + "_res", "(ind varchar(10), br varchar(5), edat varchar(30), sexe varchar(4), poblatip varchar(30), analisis varchar(3), n double)", db, rm=True)
        upload_res = [(ind, br, edat, sexe, poblatip, analisis, n) for (br, edat, sexe, poblatip, ind, analisis), n in self.resultat_res.items()]
        u.listToTable(upload_res, tb + "_res", db)
        sql_res = """
                SELECT
                    ind,
                    "Aperiodo",
                    br,
                    analisis,
                    edat,
                    poblatip,
                    sexe,
                    "N",
                    n
                FROM
                    {}.{}
              """.format(db, tb + "_res")
        u.exportKhalix(sql_res, file_res, force=1)

def sub_get_problemes(sql):
    
    problemes = c.defaultdict(lambda: c.defaultdict(set))

    for id_cip_sec, codi_ps, data_ps, data_fi_ps in u.getAll(sql, "import"):
        problemes[id_cip_sec][codi_ps].add((data_ps, data_fi_ps))

    return problemes 

if True:
    import time
    u.execute("""UPDATE dextraccio SET data_ext = '2023-01-31'""", """test""")
    u.execute("""UPDATE dextraccio SET mensual = 1""", """test""")
    time.sleep(15)
    CSMA()
    u.execute("""UPDATE dextraccio SET data_ext = '2023-02-28'""", """test""")
    u.execute("""UPDATE dextraccio SET mensual = 1""", """test""")
    time.sleep(15)
    CSMA()
    u.execute("""UPDATE dextraccio SET data_ext = '2023-03-31'""", """test""")
    u.execute("""UPDATE dextraccio SET mensual = 1""", """test""")
    time.sleep(15)
    CSMA()
    u.execute("""UPDATE dextraccio SET data_ext = '2023-04-30'""", """test""")
    u.execute("""UPDATE dextraccio SET mensual = 1""", """test""")
    CSMA()
    u.execute("""UPDATE dextraccio SET data_ext = '2023-05-31'""", """test""")
    u.execute("""UPDATE dextraccio SET mensual = 1""", """test""")
    CSMA()