# coding: latin1

"""
.
"""

import sisapUtils as u


TABLES = ("EXP_QC_FORATS", "EXP_QC_ECONSULTA", "EXP_QC_GIDA", "EXP_QC_VISITES",
          "EXP_QC_DEMORA")
FILE = "QC_ORGANITZACIO"
BASE = "select k0, k1, k2, k3, if(k4 = 'ANUAL', 'ANUAL', 'MENSUAL'), k5, \
               'DIM6SET', 'N', v \
        from altres.{} \
        inner join nodrizas.cat_centres cc \
        on substr(k2, 1, 5) = cc.ics_codi \
        where k4 {} 'PREVI' and length(k2) {} 5"


for suffix, symbol in (("_ACTUAL", "<>"), ("_PREVI", "=")):
    for susuffix, sysymbol in (("", "="), ("_UBA", ">")):
        sql = " union ".join(BASE.format(table, symbol, sysymbol)
                             for table in TABLES)
        u.exportKhalix(sql, FILE + suffix + susuffix)
