# -*- coding: utf-8 -*-

"""
.
"""

import sisapUtils as u
import collections

class Agregacio():
    def __init__(self):
        self.agrega_up()
        self.agrega_uba()

    def agrega_up(self):
        print('UP')
        print('no eqa')
        # No EQA
        print('den')
        den_no_eqa = {}
        sql = """SELECT indicador, periode, up, sum(N) DEN
            FROM dwsisap.sisap_master_indicadors_up
            WHERE poblacio = 'NOINSAT' AND ANalisi = 'DEN'
            AND INDICADOR NOT LIKE 'EQA%'
            GROUP BY indicador, periode, up"""
        for indi, periode, up, den in u.getAll(sql, 'exadata'):
            den_no_eqa[(indi, periode, up)] = den
        print('num')
        num_no_eqa = {}
        sql = """SELECT indicador, periode, up, sum(N) NUM
            FROM dwsisap.sisap_master_indicadors_up
            WHERE poblacio = 'NOINSAT' AND ANalisi = 'NUM'
            AND INDICADOR NOT LIKE 'EQA%'
            GROUP BY indicador, periode, up"""
        for indi, periode, up, num in u.getAll(sql, 'exadata'):
            num_no_eqa[(indi, periode, up)] = num
        print('penjar')
        upload = []
        for e in den_no_eqa:
            indi = e[0]
            periode = e[1]
            up = e[2]
            den = den_no_eqa[e]
            try:
                num = num_no_eqa[e]
            except: 
                num = 0
            upload.append((indi, periode, up, num, den))
        
        cols = """(indicador varchar2(20), periode varchar2(5), up varchar2(5), numerador number(38,0), denominador number(38,0))"""
        u.createTable('sisap_master_indi_agre_up', cols, 'exadata', rm=True)
        u.listToTable(upload, 'sisap_master_indi_agre_up', 'exadata')
        u.grantSelect('sisap_master_indi_agre_up','DWSISAP_ROL','exadata')

        # EQA
        print('EQA')
        print('den')
        den_no_eqa = {}
        sql = """SELECT SUBSTR(indicador,1,7), periode, up, sum(N) DEN
            FROM dwsisap.sisap_master_indicadors_up
            WHERE poblacio = 'NOINSAT' AND ANalisi = 'DEN'
            AND INDICADOR LIKE 'EQA%'
            GROUP BY SUBSTR(indicador,1,7), periode, up"""
        for indi, periode, up, den in u.getAll(sql, 'exadata'):
            den_no_eqa[(indi, periode, up)] = den
        print('num')
        num_no_eqa = {}
        sql = """SELECT SUBSTR(indicador,1,7), periode, up, sum(N) NUM
            FROM dwsisap.sisap_master_indicadors_up
            WHERE poblacio = 'NOINSAT' AND ANalisi = 'NUM'
            AND INDICADOR LIKE 'EQA%'
            GROUP BY SUBSTR(indicador,1,7), periode, up"""
        for indi, periode, up, num in u.getAll(sql, 'exadata'):
            num_no_eqa[(indi, periode, up)] = num
        print('penjar')
        upload = []
        for e in den_no_eqa:
            indi = e[0]
            periode = e[1]
            up = e[2]
            den = den_no_eqa[e]
            try:
                num = num_no_eqa[e]
            except: 
                num = 0
            upload.append((indi, periode, up, num, den))
        u.listToTable(upload, 'sisap_master_indi_agre_up', 'exadata')



    def agrega_uba(self):
        print('UBA')
        print('den')
        den = {}
        sql = """SELECT indicador, periode, professional, sum(N) DEN
            FROM dwsisap.sisap_master_indicadors_uba
            WHERE poblacio = 'NOINSAT' AND ANalisi = 'DEN'
            GROUP BY indicador, periode, professional"""
        for indi, periode, up, d in u.getAll(sql, 'exadata'):
            den[(indi, periode, up)] = d
        print('num')
        num = {}
        sql = """SELECT indicador, periode, professional, sum(N) DEN
            FROM dwsisap.sisap_master_indicadors_uba
            WHERE poblacio = 'NOINSAT' AND ANalisi = 'NUM'
            GROUP BY indicador, periode, professional"""
        for indi, periode, up, n in u.getAll(sql, 'exadata'):
            num[(indi, periode, up)] = n
        print('penjar')
        upload = []
        for e in den:
            indi = e[0]
            periode = e[1]
            up = e[2]
            d = den[e]
            try:
                n = num[e]
            except: 
                n = 0
            upload.append((indi, periode, up, n, d))
        
        cols = """(indicador varchar2(20), periode varchar2(5), professional varchar2(14), numerador number(38,0), denominador number(38,0))"""
        u.createTable('sisap_master_indi_agre_uba', cols, 'exadata', rm=True)
        u.listToTable(upload, 'sisap_master_indi_agre_uba', 'exadata')
        u.grantSelect('sisap_master_indi_agre_uba','DWSISAP_ROL','exadata')


if __name__ == "__main__":
    if u.IS_MENSUAL:
        Agregacio()