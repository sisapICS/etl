# -*- coding: utf-8 -*-

"""
.
"""

import sisapUtils as u


class mapa_ap_uba():
    def __init__(self):
        self.tb = 'sisap_master_indicadors_uba'
        self.bd = 'exadata'
        self.exclusions = ('EQA3008A', 'EQA3108E', 'EQA3108D', 'EQA3108A', 'EQA3108C', 'EQA3108B', 'EQA3006A', 'EQD024204', 'EQA3115A', 'EQA3005A')
        self.extraccio()
        self.create_table()
        self.centres()
        self.ALTRES()
        self.CATSALUT()
        self.EQA_INDICADORS()
        self.ESIAP()
        self.GIS()
        self.JAIL()
        self.LABORATORI()
        self.PEDIA()
        
    
    def extraccio(self):
        print('dextraccio')
        self.year, self.month = u.getOne("select year(data_ext), month(data_ext) from dextraccio", 'nodrizas')
        month = str(self.month) if len(str(self.month)) == 2 else '0' + str(self.month)
        self.periode = 'A' + str(self.year)[2:] + month

    def create_table(self):
        print('crear taula')
        cols =  '(indicador varchar(20), periode varchar(5), professional varchar(14), \
                analisi varchar(10), edat varchar(10), poblacio varchar(10), sexe varchar(7), n int)'
        u.createTable(self.tb, cols, self.bd, rm=True)
        u.grantSelect('sisap_master_indicadors_uba','DWSISAP_ROL','exadata')

    def centres(self):
        print('centres')
        self.centres_ics = dict()
        sql = """select scs_codi, ics_codi 
                from nodrizas.cat_centres"""
        for codi, ics in u.getAll(sql, 'nodrizas'):
            self.centres_ics[codi] = ics
    
    def ALTRES(self):
        print('altres')
        print('1')
        upload = []
        sql = """select up, uba, tipus, comb, indicador, num, den 
                from altres.eqa_tao_khalix_uba_ind etkup;"""
        for up, uba, tipus, detalle, indicador, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                professional = self.centres_ics[up] + tipus + uba
                upload.append((indicador, self.periode, professional, 'NUM', 'NOCAT', detalle, 'DIM6SET', num))
                upload.append((indicador, self.periode, professional, 'DEN', 'NOCAT', detalle, 'DIM6SET', den))
        u.listToTable(upload, self.tb, self.bd)
        print('2')
        upload = []
        sql = """select up, uba, tipus, comb, indicador, num, den 
                from altres.eqa_tir_khalix_uba_ind"""
        for up, uba, tipus, detalle, indicador, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                professional = self.centres_ics[up] + tipus + uba
                upload.append((indicador, self.periode, professional, 'NUM', 'NOCAT', detalle, 'DIM6SET', num))
                upload.append((indicador, self.periode, professional, 'DEN', 'NOCAT', detalle, 'DIM6SET', den))
        u.listToTable(upload, self.tb, self.bd)
        print('3')
        upload = []
        sql = """select indicador, up, uba, classe, tipus, sum(recompte) 
                    from altres.exp_khalix_ag_accessibilitat_uba ekaau
	                group by indicador, up, uba, classe, tipus;"""
        for indicador, up, uba, tipus, analisis, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                professional = self.centres_ics[up] + tipus + uba
                upload.append((indicador, self.periode, professional, analisis, 'NOCAT', detalle, 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('4')
        upload = []
        sql = """select up, uba, categ, tipus, recompte
                    from altres.exp_khalix_ag_longitudinalitat_uba;"""
        for up, uba, tipus, analisis, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                professional = self.centres_ics[up] + tipus + uba
                upload.append(('AGACCONT', self.periode, professional, analisis, 'NOCAT', 'NOIMP', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('5')
        upload = []
        sql = """select k0, k1, k2, k4, sum(v) 
                    from altres.exp_khalix_eqpf_uba ekeu
                    where length(k1) > 5
                    group by k0, k1, k2, k4"""
        for indicador, professional, analisis, detalle, n in u.getAll(sql, 'altres'):
            upload.append((indicador, self.periode, professional, analisis, 'NOCAT', detalle, 'SEXE', n))
        u.listToTable(upload, self.tb, self.bd)
        print('6')
        upload = []
        sql = """select d0, d1, d2, d3, d4, d5, d6, n from altres.exp_khalix_forats"""
        for indicador, periode, professional, analisis, nocat, detalle, dim6set, n in u.getAll(sql, 'altres'):
            upload.append((indicador, periode, professional, analisis, nocat, detalle, dim6set, n))
        u.listToTable(upload, self.tb, self.bd)
        print('7')
        upload = []
        sql = """select ent, ind, analisi, motiu, valor 
                from altres.exp_khalix_gida ekg
                where length(ent) > 5"""
        for professional, indicador, analisis, motiu, n in u.getAll(sql, 'altres'):
            upload.append((indicador, self.periode, professional, analisis, motiu, 'NOIMP', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('8')
        upload = []
        sql = """select entity, indicador, analisi, sum(valor)
                    from altres.exp_khalix_it eki
                    where length(entity) > 5
                    group by entity, indicador, analisi"""
        for professional, indicador, analisis, n in u.getAll(sql, 'altres'):
            upload.append((indicador, self.periode, professional, analisis, 'NOCAT', 'NOINSAT', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('9')
        upload = []
        sql = """select * from altres.exp_khalix_it_up_ind_uba ekiuiu"""
        for up, uba, indicador, analisis, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                professional = self.centres_ics[up] + 'M' + uba
                upload.append((indicador, self.periode, professional, analisis, 'NOCAT', detalle, 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('10')
        upload = []
        sql = """select indicador, up, tipus, uba, conc, n
                    from altres.exp_khalix_prof_uba ekpu;"""
        for indicador, up, tipus, uba, analisis, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                professional = self.centres_ics[up] + tipus + uba
                upload.append((indicador, self.periode, professional, analisis, 'NOCAT', 'NOIMP', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('11')
        upload = []
        sql = """select ent, indicador, analisis, n
                    from altres.exp_khalix_uba_econsulta
                    where length(ent) > 5"""
        for professional, indicador, analisis, n in u.getAll(sql, 'altres'):
            upload.append((indicador, self.periode, professional, analisis, 'NOCAT', 'NOINSAT', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('12')
        upload = []
        sql = """select ent, indicador, analisis, n
                    from altres.exp_khalix_uba_econsulta_6m
                    where length(ent) > 5"""
        for professional, indicador, analisis, n in u.getAll(sql, 'altres'):
            upload.append((indicador, self.periode, professional, analisis, 'NOCAT', 'NOINSAT', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)

    def ATDOM(self):
        # preguntar si això ve d ela bbdd de atdom o d'algun altre lloc?
        upload = []
        sql = """"""
        pass
        # en cas que sigui aquesta:
        #       - atdom.exp_khalix_gida_atdom ekga --> motiu?
        #       - atdom.exp_khalix_cont_atdom ekca  --> no tenim uba
        #       - atdom.exp_khalix_long_atdom ekla --> no tenim uba
        #       - atdom.exp_khalix_tests_atdom ekta --> no tenim uba
    
    def CATSALUT(self):
        print('catsalut')
        upload = []
        sql = """select up, uba, tipus, indicador, analisis, detalle, valor from catsalut.exp_khalix_uba_ind ekui"""
        for up, uba, tipus, indicador, analisis, detalle, valor in u.getAll(sql, 'catsalut'):
            if up in self.centres_ics:
                professional = self.centres_ics[up] + tipus + uba
                upload.append((indicador, self.periode, professional, analisis, 'NOCAT', detalle, 'DIM6SET', valor))
        u.listToTable(upload, self.tb, self.bd)

        # A CATSALUT TENIM AQUESTES DUES TAULES TAMBÉ:
        # select * from catsalut.exp_khalix_cmbdap03_uba ekcu ; 
        # select * from catsalut.exp_khalix_pcc_maca_uba ekpmu;
        # PERÒ NO CONTENEN UBA TOT I DIR-SE UBA
    
    def EQA_INDICADORS(self):
        print('eqa_indi')
        print('1')
        upload = []
        sql = """select up, uba, tipus, concat(SUBSTRING(indicador, 1, 3), substring(indicador, 5, 6)), analisis, detalle, valor 
                    from eqa_ind.exp_khalix_uba_ind ekui
                    where analisis in ('NUM', 'DEN')"""
        for up, uba, tipus, indicador, analisis, detalle, valor in u.getAll(sql, 'eqa_ind'):
            if up in self.centres_ics and indicador not in self.exclusions:
                professional = self.centres_ics[up] + tipus + uba
                upload.append((indicador, self.periode, professional, analisis, 'NOCAT', detalle, 'DIM6SET', valor))
        u.listToTable(upload, self.tb, self.bd)
        print('2')
        upload = []
        sql = """select up, uba, tipus, indicador, analisis, dimensio, proces, n from eqa_ind.exp_khalix_uba_ind_proc ekuip"""
        for up, uba, tipus, indicador, analisis, dimensio, proces, valor in u.getAll(sql, 'eqa_ind'):
            if up in self.centres_ics and indicador not in self.exclusions:
                a = '' 
                if analisis == 'DENZ': a == 'DEN' 
                else: a = 'NUM'
                professional = self.centres_ics[up] + tipus + uba
                upload.append((indicador, self.periode, professional, a, dimensio, proces, 'DIM6SET', valor))
        u.listToTable(upload, self.tb, self.bd)
        print('3')
        upload = []
        sql = """select up, uba, tipus, indicador, analisi, dimensio, proces, n 
                from eqa_ind.exp_khalix_uba_ind_proc_mpoc_prov ekuipmp"""
        for up, uba, tipus, indicador, analisis, dimensio, proces, valor in u.getAll(sql, 'eqa_ind'):
            if up in self.centres_ics and indicador not in self.exclusions:
                professional = self.centres_ics[up] + tipus + uba
                upload.append((indicador, self.periode, professional, analisis, dimensio, proces, 'DIM6SET', valor))
        u.listToTable(upload, self.tb, self.bd)
        print('4')
        upload = []
        sql = """select up, uba, tipus, concat(SUBSTRING(indicador, 1, 3), substring(indicador, 5, 7)), analisis, detalle, valor 
                    from eqa_ind.exp_khalix_uba_indeqd
                    where analisis in ('NUM', 'DEN')"""
        for up, uba, tipus, indicador, analisis, detalle, valor in u.getAll(sql, 'eqa_ind'):
            if up in self.centres_ics and indicador not in self.exclusions:
                professional = self.centres_ics[up] + tipus + uba
                upload.append((indicador, self.periode, professional, analisis, 'NOINSAT', detalle, 'DIM6SET', valor))
        u.listToTable(upload, self.tb, self.bd)
    
    def ESIAP(self): ## FRANCESC
        print('esiap')
        upload = []
        sql = """select up, uba, indicador, analisi, valor from esiap.exp_khalix"""
        for up, uba, indicador, analisis, valor in u.getAll(sql, 'esiap'):
            if up in self.centres_ics:
                professional = self.centres_ics[up] + 'N' + uba
                upload.append((indicador, self.periode, professional, analisis, 'NOCAT', 'NOINSAT', 'DIM6SET', valor))
        u.listToTable(upload, self.tb, self.bd)

    def GIS(self):
        print('gis')
        upload = []
        sql = """select indicador, entity, comb, analisis, n from gis.exp_khalix_uba_gis ekug
                    where length(entity) > 5"""
        for indicador, professional, detalle, analisis, valor in u.getAll(sql, 'gis'):
            upload.append((indicador, self.periode, professional, analisis, 'NOCAT', detalle, 'DIM6SET', valor))
        u.listToTable(upload, self.tb, self.bd)

    def JAIL(self):
        print('jail')
        upload = []
        sql = """select up, uba, tipus, indicador, analisi, sum(valor)
                    from jail.exp_khalix_iep_uba ekiu
                    group by up, uba, tipus, indicador, analisi"""
        for up, uba, tipus, indicador, analisis, valor in u.getAll(sql, 'jail'):
            if up in self.centres_ics:
                professional = self.centres_ics[up] + tipus + uba
                upload.append((indicador, self.periode, professional, analisis, 'SEXE', 'NOINSAT', 'EDATS5', valor))
        u.listToTable(upload, self.tb, self.bd)
    
    def LABORATORI(self):
        print('lab')
        upload = []
        sql = """select account, entity, analysis, sum(n) from laboratori.exp_khalix ek
                    where length(entity) > 5
                    group by account, entity, analysis"""
        for indicador, professional, analisis, valor in u.getAll(sql, 'laboratori'):
            upload.append((indicador, self.periode, professional, analisis, 'SEXE', 'NOINSAT', 'DIM6SET', valor))
        u.listToTable(upload, self.tb, self.bd)

    def PEDIA(self):
        ### NO TINC CLAR SI AIXÒ ÉS PEDIA!!!
        print('pedia')
        upload = []
        sql = """select up, uba, tipus, concat(SUBSTRING(indicador, 1, 3), substring(indicador, 5, 6)), analisis, detalle, valor from eqa_ind.exp_khalix_uba_indeqd
                        where analisis in ('NUM', 'DEN')"""
        for up, uba, tipus, indicador, analisis, detalle, valor in u.getAll(sql, 'pedia'):
            if up in self.centres_ics and indicador not in self.exclusions:
                professional = self.centres_ics[up] + tipus + uba
                upload.append((indicador, self.periode, professional, analisis, 'NOCAT', detalle, 'DIM6SET', valor))
        u.listToTable(upload, self.tb, self.bd)
    
class indicadors_a_fer():
    def __init__(self):
        self.get_uba()
        self.get_up()
        self.a_fer()
    
    def get_up(self):
        self.ups = set()
        sql = """select i.nom as indicador, c.prioritat  
                from netflics_creuament c 
                inner join indicador_indicador i on c.indicador_id = i.id 
                where c.actiu = 1
                group by i.id
                """
        for indicador, prioritat in u.getAll(sql, ('umi', 'x0001')):
            self.ups.add(indicador)
        
    def get_uba(self):
        self.ubas = set()
        sql = """SELECT DISTINCT indicador FROM dwsisap.mapa_ap_uba"""
        for indicador, in u.getAll(sql, 'exadata'):
            self.ubas.add(indicador)

    def a_fer(self):
        self.pendents_uba = self.ups - self.ubas
        print(self.pendents_uba)
        cols =  '(indicador varchar(20), up int, uba int)'
        upload = []
        for e in list(self.ups):
            if e in self.ubas: upload.append([e, 1, 1])
            else: upload.append([e, 1, 0])
        for e in list(self.ubas):
            if e not in self.ups: upload.append([e, 0, 1])
        u.createTable('a_fer', cols, 'exadata', rm=True)
        u.grantSelect('a_fer','DWSISAP_ROL','exadata')
        u.listToTable(upload, 'a_fer', 'exadata')


if __name__ == "__main__":
    if u.IS_MENSUAL:
        mapa_ap_uba()
        indicadors_a_fer()


