# -*- coding: utf-8 -*-

"""
.
"""

import sisapUtils as u

class mapa_ap_up():
    def __init__(self):
        self.tb = 'sisap_master_indicadors_up'
        self.bd = 'exadata'
        self.extraccio()
        self.create_table()
        self.centres()
        self.ALTRES()
        self.ATDOM()
        self.CATSALUT()
        self.EQA_INDICADORS()
        self.ESIAP()
        self.GIS()
        self.JAIL()
        self.LABORATORI()
        self.PEDIA()
        self.EXTRES()
    
    def extraccio(self):
        print('dextraccio')
        self.year, self.month = u.getOne("select year(data_ext), month(data_ext) from dextraccio", 'nodrizas')
        month = str(self.month) if len(str(self.month)) == 2 else '0' + str(self.month)
        self.periode = 'A' + str(self.year)[2:] + month
    
    def create_table(self):
        print('crear taula')
        cols =  '(indicador varchar(20), periode varchar(5), up varchar(5), \
                analisi varchar(30), edat varchar(30), poblacio varchar(30), detail varchar(30), n int)'
        u.createTable(self.tb, cols, self.bd, rm=True)
        u.grantSelect(self.tb,'DWSISAP_ROL',self.bd)
    
    def centres(self):
        print('centres')
        self.centres_ics = dict()
        sql = """select scs_codi, ics_codi 
                from nodrizas.cat_centres"""
        for codi, ics in u.getAll(sql, 'nodrizas'):
            self.centres_ics[codi] = ics
    
    def ALTRES(self):
        print('altres')
        print('1')
        upload = []
        sql = """select up, edat, sexe, comb, indicador, num, den 
                from altres.eqa_tao_khalix_up_ind etkup;"""
        for up, edat, sexe, comb, indicador, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], 'NUM', edat, comb, sexe, num))
                upload.append((indicador, self.periode, self.centres_ics[up], 'DEN', edat, comb, sexe, den))
        u.listToTable(upload, self.tb, self.bd)
        # El mateix x presons
        upload = []
        sql = """select up, edat, sexe, comb, indicador, num, den 
                from altres.eqa_tao_khalix_up_pre etkup;"""
        for up, edat, sexe, comb, indicador, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], 'NUM', edat, comb, sexe, num))
                upload.append((indicador, self.periode, self.centres_ics[up], 'DEN', edat, comb, sexe, den))
        u.listToTable(upload, self.tb, self.bd)
        print('2')
        upload = []
        sql = """select up, edat, sexe, comb, indicador, num, den 
                from altres.eqa_tir_khalix_up_ind"""
        for up, edat, sexe, comb, indicador, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], 'NUM', edat, comb, sexe, num))
                upload.append((indicador, self.periode, self.centres_ics[up], 'DEN', edat, comb, sexe, den))
        u.listToTable(upload, self.tb, self.bd)
        # El mateix x presons
        upload = []
        sql = """select up, edat, sexe, comb, indicador, num, den 
                from altres.eqa_tir_khalix_up_pre"""
        for up, edat, sexe, comb, indicador, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], 'NUM', edat, comb, sexe, num))
                upload.append((indicador, self.periode, self.centres_ics[up], 'DEN', edat, comb, sexe, den))
        u.listToTable(upload, self.tb, self.bd)
        print('3')
        upload = []
        sql = """select indicador, up, servei, tipus, recompte 
                    from altres.exp_khalix_ag_accessibilitat"""
        for indicador, up, servei, tipus, recompte in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], tipus, 'NOCAT', servei, 'DIM6SET', recompte))
        u.listToTable(upload, self.tb, self.bd)
        print('4')
        upload = []
        sql = """select up, tipus, recompte
                    from altres.exp_khalix_ag_longitudinalitat_uba;"""
        for up, analisis, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append(('AGACCONT', self.periode, self.centres_ics[up], analisis, 'NOCAT', 'NOIMP', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('5')
        upload = []
        sql = """select k0, k1, k2, k4, v 
                    from altres.exp_khalix_eqpf
                    where length(k1) = 5"""
        for indicador, up, analisis, detalle, n in u.getAll(sql, 'altres'):
            upload.append((indicador, self.periode, up, analisis, 'NOCAT', detalle, 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('6')
        upload = []
        sql = """select d0, d1, SUBSTRING(d2,1,5), d3, d4, d5, d6, sum(n)
                from altres.exp_khalix_forats
                group by d0, d1, SUBSTRING(d2,1,5), d3, d4, d5, d6"""
        for indicador, periode, up, analisis, nocat, detalle, dim6set, n in u.getAll(sql, 'altres'):
            upload.append((indicador, periode, up, analisis, nocat, detalle, dim6set, n))
        u.listToTable(upload, self.tb, self.bd)
        print('7')
        upload = []
        sql = """select ent, ind, analisi, motiu, valor 
                from altres.exp_khalix_gida ekg
                where length(ent) = 5"""
        for up, indicador, analisis, motiu, n in u.getAll(sql, 'altres'):
            upload.append((indicador, self.periode, up, analisis, motiu, 'NOIMP', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('8')
        upload = []
        sql = """select entity, indicador, analisi, sum(valor)
                    from altres.exp_khalix_it eki
                    where length(entity) = 5
                    group by entity, indicador, analisi"""
        for up, indicador, analisis, n in u.getAll(sql, 'altres'):
            upload.append((indicador, self.periode, up, analisis, 'NOCAT', 'NOINSAT', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('9')
        upload = []
        sql = """select up, indicador, conc, sum(n) 
                    from altres.exp_khalix_it_up_ind_uba ekiuiu 
                    group by up, indicador, conc"""
        for up, indicador, analisis, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], analisis, 'NOCAT', 'NOINSAT', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('10')
        # upload = []
        # sql = """select indicador, up, tipus, uba, conc, n
        #             from altres.exp_khalix_prof_up ekpu;"""
        # for indicador, up, tipus, uba, analisis, n in u.getAll(sql, 'altres'):
        #     if up in self.centres_ics:
        #         professional = self.centres_ics[up] + tipus + uba
        #         upload.append((indicador, self.periode, professional, analisis, 'NOCAT', 'NOIMP', 'DIM6SET', n))
        # u.listToTable(upload, self.tb, self.bd)
        print('11')
        upload = []
        sql = """select indicador, br, analisis, n
                    from altres.exp_khalix_up_econsulta"""
        for indicador, up, analisis, n in u.getAll(sql, 'altres'):
            upload.append((indicador, self.periode, up, analisis, 'NOCAT', 'NOINSAT', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('12')
        upload = []
        sql = """select indicador, br, analisis, n
                    from altres.exp_khalix_up_econsulta_6m ekuem """
        for indicador, up, analisis, n in u.getAll(sql, 'altres'):
            upload.append((indicador, self.periode, up, analisis, 'NOCAT', 'NOINSAT', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('13')
        upload = []
        sql = """select indicador, up, tipus, recompte 
                from altres.exp_khalix_ag_assir_acc"""
        for indicador, up, analisis, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], analisis, 'NOCAT', 'NOINSAT', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        print('14')
        upload = []
        sql = """select up, indicador, numerador, denominador 
                from altres.exp_khalix_agassdep_up ekau"""
        for up, indicador, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], 'NUM', 'NOCAT', 'NOINSAT', 'DIM6SET', num))
                upload.append((indicador, self.periode, self.centres_ics[up], 'DEN', 'NOCAT', 'NOINSAT', 'DIM6SET', den))
        u.listToTable(upload, self.tb, self.bd)
        print('15')
        upload = []
        sql = """select up, indicador, numerador, denominador 
                from altres.exp_khalix_agassemail_up"""
        for up, indicador, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], 'NUM', 'NOCAT', 'NOINSAT', 'DIM6SET', num))
                upload.append((indicador, self.periode, self.centres_ics[up], 'DEN', 'NOCAT', 'NOINSAT', 'DIM6SET', den))
        u.listToTable(upload, self.tb, self.bd)
        print('16')
        upload = []
        sql = """select up, indicador, tipus_pob, edat, sexe, numerador, denominador
                from altres.exp_khalix_fragilitat"""
        for up, indicador, tipus, edat, sexe, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], 'NUM', edat, tipus, sexe, num))
                upload.append((indicador, self.periode, self.centres_ics[up], 'DEN', edat, tipus, sexe, den))
        u.listToTable(upload, self.tb, self.bd)
        print('17')
        upload = []
        sql = """select indicador, up, edat, sexe, comb, analisi, resultat 
                from altres.exp_khalix_its_indicador"""
        for indicador, up, edat, sexe, comb, analisis, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], analisis, edat, comb, sexe, n))
        u.listToTable(upload, self.tb, self.bd)
        print('18')
        upload = []
        sql = """select up, indicador, numerador, denominador
                from altres.exp_khalix_longass001_up"""
        for up, indicador, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], 'NUM', 'NOCAT', 'NOINSAT', 'DIM6SET', num))
                upload.append((indicador, self.periode, self.centres_ics[up], 'DEN', 'NOCAT', 'NOINSAT', 'DIM6SET', den))
        u.listToTable(upload, self.tb, self.bd)
        print('19')
        upload = []
        sql = """select up, indicador, numerador, denominador
                from altres.exp_khalix_longass002_up"""
        for up, indicador, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], 'NUM', 'NOCAT', 'NOINSAT', 'DIM6SET', num))
                upload.append((indicador, self.periode, self.centres_ics[up], 'DEN', 'NOCAT', 'NOINSAT', 'DIM6SET', den))
        u.listToTable(upload, self.tb, self.bd)
        print('20')
        upload = []
        sql = """select up, indicador, numerador, denominador
                from altres.exp_khalix_nafres_upp_uba"""
        for up, indicador, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], 'NUM', 'NOCAT', 'NOINSAT', 'DIM6SET', num))
                upload.append((indicador, self.periode, self.centres_ics[up], 'DEN', 'NOCAT', 'NOINSAT', 'DIM6SET', den))
        u.listToTable(upload, self.tb, self.bd)
        print('21')
        upload = []
        sql = """select indicador, up, tipus, n 
                from altres.exp_khalix_up_gescasos"""
        for indicador, up, analisis, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], analisis, 'NOCAT', 'NOINSAT', 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)

    def ATDOM(self):
        upload = []
        print('atdom')
        print('1')
        sql = """select ind, entity, analysys, dim6set, valor from atdom.exp_khalix_tests_atdom"""
        for indicador, up, analisis, pob, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], analisis, 'NOCAT', pob, 'DIM6SET', n))
        u.listToTable(upload, self.tb, self.bd)
        upload = []
        print('2')
        # AQUI!!!
        sql = """select ind, entity, analysys, detail, dim6set, valor from atdom.exp_khalix_long_atdom"""
        for indicador, up, analisis, detail, pob, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], analisis, 'NOCAT', detail, pob, n))
        u.listToTable(upload, self.tb, self.bd)
        upload = []
        print('3')
        sql = """select ind, entity, analysys, detail, dim6set, valor from atdom.exp_khalix_cont_atdom"""
        for indicador, up, analisis, detail, pob, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], analisis, 'NOCAT', detail, pob, n))
        u.listToTable(upload, self.tb, self.bd)
    
    def CATSALUT(self):
        print('catsalut')
        print('1')
        upload = []
        sql = """select up, indicador, numerador, denominador from catsalut.exp_khalix_cmbdap03_uba"""
        for up, indicador, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], 'NUM', 'NOCAT', 'NOINSAT', 'DIM6SET', num))
                upload.append((indicador, self.periode, self.centres_ics[up], 'DEN', 'NOCAT', 'NOINSAT', 'DIM6SET', den))
        u.listToTable(upload, self.tb, self.bd)
        upload = []
        print('2')
        sql = """select up, indicador, numerador, denominador from catsalut.exp_khalix_pcc_maca_uba"""
        for up, indicador, num, den in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], 'NUM', 'NOCAT', 'NOINSAT', 'DIM6SET', num))
                upload.append((indicador, self.periode, self.centres_ics[up], 'DEN', 'NOCAT', 'NOINSAT', 'DIM6SET', den))
        u.listToTable(upload, self.tb, self.bd)
        upload = []
        print('3')
        sql = """select up, edat, sexe, comb, indicador, conc, n from catsalut.exp_khalix_up_ind_def"""
        for up, edat, sexe, comb, indicador, conc, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], conc, edat, comb, sexe, n))
        u.listToTable(upload, self.tb, self.bd)
        upload = []
        print('4')
        sql = """select up, edat, sexe, comb, indicador, conc, n from catsalut.exp_khalix_upass_ind"""
        for up, edat, sexe, comb, indicador, conc, n in u.getAll(sql, 'altres'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], conc, edat, comb, sexe, n))
        u.listToTable(upload, self.tb, self.bd)


    def EQA_INDICADORS(self):
        print('eqa_indi')
        print('1')
        upload = []
        sql = """select up, edat, sexe, comb, indicador, conc, valor from eqa_ind.exp_khalix_eqa_urgencies_up"""
        for up, edat, sexe, comb, indicador, conc, n in u.getAll(sql, 'eqa_ind'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], conc, edat, comb, sexe, n))
        u.listToTable(upload, self.tb, self.bd)
        print('2')
        upload = []
        sql = """select up, edat, sexe, comb, indicador, conc, n from eqa_ind.exp_khalix_up_det"""
        for up, edat, sexe, comb, indicador, conc, n in u.getAll(sql, 'eqa_ind'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], conc, edat, comb, sexe, n))
        u.listToTable(upload, self.tb, self.bd)
        print('3')
        upload = []
        sql = """select up, edat, sexe, comb, indicador, conc, n from eqa_ind.exp_khalix_up_ind"""
        for up, edat, sexe, comb, indicador, conc, n in u.getAll(sql, 'eqa_ind'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], conc, edat, comb, sexe, n))
        u.listToTable(upload, self.tb, self.bd)
        
    def GIS(self):
        print('gis')
        upload = []
        sql = """select indicador, br, comb, analisis, n from gis.exp_khalix_up_gis"""
        for indicador, up, detalle, analisis, valor in u.getAll(sql, 'gis'):
            upload.append((indicador, self.periode, up, analisis, 'NOCAT', detalle, 'DIM6SET', valor))
        u.listToTable(upload, self.tb, self.bd)

    def JAIL(self):
        print('jail')
        upload = []
        sql = """select indicador, up, edat, sexe, analisi, sum(valor) 
                    from jail.exp_khalix_iep_uba
                    where up != ''
                    group by indicador, up, edat, sexe, analisi"""
        for indicador, up, edat, sexe, analisi, n in u.getAll(sql, 'jail'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], analisi, edat, 'NOINSAT', sexe, n))
        u.listToTable(upload, self.tb, self.bd)

    def LABORATORI(self):
        print('lab')
        upload = []
        sql = """select entity, edat, sexe, account, analysis, n 
                    from laboratori.exp_khalix ek
                    where length(entity) = 5"""
        for up, edat, sexe, indicador, analisis, valor in u.getAll(sql, 'laboratori'):
            upload.append((indicador, self.periode, up, analisis, 'SEXE', 'NOINSAT', 'DIM6SET', valor))
        u.listToTable(upload, self.tb, self.bd)
    
    def PEDIA(self):
        print('pedia')
        upload = []
        sql = """select up, edat, sexe, comb, indicador, conc, n 
                from pedia.exp_khalix_up_ind"""
        for up, edat, sexe, comb, indicador, analisis, valor in u.getAll(sql, 'laboratori'):
            upload.append((indicador, self.periode, up, analisis, edat, comb, sexe, valor))
        u.listToTable(upload, self.tb, self.bd)

    def ESIAP(self): ## FRANCESC
        print('esiap')
        upload = []
        sql = """select up,indicador, analisi, sum(valor) 
                from esiap.exp_khalix
                group by up, indicador, analisi"""
        for up, indicador, analisis, valor in u.getAll(sql, 'esiap'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], analisis, 'NOCAT', 'NOINSAT', 'DIM6SET', valor))
        u.listToTable(upload, self.tb, self.bd)
    
    def EXTRES(self):
        print('extres')
        upload = []
        sql = """select indicador, up, 
            tipus, edat, comb, sexe, recompte 
            from depart.exp_khalix_departament_up"""
        for indicador, up, tipus, edat, comb, sexe, recompte in u.getAll(sql, 'depart'):
            if up in self.centres_ics:
                upload.append((indicador, self.periode, self.centres_ics[up], tipus, edat, comb, sexe, recompte))
        u.listToTable(upload, self.tb, self.bd)

        upload = []
        sql = """select indicador, periode, up, 
                conc, edat, comb, sexe, n 
                from eqdm2.exp_khalix_up_ind"""
        for indicador, periode, up, conc, edat, comb, sexe, n in u.getAll(sql, 'eqdm2'):
            if up in self.centres_ics:
                upload.append((indicador, periode, self.centres_ics[up], conc, edat, comb, sexe, n))
        u.listToTable(upload, self.tb, self.bd)

        upload = []
        sql = """select up, edat, sexe, comb, n from eqa_ind.exp_khalix_up_pob where comb = 'NOINSAT'"""
        for up, edat, sexe, comb, n in u.getAll(sql, 'eqdm2'):
            if up in self.centres_ics:
                upload.append(('ndm2', periode, self.centres_ics[up], 'DEN', edat, comb, sexe, n))
        u.listToTable(upload, self.tb, self.bd)

if __name__ == "__main__":
    if u.IS_MENSUAL:
        mapa_ap_up()
        u.grantSelect('sisap_master_indicadors_up', 'DWAQUAS', 'exadata')
          




        