# -*- coding: utf-8 -*-

"""
.
"""

import sisapUtils as u
import collections


class Sortida():
    def __init__(self):
        pass
    
    def agregacio(self):
        indicadors_a_fer = set()
        # take all UMI indicators
        sql = """select simbol from umi.netflics_creuament where actiu =1"""
        for indi, in u.getAll(sql, 'umi'):
            indicadors_a_fer.add(indi)
        
        parents_index = {}
        index_fills = collections.defaultdict(list)
        for e in indicadors_a_fer:
            # look for dim_index & sym_index from master_symbol
            sql = """SELECT sym_name, dim_index, sym_index 
                    FROM ICSAP.klx_master_symbol 
                    WHERE SYM_NAME = '{}'""".format(e)
            for name, dim, index in u.getAll(sql, 'khalix'):
                parents_index[name] = (dim, index)
            # si hi ha registres, és a dir si té fills quedar-nos
            # amb dim_index i sym_index de la parent_child
            sql = """SELECT dim_index, SYM_INDEX, parent_name 
                        FROM ICSAP.klx_parent_child
                        WHERE DIM_INDEX = 0 AND 
                        PARENT_INDEX = '{}'""".format(e)
            for dim, fill, parent_name in u.getAll(sql, 'khalix'):
                index_fills[parent_name].append(fill)
            # anar a a la master i buscar els sym_index i dim_index
            sql = """SELECT SYM_NAME FROM 
                    ICSAP.klx_master_symbol 
                    WHERE SYM_INDEX IN (5143,5144,5145) 
                    AND DIM_INDEX = 0"""